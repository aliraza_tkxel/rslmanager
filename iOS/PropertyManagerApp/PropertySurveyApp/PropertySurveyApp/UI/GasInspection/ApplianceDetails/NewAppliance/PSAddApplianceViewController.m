//
//  PSAddApplianceViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddApplianceViewController.h"
#import "PSBarButtonItem.h"
#import "PSAddApplianceTextFieldCell.h"
#import "PSAddAppliancePickerCell.h"
#import "PSAddApplianceCheckBoxCell.h"
#import "PSAddApplianceDatePickerCell.h"

#define kNumberOfRows 9
#define kRowLocation 0
#define kRowType 1
#define kRowMake 2
#define kRowModel 3
#define kRowSerialNumber 4
#define kRowGCNumber 5
#define kRowFluType 6
#define kRowInstalledDate 7
//#define kRowLandlordsProperty 8
#define kRowReplacementDate 8

#define kTextFieldCellHeight 44
#define kPickerCellHeight 44

static NSString *textFieldCellIdentifier = @"addApplianceTextFieldCellIdentifier";
static NSString *pickerCellIdentifier = @"addApliancePickerCellIdentifier";
static NSString *datePickerCellIdentifier = @"addApplianceDatePickerCellIdentifier";
static NSString *checkBoxCellIdentifier = @"addApplianceCheckBoxCellIdentifier";

@interface PSAddApplianceViewController ()
{
    
    NSMutableArray *cellsArray;
    
    NSString *_serialNumber;
    NSString *_gcNumber;
    
    NSString *_location;
    NSNumber *_locationId;
    
    NSString *_model;
    NSNumber *_modelId;
    
    NSString *_type;
    NSNumber *_typeId;
    
    NSString *_make;
    NSNumber *_makeId;
    
    NSString *_fluType;
    NSDate * _installedDate;
    NSDate *_replacementDate;
    BOOL _isLandlordProperty;
    NSInteger _pickerTag;
    NSInteger _textFieldTag;
    NSMutableDictionary *_applianceDictionary;
    
    NSArray *_manufacturers;
    NSArray *_types;
    NSArray *_locations;
    NSArray *_flueArray;
    NSArray *_models;
    
    NSMutableArray *_locationStrings;
    NSMutableArray *_typeStrings;
    NSMutableArray *_manufecturerStrings;
    NSMutableArray *_modelStrings;
    
    ApplianceLocation *_applianceLocation;
    ApplianceType *_applianceType;
    ApplianceManufacturer *_applianceManufacturer;
    ApplianceModel *_applianceModel;
}

@end

@implementation PSAddApplianceViewController

int appliancePickerOptionsSortingFunction(id item1, id item2, void * context){
    NSString *firstItem = [(NSString *)item1 lowercaseString];
    NSString *secondItem = [(NSString *)item2 lowercaseString];
    return [firstItem  compare:secondItem];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _applianceDictionary = [NSMutableDictionary dictionary];
		[self setupLayouts];
    cellsArray = [NSMutableArray array];
    _manufacturers = [[PSApplianceManager sharedManager] fetchApplianceManufacturers];
    _types = [[PSApplianceManager sharedManager] fetchApplianceTypes];
    _locations = [[PSApplianceManager sharedManager] fetchApplianceLoactions];
    _models = [[PSApplianceManager sharedManager] fetchApplianceModels];
 
    _locationStrings = [NSMutableArray array];
    _typeStrings = [NSMutableArray array];
    _manufecturerStrings = [NSMutableArray array];
    _modelStrings = [NSMutableArray array];
    
    _flueArray = @[@"FL", @"OF", @"RS"];
    
		NSSortDescriptor *sortDescriptor;
		sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"locationName"
																								 ascending:YES
																									selector:@selector(localizedCaseInsensitiveCompare:)];
		NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _locations = [_locations sortedArrayUsingDescriptors:sortDescriptors];
    //CLS_LOG(@"sorted array: %@", _locations);
    
    for (ApplianceLocation *location in _locations)
    {
        if (!isEmpty(location.locationName)) {
            [_locationStrings addObject:location.locationName];
        }
    }
    
		sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"typeName"
																								 ascending:YES
																									selector:@selector(localizedCaseInsensitiveCompare:)];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _types = [_types sortedArrayUsingDescriptors:sortDescriptors];
    //CLS_LOG(@"sorted array : %@", _types);

    
    for (ApplianceType *type in _types)
    {
        if (!isEmpty(type.typeName)) {
            [_typeStrings addObject:type.typeName];
        }
    }
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"manufacturerName"
                                                 ascending:YES
																									selector:@selector(localizedCaseInsensitiveCompare:)];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _manufacturers = [_manufacturers sortedArrayUsingDescriptors:sortDescriptors];
    //CLS_LOG(@"sorted array: %@", _manufacturers);
    
    for (ApplianceManufacturer *manufecturer in _manufacturers)
    {
        if (!isEmpty(manufecturer.manufacturerName)) {
            [_manufecturerStrings addObject:manufecturer.manufacturerName];
        }
    }
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"modelName"
                                                 ascending:YES
																									selector:@selector(localizedCaseInsensitiveCompare:)];
    sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _models = [_models sortedArrayUsingDescriptors:sortDescriptors];
    //CLS_LOG(@"sorted array: %@", _models);
    
    for (ApplianceModel *applianceModel in _models)
    {
        if (!isEmpty(applianceModel.modelName)) {
            [_modelStrings addObject:applianceModel.modelName];
        }
    }
    
    if (!isEmpty(self.appliance))
    {
        _applianceLocation = self.appliance.applianceLocation;
        _applianceType = self.appliance.applianceType;
        _applianceManufacturer = self.appliance.applianceManufacturer;
        _applianceModel = self.appliance.applianceModel;
        
        _location = _applianceLocation.locationName;
        _locationId = _applianceLocation.locationID;

        _type = _applianceType.typeName;
        _typeId = _applianceType.typeID;

        _make = _applianceManufacturer.manufacturerName;
        _makeId = _applianceManufacturer.manufacturerID;
        
        _model = _applianceModel.modelName;
        _modelId = _applianceModel.modelID;
        
        
        _fluType = self.appliance.fluType;
        _installedDate = self.appliance.installedDate;
        _replacementDate = self.appliance.replacementDate;
        _serialNumber = self.appliance.serialNumber;
        _gcNumber = self.appliance.gcNumber;
        _isLandlordProperty = [self.appliance.isLandlordAppliance boolValue];
    }
    else {
        _location = nil;
        _locationId = [NSNumber numberWithInt:-1];;
        
        _type = nil;
        _typeId = [NSNumber numberWithInt:-1];;
        
        _make = nil;
        _makeId = [NSNumber numberWithInt:-1];;
        
        _model = nil;
        _modelId = [NSNumber numberWithInt:-1];;
    }
    
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerKeyboardNotifications];
    [self registerNotification];
    [self.tableView reloadData];
    [self performSelector:@selector(scrollToBottom) withObject:nil afterDelay:0.5];
}

-(void) scrollToBottom
{
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:8 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deregisterKeyboardNotifications];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    if (!isEmpty(self.appliance)) {
        [self setTitle:@"Appliance"];
    }
    else
    {
        [self setTitle:@"Add Appliance"];
    }
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    [self resignAllResponders];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:8 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    //if(IS_NETWORK_AVAILABLE)
    //{
    [self validateForm];
    //}
//    else
//    {
//        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
//                                                       message:LOC(@"KEY_ALERT_NO_INTERNET")
//                                                      delegate:nil
//                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
//                                             otherButtonTitles:nil,nil];
//        
//        [alert show];
//    }
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 44;
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.row == kRowSerialNumber || indexPath.row == kRowGCNumber)
    {
        PSAddApplianceTextFieldCell *_cell = [self createCellWithIdentifier:textFieldCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
        
    }
//    else if (indexPath.row == kRowLandlordsProperty)
//    {
//        PSAddApplianceCheckBoxCell *_cell = [self createCellWithIdentifier:checkBoxCellIdentifier];
//        [self configureCell:&_cell atIndexPath:indexPath];
//        cell = _cell;
//
//    }
    else if (indexPath.row == kRowInstalledDate || indexPath.row == kRowReplacementDate)
    {
        PSAddApplianceDatePickerCell *_cell = [self createCellWithIdentifier:datePickerCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;

    }
    else
    {
        PSAddAppliancePickerCell *_cell = [self createCellWithIdentifier:pickerCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
	if([identifier isEqualToString:textFieldCellIdentifier])
	{
        PSAddApplianceTextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:textFieldCellIdentifier];
        if(_cell == nil)
        {
            
            [[NSBundle mainBundle] loadNibNamed:@"PSAddApplianceTextFieldCell" owner:self options:nil];
            _cell = self.addApplianceTextFieldCell;
            self.addApplianceTextFieldCell = nil;
            [cellsArray addObject:_cell];
           
        }
		return _cell;
	}
    else if ([identifier isEqualToString:pickerCellIdentifier])
    {
        PSAddAppliancePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:pickerCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAddAppliancePickerCell" owner:self options:nil];
            _cell = self.addAppliancePickerCell;
            self.addAppliancePickerCell = nil;
            [cellsArray addObject:_cell];
            
        }
		return _cell;
    }
    
    else if ([identifier isEqualToString:checkBoxCellIdentifier])
    {
        PSAddApplianceCheckBoxCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:checkBoxCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAddApplianceCheckBoxCell" owner:self options:nil];
            _cell = self.addApplianceCheckBoxCell;
            self.addApplianceCheckBoxCell = nil;
            [cellsArray addObject:_cell];
        }
		return _cell;
    }
    
    else if ([identifier isEqualToString:datePickerCellIdentifier])
    {
        PSAddApplianceDatePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:datePickerCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAddApplianceDatePickerCell" owner:self options:nil];
            _cell = self.addApplianceDatePickerCell;
            self.addApplianceDatePickerCell = nil;
            [cellsArray addObject:_cell];
        }
		return _cell;
    }
	return nil;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSAddApplianceTextFieldCell class]])
    {
        PSAddApplianceTextFieldCell *_cell = (PSAddApplianceTextFieldCell *)*cell;
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.txtTitle setDelegate:self];
        switch (indexPath.row) {
            case kRowSerialNumber:
                [_cell.lblHeader setText:LOC(@"KEY_STRING_SERIAL_NUMBER")];
                [_cell.txtTitle setTag:kRowSerialNumber];
                if (!isEmpty(_serialNumber))
                {
                    [_cell.txtTitle setText:_serialNumber];
                }
                [_cell.txtTitle setPlaceholder:@"Serial Number"];
                break;
            case kRowGCNumber:
                [_cell.lblHeader setText:LOC(@"KEY_STRING_GC_NUMBER")];
                [_cell.txtTitle setTag:kRowGCNumber];
                if (!isEmpty(_gcNumber))
                {
                    [_cell.txtTitle setText:_gcNumber];
                }
                [_cell.txtTitle setPlaceholder:@"GC Number"];
                break;

            default:
                break;
        }
        *cell = _cell;
    }

    else if ([*cell isKindOfClass:[PSAddAppliancePickerCell class]])
    {
        PSAddAppliancePickerCell *_cell = (PSAddAppliancePickerCell *)*cell;
        [_cell setDelegate:self];
        [_cell.txtTitle setDelegate:self];
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.btnEdit setHidden:NO];
        [_cell.txtTitle setEnabled:YES];
        switch (indexPath.row) {
            case kRowLocation:
                [_cell.txtTitle setTag:kRowLocation];
                [_cell setPickerOptions:_locationStrings];
                [_cell.lblHeader setText:LOC(@"KEY_STRING_LOCATION")];
                if (!isEmpty(_location))
                {
                    [_cell.txtTitle setText:_location];
                }
                [_cell.btnPicker addTarget:self action:@selector(onClickLocationPicker:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.btnEdit addTarget:self action:@selector(onClickEditLocation:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.txtTitle setPlaceholder:@"Location"];
                
                break;
            case kRowType:
                
                [_cell.txtTitle setTag:kRowType];
                [_cell setPickerOptions:_typeStrings];
                [_cell.lblHeader setText:LOC(@"KEY_STRING_TYPE")];
                if (!isEmpty(_type))
                {
                    [_cell.txtTitle setText:_type];
                }
                [_cell.btnPicker addTarget:self action:@selector(onClickTypePicker:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.btnEdit addTarget:self action:@selector(onClickEditType:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.txtTitle setPlaceholder:@"Type"];
                break;
            case kRowMake:
                [_cell.txtTitle setTag:kRowMake];
                [_cell setPickerOptions:_manufecturerStrings];
                [_cell.lblHeader setText:LOC(@"KEY_STRING_MAKE")];
                if (!isEmpty(_make))
                {
                    [_cell.txtTitle setText:_make];
                }
                [_cell.btnPicker addTarget:self action:@selector(onClickMakePicker:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.btnEdit addTarget:self action:@selector(onClickEditMake:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.txtTitle setPlaceholder:@"Make"];
                break;
            case kRowFluType:
                [_cell.lblHeader setText:LOC(@"KEY_STRING_FLUE_TYPE")];
                _cell.pickerOptions = _flueArray;
                [_cell.btnEdit setHidden:YES];
                [_cell.txtTitle setUserInteractionEnabled:NO];
                [_cell.txtTitle setText:_fluType];
                [_cell.btnPicker addTarget:self action:@selector(onCLickFluPicker:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case kRowModel:
                [_cell.lblHeader setText:LOC(@"KEY_STRING_MODEL")];
                _cell.pickerOptions = _modelStrings;
                [_cell.txtTitle setTag:kRowModel];
                if (!isEmpty(_model)) {
                    [_cell.txtTitle setText:_model];
                }
                [_cell.btnPicker addTarget:self action:@selector(onClickModelPicker:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.txtTitle setPlaceholder:@"Model"];
                break;
            default:
                break;
        }
        *cell = _cell;
    }
    else if ([*cell isKindOfClass:[PSAddApplianceDatePickerCell class]])
    {
        PSAddApplianceDatePickerCell *_cell = (PSAddApplianceDatePickerCell *)*cell;
        [_cell setDelegate:self];
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        switch (indexPath.row)
        {
            case kRowInstalledDate:
                [_cell.lblHeader setText:LOC(@"KEY_STRING_INSTALLED_DATE")];
                [_cell.lblTitle setText:nil];
                [_cell.resetDatesButton setHidden:YES];
                if (!isEmpty(_installedDate))
                {
                    [_cell.lblTitle setText:[UtilityClass stringFromDate:_installedDate dateFormat:kDateTimeStyle8]];
                    [_cell.resetDatesButton setHidden:NO];
                }
                [_cell.btnPicker addTarget:self action:@selector(onClickInstalledDate:) forControlEvents:UIControlEventTouchUpInside];
                //[_cell.resetDatesButton addTarget:self action:@selector(onClickResetInstalledDate:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case kRowReplacementDate:
                [_cell.lblHeader setText:LOC(@"KEY_STRING_REPLACEMENT_DATE")];
                [_cell.lblTitle setText:nil];
                [_cell.resetDatesButton setHidden:YES];
                if (!isEmpty(_replacementDate))
                {
                    [_cell.lblTitle setText:[UtilityClass stringFromDate:_replacementDate dateFormat:kDateTimeStyle8]];
                    [_cell.resetDatesButton setHidden:NO];
                }
                [_cell.btnPicker addTarget:self action:@selector(onClickReplacedDate:) forControlEvents:UIControlEventTouchUpInside];
               // [_cell.resetDatesButton addTarget:self action:@selector(onClickResetReplacedDate:) forControlEvents:UIControlEventTouchUpInside];
                break;
            default:
                break;
        }
        *cell = _cell;
    }
    else if ([*cell isKindOfClass:[PSAddApplianceCheckBoxCell class]])
    {
        PSAddApplianceCheckBoxCell *_cell = (PSAddApplianceCheckBoxCell *)*cell;
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell setDelegate:self];
        if (!isEmpty(self.appliance))
        {
            if (_isLandlordProperty) {
                [_cell makeSelected];
            }
        }
        *cell = _cell;
    }
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickInstalledDate:(id)sender
{
    CLS_LOG(@"onClickInstalledDate");
    [self resignAllResponders];
    _pickerTag = kRowInstalledDate;
}

- (IBAction)onClickReplacedDate:(id)sender
{
    CLS_LOG(@"onClickReplacedDate");
    [self resignAllResponders];
    _pickerTag = kRowReplacementDate;
}

- (IBAction)onClickLocationPicker:(id)sender
{
    CLS_LOG(@"onClickLocationPicker");
    [self resignAllResponders];
    _pickerTag = kRowLocation;
}
- (IBAction)onClickTypePicker:(id)sender
{
    CLS_LOG(@"onClickTypePicker");
    [self resignAllResponders];
    _pickerTag = kRowType;
}
- (IBAction)onClickMakePicker:(id)sender
{
    CLS_LOG(@"onClickMakePicker");
    [self resignAllResponders];
    _pickerTag = kRowMake;
}

- (IBAction)onClickModelPicker:(id)sender
{
    CLS_LOG(@"onClickMakePicker");
    [self resignAllResponders];
    _pickerTag = kRowModel;
}
- (IBAction)onCLickFluPicker:(id)sender
{
    CLS_LOG(@"onCLickFluPicker");
    [self resignAllResponders];
    _pickerTag = kRowFluType;
}

- (IBAction)onClickEditLocation:(id)sender
{
    CLS_LOG(@"onClickEditLocation");
    
}

- (IBAction)onClickEditType:(id)sender
{
    CLS_LOG(@"onClickEditType");
}

- (IBAction)onClickEditMake:(id)sender
{
    CLS_LOG(@"onClickEditMake");
}



- (IBAction)onClickDeleteAppliance:(id)sender {
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_DELETE_APPLIANCE")
																								 message:LOC(@"KEY_ALERT_CONFIRM_DELETE_APPLIANCE")
																								delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_NO")
																			 otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
	alert.tag = AlertViewTagMarkDelete;
	[alert show];

}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == AlertViewTagMarkDelete)
	{
		if(buttonIndex == alertView.firstOtherButtonIndex)
		{
			
			if (!isEmpty(self.appliance))
			{
				[_applianceDictionary setObject:self.appliance.applianceID forKey:kApplianceId];
				[_applianceDictionary setObject:_appliance.objectID forKey:@"applianceObjectId"];
			}
			else
			{
				[_applianceDictionary setObject:[NSNumber numberWithInt:-1] forKey:kApplianceId];
			}

			[_applianceDictionary setObject:[NSNumber numberWithBool:NO] forKey:kApplianceIsActive];
			[self performSelector:@selector(deleteAppliance:) withObject:_applianceDictionary afterDelay:0.5];
			
		}
	}
}

- (void) deleteAppliance:(NSDictionary *)appliance
{
	[[PSDataPersistenceManager sharedManager] deleteApplianceData:appliance forProperty:self.appointment.appointmentToProperty.objectID];
}


#pragma mark - Scroll Cell Methods
- (void)scrollToRectOfSelectedControl {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignAllResponders];
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case kRowSerialNumber: {
            _serialNumber = textField.text;
        }break;
        case kRowGCNumber: {
            _gcNumber = textField.text;
        }break;
        case kRowModel: {
            ApplianceModel  *model = nil;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.modelName LIKE[cd]%@",[[textField.text lowercaseString]trimString]];
            NSArray *filteredArray = [_models filteredArrayUsingPredicate:predicate];
            if (!isEmpty(filteredArray))
            {
                model = [filteredArray firstObject];
            }
            
            if (isEmpty(model))
            {
                _model = textField.text;
                _modelId = [NSNumber numberWithInt:-1];
            }
            else
            {
                _model = model.modelName;
                _modelId = model.modelID;

                [self.tableView reloadData];
            }

            
            
        } break;
        case kRowLocation:{
            ApplianceLocation *location = nil;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.locationName LIKE[cd]%@",[[textField.text lowercaseString]trimString]];
            NSArray *arr = [_locations filteredArrayUsingPredicate:predicate];
            if (!isEmpty(arr))
            {
                location = [arr firstObject];
            }
            
            if (isEmpty(location))
            {
                _location = textField.text;
                _locationId = [NSNumber numberWithInt:-1];
            }
            else
            {
                _location = location.locationName;
                _locationId = location.locationID;

                [self.tableView reloadData];
            }
        }break;
        case kRowType: {
            ApplianceType *type = nil;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.typeName LIKE[cd]%@",[[textField.text lowercaseString]trimString]];
            NSArray *filteredArray = [_types filteredArrayUsingPredicate:predicate];
            if (!isEmpty(filteredArray))
            {
                type = [filteredArray firstObject];
            }
            
            if (isEmpty(type))
            {
                _type = textField.text;
                _typeId = [NSNumber numberWithInt:-1];
            }
            else
            {
                _type = type.typeName;
                _typeId = type.typeID;
                [self.tableView reloadData];
            }

            
            
        }break;
        case kRowMake: {
             ApplianceManufacturer  *manufacturer = nil;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.manufacturerName LIKE[cd]%@",[[textField.text lowercaseString]trimString]];
            NSArray *filteredArray = [_manufacturers filteredArrayUsingPredicate:predicate];
            if (!isEmpty(filteredArray))
            {
                manufacturer = [filteredArray firstObject];
            }
            
            if (isEmpty(manufacturer))
            {
                _make = textField.text;
                _makeId = [NSNumber numberWithInt:-1];
            }
            else
            {
                _make = manufacturer.manufacturerName;
                _makeId = manufacturer.manufacturerID;
                [self.tableView reloadData];
            }
            
        } break;
        default:
            break;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL textFieldShouldReturn = YES;
    NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
    switch (textField.tag) {
        case kRowSerialNumber:
            if ([stringAfterReplacement length] > 50)
            {
                textFieldShouldReturn = NO;
            }
            break;
        case kRowGCNumber:
            if ([stringAfterReplacement length] > 10)
            {
                textFieldShouldReturn = NO;
            }
            break;
        case kRowLocation:
        {
            if ([stringAfterReplacement length] > 20)
            {
                textFieldShouldReturn = NO;
            }
        }
            break;
        case kRowType:
        {
            if ([stringAfterReplacement length] > 100)
            {
                textFieldShouldReturn = NO;
            }
        }
            break;
        case kRowMake:
        {
            if ([stringAfterReplacement length] > 50)
            {
                textFieldShouldReturn = NO;
            }
        }
            break;
        case kRowModel:
        {
            if ([stringAfterReplacement length] > 50)
            {
                textFieldShouldReturn = NO;
            }
        }
            break;
        default:
            break;
    }
    return textFieldShouldReturn;
}

#pragma mark - Protocol Methods

/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSInteger)pickerOption
{
    switch (_pickerTag)
    {
        case kRowLocation: {
            _applianceLocation = [_locations objectAtIndex:pickerOption];
            _location = _applianceLocation.locationName;
            _locationId = _applianceLocation.locationID;
        }
            break;
        case kRowType: {
            _applianceType = [_types objectAtIndex:pickerOption];
            _type = _applianceType.typeName;
            _typeId = _applianceType.typeID;
        }
            break;
        case kRowMake: {
            _applianceManufacturer = [_manufacturers objectAtIndex:pickerOption];
            _make = _applianceManufacturer.manufacturerName;
            _makeId = _applianceManufacturer.manufacturerID;

        }
            break;
        case kRowFluType: {
            _fluType = [_flueArray objectAtIndex:pickerOption];
        }
            break;
        case kRowModel: {
            _applianceModel = [_models objectAtIndex:pickerOption];
            _model = _applianceModel.modelName;
            _modelId = _applianceModel.modelID;
        }
            break;
        default:
            break;
    }
    [self.tableView reloadData];
}

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption
{
    if (!isEmpty(pickerOption))
    {
        switch (_pickerTag)
        {
            case kRowInstalledDate:
                _installedDate = pickerOption;
                break;
            case kRowReplacementDate:
                _replacementDate = pickerOption;
                break;
            default:
                break;
        }
        //[self.tableView reloadData];
    }
}

/*!
 @discussion
 Method didSelectConfirmationButton Called upon selection of landlord appliance.
 */
- (void) didSelectConfirmationButton:(BOOL)pickerOption
{
    _isLandlordProperty = pickerOption;
}

#pragma mark - Resign All Responders
- (void) resignAllResponders
{
    [self.view endEditing:YES];
}

#pragma mark - Notifications

- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveApplianceSuccessNotification) name:kSaveNewAppliacneSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveApplianceFailureNotification) name:kSaveNewAppliacneFailureNotification object:nil];
	  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveDeleteApplianceSuccessNotification) name:kDeleteApplianceSuccessNotification object:nil];
	  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveDeleteApplianceFailureNotification) name:kDeleteApplianceFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveNewAppliacneSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveNewAppliacneFailureNotification object:nil];
	  [[NSNotificationCenter defaultCenter] removeObserver:self name:kDeleteApplianceSuccessNotification object:nil];
	  [[NSNotificationCenter defaultCenter] removeObserver:self name:kDeleteApplianceFailureNotification object:nil];
}

- (void) onReceiveDeleteApplianceSuccessNotification
{
	[self hideActivityIndicator];
	[self popOrCloseViewController];
}

- (void) onReceiveDeleteApplianceFailureNotification
{
	[self hideActivityIndicator];
	
	NSString *alertMessage = LOC(@"KEY_ALERT_FAILED_TO_DELETE_APPLIANCE");
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																								 message:alertMessage
																								delegate:nil
																			 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																			 otherButtonTitles:nil,nil];
	[alert show];
	[self enableNavigationBarButtons];
}

- (void) onReceiveSaveApplianceSuccessNotification
{
    [self hideActivityIndicator];
    [self popOrCloseViewController];
}

- (void) onReceiveSaveApplianceFailureNotification
{
    [self hideActivityIndicator];
    
    NSString *alertMessage = LOC(@"KEY_ALERT_FAILED_TO_SAVE_APPLIANCE");
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                   message:alertMessage
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
    [self enableNavigationBarButtons];
}

#pragma mark - Methods

- (void) setupLayouts
{
	[self loadNavigationonBarItems];
	[self setupDeleteButton];
}

- (void) setupDeleteButton
{
	self.btnDeleteContainerView.backgroundColor = [UIColor clearColor];
	if (!isEmpty(self.appliance)) {
		self.btnDeleteContainerView.hidden = false;
	}
	else
	{
		self.btnDeleteContainerView.hidden = true;
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
	}
}



- (void) validateForm
{
    NSString *alertMessage = nil;
    NSString *emptyField;
    BOOL inValid = NO;
    BOOL isEndDateValid = NO;
    
    
    for(int row = 0 ; row < kNumberOfRows ; row++)
    {
        UITableViewCell * cell = [cellsArray objectAtIndex:row];
        
        if (row == kRowSerialNumber || row == kRowGCNumber)
        {
            PSAddApplianceTextFieldCell *_cell = (PSAddApplianceTextFieldCell*) cell;
            
            if(row == kRowSerialNumber)
            {
                if(_cell.txtTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select serial number.";
                    break;
                    
                }
            }
            if(row == kRowGCNumber)
            {
                if(_cell.txtTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select GC number.";
                    break;
                }
            }
            
        }
        
        else if (row == kRowInstalledDate || row == kRowReplacementDate)
        {
            PSAddApplianceDatePickerCell *_cell = (PSAddApplianceDatePickerCell*) cell;
            if(row == kRowInstalledDate)
            {
                if(_cell.lblTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select installed date";
                    break;
                }
            }
            if(row == kRowReplacementDate)
            {
                if(_cell.lblTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select replacement date.";
                    break;
                }
                else
                {
                    if(![_replacementDate isLaterThanDateIgnoringTime:_installedDate])
                    {
                        inValid = YES;
                        alertMessage = LOC(@"KEY_ALERT_REPLACEMENT_DATE_INVALID");
                        break;
                    }
                }
            }
            
        }
        else
        {
            PSAddAppliancePickerCell *_cell = (PSAddAppliancePickerCell*) cell;
            if(row == kRowLocation)
            {
                if(_cell.txtTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select location.";
                    break;
                }
            }
            if(row == kRowType)
            {
                if(_cell.txtTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select type.";
                    break;
                }
            }
            if(row == kRowMake)
            {
                if(_cell.txtTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select make.";
                    break;
                }
            }
            if(row == kRowModel)
            {
                if(_cell.txtTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select model.";
                    break;
                }
            }
            if(row == kRowFluType)
            {
                if(_cell.txtTitle.text.length <= 0)
                {
                    inValid = YES;
                    alertMessage = @"Please select flu type.";
                    break;
                }
            }
            
        }
    }
    
    if (inValid)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                       message:alertMessage
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
    }
    else
    {
        [self saveAppliance];
    }
    
    return;
    
//#warning if you want to check all fields data then loop should be from 0 to kNumberOfRows 
//    
//    for (int cellNumber = 0; cellNumber < 4; ++cellNumber)
//        {
//            
//        #warning We are excluding Type and Model as NonMandatory fields, Changes from client in "PM Attributes-Heating" Estimates for application version (1.1)1.4.11
//            if(cellNumber == kRowType || cellNumber == kRowModel) {
//                continue;
//            }
//            
//            
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellNumber inSection:0];
//            UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
//            emptyField = nil;
//            alertMessage = nil;
//            inValid = NO;
//            
//            if ([cell  isKindOfClass:[PSAddApplianceTextFieldCell class]]) {
//                PSAddApplianceTextFieldCell *_cell = (PSAddApplianceTextFieldCell *)cell;
//                if (![_cell isValid])
//                {
//                    emptyField = [_cell.lblHeader.text lowercaseString];
//                    inValid = YES;
//                }
//            }
//            
//            else if ([cell isKindOfClass:[PSAddAppliancePickerCell class]])
//            {
//                PSAddAppliancePickerCell *_cell = (PSAddAppliancePickerCell *)cell;
//                if (![_cell isValid])
//                {
//                    emptyField = [_cell.lblHeader.text lowercaseString];
//                    inValid = YES;
//                }
//            }
//            
//            else if ([cell isKindOfClass:[PSAddApplianceDatePickerCell class]])
//            {
//                PSAddApplianceDatePickerCell *_cell = (PSAddApplianceDatePickerCell *)cell;
//                if (![_cell isValid])
//                {
//                    emptyField = [_cell.lblHeader.text lowercaseString];
//                    inValid = YES;
//                }
//            }
//            if (inValid)
//            {
//                alertMessage = [NSString stringWithFormat:@"Please select %@.", emptyField];
//                break;
//            }
//        }
//    
//    if (!inValid)
//    {
//        if(!isEmpty(_installedDate) && !isEmpty(_replacementDate)) {
//            isEndDateValid = [_replacementDate isLaterThanDateIgnoringTime:_installedDate];
//        }
//        else if(!isEmpty(_replacementDate)) {
//            isEndDateValid = YES;
//        }
//        if (!isEndDateValid)
//        {
//            alertMessage = LOC(@"KEY_ALERT_REPLACEMENT_DATE_INVALID");
//            inValid = NO;
//        }
//        else
//        {
//            inValid = NO;
//        }
//    }
//    // }
//    if (inValid)
//    {
//        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
//                                                       message:alertMessage
//                                                      delegate:nil
//                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
//                                             otherButtonTitles:nil,nil];
//        [alert show];
//    }
//    else
//    {
//        [self saveAppliance];
//    }
}

- (void) saveAppliance
{
    [self disableNavigationBarButtons];
    //[self showActivityIndicator:LOC(@"KEY_STRING_SAVING_APPLIANCE")];
    NSMutableDictionary *manufecturerDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *locationDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *typeDictionary = [NSMutableDictionary dictionary];
    NSMutableDictionary *modelDictionary = [NSMutableDictionary dictionary];
    
    [locationDictionary setObject:_location forKey:kApplianceLocationName];
    [locationDictionary setObject:_locationId forKey:kApplianceLocationID];
    
    
    [manufecturerDictionary setObject:_make forKey:kApplianceManufacturerName];
    [manufecturerDictionary setObject:_makeId forKey:kApplianceManufacturerID];
    
    
    [typeDictionary setObject:isEmpty(_type)?[NSNull null]:_type forKey:kApplianceTypeName];
    [typeDictionary setObject:isEmpty(_typeId)?[NSNull null]:_typeId forKey:kApplianceTypeID];
    
    [modelDictionary setObject:isEmpty(_model)?[NSNull null]:_model forKey:kApplianceModelName];
    [modelDictionary setObject:isEmpty(_modelId)?[NSNull null]:_modelId forKey:kApplianceModelID];

    BOOL isLandlordsAppliance;
	
    #warning Removing IsLandlord Appliance option From UI Changes from client in "PM Attributes-Heating" Estimates for application version (1.1)1.4.11, Just sending False value to server
     isLandlordsAppliance = NO;
    
    
    NSString *serverInstalledDate;
    NSString *serverReplacementDate;
    
    if (!isEmpty(_installedDate))
    {
        serverInstalledDate = [UtilityClass convertNSDateToServerDate:_installedDate];
    }
    if (!isEmpty(_replacementDate))
    {
        serverReplacementDate = [UtilityClass convertNSDateToServerDate:_replacementDate];
    }
    if (!isEmpty(self.appliance))
    {
        [_applianceDictionary setObject:self.appliance.applianceID forKey:kApplianceId];
        [_applianceDictionary setObject:_appliance.objectID forKey:@"applianceObjectId"];
    }
    else
    {
        [_applianceDictionary setObject:[NSNumber numberWithInt:-1] forKey:kApplianceId];
        
    }
	
    [_applianceDictionary setObject:[NSNumber numberWithBool:YES] forKey:kApplianceIsActive];
    [_applianceDictionary setObject:isEmpty(_fluType)?[NSNull null]:_fluType forKey:kApplianceFlueType];
    [_applianceDictionary setObject:isEmpty(serverInstalledDate)?[NSNull null]:serverInstalledDate forKey:kApplianceInstalledDate];
    [_applianceDictionary setObject:isEmpty(serverReplacementDate)?[NSNull null]:serverReplacementDate forKey:kApplianceReplacementDate];
    
    [_applianceDictionary setObject:isEmpty(self.appointment.appointmentToProperty.propertyId)?[NSNull null]:self.appointment.appointmentToProperty.propertyId forKey:kPropertyId];
    [_applianceDictionary setObject:isEmpty(self.appointment.appointmentToProperty.schemeId)?[NSNull null]:self.appointment.appointmentToProperty.schemeId forKey:kSchemeID];
    [_applianceDictionary setObject:isEmpty(self.appointment.appointmentToProperty.blockId)?[NSNull null]:self.appointment.appointmentToProperty.blockId forKey:kSchemeBlockID];
    
    [_applianceDictionary setObject:isEmpty(_serialNumber)?[NSNull null]:_serialNumber forKey:kApplianceSerialNumber];
    [_applianceDictionary setObject:isEmpty(_gcNumber)?[NSNull null]:_gcNumber forKey:kApplianceGCNumber];
    [_applianceDictionary setObject:[NSNumber numberWithBool:isLandlordsAppliance] forKey:kApplianceIsLandlordAppliance];
    [_applianceDictionary setObject:[NSNumber numberWithBool:NO] forKey:kApplianceIsInspected];
    [_applianceDictionary setObject:locationDictionary forKey:@"ApplianceLocation"];
    [_applianceDictionary setObject:manufecturerDictionary forKey:@"ApplianceManufacturer"];
    [_applianceDictionary setObject:modelDictionary forKey:@"ApplianceModel"];
    [_applianceDictionary setObject:typeDictionary forKey:@"ApplianceType"];
    
    NSArray *locations = @[locationDictionary];
    NSArray *types = @[typeDictionary];
    NSArray *manufecturers = @[manufecturerDictionary];
    NSArray *models = @[modelDictionary];
    
    [[PSApplianceManager sharedManager] saveAppliancePropertiesOffline:locations typeArray:types manufecturerArray:manufecturers modelArray:models];
    
    [self performSelector:@selector(saveApplianceOnline:) withObject:_applianceDictionary afterDelay:0.5];

}

- (void) saveApplianceOnline:(NSDictionary *)appliance
{
#warning PSA: online appliance saving disabled. uncomment below line to activate service.
//    [[PSApplianceManager sharedManager] saveAppliance:appliance forProperty:self.appointment.appointmentToProperty.objectID];
    [[PSDataPersistenceManager sharedManager]saveApplianceData:@[appliance] forProperty:self.appointment.appointmentToProperty.objectID isAddedOffline:YES];
    [self schduleUIControlsActivation];
}

- (void) disableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    if (!isEmpty(navigationBarRightButtons))
    {
        for (PSBarButtonItem *button in navigationBarRightButtons)
        {
            button.enabled = NO;
        }
    }
    
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    if (!isEmpty(navigationBarLeftButtons))
    {
        for (PSBarButtonItem *button in navigationBarLeftButtons)
        {
            button.enabled = NO;
        }
    }
}

- (void) enableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    if (!isEmpty(navigationBarRightButtons))
    {
        for (PSBarButtonItem *button in navigationBarRightButtons) {
            button.enabled = YES;
        }
    }
    
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    
    if (!isEmpty(navigationBarLeftButtons))
    {
        for (PSBarButtonItem *button in navigationBarLeftButtons)
        {
            button.enabled = YES;
        }
    }
}
@end
