//
//  PSAddAppointmentDatePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddApplianceDatePickerCell.h"

@interface PSAddApplianceDatePickerCell()

@property (nonatomic, retain) ActionSheetDatePicker * picker;

@end
@implementation PSAddApplianceDatePickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onclickPickerBtn:(id)sender {
    if(self.selectedDate == nil)
    {
        self.selectedDate = [NSDate date];
    }
    
    self.picker = [ActionSheetDatePicker showPickerWithTitle:self.lblHeader.text
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:self.selectedDate
                                        target:self
                                        action:@selector(onDateSelected:element:)
                                        origin:sender];
}

- (void)onDateSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectDate:)])
    {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *strDate = [dateFormatter stringFromDate:self.selectedDate];
        [self.lblTitle setText:strDate];
        [(PSAddApplianceViewController *)self.delegate didSelectDate:self.selectedDate];
    }
    
}
- (IBAction)resetButtontapped:(id)sender {
    
    [self.lblTitle setText:@""];
}

- (BOOL) isValid
{
    BOOL isvalid;
    if (isEmpty(self.lblTitle.text))
    {
        isvalid = NO;
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}
@end
