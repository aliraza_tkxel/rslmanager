//
//  PSAddApplianceViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
@class PSAddApplianceTextFieldCell;
@class PSAddAppliancePickerCell;
@class PSAddApplianceCheckBoxCell;
@class PSAddApplianceDatePickerCell;


@protocol PSAddApplianceProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSInteger )pickerOption;

/*!
 @discussion
 Method didSelectConfirmationButton Called upon selection of landlord appliance.
 */
- (void) didSelectConfirmationButton:(BOOL)pickerOption;

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption;

@end

@interface PSAddApplianceViewController : PSCustomViewController <PSAddApplianceProtocol, UITextFieldDelegate>
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (weak,   nonatomic) IBOutlet PSAddApplianceTextFieldCell *addApplianceTextFieldCell;
@property (weak,   nonatomic) IBOutlet PSAddAppliancePickerCell *addAppliancePickerCell;
@property (weak,   nonatomic) IBOutlet PSAddApplianceCheckBoxCell *addApplianceCheckBoxCell;
@property (weak,   nonatomic) IBOutlet PSAddApplianceDatePickerCell *addApplianceDatePickerCell;
@property (weak,   nonatomic) IBOutlet UIView *btnDeleteContainerView;
@property (weak,   nonatomic) IBOutlet UIButton *btnDelete;
@property (weak,   nonatomic) Appointment *appointment;
@property (weak,   nonatomic) Appliance *appliance;
@end
