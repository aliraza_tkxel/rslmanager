//
//  PSAddApplianceTextFieldCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSAddApplianceTextFieldCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;

- (IBAction)onClickEditBtn:(id)sender;

- (BOOL) isValid;
@end
