//
//  PSAddAppliancePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddAppliancePickerCell.h"

@implementation PSAddAppliancePickerCell
//int applianceSortingFunction(id item1, id item2, void * context){
//    NSString *firstItem = [(NSString *)item1 lowercaseString];
//    NSString *secondItem = [(NSString *)item2 lowercaseString];
//    return [firstItem  compare:secondItem];
//}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)onClickPickerBtn:(id)sender {
    if(!isEmpty(self.pickerOptions))
    {
//        NSMutableArray* options = [NSMutableArray arrayWithArray:[self.pickerOptions sortedArrayUsingFunction:applianceSortingFunction context:nil]];
        
        [self setPickerOptions:self.pickerOptions];
        [ActionSheetStringPicker showPickerWithTitle:self.lblHeader.text
                                                rows:self.pickerOptions
                                    initialSelection:self.selectedIndex
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }
}

- (IBAction)onClickEditBtn:(id)sender
{
    [self.txtTitle becomeFirstResponder];
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self.selectedIndex = [selectedIndex integerValue];
    self.txtTitle.text = [self.pickerOptions objectAtIndex:self.selectedIndex];
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelecPickerOption:)])
    {
        [(PSAddApplianceViewController *)self.delegate didSelecPickerOption:self.selectedIndex];
    }
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}


#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
}



- (BOOL) isValid
{
    BOOL isvalid;
    if (isEmpty(self.txtTitle.text))
    {
        isvalid = NO;
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}

@end
