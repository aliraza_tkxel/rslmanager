//
//  PSApplianceDetailViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
@class PSApplianceDetailCell;

@interface PSApplianceDetailViewController : PSCustomViewController
@property (strong, nonatomic) PSBarButtonItem *addApplianceBtn;
@property (weak,   nonatomic) IBOutlet PSApplianceDetailCell *applianceDetailCell;
@property (weak,   nonatomic) Appointment *appointment;
@end
