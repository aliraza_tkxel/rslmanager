//
//  PSApplianceDetailViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSApplianceDetailViewController.h"
#import "PSBarButtonItem.h"
#import "PSApplianceDetailCell.h"
#import "PSAddApplianceViewController.h"
#import "PSInspectApplianceViewController.h"

#define kApplianceDetailCellHeight 200

static NSString *applianceDetailCellIdentifier = @"applianceDetailCellIdentifier";

@interface PSApplianceDetailViewController ()

@end

@implementation PSApplianceDetailViewController
{
    NSArray *_appliances;
    BOOL _noResultsFound;
    BOOL isLoadedOnce_;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    isLoadedOnce_ = NO;
    [self.tableView reloadData];

    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _appliances = [NSArray array];
    _appliances = [self.appointment.appointmentToProperty.propertyToAppliances allObjects];
	  _appliances = [_appliances filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isActive = 1 or isActive = nil"]];

    if (!isLoadedOnce_) {
        
        isLoadedOnce_ = YES;
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kApplianceInstalledDate ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor];
        _appliances = [_appliances sortedArrayUsingDescriptors:sortDescriptors];
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.addApplianceBtn = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NEW")
                                                                style:PSBarButtonItemStyleAdd
                                                               target:self
                                                               action:@selector(onClickNewAppplianceButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.addApplianceBtn, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Appliance Details"];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickNewAppplianceButton
{
    CLS_LOG(@"onClickNewAppliance");
    PSAddApplianceViewController *addApplianceViewController = [[PSAddApplianceViewController alloc] initWithNibName:@"PSAddApplianceViewController" bundle:nil];
    [addApplianceViewController setAppointment:self.appointment];
    [self.navigationController pushViewController:addApplianceViewController animated:YES];
}


#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
    if (_noResultsFound)
        {
        rowHeight = 40;
        }
    else
        {
        rowHeight = kApplianceDetailCellHeight;
        }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
   
    if (!isEmpty(_appliances))
        {
        rows = [_appliances count];
        _noResultsFound = NO;
        }
    else
        {
        rows = 1;
        _noResultsFound = YES;
        }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;

    if(_noResultsFound)
        {
        static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
        UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
        if(_cell == nil)
            {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
            }
            // Configure the cell...
        
        _cell.textLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
        _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
        _cell.textLabel.text = LOC(@"KEY_STRING_NO_APPLIANCE");
        _cell.textLabel.textAlignment = PSTextAlignmentCenter;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        cell = _cell;
        }
    
    else
        {
        PSApplianceDetailCell *_cell = [self createCellWithIdentifier:applianceDetailCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
        }
    
    UIImageView *backgroundimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 58)];
    backgroundimage.backgroundColor = [UIColor clearColor];
    backgroundimage.opaque = NO;
    
    if(indexPath.row %2 == 0)
    {
        backgroundimage.image = [UIImage imageNamed:@"cell_white"];
    }
    else if (indexPath.row %2 == 1){
        backgroundimage.image = [UIImage imageNamed:@"cell_grey"];
    }
    
    backgroundimage.contentMode = cell.imageView.contentMode;
    cell.backgroundView = backgroundimage;

    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{

        PSApplianceDetailCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:applianceDetailCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSApplianceDetailCell" owner:self options:nil];
            _cell = self.applianceDetailCell;
            self.applianceDetailCell = nil;
        }
		return _cell;

}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
  
        PSApplianceDetailCell *_cell = (PSApplianceDetailCell *)*cell;
        dispatch_async(dispatch_get_main_queue(), ^{
        
        [_cell.lblTitleType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleMake setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleModel setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleInstalledDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleReplacementDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        
        [_cell.lblType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblMake setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblModel setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblInstalledDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblReplacementDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        
        [_cell.btnInspect addTarget:self action:@selector(onclickApplianceBtnInspect:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnInspect setTag:indexPath.row];
        Appliance *appliance = nil;
        if (!isEmpty(_appliances))
        {
            appliance = [_appliances objectAtIndex:indexPath.row];
        }
        
        [_cell.lblType setText:nil];
        [_cell.lblMake setText:nil];
        [_cell.lblModel setText:nil];
        [_cell.lblInstalledDate setText:nil];
        [_cell.lblReplacementDate setText:nil];

   
        if (!isEmpty(appliance))
        {
 
            if (!isEmpty(appliance.applianceType.typeName))
            {
                [_cell.lblType setText:appliance.applianceType.typeName];
            }
            
            if (!isEmpty(appliance.applianceManufacturer.manufacturerName))
            {
                [_cell.lblMake setText:appliance.applianceManufacturer.manufacturerName];
            }
            
            if (!isEmpty(appliance.applianceModel.modelName))
            {
                [_cell.lblModel setText:appliance.applianceModel.modelName];
            }
                        
            [_cell.lblInstalledDate setText:[UtilityClass stringFromDate:appliance.installedDate dateFormat:kDateTimeStyle8]];
            [_cell.lblReplacementDate setText:[UtilityClass stringFromDate:appliance.replacementDate dateFormat:kDateTimeStyle8]];
            
            if ([appliance.isInspected boolValue])
            {
                [_cell setAccessoryType:UITableViewCellAccessoryNone];
                [_cell.btnInspect setBackgroundImage:[UIImage imageNamed:@"btn_Complete"] forState:UIControlStateNormal];
            }
            else
            {
                [_cell.btnInspect setBackgroundImage:[UIImage imageNamed:@"checklistIcon"] forState:UIControlStateNormal];
                [_cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

            }
        }
        *cell = _cell;
    });
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!_noResultsFound)
        {
        Appliance *appliance = [_appliances objectAtIndex:indexPath.row];
        if (![appliance.isInspected boolValue])
            {
            PSAddApplianceViewController *addApplianceViewController = [[PSAddApplianceViewController alloc] initWithNibName:@"PSAddApplianceViewController" bundle:nil];
            [addApplianceViewController setAppliance:appliance];
            [addApplianceViewController setAppointment:self.appointment];
            [self.navigationController pushViewController:addApplianceViewController animated:YES];
            }
        }

}

#pragma mark - Cell Button Click Selectors

- (IBAction)onclickApplianceBtnInspect:(id)sender
{
    CLS_LOG(@"onclickBtnInspect");
    #warning Explicitly changed to editing functionality either it was inspected. Ticket# 6112
    UIButton *btn = (UIButton *)sender;
    Appliance *appliance = [_appliances objectAtIndex:btn.tag];
    {
        PSInspectApplianceViewController *inspectApplianceViewController = [[PSInspectApplianceViewController alloc] initWithNibName:@"PSInspectApplianceViewController" bundle:nil];
        [inspectApplianceViewController setAppliance:appliance];
        [self.navigationController pushViewController:inspectApplianceViewController animated:YES];
    }
}


@end
