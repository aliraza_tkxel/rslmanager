//
//  PSInspectAppliancePickerTFCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSInspectApplianceViewController.h"
@interface PSInspectAppliancePickerTFCell : UITableViewCell
@property (weak,    nonatomic) id<PSInspectApplianceProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) IBOutlet UITextField *txtUnit;

@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (assign, nonatomic) NSInteger selectedIndex;
- (IBAction)onClickBtnPicker:(id)sender;

- (BOOL) isValid;
@end
