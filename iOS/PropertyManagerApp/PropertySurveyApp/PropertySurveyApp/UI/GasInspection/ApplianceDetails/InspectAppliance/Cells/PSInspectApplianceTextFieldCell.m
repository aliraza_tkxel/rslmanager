//
//  PSInspectApplianceTextFieldCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSInspectApplianceTextFieldCell.h"

@implementation PSInspectApplianceTextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)resignFirstResponder:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    [textField resignFirstResponder];

}

#pragma mark Methods
- (BOOL) isValid
{
    BOOL isvalid;
    if (isEmpty(self.txtTitle.text))
    {
        isvalid = NO;
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}

- (IBAction)onClickEditBtn:(id)sender
{
    [self.txtTitle becomeFirstResponder];
}
@end
