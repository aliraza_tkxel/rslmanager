//
//  PSApplianceDetailCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSApplianceDetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitleType;
@property (strong, nonatomic) IBOutlet UILabel *lblType;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleMake;
@property (strong, nonatomic) IBOutlet UILabel *lblMake;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleModel;
@property (strong, nonatomic) IBOutlet UILabel *lblModel;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleInstalledDate;
@property (strong, nonatomic) IBOutlet UILabel *lblInstalledDate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleReplacementDate;
@property (strong, nonatomic) IBOutlet UILabel *lblReplacementDate;
@property (strong, nonatomic) IBOutlet UIButton *btnInspect;
@end
