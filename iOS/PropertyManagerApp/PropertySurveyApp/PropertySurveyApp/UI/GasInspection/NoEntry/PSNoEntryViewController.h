//
//  PSNoEntryViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"
@class PSNoEntryCell;
@protocol PSNoEntryProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption;

@end

@interface PSNoEntryViewController : PSCustomViewController <PSNoEntryProtocol>

@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (weak,   nonatomic) IBOutlet PSNoEntryCell *noEntryCell;
@property (weak,   nonatomic) Appointment *appointment;
@end
