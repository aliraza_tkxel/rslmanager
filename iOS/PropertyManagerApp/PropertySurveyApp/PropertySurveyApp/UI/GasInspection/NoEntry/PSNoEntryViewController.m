//
//  PSNoEntryViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSNoEntryViewController.h"
#import "PSBarButtonItem.h"
#import "PSNoEntryCell.h"

@interface PSNoEntryViewController ()
{
    NSString *_noEntry;
    NSNumber *_isCardLeft;
    NSMutableDictionary *_noEntryDictionary;
}

@end

@implementation PSNoEntryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    _noEntryDictionary = [NSMutableDictionary dictionary];
    _noEntry = LOC(@"KEY_ALERT_NO");
    _isCardLeft = [NSNumber numberWithBool:NO];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"No Entry"];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popToRootViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_NO_ENTRY")
                                                   message:LOC(@"KEY_ALERT_CONFIRM_NO_ENTRY")
                                                  delegate:self
                                         cancelButtonTitle:LOC(@"KEY_ALERT_NO")
                                         otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
    alert.tag = AlertViewTagNoEntry;
    [alert show];
   // [self updateAppointmentWithNoEntry];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 44;
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = nil;
    
    static NSString *noEntryCellIdentifier = @"noEntryCellIedentifier";
    PSNoEntryCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noEntryCellIdentifier];
    
    if(_cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"PSNoEntryCell" owner:self options:nil];
        _cell = self.noEntryCell;
        self.noEntryCell = nil;
    }
    
    [self configureCell:&_cell atIndexPath:indexPath];
    _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell = _cell;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    PSNoEntryCell *_cell = (PSNoEntryCell *)*cell;
    [_cell setDelegate:self];
    [_cell.lblTitle setText:_noEntry];
    [_cell.lblHeader setText:LOC(@"KEY_STRING_CARD_LEFT")];
    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
    [_cell.btnPicker addTarget:self action:@selector(onClickNoEntryPicker:) forControlEvents:UIControlEventTouchUpInside];
    [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    
    *cell = _cell;
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewTagNoEntry)
    {
        if(buttonIndex == alertView.firstOtherButtonIndex)
        {
            [self updateAppointmentWithNoEntry];
        }
    }
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickNoEntryPicker:(id)sender
{
    CLS_LOG(@"OnClickNoEntryPicker");
}

#pragma mark - Notifications
- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveUpdateNoEntrySuccessNotification) name:kUpdateAppointmentWithNoEntrySuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveApplianceFailureNotification) name:kUpdateAppointmentWithNoEntryFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateAppointmentWithNoEntrySuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateAppointmentWithNoEntryFailureNotification object:nil];
}

- (void) onReceiveUpdateNoEntrySuccessNotification
{
    [self hideActivityIndicator];
    [self popToRootViewController];
}

#pragma mark - Protocol Methods
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption
{
    if (!isEmpty(pickerOption))
    {
        _noEntry = pickerOption;
        _isCardLeft = [NSNumber numberWithBool:[pickerOption boolValue]];
    }
    [self.tableView reloadData];
}

- (void) updateAppointmentWithNoEntry
{
    [_noEntryDictionary setObject:self.appointment.objectID forKey:@"objectId"];
    [_noEntryDictionary setObject:_isCardLeft forKey:@"isCardLeft"];
    [[PSDataUpdateManager sharedManager] updateAppointmentWithNoEntry:_noEntryDictionary];
}
@end
