//
//  PSDetectorDetailCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSMeterCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitleType;
@property (strong, nonatomic) IBOutlet UILabel *lblType;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleReading;
@property (strong, nonatomic) IBOutlet UILabel *lblReading;
@property (strong, nonatomic) IBOutlet UIButton *btnTest;

- (void) configureCell: (id)data;

@end
