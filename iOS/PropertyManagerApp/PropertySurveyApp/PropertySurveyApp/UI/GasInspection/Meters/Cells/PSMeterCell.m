//
//  PSDetectorDetailCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSMeterCell.h"

@implementation PSMeterCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
      
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureCell: (id)data
{
  [self.lblTitleType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  [self.lblTitleLocation setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblLocation setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  [self.lblTitleReading setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblReading setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self setBackgroundColor:[UIColor clearColor]];
  [self.lblLocation setText:nil];
  [self.lblType setText:nil];
  [self.lblReading setText:nil];
  
  Meter *meter = (Meter *) data;
  if (isEmpty(meter)== NO)
    {
    [self.lblType setText:meter.deviceType];
    [self.lblLocation setText:meter.location];
		
    [self.lblReading setText:(isEmpty(meter.readingDate)?nil:[NSString stringWithFormat:@"%@ (As at %@)", meter.reading, [UtilityClass stringFromDate:meter.readingDate dateFormat:kDateTimeStyle8]])];
		CGRect frame = self.lblReading.frame;
		frame.size.width = 287;
		self.lblReading.frame = frame;
		[self.lblReading sizeToFit];
			
			[self.btnTest setBackgroundImage:nil forState:UIControlStateNormal];
    
    if ([meter.isInspected boolValue])
      {
      [self setAccessoryType:UITableViewCellAccessoryNone];
      [self.btnTest setBackgroundImage:[UIImage imageNamed:@"btn_Complete"] forState:UIControlStateNormal];
      }
    else
      {
      [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
      [self.btnTest setBackgroundImage:[UIImage imageNamed:@"checklistIcon"] forState:UIControlStateNormal];
      }
    
    }
  [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}




@end
