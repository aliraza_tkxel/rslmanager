//
//  PSApplianceDetailViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSMetersViewController.h"
#import "PSBarButtonItem.h"
#import "PSMeterCell.h"
#import "PSAddEditMeterViewController.h"
#import "PSTestMeterViewController.h"

#define kNumberOfSections 1
#define kMeterDetailCellHeight 153

@interface PSMetersViewController ()

@end

@implementation PSMetersViewController
{
    NSArray *_meters;
    BOOL _noResultsFound;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    [self registerNibsForTableView];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _meters = [NSArray array];
    _meters = [self.appointment.appointmentToProperty.propertyToMeter allObjects];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    /*self.addMeterBtn = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NEW")
                                                                style:PSBarButtonItemStyleAdd
                                                               target:self
                                                               action:@selector(onClickNewMeterButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.addMeterBtn, nil]];*/
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Meters"];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickNewMeterButton
{
    CLS_LOG(@"onClickNewMeterButton");
    PSAddEditMeterViewController *addEditMeterViewController = [[PSAddEditMeterViewController alloc] initWithNibName:@"PSAddEditMeterViewController" bundle:nil];
    [addEditMeterViewController setAppointment:self.appointment];
    [self.navigationController pushViewController:addEditMeterViewController animated:YES];
}


#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return kNumberOfSections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
	  CGFloat readingHeight = 0.0;
	if ([_meters count] > 0) {
	 Meter *objMeter = [_meters objectAtIndex:indexPath.row];
	CGFloat readingLabelHeight = 21;
	 if (!isEmpty(objMeter.reading)) {
		NSString *meterReading =[NSString stringWithFormat:@"%@ (As at %@)", objMeter.reading, [UtilityClass stringFromDate:objMeter.readingDate dateFormat:kDateTimeStyle8]];
		readingHeight = [self heightForLabelWithText:meterReading];
		 if (readingHeight > readingLabelHeight) {
      readingHeight = readingHeight - readingLabelHeight;
		 }
 	 }
	 rowHeight = kMeterDetailCellHeight + readingHeight;
	}else
	{
		rowHeight = kMeterDetailCellHeight;
	}
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    
    if (!isEmpty(_meters))
        {
        rows =[_meters count];
        _noResultsFound = NO;
        }
    else
        {
        rows = 1;
        _noResultsFound = YES;
        }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if(_noResultsFound)
        {
        static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
        UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
        if(_cell == nil)
            {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
            }
        
        _cell.textLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
        _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
        _cell.textLabel.text = LOC(@"KEY_STRING_NO_METER");
        _cell.textLabel.textAlignment = PSTextAlignmentCenter;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        cell = _cell;
        }
    
    else
        {
        PSMeterCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSMeterCell class])];
        [_cell.btnTest addTarget:self action:@selector(onclickMeterBtnTest:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnTest setTag:indexPath.row];
        Meter *meter;
        if (isEmpty(_meters)== NO)
          {
          meter = [_meters objectAtIndex:indexPath.row];
          }
        
        [_cell configureCell:meter];
        cell = _cell;
        }

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_noResultsFound)
        {
        Meter *meter = [_meters objectAtIndex:indexPath.row];
        if ([meter.isInspected boolValue]==NO)
            {
            PSAddEditMeterViewController *editMeterViewController = [[PSAddEditMeterViewController alloc] initWithNibName:NSStringFromClass([PSAddEditMeterViewController class]) bundle:nil];
            [editMeterViewController setMeter:meter];
						[editMeterViewController setAppointment:self.appointment];
            [self.navigationController pushViewController:editMeterViewController animated:YES];
            }
        }
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onclickMeterBtnTest:(id)sender
{
    CLS_LOG(@"onclickMeterBtnTest");
    UIButton *btn = (UIButton *)sender;
    PSTestMeterViewController *testMeterViewController = [[PSTestMeterViewController alloc] initWithNibName:NSStringFromClass([PSTestMeterViewController class]) bundle:nil];
    Meter *meter = [_meters objectAtIndex:btn.tag];
    [testMeterViewController setMeter:meter];
    [self.navigationController pushViewController:testMeterViewController animated:YES];
}

-(void)registerNibsForTableView
{
  NSString * nibName = NSStringFromClass([PSMeterCell class]);
  [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

#pragma mark - Method


- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
//#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
//	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:fontFamily size:fontSize],NSFontAttributeName,nil];
//	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(287, 999999.0f)
//																						 options:NSStringDrawingUsesLineFragmentOrigin
//																					attributes:attributes
//																						 context:nil];
//	CGFloat height = boundingRect.size.height + verticalPadding;
//	
//#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
											constrainedToSize:CGSizeMake(287, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
//#endif
	
	return height;
}

@end
