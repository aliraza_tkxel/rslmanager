//
//  PSInspectAppliancePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTestMeterViewController.h"

@interface PSAEMLabelCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
-(void) configureCell:(id) data;

@end
