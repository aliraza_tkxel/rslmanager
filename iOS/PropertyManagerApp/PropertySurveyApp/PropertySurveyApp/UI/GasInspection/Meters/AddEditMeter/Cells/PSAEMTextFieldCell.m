//
//  PSAddApplianceTextFieldCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAEMTextFieldCell.h"

@implementation PSAEMTextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (IBAction)onClickEditBtn:(id)sender
{
    [self.txtTitle becomeFirstResponder];
}

#pragma mark - Methods

- (BOOL) isValid
{
    BOOL isvalid = YES;
	  NSString *title =  self.txtTitle.text;
    if (isEmpty(title.trimString))
    {
        isvalid = NO;
    }
    
    return isvalid;
}

-(void) configureCell:(id) data{
  
  [self.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  
  NSString* _data = (NSString*) data;
  if (!isEmpty(_data))
    {
    [self.txtTitle setText:_data];
    }
  
}

@end
