//
//  PSAddApplianceViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddEditMeterViewController.h"
#import "PSBarButtonItem.h"
#import "PSAEMTextFieldCell.h"
#import "PSAEMPickerCell.h"
#import "PSAEMDatePickerCell.h"
#import "PSTextViewCell.h"
#import "PSAEMLabelCell.h"

#define kNumberOfRows 6
#define kRowDeviceType 0
#define kRowMeterType 1
#define kRowLocation 2
#define kRowManufacturer 3
#define kRowSerialNumber 4
#define kRowInstalledDate 5


@interface PSAddEditMeterViewController ()
{
	MeterType *_meterType;
	NSString *_deviceType;
	NSString *_location;
	NSString *_manufacturer;
	NSString *_serialNumber;
	NSDate * _installedDate;
	NSMutableArray *_deviceTypes;
	NSMutableArray *_meterTypes;
	
	NSInteger _pickerTag;
	NSInteger _textFieldTag;
	NSMutableDictionary *_meterDictionary;
}

@end

@implementation PSAddEditMeterViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	_meterDictionary = [NSMutableDictionary dictionary];
	[self loadNavigationonBarItems];
	[self registerNibsForTableView];
	[self populateDefaultValues];
	
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	self.saveButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
																													 style:PSBarButtonItemStyleDefault
																													target:self
																													action:@selector(onClickSaveButton)];
	
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	if (isEmpty(self.meter)==NO) {
		[self setTitle:@"Meter"];
	}
	else
	{
		[self setTitle:@"Add Meter"];
	}
}

- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickSaveButton
{
	CLS_LOG(@"onClickSaveButton");
	[self resignAllResponders];
	BOOL isValid = [self isFormValid];
	if (isValid==YES) {
		[self saveMeter];
	}
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44;
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return kNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;
	
	if (indexPath.row == kRowSerialNumber || indexPath.row == kRowManufacturer || indexPath.row == kRowLocation)
	{
		PSAEMTextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAEMTextFieldCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
		
	}
	else if (indexPath.row == kRowInstalledDate)
	{
		PSAEMDatePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAEMDatePickerCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
		
	}
	else if (indexPath.row == kRowDeviceType)
	{
		PSAEMLabelCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAEMLabelCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
		
	}
	else
	{
		PSAEMPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAEMPickerCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return cell;
}


- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
	if ([*cell isKindOfClass:[PSAEMTextFieldCell class]])
	{
		PSAEMTextFieldCell *_cell = (PSAEMTextFieldCell *)*cell;
		[_cell.txtTitle setDelegate:self];
		switch (indexPath.row) {
			case kRowSerialNumber:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_SERIAL_NUMBER")];
				[_cell.txtTitle setTag:kRowSerialNumber];
				[_cell configureCell:_serialNumber];
				break;
			case kRowManufacturer:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_MANUFACTURER")];
				[_cell.txtTitle setTag:kRowManufacturer];
				[_cell configureCell:_manufacturer];
				break;
			case kRowLocation:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_LOCATION")];
				[_cell.txtTitle setTag:kRowLocation];
				[_cell configureCell:_location];
				break;
				
			default:
				break;
		}
		*cell = _cell;
	}
	
	else if ([*cell isKindOfClass:[PSAEMPickerCell class]])
	{
		PSAEMPickerCell *_cell = (PSAEMPickerCell *)*cell;
		[_cell setDelegate:self];
		[_cell.txtTitle setDelegate:self];
		switch (indexPath.row) {
				
			case kRowMeterType:
				[_cell.txtTitle setTag:kRowMeterType];
				[_cell setPickerOptions:[self getMeterTypesPickerData]];
				[_cell.lblHeader setText:LOC(@"KEY_STRING_METER_TYPE")];
				[_cell configureCell:_meterType.meterType];
				[_cell.btnPicker addTarget:self action:@selector(onClickMeterTypePicker:) forControlEvents:UIControlEventTouchUpInside];
				break;
				
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSAEMDatePickerCell class]])
	{
		PSAEMDatePickerCell *_cell = (PSAEMDatePickerCell *)*cell;
		[_cell setDelegate:self];
		switch (indexPath.row)
		{
			case kRowInstalledDate:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_INSTALLED")];
				[_cell configureCell:_installedDate];
				[_cell.btnPicker addTarget:self action:@selector(onClickInstalledDate:) forControlEvents:UIControlEventTouchUpInside];
				[_cell.resetDatesButton addTarget:self action:@selector(onClickResetInstalledDate:) forControlEvents:UIControlEventTouchUpInside];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSAEMLabelCell class]])
	{
		PSAEMLabelCell *_cell = (PSAEMLabelCell *)*cell;
		switch (indexPath.row)
		{
			case kRowDeviceType:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_DEVICE_TYPE")];
				[_cell configureCell:_deviceType];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickInstalledDate:(id)sender
{
	CLS_LOG(@"onClickInstalledDate");
	[self resignAllResponders];
	_pickerTag = kRowInstalledDate;
}

- (IBAction)onClickLocationPicker:(id)sender
{
	CLS_LOG(@"onClickLocationPicker");
	[self resignAllResponders];
	_pickerTag = kRowLocation;
}

- (IBAction)onClickMeterTypePicker:(id)sender
{
	CLS_LOG(@"onClickMeterTypePicker");
	[self resignAllResponders];
	_pickerTag = kRowMeterType;
}

- (IBAction)onClickEditLocation:(id)sender
{
	CLS_LOG(@"onClickEditLocation");
	
}

- (IBAction)onClickEditType:(id)sender
{
	CLS_LOG(@"onClickEditType");
}

- (IBAction)onClickEditMake:(id)sender
{
	CLS_LOG(@"onClickEditMake");
}

- (IBAction)onClickResetInstalledDate:(id)sender
{
	CLS_LOG(@"onClickResetInstalledDate");
	[self resignAllResponders];
	_installedDate = nil;
	[self.tableView reloadData];
}

#pragma mark - Scroll Cell Methods
- (void)scrollToRectOfSelectedControl {
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[self resignAllResponders];
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
	switch (textField.tag) {
		case kRowSerialNumber:
			_serialNumber = textField.text;
			break;
		case kRowManufacturer:
			_manufacturer = textField.text;
			break;
		case kRowLocation:
			_location = textField.text;
			break;
			
		default:
			break;
	}
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	BOOL textFieldShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
	switch (textField.tag) {
		case kRowSerialNumber:
			if ([stringAfterReplacement length] > 100)
			{
				textFieldShouldReturn = NO;
			}
			break;
		case kRowManufacturer:
		{
			if ([stringAfterReplacement length] > 100)
			{
				textFieldShouldReturn = NO;
			}
		}
			break;
			
		case kRowLocation:
		{
			if ([stringAfterReplacement length] > 200)
			{
				textFieldShouldReturn = NO;
			}
		}
			break;
			
		default:
			break;
	}
	return textFieldShouldReturn;
}

#pragma mark - Protocol Methods

/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSInteger)pickerOption
{
	switch (_pickerTag)
	{
		case kRowMeterType: {
			_meterType = [_meterTypes objectAtIndex:pickerOption];
		}
			break;
		default:
			break;
	}
	[self.tableView reloadData];
}

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption
{
	if (!isEmpty(pickerOption))
	{
		switch (_pickerTag)
		{
			case kRowInstalledDate:
				_installedDate = pickerOption;
				break;
			default:
				break;
		}
		[self.tableView reloadData];
	}
}

#pragma mark - Resign All Responders
- (void) resignAllResponders
{
	[self.view endEditing:YES];
}

#pragma mark - Methods

- (void) populateDefaultValues{
	
	if (isEmpty(self.meter) == NO)
	{
		_meterType = [[PSApplianceManager sharedManager] fetchMeterTypeWithMeterTypeId:self.meter.meterTypeId];
		_deviceType = self.meter.deviceType;
		_location = self.meter.location;
		_manufacturer = self.meter.manufacturer;
		_serialNumber = self.meter.serialNumber;
		_installedDate = self.meter.installedDate;
	}
	else {
		_meterType = nil;
		_deviceType = nil;
		_location = nil;
		_manufacturer = nil;
		_serialNumber = nil;
		_installedDate = nil;
	}
	
	[self loadDeviceMeterTypes];
	
}


- (NSMutableArray*) getMeterTypesPickerData{
	
	NSMutableArray *meterTypes = [NSMutableArray array];
	
	for (MeterType *type in _meterTypes)
	{
		[meterTypes addObject:type.meterType];
	}
	return meterTypes;
}

- (void) loadDeviceMeterTypes
{
	if (isEmpty(self.meter) == NO) {
		NSString * deviceType;
		if ([self.meter.deviceType.lowercaseString containsString:[kGasDeviceType lowercaseString] ]) {
			deviceType = kGasDeviceType;
		}else{
			deviceType = kElectricDeviceType;
		}
		_meterTypes = [[PSApplianceManager sharedManager] fetchDeviceMeterTypes:deviceType];
	}else{
		_meterTypes = nil;
	}
	
}

- (NSArray*) sortArray:(NSString*)sortDescriptorKey :(NSArray*) objArray
{
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortDescriptorKey ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	objArray = [objArray sortedArrayUsingDescriptors:sortDescriptors];
	return objArray;
}

- (Boolean) isFormValid
{
	NSString *alertMessage = nil;
	NSString *emptyField;
	BOOL isValid = YES;
	
	for (int cellNumber = 0; cellNumber < kNumberOfRows; ++cellNumber)
	{
		
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellNumber inSection:0];
		UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
		emptyField = nil;
		alertMessage = nil;
		isValid = YES;
		
		if ([cell isKindOfClass:[PSAEMDatePickerCell class]]) {
			PSAEMDatePickerCell *_cell = (PSAEMDatePickerCell *)cell;
			if ([_cell isValid] == NO)
			{
				emptyField = [_cell.lblHeader.text lowercaseString];
				isValid = NO;
			}
		}
		
		else if ([cell isKindOfClass:[PSAEMPickerCell class]])
		{
			PSAEMPickerCell *_cell = (PSAEMPickerCell *)cell;
			if ([_cell isValid] == NO)
			{
				emptyField = [_cell.lblHeader.text lowercaseString];
				isValid = NO;
			}
		}
		
		else if ([cell isKindOfClass:[PSAEMTextFieldCell class]])
		{
			PSAEMTextFieldCell *_cell = (PSAEMTextFieldCell *)cell;
			if ([_cell isValid] == NO)
			{
				emptyField = [_cell.lblHeader.text lowercaseString];
				isValid = NO;
			}
		}
		
		if (isValid == NO)
		{
			alertMessage = [NSString stringWithFormat:@"Please select %@.", emptyField];
			break;
		}
	}
	
	if (isValid == NO)
	{
		[self showAlert:alertMessage];
	}
	
	return isValid;
	
}

-(void) showAlert: (NSString*)alertMessage{
	
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																								 message:alertMessage
																								delegate:nil
																			 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																			 otherButtonTitles:nil,nil];
	[alert show];
	
}

- (void) saveMeter
{
	[self disableNavigationBarButtons];
	[self showActivityIndicator:LOC(@"KEY_STRING_SAVING_METER")];
	
	[_meterDictionary setObject:self.meter.objectID forKey:kMeterObjectId];
	[_meterDictionary setObject:[UtilityClass convertNSDateToServerDate:_installedDate] forKey:kMeterInstalledDate];
	[_meterDictionary setObject:isEmpty(_location)?[NSNull null]:_location forKey:kMeterLocation];
	[_meterDictionary setObject:isEmpty(_manufacturer)?[NSNull null]:_manufacturer  forKey:kMeterManufacturer];
	[_meterDictionary setObject:isEmpty(_meterType.meterId)?[NSNull null]:_meterType.meterId forKey:kMeterTypeId];
	[_meterDictionary setObject:isEmpty(self.appointment.appointmentToProperty.propertyId)?[NSNull null]:self.appointment.appointmentToProperty.propertyId forKey:kMeterPropertyId];
    [_meterDictionary setObject:isEmpty(self.appointment.appointmentToProperty.schemeId)?[NSNull null]:self.appointment.appointmentToProperty.schemeId forKey:kSchemeID];
    [_meterDictionary setObject:isEmpty(self.appointment.appointmentToProperty.blockId)?[NSNull null]:self.appointment.appointmentToProperty.blockId forKey:kSchemeBlockID];
	[_meterDictionary setObject:isEmpty(_serialNumber)?[NSNull null]:_serialNumber forKey:kMeterSerialNumber];
	[_meterDictionary setObject:isEmpty(self.meter.deviceType)?[NSNull null]:self.meter.deviceType forKey:kDeviceType];
	
  [_meterDictionary setObject:isEmpty(self.meter.deviceTypeId)?[NSNull null]:self.meter.deviceTypeId forKey:kDeviceTypeId];
	[_meterDictionary setObject:isEmpty(self.meter.lastReadingDate)?[NSNull null]:[UtilityClass convertNSDateToServerDate:self.meter.lastReadingDate] forKey:kMeterLastReadingDate];
	[_meterDictionary setObject:isEmpty(self.meter.lastReading)?[NSNull null]:self.meter.lastReading forKey:kMeterLastReading];
	[_meterDictionary setObject:[NSNumber numberWithBool:NO] forKey:kMeterIsInspected];
	
	BOOL isSaved = [[PSDataPersistenceManager sharedManager]saveMeter:_meterDictionary forProperty:self.appointment.appointmentToProperty.objectID] ;
	
	if (isSaved) {
		[self hideActivityIndicator];
		[self popOrCloseViewController];
	}else{
		
		[self hideActivityIndicator];
		
		NSString *alertMessage = LOC(@"KEY_ALERT_FAILED_TO_SAVE_METER");
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																									 message:alertMessage
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
		[self enableNavigationBarButtons];
		
	}
	
}


-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSAEMDatePickerCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSAEMPickerCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSAEMTextFieldCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSAEMLabelCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	
}

- (void) disableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	if (!isEmpty(navigationBarRightButtons))
	{
		for (PSBarButtonItem *button in navigationBarRightButtons)
		{
			button.enabled = NO;
		}
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	if (!isEmpty(navigationBarLeftButtons))
	{
		for (PSBarButtonItem *button in navigationBarLeftButtons)
		{
			button.enabled = NO;
		}
	}
}

- (void) enableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	if (!isEmpty(navigationBarRightButtons))
	{
		for (PSBarButtonItem *button in navigationBarRightButtons) {
			button.enabled = YES;
		}
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	
	if (!isEmpty(navigationBarLeftButtons))
	{
		for (PSBarButtonItem *button in navigationBarLeftButtons)
		{
			button.enabled = YES;
		}
	}
}
@end
