//
//  PSAddApplianceViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSTextViewCell.h"


@protocol PSAddEditMeterProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSInteger )pickerOption;

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption;

@end

@interface PSAddEditMeterViewController : PSCustomViewController <PSAddEditMeterProtocol, UITextFieldDelegate>
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutlet PSTextViewCell *textViewCell;
@property (weak,   nonatomic) Appointment *appointment;
@property (weak,   nonatomic) Meter *meter;
@end
