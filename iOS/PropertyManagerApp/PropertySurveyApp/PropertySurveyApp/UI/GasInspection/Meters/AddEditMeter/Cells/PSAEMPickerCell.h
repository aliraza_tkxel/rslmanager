//
//  PSAddAppliancePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAddEditMeterViewController.h"

@interface PSAEMPickerCell : UITableViewCell <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak,    nonatomic) id<PSAddEditMeterProtocol> delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (assign, nonatomic) NSInteger selectedIndex;
- (IBAction)onClickPickerBtn:(id)sender;
- (IBAction)onClickEditBtn:(id)sender;
- (BOOL) isValid;
-(void) configureCell:(id) data;
@end
