//
//  PSAddAppointmentDatePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAEMDatePickerCell.h"

@interface PSAEMDatePickerCell()

@property (nonatomic, retain) ActionSheetDatePicker * picker;

@end
@implementation PSAEMDatePickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onclickPickerBtn:(id)sender {
    if(self.selectedDate == nil)
    {
        self.selectedDate = [NSDate date];
    }
    
    self.picker = [ActionSheetDatePicker showPickerWithTitle:self.lblHeader.text
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:self.selectedDate
                                        target:self
                                        action:@selector(onDateSelected:element:)
                                        origin:sender];
}

- (void)onDateSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectDate:)])
    {
        [(PSAddEditMeterViewController *)self.delegate didSelectDate:self.selectedDate];
    }
    
}

- (BOOL) isValid
{
    BOOL isvalid;
    if (isEmpty(self.lblTitle.text))
    {
        isvalid = NO;
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}

-(void) configureCell:(id) data{

  NSDate * _data = (NSDate*) data;
  [self.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  [self.lblTitle setText:nil];
  [self.resetDatesButton setHidden:YES];
  if (!isEmpty(_data))
    {
    [self.lblTitle setText:[UtilityClass stringFromDate:_data dateFormat:kDateTimeStyle8]];
    [self.resetDatesButton setHidden:NO];
  }
  

}


@end
