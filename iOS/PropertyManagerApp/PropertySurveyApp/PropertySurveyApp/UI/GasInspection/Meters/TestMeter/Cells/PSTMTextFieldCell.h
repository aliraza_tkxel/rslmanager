//
//  PSAddApplianceTextFieldCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTMTextFieldCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;

- (IBAction)onClickEditBtn:(id)sender;
- (void) configureCell:(id) data;
- (BOOL) isValid;
@end
