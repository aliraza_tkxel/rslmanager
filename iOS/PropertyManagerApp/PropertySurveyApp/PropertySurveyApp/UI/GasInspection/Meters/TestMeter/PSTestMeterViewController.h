//
//  PSInspectApplianceViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"

@protocol PSTestMeterProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelectPickerOption:(NSString *)pickerOption;

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption;

@end

@interface PSTestMeterViewController : PSCustomViewController <PSTestMeterProtocol, UITextFieldDelegate,UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblMeterDetail;
@property (weak,   nonatomic) Appliance *appliance;
@property (weak,   nonatomic) Meter *meter;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@end
