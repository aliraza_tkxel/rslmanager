//
//  PSInspectApplianceViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSTestMeterViewController.h"
#import "PSBarButtonItem.h"
#import "PSTMPickerCell.h"
#import "PSTMDatePickerCell.h"
#import "PSTMLabelCell.h"
#import "PSTMTextFieldCell.h"
#import "PSTextViewCell.h"

#define kNumberOfRows 6
#define kRowLastReading 0
#define kRowReading 1
#define kRowReadingDate 2
#define kRowPassed 3
#define kRowNotes 4
#define kRowCapped 5

#define kTextViewCellHeight 94
#define ACCEPTABLE_CHARECTERS @"0123456789."

@interface PSTestMeterViewController ()
{
	NSInteger _pickerTag;
	NSString* _lastReading;
	NSDate* _lastReadingDate;
	NSString* _reading;
	NSDate* _readingDate;
	NSString* _isPassed;
	NSString*_notes;
	NSString* _isCapped;
	NSNumber* _inspectedBy;
	NSDate * _inspectionDate;
}

@end

@implementation PSTestMeterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self registerNibsForTableView];
	[self loadNavigationonBarItems];
	[self populateMeterDetail];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
																																		style:PSBarButtonItemStyleDefault
																																	 target:self
																																	 action:@selector(onClickSaveButton)];
	
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setTitle:@"Test Meter"];
}

- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickSaveButton
{
	CLS_LOG(@"onClickSaveButton");
	
	if ([self isFormValid]==YES) {
		[self saveInspectionForm];
	}
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44;
	
	if (indexPath.row == kRowNotes )
	{
		rowHeight = [self heightForTextView:nil containingString:_notes];
		if (rowHeight < kTextViewCellHeight)
		{
			rowHeight = kTextViewCellHeight;
		}
	}
	
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return kNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;
	
	if (indexPath.row == kRowLastReading)
	{
		PSTMLabelCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSTMLabelCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.row == kRowReadingDate)
	{
		PSTMDatePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSTMDatePickerCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.row == kRowPassed || indexPath.row == kRowCapped)
	{
		PSTMPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSTMPickerCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.row == kRowNotes)
	{
		PSTextViewCell *_cell = [[PSTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:textViewCellIdentifier];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.row == kRowReading)
	{
		PSTMTextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSTMTextFieldCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return cell;
}


- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
	
	if ([*cell isKindOfClass:[PSTMTextFieldCell class]])
	{
		PSTMTextFieldCell *_cell = (PSTMTextFieldCell *)*cell;
		[_cell.txtTitle setDelegate:self];
		switch (indexPath.row)
		{
			case kRowReading:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_METER_READING")];
				[_cell.txtTitle setTag:kRowReading];
				[_cell configureCell:_reading];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSTextViewCell class]])
	{
		PSTextViewCell *_cell = (PSTextViewCell *)*cell;
		UILabel *header = [[UILabel alloc] init];
		header.frame = CGRectMake(12, 3, 320,12);
		[header setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
		[_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
		[header setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12]];
		[_cell addSubview:header];
		
		PSTextView*  _textView = [[PSTextView alloc] initWithFrame:CGRectZero];
		[_textView setDelegate:self];
		[_textView setTag:indexPath.row];
		[_textView setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
		[_textView setUserInteractionEnabled:YES];
		
		switch (indexPath.row)
		{
			case kRowNotes:
				[header setText:LOC(@"KEY_STRING_METER_NOTES")];
				if (isEmpty(_notes)==NO)
				{
					_textView.text = _notes;
				}
				break;
		}
		
		_cell.textView = _textView;
		_textView = nil;
		*cell = _cell;
		
	}
	else if ([*cell isKindOfClass:[PSTMDatePickerCell class]])
	{
		PSTMDatePickerCell *_cell = (PSTMDatePickerCell *)*cell;
		[_cell setDelegate:self];
		
		switch (indexPath.row)
		{
			case kRowReadingDate:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_METER_READING_DATE")];
				[_cell configureCell:_readingDate];
				[_cell.btnPicker addTarget:self action:@selector(onClickReadingDate:) forControlEvents:UIControlEventTouchUpInside];
				[_cell.resetDatesButton addTarget:self action:@selector(onClickResetReadingDate:) forControlEvents:UIControlEventTouchUpInside];
				break;
				
			default:
				break;
		}
		*cell = _cell;
		
	}
	else if ([*cell isKindOfClass:[PSTMPickerCell class]])
	{
		PSTMPickerCell *_cell = (PSTMPickerCell *)*cell;
		[_cell setDelegate:self];
		
		switch (indexPath.row) {
			case kRowPassed:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_METER_PASSED")];
				[_cell configureCell:_isPassed];
				[_cell.btnPicker addTarget:self action:@selector(onClickPassed:) forControlEvents:UIControlEventTouchUpInside];
				break;
			case kRowCapped:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_METER_CAPPED")];
				[_cell configureCell:_isCapped];
				[_cell.btnPicker addTarget:self action:@selector(onClickCapped:) forControlEvents:UIControlEventTouchUpInside];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSTMLabelCell class]])
	{
		PSTMLabelCell *_cell = (PSTMLabelCell *)*cell;
		switch (indexPath.row) {
			case kRowLastReading:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_METER_LAST_READING")];
				[_cell configureCell:[self getLastReadingData]];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	
}


- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[self resignAllResponders];
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickPassed:(id)sender
{
	CLS_LOG(@"onClickPassed");
	[self resignAllResponders];
	_pickerTag = kRowPassed;
}

- (IBAction)onClickCapped:(id)sender
{
	CLS_LOG(@"onClickCapped");
	[self resignAllResponders];
	_pickerTag = kRowCapped;
}

- (IBAction)onClickReadingDate:(id)sender
{
	CLS_LOG(@"onClickReadingDate");
	[self resignAllResponders];
	_pickerTag = kRowReadingDate;
}

- (IBAction)onClickResetReadingDate:(id)sender
{
	CLS_LOG(@"onClickResetReadingDate");
	[self resignAllResponders];
	_readingDate = nil;
	[self.tableView reloadData];
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
	switch (textField.tag) {
		case kRowReading:
			_reading = textField.text;
			break;
		default:
			break;
	}
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	BOOL textFieldShouldReturn = YES;
	
	NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
	
	switch (textField.tag) {
		case kRowReading:
			
			if (([stringAfterReplacement length] > 24)
					|| ([self checkIsValidNumeric:stringAfterReplacement]==NO) )
			{
				textFieldShouldReturn = NO;
			}else
			{
			  _reading = [((UITextField *)self.selectedControl).text stringByReplacingCharactersInRange:range withString:string];
			}
			break;
		default:
			break;
	}
	
	return textFieldShouldReturn;
}


#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	BOOL textVeiwShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextView *)self.selectedControl).text stringByAppendingString:text];
	
	if ([stringAfterReplacement length] > 1000)
	{
		textVeiwShouldReturn = NO;
	}
	
	if (textVeiwShouldReturn == YES) {
		_notes = textView.text;
	}
	
	return textVeiwShouldReturn;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	_notes = textView.text;
}

- (void)textViewDidChange:(UITextView *)textView
{
	_notes = textView.text;
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	[self scrollToRectOfSelectedControl];
}

#pragma mark - Protocol Methods
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelectPickerOption:(NSString *)pickerOption
{
	
	switch (_pickerTag)
	{
		case kRowPassed:
			_isPassed = pickerOption;
			break;
			
		case kRowCapped:
			_isCapped = pickerOption;
			break;
			
		default:
			break;
	}

}

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption
{
	if (!isEmpty(pickerOption))
	{
		switch (_pickerTag)
		{
			case kRowReadingDate:
				_readingDate = pickerOption;
				break;
				
			default:
				break;
		}
		[self.tableView reloadData];
	}
}


#pragma mark - Methods

- (NSNumber*) getReadingData:(NSString*) value
{
	NSNumberFormatter *numFormat = [[NSNumberFormatter alloc] init];
	return [numFormat numberFromString:value];
}

- (NSString*) getLastReadingData
{
	
	NSMutableString *lastReading =[NSMutableString stringWithString:@""];
	
	if (isEmpty(_lastReading)==NO) {
		[lastReading appendString:[NSString stringWithFormat:@"%@",_lastReading]];
	}
	
	if (isEmpty(_lastReadingDate)==NO) {
		[lastReading appendString:[NSString stringWithFormat:@" (As at %@)",[UtilityClass stringFromDate:_lastReadingDate dateFormat:kDateTimeStyle8]]];
	}
	
 return lastReading;
}

- (void) populateMeterDetail{
	
	NSMutableString *meterLabel =[NSMutableString stringWithString:@""];
	
	if (isEmpty(self.meter)==NO)
	{
		[meterLabel appendString:[NSString stringWithFormat:@"%@",self.meter.deviceType]];
		
		MeterType *meterType = [[PSApplianceManager sharedManager] fetchMeterTypeWithMeterTypeId:self.meter.meterTypeId];
		if (isEmpty(meterType.meterType) == NO)
		{
			[meterLabel appendString:[NSString stringWithFormat:@" > %@",meterType.meterType]];
		}
		
		if ([self.meter.isInspected boolValue]==YES) {
			[self initializeMeterDetails];
		}
		
		_lastReading = self.meter.lastReading;
		_lastReadingDate = self.meter.lastReadingDate;
		
	}
	
	[self.lblMeterDetail setText:meterLabel];
	[self.lblMeterDetail setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	
}

- (BOOL) checkIsValidNumeric: (NSString*) value
{
	NSCharacterSet *allowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
	NSString *filtered = [[value componentsSeparatedByCharactersInSet:allowedCharacters] componentsJoinedByString:@""];
	return [value isEqualToString:filtered];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 40;
	CGFloat height = 0.0;
	if(OS_VERSION >= 7.0)
	{
		NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
		[style setLineBreakMode:NSLineBreakByWordWrapping];
		NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:style
																, NSParagraphStyleAttributeName
																,[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
																,NSFontAttributeName
																,nil];
		CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(285, 999999.0f)
																							 options:NSStringDrawingUsesLineFragmentOrigin
																						attributes:attributes
																							 context:nil];
		height = boundingRect.size.height + verticalPadding;
	}
	else
	{
		height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
								constrainedToSize:CGSizeMake(285, 999999.0f)
										lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
	}
	return height * 1.05;
}


- (Boolean) isFormValid
{

	NSString *message=nil;
	BOOL isValid = YES;

	NSInteger MAX_NOTES_LENGHT = 400;
	
	if (isEmpty(_reading)== NO) {
		
		NSArray  *arrayOfString = [_reading componentsSeparatedByString:@"."];
		if([arrayOfString count] > 2)
		{
			isValid = NO;
			message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_METER_READING")];
			
		}else if([arrayOfString count] == 2 && [_reading length] ==1)
		{
			isValid = NO;
			message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_METER_READING")];
			
		}else if (isEmpty(_readingDate)==YES){
			isValid = NO;
			message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_READING_DATE")];
		}
		
	}else if (isEmpty(_readingDate)== NO) {
		
		NSArray  *arrayOfString = [_reading componentsSeparatedByString:@"."];
		
		if (isEmpty(_reading)==YES) {
			isValid = NO;
			message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_METER_READING")];
		}else if([arrayOfString count] > 2)
		{
			isValid = NO;
			message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_METER_READING")];
			
		}else if([arrayOfString count] == 2 && [_reading length] ==1)
		{
			isValid = NO;
			message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_METER_READING")];
		}
		
	}
	
	if (isEmpty(_isPassed)==YES)
	{
		isValid = NO;
		message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_PASSED")];
	}else if (isEmpty(_isCapped)==YES)
	{
		isValid = NO;
		message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_CAPPED")];
	}else if (isEmpty(_notes)==NO)
	{
		
		if (_notes.length > MAX_NOTES_LENGHT) {
			isValid = NO;
			message = [NSString stringWithFormat:LOC(@"KEY_ALERT_INVALID_NOTES_LENGTH")];
		}
	}
	
	if (isValid == NO)
	{
		[self showAlert:message];
	}
	
	return isValid;
	
}

-(void) showAlert: (NSString*)alertMessage{
	
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																								 message:alertMessage
																								delegate:nil
																			 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																			 otherButtonTitles:nil,nil];
	[alert show];
	
}

- (void) saveInspectionForm
{
	BOOL isSaved = NO;
	NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
	_inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
	_inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
	[inspectionFormDictionary setObject:isEmpty(_isCapped)?[NSNull null]:[self getNSNumberValue:_isCapped] forKey:kMeterIsCapped];
	[inspectionFormDictionary setObject:isEmpty(_inspectedBy)?[NSNull null]:_inspectedBy forKey:kMeterInspectedBy];
	[inspectionFormDictionary setObject:isEmpty(_inspectionDate)?[NSNull null]:_inspectionDate forKey:kMeterInspectionDate];
	[inspectionFormDictionary setObject:isEmpty(_isPassed)?[NSNull null]:[self getNSNumberValue:_isPassed] forKey:kMeterIsPassed];
	[inspectionFormDictionary setObject:isEmpty(_notes)?[NSNull null]:_notes forKey:kMeterNotes];
	[inspectionFormDictionary setObject:isEmpty(_reading)?[NSNull null]:_reading forKey:kMeterReading];
	[inspectionFormDictionary setObject:isEmpty(_readingDate)?[NSNull null]:_readingDate forKey:kMeterReadingDate];
	
	isSaved = [[PSApplianceManager sharedManager] saveMeterInspectionForm:inspectionFormDictionary forMeter:self.meter];
	
	if (isSaved==NO) {
		[self showAlert:@"Problem while saving inspection"];
	}else{
		[self popOrCloseViewController];
	}
}

-(void) initializeMeterDetails {
	
	_readingDate = self.meter.readingDate;
	_isPassed = [self convertNumberToString:self.meter.isPassed];
	_isCapped = [self convertNumberToString:self.meter.isCapped];
	_notes = self.meter.notes;
	_reading = self.meter.reading;
	
}

-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSTMDatePickerCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSTMLabelCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSTMPickerCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSTMTextFieldCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	
}

- (NSString*) convertNumberToString:(NSNumber *) number
{
	return 	([number boolValue]==YES)?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO");
}

- (NSNumber*) getNSNumberValue:(NSString *) string
{
	return 	(string==LOC(@"KEY_ALERT_YES"))?[NSNumber numberWithBool:YES]:[NSNumber numberWithBool:NO];
}


@end
