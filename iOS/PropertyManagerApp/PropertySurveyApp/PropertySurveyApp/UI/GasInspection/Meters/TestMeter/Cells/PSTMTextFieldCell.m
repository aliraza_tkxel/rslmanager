//
//  PSAddApplianceTextFieldCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSTMTextFieldCell.h"

@implementation PSTMTextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (IBAction)onClickEditBtn:(id)sender
{
	  [self.txtTitle setKeyboardType:UIKeyboardTypeDecimalPad];
    [self.txtTitle becomeFirstResponder];
}

#pragma mark - Methods

- (BOOL) isValid
{
	
	NSArray  *arrayOfString = [self.txtTitle.text componentsSeparatedByString:@"."];
	
    BOOL isvalid = YES;
    if (isEmpty(self.txtTitle.text))
    {
        isvalid = NO;
    }else if([arrayOfString count] > 2)
		{
			isvalid = NO;
		}else if([arrayOfString count] == 2
						 && [self.txtTitle.text length] ==1)
		{
			isvalid = NO;
		}
	
    return isvalid;
}

- (void) configureCell:(id) data
{
  [self.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.txtTitle setKeyboardType:UIKeyboardTypeDecimalPad];
  [self.txtTitle setText:nil];
  
  NSString* reading = (NSString*) data;
  if (!isEmpty(reading))
    {
    [self.txtTitle setText:reading];
    }

}
@end
