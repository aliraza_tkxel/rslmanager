//
//  PSAddAppointmentDatePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTestMeterViewController.h"

@interface PSTMDatePickerCell : UITableViewCell
@property (weak,    nonatomic) id<PSTestMeterProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSDate *selectedDate;
@property (weak, nonatomic) IBOutlet UIButton *resetDatesButton;
- (IBAction)onclickPickerBtn:(id)sender;
- (BOOL) isValid;
-(void) configureCell:(id) data;
@end
