//
//  PSDefectsPickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSDefectsViewController.h"

@interface PSDefectsPickerCell : UITableViewCell
@property (weak,    nonatomic) id<PSDefectsProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSArray *pickerOptions;
- (IBAction)onClickPickerBtn:(id)sender;
@property (assign, nonatomic) NSInteger selectedIndex;
@end
