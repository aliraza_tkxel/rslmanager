//
//  PSAddDefectPickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddDefectPickerCell.h"

@implementation PSAddDefectPickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

- (IBAction)onClickPickerBtn:(id)sender
{
	if(!isEmpty(self.pickerOptions))
	{
		
		[ActionSheetStringPicker showPickerWithTitle:self.lblHeader.text
																						rows:self.pickerOptions
																initialSelection:self.selectedIndex
																					target:self
																	 successAction:@selector(onOptionSelected:element:)
																		cancelAction:@selector(onOptionPickerCancelled:)
																					origin:sender];
	}
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
	self.selectedIndex = [selectedIndex integerValue];
	NSString *title;
	id item = [self.pickerOptions objectAtIndex:self.selectedIndex];
	
	if ([item isKindOfClass:[NSString class]] == NO) {
		title = [item stringValue];
	}else{
	 title = (NSString*) item;
	}
	
	self.lblTitle.text = title;
	if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectPickerOption:)])
	{
		[(PSAddDefectsViewController *)self.delegate didSelectPickerOption:self.selectedIndex];
	}
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

- (void) configureCell:(NSString*)headerText
						 withTitle:(NSString*) titleText
			withPickerOption:pickerOptions
					withSelector:(SEL) selector
					withDelegate:(id<PSAddDefectProtocol>) deleg
						withTarget:(id)target

{
	[self setDelegate:deleg];
	[self.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
	[self.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblHeader setText:headerText];
	[self.lblTitle setText:titleText];
	self.pickerOptions = pickerOptions;
	[self.btnPicker addTarget:target
										 action:selector
					 forControlEvents:UIControlEventTouchUpInside];
	
}

@end
