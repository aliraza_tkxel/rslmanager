//
//  PSAddDefectTextFieldCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAddDefectsViewController.h"
@interface PSAddDefectTextFieldCell : UITableViewCell <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgEdit;
- (IBAction)onClickBtnEdit:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;

@end
