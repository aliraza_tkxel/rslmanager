//
//  PSAddDefectsViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddDefectsViewController.h"
#import "PSAddDefectTextViewCell.h"
#import "PSAddDefectTextFieldCell.h"
#import "PSAddDefectPickerCell.h"
#import "PSAddDefectDatePickerCell.h"

#import "PSCameraViewController.h"

#import "PSTextViewCell.h"
#import "Defect+JSON.h"

#define kApplianceDefectNumberOfRows 24
#define kDetectorDefectNumberOfRows 11
#define kRowDefectCategory 0
#define kRowDefectDate 1
#define kRowDefectIdentified 2
#define kRowDefectNotes 3
#define kRowRemedialAction 4
#define kRowRemedialNotes 5
#define kRowWarningIssued 6
#define kRowWarningNoteSerialNo 7
#define kRowSelectAppliance 8
#define kRowSerialNumber 9
#define kRowWarningStickerFixed 10
#define kRowGCNumber 11
#define kRowDisconnected 12
#define kRowPartsRequired 13
#define kRowPartsOrdered 14
#define kRowPartsOrderedBy 15
#define kRowPartsDue 16
#define kRowPartsDescription 17
#define kRowPartsLocation 18
#define kRowTwoPersonJob 19
#define kRowReasonForTwoPersonJob 20
#define kRowDuration 21
#define kRowPriority 22
#define kRowTrade 23

#define kDefectPickerCellHeight 44
#define kDefectTextFieldCellHeight 44
#define kDefectTextViewCellHeight 80

static NSString *pickerCellIdentifier = @"addDefectPickerCellIdentifier";
static NSString *textFieldCellIdentifier = @"textFieldCellIdentifier";
static NSString *datePickerCellIdentifier = @"addDefectDatePickerCellIdentifier";

@interface PSAddDefectsViewController ()

@property(nonatomic,strong) PSBarButtonItem * cameraButton;
@end

@implementation PSAddDefectsViewController
{
	
	BOOL _isDefectIdentified;
	NSString *_isDefectIdentifiedString;
	BOOL _isRemedialActionTaken;
	NSString *_isRemedialActionTakenString;
	BOOL _isWarningIssued;
	NSString *_isWarningIssuedString;
	BOOL _isStickerFixed;
	NSString *_isStickerFixedString;
	NSString *_serialNumber;
	NSString *_warningNoteSerialNo;
	NSString *_remedialNotes;
	NSString *_defectNotes;
	NSString *_applianceName;
	NSString *_boilerName;
	Employee *_operative;
	Trade *_trade;
	DefectPriority *_priority;
	NSInteger _pickerTag;
	
	NSString *_gcNumber;
	NSString *_isPartRequiredString;
	NSString *_isDisconnectedString;
	NSString *_isPartsOrderedString;
	NSNumber *_partsOrderedBy;
	NSDate *_partsDue;
	NSDate *_defectDate;
	NSString *_partsDescription;
	NSString *_partsLocation;
	NSString *_isTwoPersonJobString;
	NSString *_reasonForTwoPerson;
	NSNumber *_duration;
	NSString *_durationString;
	
	DefectCategory *_defectCategory;
	Appliance *_appliance;
	Boiler *_boiler;
	Detector *_detector;
	
	NSArray *_yesNoPickerOptions;
	NSArray *_yesNoNaPickerOptions;
	NSArray *_durationPickerOptions;
	NSArray *_priorityPickerOptions;
	NSArray *_tradePickerOptions;
	
	NSMutableArray *_defectCategories;
	NSArray *_appliances;
	NSArray *_detectors;
	NSArray *_boilers;
	NSArray *_operatives;
	NSArray *_priorities;
	NSArray *_trades;
	
	NSMutableArray *_defectCategoriesStrings;
	NSMutableArray *_appliancesNames;
	NSMutableArray *_boilerNames;
	NSMutableArray *_operativeNames;
	NSMutableArray *_priorityList;
	NSArray *_durationList;
	NSMutableArray *_tradeList;
	NSMutableArray *_detectorNames;
	
	NSMutableDictionary *_defectDictionary;
	BOOL _isSavingFromCamera;
	//PSTextView *_textView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self initializeCollection];
	[self loadNavigationonBarItems];
	[self populatePickersData];
	[self loadDefaultValues];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self registerNotification];
	[self reloadPhotographsBadge];
	
	if(self.defect)
	{
		CLS_LOG(@"Defect ID = %@",self.defect.defectID);
	}
	else
	{
		CLS_LOG(@"NEW Defect ID = %@",self.defectNewID);
	}

}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self deRegisterNotifications];

}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];

	// Dispose of any resources that can be recreated.
}

-(NSUInteger) getPhotographsCount
{
	NSArray * photographsList = nil;
	if([self.defect.defectID intValue]<0)
	{
		photographsList =[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:self.defect.defectID andJournalID:self.defect.journalId];
	}
	else
	{
		photographsList =[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:self.defect.defectID];
	}
	
	return [photographsList count];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	PSBarButtonItem *saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
																																								style:PSBarButtonItemStyleDefault
																																							 target:self
																																							 action:@selector(onClickSaveButton)];
	
	if (self.defect)
	{
		//Camera Button
		NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
		
		self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																															 style:PSBarButtonItemStyleCamera
																															target:self
																															action:@selector(onClickCameraButton)bagde:badgeText];
		
		[self setRightBarButtonItems:[NSArray arrayWithObjects: saveButton,self.cameraButton, nil]];
	}else
	{
		NSString * badgeText= @"0";
		
		self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																															 style:PSBarButtonItemStyleCamera
																															target:self
																															action:@selector(onClickCameraButton)bagde:badgeText];
		
		[self setRightBarButtonItems:[NSArray arrayWithObjects: saveButton,self.cameraButton, nil]];
	}
	
	
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	if (self.defect)
	{
		[self setTitle:@"Defect"];
	}
	else
	{
		[self setTitle:@"Add Defect"];
	}
	
}

-(void) reloadPhotographsBadge
{
	if (self.cameraButton && self.defect) {
		
		NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
		[self.cameraButton reloadBadge:badgeText];
	}
	else
	{
		NSString * badgeText=@"";
		[self.cameraButton reloadBadge:badgeText];
	}
	
}

- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickSaveButton
{
	CLS_LOG(@"onClickSaveButton");
	_isSavingFromCamera = NO;
	[self resignAllResponders];
	
	if ([self isFormValid]) {
		[self saveDefect];
	}
	
}



- (void)onClickCameraButton {
	
	CLS_LOG(@"Camera Button");
	
	if(self.defect)
	{
		PSCameraViewController *cameraViewController = [[PSCameraViewController alloc] initWithNibName:@"PSCameraViewController" bundle:[NSBundle mainBundle]];
		[cameraViewController setImageType:CameraViewImageTypeDefect];
		[cameraViewController setAppointment:self.appointment];
		[cameraViewController setDefect:self.defect];
        if(!isEmpty(self.defect.defectToBoiler)){
            [cameraViewController setHeatingId:self.defect.defectToBoiler.heatingId];
        }
        
        [cameraViewController setItemId:!isEmpty(self.defect.defectToBoiler)?self.defect.defectToBoiler.itemId:[NSNumber numberWithInteger:0]];
		[self.navigationController pushViewController:cameraViewController animated:YES];
	}
	else
	{
		CLS_LOG(@"Saving On Camera");
		_isSavingFromCamera = YES;
		[self resignAllResponders];
		if ([self isFormValid]) {
			[self saveDefect];
		}
	}
	
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0;
	switch (indexPath.row)
	{
		case kRowDefectNotes:
			rowHeight = [self heightForTextView:nil containingString:_defectNotes];
			if (rowHeight < kDefectTextViewCellHeight)
			{
				rowHeight = kDefectTextViewCellHeight;
			}
			break;
		case kRowRemedialNotes:
			rowHeight = [self heightForTextView:nil containingString:_remedialNotes];
			if (rowHeight < kDefectTextViewCellHeight)
			{
				rowHeight = kDefectTextViewCellHeight;
			}
			break;
		case kRowPartsDescription:
			rowHeight = [self heightForTextView:nil containingString:_partsDescription];
			if (rowHeight < kDefectTextViewCellHeight)
			{
				rowHeight = kDefectTextViewCellHeight;
			}
			break;
		case kRowReasonForTwoPersonJob:
			rowHeight = [self heightForTextView:nil containingString:_reasonForTwoPerson];
			if (rowHeight < kDefectTextViewCellHeight)
			{
				rowHeight = kDefectTextViewCellHeight;
			}
			break;
		case kRowSerialNumber:
		case kRowWarningNoteSerialNo:
		case kRowPartsLocation :
		case kRowGCNumber :
			rowHeight = kDefectTextFieldCellHeight;
			break;
		case kRowDefectCategory:
		case kRowDefectIdentified:
		case kRowWarningIssued:
		case kRowWarningStickerFixed:
		case kRowSelectAppliance:
		case kRowPartsRequired:
		case kRowPartsOrdered:
		case kRowPartsOrderedBy:
		case kRowPartsDue:
		case kRowDefectDate:
		case kRowDisconnected:
		case kRowTwoPersonJob:
		case kRowPriority:
		case kRowDuration:
		case kRowRemedialAction:
		case kRowTrade:
			rowHeight = kDefectPickerCellHeight;
			break;
		default:
			break;
	}
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (self.viewMode == ApplianceViewMode
			|| self.viewMode == BoilerViewMode)
		return kApplianceDefectNumberOfRows;
	return kDetectorDefectNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;
	if (indexPath.row == kRowDefectNotes
			|| indexPath.row == kRowRemedialNotes
			|| indexPath.row == kRowPartsDescription
			|| indexPath.row == kRowReasonForTwoPersonJob)
	{
		PSTextViewCell *_cell = [self createCellWithIdentifier:textViewCellIdentifier];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.row == kRowSerialNumber
					 || indexPath.row == kRowGCNumber
					 || indexPath.row == kRowPartsLocation)
	{
		PSAddDefectTextFieldCell *_cell = [self createCellWithIdentifier:textFieldCellIdentifier];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.row == kRowWarningNoteSerialNo)
	{
		PSAddDefectTextFieldCell *_cell = [self createCellWithIdentifier:textFieldCellIdentifier];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
		if(self.defect == nil ||
			 self.viewMode == DetectorViewMode ||
			 _isWarningIssued == FALSE)
		{
			[_cell.txtTitle setEnabled:FALSE];
		}
		if(_isWarningIssued == TRUE)
		{
			[_cell.txtTitle setEnabled:TRUE];
		}
	}
	else if ((indexPath.row == kRowDefectCategory)
					 || (indexPath.row == kRowDefectIdentified)
					 || (indexPath.row == kRowRemedialAction)
					 || (indexPath.row == kRowWarningIssued)
					 || (indexPath.row == kRowWarningStickerFixed)
					 || (indexPath.row == kRowDisconnected)
					 || (indexPath.row == kRowPartsRequired)
					 || (indexPath.row == kRowPartsOrdered)
					 || (indexPath.row == kRowPartsOrderedBy)
					 || (indexPath.row == kRowTwoPersonJob)
					 || (indexPath.row == kRowDuration)
					 || (indexPath.row == kRowPriority)
					 || (indexPath.row == kRowTrade)
					 || (indexPath.row == kRowSelectAppliance))
	{
		PSAddDefectPickerCell *_cell = [self createCellWithIdentifier:pickerCellIdentifier];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if ((indexPath.row == kRowPartsDue)
					 || (indexPath.row == kRowDefectDate))
	{
		PSAddDefectDatePickerCell *_cell = [self createCellWithIdentifier:datePickerCellIdentifier];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
	if([identifier isEqualToString:pickerCellIdentifier])
	{
		PSAddDefectPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:pickerCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSAddDefectPickerCell" owner:self options:nil];
			_cell = self.addDefectPcikerCell;
			self.addDefectPcikerCell = nil;
		}
		return _cell;
	}
	else if ([identifier isEqualToString:textFieldCellIdentifier])
	{
		PSAddDefectTextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:textFieldCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSAddDefectTextFieldCell" owner:self options:nil];
			_cell = self.addDefectTextFieldCell;
			self.addDefectTextFieldCell = nil;
		}
		
		return _cell;
	}
	else if([identifier isEqualToString:textViewCellIdentifier])
	{
		PSTextViewCell *  _cell = [[PSTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:textViewCellIdentifier];
		self.textViewCell = _cell;
		return _cell;
	}
	else if ([identifier isEqualToString:datePickerCellIdentifier])
	{
		PSAddDefectDatePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:datePickerCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSAddDefectDatePickerCell" owner:self options:nil];
			_cell = self.addDefectDatePickerCell;
			self.addDefectDatePickerCell = nil;
		}
		return _cell;
	}
	
	return nil;
}


- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
	if ([*cell isKindOfClass:[PSTextViewCell class]])
	{
		PSTextViewCell *_cell = (PSTextViewCell *)*cell;
		
		UILabel *header = [[UILabel alloc] init];
		header.frame = CGRectMake(12, 3, 320,12);
		[header setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12]];
		[_cell addSubview:header];
		
		UIImage *image = [UIImage imageNamed:@"line"];
		UIImageView *seperatorImage = [[UIImageView alloc] init];
		[seperatorImage setImage:image];
		[seperatorImage setFrame:CGRectMake(0, 1, 320, 1)];
		[_cell addSubview:seperatorImage];
		
		PSTextView*  _textView = [[PSTextView alloc] initWithFrame:CGRectZero];
		[_textView setDelegate:self];
		[_textView setTag:indexPath.row];

		
		switch (indexPath.row)
		{
			case kRowDefectNotes:
				[header setText:LOC(@"DEFECT NOTES")];
				_textView.text = _defectNotes;
				break;
			case kRowRemedialNotes:
				[header setText:LOC(@"REMEDIAL NOTES")];
				_textView.text = _remedialNotes;
				break;
			case kRowPartsDescription:
				[header setText:LOC(@"PARTS DESCRIPTION")];
				_textView.text = _partsDescription;
				break;
			case kRowReasonForTwoPersonJob:
				[header setText:LOC(@"REASON FOR 2 PERSON JOB")];
				_textView.text = _reasonForTwoPerson;
				if ([_isTwoPersonJobString boolValue])
				{
					[_textView setUserInteractionEnabled:YES];
					[_textView setEditable:YES];
				}
				else
				{
					[_textView setUserInteractionEnabled:NO];
					[_textView setEditable:NO];
					_reasonForTwoPerson = nil;
					_textView.text = _reasonForTwoPerson;
				}
				break;
				
			default:
				break;
		}
		[_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
		[_textView setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
		[header setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
		[header setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
		_cell.textView = _textView;
		_textView = nil;
		*cell = _cell;
	}
	
	else if ([*cell isKindOfClass:[PSAddDefectTextFieldCell class]])
	{
		PSAddDefectTextFieldCell *_cell = (PSAddDefectTextFieldCell *) *cell;
		
		[_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
		[_cell.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
		_cell.txtTitle.delegate = self;
		
		switch (indexPath.row)
		{
			case kRowSerialNumber:
				[_cell.lblHeader setText:@"SERIAL NUMBER"];
				[_cell.txtTitle setTag:kRowSerialNumber];
				[_cell.txtTitle setText:_serialNumber];
				break;
			case kRowWarningNoteSerialNo:
				[_cell.lblHeader setText:@"WARNING/ADVICE NOTE SERIAL No"];
				[_cell.txtTitle setTag:kRowWarningNoteSerialNo];
				[_cell.txtTitle setText:_warningNoteSerialNo];
				break;
			case kRowGCNumber:
				[_cell.lblHeader setText:@"GC NUMBER"];
				[_cell.txtTitle setTag:kRowGCNumber];
				[_cell.txtTitle setText:_gcNumber];
				[_cell.txtTitle setKeyboardType:UIKeyboardTypeNumberPad];
				break;
			case kRowPartsLocation:
				[_cell.lblHeader setText:@"PARTS LOCATION"];
				[_cell.txtTitle setTag:kRowPartsLocation];
				[_cell.txtTitle setText:_partsLocation];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSAddDefectPickerCell class]])
	{
		PSAddDefectPickerCell *_cell = (PSAddDefectPickerCell *) *cell;
		switch (indexPath.row)
		{
			case kRowDefectCategory:
				[_cell configureCell:@"DEFECT CATEGORY"
									 withTitle:_defectCategory.categoryDescription
						withPickerOption:_defectCategoriesStrings
								withSelector:@selector(onClickDefectsCategory:)
								withDelegate:self
									withTarget:self];
				break;
			case kRowDefectIdentified:
				
				[_cell configureCell:@"DEFECT IDENTIFIED"
									 withTitle:_isDefectIdentifiedString
						withPickerOption:_yesNoPickerOptions
								withSelector:@selector(onClickDefectsIdentified:)
								withDelegate:self
									withTarget:self];
				break;
			case kRowRemedialAction:
				
				[_cell configureCell:@"REMEDIAL ACTION TAKEN"
									 withTitle:_isRemedialActionTakenString
						withPickerOption:_yesNoPickerOptions
								withSelector:@selector(onClickRemedialAction:)
								withDelegate:self
									withTarget:self];
				break;
			case kRowWarningIssued:
				
				[_cell configureCell:@"WARNING/ADVICE NOTE ISSUED"
									 withTitle:_isWarningIssuedString
						withPickerOption:_yesNoPickerOptions
								withSelector:@selector(onClickWarningIssued:)
								withDelegate:self
									withTarget:self];
				break;
			case kRowWarningStickerFixed:
				
				[_cell configureCell:@"WARNING TAG/STICKER FIXED"
									 withTitle:_isStickerFixedString
						withPickerOption:_yesNoPickerOptions
								withSelector:@selector(onClickWarningSticker:)
								withDelegate:self
									withTarget:self];
				break;
				
			case kRowSelectAppliance:
				
				if (self.viewMode == ApplianceViewMode)
				{
					
					[_cell configureCell:@"SELECT APPLIANCE"
										 withTitle:_applianceName
							withPickerOption:_appliancesNames
									withSelector:@selector(onClickSelectAppliance:)
									withDelegate:self
										withTarget:self];
				}
				else if (self.viewMode == DetectorViewMode)
				{
					
					[_cell configureCell:@"SELECT DETECTOR"
										 withTitle:_detector.detectorType
							withPickerOption:_detectorNames
									withSelector:@selector(onClickSelectAppliance:)
									withDelegate:self
										withTarget:self];
				}
				else if (self.viewMode == BoilerViewMode)
				{
					
					[_cell configureCell:@"SELECT BOILER"
										 withTitle:_boiler.boilerName
							withPickerOption:_boilerNames
									withSelector:@selector(onClickSelectBoiler:)
									withDelegate:self
										withTarget:self];
				}
				break;
				
			case kRowDisconnected:
				
				[_cell configureCell:@"DISCONNECTED"
									 withTitle:_isDisconnectedString
						withPickerOption:_yesNoNaPickerOptions
								withSelector:@selector(onClickDisconnected:)
								withDelegate:self
									withTarget:self];
				break;
				
			case kRowPartsRequired:
				
				[_cell configureCell:@"PARTS REQUIRED"
									 withTitle:_isPartRequiredString
						withPickerOption:_yesNoNaPickerOptions
								withSelector:@selector(onClickPartsRequired:)
								withDelegate:self
									withTarget:self];
				break;
				
			case kRowPartsOrdered:
				
				[_cell configureCell:@"PARTS ORDERED"
									 withTitle:_isPartsOrderedString
						withPickerOption:_yesNoNaPickerOptions
								withSelector:@selector(onClickPartsOrdered:)
								withDelegate:self
									withTarget:self];
				break;
				
			case kRowPartsOrderedBy:
				
				[_cell configureCell:@"PARTS ORDERED BY"
									 withTitle:_operative.employeeName
						withPickerOption:_operativeNames
								withSelector:@selector(onClickPartsOrderedBy:)
								withDelegate:self
									withTarget:self];
				break;
			case kRowTwoPersonJob:
				[_cell configureCell:@"2 PERSON JOB"
									 withTitle:_isTwoPersonJobString
						withPickerOption:_yesNoPickerOptions
								withSelector:@selector(onClickTwoPersonJob:)
								withDelegate:self
									withTarget:self];
				break;
				
			case kRowDuration:
				
				[_cell configureCell:@"ESTIMATED DURATION"
									 withTitle:_durationString
						withPickerOption:_durationPickerOptions
								withSelector:@selector(onClickDuration:)
								withDelegate:self
									withTarget:self];
				break;
				
			case kRowPriority:
				
				[_cell configureCell:@"PRIORITY"
									 withTitle:_priority.priority
						withPickerOption:_priorityList
								withSelector:@selector(onClickPriority:)
								withDelegate:self
									withTarget:self];
				break;
				
			case kRowTrade:
				
				[_cell configureCell:@"TRADE"
									 withTitle:_trade.trade
						withPickerOption:_tradeList
								withSelector:@selector(onClickTrade:)
								withDelegate:self
									withTarget:self];
				break;
				
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSAddDefectDatePickerCell class]])
	{
		PSAddDefectDatePickerCell *_cell = (PSAddDefectDatePickerCell *)*cell;
		[_cell setDelegate:self];
		[_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
		[_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
		switch (indexPath.row)
		{
			case kRowPartsDue:
				[_cell.lblHeader setText:@"PARTS DUE"];
				[_cell.lblTitle setText:nil];
				[_cell.resetDatesButton setHidden:YES];
				if (!isEmpty(_partsDue))
				{
					[_cell.lblTitle setText:[UtilityClass stringFromDate:_partsDue dateFormat:kDateTimeStyle8]];
					[_cell.resetDatesButton setHidden:NO];
				}
				[_cell.btnPicker addTarget:self action:@selector(onClickPartsDueDate:) forControlEvents:UIControlEventTouchUpInside];
				[_cell.resetDatesButton addTarget:self action:@selector(onClickResetPartsDueDate:) forControlEvents:UIControlEventTouchUpInside];
				break;
			case kRowDefectDate:
				[_cell.lblHeader setText:@"DATE"];
				[_cell.lblTitle setText:nil];
				[_cell.resetDatesButton setHidden:YES];
				if (!isEmpty(_defectDate))
				{
					[_cell.lblTitle setText:[UtilityClass stringFromDate:_defectDate dateFormat:kDateTimeStyle8]];
					[_cell.resetDatesButton setHidden:NO];
				}
				[_cell.btnPicker addTarget:self action:@selector(onClickDefectDate:) forControlEvents:UIControlEventTouchUpInside];
				[_cell.resetDatesButton addTarget:self action:@selector(onClickResetDefectDate:) forControlEvents:UIControlEventTouchUpInside];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickPartsDueDate:(id)sender
{
	CLS_LOG(@"onClickPartsDue");
	[self resignAllResponders];
	_pickerTag = kRowPartsDue;
}

- (IBAction)onClickDefectDate:(id)sender
{
	CLS_LOG(@"onClickDefectDate");
	[self resignAllResponders];
	_pickerTag = kRowDefectDate;
}

- (IBAction)onClickResetPartsDueDate:(id)sender
{
	CLS_LOG(@"onClickResetPartsDueDate");
	[self resignAllResponders];
	_partsDue = nil;
	[self.tableView reloadData];
}

- (IBAction)onClickResetDefectDate:(id)sender
{
	CLS_LOG(@"onClickResetDefectDate");
	[self resignAllResponders];
	_defectDate = nil;
	[self.tableView reloadData];
}

- (IBAction)onClickDefectsCategory:(id)sender
{
	CLS_LOG(@"onClickDefectsCategory");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowDefectCategory;
}

- (IBAction)onClickDefectsIdentified:(id)sender
{
	CLS_LOG(@"onClickDefectsIdentified");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowDefectIdentified;
}

- (IBAction)onClickRemedialAction:(id)sender
{
	CLS_LOG(@"onClickRemedialAction");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowRemedialAction;
}

- (IBAction)onClickWarningIssued:(id)sender
{
	CLS_LOG(@"onClickWarningIssued");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowWarningIssued;
}

- (IBAction)onClickWarningSticker:(id)sender
{
	CLS_LOG(@"onClickWarningSticker");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowWarningStickerFixed;
}

- (IBAction)onClickDisconnected:(id)sender
{
	CLS_LOG(@"onClickDisconnected");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowDisconnected;
}

- (IBAction)onClickPartsRequired:(id)sender
{
	CLS_LOG(@"onClickPartsRequired");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowPartsRequired;
}

- (IBAction)onClickPartsOrdered:(id)sender
{
	CLS_LOG(@"onClickPartsOrdered");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowPartsOrdered;
}

- (IBAction)onClickPartsOrderedBy:(id)sender
{
	CLS_LOG(@"onClickPartsOrderedBy");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowPartsOrderedBy;
}

- (IBAction)onClickPartsDue:(id)sender
{
	CLS_LOG(@"onClickPartsDue");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowPartsDue;
}

- (IBAction)onClickTwoPersonJob:(id)sender
{
	CLS_LOG(@"onClickTwoPersonJob");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowTwoPersonJob;
}

- (IBAction)onClickDuration:(id)sender
{
	CLS_LOG(@"onClickDuration");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowDuration;
}

- (IBAction)onClickPriority:(id)sender
{
	CLS_LOG(@"onClickPriority");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowPriority;
}

- (IBAction)onClickTrade:(id)sender
{
	CLS_LOG(@"onClickTrade");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kRowTrade;
}

- (IBAction)onClickSelectAppliance:(id)sender
{
	CLS_LOG(@"onClickSelectAppliance");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	if (self.viewMode == ApplianceViewMode)
	{
		if (isEmpty(_appliances))
		{
			UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_NO_APPLIANCES")
																										 message:LOC(@"KEY_ALERT_NO_APPLIANCES_DETAIL")
																										delegate:nil
																					 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																					 otherButtonTitles:nil,nil];
			[alert show];
		}
	}
	_pickerTag = kRowSelectAppliance;
}

- (IBAction)onClickSelectBoiler:(id)sender
{
	CLS_LOG(@"onClickSelectBoiler");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	if (self.viewMode == BoilerViewMode)
	{
		if (isEmpty(_boilers))
		{
			UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_NO_BOILERS")
																										 message:LOC(@"KEY_ALERT_NO_BOILERS_DETAIL")
																										delegate:nil
																					 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																					 otherButtonTitles:nil,nil];
			[alert show];
		}
	}
	_pickerTag = kRowSelectAppliance;
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
	switch (textField.tag)
	{
		case kRowSerialNumber:
			_serialNumber = textField.text;
			break;
		case kRowGCNumber:
			_gcNumber = textField.text;
			break;
		case kRowPartsLocation:
			_partsLocation = textField.text;
			break;
		case kRowWarningNoteSerialNo:
			_warningNoteSerialNo = textField.text;
		default:
			break;
	}

}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	BOOL textFieldShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
	switch (textField.tag)
	{
		case kRowSerialNumber:
			
			if ([stringAfterReplacement length] > 10)
			{
				textFieldShouldReturn = NO;
			}
			break;
		case kRowGCNumber:
		{
			if (range.location == 9 || (textField.text.length >= 9 && range.length == 0) || string.length + textField.text.length > 9 ) {
				return NO;
			}
			
			// Reject appending non-digit characters
			if (range.length == 0 &&
					![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
				return NO;
			}
			
			UITextRange* selRange = textField.selectedTextRange;
			UITextPosition *currentPosition = selRange.start;
			NSInteger pos = [textField offsetFromPosition:textField.beginningOfDocument toPosition:currentPosition];
			if (range.length != 0) { //deleting
				if (range.location == 2 || range.location == 6) { //deleting a dash
					if (range.length == 1) {
						range.location--;
						pos-=2;
					}
					else {
						pos++;
					}
				}
				else {
					if (range.length > 1) {
						NSString* selectedRange = [textField.text substringWithRange:range];
						NSString* hyphenless = [selectedRange stringByReplacingOccurrencesOfString:@"-" withString:@""];
						NSInteger diff = selectedRange.length - hyphenless.length;
						pos += diff;
					}
					pos --;
				}
			}
			
			NSMutableString* changedString = [NSMutableString stringWithString:[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:@"-" withString:@""]];
			if (changedString.length > 2) {
				[changedString insertString:@"-" atIndex:2];
				if (pos == 2) {
					pos++;
				}
			}
			if (changedString.length > 6) {
				[changedString insertString:@"-" atIndex:6];
				if (pos == 6) {
					pos++;
				}
			}
			pos += string.length;
			
			textField.text = changedString;
			if (pos > changedString.length) {
				pos = changedString.length;
			}
			currentPosition = [textField positionFromPosition:textField.beginningOfDocument offset:pos];
			
			[textField setSelectedTextRange:[textField textRangeFromPosition:currentPosition toPosition:currentPosition]];
			return NO;
			
			
		}
			break;
		case kRowPartsLocation:
			
			if ([stringAfterReplacement length] > 200)
			{
				textFieldShouldReturn = NO;
			}
			break;
		default:
			break;
	}
	
	return textFieldShouldReturn;
	
}

#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	NSUInteger lengthAllowed = 200;
	if (textView.tag == kRowRemedialNotes) {
		lengthAllowed = 200;
	}else if (textView.tag == kRowDefectNotes){
		lengthAllowed = 200;
	}else if (textView.tag == kRowPartsDescription){
		lengthAllowed = 1000;
	}else if (textView.tag == kRowReasonForTwoPersonJob){
		lengthAllowed = 1000;
	}
	
	BOOL textVeiwShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextView *)self.selectedControl).text stringByAppendingString:text];
	
	if ([stringAfterReplacement length] > lengthAllowed)
	{
		textVeiwShouldReturn = NO;
	}
	return textVeiwShouldReturn;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	self.selectedControl = (UIControl *)textView;
	
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	if (textView.tag == kRowDefectNotes)
	{
		_defectNotes = textView.text;
	}
	else if (textView.tag == kRowRemedialNotes)
	{
		_remedialNotes = textView.text;
	}
	else if (textView.tag == kRowPartsDescription)
	{
		_partsDescription = textView.text;
	}
	else if (textView.tag == kRowReasonForTwoPersonJob)
	{
		_reasonForTwoPerson = textView.text;
	}
}

- (void)textViewDidChange:(UITextView *)textView
{
	if (textView.tag == kRowDefectNotes)
	{
		_defectNotes = textView.text;
	}
	else if (textView.tag == kRowRemedialNotes)
	{
		_remedialNotes = textView.text;
	}
	else if (textView.tag == kRowPartsDescription)
	{
		_partsDescription = textView.text;
	}
	else if (textView.tag == kRowReasonForTwoPersonJob)
	{
		_reasonForTwoPerson = textView.text;
	}
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	[self scrollToRectOfSelectedControl];
}

#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[self resignAllResponders];
}

#pragma mark - Scroll Cell Methods
- (void)scrollToRectOfSelectedControl {
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

#pragma mark - Notifications

- (void) registerNotification
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveDefectSuccessNotification) name:kSaveDefectSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveDefectFailureNotification) name:kSaveDefectFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveDefectSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveDefectFailureNotification object:nil];
}

- (void) onReceiveSaveDefectSuccessNotification
{
	[self hideActivityIndicator];
	[self enableNavigationBarButtons];
	if(!_isSavingFromCamera)
	{
		[self popOrCloseViewController];
	}
	else
	{
		self.defect = [[PSApplianceManager sharedManager] fetchApplianceDefectWithDefectID:self.defectNewID];
		//Double check. the defect object
		if(self.defect)
		{
			CLS_LOG(@"%@",[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self.defect JSONValue] options:0 error:nil] encoding:NSUTF8StringEncoding]);
			[self onClickCameraButton];
		}
	}
	_isSavingFromCamera=NO;
}

- (void) onReceiveSaveDefectFailureNotification
{
	[self hideActivityIndicator];
	[self enableNavigationBarButtons];
	dispatch_async(dispatch_get_main_queue(), ^{
		
		[self.activityIndicator stopAnimating];
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																									 message:LOC(@"KEY_ALERT_DEFECT_REPAIR_SAVE_FAILURE")
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
		
	});
}

#pragma mark - Protocol Methods

/*!
 @discussion
 Method didSelectPickerOption Called upon selection of picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerIndex
{
	switch (_pickerTag) {
		case kRowDefectCategory:
		{
			_defectCategory = [_defectCategories objectAtIndex:pickerIndex];
		}
			break;
		case kRowDefectIdentified:
		{
			_isDefectIdentifiedString = [_yesNoPickerOptions objectAtIndex:pickerIndex];
			_isDefectIdentified = [_isDefectIdentifiedString boolValue];
		}
			break;
		case kRowTwoPersonJob:
		{
			_isTwoPersonJobString = [_yesNoPickerOptions objectAtIndex:pickerIndex];
		}
			break;
		case kRowRemedialAction:
		{
			_isRemedialActionTakenString = [_yesNoPickerOptions objectAtIndex:pickerIndex];
			_isRemedialActionTaken = [_isRemedialActionTakenString boolValue];
		}
			break;
		case kRowWarningIssued:
		{
			_isWarningIssuedString = [_yesNoPickerOptions objectAtIndex:pickerIndex];
			_isWarningIssued = [_isWarningIssuedString boolValue];
			PSAddDefectTextFieldCell * cellView = (PSAddDefectTextFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:kRowWarningNoteSerialNo inSection:0]];
			if(self.viewMode == BoilerViewMode ||
				 self.viewMode == ApplianceViewMode)
			{
				if (_isWarningIssued == FALSE) //if warning issued 'No'
				{
					_serialNumber = nil;
					_warningNoteSerialNo = nil;
					[cellView.txtTitle setEnabled:FALSE];
				}
				else //if warning issued 'Yes'
				{
					[cellView.txtTitle setEnabled:TRUE];
				}
			}
			else
			{
				[cellView.txtTitle setEnabled:FALSE];
			}
		}
			break;
		case kRowWarningStickerFixed:
		{
			_isStickerFixedString = [_yesNoPickerOptions objectAtIndex:pickerIndex];
			_isStickerFixed = [_isStickerFixedString boolValue];
		}
			break;
		case kRowSelectAppliance:
		{
			if (self.viewMode == ApplianceViewMode)
			{
				_applianceName = [_appliancesNames objectAtIndex:pickerIndex];
				_appliance = [_appliances objectAtIndex:pickerIndex];
				_serialNumber =_appliance.serialNumber;
				_gcNumber = _appliance.gcNumber;
			}
			else if (self.viewMode == DetectorViewMode)
			{
				_detector = [_detectors objectAtIndex:pickerIndex];
				
			}
			else if (self.viewMode == BoilerViewMode)
			{
				_boilerName = [_boilerNames objectAtIndex:pickerIndex];
				_boiler = [_boilers objectAtIndex:pickerIndex];
				_serialNumber =_boiler.serialNumber;
				_gcNumber = _boiler.gcNumber;
			}
		}
			break;
		case kRowDisconnected:
		{
			_isDisconnectedString = [_yesNoNaPickerOptions objectAtIndex:pickerIndex];
		}
			break;
			
		case kRowPartsRequired:
		{
			_isPartRequiredString = [_yesNoNaPickerOptions objectAtIndex:pickerIndex];
			
		}
			break;
		case kRowPartsOrdered:
		{
			_isPartsOrderedString = [_yesNoNaPickerOptions objectAtIndex:pickerIndex];
		}
			break;
			
		case kRowPartsOrderedBy:
		{
			_operative = [_operatives objectAtIndex:pickerIndex];
			_partsOrderedBy = _operative.employeeId;
		}
			break;
		case kRowDuration:
		{
			_duration = (NSNumber *)[_durationList objectAtIndex:pickerIndex];
			_durationString = [_durationPickerOptions objectAtIndex:pickerIndex];
		}
			break;
			
		case kRowPriority:
		{
			_priority = [_priorities objectAtIndex:pickerIndex];
		}
			break;
			
		case kRowTrade:
		{
			_trade = [_trades objectAtIndex:pickerIndex];
		}
			break;
		default:
			break;
	}
	[self.tableView reloadData];
}

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption
{
	if (!isEmpty(pickerOption))
	{
		switch (_pickerTag)
		{
			case kRowPartsDue:
				_partsDue = pickerOption;
				break;
				
			case kRowDefectDate:
				_defectDate = pickerOption;
				break;
				
			default:
				break;
		}
		[self.tableView reloadData];
	}
}

#pragma mark - Resign All Responders
- (void) resignAllResponders
{
	[self.view endEditing:YES];
}



#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 40;
	CGFloat height = 0.0;
	if(OS_VERSION >= 7.0)
	{
		NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
		[style setLineBreakMode:NSLineBreakByWordWrapping];
		NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
																style, NSParagraphStyleAttributeName,
																
																[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17],NSFontAttributeName,
																nil];
		CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(285, 999999.0f)
																							 options:NSStringDrawingUsesLineFragmentOrigin
																						attributes:attributes
																							 context:nil];
		height = boundingRect.size.height + verticalPadding;
	}
	else
	{
		height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
								constrainedToSize:CGSizeMake(285, 999999.0f)
										lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
	}
	return height * 1.05;
}

#pragma mark - Methods

- (NSString*) getDurationString:(NSNumber*) value
{
	
	NSString * durationString=@"";
	
	if (isEmpty(value)==NO) {
		
		if([value intValue] > 1){
			
			durationString = [NSString stringWithFormat:@"%@ hours", [value stringValue]];
			
		}else{
			durationString = [NSString stringWithFormat:@"%@ hour", [value stringValue]];
		}
		
	}
	return durationString;
}

- (NSString*) convertNSNumberToString:(id) value
{
	NSString *boolString = nil;
	if (value == nil)
	{
		boolString = LOC(@"KEY_STRING_NOTAPPLICABLE");
	}
	else if([value boolValue] == YES)
	{
		boolString = LOC(@"KEY_ALERT_YES");
	}
	else if([value boolValue] == NO)
	{
		boolString = LOC(@"KEY_ALERT_NO");
	}
	
	return boolString;
}

- (NSNumber*) getNSNumberFromString:(NSString *) value
{
	NSNumber *numVal = nil;
	if ([value isEqualToString:LOC(@"KEY_STRING_NOTAPPLICABLE")])
	{
		numVal = nil;
	}
	else if([value isEqualToString:LOC(@"KEY_ALERT_YES")])
	{
		numVal = [NSNumber numberWithBool:YES];
	}
	else if([value isEqualToString:LOC(@"KEY_ALERT_NO")])
	{
		numVal = [NSNumber numberWithBool:NO];
	}
	
	return numVal;
}

-(void) populatePickersData{
	
	_yesNoPickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
	_yesNoNaPickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO"), LOC(@"KEY_STRING_NOTAPPLICABLE")];
	_durationPickerOptions = @[@"1 hour", @"2 hours", @"3 hours", @"4 hours", @"5 hours", @"6 hours", @"7 hours", @"8 hours", @"9 hours"];
	_durationList = @[@1, @2, @3, @4, @5 , @6, @7, @8, @9];
	[self populateDefectCategories];
	
	if (self.viewMode == ApplianceViewMode)
	{
		[self getAppliances];
		[self getOperatives];
		[self getDefectPriorityList];
		[self getTradeList];
	}
	else if (self.viewMode == DetectorViewMode)
	{
		[self getDetectors];
	}else if (self.viewMode == BoilerViewMode)
	{
		[self getBoilers];
		[self getOperatives];
		[self getDefectPriorityList];
		[self getTradeList];
	}
	
}

- (void) initializeCollection
{
	_appliancesNames =[NSMutableArray array];
	_boilerNames =[NSMutableArray array];
	_operativeNames =[NSMutableArray array];
	_detectorNames = [NSMutableArray array];
	_defectCategories = [NSMutableArray array];
	_defectCategoriesStrings = [NSMutableArray array];
	_priorityList = [NSMutableArray array];
	_defectDictionary = [NSMutableDictionary dictionary];
	
}

- (void) getAppliances
{
	[_appliancesNames removeAllObjects];
	_appliances = [self.appointment.appointmentToProperty.propertyToAppliances allObjects];
	_appliances = [_appliances filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isActive = 1 or isActive = nil"]];
	
	for (Appliance *appliance in _appliances)
	{
		if (!isEmpty(appliance.applianceType.typeName))
		{
			[_appliancesNames addObject:appliance.applianceType.typeName];
		}
	}
	
}

- (void) getDetectors
{
	[_detectorNames removeAllObjects];
	_detectors = [self.appointment.appointmentToProperty.propertyToDetector allObjects];
	for (Detector *detector in _detectors)
	{
		if (!isEmpty(detector))
		{
			[_detectorNames addObject:detector.detectorType];
		}
	}
}

- (void) getBoilers
{
	[_boilerNames removeAllObjects];
	_boilers = [self.appointment.appointmentToProperty.propertyToBoiler allObjects];
	for (Boiler *boiler in _boilers)
	{
		if (!isEmpty(boiler.boilerName))
		{
			[_boilerNames addObject:boiler.boilerName];
		}
	}
}

- (void) populateDefectCategories
{
	NSString* remedialActionTaken = [@"Remedial Action Taken" lowercaseString];
	
	NSArray* defectCategories = [[PSApplianceManager sharedManager] fetchAllDefectCategories:kCategoryId];
	_defectCategoriesStrings = [NSMutableArray array];
	
	for (DefectCategory *defectCategory in defectCategories)
	{
		NSString * category = [defectCategory.categoryDescription lowercaseString];
		if (([category containsString:remedialActionTaken]) == NO)
		{
			[_defectCategories addObject:defectCategory];
			[_defectCategoriesStrings addObject:defectCategory.categoryDescription];
		}
	}
}

- (void) getOperatives{
	
	NSString * sortKey = kEmployeeName;
	_operatives = [[PSApplianceManager sharedManager] fetchAllOperatives:sortKey];
	for (Employee *employee in _operatives)
	{
		if (isEmpty(employee.employeeName)==NO)
		{
			[_operativeNames addObject:employee.employeeName];
		}
	}
}

- (void) getDefectPriorityList{
	
	NSString * sortKey = kPriorityDesc;
	_priorities = [[PSApplianceManager sharedManager] fetchDefectPriorityList];
	_priorities = [self sortArray:sortKey :_priorities];
	for (DefectPriority *priority in _priorities)
	{
		[_priorityList addObject:priority.priority];
	}
}

- (void) getTradeList{
	
	NSString * sortKey = kTradeDescription;
	_trades = [[PSApplianceManager sharedManager] fetchTradeList];
	_trades = [self sortArray:sortKey :_trades];
	_tradeList = [NSMutableArray array];
	for (Trade *trade in _trades)
	{
		[_tradeList addObject:trade.trade];
	}
}

- (NSArray*) sortArray:(NSString*)sortDescriptorKey :(NSArray*) objArray
{
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortDescriptorKey ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	objArray = [objArray sortedArrayUsingDescriptors:sortDescriptors];
	return objArray;
}

- (Trade*) getDefaultTrade
{
	NSString* defaultTrade = @"Gas (Full)";
	return [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([Trade class]) Field:kTradeDescription WithValue:defaultTrade context:[PSDatabaseContext sharedContext].managedObjectContext];
}

- (void) loadDefaultValues
{
	if (isEmpty(self.defect)== NO)
	{
		_isDefectIdentified = [self.defect.isDefectIdentified boolValue];
		_isDefectIdentifiedString = [self convertNSNumberToString:self.defect.isDefectIdentified];
		_isRemedialActionTaken = [self.defect.isRemedialActionTaken boolValue];
		_isRemedialActionTakenString = [self convertNSNumberToString:self.defect.isRemedialActionTaken];
		_isWarningIssued = [self.defect.isAdviceNoteIssued boolValue];
		_isWarningIssuedString = [self convertNSNumberToString:self.defect.isAdviceNoteIssued];
		_isStickerFixed = [self.defect.warningTagFixed boolValue];
		_isStickerFixedString = [self convertNSNumberToString:self.defect.warningTagFixed];
		_defectCategory = self.defect.defectCategory;
		_defectDate = self.defect.defectDate;
		
		if (self.viewMode == ApplianceViewMode)
		{
			[self loadApplianceDefectInfo];
			_warningNoteSerialNo = self.defect.warningNoteSerialNo;
		}
		else if (self.viewMode == DetectorViewMode)
		{
			[self loadDetectorDefectInfo];
		}
		else if (self.viewMode == BoilerViewMode)
		{
			[self loadBoilerDefectInfo];
			_warningNoteSerialNo = self.defect.warningNoteSerialNo;
		}
		
		_remedialNotes = self.defect.remedialActionNotes;
		_serialNumber = self.defect.serialNumber;
		_defectNotes = self.defect.defectNotes;
	}
	else
	{
		[self resetDefectData];
	}
}

- (void) resetDefectData
{
	_isDefectIdentifiedString = nil;
	_isRemedialActionTakenString = nil;
	_isWarningIssuedString = nil;
	_isStickerFixedString = nil;
	_defectCategory = nil;
	_detector = nil;
	_appliance = nil;
	_boiler = nil;
	_gcNumber = nil;
	_isDisconnectedString = [self convertNSNumberToString:nil];
	_isPartRequiredString = [self convertNSNumberToString:nil];
	_isPartsOrderedString = [self convertNSNumberToString:nil];
	_partsOrderedBy = nil;
	_operative = nil;
	_partsDue = nil;
	_defectDate =nil;
	_partsDescription = nil;
	_partsLocation = nil;
	_isTwoPersonJobString = [self convertNSNumberToString:[NSNumber numberWithBool:NO]];
	_reasonForTwoPerson = nil;
	_duration = [_durationList objectAtIndex:0];
	_durationString = [self getDurationString:_duration];
	_priority = [_priorities objectAtIndex:0];
	_trade = [self getDefaultTrade];
	_remedialNotes = nil;
	_serialNumber = nil;
	_defectNotes = nil;
	PSAddDefectTextFieldCell * cellView = (PSAddDefectTextFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:kRowWarningNoteSerialNo inSection:0]];
	[cellView.txtTitle setEnabled:FALSE];
}

- (void) loadDetectorDefectInfo
{
	_detector = [[PSCoreDataManager sharedManager]detectorWithIdentifier:self.defect.detectorTypeId forProperty:self.defect.propertyId context:[PSDatabaseContext sharedContext].managedObjectContext];
}

- (void) loadApplianceDefectInfo
{
	
	_appliance = self.defect.defectToAppliance;
	if (!isEmpty(_appliance))
	{
		_applianceName = _appliance.applianceType.typeName;
	}
	
	_gcNumber = self.defect.gasCouncilNumber;
	_isDisconnectedString = [self convertNSNumberToString:self.defect.isDisconnected];
	_isPartRequiredString = [self convertNSNumberToString:self.defect.isPartsRequired];
	_isPartsOrderedString = [self convertNSNumberToString:self.defect.isPartsOrdered];
	// On Email request by client: changes made on 05-Jan-2016
	if([_isPartRequiredString compare:@"Yes"] == NSOrderedSame)
	{
		_partsOrderedBy = (isEmpty(self.defect.partsOrderedBy)?[[SettingsClass sharedObject]loggedInUser].userId:self.defect.partsOrderedBy);
		_operative = [[PSCoreDataManager sharedManager] employeeWithIdentifier:_partsOrderedBy inContext:[PSDatabaseContext sharedContext].managedObjectContext];
	}
	else
	{
		_partsOrderedBy = nil;
		_operative = nil;
	}
	
	_partsDue = self.defect.partsDueDate;
	_partsDescription = self.defect.partsDescription;
	_partsLocation = self.defect.partsLocation;
	_isTwoPersonJobString = (isEmpty(self.defect.isTwoPersonsJob)?[self convertNSNumberToString:[NSNumber numberWithBool:NO]] : [self convertNSNumberToString:self.defect.isTwoPersonsJob]);
	_reasonForTwoPerson = self.defect.reasonForTwoPerson;
	_duration = (isEmpty(self.defect.duration)?[_durationList objectAtIndex:0]:self.defect.duration);
	_durationString = [self getDurationString:_duration];
	if (isEmpty(self.defect.priorityId))
	{
		_priority = [_priorities objectAtIndex:0];
	}
	else
	{
		_priority = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([DefectPriority class]) Field:kDefectPriorityId WithValue:self.defect.priorityId context:[PSDatabaseContext sharedContext].managedObjectContext];
	}
	if (isEmpty(self.defect.tradeId))
	{
		_trade = [self getDefaultTrade];
	}
	else
	{
		_trade = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([Trade class]) Field:kDefectTradeId WithValue:self.defect.tradeId context:[PSDatabaseContext sharedContext].managedObjectContext];
	}
	
}

- (void) loadBoilerDefectInfo
{

	_boiler = self.defect.defectToBoiler;
	if (!isEmpty(_boiler))
	{
		_boilerName = _boiler.boilerToBoilerType.boilerTypeDescription;
	}
	
	_gcNumber = self.defect.gasCouncilNumber;
	_isDisconnectedString = [self convertNSNumberToString:self.defect.isDisconnected];
	_isPartRequiredString = [self convertNSNumberToString:self.defect.isPartsRequired];
	_isPartsOrderedString = [self convertNSNumberToString:self.defect.isPartsOrdered];
	if([_isPartRequiredString compare:@"Yes"] == NSOrderedSame)
	{
		_partsOrderedBy = (isEmpty(self.defect.partsOrderedBy)?[[SettingsClass sharedObject]loggedInUser].userId:self.defect.partsOrderedBy);
		_operative = [[PSCoreDataManager sharedManager] employeeWithIdentifier:_partsOrderedBy inContext:[PSDatabaseContext sharedContext].managedObjectContext];
	}
	else
	{
		_partsOrderedBy = nil;
		_operative = nil;
	}
	
	_partsDue = self.defect.partsDueDate;
	_partsDescription = self.defect.partsDescription;
	_partsLocation = self.defect.partsLocation;
	_isTwoPersonJobString = (isEmpty(self.defect.isTwoPersonsJob)?[self convertNSNumberToString:[NSNumber numberWithBool:NO]] : [self convertNSNumberToString:self.defect.isTwoPersonsJob]);
	_reasonForTwoPerson = self.defect.reasonForTwoPerson;
	_duration = (isEmpty(self.defect.duration)?[_durationList objectAtIndex:0]:self.defect.duration);
	_durationString = [self getDurationString:_duration];
	if (isEmpty(self.defect.priorityId))
	{
		_priority = [_priorities objectAtIndex:0];
	}
	else
	{
		_priority = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([DefectPriority class]) Field:kDefectPriorityId WithValue:self.defect.priorityId context:[PSDatabaseContext sharedContext].managedObjectContext];
	}
	
	if (isEmpty(self.defect.tradeId))
	{
		_trade = [self getDefaultTrade];
	}
	else
	{
		_trade = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([Trade class]) Field:kDefectTradeId WithValue:self.defect.tradeId context:[PSDatabaseContext sharedContext].managedObjectContext];
	}
	
}

- (void) disableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons) {
		button.enabled = NO;
	}
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = NO;
	}
}

- (void) enableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons) {
		button.enabled = YES;
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = YES;
	}
}

- (BOOL) isFormValid
{
	BOOL isValid = YES;
	
	NSString *message= @"";
	
	if (isEmpty(_defectCategory.categoryDescription.trimString))
	{
		message = LOC(@"KEY_ALERT_SELECT_DEFECT_CATEGORY");
		isValid = NO;
	}
	else if (isEmpty(_defectDate))
	{
		message = LOC(@"KEY_ALERT_SELECT_DEFECT_DATE");
		isValid = NO;
	}
	else if (isEmpty(_isDefectIdentifiedString.trimString))
	{
		message = LOC(@"KEY_ALERT_SELECT_DEFECT_IDENTIFIED");
		isValid = NO;
	}
	else if (isEmpty(_defectNotes.trimString))
	{
		message = LOC(@"KEY_ALERT_ENTER_DEFECT_NOTES");
		isValid = NO;
	}
	else if (isEmpty(_isRemedialActionTakenString.trimString))
	{
		message = LOC(@"KEY_ALERT_SELECT_REMEDIAL_ACTION");
		isValid = NO;
	}
	else if ([_isRemedialActionTakenString.trimString compare:@"Yes"] == NSOrderedSame &&
						isEmpty(_remedialNotes.trimString))
	{
		message = LOC(@"KEY_ALERT_ENTER_REMEDIAL_NOTES");
		isValid = NO;
	}
	else if (isEmpty(_isWarningIssuedString.trimString))
	{
		message = LOC(@"KEY_ALERT_SELECT_WARNING_ISSUED");
		isValid = NO;
	}
	else if ( [self validateDefectTypePicker])
	{
		if (self.viewMode == ApplianceViewMode) {
			message = LOC(@"KEY_ALERT_SELECT_APPLIANCE");
		}else if (self.viewMode == DetectorViewMode)
		{
			message = LOC(@"KEY_ALERT_SELECT_DETECTOR");
		}else if (self.viewMode == BoilerViewMode)
		{
			message = LOC(@"KEY_ALERT_SELECT_BOILER");
		}
		isValid = NO;
	}
	else if (self.viewMode == ApplianceViewMode || self.viewMode == BoilerViewMode)
	{
		if ((isEmpty(_gcNumber.trimString)==NO)
				&& (([_gcNumber.trimString length] > 0)
				&& ([_gcNumber.trimString length] < 9)))
		{
					
					message = LOC(@"KEY_ALERT_ENTER_GC_NUMBER");
					isValid = NO;
		}
		else if (isEmpty(_isDisconnectedString.trimString))
		{
			message = LOC(@"KEY_ALERT_SELECT_DISCONNECTED");
			isValid = NO;
		}
		else if(_isWarningIssued == YES && isEmpty(_warningNoteSerialNo.trimString) == TRUE)
		{
			message = LOC(@"KEY_ALERT_SELECT_WARNINGNOTE");
			isValid = NO;
		}
		else if (isEmpty(_isPartRequiredString.trimString))
		{
			message = LOC(@"KEY_ALERT_SELECT_PARTS_REQUIRED");
			isValid = NO;
		}
		else if ([_isPartRequiredString.trimString compare:@"Yes"] == NSOrderedSame)
		{
			if (isEmpty(_isPartsOrderedString.trimString))
			{
				message = LOC(@"KEY_ALERT_SELECT_PARTS_ORDERED");
				isValid = NO;
			}
			else if (isEmpty(_partsOrderedBy))
			{
				message = LOC(@"KEY_ALERT_SELECT_PARTS_ORDERED_BY");
				isValid = NO;
			}
			else if (isEmpty(_partsDue))
			{
				message = LOC(@"KEY_ALERT_SELECT_PARTS_DUE");
				isValid = NO;
			}
			else if (isEmpty(_partsDescription.trimString))
			{
				message = LOC(@"KEY_ALERT_ENTER_PARTS_DESCRIPTION");
				isValid = NO;
			}
			else if (isEmpty(_partsLocation.trimString))
			{
				message = LOC(@"KEY_ALERT_ENTER_PARTS_LOCATION");
				isValid = NO;
			}
		}
		else if (isEmpty(_isTwoPersonJobString.trimString))
		{
			message = LOC(@"KEY_ALERT_SELECT_TWO_PERSON_JOB");
			isValid = NO;
		}
		else if ([_isTwoPersonJobString isEqualToString:LOC(@"KEY_ALERT_YES")]
							&& isEmpty(_reasonForTwoPerson))
		{
			message = LOC(@"KEY_ALERT_ENTER_REASON_TWO_PERSON_JOB");
			isValid = NO;
		}
	}
	if (isValid==NO)
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																									 message:message
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
	}
	return isValid;
}

- (BOOL) validateDefectTypePicker
{
	BOOL isValid = false;
	if (self.viewMode == ApplianceViewMode) {
		isValid = isEmpty(_appliance);
	}else if (self.viewMode == DetectorViewMode)
	{
		isValid = isEmpty(_detector);
	}else if (self.viewMode == BoilerViewMode)
	{
		isValid = isEmpty(_boiler);
	}
	return isValid;
}


- (void) saveDefect
{
	[self showActivityIndicator:[NSString stringWithFormat:@"%@", LOC(@"KEY_ALERT_SAVING_DEFECT")]];
	[self disableNavigationBarButtons];
	
	[_defectDictionary setObject:isEmpty(_defectCategory.categoryID)?[NSNull null]:_defectCategory.categoryID forKey:kDefectCategoryType];
	[_defectDictionary setObject:[UtilityClass convertNSDateToServerDate:_defectDate] forKey:kDefectDate];
	[_defectDictionary setObject:[NSNumber numberWithBool:_isDefectIdentified] forKey:kIsDefectIdentified];
	[_defectDictionary setObject:isEmpty(_defectNotes)?[NSNull null]:_defectNotes forKey:kDefectNotes];
	[_defectDictionary setObject:[NSNumber numberWithBool:_isRemedialActionTaken] forKey:kIsRemedialActionTaken];
	[_defectDictionary setObject:isEmpty(_remedialNotes)?[NSNull null]:_remedialNotes forKey:kRemedialActionNotes];
	
	if (isEmpty(self.defect))
	{
		CLS_LOG(@"New defect ID %@",self.defectNewID);
		[_defectDictionary setObject:self.defectNewID forKey:kDefectID];
	}
	else
	{
		[_defectDictionary setObject:isEmpty(self.defect.defectID)?[NSNull null]:self.defect.defectID forKey:kDefectID];
		[_defectDictionary setObject:self.defect.objectID forKey:kDefectObjectId];
	}
	
	if (self.viewMode == ApplianceViewMode || self.viewMode == BoilerViewMode )
	{
		if (self.viewMode == ApplianceViewMode) {
			[_defectDictionary setObject:_appliance.objectID forKey:kApplianceObjectId];
			[_defectDictionary setObject:_appliance.applianceID forKey:kDefectApplianceID];
			[_defectDictionary setObject:kDefectTypeAppliance forKey:kDefectType];
			[_defectDictionary setObject:[NSNull null] forKey:kBoilerObjectId];
			[_defectDictionary setObject:[NSNull null] forKey:kDefectBoilerTypeId];
            [_defectDictionary setObject:[NSNull null] forKey:kHeatingId];
		}
		else
		{
			[_defectDictionary setObject:isEmpty(_boiler)?[NSNull null]:_boiler.objectID forKey:kBoilerObjectId];
			[_defectDictionary setObject:isEmpty(_boiler)?[NSNull null]:_boiler.boilerTypeId forKey:kDefectBoilerTypeId];
            [_defectDictionary setObject:isEmpty(_boiler)?[NSNull null]:_boiler.heatingId forKey:kHeatingId];
			[_defectDictionary setObject:[NSNull null] forKey:kApplianceObjectId];
			[_defectDictionary setObject:[NSNull null] forKey:kDefectApplianceID];
			[_defectDictionary setObject:kDefectTypeBoiler forKey:kDefectType];
		}

		[_defectDictionary setObject:[NSNull null] forKey:kDetectorTypeId];
		[_defectDictionary setObject:isEmpty(_gcNumber)?[NSNull null]:_gcNumber forKey:kGasCouncilNumber];
		[_defectDictionary setObject:(isEmpty([self getNSNumberFromString:_isDisconnectedString]))?[NSNull null]:[self getNSNumberFromString:_isDisconnectedString] forKey:kIsDisconnected];
		[_defectDictionary setObject:(isEmpty([self getNSNumberFromString:_isPartRequiredString]))?[NSNull null]:[self getNSNumberFromString:_isPartRequiredString] forKey:kIsPartsRequired];

		if([_isPartRequiredString compare:@"Yes"] == NSOrderedSame)
		{
			[_defectDictionary setObject:(isEmpty([self getNSNumberFromString:_isPartsOrderedString]))?[NSNull null]:[self getNSNumberFromString:_isPartsOrderedString] forKey:kIsPartsOrdered];
			[_defectDictionary setObject:isEmpty(_operative)?[NSNull null]:_operative.employeeId forKey:kPartsOrderedBy];
			[_defectDictionary setObject:[UtilityClass convertNSDateToServerDate:_partsDue] forKey:kPartsDueDate];
			[_defectDictionary setObject:isEmpty(_partsDescription)?[NSNull null]:_partsDescription forKey:kPartsDescription];
			[_defectDictionary setObject:isEmpty(_partsLocation)?[NSNull null]:_partsLocation forKey:kPartsLocation];
		}
		[_defectDictionary setObject:[NSNumber numberWithBool:[_isTwoPersonJobString boolValue]] forKey:kIsTwoPersonsJob];
		[_defectDictionary setObject:isEmpty(_reasonForTwoPerson)?[NSNull null]:_reasonForTwoPerson forKey:kReasonForTwoPerson];
		[_defectDictionary setObject:isEmpty(_duration)?[NSNull null]:_duration forKey:kDefectDuration];
		[_defectDictionary setObject:isEmpty(_priority.priorityId)?[NSNull null]:_priority.priorityId forKey:kDefectPriorityId];
		[_defectDictionary setObject:isEmpty(_trade.tradeId)?[NSNull null]:_trade.tradeId forKey:kDefectTradeId];
	
	}
	else if (self.viewMode == DetectorViewMode)
	{
		[_defectDictionary setObject:[NSNull null] forKey:kDefectApplianceID];
		[_defectDictionary setObject:isEmpty(_detector.detectorTypeId)?[NSNull null]:_detector.detectorTypeId forKey:kDetectorTypeId];
		[_defectDictionary setObject:kDefectTypeDetector forKey:kDefectType];
	}

	[_defectDictionary setObject:[NSNumber numberWithBool:_isWarningIssued] forKey:kisAdviceNoteIssued];
	[_defectDictionary setObject:isEmpty(_warningNoteSerialNo)?[NSNull null]:_warningNoteSerialNo forKey:kWarningNoteSerialNo];

	[_defectDictionary setObject:isEmpty(self.appointment.journalId)?[NSNull null]:self.appointment.journalId forKey:@"JournalId"];
	[_defectDictionary setObject:isEmpty(self.appointment.appointmentToProperty.propertyId)?[NSNull null]:self.appointment.appointmentToProperty.propertyId forKey:kPropertyId];
    [_defectDictionary setObject:isEmpty(self.appointment.appointmentToProperty.schemeId)?[NSNull null]:self.appointment.appointmentToProperty.schemeId forKey:kSchemeID];
    [_defectDictionary setObject:isEmpty(self.appointment.appointmentToProperty.blockId)?[NSNull null]:self.appointment.appointmentToProperty.blockId forKey:kSchemeBlockID];
	
	[_defectDictionary setObject:isEmpty(_serialNumber)?[NSNull null]:_serialNumber forKey:kSerialNo];
	[_defectDictionary setObject:[NSNumber numberWithBool:_isStickerFixed] forKey:kWarningTagFixed];
	
	NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
	[requestParameters setObject:_defectDictionary forKey:@"defect"];
	[[PSDataPersistenceManager sharedManager]saveNewDefect:_defectDictionary];
	
}
@end
