//
//  PSDefectsViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"
@class PSDefectsPickerCell;
@class PSDefectsCell;

@protocol PSDefectsProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption;

@end

@interface PSDefectsViewController : PSCustomViewController <PSDefectsProtocol, UIActionSheetDelegate>
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) PSBarButtonItem *addDefectButton;
@property (weak,   nonatomic) IBOutlet PSDefectsCell *defectsCell;
@property (weak,   nonatomic) IBOutlet PSDefectsPickerCell *defectPcikerCell;
@property (weak,   nonatomic) Appointment *appointment;
@end
