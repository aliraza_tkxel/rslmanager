//
//  PSPipeworkPickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSInstallationPipeworkViewController.h"

@interface PSPipeworkPickerCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (weak,    nonatomic) id<PSPipeworkProtocol> delegate;
- (IBAction)onClickPickerBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblValue;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;

@end
