//
//  PSInstallationPipeworkViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSInstallationPipeworkViewController.h"
#import "PSBarButtonItem.h"
#import "PSPipeworkPickerCell.h"
#import "PSCameraViewController.h"

#define kTotalNumberOfRows 4
#define kRowEmergencyControl 0
#define kRowVisualInspection 1
#define kRowSatisfactoryGasTightness 2
#define kRowEquipotentialBonding 3


@interface PSInstallationPipeworkViewController ()
{
    NSString *_emergencyControl;
    NSString *_visualInspection;
    NSString *_satisfactoryGasTightness;
    NSString *_equipotentialBonding;
    NSInteger _pickerTag;
}

@property(nonatomic,strong) PSBarButtonItem * cameraButton;

@end

@implementation PSInstallationPipeworkViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    [self loadDefaultValues];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [self registerNotification];
	[super viewWillAppear:animated];
    [self reloadPhotographsBadge];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger) getPhotographsCount
{
    NSArray * photographsList =[[PSCoreDataManager sharedManager] propertyPicturesWithPropertyIdentifier:self.appointment.appointmentToProperty appointmentId:self.appointment.appointmentId itemId:!isEmpty(_selectedBoiler.itemId)?_selectedBoiler.itemId:[NSNumber numberWithInt:0]];
    return [photographsList count];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    
    
    
    
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    
	self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
	                                                           style:PSBarButtonItemStyleCamera
	                                                          target:self
	                                                          action:@selector(onClickCameraButton)bagde:badgeText];
    
    
    
    [self setLeftBarButtonItems:@[backButton]];
    
    [self setRightBarButtonItems:@[self.saveButton,self.cameraButton]];

    
    [self setTitle:LOC(@"KEY_STRING_GAS_INSTALLATION_PIPEWORK")];   
}

-(void) reloadPhotographsBadge
{
    NSString * badgeText=[NSString stringWithFormat:@"%d",[self getPhotographsCount]];
    [self.cameraButton reloadBadge:badgeText];
}
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    [self saveInstallationPipeWork];
}


- (void)onClickCameraButton {
    
	CLS_LOG(@"Camera Button");
    PSCameraViewController *cameraViewController = [[PSCameraViewController alloc] initWithNibName:@"PSCameraViewController" bundle:[NSBundle mainBundle]];
    [cameraViewController setImageType:CameraViewImageTypeOther];
    [cameraViewController setAppointment:self.appointment];
    [cameraViewController setItemId:!isEmpty(_selectedBoiler.itemId)?_selectedBoiler.itemId:[NSNumber numberWithInt:0]];
    [cameraViewController setHeatingId:_selectedBoiler.heatingId];
    
    [self.navigationController pushViewController:cameraViewController animated:YES];
    
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 44;
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return kTotalNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = nil;
  
        static NSString *pipeworkCellIdentifier = @"pipeworkCellIdentifier";
        PSPipeworkPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:pipeworkCellIdentifier];
        
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSPipeworkPickerCell" owner:self options:nil];
            _cell = self.pipeworkCell;
            self.pipeworkCell = nil;
        }
        
        [self configureCell:&_cell atIndexPath:indexPath];
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = _cell;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == kRowEmergencyControl)
    {
        PSPipeworkPickerCell *_cell = (PSPipeworkPickerCell *)*cell;
        [_cell setDelegate:self];
        [_cell.lblValue setText:_emergencyControl];
        [_cell.lblHeading setText:LOC(@"KEY_STRING_EMERGENCY_CONTROL_ACCESSIBLE")];
        _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
        [_cell.btnPicker addTarget:self action:@selector(onClickEmergencyControl:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.lblHeading setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblValue setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];

        *cell = _cell;
    }
    
    else if (indexPath.row == kRowVisualInspection)
    {
        PSPipeworkPickerCell *_cell = (PSPipeworkPickerCell *)*cell;
        [_cell.lblValue setText:_visualInspection];
        [_cell.lblHeading setText:LOC(@"KEY_STRING_VISUAL_INSPECTION")];
        [_cell setDelegate:self];
        _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
        [_cell.btnPicker addTarget:self action:@selector(onClickVisualInspection:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.lblHeading setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblValue setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        *cell = _cell;
    }
    
    else if (indexPath.row == kRowSatisfactoryGasTightness)
    {
        PSPipeworkPickerCell *_cell = (PSPipeworkPickerCell *)*cell;
        [_cell.lblValue setText:_satisfactoryGasTightness];
        [_cell.lblHeading setText:LOC(@"KEY_STRING_SATISFACTORY_GAS_TIGHTNESS_RESULT")];
        [_cell setDelegate:self];
        _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO"), LOC(@"KEY_STRING_N/A")];
        [_cell.btnPicker addTarget:self action:@selector(onClickSatisfactoryGasTightness:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.lblHeading setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblValue setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        *cell = _cell;
    }
    else if (indexPath.row == kRowEquipotentialBonding)
    {
        PSPipeworkPickerCell *_cell = (PSPipeworkPickerCell *)*cell;
        [_cell.lblValue setText:_equipotentialBonding];
        [_cell.lblHeading setText:LOC(@"KEY_STRING_EQUIPOTENTIAL_BONDING")];
        [_cell setDelegate:self];
        _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
        [_cell.lblHeading setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblValue setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.btnPicker addTarget:self action:@selector(onClickEquipotentialBonding:) forControlEvents:UIControlEventTouchUpInside];
        *cell = _cell;
    }
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickEmergencyControl:(id)sender
{
    _pickerTag = kRowEmergencyControl;
}

- (IBAction)onClickVisualInspection:(id)sender
{
    _pickerTag = kRowVisualInspection;
}

- (IBAction)onClickSatisfactoryGasTightness:(id)sender
{
    _pickerTag = kRowSatisfactoryGasTightness;
}

- (IBAction)onClickEquipotentialBonding:(id)sender
{
    _pickerTag = kRowEquipotentialBonding;
}

#pragma mark - Protocol Methods
- (void) didSelecPickerOption:(NSString *)pickerOption
{
    switch (_pickerTag) {
        case kRowEmergencyControl:
            _emergencyControl = pickerOption;
            break;
        case kRowVisualInspection:
            _visualInspection = pickerOption;
            break;
        case kRowSatisfactoryGasTightness:
            _satisfactoryGasTightness = pickerOption;
            break;
        case kRowEquipotentialBonding:
            _equipotentialBonding = pickerOption;
            break;
        default:
            break;
    }
    [self.tableView reloadData];
}

#pragma mark - Notifications

- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSavePropertyPipeworkSuccessNotification) name:kSavePropertyPipeworkFormSuccessNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSavePropertyPipeworkFormSuccessNotification object:nil];
}

- (void) onReceiveSavePropertyPipeworkSuccessNotification
{
    [self popOrCloseViewController];
}

#pragma mark - Methods
- (void) loadDefaultValues
{
    
    if (!isEmpty(_selectedBoiler))
    {
        if(!isEmpty(_selectedBoiler.boilerToGasInstallationPipeWork)){
            _emergencyControl =_selectedBoiler.boilerToGasInstallationPipeWork.emergencyControl;
            _visualInspection = _selectedBoiler.boilerToGasInstallationPipeWork.visualInspection;
            _satisfactoryGasTightness = _selectedBoiler.boilerToGasInstallationPipeWork.gasTightnessTest;
            _equipotentialBonding = _selectedBoiler.boilerToGasInstallationPipeWork.equipotentialBonding;
        }
        else{
            _emergencyControl = LOC(@"KEY_ALERT_YES");
            _visualInspection = LOC(@"KEY_ALERT_YES");
            _satisfactoryGasTightness = LOC(@"KEY_ALERT_YES");
            _equipotentialBonding = LOC(@"KEY_ALERT_YES");
        }
        
    }
    else
    {
        _emergencyControl = LOC(@"KEY_ALERT_YES");
        _visualInspection = LOC(@"KEY_ALERT_YES");
        _satisfactoryGasTightness = LOC(@"KEY_ALERT_YES");
        _equipotentialBonding = LOC(@"KEY_ALERT_YES");
    }
    
}

- (void) saveInstallationPipeWork
{
    if(!isEmpty(_emergencyControl) && !isEmpty(_visualInspection) && !isEmpty(_equipotentialBonding) && !isEmpty(_satisfactoryGasTightness)){
        NSMutableDictionary *pipeWorkDictionary = [NSMutableDictionary dictionary];
        [pipeWorkDictionary setObject:isEmpty(_emergencyControl)?[NSNull null]:_emergencyControl forKey:kEmergencyControl];
        [pipeWorkDictionary setObject:isEmpty(_equipotentialBonding)?[NSNull null]:_equipotentialBonding forKey:kEquipotentialBonding];
        [pipeWorkDictionary setObject:isEmpty(_satisfactoryGasTightness)?[NSNull null]:_satisfactoryGasTightness forKey:kGasTightnessTest];
        [pipeWorkDictionary setObject:isEmpty(_visualInspection)?[NSNull null]:_visualInspection forKey:KVisualInspection];
        [pipeWorkDictionary setObject:isEmpty([NSNumber numberWithBool:YES])?[NSNull null]:[NSNumber numberWithInt:1] forKey:kIsInspected];
        [pipeWorkDictionary setObject:isEmpty([NSDate date])?[NSNull null]:[NSDate date] forKey:kInspectionDate];
        [pipeWorkDictionary setObject:[NSNumber numberWithInt:-1] forKey:kInspectionId];
        
        [[PSApplianceManager sharedManager]saveInstallationPipework:pipeWorkDictionary forBoiler:_selectedBoiler];
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:LOC(@"KEY_ALERT_EMPTY_MANDATORY_FIELDS") delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
        
        [alert show];
    }
    
}

@end
