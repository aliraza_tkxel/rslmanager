//
//  PSBoilersListViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSBoilersListViewController.h"
#import "PSBarButtonItem.h"
#import "PSSelectBoilerTableViewCell.h"
#import "PSInstallationPipeworkViewController.h"
#define kBoilerDetailCellHeight 205
@interface PSBoilersListViewController ()

@end

@implementation PSBoilersListViewController
{
    NSArray *_boilers;
    BOOL _noResultsFound;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    [self registerNibsForTableView];
    [self.tableView reloadData];
    
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _boilers = [NSArray array];
    _boilers = [self.appointment.appointmentToProperty.propertyToBoiler allObjects];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    if(_destinationVC==BoilerListViewControllerDestinationGasInstallation){
        [self setTitle:@"Gas Installation Pipework"];
    }
    else{
        [self setTitle:@"Select Boiler"];
    }
    
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}


#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
    if (_noResultsFound)
    {
        rowHeight = 40;
    }
    else
    {
        rowHeight = kBoilerDetailCellHeight;
    }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    
    if (!isEmpty(_boilers))
    {
        rows = [_boilers count];
        _noResultsFound = NO;
    }
    else
    {
        rows = 1;
        _noResultsFound = YES;
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if(_noResultsFound)
    {
        static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
        UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
        if(_cell == nil)
        {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
        }
        _cell.textLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
        _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
        _cell.textLabel.text = LOC(@"KEY_STRING_NO_BOILER");
        _cell.textLabel.textAlignment = PSTextAlignmentCenter;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        cell = _cell;
    }
    
    else
    {
        PSSelectBoilerTableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSSelectBoilerTableViewCell class])];
        [self configureCell:&_cell atIndexPath:indexPath];
        if(_destinationVC==BoilerListViewControllerDestinationGasInstallation){
            _cell.btnInspect.hidden = NO;
        }
        else{
            _cell.btnInspect.hidden = YES;
        }
        cell = _cell;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    PSSelectBoilerTableViewCell *_cell = (PSSelectBoilerTableViewCell *)*cell;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        Boiler *boiler = nil;
        if (!isEmpty(_boilers))
        {
            boiler = [_boilers objectAtIndex:indexPath.row];
        }
        [_cell configureCell:boiler];
        
        [_cell.btnInspect addTarget:self action:@selector(onClickBoilerBtnInspect:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnInspect setTag:indexPath.row];
        [_cell.btnInspect setBackgroundImage:[UIImage imageNamed:(!isEmpty(boiler.boilerToGasInstallationPipeWork))?@"btn_Complete":@"checklistIcon"] forState:UIControlStateNormal];
        
        *cell = _cell;
    });
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([_boilers count]>0){
        Boiler *boiler = [_boilers objectAtIndex:indexPath.row];
        if(_destinationVC==BoilerListViewControllerDestinationGasInstallation){
            PSInstallationPipeworkViewController *pipeInstallationViewController = [[PSInstallationPipeworkViewController alloc] initWithNibName:@"PSInstallationPipeworkViewController" bundle:nil];
            [pipeInstallationViewController setAppointment:self.appointment];
            [pipeInstallationViewController setSelectedBoiler:boiler];
            [self.navigationController pushViewController:pipeInstallationViewController animated:YES];
        }
        
    }
    
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickBoilerBtnInspect:(id)sender
{
    CLS_LOG(@"onClickBoilerBtnInspect");
    
    
}

-(void)registerNibsForTableView
{
    NSString * nibName = NSStringFromClass([PSSelectBoilerTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
