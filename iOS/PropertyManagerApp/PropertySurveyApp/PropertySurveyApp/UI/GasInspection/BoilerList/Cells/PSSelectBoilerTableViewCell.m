//
//  PSSelectBoilerTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 23/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSSelectBoilerTableViewCell.h"

@implementation PSSelectBoilerTableViewCell

/*- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}*/

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) configureCell:(id) data
{
    [self.lblBoilerName setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [self.lblBoilerType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [self.lblBoilerManufacturer setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [self.lblSerialNumber setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [self.lblBoilerLocation setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    
    [self.lblBoilerNameDesc setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    [self.lblBoilerTypeDesc setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    [self.lblBoilerManufacturerDesc setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    [self.lblSerialNumberDesc setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    [self.lblBoilerLocationDesc setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    
    Boiler * boiler = (Boiler*) data;
    
    [self.lblBoilerNameDesc setText:isEmpty(boiler.boilerName)?nil:boiler.boilerName];
    
    [self.lblBoilerTypeDesc setText:isEmpty(boiler.boilerToBoilerType.boilerTypeDescription)?nil:boiler.boilerToBoilerType.boilerTypeDescription];
    [self.lblBoilerManufacturerDesc setText:isEmpty(boiler.boilerToBoilerManufacturer.manufacturerDescription)?nil:boiler.boilerToBoilerManufacturer.manufacturerDescription];
    [self.lblSerialNumberDesc setText:isEmpty(boiler.serialNumber)?nil:boiler.serialNumber];
    [self.lblBoilerLocationDesc setText:isEmpty(boiler.location)?nil:boiler.location];
    
    [self.btnInspect setBackgroundImage:[UIImage imageNamed:[boiler.isInspected boolValue]==YES?@"btn_Complete":@"checklistIcon"] forState:UIControlStateNormal];
}

@end
