//
//  PSSelectBoilerTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 23/07/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSelectBoilerTableViewCell : UITableViewCell
@property (weak,nonatomic) IBOutlet UILabel*lblBoilerName;
@property (weak,nonatomic) IBOutlet UILabel*lblBoilerNameDesc;

@property (weak,nonatomic) IBOutlet UILabel*lblBoilerType;
@property (weak,nonatomic) IBOutlet UILabel*lblBoilerTypeDesc;

@property (weak,nonatomic) IBOutlet UILabel*lblBoilerManufacturer;
@property (weak,nonatomic) IBOutlet UILabel*lblBoilerManufacturerDesc;

@property (weak,nonatomic) IBOutlet UILabel*lblSerialNumber;
@property (weak,nonatomic) IBOutlet UILabel*lblSerialNumberDesc;

@property (weak,nonatomic) IBOutlet UILabel*lblBoilerLocation;
@property (weak,nonatomic) IBOutlet UILabel*lblBoilerLocationDesc;

@property (weak,nonatomic) IBOutlet UIButton*btnInspect;

- (void) configureCell:(id) data;

@end
