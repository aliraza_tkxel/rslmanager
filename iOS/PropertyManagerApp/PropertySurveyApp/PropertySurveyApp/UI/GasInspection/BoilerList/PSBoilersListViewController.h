//
//  PSBoilersListViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSBoilersListViewController : PSCustomViewController
@property (weak,   nonatomic) Appointment *appointment;
@property BoilerListViewControllerDestination destinationVC;
@end
