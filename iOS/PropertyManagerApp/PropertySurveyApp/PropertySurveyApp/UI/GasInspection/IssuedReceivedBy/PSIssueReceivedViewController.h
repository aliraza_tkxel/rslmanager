//
//  PSIssueReceivedViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"
@class PSReceivedInfoCell;
@class PSReceivedPickerCell;
@class PSDatePickerCell;
@class PSConfirmationCell;

@protocol PSIssueReceivedProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelectDateTime Called upon selection of Date picker option.
 */
- (void) didSelectDate:(NSDate *)pickerOption;

/*!
 @discussion
 Method didSelectReceivedPickerOption Called upon selection of Received on behalf of picker option.
 */
- (void) didSelectReceivedPickerOption:(NSString *)pickerOption;

/*!
 @discussion
 Method didSelectConfirmationBtn Called upon clicking confirmation button.
 */
- (void) didSelectConfirmationBtn:(BOOL )pickerOption;

@end


@interface PSIssueReceivedViewController : PSCustomViewController<PSIssueReceivedProtocol>
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (weak,   nonatomic) IBOutlet PSReceivedInfoCell *infoCell;
@property (weak,   nonatomic) IBOutlet PSReceivedPickerCell *pickerCell;
@property (weak,   nonatomic) IBOutlet PSDatePickerCell *dateCell;
@property (weak,   nonatomic) IBOutlet PSConfirmationCell *confirmationCell;
@property (weak,   nonatomic) CP12Info *cp12Info;
@property (weak,   nonatomic) Appointment *appointment;

@end
