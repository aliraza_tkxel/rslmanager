//
//  PSCP12AsbestosCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSCP12AsbestosCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblAsbestos;
@property (strong, nonatomic) IBOutlet UILabel *lblAsbestosLocation;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator;

@end
