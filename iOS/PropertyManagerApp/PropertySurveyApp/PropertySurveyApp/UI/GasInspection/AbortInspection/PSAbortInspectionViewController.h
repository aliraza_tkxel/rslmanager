//
//  PSAbortInspectionViewController.h
//  PropertySurveyApp
//
//  Created by Abdul Rehman on 26/01/2017.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"


@interface PSAbortInspectionViewController : PSCustomViewController <UITextViewDelegate>

@property (weak,   nonatomic) Appointment *appointment;

@end
