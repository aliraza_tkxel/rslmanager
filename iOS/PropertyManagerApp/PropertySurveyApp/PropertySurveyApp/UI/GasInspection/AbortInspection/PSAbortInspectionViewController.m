//
//  PSAbortInspectionViewController.m
//  PropertySurveyApp
//
//  Created by Abdul Rehman on 26/01/2017.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PSAbortInspectionViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AbortReason+CoreDataClass.h"

@interface PSAbortInspectionViewController ()

@property (strong, nonnull) NSString *pickerTitle;
@property (strong, nonnull) NSMutableArray *pickerOptions;
@property (weak, nonatomic) IBOutlet UILabel *propertyInfoLabel;
@property (strong, nonatomic) NSString *_selectedReason;
@property (weak, nonatomic) IBOutlet UILabel *selectedReason;
@property (weak, nonatomic) IBOutlet UITextField *notesText;

@property (strong, nonnull) NSArray *abortReasonsArray;


@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollVIew;
@end

@implementation PSAbortInspectionViewController

#define kSelectReasonMessage @"Please select reason of aborting the inspection."

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.abortReasonsArray = [NSArray array];
    self._selectedReason = @"";
    [self loadNavigationonBarItems];
    
    [self updateTheInformationLabels];
    [_scrollVIew setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    
    self.abortReasonsArray  = [CoreDataHelper getObjectsFromEntity:kAbortReasonType sortKey:nil sortAscending:YES context:[PSDatabaseContext sharedContext].managedObjectContext];
    [self makeArrayForAbortOptions];
    
        // Do any additional setup after loading the view.
}

-(void) updateTheInformationLabels
{
    if(!isEmpty(self.appointment.appointmentToProperty.propertyId)){
        [self.propertyInfoLabel setText:[NSString stringWithFormat:@"You have selected to abort the current service at %@ %@",self.appointment.appointmentToProperty.houseNumber,self.appointment.appointmentToProperty.address1]];
    }
    else if(!isEmpty(self.appointment.appointmentToProperty.schemeId)){
        [self.propertyInfoLabel setText:[NSString stringWithFormat:@"You have selected to abort the current service at %@",self.appointment.appointmentToProperty.schemeName]];
    }
    else if(!isEmpty(self.appointment.appointmentToProperty.blockId)){
        [self.propertyInfoLabel setText:[NSString stringWithFormat:@"You have selected to abort the current service at %@",self.appointment.appointmentToProperty.blockName]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
- (IBAction)selectReasonButtonTapped:(id)sender {
    [self showPicker:sender];
}


// Making array of abort options

-(void) makeArrayForAbortOptions
{
    self.pickerOptions = [NSMutableArray array];
    if(self.abortReasonsArray.count > 0)
    {
        for(AbortReason *reason in self.abortReasonsArray)
        {
            if(![self.pickerOptions containsObject:reason.reason])
            {
                [self.pickerOptions addObject:reason.reason];
            }
        }
    }
    else
    {
        self.pickerOptions = [NSMutableArray arrayWithObjects:@"No Gas/Electric",@"Threatening Behaviour",@"H&S",@"Property Condition",@"Customer Reason",nil];
    }

}

// Setting navigation buttons

- (void) loadNavigationonBarItems
{
    [self setTitle:@"Abort Inspection"];
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    PSBarButtonItem *confirmButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_CONFRIM")
                                                                         style:PSBarButtonItemStyleDefault
                                                                        target:self
                                                                        action:@selector(onClickConfirmButton)];
    [self setRightBarButtonItems:[NSArray arrayWithObject:confirmButton]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
}

-(void) onClickBackButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) onClickConfirmButton
{
    CLS_LOG(@"Confirm abort");
    if([self._selectedReason isEqualToString:@""])
    {
        [self showAlertWithTitle:@"Reason" message:kSelectReasonMessage tag:0 cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    }
    else
    {
        self.appointment.appointmentStatus = kAppointmentStatusAborted;
        self.appointment.appointmentAbortNotes = self.notesText.text;
        self.appointment.appointmentAbortReason = self._selectedReason;
        self.appointment.loggedDate = [NSDate date];
        
        [[self.appointment managedObjectContext]save:nil];
        [[PSDatabaseContext sharedContext] saveContext];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void) showPicker : (UIButton*) sender
{
    [self setPickerOptions:self.pickerOptions];
    [ActionSheetStringPicker showPickerWithTitle:self.pickerTitle
                                            rows:self.pickerOptions
                                initialSelection:0
                                          target:self
                                   successAction:@selector(onOptionSelected:element:)
                                    cancelAction:@selector(onOptionPickerCancelled:)
                                          origin:sender];

}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self._selectedReason = [self.pickerOptions objectAtIndex:[selectedIndex intValue]];
    self.selectedReason.text = [self.pickerOptions objectAtIndex:[selectedIndex intValue]];
}

- (void) onOptionPickerCancelled:(id)sender {
    CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
