//
//  PSAddAppliancePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAddEditDetectorViewController.h"

@interface PSAEDPickerCell : UITableViewCell <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak,    nonatomic) id<PSAddEditDetectorProtocol> delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (assign, nonatomic) NSInteger selectedIndex;
- (IBAction)onClickPickerBtn:(id)sender;
- (IBAction)onClickEditBtn:(id)sender;
- (void) configureCell;
- (BOOL) isValid;
@end
