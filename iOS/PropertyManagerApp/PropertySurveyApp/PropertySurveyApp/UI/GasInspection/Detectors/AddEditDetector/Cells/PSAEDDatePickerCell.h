//
//  PSAddAppointmentDatePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAddEditDetectorViewController.h"

@interface PSAEDDatePickerCell : UITableViewCell
@property (weak,    nonatomic) id<PSAddEditDetectorProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSDate *selectedDate;
@property (weak, nonatomic) IBOutlet UIButton *resetDatesButton;
- (IBAction)onclickPickerBtn:(id)sender;
- (void) configureCell;
- (BOOL) isValid;
@end
