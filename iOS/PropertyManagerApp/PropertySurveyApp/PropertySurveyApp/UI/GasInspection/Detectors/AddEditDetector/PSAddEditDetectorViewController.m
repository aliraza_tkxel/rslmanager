//
//  PSAddApplianceViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddEditDetectorViewController.h"
#import "PSBarButtonItem.h"
#import "PSAEDTextFieldCell.h"
#import "PSAEDPickerCell.h"
#import "PSAEDDatePickerCell.h"
#import "PSTextViewCell.h"

#define kNumberOfRows 7
#define kRowDetectorType 0
#define kRowLocation 1
#define kRowManufacturer 2
#define kRowSerialNumber 3
#define kRowPower 4
#define kRowInstalledDate 5
#define kRowLandlordsDetector 6

@interface PSAddEditDetectorViewController ()
{
	DetectorType *_detectorType;
	PowerType *_power;
	NSString *_location;
	NSString *_manufacturer;
	NSString *_serialNumber;
	NSDate *_installedDate;
	NSString *_isLandlordDetector;
	NSNumber *_isDetectorInspected;
	
	NSInteger _pickerTag;
	NSInteger _textFieldTag;
	NSMutableDictionary *_detectorDictionary;
	
	NSArray *_detectorTypes;
	NSArray *_powerTypes;
	NSArray *_yesNoPickerOption;
	
}

@end

@implementation PSAddEditDetectorViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	_detectorDictionary = [NSMutableDictionary dictionary];
	[self loadNavigationonBarItems];
	[self registerNibsForTableView];
	[self getPickersData];
	[self populateDefaultValues];
	[self.tableView reloadData];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	self.saveButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
																													 style:PSBarButtonItemStyleDefault
																													target:self
																													action:@selector(onClickSaveButton)];
	
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	if (!isEmpty(self.detector)) {
		[self setTitle:@"Detector"];
	}
	else
	{
		[self setTitle:@"Add Detector"];
	}
}

- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickSaveButton
{
	CLS_LOG(@"onClickSaveButton");
	[self resignAllResponders];
	
	if([self isFormValid]==YES){
		[self saveDetector];
	}
	
}

- (void) disableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	if (!isEmpty(navigationBarRightButtons))
	{
		for (PSBarButtonItem *button in navigationBarRightButtons)
		{
			button.enabled = NO;
		}
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	if (!isEmpty(navigationBarLeftButtons))
	{
		for (PSBarButtonItem *button in navigationBarLeftButtons)
		{
			button.enabled = NO;
		}
	}
}

- (void) enableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	if (!isEmpty(navigationBarRightButtons))
	{
		for (PSBarButtonItem *button in navigationBarRightButtons) {
			button.enabled = YES;
		}
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	
	if (!isEmpty(navigationBarLeftButtons))
	{
		for (PSBarButtonItem *button in navigationBarLeftButtons)
		{
			button.enabled = YES;
		}
	}
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44;
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return kNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;
	
	if (indexPath.row == kRowSerialNumber || indexPath.row == kRowManufacturer || indexPath.row == kRowLocation)
	{
		PSAEDTextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAEDTextFieldCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
		
	}
	else if (indexPath.row == kRowInstalledDate)
	{
		PSAEDDatePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAEDDatePickerCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
		
	}
	else
	{
		PSAEDPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAEDPickerCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
	if ([*cell isKindOfClass:[PSAEDTextFieldCell class]])
	{
		PSAEDTextFieldCell *_cell = (PSAEDTextFieldCell *)*cell;
		[_cell configureCell];
		[_cell.txtTitle setDelegate:self];
		switch (indexPath.row) {
			case kRowSerialNumber:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_SERIAL_NUMBER")];
				[_cell.txtTitle setTag:kRowSerialNumber];
				if (!isEmpty(_serialNumber))
				{
					[_cell.txtTitle setText:_serialNumber];
				}
				
				break;
			case kRowManufacturer:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_MANUFACTURER")];
				[_cell.txtTitle setTag:kRowManufacturer];
				if (!isEmpty(_manufacturer))
				{
					[_cell.txtTitle setText:_manufacturer];
				}
				break;
			case kRowLocation:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_LOCATION")];
				[_cell.txtTitle setTag:kRowLocation];
				if (!isEmpty(_location))
				{
					[_cell.txtTitle setText:_location];
				}
				break;
				
			default:
				break;
		}
		*cell = _cell;
	}
	
	else if ([*cell isKindOfClass:[PSAEDPickerCell class]])
	{
		PSAEDPickerCell *_cell = (PSAEDPickerCell *)*cell;
		[_cell setDelegate:self];
		[_cell.txtTitle setDelegate:self];
		[_cell configureCell];
		switch (indexPath.row) {
			case kRowDetectorType:
				
				[_cell.txtTitle setTag:kRowDetectorType];
				[_cell setPickerOptions:[self getDetectorPickerOptions]];
				[_cell.lblHeader setText:LOC(@"KEY_STRING_TYPE")];
				if (!isEmpty(_detectorType))
				{
					[_cell.txtTitle setText:_detectorType.detectorType];
				}
				[_cell.btnPicker addTarget:self action:@selector(onClickDetectorTypePicker:) forControlEvents:UIControlEventTouchUpInside];
				
				break;
				
			case kRowPower:
				[_cell.txtTitle setTag:kRowPower];
				[_cell setPickerOptions:[self getPowerPickerOptions]];
				[_cell.lblHeader setText:LOC(@"KEY_STRING_POWER")];
				if (!isEmpty(_power))
				{
					[_cell.txtTitle setText:_power.powerType];
				}
				[_cell.btnPicker addTarget:self action:@selector(onClickPowerPicker:) forControlEvents:UIControlEventTouchUpInside];
				
				break;
			case kRowLandlordsDetector:
				
				[_cell.txtTitle setTag:kRowLandlordsDetector];
				[_cell setPickerOptions:_yesNoPickerOption];
				[_cell.lblHeader setText:LOC(@"KEY_STRING_LANDLORDS_DETECTOR")];
				if (isEmpty(_isLandlordDetector) == NO)
				{
					[_cell.txtTitle setText:_isLandlordDetector];
				}
				[_cell.btnPicker addTarget:self action:@selector(onClickLandlordDetectorPicker:) forControlEvents:UIControlEventTouchUpInside];
				
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSAEDDatePickerCell class]])
	{
		PSAEDDatePickerCell *_cell = (PSAEDDatePickerCell *)*cell;
		[_cell setDelegate:self];
		[_cell configureCell];
		switch (indexPath.row)
		{
			case kRowInstalledDate:
				[_cell.lblHeader setText:LOC(@"KEY_STRING_INSTALLED")];
				[_cell.lblTitle setText:nil];
				[_cell.resetDatesButton setHidden:YES];
				if (!isEmpty(_installedDate))
				{
					[_cell.lblTitle setText:[UtilityClass stringFromDate:_installedDate dateFormat:kDateTimeStyle8]];
					[_cell.resetDatesButton setHidden:NO];
				}
				[_cell.btnPicker addTarget:self action:@selector(onClickInstalledDate:) forControlEvents:UIControlEventTouchUpInside];
				[_cell.resetDatesButton addTarget:self action:@selector(onClickResetInstalledDate:) forControlEvents:UIControlEventTouchUpInside];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickInstalledDate:(id)sender
{
	CLS_LOG(@"onClickInstalledDate");
	[self resignAllResponders];
	_pickerTag = kRowInstalledDate;
}

- (IBAction)onClickLocationPicker:(id)sender
{
	CLS_LOG(@"onClickLocationPicker");
	[self resignAllResponders];
	_pickerTag = kRowLocation;
}

- (IBAction)onClickDetectorTypePicker:(id)sender
{
	CLS_LOG(@"onClickDetectorTypePicker");
	[self resignAllResponders];
	_pickerTag = kRowDetectorType;
}

- (IBAction)onClickPowerPicker:(id)sender
{
	CLS_LOG(@"onClickPowerPicker");
	[self resignAllResponders];
	_pickerTag = kRowPower;
}

- (IBAction)onClickLandlordDetectorPicker:(id)sender
{
	CLS_LOG(@"onClickLandlordDetectorPicker");
	[self resignAllResponders];
	_pickerTag = kRowLandlordsDetector;
}

- (IBAction)onClickEditLocation:(id)sender
{
	CLS_LOG(@"onClickEditLocation");
	
}

- (IBAction)onClickEditType:(id)sender
{
	CLS_LOG(@"onClickEditType");
}

- (IBAction)onClickEditMake:(id)sender
{
	CLS_LOG(@"onClickEditMake");
}

- (IBAction)onClickResetInstalledDate:(id)sender
{
	CLS_LOG(@"onClickResetInstalledDate");
	[self resignAllResponders];
	_installedDate = nil;
	[self.tableView reloadData];
}

#pragma mark - Scroll Cell Methods
- (void)scrollToRectOfSelectedControl {
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[self resignAllResponders];
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
	switch (textField.tag) {
		case kRowSerialNumber:
			_serialNumber = textField.text;
			break;
		case kRowManufacturer:
			_manufacturer = textField.text;
			break;
		case kRowLocation:
			_location = textField.text;
			break;
		default:
			break;
	}
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	BOOL textFieldShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
	switch (textField.tag) {
		case kRowSerialNumber:
			if ([stringAfterReplacement length] > 100)
			{
				textFieldShouldReturn = NO;
			}
			break;
		case kRowManufacturer:
		{
			if ([stringAfterReplacement length] > 100)
			{
				textFieldShouldReturn = NO;
			}
		}
			break;
		case kRowLocation:
		{
			if ([stringAfterReplacement length] > 200)
			{
				textFieldShouldReturn = NO;
			}
		}
			break;
			
		default:
			break;
	}
	return textFieldShouldReturn;
}

#pragma mark - Protocol Methods

- (void) didSelecPickerOption:(NSInteger)pickerOption
{
	switch (_pickerTag)
	{
		case kRowDetectorType:
			_detectorType = [_detectorTypes objectAtIndex:pickerOption];
			break;
		case kRowPower:
			_power = [_powerTypes objectAtIndex:pickerOption];
			break;
		case kRowLandlordsDetector:
			_isLandlordDetector = [_yesNoPickerOption objectAtIndex:pickerOption];
			break;
			
		default:
			break;
	}
	[self.tableView reloadData];
}

- (void) didSelectDate:(NSDate *)pickerOption
{
	if (!isEmpty(pickerOption))
	{
		switch (_pickerTag)
		{
			case kRowInstalledDate:
				_installedDate = pickerOption;
				break;
			default:
				break;
		}
		[self.tableView reloadData];
	}
}

#pragma mark - Resign All Responders
- (void) resignAllResponders
{
	[self.view endEditing:YES];
}

#pragma mark - Methods

- (NSArray*) sortArray:(NSString*)sortDescriptorKey :(NSArray*) objArray
{
	NSSortDescriptor *sortDescriptor;
	sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortDescriptorKey ascending:YES];
	NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
	objArray = [objArray sortedArrayUsingDescriptors:sortDescriptors];
	return objArray;
}

-(NSArray *)getPowerPickerOptions
{
	NSMutableArray * powerTypeList = [[NSMutableArray alloc]init];
	for (PowerType *type in _powerTypes)
	{
		[powerTypeList addObject:type.powerType];
	}
	return powerTypeList;
}

-(NSArray *)getDetectorPickerOptions
{
	NSMutableArray * detectorTypeList = [[NSMutableArray alloc]init];
	for (DetectorType *type in _detectorTypes)
	{
		[detectorTypeList addObject:type.detectorType];
	}
	return detectorTypeList;
}

-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSAEDDatePickerCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSAEDPickerCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSAEDTextFieldCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void) populateDefaultValues
{
	
	if (isEmpty(self.detector)== NO)
	{
		_detectorType = [[PSApplianceManager sharedManager] fetchDetectorTypeWithDetectorTypeId:self.detector.detectorTypeId];
		_power = [[PSApplianceManager sharedManager] fetchPowerTypeWithPowerTypeId:self.detector.powerTypeId];
		_location = self.detector.location;
		_manufacturer = self.detector.manufacturer;
		_serialNumber = self.detector.serialNumber;
		_installedDate = self.detector.installedDate;
		_isLandlordDetector = [self convertNumberToString:self.detector.isLandlordsDetector];
		_isDetectorInspected = (isEmpty(self.detector.isInspected)?[NSNumber numberWithBool:NO]:self.detector.isInspected);
	}
	else {
		_detectorType = nil;
		_power = nil;
		_location = nil;
		_manufacturer = nil;
		_serialNumber = nil;
		_installedDate = nil;
		_isLandlordDetector = nil;
		_isDetectorInspected = [NSNumber numberWithBool:NO];
	}
}

- (void) getPickersData
{
	
	[self getDetectorTypes];
	[self getPowerTypes];
	[self getYesNoPickerOptions];
	
}

- (void) getDetectorTypes
{
	NSString *sortColumn = kDetectorsType;
	_detectorTypes = [[PSApplianceManager sharedManager] fetchDetectorTypes:sortColumn];
}

- (void) getPowerTypes
{
	NSString *sortColumn = kPowerTypeId;
	_powerTypes = [[PSApplianceManager sharedManager] fetchPowerTypes:sortColumn];
}

- (void) getYesNoPickerOptions
{
	_yesNoPickerOption = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
}

- (NSString*) convertNumberToString:(NSNumber *) number
{
	if (number == nil)
		return nil;
	return 	([number boolValue]==YES)?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO");
}

- (NSNumber*) getNSNumberValue:(NSString *) string
{
	return 	(string==LOC(@"KEY_ALERT_YES"))?[NSNumber numberWithBool:YES]:[NSNumber numberWithBool:NO];
}

- (Boolean) isFormValid
{
	NSString *alertMessage = nil;
	NSString *emptyField =nil;
	BOOL isValid = YES;
	
	if (isEmpty(_detectorType)) {
		emptyField = LOC(@"KEY_STRING_TYPE");
		alertMessage = [NSString stringWithFormat:@"Please select %@.", [emptyField lowercaseString]];
		isValid = NO;
	}else if (isEmpty(_location.trimString)){
		emptyField = LOC(@"KEY_STRING_LOCATION");
		alertMessage = [NSString stringWithFormat:@"Please enter %@.", [emptyField lowercaseString]];
		isValid = NO;
	}else if (isEmpty(_power)){
		emptyField = LOC(@"KEY_STRING_POWER");
		alertMessage = [NSString stringWithFormat:@"Please select %@.", [emptyField lowercaseString]];
		isValid = NO;
	}

	
	if (isValid == NO)
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																									 message:alertMessage
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
	}
	
	return isValid;
	
}

- (void) saveDetector
{
	[self disableNavigationBarButtons];
	[self showActivityIndicator:LOC(@"KEY_STRING_SAVING_DETECTOR")];
	
	if (isEmpty(self.detector))
	{
		CLS_LOG(@"New detector ID %@",self.detectorNewId);
		[_detectorDictionary setObject:self.detectorNewId forKey:kDetectorId];
	}
	else
	{
		[_detectorDictionary setObject:isEmpty(self.detector.detectorId)?[NSNull null]:self.detector.detectorId forKey:kDetectorId];
		[_detectorDictionary setObject:self.detector.objectID forKey:@"detectorObjectId"];
	}
	
	[_detectorDictionary setObject:isEmpty(_detectorType)?[NSNull null]:_detectorType.detectorTypeId forKey:kDetectorTypeId];
	[_detectorDictionary setObject:isEmpty(_detectorType)?[NSNull null]:_detectorType.detectorType forKey:kDetectorsType];
	[_detectorDictionary setObject:isEmpty(_installedDate)?[NSNull null]:[UtilityClass convertNSDateToServerDate:_installedDate] forKey:kInstalledDate];
	[_detectorDictionary setObject:_isDetectorInspected forKey:kIsDetectorInspected];
	[_detectorDictionary setObject:isEmpty(_isLandlordDetector)?[NSNull null]:[self getNSNumberValue:_isLandlordDetector] forKey:kIsLandlordsDetector];
	[_detectorDictionary setObject:isEmpty(_location)?[NSNull null]:_location forKey:kLocation];
	[_detectorDictionary setObject:isEmpty(_manufacturer)?[NSNull null]:_manufacturer forKey:kManufacturer];
	[_detectorDictionary setObject:isEmpty(self.appointment.appointmentToProperty.propertyId)?[NSNull null]:self.appointment.appointmentToProperty.propertyId forKey:kDetectorPropertyID];
	[_detectorDictionary setObject:isEmpty(_serialNumber)?[NSNull null]:_serialNumber forKey:kSerialNumber];
	[_detectorDictionary setObject:isEmpty(_power)?[NSNull null]:_power.powerTypeId forKey:kPowerTypeId];
	BOOL isSaved = [[PSDataPersistenceManager sharedManager] saveDetector:_detectorDictionary forProperty:self.appointment.appointmentToProperty.objectID];
	
	
	if (isSaved) {
		[self hideActivityIndicator];
		[self popOrCloseViewController];
	}else{
  
		[self hideActivityIndicator];
		
		NSString *alertMessage = LOC(@"KEY_ALERT_FAILED_TO_SAVE_DETECTOR");
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																									 message:alertMessage
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
		[self enableNavigationBarButtons];
  
	}
	
}

- (void) saveDetectorOnline:(NSDictionary *)detector
{
	//#warning PSA: online appliance saving disabled. uncomment below line to activate service.
	//    [[PSApplianceManager sharedManager] saveAppliance:appliance forProperty:self.appointment.appointmentToProperty.objectID];
	//    [[PSDataPersistenceManager sharedManager]saveApplianceData:@[appliance] forProperty:self.appointment.appointmentToProperty.objectID isAddedOffline:YES];
	//    [self schduleUIControlsActivation];
	
}

@end
