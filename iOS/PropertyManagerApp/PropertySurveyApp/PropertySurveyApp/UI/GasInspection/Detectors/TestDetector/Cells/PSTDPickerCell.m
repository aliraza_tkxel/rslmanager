//
//  PSInspectAppliancePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSTDPickerCell.h"

@implementation PSTDPickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClickPickerBtn:(id)sender {
    if(!isEmpty(self.pickerOptions))
    {
        [ActionSheetStringPicker showPickerWithTitle:self.lblHeader.text
                                                rows:self.pickerOptions
                                    initialSelection:self.selectedIndex
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self.selectedIndex = [selectedIndex integerValue];
    self.lblTitle.text = [self.pickerOptions objectAtIndex:self.selectedIndex];
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelecPickerOption:)])
    {
        [(PSTestDetectorViewController *)self.delegate didSelecPickerOption:self.lblTitle.text];
    }
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

-(void) configureCell{
  [self.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  [self.lblTitle setText:nil];
}

#pragma mark Methods
- (BOOL) isValid
{
    BOOL isvalid;
    if (isEmpty(self.lblTitle.text.trimString))
    {
        isvalid = NO;
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}
    
@end
