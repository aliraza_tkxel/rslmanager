//
//  PSAddApplianceTextFieldCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSTDTextViewCell.h"

@implementation PSTDTextViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (IBAction)onClickEditBtn:(id)sender
{
    [self.txtNotes becomeFirstResponder];
}

-(void) configureCell{
  [self.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.txtNotes setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  [self.txtNotes setKeyboardType:UIKeyboardTypeDefault];
  [self.txtNotes setText:nil];
}

#pragma mark - Methods

- (BOOL) isValid
{
    BOOL isvalid;
    if (isEmpty(self.txtNotes.text.trimString))
    {
        isvalid = NO;
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}
@end
