//
//  PSAddApplianceTextFieldCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTDTextViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UITextView *txtNotes;

- (IBAction)onClickEditBtn:(id)sender;
- (BOOL) isValid;
-(void) configureCell;
@end
