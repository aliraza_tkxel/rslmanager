  //
  //  PSInspectApplianceViewController.m
  //  PropertySurveyApp
  //
  //  Created by Yawar on 11/11/2013.
  //  Copyright (c) 2013 TkXel. All rights reserved.
  //

#import "PSTestDetectorViewController.h"
#import "PSBarButtonItem.h"
#import "PSTDPickerCell.h"
#import "PSTDDatePickerCell.h"
#import "PSTDLabelCell.h"
#import "PSTextViewCell.h"

#define kNumberOfRows 4
#define kRowLastTested 0
#define kRowBatteryReplaced 1
#define kRowPassed 2
#define kRowNotes 3

#define kTextViewCellHeight 94

@interface PSTestDetectorViewController ()
{
  NSInteger _pickerTag;
  NSDate *_lastTested;
  NSDate *_batteryReplaced;
  NSString* _isPassed;
  NSString *_notes;
  NSNumber *_inspectedBy;
  NSDate *_inspectionDate;
}

@end

@implementation PSTestDetectorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
      // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self registerNibsForTableView];
  [self loadNavigationonBarItems];
  [self populateDetectorDetailHeader];
}

-(void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];

}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
  PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                       style:PSBarButtonItemStyleBack
                                                                      target:self
                                                                      action:@selector(onClickBackButton)];
  
  self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                    style:PSBarButtonItemStyleDefault
                                                                   target:self
                                                                   action:@selector(onClickSaveButton)];
  
  [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
  [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
  [self setTitle:@"Test Detector"];
}

- (void) onClickBackButton
{
  CLS_LOG(@"onClickBackButton");
  [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
  CLS_LOG(@"onClickSaveButton");
  
  if ([self isFormValid]==YES) {
    [self saveInspectionForm];
  }
  
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 44;
	
	if (indexPath.row == kRowNotes )
	{
		rowHeight = [self heightForTextView:nil containingString:_notes];
		if (rowHeight < kTextViewCellHeight)
		{
			rowHeight = kTextViewCellHeight;
		}
	}
	
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return kNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = nil;
  
  if (indexPath.row == kRowLastTested )
    {
    PSTDLabelCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSTDLabelCell class])];
    [self configureCell:&_cell atIndexPath:indexPath];
    cell = _cell;
    }
  else if (indexPath.row == kRowBatteryReplaced )
    {
    PSTDDatePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSTDDatePickerCell class])];
    [self configureCell:&_cell atIndexPath:indexPath];
    cell = _cell;
    }
  else if (indexPath.row == kRowPassed)
    {
    PSTDPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSTDPickerCell class])];
    [self configureCell:&_cell atIndexPath:indexPath];
    cell = _cell;
    }
	else if (indexPath.row == kRowNotes)
	{
		PSTextViewCell *_cell = [[PSTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:textViewCellIdentifier];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	
  [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
  return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
	
	if ([*cell isKindOfClass:[PSTextViewCell class]])
	{
		PSTextViewCell *_cell = (PSTextViewCell *)*cell;
		UILabel *header = [[UILabel alloc] init];
		header.frame = CGRectMake(12, 3, 320,12);
		[header setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
		[_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
		[header setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12]];
		[_cell addSubview:header];
		
		PSTextView*  _textView = [[PSTextView alloc] initWithFrame:CGRectZero];
		[_textView setDelegate:self];
		[_textView setTag:indexPath.row];
		[_textView setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
		[_textView setUserInteractionEnabled:YES];
		
		switch (indexPath.row)
		{
			case kRowNotes:
				[header setText:LOC(@"KEY_STRING_TEST_DETECTOR_NOTES")];
				if (isEmpty(_notes)==NO)
				{
					_textView.text = _notes;
				}
				break;
		}
		
		_cell.textView = _textView;
		_textView = nil;
		*cell = _cell;
		
	}
  else if ([*cell isKindOfClass:[PSTDDatePickerCell class]])
    {
    PSTDDatePickerCell *_cell = (PSTDDatePickerCell *)*cell;
    [_cell setDelegate:self];
    [_cell configureCell];
    switch (indexPath.row)
      {
        case kRowBatteryReplaced:
        [_cell.lblHeader setText:LOC(@"KEY_STRING_DETECTOR_BATTERY_REPLACED")];
        if (!isEmpty(_batteryReplaced))
          {
          [_cell.lblTitle setText:[UtilityClass stringFromDate:_batteryReplaced dateFormat:kDateTimeStyle8]];
          [_cell.resetDatesButton setHidden:NO];
          }
        [_cell.btnPicker addTarget:self action:@selector(onClickBatteryReplaced:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.resetDatesButton addTarget:self action:@selector(onClickResetBatteryReplaced:) forControlEvents:UIControlEventTouchUpInside];
        break;
        
        default:
        break;
      }
    *cell = _cell;
    
    }
  else if ([*cell isKindOfClass:[PSTDPickerCell class]])
    {
    PSTDPickerCell *_cell = (PSTDPickerCell *)*cell;
    [_cell setDelegate:self];
    [_cell configureCell];
    
    switch (indexPath.row) {
      case kRowPassed:
        [_cell.lblHeader setText:LOC(@"KEY_STRING_TEST_DETECTOR_PASSED")];
        _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
        [_cell.lblTitle setText:isEmpty(_isPassed)?nil:([_isPassed boolValue] == YES)?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO")];
        [_cell.btnPicker addTarget:self action:@selector(onClickPassed:) forControlEvents:UIControlEventTouchUpInside];
        break;
      default:
        break;
    }
    *cell = _cell;
    }
  else if ([*cell isKindOfClass:[PSTDLabelCell class]])
    {
    PSTDLabelCell *_cell = (PSTDLabelCell *)*cell;
    [_cell configureCell];
    
    switch (indexPath.row) {
      case kRowLastTested:
        [_cell.lblHeader setText:LOC(@"KEY_STRING_TEST_DETECTOR_LAST_TESTED")];
        [_cell.lblTitle setText:[UtilityClass stringFromDate:_lastTested dateFormat:kDateTimeStyle8]];
        break;
      default:
        break;
    }
    *cell = _cell;
    }
  
  
}


- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
  [self resignAllResponders];
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickPassed:(id)sender
{
  CLS_LOG(@"onClickPassed");
  [self resignAllResponders];
  _pickerTag = kRowPassed;
}


- (IBAction)onClickBatteryReplaced:(id)sender
{
  CLS_LOG(@"onClickBatteryReplaced");
  [self resignAllResponders];
  _pickerTag = kRowBatteryReplaced;
}

- (IBAction)onClickResetBatteryReplaced:(id)sender
{
  CLS_LOG(@"onClickResetBatteryReplaced");
  [self resignAllResponders];
  _batteryReplaced = nil;
  [self.tableView reloadData];
}


#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
  BOOL textVeiwShouldReturn = YES;
  NSString *stringAfterReplacement = [((UITextView *)self.selectedControl).text stringByAppendingString:text];
  
  if ([stringAfterReplacement length] > 1000)
    {
    textVeiwShouldReturn = NO;
    }
  return textVeiwShouldReturn;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
  return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
  self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
  _notes = textView.text;
}

- (void)textViewDidChange:(UITextView *)textView
{
  _notes = textView.text;
  [self.tableView beginUpdates];
  [self.tableView endUpdates];
  [self scrollToRectOfSelectedControl];
}

#pragma mark - Protocol Methods
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption
{
  _isPassed = pickerOption;
}

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption
{
  if (!isEmpty(pickerOption))
    {
    switch (_pickerTag)
      {
        case kRowBatteryReplaced:
        _batteryReplaced = pickerOption;
        break;
        
        default:
        break;
      }
    [self.tableView reloadData];
    }
}


#pragma mark - Methods

- (NSString*) convertNumberToString:(NSNumber *) number
{
	
	if (number == nil)
		return nil;
	return 	([number boolValue]==YES)?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO");
}

- (NSNumber*) getNSNumberValue:(NSString *) string
{
	return 	(string==LOC(@"KEY_ALERT_YES"))?[NSNumber numberWithBool:YES]:[NSNumber numberWithBool:NO];
}


#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 40;
	CGFloat height = 0.0;
	if(OS_VERSION >= 7.0)
	{
		NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
		[style setLineBreakMode:NSLineBreakByWordWrapping];
		NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
																style, NSParagraphStyleAttributeName,
																
																[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17],NSFontAttributeName,
																nil];
		CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(285, 999999.0f)
																							 options:NSStringDrawingUsesLineFragmentOrigin
																						attributes:attributes
																							 context:nil];
		height = boundingRect.size.height + verticalPadding;
	}
	else
	{
		height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
								constrainedToSize:CGSizeMake(285, 999999.0f)
										lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
	}
	return height * 1.05;
}

-(void)registerNibsForTableView
{
  NSString * nibName = NSStringFromClass([PSTDLabelCell class]);
  [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
  nibName = NSStringFromClass([PSTDDatePickerCell class]);
  [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
  nibName = NSStringFromClass([PSTDPickerCell class]);
  [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void) populateDetectorDetailHeader{

  NSMutableString *detectorLabel =[NSMutableString stringWithString:@""];
  
  if (!isEmpty(self.detector))
    {
    [detectorLabel appendString:@"Detector"];
    if (!isEmpty(self.detector.detectorType))
      {
      [detectorLabel appendString:[NSString stringWithFormat:@" > %@",self.detector.detectorType]];
      }
    
    if ([self.detector.isInspected boolValue]==YES) {
      [self initializeDetectorDetails];
    }
      _lastTested =self.detector.lastTestedDate;
    }
  
  [self.lblDetectorDetail setText:detectorLabel];
  [self.lblDetectorDetail setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  
}

- (Boolean) isFormValid
{
  NSString *alertMessage = nil;
  NSString *emptyField=nil;
  BOOL isValid = YES;
  
	if (isEmpty(_isPassed)) {
		emptyField = LOC(@"KEY_STRING_TEST_DETECTOR_PASSED");
		alertMessage = [NSString stringWithFormat:@"Please select %@.", [emptyField lowercaseString]];
		isValid = NO;
	}
	
  if (isValid == NO)
    {
    [self showAlert:alertMessage];
    }
  
  return isValid;
  
}

- (void) saveInspectionForm
{
  BOOL isSaved = NO;
  NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
  _inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
  _inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
  [inspectionFormDictionary setObject:isEmpty(_batteryReplaced)?[NSNull null]:_batteryReplaced forKey:kBatteryReplaced];
  [inspectionFormDictionary setObject:isEmpty(_inspectedBy)?[NSNull null]:_inspectedBy forKey:kDetectorInspectedBy];
  [inspectionFormDictionary setObject:isEmpty(_inspectionDate)?[NSNull null]:[UtilityClass convertNSDateToServerDate:_inspectionDate]  forKey:kDetectorInspectionDate];
  [inspectionFormDictionary setObject:isEmpty(_isPassed)?[NSNull null]:[self getNSNumberValue:_isPassed] forKey:kIsDetectorPassed];
  [inspectionFormDictionary setObject:isEmpty(_lastTested)?[NSNull null]:[UtilityClass convertNSDateToServerDate:_lastTested] forKey:kDetectorLastTestedDate];
  [inspectionFormDictionary setObject:isEmpty(_notes)?[NSNull null]:_notes forKey:kDetectorNotes];
  isSaved = [[PSApplianceManager sharedManager] saveDetectorInspectionForm:inspectionFormDictionary forDetector:self.detector];
  
  if (isSaved==NO) {
    [self showAlert:@"Problem while saving inspection"];
  }else{
    [self popOrCloseViewController];
  }
}

-(void) initializeDetectorDetails {
  _inspectedBy = self.detector.inspectedBy;
  _inspectionDate = self.detector.inspectionDate;
  _batteryReplaced = self.detector.batteryReplaced;
  _isPassed = [self convertNumberToString:self.detector.isPassed];
  _notes = self.detector.notes;
}

-(void) showAlert: (NSString*)alertMessage{
  
  UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                 message:alertMessage
                                                delegate:nil
                                       cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                       otherButtonTitles:nil,nil];
  [alert show];
  
}

@end
