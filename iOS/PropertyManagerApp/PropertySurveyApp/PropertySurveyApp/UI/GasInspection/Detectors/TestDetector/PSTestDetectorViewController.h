//
//  PSInspectApplianceViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"

@protocol PSTestDetectorProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption;

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption;

@end

@interface PSTestDetectorViewController : PSCustomViewController <PSTestDetectorProtocol, UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblDetectorDetail;

@property (weak,   nonatomic) Appliance *appliance;
@property (weak,   nonatomic) Detector *detector;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@end
