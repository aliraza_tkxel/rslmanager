//
//  PSAddAppointmentDatePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSTDDatePickerCell.h"

@interface PSTDDatePickerCell()

@property (nonatomic, retain) ActionSheetDatePicker * picker;

@end
@implementation PSTDDatePickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onclickPickerBtn:(id)sender {
    if(self.selectedDate == nil)
    {
        self.selectedDate = [NSDate date];
    }
    
    self.picker = [ActionSheetDatePicker showPickerWithTitle:self.lblHeader.text
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:self.selectedDate
                                        target:self
                                        action:@selector(onDateSelected:element:)
                                        origin:sender];
}

- (void)onDateSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectDate:)])
    {
        [(PSTestDetectorViewController *)self.delegate didSelectDate:self.selectedDate];
    }
    
}

-(void) configureCell{
  [self.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  [self.lblTitle setText:nil];
  [self.resetDatesButton setHidden:YES];
}

- (BOOL) isValid
{
    BOOL isvalid;
    if (isEmpty(self.lblTitle.text.trimString))
    {
        isvalid = NO;
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}
@end
