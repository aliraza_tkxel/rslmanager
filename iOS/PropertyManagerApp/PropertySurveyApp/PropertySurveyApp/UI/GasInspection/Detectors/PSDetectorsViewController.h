//
//  PSApplianceDetailViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"

@interface PSDetectorsViewController : PSCustomViewController
@property (strong, nonatomic) PSBarButtonItem *addDetectorBtn;
@property (weak,   nonatomic) Appointment *appointment;
@end
