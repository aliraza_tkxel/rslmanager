//
//  PSGasSurveyViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 04/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSGasSurveyViewController.h"
#import "PSGasSurveyCell.h"
#import "PSIssueReceivedViewController.h"
#import "PSInstallationPipeworkViewController.h"
#import "PSDefectsViewController.h"
#import "PSApplianceDetailViewController.h"
#import "PSBoilerDetailViewController.h"
#import "PSMetersViewController.h"
#import "PSBarButtonItem.h"
#import "Appointment+JSON.h"
#import "PSDetectorsViewController.h"
#import "PSNoEntryViewController.h"
#import "PSAbortInspectionViewController.h"
#import "PSBoilersListViewController.h"


#define kTotalNumberOfRows 7
#define kRowBoilerDetails 0
#define kRowApplianceDetails 1
#define kRowGasPipeWork 2
#define kRowDetectors 3
#define kRowMeters 4
#define kRowDefects 5
#define kRowIssued 6


@interface PSGasSurveyViewController ()
{
    NSArray *_cellLabels;
	  BOOL _isBoilerInspectionComplete;
    BOOL _isApplianceInspectionComplete;
    BOOL _isInstallationPipeworkComplete;
    BOOL _isDefectInspectionComplete;
    BOOL _isIssuedReceivedByComplete;
    BOOL _isDetectorsComplete;
    BOOL _isMetersComplete;
    NSMutableArray *failReasonsArray;
}

@end

@implementation PSGasSurveyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadSurveyUI];
    
    [self loadNavigationonBarItems];
    [self.btnMarkComplete setEnabled:NO];
    _cellLabels = @[LOC(@"KEY_STRING_BOILER_DETAILS"), LOC(@"KEY_STRING_APPLIANCE_DETAILS"), LOC(@"KEY_STRING_GAS_INSTALLATION_PIPEWORK"), LOC(@"KEY_STRING_DETECTORS"), LOC(@"KEY_STRING_METERS"),LOC(@"KEY_STRING_DEFECTS"), LOC(@"KEY_STRING_ISSUED_RECEIVED_BY")];

    [self.imageView setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
    
    failReasonsArray = [NSMutableArray array];
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    
}



-(void)onPictureDefault:(NSNotification*) notification
{
    [self.imageView setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
		[self loadDefaultValue];
    [self registerNotifications];
    // NSNumber *inspectedAppliances = [[PSApplianceManager sharedManager] totalNumberOfAppliancesInspected:self.appointment.appointmentToProperty];
    
    [self onPictureDefault:nil];
    
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
     [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    if ([self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
    {
        self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
                                                                    style:PSBarButtonItemStyleDefault
                                                                   target:self
                                                                   action:@selector(onClickNoEntryButton)];
        
        self.abortButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_ABORT")
                                                                    style:PSBarButtonItemStyleAbort
                                                                   target:self
                                                                   action:@selector(onClickAbortButton)];
        
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.noEntryButton,self.abortButton, nil]];
    }
    
    [self setTitle:surveyTitle];
}

#pragma mark Navigation Bar Button Selectors

-(void) onClickAbortButton
{
    CLS_LOG(@"abort clicked");
    PSAbortInspectionViewController *abortVC = [[PSAbortInspectionViewController alloc] initWithNibName:@"PSAbortInspectionViewController" bundle:nil];
    abortVC.appointment = self.appointment;
    [self.navigationController pushViewController:abortVC animated:YES];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
}

- (void) onClickNoEntryButton
{
    PSNoEntryViewController *noEntryViewController = [[PSNoEntryViewController alloc] initWithNibName:@"PSNoEntryViewController" bundle:nil];
    [noEntryViewController setAppointment:self.appointment];
    [self.navigationController pushViewController:noEntryViewController animated:YES];
    
    CLS_LOG(@"onClickNoEntryButton");
}


#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewTagMarkComplete)
    {
       	if(buttonIndex == alertView.firstOtherButtonIndex)
        {
            //[self showActivityIndicator:LOC(@"KEY_STRING_PROCESSING")];
            
            self.appointment.appointmentToCP12Info.cp12Passed = [NSNumber numberWithBool:YES];
            [self.appointment.managedObjectContext save:nil];
            
            self.appointment.appointmentStatus = [UtilityClass statusStringForAppointmentStatus:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]]];
            [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]] appointment:self.appointment];
            [self popToRootViewController];
        }
    }
    else if (alertView.tag == AlertViewTagFailCertificate)
    {
        if(buttonIndex == 1)
        {
            self.appointment.appointmentToCP12Info.cp12Passed = [NSNumber numberWithBool:NO];
            [self.appointment.managedObjectContext save:nil];
            
            self.appointment.appointmentStatus = [UtilityClass statusStringForAppointmentStatus:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]]];
            [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]] appointment:self.appointment];
            [self popToRootViewController];

            NSLog(@"confirm");
        }
    }
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    /*
     The count check ensures that if array is empty, i.e no record exists
     for particular section, the header height is set to zero so that
     section header is not alloted any space.
     */
    CGFloat headerHeight = 0.0;
    
    return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight =44;
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kTotalNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    static NSString *gasSurveyCellIdentifier = @"gasSurveyCellIdentifier";
    PSGasSurveyCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:gasSurveyCellIdentifier];
    
    if (cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"PSGasSurveyCell" owner:self options:nil];
        _cell = self.gasSurveyCell;
        self.gasSurveyCell = nil;
    }
    [self configureCell:&_cell atIndexPath:indexPath];
    cell = _cell;
    cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSGasSurveyCell class]])
    {
        
        PSGasSurveyCell *_cell = (PSGasSurveyCell *) *cell;
        [_cell.lblTitle setText:[_cellLabels objectAtIndex:indexPath.row]];
        _cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        BOOL isInspected = NO;
        switch (indexPath.row)
        {
						case kRowBoilerDetails:
						    isInspected = [[PSApplianceManager sharedManager] isBoilerInspected:self.appointment.appointmentToProperty];
						    _isBoilerInspectionComplete = (self.appointment.appointmentToProperty.propertyToBoiler.count > 0)?isInspected:YES;
						    break;
            case kRowApplianceDetails:
                isInspected = [[PSApplianceManager sharedManager] isApplianceInspected:self.appointment.appointmentToProperty];
						    _isApplianceInspectionComplete = ([[[PSApplianceManager sharedManager] getActiveAppliances:self.appointment.appointmentToProperty] count] > 0)?isInspected:YES;
                break;
						case kRowDetectors:
						     isInspected = [[PSApplianceManager sharedManager] isDetectorInspected:self.appointment.appointmentToProperty];
						     _isDetectorsComplete = (self.appointment.appointmentToProperty.propertyToDetector.count > 0)?isInspected:YES;
						     break;
						case kRowMeters:
						    isInspected = [[PSApplianceManager sharedManager] isMeterInspected:self.appointment.appointmentToProperty];
						    _isMetersComplete = (self.appointment.appointmentToProperty.propertyToMeter.count > 0)?isInspected:YES;
						    break;
					  case kRowGasPipeWork:
                
                isInspected = [self checkGasPipelineInstallationForAllBoilers];
                _isInstallationPipeworkComplete = isInspected;
                break;
            case kRowDefects:
                isInspected = [[PSApplianceManager sharedManager]isDefectAdded:self.appointment.journalId];
                _isDefectInspectionComplete = isInspected;
                break;
            case kRowIssued:
                isInspected = [self.appointment.appointmentToCP12Info.inspectionCarried boolValue];
                _isIssuedReceivedByComplete = isInspected;
                break;
            default:
                break;
        }
        
        if (isInspected)
        {
            _cell.imgStatus.image = [UIImage imageNamed:@"icon_Complete"];
        }
        else
        {
            _cell.imgStatus.image = [UIImage imageNamed:@"icon_Cross"];
        }
        if (_isIssuedReceivedByComplete
						&& _isInstallationPipeworkComplete
						&& _isApplianceInspectionComplete
						&& _isBoilerInspectionComplete)
        {
            [self.btnMarkComplete setEnabled:YES];
            [self.btnMarkComplete setBackgroundColor:[UIColor redColor]];
        }
        else
        {
            [self.btnMarkComplete setEnabled:NO];
            [self.btnMarkComplete setBackgroundColor:[UIColor darkGrayColor]];
        }
        *cell = _cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	  if (indexPath.row == kRowBoilerDetails)
	  {
		    PSBoilerDetailViewController *boilerDetailViewController = [[PSBoilerDetailViewController alloc] initWithNibName:@"PSBoilerDetailViewController" bundle:nil];
		    [boilerDetailViewController setAppointment:self.appointment];
		    [self.navigationController pushViewController:boilerDetailViewController animated:YES];
	  }
    if (indexPath.row == kRowApplianceDetails)
    {
        PSApplianceDetailViewController *applianceDetailViewController = [[PSApplianceDetailViewController alloc] initWithNibName:@"PSApplianceDetailViewController" bundle:nil];
        [applianceDetailViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:applianceDetailViewController animated:YES];
    }
    
    else if (indexPath.row == kRowGasPipeWork)
    {
        PSBoilersListViewController *pipeInstallationViewController = [[PSBoilersListViewController alloc] initWithNibName:@"PSBoilersListViewController" bundle:nil];
        [pipeInstallationViewController setAppointment:self.appointment];
        [pipeInstallationViewController setDestinationVC:BoilerListViewControllerDestinationGasInstallation];
        [self.navigationController pushViewController:pipeInstallationViewController animated:YES];
    }
    
    else if (indexPath.row == kRowDefects)
    {
        PSDefectsViewController *defectsViewController = [[PSDefectsViewController alloc] initWithNibName:@"PSDefectsViewController" bundle:nil];
        [defectsViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:defectsViewController animated:YES];
    }
    
    else if (indexPath.row == kRowIssued)
    {
        PSIssueReceivedViewController *issueReceivedViewController = [[PSIssueReceivedViewController alloc] initWithNibName:@"PSIssueReceivedViewController" bundle:nil];
        [issueReceivedViewController setCp12Info:self.appointment.appointmentToCP12Info];
        [issueReceivedViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:issueReceivedViewController animated:YES];
    }
    else if (indexPath.row == kRowDetectors)
        {
        PSDetectorsViewController *detectorsViewController = [[PSDetectorsViewController alloc] initWithNibName:@"PSDetectorsViewController" bundle:nil];
        [detectorsViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:detectorsViewController animated:YES];
        }
    else if (indexPath.row == kRowMeters)
        {
        PSMetersViewController *metersViewController = [[PSMetersViewController alloc] initWithNibName:@"PSMetersViewController" bundle:nil];
        [metersViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:metersViewController animated:YES];
        }
    
}

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentSuccessNotificationReceive) name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentFailureNotificationReceive) name:kCompleteAppointmentFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentFailureNotification object:nil];
}

- (void) onCompleteAppointmentSuccessNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

- (void) onCompleteAppointmentFailureNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        self.appointment.appointmentStatus = kAppointmentStatusInProgress;

        [self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
                         message:LOC(@"KEY_ALERT_FAILED_TO_COMPLETE_APPOINTMENT")
                             tag:0
               cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
               otherButtonTitles:nil];
    });
}

#pragma mark - Methods


- (void)loadSurveyUI {
    Customer *customer = [self.appointment defaultTenant];
    self.lblTenantName.text = [customer fullName];
    
    Property *property = [self.appointment appointmentToProperty];
    if(!isEmpty(property.propertyId)){
        self.lblAddress.text = [property addressWithStyle:PropertyAddressStyle1];
    }
    else if(!isEmpty(property.blockId)){
        self.lblAddress.text = property.blockName;
    }
    else if(!isEmpty(property.schemeId)){
        self.lblAddress.text = property.schemeName;
    }
    
    if (!isEmpty(self.appointment.appointmentToProperty.certificateExpiry))
    {
			[self.lblCertificateExpiry setHidden:NO];
        [self.lblCertificateExpiry setText:[NSString stringWithFormat:@"Certificate Expiry: %@",[UtilityClass stringFromDate:self.appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]]];
    }
    else
    {
        [self.lblCertificateExpiry setHidden:NO];
        [self.lblCertificateExpiry setText:@"Certificate Expiry: N/A"];
    }
    if ([self.appointment getType] == AppointmentTypeGas || [self.appointment getType]== AppointmentTypeGasVoid)
		{
			surveyTitle = @"Gas Survey";
    }
    else if ([self.appointment getType] == AppointmentTypeFault)
		{
			surveyTitle = @"Fault Survey";
    }
    [self.btnBack addTarget:self action:@selector(onClickBackButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onClickBackButton:(id)sender {
    [self popOrCloseViewController];
}

- (IBAction)onClickCompleteBtn:(id)sender
{
    CLS_LOG(@"on click complete button");
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_COMPLETE_APPOINTMENT") message:LOC(@"KEY_ALERT_CONFIRM_COMPLETE_APPOINTMENT") delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_NO") otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
    alert.tag = AlertViewTagMarkComplete;
    [alert show];
    //NSDictionary *appointmentDictionary = [self.appointment JSONValue];
}

-(void) dealloc
{
   // [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:kPropertyPictureDefaultNotification];
    
}

- (void) loadDefaultValue
{
	BOOL isInspected = NO;

	isInspected = [[PSApplianceManager sharedManager] isBoilerInspected:self.appointment.appointmentToProperty];
	_isBoilerInspectionComplete = isInspected;
	
	isInspected = [[PSApplianceManager sharedManager] isApplianceInspected:self.appointment.appointmentToProperty];
	_isApplianceInspectionComplete = ([[[PSApplianceManager sharedManager] getActiveAppliances:self.appointment.appointmentToProperty] count] > 0)?isInspected:YES;

	isInspected = [[PSApplianceManager sharedManager] isDetectorInspected:self.appointment.appointmentToProperty];
	_isDetectorsComplete = (self.appointment.appointmentToProperty.propertyToDetector.count > 0)?isInspected:YES;

	isInspected = [[PSApplianceManager sharedManager] isMeterInspected:self.appointment.appointmentToProperty];
	_isMetersComplete = (self.appointment.appointmentToProperty.propertyToMeter.count > 0)?isInspected:YES;
    
    for(Boiler *boiler in self.appointment.appointmentToProperty.propertyToBoiler){
        isInspected = !isEmpty(boiler.boilerToGasInstallationPipeWork);
        if(!isInspected){
            break;
        }
    }
	_isInstallationPipeworkComplete = isInspected;

	isInspected = [[PSApplianceManager sharedManager]isDefectAdded:self.appointment.journalId];
	_isDefectInspectionComplete = isInspected;

	isInspected = [self.appointment.appointmentToCP12Info.inspectionCarried boolValue];
	_isIssuedReceivedByComplete = isInspected;
	
}
- (IBAction)failCertificateTapped:(id)sender {
    
    [failReasonsArray removeAllObjects];
    
    if([self checkIfCanFail])
    {
        NSString * finalFailString = @"";
        for(NSString * string in failReasonsArray)
        {
            finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,string];
        }
        
        finalFailString = [NSString stringWithFormat:@"The boiler has failed the annual inspeciton due to the following defects: \n\n%@",finalFailString];
        
        [self showAlertWithTitle:@"Fail Certificate." message:finalFailString tag: AlertViewTagFailCertificate cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
    }
}

-(BOOL) checkGasPipelineInstallationForAllBoilers{
    BOOL isInspected = NO;
    for(Boiler *boiler in self.appointment.appointmentToProperty.propertyToBoiler){
        isInspected = !isEmpty(boiler.boilerToGasInstallationPipeWork);
        if(!isInspected){
            break;
        }
    }
    return isInspected;
}

-(BOOL) checkIfCanFail{
    
    
    if (!_isIssuedReceivedByComplete
        || !_isInstallationPipeworkComplete
        || !_isApplianceInspectionComplete
        || !_isBoilerInspectionComplete)
    {
        [self showAlertWithTitle:@"Fail Certificate" message:@"You cannot fail the certificate until your inspection is not complete." tag:0 cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        return  false;
    }

    
    
    
    NSArray* defectCategories = [[PSApplianceManager sharedManager] fetchAllDefectCategories:kCategoryId];
    BOOL __block failFlag = NO;
    for(Boiler *boiler in self.appointment.appointmentToProperty.propertyToBoiler)
    {
        for(Defect *defect in boiler.boilerToDefect)
        {
            [defectCategories enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                DefectCategory * category = (DefectCategory*) obj;
                if([defect.defectCategory.categoryDescription isEqualToString:category.categoryDescription]&& ![category.categoryDescription isEqualToString: @"Other"] && ![category.categoryDescription isEqualToString: @"General comment"])
                {
                    failFlag = YES;
                    NSString * resultString = [NSString stringWithFormat:@"Boiler Defect recorded with %@",defect.defectCategory.categoryDescription];
                    if(![failReasonsArray containsObject:resultString])
                    {
                        [failReasonsArray addObject:[NSString stringWithFormat:@"Boiler Defect recorded with %@",defect.defectCategory.categoryDescription]];
                    }
                }
            }];
        }
        
        
        NSLog(@"Boiler:%@", boiler.boilerToBoilerInspection);
        
        if([boiler.boilerToBoilerInspection.safetyDeviceOperational isEqualToString:@"No"])
        {
            failFlag = YES;
            [failReasonsArray addObject:@"Safety device correct operation = NO"];
        }
        if([boiler.boilerToBoilerInspection.spillageTest isEqualToString:@"Fail"])
        {
            failFlag = YES;
            [failReasonsArray addObject:@"Spillage Test Failed."];
        }
        if([boiler.boilerToBoilerInspection.smokePellet isEqualToString:@"Fail"])
        {
            failFlag = YES;
            [failReasonsArray addObject:@"Smoke Pallet Flue Flow Test Failed."];
        }
        if([boiler.boilerToBoilerInspection.adequateVentilation isEqualToString:@"No"])
        {
            failFlag = YES;
            [failReasonsArray addObject:@"Adequate Ventilation = NO"];
        }
        if([boiler.boilerToBoilerInspection.flueVisualCondition isEqualToString:@"Fail"])
        {
            failFlag = YES;
            [failReasonsArray addObject:@"Flue Vision Condition Failed."];
        }
        if([boiler.boilerToBoilerInspection.satisfactoryTermination isEqualToString:@"No"] || [boiler.boilerToBoilerInspection.satisfactoryTermination isEqualToString:@"Yes"])
        {
            failFlag = YES;
            [failReasonsArray addObject:[NSString stringWithFormat:@"Satisfactory Termination is %@",boiler.boilerToBoilerInspection.satisfactoryTermination]];
        }
        if([boiler.boilerToBoilerInspection.fluePerformanceChecks isEqualToString:@"Fail"])
        {
            failFlag = YES;
            [failReasonsArray addObject:@"Flue Performance Checks Failed."];
        }
        if([boiler.boilerToBoilerInspection.boilerSafeToUse isEqualToString:@"No"])
        {
            failFlag = YES;
            [failReasonsArray addObject:@"Boiler Not Safe To Use."];
        }
        
    }
    
    if(!failFlag)
    {
        [self showAlertWithTitle:@"Fail Certificate" message:@"You cannot fail the certificate." tag:0 cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        return failFlag;
    }
    return failFlag;
}

@end
