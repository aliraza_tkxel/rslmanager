//
//  PSRepairImagesGalleryViewController.h
//  PropertySurveyApp
//
//  Created by TkXel on 10/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSCameraViewController.h"

@interface PSRepairImagesGalleryViewController : PSCameraViewController

@property (strong, nonatomic) Property * propertyObj;
@property (strong, nonatomic) Scheme * schemeObj;
@property (strong, nonatomic) JobSheet * jobDataListObj;
@property (strong, nonatomic) Appointment * appointment;
@property (strong, nonatomic) NSDictionary * repairCompletionDictionary;
@property (assign, nonatomic) BOOL isJobComplete;
@property (assign, nonatomic) BOOL hasNewFault;

@end
