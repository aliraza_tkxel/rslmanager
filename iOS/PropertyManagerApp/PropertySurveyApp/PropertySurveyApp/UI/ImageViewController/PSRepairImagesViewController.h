//
//  PSRepairImagesViewController.h
//  PropertySurveyApp
//
//  Created by TkXel on 01/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSImageViewController.h"

@interface PSRepairImagesViewController : PSImageViewController {
    
}

@property (strong, nonatomic) JobSheet * jobDataListObj;
@property (strong, nonatomic) Appointment * appointment;
@property (strong, nonatomic) NSDictionary * repairCompletionDictionary;
@property (assign, nonatomic) BOOL isJobComplete;


@end
