//
//  PSRepairImagesViewController.m
//  PropertySurveyApp
//
//  Created by TkXel on 01/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSRepairImagesViewController.h"
#import "JobSheet+Methods.h"

@interface PSRepairImagesViewController ()

@end

@implementation PSRepairImagesViewController
//@synthesize jobDataListObj;
@synthesize repairCompletionDictionary;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:@"PSImageViewController" bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobStartNotificationReceive) name:kJobStartNotification object:nil];

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureSave:) name:kRepairPictureSaveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureSaveFailure:) name:kRepairPictureSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobStartNotificationReceive) name:kJobStartNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kJobStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRepairPictureSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRepairPictureSaveNotification object:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void) onClickSaveButton
{
    if(!isEmpty(self.image)) {
        [self saveImageToServer:self.image];
    }
}

-(void) saveImageToServer:(UIImage*) _image
{
    //create an instance for ProprtyPhoto
    [self showActivityIndicator:[NSString stringWithFormat:@"Saving %@ Picture",[self getTitle]]];
    
    NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
    
    [requestParameters setObject:_image forKey:kRepairImage];
    
    
    BOOL  isBefore = YES;
    if (self.isJobComplete || [self.jobDataListObj.jobStatus isEqualToString:kJobStatusComplete])
    {
        isBefore = NO;
    }
    
    [requestParameters setObject:self.propertyObj.propertyId forKey:kPropertyId];
    [requestParameters setObject:self.jobDataListObj.jsNumber forKey:kRepairJSNumber];
    [requestParameters setObject:@".jpg" forKey:kFileExtension];
    [requestParameters setObject:[NSNumber numberWithBool:isBefore] forKey:kIsBeforeImage];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kCreatedBy];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    [requestParameters setObject:[UtilityClass getUniqueIdentifierStringWithUserName:[[SettingsClass sharedObject]loggedInUser].userName] forKey:kUniqueImageIdentifier];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
     [[PSDataPersistenceManager sharedManager] saveRepairPictureInfo:requestParameters];
    
}


-(void) onPictureSave:(NSNotification*) notification
{
    [self hideActivityIndicator];
    PropertyPicture * picture=notification.object;
    if (self.isJobComplete)
    {
        if ([self.jobDataListObj.jobStatus isEqualToString:kJobStatusComplete])
        {
            [[PSDataUpdateManager sharedManager]markJobComplete:self.jobDataListObj withUpdatedValues:self.repairCompletionDictionary];
        }
    }
    else
    {
        if ([self.jobDataListObj.jobStatus isEqualToString:kJobStatusNotStarted] || [self.jobDataListObj.jobStatus isEqualToString:kJobStatusAccepted]) {
            [self.jobDataListObj startJob];
        }
    }
    
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_SUCCESS")
                                                   message:LOC(@"KEY_ALERT_UPLOAD_SUCCESS_FAULT_PICTURE")
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
    [self setImage:nil];
    [self.saveButton setEnabled:NO];
}

-(void) onPictureSaveFailure:(NSNotification*) notification
{
    [self hideActivityIndicator];
    [self.saveButton setEnabled:YES];
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                   message:LOC(@"KEY_ALERT_UPLOAD_FAILED_FAULT_PICTURE")
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
 }


- (void) onJobStartNotificationReceive {
    
}

- (NSString *) getTitle
{
	AppointmentType appointmentType = [self.jobDataListObj.jobSheetToAppointment getType];
	
	NSString *type = nil;
	switch (appointmentType)
	{
		case AppointmentTypeDefect:
			type = @"Defect";
			break;
		default:
			type = @"Repair";
			break;
	}
	return type;
	
}

@end
