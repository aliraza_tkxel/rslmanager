//
//  PSTenantDetailViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCustomViewController.h"

@class PSTenantDetailCell;
@class PSBarButtonItem;
@class PSTenantAddressCell;
@class PSRiskVulnerabilityCell;

@interface PSTenantDetailViewController : PSCustomViewController <UITextFieldDelegate, UITextViewDelegate>
@property (weak,   nonatomic) Customer *tenant;
@property (weak,   nonatomic) IBOutlet PSTenantDetailCell *tenantDetailCell;
@property (weak,   nonatomic) IBOutlet PSTenantAddressCell *tenantAddressCell;
@property (weak,   nonatomic) IBOutlet PSRiskVulnerabilityCell *riskVulnerabilityCell;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) NSMutableArray *riskArray;
@property (strong, nonatomic) NSMutableArray *vulnerabilityArray;
@property (strong, nonatomic) NSMutableDictionary *updatedValues;
@property (strong, nonatomic) NSMutableDictionary *updatedTenant;
@property (strong, nonatomic) PSBarButtonItem *editButton;
@property (strong, nonatomic) PSBarButtonItem *saveButton;

@end
