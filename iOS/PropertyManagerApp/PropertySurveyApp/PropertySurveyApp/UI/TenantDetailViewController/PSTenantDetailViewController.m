//
//  PSTenantDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSTenantDetailViewController.h"
#import "PSTenantDetailCell.h"
#import "PSTenantAddressCell.h"
#import "PSBarButtonItem.h"
#import "PSPropertyListViewController.h"
#import "PSTextView.h"
#import "PSRiskVulnerabilityCell.h"
#import "PSMapsViewController.h"

#define kNumberOfSections 7
#define kSectionTenantName 0
#define kSectionTenantAddress 1
#define kSectionTenantEmail 2
#define kSectionTenantTelephone 3
#define kSectionTenantMobile 4
#define kSectionTenantRisk 5
#define kSectionTenantVulnerability 6


#define kSectionTenantNameRows 1
#define kSectionTenantAddressRows 1
#define kSectionTenantTelephoneRows 1
#define kSectionTenantMobileRows 1
#define kSectionTenantRiskRows 1

#define kSectionHeaderHeight 18

#define kAccomodationCellHeight 39

static NSString *tenantDetailCellIdentifier = @"tenantDetailCellIdentifier";
static NSString *tenantAddressCellIdentifier = @"tenantAddressCellIdentifier";
static NSString *riskVulCellIdentifier = @"tenantRiskVulCellIdentifier";

@interface PSTenantDetailViewController ()
{
    @private
    PSTextView *_textView;
    NSString *_riskNotes;
    NSString *_email;
    NSString *_phoneNumber;
    NSString *_mobileNumber;
}

@end

@implementation PSTenantDetailViewController

@synthesize headerViewArray;
@synthesize tenant;
@synthesize tenantDetailCell;
@synthesize address;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.headerViewArray = [NSMutableArray array];
    [self loadSectionHeader];
    [self loadNavigationonBarItems];
    [self loadDefaultValues];
    self.isEditing = NO;
    self.riskArray =  [NSMutableArray arrayWithArray:[self.tenant.customerToCustomerRiskData allObjects]];
    self.vulnerabilityArray = [NSMutableArray arrayWithArray:[self.tenant.customerToCustomerVulunarityData allObjects]];
    self.updatedValues = [NSMutableDictionary dictionary];
    self.updatedTenant = [NSMutableDictionary dictionary];
    _riskNotes = nil;//self.tenant.customerToCustomerVulunarityData.vulCatDesc;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTenantInfoUpdateSuccessNotificationReceive) name:kTenantInfoUpdateSuccessNotification object:nil];
    
    NSArray *arr = [NSArray arrayWithArray:[self.tenant.customerToCustomerRiskData allObjects]];
    CLS_LOG(@"%@",arr);
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [self registerKeyboardNotifications];
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deregisterKeyboardNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kTenantInfoUpdateSuccessNotification object:nil];
}

#pragma mark - Notifications
- (void) onTenantInfoUpdateSuccessNotificationReceive
{
    [self.tableView reloadData];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.editButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                             style:PSBarButtonItemStyleEdit
                                                            target:self
                                                            action:@selector(onClickEditButton)];
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    if (self.isEditing) {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
        [self.tableView reloadData];
    }
    
    else {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.editButton, nil]];
    }
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    [self setTitle:[self.tenant fullName]];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickEditButton
{
    self.isEditing = YES;
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    [self.tableView reloadData];
    
}

- (void) onClickSaveButton
{
    self.isEditing = NO;
    self.isDispatchReady = YES;
    [self.view endEditing:YES];
    [self validateForm];
    //[self dispatchDataUpdates];
    //[self.tableView reloadData];
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 1;
    
    if (section == kSectionTenantRisk) {
        if ([self.riskArray count] != 0) {
            numberOfRows = [self.riskArray count];
        }
    }
    else if (section == kSectionTenantVulnerability)
    {
        if ([self.vulnerabilityArray count] != 0) {
            numberOfRows = [self.vulnerabilityArray count];
        }
    }
    return numberOfRows;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kSectionHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat rowHeight = 0;
    
    if (indexPath.section == kSectionTenantAddress && indexPath.row == 0)
    {
        rowHeight = 70;
    }
    else if (indexPath.section == kSectionTenantRisk || indexPath.section == kSectionTenantVulnerability)
    {
        rowHeight = 48;
    }
    else
    {
        rowHeight = 30;
    }
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.section == kSectionTenantRisk || indexPath.section == kSectionTenantVulnerability)
    {
        PSRiskVulnerabilityCell *_cell = [self createCellWithIdentifier:riskVulCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    
    
    
    else if (indexPath.section == kSectionTenantAddress)
    {
        PSTenantAddressCell *_cell = [self createCellWithIdentifier:tenantAddressCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    else
    {
        PSTenantDetailCell *_cell = [self createCellWithIdentifier:tenantDetailCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
	if([identifier isEqualToString:tenantDetailCellIdentifier])
	{
        PSTenantDetailCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:tenantDetailCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSTenantDetailCell" owner:self options:nil];
            _cell = self.tenantDetailCell;
            self.tenantDetailCell = nil;
        }
		return _cell;
	}
    else if ([identifier isEqualToString:tenantAddressCellIdentifier])
    {
        PSTenantAddressCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:tenantAddressCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSTenantAddressCell" owner:self options:nil];
            _cell = self.tenantAddressCell;
            self.tenantAddressCell = nil;
        }
		return _cell;
    }
	else if([identifier isEqualToString:riskVulCellIdentifier])
	{
        
        PSRiskVulnerabilityCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:riskVulCellIdentifier];
        if (_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSRiskVulnerabilityCell" owner:self options:nil];
            _cell = self.riskVulnerabilityCell;
            self.riskVulnerabilityCell = nil;
        }
		return _cell;
	}
	return nil;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionTenantName)
    {
        if(!isEmpty( self.tenant.fullName))
        {
            PSTenantDetailCell *_cell = (PSTenantDetailCell *)*cell;
            _cell.lblCellDetail.text = [self.tenant fullName];
            [_cell.lblCellDetail setHidden:YES];
            _cell.txtDetail.text = [self.tenant fullName];
            [_cell.txtDetail setUserInteractionEnabled:NO];
        }
    }
    else if (indexPath.section == kSectionTenantAddress)
    {
        PSTenantAddressCell *_cell = (PSTenantAddressCell *)*cell;
        if(!isEmpty(self.address))
        {
            [_cell.btnEditAddress addTarget:self action:@selector(onClickEditAddressButton:) forControlEvents:UIControlEventTouchUpInside];
            [_cell.lblCellDetail setText:self.address];
            [_cell.lblCellDetail setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
            [_cell.btnEditAddress setHidden:YES];
            
        }
    }
    else if (indexPath.section == kSectionTenantEmail)
    {
        PSTenantDetailCell *_cell = (PSTenantDetailCell *)*cell;
        if (!isEmpty(_email))
        {
            _cell.lblCellDetail.text = _email;
            [_cell.lblCellDetail setHidden:YES];
            _cell.txtDetail.text = self.tenant.email;
            [_cell.txtDetail setUserInteractionEnabled:NO];
        }
    }
    else if (indexPath.section == kSectionTenantTelephone)
    {
        PSTenantDetailCell *_cell = (PSTenantDetailCell *)*cell;
        if(!isEmpty(_phoneNumber))
        {
            _cell.lblCellDetail.text = _phoneNumber;
            [_cell.lblCellDetail setHidden:YES];
            _cell.txtDetail.text = self.tenant.telephone;
            [_cell.txtDetail setUserInteractionEnabled:NO];
        }
        
        [_cell.txtDetail setKeyboardType:UIKeyboardTypePhonePad];
    }
    else if (indexPath.section == kSectionTenantMobile)
    {
        PSTenantDetailCell *_cell = (PSTenantDetailCell *)*cell;
        if(!isEmpty(_mobileNumber))
        {
            _cell.txtDetail.text = _mobileNumber;
            _cell.txtDetail.hidden = NO;
            [_cell.txtDetail setUserInteractionEnabled:NO];
        }
        
        [_cell.txtDetail setKeyboardType:UIKeyboardTypePhonePad];
    }
    else if (indexPath.section == kSectionTenantRisk)
    {
        PSRiskVulnerabilityCell *_cell = (PSRiskVulnerabilityCell *)*cell;
        [_cell.lblCategory setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblSubCategory setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        
        if ([self.riskArray count] == 0)
        {
            [_cell.lblCategory setText:LOC(@"KEY_STRING_NONE")];
        }
        
        else
        {
            CustomerRiskData *riskData = [self.riskArray objectAtIndex:indexPath.row];
            
            if (!isEmpty(riskData)) {
                [_cell.lblCategory setText:riskData.riskCatDesc];
                [_cell.lblSubCategory setText:riskData.riskSubCatDesc];
            }
        }
    }
    
    else if (indexPath.section == kSectionTenantVulnerability)
    {
        PSRiskVulnerabilityCell *_cell = (PSRiskVulnerabilityCell *)*cell;
        [_cell.lblCategory setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblSubCategory setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.imgSeperator setHidden:YES];
         CustomerVulnerabilityData *vulnerabilityData = [self.vulnerabilityArray count] ?[self.vulnerabilityArray objectAtIndex:indexPath.row]:nil;
        if (!isEmpty(vulnerabilityData.vulCatDesc) && !isEmpty(vulnerabilityData.vulSubCatDesc)) {
            _cell.lblCategory.text = vulnerabilityData.vulCatDesc;
            _cell.lblSubCategory.text = vulnerabilityData.vulSubCatDesc;
        }
        else
        {
            _cell.lblCategory.text = LOC(@"KEY_STRING_NONE");
            _cell.imgSeperator.hidden = NO;
        }
        
        if (indexPath.row == [self.vulnerabilityArray count] -1) {
            _cell.imgSeperator.hidden = NO;
        }
    }
    
    //Handlle Editing Scnerios
    if([*cell isKindOfClass:[PSTenantDetailCell class]])
    {
        PSTenantDetailCell *_cell = (PSTenantDetailCell *)*cell;
        if (self.isEditing)
        {
            if (indexPath.section != kSectionTenantAddress && indexPath.section != kSectionTenantName)
            {
    
                if (!isEmpty(_cell.lblCellDetail.text))
                {
                    _cell.txtDetail.text = [_cell.lblCellDetail.text trimString];
                }
                _cell.txtDetail.hidden = NO;
                _cell.lblCellDetail.hidden = YES;
                [self.tableView setScrollEnabled:YES];
                [_cell.txtDetail setUserInteractionEnabled:YES];
            }
        }
        else
        {
            if (indexPath.section != kSectionTenantAddress && indexPath.section != kSectionTenantName)
            {
                if (!isEmpty(_cell.txtDetail.text))
                {
                    _cell.lblCellDetail.text = _cell.txtDetail.text;
                }
                else
                {
                    _cell.lblCellDetail.text = @"";
                }
                _cell.lblCellDetail.hidden = YES;
                [_cell.txtDetail setUserInteractionEnabled:NO];
                [self.tableView setScrollEnabled:YES];
            }
        }
        _cell.txtDetail.tag = indexPath.section;
        _cell.txtDetail.delegate = self;
        if (indexPath.section != kSectionTenantAddress) {
            _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        _cell.lblCellDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.txtDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionTenantAddress)
    {
        PSMapsViewController *mapViewController = [[PSMapsViewController alloc] initWithNibName:@"PSMapsViewController" bundle:[NSBundle mainBundle]];
        [mapViewController setAddress:[self.tenant.customerToAppointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull]];
        [mapViewController setNavigationTitle:[self.tenant.customerToAppointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
        [self.navigationController pushViewController:mapViewController animated:YES];
    }
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    NSString *textFieldText = ((UITextField *)self.selectedControl).text;

    if (self.selectedControl.tag == kSectionTenantEmail) {
        
        _email = textFieldText;
    }
    
    else if (self.selectedControl.tag == kSectionTenantTelephone)
    {
        _phoneNumber = textFieldText;
    }
    
    else if (self.selectedControl.tag == kSectionTenantMobile)
    {
        _mobileNumber = textFieldText;
    }
    
    if (self.isDispatchReady)
    {
        [self validateForm];
    }
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL textFieldShouldReturn = YES;
    NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
    switch (textField.tag) {
        case kSectionTenantMobile:
            if ([stringAfterReplacement length] > 20)
            {
                textFieldShouldReturn = NO;
            }
            break;
        case kSectionTenantTelephone:
            if ([stringAfterReplacement length] > 20)
            {
                textFieldShouldReturn = NO;
            }
            break;
        default:
            break;
    }
    return textFieldShouldReturn;;
}


#pragma mark - UITextView Delegate Methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    //self.tenant.customerToCustomerVulunarityData.vulCatDesc = _riskNotes;
    NSManagedObjectID *objectId = self.tenant.objectID;
    if(!_updatedTenant)
    {
        self.updatedTenant = [NSMutableDictionary dictionary];
    }
    [self.updatedValues setObject:((UITextView *)self.selectedControl).text forKey:kCustomerRiskData];
    if (!isEmpty(self.updatedValues))
    {
        [self.updatedTenant setObject:self.updatedValues forKey:objectId];
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
  //  CLS_LOG(@"textViewDidChange");
    _riskNotes = textView.text;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}



#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignAllResponders];
}

#pragma mark - Scroll Text Field Methods
- (void)scrollToRectOfSelectedControl {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.selectedControl.tag] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}


#pragma mark - Dispatch Data Updates
- (void) dispatchDataUpdates
{
    NSManagedObjectID *objectId = self.tenant.objectID;
    [self.updatedValues setObject:isEmpty(_email)?[NSNull null]:_email forKey:kCustomerEmail];
    [self.updatedValues setObject:isEmpty(_phoneNumber)?[NSNull null]:_phoneNumber forKey:kCustomerTelephone];
    [self.updatedValues setObject:isEmpty(_mobileNumber)?[NSNull null]:_mobileNumber forKey:kCustomerMobile];
    
    if (!isEmpty(self.updatedValues))
    {
        [self.updatedTenant setObject:self.updatedValues forKey:objectId];
    }
    
    @synchronized(self)
    {
        if(!isEmpty(self.updatedTenant))
        {
            self.isDispatchReady = NO;
            [[PSDataUpdateManager sharedManager] updateTenant:self.updatedTenant];
        }
         [self setRightBarButtonItems:[NSArray arrayWithObjects:self.editButton, nil]];
        [self.tableView reloadData];
    }
}


#pragma mark - Edit Address Button Selector
- (IBAction)onClickEditAddressButton:(id)sender
{
    PSPropertyListViewController *propertyListViewViewConroller = [[PSPropertyListViewController alloc] initWithNibName:@"PSPropertyListViewController" bundle:[NSBundle mainBundle]];
    
    [self.navigationController pushViewController:propertyListViewViewConroller animated:YES];
}

#pragma mark - Methods

- (void) loadSectionHeader
{
    NSArray *sectionTitles = @[LOC(@"KEY_STRING_TENANT_NAME"),LOC(@"KEY_STRING_ADDRESS"),LOC(@"KEY_STRING_EMAIL"), LOC(@"KEY_STRING_TELEPHONE"),LOC(@"KEY_STRING_MOBILE"),LOC(@"KEY_STRING_RISK"), LOC(@"KEY_STRING_VULNERABILITY")];
    [self loadSectionHeaderViews:sectionTitles headerViews:self.headerViewArray];
}

- (void) validateForm
{
    if (self.isDispatchReady)
    {
        BOOL isEmailValid = NO;
        if (!isEmpty(_email))
        {
            isEmailValid = [UtilityClass isEmailValid:_email];
        }
        else
        {
            isEmailValid = YES;
        }
        BOOL isTelephoneNumberValid = YES;
        if (!isEmpty(_phoneNumber))
        {
            isTelephoneNumberValid = [UtilityClass isValidNumber:_phoneNumber];
        }
        else
        {
            isTelephoneNumberValid = YES;
        }
        BOOL isMobileNumberValid = YES;
        if (!isEmpty(_mobileNumber))
        {
            isMobileNumberValid = [UtilityClass isValidNumber:_mobileNumber];
        }
        
        else
        {
            isMobileNumberValid = YES;
        }
        BOOL isFormValid = YES;
        NSString *alertMessage = nil;
        if (!isEmailValid)
        {
            alertMessage = LOC(@"KEY_ALERT_INVALID_EMAIL");
            isFormValid = NO;
            
        }
        
        else if (!isTelephoneNumberValid)
        {
            alertMessage = LOC(@"KEY_ALERT_INVALID_PHONE_NUMBER");
            isFormValid = NO;
        }
        
        else if (!isMobileNumberValid)
        {
            alertMessage = LOC(@"KEY_ALERT_INVALID_MOBILE_NUMBER");
            isFormValid = NO;
        }
        
        if (isFormValid)
        {
            if (self.isDispatchReady)
            {
                [self dispatchDataUpdates];
            }
        }
        else
        {
            self.isDispatchReady = NO;
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                           message:alertMessage
                                                          delegate:nil
                                                 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                 otherButtonTitles:nil,nil];
            
            [alert show];
        }
    }
}

- (void) loadDefaultValues
{
    _email = isEmpty(self.tenant.email)?nil:self.tenant.email;
    _phoneNumber = isEmpty(self.tenant.telephone)?nil:self.tenant.telephone;
    _mobileNumber = isEmpty(self.tenant.mobile)?nil:self.tenant.mobile;
}
@end
