//
//  PSTenantDetailCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTenantDetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCellDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgLine;
@property (strong, nonatomic) IBOutlet UITextField *txtDetail;
@property (strong, nonatomic) IBOutlet UIButton *btnEditAddress;
@property (strong, nonatomic) IBOutlet UITextView *txtViewRisk;

- (IBAction)resignFirstResponder:(id)sender;
@end
