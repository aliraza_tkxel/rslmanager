//
//  PSTenantAddressCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 19/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTenantAddressCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCellDetail;
@property (strong, nonatomic) IBOutlet UIButton *btnEditAddress;

@end
