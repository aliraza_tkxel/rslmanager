//
//  PSAddOilDefectsViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAddOilDefectsViewController.h"
#import "PSOilPhotographsViewController.h"


#define kTotalSections  1
#define kTotalRows  22

#define kRowDefectCategory  0
#define kRowDefectDate  1
#define kRowIsDefectIdentified  2
#define kRowDefectNotes 3
#define kRowIsRemedialActionTaken   4
#define kRowRemedialActionNotes 5
#define kRowIsAdviceNoteIssued  6
#define kRowWarningNoteSerialNumber 7
#define kRowSerialNumber    8
#define kRowWarningTagFixed 9
#define kRowIsDisconnected  10
#define kRowIsPartsRequired 11
#define kRowIsPartsOrdered  12
#define kRowPartsOrderedBy 13
#define kRowPartsDueDate 14
#define kRowPartsDescription    15
#define kRowPartsLocation 16
#define kRowIsTwoPersonsJob 17
#define kRowReasonForTwoPersonsJob  18
#define kRowDuration    19
#define kRowDefectPriority  20
#define kRowDefectTrade 21



@interface PSAddOilDefectsViewController ()

@end

@implementation PSAddOilDefectsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [self registerCells];
    [self initDefaultValues];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [self refreshPhotosCount];
}
#pragma mark - Init methods

-(void) initData{
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 44.0;
    _tblView.allowsSelection = NO;
    [_tblView reloadData];
}

-(void) initDefaultValues{
    if(!isEmpty(_selectedDefect)){
        _defectCategory = _selectedDefect.defectCategory;
        _defectDate = !isEmpty(_selectedDefect.defectDate)?_selectedDefect.defectDate:[NSDate date];
        _isDefectIdentified = !isEmpty(_selectedDefect.isDefectIdentified)?[_selectedDefect.isDefectIdentified boolValue]:NO;
        _defectNotes = !isEmpty(_selectedDefect.defectNotes)?_selectedDefect.defectNotes:@"";
        _isRemedialActionTaken = !isEmpty(_selectedDefect.isRemedialActionTaken)?[_selectedDefect.isRemedialActionTaken boolValue]:NO;
        _remedialActionNotes = !isEmpty(_selectedDefect.remedialActionNotes)?_selectedDefect.remedialActionNotes:@"";
        _isAdviceNoteIssued = !isEmpty(_selectedDefect.isAdviceNoteIssued)?[_selectedDefect.isAdviceNoteIssued boolValue]:NO;
        _warningNoteSerialNo = !isEmpty(_selectedDefect.warningNoteSerialNo)?_selectedDefect.warningNoteSerialNo:@"";
        _serialNumber = !isEmpty(_selectedDefect.serialNumber)?_selectedDefect.serialNumber:@"";
        _warningTagFixed = !isEmpty(_selectedDefect.warningTagFixed)?[_selectedDefect.warningTagFixed boolValue]:NO;
         _isDisconnected = !isEmpty(_selectedDefect.isDisconnected)?[_selectedDefect.isDisconnected boolValue]:NO;
         _isPartsRequired = !isEmpty(_selectedDefect.isPartsRequired)?[_selectedDefect.isPartsRequired boolValue]:NO;
        _isPartsOrdered = !isEmpty(_selectedDefect.isPartsOrdered)?[_selectedDefect.isPartsOrdered boolValue]:NO;
        _partsOrderedBy = _selectedDefect.partsOrderedBy;
        _partsOrderedByEntity = !isEmpty(_partsOrderedBy)?[self getEmployeeForId:_partsOrderedBy]:nil;
        _partsDueDate = !isEmpty(_selectedDefect.partsDueDate)?_selectedDefect.partsDueDate:[NSDate date];
        _partsDescription = !isEmpty(_selectedDefect.partsDescription)?_selectedDefect.partsDescription:@"";
        _partsLocation = !isEmpty(_selectedDefect.partsLocation)?_selectedDefect.partsLocation:@"";
        _isTwoPersonsJob = !isEmpty(_selectedDefect.isTwoPersonsJob)?[_selectedDefect.isTwoPersonsJob boolValue]:NO;
        _reasonForTwoPerson = !isEmpty(_selectedDefect.reasonForTwoPerson)?_selectedDefect.reasonForTwoPerson:@"";
        _duration = _selectedDefect.duration;
        _defectPriority = _selectedDefect.defectToDefectPriority;
        _defectTrade = _selectedDefect.defectToTrade;
        
    }
    else{
        _defectDate = [NSDate date];
        _isDefectIdentified = NO;
        _defectNotes = @"";
        _isRemedialActionTaken = NO;
        _remedialActionNotes = @"";
        _isAdviceNoteIssued = NO;
        _warningNoteSerialNo = @"";
        _serialNumber = @"";
        _warningTagFixed = NO;
        _isDisconnected = NO;
        _isPartsRequired = NO;
        _isPartsOrdered = NO;
        _partsDueDate = [NSDate date];
        _partsDescription = @"";
        _partsLocation = @"";
        _isTwoPersonsJob = NO;
        _reasonForTwoPerson = @"";
        _duration = [NSNumber numberWithInt:1];
    }
}

-(Employee *) getEmployeeForId:(NSNumber *)employeeId{
    Employee *type;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.employeeId = %@", employeeId];
    NSArray *filteredArray = [[[PSApplianceManager sharedManager] fetchAllOperatives:kEmployeeName] filteredArrayUsingPredicate:predicate];
    if([filteredArray count]>0){
        type = [filteredArray objectAtIndex:0];
    }
    return type;
}

-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSTextViewTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSPickerViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSPickerViewTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSTextfieldTableViewCell"];
}
-(NSUInteger) getPhotographsCount
{
    NSArray * photographsList = !isEmpty(_selectedDefect)?[_selectedDefect.defectPictures allObjects]:@[];
    return !isEmpty(photographsList)?[photographsList count]:0;
}


-(void) refreshPhotosCount{
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    if(!isEmpty(self.cameraButton)){
        [self.cameraButton reloadBadge:badgeText];
    }
    
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSaveBoiler = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                style:PSBarButtonItemStyleDefault
                                                               target:self
                                                               action:@selector(btnSaveTapped)];
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    
    self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                               style:PSBarButtonItemStyleCamera
                                                              target:self
                                                              action:@selector(onClickCameraButton)bagde:badgeText];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.cameraButton,self.btnSaveBoiler, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    if(!isEmpty(_selectedDefect)){
        [self setTitle:@"Edit Defect"];
    }
    else{
        [self setTitle:@"Add Defect"];
    }
    
}
#pragma mark - Actions

- (void) btnBackTapped
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

- (void)onClickCameraButton {
    [self.view endEditing:YES];
    CLS_LOG(@"Camera Button");
    if([self isValidData]){
        [self performSegueWithIdentifier:SEG_OIL_ADD_DEFECTS_TO_PHOTOGRAPHS sender:self];
    }
    
}

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update this heating type?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveDefect];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please fill all the mandatory fields"];
    }
    
    
    
}
-(void) saveAllData{
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    Defect *defectEntity;
    if(!isEmpty(_selectedDefect)){
        defectEntity = _selectedDefect;
    }
    else{
        defectEntity = [NSEntityDescription insertNewObjectForEntityForName:kDefect inManagedObjectContext:managedObjectContext];
        defectEntity.defectID = [NSNumber numberWithInteger:-1];
    }
    defectEntity.journalId = self.appointment.journalId;
    defectEntity.blockId = self.appointment.appointmentToProperty.blockId;
    defectEntity.defectDate = _defectDate;
    defectEntity.defectNotes = _defectNotes;
    defectEntity.duration = _duration;
    defectEntity.faultCategory = _defectCategory.categoryID;
    defectEntity.heatingId = _selectedBoiler.heatingId;
    defectEntity.isAdviceNoteIssued = [NSNumber numberWithBool:_isAdviceNoteIssued];
    defectEntity.isDefectIdentified = [NSNumber numberWithBool:_isDefectIdentified];
    defectEntity.isDisconnected = [NSNumber numberWithBool:_isDisconnected];
    defectEntity.isPartsOrdered = [NSNumber numberWithBool:_isPartsOrdered];
    defectEntity.isPartsRequired = [NSNumber numberWithBool:_isPartsRequired];
    defectEntity.isRemedialActionTaken = [NSNumber numberWithBool:_isRemedialActionTaken];
    defectEntity.isTwoPersonsJob = [NSNumber numberWithBool:_isTwoPersonsJob];
    defectEntity.partsDescription = _partsDescription;
    defectEntity.partsDueDate = _partsDueDate;
    defectEntity.partsLocation = _partsLocation;
    defectEntity.partsOrderedBy = _partsOrderedBy;
    defectEntity.priorityId = _defectPriority.priorityId;
    defectEntity.propertyId = self.appointment.appointmentToProperty.propertyId;
    defectEntity.reasonForTwoPerson = _reasonForTwoPerson;
    defectEntity.remedialActionNotes = _remedialActionNotes;
    defectEntity.schemeId = self.appointment.appointmentToProperty.schemeId;
    defectEntity.serialNumber = _serialNumber;
    defectEntity.tradeId = _defectTrade.tradeId;
    defectEntity.warningNoteSerialNo = _warningNoteSerialNo;
    defectEntity.warningTagFixed = [NSNumber numberWithBool:_warningTagFixed];
    
    defectEntity.defectCategory = _defectCategory;
    defectEntity.defectToDefectPriority = _defectPriority;
    defectEntity.defectToTrade = _defectTrade;
    defectEntity.defectToOilHeating = _selectedBoiler;
    [_selectedBoiler addOilHeatingToDefectObject:defectEntity];
    self.appointment.isModified = [NSNumber numberWithBool:YES];
    self.appointment.isSurveyChanged = [NSNumber numberWithBool:YES];
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    _selectedDefect = defectEntity;
}
-(void) saveDefect{
    [self saveAllData];
    [self popOrCloseViewController];
}

-(BOOL) isValidData{
    BOOL isValid = YES;
    
    NSString *message= @"";
    
    if (isEmpty(_defectCategory))
    {
        message = LOC(@"KEY_ALERT_SELECT_DEFECT_CATEGORY");
        isValid = NO;
    }
    else if (isEmpty(_defectDate))
    {
        message = LOC(@"KEY_ALERT_SELECT_DEFECT_DATE");
        isValid = NO;
    }
    else if (isEmpty(_defectNotes))
    {
        message = LOC(@"KEY_ALERT_ENTER_DEFECT_NOTES");
        isValid = NO;
    }
    else if (_isRemedialActionTaken==YES && isEmpty(_remedialActionNotes))
    {
        message = LOC(@"KEY_ALERT_ENTER_REMEDIAL_NOTES");
        isValid = NO;
    }
    else if(_isAdviceNoteIssued == YES && isEmpty(_warningNoteSerialNo) == TRUE)
    {
        message = LOC(@"KEY_ALERT_SELECT_WARNINGNOTE");
        isValid = NO;
    }
    else if (_isPartsRequired)
    {
        if (isEmpty(_partsOrderedBy))
        {
            message = LOC(@"KEY_ALERT_SELECT_PARTS_ORDERED_BY");
            isValid = NO;
        }
        else if (isEmpty(_partsDueDate))
        {
            message = LOC(@"KEY_ALERT_SELECT_PARTS_DUE");
            isValid = NO;
        }
        else if (isEmpty(_partsDescription))
        {
            message = LOC(@"KEY_ALERT_ENTER_PARTS_DESCRIPTION");
            isValid = NO;
        }
        else if (isEmpty(_partsLocation))
        {
            message = LOC(@"KEY_ALERT_ENTER_PARTS_LOCATION");
            isValid = NO;
        }
    }
    else if (_isTwoPersonsJob == YES && isEmpty(_reasonForTwoPerson))
    {
        message = LOC(@"KEY_ALERT_ENTER_REASON_TWO_PERSON_JOB");
        isValid = NO;
    }
    if (isValid==NO)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                       message:message
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
    }
    return isValid;
}

#pragma mark - Config Methods

-(NSString *) getHeaderForRow:(NSInteger) row andSection:(NSInteger) section{
    NSString *header = @"";
    switch (row) {
        case kRowDefectCategory:
            header = @"DEFECT CATEGORY";
            break;
        case kRowDefectDate:
            header = @"DATE";
            break;
        case kRowIsDefectIdentified:
            header = @"DEFECT IDENTIFIED";
            break;
        case kRowDefectNotes:
            header = @"DEFECT NOTES";
            break;
        case kRowIsRemedialActionTaken:
            header = @"REMEDIAL ACTION TAKEN";
            break;
        case kRowRemedialActionNotes:
            header = @"REMEDIAL ACTION NOTES";
            break;
        case kRowIsAdviceNoteIssued:
            header = @"WARNING/ADVICE NOTE ISSUED";
            break;
        case kRowWarningNoteSerialNumber:
            header = @"WARNING/ADVICE NOTE SERIAL NUMBER";
            break;
        case kRowSerialNumber:
            header = @"SERIAL NUMBER";
            break;
        case kRowWarningTagFixed:
            header = @"WARNING TAG/STICKER FIXED";
            break;
        case kRowIsDisconnected:
            header = @"DISCONNECTED";
            break;
        case kRowIsPartsRequired:
            header = @"PARTS REQUIRED";
            break;
        case kRowIsPartsOrdered:
            header = @"PARTS ORDERED";
            break;
        case kRowPartsOrderedBy:
            header = @"PARTS ORDERED BY";
            break;
        case kRowPartsDueDate:
            header = @"PARTS DUE DATE";
            break;
        case kRowPartsDescription:
            header = @"PARTS DESCRIPTION";
            break;
        case kRowPartsLocation:
            header = @"PARTS LOCATION";
            break;
        case kRowIsTwoPersonsJob:
            header = @"TWO PERSON JOB";
            break;
        case kRowReasonForTwoPersonsJob:
            header = @"REASON FOR TWO PERSON JOB";
            break;
        case kRowDuration:
            header = @"ESTIMATED DURATION: HOURS";
            break;
        case kRowDefectPriority:
            header = @"PRIORITY";
            break;
        case kRowDefectTrade:
            header = @"TRADE";
            break;
        default:
            break;
    }
    return header;
}

-(NSString *) getValueForRow:(NSInteger) row andSection:(NSInteger) section{
    NSString *header = @"";
    switch (row) {
        case kRowDefectCategory:
            header = !isEmpty(_defectCategory)?_defectCategory.categoryDescription:@"";
            break;
        case kRowDefectDate:
            header = [UtilityClass stringFromDate:_defectDate dateFormat:kDateTimeStyle8];
            break;
        case kRowIsDefectIdentified:
            header = (_isDefectIdentified)?@"YES":@"NO";
            break;
        case kRowDefectNotes:
            header = !isEmpty(_defectNotes)?_defectNotes:@"";
            break;
        case kRowIsRemedialActionTaken:
            header = (_isRemedialActionTaken)?@"YES":@"NO";
            break;
        case kRowRemedialActionNotes:
            header = !isEmpty(_remedialActionNotes)?_remedialActionNotes:@"";
            break;
        case kRowIsAdviceNoteIssued:
            header = (_isAdviceNoteIssued)?@"YES":@"NO";
            break;
        case kRowWarningNoteSerialNumber:
            header = !isEmpty(_warningNoteSerialNo)?_warningNoteSerialNo:@"";
            break;
        case kRowSerialNumber:
            header = !isEmpty(_serialNumber)?_serialNumber:@"";
            break;
        case kRowWarningTagFixed:
            header = (_warningTagFixed)?@"YES":@"NO";
            break;
        case kRowIsDisconnected:
            header = (_isDisconnected)?@"YES":@"NO";
            break;
        case kRowIsPartsRequired:
            header = (_isPartsRequired)?@"YES":@"NO";
            break;
        case kRowIsPartsOrdered:
            header = (_isPartsOrdered)?@"YES":@"NO";
            break;
        case kRowPartsOrderedBy:
            header = !isEmpty(_partsOrderedByEntity)?_partsOrderedByEntity.employeeName:@"";
            break;
        case kRowPartsDueDate:
            header = [UtilityClass stringFromDate:_partsDueDate dateFormat:kDateTimeStyle8];
            break;
        case kRowPartsDescription:
            header = !isEmpty(_partsDescription)?_partsDescription:@"";
            break;
        case kRowPartsLocation:
            header = !isEmpty(_partsLocation)?_partsLocation:@"";
            break;
        case kRowIsTwoPersonsJob:
            header = (_isTwoPersonsJob)?@"YES":@"NO";
            break;
        case kRowReasonForTwoPersonsJob:
            header = !isEmpty(_reasonForTwoPerson)?_reasonForTwoPerson:@"";
            break;
        case kRowDuration:
            header =!isEmpty(_duration)?[_duration stringValue]:@"";
            break;
        case kRowDefectPriority:
            header =!isEmpty(_defectPriority)?_defectPriority.priority:@"";
            break;
        case kRowDefectTrade:
            header = !isEmpty(_defectTrade)?_defectTrade.trade:@"";;
            break;
        default:
            break;
    }
    return header;
}

-(PSAltFuelDynamicCellsConfiguartion*) getCellConfigurationForRow:(NSInteger) row andSection:(NSInteger) section{
    PSAltFuelDynamicCellsConfiguartion * config = [[PSAltFuelDynamicCellsConfiguartion alloc] init];
    config.cellRowId = row;
    config.cellSectionId = section;
    config.inputFieldTitle = [self getHeaderForRow:row andSection:section];
    config.selectedValue = [self getValueForRow:row andSection:section];
    config.isNonMutableField = NO;
    config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeFreeText;
    config.appliedCellFieldType = PSAFSDynamicCellsFreeTextFieldTypeText;
    if(   row==kRowDefectCategory
       || row==kRowIsDefectIdentified
       || row==kRowIsRemedialActionTaken
       || row==kRowIsAdviceNoteIssued
       || row==kRowWarningTagFixed
       || row==kRowIsDisconnected
       || row==kRowIsPartsOrdered
       || row==kRowIsPartsRequired
       || row==kRowIsTwoPersonsJob
       || row==kRowPartsOrderedBy
       || row==kRowDuration
       || row==kRowDefectPriority
       || row==kRowDefectTrade){
        config.dropDownHeaderTitle = config.inputFieldTitle;
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDropDown;
        config.isNonMutableField = YES;
        
    }
    else if(row==kRowDefectDate || row==kRowPartsDueDate){
        config.dropDownHeaderTitle = config.inputFieldTitle;
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDate;
        config.appliedCellFieldType = PSAFSDynamicCellsFreeTextFieldTypeText;
        config.startingDate = !isEmpty(config.selectedValue)?[UtilityClass dateFromString:config.selectedValue dateFormat:kDateTimeStyle8]:[NSDate date];
        config.isNonMutableField = YES;
    }
    else if(row==kRowDefectNotes || row==kRowRemedialActionNotes || row==kRowPartsDescription
            || row==kRowReasonForTwoPersonsJob){
        config.isNonMutableField = NO;
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeTextView;
        config.appliedCellFieldType = PSAFSDynamicCellsFreeTextFieldTypeText;
    }
    return config;
}

#pragma mark - PickerRows
- (NSArray*) sortArray:(NSString*)sortDescriptorKey :(NSArray*) objArray
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortDescriptorKey ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    objArray = [objArray sortedArrayUsingDescriptors:sortDescriptors];
    return objArray;
}

- (NSArray *) getPickerDataForRow:(NSInteger) row
{
    NSArray *rowsData = [NSArray array];
    NSArray* boilerData;
    switch (row) {
        case kRowDefectCategory:
            boilerData = [self getDefectCategories];
            
            break;
        case kRowIsDefectIdentified:
        case kRowIsRemedialActionTaken:
        case kRowIsAdviceNoteIssued:
        case kRowWarningTagFixed:
        case kRowIsDisconnected:
        case kRowIsPartsOrdered:
        case kRowIsPartsRequired:
        case kRowIsTwoPersonsJob:
            boilerData = @[@"YES",@"NO"];
            break;
        case kRowPartsOrderedBy:
            boilerData = [[PSApplianceManager sharedManager] fetchAllOperatives:kEmployeeName];
            break;
        case kRowDuration:
            boilerData = @[@"1", @"2", @"3", @"4", @"5" , @"6", @"7", @"8", @"9"];
            break;
        case kRowDefectPriority:
            boilerData = [[PSApplianceManager sharedManager] fetchDefectPriorityList];
            boilerData = [self sortArray:kPriorityDesc :boilerData];
            break;
        case kRowDefectTrade:
            boilerData = [[PSApplianceManager sharedManager] fetchTradeList];
            boilerData = [self sortArray:kTradeDescription :boilerData];
            break;
        default:
            break;
    }
    if([boilerData count] > 0)
    {
        rowsData = [NSArray arrayWithArray:boilerData];
    }
    return rowsData;
}

-(NSArray *) getDefectCategories{
    NSString* remedialActionTaken = [@"Remedial Action Taken" lowercaseString];
    
    NSArray* defectCategories = [[PSApplianceManager sharedManager] fetchAllDefectCategories:kCategoryId];
    NSMutableArray *defCats = [NSMutableArray array];
    
    for (DefectCategory *defectCategory in defectCategories)
    {
        NSString * category = [defectCategory.categoryDescription lowercaseString];
        if (([category containsString:remedialActionTaken]) == NO)
        {
            [defCats addObject:defectCategory];
        }
    }
    return [NSArray arrayWithArray:defCats];
}


-(NSMutableArray *) createPresentablePickerDataForRow:(NSInteger) row{
    NSArray *objectsArray = [self getPickerDataForRow:row];
    NSMutableArray *presentableData = [[NSMutableArray alloc] init];
    if(row == kRowDefectCategory){
        for(DefectCategory *type in objectsArray){
            [presentableData addObject:type.categoryDescription];
        }
    }
    else  if(row == kRowPartsOrderedBy){
        for(Employee *type in objectsArray){
            [presentableData addObject:type.employeeName];
        }
    }
    else  if(row == kRowDefectPriority){
        for(DefectPriority *type in objectsArray){
            [presentableData addObject:type.priority];
        }
    }
    else  if(row == kRowDefectTrade){
        for(Trade *type in objectsArray){
            [presentableData addObject:type.trade];
        }
    }
    else{
        presentableData = [[NSMutableArray alloc] initWithArray:objectsArray];
    }
    return presentableData;
}

#pragma mark - UITableViewDelegates

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return kTotalSections;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return kTotalRows;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    PSAltFuelDynamicCellsConfiguartion *config = [self getCellConfigurationForRow:indexPath.row andSection:indexPath.section];
    if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeTextView)
    {
        PSAFSTextViewTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSTextViewTableViewCell class])];
        manualCell.baseConfig = config;
        manualCell.delegate = self;
        [manualCell initWithConfig];
        [manualCell setNeedsUpdateConstraints];
        [manualCell updateConstraintsIfNeeded];
        [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        cell = manualCell;
    }
    else if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeFreeText){
        PSAFSTextfieldTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSTextfieldTableViewCell class])];
        manualCell.baseConfig = config;
        manualCell.delegate = self;
        [manualCell initializeData];
        [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        cell = manualCell;
    }
    else if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeDropDown || config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeDate){
        PSAFSPickerViewTableViewCell *pickerCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSPickerViewTableViewCell class])];
        pickerCell.btnClearValue.hidden = YES;
        pickerCell.baseConfig = config;
        pickerCell.delegate = self;
        [pickerCell initializeData];
        [pickerCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        cell = pickerCell;
    }
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

#pragma mark - Delegates

-(void) setValue:(NSString *) value forRow:(NSInteger) row{
    switch (row) {
        case kRowDefectDate:
            _defectDate = [UtilityClass dateFromString:value dateFormat:kDateTimeStyle8];
            break;
        case kRowDefectNotes:
            _defectNotes = value;
            break;
        case kRowRemedialActionNotes:
            _remedialActionNotes = value;
            break;
        case kRowWarningNoteSerialNumber:
            _warningNoteSerialNo = value;
            break;
        case kRowSerialNumber:
            _serialNumber = value;
            break;
        case kRowPartsDescription:
            _partsDescription = value;
            break;
        case kRowPartsLocation:
            _partsLocation = value;
            break;
        case kRowPartsDueDate:
            _partsDueDate = [UtilityClass dateFromString:value dateFormat:kDateTimeStyle8];
            break;
        case kRowReasonForTwoPersonsJob:
            _reasonForTwoPerson = value;
            break;
        default:
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:0];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion *)config{
    [self setValue:config.selectedValue forRow:config.cellRowId];
}

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion *)config andIsForBoolValue:(BOOL)isForBoolVal{
    [self setValue:config.selectedValue forRow:config.cellRowId];
}

-(void) didClickShowDropDownFor:(PSAltFuelDynamicCellsConfiguartion *)config andSender:(id)sender{
    [self.view endEditing:YES];
    _pickerTag = config.cellRowId;
    _pickerArray = [[NSMutableArray alloc] initWithArray:[self createPresentablePickerDataForRow:config.cellRowId]];
    NSInteger initialIndex = [_pickerArray indexOfObject:config.selectedValue];
    if(initialIndex>[_pickerArray count]){
        initialIndex = 0;
    }
    [ActionSheetStringPicker showPickerWithTitle:config.dropDownHeaderTitle
                                            rows:_pickerArray
                                initialSelection:initialIndex
                                          target:self
                                   successAction:@selector(onOptionSelected:element:)
                                    cancelAction:@selector(onOptionPickerCancelled:)
                                          origin:sender];
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    NSString *value = [_pickerArray objectAtIndex:[selectedIndex integerValue]];
    [_pickerArray removeAllObjects];
    switch (_pickerTag) {
        case kRowDefectCategory:
            _defectCategory = [self getObjectForPickerTag:_pickerTag andSection:0 andValue:value];
            _defectCategoryId = _defectCategory.categoryID;
            break;
        case kRowIsDefectIdentified:
            _isDefectIdentified = [[self getObjectForPickerTag:_pickerTag andSection:0 andValue:value] boolValue];
            break;
        case kRowIsRemedialActionTaken:
            _isRemedialActionTaken = [[self getObjectForPickerTag:_pickerTag andSection:0 andValue:value] boolValue];
            break;
        case kRowIsAdviceNoteIssued:
            _isAdviceNoteIssued = [[self getObjectForPickerTag:_pickerTag andSection:0 andValue:value] boolValue];
            break;
        case kRowWarningTagFixed:
            _warningTagFixed = [[self getObjectForPickerTag:_pickerTag andSection:0 andValue:value] boolValue];
            break;
        case kRowIsDisconnected:
            _isDisconnected = [[self getObjectForPickerTag:_pickerTag andSection:0 andValue:value] boolValue];
            break;
        case kRowIsPartsOrdered:
            _isPartsOrdered = [[self getObjectForPickerTag:_pickerTag andSection:0 andValue:value] boolValue];
            break;
        case kRowIsPartsRequired:
            _isPartsRequired = [[self getObjectForPickerTag:_pickerTag andSection:0 andValue:value] boolValue];
            break;
        case kRowIsTwoPersonsJob:
            _isTwoPersonsJob = [[self getObjectForPickerTag:_pickerTag andSection:0 andValue:value] boolValue];
            break;
        case kRowPartsOrderedBy:
            _partsOrderedByEntity = [self getObjectForPickerTag:_pickerTag andSection:0 andValue:value];
            _partsOrderedBy = _partsOrderedByEntity.employeeId;
            break;
        case kRowDuration:
            _duration = [self getObjectForPickerTag:_pickerTag andSection:0 andValue:value];
            break;
        case kRowDefectPriority:
             _defectPriority = [self getObjectForPickerTag:_pickerTag andSection:0 andValue:value];
             _defectPriorityId = _defectPriority.priorityId;
            break;
        case kRowDefectTrade:
            _defectTrade =  [self getObjectForPickerTag:_pickerTag andSection:0 andValue:value];
            _defectTradeId = _defectTrade.tradeId;
            break;
        default:
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:_pickerTag inSection:0];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (void) onOptionPickerCancelled:(id)sender {
    CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

-(id) getObjectForPickerTag:(NSInteger)pickerTag andSection:(NSInteger) secton andValue:(NSString *) value{
    NSArray *dataArray = [self getPickerDataForRow:pickerTag];
    if(_pickerTag==kRowDefectCategory){
        DefectCategory *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.categoryDescription = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    else if(_pickerTag==kRowIsDefectIdentified||
            _pickerTag==kRowIsRemedialActionTaken||
            _pickerTag==kRowIsAdviceNoteIssued||
            _pickerTag==kRowWarningTagFixed||
            _pickerTag==kRowIsDisconnected||
            _pickerTag==kRowIsPartsOrdered||
            _pickerTag==kRowIsPartsRequired||
            _pickerTag==kRowIsTwoPersonsJob){
        BOOL valueBool = ([[value lowercaseString] isEqualToString:@"yes"])?YES:NO;
        return [NSNumber numberWithBool:valueBool];
    }
    else if(_pickerTag==kRowPartsOrderedBy){
        Employee *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.employeeName = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    else if(_pickerTag==kRowDuration){
        NSNumber *numeo = [NSNumber numberWithInteger:[value integerValue]];
        return numeo;
    }
    else if(_pickerTag==kRowDefectPriority){
        DefectPriority *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.priority = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    else if(_pickerTag==kRowDefectTrade){
        Trade *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.trade = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    return @"";
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([[segue identifier] isEqualToString:SEG_OIL_ADD_DEFECTS_TO_PHOTOGRAPHS]){
        [self saveAllData];
        PSOilPhotographsViewController *dest = (PSOilPhotographsViewController*)[segue destinationViewController];
        dest.appointment = self.appointment;
        dest.defect = self.selectedDefect;
        dest.altHeating = self.selectedBoiler;
        dest.rootVC = OilPhotoGridRootDefect;
    }
}


@end
