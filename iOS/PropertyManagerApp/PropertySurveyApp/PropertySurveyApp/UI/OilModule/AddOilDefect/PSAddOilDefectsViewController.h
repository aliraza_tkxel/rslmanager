//
//  PSAddOilDefectsViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSTextfieldTableViewCell.h"
#import "PSAFSPickerViewTableViewCell.h"
#import "PSAFSTextViewTableViewCell.h"
@interface PSAddOilDefectsViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property OilHeating *selectedBoiler;
@property Defect *selectedDefect;
@property (weak,   nonatomic) Appointment *appointment;
@property (weak,nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;

@property (strong,nonatomic) DefectCategory *defectCategory;
@property (strong,nonatomic) NSNumber*defectCategoryId;
@property (strong,nonatomic) NSDate *defectDate;
@property (nonatomic) BOOL isDefectIdentified;
@property (strong,nonatomic) NSString *defectNotes;
@property (nonatomic) BOOL isRemedialActionTaken;
@property (strong,nonatomic) NSString *remedialActionNotes;
@property (nonatomic) BOOL isAdviceNoteIssued;
@property (strong,nonatomic) NSString *warningNoteSerialNo;
@property (strong,nonatomic) NSString *serialNumber;
@property (nonatomic) BOOL warningTagFixed;
@property (nonatomic) BOOL isDisconnected;
@property (nonatomic) BOOL isPartsRequired;
@property (nonatomic) BOOL isPartsOrdered;
@property (strong,nonatomic) Employee *partsOrderedByEntity;
@property (strong,nonatomic) NSNumber *partsOrderedBy;
@property (strong,nonatomic) NSDate *partsDueDate;
@property (strong,nonatomic) NSString *partsDescription;
@property (strong,nonatomic) NSString *partsLocation;
@property (nonatomic) BOOL isTwoPersonsJob;
@property (strong,nonatomic) NSString *reasonForTwoPerson;
@property (strong,nonatomic) NSNumber *duration;
@property (strong,nonatomic) DefectPriority *defectPriority;
@property (strong,nonatomic) NSNumber*defectPriorityId;
@property (strong,nonatomic) Trade *defectTrade;
@property (strong,nonatomic) NSNumber*defectTradeId;
@property (strong, nonatomic) PSBarButtonItem *btnSaveBoiler;
@property(nonatomic,strong) PSBarButtonItem * cameraButton;
@property NSInteger pickerTag;
@property (strong,nonatomic) NSMutableArray *pickerArray;
@end
