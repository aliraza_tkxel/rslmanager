//
//  PSOilDefectsListViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"
#import "PSAddOilDefectsViewController.h"
#import "PSAlternateFuelServiceSurveyTableViewCell.h"
@interface PSOilDefectsListViewController :PSCustomViewController
@property (strong, nonatomic) PSBarButtonItem *addDefectButton;
@property (strong,   nonatomic) Appointment *appointment;
@property NSArray *defects;
@property BOOL noResultsFound;
@property OilHeating *selectedBoiler;
@property Defect *selectedDefect;
@end
