//
//  PSOilDefectsListViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSOilDefectsListViewController.h"
#define kBoilerDetailCellHeight 44
@interface PSOilDefectsListViewController ()

@end

@implementation PSOilDefectsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    [self registerNibsForTableView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    
    _defects = [NSArray array];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kDefectNotes
                                                 ascending:YES];
    NSArray *sortedArray = [[_selectedBoiler.oilHeatingToDefect allObjects] sortedArrayUsingDescriptors:@[sortDescriptor]];
    _defects = [[NSArray alloc] initWithArray:sortedArray] ;
    _selectedDefect = nil;
    [self.tableView reloadData];
}



#pragma mark - Init Methods
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    self.addDefectButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_ADD_DEFECT")
                                                                           style:PSBarButtonItemStyleAdd
                                                                          target:self
                                                                          action:@selector(onClickAddDefectButton)];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.addDefectButton, nil]];
    [self setTitle:@"Defects"];
    
}
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickAddDefectButton
{
    CLS_LOG(@"onClickAddDefectButton");
    _selectedDefect = nil;
    [self performSegueWithIdentifier:SEG_OIL_DEFECT_LIST_TO_ADD_DEFECT sender:self];
    
}

-(void)registerNibsForTableView
{
    NSString * nibName = NSStringFromClass([PSAlternateFuelServiceSurveyTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
    if (_noResultsFound)
    {
        rowHeight = 40;
    }
    else
    {
        rowHeight = kBoilerDetailCellHeight;
    }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    
    if (!isEmpty(_defects))
    {
        rows = [_defects count];
        _noResultsFound = NO;
    }
    else
    {
        rows = 1;
        _noResultsFound = YES;
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if(_noResultsFound)
    {
        static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
        UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
        if(_cell == nil)
        {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
        }
        _cell.textLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
        _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
        _cell.textLabel.text = @"No defects added";
        _cell.textLabel.textAlignment = PSTextAlignmentCenter;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        cell = _cell;
    }
    
    else
    {
        PSAlternateFuelServiceSurveyTableViewCell *_cell =[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAlternateFuelServiceSurveyTableViewCell class])];
        Defect *defect = [_defects objectAtIndex:indexPath.row];
        [_cell.lblItemName setText:defect.defectNotes];
        _cell.lblItemName.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblItemName.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.imgViewStatus.hidden = YES;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        
        cell = _cell;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedDefect = [_defects objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:SEG_OIL_DEFECT_LIST_TO_ADD_DEFECT sender:self];
}


#pragma mark - Navigation

//In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue destinationViewController] isKindOfClass:[PSAddOilDefectsViewController class]]){
        PSAddOilDefectsViewController *dest = [segue destinationViewController];
        dest.selectedBoiler = _selectedBoiler;
        dest.appointment = _appointment;
        dest.selectedDefect = _selectedDefect;
    }
}

@end
