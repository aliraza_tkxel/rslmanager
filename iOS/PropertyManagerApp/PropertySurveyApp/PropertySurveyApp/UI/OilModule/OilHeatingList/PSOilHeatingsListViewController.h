//
//  PSOilHeatingsListViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"

@interface PSOilHeatingsListViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong,   nonatomic) Appointment *appointment;
@property NSArray *heatings;
@property BOOL noResultsFound;
@property OilHeating *selectedBoiler;
@property OilHeatingListDestination destinationVC;
@end
