//
//  PSOilPhotographsViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PhotographCollectionViewCell.h"
#import "MWPhotoBrowser.h"
@interface PSOilPhotographsViewController : PSCustomViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIActionSheetDelegate>
@property(assign) int selectedIndex;
@property (strong, nonatomic) PSBarButtonItem *cameraButton;
@property (strong, nonatomic) Appointment *appointment;
@property (strong, nonatomic) OilHeating *altHeating;
@property (strong, nonatomic) Defect *defect;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,retain) NSMutableArray*  picturesDataSource;
@property OilPhotoGridRoot rootVC;
@property (assign, nonatomic) CameraViewImageTypeTag imageType;
@property (strong, nonatomic) NSNumber *itemId;
-(void) showCameraOfSourceType:(UIImagePickerControllerSourceType) type;

@end
