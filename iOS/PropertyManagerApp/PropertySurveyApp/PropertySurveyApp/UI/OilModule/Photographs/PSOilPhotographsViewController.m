//
//  PSOilPhotographsViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSOilPhotographsViewController.h"
#import "PSImagePickerController.h"
#import "PropertyPicture+JSON.h"
#import "AlternativeFuelPicture+JSON.h"
#import "DefectPicture+JSON.h"
static NSString *collectionViewCellIdentifier = @"collecitonViewCell";
static NSString *cameraButtonCellIdentifier = @"cameraButtonCell";
@interface PSOilPhotographsViewController ()

@end

@implementation PSOilPhotographsViewController

-(void) viewWillAppear:(BOOL)animated{
    [self registerNotifications];
    [self refreshDataSource];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initModule];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated{
    [self deregisterNotifications];
}

#pragma mark - Init

-(void) initModule{
    [self.view setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self loadNavigationonBarItems];
    [self.collectionView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self.collectionView registerClass:[PhotographCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellIdentifier];
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    [self refreshDataSource];
    [_collectionView reloadData];
}

-(void) refreshDataSource{
    _picturesDataSource  = [[NSMutableArray alloc] init];
    switch (_rootVC) {
        case OilPhotoGridRootProperty:
            [self populateDataSourceForProperty];
            break;
        case OilPhotoGridRootDefect:
            [self populateDataSourceForDefect];
            break;
        case OilPhotoGridRootFireServicingInspection:
            [self populateDataSourceForFireServicingInspection];
            break;
        default:
            break;
    }
    [_collectionView reloadData];
}

-(void) populateDataSourceForProperty{
    OilHeating *heating = [[_appointment.appointmentToProperty.propertyToOilHeatings allObjects] objectAtIndex:0];
    _picturesDataSource = [NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] propertyPicturesWithPropertyIdentifier:self.appointment.appointmentToProperty appointmentId:self.appointment.appointmentId itemId:@0]];
}

-(void) populateDataSourceForDefect{
    _picturesDataSource = [[NSMutableArray alloc] initWithArray:[_defect.defectPictures allObjects]];
}
-(void) populateDataSourceForFireServicingInspection{
    _picturesDataSource = [[NSMutableArray alloc] initWithArray:[_altHeating.oilHeatingToFireServiceInspection.fireServicingInspectionToPictures allObjects]];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    
    _cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                           style:PSBarButtonItemStyleCamera
                                                          target:self
                                                          action:@selector(onClickBtnCamera:)];
    
    [self setLeftBarButtonItems:@[backButton]];
    [self setRightBarButtonItems:@[_cameraButton]];
    [self setTitle:@"Photographs"];
}


#pragma mark - Actions


-(void) onClickBtnCamera: (id) sender{
    UIActionSheet * _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:          nil
                                                       otherButtonTitles:NSLocalizedString(@"Take a Photo", nil),
                                     NSLocalizedString(@"Choose from Library", nil),
                                     nil] ;
    
    _actionsSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    _actionsSheet.tag=1;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [_actionsSheet showFromBarButtonItem:sender animated:YES];
    } else {
        [_actionsSheet showInView:self.view];
    }
}

-(void) showCameraOfSourceType:(UIImagePickerControllerSourceType) type
{
    if ([UIImagePickerController isSourceTypeAvailable:type])
    {
        PSImagePickerController *imagePicker = [[PSImagePickerController alloc] init];
        
        
        imagePicker.delegate = self;
        imagePicker.sourceType = type;
        imagePicker.allowsEditing = YES;
        
        if( type == UIImagePickerControllerSourceTypeCamera)
        {
            imagePicker.showsCameraControls = YES;
        }
        
        [self presentViewController:imagePicker animated:YES completion:^{
            
        }];
        
        
    }
    
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

#pragma mark Gesture Delegate
-(void)handleLongPressGesture:(int)selectedIndex
{
    //self.selectedIndex=selectedIndex;
    
    //[self showActionSheet];
}

-(void)showActionSheet {
    
    UIActionSheet * _actionsSheet=nil;
    
    PropertyPicture * picture=[self.picturesDataSource objectAtIndex:self.selectedIndex];
    
    if (picture.imagePath) {
        
        _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:@"Delete"
                                           otherButtonTitles:NSLocalizedString(@"Set as Default", nil),
                         nil] ;
        
    }else
    {
        
        _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:@"Delete"
                                           otherButtonTitles:NSLocalizedString(@"Set as Default", nil),NSLocalizedString(@"Sync", nil),
                         
                         nil] ;
        
    }
    
    _actionsSheet.tag=2;
    
    _actionsSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [_actionsSheet showInView:self.view];
    
}


#pragma mark - Image Picker Controller Delegate


-(void) saveImageToServer:(UIImage*) image
{
    if(self.appointment)
    {
        [[PSDataPersistenceManager sharedManager] updateModificationStatusOfAppointments:[NSArray arrayWithObject:self.appointment] toModificationStatus:[NSNumber numberWithBool:YES]];
    }
    NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
    [requestParameters setObject:image forKey:kPropertyImage];
    [requestParameters setObject:@".jpg" forKey:kFileExtension];
    [requestParameters setObject:@"false" forKey:kIsDefault];
    [requestParameters setObject:[NSNumber numberWithInt:0] forKey:@"picId"];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    [requestParameters setObject:isEmpty(self.appointment.appointmentToProperty.propertyId)?[NSNull null]:self.appointment.appointmentToProperty.propertyId forKey:kPropertyId];
    [requestParameters setObject:isEmpty(self.appointment.appointmentToProperty.schemeId)?[NSNull null]:self.appointment.appointmentToProperty.schemeId forKey:kSchemeID];
    [requestParameters setObject:isEmpty(self.appointment.appointmentToProperty.blockId)?[NSNull null]:self.appointment.appointmentToProperty.blockId forKey:kSchemeBlockID];
    [requestParameters setObject:[UtilityClass getUniqueIdentifierStringWithUserName:[[SettingsClass sharedObject]loggedInUser].userName] forKey:kUniqueImageIdentifier];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kCreatedBy];
    
    [requestParameters setObject:self.appointment.appointmentType forKey:@"type"];
    //AlternativeHeating *heating = [[_appointment.appointmentToProperty.propertyToAlternativeHeating allObjects] objectAtIndex:0];
    [requestParameters setObject:!isEmpty(_altHeating)?_altHeating.itemId:[NSNumber numberWithInt:0] forKey:kItemId];
    [requestParameters setObject:!isEmpty(_altHeating)?_altHeating.heatingId:[NSNumber numberWithInt:0] forKey:kHeatingId];
    
    [requestParameters setObject:self.appointment.appointmentId forKey:kAppointmentId];
    
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
    if(_rootVC==OilPhotoGridRootProperty){
        [[PSDataPersistenceManager sharedManager] savePropertyPictureInfo:requestParameters withProperty:self.appointment.appointmentToProperty];
    }
    else if(_rootVC==OilPhotoGridRootDefect){
        [self saveDefectPicture:image];
    }
    else if(_rootVC==OilPhotoGridRootFireServicingInspection){
        [self saveAltFuelImage:image];
    }
    
}

-(void) saveDefectPicture:(UIImage *) image{
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    DefectPicture *propertyPicture = (DefectPicture *)[NSEntityDescription insertNewObjectForEntityForName:kDefectPicture inManagedObjectContext:managedObjectContext];
    propertyPicture.createdOn=[NSDate date];
    propertyPicture.imagePath= [propertyPicture getPicturePath];
    propertyPicture.synchStatus=[NSNumber numberWithInteger:0];
    propertyPicture.imageIdentifier = [UtilityClass getUniqueIdentifierStringWithUserName:[[SettingsClass sharedObject]loggedInUser].userName];
    propertyPicture.defectPictureToDefect = _defect;
    [_defect addDefectPicturesObject:propertyPicture];
    
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    image =[image scaleToSize:CGSizeMake(500, 500)];
    FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
    [manager storeImage:image forKey:propertyPicture.imagePath];
}

-(void) saveAltFuelImage:(UIImage *) image{
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _altHeating.oilHeatingToFireServiceInspection = [self getFirServiceInspection];
    AlternativeFuelPicture *picture = [NSEntityDescription insertNewObjectForEntityForName:kAlternativeFuelPicture inManagedObjectContext:managedObjectContext];
    picture.createdBy = isEmpty([[SettingsClass sharedObject]loggedInUser].userId)?nil:[[SettingsClass sharedObject]loggedInUser].userId;
    picture.createdOn = [NSDate date];
    picture.imageIdentifier = [UtilityClass getUniqueIdentifierStringWithUserName:[[SettingsClass sharedObject]loggedInUser].userName];
    picture.pictureId = [NSNumber numberWithInt:0];
    picture.syncStatus = [NSNumber numberWithInt:0];
    picture.schemeId = self.appointment.appointmentToProperty.schemeId;
    picture.blockId = self.appointment.appointmentToProperty.blockId;
    picture.propertyId = self.appointment.appointmentToProperty.propertyId;
    picture.appointmentId = self.appointment.appointmentId;
    picture.itemId = _altHeating.itemId;
    picture.isDefault = [NSNumber numberWithBool:NO];
    picture.imagePath = [picture getPicturePath];
    picture.propertyPictureName = [picture imagePath];
    picture.alternateFuelPictureToFireServiceInspection = _altHeating.oilHeatingToFireServiceInspection;
    [_altHeating.oilHeatingToFireServiceInspection addFireServicingInspectionToPicturesObject:picture];
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    [picture uploadPropertyPicture:@{} withImage:image];
    //[_picturesDataSource addObject:picture];
    //[self.collectionView reloadData];
    
}

-(OilFiringServiceInspection *) getFirServiceInspection{
    OilFiringServiceInspection *solarInspection;
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    if(self.altHeating.oilHeatingToFireServiceInspection!=nil){
        solarInspection = self.altHeating.oilHeatingToFireServiceInspection;
    }
    else{
        solarInspection = [NSEntityDescription insertNewObjectForEntityForName:kOilFireServiceInspectionEntity inManagedObjectContext:managedObjectContext];
        solarInspection.firingServiceInspectionToOilHeating = self.altHeating;
        self.altHeating.oilHeatingToFireServiceInspection = solarInspection;
        
        self.appointment.isModified = [NSNumber numberWithBool:YES];
        self.appointment.isSurveyChanged = [NSNumber numberWithBool:YES];
        [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    }
    return solarInspection;
}

-(void) deleteImageFromServer:(PropertyPicture*) propertyPicture
{
    
    //Server URL
    //create an instance for ProprtyPhotos
    [self showActivityIndicator:LOC(@"Removing Picture")];
    
    //Soft Deletion
    [self onPictureDelete:nil];
    
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image;
    if([info objectForKey:UIImagePickerControllerEditedImage])
    {
        image = [info valueForKey:UIImagePickerControllerEditedImage];
    }
    else
    {
        image = [info valueForKey:UIImagePickerControllerOriginalImage];
    }
    
    [self saveImageToServer:image];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: LOC(@"KEY_ALERT_SAVE_FAILURE")
                              message: LOC(@"KEY_ALERT_IMAGE_SAVE_FAILURE")
                              delegate: nil
                              cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                              otherButtonTitles:nil];
        [alert show];
    }else
    {
        
    }
}


- (void) setAsDefault:(PropertyPicture*) propertyPicture
{
    if(_rootVC==OilPhotoGridRootProperty){
        NSDictionary * dict=[NSDictionary dictionaryWithObject:@"true" forKey:kPropertyPictureIsDefault];
        
        [[PSDataUpdateManager sharedManager] updatePropertyPicture:propertyPicture forDictionary:dict];
    }
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    CLS_LOG(@"Clicked Index %d",buttonIndex);
    
    if (actionSheet.tag==1) {
        
        
        ///if (actionSheet == _actionsSheet)
        {
            // Actions
            if (buttonIndex != actionSheet.cancelButtonIndex) {
                if (buttonIndex == actionSheet.firstOtherButtonIndex) {
                    
                    [self showCameraOfSourceType:UIImagePickerControllerSourceTypeCamera];
                    
                } else if (buttonIndex == actionSheet.firstOtherButtonIndex + 1) {
                    [self showCameraOfSourceType:UIImagePickerControllerSourceTypePhotoLibrary]; return;
                }
                
            }
        }
        
    }else if (actionSheet.tag==2)
    {
        
        if (actionSheet.cancelButtonIndex==buttonIndex) {
            
        }else
            if(buttonIndex == actionSheet.destructiveButtonIndex) {
                
                [self deleteImageFromServer:[self.picturesDataSource objectAtIndex:self.selectedIndex]];
                
            }
            else if(buttonIndex == actionSheet.firstOtherButtonIndex) {
                
                [self setAsDefault:[self.picturesDataSource objectAtIndex:self.selectedIndex]];
            }
            else if(buttonIndex == actionSheet.firstOtherButtonIndex+1) {
                
                PropertyPicture * picture =[self.picturesDataSource objectAtIndex:self.selectedIndex];
                [picture uploadPicture];
            }
        
    }else  if (actionSheet.tag==3)
    {
        if(buttonIndex == actionSheet.firstOtherButtonIndex) {
            
            PropertyPicture * picture =[self.picturesDataSource objectAtIndex:self.selectedIndex];
            [picture uploadPicture];
        }
    }
}


#pragma mark - Notifications

-(void) registerNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDelete:) name:kPropertyPicturesRemoveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureSave:) name:kPropertyPictureSaveNotification object:nil];
}

-(void) deregisterNotifications{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPropertyPicturesRemoveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPropertyPictureSaveNotification object:nil];
}

-(void) onPictureRefresh{
    
    [self.collectionView reloadData];
    
}

-(void) onPictureDelete:(NSNotification*) notification
{
    [self hideActivityIndicator];
    if(_rootVC==OilPhotoGridRootProperty){
        PropertyPicture * picture=[self.picturesDataSource objectAtIndex:self.selectedIndex];
        //marking deleted image for user offline support
        //Dirty Marking
        [[PSDataUpdateManager sharedManager] markDeletePropertyPicture:picture];
        [self.picturesDataSource removeObject:picture];
        [self.collectionView reloadData];
    }
    
}

-(void) onPictureSave:(NSNotification*) notification
{
    if(_rootVC==OilPhotoGridRootProperty){
        [self hideActivityIndicator];
        
        PropertyPicture * picture=notification.object;
        if (picture) {
            
            [_picturesDataSource addObject:picture];
            [self.collectionView reloadData];
        }
    }
    
    
}


#pragma mark - UICollectionView Delegate Methods
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(90, 90);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_picturesDataSource count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PhotographCollectionViewCell *cell = (PhotographCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellIdentifier forIndexPath:indexPath];
    
    [cell setDataObject:[self.picturesDataSource objectAtIndex:[indexPath row]]];
    [cell.cellImageView setImage:nil];
    [cell loadImageView];
    
    cell.tag=indexPath.row;
    cell.gestureDelegate=self;
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    browser.allPhotos=self.picturesDataSource;
    browser.currentPageIndex=indexPath.row;
    browser.displayActionButton = NO;
    
    [self.navigationController pushViewController:browser animated:NO];
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 0, 5);
}

@end
