//
//  PSOilABTDetailsViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSTextfieldTableViewCell.h"
#import "PSAFSPickerViewTableViewCell.h"
@interface PSOilABTDetailsViewController :PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (strong, nonatomic) OilHeating *boiler;
@property (strong, nonatomic) PSBarButtonItem *btnSaveBoiler;
@property NSInteger pickerTagRow;
@property NSInteger pickerTagSection;
@property (strong,nonatomic) NSMutableArray *pickerArray;

@property (strong,nonatomic) NSString * applianceLocation;
@property (strong,nonatomic) NSString * applianceMake;
@property (strong,nonatomic) NSString * applianceModel;
@property (strong,nonatomic) NSString * applianceSerialNumber;
@property (strong,nonatomic) NSDate * originalInstallDate;
@property (strong,nonatomic) NSString * burnerMake;
@property (strong,nonatomic) NSString * burnerModel;
@property (strong,nonatomic) BurnerType * burnerType;
@property (strong,nonatomic) TankType * tankType;
@property (strong,nonatomic) OilFuelType * fuelType;
@property (strong,nonatomic) OilFlueType * flueType;

@end
