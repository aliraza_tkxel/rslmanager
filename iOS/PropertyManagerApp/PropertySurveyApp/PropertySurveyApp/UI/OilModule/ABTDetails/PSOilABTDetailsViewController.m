//
//  PSOilABTDetailsViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSOilABTDetailsViewController.h"

#define kTotalSections  3
#define kSectionAppliance 0
#define kSectionBurner  1
#define kSectionTank 2

#define kSectionApplianceTotalRows  5
#define kSectionApplianceRowLocation    0
#define kSectionApplianceRowMake    1
#define kSectionApplianceRowModel   2
#define kSectionApplianceRowSerialNumber   3
#define kSectionApplianceRowInstalledDate 4

#define kSectionBurnerTotalRows 3
#define kSectionBurnerRowMake   0
#define kSectionBurnerRowModel  1
#define kSectionBurnerRowType   2

#define kSectionTankTotalRows   3
#define kSectionTankRowType 0
#define kSectionTankRowFuel 1
#define kSectionTankRowFlue 2

@interface PSOilABTDetailsViewController ()

@end

@implementation PSOilABTDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [self registerCells];
    [self initDefaultValues];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init methods

-(void) initData{
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 44.0;
    _tblView.allowsSelection = NO;
    [_tblView reloadData];
}

-(void) initDefaultValues{
    _applianceMake = _boiler.applianceMake;
    _applianceLocation = _boiler.applianceLocation;
    _applianceSerialNumber = _boiler.applianceSerialNumber;
    _applianceModel = _boiler.applianceModel;
    _originalInstallDate = _boiler.originalInstallDate;
    
    _burnerMake = _boiler.burnerMake;
    _burnerModel = _boiler.burnerModel;
    _burnerType = _boiler.oilHeatingToBurnerType;
    
    _tankType = _boiler.oilHeatingToTankType;
    _fuelType = _boiler.oilHeatingToOilFuelType;
    _flueType = _boiler.oilHeatingToOilFlueType;
}

-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSPickerViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSPickerViewTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSTextfieldTableViewCell"];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSaveBoiler = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                style:PSBarButtonItemStyleDefault
                                                               target:self
                                                               action:@selector(btnSaveTapped)];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.btnSaveBoiler, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"APPLIANCE/BURNER/TANK"];
}

#pragma mark - Actions

- (void) btnBackTapped
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update this heating type?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveBoiler];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please fill all the mandatory fields"];
    }
    
    
    
}

-(void) saveBoiler{
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _boiler.applianceMake = _applianceMake;
    _boiler.applianceLocation = _applianceLocation;
    _boiler.applianceSerialNumber = _applianceSerialNumber;
    _boiler.applianceModel = _applianceModel;
    _boiler.originalInstallDate = _originalInstallDate;
    
    _boiler.burnerMake = _burnerMake;
    _boiler.burnerModel = _burnerModel;
    _boiler.oilHeatingToBurnerType = _burnerType;
    _boiler.burnerTypeId = _burnerType.lookupId;
    
    _boiler.oilHeatingToTankType = _tankType;
    _boiler.tankTypeId = _tankType.lookupId;
    _boiler.oilHeatingToOilFuelType = _fuelType;
    _boiler.fuelTypeId = _fuelType.lookupId;
    _boiler.oilHeatingToOilFlueType = _flueType;
    _boiler.flueTypeId = _flueType.lookupId;
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    [self popOrCloseViewController];
}

-(PSAltFuelDynamicCellsConfiguartion*) getCellConfigurationForRow:(NSInteger) row andSection:(NSInteger) section{
    PSAltFuelDynamicCellsConfiguartion * config = [[PSAltFuelDynamicCellsConfiguartion alloc] init];
    config.cellRowId = row;
    config.cellSectionId = section;
    config.inputFieldTitle = [self getHeaderForRow:row andSection:section];
    config.selectedValue = [self getValueForRow:row andSection:section];
    config.isNonMutableField = NO;
    config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeFreeText;
    config.appliedCellFieldType = PSAFSDynamicCellsBaseTypeFreeText;
    if(section==kSectionAppliance){
        if(row==kSectionApplianceRowInstalledDate){
            config.dropDownHeaderTitle = @"INSTALLATION DATE";
            config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDate;
            config.isNonMutableField = YES;
            config.startingDate = !isEmpty(_originalInstallDate)?_originalInstallDate:[NSDate date];
        }
    }
    if(section==kSectionBurner){
        if(row==kSectionBurnerRowType){
            config.dropDownHeaderTitle = @"BURNER TYPE";
            config.dropDownSourceArray = [[NSArray alloc] initWithArray:[self createPresentablePickerDataForRow:row andSection:section]];
            config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDropDown;
            config.isNonMutableField = YES;
        }
    }
    else if(section==kSectionTank){
        config.dropDownSourceArray = [[NSArray alloc] initWithArray:[self createPresentablePickerDataForRow:row andSection:section]];
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDropDown;
        config.isNonMutableField = YES;
        if(row==kSectionTankRowType){
           config.dropDownHeaderTitle = @"TANK TYPE";
        }
        else if(row==kSectionTankRowFlue){
            config.dropDownHeaderTitle = @"FLUE TYPE";
        }
        else if(row==kSectionTankRowFuel){
            config.dropDownHeaderTitle = @"FUEL TYPE";
        }
    }
    
    return config;
}

#pragma mark - PickerRows

- (NSArray *) getPickerDataForRow:(NSInteger) row andSection:(NSInteger) section
{
    NSArray *rowsData = [NSArray array];
    STARTEXCEPTION
    NSArray* boilerData = [NSArray array];
    if(section==kSectionBurner){
        if(row==kSectionBurnerRowType){
            boilerData = [CoreDataHelper getObjectsFromEntity:kBurnerTypeEntity
                                                    predicate:nil
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
        }
    }
    else if(section==kSectionTank){
        if(row==kSectionTankRowType){
            boilerData = [CoreDataHelper getObjectsFromEntity:kTankTypeEntity
                                                    predicate:nil
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
        }
        else if(row==kSectionTankRowFlue){
            boilerData = [CoreDataHelper getObjectsFromEntity:kOilFlueTypeEntity
                                                    predicate:nil
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
        }
        else if(row==kSectionTankRowFuel){
            boilerData = [CoreDataHelper getObjectsFromEntity:kOilFuelTypeEntity
                                                    predicate:nil
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
        }
    }
    if([boilerData count] > 0)
    {
        rowsData = [NSArray arrayWithArray:boilerData];
    }
    ENDEXCEPTION
    return rowsData;
}

-(NSMutableArray *) createPresentablePickerDataForRow:(NSInteger) row andSection:(NSInteger) section{
    NSArray *objectsArray = [self getPickerDataForRow:row andSection: section];
    NSMutableArray *presentableData = [[NSMutableArray alloc] init];
    if(section==kSectionBurner){
        if(row==kSectionBurnerRowType){
            for(BurnerType *type in objectsArray){
                [presentableData addObject:type.value];
            }
        }
    }
    else if(section==kSectionTank){
        if(row==kSectionTankRowType){
            for(TankType *type in objectsArray){
                [presentableData addObject:type.value];
            }
        }
        else if(row==kSectionTankRowFlue){
            for(OilFlueType *type in objectsArray){
                [presentableData addObject:type.value];
            }
        }
        else if(row==kSectionTankRowFuel){
            for(OilFuelType *type in objectsArray){
                [presentableData addObject:type.value];
            }
        }
    }
    return presentableData;
}

#pragma mark - Utility
- (void)addBottomBorderTo:(UIView*)parentView WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth {
    UIView *border = [UIView new];
    border.backgroundColor = color;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    border.frame = CGRectMake(0, parentView.frame.size.height - borderWidth, parentView.frame.size.width, borderWidth);
    [parentView addSubview:border];
}
-(NSString *) getHeaderForSection:(NSInteger) section{
    NSString *header = @"";
    switch (section) {
        case kSectionAppliance:
            header = @"APPLIANCE";
            break;
        case kSectionBurner:
            header = @"BURNER";
            break;
        case kSectionTank:
            header = @"TANK";
            break;
        default:
            break;
    }
    return header;
}

-(BOOL) isValidData{
    if( !isEmpty(_applianceModel) && !isEmpty(_applianceSerialNumber) && !isEmpty(_applianceLocation)
       && !isEmpty(_applianceMake) && !isEmpty(_originalInstallDate)
       && !isEmpty(_burnerModel)&& !isEmpty(_burnerMake)&& !isEmpty(_burnerType)
       && !isEmpty(_tankType)&& !isEmpty(_flueType)&& !isEmpty(_fuelType)){
        if([_burnerType.value isEqualToString:@"Please Select"]
           || [_tankType.value isEqualToString:@"Please Select"]
           || [_flueType.value isEqualToString:@"Please Select"]
           || [_fuelType.value isEqualToString:@"Please Select"])
        {
            return NO;
        }
        return YES;
    }
    return NO;
}

-(NSString *) getHeaderForRow:(NSInteger)row andSection:(NSInteger) section{
    NSString *header = @"";
    
    if(section==kSectionAppliance){
        switch (row) {
            case kSectionApplianceRowMake:
                header = @"MAKE";
                break;
            case kSectionApplianceRowModel:
                header = @"MODEL";
                break;
            case kSectionApplianceRowLocation:
                header = @"LOCATION";
                break;
            case kSectionApplianceRowSerialNumber:
                header = @"SERIAL NUMBER";
                break;
            case kSectionApplianceRowInstalledDate:
                header = @"INSTALLED DATE";
                break;
            default:
                break;
        }
    }
    else if(section==kSectionBurner){
        switch (row) {
            case kSectionBurnerRowMake:
                header = @"MAKE";
                break;
            case kSectionBurnerRowModel:
                header = @"MODEL";
                break;
            case kSectionBurnerRowType:
                header = @"TYPE";
                break;
            default:
                break;
        }
    }
    else if(section==kSectionTank){
        switch (row) {
            case kSectionTankRowType:
                header = @"TYPE";
                break;
            case kSectionTankRowFlue:
                header = @"FLUE TYPE";
                break;
            case kSectionTankRowFuel:
                header = @"FUEL TYPE";
                break;
            default:
                break;
        }
    }
    return header;
}

-(NSString *) getValueForRow:(NSInteger)row andSection:(NSInteger) section{
    NSString *header = @"";
    if(section==kSectionAppliance){
        switch (row) {
            case kSectionApplianceRowMake:
                header = _applianceMake;
                break;
            case kSectionApplianceRowModel:
                header = _applianceModel;
                break;
            case kSectionApplianceRowLocation:
                header = _applianceLocation;
                break;
            case kSectionApplianceRowSerialNumber:
                header = _applianceSerialNumber;
                break;
            case kSectionApplianceRowInstalledDate:
                header = [UtilityClass stringFromDate:_originalInstallDate dateFormat:kDateTimeStyle8];
                break;
            default:
                break;
        }
    }
    else if(section==kSectionBurner){
        switch (row) {
            case kSectionBurnerRowMake:
                header = _burnerMake;
                break;
            case kSectionBurnerRowModel:
                header = _burnerModel;
                break;
            case kSectionBurnerRowType:
                header = _burnerType.value;
                break;
            default:
                break;
        }
    }
    else if(section==kSectionTank){
        switch (row) {
            case kSectionTankRowType:
                header = _tankType.value;
                break;
            case kSectionTankRowFlue:
                header = _flueType.value;
                break;
            case kSectionTankRowFuel:
                header = _fuelType.value;
                break;
            default:
                break;
        }
    }
    if(isEmpty(header)==YES){
        header = @"";
    }
    return header;
}


-(void) setValueForBaseConfig:(PSAltFuelDynamicCellsConfiguartion *) config{
    NSString *header = config.selectedValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    if(section==kSectionAppliance){
        switch (row) {
            case kSectionApplianceRowMake:
                _applianceMake = header;
                break;
            case kSectionApplianceRowModel:
                _applianceModel = header;
                break;
            case kSectionApplianceRowLocation:
                _applianceLocation = header;
                break;
            case kSectionApplianceRowSerialNumber:
                _applianceSerialNumber = header;
                break;
            case kSectionApplianceRowInstalledDate:
                _originalInstallDate = [UtilityClass dateFromString:header dateFormat:kDateTimeStyle8];
                break;
            default:
                break;
        }
    }
    else if(section==kSectionBurner){
        switch (row) {
            case kSectionBurnerRowMake:
                _burnerMake = header;
                break;
            case kSectionBurnerRowModel:
                _burnerModel = header;
                break;
            case kSectionBurnerRowType:
                break;
            default:
                break;
        }
    }
    else if(section==kSectionTank){
        switch (row) {
            case kSectionTankRowType:
                break;
            case kSectionTankRowFlue:
                break;
            case kSectionTankRowFuel:
                break;
            default:
                break;
        }
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}
#pragma mark - TableViewDelegates


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 40)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [label setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    
    label.text = [self getHeaderForSection:section];
    label.textAlignment = NSTextAlignmentLeft;
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    [self addBottomBorderTo:view WithColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR) andWidth:2.0f];
    return view;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger total = 0;
    switch (section) {
        case kSectionAppliance:
            total = kSectionApplianceTotalRows;
            break;
        case kSectionBurner:
            total = kSectionBurnerTotalRows;
            break;
        case kSectionTank:
            total = kSectionTankTotalRows;
            break;
        default:
            break;
    }
    return total;
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return kTotalSections;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    PSAltFuelDynamicCellsConfiguartion *config = [self getCellConfigurationForRow:indexPath.row andSection:indexPath.section];
    if(config.baseTypeForConfig == PSAFSDynamicCellsBaseTypeFreeText){
        PSAFSTextfieldTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSTextfieldTableViewCell class])];
        manualCell.delegate = self;
        manualCell.baseConfig = config;
        [manualCell initializeData];
        cell = manualCell;
    }
    else{
        PSAFSPickerViewTableViewCell *pickerCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSPickerViewTableViewCell class])];
        pickerCell.delegate = self;
        pickerCell.baseConfig = config;
        pickerCell.btnClearValue.hidden = YES;
        [pickerCell initializeData];
        cell = pickerCell;
    }
    cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Delegates

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion*)config{
    [self setValueForBaseConfig:config];
}

-(void) didClickShowDropDownFor:(PSAltFuelDynamicCellsConfiguartion *)config andSender:(id) sender{
    [self.view endEditing:YES];
    _pickerTagRow = config.cellRowId;
    _pickerTagSection = config.cellSectionId;
    _pickerArray = [[NSMutableArray alloc] initWithArray:config.dropDownSourceArray];
    NSInteger initialIndex = [_pickerArray indexOfObject:config.selectedValue];
    if(initialIndex>[config.dropDownSourceArray count]){
        initialIndex = 0;
    }
    [ActionSheetStringPicker showPickerWithTitle:config.dropDownHeaderTitle
                                            rows:_pickerArray
                                initialSelection:initialIndex
                                          target:self
                                   successAction:@selector(onOptionSelected:element:)
                                    cancelAction:@selector(onOptionPickerCancelled:)
                                          origin:sender];
}


- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    NSString *value = [_pickerArray objectAtIndex:[selectedIndex integerValue]];
    [_pickerArray removeAllObjects];
    if(_pickerTagSection==kSectionBurner){
        if(_pickerTagRow==kSectionBurnerRowType){
            BurnerType *mfr = [self getObjectForPickerTag:_pickerTagRow andSection:_pickerTagSection andValue:value];
            _burnerType = mfr;
        }
    }
    else if(_pickerTagSection==kSectionTank){
        if(_pickerTagRow==kSectionTankRowFuel){
            OilFuelType *mfr = [self getObjectForPickerTag:_pickerTagRow andSection:_pickerTagSection andValue:value];
            _fuelType = mfr;
        }
        else if(_pickerTagRow==kSectionTankRowFlue){
            OilFlueType *mfr = [self getObjectForPickerTag:_pickerTagRow andSection:_pickerTagSection andValue:value];
            _flueType = mfr;
        }
        else if(_pickerTagRow==kSectionTankRowType){
            TankType *mfr = [self getObjectForPickerTag:_pickerTagRow andSection:_pickerTagSection andValue:value];
            _tankType = mfr;
        }
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:_pickerTagRow inSection:_pickerTagSection];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (void) onOptionPickerCancelled:(id)sender {
    CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

-(id) getObjectForPickerTag:(NSInteger)pickerTag andSection:(NSInteger) secton andValue:(NSString *) value{
    NSArray *dataArray = [self getPickerDataForRow:pickerTag andSection:secton];
    
    if(_pickerTagSection==kSectionBurner){
        if(_pickerTagRow==kSectionBurnerRowType){
            BurnerType *type;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.value = %@", value];
            NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
            if([filteredArray count]>0){
                type = [filteredArray objectAtIndex:0];
            }
            return type;
        }
    }
    else if(_pickerTagSection==kSectionTank){
        if(_pickerTagRow==kSectionTankRowFuel){
             OilFuelType *type;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.value = %@", value];
            NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
            if([filteredArray count]>0){
                type = [filteredArray objectAtIndex:0];
            }
            return type;
        }
        else if(_pickerTagRow==kSectionTankRowFlue){
            OilFlueType *type;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.value = %@", value];
            NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
            if([filteredArray count]>0){
                type = [filteredArray objectAtIndex:0];
            }
            return type;
        }
        else if(_pickerTagRow==kSectionTankRowType){
             TankType *type;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.value = %@", value];
            NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
            if([filteredArray count]>0){
                type = [filteredArray objectAtIndex:0];
            }
            return type;
        }
    }
    return @"";
}

@end
