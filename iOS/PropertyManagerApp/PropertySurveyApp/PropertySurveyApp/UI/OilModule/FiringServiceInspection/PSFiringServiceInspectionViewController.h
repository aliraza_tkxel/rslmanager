//
//  PSFiringServiceInspectionViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSBoolAndTextTableViewCell.h"
#import "PSOilPhotographsViewController.h"
@interface PSFiringServiceInspectionViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (strong, nonatomic) OilHeating *boiler;
@property (strong, nonatomic) PSBarButtonItem *btnSave;
@property(nonatomic,strong) PSBarButtonItem * cameraButton;
@property (strong,nonatomic) Appointment *appointment;


@property (nonatomic) BOOL airSupply;
@property (strong, nonatomic) NSString *airSupplyDetail;
@property (nonatomic) BOOL applianceSafety;
@property (strong, nonatomic) NSString *applianceSafetyDetail;
@property (nonatomic) BOOL chimneyFlue;
@property (strong, nonatomic) NSString *chimneyFlueDetail;
@property (nonatomic) BOOL combustionChamber;
@property (strong, nonatomic) NSString *combustionChamberDetail;
@property (nonatomic) BOOL controlCheck;
@property (strong, nonatomic) NSString *controlCheckDetail;
@property (nonatomic) BOOL electricalSafety;
@property (strong, nonatomic) NSString *electricalSafetyDetail;
@property (nonatomic) BOOL heatExchanger;
@property (strong, nonatomic) NSString *heatExchangerDetail;
@property (nonatomic) BOOL hotWaterType;
@property (strong, nonatomic) NSString *hotWaterTypeDetail;
@property (nonatomic) BOOL oilStorage;
@property (strong, nonatomic) NSString *oilStorageDetail;
@property (nonatomic) BOOL oilSupplySystem;
@property (strong, nonatomic) NSString *oilSupplySystemDetail;
@property (nonatomic) BOOL pressureJet;
@property (strong, nonatomic) NSString *pressureJetDetail;
@property (nonatomic) BOOL vaporisingBurner;
@property (strong, nonatomic) NSString *vaporisingBurnerDetail;
@property (nonatomic) BOOL wallflameBurner;
@property (strong, nonatomic) NSString *wallflameBurnerDetail;
@property (nonatomic) BOOL warmAirType;
@property (strong, nonatomic) NSString *warmAirTypeDetail; //14 total

@property (strong, nonatomic) NSNumber *inspectedBy;
@property (strong, nonatomic) NSDate *inspectionDate;
@property (strong, nonatomic) NSNumber *inspectionId;
@property (nonatomic) BOOL isInspected;

@end
