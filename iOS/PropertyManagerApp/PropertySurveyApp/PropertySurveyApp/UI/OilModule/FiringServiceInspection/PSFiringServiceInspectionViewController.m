//
//  PSFiringServiceInspectionViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSFiringServiceInspectionViewController.h"

#define kSectionFiringServiceInspection    0


#define kTotalRows  14
#define kRowAirSupply   0
#define kRowApplianceSafety 1
#define kRowChimneyFlue 2
#define kRowCombustionChamber   3
#define kRowControlCheck    4
#define kRowElectricalSafety    5
#define kRowHeatExchanger   6
#define kRowHotWaterType    7
#define kRowOilStorage  8
#define kRowOilSupplySystem 9
#define kRowPressureJet 10
#define kRowVaporisingBurner 11
#define kRowWallFlameBurner 12
#define kRowWarmAirType    13

@interface PSFiringServiceInspectionViewController ()

@end

@implementation PSFiringServiceInspectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [self registerCells];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [self refreshPhotosCount];
}

#pragma mark - Init methods

-(void) initData{
    [self populateDefaultValues];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 44.0;
    _tblView.allowsSelection = NO;
    [_tblView reloadData];
}
-(void) refreshPhotosCount{
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    if(!isEmpty(self.cameraButton)){
        [self.cameraButton reloadBadge:badgeText];
    }
    
}
-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSBoolAndTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSBoolAndTextTableViewCell"];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSave = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                          style:PSBarButtonItemStyleDefault
                                                         target:self
                                                         action:@selector(btnSaveTapped)];
    
    
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    
    self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                               style:PSBarButtonItemStyleCamera
                                                              target:self
                                                              action:@selector(onClickCameraButton)bagde:badgeText];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.cameraButton,self.btnSave, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Oil Firing & Service"];
}

-(void) populateDefaultValues{
    if(!isEmpty(_boiler.oilHeatingToFireServiceInspection)){
        
        _airSupply = !isEmpty(_boiler.oilHeatingToFireServiceInspection.airSupply)?[_boiler.oilHeatingToFireServiceInspection.airSupply boolValue]:NO;
        _applianceSafety = !isEmpty(_boiler.oilHeatingToFireServiceInspection.applianceSafety)?[_boiler.oilHeatingToFireServiceInspection.applianceSafety boolValue]:NO;
        _chimneyFlue = !isEmpty(_boiler.oilHeatingToFireServiceInspection.chimneyFlue)?[_boiler.oilHeatingToFireServiceInspection.chimneyFlue boolValue]:NO;
        _combustionChamber = !isEmpty(_boiler.oilHeatingToFireServiceInspection.combustionChamber)?[_boiler.oilHeatingToFireServiceInspection.combustionChamber boolValue]:NO;
        _controlCheck = !isEmpty(_boiler.oilHeatingToFireServiceInspection.controlCheck)?[_boiler.oilHeatingToFireServiceInspection.controlCheck boolValue]:NO;
        _electricalSafety = !isEmpty(_boiler.oilHeatingToFireServiceInspection.electricalSafety)?[_boiler.oilHeatingToFireServiceInspection.electricalSafety boolValue]:NO;
        _heatExchanger = !isEmpty(_boiler.oilHeatingToFireServiceInspection.heatExchanger)?[_boiler.oilHeatingToFireServiceInspection.heatExchanger boolValue]:NO;
        _hotWaterType = !isEmpty(_boiler.oilHeatingToFireServiceInspection.hotWaterType)?[_boiler.oilHeatingToFireServiceInspection.hotWaterType boolValue]:NO;
        _oilStorage = !isEmpty(_boiler.oilHeatingToFireServiceInspection.oilStorage)?[_boiler.oilHeatingToFireServiceInspection.oilStorage boolValue]:NO;
        _oilSupplySystem = !isEmpty(_boiler.oilHeatingToFireServiceInspection.oilSupplySystem)?[_boiler.oilHeatingToFireServiceInspection.oilSupplySystem boolValue]:NO;
        _pressureJet = !isEmpty(_boiler.oilHeatingToFireServiceInspection.pressureJet)?[_boiler.oilHeatingToFireServiceInspection.pressureJet boolValue]:NO;
        _vaporisingBurner = !isEmpty(_boiler.oilHeatingToFireServiceInspection.vaporisingBurner)?[_boiler.oilHeatingToFireServiceInspection.vaporisingBurner boolValue]:NO;
        _wallflameBurner = !isEmpty(_boiler.oilHeatingToFireServiceInspection.wallflameBurner)?[_boiler.oilHeatingToFireServiceInspection.wallflameBurner boolValue]:NO;
        _warmAirType = !isEmpty(_boiler.oilHeatingToFireServiceInspection.warmAirType)?[_boiler.oilHeatingToFireServiceInspection.warmAirType boolValue]:NO;
        
        
        _airSupplyDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.airSupplyDetail)?_boiler.oilHeatingToFireServiceInspection.airSupplyDetail:@"";
        _applianceSafetyDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.applianceSafetyDetail)?_boiler.oilHeatingToFireServiceInspection.applianceSafetyDetail:@"";
        _chimneyFlueDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.chimneyFlueDetail)?_boiler.oilHeatingToFireServiceInspection.chimneyFlueDetail:@"";
        _combustionChamberDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.combustionChamberDetail)?_boiler.oilHeatingToFireServiceInspection.combustionChamberDetail:@"";
        _controlCheckDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.controlCheckDetail)?_boiler.oilHeatingToFireServiceInspection.controlCheckDetail:@"";
        _electricalSafetyDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.electricalSafetyDetail)?_boiler.oilHeatingToFireServiceInspection.electricalSafetyDetail:@"";
        _heatExchangerDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.heatExchangerDetail)?_boiler.oilHeatingToFireServiceInspection.heatExchangerDetail:@"";
        _hotWaterTypeDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.hotWaterTypeDetail)?_boiler.oilHeatingToFireServiceInspection.hotWaterTypeDetail:@"";
        _oilStorageDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.oilStorageDetail)?_boiler.oilHeatingToFireServiceInspection.oilStorageDetail:@"";
        _oilSupplySystemDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.oilSupplySystemDetail)?_boiler.oilHeatingToFireServiceInspection.oilSupplySystemDetail:@"";
        _pressureJetDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.pressureJetDetail)?_boiler.oilHeatingToFireServiceInspection.pressureJetDetail:@"";
        _vaporisingBurnerDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.vaporisingBurnerDetail)?_boiler.oilHeatingToFireServiceInspection.vaporisingBurnerDetail:@"";
        _wallflameBurnerDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.wallflameBurnerDetail)?_boiler.oilHeatingToFireServiceInspection.wallflameBurnerDetail:@"";
        _warmAirTypeDetail = !isEmpty(_boiler.oilHeatingToFireServiceInspection.warmAirTypeDetail)?_boiler.oilHeatingToFireServiceInspection.warmAirTypeDetail:@"";
    }
    else{
        _isInspected = NO;
        _inspectionDate = [NSDate date];
    }
}


#pragma mark - Actions

- (void) btnBackTapped
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update this heating type?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveHeatingInspection];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please fill all the mandatory fields"];
    }
    
    
    
}


- (void)onClickCameraButton {
    [self.view endEditing:YES];
    CLS_LOG(@"Camera Button");
    if([self isValidData]){
         [self performSegueWithIdentifier:SEG_OIL_FIRE_INSPECTION_TO_PHOTOGRAPHS sender:self];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please check/fill all the mandatory fields"];
    }
   
    
}

#pragma mark - Utility

-(NSUInteger) getPhotographsCount
{
    NSArray * photographsList =[_boiler.oilHeatingToFireServiceInspection.fireServicingInspectionToPictures allObjects];
    return !isEmpty(photographsList)?[photographsList count]:0;
}


-(BOOL) isValidData{
    if(!isEmpty(_airSupplyDetail) && !isEmpty(_applianceSafetyDetail) && !isEmpty(_chimneyFlueDetail) &&
       !isEmpty(_combustionChamberDetail)
       && !isEmpty(_controlCheckDetail)
       && !isEmpty(_electricalSafetyDetail)
       && !isEmpty(_heatExchangerDetail)
       && !isEmpty(_hotWaterTypeDetail)
       && !isEmpty(_oilSupplySystemDetail)
       && !isEmpty(_oilStorageDetail)
       && !isEmpty(_pressureJetDetail)
       && !isEmpty(_vaporisingBurnerDetail)
       && !isEmpty(_wallflameBurnerDetail)
       && !isEmpty(_warmAirTypeDetail)){
        return YES;
    }
    return NO;
}

-(NSString *) getHeaderForSection:(NSInteger) section{
    NSString *header = @"";
    switch (section) {
        case kSectionFiringServiceInspection:
            header = @"OIL FIRING SERVICE AND COMMISIONING SCHEDULE";
            break;
        default:
            break;
    }
    return header;
}

-(NSString *) getCellLabelForSection:(NSInteger) section andRow:(NSInteger) row{
    NSString *label = @"";
    switch (row) {
        case kRowAirSupply:
            label = @"AIR SUPPLY";
            break;
        case kRowApplianceSafety:
            label = @"APPLIANCE SAFETY";
            break;
        case kRowChimneyFlue:
            label = @"CHIMNEY FLUE";
            break;
        case kRowCombustionChamber:
            label = @"COMBUSTION CHAMBER";
            break;
        case kRowControlCheck:
            label = @"CONTROL CHECK";
            break;
        case kRowElectricalSafety:
            label = @"ELECTRICAL SAFETY";
            break;
        case kRowHeatExchanger:
            label = @"HEAT EXCHANGE";
            break;
        case kRowHotWaterType:
            label = @"HOT WATER TYPE";
            break;
        case kRowOilStorage:
            label = @"OIL STORAGE";
            break;
        case kRowOilSupplySystem:
            label = @"OIL SUPPLY SYSTEM";
            break;
        case kRowPressureJet:
            label = @"PRESSURE JET";
            break;
        case kRowVaporisingBurner:
            label = @"VAPORISING BURNER";
            break;
        case kRowWallFlameBurner:
            label = @"WALL FLAME BURNER";
            break;
        case kRowWarmAirType:
            label = @"WARM AIR TYPE";
            break;
        default:
            break;
    }
    return label;
}

- (void)addBottomBorderTo:(UIView*)parentView WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth {
    UIView *border = [UIView new];
    border.backgroundColor = color;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    border.frame = CGRectMake(0, parentView.frame.size.height - borderWidth, parentView.frame.size.width, borderWidth);
    [parentView addSubview:border];
}

#pragma mark - Getters and Setters

-(NSString *) getSelectedStringValueForSection:(NSInteger) section andRow:(NSInteger) row{
    NSString *label = @"";
    switch (row) {
        case kRowAirSupply:
            label = _airSupplyDetail;
            break;
        case kRowApplianceSafety:
            label = _applianceSafetyDetail;
            break;
        case kRowChimneyFlue:
            label = _chimneyFlueDetail;
            break;
        case kRowCombustionChamber:
            label = _combustionChamberDetail;
            break;
        case kRowControlCheck:
            label = _controlCheckDetail;
            break;
        case kRowElectricalSafety:
            label = _electricalSafetyDetail;
            break;
        case kRowHeatExchanger:
            label = _heatExchangerDetail;
            break;
        case kRowHotWaterType:
            label = _hotWaterTypeDetail;
            break;
        case kRowOilStorage:
            label = _oilStorageDetail;
            break;
        case kRowOilSupplySystem:
            label = _oilSupplySystemDetail;
            break;
        case kRowPressureJet:
            label = _pressureJetDetail;
            break;
        case kRowVaporisingBurner:
            label = _vaporisingBurnerDetail;
            break;
        case kRowWallFlameBurner:
            label = _wallflameBurnerDetail;
            break;
        case kRowWarmAirType:
            label = _warmAirTypeDetail;
            break;
        default:
            break;
    }
    return label;
}

-(void) setSelectedStringValueWithConfig:(PSAltFuelDynamicCellsConfiguartion*)config{
    NSString *label = config.selectedValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    switch (row) {
        case kRowAirSupply:
            _airSupplyDetail = label;
            break;
        case kRowApplianceSafety:
            _applianceSafetyDetail = label;
            break;
        case kRowChimneyFlue:
            _chimneyFlueDetail = label;
            break;
        case kRowCombustionChamber:
            _combustionChamberDetail = label;
            break;
        case kRowControlCheck:
            _controlCheckDetail = label;
            break;
        case kRowElectricalSafety:
            _electricalSafetyDetail= label;
            break;
        case kRowHeatExchanger:
            _heatExchangerDetail = label;
            break;
        case kRowHotWaterType:
            _hotWaterTypeDetail = label;
            break;
        case kRowOilStorage:
            _oilStorageDetail = label;
            break;
        case kRowOilSupplySystem:
            _oilSupplySystemDetail = label;
            break;
        case kRowPressureJet:
            _pressureJetDetail = label;
            break;
        case kRowVaporisingBurner:
            _vaporisingBurnerDetail = label;
            break;
        case kRowWallFlameBurner:
            _wallflameBurnerDetail = label;
            break;
        case kRowWarmAirType:
            _warmAirTypeDetail = label;
            break;
        default:
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}


-(BOOL) getBoolSelectedValueForSection:(NSInteger) section andRow:(NSInteger) row{
    BOOL label = NO;
    switch (row) {
        case kRowAirSupply:
            label = _airSupply;
            break;
        case kRowApplianceSafety:
            label = _applianceSafety;
            break;
        case kRowChimneyFlue:
            label = _chimneyFlue;
            break;
        case kRowCombustionChamber:
            label = _combustionChamber;
            break;
        case kRowControlCheck:
            label = _controlCheck;
            break;
        case kRowElectricalSafety:
            label = _electricalSafety;
            break;
        case kRowHeatExchanger:
            label = _heatExchanger;
            break;
        case kRowHotWaterType:
            label = _hotWaterType;
            break;
        case kRowOilStorage:
            label = _oilStorage;
            break;
        case kRowOilSupplySystem:
            label = _oilSupplySystem;
            break;
        case kRowPressureJet:
            label = _pressureJet;
            break;
        case kRowVaporisingBurner:
            label = _vaporisingBurner;
            break;
        case kRowWallFlameBurner:
            label = _wallflameBurner;
            break;
        case kRowWarmAirType:
            label = _warmAirType;
            break;
        default:
            break;
    }
    return label;
}

-(void) setBoolSelectedValueWithConfig:(PSAltFuelDynamicCellsConfiguartion*)config{
    BOOL label = config.selectedBoolValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    switch (row) {
        case kRowAirSupply:
            _airSupply = label;
            break;
        case kRowApplianceSafety:
            _applianceSafety = label;
            break;
        case kRowChimneyFlue:
            _chimneyFlue = label;
            break;
        case kRowCombustionChamber:
            _combustionChamber = label;
            break;
        case kRowControlCheck:
            _controlCheck = label;
            break;
        case kRowElectricalSafety:
            _electricalSafety= label;
            break;
        case kRowHeatExchanger:
            _heatExchanger = label;
            break;
        case kRowHotWaterType:
            _hotWaterType = label;
            break;
        case kRowOilStorage:
            _oilStorage = label;
            break;
        case kRowOilSupplySystem:
            _oilSupplySystem = label;
            break;
        case kRowPressureJet:
            _pressureJet = label;
            break;
        case kRowVaporisingBurner:
            _vaporisingBurner = label;
            break;
        case kRowWallFlameBurner:
            _wallflameBurner = label;
            break;
        case kRowWarmAirType:
            _warmAirType = label;
            break;
        default:
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}

-(PSAltFuelDynamicCellsConfiguartion*) getCellConfigurationForRow:(NSInteger) row andSection:(NSInteger) section{
    PSAltFuelDynamicCellsConfiguartion * config = [[PSAltFuelDynamicCellsConfiguartion alloc] init];
    config.cellRowId = row;
    config.cellSectionId = section;
    config.inputFieldTitle = [self getCellLabelForSection:section andRow:row];
    config.selectedBoolValue = [self getBoolSelectedValueForSection:section andRow:row];
    config.selectedValue = [self getSelectedStringValueForSection:section andRow:row];
    config.isNonMutableField = NO;
    config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeBoolAndText;
    config.appliedCellFieldType = PSAFSDynamicCellsBaseTypeFreeText;
    return config;
}

#pragma mark - UITableViewDelegates

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return kTotalRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 40)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [label setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    
    label.text = [self getHeaderForSection:section];
    label.textAlignment = NSTextAlignmentLeft;
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    [self addBottomBorderTo:view WithColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR) andWidth:2.0f];
    return view;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    PSAltFuelDynamicCellsConfiguartion *config = [self getCellConfigurationForRow:indexPath.row andSection:indexPath.section];
    PSAFSBoolAndTextTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSBoolAndTextTableViewCell class])];
    manualCell.baseConfig = config;
    manualCell.delegate = self;
    [manualCell initWithConfig];
    [manualCell setNeedsUpdateConstraints];
    [manualCell updateConstraintsIfNeeded];
    [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    cell = manualCell;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

#pragma mark - Delegates

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion *)config andIsForBoolValue:(BOOL)isForBoolVal{
    if(!isForBoolVal){
        [self setSelectedStringValueWithConfig:config];
    }
    else{
        [self setBoolSelectedValueWithConfig:config];
    }
}


#pragma mark - Save heating Inspection

-(void) saveAllData{
    OilFiringServiceInspection *boilerInspection;
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
    _inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
    _inspectionId = (isEmpty(_inspectionId) || (_inspectionId == [NSNumber numberWithInt:-1]))?[NSNumber numberWithInt:-1]:_inspectionId;
    if(self.boiler.oilHeatingToFireServiceInspection!=nil){
        boilerInspection = self.boiler.oilHeatingToFireServiceInspection;
    }
    else{
        boilerInspection = [NSEntityDescription insertNewObjectForEntityForName:kOilFireServiceInspectionEntity inManagedObjectContext:managedObjectContext];
    }
    self.boiler.isInspected = [NSNumber numberWithBool:YES];
    boilerInspection.isInspected = [NSNumber numberWithBool:YES];
    boilerInspection.inspectedBy = isEmpty(_inspectedBy)?nil:_inspectedBy;
    boilerInspection.inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
    boilerInspection.inspectionId = isEmpty(_inspectionId)?nil:_inspectionId;
    boilerInspection.appointmentId = self.appointment.appointmentId;
    boilerInspection.heatingId = self.boiler.heatingId;
    boilerInspection.journalId = self.appointment.journalId;
    
    boilerInspection.airSupply = [NSNumber numberWithBool:_airSupply];
    boilerInspection.applianceSafety = [NSNumber numberWithBool:_applianceSafety];
    boilerInspection.chimneyFlue = [NSNumber numberWithBool:_chimneyFlue];
    boilerInspection.combustionChamber = [NSNumber numberWithBool:_combustionChamber];
    boilerInspection.controlCheck = [NSNumber numberWithBool:_controlCheck];
    boilerInspection.electricalSafety = [NSNumber numberWithBool:_electricalSafety];
    boilerInspection.heatExchanger = [NSNumber numberWithBool:_heatExchanger];
    boilerInspection.hotWaterType = [NSNumber numberWithBool:_hotWaterType];
    boilerInspection.oilStorage = [NSNumber numberWithBool:_oilStorage];
    boilerInspection.oilSupplySystem = [NSNumber numberWithBool:_oilSupplySystem];
    boilerInspection.pressureJet = [NSNumber numberWithBool:_pressureJet];
    boilerInspection.vaporisingBurner = [NSNumber numberWithBool:_vaporisingBurner];
    boilerInspection.wallflameBurner = [NSNumber numberWithBool:_wallflameBurner];
    boilerInspection.warmAirType = [NSNumber numberWithBool:_warmAirType];
    
    boilerInspection.airSupplyDetail = isEmpty(_airSupplyDetail)?nil:_airSupplyDetail;
    boilerInspection.applianceSafetyDetail = isEmpty(_applianceSafetyDetail)?nil:_applianceSafetyDetail;
    boilerInspection.chimneyFlueDetail = isEmpty(_chimneyFlueDetail)?nil:_chimneyFlueDetail;
    boilerInspection.combustionChamberDetail = isEmpty(_combustionChamberDetail)?nil:_combustionChamberDetail;
    boilerInspection.controlCheckDetail = isEmpty(_controlCheckDetail)?nil:_controlCheckDetail;
    boilerInspection.electricalSafetyDetail = isEmpty(_electricalSafetyDetail)?nil:_electricalSafetyDetail;
    boilerInspection.heatExchangerDetail = isEmpty(_heatExchangerDetail)?nil:_heatExchangerDetail;
    boilerInspection.hotWaterTypeDetail = isEmpty(_hotWaterTypeDetail)?nil:_hotWaterTypeDetail;
    boilerInspection.oilStorageDetail = isEmpty(_oilStorageDetail)?nil:_oilStorageDetail;
    boilerInspection.oilSupplySystemDetail = isEmpty(_oilSupplySystemDetail)?nil:_oilSupplySystemDetail;
    boilerInspection.pressureJetDetail = isEmpty(_pressureJetDetail)?nil:_pressureJetDetail;
    boilerInspection.vaporisingBurnerDetail = isEmpty(_vaporisingBurnerDetail)?nil:_vaporisingBurnerDetail;
    boilerInspection.wallflameBurnerDetail = isEmpty(_wallflameBurnerDetail)?nil:_wallflameBurnerDetail;
    boilerInspection.warmAirTypeDetail = isEmpty(_warmAirTypeDetail)?nil:_warmAirTypeDetail;
    
    
    boilerInspection.firingServiceInspectionToOilHeating = self.boiler;
    self.boiler.oilHeatingToFireServiceInspection = boilerInspection;
    
    self.appointment.isModified = [NSNumber numberWithBool:YES];
    self.appointment.isSurveyChanged = [NSNumber numberWithBool:YES];
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
}

-(void) saveHeatingInspection{
    [self saveAllData];
    [self popOrCloseViewController];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue identifier] isEqualToString:SEG_OIL_FIRE_INSPECTION_TO_PHOTOGRAPHS]){
        [self saveAllData];
        PSOilPhotographsViewController * dest = (PSOilPhotographsViewController*)[segue destinationViewController];
        dest.rootVC = OilPhotoGridRootFireServicingInspection;
        dest.appointment = self.appointment;
        dest.altHeating = _boiler;
    }
}

@end
