//
//  PSOilSurveyHomeViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
@interface PSOilSurveyHomeViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet FBRImageView *imgViewProperty;
@property (weak, nonatomic) IBOutlet UIView *locationDetailView;
@property (weak, nonatomic) IBOutlet UILabel *lblTenantName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblCertificateExpiry;
@property (weak, nonatomic) IBOutlet UIView *issueCertificateView;
@property (weak, nonatomic) IBOutlet UIButton *btnIssueCertificate;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (strong,   nonatomic) Appointment *appointment;
@property (strong, nonatomic) PSBarButtonItem *noEntryButton;
@property (strong, nonatomic) PSBarButtonItem *abortButton;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property OilHeatingListDestination directionOfListSegue;
@property NSArray *cellTitles;
@end
