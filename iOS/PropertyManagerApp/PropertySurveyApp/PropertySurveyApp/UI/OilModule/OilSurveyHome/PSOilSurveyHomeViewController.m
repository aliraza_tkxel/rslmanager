//
//  PSOilSurveyHomeViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 04/09/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSOilSurveyHomeViewController.h"
#import "PSOilPhotographsViewController.h"
#import "PSOilIssueCertificateViewController.h"
#import "PSOilHeatingsListViewController.h"
#import "PSNoEntryViewController.h"
#import "PSAbortInspectionViewController.h"
#import "PSAlternateFuelServiceSurveyTableViewCell.h"

#define kTotalNumberOfRows 5
#define kRowApplianceBurnerTank 0
#define kRowOilFireServicingInspection 1
#define kRowPhotographs 2
#define kRowDefects 3
#define kRowIssueReceivedBy 4

@interface PSOilSurveyHomeViewController ()

@end

@implementation PSOilSurveyHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [self registerNotifications];
    [self configureIssueCertificateButton];
    [self initModule];
}

-(void) viewWillDisappear:(BOOL)animated{
    [self deRegisterNotifications];
}

#pragma mark - Init methods

-(void) configureIssueCertificateButton{
    if ([self checkInspectionStatus] && [self.appointment.appointmentToCP12Info.inspectionCarried boolValue])
    {
        [self.btnIssueCertificate setEnabled:YES];
        [self.btnIssueCertificate setBackgroundColor:[UIColor redColor]];
    }
    else
    {
        [self.btnIssueCertificate setEnabled:NO];
        [self.btnIssueCertificate setBackgroundColor:[UIColor darkGrayColor]];
    }
}

-(BOOL) checkInspectionStatus{
    BOOL isCompletelyInspected = YES;
    for(OilHeating *blr in self.appointment.appointmentToProperty.propertyToOilHeatings){
        if(![blr.isInspected boolValue]){
            isCompletelyInspected = NO;
            break;
        }
    }
    return isCompletelyInspected;
}
-(void) initModule{
    //[_btnIssueCertificate setEnabled:FALSE];
    [self loadNavigationonBarItems];
    [self.imgViewProperty setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
    _cellTitles = @[@"APPLIANCE/BURNER/TANK", @"OIL FIRING SERVICE & CHECKS", @"PHOTOGRAPHS",@"DEFECTS", @"ISSUE/RECEIVED BY"];
    
    Customer *customer = [self.appointment defaultTenant];
    self.lblTenantName.text = [customer fullName];
    
    Property *property = [self.appointment appointmentToProperty];
    if(!isEmpty(property.propertyId)){
        self.lblLocationAddress.text = [property addressWithStyle:PropertyAddressStyle1];
    }
    else if(!isEmpty(property.blockId)){
        self.lblLocationAddress.text = property.blockName;
    }
    else if(!isEmpty(property.schemeId)){
        self.lblLocationAddress.text = property.schemeName;
    }
    
    if (!isEmpty(self.appointment.appointmentToProperty.certificateExpiry))
    {
        [self.lblCertificateExpiry setHidden:NO];
        [self.lblCertificateExpiry setText:[NSString stringWithFormat:@"Certificate Expiry: %@",[UtilityClass stringFromDate:self.appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]]];
    }
    else
    {
        [self.lblCertificateExpiry setHidden:NO];
        [self.lblCertificateExpiry setText:@"Certificate Expiry: N/A"];
    }
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PSAlternateFuelServiceSurveyTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([PSAlternateFuelServiceSurveyTableViewCell class])];
    [self.tableView reloadData];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    if ([self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
    {
        self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
                                                                    style:PSBarButtonItemStyleDefault
                                                                   target:self
                                                                   action:@selector(onClickNoEntryButton)];
        
        self.abortButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_ABORT")
                                                                  style:PSBarButtonItemStyleAbort
                                                                 target:self
                                                                 action:@selector(onClickAbortButton)];
        
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.noEntryButton,self.abortButton, nil]];
    }
    
    [self setTitle:@"OIL"];
}

#pragma mark - Navbar methods

-(void) onClickAbortButton
{
    CLS_LOG(@"abort clicked");
    PSAbortInspectionViewController *abortVC = [[PSAbortInspectionViewController alloc] initWithNibName:@"PSAbortInspectionViewController" bundle:nil];
    abortVC.appointment = self.appointment;
    [self.navigationController pushViewController:abortVC animated:YES];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickNoEntryButton
{
    PSNoEntryViewController *noEntryViewController = [[PSNoEntryViewController alloc] initWithNibName:@"PSNoEntryViewController" bundle:nil];
    [noEntryViewController setAppointment:self.appointment];
    [self.navigationController pushViewController:noEntryViewController animated:YES];
    
    CLS_LOG(@"onClickNoEntryButton");
}


#pragma mark - Tableview methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    /*
     The count check ensures that if array is empty, i.e no record exists
     for particular section, the header height is set to zero so that
     section header is not alloted any space.
     */
    CGFloat headerHeight = 0.0;
    
    return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kTotalNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    PSAlternateFuelServiceSurveyTableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAlternateFuelServiceSurveyTableViewCell class])];
    [self configureCell:_cell atIndexPath:indexPath];
    cell = _cell;
    cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[PSAlternateFuelServiceSurveyTableViewCell class]])
    {
        
        PSAlternateFuelServiceSurveyTableViewCell *_cell = (PSAlternateFuelServiceSurveyTableViewCell *) cell;
        [_cell.lblItemName setText:[_cellTitles objectAtIndex:indexPath.row]];
        _cell.lblItemName.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblItemName.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        BOOL isInspected = [self checkInspectionStatus];
        BOOL isCertIssued = [self.appointment.appointmentToCP12Info.inspectionCarried boolValue];
        _cell.imgViewStatus.image = [UIImage imageNamed:@"icon_Cross"];
        
        if (isInspected && indexPath.row==kRowOilFireServicingInspection)
        {
            _cell.imgViewStatus.image = [UIImage imageNamed:@"icon_Complete"];
        }
        if(isCertIssued && indexPath.row == kRowIssueReceivedBy)
        {
            _cell.imgViewStatus.image = [UIImage imageNamed:@"icon_Complete"];
        }
        
        if(indexPath.row!=kRowOilFireServicingInspection && indexPath.row!=kRowIssueReceivedBy){
            _cell.imgViewStatus.hidden = YES;
        }
        else
        {
            _cell.imgViewStatus.hidden = NO;
        }
        cell = _cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == kRowApplianceBurnerTank){
        _directionOfListSegue=OilHeatingListDestinationApplianceBurnerTank;
        [self performSegueWithIdentifier:SEG_OIL_SURVEY_TO_HEATING_LIST sender:self];
    }
    else if(indexPath.row == kRowOilFireServicingInspection){
        _directionOfListSegue = OilHeatingListDestinationFireServiceInspection;
        [self performSegueWithIdentifier:SEG_OIL_SURVEY_TO_HEATING_LIST sender:self];
    }
    else if(indexPath.row==kRowPhotographs){
        [self performSegueWithIdentifier:SEG_OIL_SURVEY_TO_PHOTOGRAPHS sender:self];
    }
    else if(indexPath.row==kRowDefects){
        _directionOfListSegue = OilHeatingListDestinationDefects;
        [self performSegueWithIdentifier:SEG_OIL_SURVEY_TO_HEATING_LIST sender:self];
    }
    else if(indexPath.row==kRowIssueReceivedBy){
        [self performSegueWithIdentifier:SEG_OIL_SURVEY_TO_ISSUE_CERTIFICATE sender:self];
    }
    
}




#pragma mark - Actions
- (IBAction)btnBackTapped:(id)sender {
    [self onClickBackButton];
}


#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentSuccessNotificationReceive) name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentFailureNotificationReceive) name:kCompleteAppointmentFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentFailureNotification object:nil];
}


- (void) onCompleteAppointmentSuccessNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

- (void) onCompleteAppointmentFailureNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        self.appointment.appointmentStatus = kAppointmentStatusInProgress;
        
        [self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
                         message:LOC(@"KEY_ALERT_FAILED_TO_COMPLETE_APPOINTMENT")
                             tag:0
               cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
               otherButtonTitles:nil];
    });
}

- (IBAction)btnIssueCertificateTapped:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOC(@"KEY_ALERT_COMPLETE_APPOINTMENT") message:LOC(@"KEY_ALERT_CONFIRM_COMPLETE_APPOINTMENT") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self completeAppointment];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_NO") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(BOOL) checkIfCanFail{
    BOOL isInspected = [self checkInspectionStatus];
    BOOL isCertIssued = [self.appointment.appointmentToCP12Info.inspectionCarried boolValue];
    if (!isInspected
        || !isCertIssued)
    {
        [self showAlertWithTitle:@"Fail Certificate" message:@"You cannot fail the certificate until your inspection is not complete." tag:0 cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        return  NO;
    }
    return YES;
}

- (IBAction)btnFailTapped:(id)sender {
    if([self checkIfCanFail]){
        NSString * finalFailString = @"";
        for(OilHeating *heating in self.appointment.appointmentToProperty.propertyToOilHeatings){
            for(Defect *defect in heating.oilHeatingToDefect){
                finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,defect.defectCategory.categoryDescription];
            }
            if(!isEmpty(heating.oilHeatingToFireServiceInspection)){
                if([heating.oilHeatingToFireServiceInspection.airSupply boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Air Supply Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.applianceSafety boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Appliance Safety Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.chimneyFlue boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Chimney Flue Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.combustionChamber boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Combustion Chamber Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.controlCheck boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Control Check Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.electricalSafety boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Electrical Safety Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.heatExchanger boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Heat Exchanger Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.hotWaterType boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Hot water type Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.oilStorage boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Oil storage Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.oilSupplySystem boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Oil Supply System Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.pressureJet boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Pressure Jet Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.vaporisingBurner boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Vaporising burner Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.wallflameBurner boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Wallflame burner Passed = NO"];
                }
                if([heating.oilHeatingToFireServiceInspection.warmAirType boolValue]==NO){
                    finalFailString = [NSString stringWithFormat:@"%@\n-%@",finalFailString,@"Warm air type Passed = NO"];
                }
            }
        }
        finalFailString = [NSString stringWithFormat:@"The heating has failed the annual inspeciton due to the following defects: \n\n%@",finalFailString];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Fail Certificate." message:finalFailString preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            self.appointment.appointmentToCP12Info.cp12Passed = [NSNumber numberWithBool:NO];
            [self.appointment.managedObjectContext save:nil];
            
            self.appointment.appointmentStatus = [UtilityClass statusStringForAppointmentStatus:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]]];
            [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]] appointment:self.appointment];
            [self popToRootViewController];

        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    } //CanFail Ends here
}

-(void) completeAppointment{
    self.appointment.appointmentToCP12Info.cp12Passed = [NSNumber numberWithBool:YES];
    [self.appointment.managedObjectContext save:nil];
    
    self.appointment.appointmentStatus = [UtilityClass statusStringForAppointmentStatus:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]]];
    [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]] appointment:self.appointment];
    [self popToRootViewController];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue identifier] isEqualToString:SEG_OIL_SURVEY_TO_HEATING_LIST]){
        PSOilHeatingsListViewController *dest = (PSOilHeatingsListViewController *)[segue destinationViewController];
        dest.appointment = self.appointment;
        dest.destinationVC = _directionOfListSegue;
    }
    else if([[segue identifier] isEqualToString:SEG_OIL_SURVEY_TO_ISSUE_CERTIFICATE]){
        PSOilIssueCertificateViewController *dest = (PSOilIssueCertificateViewController *)[segue destinationViewController];
        dest.appointment = self.appointment;
        dest.cp12Info = self.appointment.appointmentToCP12Info;
    }
    else if([[segue identifier] isEqualToString:SEG_OIL_SURVEY_TO_PHOTOGRAPHS]){
        PSOilPhotographsViewController *dest = (PSOilPhotographsViewController *)[segue destinationViewController];
        dest.appointment = self.appointment;
        dest.rootVC = OilPhotoGridRootProperty;
    }
}

-(BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:SEG_OIL_SURVEY_TO_ISSUE_CERTIFICATE]){
        if(![self checkInspectionStatus]){
            [self showMessageWithHeader:@"Alert" andBody:@"Please inspect all heatings first"];
            return NO;
        }
    }
    return YES;
}
@end
