//
//  PSOilIssueCertificateViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"
@class PSReceivedInfoCell;
@class PSReceivedPickerCell;
@class PSDatePickerCell;
@class PSConfirmationCell;

@protocol PSIssueReceivedProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelectDateTime Called upon selection of Date picker option.
 */
- (void) didSelectDate:(NSDate *)pickerOption;

/*!
 @discussion
 Method didSelectReceivedPickerOption Called upon selection of Received on behalf of picker option.
 */
- (void) didSelectReceivedPickerOption:(NSString *)pickerOption;

/*!
 @discussion
 Method didSelectConfirmationBtn Called upon clicking confirmation button.
 */
- (void) didSelectConfirmationBtn:(BOOL )pickerOption;

@end
@interface PSOilIssueCertificateViewController :PSCustomViewController<PSIssueReceivedProtocol,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong,   nonatomic) CP12Info *cp12Info;
@property (strong,   nonatomic) Appointment *appointment;
@property (weak,   nonatomic) IBOutlet PSReceivedInfoCell *infoCell;
@property (weak,   nonatomic) IBOutlet PSReceivedPickerCell *pickerCell;
@property (weak,   nonatomic) IBOutlet PSDatePickerCell *dateCell;
@property (weak,   nonatomic) IBOutlet PSConfirmationCell *confirmationCell;

@end
