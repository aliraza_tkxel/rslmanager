//
//  PSSurveyorsViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyorsViewController.h"
#import "PSSurveyorCell.h"
#import "NSFetchedResultsControllerHelper.h"

#define kAppointmentViewSectionHeaderHeight 20

@interface PSSurveyorsViewController ()
{
	BOOL _reloadingTable;
    BOOL _noResultsFound;
    BOOL _firstTime;
    BOOL _isSearching;
}
@end


@implementation PSSurveyorsViewController
@synthesize searchString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    _noResultsFound = NO;
    _firstTime = YES;
    _isSearching = NO;
    
    
    if(OS_VERSION < 7.0)
    {
        CGRect frame = self.searchBarView.frame;
        self.searchBarView.frame = CGRectMake(frame.origin.x,
                                              frame.origin.y,
                                              frame.size.width,
                                              44);
        
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                          self.searchBarView.frame.origin.y + self.searchBarView.frame.size.height,
                                          self.tableView.frame.size.width,
                                          self.tableView.frame.size.height);
    }
    else
    {
        //Change the Section Index Backgorund Color
        self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        self.tableView.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
    }
    
    self.view.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    self.txtSearch.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.txtSearch.delegate = self;
    
    
    [self performFetchOnNSFetchedResultsController];
    if([[self.fetchedResultsController fetchedObjects] count] <= 0)
    {
        
        [self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
                                     LOC(@"KEY_ALERT_LOADING"),
                                     LOC(@"KEY_STRING_SURVEYORS")]];
    }
    [[PSSurveyorsManager sharedManager] fetchAllSurveyors];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotifications];
    self.navigationController.navigationBar.hidden = YES;
    [self.navigationItem setHidesBackButton:YES animated:NO];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
     [self deRegisterNotificaitons];
    _noResultsFound = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self setTxtSearch:nil];
}

#pragma mark - UITableView Delegate Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *surveyorCellIdentifier = @"surveyorCellIdentifier";
    PSSurveyorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:surveyorCellIdentifier];
    
    
    if(cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"PSSurveyorCell" owner:self options:nil];
        cell = self.surveyorCell;
        self.surveyorCell = nil;
    }
    [self configureCell:&cell atIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)configureCell:(PSSurveyorCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    _firstTime = NO;

    Surveyor *surveyor;
    if (_noResultsFound)
    {
        (*cell).lblSurveyorName.text = LOC(@"KEY_STRING_NO_RESULTS_FOUND");
        (*cell).lblSurveyorName.textAlignment = NSTextAlignmentCenter;
        (*cell).imgSeperator.hidden = YES;
        (*cell).lblSurveyorName.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        (*cell).selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else
    {
        surveyor = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        if (!isEmpty(surveyor))
        {
            (*cell).lblSurveyorName.text = surveyor.fullName;
            (*cell).lblSurveyorName.textAlignment = NSTextAlignmentLeft;
            (*cell).imgSeperator.hidden = NO;
            (*cell).lblSurveyorName.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 33;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    int sections = 0;
    if(_noResultsFound)
    {
        sections = 1; //For Showing the No Results Found
    }
    else
    {
        sections = [[self.fetchedResultsController sections] count];
    }
    return sections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSInteger heightForHeader = 0;
    if (!_noResultsFound && !_isSearching) {
        heightForHeader = kAppointmentViewSectionHeaderHeight;
    }
    return heightForHeader;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *header = nil;
    if (!_noResultsFound && !_isSearching) {
        
        id <NSFetchedResultsSectionInfo> sectionInfo;
        sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        header = [sectionInfo indexTitle];
    }
    return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numberOfRows;
    if(_noResultsFound)
    {
        numberOfRows = 1; //For Showing the No Results Found
    }
    else
    {
        id <NSFetchedResultsSectionInfo> sectionInfo;
        sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    return numberOfRows;
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(_isSearching)
    {
        return nil;
    }
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if(_isSearching)
    {
        return -1;
    }
    return [self.fetchedResultsController.sectionIndexTitles indexOfObject:title];
}


#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource
{
	_reloadingTable = YES;
    [self.tableView setUserInteractionEnabled:NO];
    [self.tableView reloadData];
}

- (void)doneLoadingTableViewData
{
	_reloadingTable = NO;
    [self.tableView setUserInteractionEnabled:YES];
    [self.tableView reloadData];
}


#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    _isSearching = YES;
    self.selectedControl = (UITextField *)textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (isEmpty(self.searchString))
    {
        _isSearching = NO;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (isEmpty(self.searchString))
    {
        _isSearching = NO;
    }
	[textField resignFirstResponder];
    [self.tableView reloadData];
	return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //  BOOL isBackspaceHit = NO;
    NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
    
    if([string isEqualToString:@""] && !isEmpty(textField.text)) //Handle Backspace Here
    {
        // isBackspaceHit = YES;
        stringAfterReplacement = [textField.text substringToIndex:textField.text.length - 1];
    }
    if (isEmpty(stringAfterReplacement))
    {
        _isSearching = NO;
    }
    else
    {
        _isSearching = YES;
    }
    self.searchString = stringAfterReplacement;
    [self FilterSurveyorsWithString];
    CLS_LOG(@"string after replacement: %@",stringAfterReplacement);
    return YES;
}


#pragma mark - Fetched results controller
- (void) performFetchOnNSFetchedResultsController
{
    CLS_LOG(@"performFetchOnNSFetchedResultsController");
    NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		CLS_LOG(@"fetchedResultsController, Unresolved error %@, %@", error, [error userInfo]);
	}
    if (!_firstTime) {
        [self checkResultsEmptiness];
    }
    // [self checkResultsEmptiness];
    [self.tableView reloadData];
}

- (void) checkResultsEmptiness
{
    if([[self.fetchedResultsController fetchedObjects] count] <= 0)
    {
        _noResultsFound = YES;
    }
    else
    {
        _noResultsFound = NO;
    }
    //#warning PSA: FIX NO RESULTS YES BUG HERE (ON RESET SIMULATOR)
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil)
    {
        if (!isEmpty(self.searchString))
        {
            self.surveyorsFilterPredicate = [NSPredicate predicateWithFormat:@"SELF.fullName CONTAINS[cd] %@", self.searchString];
            [self.fetchRequest setPredicate:self.surveyorsFilterPredicate];

        }
        else
        {
            self.surveyorsFilterPredicate = nil;
            [self.fetchRequest setPredicate:self.surveyorsFilterPredicate];
        }
        return _fetchedResultsController;
    }
    
    self.fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:kSurveyor inManagedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext];
    [self.fetchRequest setEntity:entity];
    self.fetchRequest.returnsDistinctResults = YES;
    // Set the batch size to a suitable number.
    [self.fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    //   NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kFullName ascending:YES];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:kFullName
                                        ascending:YES
                                        selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [self.fetchRequest setSortDescriptors:sortDescriptors];
    [self.fetchRequest setReturnsDistinctResults:YES];

    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:self.fetchRequest managedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext sectionNameKeyPath:kSurveyorNameSectionIdentifier cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	    CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    if (!_firstTime) {
        [self checkResultsEmptiness];
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //[[self tableView ]cellForRowAtIndexPath:indexPath];
            //[self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            [tableView cellForRowAtIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSurveyorsSaveSuccessNotification) name:kSurveyorSaveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSurveyorsSaveFailureNotification) name:kSurveyorSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveConnectivityChangeNotification) name:NOTIF_NO_CONNECTIVITY object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveNoConnectivityNotification) name:kInternetUnavailable object:nil];
}

- (void) deRegisterNotificaitons
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSurveyorSaveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSurveyorSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIF_NO_CONNECTIVITY object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInternetUnavailable object:nil];
}

- (void) onReceiveSurveyorsSaveSuccessNotification
{
    [self hideActivityIndicator];
    [self performFetchOnNSFetchedResultsController];
    if(_reloadingTable)
    {
        [self doneLoadingTableViewData];
    }
    else
    {
        [self.tableView reloadData];
    }
}

- (void) onReceiveSurveyorsSaveFailureNotification
{
//    [self hideActivityIndicator];
//
//    [self.tableView reloadData];
    [self onReceiveNoConnectivityNotification];
}

- (void) onReceiveNoConnectivityNotification
{
    [self hideActivityIndicator];
    [self checkResultsEmptiness];
    [self.tableView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicator stopAnimating];
        if (_noResultsFound)
        {
            
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                           message:LOC(@"KEY_ALERT_NO_INTERNET")
                                                          delegate:self
                                                 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                 otherButtonTitles:nil,nil];
            alert.tag = AlertViewTagNoInternet;
            
            [alert show];
        }
    });
}

- (void) onReceiveConnectivityChangeNotification
{
    [self hideActivityIndicator];
    [self checkResultsEmptiness];
    [self.tableView reloadData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicator stopAnimating];
    });
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewTagNoInternet)
    {
        if (_noResultsFound)
        {
            // [self popOrCloseViewController];
        }
    }
}

#pragma mark - Core Data Update Events
- (void) updateSurveyorDetailOnSurveyorUpdate
{/*
  if (self.appointmentDetailViewConroller != nil)
  {
  [self.appointmentDetailViewConroller onAppointmentObjectUpdate];
  }*/
}


#pragma mark - IBActions

- (IBAction)onClickCancelSearch:(id)sender
{
    CLS_LOG(@"onClickCancelSearch");
    [(UITextField *)self.selectedControl setText:@""];
    [self resignAllResponders];
    [self popOrCloseViewController];
}

- (IBAction)onClickSearch:(id)sender
{
    CLS_LOG(@"onClickSearch");
}

- (IBAction)resignFirstResponder:(id)sender{
    [self resignAllResponders];
}

- (IBAction)onClickClearSearch:(id)sender
{
    CLS_LOG(@"onClickClearSearchs");
    NSString *searchFieldText;
    if (!isEmpty(((UITextField *)self.selectedControl).text))
    {
        searchFieldText = [((UITextField *)self.selectedControl).text substringToIndex:((UITextField *)self.selectedControl).text.length -1];
        self.txtSearch.text = searchFieldText;
        self.searchString = searchFieldText;
        _isSearching = !isEmpty(searchFieldText);
    }

    [self FilterSurveyorsWithString];
}

#pragma mark - Filter/Search Methods
- (void) FilterSurveyorsWithString
{
  //  self.fetchedResultsController = nil;
    [self performFetchOnNSFetchedResultsController];
}


@end
