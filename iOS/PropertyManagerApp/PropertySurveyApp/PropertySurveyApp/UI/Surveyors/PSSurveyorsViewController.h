//
//  PSSurveyorsViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSSurveyorCell;
@interface PSSurveyorsViewController : PSCustomViewController <UITextFieldDelegate,  NSFetchedResultsControllerDelegate>
@property (strong,  nonatomic) NSPredicate *surveyorsFilterPredicate;
@property (strong,  nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong,  nonatomic) NSFetchRequest *fetchRequest;
@property (strong,  nonatomic) IBOutlet PSSurveyorCell *surveyorCell;
@property (strong,  nonatomic) NSMutableArray *filteredArray;
@property (strong,  nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UIView *searchBarView;

@property (strong,  nonatomic) NSString *searchString;
- (IBAction)onClickCancelSearch:(id)sender;
- (IBAction)onClickSearch:(id)sender;
- (IBAction)resignFirstResponder:(id)sender;
- (IBAction)onClickClearSearch:(id)sender;

@end
