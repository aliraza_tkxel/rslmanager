//
//  PSSurveyorCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSurveyorCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblSurveyorName;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator;

@end
