//
//  TypeCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVPreTypeCell.h"

@implementation PSVPreTypeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(id)data
{
	[self setAppointment:data];
	self.lblDetail.text = self.appointment.appointmentType;
	self.lblDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblDetail.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	[self.imgStatus setHidden:YES];
	self.selectionStyle = UITableViewCellSelectionStyleGray;
}

@end
