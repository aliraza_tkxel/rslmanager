//
//  PSFaultAppointmentPropertyCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVPreAppointmentPropertyCell : UITableViewCell

@property (strong, nonatomic) IBOutlet FBRImageView *imgProperty;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblAssignedBy;
@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStarts;
@property (strong, nonatomic) IBOutlet UILabel *lblEnds;
@property (strong, nonatomic) IBOutlet UILabel *lblTerminationDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEndTime;
@property (strong, nonatomic) IBOutlet UILabel *lblReletDate;
@property (weak,   nonatomic) Appointment *appointment;

-(void) setAddressGestureRecognizer:(id)owner action:(SEL)action;
-(void) setImagePropertyGestureRecognizer:(id)owner action:(SEL)action;
-(void) configureCell:(id)data;
@end
