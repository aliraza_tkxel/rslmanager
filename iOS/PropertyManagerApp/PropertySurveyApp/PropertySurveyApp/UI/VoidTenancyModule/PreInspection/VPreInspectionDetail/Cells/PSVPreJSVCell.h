//
//  PSJSNumberCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVPreJSVCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblFaultDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblClickHereJobSheet;

@property (weak,   nonatomic) Appointment *appointment;

-(void)configureCell:(id)data;

@end
