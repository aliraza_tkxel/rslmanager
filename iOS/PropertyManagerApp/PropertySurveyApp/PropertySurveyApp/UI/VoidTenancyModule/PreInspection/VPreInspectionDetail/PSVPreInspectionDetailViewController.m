//
//  PSAppointmentDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVPreInspectionDetailViewController.h"
#import "PSVPreAppointmentPropertyCell.h"
#import "PSVPreDetailNotesCell.h"

#import "PSAppointmentNotesViewController.h"
#import "PSTenantDetailViewController.h"
#import "PSVPreJSVInspectionViewController.h"
#import "PSNoEntryNoCardViewController.h"
#import "PSSynchronizationManager.h"

#import "PSVPreTypeCell.h"
#import "PSVPreTypeCell.h"
#import "PSVPreAsbestosCell.h"
#import "PSVPreTenantCell.h"
#import "PSVPreJSVCell.h"
#import "PSMapsViewController.h"
#import "PSImageViewController.h"
#import "VoidData.h"

#define kNumberOfSections 6

#define kSectionProperty 0
#define kSectionType 1
#define kSectionTenant 2
#define kSectionJSV 3
#define kSectionNotes 4
#define kSectionAsbestos 5

#define kSectionPlannedComponentRows 1
#define kSectionPropertyRows 1
#define kPropertyCellRow 0

#define kSectionTypeRows 1
#define kTypeCellRow 0

#define kTenantCellRow 2

#define kPropertyCellHeight 80
#define kFaultPropertyCellHeight 160
#define kFaultSchemeCellHeight 134

#define kTypeCellHeight 29
#define kTenantCellHeight 98
#define kAsbestosCellHeight 48
#define kJSNumberCellHeight 65
#define kAccomodationCellHeight 39
#define kNotesCellHeight 150

#define kDownloadSurveyAlertView 1

#define kViewSectionHeaderHeight 20


@implementation PSVPreInspectionDetailViewController

@synthesize headerViewArray;
@synthesize appointment;
@synthesize property;
@synthesize tenantsArray;
@synthesize jobDataListArray;
@synthesize asbestosArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		self.isEditing = YES;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.headerViewArray = [NSMutableArray array];
	self.tenantsArray = [NSMutableArray arrayWithArray:[appointment.appointmentToCustomer allObjects]];
	self.jobDataListArray =  [NSMutableArray arrayWithArray:[appointment.appointmentToJobSheet allObjects]];
	self.property = appointment.appointmentToProperty;
	self.asbestosArray =  [NSMutableArray arrayWithArray:[property.propertyToPropertyAsbestosData allObjects]];
	[self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	[self loadNavigationonBarItems];
	[self addTableHeaderView];
	
	[self registerNibsForTableView];
	
	[self loadSectionHeaderViews:@[LOC(@"KEY_STRING_PROPERTY"),LOC(@"KEY_STRING_TYPE"),LOC(@"KEY_STRING_TENANT"),LOC(@"KEY_STRING_JSV"),LOC(@"KEY_STRING_CUSTOMER_NOTES"),LOC(@"KEY_STRING_ASBESTOS")] headerViews:self.headerViewArray];
}

-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSVPreDetailNotesCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVPreAppointmentPropertyCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVPreAsbestosCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVPreJSVCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVPreTenantCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVPreTypeCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self registerNotifications];
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.notesButton, self.noEntryButton, nil]];
	if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
			|| [self.appointment getStatus] == AppointmentStatusNoEntry)
	{
		[self.noEntryButton setEnabled:NO];
		[self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
	}
	else
	{
		[self.noEntryButton setEnabled:YES];
	}
	
	[self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self deRegisterNotifications];
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentStartNotificationReceive) name:kAppointmentStartNotification object:nil];
    
}
- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentStartNotification object:nil];
}

-(void) onAppointmentStartNotificationReceive{
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
																															style:PSBarButtonItemStyleDefault
																														 target:self
																														 action:@selector(onClickNoEntryButton)];
	self.notesButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														style:PSBarButtonItemStyleNotes
																													 target:self
																													 action:@selector(onClickNotesButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.notesButton, self.noEntryButton, nil]];
	if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
			|| [self.appointment getStatus] == AppointmentStatusNoEntry)
	{
			[self.noEntryButton setEnabled:NO];
	}
	else
	{
			[self.noEntryButton setEnabled:YES];
	}
	
	[self setTitle:[appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[[PSDatabaseContext sharedContext] saveContext];
	[self popOrCloseViewController];
}

- (void) onClickNoEntryButton
{
	CLS_LOG(@"onClickNoEntryButton");
	PSNoEntryNoCardViewController * noEntryView = [[PSNoEntryNoCardViewController alloc] initWithNibName:NSStringFromClass([PSNoEntryNoCardViewController class])
																																																bundle:[NSBundle mainBundle]];
	[noEntryView setAppointment:self.appointment];
	[self.navigationController pushViewController:noEntryView animated:YES];
}

- (void) onClickNotesButton
{
	CLS_LOG(@"onClickNotesButton");
	PSAppointmentNotesViewController * notesView = [[PSAppointmentNotesViewController alloc] initWithNibName:NSStringFromClass([PSAppointmentNotesViewController class])
																																																		bundle:[NSBundle mainBundle]];
	[notesView setAppointment:self.appointment];
	[self.navigationController pushViewController:notesView animated:YES];
}
#pragma mark - IBActions
- (IBAction)onClickStartAppoiontmentButton:(id)sender
{
	[self.appointment startAppointment];
	[self hideTableHeaderView];
}
- (IBAction)onClickAcceptAppoiontmentButton:(id)sender
{
    [self.appointment acceptAppointment];
    [_btnStartAppointment removeTarget:self action:@selector(onClickAcceptAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    [_btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
    [_btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	NSInteger numberOfSelections = kNumberOfSections;
	return numberOfSelections;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	/*
	 The count check ensures that if array is empty, i.e no record exists
	 for particular section, the header height is set to zero so that
	 section header is not alloted any space.
	 */
	
	CGFloat headerHeight = 0.0;
	
	if(section == kSectionJSV)
	{
		if ([self.jobDataListArray count])
		{
			headerHeight = 1;
		}
		else
		{
			headerHeight = 1;
		}
		headerHeight = kViewSectionHeaderHeight;
	}
	else if (section == kSectionAsbestos)
	{
		headerHeight = kViewSectionHeaderHeight;
	}
	else if (section == kSectionTenant)
	{
		if([self.tenantsArray count] > 0)
		{
			headerHeight = kViewSectionHeaderHeight;
		}
		headerHeight = kViewSectionHeaderHeight;
	}
	else if (section == kSectionNotes)
	{
		headerHeight = kViewSectionHeaderHeight;
	}
	else
	{
		headerHeight = kViewSectionHeaderHeight;
	}
	return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0.0;
	if(indexPath.section == kSectionProperty)
	{
		rowHeight = kFaultPropertyCellHeight;
	}
	else if (indexPath.section == kSectionType)
	{
		rowHeight = kTypeCellHeight;
	}
	else if (indexPath.section == kSectionTenant)
	{
		rowHeight = kTenantCellHeight;
	}
	else if (indexPath.section == kSectionAsbestos)
	{
		rowHeight = kAsbestosCellHeight;
	}
	else if (indexPath.section == kSectionJSV)
	{
		rowHeight = kJSNumberCellHeight;
	}
	else if (indexPath.section == kSectionNotes)
	{
		if (!isEmpty(self.appointment.appointmentNotes))
		{
			rowHeight = [self heightForTextView:nil containingString:self.appointment.appointmentNotes];
			rowHeight += 25;
			if (rowHeight < 50)
			{
				rowHeight = 50;
			}
		}
		else
		{
			rowHeight = 50;
		}
	}
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows=0;
	if (section == kSectionProperty)
	{
		numberOfRows = kSectionPropertyRows;
	}
	else if (section == kSectionType)
	{
		numberOfRows = kSectionTypeRows;
	}
	else if (section == kSectionTenant)
	{
		return [self.tenantsArray count];
	}
	else if (section == kSectionAsbestos)
	{
		if (!isEmpty(self.asbestosArray))
		{
			numberOfRows = [self.asbestosArray count];
		}
		else
		{
			numberOfRows = 1;
		}
	}
	else if (section == kSectionJSV)
	{
		numberOfRows = 1;
		[self.jobDataListArray count]; //TODO
	}
	else if (section == kSectionNotes)
	{
		numberOfRows = 1;
	}
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *_cell = nil;
	if (indexPath.section == kSectionProperty)
	{
		PSVPreAppointmentPropertyCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPreAppointmentPropertyCell class])];
		[cell configureCell:self.appointment];
		[cell setAddressGestureRecognizer:self action:@selector(onClickAddress:)];
		[cell setImagePropertyGestureRecognizer:self action:@selector(onClickImageProperty:)];
		_cell = cell;
	}
	else if (indexPath.section == kSectionType)
	{
		PSVPreTypeCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPreTypeCell class])];
		[cell configureCell:appointment];
		_cell = cell;
	}
	else if (indexPath.section == kSectionTenant)
	{
		PSVPreTenantCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPreTenantCell class])];
        if(self.tenantsArray.count > 0)
        {
            [cell configureCell:[self.tenantsArray objectAtIndex:0]];
        }
		
		[cell setTelephoneTarget:self Action:@selector(onClickTelephoneButton:)];
		[cell setMobileTarget:self Action:@selector(onClickMobileButton:)];
		[cell setDisclosureIndicatorTarget:self Action:@selector(onClickDisclosureButton:)];
		[cell setEditTenantTarget:self Action:@selector(onClickEditTenantButton:)];
		_cell = cell;
	}
	else if (indexPath.section == kSectionAsbestos)
	{
		PSVPreAsbestosCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPreAsbestosCell class])];
		[cell configureCell:self.asbestosArray :indexPath.row];
		_cell = cell;
	}
	else if (indexPath.section == kSectionJSV)
	{
		PSVPreJSVCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPreJSVCell class])];
		[cell configureCell:self.appointment];
		_cell = cell;
	}
	else if (indexPath.section == kSectionNotes)
	{
		PSVPreDetailNotesCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPreDetailNotesCell class])];
		[cell configureCell:self.appointment];
		_cell = cell;
	}
	_cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	return _cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if (indexPath.section == kSectionJSV)
	{
        if([self.appointment getStatus] != AppointmentStatusNotStarted){
            if([self.appointment getStatus] != AppointmentStatusAccepted)
            {
                UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_PreInspection bundle:nil];
                PSVPreJSVInspectionViewController * jobScreen = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSVPreJSVInspectionViewController class])];
                [jobScreen setAppointment:self.appointment];
                [self.navigationController pushViewController:jobScreen animated:YES];
            }
            else{
                UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_ALERT")
                                                               message:LOC(@"KEY_READ_ONLY_JOB_SHEET_DENIED")
                                                              delegate:nil
                                                     cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                     otherButtonTitles:nil, nil];
                [alert show];
                
            }

        }
        
	}
}

#pragma mark - Edit Tenant Button Selector
- (IBAction)onClickEditTenantButton:(id)sender
{
	CLS_LOG(@"onClickEditAddressButton");
	
	UIButton *btnEditTenant = (UIButton *) sender;
	Customer *customer = [self.tenantsArray objectAtIndex:btnEditTenant.tag];
	NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
	if (customer != nil)
	{
		PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:NSStringFromClass([PSTenantDetailViewController class]) bundle:[NSBundle mainBundle]];
		[tenantDetailViewConroller setAddress:address];
		[tenantDetailViewConroller setTenant:customer];
		[tenantDetailViewConroller setEditing:YES];
		[self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
	}
}

- (IBAction)onClickDisclosureButton:(id)sender
{
	UIButton *btnDisclosure = (UIButton *) sender;
	Customer *customer = [self.tenantsArray objectAtIndex:btnDisclosure.tag];
	NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
	if (customer != nil)
	{
		PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:NSStringFromClass([PSTenantDetailViewController class]) bundle:[NSBundle mainBundle]];
		[tenantDetailViewConroller setAddress:address];
		[tenantDetailViewConroller setTenant:customer];
		[self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
	}
}

- (IBAction)onClickTelephoneButton:(id)sender
{
	UIButton *btnTel = (UIButton *)sender;
	Customer *customer = [self.tenantsArray objectAtIndex:btnTel.tag];
	[self setupDirectCallToNumber:customer.telephone];
}

- (IBAction)onClickMobileButton:(id)sender
{
	// [self setupDirectCallToNumber:self.customer.mobile];
	UIButton *btnMob = (UIButton *)sender;
	Customer *customer = [self.tenantsArray objectAtIndex:btnMob.tag];
	[self setupDirectCallToNumber:customer.mobile];
}

- (void) setupDirectCallToNumber:(NSString *)number
{
	if (!isEmpty(number))
	{
		number = [UtilityClass getValidNumber:number];
		CLS_LOG(@"Direct Call Number Verified: %@", number);
		NSString *url = [@"tel:" stringByAppendingString:number];
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
	}
}

#pragma mark - Core Data Update Events
- (void) onAppointmentObjectUpdate
{
	dispatch_async(dispatch_get_main_queue(), ^{
		if([self isViewLoaded] && self.view.window)
		{
			[self.tableView reloadData];
		}
	});
}

- (void)popOrCloseViewController
{
	[super popOrCloseViewController];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(300, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(300, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	return height;
}

#pragma mark - Methods

- (void) disableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons)
	{
		button.enabled = NO;
	}
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = NO;
	}
}

- (void) enableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons)
	{
		button.enabled = YES;
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = YES;
	}
}

- (void) addTableHeaderView
{
    CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
	UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
	headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	
	UILabel *lbl = [[UILabel alloc] init];
	lbl.frame = CGRectMake(8, 0, 245, 29);
	lbl.center = CGPointMake(lbl.center.x, headerView.center.y);
	lbl.backgroundColor = [UIColor clearColor];
	[lbl setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[lbl setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[lbl setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
	lbl.text =LOC(@"KEY_STRING_APPOINTMENT_START_MESSAGE");
	
	_btnStartAppointment = [[UIButton alloc]init];
	_btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
	_btnStartAppointment.center = CGPointMake(_btnStartAppointment.center.x, headerView.center.y);
	
	[_btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
	[_btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:UIControlStateNormal];
    if([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame){
        [_btnStartAppointment setTitle:LOC(@"KEY_STRING_ACCEPT") forState:UIControlStateNormal];
        [_btnStartAppointment addTarget:self action:@selector(onClickAcceptAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if([self.appointment.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame){
        [_btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
        [_btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    }

	
	[headerView addSubview:lbl];
	[headerView addSubview:_btnStartAppointment];
	
	if ([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame|| [self.appointment.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame)
	{
		headerViewRect = self.tableView.tableHeaderView.frame;
		headerViewRect.size.height = 40;
		headerViewRect.size.width = 320;
		[self.tableView.tableHeaderView setFrame:headerViewRect];
		[self.tableView setTableHeaderView:headerView];
	}
}

- (void) hideTableHeaderView
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.5];
	[self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
	self.tableView.tableHeaderView = nil;
	[self.tableView.tableHeaderView setHidden:YES];
	//[self.tableView reloadData];
	[UIView commitAnimations];
}

- (IBAction)onClickAddress:(UIGestureRecognizer *)sender
{
	// Code to respond to gesture here
	PSMapsViewController *mapViewController = [[PSMapsViewController alloc] initWithNibName:NSStringFromClass([PSMapsViewController class]) bundle:[NSBundle mainBundle]];
	[mapViewController setAddress:[self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull]];
	[mapViewController setNavigationTitle:[self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
	[self.navigationController pushViewController:mapViewController animated:YES];
}

- (IBAction)onClickImageProperty:(UIGestureRecognizer *)sender
{
	// Code to respond to gesture here
	PSImageViewController *imageViewController = [[PSImageViewController alloc] initWithNibName:NSStringFromClass([PSImageViewController class]) bundle:[NSBundle mainBundle]];
	imageViewController.propertyObj=self.appointment.appointmentToProperty;
	[self.navigationController pushViewController:imageViewController animated:YES];
}

//
@end
