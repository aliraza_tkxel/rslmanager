//
//  PSFaultAppointmentPropertyCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSVPreAppointmentPropertyCell.h"
#import "PropertyPicture+MWPhoto.h"
#import "VoidData.h"

@implementation PSVPreAppointmentPropertyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
	self=[super initWithCoder:aDecoder];
	if (self)
	{
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
	}
	return self;
}

-(void)onPictureDefault:(NSNotification*) notification
{
	PropertyPicture * picture=notification.object;
	[self.imgProperty setImageWithURL:[NSURL URLWithString:picture.imagePath] andAddBorder:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	// Configure the view for the selected state
}

-(void) configureCell:(id)data
{
	[self setAppointment:data];
	UIImage * image=[self.appointment.appointmentToProperty.defaultPicture underlyingImage];
	if (image)
	{
		[self.imgProperty setImage: image];
	}
	else
	{
		[self.imgProperty setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
	}
	
	self.lblAddress.text = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyle1];
	self.lblAssignedBy.text = self.appointment.createdByPerson;
	self.lblTerminationDate.text = [UtilityClass stringFromDate:self.appointment.appointmentToVoidData.terminationDate dateFormat:kDateTimeStyle9];
	self.lblReletDate.text = [UtilityClass stringFromDate:self.appointment.appointmentToVoidData.reletDate dateFormat:kDateTimeStyle9];
	
	self.lblStartDate.text = [UtilityClass stringFromDate:self.appointment.appointmentStartTime dateFormat:kDateTimeStyle9];
	self.lblEndDate.text = [UtilityClass stringFromDate:self.appointment.appointmentEndTime dateFormat:kDateTimeStyle9];
	self.lblStartTime.text = [UtilityClass stringFromDate:self.appointment.appointmentStartTime dateFormat:kDateTimeStyle3];
	self.lblEndTime.text = [UtilityClass stringFromDate:self.appointment.appointmentEndTime dateFormat:kDateTimeStyle3];
	
	self.lblAddress.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
	self.lblStarts.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
	self.lblEnds.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) setAddressGestureRecognizer:(id)owner action:(SEL)action
{
	// if labelView is not set userInteractionEnabled, you must do so
	UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:owner action:action];
	[self.lblAddress setUserInteractionEnabled:YES];
	[self.lblAddress addGestureRecognizer:gesture];
}

-(void) setImagePropertyGestureRecognizer:(id)owner action:(SEL)action
{
	// if labelView is not set userInteractionEnabled, you must do so
	UITapGestureRecognizer* imageGesture = [[UITapGestureRecognizer alloc] initWithTarget:owner action:action];
	[self.imgProperty setUserInteractionEnabled:YES];
	[self.imgProperty addGestureRecognizer:imageGesture];
}
@end
