//
//  PSAppointmentDetailNotesCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVPreDetailNotesCell.h"

@implementation PSVPreDetailNotesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(id)data
{
	[self setAppointment:data];
	self.txtViewNotes.textAlignment = NSTextAlignmentJustified;
	self.txtViewNotes.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	[self.txtViewNotes setScrollEnabled:NO];
	
	if (!isEmpty(self.appointment.appointmentNotes))
	{
		self.txtViewNotes.text = self.appointment.appointmentNotes;
	}
	else
	{
		self.txtViewNotes.text = LOC(@"KEY_STRING_NONE");
	}
	self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
