//
//  PSRecordMajorWorkViewController.m
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPreRecordMajorWorkViewController.h"
#import "AbstractActionSheetPicker.h"
#import "PSVPreInspectionManager.h"
#import "PSBarButtonItem.h"
#import "PropertyComponents.h"
#import "VoidData.h"

@interface PSVPreRecordMajorWorkViewController ()
{
	PSVPreInspectionManager * _manager;
	PropertyComponents * _selectedComponent;
	NSArray * _componentsList;
	NSArray * _conditionList;
}
@end

@implementation PSVPreRecordMajorWorkViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setupUI];
	[self loadNavigationonBarItems];
	// Do any additional setup after loading the view.
	[self initData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Functions
- (void)setupUI
{
	[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
}

-(void)initData
{
	_manager = [[PSVPreInspectionManager alloc]init:self.appointment];
	_componentsList = self.appointment.appointmentToVoidData.voidDataToPropertyComponents.allObjects;
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"component" ascending:YES];
    _componentsList =[_componentsList sortedArrayUsingDescriptors:@[sort]];
	_conditionList = [_manager getConditionList];
	[self setUIComponentsState:FALSE];
}

-(NSArray *)getComponentsList
{
	NSMutableArray * list = [[NSMutableArray alloc]init];
	for (PropertyComponents * ptr in _componentsList)
	{
		[list addObject:ptr.component];
	}
	return list;
}

-(void) setUIComponentsState:(BOOL)enable
{
	self.btnCondition.enabled = enable;
	self.btnReplacementDue.enabled = enable;
	self.addMajorWorksButton.enabled = enable;
	self.tvNotes.editable = enable;
}

-(void)setReplacementDueValue:(NSDate *)date
{
	self.lblReplacementDue.text = [UtilityClass stringFromDate:date dateFormat:kDateTimeStyle8];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	[self setTitle:LOC(@"KEY_TITLE_VOID_RECORD_MAJOR_WORKS")];
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	self.addMajorWorksButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_ADD_WORKS")
																																		style:PSBarButtonItemStyleAdd
																																	 target:self
																																	 action:@selector(onClickAddWorksButton)];
	
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.addMajorWorksButton, nil]];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
}

#pragma mark - Navigation Bar
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickAddWorksButton
{
	CLS_LOG(@"");
	[_manager addMajorWorkRequired:_selectedComponent
											 Condition:self.lblCondition.text
									ReplacementDue:self.lblReplacementDue.text
													 Notes:self.tvNotes.text];
	
	if (self.delegate && [self.delegate respondsToSelector:@selector(addedMajorWorkRequired)])
	{
		[self.delegate addedMajorWorkRequired];
	}
	[self onClickBackButton];
}

#pragma mark - Button Events
- (IBAction)onClickComponent:(id)sender
{
	NSArray * componentList = [self getComponentsList];
	[ActionSheetStringPicker showPickerWithTitle:self.lblComponentTitle.text
																					rows:componentList
															initialSelection:0
																				target:self
																 successAction:@selector(onComponentSelected:element:)
																	cancelAction:nil
																				origin:sender];
}

- (void)onComponentSelected:(NSNumber *)selectedIndex element:(id)element
{
	_selectedComponent = [_componentsList objectAtIndex:[selectedIndex integerValue]];
	self.lblComponent.text = _selectedComponent.component;
	[self setReplacementDueValue:_selectedComponent.replacementDue];
	self.lblCondition.text = _selectedComponent.condition;
	if(self.lblCondition.text.length < 4) //condition length either empty or dash
	{
		self.lblCondition.text = [_manager getConditionList][0];
	}
	[self setUIComponentsState:TRUE];
}

- (IBAction)onClickCondition:(id)sender
{
	[ActionSheetStringPicker showPickerWithTitle:self.lblConditionTitle.text
																					rows:_conditionList
															initialSelection:0
																				target:self
																 successAction:@selector(onConditionSelected:element:)
																	cancelAction:nil
																				origin:sender];
}

- (void)onConditionSelected:(NSNumber *)selectedIndex element:(id)element
{
	self.lblCondition.text = [_conditionList objectAtIndex:[selectedIndex integerValue]];
}

@end
