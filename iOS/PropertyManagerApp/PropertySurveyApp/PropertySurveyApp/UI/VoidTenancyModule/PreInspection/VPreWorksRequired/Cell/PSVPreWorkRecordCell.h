//
//  PSVPreWorkRecordCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-28.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPreWorkRequiredProtocol.h"
#import "WorkRequired+CoreDataClass.h"

@interface PSVPreWorkRecordCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblComponent;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnTenantAgreed;

@property (weak, nonatomic) id<PSVPreWorkRequiredProtocol> delegate;
@property (weak, nonatomic) WorkRequired * workRequired;

-(void)setWorkRequired:(WorkRequired *)workRequired;

@end
