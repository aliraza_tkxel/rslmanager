//
//  PSWorksRequiredViewController.m
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPreWorksRequiredViewController.h"
#import "PSVPreWorkRecordCell.h"
#import "PSVPreInspectionManager.h"
#import "PSBarButtonItem.h"
#import "VoidData.h"
#import "RoomData.h"
#import "WorkRequired+CoreDataClass.h"
#import "PSRSearchWorkViewController.h"
#import "FaultDetailList.h"
#import "FaultAreaList.h"

@interface PSVPreWorksRequiredViewController ()
{
	PSVPreInspectionManager * _manager;
	CGRect recordWorksFrame;
	CGRect checksFrame;
	CGRect contentFrame;
}
@end


@implementation PSVPreWorksRequiredViewController

#pragma mark - Events

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self initData];
	[self setupUI];
	[self loadNavigationonBarItems];
	[self reloadData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	[self.scrollView layoutIfNeeded];
	self.scrollView.contentSize = self.contentView.bounds.size;
}

#pragma mark - Functions
-(void) initData
{
	_manager = [[PSVPreInspectionManager alloc]init:self.appointment];
}

-(void) setupUI
{
	[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	
	recordWorksFrame = CGRectMake(self.recordWorksContentView.frame.origin.x,
																		 self.recordWorksContentView.frame.origin.y,
																		 self.recordWorksContentView.frame.size.width,
																		 self.recordWorksContentView.frame.size.height);
	checksFrame = CGRectMake(self.checksContentView.frame.origin.x,
																self.checksContentView.frame.origin.y,
																self.checksContentView.frame.size.width,
																self.checksContentView.frame.size.height);
	contentFrame = CGRectMake(self.contentView.frame.origin.x,
																 self.contentView.frame.origin.y,
																 self.contentView.frame.size.width,
																 self.contentView.frame.size.height);
}

-(void) hideRecordWorksTable:(BOOL)hide
{
	self.recordWorksContentView.hidden = hide;
	if(hide == FALSE)
	{
		CGRect newRecordFrame = CGRectMake(recordWorksFrame.origin.x,
																		   recordWorksFrame.origin.y,
																		   recordWorksFrame.size.width,
																		   recordWorksFrame.size.height);
		CGRect newCheckFrame = CGRectMake(checksFrame.origin.x,
																		  checksFrame.origin.y,
																		  checksFrame.size.width,
																		  checksFrame.size.height);
		CGRect newContentFrame = CGRectMake(contentFrame.origin.x,
																			  contentFrame.origin.y,
																			  contentFrame.size.width,
																			  contentFrame.size.height);
		self.recordWorksContentView.frame = newRecordFrame;
		self.checksContentView.frame = newCheckFrame;
		self.scrollView.contentSize = newContentFrame.size;
	}
	else
	{
		CGRect newCheckView = CGRectMake(checksFrame.origin.x,
																		 checksFrame.origin.y - recordWorksFrame.size.height,
																		 checksFrame.size.width,
																		 checksFrame.size.height);
		CGRect newContentView = CGRectMake(contentFrame.origin.x,
																			 contentFrame.origin.y,
																			 contentFrame.size.width,
																			 contentFrame.size.height - recordWorksFrame.size.height);
		self.checksContentView.frame = newCheckView;
		self.scrollView.contentSize = newContentView.size;
	}
}

-(void)reloadData
{
	self.swWorksRequired.on = [self.appointment.appointmentToVoidData.isWorksRequired boolValue];
	self.swElectricCheck.on = [self.appointment.appointmentToVoidData.isElectricCheckRequired boolValue];
	self.swEPCCheck.on = [self.appointment.appointmentToVoidData.isEPCCheckRequired boolValue];
	self.swGasCheck.on = [self.appointment.appointmentToVoidData.isGasCheckRequired boolValue];
	self.swAsbestosCheck.on = [self.appointment.appointmentToVoidData.isAsbestosCheckRequired boolValue];
	[self enableComponents:self.swWorksRequired.on];
}

-(void)enableComponents:(BOOL)enable
{
	self.btnRecordWorks.enabled = enable;
	self.swElectricCheck.enabled = enable;
	self.swEPCCheck.enabled = enable;
	self.swGasCheck.enabled = enable;
	self.swAsbestosCheck.enabled = enable;
	NSUInteger count = self.appointment.appointmentToVoidData.voidDataToWorkRequired.count;
	if(count > 0 && enable == TRUE)
	{
		[self hideRecordWorksTable:FALSE];
		[self calculateTenantEstimate];
	}
	else
	{
		[self hideRecordWorksTable:TRUE];
	}
}

-(void)calculateTenantEstimate
{
	float tenantEstimate = 0.0;
	for (WorkRequired * workRequired in self.appointment.appointmentToVoidData.voidDataToWorkRequired)
	{
		if([workRequired.isTenantWork boolValue] == TRUE)
		{
			tenantEstimate += [workRequired.gross floatValue];
		}
	}
	[self setTenantEstimate:tenantEstimate];
}

-(void)setTenantEstimate:(float)tenantEstimate
{
	self.lblTenantEstimate.text = [NSString stringWithFormat:@"%.2f", tenantEstimate];
	self.appointment.appointmentToVoidData.tenantEstimate = [NSNumber numberWithFloat:tenantEstimate];
}
- (IBAction)btnRecordWorksTapped:(id)sender {
    //TODO: Link Add Required work process
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_AddRequiredWork bundle:nil];
    PSRSearchWorkViewController * jobScreen = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSRSearchWorkViewController class])];
    [jobScreen setAppointment:self.appointment];
    [self.navigationController pushViewController:jobScreen animated:YES];
    
}

#pragma mark - Navigation Segue
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setTitle:LOC(@"KEY_TITLE_VOID_WORKS_REQUIRED")];
}

- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[_manager saveContext];
	[self popOrCloseViewController];
}

#pragma mark - UISwitch events
- (IBAction)onChangeWorkRequired:(id)sender
{
	CLS_LOG(@"onChangeWorkRequired");
	self.appointment.appointmentToVoidData.isWorksRequired = [NSNumber numberWithBool:self.swWorksRequired.on];
	[self enableComponents:self.swWorksRequired.on];
}
- (IBAction)onChangeElectricCheck:(id)sender
{
	CLS_LOG(@"onChangeElectricCheck");
	self.appointment.appointmentToVoidData.isElectricCheckRequired = [NSNumber numberWithBool:self.swElectricCheck.on];
}
- (IBAction)onChangeEpcCheck:(id)sender
{
	CLS_LOG(@"onChangeEpcCheck");
	self.appointment.appointmentToVoidData.isEPCCheckRequired = [NSNumber numberWithBool:self.swEPCCheck.on];
}
- (IBAction)onChangeAsbestosCheck:(id)sender
{
	CLS_LOG(@"onChangeAsbestosCheck");
	self.appointment.appointmentToVoidData.isAsbestosCheckRequired = [NSNumber numberWithBool:self.swAsbestosCheck.on];
}
- (IBAction)onChangeGasCheck:(id)sender
{
	CLS_LOG(@"onChangeGasCheck");
	self.appointment.appointmentToVoidData.isGasCheckRequired = [NSNumber numberWithBool:self.swGasCheck.on];
}

#pragma mark - TableView Functions
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	VoidData * voidData = self.appointment.appointmentToVoidData;
	NSInteger count = [voidData.voidDataToWorkRequired count];
	CLS_LOG(@"tableView: numberOfRowsInSection: count: %ld", (long)count);
	return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PSVPreWorkRecordCell * cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPreWorkRecordCell class])];
	WorkRequired * workRequired = [self.appointment.appointmentToVoidData.voidDataToWorkRequired.allObjects objectAtIndex:indexPath.row];
	CLS_LOG(@"tableView: cellForRowAtIndexPath: row: %ld component: %@", (long)indexPath.row, workRequired.workRequiredToFaultArea.faultAreaName);
	[cell setWorkRequired:workRequired];
	cell.delegate = self;
	return cell;
}

#pragma mark - Protocol Implementation
-(void)addedWorkRequired
{
	CLS_LOG(@"Protocol: preWorkRequiredRecorded");
	[self reloadData];
	[self.tableView reloadData];
	[_manager saveContext];
}

-(void)recalculateTenantAgreement:(WorkRequired *)workRequired
{
	CLS_LOG(@"Protocol: recalculateTenantAgreement");
	float tenantEstimate = [self.lblTenantEstimate.text floatValue];
	if([workRequired.isTenantWork boolValue] == TRUE)
	{
		tenantEstimate += [workRequired.gross floatValue];
	}
	else
	{
		tenantEstimate -= [workRequired.gross floatValue];
	}
	[self setTenantEstimate:tenantEstimate];
	[_manager saveContext];
}

@end
