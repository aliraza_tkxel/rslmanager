//
//  PSVPreWorkRequiredProtocol.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-29.
//  Copyright (c) 2015 TkXel. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "WorkRequired+CoreDataClass.h"

@protocol PSVPreWorkRequiredProtocol <NSObject>

@required
-(void)addedWorkRequired;
-(void)recalculateTenantAgreement:(WorkRequired *)workRequired;

@optional

@end