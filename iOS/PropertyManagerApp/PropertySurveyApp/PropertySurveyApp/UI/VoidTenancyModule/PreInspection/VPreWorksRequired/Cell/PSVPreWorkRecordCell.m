//
//  PSVPreWorkRecordCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-28.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPreWorkRecordCell.h"
#import "RoomData.h"
#import "FaultDetailList.h"
#import "FaultAreaList.h"

@implementation PSVPreWorkRecordCell

#pragma mark - Cell Init Setting
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{ }
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:FALSE animated:YES];
	// Configure the view for the selected state
}

#pragma mark - Functions
-(void)setWorkRequired:(WorkRequired *)workRequired
{
	_workRequired = workRequired;
	self.lblComponent.text = self.workRequired.workRequiredToFaultArea.faultAreaName;
	self.lblDescription.text = workRequired.repairDescription;
	[self.btnTenantAgreed setSelected:[self.workRequired.isTenantWork boolValue]];
}

#pragma mark - Button Events
- (IBAction)onClickTenantAgreed:(id)sender
{
	UIButton * button = sender;
	if(button.selected == YES)
	{
		[button setSelected:NO];
	}
	else
	{
		[button setSelected:YES];
	}
	self.workRequired.isTenantWork = [NSNumber numberWithBool:button.selected];
	self.workRequired.isBRSWork = [NSNumber numberWithBool:([self.workRequired.isTenantWork boolValue] == TRUE ? FALSE : TRUE)];
	if (self.delegate && [self.delegate respondsToSelector:@selector(recalculateTenantAgreement:)])
	{
		[self.delegate recalculateTenantAgreement:self.workRequired];
	}
}

@end
