//
//  PSVPreMajorWorkRequiredProtocol.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-30.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

@protocol PSVPreMajorWorkRequiredProtocol <NSObject>

@required
-(void)addedMajorWorkRequired;

@optional

@end
