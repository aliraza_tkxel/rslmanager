//
//  PSVPreMajorWorkRecordCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-28.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPreMajorWorkRecordCell.h"

@implementation PSVPreMajorWorkRecordCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{ }
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{ }
	return self;
}

-(void)setMajorWorkRequired:(MajorWorkRequired *)majorWorkRequired
{
	_majorWorkRequired = majorWorkRequired;
	self.lblComponent.text = majorWorkRequired.component;
	self.lblCondition.text = majorWorkRequired.condition;
	[self setDueDate:majorWorkRequired.replacementDue];
}

-(void)setDueDate:(NSDate *)dueDate
{
	self.lblDueDate.text = [UtilityClass stringFromDate:dueDate dateFormat:kDateTimeStyle8];
}

@end
