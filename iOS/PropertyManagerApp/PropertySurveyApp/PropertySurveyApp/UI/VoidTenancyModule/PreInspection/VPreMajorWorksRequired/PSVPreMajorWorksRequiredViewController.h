//
//  PSMajorWorksRequiredViewController.h
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPreMajorWorkRequiredProtocol.h"

#define MAJOR_WORKS_REQUIRED_SEGUE_IDENTIFIER @"MajorWorksRequiredScreen"

@interface PSVPreMajorWorksRequiredViewController : PSCustomViewController<UITableViewDelegate, PSVPreMajorWorkRequiredProtocol>

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UISwitch *swMajorWorksRequired;
@property (strong, nonatomic) IBOutlet UIButton *btnRecordMajorWorks;

@property (weak,   nonatomic) Appointment *appointment;

@end
