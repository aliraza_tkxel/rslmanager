//
//  PSJSVInspectionViewController.h
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVPreJSVInspectionViewController : PSCustomViewController
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *lblReletDate;
@property (strong, nonatomic) IBOutlet UILabel *lblReletDateTitle;

@property (strong, nonatomic) Appointment *appointment;

@end
