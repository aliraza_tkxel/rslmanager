//
//  PSFaultDetailsViewController.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-15.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "FaultRepairData+CoreDataClass.h"
#import "PSVPreWorkRequiredProtocol.h"
#import "PSVPostWorksRequiredProtocol.h"

@protocol PSVPreWorkRequiredProtocol;
@protocol PSVPostWorksRequiredProtocol;
@interface PSRWorkDetailsViewController : PSCustomViewController

@property (strong, nonatomic) Appointment * appointment;
@property (strong, nonatomic) FaultRepairData * faultDetail;

@property (weak, nonatomic) id<PSVPreWorkRequiredProtocol> delegate;
@property (weak, nonatomic) id<PSVPostWorksRequiredProtocol> delegate2;
@end
