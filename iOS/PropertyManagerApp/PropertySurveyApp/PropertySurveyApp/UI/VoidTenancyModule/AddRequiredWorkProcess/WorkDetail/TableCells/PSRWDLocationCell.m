//
//  PSRFDLocationCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRWDLocationCell.h"
#import "FaultAreaList.h"

@interface PSRWDLocationCell ()
{
	NSArray * _faultAreaList;
	NSArray * _faultAreaNameList;
}
@end

@implementation PSRWDLocationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{ }
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		self.faultArea = nil;
		NSManagedObjectContext * managedContext = [PSDatabaseContext sharedContext].managedObjectContext;
		NSString * entityName = NSStringFromClass([FaultAreaList class]);
		_faultAreaList = [CoreDataHelper getObjectsFromEntity:entityName
																							 predicate:nil
																								 sortKey:nil
																					 sortAscending:YES
																								 context:managedContext];
		_faultAreaNameList = [_faultAreaList valueForKeyPath:@"faultAreaName"];
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	// Configure the view for the selected state
}

- (IBAction)onClickFaultArea:(id)sender
{
	[ActionSheetStringPicker showPickerWithTitle:@"Fault Area"
																					rows:_faultAreaNameList
															initialSelection:0
																				target:self
																 successAction:@selector(onOptionSelected:element:)
																	cancelAction:nil
																				origin:sender];
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
	self.lblFaultLocation.text = [_faultAreaNameList objectAtIndex:[selectedIndex integerValue]];
	NSPredicate * predicate = [NSPredicate predicateWithFormat:@"faultAreaName LIKE %@", self.lblFaultLocation.text];
	self.faultArea = [[_faultAreaList filteredArrayUsingPredicate:predicate] objectAtIndex:0];
}

@end
