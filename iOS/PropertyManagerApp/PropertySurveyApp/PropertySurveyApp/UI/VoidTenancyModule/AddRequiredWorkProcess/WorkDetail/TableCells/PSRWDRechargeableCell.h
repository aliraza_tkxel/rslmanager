//
//  PSRFDRechargeableCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaultRepairData+CoreDataClass.h"

@interface PSRWDRechargeableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblCost;
@property (strong, nonatomic) FaultRepairData * faultDetail;

-(void)configureCell:(FaultRepairData *)faultDetail;

@end
