//
//  PSFaultDetailsViewController.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-15.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRWorkDetailsViewController.h"
#import "PSRFaultBasketViewController.h"
#import "PSRWDLocationCell.h"
#import "PSRWDDecriptionCell.h"
#import "PSRWDRechargeableCell.h"
#import "PSRWDNotesCell.h"
#import "PSRFaultDetailsManager.h"

#define iPSRFDLocationCell 0
#define iPSRFDDecriptionCell 1
#define iPSRFDRechargeableCell 2
#define iPSRFDNotesCell 3

@interface PSRWorkDetailsViewController ()
{
	PSRFaultDetailsManager * _manager;
}
@end

@implementation PSRWorkDetailsViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self loadNavigationonBarItems];
	[self initData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)initData
{
	_manager = [[PSRFaultDetailsManager alloc]init:self.appointment];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	PSBarButtonItem *nextButton = [[PSBarButtonItem alloc] initWithCustomStyle:@"Next"
																																			 style:PSBarButtonItemStyleDefault
																																			target:self
																																			action:@selector(onClickNextButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setRightBarButtonItems:[NSArray arrayWithObjects:nextButton, nil]];
	[self setTitle:@"Repair Details"];
}

#pragma mark Navigation Bar Button Selectors
-(void)onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

-(void)onClickNextButton
{
	CLS_LOG(@"onClickNextButton");
	NSIndexPath * indexPath = [NSIndexPath indexPathForRow:iPSRFDNotesCell inSection:0];

	PSRWDNotesCell * notesCell = (PSRWDNotesCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	
	indexPath = [NSIndexPath indexPathForRow:iPSRFDLocationCell inSection:0];
	PSRWDLocationCell * locationCell = (PSRWDLocationCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    indexPath = [NSIndexPath indexPathForRow:iPSRFDRechargeableCell  inSection:0];
    PSRWDRechargeableCell * rechargeableCell = (PSRWDRechargeableCell *)[self.tableView cellForRowAtIndexPath:indexPath];

	if(locationCell.faultArea == nil || isEmpty(notesCell.tvNotes.text) == TRUE)
	{
		UIAlertView * alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_WARNING")
																										message:LOC(@"Location and Fault Notes and  are mandatory fields.")
																									 delegate:nil
																					cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																					otherButtonTitles:nil,nil];
		[alert show];
	}
	else
	{
		
        [_manager reportVoidWorkWith:self.faultDetail
                           faultArea:locationCell.faultArea
                               notes:notesCell.tvNotes.text];
		
        if(![_delegate isKindOfClass:[NSNull class]] && _delegate!=nil && [_delegate respondsToSelector:@selector(addedWorkRequired)]){
            
            [_delegate addedWorkRequired];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3] animated:YES];
        }
        else if(![_delegate2 isKindOfClass:[NSNull class]] && _delegate2!=nil && [_delegate2 respondsToSelector:@selector(addedWorkRequired)]){
            [_delegate2 addedWorkRequired];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3] animated:YES];
        }
		
    }
}

#pragma mark - UITableView Delegate Methods
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *_cell = nil;
	CLS_LOG(@"row: %zd, section: %zd", indexPath.row, indexPath.section);
	switch (indexPath.row)
	{
		case iPSRFDLocationCell:
		{
			PSRWDLocationCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRWDLocationCell class])];
			_cell = cell;
			break;
		}
		case iPSRFDDecriptionCell:
		{
			PSRWDDecriptionCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRWDDecriptionCell class])];
			[cell configureCell:self.faultDetail];
			_cell = cell;
			break;
		}
		case iPSRFDRechargeableCell:
		{
			PSRWDRechargeableCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRWDRechargeableCell class])];
			[cell configureCell:self.faultDetail];
			_cell = cell;
			break;
		}
		case iPSRFDNotesCell:
		{
			PSRWDNotesCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRWDNotesCell class])];
			_cell = cell;
			break;
		}
		default:
			break;
	}
	[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	return _cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0.0;
	if(indexPath.row == 3)
	{
		rowHeight = 270;
	}
	else
	{
		rowHeight = 44;
	}
	return rowHeight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 4;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

@end
