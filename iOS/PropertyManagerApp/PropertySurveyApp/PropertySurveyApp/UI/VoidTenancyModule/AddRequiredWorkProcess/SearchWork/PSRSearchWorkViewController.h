//
//  PSAddNewFaultViewController.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-15.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "SearchViewComingFrom.h"

@interface PSRSearchWorkViewController : PSCustomViewController <NSFetchedResultsControllerDelegate, UISearchDisplayDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *sbSearchFault;

@property (assign, nonatomic) SearchViewComingFrom viewComingFrom;
@property (strong, nonatomic) Appointment * appointment;
@property (nonatomic, retain) NSFetchedResultsController * fetchResultCtrl;


@end
