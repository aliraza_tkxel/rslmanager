//
//  PSAddNewFaultViewController.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-15.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRSearchWorkViewController.h"
#import "PSRWorkDetailsViewController.h"
#import "PSVPostWorksRequiredViewController.h"
#import "PSVPreWorksRequiredViewController.h"
// Cell Header files
#import "PSRWorkCell.h"
// CoreData Managed Objects
#import "FaultRepairData+CoreDataClass.h"

#define kSectionHeaderHeight 20
#define kVoidWorkDetailsCellHeight 65

@interface PSRSearchWorkViewController ()
{
	NSMutableArray * _headerViewArray;
}
@end

@implementation PSRSearchWorkViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self initData];
	[self loadNavigationonBarItems];
	[self fetchedResultsController:nil];
}

- (void)viewDidUnload
{
	_fetchResultCtrl = nil;
}

-(void)initData
{
	self.edgesForExtendedLayout = UIRectEdgeNone;
	_headerViewArray = [NSMutableArray array];
	[self loadSectionHeaderViews:@[@"Results:"] headerViews:_headerViewArray];
}

-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//	// Get the new view controller using [segue destinationViewController].
//	// Pass the selected object to the new view controller.
//	CLS_LOG(@"prepareForSegue: %@", segue.identifier);
//	if([segue.identifier isEqualToString:NSStringFromClass([PSRFaultDetailsViewController class])])
//	{
//		PSRFaultDetailsViewController * controller = (PSRFaultDetailsViewController *)segue.destinationViewController;
//	}
//}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setTitle:@"Add New Repair"];
}

#pragma mark Navigation Bar Button Selectors
-(void)onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

#pragma mark - UISearchBar Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
	NSPredicate * predicate = [NSPredicate predicateWithFormat:@"faultRepairDescription contains[c] %@", searchString];
	NSError *error = nil;
	
	CLS_LOG(@"%@", predicate.description);
	
	[[self fetchedResultsController:predicate] performFetch:&error];
	if (error)
	{
		CLS_LOG(@"Unable to perform fetch.");
		CLS_LOG(@"%@, %@", error, error.localizedDescription);
	}
	return YES;
}

#pragma mark - UITableView Delegate Methods
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *_cell = NULL;
	FaultRepairData * faultDetailList = NULL;
	id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchResultCtrl sections] objectAtIndex:[indexPath section]];
	if ([sectionInfo numberOfObjects] >= [indexPath row])
	{
		faultDetailList = [_fetchResultCtrl objectAtIndexPath:indexPath];
		PSRWorkCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRWorkCell class])];
		[cell configureCell:faultDetailList searchedKey:self.sbSearchFault.text];
		_cell = cell;
	}
	[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	return _cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0.0;
	if(indexPath.section == 0)
	{
		rowHeight = kVoidWorkDetailsCellHeight;
	}
	return rowHeight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [[[_fetchResultCtrl sections] objectAtIndex:section] numberOfObjects];
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	return [_headerViewArray objectAtIndex:section];
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return kSectionHeaderHeight;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return [[_fetchResultCtrl sections] count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	PSRWorkCell * cell = (PSRWorkCell *)[tableView cellForRowAtIndexPath:indexPath];
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_AddRequiredWork bundle:nil];
	PSRWorkDetailsViewController * uiView = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSRWorkDetailsViewController class])];
	uiView.faultDetail = cell.faultDetail;
	uiView.appointment = self.appointment;
    
    UIViewController *previousVC = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    if([previousVC isKindOfClass:[PSVPostWorksRequiredViewController class]]){
        uiView.delegate2 = (PSVPostWorksRequiredViewController *)previousVC;
        
    }
    else{
        uiView.delegate = (PSVPreWorksRequiredViewController *)previousVC;
    }
    
	[self.navigationController pushViewController:uiView animated:YES];
}

#pragma mark - NSFetchedResultsControllerDelegate Delegate Methods
- (NSFetchedResultsController *)fetchedResultsController:(NSPredicate *)predicate
{
	NSString * entityName = NSStringFromClass([FaultRepairData class]);
	NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
																						inManagedObjectContext:dbContext];
	[fetchRequest setEntity:entity];
 
	NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"faultRepairDescription"
																											 ascending:NO];
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
	
	[fetchRequest setPredicate:predicate];
	
	_fetchResultCtrl = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
																												 managedObjectContext:dbContext
																													 sectionNameKeyPath:nil
																																		cacheName:nil];
	_fetchResultCtrl.delegate = self;
	return _fetchResultCtrl;
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	CLS_LOG(@"<<<<<<<<<<<< controllerWillChangeContent");
	[self.tableView beginUpdates];
	CLS_LOG(@">>>>>>>>>>>>>>>>>> controllerWillChangeContent");
}

- (void)controller:(NSFetchedResultsController *)controller
	didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
					 atIndex:(NSUInteger)sectionIndex
		 forChangeType:(NSFetchedResultsChangeType)type
{
	switch(type)
	{
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		default:
			break;
	}
}

- (void)controller:(NSFetchedResultsController *)controller
	 didChangeObject:(id)anObject
			 atIndexPath:(NSIndexPath *)indexPath
		 forChangeType:(NSFetchedResultsChangeType)type
			newIndexPath:(NSIndexPath *)newIndexPath
{
	UITableView *tableView = self.tableView;
	
	switch(type)
	{
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[tableView cellForRowAtIndexPath:indexPath];
			break;
			
		case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	[self.tableView endUpdates];
}

@end
