//
//  PSNoEntryNoCardViewController.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"

@interface PSNoEntryNoCardViewController : PSCustomViewController <UITextViewDelegate>

@property (strong, nonatomic) IBOutlet SZTextView *tvNoEntry;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) Appointment *appointment;

@end
