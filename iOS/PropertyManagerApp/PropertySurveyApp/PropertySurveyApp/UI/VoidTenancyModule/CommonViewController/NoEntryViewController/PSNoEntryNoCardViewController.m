//
//  PSNoEntryNoCardViewController.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-10.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSNoEntryNoCardViewController.h"
#import "PSVBaseManager.h"

@interface PSNoEntryNoCardViewController ()
{
	PSVBaseManager * _manager;
}
@end

@implementation PSNoEntryNoCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self initData];
	[self loadNavigationonBarItems];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Functions
-(void)initData
{
	_manager = [[PSVBaseManager alloc]init:self.appointment];
	[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	[self setTitle:@"No Entry"];
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	PSBarButtonItem * saveButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
																																				style:PSBarButtonItemStyleDefault
																																			 target:self
																																			 action:@selector(onClickSaveButton)];
	
	[self setRightBarButtonItems:[NSArray arrayWithObjects:saveButton, nil]];
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
}

#pragma mark - Navigation Bar Events
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickSaveButton
{
	CLS_LOG(@"onClickSaveButton");
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_NO_ENTRY")
																								 message:LOC(@"KEY_ALERT_CONFIRM_NO_ENTRY")
																								delegate:self
																			 cancelButtonTitle:LOC(@"KEY_ALERT_NO")
																			 otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
	alert.tag = AlertViewTagNoEntry;
	[alert show];
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == AlertViewTagNoEntry)
	{
		if(buttonIndex == alertView.firstOtherButtonIndex)
		{
			[_manager markCompleteWithNoEntry:self.tvNoEntry.text];
			[self popToRootViewController];
		}
	}
}

#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	NSUInteger lengthAllowed = 300;
	
	BOOL textVeiwShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextView *)self.selectedControl).text stringByAppendingString:text];
	
	if ([stringAfterReplacement length] > lengthAllowed)
	{
		textVeiwShouldReturn = NO;
	}
	else{
		
		self.tvNoEntry.text = textView.text;
		
	}
	return textVeiwShouldReturn;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	
	self.isEditing = NO;
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	self.tvNoEntry.text = textView.text;
	[self scrollToRectOfSelectedControl];
}

- (void)textViewDidChange:(UITextView *)textView
{
	self.isEditing = YES;
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	[self scrollToRectOfSelectedControl];
}

@end
