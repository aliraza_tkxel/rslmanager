//
//  PSGasElectricCheckViewController.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 22/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSCustomNavigationController.h"
#import "TPKeyboardAvoidingCollectionView.h"

@interface PSVGECheckViewController : PSCustomViewController

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIButton *btnMeterType;
@property (strong, nonatomic) IBOutlet UIButton *btnTenantType;
@property (strong, nonatomic) IBOutlet UITextField *tfMeterLocation;
@property (strong, nonatomic) IBOutlet UITextField *tfMeterReading;
@property (strong, nonatomic) IBOutlet UIButton *btnComplete;
@property (strong, nonatomic) IBOutlet UILabel *lblMeterReading;
@property (strong, nonatomic) IBOutlet UILabel *lblMeterType;
@property (strong, nonatomic) IBOutlet UILabel *lblMeterLocation;

@property (strong, nonatomic) Appointment *appointment;

@end
