//
//  PSGasElectricCheckViewController.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 22/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVGECheckViewController.h"
#import "PSVGECheckManager.h"
#import "Appointment+CoreDataClass.h"
#import "MeterData.h"
#import "MeterType.h"

@interface PSVGECheckViewController ()
{
	PSVGECheckManager * _manager;
	MeterType * _meterType;
	NSArray * _tenantTypeList;
	NSArray * _meterTypeList;
}
@end

@implementation PSVGECheckViewController

#pragma mark - Parent Function
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self initData];
	[self loadNavigationonBarItems];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Function
-(void) initData
{
	_manager = [[PSVGECheckManager alloc]init:self.appointment];
	_tenantTypeList = [_manager getTenantTypeList];
	_meterTypeList = [_manager getMeterTypeListFromDB:self.appointment.appointmentToMeterData.deviceType];
	
	MeterType * meterType = [_meterTypeList objectAtIndex:0];
	_meterType = meterType;
	[self.btnMeterType setTitle:_meterType.meterType forState:UIControlStateNormal];
	[self.btnTenantType setTitle:[_tenantTypeList objectAtIndex:0] forState:UIControlStateNormal];
	NSString * completeButtonTitle = [NSString stringWithFormat:@"Complete %@ Check",
																		self.appointment.appointmentToMeterData.deviceType];
	[self.btnComplete setTitle:completeButtonTitle forState:UIControlStateNormal];
	[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	
	NSString * labelString = [NSString stringWithFormat:@"%@ %@", self.appointment.appointmentToMeterData.deviceType, self.lblMeterType.text];
	self.lblMeterType.text = [labelString uppercaseString];
	labelString = [NSString stringWithFormat:@"%@ %@", self.appointment.appointmentToMeterData.deviceType, self.lblMeterReading.text];
	self.lblMeterReading.text = [labelString uppercaseString];
	labelString = [NSString stringWithFormat:@"%@ %@", self.appointment.appointmentToMeterData.deviceType, self.lblMeterLocation.text];
	self.lblMeterLocation.text = [labelString uppercaseString];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem * backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																				style:PSBarButtonItemStyleBack
																																			 target:self
																																			 action:@selector(onClickBackButton)];
	
	[self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil] animated:YES];
	
	[self setTitle:[NSString stringWithFormat:@"%@ Check",
									self.appointment.appointmentToMeterData.deviceType]];
}

-(void)onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark - Button Events
- (IBAction)onClickTenantType:(id)sender
{
	[ActionSheetStringPicker showPickerWithTitle:@"Tenant Type"
																					rows:_tenantTypeList
															initialSelection:0
																				target:self
																 successAction:@selector(onTenantTypeSelected:element:)
																	cancelAction:nil
																				origin:sender];
}

- (void)onTenantTypeSelected:(NSNumber *)selectedIndex element:(id)element
{
	NSString * tenantType = [_tenantTypeList objectAtIndex:[selectedIndex integerValue]];
	[self.btnTenantType setTitle:tenantType forState:UIControlStateNormal];
}

-(NSArray *)getMeterTypeNameList
{
	NSMutableArray * list = [[NSMutableArray alloc]init];
	for (MeterType * ptr in _meterTypeList)
	{
		[list addObject:ptr.meterType];
	}
	return list;
}

- (IBAction)onClickMeterType:(id)sender
{
	NSArray * list = [self getMeterTypeNameList];
	[ActionSheetStringPicker showPickerWithTitle:@"Meter Type"
																					rows:list
															initialSelection:0
																				target:self
																 successAction:@selector(onMeterTypeSelected:element:)
																	cancelAction:nil
																				origin:sender];
}

- (void)onMeterTypeSelected:(NSNumber *)selectedIndex element:(id)element
{
	_meterType = [_meterTypeList objectAtIndex:[selectedIndex integerValue]];
	[self.btnMeterType setTitle:_meterType.meterType forState:UIControlStateNormal];
}

- (IBAction)onClickCompleteAppointment:(id)sender
{
	CLS_LOG(@"");
	NSString * meterLocation = self.tfMeterLocation.text;
	NSString * meterReading = self.tfMeterReading.text;
	if(isEmpty(meterLocation) == TRUE ||
		 isEmpty(meterReading) == TRUE)
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_WARNING")
																									 message:LOC(@"KEY_ALERT_EMPTY_MANDATORY_FIELDS")
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
	}
	else
	{
        MeterData * meterData = _appointment.appointmentToMeterData;
        meterData.meterLocation = self.tfMeterLocation.text;
        meterData.meterReading = [NSNumber numberWithLongLong:[self.tfMeterReading.text longLongValue]];
        meterData.tenantType = self.btnTenantType.titleLabel.text;
        meterData.meterDataToMeterType = _meterType;
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOC(@"KEY_ALERT_CONFIRM_POPUP_HEADING") message:LOC(@"KEY_CONFIRM_COMPLETE_ACTION") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_NO") style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            [_manager saveAppointmentAndMarkCompleted:NO];
            [self popToRootViewController];
            NSLog(@"Cancelled Appointment Completion");
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            [_manager saveAppointmentAndMarkCompleted:YES];
            [self popToRootViewController];
            NSLog(@"Confirmed Appointment Completion");
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated: YES completion: nil];
		
	}
}

@end
