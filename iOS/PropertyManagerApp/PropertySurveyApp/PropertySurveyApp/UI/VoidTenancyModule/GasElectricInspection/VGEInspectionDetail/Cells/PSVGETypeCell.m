//
//  TypeCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVGETypeCell.h"
#import "MeterData.h"

@interface PSVGETypeCell ()
{
	Appointment * _appointment;
}
@end

@implementation PSVGETypeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(id)data
{
	_appointment = data;
	self.lblDetail.text = [NSString stringWithFormat:@"%@ Check", _appointment.appointmentToMeterData.deviceType];
	self.lblDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblDetail.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	[self.imgStatus setHidden:YES];
	[self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
	self.selectionStyle = UITableViewCellSelectionStyleGray;
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
}

@end
