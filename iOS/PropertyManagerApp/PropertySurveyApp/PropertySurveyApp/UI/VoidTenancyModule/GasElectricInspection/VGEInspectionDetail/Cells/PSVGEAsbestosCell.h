//
//  PSAsbestosCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVGEAsbestosCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblAsbestos;
@property (strong, nonatomic) IBOutlet UILabel *lblAsbestosLocation;

-(void)configureCell:(id)data :(NSInteger)index;

@end
