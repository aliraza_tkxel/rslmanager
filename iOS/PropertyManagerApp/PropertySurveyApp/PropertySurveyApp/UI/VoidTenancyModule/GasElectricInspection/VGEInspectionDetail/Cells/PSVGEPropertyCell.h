//
//  PropertyCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVGEPropertyCell : UITableViewCell
@property (strong, nonatomic) IBOutlet FBRImageView *imgProperty;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblExpiryDate;
@property (strong, nonatomic) IBOutlet UILabel *lblExpiryTitle;

-(void) reloadData:(Property*) property;

@end
