//
//  PSAsbestosCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVGEAsbestosCell.h"

@interface PSVGEAsbestosCell ()
{
	NSArray * _asbestosArray;
}
@end

@implementation PSVGEAsbestosCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(id)data :(NSInteger)index
{
	_asbestosArray = data;
	self.lblAsbestos.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblAsbestos.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblAsbestosLocation.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblAsbestosLocation.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	if ([_asbestosArray count] > 0)
	{
		PropertyAsbestosData *asbestosData = [_asbestosArray objectAtIndex:index];
		if (!isEmpty(asbestosData))
		{
			NSMutableString *asbestosInfo = [[NSMutableString alloc]init];
			[asbestosInfo appendString:asbestosData.asbRiskLevelDesc];
			
			if([UtilityClass isEmpty:asbestosData.type] == NO)
			{
				[asbestosInfo appendString:[NSString stringWithFormat:@" (%@)",asbestosData.type]];
			}
			
			if([UtilityClass isEmpty:asbestosData.riskLevel] == NO)
			{
				[asbestosInfo appendString:[NSString stringWithFormat:@" - %@",asbestosData.riskLevel]];
			}
			self.lblAsbestos.text = asbestosInfo;
			self.lblAsbestosLocation.text = asbestosData.riskDesc;
		}
	}
	else
	{
		self.lblAsbestos.text = LOC(@"KEY_STRING_NONE");
	}
	self.selectionStyle = UITableViewCellSelectionStyleNone;
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
}

@end
