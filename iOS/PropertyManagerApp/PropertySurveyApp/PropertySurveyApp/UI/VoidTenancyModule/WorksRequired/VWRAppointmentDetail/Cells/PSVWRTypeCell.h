//
//  TypeCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVWRTypeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgStatus;

-(void)configureCell:(id)data;

@end
