//
//  PSAppointmentDetailNotesCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVWRAppointmentNotesCell.h"

@interface PSVWRAppointmentNotesCell ()
{
	Appointment * _appointment;
}
@end

@implementation PSVWRAppointmentNotesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(id)data
{
	_appointment = data;
	self.txtViewNotes.textAlignment = NSTextAlignmentJustified;
	self.txtViewNotes.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	[self.txtViewNotes setScrollEnabled:NO];
	
	if (!isEmpty(_appointment.appointmentNotes))
	{
		self.txtViewNotes.text = _appointment.appointmentNotes;
	}
	else
	{
		self.txtViewNotes.text = LOC(@"KEY_STRING_NONE");
	}
	self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
