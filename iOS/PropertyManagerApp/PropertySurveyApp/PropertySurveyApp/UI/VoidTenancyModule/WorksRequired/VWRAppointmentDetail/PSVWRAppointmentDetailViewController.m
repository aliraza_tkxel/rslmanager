//
//  PSVWRAppointmentDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVWRAppointmentDetailViewController.h"
#import "PSAppointmentNotesViewController.h"
#import "PSVWRPropertyCell.h"
#import "PSTenantDetailViewController.h"
#import "PSVWRAppointmentNotesCell.h"
#import "PSVWRJobDetailViewController.h"
#import "PSVWRTypeCell.h"
#import "PSVWRTypeCell.h"
#import "PSVWRAsbestosCell.h"
#import "PSVWRTenantCell.h"
#import "PSVWRJSVCell.h"
#import "PSMapsViewController.h"
#import "PSImageViewController.h"
#import "PSNoEntryNoCardViewController.h"
#import "PSSynchronizationManager.h"

#define kNumberOfSections 6

#define kSectionProperty 0
#define kSectionType 1
#define kSectionTenant 2
#define kSectionJSV 3
#define kSectionNotes 4
#define kSectionAsbestos 5

#define kSectionPlannedComponentRows 1
#define kSectionPropertyRows 1
#define kPropertyCellRow 0

#define kSectionTypeRows 1
#define kTypeCellRow 0

#define kTenantCellRow 2

#define kPropertyCellHeight 80
#define kFaultPropertyCellHeight 160
#define kFaultSchemeCellHeight 134

#define kTypeCellHeight 29
#define kTenantCellHeight 98
#define kAsbestosCellHeight 48
#define kAccomodationCellHeight 39
#define kNotesCellHeight 150

#define kDownloadSurveyAlertView 1

#define kViewSectionHeaderHeight 20

@interface PSVWRAppointmentDetailViewController ()
{
	AppointmentType _appointmentType;
	AppointmentStatus _appointmentStatus;
	
}
@end

@implementation PSVWRAppointmentDetailViewController

@synthesize headerViewArray;
@synthesize appointment;
@synthesize property;
@synthesize tenantsArray;
@synthesize jobDataListArray;
@synthesize asbestosArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		self.isEditing = NO;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	_appointmentType = [self.appointment getType];
	_appointmentStatus = [self.appointment getStatus];
	self.headerViewArray = [NSMutableArray array];
	self.tenantsArray = [NSMutableArray arrayWithArray:[appointment.appointmentToCustomer allObjects]];
	self.jobDataListArray =  [NSMutableArray arrayWithArray:[appointment.appointmentToJobSheet allObjects]];
	self.property = appointment.appointmentToProperty;
	self.asbestosArray =  [NSMutableArray arrayWithArray:[property.propertyToPropertyAsbestosData allObjects]];
	[self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	[self loadNavigationonBarItems];
	[self addTableHeaderView];
	[self registerNibsForTableView];
	[self loadSectionHeaderViews:@[LOC(@"KEY_STRING_PROPERTY"),LOC(@"KEY_STRING_TYPE"),LOC(@"KEY_STRING_TENANT"),LOC(@"KEY_STRING_JSV"),LOC(@"KEY_STRING_CUSTOMER_NOTES"),LOC(@"KEY_STRING_ASBESTOS")] headerViews:self.headerViewArray];
}


-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSVWRTypeCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRTenantCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRAsbestosCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRJSVCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRAppointmentNotesCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRPropertyCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self registerNotifications];
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.notesButton, self.noEntryButton, nil]];
	if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
			|| [self.appointment getStatus] == AppointmentStatusNoEntry)
	{
		[self.noEntryButton setEnabled:NO];
		[self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
	}
	else
	{
		[self.noEntryButton setEnabled:YES];
	}
	
	[self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self deRegisterNotifications];
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentStartNotificationReceive) name:kAppointmentStartNotification object:nil];
    
}
- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentStartNotification object:nil];
}

-(void) onAppointmentStartNotificationReceive{
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
																															style:PSBarButtonItemStyleDefault
																														 target:self
																														 action:@selector(onClickNoEntryButton)];
	self.notesButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														style:PSBarButtonItemStyleNotes
																													 target:self
																													 action:@selector(onClickNotesButton)];
	_appointmentStatus = [self.appointment getStatus];
	_appointmentType = [self.appointment getType];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.notesButton, self.noEntryButton, nil]];
	if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
			|| [self.appointment getStatus] == AppointmentStatusNoEntry)
	{
		[self.noEntryButton setEnabled:NO];
	}
	else
	{
		[self.noEntryButton setEnabled:YES];
	}
	
	[self setTitle:[appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
	
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickNoEntryButton
{
	CLS_LOG(@"onClickNoEntryButton");
	PSNoEntryNoCardViewController * noEntryView = [[PSNoEntryNoCardViewController alloc] initWithNibName:NSStringFromClass([PSNoEntryNoCardViewController class])
																																																bundle:[NSBundle mainBundle]];
	[noEntryView setAppointment:self.appointment];
	[self.navigationController pushViewController:noEntryView animated:YES];
}

- (void) onClickNotesButton
{
	CLS_LOG(@"onClickNotesButton");
	PSAppointmentNotesViewController *appointmentNotesViewConroller = [[PSAppointmentNotesViewController alloc] initWithNibName:@"PSAppointmentNotesViewController" bundle:[NSBundle mainBundle]];
	[appointmentNotesViewConroller setAppointment:self.appointment];
	[self.navigationController pushViewController:appointmentNotesViewConroller animated:YES];
}
#pragma mark - IBActions
- (IBAction)onClickStartAppoiontmentButton:(id)sender
{
	[self.appointment startAppointment];
	[self hideTableHeaderView];
}
- (IBAction)onClickAcceptAppoiontmentButton:(id)sender
{
    [self.appointment acceptAppointment];
    [_btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
    [_btnStartAppointment removeTarget:self action:@selector(onClickAcceptAppoiontmentButton) forControlEvents:UIControlEventTouchUpInside];
    [_btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	CLS_LOG(@"Appointment ID: %@", self.appointment.appointmentId);
	if (alertView.tag == AlertViewTagTrash)
	{
		if(buttonIndex == alertView.cancelButtonIndex)
		{
			//Delete Appointment
			[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																	 @"Deleting",
																	 @"Appointment"]];
			[self disableNavigationBarButtons];
			[[PSAppointmentsManager sharedManager] deleteAppointment:self.appointment];
			[self schduleUIControlsActivation];
			CLS_LOG(@"Delete Appointment");
		}
	}
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	NSInteger numberOfSelections = kNumberOfSections;
	return numberOfSelections;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	/*
	 The count check ensures that if array is empty, i.e no record exists
	 for particular section, the header height is set to zero so that
	 section header is not alloted any space.
	 */
	
	CGFloat headerHeight = 0.0;
	
	if(section == kSectionJSV)
	{
		headerHeight = kViewSectionHeaderHeight;
	}
	else if (section == kSectionAsbestos)
	{
		headerHeight = kViewSectionHeaderHeight;
	}
	else if (section == kSectionTenant)
	{
		headerHeight = kViewSectionHeaderHeight;
	}
	
	else if (section == kSectionNotes)
	{
		headerHeight = kViewSectionHeaderHeight;
	}
	else
	{
		headerHeight = kViewSectionHeaderHeight;
	}
	return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0.0;
	if(indexPath.section == kSectionProperty)
	{
		rowHeight = kFaultPropertyCellHeight;
	}
	else if (indexPath.section == kSectionType)
	{
		rowHeight = kTypeCellHeight;
		
	}
	else if (indexPath.section == kSectionTenant)
	{
		rowHeight = kTenantCellHeight;
	}
	
	else if (indexPath.section == kSectionAsbestos)
	{
		rowHeight = kAsbestosCellHeight;
	}
	
	else if (indexPath.section == kSectionJSV)
	{
		rowHeight = 65;
	}
	else if (indexPath.section == kSectionNotes)
	{
		if (!isEmpty(self.appointment.appointmentNotes))
		{
			rowHeight = [self heightForTextView:nil containingString:self.appointment.appointmentNotes];
			rowHeight += 25;
			
			if (rowHeight < 50)
			{
				rowHeight = 50;
			}
		}
		
		else
		{
			rowHeight = 50;
		}
	}
	
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
	NSInteger numberOfRows=0;
	
	if (section == kSectionProperty)
	{
		numberOfRows = kSectionPropertyRows;
	}
	
	else if (section == kSectionType)
	{
		numberOfRows = kSectionTypeRows;
	}
	
	else if (section == kSectionTenant)
	{
		numberOfRows = [self.tenantsArray count];
	}
	
	else if (section == kSectionAsbestos)
	{
		if (!isEmpty(self.asbestosArray))
		{
			numberOfRows = [self.asbestosArray count];
		}
		else
		{
			numberOfRows = 1;
		}
	}
	
	else if (section == kSectionJSV)
	{
		numberOfRows = [self.jobDataListArray count];
	}
	else if (section == kSectionNotes)
	{
		numberOfRows = 1;
	}
	return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CLS_LOG(@"Index Path Section : %ld", indexPath.section);
	UITableViewCell *_cell = nil;
	if (indexPath.section == kSectionProperty)
	{
		PSVWRPropertyCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRPropertyCell class])];
		[cell configureCell:appointment];
		[cell setAddressGestureRecognizer:self action:@selector(onClickAddress:)];
		[cell setImagePropertyGestureRecognizer:self action:@selector(onClickPropertyImage:)];
		_cell = cell;
	}
	else if (indexPath.section == kSectionType)
	{
		PSVWRTypeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRTypeCell class])];
		[cell configureCell:appointment];
		_cell = cell;
	}
	
	else if (indexPath.section == kSectionTenant)
	{
		PSVWRTenantCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRTenantCell class])];
		[cell configureCell:tenantsArray Index:indexPath.row];
		[cell setEditTenant:self Action:@selector(onClickEditTenantButton:)];
		[cell setTelephone:self Action:@selector(onClickTelephoneButton:)];
		[cell setMobile:self Action:@selector(onClickMobileButton:)];
		[cell setDisclosureIndicator:self Action:@selector(onClickDisclosureButton:)];
		_cell = cell;
	}
	else if (indexPath.section == kSectionAsbestos)
	{
		PSVWRAsbestosCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRAsbestosCell class])];
		[cell configureCell:self.asbestosArray :indexPath.row];
		_cell = cell;
	}
	else if (indexPath.section == kSectionJSV)
	{
		PSVWRJSVCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRJSVCell class])];
		[cell configureCell:self.appointment Index:indexPath.row];
		_cell = cell;
		
	}
	else if (indexPath.section == kSectionNotes)
	{
		PSVWRAppointmentNotesCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRAppointmentNotesCell class])];
		[cell configureCell:self.appointment];
		_cell = cell;
	}
	_cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	return _cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if (indexPath.section == kSectionJSV)
	{
        if([self.appointment getStatus] !=  AppointmentStatusNotStarted){
            if([self.appointment getStatus] != AppointmentStatusAccepted){
                NSArray *objAsbestosArray = [self.appointment.appointmentToProperty.propertyToPropertyAsbestosData allObjects];
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:VT_SB_WorksRequired bundle:nil];
                PSVWRJobDetailViewController * jobDetailDetailViewConroller = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSVWRJobDetailViewController class])];
                [jobDetailDetailViewConroller setAppointment:self.appointment];
                [jobDetailDetailViewConroller setJobSheet:[self.jobDataListArray objectAtIndex:indexPath.row]];
                [jobDetailDetailViewConroller setJobAsbestosArray:objAsbestosArray];
                [self.navigationController pushViewController:jobDetailDetailViewConroller animated:YES];
            }
            else{
                UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_ALERT")
                                                               message:LOC(@"KEY_READ_ONLY_JOB_SHEET_DENIED")
                                                              delegate:nil
                                                     cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                     otherButtonTitles:nil, nil];
                [alert show];
                
            }
            
        }
        
		
	}
}

#pragma mark - Edit Tenant Button Selector
- (IBAction)onClickEditTenantButton:(id)sender
{
	CLS_LOG(@"onClickEditAddressButton");
	
	UIButton *btnEditTenant = (UIButton *) sender;
	Customer *customer = [self.tenantsArray objectAtIndex:btnEditTenant.tag];
	NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
	if (customer != nil)
 {
		PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:NSStringFromClass([PSTenantDetailViewController class]) bundle:[NSBundle mainBundle]];
		[tenantDetailViewConroller setAddress:address];
		[tenantDetailViewConroller setTenant:customer];
		[tenantDetailViewConroller setEditing:YES];
		[self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
	}
}

- (IBAction)onClickDisclosureButton:(id)sender
{
	UIButton *btnDisclosure = (UIButton *) sender;
	Customer *customer = [self.tenantsArray objectAtIndex:btnDisclosure.tag];
	NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
	if (customer != nil)
	{
		PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:NSStringFromClass([PSTenantDetailViewController class]) bundle:[NSBundle mainBundle]];
		[tenantDetailViewConroller setAddress:address];
		[tenantDetailViewConroller setTenant:customer];
		[self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
	}
}

- (IBAction)onClickTelephoneButton:(id)sender
{
	UIButton *btnTel = (UIButton *)sender;
	Customer *customer = [self.tenantsArray objectAtIndex:btnTel.tag];
	[self setupDirectCallToNumber:customer.telephone];
}

- (IBAction)onClickMobileButton:(id)sender
{
	// [self setupDirectCallToNumber:self.customer.mobile];
	UIButton *btnMob = (UIButton *)sender;
	Customer *customer = [self.tenantsArray objectAtIndex:btnMob.tag];
	[self setupDirectCallToNumber:customer.mobile];
}

- (void) setupDirectCallToNumber:(NSString *)number
{
	if (!isEmpty(number))
	{
		number = [UtilityClass getValidNumber:number];
		CLS_LOG(@"Direct Call Number Verified: %@", number);
		NSString *url = [@"tel:" stringByAppendingString:number];
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
	}
}

#pragma mark - Core Data Update Events
- (void) onAppointmentObjectUpdate
{
	dispatch_async(dispatch_get_main_queue(), ^{
		if([self isViewLoaded] && self.view.window)
		{
			[self.tableView reloadData];
		}
	});
}

- (void)popOrCloseViewController
{
	[super popOrCloseViewController];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(300, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(300, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}

#pragma mark - Methods

- (void) disableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons)
	{
		button.enabled = NO;
	}
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = NO;
	}
}

- (void) enableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons)
	{
		button.enabled = YES;
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = YES;
	}
}

- (void) addTableHeaderView
{
    CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
	UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
	headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	
	UILabel *lbl = [[UILabel alloc] init];
	lbl.frame = CGRectMake(8, 0, 245, 29);
	lbl.center = CGPointMake(lbl.center.x, headerView.center.y);
	lbl.backgroundColor = [UIColor clearColor];
	[lbl setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[lbl setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[lbl setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
	lbl.text =LOC(@"KEY_STRING_APPOINTMENT_START_MESSAGE");
	
	_btnStartAppointment = [[UIButton alloc]init];
	_btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
	_btnStartAppointment.center = CGPointMake(_btnStartAppointment.center.x, headerView.center.y);
	[_btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
	[_btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:
	 UIControlStateNormal];
    if([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame){
        [_btnStartAppointment setTitle:LOC(@"KEY_STRING_ACCEPT") forState:UIControlStateNormal];
        [_btnStartAppointment addTarget:self action:@selector(onClickAcceptAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if([self.appointment.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame){
        [_btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
        [_btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    }

	
	[headerView addSubview:lbl];
	[headerView addSubview:_btnStartAppointment];
	
	if ([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame || [self.appointment.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame)
	{
		headerViewRect = self.tableView.tableHeaderView.frame;
		headerViewRect.size.height = 40;
		headerViewRect.size.width = 320;
		[self.tableView.tableHeaderView setFrame:headerViewRect];
		[self.tableView setTableHeaderView:headerView];
	}
}

- (void) hideTableHeaderView
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.5];
	[self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
	self.tableView.tableHeaderView = nil;
	[self.tableView.tableHeaderView setHidden:YES];
	//[self.tableView reloadData];
	[UIView commitAnimations];
}

- (IBAction)onClickAddress:(UIGestureRecognizer *)sender
{
	// Code to respond to gesture here
	PSMapsViewController * mapViewController = [[PSMapsViewController alloc] initWithNibName:NSStringFromClass([PSMapsViewController class]) bundle:[NSBundle mainBundle]];
	if(self.appointment.appointmentToProperty)
	{
		[mapViewController setAddress:[self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull]];
		[mapViewController setNavigationTitle:[self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
	}
	else
	{
		//[mapViewController setAddress:[self.appointment.appointmentToScheme fullAddress]];
		//[mapViewController setNavigationTitle:[self.appointment.appointmentToScheme shortAddress]];
	}
	[self.navigationController pushViewController:mapViewController animated:YES];
	
}

- (IBAction)onClickPropertyImage:(UIGestureRecognizer *)sender
{
	// Code to respond to gesture here
	PSImageViewController * imageViewController = [[PSImageViewController alloc] initWithNibName:NSStringFromClass([PSImageViewController class]) bundle:[NSBundle mainBundle]];
	
	//imageViewController.propertyObj=self.appointment.appointmentToProperty;
	
	[self.navigationController pushViewController:imageViewController animated:YES];
	
}

//
@end
