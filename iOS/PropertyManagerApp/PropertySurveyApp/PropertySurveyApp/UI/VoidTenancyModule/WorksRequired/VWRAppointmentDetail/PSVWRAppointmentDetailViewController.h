//
//  PSAppointmentDetailViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSVWRTypeCell;
@class PSVWRTenantCell;
@class PSVWRAsbestosCell;
@class PSVWRJSVCell;
@class PSVWRAccomodationsCell;
@class Appointment;
@class PSBarButtonItem;
@class PSAccomodationViewController;
@class PSVWRAppointmentNotesCell;
@class PSVWRPropertyCell;
@class PSVWRAppointmentSchemeCell;

@interface PSVWRAppointmentDetailViewController : PSCustomViewController

@property (strong,   nonatomic) Appointment *appointment;
@property (weak,   nonatomic) Property *property;
@property (strong, nonatomic) UIButton *btnStartAppointment;
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) NSMutableArray *tenantsArray;
@property (strong, nonatomic) NSMutableArray *jobDataListArray;
@property (strong, nonatomic) NSMutableArray *asbestosArray;
@property (strong, nonatomic) PSBarButtonItem *trashButton;
@property (strong, nonatomic) PSBarButtonItem *downloadButton;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong, nonatomic) PSBarButtonItem *noEntryButton;
@property (strong, nonatomic) PSBarButtonItem *notesButton;
@property (strong, nonatomic) PSAccomodationViewController *accomodationlViewConroller;
@property (strong, nonatomic) Customer *customer;

- (void) onAppointmentObjectUpdate;
@end
