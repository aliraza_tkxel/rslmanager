//
//  TenantCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVWRTenantCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTenantName;

@property (strong, nonatomic) IBOutlet UILabel *lblTelephone;
@property (strong, nonatomic) IBOutlet UILabel *lblMobile;
@property (strong, nonatomic) IBOutlet UIButton *btnEditTenant;
@property (strong, nonatomic) IBOutlet UIButton *btnTelephone;
@property (strong, nonatomic) IBOutlet UIButton *btnMobile;
@property (strong, nonatomic) IBOutlet UIButton *btnDisclosureIndicator;

-(void)configureCell:(id)data Index:(NSInteger)index;
-(void)setEditTenant:(id)owner Action:(SEL)action;
-(void)setTelephone:(id)owner Action:(SEL)action;
-(void)setMobile:(id)owner Action:(SEL)action;
-(void)setDisclosureIndicator:(id)owner Action:(SEL)action;

@end
