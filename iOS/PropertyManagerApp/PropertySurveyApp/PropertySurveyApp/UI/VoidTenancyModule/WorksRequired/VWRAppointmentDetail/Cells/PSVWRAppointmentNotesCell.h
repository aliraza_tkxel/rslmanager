//
//  PSAppointmentDetailNotesCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVWRAppointmentNotesCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextView *txtViewNotes;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator;

-(void)configureCell:(id)data;

@end
