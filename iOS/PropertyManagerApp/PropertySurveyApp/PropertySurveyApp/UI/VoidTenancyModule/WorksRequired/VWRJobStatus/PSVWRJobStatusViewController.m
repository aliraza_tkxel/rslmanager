//
//  PSJobStatusViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate.h"
#import "PSVWRJobStatusViewController.h"
#import "PSVWRReasonOrRepairCell.h"
#import "PSVWRNotesCell.h"
#import "PSVWRFollowOnRequiredCell.h"
#import "PSBarButtonItem.h"
#import "PSTextViewCell.h"
#import "PSRepairListViewController.h"
#import "FaultJobSheet+JSON.h"
#import "Appointment+JSON.h"
#import "PSRepairImagesViewController.h"
#import "PSRepairImagesGalleryViewController.h"

/*!
 @discussion
 Number of Sections in Pause Data Source and their names.
 */
#define kDataSourcePause 0
#define kPauseNumberOfSections 2
#define kPauseSectionReason 0
#define kPauseSectionNotes 1


#define kPauseSectionReasonRows 1
/**/
/*!
 @discussion
 Number of rows in each section of Pause Data Source.
 */
#define kPauseSectionReasonRows 1
#define kPauseSectionNotesRows 1


#define kSectionHeaderHeight 18

/*!
 @discussion
 Cell height for each custom cell in Pause Data Source.
 */
#define kReasonCellHeight 35
#define kPauseNotesCellHeight 235


/*!
 @discussion
 Number of Sections in Complete Data Source and their names.
 */
#define kCompleteNumberOfSection 5
#define kFaultDetailSection 0
#define kRepairDetailsSection 1
#define kRepairNotesSection 2
#define kFollowOnWorkSection 3
#define kFollowOnNotesSection 4

/*!
 @discussion
 Number of rows in each section of Complete Data Source.
 */
#define kRepairDetailSectionRows 1
#define kRepairNotesSectionRows 1
#define kFollowOnWorkSectionRows 1
#define kFollowOnWorkNotesRows 1
#define kFaultDetailRows 1

#define kRowSelectRepairs 0

#define kRepairListViewMode 0
#define kRecentViewMode 1
/*!
 @discussion
 Cell height for each custom cell in Complete Data Source.
 */
#define kCompleteRepaorDetailCellHeight 35
#define kCompleteFollowOnWorkHeight 36

@interface PSVWRJobStatusViewController ()
{
	NSString *_reason;
	NSString *_repairNotes;
	NSString *_followOnNotes;
	NSArray *_pauseReasons;
	NSMutableArray *_pauseReasonStrings;
	NSString *_pauseNotes;
	// PSTextView *_textView;
	JobPauseData *_jobPauseReason;
	NSArray *_jobRepairList;
	BOOL _isfollowOnWorkRequired;
	BOOL _isInternetAvailable;
}

@end

@implementation PSVWRJobStatusViewController

@synthesize tableView;
@synthesize viewMode;
@synthesize headerViewArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.isEditing = NO;
	_isInternetAvailable = IS_NETWORK_AVAILABLE;
	self.headerViewArray = [NSMutableArray array];
	_pauseReasonStrings = [NSMutableArray array];
	[self.view setBackgroundColor:UIColorFromHex (THEME_BG_COLOUR)];
	[self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	self.lblTitle.textColor = UIColorFromHex(NAVIGATION_BAR_TITLE_COLOR);
	
	[self loadSectionHeader];
	[self loadNavigationonBarItems];
	[self registerNibsForTableView];
	[self.tableView setUserInteractionEnabled:YES];
	_isfollowOnWorkRequired = NO;
	if (self.viewMode == PauseJobViewMode)
	{
		//[self.tableView setScrollEnabled:NO];
		NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
		[requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:@"username"];
		[requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:@"salt"];
		_pauseReasons = [[PSFaultManager sharedManager]fetchAllJobReasons];
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			
			if ([_pauseReasons count] <= 0)
			{
				if(_isInternetAvailable)
				{
					[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																			 LOC(@"KEY_ALERT_LOADING"),
																			 @"Reasons"]];
				}
				
			}
			[[PSFaultManager sharedManager]fetchJobPauseReasonList:requestParameters];
		});
		
		if ([_pauseReasons count] <= 0 && !_isInternetAvailable)
		{
			[self hideActivityIndicator];
			// _isInternetAvailable = NO;
			UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																										 message:LOC(@"KEY_ALERT_PAUSE_REASONS_NO_INTERNET")
																										delegate:nil
																					 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																					 otherButtonTitles:nil,nil];
			
			[alert show];
			
		}
		
		[self populatePauseList];
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		[self.tableView setScrollEnabled:YES];
		self.lblTitle.text = @"Complete this Job?";
	}
}

-(void)registerNibsForTableView
{
	NSString * cellName = NSStringFromClass([PSVWRNotesCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
	cellName = NSStringFromClass([PSVWRReasonOrRepairCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
	cellName = NSStringFromClass([PSVWRFollowOnRequiredCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:cellName bundle:nil] forCellReuseIdentifier:cellName];
	[self.tableView registerClass:[PSTextViewCell class] forCellReuseIdentifier:NSStringFromClass([PSTextViewCell class])];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if (!isEmpty(self.jobData.faultRepairDataList))
	{
		_jobRepairList = [self.jobData.faultRepairDataList allObjects];
	}
	else
	{
		_jobRepairList = nil;
	}
	[self.tableView reloadData];
	[self registerNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	[self setLblTitle:nil];
	[self setBtnConfirm:nil];
	[self setBtnCancel:nil];
	[self setTableView:nil];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	NSString *title = [NSString stringWithFormat:@"Job %@",self.jobData.jsNumber];
	
	[self setTitle:title];
	
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
	CLS_LOG(@"onClickPauseButton");
	[self popOrCloseViewController];
}

- (void) onClickPauseButton
{
	CLS_LOG(@"onClickPauseButton");
	
}

- (void) onClickPlayButton
{
	CLS_LOG(@"onClickPlayButton");
	
	
}


#pragma mark - Table View Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	NSInteger numberOfSections = 0;
	if (self.viewMode == PauseJobViewMode )
	{
		numberOfSections = kPauseNumberOfSections;
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		numberOfSections = kCompleteNumberOfSection;
	}
	return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
	NSInteger numberOfRows=0;
	
	if (self.viewMode == PauseJobViewMode)
	{
		if(section == kPauseSectionReason){
			numberOfRows = kPauseSectionReasonRows;
		}
		
		else if (section == kPauseSectionNotes){
			numberOfRows = kPauseSectionNotesRows;
			
		}
		
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		if (section == kFaultDetailSection)
		{
			numberOfRows = kFaultDetailRows;
		}
		
		else if (section == kRepairDetailsSection)
		{
			if (!isEmpty(self.jobData.faultRepairDataList))
			{
				numberOfRows = [[self.jobData.faultRepairDataList allObjects] count] + kRepairDetailSectionRows;
			}
			else
			{
				numberOfRows  = kRepairDetailSectionRows;
			}
		}
		else if (section == kRepairNotesSection)
		{
			numberOfRows  = kRepairNotesSectionRows;
		}
		else if (section == kFollowOnWorkSection)
		{
			numberOfRows  = kFollowOnWorkSectionRows;
		}
		else if (section == kFollowOnNotesSection)
		{
			numberOfRows  = kFollowOnWorkNotesRows;
		}
	}
	return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	CGFloat rowHeight = 0.0;
	CGFloat scale = 1.0;
	if (self.isEditing)
	{
		scale = 1.03;
	}
	
	if (self.viewMode == PauseJobViewMode )
	{
		if(indexPath.section == kPauseSectionReason){
			rowHeight = kReasonCellHeight;
		}
		else if (indexPath.section == kPauseSectionNotes)
		{
			rowHeight = [self heightForTextView:nil containingString:_pauseNotes withScale:scale];
			if (rowHeight < 200)
			{
				rowHeight = 200;
			}
		}
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		if (indexPath.section == kFaultDetailSection)
		{
			rowHeight = kCompleteRepaorDetailCellHeight;
		}
		if (indexPath.section == kRepairDetailsSection)
		{
			rowHeight = kCompleteRepaorDetailCellHeight;
		}
		else if (indexPath.section == kRepairNotesSection)
		{
			rowHeight = [self heightForTextView:nil containingString:_repairNotes withScale:scale];
			if (rowHeight < 74)
			{
				rowHeight = 74;
			}
		}
		else if (indexPath.section == kFollowOnWorkSection)
		{
			rowHeight = kCompleteFollowOnWorkHeight;
		}
		else if (indexPath.section == kFollowOnNotesSection)
		{
			rowHeight = [self heightForTextView:nil containingString:_followOnNotes withScale:scale];
			if (rowHeight < 74)
			{
				rowHeight = 74;
			}
		}
	}
	return rowHeight;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return kSectionHeaderHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	return [self.headerViewArray objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *_cell = nil;
	
	if (self.viewMode == PauseJobViewMode)
	{
		if (indexPath.section == kPauseSectionReason)
		{
			PSVWRReasonOrRepairCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRReasonOrRepairCell class])];
			[self configureCell:&cell atIndexPath:indexPath];
			_cell = cell;
		}
		else if (indexPath.section == kPauseSectionNotes)
		{
			PSTextViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:textViewCellIdentifier];
			[self configureCell:&cell atIndexPath:indexPath];
			_cell = cell;
		}
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		if (indexPath.section == kFaultDetailSection)
		{
			PSVWRReasonOrRepairCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRReasonOrRepairCell class])];
			[self configureCell:&cell atIndexPath:indexPath];
			_cell = cell;
		}
		else if (indexPath.section == kRepairDetailsSection)
		{
			PSVWRReasonOrRepairCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRReasonOrRepairCell class])];
			[self configureCell:&cell atIndexPath:indexPath];
			_cell = cell;
		}
		else if (indexPath.section == kRepairNotesSection){
			PSTextViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:textViewCellIdentifier];
			cell.parentTableView = self.tableView;
			cell.parentTableViewSection = indexPath.section;
			[self configureCell:&cell atIndexPath:indexPath];
			_cell = cell;
		}
		else if (indexPath.section == kFollowOnWorkSection)
		{
			PSVWRFollowOnRequiredCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRFollowOnRequiredCell class])];
			[self configureCell:&cell atIndexPath:indexPath];
			_cell = cell;
		}
		else if (indexPath.section == kFollowOnNotesSection)
		{
			PSTextViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:textViewCellIdentifier];
			cell.parentTableView = self.tableView;
			cell.parentTableViewSection = indexPath.section;
			[self configureCell:&cell atIndexPath:indexPath];
			_cell = cell;
		}
	}
	[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return _cell;
}


- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
	if (self.viewMode == PauseJobViewMode)
	{
		if ([*cell isKindOfClass:[PSVWRReasonOrRepairCell class]])
		{
			PSVWRReasonOrRepairCell *_cell = (PSVWRReasonOrRepairCell *) *cell;
			_cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
			
			if (!isEmpty(_pauseReasonStrings))
			{
				_cell.pickerOptions = _pauseReasonStrings;
				[_cell.btnPicker setEnabled:YES];
			}
			else
			{
				[_cell.btnPicker setEnabled:NO];
			}
			if (!isEmpty(_reason))
			{
				_cell.lblTitle.text = _reason;
			}
			_cell.lblHeader = LOC(@"KEY_STRING_REASON");
			_cell.delegate = self;
			*cell = _cell;
		}
		else if ([*cell isKindOfClass:[PSTextViewCell class]])
		{
			PSTextViewCell *_cell = (PSTextViewCell *)*cell;
			PSTextView * _textView = [[PSTextView alloc] initWithFrame:CGRectZero];
			_textView.delegate = self;
			_textView.tag = indexPath.section;
			[_textView setFrame:_cell.frame];
			if(!isEmpty(_pauseNotes))
			{
				_textView.text = _pauseNotes;
			}
			// [_textView setScrollEnabled:YES];
			_cell.textView = _textView;
			_cell.textView.editable = YES;
		}
	}
	
	else if (self.viewMode == CompleteJobViewMode)
	{
		if ([*cell isKindOfClass:[PSVWRReasonOrRepairCell class]])
		{
			PSVWRReasonOrRepairCell *_cell = (PSVWRReasonOrRepairCell *) *cell;
			if (indexPath.section == kFaultDetailSection)
			{
				_cell.accessoryType = UITableViewCellAccessoryNone;
				_cell.lblTitle.text = self.jobData.jsnDescription;
				_cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
				[_cell.btnPicker setHidden:YES];
			}
			else if (indexPath.section == kRepairDetailsSection)
			{
				[_cell.btnPicker setHidden:YES];
				_cell.delegate = self;
				if (indexPath.row == kRowSelectRepairs)
				{
					_cell.lblTitle.text = @"Add Repairs";
					_cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					_cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
				}
				
				else
				{
					FaultRepairData *faultRepairData = [_jobRepairList objectAtIndex:(indexPath.row - 1)];
					_cell.lblTitle.text = faultRepairData.faultRepairDescription;
					_cell.accessoryType = UITableViewCellAccessoryNone;
					_cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
				}
			}
			*cell = _cell;
		}
		else if ([*cell isKindOfClass:[PSTextViewCell class]])
		{
			PSTextViewCell *_cell = (PSTextViewCell *) *cell;
			PSTextView * _textView = [[PSTextView alloc] initWithFrame:CGRectZero];
			[_textView setDelegate:self];
			[_textView setTag:indexPath.section];
			[_textView setScrollEnabled:YES];
			[_textView setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
			
			_cell.textView.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
			_cell.textView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
			
			switch (indexPath.section)
			{
				case kRepairNotesSection:
					{
						if (!isEmpty(_repairNotes))
						{
							[_textView setText:_repairNotes];
						}
					}
					break;
				case kFollowOnNotesSection:
					{
						if (!isEmpty(_followOnNotes))
						{
							[_textView setText:_followOnNotes];
						}
						if (_isfollowOnWorkRequired)
						{
							[_textView setUserInteractionEnabled:YES];
						}
						else
						{
							[_textView setUserInteractionEnabled:NO];
							_followOnNotes = nil;
							[_textView setText:_followOnNotes];
							
						}
					}
					break;
				default:
					break;
			}
			_cell.textView.editable = YES;
			_cell.textView = _textView;
			*cell = _cell;
		}
		else if ([*cell isKindOfClass:[PSVWRFollowOnRequiredCell class]])
		{
			PSVWRFollowOnRequiredCell *_cell = (PSVWRFollowOnRequiredCell *) *cell;
			_cell.lblFollowOn.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
			_cell.delegate = self;
			*cell = _cell;
		}
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.viewMode == CompleteJobViewMode)
	{
		if (indexPath.section == kRepairDetailsSection && indexPath.row == kRowSelectRepairs)
		{
			PSRepairListViewController *repairListViewConroller = [[PSRepairListViewController alloc] initWithNibName:@"PSRepairListViewController" bundle:[NSBundle mainBundle]];
			[repairListViewConroller setJobData:self.jobData];
			[self.navigationController pushViewController:repairListViewConroller animated:YES];
		}
	}
}

#pragma mark - UIScrollView Delegates
- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[self resignAllResponders];
}

#pragma mark - Notifications
- (void) registerNotification
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobStatusUpdateNotificationReceive) name:kJobStatusUpdateNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobPauseReasonsSaveSuccessNotificationReceive) name:kFetchJobPauseReasonsSaveSuccessNotification object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobPauseReasonsSaveFailureNotificationReceive) name:kFetchJobPauseReasonsSaveFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobPauseDataSaveSuccessNotificationReceive) name:kSaveJobPauseDataSuccessNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobPauseDataSaveFailureNotificationReceive) name:kSaveJobPauseDataFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobCompleteSuccessNotificationReceive) name:kJobCompleteSuccessNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobCompleteFailureNotificationReceive) name:kJobCompleteFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentStatusUpdateSuccessNotificationReceive) name:kAppointmentStatusUpdateSuccessNotification object:nil];
}

- (void) deRegisterNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kJobStatusUpdateNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchJobPauseReasonsSaveSuccessNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchJobPauseReasonsSaveFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveJobPauseDataSuccessNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveJobPauseDataFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kJobCompleteSuccessNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kJobCompleteFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentStatusUpdateSuccessNotification object:nil];
}

- (void) onJobStatusUpdateNotificationReceive
{
}

- (void) onJobPauseReasonsSaveSuccessNotificationReceive
{
	_pauseReasons = [[PSFaultManager sharedManager]fetchAllJobReasons];
	[self populatePauseList];
	[self hideActivityIndicator];
	[self.tableView reloadData];
}

- (void) onJobPauseDataSaveSuccessNotificationReceive
{
	[self hideActivityIndicator];
	[self popOrCloseViewController];
}

- (void) onJobPauseDataSaveFailureNotificationReceive
{
	[self hideActivityIndicator];
}

- (void) onJobPauseReasonsSaveFailureNotificationReceive
{
	[self hideActivityIndicator];
//#warning failed to download pause reasons alert
}

- (void) onJobCompleteSuccessNotificationReceive
{
	// NSMutableDictionary *jobDictionary = [NSMutableDictionary dictionaryWithDictionary:[self.jobData JSONValue]];
	if (self.viewMode == CompleteJobViewMode)
	{
		BOOL isAppointmentComplete = [[PSAppointmentsManager sharedManager]isFaultAppointmentComplete:self.jobData.jobSheetToAppointment];
		if (isAppointmentComplete)
		{
			[[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.jobData.jobSheetToAppointment getType]] appointment:self.jobData.jobSheetToAppointment];
		}
	}
}

- (void) onJobCompleteFailureNotificationReceive
{
//#warning failed to complete alert
}

- (void) onAppointmentStatusUpdateSuccessNotificationReceive
{
	UIViewController * viewController = [self.navigationController.viewControllers objectAtIndex:1];
	[self.navigationController popToViewController:viewController animated:YES];
}

#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	BOOL textVeiwShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextView *)self.selectedControl).text stringByAppendingString:text];
	if (self.viewMode == PauseJobViewMode)
	{
		if ([stringAfterReplacement length] > 100)
		{
			textVeiwShouldReturn = NO;
		}
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		if ([stringAfterReplacement length] > 500)
		{
			textVeiwShouldReturn = NO;
		}
	}
	return textVeiwShouldReturn;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	
	self.isEditing = NO;
	if (self.viewMode == PauseJobViewMode)
	{
		_pauseNotes = textView.text;
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		switch (textView.tag) {
			case kRepairNotesSection:
				_repairNotes = textView.text;
				break;
			case kFollowOnNotesSection:
				_followOnNotes = textView.text;
				break;
			default:
				break;
		}
	}
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	//[self scrollToRectOfSelectedControl];
}

- (void)textViewDidChange:(UITextView *)textView
{
	self.isEditing = YES;
	if (self.viewMode == CompleteJobViewMode)
	{
		switch (textView.tag) {
			case kRepairNotesSection:
				_repairNotes = textView.text;
				break;
			case kFollowOnNotesSection:
				_followOnNotes = textView.text;
				break;
			default:
				break;
		}
	}
	else if (self.viewMode == PauseJobViewMode)
	{
		_pauseNotes = textView.text;
	}
	
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	//[self scrollToRectOfSelectedControl];
}

#pragma mark - Scroll Text Field Methods
- (void)scrollToRectOfSelectedControl
{
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.selectedControl.tag] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string withScale:(CGFloat)scale
{
	float horizontalPadding = 0;
	float verticalPadding = 82;
	if(OS_VERSION >= 7.0)
	{
		verticalPadding = 47;
	}
	CGFloat height = 0.0;
	if(OS_VERSION >= 7.0)
	{
		NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
																[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17],NSFontAttributeName,
																nil];
		CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(280, 999999.0f)
																							 options:NSStringDrawingUsesLineFragmentOrigin
																						attributes:attributes
																							 context:nil];
		height = boundingRect.size.height + verticalPadding;
	}
	else
	{
		height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
								constrainedToSize:CGSizeMake(280, 999999.0f)
										lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
	}
	return height * scale;
}

#pragma mark - Methods
- (void) loadSectionHeader
{
	if (self.viewMode == PauseJobViewMode)
	{
		NSArray *sectionTitles = @[LOC(@"KEY_STRING_REASON"),LOC(@"KEY_STRING_NOTES")];
		[self loadSectionHeaderViews:sectionTitles headerViews:self.headerViewArray];
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		NSArray *sectionTitles = @[LOC(@"KEY_STRING_VOID_WORK_DETAILS"),LOC(@"KEY_STRING_DETAILS_OF_REPAIR_CARRIED_OUR"),LOC(@"KEY_STRING_REPAIR_NOTES"),@"",LOC(@"KEY_STRING_FOLLOW_ON_NOTES")];
		[self loadSectionHeaderViews:sectionTitles headerViews:self.headerViewArray];
		
	}
}

-(void) populatePauseList
{
 if (!isEmpty(_pauseReasons))
 {
	 NSSortDescriptor *_sortDescriptor;
	 NSArray *_sortDescriptors;
	 if (!_sortDescriptor)
	 {
		 _sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kPauseReasonTitle ascending:YES];
		 _sortDescriptors = @[_sortDescriptor];
	 }
	 _pauseReasons = [NSMutableArray arrayWithArray:[_pauseReasons sortedArrayUsingDescriptors:_sortDescriptors]];
	 
		for (JobPauseReason *pauseReason in _pauseReasons)
		{
			if (pauseReason != nil) {
				if (![_pauseReasonStrings containsObject:pauseReason.pauseReason])
				{
					[_pauseReasonStrings addObject:pauseReason.pauseReason];
				}
			}
		}
	}
	
}

-(NSDictionary *)getRequestParameters
{
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:kFollowOnWorkSection];
	PSVWRFollowOnRequiredCell *cell = (PSVWRFollowOnRequiredCell *) [self.tableView cellForRowAtIndexPath:indexPath];
	BOOL isFollowOnWorkRequired = cell.swtFollowOn.on;
	NSMutableDictionary *updatedValues = [NSMutableDictionary dictionary];
	[updatedValues setObject:isEmpty(_repairNotes)?[NSNull null]:_repairNotes forKey:kJobRepairNotes];
	[updatedValues setObject:isEmpty(_followOnNotes)?[NSNull null]:_followOnNotes forKey:kFollowOnNotes];
	[updatedValues setObject:[NSNumber numberWithBool:isFollowOnWorkRequired] forKey:@"isFollowOnWorkRequired"];
	return updatedValues;
}

#pragma mark - IBActions
- (IBAction)onClickConfirmButton:(id)sender
{
	NSString *jobStatus = nil;
	if (self.viewMode == PauseJobViewMode)
	{
		if (!isEmpty(_reason) && !isEmpty(_pauseNotes))
		{
            NSMutableDictionary *pauseJobDictionary = [NSMutableDictionary dictionary];
            [pauseJobDictionary setObject:isEmpty(_pauseNotes)?[NSNull null]:_pauseNotes forKey:kPauseNote];
            [pauseJobDictionary setObject:isEmpty(_jobPauseReason)?[NSNull null]:_jobPauseReason forKey:kJobPauseReason];
            [pauseJobDictionary setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:@"pausedBy"];
			[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...", @"Pausing ", @"Job"]];
			[[PSDataUpdateManager sharedManager] updateJobStatus:self.jobData
                                                        jobStaus:kJobStatusPaused
                                                    andPauseData:pauseJobDictionary];
		}
		else
		{
			UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Pause Job"
																										 message:LOC(@"KEY_ALERT_EMPTY_MANDATORY_FIELDS")
																										delegate:nil
																					 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																					 otherButtonTitles:nil,nil];
			[alert show];
		}
	}
	else if (self.viewMode == CompleteJobViewMode)
	{
		if(_isfollowOnWorkRequired == YES && [_followOnNotes length] < 1) {
			
			UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ATTENTION")
																										 message:LOC(@"KEY_ALERT_ADD_NOTES")
																										delegate:self
																					 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																					 otherButtonTitles:nil, nil];
			[alert show];
			return;
		}
		
		if(_jobRepairList == nil || [_jobRepairList count] == 0) {
			
			UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ATTENTION")
																										 message:LOC(@"KEY_ALERT_SELECT_REPAIR")
																										delegate:self
																					 cancelButtonTitle:LOC(@"KEY_ALERT_ADD_REPAIR")
																					 otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"), nil];
			alert.tag = AlertViewTagFaultRepairsEmpty;
			[alert show];
			return;
		}
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOC(@"KEY_ALERT_CONFIRM_POPUP_HEADING") message:LOC(@"KEY_CONFIRM_COMPLETE_ACTION") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_NO") style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            NSLog(@"Cancelled Appointment Completion");
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSLog(@"Confirmed Appointment Completion");
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_PHOTOGRAPH_A_REPAIR")
                                                           message:LOC(@"KEY_ALERT_AFTER_FAULT_PICTURE")
                                                          delegate:self
                                                 cancelButtonTitle:LOC(@"KEY_ALERT_YES")
                                                 otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"), nil];
            alert.tag = AlertViewTagFaultPicture;
            [alert show];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated: YES completion: nil];
		
		
	}
}

- (IBAction)onClickCancelButton:(id)sender
{
	CLS_LOG(@"onClickBtnCancel");
	[self popOrCloseViewController];
}

#pragma mark - Protocol Methods
- (void) didSelectPickerOption:(NSInteger)pickerOption
{
	_reason = [_pauseReasonStrings objectAtIndex:pickerOption];
	_jobPauseReason = [_pauseReasons objectAtIndex:pickerOption];
	[self.tableView reloadData];
	[self.view endEditing:YES];
}

- (void) didChangeSwitchOption:(BOOL) valueChanged
{
	_isfollowOnWorkRequired = valueChanged;
	[self.tableView reloadData];
}

#pragma mark - UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(alertView.tag == AlertViewTagFaultPicture)
	{
		CLS_LOG(@"Clicked at Index %ld",buttonIndex);
		if(buttonIndex == 0) //YES
		{
			[self.jobData setCompletionDate:[NSDate date]];
			PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
			if(self.jobData.jobSheetToAppointment.appointmentToProperty)
			{
				[repairImagesViewConroller setPropertyObj:self.jobData.jobSheetToAppointment.appointmentToProperty];
				[repairImagesViewConroller setSchemeObj:nil];
			}
			else
			{
				[repairImagesViewConroller setSchemeObj:self.jobData.jobSheetToAppointment.appointmentToScheme];
				[repairImagesViewConroller setPropertyObj:nil];
			}
			[repairImagesViewConroller setJobDataListObj:self.jobData];
			[repairImagesViewConroller setIsJobComplete:YES];
			[repairImagesViewConroller setRepairCompletionDictionary:[self getRequestParameters]];
			[self.navigationController pushViewController:repairImagesViewConroller animated:YES];
		}
	}
	else if(alertView.tag == AlertViewTagFaultRepairsEmpty)
	{
		CLS_LOG(@"Clicked at Index %ld",buttonIndex);
		if(buttonIndex == 0) //Add Repair
		{
			PSRepairListViewController *repairListViewConroller = [[PSRepairListViewController alloc] initWithNibName:@"PSRepairListViewController" bundle:[NSBundle mainBundle]];
			[repairListViewConroller setJobData:self.jobData];
			[self.navigationController pushViewController:repairListViewConroller animated:YES];
		}
	}
}

-(void)dealloc
{
	
}

@end
