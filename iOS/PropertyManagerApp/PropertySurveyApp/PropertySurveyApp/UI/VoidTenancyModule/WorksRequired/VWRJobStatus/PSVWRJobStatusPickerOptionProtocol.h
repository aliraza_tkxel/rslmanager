//
//  PSVWRJobStatusPickerOptionProtocol.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-08-17.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

@protocol PSVWRJobStatusPickerOptionProtocol <NSObject>

@required
/*!
 @discussion
 Method didPickerOptionSelect Called upon selection of any picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerOption;

/*!
 @discussion
 Method didPickerOptionSelect Called upon changing switch value.
 */
- (void) didChangeSwitchOption:(BOOL) valueChanged;

@optional


@end
