//
//  PSJobStatusViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextViewCell.h"
#import "PSVWRJobStatusPickerOptionProtocol.h"


@interface PSVWRJobStatusViewController : PSCustomViewController <PSVWRJobStatusPickerOptionProtocol, UITextViewDelegate>

@property (assign, nonatomic) ViewMode viewMode;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) FaultJobSheet *jobData;
@property (strong, nonatomic) PSBarButtonItem *playButton;

@end
