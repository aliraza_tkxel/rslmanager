//
//  PSFaultDetailsCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVWRWorkCheckboxCell.h"
#import "FaultJobSheet+Methods.h"


@implementation PSVWRWorkCheckboxCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(id)data atIndexPath:(NSIndexPath *)indexPath withDelegate:(id<JobDetailProtocol>) deleg
{
	self._jobData = data;
	[self setDelegate:deleg];
	
	if (!isEmpty(__jobData.isLegionella)) {
		[_btnLegionellaSelected setSelected:[__jobData.isLegionella boolValue]];
	}
	
	
	if ([self._jobData.jobStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame
			|| [self._jobData.jobStatus compare:kAppointmentStatusAccepted] == NSOrderedSame
			)
	{
      self.btnLegionellaSelected.enabled = FALSE;
	}else{
			self.btnLegionellaSelected.enabled = TRUE;
	}
	
}

#pragma mark - Button Events
- (IBAction)onClickLegionellaSelected:(id)sender
{
	UIButton * button = sender;
	BOOL isSelected = NO;
	if(button.selected == YES)
	{
		isSelected = NO;
	}
	else
	{
		isSelected = YES;
	}
	
	self._jobData.isLegionella = [NSNumber numberWithBool:isSelected];
	[button setSelected:isSelected];
	if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(updateLigionella:)])
	{
		[(PSVWRJobDetailViewController *)self.delegate updateLigionella:isSelected];
	}
}

@end
