//
//  PSFaultDetailsCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVWRWorkDetailsCell.h"
#import "FaultJobSheet+Methods.h"

@interface PSVWRWorkDetailsCell ()
{
	FaultJobSheet * _jobData;
}
@end

@implementation PSVWRWorkDetailsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(id)data atIndexPath:(NSIndexPath *)indexPath
{
	_jobData = data;
	NSString *faultDescription = [_jobData getFaultDescription];
	if (!isEmpty(faultDescription))
	{
		self.lblVoidWorkDetails.numberOfLines = 0;
		self.lblVoidWorkDetails.text = faultDescription;
		[self.lblVoidWorkDetails sizeToFit];

	}
	self.lblVoidWorkDetails.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
}


@end
