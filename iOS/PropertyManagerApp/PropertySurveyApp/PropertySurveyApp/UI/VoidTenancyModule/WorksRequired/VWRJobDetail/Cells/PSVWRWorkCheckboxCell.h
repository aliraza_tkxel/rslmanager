//
//  PSFaultDetailsCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVWRJobDetailViewController.h"

@interface PSVWRWorkCheckboxCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnLegionellaSelected;
@property (weak,    nonatomic) id<JobDetailProtocol> delegate;
@property (weak,   nonatomic) FaultJobSheet * _jobData;
- (void)configureCell:(id)data atIndexPath:(NSIndexPath *)indexPath withDelegate:(id<JobDetailProtocol>) deleg;
@end
