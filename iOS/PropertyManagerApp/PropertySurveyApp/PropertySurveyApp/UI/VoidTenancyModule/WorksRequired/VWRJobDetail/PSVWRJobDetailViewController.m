//
//  PSJobDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVWRJobDetailViewController.h"

#import "PSVWRWorkDetailsCell.h"
#import "PSVWRWorkNotesCell.h"
#import "PSVWRDurationCell.h"
#import "PSVWRReletDateCell.h"
#import "PSVWRWorkCheckboxCell.h"
#import "PSVWRJobStatusViewController.h"
#import "PSBarButtonItem.h"
#import "FaultJobSheet+Methods.h"

#import "PSRepairImagesViewController.h"
#import "PSRepairImagesGalleryViewController.h"

#define kNumberOfSections 5
#define kSectionVoidWorkDetails 0
#define kSectionVoidLegionellaTest 1
#define kSectionVoidWorkNotes 2
#define kSectionDuration 3
#define kSectionReletDate 4

#define kSectionHeaderHeight 20

#define kSectionVoidWorkDetailsRows 1
#define kSectionLegionellaTestRows 1
#define kSectionVoidWorkNotesRows 1
#define kSectionDurationRows 1
#define kSectionReletDateRows 1

#define kVoidWorkDetailsCellHeight 56
#define kVoidLegionellaTestCellHeight 33
#define kVoidWorkNotesCellHeight 33
#define kDurationCellHeight 34
#define kReletDateCellHeight 32

@interface PSVWRJobDetailViewController()
{
	PSDataUpdateManager * _manager;
}
@end

@implementation PSVWRJobDetailViewController

@synthesize headerViewArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setupBackground];
	self.headerViewArray = [NSMutableArray array];
	[self loadSectionHeaderViews:@[LOC(@"KEY_STRING_VOID_WORK_DETAILS"),LOC(@"KEY_STRING_VOID_WORK_LEGIONELLA_TEST"),LOC(@"KEY_STRING_VOID_WORK_NOTES"),LOC(@"KEY_STRING_DURATION"),LOC(@"KEY_STRING_RELET_DATE")
																 ]   headerViews:self.headerViewArray];
	[self addTableHeaderView];
	[self loadNavigationonBarItems];
	// Do any additional setup after loading the view from its nib.
	[self registerNibsForTableView];
	[self initData];
}

-(void) initData
{
	_manager = [PSDataUpdateManager sharedManager];
}

-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSVWRWorkDetailsCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRWorkNotesCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRDurationCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRReletDateCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWRWorkCheckboxCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if(!isEmpty(self.jobSheet) && ![self.jobSheet.jobStatus isEqualToString:kJobStatusNotStarted] && ![self.jobSheet.jobStatus isEqualToString:kJobStatusAccepted])
	{
		[self hideTableHeaderView];
	}
	
	if (!isEmpty(self.jobSheet) &&  ![self.jobSheet.jobStatus isEqualToString:kJobStatusComplete] && ![self.jobSheet.jobStatus isEqualToString:kJobStatusNoEntry])
	{
		if ([self.jobSheet.jobStatus isEqualToString:kJobStatusPaused])
		{
			[self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
		}
		else if ([self.jobSheet.jobStatus isEqualToString:kJobStatusInProgress])
		{
			[self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
		}
		[self addCameraButtonInRightBarButtons];
	}
	else
	{
		[self.pauseButton setEnabled:NO];
		[self.resumeButton setEnabled:NO];
		[self.completeButton setEnabled:NO];
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	[self setTableView:nil];
}

-(void) setupBackground
{
	self.contentView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	self.tableView.backgroundColor =UIColorFromHex(THEME_BG_COLOUR);
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	self.pauseButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														style:PSBarButtonItemStylePause
																													 target:self
																													 action:@selector(onClickPauseButton)];
	self.resumeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														 style:PSBarButtonItemStylePlay
																														target:self
																														action:@selector(onClickResumeButton)];
	self.completeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																															 style:PSBarButtonItemStyleCheckmark
																															target:self
																															action:@selector(onClickCompleteButton)];
	self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														 style:PSBarButtonItemStyleCamera
																														target:self
																														action:@selector(onClickCameraButton)];
	
	//[self.completeButton setEnabled:NO];
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	if (![self.jobSheet.jobStatus isEqualToString:kJobStatusComplete] || ![self.jobSheet.jobStatus isEqualToString:kJobStatusNoEntry])
	{
		if ([self.jobSheet.jobStatus isEqualToString:kJobStatusPaused])
		{
			[self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
			[self.completeButton setEnabled:FALSE];
		}
		else if ([self.jobSheet.jobStatus isEqualToString:kJobStatusInProgress])
		{
			[self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
			[self.completeButton setEnabled:TRUE];
		}
		if([self isStandardCleaningJobSheet] == FALSE)
		{
			[self addCameraButtonInRightBarButtons];
		}
	}
	
	NSString *title = [NSString stringWithFormat:@"Job %@",self.jobSheet.jsNumber];
	[self setTitle:title];
}

-(BOOL) isStandardCleaningJobSheet
{
	CLS_LOG(@"JSType: %@ : %ld", self.jobSheet.jsType, NSOrderedSame);
	CLS_LOG(@"%ld", (long)[self.jobSheet.jsType compare:kJSTYPE_STANDARD_VOID_WORKS]);
	CLS_LOG(@"%ld", (long)[self.jobSheet.jsType compare:kJSTYPE_SANI_CLEAN]);
	if(self.jobSheet.jsType == nil || isEmpty(self.jobSheet.jsType))
	{
		return FALSE;
	}
	if([self.jobSheet.jsType compare:kJSTYPE_STANDARD_VOID_WORKS] == NSOrderedSame ||
		 [self.jobSheet.jsType compare:kJSTYPE_SANI_CLEAN] == NSOrderedSame)
	{
		return TRUE;
	}
	return FALSE;
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickPauseButton
{
	CLS_LOG(@"onClickPauseButton");
	
	UIStoryboard* storyboard = [UIStoryboard storyboardWithName:VT_SB_WorksRequired bundle:nil];
	PSVWRJobStatusViewController * jobStatusVC = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSVWRJobStatusViewController class])];
	[jobStatusVC setJobData:self.jobSheet];
	[jobStatusVC setViewMode:kPauseJobViewMode];
	[self.navigationController pushViewController:jobStatusVC animated:YES];
}

- (void) onClickResumeButton
{
	CLS_LOG(@"onClickResumeButton");
	[self setRightBarButtonItems:@[self.pauseButton, self.completeButton]];
	[self.completeButton setEnabled:TRUE];
	[_manager updateJobStatus:self.jobSheet jobStaus:kJobStatusInProgress andPauseData:nil];
	[self addCameraButtonInRightBarButtons];
}

- (void) onClickCompleteButton
{
	CLS_LOG(@"onClickCompleteButton");
    
	if([self isStandardCleaningJobSheet] == TRUE)
	{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOC(@"KEY_ALERT_CONFIRM_POPUP_HEADING") message:LOC(@"KEY_CONFIRM_COMPLETE_ACTION") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_NO") style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            NSLog(@"Cancelled Appointment Completion");
            [self onClickBackButton];
            
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSLog(@"Confirmed Appointment Completion");
            [_manager updateJobStatus:self.jobSheet jobStaus:kJobStatusComplete andPauseData:nil];
            [_manager addJobDataForComplete:self.jobSheet];
            BOOL isAppointmentComplete = [[PSAppointmentsManager sharedManager] isFaultAppointmentComplete:self.jobSheet.jobSheetToAppointment];
            if (isAppointmentComplete)
            {
                AppointmentStatus status = [UtilityClass completionStatusForAppointmentType:[self.jobSheet.jobSheetToAppointment getType]];
                [_manager updateAppointmentStatusOnly:status appointment:self.jobSheet.jobSheetToAppointment];
            }
            [self onClickBackButton];
            
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated: YES completion: nil];
        
		
	}
	else
	{
		UIStoryboard* storyboard = [UIStoryboard storyboardWithName:VT_SB_WorksRequired bundle:nil];
		PSVWRJobStatusViewController * jobStatusVC = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSVWRJobStatusViewController class])];
		[jobStatusVC setJobData:self.jobSheet];
		[jobStatusVC setViewMode:kCompleteJobViewMode];
		[self.navigationController pushViewController:jobStatusVC animated:YES];
	}
}

- (void) onClickCameraButton
{
	CLS_LOG(@"onClickCameraButton");
	
	PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
	if(self.jobSheet.jobSheetToAppointment.appointmentToProperty)
	{
		[repairImagesViewConroller setPropertyObj:self.jobSheet.jobSheetToAppointment.appointmentToProperty];
		[repairImagesViewConroller setSchemeObj:nil];
	}
	else
	{
		[repairImagesViewConroller setPropertyObj:nil];
		[repairImagesViewConroller setSchemeObj:self.jobSheet.jobSheetToAppointment.appointmentToScheme];
	}
	[repairImagesViewConroller setJobDataListObj:self.jobSheet];
	[self.navigationController pushViewController:repairImagesViewConroller animated:YES];
}

- (IBAction)onClickStartJobButton:(id)sender
{
	if([self isStandardCleaningJobSheet] == TRUE)
	{
		[_manager updateJobStatus:self.jobSheet jobStaus:kJobStatusInProgress andPauseData:nil];
		[self hideTableHeaderView];
		[self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
	}
	else
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_PHOTOGRAPH_A_FAULT")
																									 message:LOC(@"KEY_ALERT_BEFORE_FAULT_PICTURE")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_ALERT_YES")
																				 otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"), nil];
		alert.tag = AlertViewTagFaultPicture;
		[alert show];
	}
}

#pragma mark - UITableView Delegate Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *_cell = nil;
	if (indexPath.section == kSectionVoidWorkDetails)
	{
		PSVWRWorkDetailsCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRWorkDetailsCell class])];
		[cell configureCell:self.jobSheet atIndexPath:indexPath];
		_cell = cell;
	}
	else if (indexPath.section == kSectionVoidLegionellaTest)
	{
		PSVWRWorkCheckboxCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRWorkCheckboxCell class])];
		[cell configureCell:self.jobSheet atIndexPath:indexPath withDelegate:self];
		_cell = cell;
		
	}
	else if (indexPath.section == kSectionVoidWorkNotes)
	{
		PSVWRWorkNotesCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRWorkNotesCell class])];
		[cell configureCell:self.jobSheet atIndexPath:indexPath];
		_cell = cell;
		
	}
	else if (indexPath.section == kSectionDuration)
	{
		PSVWRDurationCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRDurationCell class])];
		[cell configureCell:self.jobSheet atIndexPath:indexPath];
		_cell = cell;
		
	}
	else if (indexPath.section == kSectionReletDate)
	{
		PSVWRReletDateCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWRReletDateCell class])];
		[cell configureCell:self.appointment atIndexPath:indexPath];
		_cell = cell;
	}
	[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	return _cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0.0;
	if(indexPath.section == kSectionVoidWorkDetails)
	{
		rowHeight = [self heightForLabelWithText:[self.jobSheet getFaultDescription]];
		if (rowHeight < kVoidWorkDetailsCellHeight)
		{
			rowHeight = kVoidWorkDetailsCellHeight;
		}
	}
	else if (indexPath.section == kSectionVoidWorkNotes)
	{
		rowHeight = [self heightForLabelWithText:self.jobSheet.jsnNotes];
		if (rowHeight < kVoidWorkNotesCellHeight)
		{
			rowHeight = kVoidWorkNotesCellHeight;
		}
	}
	else if (indexPath.section == kSectionDuration)
	{
		rowHeight = kDurationCellHeight;
	}
	else if (indexPath.section == kSectionReletDate)
	{
		rowHeight = kReletDateCellHeight;
	}
	else if (indexPath.section == kSectionVoidLegionellaTest)
	{
		rowHeight = kVoidLegionellaTestCellHeight;
	}
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows=0;
	if(section == kSectionVoidWorkDetails)
	{
		numberOfRows = kSectionVoidWorkDetailsRows;
	}
	else if (section == kSectionVoidWorkNotes)
	{
		numberOfRows = kSectionVoidWorkNotesRows;
	}
	else if (section == kSectionDuration)
	{
		numberOfRows = kSectionDurationRows;
	}
	else if (section == kSectionVoidLegionellaTest)
	{
		numberOfRows = kSectionLegionellaTestRows;
	}
	return numberOfRows;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return kSectionHeaderHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return kNumberOfSections;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	/*PSJobStatusViewController *jobStatusViewConroller = [[PSJobStatusViewController alloc] initWithNibName:@"PSJobStatusViewController" bundle:[NSBundle mainBundle]];
	 [jobStatusViewConroller setJobData:self.jobSheet];
	 
	 if (indexPath.section == kSectionPriority) {
	 [jobStatusViewConroller setViewMode:0];
	 }
	 
	 else if (indexPath.section == kSectionDuration) {
	 [jobStatusViewConroller setViewMode:1];
	 }
	 [self.navigationController pushViewController:jobStatusViewConroller animated:YES];*/
}

- (void) onJobStartNotificationReceive
{
	[self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
}

#pragma mark - Methods
- (void) addTableHeaderView
{
	CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
	UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
	headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	
	UILabel *lbl = [[UILabel alloc] init];
	lbl.frame = CGRectMake(8, 0, 245, 29);
	lbl.center = CGPointMake(lbl.center.x, headerView.center.y);
	lbl.backgroundColor = [UIColor clearColor];
	[lbl setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[lbl setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[lbl setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
	lbl.text =LOC(@"KEY_STRING_JOB_START_MESSAGE");
	
	UIButton *btnStartAppointment = [[UIButton alloc]init];
	btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
	btnStartAppointment.center = CGPointMake(btnStartAppointment.center.x, headerView.center.y);
	[btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
	[btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
	[btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:
	 UIControlStateNormal];
	[btnStartAppointment addTarget:self action:@selector(onClickStartJobButton:) forControlEvents:UIControlEventTouchUpInside];
	
	[headerView addSubview:lbl];
	[headerView addSubview:btnStartAppointment];
	
	if ([self.jobSheet.jobStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame
			|| [self.jobSheet.jobStatus compare:kAppointmentStatusAccepted] == NSOrderedSame
			)
	{
		headerViewRect = self.tableView.tableHeaderView.frame;
		headerViewRect.size.height = 40;
		headerViewRect.size.width = 320;
		[self.tableView.tableHeaderView setFrame:headerViewRect];
		[self.tableView setTableHeaderView:headerView];
	}
}

- (void) hideTableHeaderView
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.5];
	[self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
	self.tableView.tableHeaderView = nil;
	[self.tableView.tableHeaderView setHidden:YES];
	[UIView commitAnimations];
	[self loadNavigationonBarItems];
	[self.tableView reloadData];
	
}

-(void)addCameraButtonInRightBarButtons
{
	NSMutableArray *rightBarButtons = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
	NSInteger jobPicturesCount  = [self.jobSheet.jobSheetToRepairImages count];
	if (jobPicturesCount > 0)
	{
		BOOL found = FALSE;
		for (PSBarButtonItem* button in rightBarButtons)
		{
			if([button tag] == PSBarButtonItemStyleCamera)
			{
				found = TRUE;
				break;
			}
		}
		
		if(!found)
		{
			[rightBarButtons insertObject:self.cameraButton atIndex:[rightBarButtons count]];
			[self setRightBarButtonItems:rightBarButtons];
		}
	}
	
}

#pragma mark - Update Legionella
- (void) updateLigionella:(BOOL) isChecked
{
	self.jobSheet.isLegionella = [NSNumber numberWithBool:isChecked];
	[_manager updateLegionella:self.jobSheet];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(300, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(300, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}

#pragma mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
 if(alertView.tag == AlertViewTagFaultPicture)
 {
	 CLS_LOG(@"Clicked at Index %ld",buttonIndex);
	 if(buttonIndex == 0) //YES
	 {
		 PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
		 if(self.jobSheet.jobSheetToAppointment.appointmentToProperty)
		 {
			 [repairImagesViewConroller setPropertyObj:self.jobSheet.jobSheetToAppointment.appointmentToProperty];
			 [repairImagesViewConroller setSchemeObj:nil];
		 }
		 else
		 {
			 [repairImagesViewConroller setPropertyObj:nil];
			 [repairImagesViewConroller setSchemeObj:self.jobSheet.jobSheetToAppointment.appointmentToScheme];
		 }
		 [repairImagesViewConroller setJobDataListObj:self.jobSheet];
		 [self.navigationController pushViewController:repairImagesViewConroller animated:YES];
	 }
 }
}

@end
