//
//  PSDurationCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVWRDurationCell.h"

@interface PSVWRDurationCell ()
{
	FaultJobSheet * _jobData;
}
@end

@implementation PSVWRDurationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

- (void)configureCell:(id)data atIndexPath:(NSIndexPath *)indexPath
{
	_jobData = data;
	self.lblDuration.text = [NSString stringWithFormat:@"%@ Hours", [_jobData.duration stringValue]];
	self.lblDuration.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
}

@end
