//
//  PSJobDetailViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaultJobSheet+CoreDataProperties.h"

#define kPauseJobViewMode 0
#define kCompleteJobViewMode 1


@protocol JobDetailProtocol <NSObject>
@required
- (void) updateLigionella:(BOOL) isChecked;
@end

@interface PSVWRJobDetailViewController : PSCustomViewController <JobDetailProtocol,UIAlertViewDelegate>

@property (strong, nonatomic) Appointment * appointment;
@property (strong, nonatomic) FaultJobSheet * jobSheet;
@property (strong, nonatomic) NSMutableArray * headerViewArray;
@property (strong, nonatomic) NSArray * jobAsbestosArray;
@property (strong, nonatomic) PSBarButtonItem * completeButton;
@property (strong, nonatomic) PSBarButtonItem * pauseButton;
@property (strong, nonatomic) PSBarButtonItem * resumeButton;
@property (strong, nonatomic) PSBarButtonItem * cameraButton;
@property (strong, nonatomic) IBOutlet UIView * contentView;

- (void) loadNavigationonBarItems;
- (void) addTableHeaderView;

@end
