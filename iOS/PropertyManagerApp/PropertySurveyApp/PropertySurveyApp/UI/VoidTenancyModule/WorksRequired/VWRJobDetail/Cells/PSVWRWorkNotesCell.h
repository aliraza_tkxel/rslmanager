//
//  PSFaultNotesCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSVWRWorkNotesCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblVoidWorkNotes;

- (void)configureCell:(id)data atIndexPath:(NSIndexPath *)indexPath;

@end
