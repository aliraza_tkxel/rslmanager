//
//  PSResoponseTimeCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVWRReletDateCell.h"
#import "VoidWorks.h"

@interface PSVWRReletDateCell ()
{
	Appointment * _appointment;
}
@end

@implementation PSVWRReletDateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

- (void)configureCell:(id)data atIndexPath:(NSIndexPath *)indexPath
{
	_appointment = data;
	self.lblReletDate.text = [UtilityClass stringFromDate:_appointment.appointmentToVoidWorks.reletDate dateFormat:kDateTimeStyle8];
	self.lblReletDate.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
}

@end
