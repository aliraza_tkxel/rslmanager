//
//  PSVPrePaintPackCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-30.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostInspectionManager.h"
#import "RoomData.h"

@interface PSVPostPaintPackCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblRoomName;
@property (strong, nonatomic) IBOutlet UIButton *btnRoomSelected;

@property (weak,   nonatomic) RoomData * roomData;
@property (weak,   nonatomic) Appointment * appointment;

-(void)setRoomData:(RoomData *)roomData
		AndAppointment:(Appointment *)appointment
				AndManager:(PSVPostInspectionManager *)manager;

-(void)enableCell:(BOOL)enable;

@end
