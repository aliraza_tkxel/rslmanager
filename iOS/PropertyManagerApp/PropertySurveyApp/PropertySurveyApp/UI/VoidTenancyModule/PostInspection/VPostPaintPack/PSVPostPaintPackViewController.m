//
//  PSPaintPackViewController.m
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPostPaintPackViewController.h"
#import "PSVPostInspectionManager.h"
#import "PSVPostPaintPackCell.h"
#import "PSBarButtonItem.h"
#import "VoidData.h"

@interface PSVPostPaintPackViewController ()
{
	PSVPostInspectionManager * _manager;
	NSArray * _dbRoomList;
}
@end

@implementation PSVPostPaintPackViewController

#pragma mark - View Functions
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setupUI];
	[self loadNavigationonBarItems];
	// Do any additional setup after loading the view.
	[self initData];
	[self reloadData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Functions
- (void)setupUI
{
	[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
}

-(void)initData
{
	_manager = [[PSVPostInspectionManager alloc]init:self.appointment];
	_dbRoomList = [_manager getRoomsListFromDB];
}

-(void)reloadData
{
	self.swPaintPack.on = [self.appointment.appointmentToVoidData.isPaintPackRequired boolValue];
	[self.tableView reloadData];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setTitle:LOC(@"KEY_TITLE_VOID_PAINT_PACKS_REQUIRED")];
}

#pragma mark - Switch Events
- (IBAction)onClickPaintPack:(id)sender
{
	self.appointment.appointmentToVoidData.isPaintPackRequired = [NSNumber numberWithBool:self.swPaintPack.on];
	[self.tableView reloadData];
}

#pragma mark - Navigation Bar
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[_manager saveContext];
	[self popOrCloseViewController];
}

#pragma mark - TableView Functions
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger count = _dbRoomList.count;
	CLS_LOG(@"tableView: numberOfRowsInSection: count: %ld", count);
	return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PSVPostPaintPackCell * cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPostPaintPackCell class])];
	if (cell == nil)
	{
		cell = [[PSVPostPaintPackCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([PSVPostPaintPackCell class])];
	}
	RoomData * roomData = [_dbRoomList objectAtIndex:indexPath.row];
	CLS_LOG(@"tableView: cellForRowAtIndexPath: row: %ld component: %@", indexPath.row, roomData.roomName);
	[cell setRoomData:roomData AndAppointment:self.appointment AndManager:_manager];
	[cell enableCell:self.swPaintPack.on];
	return cell;
}

@end
