//
//  PSPaintPackViewController.h
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

#define PAINT_PACK_SEGUE_IDENTIFIER @"PaintPackScreen"

@interface PSVPostPaintPackViewController : PSCustomViewController

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UISwitch *swPaintPack;

@property (weak,   nonatomic) Appointment *appointment;

@end
