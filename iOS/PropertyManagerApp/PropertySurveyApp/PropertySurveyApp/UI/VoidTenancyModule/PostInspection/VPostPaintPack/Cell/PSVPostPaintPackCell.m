//
//  PSVPrePaintPackCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-30.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPostPaintPackCell.h"
#import "PaintPack.h"
#import "VoidData.h"

@interface PSVPostPaintPackCell ()
{
	PaintPack * _paintPack;
	PSVPostInspectionManager * _manager;
}
@end

@implementation PSVPostPaintPackCell

#pragma mark - Cell Init Setting
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{ }
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:FALSE animated:YES];
	// Configure the view for the selected state
}

#pragma mark - Functions
-(void)setRoomData:(RoomData *)roomData
		AndAppointment:(Appointment *)appointment
				AndManager:(PSVPostInspectionManager *)manager
{
	_roomData = roomData;
	_appointment = appointment;
	_manager = manager;
	self.lblRoomName.text = roomData.roomName;
	self.btnRoomSelected.selected = [self getSelectionStatus];
}

-(BOOL)getSelectionStatus
{
	NSPredicate *filter = [NSPredicate predicateWithFormat:@"SELF.roomId == %@", _roomData.roomId];
	NSArray * result = [self.appointment.appointmentToVoidData.voidDataToPaintPack.allObjects filteredArrayUsingPredicate:filter];
	if(result.count > 0)
	{
		_paintPack = [result objectAtIndex:0];
		return YES;
	}
	return NO;
}

-(void)enableCell:(BOOL)enable
{
	self.userInteractionEnabled = enable;
	self.btnRoomSelected.enabled = enable;
}

#pragma mark - Button Events
- (IBAction)onClickRoomSelected:(id)sender
{
	BOOL selected = NO;
	UIButton * button = sender;
	if(button.selected == YES)
	{
		[self.appointment.appointmentToVoidData removeVoidDataToPaintPackObject:_paintPack];
		selected = NO;
	}
	else
	{
		if(_paintPack == NULL)
		{
			_paintPack = [_manager addPaintPack:self.roomData];
		}
		else
		{
			[self.appointment.appointmentToVoidData addVoidDataToPaintPackObject:_paintPack];
		}
		selected = YES;
	}
	CLS_LOG(@"selected : %d", (int)selected);
	[button setSelected:selected];
}

@end
