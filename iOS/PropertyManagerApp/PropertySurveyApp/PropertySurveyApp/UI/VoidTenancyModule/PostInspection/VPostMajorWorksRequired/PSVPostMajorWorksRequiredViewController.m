//
//  PSMajorWorksRequiredViewController.m
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPostMajorWorksRequiredViewController.h"
#import "PSVPostRecordMajorWorkViewController.h"
#import "PSVPostMajorWorkRecordCell.h"
#import "PSBarButtonItem.h"
#import "VoidData.h"

@interface PSVPostMajorWorksRequiredViewController ()

@property (nonatomic) CGRect tableViewFrame;

@end

@implementation PSVPostMajorWorksRequiredViewController

#pragma mark - View Functions
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setupUI];
	[self loadNavigationonBarItems];
	[self reloadData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Functions
- (void)setupUI
{
	[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	self.tableViewFrame = CGRectMake(self.tableView.frame.origin.x,
																	 self.tableView.frame.origin.y,
																	 self.tableView.frame.size.width,
																	 self.tableView.frame.size.height);
}

-(void) hideMajorWorksTable:(BOOL)hide
{
	CGRect newTableViewFrame;
	if(hide == FALSE)
	{
		newTableViewFrame = CGRectMake(self.tableViewFrame.origin.x,
																	 self.tableViewFrame.origin.y,
																	 self.tableViewFrame.size.width,
																	 self.tableViewFrame.size.height);
	}
	else
	{
		newTableViewFrame = CGRectMake(self.tableViewFrame.origin.x,
																	 self.tableViewFrame.origin.y,
																	 self.tableViewFrame.size.width,
																	 0);
	}
	self.tableView.frame = newTableViewFrame;
}

-(void)reloadData
{
	self.swMajorWorksRequired.on = [self.appointment.appointmentToVoidData.isMajorWorkRequired boolValue];
	self.btnRecordMajorWorks.enabled = self.swMajorWorksRequired.on;
	NSInteger count = self.appointment.appointmentToVoidData.voidDataToMajorWorkRequired.count;
	if(self.btnRecordMajorWorks.enabled == TRUE && count > 0)
	{
		[self hideMajorWorksTable:FALSE];
	}
	else
	{
		[self hideMajorWorksTable:TRUE];
	}
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setTitle:LOC(@"KEY_TITLE_VOID_MAJOR_WORKS_REQUIRED")];
}

#pragma mark - Navigation Bar
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[[PSDatabaseContext sharedContext] saveContext];
	[self popOrCloseViewController];
}

#pragma mark - Navigation Segue
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	CLS_LOG(@"prepareForSegue: %@", segue.identifier);
	if([segue.identifier isEqualToString:RECORD_MAJOR_WORKS_SEGUE_IDENTIFIER])
	{
		PSVPostRecordMajorWorkViewController * controller = (PSVPostRecordMajorWorkViewController *)segue.destinationViewController;
		[controller setAppointment:self.appointment];
		controller.delegate = self;
	}
}

#pragma mark - Switch Events
- (IBAction)onChangeMajorWorks:(id)sender
{
	self.appointment.appointmentToVoidData.isMajorWorkRequired = [NSNumber numberWithBool:self.swMajorWorksRequired.on];
	self.btnRecordMajorWorks.enabled = self.swMajorWorksRequired.on;
}

#pragma mark - Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	VoidData * voidData = self.appointment.appointmentToVoidData;
	return [voidData.voidDataToMajorWorkRequired count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	PSVPostMajorWorkRecordCell * cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPostMajorWorkRecordCell class])];
	MajorWorkRequired * majorWorkRequired = [self.appointment.appointmentToVoidData.voidDataToMajorWorkRequired.allObjects objectAtIndex:indexPath.row];
	[cell setMajorWorkRequired:majorWorkRequired];
	return cell;
}

#pragma mark - Protocols
-(void)addedMajorWorkRequired
{
	[self hideMajorWorksTable:FALSE];
	[self.tableView reloadData];
}

@end
