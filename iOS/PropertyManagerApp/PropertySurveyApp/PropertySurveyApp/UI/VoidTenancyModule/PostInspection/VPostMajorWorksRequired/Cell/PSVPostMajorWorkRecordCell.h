//
//  PSVPreMajorWorkRecordCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-28.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MajorWorkRequired.h"
#import "PSVPostMajorWorkRequiredProtocol.h"

@interface PSVPostMajorWorkRecordCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel * lblComponent;
@property (strong, nonatomic) IBOutlet UILabel * lblCondition;
@property (strong, nonatomic) IBOutlet UILabel * lblDueDate;

@property (weak, nonatomic) id<PSVPostMajorWorkRequiredProtocol> delegate;
@property (weak, nonatomic) MajorWorkRequired * majorWorkRequired;

-(void)setMajorWorkRequired:(MajorWorkRequired *)majorWorkRequired;

@end
