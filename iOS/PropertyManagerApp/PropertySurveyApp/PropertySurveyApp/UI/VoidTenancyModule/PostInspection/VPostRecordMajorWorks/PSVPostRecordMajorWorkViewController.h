//
//  PSRecordMajorWorkViewController.h
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostMajorWorkRequiredProtocol.h"
#import "SZTextView.h"

#define RECORD_MAJOR_WORKS_SEGUE_IDENTIFIER @"RecordMajorWorksScreen"

@interface PSVPostRecordMajorWorkViewController : PSCustomViewController

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UILabel *lblComponentTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblComponent;
@property (strong, nonatomic) IBOutlet UILabel *lblReplacementDueTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblReplacementDue;
@property (strong, nonatomic) IBOutlet UILabel *lblConditionTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblCondition;
@property (strong, nonatomic) IBOutlet UIButton *btnReplacementDue;
@property (strong, nonatomic) IBOutlet UIButton *btnCondition;
@property (strong, nonatomic) IBOutlet SZTextView *tvNotes;

@property (weak, nonatomic) id<PSVPostMajorWorkRequiredProtocol> delegate;

@property (strong, nonatomic) PSBarButtonItem *addMajorWorksButton;
@property (weak,   nonatomic) Appointment *appointment;

@end
