//
//  PSVNewPostWorksRequiredViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostWorksRequiredProtocol.h"
#import "PSVPostWorkRecordCell.h"
#import "PSVPostInspectionManager.h"
#import "PSBarButtonItem.h"
#import "VoidData.h"
#import "RoomData.h"
#import "WorkRequired+CoreDataClass.h"
#import "FaultDetailList.h"
#import "FaultAreaList.h"
#import "PSRSearchWorkViewController.h"
#import "PSNewPostVoidRequiredWorksTableViewCell.h"
#import "PSPostVoidDetailSwitchTableViewCell.h"
#import "PSPostVoidGeneralTableViewCell.h"
#import "PSPVRequiredWorksTableViewCell.h"

#define WORKS_REQUIRED_SEGUE_IDENTIFIER @"WorksRequiredScreen"

@interface PSVNewPostWorksRequiredViewController : PSCustomViewController<PSVPostWorksRequiredProtocol,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) Appointment *appointment;
@property  PSVPostInspectionManager * manager;
@property NSMutableArray *cellTitles;
@property NSMutableArray *worksRequired;
@property BOOL isTenantWork;
@end
