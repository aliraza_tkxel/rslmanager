//
//  PSPostVoidDetailSwitchTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostWorksRequiredProtocol.h"
@interface PSPostVoidDetailSwitchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblMainTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailText1;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailText2;
@property (weak, nonatomic) IBOutlet UISwitch *detailSwitch;
@property (weak, nonatomic) Appointment *appointment;
-(void) configureCell;
@property (weak, nonatomic) id<PSVPostWorksRequiredProtocol> delegate;
@end
