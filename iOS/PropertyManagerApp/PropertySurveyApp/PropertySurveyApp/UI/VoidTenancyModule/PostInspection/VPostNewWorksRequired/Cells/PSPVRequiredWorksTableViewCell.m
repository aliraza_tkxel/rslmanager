//
//  PSPVRequiredWorksTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSPVRequiredWorksTableViewCell.h"
#import "RoomData.h"
#import "FaultAreaList.h"
#import "FaultDetailList.h"
#import "VoidData.h"
@implementation PSPVRequiredWorksTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Functions
-(void)setWorkRequired:(WorkRequired *)workRequired
{
    _workRequired = workRequired;
    self.lblComponent.text = self.workRequired.workRequiredToFaultArea.faultAreaName;
    self.lblDescription.text = workRequired.repairDescription;
    [self.btnTenantWork setSelected:[self.workRequired.isVerified boolValue]];
    self.lblStatusValue.text = _workRequired.status;
    self.btnCheckMark.hidden = [_workRequired.isBRSWork boolValue];
}

- (IBAction)btnTenantWorkTapped:(id)sender {
    UIButton * button = sender;
    if(button.selected == YES)
    {
        [button setSelected:NO];
    }
    else
    {
        [button setSelected:YES];
    }
    self.workRequired.isVerified = [NSNumber numberWithBool:button.selected];
    if (self.delegate && [self.delegate respondsToSelector:@selector(recalTenantEstimate:)])
    {
        [self.delegate recalTenantEstimate:self.workRequired];
        
    }
}
- (IBAction)btnCheckMarkTapped:(id)sender {
    CLS_LOG(@"onClickShiftToBRS");
    self.workRequired.isBRSWork = [NSNumber numberWithBool:TRUE];
    self.workRequired.isTenantWork = [NSNumber numberWithBool:FALSE];
    if (self.delegate && [self.delegate respondsToSelector:@selector(refreshTableView)])
    {
        [self.delegate refreshTableView];
    }
}

@end
