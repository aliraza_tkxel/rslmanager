//
//  PSPostVoidGeneralTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSPostVoidGeneralTableViewCell.h"
#import "PSVPostInspectionManager.h"
#import "VoidData.h"
@implementation PSPostVoidGeneralTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)switchTapped:(id)sender {
    switch (self.tag) {
        case 0:
            self.appointment.appointmentToVoidData.isWorksRequired = [NSNumber numberWithBool:self.generalSwitch.on];
            if(self.generalSwitch.on){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"worksRequiredStatusTurnedOn" object:nil];
            }
            else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"worksRequiredStatusTurnedOff" object:nil];
            }
            break;
        case 5:
            self.appointment.appointmentToVoidData.isEPCCheckRequired = [NSNumber numberWithBool:self.generalSwitch.on];
            break;
        case 6:
            self.appointment.appointmentToVoidData.isAsbestosCheckRequired = [NSNumber numberWithBool:self.generalSwitch.on];
            break;
            
        default:
            break;
    }


}

-(void) configureCell{
    self.generalSwitch.hidden = NO;
    self.imgViewArrow.hidden = YES;
    [self removeNotifications];
    [self registerNotifications];
    if(self.tag==0){
        [self.generalSwitch setOn:[self.appointment.appointmentToVoidData.isWorksRequired boolValue]];
    }
    else if(self.tag==1){
        self.generalSwitch.hidden = YES;
        self.imgViewArrow.hidden = NO;
    }
    else if(self.tag==5){
        self.generalSwitch.enabled = [self.appointment.appointmentToVoidData.isWorksRequired boolValue];
        [self.generalSwitch setOn:[self.appointment.appointmentToVoidData.isEPCCheckRequired boolValue]];
    }
    else if(self.tag==6){
        self.generalSwitch.enabled = [self.appointment.appointmentToVoidData.isWorksRequired boolValue];
        [self.generalSwitch setOn:[self.appointment.appointmentToVoidData.isAsbestosCheckRequired boolValue]];
    }

}

-(void) registerNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(worksRequiredStatusOn) name:@"worksRequiredStatusTurnedOn" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(worksRequiredStatusOff) name:@"worksRequiredStatusTurnedOff" object:nil];
}

-(void) removeNotifications{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) worksRequiredStatusOn{
    if(self.tag==5 || self.tag ==6){
        self.generalSwitch.enabled = YES;
    }
    
}
-(void) worksRequiredStatusOff{
    if(self.tag==5 || self.tag ==6){
        self.generalSwitch.enabled = NO;
    }
}

@end
