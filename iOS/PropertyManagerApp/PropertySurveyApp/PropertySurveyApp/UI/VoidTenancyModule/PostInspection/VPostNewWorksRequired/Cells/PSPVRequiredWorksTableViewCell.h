//
//  PSPVRequiredWorksTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostWorksRequiredProtocol.h"
#import "WorkRequired+CoreDataClass.h"

@interface PSPVRequiredWorksTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblComponent;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusValue;
@property (weak, nonatomic) IBOutlet UIButton *btnTenantWork;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckMark;

@property (weak, nonatomic) id<PSVPostWorksRequiredProtocol> delegate;
@property (weak, nonatomic) WorkRequired * workRequired;



-(void)setWorkRequired:(WorkRequired *)workRequired;

@end
