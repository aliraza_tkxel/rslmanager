//
//  PSVNewPostWorksRequiredViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSVNewPostWorksRequiredViewController.h"

#define kGeneralCellHeight 50.0
#define kCellTableViewCellHeight 80
#define kWorksRequiredCellHeight 250.0
#define kMainTableViewTag 2681994
#define kCellTableViewTag 2681995
@interface PSVNewPostWorksRequiredViewController ()

@end

@implementation PSVNewPostWorksRequiredViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [_tblView registerNib:[UINib nibWithNibName:@"PSPostVoidGeneralTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSPostVoidGeneralTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSPostVoidDetailSwitchTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSPostVoidDetailSwitchTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSNewPostVoidRequiredWorksTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSNewPostVoidRequiredWorksTableViewCell"];
    if ( [(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] ) {
        _tblView.contentInset = UIEdgeInsetsMake(0, 0, 150, 0); /* Device is iPad */
    }
    else{
        _tblView.contentInset = UIEdgeInsetsMake(0, 0, 84, 0);
    }
    _isTenantWork = NO;
    [self initData];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - init
-(void) initData
{
    _manager = [[PSVPostInspectionManager alloc]init:self.appointment];
    _cellTitles  = [[NSMutableArray alloc] initWithObjects:@"Works Required?",@"Record Works",@"",@"Gas Check Required?",@"Electric Check Required?",@"EPC Check Required?",@"Asbestos Check Required?", nil];
   // _worksRequired = [[NSMutableArray alloc] initWithArray:self.appointment.appointmentToVoidData.voidDataToWorkRequired.allObjects];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.tag = kMainTableViewTag;
    [self didSelectTenantWork:_isTenantWork forTableView:_tblView];
}

#pragma mark - TableViewDelegates

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == kMainTableViewTag){
        return [_cellTitles count];
    }
    else if(tableView.tag == kCellTableViewTag){
        return [_worksRequired count];
    }
    return 0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == kMainTableViewTag){
        if(indexPath.row==2){ //Required Works cell
            if([self.appointment.appointmentToVoidData.voidDataToWorkRequired.allObjects count]>0){
                return kWorksRequiredCellHeight;
            }
            return 0;
        }
        return kGeneralCellHeight;
    }
    if(tableView.tag ==kCellTableViewTag){
        if([_worksRequired count]>0){
            return kCellTableViewCellHeight;
        }
        else{
            return 0;
        }
    }
    return 0;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if(tableView.tag == kMainTableViewTag){
        if(indexPath.row==0 || indexPath.row==1 || indexPath.row ==5 || indexPath.row==6){
            PSPostVoidGeneralTableViewCell *generalCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSPostVoidGeneralTableViewCell class])];
            generalCell.lblMainText.text = [_cellTitles objectAtIndex:indexPath.row];
            generalCell.appointment = self.appointment;
            generalCell.tag = indexPath.row;
            generalCell.delegate = self;
            [generalCell configureCell];
            
            cell = generalCell;
        }
        else if(indexPath.row==3 || indexPath.row ==4){
            PSPostVoidDetailSwitchTableViewCell *detailCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSPostVoidDetailSwitchTableViewCell class])];
            detailCell.lblMainTitle.text = [_cellTitles objectAtIndex:indexPath.row];
            detailCell.appointment = self.appointment;
            detailCell.tag = indexPath.row;
            detailCell.delegate = self;
            [detailCell configureCell];
            cell = detailCell;
        }
        else if(indexPath.row==2){
            PSNewPostVoidRequiredWorksTableViewCell *voidWorksCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSNewPostVoidRequiredWorksTableViewCell class])];
            [voidWorksCell.cellTblView registerNib:[UINib nibWithNibName:@"PSPVRequiredWorksTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSPVRequiredWorksTableViewCell"];
            voidWorksCell.delegate = self;
            voidWorksCell.cellTblView.delegate = self;
            voidWorksCell.cellTblView.dataSource = self;
            voidWorksCell.cellTblView.tag = kCellTableViewTag;
            [voidWorksCell configureCellForType:_isTenantWork];
            if([_worksRequired count]==0){
                voidWorksCell.hidden = YES;
            }
            else{
                voidWorksCell.hidden = NO;
            }
            cell = voidWorksCell;
        
        }
    }
    else if(tableView.tag == kCellTableViewTag){
        PSPVRequiredWorksTableViewCell *reqWorksCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSPVRequiredWorksTableViewCell class])];
        reqWorksCell.delegate = self;
        [reqWorksCell setWorkRequired:[_worksRequired objectAtIndex:indexPath.row]];
        cell = reqWorksCell;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.appointment.appointmentToVoidData.isWorksRequired boolValue] && tableView.tag==kMainTableViewTag && indexPath.row==1){
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_AddRequiredWork bundle:nil];
        PSRSearchWorkViewController * jobScreen = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSRSearchWorkViewController class])];
        [jobScreen setAppointment:self.appointment];
        [self.navigationController pushViewController:jobScreen animated:YES];
    }
}



#pragma mark - PostWorksDelegates
-(void) addedWorkRequired{
    [_manager saveContext];
    [self initData];
}

-(void) refreshTableView{
    [self addedWorkRequired];
}

-(void) recalTenantEstimate:(WorkRequired *)workRequired{
    [self addedWorkRequired];
}

-(void) didSelectTenantWork:(BOOL)tenantWork forTableView:(UITableView *)tableView{
    _isTenantWork = tenantWork;
    NSPredicate * filter = [NSPredicate predicateWithFormat:@"SELF.isTenantWork == %d", tenantWork];
    _worksRequired = [[NSMutableArray alloc] initWithArray:[self.appointment.appointmentToVoidData.voidDataToWorkRequired.allObjects filteredArrayUsingPredicate:filter]];
    [tableView reloadData];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:LOC(@"KEY_TITLE_VOID_WORKS_REQUIRED")];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [_manager saveContext];
    [self popOrCloseViewController];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
