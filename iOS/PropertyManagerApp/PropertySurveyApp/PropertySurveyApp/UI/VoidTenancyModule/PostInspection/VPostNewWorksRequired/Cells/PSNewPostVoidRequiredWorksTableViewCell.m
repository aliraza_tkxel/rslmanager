//
//  PSNewPostVoidRequiredWorksTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSNewPostVoidRequiredWorksTableViewCell.h"
#import "VoidData.h"
#import "WorkRequired+CoreDataClass.h"
#import "PSVPostInspectionManager.h"
#import "VoidData.h"

@implementation PSNewPostVoidRequiredWorksTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnBRSWorksTapped:(id)sender {
    self.btnBRSWorks.layer.backgroundColor = [UIColor clearColor].CGColor;
    self.btnTenantWorks.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
    [_delegate didSelectTenantWork:NO forTableView:_cellTblView];
    
    
}
- (IBAction)btnTenantWorksTapped:(id)sender {
    self.btnBRSWorks.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
    self.btnTenantWorks.layer.backgroundColor = [UIColor clearColor].CGColor;
    [_delegate didSelectTenantWork:YES forTableView:_cellTblView];
    
}

#pragma mark - Tenants methods

-(void) configureCellForType:(BOOL) isTenantWorkSelected{
    self.btnBRSWorks.layer.borderWidth = 0.5f;
    self.btnBRSWorks.layer.borderColor = [UIColor blackColor].CGColor;
    self.btnBRSWorks.layer.backgroundColor = [UIColor clearColor].CGColor;
    self.btnTenantWorks.layer.borderWidth = 0.5f;
    self.btnTenantWorks.layer.borderColor = [UIColor blackColor].CGColor;
    self.btnTenantWorks.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
    [self.cellTblView reloadData];
    [self calculateTenantEstimate];
    if(isTenantWorkSelected){
        self.btnBRSWorks.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
        self.btnTenantWorks.layer.backgroundColor = [UIColor clearColor].CGColor;
    }
    else{
        self.btnBRSWorks.layer.backgroundColor = [UIColor clearColor].CGColor;
        self.btnTenantWorks.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
    }
}

-(void)calculateTenantEstimate
{
    float tenantEstimate = 0.0;
    for (WorkRequired * workRequired in self.appointment.appointmentToVoidData.voidDataToWorkRequired)
    {
        if([workRequired.isTenantWork boolValue] == TRUE &&
           [workRequired.isVerified boolValue] == TRUE)
        {
            tenantEstimate += [workRequired.gross floatValue];
        }
    }
    [self setTenantEstimate:tenantEstimate];
}

-(void)setTenantEstimate:(float)tenantEstimate
{
    self.lblTenantNeglect.text = [NSString stringWithFormat:@"%.2f", tenantEstimate];
    self.appointment.appointmentToVoidData.tenantEstimate = [NSNumber numberWithFloat:tenantEstimate];
}

@end
