//
//  PSNewPostVoidRequiredWorksTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostWorksRequiredProtocol.h"
@interface PSNewPostVoidRequiredWorksTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnBRSWorks;
@property (weak, nonatomic) IBOutlet UIButton *btnTenantWorks;
@property (weak, nonatomic) IBOutlet UITableView *cellTblView;
@property (weak, nonatomic) IBOutlet UILabel *lblTenantNeglect;
@property (weak, nonatomic) Appointment *appointment;
-(void) configureCellForType:(BOOL) isTenantWorkSelected;
@property (weak, nonatomic) id<PSVPostWorksRequiredProtocol> delegate;
@end
