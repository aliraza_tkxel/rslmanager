//
//  PSPostVoidDetailSwitchTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSPostVoidDetailSwitchTableViewCell.h"
#import "PSVPostInspectionManager.h"
#import "VoidData.h"
@implementation PSPostVoidDetailSwitchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)detailSwitchTapped:(id)sender {
    switch (self.tag) {
        case 3:
            self.appointment.appointmentToVoidData.isGasCheckRequired = [NSNumber numberWithBool:self.detailSwitch.on];
            break;
        case 4:
            self.appointment.appointmentToVoidData.isElectricCheckRequired = [NSNumber numberWithBool:self.detailSwitch.on];
            break;
            
        default:
            break;
    }
}

-(void) configureCell{
    if(self.tag==3){
        [self.detailSwitch setOn:[self.appointment.appointmentToVoidData.isGasCheckRequired boolValue]];
        self.lblDetailText1.text = self.appointment.appointmentToVoidData.gasCheckStatus;
        self.lblDetailText2.text = [UtilityClass stringFromDate:self.appointment.appointmentToVoidData.gasCheckDate dateFormat:kDateTimeStyle8];
        if([self.appointment.appointmentToVoidData.isGasCheckRequired boolValue]==YES){
            self.detailSwitch.enabled = NO;
        }
    }
    else if(self.tag==4){
        [self.detailSwitch setOn:[self.appointment.appointmentToVoidData.isElectricCheckRequired boolValue]];
        self.lblDetailText1.text = self.appointment.appointmentToVoidData.electricCheckStatus;
        self.lblDetailText2.text = [UtilityClass stringFromDate:self.appointment.appointmentToVoidData.electricCheckDate dateFormat:kDateTimeStyle8];
        if([self.appointment.appointmentToVoidData.isElectricCheckRequired boolValue]==YES){
            self.detailSwitch.enabled = NO;
        }
    }
}

@end
