//
//  PSPostVoidGeneralTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 10/31/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostWorksRequiredProtocol.h"
@interface PSPostVoidGeneralTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgViewArrow;
@property (weak, nonatomic) IBOutlet UILabel *lblMainText;
@property (weak, nonatomic) IBOutlet UISwitch *generalSwitch;
@property (weak, nonatomic) Appointment *appointment;
-(void) configureCell;
@property (weak, nonatomic) id<PSVPostWorksRequiredProtocol> delegate;
@end
