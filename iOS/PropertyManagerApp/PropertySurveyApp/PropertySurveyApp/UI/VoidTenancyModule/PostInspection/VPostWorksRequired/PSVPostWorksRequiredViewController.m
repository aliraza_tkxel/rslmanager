//
//  PSWorksRequiredViewController.m
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPostWorksRequiredViewController.h"
#import "PSVPostWorkRecordCell.h"
#import "PSVPostInspectionManager.h"
#import "PSBarButtonItem.h"
#import "VoidData.h"
#import "RoomData.h"
#import "WorkRequired+CoreDataClass.h"
#import "FaultDetailList.h"
#import "FaultAreaList.h"
#import "PSRSearchWorkViewController.h"
@interface PSVPostWorksRequiredViewController ()
{
	PSVPostInspectionManager * _manager;
	
	CGRect recordWorksFrame;
	CGRect checksFrame;
	CGRect contentFrame;
	Boolean isTenantWork;
	NSArray * cellData;
}
@end


@implementation PSVPostWorksRequiredViewController

#pragma mark - Events

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self initData];
	[self setupUI];
	[self loadNavigationonBarItems];
	[self reloadData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	[self.scrollView layoutIfNeeded];
	self.scrollView.contentSize = self.contentView.bounds.size;
}

#pragma mark - Functions
-(void) initData
{
	_manager = [[PSVPostInspectionManager alloc]init:self.appointment];
}

-(void) setupUI
{
	[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	[self.recordWorksContentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	
	recordWorksFrame = CGRectMake(self.recordWorksContentView.frame.origin.x,
																self.recordWorksContentView.frame.origin.y,
																self.recordWorksContentView.frame.size.width,
																self.recordWorksContentView.frame.size.height);
	checksFrame = CGRectMake(self.checksContentView.frame.origin.x,
													 self.checksContentView.frame.origin.y,
													 self.checksContentView.frame.size.width,
													 self.checksContentView.frame.size.height);
	contentFrame = CGRectMake(self.contentView.frame.origin.x,
														self.contentView.frame.origin.y,
														self.contentView.frame.size.width,
														self.contentView.frame.size.height);
	self.btnBRSWorks.layer.borderWidth = 0.5f;
	self.btnBRSWorks.layer.borderColor = [UIColor blackColor].CGColor;
	self.btnBRSWorks.layer.backgroundColor = [UIColor clearColor].CGColor;
	self.btnTenantWorks.layer.borderWidth = 0.5f;
	self.btnTenantWorks.layer.borderColor = [UIColor blackColor].CGColor;
	self.btnTenantWorks.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
}

-(void) hideRecordWorksTable:(BOOL)hide
{
	self.recordWorksContentView.hidden = hide;
	if(hide == FALSE)
	{
		CGRect newRecordFrame = CGRectMake(recordWorksFrame.origin.x,
																		   recordWorksFrame.origin.y,
																		   recordWorksFrame.size.width,
																		   recordWorksFrame.size.height);
		CGRect newCheckFrame = CGRectMake(checksFrame.origin.x,
																		  checksFrame.origin.y,
																		  checksFrame.size.width,
																		  checksFrame.size.height);
		CGRect newContentFrame = CGRectMake(contentFrame.origin.x,
																			  contentFrame.origin.y,
																			  contentFrame.size.width,
																			  contentFrame.size.height);
		self.recordWorksContentView.frame = newRecordFrame;
		self.checksContentView.frame = newCheckFrame;
		self.scrollView.contentSize = newContentFrame.size;
	}
	else
	{
		CGRect newCheckView = CGRectMake(checksFrame.origin.x,
																		 checksFrame.origin.y - recordWorksFrame.size.height,
																		 checksFrame.size.width,
																		 checksFrame.size.height);
		CGRect newContentView = CGRectMake(contentFrame.origin.x,
																			 contentFrame.origin.y,
																			 contentFrame.size.width,
																			 contentFrame.size.height - recordWorksFrame.size.height);
		self.checksContentView.frame = newCheckView;
		self.scrollView.contentSize = newContentView.size;
	}
}

-(void)reloadData
{
	self.swWorksRequired.on = [self.appointment.appointmentToVoidData.isWorksRequired boolValue];
	self.swElectricCheck.on = [self.appointment.appointmentToVoidData.isElectricCheckRequired boolValue];
	self.swEPCCheck.on = [self.appointment.appointmentToVoidData.isEPCCheckRequired boolValue];
	self.swGasCheck.on = [self.appointment.appointmentToVoidData.isGasCheckRequired boolValue];
	self.swAsbestosCheck.on = [self.appointment.appointmentToVoidData.isAsbestosCheckRequired boolValue];
	self.lblGasStatus.text = self.appointment.appointmentToVoidData.gasCheckStatus;
	self.lblGasDate.text = [UtilityClass stringFromDate:self.appointment.appointmentToVoidData.gasCheckDate dateFormat:kDateTimeStyle8];
	self.lblElectricStatus.text = self.appointment.appointmentToVoidData.electricCheckStatus;
	self.lblElectricDate.text = [UtilityClass stringFromDate:self.appointment.appointmentToVoidData.electricCheckDate dateFormat:kDateTimeStyle8];
	[self enableComponents:self.swWorksRequired.on];
	isTenantWork = TRUE;
}

-(void)enableComponents:(BOOL)enable
{
	self.btnRecordWorks.enabled = enable;
	self.swEPCCheck.enabled = enable;
	self.swAsbestosCheck.enabled = enable;
	if ([self.appointment.appointmentToVoidData.isElectricCheckRequired boolValue] == TRUE)
	{
		self.swElectricCheck.enabled = FALSE;
	}
	else
	{
		self.swElectricCheck.enabled = enable;
	}
	if([self.appointment.appointmentToVoidData.isGasCheckRequired boolValue] == TRUE)
	{
		self.swGasCheck.enabled = FALSE;
	}
	else
	{
		self.swGasCheck.enabled = enable;
	}
	NSUInteger count = self.appointment.appointmentToVoidData.voidDataToWorkRequired.count;
	if(count > 0 && enable == TRUE)
	{
		[self hideRecordWorksTable:FALSE];
		[self calculateTenantEstimate];
	}
	else
	{
		[self hideRecordWorksTable:TRUE];
	}
}

-(void)calculateTenantEstimate
{
	float tenantEstimate = 0.0;
	for (WorkRequired * workRequired in self.appointment.appointmentToVoidData.voidDataToWorkRequired)
	{
		if([workRequired.isTenantWork boolValue] == TRUE &&
			 [workRequired.isVerified boolValue] == TRUE)
		{
			tenantEstimate += [workRequired.gross floatValue];
		}
	}
	[self setTenantEstimate:tenantEstimate];
}

-(void)setTenantEstimate:(float)tenantEstimate
{
	self.lblTenantEstimate.text = [NSString stringWithFormat:@"%.2f", tenantEstimate];
	self.appointment.appointmentToVoidData.tenantEstimate = [NSNumber numberWithFloat:tenantEstimate];
}
- (IBAction)btnRecordWorksTapped:(id)sender {
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_AddRequiredWork bundle:nil];
    PSRSearchWorkViewController * jobScreen = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSRSearchWorkViewController class])];
    [jobScreen setAppointment:self.appointment];
    [self.navigationController pushViewController:jobScreen animated:YES];
}

#pragma mark - Navigation Segue
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setTitle:LOC(@"KEY_TITLE_VOID_WORKS_REQUIRED")];
}

- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[_manager saveContext];
	[self popOrCloseViewController];
}

#pragma mark - UISwitch events
- (IBAction)onChangeWorkRequired:(id)sender
{
	CLS_LOG(@"onChangeWorkRequired");
	self.appointment.appointmentToVoidData.isWorksRequired = [NSNumber numberWithBool:self.swWorksRequired.on];
	[self enableComponents:self.swWorksRequired.on];
}

- (IBAction)onClickTenantWorks:(id)sender
{
	CLS_LOG(@"onClickTenantWorks");
	isTenantWork = TRUE;
	self.btnBRSWorks.layer.backgroundColor = [UIColor clearColor].CGColor;
	self.btnTenantWorks.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
	[self.tableView reloadData];
}

- (IBAction)onClickBRSWorks:(id)sender
{
	CLS_LOG(@"onClickBRSWorks");
	isTenantWork = FALSE;
	self.btnBRSWorks.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
	self.btnTenantWorks.layer.backgroundColor = [UIColor clearColor].CGColor;
	[self.tableView reloadData];
}
- (IBAction)onChangeElectricCheck:(id)sender
{
	CLS_LOG(@"onChangeElectricCheck");
	self.appointment.appointmentToVoidData.isElectricCheckRequired = [NSNumber numberWithBool:self.swElectricCheck.on];
}
- (IBAction)onChangeEpcCheck:(id)sender
{
	CLS_LOG(@"onChangeEpcCheck");
	self.appointment.appointmentToVoidData.isEPCCheckRequired = [NSNumber numberWithBool:self.swEPCCheck.on];
}
- (IBAction)onChangeAsbestosCheck:(id)sender
{
	CLS_LOG(@"onChangeAsbestosCheck");
	self.appointment.appointmentToVoidData.isAsbestosCheckRequired = [NSNumber numberWithBool:self.swAsbestosCheck.on];
}
- (IBAction)onChangeGasCheck:(id)sender
{
	CLS_LOG(@"onChangeGasCheck");
	self.appointment.appointmentToVoidData.isGasCheckRequired = [NSNumber numberWithBool:self.swGasCheck.on];
}

#pragma mark - TableView Functions
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger count = 0;
	NSPredicate * filter = [NSPredicate predicateWithFormat:@"SELF.isTenantWork == %d", isTenantWork];
	cellData = [self.appointment.appointmentToVoidData.voidDataToWorkRequired.allObjects filteredArrayUsingPredicate:filter];
	count = [cellData count];
	CLS_LOG(@"tableView: numberOfRowsInSection: count: %ld", (long)count);
	return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PSVPostWorkRecordCell * cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPostWorkRecordCell class])];
	WorkRequired * workRequired = [cellData objectAtIndex:indexPath.row];
	FaultAreaList * roomData = workRequired.workRequiredToFaultArea;
	CLS_LOG(@"tableView: cellForRowAtIndexPath: row: %ld component: %@", (long)indexPath.row, roomData.faultAreaName);
	[cell setWorkRequired:workRequired];
	cell.delegate = self;
	return cell;
}

#pragma mark - Protocol Implementation
-(void)addedWorkRequired
{
	CLS_LOG(@"Protocol: preWorkRequiredRecorded");
	[self reloadData];
	[self.tableView reloadData];
	[_manager saveContext];
}

-(void)recalTenantEstimate:(WorkRequired *)workRequired
{
	CLS_LOG(@"Protocol: recalculateTenantAgreement");
	float tenantEstimate = [self.lblTenantEstimate.text floatValue];
	if([workRequired.isTenantWork boolValue] == TRUE)
	{
		if([workRequired.isVerified boolValue] == TRUE)
		{
			tenantEstimate += [workRequired.gross floatValue];
		}
		else
		{
			tenantEstimate -= [workRequired.gross floatValue];
		}
	}
	else
	{
		tenantEstimate -= [workRequired.gross floatValue];
	}
	[self setTenantEstimate:tenantEstimate];
	[_manager saveContext];
}

-(void)refreshTableView
{
	[self.tableView reloadData];
}

@end
