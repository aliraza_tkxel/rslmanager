//
//  PSVPreWorkRecordCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-28.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostWorksRequiredProtocol.h"
#import "WorkRequired+CoreDataClass.h"

@interface PSVPostWorkRecordCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblComponent;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnIsCompleted;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UIButton *btnShiftWork;

@property (weak, nonatomic) id<PSVPostWorksRequiredProtocol> delegate;
@property (weak, nonatomic) WorkRequired * workRequired;

-(void)setWorkRequired:(WorkRequired *)workRequired;

@end
