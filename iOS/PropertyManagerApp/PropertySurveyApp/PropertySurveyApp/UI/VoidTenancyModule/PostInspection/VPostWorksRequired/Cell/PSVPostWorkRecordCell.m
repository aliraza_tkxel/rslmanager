//
//  PSVPreWorkRecordCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-28.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPostWorkRecordCell.h"
#import "RoomData.h"
#import "FaultAreaList.h"
#import "FaultDetailList.h"

@implementation PSVPostWorkRecordCell

#pragma mark - Cell Init Setting
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{ }
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:FALSE animated:YES];
	// Configure the view for the selected state
}

#pragma mark - Functions
-(void)setWorkRequired:(WorkRequired *)workRequired
{
	_workRequired = workRequired;
	self.lblComponent.text = self.workRequired.workRequiredToFaultArea.faultAreaName;
	self.lblDescription.text = workRequired.repairDescription;
	[self.btnIsCompleted setSelected:[self.workRequired.isVerified boolValue]];
	self.lblStatus.text = _workRequired.status;
	self.btnShiftWork.hidden = [_workRequired.isBRSWork boolValue];
}

#pragma mark - Button Events
- (IBAction)onClickTenantAgreed:(id)sender
{
	UIButton * button = sender;
	if(button.selected == YES)
	{
		[button setSelected:NO];
	}
	else
	{
		[button setSelected:YES];
	}
	self.workRequired.isVerified = [NSNumber numberWithBool:button.selected];
	if (self.delegate && [self.delegate respondsToSelector:@selector(recalTenantEstimate:)])
	{
		[self.delegate recalTenantEstimate:self.workRequired];
	}
}

- (IBAction)onClickShiftToBRS:(id)sender
{
	CLS_LOG(@"onClickShiftToBRS");
	self.workRequired.isBRSWork = [NSNumber numberWithBool:TRUE];
	self.workRequired.isTenantWork = [NSNumber numberWithBool:FALSE];
	if (self.delegate && [self.delegate respondsToSelector:@selector(refreshTableView)])
	{
		[self.delegate refreshTableView];
	}
}

@end
