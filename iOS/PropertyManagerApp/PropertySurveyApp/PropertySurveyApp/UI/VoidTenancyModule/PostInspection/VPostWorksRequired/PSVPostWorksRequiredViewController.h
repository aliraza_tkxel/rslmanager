//
//  PSWorksRequiredViewController.h
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSVPostWorksRequiredProtocol.h"

#define WORKS_REQUIRED_SEGUE_IDENTIFIER @"WorksRequiredScreen"

@interface PSVPostWorksRequiredViewController : PSCustomViewController<PSVPostWorksRequiredProtocol>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIView *recordWorksContentView;
@property (strong, nonatomic) IBOutlet UIView *checksContentView;
@property (strong, nonatomic) IBOutlet UIButton *btnRecordWorks;
@property (strong, nonatomic) IBOutlet UILabel *lblGasStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblGasDate;
@property (strong, nonatomic) IBOutlet UILabel *lblElectricStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblElectricDate;

@property (strong, nonatomic) IBOutlet UIButton *btnBRSWorks;
@property (strong, nonatomic) IBOutlet UIButton *btnTenantWorks;

@property (strong, nonatomic) IBOutlet UILabel *lblTenantEstimate;

@property (strong, nonatomic) IBOutlet UISwitch *swWorksRequired;
@property (strong, nonatomic) IBOutlet UISwitch *swElectricCheck;
@property (strong, nonatomic) IBOutlet UISwitch *swEPCCheck;
@property (strong, nonatomic) IBOutlet UISwitch *swAsbestosCheck;
@property (strong, nonatomic) IBOutlet UISwitch *swGasCheck;

@property (weak,   nonatomic) Appointment *appointment;
@end
