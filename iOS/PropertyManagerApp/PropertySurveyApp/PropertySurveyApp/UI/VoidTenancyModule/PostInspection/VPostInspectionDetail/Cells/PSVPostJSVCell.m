//
//  PSJSNumberCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVPostJSVCell.h"
#import "VoidData.h"

@implementation PSVPostJSVCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(id)data
{
	[self setAppointment:data];
	
	self.lblTitle.text = self.appointment.appointmentToVoidData.jsvNumber;
	self.lblClickHereJobSheet.text = LOC(@"KEY_STRING_CLICK_HERE_FOR_JOBSHEET");
	[self.lblFaultDetail setHidden:NO];
	
	NSString * jobStatus = self.appointment.appointmentStatus;
	if ([jobStatus isEqualToString:kJobStatusPaused])
	{
		[self.imgStatus setHidden:NO];
		self.imgStatus.image = [UIImage imageNamed:@"btn_Pause"];
	}
	else if ([jobStatus isEqualToString:kJobStatusComplete] || [jobStatus isEqualToString:kJobStatusNoEntry])
	{
		[self.imgStatus setHidden:NO];
		self.imgStatus.image = [UIImage imageNamed:@"icon_completed"];
	}
	else if ([jobStatus isEqualToString:kJobStatusInProgress])
	{
		[self.imgStatus setHidden:NO];
		self.imgStatus.image = [UIImage imageNamed:@"btn_Play"];
	}
	else if ([jobStatus isEqualToString:kJobStatusNotStarted] || [jobStatus isEqualToString:kJobStatusAccepted])
	{
		[self.imgStatus setHidden:YES];
	}
	
	if([self.appointment getStatus] == AppointmentStatusInProgress)
	{
		[self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
	}
	else
	{
		[self setAccessoryType:UITableViewCellAccessoryNone];
	}
	
	self.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT );
	self.lblClickHereJobSheet.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT );
	self.lblFaultDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblClickHereJobSheet.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
