//
//  TenantCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSVPostTenantCell.h"

@implementation PSVPostTenantCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(id)data
{
	[self setCustomer:data];
	
	NSString *customerName = [self.customer fullName];
	if(!isEmpty(self.customer.telephone))
	{
		NSMutableString *telephone = [NSMutableString stringWithFormat:@"   %@",LOC(@"KEY_STRING_TEL")];
		[telephone appendString:@". "];
		[telephone appendString:self.customer.telephone];
		self.btnTelephone.hidden = NO;
		[self.btnTelephone setTitle:telephone forState:UIControlStateNormal];
		[self.btnTelephone setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateNormal];
		[self.btnTelephone setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateHighlighted];
		self.btnTelephone.tag = 0;
		[self.btnTelephone setFrame:CGRectMake(12, 37, 240, 24)];
		[self.btnMobile setFrame:CGRectMake(12, 65, 240, 24)];
	}
	else
	{
		[self.lblMobile setFrame:self.lblTelephone.frame];
		[self.btnMobile setFrame:self.btnTelephone.frame];
	}
	
	if (!isEmpty(self.customer.mobile) && ![self.customer.mobile isEqualToString:@"(null)"])
	{
		NSMutableString *mobile = [NSMutableString stringWithFormat:@"   %@",LOC(@"KEY_STRING_MOB")];
		[mobile appendString:@". "];
		[mobile appendString:self.customer.mobile];
		self.lblMobile.text = mobile;
		self.lblMobile.hidden = NO;
		self.btnMobile.hidden = NO;
		[self.btnMobile setTitle:mobile forState:UIControlStateNormal];
		[self.btnMobile setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateNormal];
		[self.btnMobile setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateHighlighted];
		self.btnMobile.tag = 0;
	}
	else
	{
		self.lblMobile.hidden = YES;
		self.btnMobile.hidden = YES;
	}
	self.lblTenantName.text = customerName;
	self.lblTenantName.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
	self.lblMobile.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblTelephone.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblTenantName.highlightedTextColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
	self.lblMobile.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblTelephone.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	
	
	[self.btnEditTenant setTag:0];
	[self.btnDisclosureIndicator setTag:0];
	
	if (self.isEditing)
	{
		[self.btnEditTenant setHidden:NO];
	}
	else
	{
		[self.btnEditTenant setHidden:YES];
	}
	self.selectionStyle = UITableViewCellSelectionStyleGray;
}

-(void) setTelephoneTarget:(id)owner Action:(SEL)action
{
	[self.btnTelephone addTarget:owner action:action forControlEvents:UIControlEventTouchUpInside];
}

-(void) setMobileTarget:(id)owner Action:(SEL)action
{
	[self.btnMobile addTarget:owner action:action forControlEvents:UIControlEventTouchUpInside];
}

-(void) setDisclosureIndicatorTarget:(id)owner Action:(SEL)action
{
	[self.btnDisclosureIndicator addTarget:owner action:action forControlEvents:UIControlEventTouchUpInside];
}

-(void) setEditTenantTarget:(id)owner Action:(SEL)action
{
	[self.btnEditTenant addTarget:owner action:action forControlEvents:UIControlEventTouchUpInside];
}

@end
