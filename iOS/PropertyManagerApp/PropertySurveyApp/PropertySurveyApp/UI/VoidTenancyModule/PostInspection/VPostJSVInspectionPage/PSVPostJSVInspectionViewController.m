//
//  PSJSVInspectionViewController.m
//  PropertySurveyApp
//
//  Created by aqib javed on 18/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVPostJSVInspectionViewController.h"
#import "PSVPostWorksRequiredViewController.h"
#import "PSVPostMajorWorksRequiredViewController.h"
#import "PSVPostPaintPackViewController.h"
#import "PSVPostInspectionManager.h"
#import "PSBarButtonItem.h"
#import "VoidData.h"
#import "PSVNewPostWorksRequiredViewController.h"

@interface PSVPostJSVInspectionViewController ()
{
	PSVPostInspectionManager * _manager;
}
@end

@implementation PSVPostJSVInspectionViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self initData];
	[self loadNavigationonBarItems];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


#pragma mark - Functions
- (void)initData
{
	_manager = [[PSVPostInspectionManager alloc]init:self.appointment];
	[self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	[self setReletDateValue];
}

-(void)setReletDateValue
{
	self.lblReletDate.text = [UtilityClass stringFromDate:self.appointment.appointmentToVoidData.reletDate dateFormat:kDateTimeStyle8];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];

	[self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	NSString * title = [NSString stringWithFormat:@"%@ %@", self.appointment.appointmentToVoidData.jsvNumber, self.appointment.appointmentType];
	[self setTitle:title];
}

- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[_manager saveContext];
	[self popOrCloseViewController];
}

- (IBAction)onClickCompleteInspection:(id)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOC(@"KEY_ALERT_CONFIRM_POPUP_HEADING") message:LOC(@"KEY_CONFIRM_COMPLETE_ACTION") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_NO") style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        [_manager saveAppointmentAndMarkCompleted:NO];
        [self popToRootViewController];
        NSLog(@"Cancelled Appointment Completion");
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [_manager saveAppointmentAndMarkCompleted:YES];
        [self popToRootViewController];
        NSLog(@"Confirmed Appointment Completion");
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated: YES completion: nil];

}

#pragma mark Navigation Segue
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	CLS_LOG(@"prepareForSegue: %@", segue.identifier);
	if([segue.identifier isEqualToString:WORKS_REQUIRED_SEGUE_IDENTIFIER])
	{
		PSVNewPostWorksRequiredViewController * controller = (PSVNewPostWorksRequiredViewController *)segue.destinationViewController;
		[controller setAppointment:self.appointment];
	}
	else if([segue.identifier isEqualToString:MAJOR_WORKS_REQUIRED_SEGUE_IDENTIFIER])
	{
		PSVPostMajorWorksRequiredViewController * controller = (PSVPostMajorWorksRequiredViewController *)segue.destinationViewController;
		[controller setAppointment:self.appointment];
	}
	else if ([segue.identifier isEqualToString:PAINT_PACK_SEGUE_IDENTIFIER])
	{
		PSVPostPaintPackViewController * controller = (PSVPostPaintPackViewController *)segue.destinationViewController;
		[controller setAppointment:self.appointment];
	}
}

#pragma mark Date Picker Event
- (IBAction)onClickReletDate:(id)sender
{
	NSDate * selectedDate = self.appointment.appointmentToVoidData.reletDate;
	[ActionSheetDatePicker showPickerWithTitle:self.lblReletDateTitle.text
															datePickerMode:UIDatePickerModeDate
																selectedDate:selectedDate
																			target:self
																			action:@selector(onDateSelected:element:)
																			origin:sender];
}

- (void)onDateSelected:(NSDate *)selectedDate element:(id)element
{
	NSDate* terminationDate = self.appointment.appointmentToVoidData.terminationDate;
	
	if ([selectedDate compare:terminationDate] == NSOrderedAscending) {
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																									 message:LOC(@"KEY_ALERT_INVALID_RELET_MESSAGE")
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
	}else{
		self.appointment.appointmentToVoidData.reletDate = selectedDate;
		[self setReletDateValue];
	}

}

@end
