//
//  PSFaultRepairListCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 13/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFaultRepairListCell.h"

@implementation PSFaultRepairListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self.lblRepairDetail setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        self.lblRepairDetail.textAlignment = NSTextAlignmentLeft;
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) makeSelected
{
    [self.imgSelected setHidden:NO];
}

- (void) makeDeselected
{
    [self.imgSelected setHidden:YES];
}

@end
