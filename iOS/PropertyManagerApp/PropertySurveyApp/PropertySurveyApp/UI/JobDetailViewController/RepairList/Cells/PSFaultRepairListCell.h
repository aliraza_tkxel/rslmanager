//
//  PSFaultRepairListCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 13/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSFaultRepairListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblRepairDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgSelected;
- (void) makeSelected;
- (void) makeDeselected;
@end
