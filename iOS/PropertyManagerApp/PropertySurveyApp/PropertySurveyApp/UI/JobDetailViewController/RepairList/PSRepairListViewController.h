//
//  PSRepairListViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 13/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
@class PSFaultRepairListCell;

@interface PSRepairListViewController : PSCustomViewController <UITextFieldDelegate,  NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btnRepair;
@property (strong, nonatomic) IBOutlet UIButton *recentBtn;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong,  nonatomic) NSString * searchString;
@property (strong,  nonatomic) NSNumber * isAssociated;
@property (strong,  nonatomic) NSPredicate *repairFilterPredicate;
@property (weak,   nonatomic) IBOutlet PSFaultRepairListCell *repairDetailCell;
@property (strong,  nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong,  nonatomic) NSFetchedResultsController *repairFetchedResultsController;
@property (strong,  nonatomic) NSFetchRequest *repairFetchRequest;
@property (strong, nonatomic) FaultJobSheet *jobData;

@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
- (IBAction)onClickClearSearch:(id)sender;
- (IBAction)resignFirstResponder:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *searchBarView;
@end
