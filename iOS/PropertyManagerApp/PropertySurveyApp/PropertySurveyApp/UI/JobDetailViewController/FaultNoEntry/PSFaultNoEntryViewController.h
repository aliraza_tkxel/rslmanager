//
//  PSFaultNoEntryViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 13/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"

@interface PSFaultNoEntryViewController : PSCustomViewController <UITextViewDelegate>
@property (weak,   nonatomic) Appointment *appointment;
@property (strong, nonatomic) IBOutlet UITextView *txtViewNoEntry;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@end
