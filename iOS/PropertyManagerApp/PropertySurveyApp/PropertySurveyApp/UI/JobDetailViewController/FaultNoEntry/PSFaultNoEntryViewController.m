//
//  PSFaultNoEntryViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 13/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFaultNoEntryViewController.h"
#import "PSBarButtonItem.h"
#import "PSTextView.h"

@interface PSFaultNoEntryViewController ()
{
    PSTextView *_textView;
}

@end

@implementation PSFaultNoEntryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _textView = [[PSTextView alloc] initWithFrame:self.txtViewNoEntry.frame];
	_textView.delegate = self;
	[_textView setBackgroundColor:[UIColor grayColor]];
	[_textView canBecomeFirstResponder];
	self.txtViewNoEntry = _textView;
	[self.txtViewNoEntry setUserInteractionEnabled:YES];
	[self.txtViewNoEntry setEditable:YES];
    [self.txtViewNoEntry setBackgroundColor:[UIColor whiteColor]];
	[self.txtViewNoEntry setDelegate:self];
	[self.txtViewNoEntry setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
    
	[self.view addSubview:self.txtViewNoEntry];
	[self.txtViewNoEntry setReturnKeyType:UIReturnKeyDefault];
    
	[self loadNavigationonBarItems];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotification];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:LOC(@"KEY_STRING_NO_ENTRY")];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_NO_ENTRY")
                                                   message:LOC(@"KEY_ALERT_CONFIRM_NO_ENTRY")
                                                  delegate:self
                                         cancelButtonTitle:LOC(@"KEY_ALERT_NO")
                                         otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
    alert.tag = AlertViewTagNoEntry;
    [alert show];
    
}

#pragma mark - Notifications

- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveUpdateNoEntrySuccessNotification) name:kUpdateFaultAppointmentWithNoEntrySuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveApplianceFailureNotification) name:kUpdateFaultAppointmentWithNoEntryFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateFaultAppointmentWithNoEntrySuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateFaultAppointmentWithNoEntryFailureNotification object:nil];
}

- (void) onReceiveUpdateNoEntrySuccessNotification
{
    [self hideActivityIndicator];
    [self popToRootViewController];
}

#pragma mark - UITextView Delegate Methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
	self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	[self.view endEditing:YES];
}

- (void)textViewDidChange:(UITextView *)textView {
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewTagNoEntry)
    {
        if(buttonIndex == alertView.firstOtherButtonIndex)
        {
            [self updateAppointmentWithNoEntry];
        }
    }
}


#pragma mark - Methods
- (void) updateAppointmentWithNoEntry
{
    NSMutableDictionary *noEntryDictionary = [NSMutableDictionary dictionary];
    [noEntryDictionary setObject:self.appointment.objectID forKey:@"objectId"];
    [noEntryDictionary setObject:isEmpty(self.txtViewNoEntry.text)?[NSNull null]:self.txtViewNoEntry.text forKey:kNoEntryNotes];
    
    [[PSDataUpdateManager sharedManager] updateFaultAppointmentWithNoEntry:noEntryDictionary];
    
}
@end
