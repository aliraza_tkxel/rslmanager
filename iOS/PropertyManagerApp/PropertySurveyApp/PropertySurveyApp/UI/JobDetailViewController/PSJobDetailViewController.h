//
//  PSJobDetailViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDConstants.h"

#define kPauseJobViewMode 0
#define kCompleteJobViewMode 1



@class PSFaultDetailsCell;
@class PSFaultNotesCell;
@class PSPriorityCell;
@class PSJobDetailAsbestosCell;
@class PSDurationCell;
@class PSResponseTimeCell;
@class PSBarButtonItem;

@interface PSJobDetailViewController : PSCustomViewController <UIAlertViewDelegate>

@property (weak,   nonatomic) IBOutlet PSFaultDetailsCell *faultDetailsCell;
@property (weak,   nonatomic) IBOutlet PSFaultNotesCell *faultNotesCell;
@property (weak,   nonatomic) IBOutlet PSPriorityCell *priorityCell;
@property (weak,   nonatomic) IBOutlet PSJobDetailAsbestosCell *jobDetailAsbestosCell;
@property (weak,   nonatomic) IBOutlet PSDurationCell *durationCell;
@property (weak,   nonatomic) IBOutlet PSResponseTimeCell *responseTimeCell;
@property (assign, nonatomic) AppointmentType appointmentType;
@property (strong, nonatomic) FaultJobSheet *jobData;
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) NSArray *jobAsbestosArray;
@property (strong, nonatomic) PSBarButtonItem *completeButton;
@property (strong, nonatomic) PSBarButtonItem *pauseButton;
@property (strong, nonatomic) PSBarButtonItem *resumeButton;
@property (strong, nonatomic) PSBarButtonItem *cameraButton;

- (void) loadNavigationonBarItems;
- (void) addTableHeaderView;
- (CGFloat) heightForLabelWithText:(NSString*)string;

@end
