//
//  PSTableView.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 10/12/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSTableView.h"

@implementation PSTableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)reloadData
{
    if(_allowRelaodingTableView_)
    {
        [super reloadData];
    }
    
}

@end
