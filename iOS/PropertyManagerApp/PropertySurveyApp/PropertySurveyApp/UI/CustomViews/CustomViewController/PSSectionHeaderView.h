//
//  PSSectionHeaderView.h
//  PropertySurveyApp
//
//  Created by TkXel on 05/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSectionHeaderView : UIView
{
    @private
        UILabel *_titleLabel;
        UIImageView *_titleImgView;
    
}
@property(strong, nonatomic, setter = setTitle:) NSString *title;

/*!
 @discussion
 initWithTitle Inititalizes the Section Header View With Given Title.
 */
- (id)initWithTitle:(NSString *)title;
- (id)init;
- (void) setHeaderTitle:(NSString *)title;
- (void) setHeaderTitleImage:(NSString *)imageName;
- (void) hideHeaderTitleImage;
@end
