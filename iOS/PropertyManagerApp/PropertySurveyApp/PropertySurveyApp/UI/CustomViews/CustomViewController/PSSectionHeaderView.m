//
//  PSSectionHeaderView.m
//  PropertySurveyApp
//
//  Created by TkXel on 05/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSectionHeaderView.h"

#define kViewSectionHeaderHeight 20

@implementation PSSectionHeaderView

- (id)initWithTitle:(NSString *)title
{
    CGRect frame = CGRectZero;
    if(!isEmpty(title))
    {
        frame = CGRectMake(0, 0, 320, kViewSectionHeaderHeight);
    }
    
    if(self = [self initWithFrame:frame])
    {
        [self setHeaderTitle:title];
    }
    return self;
}

- (id)init
{
	CGRect frame = CGRectZero;
	frame = CGRectMake(0, 0, 320, kViewSectionHeaderHeight);
	self = [self initWithFrame:frame];
	return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.frame = CGRectMake(12, 3, 320, kViewSectionHeaderHeight);
        _titleLabel.backgroundColor = [UIColor clearColor];
        [_titleLabel setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        _titleLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12];
        _titleLabel.text = self.title;
        
        CGRect imageRect = [[UIScreen mainScreen] bounds];
        imageRect.size.height=1;
        UIImage *image = [UIImage imageNamed:@"line"];
        UIImageView *headerImage = [[UIImageView alloc] init];
        [headerImage setImage:image];
        [headerImage setFrame:imageRect];
    
        CGRect titleImageRect = CGRectMake(self.frame.size.width-30,3,20, 18);
        _titleImgView = [[UIImageView alloc] init];
        [_titleImgView setFrame:titleImageRect];
        _titleImgView.hidden = YES;
    
        [self addSubview:headerImage];
        [self addSubview:_titleLabel];
        [self addSubview:_titleImgView];
    }
    return self;
}

- (void) setHeaderTitle:(NSString *)title
{
    self.title = [NSString stringWithString:title];
    _titleLabel.text = self.title;
    [self layoutSubviews];
}

- (void) setHeaderTitleImage:(NSString *)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    [_titleImgView setImage:image];
    _titleImgView.hidden = NO;
    [self layoutSubviews];
}

- (void) hideHeaderTitleImage
{
	_titleImgView.hidden = YES;
}

@end
