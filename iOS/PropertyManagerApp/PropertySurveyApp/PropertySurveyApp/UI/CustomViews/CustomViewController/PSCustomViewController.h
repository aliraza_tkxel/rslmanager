//
//  PSCustomViewController.h
//  PropertySurveyApp
//
//  Created by TkXel on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSCustomViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    
    UIActivityIndicatorView *activityIndicator;
    
    /*!
     @discussion
     isEditing represetnts Controller Edit Mode.
     */
    BOOL isEditing;
    
    /*!
     @discussion
     isDispatchReady represents Data Updates Ready State. (Core Data Update should be performed on TRUE)
     */
    BOOL isDispatchReady;
    
}
@property (assign, nonatomic, setter = setEditing:) BOOL isEditing;
@property (assign, nonatomic, setter = setDispatchReady:) BOOL isDispatchReady;
@property (strong, nonatomic) UIControl *selectedControl;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

#pragma mark - Navigation Bar
/*!
 @discussion
 Method to Set Navigation Bar Items.
 */
- (void) loadNavigationonBarItems;

/*!
 @discussion
 Sets the Title of the UINavigationBar. This method must be called after setting the Left and Right Bar Button Items.
 */
- (void) setTitle:(NSString *)title;


/*!
 @discussion
 Sets the Left Bar Button Items
 */
- (void) setLeftBarButtonItems:(NSArray *)leftBarButtonItems;

/*!
 @discussion
 Sets the Right Bar Button Items
 */
- (void) setRightBarButtonItems:(NSArray *)rightBarButtonItems;

#pragma mark - Activity Indicator
/*!
 @discussion
 Shows the UIActivityIndicator Animated.
 */
-(void)showActivityIndicator:(NSString*)title;

/*!
 @discussion
 Hides the UIActivityIndicator Animated.
 */
-(void)hideActivityIndicator;


#pragma mark - Alert View
/*!
 @discussion
 Shows the UIAlertView Animated.
 */
- (void) showAlertWithTitle:(NSString*)title message:(NSString *)message tag:(NSInteger)tag cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles,... NS_REQUIRES_NIL_TERMINATION;


#pragma mark -
/*!
 @result
 The tint color to use for UINavigationBars.
 */
- (UIColor *)navigationBarTint;

/*!
 @result
 The tint color to use for UIToolbars.
 */
- (UIColor *)toolbarTint;

/*!
 @result
 The tint color to use for UISearchBars.
 */
- (UIColor *)searchBarTint;

/*!
 @result
 YES if this is the root view controller contained in a nav controller.
 */
- (BOOL)isRootViewController;

/*!
 @discussion
 If this view controller is the top view controller in the navigation bar stack, closes the view
 controller and takes the user back to the dashboard; othrewise, executes a pop operation on the
 navigation controller.
 */

- (void)popOrCloseViewController;


- (void)popToRootViewController;

#pragma mark - UI Keyboard Notifications
/*!
 @discussion
 Keyboard Notifications:
 @li - UIKeyboardDidShowNotification
 @li - UIKeyboardWillHideNotification
 @li
 Each notification includes a nil object and a userInfo dictionary containing the
 begining and ending keyboard frame in screen coordinates. Use the various UIView and
 UIWindow convertRect facilities to get the frame in the desired coordinate system.
 Animation key/value pairs are only available for the "will" family of notification.
 */
- (void) registerKeyboardNotifications;
- (void) deregisterKeyboardNotifications;
- (void) scrollToRectOfSelectedControl;

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Creates an array consisting of table view section header views
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string;

#pragma mark - Resign All Responders
- (void) resignAllResponders;

#pragma mark - Load Section Header

/*!
 @discussion
 Creates an array consisting of table view section header views
 */
- (void) loadSectionHeaderViews:(NSArray *)sectionTitles headerViews:(NSMutableArray *)headerViews;

#pragma mark - View Controller Visible
/*!
 @discussion
 Check if View Controller is visible
 */
- (BOOL)isVisible;

#pragma mark - Navigation Bar Methods
/*!
 @discussion
 Enables all Navigation Bar Button Items
 */
- (void) enableNavigationBarButtons;

/*!
 @discussion
 Disables all Navigation Bar Button Items
 */
- (void) disableNavigationBarButtons;


#pragma mark - UI Methods
/*!
 @discussion
 Schedules the "activateUIControls" method on mail loop for 30 sec delay.
 */
- (void) schduleUIControlsActivation;

/*!
 @discussion
 Remove all Activity Indicators and Enables all Navigation Controls.
 */
- (void) activateUIControls;

-(void) showMessageWithHeader:(NSString *)header
                      andBody:(NSString *)bodyMessage;

@end
