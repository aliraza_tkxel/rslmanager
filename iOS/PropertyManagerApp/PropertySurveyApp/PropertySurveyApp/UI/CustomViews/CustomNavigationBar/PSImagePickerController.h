//
//  PSCustomNavigationController.h
//  PropertySurveyApp
//
//  Created by TkXel on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSImagePickerController : UIImagePickerController

- (UILabel *) titleViewWithTitle:(NSString *)title;
- (UILabel *) titleViewWithTitle:(NSString *)title rect:(CGRect)rect;

@end
