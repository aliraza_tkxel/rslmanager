//
//  PSBarButtonItem.h
//  PropertySurveyApp
//
//  Created by TkXel on 23/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PSBarButtonItemStyle)
{
    PSBarButtonItemStyleDefault     = 1, //
    PSBarButtonItemStyleBack        = 2, //
    PSBarButtonItemStyleSearch      = 3, //
    PSBarButtonItemStyleAdd         = 4, //
    PSBarButtonItemStyleTrash       = 5, //
    PSBarButtonItemStyleNotes       = 6, //
    PSBarButtonItemStyleCamera      = 7, //
    PSBarButtonItemStyleHome        = 8, //
    PSBarButtonItemStylePlay        = 9, //
    PSBarButtonItemStylePause       = 10,//
    PSBarButtonItemStyleCheckmark   = 11,//
    PSBarButtonItemStyleEdit        = 12,//
    PSBarButtonItemStyleDownload    = 13,//
    PSBarButtonItemStyleLogout      = 15,//
    PSBarButtonItemStyleAbort      = 16,//
    
    
};

typedef NS_ENUM(NSInteger, PSBarButtonItemSeperatorPosition)
{
    PSBarButtonItemSeperatorPositionLeft    = 1, //
    PSBarButtonItemSeperatorPositionRight   = 2, //
};

@interface PSBarButtonItem : UIBarButtonItem
@property (nonatomic, assign) CGSize buttonSize;

- (id)initWithCustomStyle:(NSString *)title style:(PSBarButtonItemStyle)style target:(id)target action:(SEL)action;

- (id)initWithCustomStyle:(NSString *)title style:(PSBarButtonItemStyle)style target:(id)target action:(SEL)action bagde:(NSString*) badgeCount;

-(void) reloadBadge:(NSString*) badgeCount;

@end
