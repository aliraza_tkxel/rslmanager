//
//  PSButton.h
//  PropertySurveyApp
//
//  Created by TkXel on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSBarButtonItem.h"

@interface PSButton : UIButton
@property (nonatomic, assign) CGSize buttonSize;

- (PSButton *) initWithStyle:(PSBarButtonItemStyle)style;
- (void) setTitle:(NSString *)title;

@end

