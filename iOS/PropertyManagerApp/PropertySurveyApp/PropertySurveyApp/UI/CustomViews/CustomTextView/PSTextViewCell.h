//
//  PSTextViewCell.h
//  PropertySurveyApp
//
//  Created by TkXel on 11/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextView.h"

//PSTextView Cell Resuse Identifier
static NSString *textViewCellIdentifier = @"PSTextViewCell";

@interface PSTextViewCell : UITableViewCell <UITextViewDelegate>
@property (strong, nonatomic) PSTextView *textView;
@property (nonatomic,weak) UITableView *parentTableView;
@property NSInteger parentTableViewSection;
@end
