//
//  PSTextView.h
//  PropertySurveyApp
//
//  Created by TkXel on 11/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTextView : UITextView

@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, strong) UIColor *realTextColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *placeholderColor UI_APPEARANCE_SELECTOR;

- (void)setPlaceholderText:(NSString *)aPlaceholder;

@end
