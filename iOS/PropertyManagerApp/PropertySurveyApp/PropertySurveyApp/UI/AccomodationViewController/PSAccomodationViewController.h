//
//  PSAccomodationViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PSAccomodationProtocol
- (void) didChangeAccomodationsAtIndexPath:(NSIndexPath *)indexPath;
@end

@class PSAccomodationViewCell;
@class PSBarButtonItem;

@interface PSAccomodationViewController : PSCustomViewController <UITextFieldDelegate, PSAccomodationProtocol>

@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (weak,   nonatomic) IBOutlet PSAccomodationViewCell *accomodationViewCell;
@property (strong, nonatomic) NSMutableArray *accomodationsArray;
@property (strong, nonatomic) NSMutableDictionary *updatedAccomodations;
@property (strong, nonatomic) PSBarButtonItem *editButton;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (weak,   nonatomic) Appointment *appointment;
@end
