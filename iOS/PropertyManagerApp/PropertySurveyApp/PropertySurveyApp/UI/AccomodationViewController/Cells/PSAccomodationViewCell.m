//
//  PSAccomodationCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAccomodationViewCell.h"

@implementation PSAccomodationViewCell
@synthesize lblCellTitle;
@synthesize txtWidth;
@synthesize txtArea;
@synthesize txtLength;
@synthesize delegate;
@synthesize indexPath;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)resignFirstResponder:(id)sender{
    UITextField *textField = (UITextField *)sender;
    [textField resignFirstResponder];
}

//- (void) setKeyValueObservers
//{
//    [lblArea addObserver:self
//           forKeyPath:@"text"
//              options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew
//              context:nil];
//}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//    
//    //CLS_LOG(@"Change: %@", change);
//    NSString *oldValue = [change objectForKey:@"old"];
//    NSString *newValue = [change objectForKey:@"new"];
//    
//    if(!isEmpty(newValue) && !isEmpty(oldValue))
//    {
//       if(![newValue compare:oldValue] == NSOrderedSame)
//       {
//            //Area Changed
//            if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didChangeAccomodationsAtIndexPath:)])
//            {
//                [(NSObject *)self.delegate performSelector:@selector(didChangeAccomodationsAtIndexPath:)
//                                           withObject:self.indexPath];
//            }
//       }
//    }
//}

@end
