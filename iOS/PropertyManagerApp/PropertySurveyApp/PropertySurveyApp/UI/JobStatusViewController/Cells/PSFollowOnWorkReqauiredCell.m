//
//  PSFollowOnWorkReqauiredCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFollowOnWorkReqauiredCell.h"

@implementation PSFollowOnWorkReqauiredCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)valueChanged:(id)sender
{
    UISwitch *followonSwitch = (UISwitch *)sender;
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didChangeSwitchOption:)])
    {
        [(PSJobStatusViewController *)self.delegate didChangeSwitchOption:followonSwitch.on];
    }

}
@end
