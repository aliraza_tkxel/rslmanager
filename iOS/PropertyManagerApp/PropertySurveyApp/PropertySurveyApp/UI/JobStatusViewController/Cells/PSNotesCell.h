//
//  PSNotesCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSNotesCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UITextView *txtViewNote;

@end
