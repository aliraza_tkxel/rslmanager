//
//  PSReasonOrRepairCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSReasonOrRepairCell.h"
#import "PSJobStatusViewController.h"


@interface PSReasonOrRepairCell()
@end

@implementation PSReasonOrRepairCell
@synthesize lblHeader;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)onClickBtnPicker:(id)sender {
    if(!isEmpty(self.pickerOptions))
    {
        [ActionSheetStringPicker showPickerWithTitle:self.lblHeader
                                                rows:self.pickerOptions
                                    initialSelection:self.selectedIndex
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self.selectedIndex = [selectedIndex integerValue];
    self.lblTitle.text = [self.pickerOptions objectAtIndex:self.selectedIndex];
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectPickerOption:)])
    {
        [(PSJobStatusViewController *)self.delegate didSelectPickerOption:self.selectedIndex];
    }
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

@end
