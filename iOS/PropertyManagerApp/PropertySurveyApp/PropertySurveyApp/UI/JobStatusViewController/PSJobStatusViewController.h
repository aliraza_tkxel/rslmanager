//
//  PSJobStatusViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextViewCell.h"

@class PSReasonOrRepairCell;
@class PSNotesCell;
@class PSFollowOnWorkReqauiredCell;
@class PSBarButtonItem;


@protocol PSJobStatusPickerOptionProtocol <NSObject>
@required
/*!
 @discussion
 Method didPickerOptionSelect Called upon selection of any picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerOption;
/*!
 @discussion
 Method didPickerOptionSelect Called upon changing switch value.
 */
- (void) didChangeSwitchOption:(BOOL) valueChanged;
@end


@interface PSJobStatusViewController : PSCustomViewController <PSJobStatusPickerOptionProtocol, UITextViewDelegate>

@property (assign, nonatomic) ViewMode viewMode;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)onClickBtnConfirm:(id)sender;
- (IBAction)onClickBtnCancel:(id)sender;
@property (weak,   nonatomic) IBOutlet PSReasonOrRepairCell *reasonOrResponseCell;
@property (weak,   nonatomic) IBOutlet PSNotesCell *notesCell;
@property (strong, nonatomic) IBOutlet PSTextViewCell *textViewCell;
@property (weak,   nonatomic) IBOutlet PSFollowOnWorkReqauiredCell *followOnWorkRequiredCell;
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) FaultJobSheet *jobData;

@property (strong, nonatomic) PlannedTradeComponent *plannedComponent;

@property (strong, nonatomic) PSBarButtonItem *playButton;

@end
