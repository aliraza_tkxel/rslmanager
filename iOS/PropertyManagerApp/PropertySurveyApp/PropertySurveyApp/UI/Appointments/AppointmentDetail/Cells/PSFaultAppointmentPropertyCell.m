//
//  PSFaultAppointmentPropertyCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSFaultAppointmentPropertyCell.h"
#import "PropertyPicture+MWPhoto.h"

@implementation PSFaultAppointmentPropertyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    }
    
    return self;
}

-(void)onPictureDefault:(NSNotification*) notification
{
    PropertyPicture * picture=notification.object;
    
    [self.imgProperty setImageWithURL:[NSURL URLWithString:picture.imagePath] andAddBorder:YES];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) reloadData:(Appointment*)appointment
{
    //self.imgProperty.thumbnail=YES;
    if([appointment getType] == AppointmentTypeGas || [appointment getType] ==AppointmentTypeGasVoid)
    {
        if(!isEmpty(appointment.appointmentToProperty.certificateExpiry)){
            self.lblCertificateExpiryDate.text = [UtilityClass stringFromDate:appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8];
            [self.lblCertificateExpiry setHidden:NO];
            [self.lblCertificateExpiryDate setHidden:NO];
        }
        else{
            [self.lblCertificateExpiry setHidden:NO];
            [self.lblCertificateExpiryDate setHidden:NO];
            self.lblCertificateExpiryDate.text = @"N/A";
        }
        
    }
    else
    {
        [self.lblCertificateExpiry setHidden:YES];
        [self.lblCertificateExpiryDate setHidden:YES];
    }
    
    
    
    UIImage * image=[appointment.appointmentToProperty.defaultPicture underlyingImage];
    if (image) {
        
        [self.imgProperty setImage: image];
        
    }else
    {
        [self.imgProperty setImageWithURL:[NSURL URLWithString:appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
    }
    
    NSString *propertyTitle = @"";
    if(!isEmpty(appointment.appointmentToProperty.propertyId)){
        propertyTitle = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort];
    }
    else if(!isEmpty(appointment.appointmentToProperty.blockId)){
        propertyTitle = appointment.appointmentToProperty.blockName;
    }
    else{
        propertyTitle =appointment.appointmentToProperty.schemeName;
    }
    self.lblAddress.text = propertyTitle;
    self.lblReported.text = [appointment stringWithAppointmentStatus:[appointment getStatus]];
    self.lblSurveyor.text = [NSString stringWithFormat:@"%@: %@",
                             LOC(@"KEY_STRING_ASSIGNED_BY"),
                             appointment.createdByPerson];
    self.lblStartDate.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle9];
    self.lblEndDate.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle9];
    self.lblStartTime.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle3];
    self.lblEndTime.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle3];
       
    self.lblName.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
    self.lblAddress.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT );
    self.lblReportedDate.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblReported.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblStarts.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblEnds.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
