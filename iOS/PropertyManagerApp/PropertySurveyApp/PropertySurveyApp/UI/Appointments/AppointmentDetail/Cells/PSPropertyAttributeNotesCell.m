//
//  PSPropertyAttributeNotesCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/9/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "PSPropertyAttributeNotesCell.h"
#import "PropertyAttributesNotes.h"
@implementation PSPropertyAttributeNotesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureCellForNotes:(NSMutableArray*) notesArray{
    for(PropertyAttributesNotes * notes in notesArray){
        NSMutableAttributedString *attributePath = [[NSMutableAttributedString alloc]initWithString:notes.attributePath attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:14]}];
        
        NSAttributedString *attributeNotes = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"\r%@\r",notes.attributeNotes] attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Light" size:14]}];
        
        [attributePath appendAttributedString:attributeNotes];
        NSMutableAttributedString *textViewStr = [[NSMutableAttributedString alloc] initWithAttributedString:_txtView.attributedText];
        [textViewStr appendAttributedString:attributePath];
        [_txtView setAttributedText:textViewStr];
        
    }
}

@end
