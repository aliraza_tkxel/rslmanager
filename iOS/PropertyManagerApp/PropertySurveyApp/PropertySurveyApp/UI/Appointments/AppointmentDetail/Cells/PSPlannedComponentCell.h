//
//  PSPlannedComponentCell.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSPlannedComponentCell : UITableViewCell {
    
}

@property (nonatomic, retain)UILabel* componentTitleLabel;
@property(nonatomic,strong) UILabel * componentValueLabel;

@property (nonatomic, retain)UILabel* pmoTitleNumber;
@property(nonatomic,strong) UILabel * pmoNumber;


-(void) reloadData:(PlannedTradeComponent*) plannedComponent;

+(CGFloat) getHeight:(PlannedTradeComponent*) plannedComponent;

@end
