//
//  PSAccomodationsCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSafeguardCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblSafeguard;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlaceholder1;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlaceholder2;

@end
