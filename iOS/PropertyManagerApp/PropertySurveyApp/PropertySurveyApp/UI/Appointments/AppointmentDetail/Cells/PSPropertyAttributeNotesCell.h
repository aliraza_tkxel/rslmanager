//
//  PSPropertyAttributeNotesCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/9/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSPropertyAttributeNotesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *txtView;
-(void) configureCellForNotes:(NSMutableArray*) notesArray;
@end
