//
//  PSAsbestosCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAsbestosCell.h"

@implementation PSAsbestosCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(id)data :(NSInteger)index
{
	Appointment *appointment = data;
	
	self.lblAsbestos.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblAsbestos.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblAsbestosLocation.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblAsbestosLocation.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	
	if (appointment.appointmentToScheme) {
		[self populateSchemeAsbestosInfo:appointment :index];
	}else
	{
		[self populatePropertyAsbestosInfo:appointment :index];
	}
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void) populateSchemeAsbestosInfo :(Appointment*) appointment :(NSInteger) index
{
	
	NSArray * _asbestosArray = [appointment.appointmentToScheme.schemeToSchemeAsbestosData allObjects];
	if ([_asbestosArray count] > 0)
	{
		SchemeAsbestosData *asbestosData = [_asbestosArray objectAtIndex:index];
		if (!isEmpty(asbestosData))
		{
			NSMutableString *asbestosInfo = [[NSMutableString alloc]init];
			[asbestosInfo appendString:asbestosData.asbRiskLevelDesc];
			
			if([UtilityClass isEmpty:asbestosData.type] == NO)
			{
				[asbestosInfo appendString:[NSString stringWithFormat:@" (%@)",asbestosData.type]];
			}
			
			if([UtilityClass isEmpty:asbestosData.riskLevel] == NO)
			{
				[asbestosInfo appendString:[NSString stringWithFormat:@" - %@",asbestosData.riskLevel]];
			}
			self.lblAsbestos.text = asbestosInfo;
			self.lblAsbestosLocation.text = asbestosData.riskDesc;
		}
	}
	else
	{
		self.lblAsbestos.text = LOC(@"KEY_STRING_NONE");
	}
	
}

-(void) populatePropertyAsbestosInfo :(Appointment*) appointment :(NSInteger) index
{
	
	NSArray * _asbestosArray = [appointment.appointmentToProperty.propertyToPropertyAsbestosData allObjects];
	if ([_asbestosArray count] > 0)
	{
		PropertyAsbestosData *asbestosData = [_asbestosArray objectAtIndex:index];
		if (!isEmpty(asbestosData))
		{
			NSMutableString *asbestosInfo = [[NSMutableString alloc]init];
			[asbestosInfo appendString:asbestosData.asbRiskLevelDesc];
			
			if([UtilityClass isEmpty:asbestosData.type] == NO)
			{
				[asbestosInfo appendString:[NSString stringWithFormat:@" (%@)",asbestosData.type]];
			}
			
			if([UtilityClass isEmpty:asbestosData.riskLevel] == NO)
			{
				[asbestosInfo appendString:[NSString stringWithFormat:@" - %@",asbestosData.riskLevel]];
			}
			self.lblAsbestos.text = asbestosInfo;
			self.lblAsbestosLocation.text = asbestosData.riskDesc;
		}
	}
	else
	{
		self.lblAsbestos.text = LOC(@"KEY_STRING_NONE");
	}
	
}


@end
