//
//  PSAppointmentDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSPlannedAppointmentDetailViewController.h"
#import "PSPropertyCell.h"
#import "PSTypeCell.h"
#import "PSTypeCell.h"
#import "PSAsbestosCell.h"
#import "PSTenantCell.h"
#import "PSJSNumberCell.h"
#import "PSAccomodationsCell.h"
#import "PSAppDelegate.h"
#import "Appointment+CoreDataClass.h"
#import "Customer.h"
#import "PSTenantDetailViewController.h"
#import "PSAccomodationViewController.h"
#import "PSBarButtonItem.h"
#import "PSJobDetailViewController.h"
#import "PSSurveyViewController.h"
#import "PSAppointmentDetailNotesCell.h"
#import "Appointment+SurveyDownload.h"
#import "Survey+JSON.h"
#import "Appointment+JSON.h"
#import "PSCP12InfoViewController.h"
#import "PSFaultNoEntryViewController.h"
#import "PSPlannedJobDetailViewController.h"
#import "PSSynchronizationManager.h"
#define kViewSectionHeaderHeight 20

@interface PSPlannedAppointmentDetailViewController ()
{
    AppointmentType _appointmentType;
    AppointmentStatus _appointmentStatus;
}
@end

@implementation PSPlannedAppointmentDetailViewController

@synthesize headerViewArray;
@synthesize appointment;
@synthesize property;
@synthesize tenantsArray;
@synthesize jobDataListArray;
@synthesize asbestosArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isEditing = NO;
    }
    return self;
}

-(void) loadView
{
    [super loadView];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480) style:(UITableViewStylePlain)];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    [self.view addSubview:self.tableView];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _appointmentType = [self.appointment getType];
    _appointmentStatus = [self.appointment getStatus];
    self.headerViewArray = [NSMutableArray array];
    
    self.tenantsArray = [NSMutableArray arrayWithArray:[appointment.appointmentToCustomer allObjects]];
    self.jobDataListArray =  [NSMutableArray arrayWithArray:[appointment.appointmentToJobSheet allObjects]];
    self.property = appointment.appointmentToProperty;
    self.asbestosArray =  [NSMutableArray arrayWithArray:[property.propertyToPropertyAsbestosData allObjects]];
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self loadNavigationonBarItems];
    [self addTableHeaderView];
    [self loadSectionHeaderViews:@[LOC(@"KEY_STRING_PROPERTY"),LOC(@"KEY_STRING_TYPE"),LOC(@"KEY_STRING_Component"),LOC(@"KEY_STRING_TENANT"),LOC(@"KEY_STRING_ASBESTOS"),@"",@"",LOC(@"KEY_STRING_NOTES")] headerViews:self.headerViewArray];

    [self registerNotifications];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.appointment getType] != AppointmentTypeFault)
    {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.downloadButton, self.trashButton, nil]];
    }
    else
    {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.noEntryButton, nil]];
        if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
            || [self.appointment getStatus] == AppointmentStatusNoEntry)
        {
            [self.noEntryButton setEnabled:NO];
           [self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
        }
        else
        {
            [self.noEntryButton setEnabled:YES];
        }
        
    }

    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [self deRegisterNotifications];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.trashButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                              style:PSBarButtonItemStyleTrash
                                                             target:self
                                                             action:@selector(onClickTrashButton)];
    
    self.downloadButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                 style:PSBarButtonItemStyleDownload
                                                                target:self
                                                                action:@selector(onClickDownloadButton)];
    
    self.saveButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                             style:PSBarButtonItemStyleDefault
                                                            target:self
                                                            action:@selector(onClickSaveButton)];
    self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
                                                             style:PSBarButtonItemStyleDefault
                                                            target:self
                                                            action:@selector(onClickNoEntryButton)];

    
    //Set Download Button
    if ((_appointmentStatus == [UtilityClass completionStatusForAppointmentType:_appointmentType] ||
        (_appointmentStatus == AppointmentStatusNoEntry)) ||
        ([self.appointment isPreparedForOffline]))
    {
        [self.downloadButton setEnabled:NO];
    }
    
    if(_appointmentType == ApplicationTypeStock)
    {
        [self.trashButton setEnabled:YES];
    }
    else
    {
        [self.trashButton setEnabled:NO];
    }
    
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    if ([self.appointment getType] == AppointmentTypeFault || [self.appointment getType] == AppointmentTypePlanned)
    {
        
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.noEntryButton, nil]];
        if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
            || [self.appointment getStatus] == AppointmentStatusNoEntry)
        {
            [self.noEntryButton setEnabled:NO];
        }
        else
        {
            [self.noEntryButton setEnabled:YES];
        }
        
    }
    else
    {
       [self setRightBarButtonItems:[NSArray arrayWithObjects:self.downloadButton, self.trashButton, nil]];
   
    }
    
    [self setTitle:[appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickTrashButton
{
    CLS_LOG(@"onClickTrashButton");
    if(IS_NETWORK_AVAILABLE)
    {
        [self.activityIndicator stopAnimating];
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_DELETE_APPOINTMENT")
                                                       message:LOC(@"KEY_ALERT_CONFIRM_DELETE")
                                                      delegate:self
                                             cancelButtonTitle:LOC(@"KEY_ALERT_YES")
                                             otherButtonTitles:LOC(@"KEY_ALERT_NO"), nil];
        alert.tag = AlertViewTagTrash;
        [alert show];
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                       message:LOC(@"KEY_ALERT_NO_INTERNET")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
    }
}

- (void) onClickDownloadButton
{
    CLS_LOG(@"onClickDownloadButton");
    if(IS_NETWORK_AVAILABLE)
    {
            [self disableNavigationBarButtons];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
			[self showActivityIndicator:LOC(@"KEY_STRING_SURVEY_DOWNLOADING")];
			[self.appointment prepareForOffline];
            [self popOrCloseViewController];
			[self.tableView reloadData];

    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                       message:LOC(@"KEY_ALERT_NO_INTERNET")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
    }
}

- (void) onClickEditButton
{
    CLS_LOG(@"onClickEditButton");
    self.isEditing = YES;
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, self.downloadButton, self.trashButton, nil]];
    [self.tableView reloadData];
}

- (void) onClickSaveButton
{
    self.isEditing = NO;
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.downloadButton, self.trashButton, nil]];
    [self.tableView reloadData];
}

- (void) onClickNoEntryButton
{
    CLS_LOG(@"onClickNoEntryButton");
    PSFaultNoEntryViewController *faultNoEntryViewConroller = [[PSFaultNoEntryViewController alloc] initWithNibName:@"PSFaultNoEntryViewController" bundle:[NSBundle mainBundle]];
    [faultNoEntryViewConroller setAppointment:self.appointment];
    
    [self.navigationController pushViewController:faultNoEntryViewConroller animated:YES];
}

#pragma mark - IBActions
- (IBAction)onClickStartAppoiontmentButton:(id)sender
{
    [self.appointment startAppointment];
    [self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
}
- (IBAction)onClickAcceptAppoiontmentButton:(id)sender
{
    [self.appointment acceptAppointment];
    [_btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
    [_btnStartAppointment removeTarget:self action:@selector(onClickAcceptAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    [_btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    CLS_LOG(@"Appointment ID: %@", self.appointment.appointmentId);
    if (alertView.tag == AlertViewTagTrash)
    {
       	if(buttonIndex == alertView.cancelButtonIndex)
        {
            //Delete Appointment
            [self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
                                         @"Deleting",
                                         @"Appointment"]];
            [self disableNavigationBarButtons];
            [[PSAppointmentsManager sharedManager] deleteAppointment:self.appointment];
            [self schduleUIControlsActivation];
            CLS_LOG(@"Delete Appointment");
        }
    }
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return kNumberOfSections;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    /*
     The count check ensures that if array is empty, i.e no record exists
     for particular section, the header height is set to zero so that
     section header is not alloted any space.
     */
    
    CGFloat headerHeight = 0.0;
    
    if(section == kSectionJSNumber)
    {
        if ([self.jobDataListArray count] == 0)
        {
            headerHeight = 0;
        }
        else
        {
            headerHeight = 1;
        }
    }
    else if (section == kSectionAccomodation)
    {
        headerHeight = 1;
    }
    else if (section == kSectionAsbestos)
    {
        if ([self.asbestosArray count] > 0)
        {
            headerHeight = kViewSectionHeaderHeight;
        }
    }
    
    else if (section == kSectionTenant)
    {
        if([self.tenantsArray count] > 0)
        {
            headerHeight = kViewSectionHeaderHeight;
        }
    }
    
    else if (section == kSectionNotes)
    {
        headerHeight = kViewSectionHeaderHeight;
    }
    else
    {
        headerHeight = kViewSectionHeaderHeight;
    }
    return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0.0;
    if(indexPath.section == kSectionProperty)
    {
        
        rowHeight = kPropertyCellHeight;
    }
    
    else if (indexPath.section == kSectionType)
    {
        rowHeight = kTypeCellHeight;
        
    }
    
    else if (indexPath.section == kSectionTenant)
    {
        rowHeight = kTenantCellHeight;
    }
    
    else if (indexPath.section == kSectionAsbestos)
    {
        rowHeight = kAsbestosCellHeight;
    }
    
    else if (indexPath.section == kSectionJSNumber)
    {
        rowHeight = kJSNumberCellHeight;
    }
    
    else if (indexPath.section == kSectionAccomodation)
    {
        if ([self.appointment getType] != AppointmentTypeFault )
        {
            rowHeight = kAccomodationCellHeight;
        }
        
    }
    
    else if (indexPath.section == kSectionNotes)
    {
        if (!isEmpty(self.appointment.appointmentNotes))
        {
            rowHeight = [self heightForTextView:nil containingString:self.appointment.appointmentNotes];
            
            if (rowHeight < 50)
            {
                rowHeight = 50;
            }
        }
        
        else
        {
            rowHeight = 50;
        }
        
        
    }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger numberOfRows=0;
    
    if (section == kSectionProperty)
    {
        numberOfRows = kSectionPropertyRows;
    }
    
    else if (section == kSectionType)
    {
        numberOfRows = kSectionTypeRows;
    }
    
    else if (section == kSectionTenant)
    {
        numberOfRows = [self.tenantsArray count];
    }
    
    else if (section == kSectionAsbestos)
    {
        numberOfRows = [self.asbestosArray count];
    }
    
    else if (section == kSectionJSNumber)
    {
        
        numberOfRows = [self.jobDataListArray count];
    }
    
    else if (section == kSectionAccomodation)
        
    {
        if ([self.appointment getType] != AppointmentTypeFault)
        {
            numberOfRows = 1;
        }
    }
    
    else if (section == kSectionNotes)
    {
        numberOfRows = 1;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *_cell = nil;
    
    if (indexPath.section == kSectionProperty)
    {
        static NSString *propertyCellIdentifier = @"PropertyCellIdentifier";
        PSPropertyCell *cell = [self.tableView dequeueReusableCellWithIdentifier:propertyCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSPropertyCell" owner:self options:nil];
            cell = self.propertyCell;
            self.propertyCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    else if (indexPath.section == kSectionComponent)
    {
        static NSString *typeCellIdentifier = @"TypeCellIdentifier";
        PSTypeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:typeCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSTypeCell" owner:self options:nil];
            cell = self.typeCell;
            self.typeCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    else if (indexPath.section == kSectionType)
    {
        static NSString *typeCellIdentifier = @"TypeCellIdentifier";
        PSTypeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:typeCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSTypeCell" owner:self options:nil];
            cell = self.typeCell;
            self.typeCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    else if (indexPath.section == kSectionTenant)
    {
        static NSString *tenantCellIdentifier = @"TenantCellIdentifier";
        PSTenantCell *cell = (PSTenantCell *)[self.tableView dequeueReusableCellWithIdentifier:tenantCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSTenantCell" owner:self options:nil];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell = self.tenantCell;
            self.tenantCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    else if (indexPath.section == kSectionAsbestos)
    {
        static NSString *asbestosCellIdentifier = @"AsbestosCellIdentifier";
        PSAsbestosCell *cell = [self.tableView dequeueReusableCellWithIdentifier:asbestosCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAsbestosCell" owner:self options:nil];
            cell = self.asbestosCell;
            self.asbestosCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    else if (indexPath.section == kSectionJSNumber)
    {
        static NSString *JSNumberCellIdentifier = @"JSNumberCellIdentifier";
        PSJSNumberCell *cell = [self.tableView dequeueReusableCellWithIdentifier:JSNumberCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSJSNumberCell" owner:self options:nil];
            cell = self.jsNumberCell;
            self.jsNumberCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    else if (indexPath.section == kSectionAccomodation)
    {
        static NSString *accomodationCellIdentifier = @"AccomodationCellIdentifier";
        PSAccomodationsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:accomodationCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAccomodationsCell" owner:self options:nil];
            cell = self.accomodationCell;
            self.accomodationCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleGray;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    }
    
    else if (indexPath.section == kSectionNotes)
    {
        static NSString *notesCellIdentifier = @"notesCellIdentifier";
        PSAppointmentDetailNotesCell *cell = [self.tableView dequeueReusableCellWithIdentifier:notesCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAppointmentDetailNotesCell" owner:self options:nil];
            cell = self.notesCell;
            self.notesCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        
    }
    return _cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSPropertyCell class]])
    {
        
        PSPropertyCell *_cell = (PSPropertyCell *) *cell;
        
        Customer *customer = [self.appointment defaultTenant];

        if (!isEmpty(customer))
        {
            _cell.lblName.text = [customer fullName];
        }
        
        
        [_cell reloadData:self.property];
        
        if([appointment.appointmentType compare:kAppointmentTypeGas] == NSOrderedSame || [appointment.appointmentType compare:kAppointmentTypeGasVoid] == NSOrderedSame)
        {
            _cell.lblExpiryDate.hidden = NO;
            _cell.lblExpiryTitle.hidden = NO;
        }
        else
        {
            _cell.lblExpiryDate.hidden = YES;
            _cell.lblExpiryTitle.hidden = YES;
        }
        
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSTypeCell class]])
    {
        PSTypeCell *_cell = (PSTypeCell *) *cell;
        _cell.lblDetail.text = appointment.appointmentType;
        _cell.lblDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblDetail.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]] && [appointment getType] != AppointmentTypeGas && [appointment getType] != AppointmentTypeGasVoid)
        {
            [_cell.imgStatus setHidden:NO];
        }
        else
        {
            [_cell.imgStatus setHidden:YES];
        }
				if ([self.appointment.appointmentType isEqualToString:kAppointmentTypeGas] || [self.appointment.appointmentType isEqualToString:kAppointmentTypeGasVoid])
        {
            if (([appointment isPreparedForOffline] && [appointment getStatus] != AppointmentStatusNotStarted) || [self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
            {
                [_cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            else
            {
                [_cell setAccessoryType:UITableViewCellAccessoryNone];
            }

        }
        
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSTenantCell class]])
    {
        PSTenantCell *_cell = (PSTenantCell *) *cell;
        
        self.customer= [self.tenantsArray objectAtIndex:indexPath.row];
        
        NSString *customerName = [self.customer fullName];

        if(!isEmpty(self.customer.telephone)){
            
            NSMutableString *telephone = [NSMutableString stringWithString:LOC(@"KEY_STRING_TEL")];
            [telephone appendString:@". "];
            [telephone appendString:self.customer.telephone];
            _cell.lblTelephone.text = telephone;
            _cell.lblTelephone.hidden = NO;
             _cell.btnTelephone.hidden = NO;
            _cell.btnTelephone.tag = indexPath.row;
            
            [_cell.lblTelephone setFrame:CGRectMake(43, 39, 203, 20)];
            [_cell.btnTelephone setFrame:CGRectMake(12, 37, 24, 24)];
            [_cell.lblMobile setFrame:CGRectMake(43, 67, 203, 20)];
            [_cell.btnMobile setFrame:CGRectMake(12, 65, 24, 24)];
            
        }
        
        else {
            _cell.lblTelephone.hidden = YES;
            _cell.btnTelephone.hidden = YES;
            [_cell.lblMobile setFrame:_cell.lblTelephone.frame];
            [_cell.btnMobile setFrame:_cell.btnTelephone.frame];
        }
        
        if (!isEmpty(self.customer.mobile) && ![self.customer.mobile isEqualToString:@"(null)"]) {
            
            NSMutableString *mobile = [NSMutableString stringWithString:LOC(@"KEY_STRING_MOB")];
            [mobile appendString:@". "];
            [mobile appendString:self.customer.mobile];
            _cell.lblMobile.text = mobile;
            _cell.lblMobile.hidden = NO;
            _cell.btnMobile.hidden = NO;
            _cell.btnMobile.tag = indexPath.row;
        }
        
        else {
            _cell.lblMobile.hidden = YES;
            _cell.btnMobile.hidden = YES;
        }
        _cell.lblTenantName.text = customerName;
        _cell.lblTenantName.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
        _cell.lblMobile.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblTelephone.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblTenantName.highlightedTextColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
        _cell.lblMobile.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblTelephone.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);

        [_cell.btnEditTenant addTarget:self action:@selector(onClickEditTenantButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnTelephone addTarget:self action:@selector(onClickTelephoneButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnMobile addTarget:self action:@selector(onClickMobileButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnDisclosureIndicator addTarget:self action:@selector(onClickDisclosureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnEditTenant setTag:indexPath.row];
        [_cell.btnDisclosureIndicator setTag:indexPath.row];
        
        if (self.isEditing)
        {
            [_cell.btnEditTenant setHidden:NO];
        }
        
        else
        {
            [_cell.btnEditTenant setHidden:YES];
        }
        
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSAsbestosCell class]])
    {
        PSAsbestosCell *_cell = (PSAsbestosCell *) *cell;
			  [_cell configureCell:self.appointment :indexPath.row];
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSJSNumberCell class]])
    {
        PSJSNumberCell *_cell = (PSJSNumberCell *) *cell;
        FaultJobSheet *jobData = [self.jobDataListArray objectAtIndex:indexPath.row];
        
        if ([jobData.jobStatus isEqualToString:kJobStatusPaused])
        {
            [_cell.imgStatus setHidden:NO];
            _cell.imgStatus.image = [UIImage imageNamed:@"btn_Pause"];
        }
        else if ([jobData.jobStatus isEqualToString:kJobStatusComplete] || [jobData.jobStatus isEqualToString:kJobStatusNoEntry])
        {
            [_cell.imgStatus setHidden:NO];
            _cell.imgStatus.image = [UIImage imageNamed:@"icon_completed"];
        }
        else if ([jobData.jobStatus isEqualToString:kJobStatusInProgress])
        {
            [_cell.imgStatus setHidden:NO];
            _cell.imgStatus.image = [UIImage imageNamed:@"btn_Play"];
        }
        else if ([jobData.jobStatus isEqualToString:kJobStatusNotStarted] || [jobData.jobStatus isEqualToString:kJobStatusAccepted])
        {
            [_cell.imgStatus setHidden:YES];
        }
        if([self.appointment getStatus] == AppointmentStatusInProgress )
        {
            [_cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        
        else
        {
            [_cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        _cell.lblTitle.text = jobData.jsNumber;
        
        _cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.lblTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        UIView* backgroundView = [ [ UIView alloc ] initWithFrame:CGRectZero ];
        backgroundView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);//UIColorFromRGB(240, 241, 241);
        
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSAccomodationsCell class]])
    {
        PSAccomodationsCell *_cell = (PSAccomodationsCell *) *cell;
        _cell.lblAccommodation.text = LOC(@"KEY_STRING_ACCOMMODATIONS");
        _cell.lblAccommodation.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.lblAccommodation.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        NSMutableArray *accomodations = [NSMutableArray arrayWithArray:[self.appointment.appointmentToProperty.propertyToAccomodation allObjects]];
        
        if (!isEmpty(accomodations))
        {
            _cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        else
        {
            _cell.accessoryType = UITableViewCellAccessoryNone;
        }
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSAppointmentDetailNotesCell class]])
    {
        PSAppointmentDetailNotesCell *_cell = (PSAppointmentDetailNotesCell *) *cell;
        _cell.txtViewNotes.textAlignment = NSTextAlignmentJustified;
        _cell.txtViewNotes.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        [_cell.txtViewNotes setScrollEnabled:NO];
        CGRect seperatorRect = _cell.imgSeperator.frame;
        seperatorRect.size.height = 1;
        CGRect textViewRect = _cell.txtViewNotes.frame;
        
        
        if (!isEmpty(self.appointment.appointmentNotes)) {
            textViewRect.size.height = [self heightForTextView:nil containingString:self.appointment.appointmentNotes];
            if (textViewRect.size.height < 49) {
                textViewRect.size.height = 49;
            }
        }
        else
        {
            textViewRect.size.height = 49;
        }
        [_cell.txtViewNotes setFrame:textViewRect];
        
        if (!isEmpty(self.appointment.appointmentNotes)) {
            seperatorRect.origin.y = [self heightForTextView:nil containingString:self.appointment.appointmentNotes];
            if (seperatorRect.origin.y < 49) {
                seperatorRect.origin.y = 49;
            }
        }
        else
        {
            seperatorRect.origin.y = 49;
        }
        
      // seperatorRect.origin.y = [self heightForTextView:nil containingString:self.appointment.appointmentNotes];
        [_cell.imgSeperator setHighlighted:NO];
        [_cell.imgSeperator setFrame:seperatorRect];
        
        if (!isEmpty(self.appointment.appointmentNotes)) {
            _cell.txtViewNotes.text = self.appointment.appointmentNotes;
        }
        else
        {
            _cell.txtViewNotes.text = LOC(@"KEY_STRING_NONE");
        }
        
        *cell = _cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == kSectionType)
    {
        NSMutableDictionary *surveyJSON = [self.appointment.appointmentToSurvey.surveyJSON JSONValue];
        if ([self.appointment.appointmentType isEqualToString:kAppointmentTypeGas] ||[self.appointment.appointmentType isEqualToString:kAppointmentTypeGasVoid])
        {
            if (([appointment isPreparedForOffline] && [appointment getStatus] != AppointmentStatusNotStarted && [self.appointment getStatus] != AppointmentStatusAccepted)|| [self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
            {
                PSCP12InfoViewController *cp12InfoViewController = [[PSCP12InfoViewController alloc] initWithNibName:@"PSCP12InfoViewController" bundle:[NSBundle mainBundle]];
                [cp12InfoViewController setAppointment:self.appointment];
                [self.navigationController pushViewController:cp12InfoViewController animated:YES];
            }
            else
            {
                if ([appointment isPreparedForOffline]  && [self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
                {
                    
                    
                    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                                   message:LOC(@"KEY_STRING_APPOINTMENT_NOT_STARTED")
                                                                  delegate:nil
                                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                         otherButtonTitles:nil,nil];
                    [alert show];
                }
                else
                {
                    if ([self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
                    {
                        
                        
                        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                                       message:LOC(@"KEY_STRING_SURVEY_NOT_DOWNLOADED")
                                                                      delegate:nil
                                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                             otherButtonTitles:nil,nil];
                        [alert show];
                        
                    }
                }
            }
        }
    }
  	else if (indexPath.section == kSectionTenant)
    {
    	//Do Nothing
    }
    else if (indexPath.section == kSectionJSNumber)
    {
			if([self.appointment getStatus] != AppointmentStatusNotStarted)
			{
				NSArray *jobAsbestosArray = [self.appointment.appointmentToProperty.propertyToPropertyAsbestosData allObjects];
				if (self.appointment.appointmentToPlannedComponent)
				{
					PSPlannedJobDetailViewController *jobDetailDetailViewConroller = [[PSPlannedJobDetailViewController alloc] init];
					[jobDetailDetailViewConroller setPlannedComponent:self.appointment.appointmentToPlannedComponent];
					[jobDetailDetailViewConroller setJobAsbestosArray:jobAsbestosArray];
					[self.navigationController pushViewController:jobDetailDetailViewConroller animated:YES];
				}
				else
				{
					PSJobDetailViewController *jobDetailDetailViewConroller = [[PSJobDetailViewController alloc] initWithNibName:@"PSJobDetailViewController" bundle:[NSBundle mainBundle]];
					[jobDetailDetailViewConroller setJobData:[self.jobDataListArray objectAtIndex:indexPath.row]];
					[jobDetailDetailViewConroller setJobAsbestosArray:jobAsbestosArray];
					[self.navigationController pushViewController:jobDetailDetailViewConroller animated:YES];
				}
			}
    }
    else if (indexPath.section == kSectionAccomodation)
		{
			NSMutableArray *accomodations = [NSMutableArray arrayWithArray:[self.appointment.appointmentToProperty.propertyToAccomodation allObjects]];
			if (!isEmpty(accomodations))
			{
				self.accomodationlViewConroller = [[PSAccomodationViewController alloc] initWithNibName:@"PSAccomodationViewController" bundle:[NSBundle mainBundle]];
				[self.accomodationlViewConroller setAccomodationsArray:accomodations];
				[self.accomodationlViewConroller setEditing:self.isEditing];
				[self.navigationController pushViewController:self.accomodationlViewConroller animated:YES];
			}
    }
}

#pragma mark - Edit Tenant Button Selector
- (IBAction)onClickEditTenantButton:(id)sender
{
    CLS_LOG(@"onClickEditAddressButton");
    
    UIButton *btnEditTenant = (UIButton *) sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnEditTenant.tag];
    NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
    if (customer != nil) {
        PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:@"PSTenantDetailViewController" bundle:[NSBundle mainBundle]];
        [tenantDetailViewConroller setAddress:address];
        [tenantDetailViewConroller setTenant:customer];
        [tenantDetailViewConroller setEditing:YES];
        [self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
    }
}

- (IBAction)onClickDisclosureButton:(id)sender
{
    UIButton *btnDisclosure = (UIButton *) sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnDisclosure.tag];
    NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
    if (customer != nil)
    {
        PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:@"PSTenantDetailViewController" bundle:[NSBundle mainBundle]];
        [tenantDetailViewConroller setAddress:address];
        [tenantDetailViewConroller setTenant:customer];
        [self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
    }
    
}

- (IBAction)onClickTelephoneButton:(id)sender
{
    UIButton *btnTel = (UIButton *)sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnTel.tag];
    [self setupDirectCallToNumber:customer.telephone];
}

- (IBAction)onClickMobileButton:(id)sender
{
   // [self setupDirectCallToNumber:self.customer.mobile];
    UIButton *btnMob = (UIButton *)sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnMob.tag];
    [self setupDirectCallToNumber:customer.mobile];
}

- (void) setupDirectCallToNumber:(NSString *)number
{
    if (!isEmpty(number))
    {
        number = [UtilityClass getValidNumber:number];
        CLS_LOG(@"Direct Call Number Verified: %@", number);
        NSString *url = [@"tel:" stringByAppendingString:number];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

#pragma mark - Core Data Update Events
- (void) onAppointmentObjectUpdate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self isViewLoaded] && self.view.window)
        {
            [self.tableView reloadData];
        }
    });
}

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentStartNotificationReceive) name:kAppointmentStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSurveyFormSaveSuccessNotificationReceive) name:kSurveyFormSaveSucessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSurveyFormSaveFailureNotificationReceive) name:kSurveyFormSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentDataRemovalSuccessNotificationReceive) name:kAppointmentsDataRemoveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentDataRemovalFailureNotificationReceive) name:kAppointmentsDataRemoveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppliancesSaveSuccessNotificationReceive) name:kSaveAppliacnesSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppliancesSaveFailureNotificationReceive) name:kSaveAppliacnesFailureNotification object:nil];
    
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSurveyFormSaveSucessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSurveyFormSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsDataRemoveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsDataRemoveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveAppliacnesSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveAppliacnesFailureNotification object:nil];
}

- (void) onAppointmentStartNotificationReceive
{
    
}

- (void) onSurveyFormSaveSuccessNotificationReceive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
}

- (void) onSurveyFormSaveFailureNotificationReceive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
}

- (void) onAppointmentDataRemovalSuccessNotificationReceive
{
    
   /* if (!isEmpty(self.appointment.appointmentEventIdentifier)) {
        
        [[PSEventsManager sharedManager] removeEventWithIdentifier:self.appointment.appointmentEventIdentifier completion:^(bool success, NSError *error) {
        }];
    }*/
    [self enableNavigationBarButtons];
    [[PSDataUpdateManager sharedManager] deleteAppointment:self.appointment];
    [[PSDatabaseContext sharedContext] saveContext];
    [self hideActivityIndicator];
    [self popOrCloseViewController];
}

- (void) onAppointmentDataRemovalFailureNotificationReceive
{
    //KEY_ALERT_DELETE_APPOINTMENT_FAILIURE
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_DELETE_APPOINTMENT")
                                                   message:LOC(@"KEY_ALERT_DELETE_APPOINTMENT_FAILIURE")
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
    [self enableNavigationBarButtons];
    [self hideActivityIndicator];
}
- (void) onAppliancesSaveSuccessNotificationReceive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self enableNavigationBarButtons];
    [self hideActivityIndicator];
}


- (void) onAppliancesSaveFailureNotificationReceive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self enableNavigationBarButtons];
    [self hideActivityIndicator];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
    float horizontalPadding = 0;
    float verticalPadding = 8;
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                //[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
                                [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
                                nil];
    CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(300, 999999.0f)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:attributes
                                               context:nil];
    CGFloat height = boundingRect.size.height + verticalPadding;
    
#else
    CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
                        constrainedToSize:CGSizeMake(300, 999999.0f)
                            lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
    
    return height;
}

#pragma mark - Methods

- (void) disableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = NO;
    }
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = NO;
    }
}

- (void) enableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = YES;
    }
    
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = YES;
    }
}

- (void) addTableHeaderView
{
    CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
    UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
    headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.frame = CGRectMake(8, 0, 245, 29);
    lbl.center = CGPointMake(lbl.center.x, headerView.center.y);
    lbl.backgroundColor = [UIColor clearColor];
    [lbl setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lbl setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lbl setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
    lbl.text =LOC(@"KEY_STRING_APPOINTMENT_START_MESSAGE");
    
    _btnStartAppointment = [[UIButton alloc]init];
    _btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
    _btnStartAppointment.center = CGPointMake(_btnStartAppointment.center.x, headerView.center.y);
    //[btnStartAppointment setTitle:LOC(@"KEY_STRING_ACCEPT") forState:UIControlStateNormal];
    [_btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
    [_btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:
     UIControlStateNormal];
    
    [headerView addSubview:lbl];
    [headerView addSubview:_btnStartAppointment];
    if([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame){
        [_btnStartAppointment setTitle:LOC(@"KEY_STRING_ACCEPT") forState:UIControlStateNormal];
        [_btnStartAppointment addTarget:self action:@selector(onClickAcceptAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if([self.appointment.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame){
        [_btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
        [_btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame || [self.appointment.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame)
    {
        headerViewRect = self.tableView.tableHeaderView.frame;
        headerViewRect.size.height = 40;
        headerViewRect.size.width = 320;
        [self.tableView.tableHeaderView setFrame:headerViewRect];
        [self.tableView setTableHeaderView:headerView];
    }
}

- (void) hideTableHeaderView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.5];
    [self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
    self.tableView.tableHeaderView = nil;
    [self.tableView.tableHeaderView setHidden:YES];
    [UIView commitAnimations];
    [self.tableView reloadData];
//#warning Commented the following line to fix Jira issue# 115
    //[self loadNavigationonBarItems];
}

@end
