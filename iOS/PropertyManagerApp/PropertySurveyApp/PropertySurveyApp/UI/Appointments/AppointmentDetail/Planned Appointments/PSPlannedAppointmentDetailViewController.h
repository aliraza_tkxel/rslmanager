//
//  PSAppointmentDetailViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSPropertyCell;
@class PSTypeCell;
@class PSTenantCell;
@class PSAsbestosCell;
@class PSJSNumberCell;
@class PSAccomodationsCell;
@class Appointment;
@class PSBarButtonItem;
@class PSAccomodationViewController;
@class PSAppointmentDetailNotesCell;

#define kNumberOfSections 8

#define kSectionProperty 0
#define kSectionComponent 1
#define kSectionType 2
#define kSectionTenant 3
#define kSectionJSNumber 4
#define kSectionAsbestos 5
#define kSectionAccomodation 6
#define kSectionNotes 7

#define kSectionPropertyRows 1
#define kPropertyCellRow 0

#define kSectionTypeRows 1
#define kTypeCellRow 0

#define kTenantCellRow 2

#define kPropertyCellHeight 80
#define kTypeCellHeight 29
#define kTenantCellHeight 98
#define kAsbestosCellHeight 48
#define kJSNumberCellHeight 65
#define kAccomodationCellHeight 39
#define kNotesCellHeight 150

#define kDownloadSurveyAlertView 1

@interface PSPlannedAppointmentDetailViewController : PSCustomViewController

@property (weak,   nonatomic) Appointment *appointment;
@property (weak,   nonatomic) Property *property;
@property (weak,   nonatomic) IBOutlet PSPropertyCell *propertyCell;
@property (weak,   nonatomic) IBOutlet PSTypeCell *typeCell;
@property (weak,   nonatomic) IBOutlet PSTenantCell *tenantCell;
@property (weak,   nonatomic) IBOutlet PSAsbestosCell *asbestosCell;
@property (weak,   nonatomic) IBOutlet PSJSNumberCell *jsNumberCell;
@property (weak,   nonatomic) IBOutlet PSAccomodationsCell *accomodationCell;
@property (weak,   nonatomic) IBOutlet PSAppointmentDetailNotesCell *notesCell;
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) NSMutableArray *tenantsArray;
@property (strong, nonatomic) NSMutableArray *jobDataListArray;
@property (strong, nonatomic) NSMutableArray *asbestosArray;
@property (strong, nonatomic) UIButton *btnStartAppointment;
@property (strong, nonatomic) PSBarButtonItem *trashButton;
@property (strong, nonatomic) PSBarButtonItem *downloadButton;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong, nonatomic) PSBarButtonItem *noEntryButton;
@property (strong, nonatomic) PSAccomodationViewController *accomodationlViewConroller;
@property (strong, nonatomic) Customer *customer;

- (void) onAppointmentObjectUpdate;

- (void) addTableHeaderView;

- (void) loadNavigationonBarItems;

@end
