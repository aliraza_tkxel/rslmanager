//
//  PSOldAppfilterViewCell.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 10/02/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsFilterViewController.h"
@interface PSOldAppfilterViewCell : UITableViewCell
{
    UILabel * _labelInAction;
    UIView * _BGPickerView;
}

@property (strong, nonatomic) IBOutlet UIButton *btnFrom;
@property (strong, nonatomic) IBOutlet UIButton *btnTo;
@property (strong, nonatomic) IBOutlet UILabel *lblFrom;
@property (strong, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) id<PSAppointmentsFilterProtocol>  delegate;
+(CGFloat) getHeight;
@end
