//
//  PSCheckBoxCell.h
//  PSAOffline
//
//  Created by My Mac on 08/08/2013.
//  Copyright (c) 2013 My Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsFilterViewController.h"
@interface PSCheckBoxCell : UITableViewCell{
    
}

@property (assign, nonatomic, setter=setCheckBoxSelected:) BOOL isCheckBoxSelected;
@property (weak,   nonatomic) id<PSAppointmentsFilterOptionsProtocol> delegate;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) IBOutlet UILabel *setCellLabel;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckBox;

- (IBAction)submitAction:(id)sender;

- (void) makeSelected;
- (void) makeDeselected;
@property (weak, nonatomic) IBOutlet UILabel *labelOutlet;

@end
