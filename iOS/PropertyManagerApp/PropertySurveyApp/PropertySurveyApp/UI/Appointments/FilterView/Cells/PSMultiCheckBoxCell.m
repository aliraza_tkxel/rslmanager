//
//  PSMultiCheckBoxCell.m
//  PSAOffline
//
//  Created by My Mac on 08/08/2013.
//  Copyright (c) 2013 My Mac. All rights reserved.
//

#import "PSMultiCheckBoxCell.h"

static NSString *checkButtonActive = @"checkbox_marked";

@implementation PSMultiCheckBoxCell
@synthesize btnCollectionOutlet;
@synthesize buttonIndex;
@synthesize labelCollectionOutlet;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
		{
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



- (IBAction)submitAction:(id)sender{
    
    UIButton* button = (UIButton*) sender;
    buttonIndex = button.tag;
    CLS_LOG(@"%i", buttonIndex);
    
    if ([button.currentImage isEqual:[UIImage imageNamed:@"checkbox_marked"]])
    {
        [self makeDeselected];
        if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didDeselectFilterOption:indexPath:)])
        {
            [(PSAppointmentsFilterViewController *)self.delegate didDeselectFilterOption:button.tag indexPath:self.indexPath];
        }
    }
    else
    {
        [self makeSelected];
        if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectFilterOption:indexPath:)])
        {
            [(PSAppointmentsFilterViewController *)self.delegate didSelectFilterOption:button.tag indexPath:self.indexPath];
        }
    }
}

- (void) makeSelected
{
    self.isCheckBoxSelected = YES;
    [[self.btnCollectionOutlet objectAtIndex:buttonIndex -5 ] setImage:[UIImage imageNamed:@"checkbox_marked"] forState:UIControlStateNormal]; /*-5 because we have to minus the buttons which do not apply to "Show only" section*/
    
    [[self.labelCollectionOutlet objectAtIndex:buttonIndex -5] setFont:[UIFont boldSystemFontOfSize:15.0F]];
    
}

- (void) makeDeselected
{
    self.isCheckBoxSelected = NO;
    [[self.btnCollectionOutlet objectAtIndex:buttonIndex  -5] setImage:[UIImage imageNamed:@"checkbox_unmark"] forState:UIControlStateNormal];
    [[self.labelCollectionOutlet objectAtIndex:buttonIndex -5] setFont:[UIFont systemFontOfSize:15.0F]];
    
}

+(CGFloat) getHeight
{
    return 271;
}

@end
