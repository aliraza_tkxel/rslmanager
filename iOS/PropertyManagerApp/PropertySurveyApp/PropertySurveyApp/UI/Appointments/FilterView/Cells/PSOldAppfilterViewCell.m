//
//  PSOldAppfilterViewCell.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 10/02/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSOldAppfilterViewCell.h"
#define kDateFormatOldAppointmentFetchStyle @"dd-MMM-yyyy"
@implementation PSOldAppfilterViewCell
@synthesize lblTo,lblFrom,btnFrom,btnTo,delegate;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)selectFromDate:(id)sender
{
    _labelInAction = self.lblFrom;
    [self showPickerView];
}

-(IBAction)selectToDate:(id)sender
{
    _labelInAction = self.lblTo;
    [self showPickerView];
}

-(IBAction)loadAppointmentsClicked:(id)sender
{
    
    NSDate * startDate = [UtilityClass dateFromString:lblFrom.text dateFormat:kDateFormatOldAppointmentFetchStyle];
    NSDate * endDate = [UtilityClass dateFromString:lblTo.text dateFormat:kDateFormatOldAppointmentFetchStyle];
    
    
    if(startDate && endDate)
    {
        if([self.delegate respondsToSelector:@selector(loadInprogressOldAppointmentsFromDate:toDate:)])
        {
       
            [self.delegate loadInprogressOldAppointmentsFromDate:startDate toDate:endDate];
        }
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:APP_NAME_STRING message:LOC(@"KEY_STRING_TO_FROM_VALID_VALUES") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

-(void)showPickerView
{
    //Date Settings
    NSDate * date01 =[UtilityClass dateFromString:_labelInAction.text dateFormat:kDateFormatOldAppointmentFetchStyle];
    if(!date01)
    {
        date01 = [NSDate date];
    }
    _labelInAction.text = [UtilityClass stringFromDate:date01 dateFormat:kDateFormatOldAppointmentFetchStyle];
    
    //BGview
    PSAppDelegate * deleg = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
    CGRect bounds = deleg.window.bounds;
    _BGPickerView = [[UIView alloc] initWithFrame:bounds];
    _BGPickerView.backgroundColor = CLEAR_COLOR;
    
    //Picker
    CGRect pickerframe = CGRectMake(0, 0, bounds.size.width-60, 168);
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:pickerframe];
    datePicker.backgroundColor = [UIColor whiteColor];
    [datePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [_BGPickerView addSubview:datePicker];
    
    //closeButton
    UIButton * closeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    closeBtn.frame = CGRectMake(bounds.size.width/2-30, bounds.size.height, 60, 30);
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"btn_view.png"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closePopUp) forControlEvents:UIControlEventTouchUpInside];
    [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
    closeBtn.titleLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:15];
    [closeBtn setTitleColor:WHITE_COLOR forState:UIControlStateNormal];
    [_BGPickerView addSubview:closeBtn];
    
    //Animation Slidein
    [UIView beginAnimations:@"slideIn" context:nil];
    [datePicker setCenter:CGPointMake(bounds.size.width/2,bounds.size.height/2)];
    [closeBtn setCenter:CGPointMake(bounds.size.width/2, bounds.size.height/2+109)];
    [UIView commitAnimations];
    [deleg.window addSubview:_BGPickerView];
    datePicker.date = date01;
}

- (void)pickerChanged:(id)sender
{
    NSDate * date = [sender date];
    _labelInAction.text = [UtilityClass stringFromDate:date dateFormat:kDateFormatOldAppointmentFetchStyle];
}

-(void)closePopUp
{
    [_BGPickerView removeFromSuperview];
    _BGPickerView=nil;
}
+(CGFloat) getHeight
{
    return 109;
}

@end
