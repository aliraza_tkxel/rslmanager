//
//  PSMultiCheckBoxCell.h
//  PSAOffline
//
//  Created by My Mac on 08/08/2013.
//  Copyright (c) 2013 My Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsFilterViewController.h"

@interface PSMultiCheckBoxCell : UITableViewCell

@property (assign, nonatomic, setter=setCheckBoxSelected:) BOOL isCheckBoxSelected;
@property (weak,   nonatomic) id<PSAppointmentsFilterOptionsProtocol> delegate;
@property (strong, nonatomic) NSIndexPath *indexPath;

/*Collection Outlets must be connected in the same order that they are defined in the FilterOptionEnum and FilterOption Array.*/

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnCollectionOutlet;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labelCollectionOutlet;
@property NSInteger buttonIndex;


@property (weak, nonatomic) IBOutlet UIButton *btnStockOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnPrePostOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnGasOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnVoidOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnInProgressOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnArrangedOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnCompletedOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnFaultOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnDefectOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnPlannedOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnMiscellaneousOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnAdaptationsOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnConditionRatingToolOutlet;

@property (weak, nonatomic) IBOutlet UIButton *btnAlternativeOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnOilOutlet;






- (IBAction)submitAction:(id)sender;

+(CGFloat) getHeight;
- (void) makeSelected;
- (void) makeDeselected;


@end
