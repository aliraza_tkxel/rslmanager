//
//  PSAppointmentsFilterViewController.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsViewController.h"

@class PSCheckBoxCell;
@class PSMultiCheckBoxCell;
@class PSOldAppfilterViewCell;

@protocol PSAppointmentsFilterOptionsProtocol <NSObject>
@required
/*!
 @discussion
 Method didFilterOptionSelectAtIndexPath Called upon selection of any filter option.
 */
- (void) didSelectFilterOption:(PSFilterOption)filterOption indexPath:(NSIndexPath *)indexPath;
/*!
 @discussion
 Method didFilterOptionDeselectAtIndexPath Called upon deselection of any filter option.
 */
- (void) didDeselectFilterOption:(PSFilterOption)filterOption indexPath:(NSIndexPath *)indexPath;
@end


@interface PSAppointmentsFilterViewController : UIViewController <PSAppointmentsFilterOptionsProtocol>
@property (strong, nonatomic) IBOutlet UILabel *lblAppVersion;

@property (weak,    nonatomic) id<PSAppointmentsFilterProtocol> delegate;
@property (weak,    nonatomic) IBOutlet UILabel *topBarOutlet;
@property (strong,  nonatomic) IBOutlet UIButton *btnViewAppointments;
@property (strong,  nonatomic) IBOutlet UITableView *tblAppointmentsFilter;
@property (strong,  nonatomic) IBOutlet PSCheckBoxCell *checkBoxCell;
@property (strong,  nonatomic) IBOutlet PSMultiCheckBoxCell *multiCheckBoxCell;
@property (strong,  nonatomic) IBOutlet PSOldAppfilterViewCell *oldAppointmentFetchCell;
@property (strong,  nonatomic) IBOutlet UIView *headerBGOutlet;
@property (weak,    nonatomic) IBOutlet UILabel *lblResultsFound;
@property (weak,    nonatomic) IBOutlet UILabel *lblFetchedResultsCount;

@property (strong,  nonatomic) NSArray *viewSectionLabels;
@property (strong,  nonatomic) NSArray *sortBySectionLabels;
@property (strong,  nonatomic) NSMutableArray *headerViewArray;

@property (strong,nonatomic) NSMutableArray *filterOptionsArray;

- (IBAction) onClickViewAppointmentsButton:(id)sender;
@end
