//
//  PSAppointmetnsFilterViewController.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppointmentsFilterViewController.h"
#import "PSCheckBoxCell.h"
#import "PSMultiCheckBoxCell.h"
#import "PSOldAppfilterViewCell.h"


@interface PSAppointmentsFilterViewController ()

@end

#define kNumberOfSections 3
#define kHeightForHeaderView 39

#define kSectionView 0
#define kSectionSortyBy 1
#define kSectionShowOnly 2


#define kSectionViewRows 3
#define kViewAll 0
#define kViewToday 1
#define kViewNextFivedays 2

#define kSectionSortByRows 2
#define kSortByName 0
#define kSortByDate 1

#define kSectionShowOnlyRows 1
#define kShowOnlyOptions 0

#define kSectionOldAppointments 4  
#define kSectionOldAppointmentRows 1
#define kSectionOldAppointmentFilling 0


@implementation PSAppointmentsFilterViewController

@synthesize tblAppointmentsFilter;
@synthesize checkBoxCell;
@synthesize multiCheckBoxCell,oldAppointmentFetchCell;
@synthesize btnViewAppointments;
@synthesize delegate;
@synthesize topBarOutlet;
@synthesize lblResultsFound;
@synthesize lblFetchedResultsCount;
@synthesize headerBGOutlet;
@synthesize headerViewArray;
@synthesize filterOptionsArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.headerViewArray = [NSMutableArray array];
	self.filterOptionsArray = [NSMutableArray arrayWithArray:[SettingsClass sharedObject].filterOptions];
	self.viewSectionLabels = [[NSArray alloc] initWithObjects:
	                          LOC(@"KEY_STRING_VIEW_ALL"), LOC(@"KEY_STRING_TODAY"),
	                          LOC(@"KEY_STRING_VIEW_NEXT_FIVE_DAYS"), nil];
    

    self.sortBySectionLabels=[[NSArray alloc] initWithObjects:
                              LOC(@"KEY_STRING_SORT_BY_NAME"),
                              LOC(@"KEY_STRING_SORT_BY_DATE"), nil];
    
    [self.tblAppointmentsFilter setBackgroundColor:UIColorFromHex(FILTER_TABLEVIEW_BG_COLOUR)];
    [self.topBarOutlet setBackgroundColor:UIColorFromHex(FILTER_TABLEVIEW_TOP_HEADER_BG_COLOUR)];
    [self.lblResultsFound setTextColor:UIColorFromHex(FILTER_TABLEVIEW_HEADER_TEXT_COLOUR)];
    [self.headerBGOutlet setBackgroundColor:UIColorFromHex(FILTER_TABLEVIEW_BG_COLOUR)];
    self.tblAppointmentsFilter.separatorColor = UIColorFromHex(FILTER_TABLEVIEW_SEPERATOR_COLOUR);
    
	[self.btnViewAppointments setTitle:LOC(@"KEY_STRING_SHOW") forState:UIControlStateNormal];
    
    [self loadSectionHeader];
    NSString * appVersion = [UtilityClass getAPPVersionString];
       self.lblAppVersion.text = appVersion;
    self.lblAppVersion.backgroundColor = UIColorFromHex(FILTER_TABLEVIEW_HEADER_COLOUR);
       //Do Not Hide the Results Labels.
    lblResultsFound.hidden = NO;
    lblFetchedResultsCount.hidden = YES;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   

}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	[self setTblAppointmentsFilter:nil];
	[self setTopBarOutlet:nil];
	[self setLblResultsFound:nil];
	[self setLblFetchedResultsCount:nil];
	[self setHeaderBGOutlet:nil];
}

#pragma mark - IBAction
- (IBAction)onClickViewAppointmentsButton:(id)sender
{
	if (self.delegate && [(NSObject *)self.delegate respondsToSelector : @selector(filterAppointmentsWithOptions:)])
	{
		[(PSAppointmentsViewController *)self.delegate filterAppointmentsWithOptions : self.filterOptionsArray];
	}
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return kNumberOfSections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return kHeightForHeaderView;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 38;
	if (indexPath.section == kSectionView || indexPath.section == kSectionSortyBy)
	{
		height = 38;
	}
	else if (indexPath.section == kSectionShowOnly)
	{
		height = [PSMultiCheckBoxCell getHeight];
	}
	else if (indexPath.section == kSectionOldAppointments)
	{
			height = [PSOldAppfilterViewCell getHeight];
	}
	return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	int rows = 0;
	if (section ==  kSectionView)
	{
		rows = kSectionViewRows;
	}
	else if (section == kSectionSortyBy)
	{
		rows = kSectionSortByRows;
	}
	else if (section == kSectionOldAppointments)
	{
		rows = kSectionOldAppointmentRows;
	}
	else if(section == kSectionShowOnly)
	{
		rows = kSectionShowOnlyRows;
	}
	else
	{
		rows = 0;
	}
	return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *checkBocCellIdentifier = @"checkBocCellIdentifier";
	static NSString *multiCheckBoxCellIdentifier = @"multiCheckBoxCellIdentifier";
	static NSString *oldAppointmentloadIdentifier = @"oldAppointmentloadIdentifier";
	UITableViewCell *_cell = nil;
	if (indexPath.section == kSectionView || indexPath.section == kSectionSortyBy)
	{
		PSCheckBoxCell *cell = [self.tblAppointmentsFilter dequeueReusableCellWithIdentifier:checkBocCellIdentifier];
		if (cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSCheckBoxCell" owner:self options:nil];
			cell = self.checkBoxCell;
			self.checkBoxCell = nil;
		}
        
		if (indexPath.section == kSectionView) {
			cell.setCellLabel.text = [self.viewSectionLabels objectAtIndex:indexPath.row];
            
			if (indexPath.row == kViewAll) {
				cell.tag = PSFilterOptionViewAll;
				if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionViewAll] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:cell.tag]]) {
					[cell makeSelected];
				}
			}
            
			else if (indexPath.row == kViewToday) {
				cell.tag = PSFilterOptionViewToday;
				if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionViewToday] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:cell.tag]]) {
					[cell makeSelected];
				}
			}
            
			else if (indexPath.row == kViewNextFivedays) {
				cell.tag = PSFilterOptionViewNextFiveDays;
				if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionViewNextFiveDays] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:cell.tag]]) {
					[cell makeSelected];
				}
			}
		}
		else if (indexPath.section == kSectionSortyBy) {
			cell.setCellLabel.text = [self.sortBySectionLabels objectAtIndex:indexPath.row];
            
			if (indexPath.row == kSortByName) {
				cell.tag = PSFilterOptionSortByName;
				if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionSortByName] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:cell.tag]]) {
					[cell makeSelected];
				}
			}
            
			else if (indexPath.row == kSortByDate) {
				cell.tag = PSFilterOptionSortByDate;
				if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionSortByDate] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:cell.tag]]) {
					[cell makeSelected];
				}
			}
		}
        
		cell.delegate = self;
		_cell = cell;
	}
	else if (indexPath.section == kSectionShowOnly) {
		PSMultiCheckBoxCell *cell = [self.tblAppointmentsFilter dequeueReusableCellWithIdentifier:multiCheckBoxCellIdentifier];
		if (cell == nil) {
			[[NSBundle mainBundle] loadNibNamed:@"PSMultiCheckBoxCell" owner:self options:nil];
			cell = self.multiCheckBoxCell;
			self.multiCheckBoxCell = nil;
		}
        
		cell.btnInProgressOutlet.tag = PSFilterOptionShowOnlyInProgress;
		cell.btnArrangedOutlet.tag = PSFilterOptionShowOnlyArranged;
		cell.btnGasOutlet.tag = PSFilterOptionShowOnlyGas;
		cell.btnVoidOutlet.tag = PSFilterOptionShowOnlyVoid;
		cell.btnFaultOutlet.tag = PSFilterOptionShowOnlyFault;
		cell.btnPlannedOutlet.tag = PSFilterOptionShowOnlyPlanned;
		cell.btnAdaptationsOutlet.tag = PSFilterOptionShowOnlyAdaptations;
		cell.btnMiscellaneousOutlet.tag = PSFilterOptionShowOnlyMiscellaneous;
		cell.btnDefectOutlet.tag = PSFilterOptionShowOnlyDefect;
        cell.btnOilOutlet.tag = PSFilterOptionShowOnlyOil;
        cell.btnAlternativeOutlet.tag = PSFilterOptionShowOnlyAlternative;
		
		cell.btnConditionRatingToolOutlet.tag = PSFilterOptionShowOnlyConditionRatingTool;
		
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyInProgress] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyInProgress]])
		{
			cell.buttonIndex = cell.btnInProgressOutlet.tag;
			[cell makeSelected];
		}
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyArranged] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyArranged]])
		{
			cell.buttonIndex = cell.btnArrangedOutlet.tag;
			[cell makeSelected];
		}
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyGas] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyGas]])
		{
			cell.buttonIndex = cell.btnGasOutlet.tag;
			[cell makeSelected];
		}
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyVoid] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyVoid]])
		{
			cell.buttonIndex = cell.btnVoidOutlet.tag;
			[cell makeSelected];
		}
        
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyFault] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyFault]])
		{
			cell.buttonIndex = cell.btnFaultOutlet.tag;
			[cell makeSelected];
		}
        
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyPlanned] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyPlanned]])
		{
			cell.buttonIndex = cell.btnPlannedOutlet.tag;
			[cell makeSelected];
		}
        
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyAdaptations] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyAdaptations]])
		{
			cell.buttonIndex = cell.btnAdaptationsOutlet.tag;
			[cell makeSelected];
		}
        
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyDefect] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyDefect]])
		{
			cell.buttonIndex = cell.btnDefectOutlet.tag;
			[cell makeSelected];
		}
		
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyMiscellaneous] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyMiscellaneous]])
		{
			cell.buttonIndex = cell.btnMiscellaneousOutlet.tag;
			[cell makeSelected];
		}
        
		if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyConditionRatingTool] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyConditionRatingTool]])
		{
			cell.buttonIndex = cell.btnConditionRatingToolOutlet.tag;
			[cell makeSelected];
		}
        
        if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyAlternative] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyAlternative]])
        {
            cell.buttonIndex = cell.btnAlternativeOutlet.tag;
            [cell makeSelected];
        }
        
        if ([[SettingsClass sharedObject] isFilterOptionSelected:PSFilterOptionShowOnlyOil] || [self.filterOptionsArray containsObject:[NSNumber numberWithInt:PSFilterOptionShowOnlyOil]])
        {
            cell.buttonIndex = cell.btnOilOutlet.tag;
            [cell makeSelected];
        }

        
		cell.delegate = self;
		_cell = cell;
	}
	else if(indexPath.section == kSectionOldAppointments)
	{
			PSOldAppfilterViewCell *cell = [self.tblAppointmentsFilter dequeueReusableCellWithIdentifier:oldAppointmentloadIdentifier];
			if (cell == nil) {
					[[NSBundle mainBundle] loadNibNamed:@"PSOldAppfilterViewCell" owner:self options:nil];
					cell = self.oldAppointmentFetchCell;
			}
			self.oldAppointmentFetchCell = nil;
			cell.delegate = self.delegate;
			_cell = cell;
	}
	// Configure the cell...
	[_cell setBackgroundColor:[UIColor clearColor]];
	[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return _cell;
}


#pragma mark - PSAppointmentsFilterOptionsProtocol
- (void)didSelectFilterOption:(PSFilterOption)filterOption indexPath:(NSIndexPath *)indexPath {
	// CLS_LOG(@"didSelectFilterOption: %d", filterOption);
    
    
	NSNumber *filter = [NSNumber numberWithInt:filterOption];
	if (![self.filterOptionsArray containsObject:filter]) {
		[self.filterOptionsArray addObject:filter];
	}
    
	[SettingsClass sharedObject].filterOptions = [NSArray arrayWithArray:filterOptionsArray];
	[[SettingsClass sharedObject] saveSettings];
}

- (void)didDeselectFilterOption:(PSFilterOption)filterOption indexPath:(NSIndexPath *)indexPath {
	// CLS_LOG(@"didDeselectFilterOption: %d", filterOption);
    
	NSNumber *filter = [NSNumber numberWithInt:filterOption];
    
	if ([self.filterOptionsArray containsObject:filter]) {
		[self.filterOptionsArray removeObject:filter];
	}
	[SettingsClass sharedObject].filterOptions = [NSArray arrayWithArray:filterOptionsArray];
	[[SettingsClass sharedObject] saveSettings];
}

- (void)loadSectionHeader
{
	for (int headerSectionNumber = 0; headerSectionNumber < kNumberOfSections; headerSectionNumber++)
	{
		UILabel *label = [[UILabel alloc] init];
		label.frame = CGRectMake(40, 0, 100, 39);
        
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor whiteColor];
		[label setTextColor:UIColorFromHex(FILTER_TABLEVIEW_HEADER_TEXT_COLOUR)];
        
		label.shadowOffset = CGSizeMake(10.0, 1.0);
		label.font = [UIFont boldSystemFontOfSize:16];
        
		CGRect imageRect; // = CGRectMake(10, 3, 34, 40);
		UIImageView *headerImage = [[UIImageView alloc] init];
		UIImage *image = nil;
        
		if (headerSectionNumber == kSectionView)
		{
			imageRect = CGRectMake(10, 3, 21, 13);
			image = [UIImage imageNamed:@"icon_view"];
			label.text = LOC(@"KEY_STRING_VIEW");
		}
		else if (headerSectionNumber == kSectionSortyBy)
		{
			label.text = LOC(@"KEY_STRING_SORTED_BY");
			image = [UIImage imageNamed:@"icon_sort"];
			imageRect = CGRectMake(10, 3, 17, 17);
		}
		else if (headerSectionNumber == kSectionShowOnly)
		{
			imageRect = CGRectMake(10, 3, 21, 13);
			image = [UIImage imageNamed:@"icon_only_show"];
			label.text = LOC(@"KEY_STRING_ONLY_SHOW");
		}
		else if (headerSectionNumber == kSectionOldAppointments)
		{
			imageRect = CGRectMake(10, 3, 21, 13);
			image = [UIImage imageNamed:@"icon_view"];
			label.text = LOC(@"KEY_STRING_LOAD_OLD_APPOINTMENTS");
		}
		
		UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 46)];
		[view setBackgroundColor:UIColorFromHex(FILTER_TABLEVIEW_HEADER_COLOUR)];
		[label setCenter:CGPointMake(label.center.x, view.center.y)];
        
		if (headerSectionNumber == kSectionView)
		{
			UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
			lineView.backgroundColor = UIColorFromHex(FILTER_LINE_SEPERATOR_COLOUR);
			[self.view addSubview:lineView];
			[view addSubview:lineView];
		}
        
		[headerImage setImage:image];
		[headerImage setFrame:imageRect];
		[headerImage setCenter:CGPointMake(headerImage.center.x, view.center.y)];
		[view addSubview:headerImage];
		[view addSubview:label];
		[view setAlpha:1.0F];
        
		[self.headerViewArray addObject:view];
	}
}

@end
