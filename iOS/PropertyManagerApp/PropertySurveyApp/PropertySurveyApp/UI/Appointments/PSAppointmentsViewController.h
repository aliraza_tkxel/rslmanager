//
//  PSAppointmentsViewController.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "PSCustomViewController.h"
#import "PSMapsViewController.h"
#import "PSTableView.h"
#import "PSSynchronizationManager.h"

@class PSAppointmentsFilterViewController;
@class PSAppointmentDetailViewController;
@class PSFaultSchemeAppointmentCell;
@class PSMiscSchemeAppointmentCell;
@class PSAdaptSchemeAppointmentCell;
@class PSFaultAppointmentCell;
@class PSPlannedAppointmentCell;
@class PSAppointmentCell;
@class PSAdaptationsAppointmentCell;
@class PSMiscellaneousAppointmentCell;
@class PSConditionAppointmentCell;
@class PSDefectAppointmentCell;
@class PSGasServicingAppointmentTableViewCell;
@class PSAlternateFuelTableViewCell;
@class PSOilAppointmentTableViewCell;

@protocol PSAppointmentsFilterProtocol <NSObject>
@required
/*!
 @discussion
 Method "filterAppointmentsWithOptions" Called upon tapping view button. Provides the filterOptions.
 */
- (void) filterAppointmentsWithOptions:(NSArray *)filterOptions;
@required
/*!
 @discussion
 Method "loadInprogressOldAppointmentsFromDate" Called upon tapping "Load Appointment" button. Adds up old in progress appointments in the AppointmentsView
 */
-(void)loadInprogressOldAppointmentsFromDate:(NSDate*)from toDate:(NSDate*)to;

@end

@protocol PSAppointmentsAddressMapsProtocol <NSObject>
@required
/*!
 @discussion
 Method "addressTapped" Called upon tapping address button.
 */
- (void) addressTapped:(Appointment *)appointment;
@end



@interface PSAppointmentsViewController : PSCustomViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, PSAppointmentsFilterProtocol, EGORefreshTableHeaderDelegate,PSAppointmentsAddressMapsProtocol,PSSynchronizationManagerDelegate>
{
    PSAppointmentsFilterViewController *filterViewController;
    BOOL isSearching;
}
@property (strong,  nonatomic) NSPredicate *appointmentFilterPredicate;
@property (strong,  nonatomic) NSFetchedResultsController*fetchedResultsController;
@property (strong,  nonatomic) IBOutlet PSAppointmentCell *appointmentCell;
@property (strong,  nonatomic) IBOutlet PSGasServicingAppointmentTableViewCell *gasServicingCell;

@property (strong, nonatomic) IBOutlet PSAlternateFuelTableViewCell *alternativeServicingCell;

@property (strong, nonatomic) IBOutlet PSOilAppointmentTableViewCell *oilServicingCell;

@property (strong,  nonatomic) IBOutlet PSFaultAppointmentCell *faultAppointmentCell;
@property (strong,  nonatomic) IBOutlet PSPlannedAppointmentCell *plannedAppointmentCell;
@property (strong,  nonatomic) IBOutlet PSAdaptationsAppointmentCell *adaptationsAppointmentCell;
@property (strong,  nonatomic) IBOutlet PSFaultSchemeAppointmentCell *faultSchemeCell;
@property (strong,  nonatomic) IBOutlet PSMiscSchemeAppointmentCell *miscSchemeCell;
@property (strong,  nonatomic) IBOutlet PSAdaptSchemeAppointmentCell *adaptSchemeCell;
@property (strong,  nonatomic) IBOutlet PSMiscellaneousAppointmentCell *miscellaneousAppointmentCell;
@property (strong,  nonatomic) IBOutlet PSConditionAppointmentCell *conditionAppointmentCell;
@property (strong,  nonatomic) IBOutlet PSDefectAppointmentCell *defectAppointmentCell;

@property (strong,  nonatomic) IBOutlet PSTableView *tblAppointments;
@property (strong,  nonatomic) PSAppointmentDetailViewController *appointmentDetailViewConroller;
@property (strong, nonatomic) IBOutlet UIView *noAppointmentsHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *noAppointmentsLabel;
@property (nonatomic) BOOL isAnyAppointmentDownloading;

@end
