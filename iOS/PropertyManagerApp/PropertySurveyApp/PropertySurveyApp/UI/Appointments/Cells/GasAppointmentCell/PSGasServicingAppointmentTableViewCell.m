//
//  PSGasServicingAppointmentTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 09/05/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSGasServicingAppointmentTableViewCell.h"
@interface PSGasServicingAppointmentTableViewCell ()
{
    BOOL isProgressViewVisible;
    BOOL isAnimating;
}

- (void)showProgressView:(BOOL)animated;
- (void)hideProgressView:(BOOL)animated;

@end

#define MAX_PROGRESS_VALUE 100.0
#define ANIMATION_DURATION 0.3
#define ANIMATION_DELAY 0
@implementation PSGasServicingAppointmentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (void)animationTest {
    [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(testMethod) userInfo:nil repeats:YES];
}

- (void)testMethod {
    [self updateProgess:self.downloadProgressBarView.progress * MAX_PROGRESS_VALUE + 0.01];
}

- (void)updateProgess:(CGFloat)progress {
    if (!isProgressViewVisible) {
        [self showProgressView:YES];
    }
    
    self.downloadProgressBarView.progress = progress / MAX_PROGRESS_VALUE;
    
    /*if (progress * MAX_PROGRESS_VALUE == MAX_PROGRESS_VALUE) {
     [self hideProgressView:YES];
     }*/
}

- (BOOL)view:(UIView *)view containsView:(UIView *)subview {
    for (UIView *_subView in[self subviews]) {
        if ([_subView isEqual:subview]) {
            return YES;
        }
    }
    return NO;
}

- (void)showProgressView:(BOOL)show animated:(BOOL)animated
{
    if (show)
    {
        [self showProgressView:animated];
    }
    else
    {
        [self hideProgressView:animated];
    }
}

- (void)showProgressView:(BOOL)animated {
    @synchronized(self)
    {
        if (!isAnimating && !isProgressViewVisible) {
            isAnimating = TRUE;
            [UIView animateWithDuration:(animated) ? ANIMATION_DURATION:0 delay:ANIMATION_DELAY options:UIViewAnimationOptionCurveEaseIn animations: ^{
                CGRect progressViewFrame = self.progressView.frame;
                CGRect contentViewFrame = self.detailView.frame;
                
                CGFloat x = contentViewFrame.origin.x;
                contentViewFrame.origin.x = progressViewFrame.size.width;
                progressViewFrame.origin.x = x;
                [self.detailView setFrame:contentViewFrame];
                [self.progressView setFrame:progressViewFrame];
            } completion: ^(BOOL finished) {
                isProgressViewVisible = YES;
                isAnimating = FALSE;
            }];
        }
    }
}

- (void)hideProgressView:(BOOL)animated {
    @synchronized(self)
    {
        if (!isAnimating && isProgressViewVisible) {
            isAnimating = TRUE;
            [UIView animateWithDuration:(isProgressViewVisible && animated) ? ANIMATION_DURATION:0 delay:ANIMATION_DELAY options:UIViewAnimationOptionCurveEaseOut animations: ^{
                CGRect progressViewFrame = self.progressView.frame;
                CGRect contentViewFrame = self.detailView.frame;
                
                contentViewFrame.origin.x = 0;
                [self.detailView setFrame:contentViewFrame];
                
                progressViewFrame.origin.x = -progressViewFrame.size.width;
                [self.progressView setFrame:progressViewFrame];
            } completion: ^(BOOL finished) {
                isProgressViewVisible = NO;
                isAnimating = FALSE;
            }];
        }
    }
}

-(void) reloadData:(Appointment*)appointment
{
    [self setAppointment:appointment];
    [self.btnMapView addTarget:self action:@selector(loadMapView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnFailedReason setHidden:isEmpty(self.appointment.failedReason)];
    
    self.lblAppointmentTitle.text = self.appointment.appointmentTitle;
    self.lblAppointmentTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.lblAppointmentTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    
    //Setting label Address dynamic size
    NSString * addressText = @"";
    
    if(!isEmpty(appointment.appointmentToProperty.propertyId)){
        addressText = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleAddress2Onwards];
    }
    else if(!isEmpty(appointment.appointmentToProperty.blockId)){
        addressText = appointment.appointmentToProperty.blockName;
    }
    else{
        addressText = appointment.appointmentToProperty.schemeName;
    }
    
    
    self.lblAddress.text = addressText;
    
    
    NSString * idForAppointmentDestination = @"";
    if(!isEmpty(appointment.appointmentToProperty.propertyId)){
        idForAppointmentDestination = @"Property";
    }
    else if(!isEmpty(appointment.appointmentToProperty.blockId)){
        idForAppointmentDestination = @"Block";
    }
    else{
        idForAppointmentDestination = @"Scheme";
    }
    
    _lblSchemeBlockPropertyIdentifier.text = idForAppointmentDestination;
    
    NSInteger lblAddressWidth = [self.lblAddress.text sizeWithFont:self.lblAddress.font].width + 1;
    NSInteger max_address_width = 245;
    if (lblAddressWidth > max_address_width) {
        lblAddressWidth = max_address_width;
    }
    CGRect labelFrame = self.lblAddress.frame;
    labelFrame.size.width = lblAddressWidth;
    [self.lblAddress setFrame:labelFrame];
    
    labelFrame = self.lblAddress.frame;
    labelFrame.origin.x += labelFrame.size.width+4;
    labelFrame.size.width = 7;
    labelFrame.size.height = 12;
    [self.iconAddress setFrame:labelFrame];
    
    [self setUpIcons];
}

-(void)setUpIcons
{
    self.iconCustomerNotes.image = [UIImage imageNamed:kCustomerNotesImageName];
    self.iconAsbestos.image = [UIImage imageNamed:kAsbestosImageName];
    self.iconRiskValunarability.image = [UIImage imageNamed:kRiskImageName];
    
    self.iconRiskValunarability.hidden = YES;
    self.iconAsbestos.hidden = YES;
    self.iconCustomerNotes.hidden = YES;
    
    NSSet * customers = self.appointment.appointmentToCustomer;
    NSSet * asbesos = nil;
    if (self.appointment.appointmentToScheme) {
        asbesos = self.appointment.appointmentToScheme.schemeToSchemeAsbestosData;
    }else
    {
        asbesos = self.appointment.appointmentToProperty.propertyToPropertyAsbestosData;
    }
    
    
    if(customers.count>0)
    {
        for(Customer * customer in customers)
        {
            if(!isEmpty(customer.customerToCustomerRiskData) || !isEmpty(customer.customerToCustomerVulunarityData))
            {
                self.iconRiskValunarability.hidden = NO;
                break;
            }
        }
    }
    
    if(asbesos.count>0)
    {
        //Checking for Previous image Shown and changing locations
        if(self.iconRiskValunarability.hidden)
        {
            self.iconRiskValunarability.image = self.iconAsbestos.image;
            self.iconRiskValunarability.hidden=NO;
        }
        else
        {
            self.iconAsbestos.hidden = NO;
        }
    }
    if(!isEmpty(self.appointment.appointmentNotes))
    {
        //Setting positions of icons 2
        if(self.iconRiskValunarability.hidden && self.iconAsbestos.hidden)
        {
            self.iconRiskValunarability.image = self.iconCustomerNotes.image;
            self.iconRiskValunarability.hidden=NO;
        }
        else if(self.iconAsbestos.hidden && !self.iconRiskValunarability.hidden)
        {
            self.iconAsbestos.image = self.iconCustomerNotes.image;
            self.iconAsbestos.hidden=NO;
        }
        else
        {
            self.iconCustomerNotes.hidden=NO;
        }
    }
    [self configurePropertyNotesIcon];
    
}


-(void) loadMapView {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(addressTapped:)]) {
        [self.delegate addressTapped:self.appointment];
    }
}

-(void) configurePropertyNotesIcon{
    
    self.imgViewPropertyNotesIcon.hidden = YES;
    
    if([self.appointment getType]==AppointmentTypeGas){
        
        if(self.appointment.appointmentToPropertyAttributesNotes!=nil && ![self.appointment.appointmentToPropertyAttributesNotes isKindOfClass:[NSNull class]]){
            
            if([self.appointment.appointmentToPropertyAttributesNotes count]>0){
                
                self.imgViewPropertyNotesIcon.hidden = NO;
            }
        }
    }
    
    
}


#pragma mark - IBActions Methods
- (IBAction)viewFailedReason:(id)sender {
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                   message:self.appointment.failedReason
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
}


@end
