//
//  PSMiscSchemeAppointmentCell.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 23/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsViewController.h"

@interface PSMiscSchemeAppointmentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblReported;
@property (strong, nonatomic) IBOutlet UILabel *lblSurveyor;
@property (strong, nonatomic) IBOutlet UILabel *lblStarts;
@property (strong, nonatomic) IBOutlet UILabel *lblEnds;
@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEndTime;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppointmentStatus;
@property (strong, nonatomic) IBOutlet FBRImageView *imgAppointmentType;
@property (strong, nonatomic) IBOutlet UIImageView *iconAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (strong, nonatomic) IBOutlet UILabel *lblBlockName;
@property (weak, nonatomic) IBOutlet UIButton *btnMapView;
@property (weak, nonatomic) IBOutlet UIButton *btnFailedReason;
- (IBAction)viewFailedReason:(id)sender;



@property (strong,   nonatomic) Appointment *appointment;
@property (weak,    nonatomic) id<PSAppointmentsAddressMapsProtocol> delegate;

-(void) reloadData:(Appointment*)appointment;
@end
