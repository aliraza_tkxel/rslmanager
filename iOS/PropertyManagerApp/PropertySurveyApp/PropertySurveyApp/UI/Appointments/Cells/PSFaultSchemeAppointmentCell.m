//
//  PSFaultSchemeAppointmentCell.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 23/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSFaultSchemeAppointmentCell.h"
#import "Scheme+JSON.h"

@implementation PSFaultSchemeAppointmentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    
    if (self)
    {
    }
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) reloadData:(Appointment*)appointment
{
    [self setAppointment:appointment];

    self.lblReported.text = [appointment stringWithAppointmentStatus:[appointment getStatus]];
    self.lblSurveyor.text = [NSString stringWithFormat:@"%@: %@",
                             LOC(@"KEY_STRING_ASSIGNED_BY"),
                             appointment.createdByPerson];
    self.lblStartDate.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle9];
    self.lblEndDate.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle9];
    self.lblStartTime.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle3];
    self.lblEndTime.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle3];
    self.lblBlockName.text = isEmpty(appointment.appointmentToScheme.blockName)?@"N/A":appointment.appointmentToScheme.blockName;
    self.lblSchemeName.text = appointment.appointmentToScheme.schemeName;
    [self.btnFailedReason setHidden:isEmpty(self.appointment.failedReason)];
    
    self.lblSchemeName.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.lblBlockName.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.lblReported.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblStarts.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblEnds.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);

    CGRect labelFrame = self.lblReported.frame;    
    [self.btnFailedReason setHidden:isEmpty(self.appointment.failedReason)];
        //repositioning
    for (FaultJobSheet * jobData in appointment.appointmentToJobSheet)
    {
        UILabel *JSNLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        JSNLabel.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        JSNLabel.text = [self stringForJSNLabel:jobData];
        JSNLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:13.0];
        JSNLabel.backgroundColor = [UIColor clearColor];
        [JSNLabel setFrame:labelFrame];
        [self addSubview:JSNLabel];
        labelFrame.origin.y += 14;
    }
    //14 offset already added
    labelFrame.origin.y += 4;
    [self.lblReported setFrame:labelFrame];
}

#pragma mark - Private Methods

-(NSString *) stringForJSNLabel:(FaultJobSheet *)jobData
{
    NSString *JSNString = [NSString stringWithFormat:@"%@: %@",jobData.jsNumber,jobData.priority];
    return JSNString;
}

-(IBAction)loadMapView:(id)sender
{
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(addressTapped:)]) {
        [self.delegate addressTapped:self.appointment];
    }
}

#pragma mark - IBActions Methods
- (IBAction)viewFailedReason:(id)sender {
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                   message:self.appointment.failedReason
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
}


@end
