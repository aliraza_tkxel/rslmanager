//
//  PSPlannedAppointmentCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 15/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSPlannedAppointmentCell.h"

@implementation PSPlannedAppointmentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) reloadData:(Appointment*)appointment
{
    [self setAppointment:appointment];
    [self.btnMapView addTarget:self action:@selector(loadMapView) forControlEvents:UIControlEventTouchUpInside];
    NSString * addressText = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleAddress2Onwards];
    self.lblAddress.text = addressText;
    self.lblReported.text = [appointment stringWithAppointmentStatus:[appointment getStatus]];
    self.lblSurveyor.text = [NSString stringWithFormat:@"%@: %@",
                             LOC(@"KEY_STRING_ASSIGNED_BY"),
                             appointment.createdByPerson];
    self.lblStartDate.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle9];
    self.lblEndDate.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle9];
    self.lblStartTime.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle3];
    self.lblEndTime.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle3];
    
    self.lblAppointmentTitle.text = appointment.appointmentTitle;
    self.lblPMO.text = [self stringForPMOLabel:appointment.appointmentToPlannedComponent];
    [self.btnFailedReason setHidden:isEmpty(self.appointment.failedReason)];

    self.lblPMO.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblAppointmentTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.lblAppointmentTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.lblReported.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblReported.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblStarts.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblEnds.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    
    CGRect labelFrame = self.lblPMO.frame;
    labelFrame.size.width = self.lblReported.frame.size.width;
    labelFrame.origin.y += 18;
    [self.lblReported setFrame:labelFrame];
    
    //Setting label Address dynamic size
    float lblAddressWidth =
    [addressText
     boundingRectWithSize:self.lblAddress.frame.size
     options:NSStringDrawingUsesLineFragmentOrigin
     attributes:@{ NSFontAttributeName:self.lblAddress.font }
     context:nil]
    .size.width;
    
    labelFrame = self.lblAddress.frame;
    labelFrame.size.width = lblAddressWidth;
    [self.lblAddress setFrame:labelFrame];
    
    labelFrame = self.lblAddress.frame;
    labelFrame.origin.x += labelFrame.size.width+4;
    labelFrame.size.width = 7;
    labelFrame.size.height = 12;
    [self.iconAddress setFrame:labelFrame];
    [self setUpIcons];
}

-(void)setUpIcons
{
    self.iconCustomerNotes.image = [UIImage imageNamed:kCustomerNotesImageName];
    self.iconAsbestos.image = [UIImage imageNamed:kAsbestosImageName];
    self.iconRiskValunarability.image = [UIImage imageNamed:kRiskImageName];
    self.iconRiskValunarability.hidden = YES;
    self.iconAsbestos.hidden = YES;
    self.iconCustomerNotes.hidden = YES;
    
    NSSet * customers = self.appointment.appointmentToCustomer;
    NSSet * asbesos = self.appointment.appointmentToProperty.propertyToPropertyAsbestosData;
    
    if(customers.count>0)
    {
        for(Customer * customer in customers)
        {
            if(!isEmpty(customer.customerToCustomerRiskData) || !isEmpty(customer.customerToCustomerVulunarityData))
            {
                self.iconRiskValunarability.hidden = NO;
                break;
            }
        }
    }
    
    if(asbesos.count>0)
    {
        //Checking for Previous image Shown and changing locations
        if(self.iconRiskValunarability.hidden)
        {
            self.iconRiskValunarability.image = self.iconAsbestos.image;
            self.iconRiskValunarability.hidden=NO;
        }
        else
        {
            self.iconAsbestos.hidden = NO;
        }
    }
    if(!isEmpty(self.appointment.appointmentNotes))
    {
        //Setting positions of icons 2
        if(self.iconRiskValunarability.hidden && self.iconAsbestos.hidden)
        {
            self.iconRiskValunarability.image = self.iconCustomerNotes.image;
            self.iconRiskValunarability.hidden=NO;
        }
        else if(self.iconAsbestos.hidden && !self.iconRiskValunarability.hidden)
        {
            self.iconAsbestos.image = self.iconCustomerNotes.image;
            self.iconAsbestos.hidden=NO;
        }
        else
        {
            self.iconCustomerNotes.hidden=NO;
        }
    }
}

#pragma mark - Private Methods

-(NSString *) stringForPMOLabel:(PlannedTradeComponent *)plannedComponent
{
    
    NSString *componentName = plannedComponent.componentName;
    NSString *pmoDesc = plannedComponent.pmoDescription;
    if (isEmpty(componentName)) {
        componentName = @"";
    }
    
    if (isEmpty(pmoDesc)) {
        pmoDesc = @"";
    }

    
    NSString *JSNString = [NSString stringWithFormat:@"%@: %@", pmoDesc, componentName];
    return JSNString;
}

-(void) loadMapView {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(addressTapped:)]) {
        [self.delegate addressTapped:self.appointment];
    }
}

#pragma mark - IBActions Methods
- (IBAction)viewFailedReason:(id)sender {
    
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                   message:self.appointment.failedReason
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
}


@end
