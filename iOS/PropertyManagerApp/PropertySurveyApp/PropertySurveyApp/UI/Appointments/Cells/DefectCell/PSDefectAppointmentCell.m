  //
  //  PSPreVoidInspectionCell.m
  //  PropertySurveyApp
  //
  //  Created by aqib javed on 20/06/2015.
  //  Copyright (c) 2015 TkXel. All rights reserved.
  //

#import "PSDefectAppointmentCell.h"
#import "DefectJobSheet+CoreDataClass.h"

@implementation PSDefectAppointmentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
  self=[super initWithCoder:aDecoder];
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
  [super setSelected:selected animated:animated];
}

#pragma mark - Reload Methods
-(void) reloadData:(Appointment*)appointment
{
  [self setAppointment:appointment];
	[self.btnAddress addTarget:self action:@selector(loadMapView) forControlEvents:UIControlEventTouchUpInside];
    NSString * idForAppointmentDestination = @"";
    if(!isEmpty(appointment.appointmentToProperty.propertyId)){
        idForAppointmentDestination = @"Property";
    }
    else if(!isEmpty(appointment.appointmentToProperty.blockId)){
        idForAppointmentDestination = @"Block";
    }
    else{
        idForAppointmentDestination = @"Scheme";
    }
    _lblSchemeBlockProperty.text =idForAppointmentDestination;
    
    
    NSString * addressText = @"";
    
    if(!isEmpty(appointment.appointmentToProperty.propertyId)){
        addressText = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort];
    }
    else if(!isEmpty(appointment.appointmentToProperty.blockId)){
        addressText = appointment.appointmentToProperty.blockName;
    }
    else{
        addressText = appointment.appointmentToProperty.schemeName;
    }
  self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	self.lblAddress1.text = addressText;
	self.lblPostcode.text = [appointment.appointmentToProperty postCode];
	self.lblSurveyor.text = appointment.createdByPerson;
  self.lblStartDate.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle9];
  self.lblEndDate.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle9];
  self.lblStartTime.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle3];
  self.lblEndTime.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle3];
	self.lblAddress1.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblAddress1.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblSurveyor.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblSurveyor.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblPostcode.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblPostcode.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
  self.lblAddress.text = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleLong];
	[self setSelectionStyle:UITableViewCellSelectionStyleNone];
	[self populateJobsheetInfo:appointment];
  [self setUpIcons];
}

- (void) populateJobsheetInfo:(Appointment*)appointment
{
	
	CGRect labelFrame = self.lblEnd.frame;
	labelFrame.size.width = self.btnAddress.frame.size.width;
	
	for (JobSheet *jobData in appointment.appointmentToJobSheet)
	{
		DefectJobSheet *objDefectJobData = (DefectJobSheet *)jobData;
		
		NSString *jsData = [NSString stringWithFormat:@"%@ / Ref: %@",objDefectJobData.jsNumber,objDefectJobData.inspectionRef];
		UILabel *lblJobsheetInfo = [[UILabel alloc]initWithFrame:CGRectZero];
		lblJobsheetInfo.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
		lblJobsheetInfo.text = jsData;
		lblJobsheetInfo.font = [UIFont fontWithName:kFontFamilyHelveticaNeueRegular size:12.0];
		lblJobsheetInfo.backgroundColor = [UIColor clearColor];
		labelFrame.origin.y +=20;
		[lblJobsheetInfo setFrame:labelFrame];
		[self addSubview:lblJobsheetInfo];
		
		DefectCategory * objDefectCategory = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([DefectCategory class])
																																										 Field:kCategoryId
																																								 WithValue:objDefectJobData.defectCategoryId
																																									 context:[PSDatabaseContext sharedContext].managedObjectContext];

		NSString *defectData = [NSString stringWithFormat:@"%@: %@",objDefectCategory.categoryDescription,objDefectJobData.appliance];
		UILabel *lblDefectInfo = [[UILabel alloc]initWithFrame:CGRectZero];
		lblDefectInfo.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
		lblDefectInfo.text = defectData;
		lblDefectInfo.font = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:12.0];
		lblDefectInfo.backgroundColor = [UIColor clearColor];
		labelFrame.origin.y +=15;
		[lblDefectInfo setFrame:labelFrame];
		[self addSubview:lblDefectInfo];

	}
	labelFrame.origin.y += 20;
	
	CGRect btnAddressFrame = self.btnAddress.frame;
	btnAddressFrame.origin.y = labelFrame.origin.y;
	[self.btnAddress setFrame:btnAddressFrame];
	
	CGRect iconAddressFrame = self.iconAddress.frame;
	iconAddressFrame.origin.y = labelFrame.origin.y;
	[self.iconAddress setFrame:iconAddressFrame];
	
	CGRect lblAddressFrame = self.lblAddress.frame;
	lblAddressFrame.origin.y = labelFrame.origin.y;
	[self.lblAddress setFrame:lblAddressFrame];
	
	/*
	//Setting label Address dynamic size
	float lblAddressWidth =
	[addressText
	 boundingRectWithSize:self.lblAddress.frame.size
	 options:NSStringDrawingUsesLineFragmentOrigin
	 attributes:@{ NSFontAttributeName:self.lblAddress.font }
	 context:nil]
	.size.width;
	
	labelFrame = self.lblAddress.frame;
	labelFrame.size.width = lblAddressWidth;
	[self.lblAddress setFrame:labelFrame];
	
	labelFrame = self.lblAddress.frame;
	labelFrame.origin.y -= 16;
	labelFrame.origin.x += labelFrame.size.width+4;
	labelFrame.size.width = 7;
	labelFrame.size.height = 12;
	[self.iconAddress setFrame:labelFrame]; */

}

-(void)setUpIcons
{
	
  self.imgSlotOne.hidden = YES;
  self.imgSlotTwo.hidden = YES;
  self.imgSlotThree.hidden = YES;
  self.imgSlotFour.hidden = YES;
  
  NSSet * customers = self.appointment.appointmentToCustomer;
  NSSet * asbestos = self.appointment.appointmentToProperty.propertyToPropertyAsbestosData;
	
	// Check visibility of customer vulnarity
	for(Customer * customer in customers)
	{
		if(!isEmpty(customer.customerToCustomerRiskData) || !isEmpty(customer.customerToCustomerVulunarityData))
		{
			[self displayIcon:kRiskImageName];
			break;
		}
	}
	
	// Check visibility of asbestos
	if(asbestos.count>0)
	{
		[self displayIcon:kAsbestosImageName];
	}
	
	// Check visibility of customer notes
	if(isEmpty(self.appointment.appointmentNotes)== NO)
	{
		[self displayIcon:kCustomerNotesImageName];
	}
	
	// Check visibility of Two Person Icon
	for(DefectJobSheet * objJobDataList in self.appointment.appointmentToJobSheet)
	{
		DefectJobSheet *objDefectJobDataList = (DefectJobSheet *) objJobDataList;
		
		if([objDefectJobDataList.isTwoPersonsJob boolValue])
		{
			[self displayIcon:kTwoPersonImageName];
			break;
		}
	}

}

- (void) displayIcon:(NSString *) imageName
{
  UIImage *img = [UIImage imageNamed:imageName];
  if (self.imgSlotOne.hidden == YES)
    {
    self.imgSlotOne.hidden = NO;
    self.imgSlotOne.image = img;
    }else if (self.imgSlotTwo.hidden == YES)
    {
    self.imgSlotTwo.hidden = NO;
    self.imgSlotTwo.image = img;
    }else if (self.imgSlotThree.hidden == YES)
    {
    self.imgSlotThree.hidden = NO;
    self.imgSlotThree.image = img;
    }else if (self.imgSlotFour.hidden == YES)
    {
    self.imgSlotFour.hidden = NO;
    self.imgSlotFour.image = img;
    }
  
}

-(void) loadMapView {
	
	if(self.delegate && [self.delegate respondsToSelector:@selector(addressTapped:)]) {
		[self.delegate addressTapped:self.appointment];
	}
}


@end
