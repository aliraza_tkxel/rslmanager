//
//  PSPreVoidInspectionCell.h
//  PropertySurveyApp
//
//  Created by aqib javed on 20/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsViewController.h"


@interface PSDefectAppointmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeBlockProperty;

@property (strong, nonatomic) IBOutlet UILabel *lblAddress1;
@property (strong, nonatomic) IBOutlet UILabel *lblPostcode;
@property (strong, nonatomic) IBOutlet UILabel *lblSurveyor;
@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEnd;
@property (strong, nonatomic) IBOutlet UILabel *lblEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lblEndTime;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppointmentStatus;

@property (strong, nonatomic) IBOutlet UIImageView *imgSlotOne;
@property (strong, nonatomic) IBOutlet UIImageView *imgSlotTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgSlotThree;
@property (strong, nonatomic) IBOutlet UIImageView *imgSlotFour;

@property (strong, nonatomic) IBOutlet FBRImageView *imgAppointmentType;
@property (strong, nonatomic) IBOutlet UIButton *btnAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblJobsheetDetail;
@property (strong, nonatomic) IBOutlet UILabel *lblDefectDetail;
@property (strong, nonatomic) IBOutlet UIImageView *iconAddress;

//User Properties
@property (strong, nonatomic) Appointment * appointment;
@property (weak,   nonatomic) id<PSAppointmentsAddressMapsProtocol> delegate;

-(void) reloadData:(Appointment *)appointment;
@end
