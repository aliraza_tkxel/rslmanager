//
//  PSGasElectricCheckCell
//  PropertySurveyApp
//
//  Created by aqib javed on 20/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSVGasElectricCheckCell.h"
#import "MeterData.h"

@interface PSVGasElectricCheckCell ()
{
	Appointment * _appointment;
}
@end

@implementation PSVGasElectricCheckCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
	self=[super initWithCoder:aDecoder];
	
	if (self)
	{
	}
	
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	// Configure the view for the selected state
}

-(void) reloadData:(Appointment *)appointment
{
	_appointment = appointment;
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	
	self.lblStartDate.text = [UtilityClass stringFromDate:_appointment.appointmentStartTime dateFormat:kDateTimeStyle9];
	self.lblEndDate.text = [UtilityClass stringFromDate:_appointment.appointmentEndTime dateFormat:kDateTimeStyle9];
	self.lblStartTime.text = [UtilityClass stringFromDate:_appointment.appointmentStartTime dateFormat:kDateTimeStyle3];
	self.lblEndTime.text = [UtilityClass stringFromDate:_appointment.appointmentEndTime dateFormat:kDateTimeStyle3];
	
	self.lblSurveyor.text = _appointment.createdByPerson;
	self.lblAddress.text = [_appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleAddress2Onwards];
	self.lblName.text = [_appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort];
	self.lblTermination.text = [UtilityClass stringFromDate:_appointment.appointmentToMeterData.terminationDate dateFormat:kDateTimeStyle9];
	self.lblRelet.text = [UtilityClass stringFromDate:_appointment.appointmentToMeterData.reletDate dateFormat:kDateTimeStyle9];
	
	self.lblAppointmentType.text = [NSString stringWithFormat:@"%@ Check", _appointment.appointmentToMeterData.deviceType];
	
	NSInteger lblAddressWidth = [self.lblAddress.text sizeWithFont:self.lblAddress.font].width + 1;
	NSInteger max_address_width = 245;
	if (lblAddressWidth > max_address_width) {
		lblAddressWidth = max_address_width;
	}
	CGRect labelFrame = self.lblAddress.frame;
	labelFrame.size.width = lblAddressWidth;
	[self.lblAddress setFrame:labelFrame];
	
	labelFrame = self.lblAddress.frame;
	labelFrame.origin.x += labelFrame.size.width+4;
	labelFrame.size.width = 7;
	labelFrame.size.height = 12;
	[self.iconAddress setFrame:labelFrame];
	
	[self.btnAddress addTarget:self action:@selector(loadMapView) forControlEvents:UIControlEventTouchUpInside];
}

-(void) loadMapView
{
	if(self.delegate && [self.delegate respondsToSelector:@selector(addressTapped:)])
	{
		[self.delegate addressTapped:_appointment];
	}
}

@end
