//
//  PSGasElectricCheckCell
//  PropertySurveyApp
//
//  Created by aqib javed on 20/06/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsViewController.h"


@interface PSVGasElectricCheckCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEndTime;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTermination;
@property (strong, nonatomic) IBOutlet UILabel *lblRelet;
@property (strong, nonatomic) IBOutlet UILabel *lblSurveyor;

@property (strong, nonatomic) IBOutlet UILabel *lblAppointmentType;
@property (strong, nonatomic) IBOutlet UIButton *btnAddress;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppointmentStatus;
@property (strong, nonatomic) IBOutlet FBRImageView *imgAppointmentType;
@property (strong, nonatomic) IBOutlet UIImageView *iconAddress;

@property (weak,   nonatomic) id<PSAppointmentsAddressMapsProtocol> delegate;

-(void) reloadData:(Appointment *)appointment;

@end
