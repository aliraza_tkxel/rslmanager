//
//  PSPlannedAppointmentCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 15/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsViewController.h"

@interface PSPlannedAppointmentCell : UITableViewCell
@property (strong, nonatomic) IBOutlet FBRImageView *imgAppointmentType;
@property (strong, nonatomic) IBOutlet UILabel *lblAppointmentTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppointmentStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblSurveyor;
@property (strong, nonatomic) IBOutlet UILabel *lblReported;
@property (strong, nonatomic) IBOutlet UILabel *lblEnds;
@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEndTime;

@property (strong, nonatomic) IBOutlet UILabel *lblStarts;
@property (strong, nonatomic) IBOutlet UILabel *lblEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lblPMO;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIImageView *iconAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnMapView;
@property (weak, nonatomic) IBOutlet UIButton *btnFailedReason;
- (IBAction)viewFailedReason:(id)sender;


@property (weak,   nonatomic) Appointment *appointment;
@property (weak,    nonatomic) id<PSAppointmentsAddressMapsProtocol> delegate;

-(void) reloadData:(Appointment*)appointment;

#pragma mark appalert icons
@property (nonatomic,strong) IBOutlet UIImageView * iconCustomerNotes;
@property (nonatomic,strong) IBOutlet UIImageView * iconAsbestos;
@property (nonatomic,strong) IBOutlet UIImageView * iconRiskValunarability;
@end
