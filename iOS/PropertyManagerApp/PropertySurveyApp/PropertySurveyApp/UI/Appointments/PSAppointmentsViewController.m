//
//  PSAppointmentsViewController.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppointmentsViewController.h"
#import "PSAppointmentsFilterViewController.h"
#import "PSAppointmentDetailViewController.h"
#import "PSBarButtonItem.h"
#import "NSFetchedResultsControllerHelper.h"
#import "PSPlannedAppointmentDetailViewController.h"
#import "PSEventsManager.h"
#import "Appointment+SurveyDownload.h"
#import "Appointment+JSON.h"
#import "PSFaultAppointmentCell.h"
#import "PSPlannedAppointmentCell.h"
#import "PSAppointmentCell.h"
#import "PSMiscellaneousAppointmentCell.h"
#import "PSAdaptationsAppointmentCell.h"
#import "PSConditionAppointmentCell.h"
#import "PSDefectAppointmentCell.h"
#import "PropertyPicture+JSON.h"
#import "RepairPictures+JSON.h"
#import "Appliance+JSON.h"
#import "Defect+JSON.h"
#import "Scheme+JSON.h"
#import "PSFaultSchemeAppointmentCell.h"
#import "PSMiscSchemeAppointmentCell.h"
#import "PSAdaptSchemeAppointmentCell.h"
#import "PSVPreInspectionDetailViewController.h"
#import "PSVGEInspectionDetailViewController.h"
#import "PSVWRAppointmentDetailViewController.h"
#import "PSVPostInspectionDetailViewController.h"
#import "PSDAppointmentDetailViewController.h"
#import "PSVWorksRequiredCell.h"
#import "PSVPreInspectionCell.h"
#import "PSVPostInspectionCell.h"
#import "PSVGasElectricCheckCell.h"
#import "VoidData.h"
#import "WorkRequired+CoreDataClass.h"
#import "PSGasServicingAppointmentTableViewCell.h"
#import "PSOilAppointmentTableViewCell.h"
#import "PSAlternateFuelTableViewCell.h"

#define kAppointmentViewSectionHeaderHeight 20
#define kFaultAppointmentCellDefaultHeight 130
#define kDefectAppointmentCellDefaultHeight 160
#define kFaultSchemeAppointmentCellDefaultHeight 183
#define kMiscSchemeAppointmentCellDefaultHeight 183
#define kAdaptSchemeAppointmentCellDefaultHeight 183
#define kPlannedAppointmentCellDefaultHeight 143
#define kAdaptationAppointmentCellDefaultHeight 155

static NSString *CellIdentifier = @"appointmentCellIdentifier";
static NSString *faultAppointmentCellIdentifier = @"faultAppointmentCellIdentifier";
static NSString *plannedAppointmentCellIdentifier = @"plannedAppointmentCellIdentifier";
static NSString *adaptationsAppointmentCellIdentifier = @"adaptationsAppointmentCellIdentifier";
static NSString *miscellaneousAppointmentCellIdentifier = @"miscellaneousAppointmentCellIdentifier";
static NSString *miscSchemeAppointmentViewCellIdentifier = @"miscSchemeAppointmentViewCellIdentifier";
static NSString *adaptSchemeAppointmentViewCellIdentifier = @"adaptSchemeAppointmentViewCellIdentifier";
static NSString *conditionAppointmentCellIdentifier = @"conditionAppointmentCellIdentifier";
static NSString *faultAppointmentSchemeCellIdentifier = @"faultSchemeAppointmentViewCellIdentifier";
static NSString *gasAppointmentCellIdentifier = @"PSGasServicingAppointmentTableViewCell";
static NSString *alternativeFuelsCellIdentifier = @"PSAlternateFuelTableViewCell";
static NSString *oilServicingCellIdentifier = @"PSOilAppointmentTableViewCell";

@interface PSAppointmentsViewController ()
{
	EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloadingTable;
	BOOL _noResultsFound;
	BOOL _isAppointmentListEmpty;
	NSArray *_modifiedAppointments;
	NSInteger _todaySection;
	//image resending mechanism helper variables
	NSArray * tempStorageModifiedAppointments;
	NSMutableArray * imagesToUpload;
	NSMutableArray * imagesToUploadType;
	NSMutableArray * defectsToUpload;
	BOOL isAnyImageFail;
	int totalImages;
	int totalDefects;
	MBProgressHUD * spinnerView;
	NSInteger _rowCountDebug;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (UIImage *)imageForAppointmentType:(AppointmentType)type;
- (UIImage *)imageForAppointmentCompletionStatus:(AppointmentStatus)status;
@end

@implementation PSAppointmentsViewController

@synthesize isAnyAppointmentDownloading = _isAnyAppointmentDownloading;

- (BOOL)isAnyAppointmentDownloading
{
	return _isAnyAppointmentDownloading;
}

- (void)setIsAnyAppointmentDownloading:(BOOL)status
{
	_isAnyAppointmentDownloading = status;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	imagesToUpload = [[NSMutableArray alloc] init];
	defectsToUpload = [[NSMutableArray alloc] init];
	

//	[[PSOutBox sharedOutbox] startMonitoring];
	//[self checkSafetyAlert];
	
	_todaySection = -1;
	
	[self.tblAppointments setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	[self loadNavigationonBarItems];
	if ([[SettingsClass sharedObject].loggedInUser.userToAppointments count] <= 0)
	{
		[self disableNavigationBarButtons];
	}
	[self addAppointmentsFilterView];
	[self addPullToRefreshControl];
	[self registerNibsForTableView];
	[self todaySection];
	if (_todaySection)
	{
		[self performSelector:@selector(scrollToTodaySection) withObject:nil afterDelay:1.0];
	}
    [self startMonitoringNetworkChanges];
    if ([self isSafetyAlertDisplayedToday]) {
        [[PSEventsManager sharedManager] requestCalendarPermissions];
    }
}

-(void)registerNibsForTableView
{
	NSString * nibName;
	nibName = NSStringFromClass([PSVPreInspectionCell class]);
	[self.tblAppointments registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVGasElectricCheckCell class]);
	[self.tblAppointments registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVWorksRequiredCell class]);
	[self.tblAppointments registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSVPostInspectionCell class]);
	[self.tblAppointments registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    
    nibName = NSStringFromClass([PSGasServicingAppointmentTableViewCell class]);
    [self.tblAppointments registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    
    nibName = NSStringFromClass([PSAlternateFuelTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    
    nibName = NSStringFromClass([PSOilAppointmentTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	_allowRelaodingTableView_ = YES;
	spinnerView = NULL;
	// self.fetchedResultsController = nil;
	[[PSAppDelegate delegate] setUserLoggedInToApplication:YES];
	[self registerNotifications];
	if ([[SettingsClass sharedObject].loggedInUser.userToAppointments count] <= 0 && _fetchedResultsController == nil)
	{
		_isAppointmentListEmpty = YES;
		[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																 LOC(@"KEY_ALERT_LOADING"),
																 LOC(@"KEY_STRING_APPOINTMENTS")]];
	}
	else
	{
		_isAppointmentListEmpty = NO;
	}
	
	if (_fetchedResultsController == nil)
	{
		if( !_isAppointmentListEmpty)
		{
			[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																	 LOC(@"KEY_ALERT_LOADING"),
																	 LOC(@"KEY_STRING_APPOINTMENTS")]];
		}
		[[PSAppointmentsManager sharedManager] fetchAllAppointsments];
		[self safelyPerformFetchOnController];
	}
	else
	{
		//Force full repopulation
		self.fetchedResultsController = nil;
		[self safelyPerformFetchOnController];
	}
	[self setTitle:[[SettingsClass sharedObject] userFullName]];
	[NSFetchedResultsControllerHelper filterAppointmentsWithOptions:[SettingsClass sharedObject].filterOptions
																											 controller:self.fetchedResultsController];
	[self.tblAppointments reloadData];
}

-(void)loadInprogressOldAppointmentsFromDate:(NSDate*)from toDate:(NSDate*)to
{
	[self hideAppointmentsFilterView];
	[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
															 LOC(@"KEY_ALERT_LOADING"),
															 LOC(@"KEY_STRING_APPOINTMENTS")]];
	[[PSAppointmentsManager sharedManager] fetchAllInprogressAppointsmentsFromDate:from toDate:to];
}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	if(self.isAnyAppointmentDownloading == TRUE)
	{
		if(spinnerView == NULL)
		{
			spinnerView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
			[self disableNavigationBarButtons];
			[spinnerView setLabelText:LOC(@"KEY_STRING_SURVEY_DOWNLOADING")];
			[spinnerView show:TRUE];
		}
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self deRegisterNotifications];
	_noResultsFound = NO;
	self.fetchedResultsController.delegate=nil;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	
	
	[self setTblAppointments:nil];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *logoutButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																				 style:PSBarButtonItemStyleLogout
																																				target:self
																																				action:@selector(onClickLogout)];
	
	PSBarButtonItem *searchButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																				 style:PSBarButtonItemStyleSearch
																																				target:self
																																				action:@selector(onClickSearchButton)];
	
	[self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:logoutButton, nil] animated:YES];
	[self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:searchButton, nil] animated:YES];
	
	[self setTitle:[[SettingsClass sharedObject] userFullName]];
}

- (void) onClickLogout
{
	CLS_LOG(@"onClickLogout");
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_LOGOUT")
																								 message:LOC(@"KEY_ALERT_CONFIRM_LOGOUT")
																								delegate:self
																			 cancelButtonTitle:LOC(@"KEY_ALERT_YES")
																			 otherButtonTitles:LOC(@"KEY_ALERT_NO"),nil];
	alert.tag = AlertViewTagLogout;
	[alert show];
   
}

- (void)onClickSearchButton
{
	CLS_LOG(@"onClickSearchButton");
	if (!isSearching)
	{
		[self showAppointmentsFilterView];
	}
}

#pragma mark UIAlertViewDelegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	CLS_LOG(@"tag: %td", alertView.tag);
	
	if (alertView.tag == AlertViewTagSafety)
	{
		[self dismissAlert:alertView];
	}
	else if (alertView.tag == AlertViewTagLogout)
	{
		if(buttonIndex == alertView.cancelButtonIndex)
		{
			_fetchedResultsController = nil;
			[[SettingsClass sharedObject] signOut];
            PSAppDelegate* appDel = (PSAppDelegate*)[[UIApplication sharedApplication]delegate];
            [[NSNotificationCenter defaultCenter] removeObserver:appDel name:kReachabilityChangedNotification object:nil];
		}
	}
    else if (alertView.tag == AlertViewTagPostAppointmentData || alertView.tag == AlertViewTagSyncingFailed)
	{
		if(buttonIndex == alertView.cancelButtonIndex)
		{
			//Checking here if an image is in posting
			if(![PSPropertyPictureManager sharedManager].isPostingImage)
			{
                [PSSynchronizationManager sharedManager].delegate = self;
                [[PSSynchronizationManager sharedManager]  syncCompletedAppointments];
			}
			else
			{
				UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																											 message:LOC(@"KEY_ALERT_DATA_POSTING_IMAGE_INPROGRESS")
																											delegate:nil
																						 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																						 otherButtonTitles:nil,nil];
				
				[alert show];
			}
		}
		else if (buttonIndex == alertView.firstOtherButtonIndex)
		{
			_modifiedAppointments = nil;
			[self doneLoadingTableViewData];
		}
	}
	else if (alertView.tag == AlertViewTagNoInternet)
	{
		[spinnerView show:NO];
		_modifiedAppointments = nil;
		[self doneLoadingTableViewData];
		[MBProgressHUD hideAllHUDsForView:self.view animated:NO];
		[self enableNavigationBarButtons];
	}
	else if (alertView.tag == AlertViewTagRefreshAppointments)
	{
		CLS_LOG(@"Button Index: %td, Alert Index: %td", buttonIndex, alertView.firstOtherButtonIndex);
		if (buttonIndex == alertView.firstOtherButtonIndex)
		{
			[self reloadTableViewDataSource];
			[alertView dismissWithClickedButtonIndex:buttonIndex animated:TRUE];
		}
		else
		{
			[self doneLoadingTableViewData];
			[alertView dismissWithClickedButtonIndex:buttonIndex animated:TRUE];
		}
	}
	else if (alertView.tag == AlertViewTagException)
	{
		[self doneLoadingTableViewData];
	}
	else if(alertView.tag == AlertViewTagFailedToSyncFewAppointmentsOnServer)
	{
		if (buttonIndex == alertView.firstOtherButtonIndex)
		{
			// Handle Failed Appointments to View
		}
		else
		{
			[self doneLoadingTableViewData];
		}
	}
}

- (void)didPresentAlertView:(UIAlertView *)alertView
{
	CLS_LOG(@"didPresentAlertView");
}

#pragma mark - Notifications

-(void) registerNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAppointmentsSaveSuccessNotification) name:kAppointmentsSaveSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAppointmentsSaveFailureNotification) name:kAppointmentsSaveFailureNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAppointmentsNoResultFoundNotification) name:kAppointmentsNoResultFoundNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRefreshAppointmentExceptionNotificationReceive) name:kRefreshAppointmentsExceptionReceiveNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveNoConnectivityNotification) name:kInternetUnavailable object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveRequestTimeoutNotification) name:kInternetRequestTimeOut object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSurveyDownloadFailed) name:kSurveyDownloadServiceNetworkFailed object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPropertyAppliancesDataDownloadNotificationReceive) name:kFetchPropertyAppliancesDataDownloadedNotification object:nil];
	
}

-(void) startMonitoringNetworkChanges{
    PSAppDelegate* appDel = (PSAppDelegate*)[[UIApplication sharedApplication]delegate];
    [[NSNotificationCenter defaultCenter] addObserver:appDel selector:@selector(reachabilityChangedNotReceived:) name:kReachabilityChangedNotification object:nil];
    [[PSReachibilityManager sharedManager] startMonitoringNetworkChanges];
}

-(void) deRegisterNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsSaveSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsSaveFailureNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kRefreshAppointmentsExceptionReceiveNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kInternetUnavailable object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kInternetRequestTimeOut object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSurveyDownloadServiceNetworkFailed object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchPropertyAppliancesDataDownloadedNotification object:nil];
}

- (void)onReceiveAppointmentsSaveSuccessNotification
{
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(methodToExecuteonReceiveAppointmentsSaveSuccessNotification) object:nil];
	[self performSelector:@selector(methodToExecuteonReceiveAppointmentsSaveSuccessNotification) withObject:nil afterDelay:1.0];
}

-(void) onReceiveSurveyDownloadFailed{
    self.isAnyAppointmentDownloading = FALSE;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)methodToExecuteonReceiveAppointmentsSaveSuccessNotification
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.activityIndicator stopAnimating];
		[self hideActivityIndicator];
		[self enableNavigationBarButtons];
        [self safelyPerformFetchOnController];
        if (_reloadingTable)
        {
            [self doneLoadingTableViewData];
        }
        else
        {
            //     [self.tblAppointments reloadData];
        }
        
        [self todaySection];
        if (_todaySection)
        {
            [self performSelector:@selector(scrollToTodaySection) withObject:nil afterDelay:1.0];
        }
		
        [self showDailySafetyAlert];
	});
	[MBProgressHUD hideHUDForView:self.view animated:NO];
}

- (void)onReceiveAppointmentsSaveFailureNotification
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[self hideActivityIndicator];
		[self enableNavigationBarButtons];
		[self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
										 message:LOC(@"KEY_ALERT_FAILED_TO_DOWNLOAD_APPOINTMENT")
												 tag:AlertViewTagException
					 cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
					 otherButtonTitles:nil];
	});
}

-(void) onReceiveAppointmentsNoResultFoundNotification
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[self hideActivityIndicator];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
		[self doneLoadingTableViewData];
		[self enableNavigationBarButtons];
        if([[self.fetchedResultsController sections] count]==0){
            _noResultsFound = TRUE;
        }

		[_tblAppointments reloadData];
        [self showDailySafetyAlert];
	});
	
}


- (void) onRefreshAppointmentExceptionNotificationReceive
{
    if([[[PSSynchronizationManager sharedManager] sendingAppointments] count]==0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
                             message:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS_FAILURE")
                                 tag:AlertViewTagException
                   cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                   otherButtonTitles:nil];
        });
        //  [self doneLoadingTableViewData];
        [self enableNavigationBarButtons];
    }
	
}

- (void) onReceiveNoConnectivityNotification
{
	dispatch_async(dispatch_get_main_queue(), ^{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																									 message:LOC(@"KEY_ALERT_NO_INTERNET")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		alert.tag = AlertViewTagNoInternet;
		alert.delegate = self;
		[alert show];
	});
	//[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) onReceiveRequestTimeoutNotification
{
	[self doneLoadingTableViewData];
	//[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) onPropertyAppliancesDataDownloadNotificationReceive {
    [self doneLoadingTableViewData];
    self.isAnyAppointmentDownloading = FALSE;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

#pragma mark  - Synchronization delegate methods

-(void) updateSpinnerWithLabel:(NSString *)progressLabel{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        spinnerView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [spinnerView setLabelText:progressLabel];
        [spinnerView show:TRUE];
    });
}

-(void) appointmentSyncingCompleted{
    //All Data is posted to server, so refresh all the appointments.
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(reloadTableViewDataSource) object:nil];
        _allowRelaodingTableView_ = YES;
        [self performSelector:@selector(reloadTableViewDataSource) withObject:nil afterDelay:1.0];
        
    });
}
//Data syncing
-(void) dataSynchingFailedForAppointment:(NSArray *)failedAppointments{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if(!isEmpty(failedAppointments)) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if([failedAppointments count]>0){
                NSString *errorMessage = [UtilityClass createErrorMessageStringForFailedAppointments:failedAppointments];
                [UtilityClass showPopUpForErrorMessage:[NSString stringWithFormat:@"Following appointments have failed:\r%@",errorMessage] forViewController:self];
            }
            
        });
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
           [self showAlertForSyncingFailureWithTitle:LOC(@"KEY_ALERT_ERROR") andBodyMessage:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS_FAILURE")];
        });
    }
    [self doneLoadingTableViewData];
    [self enableNavigationBarButtons];
    [self hideActivityIndicator];

}
-(void) dataSyncingFailedOnNetworkLevel:(NSArray*) failedAppointments{
    dispatch_async(dispatch_get_main_queue(), ^{
         [self showAlertForSyncingFailureWithTitle:LOC(@"KEY_ALERT_ERROR") andBodyMessage:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS_FAILURE")];
    });
    [self doneLoadingTableViewData];
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];

}
//Defect syncing

-(void) defectsSynchingFailedForAppointment:(Appointment *)appointment{
    [self showAlertForSyncingFailureWithTitle:LOC(@"KEY_STRING_APP_TITLE") andBodyMessage:LOC(@"KEY_ALERT_DEFECT_SYNCING_FAILURE")];
    [self doneLoadingTableViewData];
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
}

//Images syncing
-(void) imagesSynchingFailedForAppointment:(Appointment *)appointment{
    isAnyImageFail = YES;
    [self RollBackIfNoInternetAvailability];
    [self showAlertForSyncingFailureWithTitle:LOC(@"KEY_STRING_APP_TITLE") andBodyMessage:LOC(@"KEY_ALERT_IMAGES_SYNCING_FAILURE")];
    [self doneLoadingTableViewData];
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
}

//Rollback

-(void)RollBackIfNoInternetAvailability
{
    CLS_LOG(@"Rollback");
    [imagesToUpload removeAllObjects];
    [defectsToUpload removeAllObjects];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineDefectsSaveNotificationFail object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineDefectsSaveNotificationSuccess object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineImageSaveNotificationFail object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
    
    PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
    delegate.postingDataObjectAppointments =0;
    tempStorageModifiedAppointments = nil;
    [self enableNavigationBarButtons];
    [self hideActivityIndicator];
    [self doneLoadingTableViewData];
    
    //[MBProgressHUD hideAllHUDsForView:self.view animated:NO];
}

-(void) showAlertForSyncingFailureWithTitle:(NSString *) title andBodyMessage:(NSString *) bodyMessage{
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:title
                                                   message:bodyMessage
                                                  delegate:self
                                         cancelButtonTitle:LOC(@"KEY_ALERT_RETRY_SYNCING")
                                         otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"),nil];
    alert.tag = AlertViewTagSyncingFailed;
    [alert show];
}

#pragma mark - TableView header

- (void)setAppointmentsTableHeader:(NSInteger)sections
{
	self.noAppointmentsLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
	self.noAppointmentsLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
	self.noAppointmentsLabel.text = LOC(@"KEY_STRING_NO_APPOINTMENT");
	self.noAppointmentsLabel.textAlignment = PSTextAlignmentCenter;
	//self.noAppointmentsLabel.backgroundColor = [UIColor clearColor];
	[self.noAppointmentsHeaderView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	CGRect frame = [self.noAppointmentsHeaderView frame];
	frame.size.height = CGRectGetHeight([self.tblAppointments frame]);
	[self.noAppointmentsHeaderView setFrame:frame];
	
	if(sections == 0)
	{
		[self.tblAppointments setTableHeaderView:self.noAppointmentsHeaderView];
		//[self.tblAppointments setScrollEnabled:NO];
	}
	else
	{
		[self.tblAppointments setTableHeaderView:nil];
		//[self.tblAppointments setScrollEnabled:YES];
	}
}
#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	NSInteger sections = 0;
	sections = [[self.fetchedResultsController sections] count];
	[self setAppointmentsTableHeader:sections];
	CLS_LOG(@"sections = %td", sections);
	_rowCountDebug = 0;
	return sections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 138;
	Appointment *appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (isEmpty(appointment)){
        return 0;
    }
	AppointmentType type = [appointment getType];
	
	switch (type)
	{
		case AppointmentTypeFault:
		{
			NSInteger jobCount = [[appointment.appointmentToJobSheet allObjects] count];
			CGFloat labelsHeight = jobCount * 18;
			if(appointment.appointmentToProperty)
			{
				height = kFaultAppointmentCellDefaultHeight + labelsHeight;
			}
			else
			{
				height = kFaultSchemeAppointmentCellDefaultHeight + labelsHeight;
			}
		}
		break;
		case AppointmentTypeGas:
		{
			height = 148;
        }
        break;
        case AppointmentTypeOilServicing:
        case AppointmentTypeAlternativeFuels:
        case AppointmentTypeGasVoid:
        {
            height = 138;
        }
    break;
    case AppointmentTypeDefect:
    {
      //TODO CHANGE TO DEFECT APPOINTMENT LIST
    NSInteger jobCount = [[appointment.appointmentToJobSheet allObjects] count];
    CGFloat labelsHeight = jobCount * 22;
			height = kDefectAppointmentCellDefaultHeight + labelsHeight + ((jobCount>1)?15:0);
		}
		break;
		case AppointmentTypePreVoid:
		case AppointmentTypeVoidWorkRequired:
		case AppointmentTypeGasCheck:
		case AppointmentTypeElectricCheck:
		case AppointmentTypePostVoid:
		{
			height = 150;
		}
		break;
		case AppointmentTypeAdaptations:
		{
			if(appointment.appointmentToProperty)
			{
				height = kAdaptationAppointmentCellDefaultHeight;
			}
			else
			{
				height = kAdaptSchemeAppointmentCellDefaultHeight + 18;
			}
			
		}
		break;
		case AppointmentTypePlanned:
		case AppointmentTypeMiscellaneous:
		case AppointmentTypeCondition:
		{
			if(appointment.appointmentToProperty)
			{
				height = kPlannedAppointmentCellDefaultHeight;
			}
			else
			{
				height = kMiscSchemeAppointmentCellDefaultHeight + 18;
			}
			
		}
		break;
		default:
		{
			height = 103;
		}
		break;
	}
	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return (_noResultsFound)?0.0:kAppointmentViewSectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] init];
    if ([[_fetchedResultsController sections] count] > section)
    {
        if(!_noResultsFound)
        {
            NSString *header = nil;
            id <NSFetchedResultsSectionInfo> sectionInfo;
            //[__NSArrayM objectAtIndex:]: index 1 beyond bounds [0 .. 0]
            if ([[_fetchedResultsController sections] count] > 1)
            {
                sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
            }
            else if ([[_fetchedResultsController sections] count] == 1)
            {
                sectionInfo = [[_fetchedResultsController sections] objectAtIndex:0];
            }
            //        sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
            NSArray *sectionLabels = [[sectionInfo name] componentsSeparatedByString:@","];
            
            if(sectionLabels && [sectionLabels count] >= 2)
            {
                headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, tableView.bounds.size.width, kAppointmentViewSectionHeaderHeight)];
                [headerView setBackgroundColor:[UIColor clearColor]];
                UIImageView *imgSectionHeaderBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sectionHeaderBar"]];
                [headerView addSubview:imgSectionHeaderBar];
                
                UIFont *dayFont = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:18];
                UIFont *dateFont = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:18];
                NSString *dayString = [sectionLabels objectAtIndex:0];
                NSString *dateString = [sectionLabels objectAtIndex:1];
                
                CGSize dayStrigSize = CGSizeZero;
                CGSize dateStrigSize = CGSizeZero;
                if(OS_VERSION >= 7.0)
                {
                    dayStrigSize = [dayString sizeWithAttributes:[NSDictionary dictionaryWithObject:dayFont forKey:NSFontAttributeName]];
                    dateStrigSize = [dateString sizeWithAttributes:[NSDictionary dictionaryWithObject:dateFont forKey:NSFontAttributeName]];
                }
                else
                {
                    dayStrigSize = [dayString sizeWithFont:dayFont];
                    dateStrigSize = [dateString sizeWithFont:dateFont];
                }
                
                UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, dayStrigSize.width, kAppointmentViewSectionHeaderHeight)];
                [dayLabel setFont:dayFont];
                [dayLabel setBackgroundColor:[UIColor clearColor]];
                [dayLabel setTextColor:[UIColor whiteColor]];
                [dayLabel setText:dayString];
                
                UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(dayStrigSize.width + 7, 0, dateStrigSize.width, kAppointmentViewSectionHeaderHeight)];
                [dateLabel setFont:dateFont];
                [dateLabel setBackgroundColor:[UIColor clearColor]];
                [dateLabel setTextColor:[UIColor whiteColor]];
                [dateLabel setText:dateString];
                
                [headerView setAlpha:1.0];
                [headerView addSubview:dayLabel];
                [headerView addSubview:dateLabel];
            }
        }
    }
	
	return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	NSInteger rows = 0;
	id <NSFetchedResultsSectionInfo> sectionInfo;
	sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	rows = [sectionInfo numberOfObjects];
	_rowCountDebug = _rowCountDebug + rows;
	CLS_LOG(@"Sections = %td, Rows = %td, Net Rows = %td", section, rows, _rowCountDebug);
	return rows;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell * cell = nil;
	Appointment * appointment = nil;
	//check if indexPath is within range of fetchedResultsController limit
	if ([[self.fetchedResultsController sections] count] > [indexPath section])
	{
		id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:[indexPath section]];
		if ([sectionInfo numberOfObjects] > [indexPath row])
		{
			appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
		}
	}
	// return empty cell
	if (appointment == nil)
	{
		PSFaultAppointmentCell *_cell = [self createCellWithIdentifier:faultAppointmentCellIdentifier];
		_cell.hidden = YES;
		cell = _cell;
		return cell;
	}
	CLS_LOG(@"Appoinment id %@",appointment.appointmentId);
	AppointmentType appointmentType = [appointment getType];
	
	if(appointmentType == AppointmentTypeDefect)
    {
    PSDefectAppointmentCell * _cell = [self createCellWithIdentifier:NSStringFromClass([PSDefectAppointmentCell class])];
    [_cell reloadData:appointment];
    [_cell setDelegate:self];
    _cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
    _cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
    cell = _cell;
    }
  else if(appointmentType == AppointmentTypePostVoid)
	{
		PSVPostInspectionCell *_cell = [self.tblAppointments dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPostInspectionCell class])];
		[_cell reloadData:appointment];
		[_cell setDelegate:self];
		_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
		_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
		cell = _cell;
	}
	else if(appointmentType == AppointmentTypeVoidWorkRequired)
	{
		PSVWorksRequiredCell *_cell = [self.tblAppointments dequeueReusableCellWithIdentifier:NSStringFromClass([PSVWorksRequiredCell class])];
		[_cell reloadData:appointment];
		[_cell setDelegate:self];
		_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
		_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
		cell = _cell;
	}
	else if(appointmentType == AppointmentTypeElectricCheck)
	{
		PSVGasElectricCheckCell *_cell = [self.tblAppointments dequeueReusableCellWithIdentifier:NSStringFromClass([PSVGasElectricCheckCell class])];
		[_cell reloadData:appointment];
		[_cell setDelegate:self];
		_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
		_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
		cell = _cell;
	}
	else if(appointmentType == AppointmentTypeGasCheck)
	{
		PSVGasElectricCheckCell *_cell = [self.tblAppointments dequeueReusableCellWithIdentifier:NSStringFromClass([PSVGasElectricCheckCell class])];
		[_cell reloadData:appointment];
		[_cell setDelegate:self];
		_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
		_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
		cell = _cell;
	}
	else if(appointmentType == AppointmentTypePreVoid)
	{
		PSVPreInspectionCell *_cell = [self.tblAppointments dequeueReusableCellWithIdentifier:NSStringFromClass([PSVPreInspectionCell class])];
		[_cell reloadData:appointment];
		[_cell setDelegate:self];
		_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
		_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
		cell = _cell;
	}
	else if (appointmentType == AppointmentTypeFault)
	{
		if(appointment.appointmentToProperty)
		{
			PSFaultAppointmentCell *_cell = [self createCellWithIdentifier:faultAppointmentCellIdentifier];
			[_cell reloadData:appointment];
			[_cell setDelegate:self];
			_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
			_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
			cell = _cell;
		}
		else
		{
			PSFaultSchemeAppointmentCell *_cell = [self createCellWithIdentifier:faultAppointmentSchemeCellIdentifier];
			[_cell reloadData:appointment];
			[_cell setDelegate:self];
			_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
			_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
			cell = _cell;
		}
	}
	else if (appointmentType == AppointmentTypePlanned)
	{
		PSPlannedAppointmentCell *_cell = [self createCellWithIdentifier:plannedAppointmentCellIdentifier];
		[_cell reloadData:appointment];
		[_cell setDelegate:self];
		_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
		_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
		cell = _cell;
	}
	else if (appointmentType == AppointmentTypeAdaptations)
	{
		
		if(appointment.appointmentToProperty)
		{
			PSAdaptationsAppointmentCell *_cell = [self createCellWithIdentifier:adaptationsAppointmentCellIdentifier];
			[_cell reloadData:appointment];
			[_cell setDelegate:self];
			_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
			_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
			cell = _cell;
		}
		else
		{
			PSAdaptSchemeAppointmentCell *_cell = [self createCellWithIdentifier:adaptSchemeAppointmentViewCellIdentifier];
			[_cell reloadData:appointment];
			[_cell setDelegate:self];
			_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
			_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
			cell = _cell;
		}
		
	}
	else if (appointmentType == AppointmentTypeMiscellaneous)
	{
		
		if(appointment.appointmentToProperty)
		{
			PSMiscellaneousAppointmentCell *_cell = [self createCellWithIdentifier:miscellaneousAppointmentCellIdentifier];
			[_cell reloadData:appointment];
			[_cell setDelegate:self];
			_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
			_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
			cell = _cell;
		}
		else
		{
			PSMiscSchemeAppointmentCell *_cell = [self createCellWithIdentifier:miscSchemeAppointmentViewCellIdentifier];
			[_cell reloadData:appointment];
			[_cell setDelegate:self];
			_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
			_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
			cell = _cell;
		}
	}
	else if (appointmentType == AppointmentTypeCondition)
	{
		PSConditionAppointmentCell *_cell = [self createCellWithIdentifier:conditionAppointmentCellIdentifier];
		[_cell reloadData:appointment];
		[_cell setDelegate:self];
		_cell.imgAppointmentType.image = [self imageForAppointmentType:[appointment getType]];
		_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:[appointment getStatus]];
		cell = _cell;
	}
    else if(appointmentType == AppointmentTypeGas){
        PSGasServicingAppointmentTableViewCell *_cell = [self createCellWithIdentifier:gasAppointmentCellIdentifier];
        [self configureCell:_cell atIndexPath:indexPath];
        [_cell reloadData:appointment];
        [_cell setDelegate:self];
        cell = _cell;
    }
    else if(appointmentType==AppointmentTypeAlternativeFuels){
        PSAlternateFuelTableViewCell *_cell = [self createCellWithIdentifier:alternativeFuelsCellIdentifier];
        [self configureCell:_cell atIndexPath:indexPath];
        [_cell reloadData:appointment];
        [_cell setDelegate:self];
        cell = _cell;
    }
    
    else if(appointmentType==AppointmentTypeOilServicing){
        PSOilAppointmentTableViewCell *_cell = [self createCellWithIdentifier:oilServicingCellIdentifier];
        [self configureCell:_cell atIndexPath:indexPath];
        [_cell reloadData:appointment];
        [_cell setDelegate:self];
        cell = _cell;
    }
	else
	{
		PSAppointmentCell *_cell = [self.tblAppointments dequeueReusableCellWithIdentifier:CellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSAppointmentCell" owner:self options:nil];
			_cell = self.appointmentCell;
			self.appointmentCell = nil;
		}
		// Configure the cell...
		[self configureCell:_cell atIndexPath:indexPath];
		[_cell reloadData:appointment];
		[_cell setDelegate:self];
		cell = _cell;
	}
	return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
	if([identifier isEqualToString:faultAppointmentCellIdentifier])
	{
		PSFaultAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:faultAppointmentCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSFaultAppointmentCell" owner:self options:nil];
			_cell = self.faultAppointmentCell;
			self.faultAppointmentCell = nil;
      }
    return _cell;
    }
  if ([identifier isEqualToString:NSStringFromClass([PSDefectAppointmentCell class])])
    {
    PSDefectAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDefectAppointmentCell class])];
    if(_cell == nil)
      {
      [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PSDefectAppointmentCell class]) owner:self options:nil];
      _cell = self.defectAppointmentCell;
      self.defectAppointmentCell=nil;
		}
		return _cell;
	}
	else if ([identifier isEqualToString:faultAppointmentSchemeCellIdentifier])
	{
		PSFaultSchemeAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:faultAppointmentSchemeCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSFaultSchemeAppointmentCell" owner:self options:nil];
			_cell = self.faultSchemeCell;
			self.faultSchemeCell=nil;
		}
		return _cell;
	}
	else if ([identifier isEqualToString:miscSchemeAppointmentViewCellIdentifier])
	{
		PSMiscSchemeAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:miscSchemeAppointmentViewCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSMiscSchemeAppointmentCell" owner:self options:nil];
			_cell = self.miscSchemeCell;
			self.miscSchemeCell=nil;
		}
		return _cell;
	}
	else if ([identifier isEqualToString:adaptSchemeAppointmentViewCellIdentifier])
	{
		PSAdaptSchemeAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:adaptSchemeAppointmentViewCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSAdaptSchemeAppointmentCell" owner:self options:nil];
			_cell = self.adaptSchemeCell;
			self.adaptSchemeCell=nil;
		}
		return _cell;
	}
	else if ([identifier isEqualToString:plannedAppointmentCellIdentifier])
	{
		PSPlannedAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:plannedAppointmentCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSPlannedAppointmentCell" owner:self options:nil];
			_cell = self.plannedAppointmentCell;
			self.plannedAppointmentCell = nil;
		}
		return _cell;
	}
	else if ([identifier isEqualToString:adaptationsAppointmentCellIdentifier])
	{
		PSAdaptationsAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:adaptationsAppointmentCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSAdaptationsAppointmentCell" owner:self options:nil];
			_cell = self.adaptationsAppointmentCell;
			self.adaptationsAppointmentCell = nil;
		}
		return _cell;
	}
	else if ([identifier isEqualToString:miscellaneousAppointmentCellIdentifier])
	{
		PSMiscellaneousAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:miscellaneousAppointmentCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSMiscellaneousAppointmentCell" owner:self options:nil];
			_cell = self.miscellaneousAppointmentCell;
			self.miscellaneousAppointmentCell = nil;
		}
		return _cell;
	}
	else if ([identifier isEqualToString:conditionAppointmentCellIdentifier])
	{
		PSConditionAppointmentCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:conditionAppointmentCellIdentifier];
		if(_cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSConditionAppointmentCell" owner:self options:nil];
			_cell = self.conditionAppointmentCell;
			self.conditionAppointmentCell = nil;
		}
		return _cell;
	}
    else if ([identifier isEqualToString:gasAppointmentCellIdentifier])
    {
        PSGasServicingAppointmentTableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:gasAppointmentCellIdentifier];
        if(_cell == nil)
        {
           self.gasServicingCell = [[[NSBundle mainBundle] loadNibNamed:@"PSGasServicingAppointmentTableViewCell" owner:self options:nil] objectAtIndex:0];
            _cell = self.gasServicingCell;
            self.gasServicingCell = nil;
        }
        return _cell;
    }
    
    else if ([identifier isEqualToString:alternativeFuelsCellIdentifier])
    {
        PSAlternateFuelTableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:alternativeFuelsCellIdentifier];
        if(_cell == nil)
        {
            self.alternativeServicingCell = [[[NSBundle mainBundle] loadNibNamed:@"PSAlternateFuelTableViewCell" owner:self options:nil] objectAtIndex:0];
            _cell = self.alternativeServicingCell;
            self.alternativeServicingCell = nil;
        }
        return _cell;
    }
    
    else if ([identifier isEqualToString:oilServicingCellIdentifier])
    {
        PSOilAppointmentTableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:oilServicingCellIdentifier];
        if(_cell == nil)
        {
            self.oilServicingCell = [[[NSBundle mainBundle] loadNibNamed:@"PSOilAppointmentTableViewCell" owner:self options:nil] objectAtIndex:0];
            _cell = self.oilServicingCell;
            self.oilServicingCell = nil;
        }
        return _cell;
    }
    
	return nil;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Appointment * appointment = nil;
    //check if indexPath is within range of fetchedResultsController limit
    if ([[self.fetchedResultsController sections] count] > [indexPath section])
    {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:[indexPath section]];
        if ([sectionInfo numberOfObjects] > [indexPath row])
        {
            appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
        }
    }
    if (isEmpty(appointment)){
        return;
    }
	AppointmentStatus appointmentStatus = [appointment getStatus];
	AppointmentType appointmentType = [appointment getType];
	
	if ([cell isKindOfClass:[PSFaultAppointmentCell class]])
	{
		PSFaultAppointmentCell *_cell = (PSFaultAppointmentCell *)cell;
		[_cell reloadData:appointment];
		cell = _cell;
	}
	else if ([cell isKindOfClass:[PSPlannedAppointmentCell class]])
	{
		PSFaultAppointmentCell *_cell = (PSFaultAppointmentCell *)cell;
		[_cell reloadData:appointment];
		cell = _cell;
	}
	else if ([cell isKindOfClass:[PSAdaptationsAppointmentCell class]])
	{
		PSAdaptationsAppointmentCell *_cell = (PSAdaptationsAppointmentCell *)cell;
		[_cell reloadData:appointment];
		cell = _cell;
	}
	else if ([cell isKindOfClass:[PSMiscellaneousAppointmentCell class]])
	{
		PSMiscellaneousAppointmentCell *_cell = (PSMiscellaneousAppointmentCell *)cell;
		[_cell reloadData:appointment];
		cell = _cell;
	}
    else if ([cell isKindOfClass:[PSGasServicingAppointmentTableViewCell class]])
    {
        PSGasServicingAppointmentTableViewCell *_cell = (PSGasServicingAppointmentTableViewCell *)cell;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        _cell.lblAppointmentTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblAppointmentStatus.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        _cell.lblArrangedBy.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        _cell.lblCertificateExpiry.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        
        _cell.lblAppointmentTitle.text = appointment.appointmentTitle;
        _cell.lblArrangedBy.text = [NSString stringWithFormat:@"%@: %@",
                                    LOC(@"KEY_STRING_ASSIGNED_BY"),
                                    appointment.createdByPerson];
        _cell.lblAppointmentDate.text = [appointment appointmentDateWithStyle:AppointmentDateStyleComplete
                                                                   dateFormat:kDateTimeStyle9];
        
        _cell.lblAppointmentTime.text = [appointment appointmentDateWithStyle:AppointmentDateStyleStartToEnd
                                                                   dateFormat:kDateTimeStyle3];
        if(!isEmpty(appointment.appointmentToProperty.propertyId)){
            _cell.lblAddress.text = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleLong];
        }
        else if(!isEmpty(appointment.appointmentToProperty.blockId)){
            _cell.lblAddress.text = appointment.appointmentToProperty.blockName;
        }
        else{
            _cell.lblAddress.text = appointment.appointmentToProperty.schemeName;
        }
        
        _cell.lblAppointmentStatus.text = [appointment stringWithAppointmentStatus:appointmentStatus];
        
        _cell.imgAppointmentType.image = [self imageForAppointmentType:appointmentType];
        _cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:appointmentStatus];
        
        if ([appointment.appointmentType compare:kAppointmentTypeGas] == NSOrderedSame || [appointment.appointmentType compare:kAppointmentTypeGasVoid] == NSOrderedSame)
        {
            if (!isEmpty(appointment.appointmentToProperty.certificateExpiry))
            {
                _cell.lblCertificateExpiry.hidden = NO;
                _cell.lblCertificateExpiry.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_CERTIFICATE_EXPIRY"), [UtilityClass stringFromDate:appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]];
            }
            else
            {
                _cell.lblCertificateExpiry.hidden = NO;
                _cell.lblCertificateExpiry.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_CERTIFICATE_EXPIRY"), @"N/A"];
            }
            
            if (!isEmpty([appointment.jsgNumber stringValue]))
            {
                _cell.lblJSGas.hidden = NO;
                _cell.lblJSGas.text = [NSString stringWithFormat:@"JS%@ : %@", [appointment.jsgNumber stringValue], appointment.appointmentType];
            }
            else
            {
                _cell.lblJSGas.hidden = YES;
            }
        }
        else
        {
            _cell.lblCertificateExpiry.hidden = YES;
            _cell.lblJSGas.hidden = YES;
        }
        
        __weak typeof(self) weakSelf = self;
        
        if ([appointment isDownloadInProgress])
        {
            [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [_cell showProgressView:YES animated:[self isViewLoaded]];
            [_cell updateProgess:0];
            if ([appointment progressBlock] == NULL)
            {
                [appointment setProgressBlock: ^(long long totalBytesRead, long long totalBytesExpected)
                 {
                     //NSIndexPath *cellIndexPath = [indexPath copy];
                     NSIndexPath *indexPathOfCell = [self.tblAppointments indexPathForCell:cell];
                     if ([indexPathOfCell isEqual:indexPath])
                     {
                         if (totalBytesRead < totalBytesExpected)
                         {
                             [_cell showProgressView:YES animated:[self isViewLoaded]];
                             float progress = (float)totalBytesRead * 100.0 / (float)totalBytesExpected;
                             [_cell updateProgess:progress];
                         }
                         else
                         {
                             [_cell showProgressView:NO animated:[self isViewLoaded]];
                             [weakSelf configureCell:cell atIndexPath:indexPath];
                             
                             [self enableNavigationBarButtons];
                             [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                             _isAnyAppointmentDownloading = FALSE;
                         }
                     }
                 }];
            }
        }
        else if ([appointment isPreparedForOffline] && [appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[appointment getType]])
        {
            [_cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [_cell showProgressView:NO animated:NO];
            _cell.imgAppointmentOfflineStatus.image = [UIImage imageNamed:@"icon_offline_ready"];
        }
        else
        {
            [_cell showProgressView:NO animated:NO];
            _cell.imgAppointmentOfflineStatus.image = nil;
        }
        cell = _cell;
    }
    
    
    else if ([cell isKindOfClass:[PSAlternateFuelTableViewCell class]])
    {
        PSAlternateFuelTableViewCell *_cell = (PSAlternateFuelTableViewCell *)cell;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        _cell.lblAppointmentTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblAppointmentStatus.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        _cell.lblArrangedBy.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        _cell.lblCertificateExpiry.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        
        _cell.lblAppointmentTitle.text = appointment.appointmentTitle;
        _cell.lblArrangedBy.text = [NSString stringWithFormat:@"%@: %@",
                                    LOC(@"KEY_STRING_ASSIGNED_BY"),
                                    appointment.createdByPerson];
        _cell.lblAppointmentDate.text = [appointment appointmentDateWithStyle:AppointmentDateStyleComplete
                                                                   dateFormat:kDateTimeStyle9];
        
        _cell.lblAppointmentTime.text = [appointment appointmentDateWithStyle:AppointmentDateStyleStartToEnd
                                                                   dateFormat:kDateTimeStyle3];
        if(!isEmpty(appointment.appointmentToProperty.propertyId)){
            _cell.lblAddress.text = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleLong];
        }
        else if(!isEmpty(appointment.appointmentToProperty.blockId)){
            _cell.lblAddress.text = appointment.appointmentToProperty.blockName;
        }
        else{
            _cell.lblAddress.text = appointment.appointmentToProperty.schemeName;
        }
        
        _cell.lblAppointmentStatus.text = [appointment stringWithAppointmentStatus:appointmentStatus];
        
        _cell.imgAppointmentType.image = [self imageForAppointmentType:appointmentType];
        _cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:appointmentStatus];
        
        if (!isEmpty(appointment.appointmentToProperty.certificateExpiry))
        {
            _cell.lblCertificateExpiry.hidden = NO;
            _cell.lblCertificateExpiry.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_CERTIFICATE_EXPIRY"), [UtilityClass stringFromDate:appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]];
        }
        else
        {
            _cell.lblCertificateExpiry.hidden = NO;
            _cell.lblCertificateExpiry.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_CERTIFICATE_EXPIRY"), @"N/A"];
        }
        
        if (!isEmpty([appointment.jsgNumber stringValue]))
        {
            _cell.lblJSGas.hidden = NO;
            _cell.lblJSGas.text = [NSString stringWithFormat:@"JS%@ : %@", [appointment.jsgNumber stringValue], appointment.heatingFuel];
        }
        else
        {
            _cell.lblJSGas.hidden = YES;
        }

        
        __weak typeof(self) weakSelf = self;
        
        if ([appointment isDownloadInProgress])
        {
            [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [_cell showProgressView:YES animated:[self isViewLoaded]];
            [_cell updateProgess:0];
            if ([appointment progressBlock] == NULL)
            {
                [appointment setProgressBlock: ^(long long totalBytesRead, long long totalBytesExpected)
                 {
                     //NSIndexPath *cellIndexPath = [indexPath copy];
                     NSIndexPath *indexPathOfCell = [self.tblAppointments indexPathForCell:cell];
                     if ([indexPathOfCell isEqual:indexPath])
                     {
                         if (totalBytesRead < totalBytesExpected)
                         {
                             [_cell showProgressView:YES animated:[self isViewLoaded]];
                             float progress = (float)totalBytesRead * 100.0 / (float)totalBytesExpected;
                             [_cell updateProgess:progress];
                         }
                         else
                         {
                             [_cell showProgressView:NO animated:[self isViewLoaded]];
                             [weakSelf configureCell:cell atIndexPath:indexPath];
                             
                             [self enableNavigationBarButtons];
                             [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                             _isAnyAppointmentDownloading = FALSE;
                         }
                     }
                 }];
            }
        }
        else if ([appointment isPreparedForOffline] && [appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[appointment getType]])
        {
            [_cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [_cell showProgressView:NO animated:NO];
            _cell.imgAppointmentOfflineStatus.image = [UIImage imageNamed:@"icon_offline_ready"];
        }
        else
        {
            [_cell showProgressView:NO animated:NO];
            _cell.imgAppointmentOfflineStatus.image = nil;
        }
        cell = _cell;
    }
    
    
    else if ([cell isKindOfClass:[PSOilAppointmentTableViewCell class]])
    {
        PSOilAppointmentTableViewCell *_cell = (PSOilAppointmentTableViewCell *)cell;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        _cell.lblAppointmentTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblAppointmentStatus.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        _cell.lblArrangedBy.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        _cell.lblCertificateExpiry.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        
        _cell.lblAppointmentTitle.text = appointment.appointmentTitle;
        _cell.lblArrangedBy.text = [NSString stringWithFormat:@"%@: %@",
                                    LOC(@"KEY_STRING_ASSIGNED_BY"),
                                    appointment.createdByPerson];
        _cell.lblAppointmentDate.text = [appointment appointmentDateWithStyle:AppointmentDateStyleComplete
                                                                   dateFormat:kDateTimeStyle9];
        
        _cell.lblAppointmentTime.text = [appointment appointmentDateWithStyle:AppointmentDateStyleStartToEnd
                                                                   dateFormat:kDateTimeStyle3];
        if(!isEmpty(appointment.appointmentToProperty.propertyId)){
            _cell.lblAddress.text = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleLong];
        }
        else if(!isEmpty(appointment.appointmentToProperty.blockId)){
            _cell.lblAddress.text = appointment.appointmentToProperty.blockName;
        }
        else{
            _cell.lblAddress.text = appointment.appointmentToProperty.schemeName;
        }
        
        _cell.lblAppointmentStatus.text = [appointment stringWithAppointmentStatus:appointmentStatus];
        
        _cell.imgAppointmentType.image = [self imageForAppointmentType:appointmentType];
        _cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:appointmentStatus];
        
        if (!isEmpty(appointment.appointmentToProperty.certificateExpiry))
        {
            _cell.lblCertificateExpiry.hidden = NO;
            _cell.lblCertificateExpiry.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_CERTIFICATE_EXPIRY"), [UtilityClass stringFromDate:appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]];
        }
        else
        {
            _cell.lblCertificateExpiry.hidden = NO;
            _cell.lblCertificateExpiry.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_CERTIFICATE_EXPIRY"), @"N/A"];
        }
        
        if (!isEmpty([appointment.jsgNumber stringValue]))
        {
            _cell.lblJSGas.hidden = NO;
            _cell.lblJSGas.text = [NSString stringWithFormat:@"JS%@ : %@", [appointment.jsgNumber stringValue], appointment.heatingFuel];
        }
        else
        {
            _cell.lblJSGas.hidden = YES;
        }
        
        __weak typeof(self) weakSelf = self;
        
        if ([appointment isDownloadInProgress])
        {
            [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [_cell showProgressView:YES animated:[self isViewLoaded]];
            [_cell updateProgess:0];
            if ([appointment progressBlock] == NULL)
            {
                [appointment setProgressBlock: ^(long long totalBytesRead, long long totalBytesExpected)
                 {
                     //NSIndexPath *cellIndexPath = [indexPath copy];
                     NSIndexPath *indexPathOfCell = [self.tblAppointments indexPathForCell:cell];
                     if ([indexPathOfCell isEqual:indexPath])
                     {
                         if (totalBytesRead < totalBytesExpected)
                         {
                             [_cell showProgressView:YES animated:[self isViewLoaded]];
                             float progress = (float)totalBytesRead * 100.0 / (float)totalBytesExpected;
                             [_cell updateProgess:progress];
                         }
                         else
                         {
                             [_cell showProgressView:NO animated:[self isViewLoaded]];
                             [weakSelf configureCell:cell atIndexPath:indexPath];
                             
                             [self enableNavigationBarButtons];
                             [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                             _isAnyAppointmentDownloading = FALSE;
                         }
                     }
                 }];
            }
        }
        else if ([appointment isPreparedForOffline] && [appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[appointment getType]])
        {
            [_cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [_cell showProgressView:NO animated:NO];
            _cell.imgAppointmentOfflineStatus.image = [UIImage imageNamed:@"icon_offline_ready"];
        }
        else
        {
            [_cell showProgressView:NO animated:NO];
            _cell.imgAppointmentOfflineStatus.image = nil;
        }
        cell = _cell;
    }
    
    
    else if ([cell isKindOfClass:[PSAppointmentCell class]])
    {
        PSAppointmentCell *_cell = (PSAppointmentCell *)cell;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        _cell.lblAppointmentTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblAppointmentStatus.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        _cell.lblArrangedBy.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        _cell.lblCertificateExpiry.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
        
        _cell.lblAppointmentTitle.text = appointment.appointmentTitle;
        _cell.lblArrangedBy.text = [NSString stringWithFormat:@"%@: %@",
                                    LOC(@"KEY_STRING_ASSIGNED_BY"),
                                    appointment.createdByPerson];
        _cell.lblAppointmentDate.text = [appointment appointmentDateWithStyle:AppointmentDateStyleComplete
                                                                   dateFormat:kDateTimeStyle9];
        
        _cell.lblAppointmentTime.text = [appointment appointmentDateWithStyle:AppointmentDateStyleStartToEnd
                                                                   dateFormat:kDateTimeStyle3];
        if(!isEmpty(appointment.appointmentToProperty.propertyId)){
            _cell.lblAddress.text = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleLong];
        }
        else if(!isEmpty(appointment.appointmentToProperty.blockId)){
            _cell.lblAddress.text = appointment.appointmentToProperty.blockName;
        }
        else{
            _cell.lblAddress.text = appointment.appointmentToProperty.schemeName;
        }
        
        _cell.lblAppointmentStatus.text = [appointment stringWithAppointmentStatus:appointmentStatus];
        
        _cell.imgAppointmentType.image = [self imageForAppointmentType:appointmentType];
        _cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:appointmentStatus];
        
        if ([appointment.appointmentType compare:kAppointmentTypeGas] == NSOrderedSame || [appointment.appointmentType compare:kAppointmentTypeGasVoid] == NSOrderedSame)
        {
            if (!isEmpty(appointment.appointmentToProperty.certificateExpiry))
            {
                _cell.lblCertificateExpiry.hidden = NO;
                _cell.lblCertificateExpiry.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_CERTIFICATE_EXPIRY"), [UtilityClass stringFromDate:appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]];
            }
            else
            {
                _cell.lblCertificateExpiry.hidden = YES;
            }
            
            if (!isEmpty([appointment.jsgNumber stringValue]))
            {
                _cell.lblJSGas.hidden = NO;
                _cell.lblJSGas.text = [NSString stringWithFormat:@"JS%@ : %@", [appointment.jsgNumber stringValue], appointment.appointmentType];
            }
            else
            {
                _cell.lblJSGas.hidden = YES;
            }
        }
        else
        {
            _cell.lblCertificateExpiry.hidden = YES;
            _cell.lblJSGas.hidden = YES;
        }
        
        __weak typeof(self) weakSelf = self;
        
        if ([appointment isDownloadInProgress])
        {
            [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [_cell showProgressView:YES animated:[self isViewLoaded]];
            [_cell updateProgess:0];
            if ([appointment progressBlock] == NULL)
            {
                [appointment setProgressBlock: ^(long long totalBytesRead, long long totalBytesExpected)
                 {
                     //NSIndexPath *cellIndexPath = [indexPath copy];
                     NSIndexPath *indexPathOfCell = [self.tblAppointments indexPathForCell:cell];
                     if ([indexPathOfCell isEqual:indexPath])
                     {
                         if (totalBytesRead < totalBytesExpected)
                         {
                             [_cell showProgressView:YES animated:[self isViewLoaded]];
                             float progress = (float)totalBytesRead * 100.0 / (float)totalBytesExpected;
                             [_cell updateProgess:progress];
                         }
                         else
                         {
                             [_cell showProgressView:NO animated:[self isViewLoaded]];
                             [weakSelf configureCell:cell atIndexPath:indexPath];
                             
                             [self enableNavigationBarButtons];
                             [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
                             _isAnyAppointmentDownloading = FALSE;
                         }
                     }
                 }];
            }
        }
        else if ([appointment isPreparedForOffline] && [appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[appointment getType]])
        {
            [_cell setSelectionStyle:UITableViewCellSelectionStyleGray];
            [_cell showProgressView:NO animated:NO];
            _cell.imgAppointmentOfflineStatus.image = [UIImage imageNamed:@"icon_offline_ready"];
        }
        else
        {
            [_cell showProgressView:NO animated:NO];
            _cell.imgAppointmentOfflineStatus.image = nil;
        }
        cell = _cell;
    }
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	CLS_LOG(@"Click on Appointment");
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[self checkResultsEmptiness];
	if(self.isAnyAppointmentDownloading == FALSE)
	{
		if(!_noResultsFound)
		{
            if(![self isIndexPathValid:indexPath]){
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Your appointments list is outdated. Please refresh the list and then select your desired appointment." preferredStyle:UIAlertControllerStyleActionSheet];
                UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated: YES completion: nil];
                return;
            }
			Appointment *appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
            if([self isOutdatedAppointment:appointment]==YES){
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"This appointment is outdated. Please contact the administration to reschedule it." preferredStyle:UIAlertControllerStyleActionSheet];
                UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                }];
                [alertController addAction:okAction];
                [self presentViewController:alertController animated: YES completion: nil];
                return;
            }
			if ([appointment isDownloadInProgress] == NO)
			{
				switch([appointment getType])
				{
					case AppointmentTypePreVoid:
					{
						UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_PreInspection bundle:nil];
						PSVPreInspectionDetailViewController * uiView = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSVPreInspectionDetailViewController class])];
						uiView.appointment = appointment;
						[self.navigationController pushViewController:uiView animated:YES];
						
						break;
					}
					case AppointmentTypeVoidWorkRequired:
					{
						UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_WorksRequired bundle:nil];
						PSVWRAppointmentDetailViewController * uiView = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSVWRAppointmentDetailViewController class])];
						uiView.appointment = appointment;
						[self.navigationController pushViewController:uiView animated:YES];
						break;
					}
					case AppointmentTypeGasCheck:
					case AppointmentTypeElectricCheck:
					{
						UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_GasElectricInspection bundle:nil];
						PSVGEInspectionDetailViewController * uiView = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSVGEInspectionDetailViewController class])];
						[uiView setAppointment:appointment];
						[self.navigationController pushViewController:uiView animated:YES];
						break;
					}
					case AppointmentTypePostVoid:
					{
						UIStoryboard * storyboard = [UIStoryboard storyboardWithName:VT_SB_PostInspection bundle:nil];
						PSVPostInspectionDetailViewController * uiView = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSVPostInspectionDetailViewController class])];
						uiView.appointment = appointment;
						[self.navigationController pushViewController:uiView animated:YES];
						break;
					}
					case AppointmentTypeDefect:
		            {
                        if (![NSStringFromClass([self.navigationController.topViewController class]) isEqualToString:NSStringFromClass([PSDAppointmentDetailViewController class])]) {
                            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:D_SB_Defect bundle:nil];
                            PSDAppointmentDetailViewController * uiView = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSDAppointmentDetailViewController class])];
                            [uiView setAppointment:appointment];
                            [self.navigationController pushViewController:uiView animated:YES];
                        }
			            break;
		            }
					default:
						if (![NSStringFromClass([self.navigationController.topViewController class]) isEqualToString:NSStringFromClass([PSAppointmentDetailViewController class])]) {
						self.appointmentDetailViewConroller = [[PSAppointmentDetailViewController alloc] initWithNibName:@"PSAppointmentDetailViewController" bundle:[NSBundle mainBundle]];
						[(PSAppointmentDetailViewController*)self.appointmentDetailViewConroller setAppointment:appointment];
						[(PSAppointmentDetailViewController*)self.appointmentDetailViewConroller setMainAppViewController:self];//week assign
						[self.navigationController pushViewController:self.appointmentDetailViewConroller animated:YES];
						}
				}
			}
		}
	}
}

#pragma mark - Pull To Refresh Methods
- (void)addPullToRefreshControl
{
	if (_refreshHeaderView == nil)
	{
		EGORefreshTableHeaderView *headerView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tblAppointments.bounds.size.height, self.view.frame.size.width, self.tblAppointments.bounds.size.height)];
		headerView.delegate = self;
		[self.tblAppointments addSubview:headerView];
		_refreshHeaderView = headerView;
	}
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource
{
	_reloadingTable = YES;
	[self disableNavigationBarButtons];
	[self.tblAppointments setUserInteractionEnabled:NO];
	[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
															 LOC(@"KEY_ALERT_LOADING"),
															 LOC(@"KEY_STRING_APPOINTMENTS")]];
	[[PSAppointmentsManager sharedManager] refreshAllAppointsments];
	[self.tblAppointments reloadData];
}

- (void)doneLoadingTableViewData
{
	_reloadingTable = NO;
	[self.tblAppointments reloadData];
	[self.tblAppointments setUserInteractionEnabled:YES];
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tblAppointments];
}



#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if(self.isAnyAppointmentDownloading == FALSE)
	{
		[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	}
}

//This method is called to halt the scrolling on any updation that might cause
//insertion or deletion in NSfetchresultcontroller for section
- (void)killScroll
{
	self.tblAppointments.scrollEnabled = NO;
	self.tblAppointments.scrollEnabled = YES;
}


#pragma mark - Scroll Cell Methods
- (void)scrollToTodaySection {
	if (_todaySection >= 0 && _todaySection < [[self.fetchedResultsController sections] count])
	{
		id <NSFetchedResultsSectionInfo> sectionInfo;
		sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:_todaySection];
		NSUInteger rowcount = [sectionInfo numberOfObjects];
		if(rowcount > 0)
		{
			//Found bug.. it is not necessary that today always have the row
			[self.tblAppointments scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:_todaySection] atScrollPosition:UITableViewScrollPositionTop animated:YES];
		}
	}
}
#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
	//Refresh Table View Data Source
	if(IS_NETWORK_AVAILABLE == TRUE)
	{
		[self checkAppointmentsForModifications];
	}
	else
	{
		_modifiedAppointments = nil;
		[self doneLoadingTableViewData];
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																									 message:LOC(@"KEY_ALERT_NO_INTERNET")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		alert.tag = AlertViewTagNoInternet;
		[alert show];
	}
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
	return _reloadingTable; // should return if data source model is reloading
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
	return [NSDate date]; // should return date data source was last changed
}

#pragma mark - Fetched results controller

-(void) safelyPerformFetchOnController{
    @try {
        [self performFetchOnNSFetchedResultsController];
    } @catch (NSException *exception) {
        CLS_LOG(@"Exception occured with name: %@ and Reason: %@", exception.name, exception.reason );
        [self showMessageWithHeader:APP_NAME_STRING andBody:@"Failed to fetch appointments from data store. Please try again. If the issue persists, please contact support"];
    } @finally {
    }
}

- (void)performFetchOnNSFetchedResultsController
{
    dispatch_async(dispatch_get_main_queue(), ^(){
        CLS_LOG(@"performFetchOnNSFetchedResultsController");
        
        NSError *error;
        if (![[self fetchedResultsController] performFetch:&error])
        {
            // Update to handle the error appropriately.
            CLS_LOG(@"fetchedResultsController, Unresolved error %@, %@", error, [error userInfo]);
            [self showMessageWithHeader:APP_NAME_STRING andBody:@"Failed to fetch appointments from data store. Please try again. If the issue persists, please contact support"];
        }
        else
        {
            [self.tblAppointments reloadData];
        }
    });
	
	
}

- (void) checkResultsEmptiness
{
	if([[self.fetchedResultsController fetchedObjects] count] <= 0)
	{
		_noResultsFound = YES;
	}
	else
	{
		_noResultsFound = NO;
	}
	//#warning PSA: FIX NO RESULTS YES BUG HERE (ON RESET SIMULATOR)
}

- (NSFetchedResultsController *)fetchedResultsController
{
	if (_fetchedResultsController != nil)
	{
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	// Edit the entity name as appropriate.
	NSEntityDescription *entity = [NSEntityDescription entityForName:kAppointment inManagedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext];
	[fetchRequest setEntity:entity];
	
	// Set the batch size to a suitable number.
	[fetchRequest setFetchBatchSize:20];
	
	// Edit the sort key as appropriate.
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kAppointmentStartDateTime ascending:YES];
	NSArray *sortDescriptors = @[sortDescriptor];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	
	
	// self.appointmentFilterPredicate = [NSPredicate predicateWithFormat:@"SELF.appointmentToUser.userId == %@", [SettingsClass sharedObject].loggedInUser.userId];
	self.appointmentFilterPredicate = [NSPredicate predicateWithFormat:@"(SELF.appointmentToUser.userName LIKE[cd] SELF.surveyorUserName) AND (appointmentToUser.userId == %@)", [SettingsClass sharedObject].loggedInUser.userId];
	
	[fetchRequest setPredicate:self.appointmentFilterPredicate];
	
	// Edit the section name key path and cache name if appropriate.
	// nil for section name key path means "no sections".
	NSFetchedResultsController*aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext sectionNameKeyPath:kAppointmentDateSectionIdentifier cacheName:nil];
	aFetchedResultsController.delegate = self;
	self.fetchedResultsController = aFetchedResultsController;
	
	[NSFetchedResultsControllerHelper filterAppointmentsWithOptions:[SettingsClass sharedObject].filterOptions
																											 controller:self.fetchedResultsController];
	[self todaySection];
	
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
		CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
	}
	
	return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
	[self.tblAppointments beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo> )sectionInfo
					 atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	[self killScroll];
	switch (type) {
		case NSFetchedResultsChangeInsert:
			[self.tblAppointments insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tblAppointments deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
		case NSFetchedResultsChangeMove:
		case NSFetchedResultsChangeUpdate:
			break;
	}
	_allowRelaodingTableView_ = YES;
	[self todaySection];
	[self.tblAppointments reloadData];
	_allowRelaodingTableView_ = NO;
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
			 atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
			newIndexPath:(NSIndexPath *)newIndexPath {
	UITableView *tableView = self.tblAppointments;
	
	switch (type) {
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self killScroll];
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			//         [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
			[self updateAppointmentDetailOnAppointmentUpdate];
			//          [self.tblAppointments reloadData];
			break;
			
		case NSFetchedResultsChangeMove:
			[self killScroll];
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
	//Reloading table view for removing duplicates and
	_allowRelaodingTableView_ = YES;
	[self todaySection];
	[self.tblAppointments reloadData];
	_allowRelaodingTableView_ = NO;
	
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	[self.tblAppointments endUpdates];
}

/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */

#pragma mark - Appointmetns Filter View
- (void)addAppointmentsFilterView {
	CGRect frame = self.view.frame;
	CGRect rect = CGRectMake(frame.size.width,
													 frame.origin.y,
													 270,
													 frame.size.height);
	filterViewController = [[PSAppointmentsFilterViewController alloc] initWithNibName:@"PSAppointmentsFilterViewController" bundle:[NSBundle mainBundle]];
	filterViewController.delegate = self;
	filterViewController.view.frame = rect;
	CGRect lblFrame = filterViewController.lblAppVersion.frame;
	lblFrame.size.width = filterViewController.view.frame.size.width;
	lblFrame.origin.x = 0;
	[filterViewController.lblAppVersion setFrame:lblFrame];
	[filterViewController.view setHidden:YES];
	[self.view addSubview:filterViewController.view];
}

- (void)showAppointmentsFilterView {
	__block CGRect frame = filterViewController.view.frame;
	filterViewController.view.hidden = NO;
	frame.origin.x = 50;
	[UIView animateWithDuration:0.5
									 animations: ^{
										 filterViewController.view.frame = frame;
									 }
	 
									 completion: ^(BOOL finished) {
										 isSearching = YES;
										 filterViewController.view.userInteractionEnabled = YES;
										 self.tblAppointments.userInteractionEnabled = NO;
									 }];
}

- (void)hideAppointmentsFilterView {
	__block CGRect frame = filterViewController.view.frame;
	frame.origin.x = self.view.frame.size.width;
	[UIView animateWithDuration:0.5
									 animations: ^{
										 filterViewController.view.frame = frame;
									 }
	 
									 completion: ^(BOOL finished) {
										 isSearching = NO;
										 filterViewController.view.hidden = YES;
										 filterViewController.view.userInteractionEnabled = NO;
										 self.tblAppointments.userInteractionEnabled = YES;
									 }];
}

#pragma mark - PSAppointmetnsFilterProtocol
- (void)filterAppointmentsWithOptions:(NSArray *)filterOptions
{
	[self killScroll];
	NSMutableArray *filterOptionCopy = [filterOptions mutableCopy];
	//remove completed assignment filter
	//[filterOptionCopy removeObject:@10];
	CLS_LOG(@"Filter Options: %@", filterOptionCopy);
	[SettingsClass sharedObject].filterOptions = [NSArray arrayWithArray:filterOptionCopy];
	[[SettingsClass sharedObject] saveSettings];
	[NSFetchedResultsControllerHelper filterAppointmentsWithOptions:filterOptionCopy
																											 controller:self.fetchedResultsController];
	[self todaySection];
	[self checkResultsEmptiness];
	[self.tblAppointments reloadData];
	[self scrollToTodaySection];
	[self hideAppointmentsFilterView];
	
	for (NSNumber *filterOption in filterOptionCopy)
	{
		switch ([filterOption integerValue])
		{
			case PSFilterOptionViewAll:
			{
				//[[PSAppointmentsManager sharedManager] fetchAllAppointsments];
			}
				break;
			case PSFilterOptionViewToday:
			{
				NSDate *startDate = [[NSDate date] dateAtStartOfDay]; //12:00:00 am current date
				NSDate *endDate = [[[[NSDate date] dateByAddingDays:1] dateAtStartOfDay] dateBySubtractingSeconds:1]; //11:59:59 pm current date
				[[PSAppointmentsManager sharedManager] fetchAllAppointsmentsFromDate:startDate toDate:endDate];
			}
				break;
			case PSFilterOptionViewNextFiveDays:
			{
				NSDate *startDate = [[NSDate date] dateAtStartOfDay]; //12:00:00 am current date
				NSDate *endDate = [[[[NSDate date] dateByAddingDays:5] dateAtStartOfDay] dateBySubtractingSeconds:1]; //11:59:59 PM 5th day next to today
				[[PSAppointmentsManager sharedManager] fetchAllAppointsmentsFromDate:startDate toDate:endDate];
			}
				break;
		}
	}
}

#pragma mark - PSAppointmentsAddressMapsProtocol
- (void)addressTapped:(Appointment *)appointment
{
	PSMapsViewController *mapViewController = [[PSMapsViewController alloc] initWithNibName:@"PSMapsViewController" bundle:[NSBundle mainBundle]];
	
	if(appointment.appointmentToProperty)
	{
        NSString *address = @"";
        NSString *navigationTitle = @"";
        if(!isEmpty(appointment.appointmentToProperty.propertyId)){
            address = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
            navigationTitle = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort];
        }
        else if(!isEmpty(appointment.appointmentToProperty.schemeId)){
            address = appointment.appointmentToProperty.schemeName;
            navigationTitle = appointment.appointmentToProperty.schemeName;
        }
        else if(!isEmpty(appointment.appointmentToProperty.blockId)){
            address = appointment.appointmentToProperty.blockName;
            navigationTitle = appointment.appointmentToProperty.blockName;
        }
		[mapViewController setAddress:address];
		[mapViewController setNavigationTitle:navigationTitle];
	}
	else if(appointment.appointmentToScheme)
	{
		[mapViewController setAddress:[appointment.appointmentToScheme fullAddress]];
		[mapViewController setNavigationTitle:[appointment.appointmentToScheme shortAddress]];
	}
	[self.navigationController pushViewController:mapViewController animated:YES];
}

#pragma mark - PSAppointments FailedToSyncReasonProtocol

- (void)failedReasonButtonTapped:(Appointment *)appointment {
	
}

#pragma mark - Appointment Modifications
-(void) checkAppointmentsForModifications
{
	_modifiedAppointments = [NSArray arrayWithArray:[[PSAppointmentsManager sharedManager] completedAppointments]];
	if (!isEmpty(_modifiedAppointments))
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_WARNING")
																									 message:LOC(@"KEY_ALERT_MODIFIED_APPOINTMENTS")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_STRING_POST_DATA")
																				 otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"),nil];
		alert.tag = AlertViewTagPostAppointmentData;
		[alert show];
	}
	else
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS")
																									 message:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS_MESSAGE")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_ALERT_NO")
																				 otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
		alert.tag = AlertViewTagRefreshAppointments;
		[alert show];
	}
}

#pragma mark - Methods

-(void) showDailySafetyAlert
{
	if ([self isSafetyAlertDisplayedToday]==NO) {
		[self displaySafetyAlert];
		[[SettingsClass sharedObject] setCurrentUserLastAlertDisplayDate:[NSDate date]];
	}
}

-(void) displaySafetyAlert
{
    dispatch_async(dispatch_get_main_queue(), ^(){
       
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:LOC(@"KEY_ALERT_START_OF_DAY_REMINDER")
                                                      message:LOC(@"KEY_ALERT_START_OF_DAY_REMINDER_MESSAGE")
                                                     delegate:self
                                            cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                            otherButtonTitles:nil, nil];
        alert.tag = AlertViewTagSafety;
        [alert show];
        //[self performSelector:@selector(dismissAlert:) withObject:alert afterDelay:5.0f];
    });
	
}

-(void)dismissAlert:(UIAlertView *) alertView
{
	if ([alertView isVisible]==YES) {
		[alertView dismissWithClickedButtonIndex:0 animated:YES];
	}
	
	[[PSEventsManager sharedManager] requestCalendarPermissions];
}

-(BOOL) isSafetyAlertDisplayedToday
{
	// Get Currently logged user last alert display date
  // If alert date is nil then set current date else
	// Compare current system date with last alert display date, if not equal then display alert
	
	BOOL isAlertDisplayedToday;
	NSDate * lastDisplayDateOfAlert = [[SettingsClass sharedObject] getCurrentUserLastAlertDisplayDate];
	
	if (lastDisplayDateOfAlert == nil)
	{
		isAlertDisplayedToday = NO;
	} else
	{
		
		NSDate *currentDate = [NSDate date];
		
		if ([self isSameDay:currentDate otherDay:lastDisplayDateOfAlert] == NO)
		{
			isAlertDisplayedToday = NO;
		}
		else
		{
			isAlertDisplayedToday = YES;
		}
	}
 return isAlertDisplayedToday;
}

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
	NSCalendar* calendar = [NSCalendar currentCalendar];
	
	unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
	NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
	NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
	
	return [comp1 day] == [comp2 day] &&
	[comp1 month] == [comp2 month] &&
	[comp1 year]  == [comp2 year];
}

- (UIImage *)imageForAppointmentType:(AppointmentType)type
{
	UIImage *imgAppointmentType = nil;
	@autoreleasepool
	{
		if(type == AppointmentTypeVoidWorkRequired)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_work"];
		}
		else if(type == AppointmentTypeGasCheck ||
						type == AppointmentTypeElectricCheck)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_check"];
		}
		else if (type == AppointmentTypePreVoid ||
						 type == AppointmentTypePostVoid)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_void"];
		}
		else if (type == AppointmentTypeGas)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_gas"];
		}
		else if (type == AppointmentTypeFault)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_repair"];
		}
		else if (type == AppointmentTypePlanned)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_planned"];
		}
		else if (type == AppointmentTypeAdaptations)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_adaptations"];
		}
		else if (type == AppointmentTypeMiscellaneous)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_miscellaneous"];
		}
		else if (type == AppointmentTypeCondition)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_crt"];
        }
        else if(type == AppointmentTypeGasVoid){
            imgAppointmentType = [UIImage imageNamed:@"icon_void_gas"];
        }
        else if (type == AppointmentTypeDefect)
        {
            imgAppointmentType = [UIImage imageNamed:@"icon_defect"];
		}
        else if (type == AppointmentTypeOilServicing)
        {
            imgAppointmentType = [UIImage imageNamed:@"icon_oil"];
        }
        else if (type == AppointmentTypeAlternativeFuels)
        {
            imgAppointmentType = [UIImage imageNamed:@"icon_alternative"];
        }
	}
	return imgAppointmentType;
}

- (UIImage *)imageForAppointmentCompletionStatus:(AppointmentStatus)status
{
	UIImage *imgAppointmentStatus = nil;
	@autoreleasepool
	{
		if (status == AppointmentStatusComplete)
		{
			imgAppointmentStatus = [UIImage imageNamed:@"icon_completed"];
		}
		else if (status == AppointmentStatusNoEntry)
		{
			imgAppointmentStatus = [UIImage imageNamed:@"icon_completed"];
		}
		else if (status == AppointmentStatusFinished)
		{
			imgAppointmentStatus = [UIImage imageNamed:@"icon_completed"];
		}
		else if (status == AppointmentStatusInProgress)
		{
			imgAppointmentStatus = [UIImage imageNamed:@"icon_progress"];
		}
		else if (status == AppointmentStatusPaused)
		{
			imgAppointmentStatus = [UIImage imageNamed:@"icon_pause"];
		}
		else if (status == AppointmentStatusNotStarted)
		{
			imgAppointmentStatus = [UIImage imageNamed:@"icon_notepad"];
		}
	}
	return imgAppointmentStatus;
}

- (void) disableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons)
	{
		button.enabled = NO;
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = NO;
	}
	
}

- (void) enableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons)
	{
		button.enabled = YES;
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = YES;
	}
}

- (void) todaySection
{
	id <NSFetchedResultsSectionInfo> sectionInfo;
	
	NSString *currentDateString;
	NSString *tomorrowDateString;
	NSString * dayAfterTomorrowDateString;
	NSInteger tomorrowSection = -1;
	NSInteger dayAfterTomorrowSection = -1;
	
	currentDateString = [UtilityClass stringFromDate:[NSDate date] dateFormat:kDateTimeStyle18];
	tomorrowDateString = [UtilityClass stringFromDate:[[NSDate date] dateByAddingDays:1] dateFormat:kDateTimeStyle18];
	dayAfterTomorrowDateString = [UtilityClass stringFromDate:[[NSDate date] dateByAddingDays:2] dateFormat:kDateTimeStyle18];
	
	for (int sectionNumber = 0; sectionNumber < [[_fetchedResultsController sections] count]; ++sectionNumber)
	{
		sectionInfo = [[_fetchedResultsController sections] objectAtIndex:sectionNumber];
		NSArray *sectionLabels = [[sectionInfo name] componentsSeparatedByString:@","];
		NSString *dateString = [sectionLabels objectAtIndex:1];
		if ([[dateString trimWhitespace] isEqualToString:[currentDateString trimWhitespace]])
		{
			_todaySection = sectionNumber;
		}
		else if ([[dateString trimWhitespace] isEqualToString:[tomorrowDateString trimWhitespace]])
		{
			tomorrowSection = sectionNumber;
		}
		else if ([[dateString trimWhitespace] isEqualToString:[dayAfterTomorrowDateString trimWhitespace]])
		{
			dayAfterTomorrowSection = sectionNumber;
		}
	}
	if (_todaySection == - 1)
	{
		if (tomorrowSection > - 1) {
			_todaySection = tomorrowSection;
		}
		else if (dayAfterTomorrowSection > - 1)
		{
			_todaySection = dayAfterTomorrowSection;
		}
	}
}

#pragma mark - Core Data Update Events
- (void) updateAppointmentDetailOnAppointmentUpdate {
	if (self.appointmentDetailViewConroller != nil)
	{
		if([self.appointmentDetailViewConroller respondsToSelector:@selector(onAppointmentObjectUpdate)])
		{
			[self.appointmentDetailViewConroller onAppointmentObjectUpdate];
		}
	}
}

#pragma mark - Legacy support
-(BOOL) isIndexPathValid:(NSIndexPath *) indexPath {
    BOOL isValid = NO;
    if([[self.fetchedResultsController sections] count]>indexPath.section){
        id <NSFetchedResultsSectionInfo> sectionInfo;
        sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:indexPath.section];
        NSInteger rows = [sectionInfo numberOfObjects];
        if(rows>indexPath.row){
            isValid = YES;
        }
        else{
            isValid = NO;
        }
    }
    else{
        isValid = NO;
    }
    return isValid;
}
-(BOOL) isOutdatedAppointment:(Appointment *) appointment{
    if(isEmpty(appointment.creationDate)){
        return YES;
    }
    NSArray *worksRequired = appointment.appointmentToVoidData.voidDataToWorkRequired.allObjects;
    for(WorkRequired *requiredWork in worksRequired){
        
        if([requiredWork.repairId integerValue]==0 && isEmpty(requiredWork.repairDescription)){
            return YES;
        }
        else if([requiredWork.repairId integerValue]==0 && !isEmpty(requiredWork.repairDescription)){
            if(![requiredWork.repairDescription isEqualToString:@"Standard Void Works"] && ![requiredWork.repairDescription isEqualToString:@"Sani Clean"] ){
                return YES;
            }
        }
    }
    return NO;
}


@end
