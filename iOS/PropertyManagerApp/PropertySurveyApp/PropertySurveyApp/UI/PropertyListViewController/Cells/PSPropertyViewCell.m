//
//  PSPropertyViewCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 28/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSPropertyViewCell.h"
#import "PropertyPicture+MWPhoto.h"

@implementation PSPropertyViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
        // Initialization code
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    }
    
    return self;
}


-(void)onPictureDefault:(NSNotification*) notification
{
    PropertyPicture * picture=notification.object;
    
    [self.imgProperty setImageWithURL:[NSURL URLWithString:picture.imagePath] andAddBorder:YES];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) reloadData:(SProperty*) property
{
 
    if (!isEmpty(property)) {
        
        self.lblAddress.hidden = NO;
        self.lblExpiryDate.hidden = NO;
        self.lblExpiryTitle.hidden = NO;
        self.imgProperty.hidden = NO;
        self.imgSeperator.hidden = NO;
        self.lblAddress.text = [property SPropertyAddress];
        self.lblName.text = [property fullName];
        self.lblExpiryDate.text = [UtilityClass stringFromDate:property.certificateExpiry dateFormat:kDateTimeStyle8];
        
        if (isEmpty(property.certificateExpiry))
        {
            self.lblExpiryDate.hidden = YES;
            self.lblExpiryTitle.hidden = YES;
        }
        else
        {
            self.lblExpiryDate.hidden = NO;
            self.lblExpiryTitle.hidden = NO;
        }
    }
    
    Property * propertyObj= [[PSCoreDataManager sharedManager] propertyWithIdentifier:property.propertyId];
    
    UIImage * image=[propertyObj.defaultPicture underlyingImage];
    if (image) {
        
        [self.imgProperty setImage: image];
        
    }else
    {
        [self.imgProperty setImageWithURL:[NSURL URLWithString:propertyObj.defaultPicture.imagePath] andAddBorder:YES];
    }
    
    
    
    self.lblName.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
    self.lblAddress.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblExpiryDate.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
}

-(void) dealloc
{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

@end
