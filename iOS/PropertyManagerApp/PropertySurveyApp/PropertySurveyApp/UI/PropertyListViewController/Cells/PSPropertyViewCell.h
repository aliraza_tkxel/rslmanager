//
//  PSPropertyViewCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 28/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSPropertyViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet FBRImageView *imgProperty;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblExpiryDate;
@property (weak, nonatomic) IBOutlet UILabel *lblExpiryTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgSeperator;

-(void) reloadData:(SProperty*) property;

@end
