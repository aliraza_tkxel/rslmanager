//
//  PSPropertyListViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 28/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSPropertyViewCell;
@class Property;

@protocol PSPropertyListProtocol <NSObject>
@required

/*!
 @discussion
 Method didReceivePageInformation Called upon when page information is received in server response.
 */
- (void) didReceivePageInformation:(NSDictionary *)pageInfo;
@end

@interface PSPropertyListViewController : PSCustomViewController<PSPropertyListProtocol, UITextFieldDelegate,  NSFetchedResultsControllerDelegate>

@property (strong,  nonatomic) NSPredicate *propertyFilterPredicate;
@property (strong,  nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong,  nonatomic) NSFetchRequest *fetchRequest;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (weak,    nonatomic) IBOutlet PSPropertyViewCell *propertyViewCell;
@property (strong,  nonatomic) NSMutableArray *filteredResultsArray;
@property (strong,  nonatomic) NSMutableArray *propertyArray;
@property (strong,  nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UIView *searchBarView;
@property (strong,  nonatomic) UIButton *searchBarCancelButton;
@property (strong,  nonatomic) NSString *searchString;
@property (strong,  nonatomic) NSMutableDictionary *parameterDictionary;

- (IBAction)onClickCancelSearch:(id)sender;
- (IBAction)onClickSearch:(id)sender;
- (IBAction)resignFirstResponder:(id)sender;
- (IBAction)onClickClearSearch:(id)sender;
- (IBAction)onClickBackButton:(id)sender;
@end
