//
//  PSLoginViewController.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSLoginViewController : PSCustomViewController <UIGestureRecognizerDelegate,CoreRESTServiceDelegate,UIAlertViewDelegate> {
    BOOL isKeyboardShown;
}

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UISwitch *swtStayLoggedIn;
@property (weak, nonatomic) IBOutlet UILabel *lblStayLoggedIn;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)loginMe:(id)sender;
- (IBAction)resignFirstResponder:(id)sender;
- (IBAction)onClickForgotPassword:(id)sender;
- (IBAction)onClickChangePassword:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *upDateView;

@end
