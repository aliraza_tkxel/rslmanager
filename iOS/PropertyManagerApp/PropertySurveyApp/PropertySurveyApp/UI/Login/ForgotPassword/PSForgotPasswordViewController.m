//
//  PSForgotPasswordViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 09/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSForgotPasswordViewController.h"

@interface PSForgotPasswordViewController ()
{
    NSString *_email;
}

@end

@implementation PSForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setFrame:[[UIScreen mainScreen] bounds]];
    
    UIImage* bgImage = nil;
    if(IS_IPHONE_5)
    {
        bgImage = [UIImage imageNamed:@"LoginBG-568h"];
        self.containerView.center = CGPointMake(self.containerView.center.x, self.view.center.y + 70);
    }
    else
    {
        bgImage = [UIImage imageNamed:@"LoginBG"];
        self.containerView.center = self.view.center;
    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    UIImageView* bgImageView = [[UIImageView alloc] initWithImage:bgImage];
    [bgImageView setFrame:[[UIScreen mainScreen] bounds]];
    [bgImageView setContentMode:UIViewContentModeScaleAspectFit];
    [bgImageView setBackgroundColor:[UIColor clearColor]];
    [bgImageView setTag:kBackgroundImageTag];
    
    [self.view insertSubview:bgImageView atIndex:0];
    [self.txtEmail setTextColor: UIColorFromHex(TEXT_FIELD_TEXT_COLOUR)];
    [self.txtEmail setDelegate:self];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapView:)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    isKeyboardShown = NO;
    
    CGRect footerRect = [[UIScreen mainScreen] bounds];
    footerRect.origin.y = footerRect.size.height - 95;
    footerRect.size.height = 93;
    
    CGRect containerRect = self.containerView.frame;
    containerRect.origin.y = footerRect.origin.y - containerRect.size.height;
  
    if(!IS_IPHONE_5)
    {
        containerRect.origin.y += 70;
        footerRect.origin.y -= 15;
    }
    [self.footerView setFrame:footerRect];
    [self.containerView setFrame:containerRect];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [[UILabel appearanceWhenContainedIn:[UITextField class], nil] setTextColor:[UIColor whiteColor]];
    [self registerNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewTagForgotPassword)
    {
        if(buttonIndex == alertView.cancelButtonIndex)
        {
            [self popToRootViewController];
        }
    }
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    BOOL shouldReceiveTouch = YES;
    if(touch.view == self.txtEmail )
    {
        shouldReceiveTouch = NO;
    }
    return shouldReceiveTouch;
}

- (void) onTapView:(id)sender
{
    if(isKeyboardShown)
    {
        //Will Resign All Responders on the Screen.
        [self.view endEditing:YES];
    }
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.selectedControl = textField;
    isKeyboardShown = YES;
    // [self scrollToRectOfSelectedControl];
    [self performSelector:@selector(keyboardWillShow:) withObject:nil];
    [self performSelector:@selector(scrollToRectOfSelectedControl) withObject:nil afterDelay:0.25];
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    _email = textField.text;
}

#pragma mark - UIKeyboard Show/Hide Notifications
- (void)keyboardWillHide:(NSNotification *)n
{
    CGRect viewFrame = CGRectMake(self.view.frame.origin.x,
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    isKeyboardShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    if (isKeyboardShown) {
      //  return;
    }
    
    CGFloat offset = 0;
    if (IS_IPHONE_5)
    {
        offset = -50;
    }
    else
    {
        offset = -90;
    }
    CGRect viewFrame = CGRectMake(self.view.frame.origin.x,
                                  offset,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
    isKeyboardShown = YES;
}

#pragma mark - IBActions
- (IBAction)onClickSubmitBtn:(id)sender
{
    [self validateForm];
}

- (IBAction)onClickTryAgain:(id)sender
{
    [self popToRootViewController];
}

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onForgotPasswordSuccessNotificationReceive:) name:kForgotPasswordSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onForgotPasswordFailureNotificationReceive:) name:kForgotPasswordFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kForgotPasswordSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kForgotPasswordFailureNotification object:nil];
}

- (void) onForgotPasswordSuccessNotificationReceive:(NSNotification *)notification
{
    [self hideActivityIndicator];
    NSDictionary *responseDictionary = [notification object];
    NSString *message = @"";
    if (!isEmpty(responseDictionary))
    {
        message = [responseDictionary valueForKey:@"message"];
    }
    else
    {
        message = @"Failed to send Email";
    }
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LOC(@"KEY_ALERT_SUCCESS")
                                                     message:message
                                                    delegate:self
                                           cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                           otherButtonTitles:nil];
    alert.tag = AlertViewTagForgotPassword;
    [alert show];
}

- (void) onForgotPasswordFailureNotificationReceive:(NSNotification *)notification
{
    [self hideActivityIndicator];
    NSDictionary *responseDictionary = [notification object];
    NSString *message = @"";
    if (!isEmpty(responseDictionary))
    {
        message = [responseDictionary valueForKey:@"message"];
    }
    else
    {
        message = @"Failed to update Password";
    }
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                     message:message
                                                    delegate:self
                                           cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                           otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Private Methods
- (void) validateForm
{
    NSString *alertMessage;
    BOOL isValid = YES;
    if (isEmpty(_email))
    {
        isValid = NO;
        alertMessage = [NSString stringWithFormat:@"Please enter %@.", [self.txtEmail.placeholder lowercaseString]];
    
    }
    else if(![UtilityClass isEmailValid:_email])
    {
        isValid = NO;
        alertMessage = LOC(@"KEY_ALERT_INVALID_EMAIL");
    }
    if (!isValid)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                       message:alertMessage
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
    }
    else
    {
        [self requestEmailForPassword];
    }

}

- (void) requestEmailForPassword
{
    [self showActivityIndicator:LOC(@"KEY_STRING_PROCESSING")];
    NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
    [requestParameters setObject:_email forKey:@"emailAddress"];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[PSAuthenticator sharedAuthenticator] forgotPassword:requestParameters];
        
    });
}

/*- (void)layoutSubviews
{
   [[UILabel appearanceWhenContainedIn:[UITextField class], nil] setTextColor:[UIColor whiteColor]];
}*/

- (IBAction)didEndOnExit:(id)sender
{
    [self.view endEditing:YES];
}
@end
