//
//  PSForgotPasswordViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 09/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSForgotPasswordViewController : PSCustomViewController  <UIGestureRecognizerDelegate, UITextFieldDelegate>
{
     BOOL isKeyboardShown;
}
@property (strong, nonatomic) IBOutlet UIView *containerView;
- (IBAction)onClickSubmitBtn:(id)sender;
- (IBAction)onClickTryAgain:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIView *footerView;
- (IBAction)didEndOnExit:(id)sender;

@end
