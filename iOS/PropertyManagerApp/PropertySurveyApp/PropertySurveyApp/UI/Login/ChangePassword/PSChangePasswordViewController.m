//
//  PSChangePasswordViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 09/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSChangePasswordViewController.h"
#import "PSChangePasswordCell.h"

@interface PSChangePasswordViewController ()
{
    NSString *_userName;
    NSString *_oldPassword;
    NSString *_newPassword;
    NSString *_confirmNewPassword;
}

@end

#define kNumberOfRows 4
#define kRowUserName 0
#define kRowOldPassword 1
#define kRowNewPassword 2
#define kRowConfirmNewPassword 3

#define kRowCellHeight 60


static NSString *changePasswordCellIdentifier = @"changePasswordCellIdentifier";

@implementation PSChangePasswordViewController
@synthesize selectedControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage* bgImage = nil;
    if(IS_IPHONE_5)
    {
        bgImage = [UIImage imageNamed:@"LoginBG-568h"];
        self.tableView.center = CGPointMake(self.view.center.x, self.view.center.y + 30);
    }
    else
    {
        bgImage = [UIImage imageNamed:@"LoginBG"];
        
    }
  //  self.tableView.center = CGPointMake(self.view.center.x, self.view.center.y + 60);
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    UIImageView* bgImageView = [[UIImageView alloc] initWithImage:bgImage];
    [bgImageView setFrame:[[UIScreen mainScreen] bounds]];
    [bgImageView setContentMode:UIViewContentModeScaleAspectFit];
    [bgImageView setBackgroundColor:[UIColor clearColor]];
    [bgImageView setTag:kBackgroundImageTag];
    
    [self.view insertSubview:bgImageView atIndex:0];
    CGRect footerRect = [[UIScreen mainScreen] bounds];
    footerRect.origin.y = footerRect.size.height - 100;
    footerRect.size.height = 93;
    [self.footerView setFrame:footerRect];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
    CGRect tableViewRect = [[UIScreen mainScreen] bounds];
    tableViewRect.origin.y = self.view.center.y - 15 ;
    if(OS_VERSION >= 7.0)
    {
        tableViewRect.origin.y -= 15;
    }
    tableViewRect.size.height -=  footerRect.size.height + self.view.center.y;
    [self.tableView setFrame:tableViewRect];
    
    CGRect labelRect = self.lblChangePassword.frame;
    labelRect.origin.y = tableViewRect.origin.y - labelRect.size.height - 15;
    [self.lblChangePassword setFrame:labelRect];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapView:)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    isKeyboardShown = NO;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI TableView Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kRowCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return kNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    PSChangePasswordCell *_cell = [self createCellWithIdentifier:changePasswordCellIdentifier];
    [self configureCell:&_cell atIndexPath:indexPath];
    cell = _cell;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
    if([identifier isEqualToString:changePasswordCellIdentifier])
    {
        PSChangePasswordCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:changePasswordCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSChangePasswordCell" owner:self options:nil];
            _cell = self.changePasswordCell;
            self.changePasswordCell = nil;
        }
        return _cell;
    }
    return nil;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSChangePasswordCell class]])
    {
        PSChangePasswordCell *_cell = (PSChangePasswordCell *)*cell;
        NSString *text = @"";
        switch (indexPath.row)
        {
               // [[UILabel appearanceWhenContainedIn:[UITextField class], nil] setTextColor:[UIColor whiteColor]];
            case kRowUserName:
                text = _userName;
                break;
            case kRowOldPassword:
                text = _oldPassword;
                break;
            case kRowNewPassword:
                text = _newPassword;
                break;
            case kRowConfirmNewPassword:
                text = _confirmNewPassword;
                break;
            default:
                break;
        }
        
        [_cell reloadData:indexPath withText:text];
        [_cell.txtField setDelegate:self];
    }
    
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    BOOL shouldReceiveTouch = YES;
    return shouldReceiveTouch;
}

- (void) onTapView:(id)sender
{
    if(isKeyboardShown)
    {
        //Will Resign All Responders on the Screen.
        [self.view endEditing:YES];
    }
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewTagChangePassword)
    {
        if(buttonIndex == alertView.cancelButtonIndex)
        {
            [self popToRootViewController];
        }
    }
}

#pragma mark - Scroll Text Field Methods
- (void)scrollToRectOfSelectedControl {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - UIScrollView Delegates
- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignAllResponders];
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.selectedControl = textField;
    isKeyboardShown = YES;
   // [self scrollToRectOfSelectedControl];
    [self performSelector:@selector(keyboardWillShow:) withObject:nil];
    [self performSelector:@selector(scrollToRectOfSelectedControl) withObject:nil afterDelay:0.25];
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case kRowUserName:
            _userName = textField.text;
            break;
        case kRowOldPassword:
            _oldPassword = textField.text;
            break;
        case kRowNewPassword:
            _newPassword = textField.text;
            break;
        case kRowConfirmNewPassword:
            _confirmNewPassword = textField.text;
            break;
        default:
            break;
    }
    isKeyboardShown = NO;
}

#pragma IBActions
- (IBAction)onClickChangePassword:(id)sender
{
    [self validateForm];
}

- (IBAction)onClickLoginAgain:(id)sender
{
    [self popToRootViewController];
}

#pragma mark - UIKeyboard Show/Hide Notifications
- (void)keyboardWillHide:(NSNotification *)n
{
    CGRect viewFrame = CGRectMake(self.view.frame.origin.x,
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    isKeyboardShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    /* if (isKeyboardShown) {
     return;
     }*/

    NSInteger tag = self.selectedControl.tag;
    if (tag > 2) {
        tag -= 1;
    }
    CGFloat offset = -1 * (tag * 50);
    if (offset >= 0)
    {
        offset = -50;
    }
    CGRect viewFrame = CGRectMake(self.view.frame.origin.x,
                                  offset,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    isKeyboardShown = YES;
}

#pragma mark - Notifications
- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdatePasswordSuccessNotificationReceive:) name:kUpdatePasswordSuccessNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdatePasswordFailureNotificationReceive:) name:kUpdatePasswordFailureNotification object:nil];

}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdatePasswordSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdatePasswordFailureNotification object:nil];
}

- (void) onUpdatePasswordSuccessNotificationReceive:(NSNotification *)notification
{
    [self hideActivityIndicator];
    NSDictionary *responseDictionary = [notification object];
    NSString *message = @"";
    if (!isEmpty(responseDictionary))
    {
        message = [responseDictionary valueForKey:@"message"];
    }
    else
    {
        message = @"Failed to update Password";
    }

    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LOC(@"KEY_ALERT_SUCCESS")
                                                     message:message
                                                    delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                           otherButtonTitles:nil];
    alert.tag = AlertViewTagChangePassword;
    [alert show];
}

- (void) onUpdatePasswordFailureNotificationReceive:(NSNotification *)notification
{
    [self hideActivityIndicator];
    NSDictionary *responseDictionary = [notification object];
    NSString *message = @"";
    if (!isEmpty(responseDictionary))
    {
        message = [responseDictionary valueForKey:@"message"];
    }
    else
    {
        message = @"Failed to update Password";
    }
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                     message:message
                                                    delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                           otherButtonTitles:nil];
   // alert.tag = AlertViewTagChangePassword;
    [alert show];
}

#pragma mark - Private Methods
- (void) validateForm
{
    NSString *alertMessage = nil;
    NSString *emptyField;
    BOOL inValid = NO;

    for (int cellNumber = 0; cellNumber < kNumberOfRows; ++cellNumber)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellNumber inSection:0];
        UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
        emptyField = nil;
        alertMessage = nil;
        inValid = NO;
        
        if ([cell  isKindOfClass:[PSChangePasswordCell class]]) {
            PSChangePasswordCell *_cell = (PSChangePasswordCell *)cell;
            if (![_cell isValid])
            {
                emptyField = [_cell.txtField.placeholder lowercaseString];
                inValid = YES;
            }
        }
        if (inValid)
        {
            alertMessage = [NSString stringWithFormat:@"Please enter %@.", emptyField];
            break;
        }
        else
        {
            if (![_newPassword isEqualToString:_confirmNewPassword])
            {
                alertMessage = @"New password and confirm new password do not match";
                inValid = YES;
                break;
            }
            else if ([_oldPassword isEqualToString:_newPassword])
            {
                alertMessage = @"Old password and new password must be different";
                inValid = YES;
                break;
            }
        }
    }

    if (inValid)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                       message:alertMessage
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
    }
    else
    {
        [self updatePassword];
    }
}

- (void) updatePassword
{
    [self showActivityIndicator:[NSString stringWithFormat:@"%@",
                                LOC(@"KEY_STRING_PROCESSING")]];
    NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
    [requestParameters setObject:_userName forKey:@"username"];
    [requestParameters setObject:_oldPassword forKey:@"oldPassword"];
    [requestParameters setObject:_newPassword forKey:@"newPassword"];
 //   [requestParameters setObject:[SettingsClass sharedObject].deviceToken forKey:@"devicetoken"];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       [[PSAuthenticator sharedAuthenticator] updatePassword:requestParameters];
        
    });
}
@end
