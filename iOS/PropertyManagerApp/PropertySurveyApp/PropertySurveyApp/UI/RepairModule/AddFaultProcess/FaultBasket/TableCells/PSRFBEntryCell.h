//
//  PSRFBEntryCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaultReported.h"

@interface PSRFBEntryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;

@property (strong, nonatomic) FaultReported * faultReported;

-(void)configureCell:(FaultReported *)faultReported;

@end
