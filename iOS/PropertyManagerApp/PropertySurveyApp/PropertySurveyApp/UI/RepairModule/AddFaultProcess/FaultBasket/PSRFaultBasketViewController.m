//
//  PSRFaultBasketViewController.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRFaultBasketViewController.h"
#import "PSRSearchFaultViewController.h"
#import "PSRFBEntryCell.h"
#import "PSRFaultBasketManager.h"

@interface PSRFaultBasketViewController ()
{
	PSRFaultBasketManager * _manager;
}
@end

@implementation PSRFaultBasketViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self loadNavigationonBarItems];
	[self initData];
	self.tableView.allowsMultipleSelectionDuringEditing = NO;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)initData
{
	_manager = [[PSRFaultBasketManager alloc]init:self.appointment];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *addButton = [[PSBarButtonItem alloc] initWithCustomStyle:NULL
																																			style:PSBarButtonItemStyleAdd
																																		 target:self
																																		 action:@selector(onClickAddButton)];
	
	PSBarButtonItem *nextButton = [[PSBarButtonItem alloc] initWithCustomStyle:@"Save"
																																			 style:PSBarButtonItemStyleDefault
																																			target:self
																																			action:@selector(onClickSaveButton)];
	
	[self setRightBarButtonItems:[NSArray arrayWithObjects:addButton, nextButton, nil]];
	[self setTitle:@"Fault Basket"];
}

#pragma mark Navigation Bar Button Selectors
-(void)onClickAddButton
{
	CLS_LOG(@"onClickAddButton");
	UIStoryboard * storyboard = [UIStoryboard storyboardWithName:RM_SB_AddFaultProcess bundle:nil];
	PSRSearchFaultViewController * uiView = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSRSearchFaultViewController class])];
	uiView.appointment = self.appointment;
	uiView.viewComingFrom = ViewComingFromFaultBasket;
	[self.navigationController pushViewController:uiView animated:YES];
}

-(void)onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

-(void)onClickSaveButton
{
	CLS_LOG(@"onClickNextButton");
	[_manager saveContext];
	NSArray* viewControllers = self.navigationController.viewControllers;
	UIViewController * viewController = nil;
	if ([viewControllers count] > 1 && [self.appointment.appointmentStatus isEqualToString:kJobStatusComplete] == FALSE)
	{
		viewController = [viewControllers objectAtIndex:1];
	}
	else
	{
		viewController = [viewControllers objectAtIndex:0];
	}
	[self.navigationController popToViewController:viewController animated:YES];
}

#pragma mark - UITableView Delegate Methods
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *_cell = nil;
	CLS_LOG(@"row: %zd, section: %zd", indexPath.row, indexPath.section);
	FaultReported * faultReported = [[self.appointment.appointmentToFaultReported allObjects] objectAtIndex:indexPath.row];
	PSRFBEntryCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRFBEntryCell class])];
	[cell configureCell:faultReported];
	_cell = cell;
	[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	return _cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.appointment.appointmentToFaultReported count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Return YES if you want the specified item to be editable.
	return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		//add code here for when you hit delete
		FaultReported * fault = [[self.appointment.appointmentToFaultReported allObjects] objectAtIndex:[indexPath row]];
		[self.appointment removeAppointmentToFaultReportedObject:fault];
		[_manager saveContext];
		[self.tableView reloadData];
	}
}

@end
