//
//  PSRFaultBasketViewController.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"

@interface PSRFaultBasketViewController : PSCustomViewController

@property (strong, nonatomic) Appointment * appointment;

@end
