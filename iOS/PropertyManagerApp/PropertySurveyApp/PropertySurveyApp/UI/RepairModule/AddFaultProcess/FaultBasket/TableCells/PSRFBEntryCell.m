//
//  PSRFBEntryCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRFBEntryCell.h"
#import "FaultDetailList.h"
#import "PSRFaultBasketViewController.h"
#import "FaultAreaList.h"

@implementation PSRFBEntryCell

- (void)awakeFromNib
{
	// Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

-(void)configureCell:(FaultReported *)faultReported
{
	self.faultReported = faultReported;
	self.lblLocation.text = self.faultReported.faultReportedToFaultArea.faultAreaName;
	self.lblDescription.text = self.faultReported.faultReportedToFaultDetail.faultDescription;
}

@end
