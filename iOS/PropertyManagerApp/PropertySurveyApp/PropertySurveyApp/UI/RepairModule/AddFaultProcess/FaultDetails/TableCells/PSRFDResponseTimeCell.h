//
//  PSRFDResponseTImeCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaultDetailList.h"

@interface PSRFDResponseTimeCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblResponseTime;
@property (strong, nonatomic) IBOutlet UILabel *lblResponsePriority;
@property (strong, nonatomic) FaultDetailList * faultDetail;

-(void)configureCell:(FaultDetailList *)faultDetail;

@end
