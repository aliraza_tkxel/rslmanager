//
//  PSRFDRechargeableCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRFDRechargeableCell.h"

@implementation PSRFDRechargeableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{ }
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{ }
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

-(void)configureCell:(FaultDetailList *)faultDetail
{
	self.faultDetail = faultDetail;
	if([self.faultDetail.isRecharge boolValue] == YES)
	{
		self.lblIsRechargeable.text = @"YES";
	}
	else
	{
		self.lblIsRechargeable.text = @"NO";
	}
	self.lblCost.text = [self.faultDetail.gross stringValue];
}

@end
