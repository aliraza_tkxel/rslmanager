//
//  PSRFDRequiredTimeCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRFDProblemDaysCell.h"

@interface PSRFDProblemDaysCell ()
{
	NSArray * _durationList;
}
@end

@implementation PSRFDProblemDaysCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{ }
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{ }
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

- (IBAction)onClickRequiredTime:(id)sender
{
	_durationList = @[@"1", @"2", @"3", @"4", @"5"];
	[ActionSheetStringPicker showPickerWithTitle:@"Duration (Days)"
																					rows:_durationList
															initialSelection:0
																				target:self
																 successAction:@selector(onOptionSelected:element:)
																	cancelAction:nil
																				origin:sender];
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
	self.lblDuration.text = [_durationList objectAtIndex:[selectedIndex integerValue]];
}

@end
