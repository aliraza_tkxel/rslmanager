//
//  PSRFDLocationCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaultDetailList.h"
#import "FaultAreaList.h"

@interface PSRFDLocationCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel * lblFaultLocation;
@property (strong, nonatomic) FaultDetailList * faultDetail;
@property (strong, nonatomic) FaultAreaList * faultArea;

-(void)configureCell:(FaultDetailList *)faultDetail;

@end
