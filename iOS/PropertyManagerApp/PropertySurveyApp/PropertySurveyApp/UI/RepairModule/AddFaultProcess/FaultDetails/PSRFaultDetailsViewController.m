//
//  PSFaultDetailsViewController.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-15.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRFaultDetailsViewController.h"
#import "PSRFaultBasketViewController.h"
#import "PSRFDLocationCell.h"
#import "PSRFDDecriptionCell.h"
#import "PSRFDResponseTimeCell.h"
#import "PSRFDProblemDaysCell.h"
#import "PSRFDRecurringProblemCell.h"
#import "PSRFDRechargeableCell.h"
#import "PSRFDNotesCell.h"
#import "PSRFaultDetailsManager.h"

#define iPSRFDLocationCell 0
#define iPSRFDDecriptionCell 1
#define iPSRFDResponseTimeCell 2
#define iPSRFDRequiredTimeCell 3
#define iPSRFDRecurringProblemCell 4
#define iPSRFDRechargeableCell 5
#define iPSRFDNotesCell 6

@interface PSRFaultDetailsViewController ()
{
	PSRFaultDetailsManager * _manager;
}
@end

@implementation PSRFaultDetailsViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self loadNavigationonBarItems];
	[self initData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)initData
{
	_manager = [[PSRFaultDetailsManager alloc]init:self.appointment];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	PSBarButtonItem *nextButton = [[PSBarButtonItem alloc] initWithCustomStyle:@"Next"
																																			 style:PSBarButtonItemStyleDefault
																																			target:self
																																			action:@selector(onClickNextButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setRightBarButtonItems:[NSArray arrayWithObjects:nextButton, nil]];
	[self setTitle:@"Fault Details"];
}

#pragma mark Navigation Bar Button Selectors
-(void)onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

-(void)onClickNextButton
{
	CLS_LOG(@"onClickNextButton");
	NSIndexPath * indexPath = [NSIndexPath indexPathForRow:iPSRFDRequiredTimeCell inSection:0];
	PSRFDProblemDaysCell * problemDaysCell = (PSRFDProblemDaysCell *)[self.tableView cellForRowAtIndexPath:indexPath];

	indexPath = [NSIndexPath indexPathForRow:iPSRFDNotesCell inSection:0];
	PSRFDNotesCell * notesCell = (PSRFDNotesCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	
	indexPath = [NSIndexPath indexPathForRow:iPSRFDLocationCell inSection:0];
	PSRFDLocationCell * locationCell = (PSRFDLocationCell *)[self.tableView cellForRowAtIndexPath:indexPath];

	if(isEmpty(problemDaysCell.lblDuration.text) == TRUE ||
		 locationCell.faultArea == nil ||
		 isEmpty(notesCell.tvNotes.text) == TRUE)
	{
		UIAlertView * alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_WARNING")
																										message:LOC(@"Location, Fault Notes and Duration are mandatory fields.")
																									 delegate:nil
																					cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																					otherButtonTitles:nil,nil];
		[alert show];
	}
	else
	{
		indexPath = [NSIndexPath indexPathForRow:iPSRFDRecurringProblemCell inSection:0];
		PSRFDRecurringProblemCell * recurringCell = (PSRFDRecurringProblemCell *)[self.tableView cellForRowAtIndexPath:indexPath];
		
		[_manager reportNewFaultDetail:self.faultDetail
												 faultArea:locationCell.faultArea
											 isRecurring:recurringCell.swIsRecurring.on
											 problemDays:problemDaysCell.lblDuration.text
														 notes:notesCell.tvNotes.text];
		
		UIStoryboard * storyboard = [UIStoryboard storyboardWithName:RM_SB_AddFaultProcess bundle:nil];
		PSRFaultBasketViewController * uiView = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSRFaultBasketViewController class])];
		uiView.appointment = self.appointment;
		[self.navigationController pushViewController:uiView animated:YES];
	}
}

#pragma mark - UITableView Delegate Methods
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *_cell = nil;
	CLS_LOG(@"row: %zd, section: %zd", indexPath.row, indexPath.section);
	switch (indexPath.row)
	{
		case iPSRFDLocationCell:
		{
			PSRFDLocationCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRFDLocationCell class])];
			[cell configureCell:self.faultDetail];
			_cell = cell;
			break;
		}
		case iPSRFDDecriptionCell:
		{
			PSRFDDecriptionCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRFDDecriptionCell class])];
			[cell configureCell:self.faultDetail];
			_cell = cell;
			break;
		}
		case iPSRFDResponseTimeCell:
		{
			PSRFDResponseTimeCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRFDResponseTimeCell class])];
			[cell configureCell:self.faultDetail];
			_cell = cell;
			break;
		}
		case iPSRFDRequiredTimeCell:
		{
			PSRFDProblemDaysCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRFDProblemDaysCell class])];
			_cell = cell;
			break;
		}
		case iPSRFDRecurringProblemCell:
		{
			PSRFDRecurringProblemCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRFDRecurringProblemCell class])];
			_cell = cell;
			break;
		}
		case iPSRFDRechargeableCell:
		{
			PSRFDRechargeableCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRFDRechargeableCell class])];
			[cell configureCell:self.faultDetail];
			_cell = cell;
			break;
		}
		case iPSRFDNotesCell:
		{
			PSRFDNotesCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSRFDNotesCell class])];
			_cell = cell;
			break;
		}
		default:
			break;
	}
	[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	[_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	return _cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat rowHeight = 0.0;
	if(indexPath.row == 6)
	{
		rowHeight = 230;
	}
	else
	{
		rowHeight = 44;
	}
	return rowHeight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 7;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

@end
