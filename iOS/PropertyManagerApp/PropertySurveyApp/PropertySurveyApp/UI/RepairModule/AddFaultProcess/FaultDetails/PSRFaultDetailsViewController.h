//
//  PSFaultDetailsViewController.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-15.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "FaultDetailList.h"

@interface PSRFaultDetailsViewController : PSCustomViewController

@property (strong, nonatomic) Appointment * appointment;
@property (strong, nonatomic) FaultDetailList * faultDetail;

@end
