//
//  PSRFDNotesCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-16.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaultDetailList.h"
#import "SZTextView.h"

@interface PSRFDNotesCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SZTextView *tvNotes;

@end
