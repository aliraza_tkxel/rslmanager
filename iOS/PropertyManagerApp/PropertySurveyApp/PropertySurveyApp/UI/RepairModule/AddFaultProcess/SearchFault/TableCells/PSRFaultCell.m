//
//  PSRAddNewFaultCell.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-15.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSRFaultCell.h"
#import "UILabel+Boldify.h"

@implementation PSRFaultCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self)
	{ }
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{ }
	return self;
}

-(void)configureCell:(FaultDetailList *)faultDetail searchedKey:(NSString *)key
{
	_faultDetail = faultDetail;
	self.lblFaultDetail.text = [NSString stringWithFormat:@"%@", _faultDetail.faultDescription];
	[self.lblFaultDetail boldSubstring:key options:NSCaseInsensitiveSearch];
}

@end
