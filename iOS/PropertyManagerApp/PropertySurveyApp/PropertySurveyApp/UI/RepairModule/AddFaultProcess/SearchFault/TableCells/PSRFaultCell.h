//
//  PSRAddNewFaultCell.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-15.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FaultDetailList.h"

@interface PSRFaultCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblFaultDetail;

@property (strong, nonatomic) FaultDetailList * faultDetail;

-(void)configureCell:(FaultDetailList *)faultDetail searchedKey:(NSString *)key;

@end
