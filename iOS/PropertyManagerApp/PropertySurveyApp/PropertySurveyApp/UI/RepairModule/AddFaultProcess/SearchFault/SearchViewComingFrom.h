//
//  PSRSearchViewComingFrom.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-22.
//  Copyright © 2015 TkXel. All rights reserved.
//

typedef NS_ENUM(NSInteger, SearchViewComingFrom)
{
	ViewComingFromJobSheet,
	ViewComingFromFaultBasket,
};
