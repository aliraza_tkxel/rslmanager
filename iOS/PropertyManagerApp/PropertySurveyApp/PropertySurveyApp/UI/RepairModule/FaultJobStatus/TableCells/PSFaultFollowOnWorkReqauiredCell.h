//
//  PSFollowOnWorkReqauiredCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSFaultJobStatusViewController.h"

@interface PSFaultFollowOnWorkReqauiredCell : UITableViewCell
@property (weak,    nonatomic) id<PSFaultJobStatusPickerOptionProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblFollowOn;
@property (strong, nonatomic) IBOutlet UISwitch *swtFollowOn;
- (IBAction)valueChanged:(id)sender;

@end
