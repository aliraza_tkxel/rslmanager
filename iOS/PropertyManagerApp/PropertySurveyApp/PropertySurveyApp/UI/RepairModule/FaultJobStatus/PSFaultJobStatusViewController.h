//
//  PSJobStatusViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextViewCell.h"
#import "PSFaultJobStatusPickerOptionProtocol.h"


@interface PSFaultJobStatusViewController : PSCustomViewController <PSFaultJobStatusPickerOptionProtocol, UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirmAndNew;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;

@property (assign, nonatomic) ViewMode viewMode;
@property (strong, nonatomic) NSMutableArray * headerViewArray;
@property (strong, nonatomic) FaultJobSheet * jobSheet;
@property (strong, nonatomic) PlannedTradeComponent * plannedComponent;
@property (assign, nonatomic) BOOL hasNewFault;

@end
