//
//  PSFaultJobStatusPickerOptionProtocol.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-09-14.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

@protocol PSFaultJobStatusPickerOptionProtocol <NSObject>

@required
/*!
 @discussion
 Method didPickerOptionSelect Called upon selection of any picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerOption;

/*!
 @discussion
 Method didPickerOptionSelect Called upon changing switch value.
 */
- (void) didChangeSwitchOption:(BOOL) valueChanged;

@end
