//
//  PSAddNotesViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddNotesViewController.h"
#import "PSBarButtonItem.h"

@interface PSAddNotesViewController ()
{
@private
	PSTextView *_textView;
}

@end

@implementation PSAddNotesViewController
@synthesize notesText = _notesText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    
	[self.view setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	_textView = [[PSTextView alloc] initWithFrame:self.txtViewNote.frame];
	_textView.delegate = self;
	[_textView setBackgroundColor:[UIColor grayColor]];
	[_textView canBecomeFirstResponder];
	self.txtViewNote = _textView;
	[self.txtViewNote setUserInteractionEnabled:YES];
	[self.txtViewNote setEditable:YES];
	[self.txtViewNote setText:self.notesText];
    [self.txtViewNote setBackgroundColor:[UIColor whiteColor]];
	[self.txtViewNote setDelegate:self];
	[self.txtViewNote setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
	[self.view addSubview:self.txtViewNote];
	[self.txtViewNote setReturnKeyType:UIReturnKeyDefault];
    
	[self.txtViewNote becomeFirstResponder];
    
	[self loadNavigationonBarItems];
}

- (void)setNotesText:(NSString *)notesText {
	_notesText = notesText;
	[self.txtViewNote setText:notesText];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	[self setTxtViewNote:nil];
	[self setTxtViewNote:nil];
}

#pragma mark - Navigation Bar
- (void)loadNavigationonBarItems {
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
	                                                                     style:PSBarButtonItemStyleBack
	                                                                    target:self
	                                                                    action:@selector(onClickBackButton)];
	self.homeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
	                                                         style:PSBarButtonItemStyleHome
	                                                        target:self
	                                                        action:@selector(onClickHomeButton)];

    
	[self setLeftBarButtonItems:@[backButton]];
	[self setRightBarButtonItems:@[ self.homeButton]];
	[self setTitle:@"Notes"];
}

#pragma mark Navigation Bar Button Selectors
- (void)onClickBackButton {
	CLS_LOG(@"onClickBackButton");
    [self.txtViewNote resignFirstResponder];
	if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeSurveyFormNotes:)]) {
		[self.delegate didChangeSurveyFormNotes:self.txtViewNote.text];
	}
    
    [self popOrCloseViewController];
}

- (void)onClickHomeButton {
    CLS_LOG(@"Home Button");
    [self.txtViewNote resignFirstResponder];
	[self.navigationController popToViewController:((PSSurveyViewController *)self.delegate).mainViewController animated:YES];
}

- (void)onClickCameraButton {
	CLS_LOG(@"onClickCameraButton");
    [self popOrCloseViewController];
}

#pragma mark - UITextView Delegate Methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
	self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	//self.notes = _riskNotes;
	[self.view endEditing:YES];
}

- (void)textViewDidChange:(UITextView *)textView {
	CLS_LOG(@"textViewDidChange");
    
	// _riskNotes = textView.text;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	/*if ([text isEqualToString:@"\n"]) {
		[self.selectedControl resignFirstResponder];
		[self.view endEditing:YES];
		return NO;
	}
	else*/
		return YES;
}

@end
