//
//  PSSurveyTextboxCell.h
//  PropertySurveyApp
//
//  Created by TkXel on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSSurveyViewController.h"

@class FormData;
@interface PSSurveyTextboxCell : UITableViewCell <UITextFieldDelegate>

@property (weak,   nonatomic) id<SurveyFormDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UITextField *txtValue;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;

@property (strong, nonatomic) NSString *surveyParamName;
@property (strong, nonatomic) NSDictionary *dataDictionary;

@property (nonatomic, weak) FormData *formData;

- (IBAction)onClickEditButton:(UIButton *)sender;
- (IBAction)onTextFieldDidEndOnExit:(UITextField *)sender;
- (void) loadCellData;

@end
