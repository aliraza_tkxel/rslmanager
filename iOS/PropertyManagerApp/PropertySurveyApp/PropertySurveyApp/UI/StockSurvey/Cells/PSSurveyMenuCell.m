//
//  PSSurveyMenuCell.m
//  PropertySurveyApp
//
//  Created by TkXel on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyMenuCell.h"

@implementation PSSurveyMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
