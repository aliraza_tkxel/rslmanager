//
//  PSSurveyCheckboxCell.m
//  PropertySurveyApp
//
//  Created by TkXel on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyCheckboxCell.h"
#import "PSCheckboxButton.h"

@implementation PSSurveyCheckboxCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
    
	// Configure the view for the selected state
}

- (void)layoutSubviews {
	[super layoutSubviews];
}

- (void)loadCellWithData:(NSDictionary *)dataDictionary {
	if (!self.formDataDictionary) {
		self.formDataDictionary = [NSMutableDictionary dictionary];
	}
    
	self.checkboxOptions = dataDictionary;
	self.itemParamId = [dataDictionary objectForKey:@"itemParamId"];
	self.paramId = [dataDictionary objectForKey:@"paramId"];
    
	NSNumber *surveyItemParamId = [dataDictionary objectForKey:@"itemParamId"];
	NSNumber *surveyParameterId = [dataDictionary objectForKey:@"paramId"];
	NSString *controlType = [dataDictionary objectForKey:@"Type"];
    
	[self.formDataDictionary setObject:surveyItemParamId forKey:@"surveyItemParamId"];
	[self.formDataDictionary setObject:surveyParameterId forKey:@"surveyParameterId"];
	[self.formDataDictionary setObject:controlType forKey:@"controlType"];
    [self.formDataDictionary setObject:self.surveyParamName forKey:@"surveyParamName"];
    
	NSMutableDictionary *selectedJSON = [self.checkboxOptions objectForKey:@"Selected"];
	NSMutableDictionary *valuesJSON = [self.checkboxOptions objectForKey:@"Values"];
    
	NSArray *allKeys = [valuesJSON allKeys];
	float xPosition = 5;
	float yPosition = 20;
	float height = 25;
    float width = 0;
    
    if([[self.surveyParamName lowercaseString] isEqualToString:LOC(@"KEY_STRING_LANDLORD_APPLIANCE_SMALL_CASE")]) {
        
        xPosition = CGRectGetMaxX([self.lblTitle frame])+10;
        yPosition = 4;
        height = 35;
        width = 35;
        
        
        CGRect frame = [self.lblTitle frame];
        frame.origin.y = 11;
        [self.lblTitle setFrame:frame];
    }
    
    
	for (NSString *key in allKeys) {
		CGRect buttonFrame = CGRectMake(xPosition, yPosition, width, height);
		yPosition += height;
		PSCheckboxButton *checkboxButton = [[PSCheckboxButton alloc] initWithFrame:buttonFrame
		                                                                     title:key
		                                                          checkBoxSelected:[[[selectedJSON objectForKey:key] objectForKey:@"IsCheckBoxSelected"] boolValue]];
		checkboxButton.tag = [[valuesJSON objectForKey:key] intValue];
		[checkboxButton addTarget:self action:@selector(onClickCheckboxButton:) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:checkboxButton];
	}
    
	if (self.formData) {
		NSDictionary *selectedValues = self.formData.surveyParamItemFieldValue;
		NSArray *allKeys = [selectedValues allKeys];
		for (NSString *key in allKeys) {
			PSCheckboxButton *checkboxButton = (PSCheckboxButton *)[self viewWithTag:[[[selectedValues objectForKey:key] objectForKey:@"paramId"] integerValue]];
			if (checkboxButton) {
				BOOL isSelected = [[[selectedValues objectForKey:key] objectForKey:@"IsCheckBoxSelected"] boolValue];
				if (isSelected)
					[checkboxButton makeSelected];
				else
					[checkboxButton makeDeselected];
			}
		}
	}
}

+ (CGFloat)cellHeightForCheckboxesCount:(NSInteger)checkboxesCount {
	CGFloat cellHeight = 50;
	if (checkboxesCount > 1) {
		cellHeight = (checkboxesCount * 25) + 50;
	}
	return cellHeight;
}

- (IBAction)onClickCheckboxButton:(PSCheckboxButton *)checkboxButton {
	[checkboxButton toggle];
    
	self.itemParamId = [self.checkboxOptions objectForKey:@"itemParamId"];
	self.paramId = [self.checkboxOptions objectForKey:@"paramId"];
    
	NSMutableDictionary *selectedJSON = [self.checkboxOptions objectForKey:@"Selected"];
	NSMutableDictionary *valuesJSON = [self.checkboxOptions objectForKey:@"Values"];
    
	NSMutableDictionary *valuesDictionary = [NSMutableDictionary dictionary];
    
	NSArray *allKeys = [valuesJSON allKeys];
    
	for (NSString *key in allKeys) {
		PSCheckboxButton *checkboxButton = (PSCheckboxButton *)[self viewWithTag:[[valuesJSON objectForKey:key] intValue]];
		NSMutableDictionary *values = [NSMutableDictionary dictionary];
		[values setObject:[NSNumber numberWithBool:checkboxButton.isCheckBoxSelected] forKey:@"IsCheckBoxSelected"];
		[values setObject:[valuesJSON objectForKey:key] forKey:@"paramId"];
		[valuesDictionary setObject:values forKey:key];
	}
    
	[self.formDataDictionary setObject:valuesDictionary forKey:@"surveyParamItemFieldValue"];
    
	if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeSurveyFormWithData:)]) {
		[self.delegate didChangeSurveyFormWithData:self.formDataDictionary];
	}
}

@end
