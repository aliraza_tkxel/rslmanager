//
//  PSSurveyCheckboxCell.h
//  PropertySurveyApp
//
//  Created by TkXel on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSSurveyViewController.h"

@protocol PSSurveyCheckboxCellDelegate <NSObject>

- (void) didDeselectCheckboxOption:(NSInteger)optionId;
- (void) didSelectCheckboxOption:(NSInteger)optionId;

@end

@class FormData;
@interface PSSurveyCheckboxCell : UITableViewCell
@property (weak,   nonatomic) id<SurveyFormDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) NSNumber *itemParamId;
@property (strong, nonatomic) NSNumber *paramId;
@property (strong, nonatomic) NSString *surveyParamName;

@property (strong, nonatomic) NSDictionary *checkboxOptions;
@property (strong, nonatomic) NSMutableDictionary *formDataDictionary;

@property (nonatomic, weak) FormData *formData;

- (void) loadCellWithData:(NSDictionary *)dataDictionary;
+ (CGFloat) cellHeightForCheckboxesCount:(NSInteger)checkboxesCount;

@end
