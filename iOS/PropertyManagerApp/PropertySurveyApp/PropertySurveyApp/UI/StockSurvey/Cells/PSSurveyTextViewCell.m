//
//  PSSurveyTextboxCell.m
//  PropertySurveyApp
//
//  Created by TkXel on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyTextViewCell.h"
#import "PSTextView.h"

@interface PSSurveyTextViewCell ()
{
	BOOL _isEdited;
    NSString *_initialText;
    PSTextView *txtValue;
}
@property (strong, nonatomic) NSMutableDictionary *formDataDictionary;

@end

@implementation PSSurveyTextViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
        
        
  	}
	return self;
}
-(void)awakeFromNib
{
    self.txtValue = [[PSTextView alloc] initWithFrame:CGRectMake(10, 40, 300, 100)];
	[self.txtValue setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
    self.txtValue.text = @"";
    self.txtValue.delegate = self;
    self.txtValue.backgroundColor = [UIColor clearColor];
    [self.txtValue setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    [self.contentView addSubview:self.txtValue];
}
-(void)registerForNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
    
	// Configure the view for the selected state
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    _initialText = textView.text;
    //[[NSNotificationCenter defaultCenter] post]
	if (self.delegate && [self.delegate isKindOfClass:[PSSurveyViewController class]]) {
		[(PSSurveyViewController *)self.delegate setSelectedControl : self.txtValue];
	}
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
	_isEdited = YES;
    BOOL _1000InComplete = YES;
    if(text.length + textView.text.length >= 999)
    {
        _1000InComplete = NO;
    }
    if(text.length<1) // This case is for handling of delete button
    {
        _1000InComplete = YES;
    }
	return _1000InComplete;
}

- (void) textViewDidEndEditing:(UITextView *)textView
{
      NSString *selectedValue = self.txtValue.text;
    if (![_initialText isEqualToString:selectedValue])
    {
        // [self onTextFieldDidEndOnExit:textField];
        [self.txtValue setUserInteractionEnabled:NO];
        [self.txtValue resignFirstResponder];
        
     //   if (!isEmpty(selectedValue))
     //   {
            NSString *surveyParamItemFieldValue = selectedValue;
            NSString *surveyPramItemFieldId = @"";
            [self.formDataDictionary setObject:surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
            [self.formDataDictionary setObject:isEmpty(surveyParamItemFieldValue)?[NSNull null]:surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
    //    }
        if (_isEdited)
        {
            //Propogate the Change Call Back
            if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeSurveyFormWithData:)]) {
                [self.delegate performSelector:@selector(didChangeSurveyFormWithData:) withObject:self.formDataDictionary];
            }
        }
    }
}

#pragma mark - IBActions
- (IBAction)onClickEditButton:(UIButton *)sender {
	[self.txtValue setUserInteractionEnabled:YES];
	[self.txtValue becomeFirstResponder];
}


#pragma mark Methods
- (void)loadCellData {
	if (!self.formDataDictionary) {
		self.formDataDictionary = [NSMutableDictionary dictionary];
	}
    
	NSMutableDictionary *selectedJSON = [self.dataDictionary objectForKey:@"Selected"];
	NSMutableDictionary *valuesJSON = [self.dataDictionary objectForKey:@"Values"];
    
    
	NSNumber *surveyItemParamId = [self.dataDictionary objectForKey:@"itemParamId"];
	NSNumber *surveyParameterId = [self.dataDictionary objectForKey:@"paramId"];
	NSString *controlType = [self.dataDictionary objectForKey:@"Type"];

	[self.formDataDictionary setObject:surveyItemParamId forKey:@"surveyItemParamId"];
	[self.formDataDictionary setObject:surveyParameterId forKey:@"surveyParameterId"];
	[self.formDataDictionary setObject:controlType forKey:@"controlType"];
    [self.formDataDictionary setObject:self.surveyParamName forKey:@"surveyParamName"];
    
    NSNumber *readOnly = [self.dataDictionary objectForKey:@"ReadOnly"];
    [self.btnEdit setHidden:[readOnly boolValue]];
    
	NSString *selectedValue = nil;
	if (isEmpty(selectedJSON)) {
		self.txtValue.text = @"";
		//self.txtValue.placeholder = @"Type here";
	}
	else {
		NSString *surveyParamItemFieldValue = [self.dataDictionary objectForKey:@"paramId"];
		NSString *surveyPramItemFieldId = [self.dataDictionary objectForKey:@"paramId"];
        
        
		selectedValue = [[selectedJSON allKeys] objectAtIndex:0];
		self.txtValue.text = selectedValue;
        
		[self.formDataDictionary setObject:surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
		[self.formDataDictionary setObject:surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
		[self.formDataDictionary setObject:@"" forKey:@"isSelected"];
	}
	self.lblTitle.text = [self.surveyParamName uppercaseString];
	self.txtValue.delegate = self;
    
	if (self.formData) {
		self.txtValue.text = self.formData.surveyParamItemFieldValue;
	}
}

@end
