//
//  PSSurveyTextboxCell.m
//  PropertySurveyApp
//
//  Created by TkXel on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyTextboxCell.h"

@interface PSSurveyTextboxCell ()
{
	BOOL _isEdited;
    NSString *_initialText;
}
@property (strong, nonatomic) NSMutableDictionary *formDataDictionary;

@end

@implementation PSSurveyTextboxCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
    
	// Configure the view for the selected state
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _initialText = textField.text;
    
	if (self.delegate && [self.delegate isKindOfClass:[PSSurveyViewController class]]) {
		[(PSSurveyViewController *)self.delegate setSelectedControl : self.txtValue];
	}
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	_isEdited = YES;
	return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
      NSString *selectedValue = self.txtValue.text;
    if (![_initialText isEqualToString:selectedValue])
    {
        // [self onTextFieldDidEndOnExit:textField];
        [self.txtValue setUserInteractionEnabled:NO];
        [self.txtValue resignFirstResponder];
        
     //   if (!isEmpty(selectedValue))
     //   {
            NSString *surveyParamItemFieldValue = selectedValue;
            NSString *surveyPramItemFieldId = @"";
            [self.formDataDictionary setObject:surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
            [self.formDataDictionary setObject:isEmpty(surveyParamItemFieldValue)?[NSNull null]:surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
    //    }
        if (_isEdited)
        {
            //Propogate the Change Call Back
            if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeSurveyFormWithData:)]) {
                [self.delegate performSelector:@selector(didChangeSurveyFormWithData:) withObject:self.formDataDictionary];
            }
        }
    }
}

#pragma mark - IBActions
- (IBAction)onClickEditButton:(UIButton *)sender {
	[self.txtValue setUserInteractionEnabled:YES];
	[self.txtValue becomeFirstResponder];
}

- (IBAction)onTextFieldDidEndOnExit:(UITextField *)sender {
	/*[self.txtValue setUserInteractionEnabled:NO];
	[self.txtValue resignFirstResponder];
    
    
	NSString *selectedValue = self.txtValue.text;
    
	if (!isEmpty(selectedValue))
    {
		NSString *surveyParamItemFieldValue = selectedValue;
		NSString *surveyPramItemFieldId = @"";
		[self.formDataDictionary setObject:surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
		[self.formDataDictionary setObject:surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
    }
    if (_isEdited)
    {
        //Propogate the Change Call Back
        if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeSurveyFormWithData:)]) {
            [self.delegate performSelector:@selector(didChangeSurveyFormWithData:) withObject:self.formDataDictionary];
        }
    }*/
}

#pragma mark Methods
- (void)loadCellData {
	if (!self.formDataDictionary) {
		self.formDataDictionary = [NSMutableDictionary dictionary];
	}
    
	NSMutableDictionary *selectedJSON = [self.dataDictionary objectForKey:@"Selected"];
	NSMutableDictionary *valuesJSON = [self.dataDictionary objectForKey:@"Values"];
    
    
	NSNumber *surveyItemParamId = [self.dataDictionary objectForKey:@"itemParamId"];
	NSNumber *surveyParameterId = [self.dataDictionary objectForKey:@"paramId"];
	NSString *controlType = [self.dataDictionary objectForKey:@"Type"];

	[self.formDataDictionary setObject:surveyItemParamId forKey:@"surveyItemParamId"];
	[self.formDataDictionary setObject:surveyParameterId forKey:@"surveyParameterId"];
	[self.formDataDictionary setObject:controlType forKey:@"controlType"];
    [self.formDataDictionary setObject:self.surveyParamName forKey:@"surveyParamName"];
    
    NSNumber *readOnly = [self.dataDictionary objectForKey:@"ReadOnly"];
    [self.btnEdit setHidden:[readOnly boolValue]];
    
	NSString *selectedValue = nil;
	if (isEmpty(selectedJSON)) {
		self.txtValue.text = @"";
		self.txtValue.placeholder = @"Type here";
	}
	else {
		NSString *surveyParamItemFieldValue = [self.dataDictionary objectForKey:@"paramId"];
		NSString *surveyPramItemFieldId = [self.dataDictionary objectForKey:@"paramId"];
        
        
		selectedValue = [[selectedJSON allKeys] objectAtIndex:0];
		self.txtValue.text = selectedValue;
        
		[self.formDataDictionary setObject:surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
		[self.formDataDictionary setObject:surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
		[self.formDataDictionary setObject:@"" forKey:@"isSelected"];
	}
	self.lblTitle.text = [self.surveyParamName uppercaseString];
	self.txtValue.delegate = self;
    
	if (self.formData) {
		self.txtValue.text = self.formData.surveyParamItemFieldValue;
	}
}

@end
