//
//  GenericViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 6/15/11.
//  Copyright 2011 PCC. All rights reserved.
//

#import "GenericViewController.h"
#import "AddressDetail.h"
#import "DropDownCell.h"
#import "DateTimeCell.h"
#import "CheckBoxCell.h"
#import "YesNoCell.h"
#import "DropDownDateTimeCell.h"
#import "DropDownCheckBoxCell.h"
#import "CheckBoxesCell.h"
#import "SingletonData.h"
#import "NotesViewController.h"
#import "PhotosViewController.h"
#import "ServerKeys.h"
#import "PropertySurveyHelperFunctions.h"
#import "JSON.h"
#import "ServerConnection.h"
#import "UpdateToServer.h"
#import "TextFieldCell.h"

#import "ApplianceDetailsViewController.h"
#import "ApplianceCountCell.h"

#define FONT_SIZE 11;

@implementation GenericViewController

@synthesize calenderEvent, propertyId, totalNumberOfTicks,appointmentType,formPath, jsonRequest, completeTitle, isMainSurveyMenu, fields;
@synthesize selectedCalenderEventIndex;

int sortingFunction(id item1, id item2, void * context){
	return [item1 compare:item2];
}

- (id)initWithTitle:(NSString *)viewTitle parentTitle:(NSString *)parentT withViewTitles:(NSMutableDictionary *)dict withData:(NSMutableDictionary *)surveyDataDict prevData:(NSMutableDictionary *)prevDataDict withKey:(NSString *)prevDictKey
{
    if (self = [super init])
	{
        self.completeTitle = viewTitle;
        
        if ([self.completeTitle isEqualToString:@"Gas"])
        {
            isLocalFieldScreen=YES;
        }
        
		self.title = viewTitle;
		self.formPath = [self getFormIndexPath];
		parentTitle = parentT;
		dictData = dict;
        
        
        itemId = [[dict objectForKey:@"itemId"] stringValue];
		
		if(parentT == nil)
		{
			NSMutableDictionary * tempSurveyServerDict = [[NSMutableDictionary alloc] init];
			NSMutableDictionary * tempAppointmentServerDict = [[NSMutableDictionary alloc] initWithDictionary:[[SingletonData sharedInstance] appointmentSelectedForViewing]];
			[tempAppointmentServerDict removeObjectForKey:@"Survey"];
			[tempAppointmentServerDict removeObjectForKey:@"Accommodation"];
			[tempAppointmentServerDict removeObjectForKey:@"IndexInArray"];
			[tempAppointmentServerDict removeObjectForKey:@"PropertyImages"];
            [tempSurveyServerDict setObject:tempAppointmentServerDict forKey:[ServerKeys appointmentDetailsKey]];
			[[SingletonData sharedInstance] setUpdateSurveyToServer:tempSurveyServerDict];
			[tempAppointmentServerDict release];
			[tempSurveyServerDict release];
		}
		
		if(surveyData != nil)
		{
			surveyData = nil;
		}
		surveyData = [[NSMutableDictionary alloc] initWithDictionary:surveyDataDict];
        
		
		[self getSurveyData:surveyDataDict];
        
		
		if(prevDataDict != nil && [prevDataDict objectForKey:prevDictKey] != nil)
		{
			[prevDataDict setObject:surveyData forKey:prevDictKey];
		}
		
		if([[dictData objectForKey:TYPE] isEqualToString:MENU_TYPE])
		{
            
		}
        
        //Change by hussain
        //self.fields = [[dictData objectForKey:FIELDS] allKeys];
        self.fields = [dictData objectForKey:@"Order"];
        //self.fields = [self.fields sortedArrayUsingFunction:sortingFunction context:nil];
    }
    
    return  self;
}

- (void)dealloc
{
    [totalNumberOfTicks release];
    
    [ticksDictionay release];
    
    [self.propertyId release];
    
	if(nextSurveyData)
		[nextSurveyData release];
	if(surveyDataFields)
		[surveyDataFields release];
	//if(jsonRequest)
	//	[jsonRequest release];
    if (fields)
        [fields release];
	[calenderEvent release];
	[formPath release];
    [genericTableView release];
    
    if (imageNameLbl != NULL) {
        [imageNameLbl release];
    }
    if (imageAddressLbl != NULL) {
        [imageAddressLbl release];
    }
    if (stockConditionLbl != NULL) {
        [stockConditionLbl release];
    }
    if (propertyImageView != NULL) {
        [propertyImageView release];
    }
	
    if (nameLbl != NULL) {
        [nameLbl release];
    }
    if (addressLbl != NULL) {
        [addressLbl release];
    }
    if (customerNameLbl != NULL) {
        [customerNameLbl release];
    }
    if (customerAddressLbl != NULL) {
        [customerAddressLbl release];
    }
	
    if (notesButton != NULL) {
        [notesButton release];
    }
    if (cameraButton != NULL) {
        [cameraButton release];
    }
	
    if (sectionNotAvailableLabel != NULL) {
        [sectionNotAvailableLabel release];
    }
    if (activityIndicator != NULL) {
        [activityIndicator release];
    }

    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

#pragma mark - View lifecycle
/*
 willappear of dev
 Dwelling =         {
 Fields =             {
 Exposure =
 Fields =
 "Form_Completed" = YES;
 Roof =
 Fields =
 "Form_Completed" = YES;
 Structure =
 Fields =
 "Form_Completed" = YES;
 } ;
 Image = 0;
 Order =             (
 Roof,
 Structure,
 Exposure
 );
 Type = Menu;
 
 
 {
 Fields =     {
 Exposure =
 "Form_Completed" = YES;
 
 Roof =
 "Form_Completed" = YES;
 
 Structure =
 "Form_Completed" = YES;
 
 };
 Image = 0;
 Order =     (
 Roof,
 Structure,
 Exposure
 );
 Type = Menu;
 }
 
 
 */

-(void) getApplianceCount: (NSData *) data
{

    [[PropertySurveyAppAppDelegate appDelegate] hideProgressAlertView];
    
    if(data == nil)
	{
		return;
	}
    
	NSString * numberOfAppliances = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    noOfAppliances=[numberOfAppliances integerValue];
    [numberOfAppliances release];
    
    
    
    //    NSIndexPath *myIP = [NSIndexPath indexPathForRow:3 inSection:0];
    //    NSArray *indexArray=[[NSArray alloc] initWithObjects:myIP, nil];
    
    [genericTableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [[PropertySurveyAppAppDelegate appDelegate] showProgressAlertView:[self view] withTitle:@"Loading. . ."];
    
    self.totalNumberOfTicks=[NSNumber numberWithInt:0];
    [ticksDictionay removeAllObjects];
    
    if (isLocalFieldScreen)
    {
        ServerConnection * conn = [[ServerConnection alloc] init];
        conn.propertyId=self.propertyId;
        conn.httpMethod = @"GET";
        [conn getApplianceCount:self requestSelector:@selector(getApplianceCount:)];
        [conn release];
    }
    
	if(parentTitle == nil)
	{
		if([[SingletonData sharedInstance] lastView] == nil)
		{
			[[[SingletonData sharedInstance] imageIndexes] addObject:@"Index"];
		}
		else if(![[[SingletonData sharedInstance] lastView] isEqualToString:@"Index"] && [[[SingletonData sharedInstance] imageIndexes] count] > 1)
		{
			//remove last title
			[[[SingletonData sharedInstance] imageIndexes] removeLastObject];
		}
        
		[[SingletonData sharedInstance] setLastView:@"Index"];
	}
	else
	{
		if([[[SingletonData sharedInstance] lastView] isEqualToString:parentTitle])
		{
			//add this view's title
			[[[SingletonData sharedInstance] imageIndexes] addObject:self.title];
		}
		else
		{
			//remove last title
            if ([[[SingletonData sharedInstance] imageIndexes] count] > 0){
                [[[SingletonData sharedInstance] imageIndexes] removeLastObject];
            }
		}
		//set last view to this view's title
		[[SingletonData sharedInstance] setLastView:self.title];
	}
    
    //	if([activityIndicator isAnimating])
    //		[activityIndicator stopAnimating];
}

- (void) viewDidAppear:(BOOL)animated
{
    if (!isLocalFieldScreen)
    {
    
        [[PropertySurveyAppAppDelegate appDelegate] hideProgressAlertView];
        [genericTableView reloadData];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[SingletonData sharedInstance] saveData];
    
}

//changed by aaban
-(BOOL) isNumber :(NSString *)string
{
    NSString *phoneNumberRegex = @"[0-9]*";
    NSPredicate *phoneNumberTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneNumberRegex];
    return [phoneNumberTest evaluateWithObject:string];
}
//Ends

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    
    if(dictData != nil)
	{
		if([dictData objectForKey:FIELDS] != nil && [[dictData objectForKey:FIELDS]count] > 0)
		{
			if([[dictData objectForKey:TYPE] isEqualToString:MENU_TYPE])
			{
				[self.view sendSubviewToBack:activityIndicator];
				//if([[dictData objectForKey:HAS_LABEL] boolValue])
				//{
                if([[dictData objectForKey:HAS_IMAGE] boolValue])
                {
                    //Menu with image and labels
                    [self setChecks:IMAGE_TABLE];
                    [self relocateTable:IMAGE_TABLE];
                    
                    
					//waiting for client response
                    //unit testing needed
                    if (calenderEvent.details.customers.count > 0){
                        [imageNameLbl setText:[(ContactDetails *)[calenderEvent.details.customers objectAtIndex:[[self.calenderEvent.details customerDefaultIndex] intValue]] name]];
                    }
                    else{
                        [imageNameLbl setText:"KEY_STRING_N/A"];
                    }
                    
                    AddressDetail *addresDetail=[calenderEvent.details addressDetail];
                    
                    [imageAddressLbl setText:[NSString stringWithFormat:@"%@, %@ %@, %@",
                                              [addresDetail address1],
                                              [addresDetail city],
                                              [addresDetail state],
                                              [addresDetail postalCode]]];
                    
                    //TODO: get image from server - if no image found, set this image
                    [propertyImageView setImage:[UIImage imageNamed:[calenderEvent.details propertyImage]]];
                }
                else
                {
                    //Menu with Labels
                    //waiting for client response
                    //unit testing needed
                    if (calenderEvent.details.customers.count > 0){
                        [customerNameLbl setText:[(ContactDetails *)[calenderEvent.details.customers objectAtIndex:[[self.calenderEvent.details customerDefaultIndex] intValue]] name]];
                    }
                    else{
                        [customerNameLbl setText:"KEY_STRING_N/A"];
                    }
                    
                    AddressDetail *addresDetail=[calenderEvent.details addressDetail];
                    
                    [customerAddressLbl setText:[NSString stringWithFormat:@"%@, %@ %@, %@",
                                                 [addresDetail address1],
                                                 [addresDetail city],
                                                 [addresDetail state],
                                                 [addresDetail postalCode]]];
                    [self setChecks:LABEL_TABLE];
                    [self relocateTable:LABEL_TABLE];
                }
				//}
				//else
				//{
                //Simple Menu
				//	[self setChecks:SIMPLE_TABLE];
				//	[self relocateTable:SIMPLE_TABLE];
				//}
                
                if (isMainSurveyMenu)
                {
                    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithTitle:COMPLETED_STATUS style:UIBarButtonItemStylePlain target:self action:@selector(completePressed)];
                    self.navigationItem.rightBarButtonItem = rightButton;
                    [rightButton release];
                }
			}
			else if([[dictData objectForKey:TYPE] isEqualToString:FORM_TYPE])
			{
				//Forms
				[self.view bringSubviewToFront:activityIndicator];
				UIBarButtonItem * saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
				self.navigationItem.rightBarButtonItem = saveButton;
				[saveButton release];
				
				UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:parentTitle style:UIBarButtonItemStylePlain target:self action:@selector(checkSaves)];
				self.navigationItem.leftBarButtonItem = backButton;
				[backButton release];
				
				[self setChecks:SIMPLE_TABLE];
				cameraButton.hidden = NO;
				cameraButton.enabled = YES;
				notesButton.hidden = NO;
				notesButton.enabled = YES;
				[self relocateTable:LABEL_TABLE];
				
				if([[SingletonData sharedInstance] tempFormsData] != nil)
				{
					[[SingletonData sharedInstance] setTempFormsData:nil];
				}
				
                NSMutableDictionary * tempFormsData = [[NSMutableDictionary alloc] initWithDictionary:surveyDataFields];
				[[SingletonData sharedInstance] setTempFormsData:tempFormsData];
				[tempFormsData release];
                
			}
		}
		else
		{
			[self setChecks:NO_TABLE];
		}
	}
    
    ticksDictionay = [[NSMutableDictionary alloc] init];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Helper methods

- (void)relocateTable:(NSString *)type
{
	genericTableView.frame = CGRectFromString(type);
	genericTableView.dataSource = self;
	genericTableView.delegate = self;
    
	[self.view addSubview:genericTableView];
}

- (BOOL)isCustomCell:(NSString *)type
{
	if([type isEqualToString: DROPDOWN_CELL] ||
	   [type isEqualToString: DATETIME_CELL] ||
	   [type isEqualToString: COMPOSITE_CELL] ||
	   [type isEqualToString: CHECKBOX_CELL] ||
	   [type isEqualToString: YES_NO_CELL] ||
       [type isEqualToString: TEXTFIELD_CELL] ||
       [type isEqualToString: CHECKBOXES]
       )
	{
		return YES;
	}
	return NO;
}

- (NSString *)getFormIndexPath
{
	NSMutableString * path = [NSMutableString string];
	for(int i = 0; i < (int)[[[SingletonData sharedInstance] imageIndexes] count]; i++)
	{
		if(path == nil)
		{
			path = [NSMutableString stringWithFormat:@"%@", [[[SingletonData sharedInstance] imageIndexes] objectAtIndex:i]];
		}
		else
		{
			[path appendFormat:@";%@", [[[SingletonData sharedInstance] imageIndexes] objectAtIndex:i]];
		}
	}
	NSString * returnPath = [NSString stringWithFormat:@"%@", path];
    returnPath = [returnPath stringByAppendingFormat:@";%@", self.title];
	return returnPath;
}

- (void)setChecks:(NSString *)type
{
    
	cameraButton.hidden = YES;
	cameraButton.enabled = NO;
	notesButton.hidden = YES;
	notesButton.enabled = NO;
	
	if([type isEqualToString:SIMPLE_TABLE])
	{
		nameLbl.hidden = YES;
		addressLbl.hidden = YES;
		customerNameLbl.hidden = YES;
		customerAddressLbl.hidden = YES;
		sectionNotAvailableLabel.hidden = YES;
		
		imageNameLbl.hidden = YES;
		imageAddressLbl.hidden = YES;
		propertyImageView.hidden = YES;
		stockConditionLbl.hidden = YES;
	}
	else if([type isEqualToString:LABEL_TABLE])
	{
		nameLbl.hidden = NO;
		addressLbl.hidden = NO;
		customerNameLbl.hidden = NO;
		customerAddressLbl.hidden = NO;
		sectionNotAvailableLabel.hidden = YES;
		
		imageNameLbl.hidden = YES;
		imageAddressLbl.hidden = YES;
		propertyImageView.hidden = YES;
		stockConditionLbl.hidden = YES;
	}
	else if([type isEqualToString:IMAGE_TABLE])
	{
		nameLbl.hidden = YES;
		addressLbl.hidden = YES;
		customerNameLbl.hidden = YES;
		customerAddressLbl.hidden = YES;
		sectionNotAvailableLabel.hidden = YES;
		
		imageNameLbl.hidden = NO;
		imageAddressLbl.hidden = NO;
		propertyImageView.hidden = NO;
		stockConditionLbl.hidden = NO;
	}
	else if([type isEqualToString:NO_TABLE])
	{
		nameLbl.hidden = YES;
		addressLbl.hidden = YES;
		customerNameLbl.hidden = YES;
		customerAddressLbl.hidden = YES;
		sectionNotAvailableLabel.hidden = NO;
		
		imageNameLbl.hidden = YES;
		imageAddressLbl.hidden = YES;
		propertyImageView.hidden = YES;
		stockConditionLbl.hidden = YES;
	}
    
}

- (void)getSurveyData:(NSMutableDictionary *)dict
{
	if(surveyDataFields == nil)
	{
		surveyDataFields = [[NSMutableDictionary alloc] init];
	}
	
	//if(dict != nil && [dict objectForKey:FIELDS] != nil)
	if(dict != nil)
	{
		if([dict objectForKey:FIELDS] != nil)
		{
            
			if([[dict objectForKey:FIELDS] isKindOfClass:[NSDictionary class]])
			{
				[surveyDataFields setDictionary:[dict objectForKey:FIELDS]];
			}
			else
			{
				surveyDataFields = [dict objectForKey:FIELDS];
			}
			[surveyData setObject:surveyDataFields forKey:FIELDS];
		}
		else
		{
            
		}
        
	}
	else
	{
	}
    
}

- (UITableViewCell *)initCustomCell:(UITableViewCell *)cell withDict:(NSDictionary *)viewCells withReuseIdentifier:(NSString*)identifier  andIndexPath:(NSIndexPath *)indexPath
{
    //Changes by hussain
	//NSArray * fields = [viewCells allKeys];
    NSArray * fieldsL = self.fields;
    //fields = [fields sortedArrayUsingFunction:sortingFunction context:nil];
    int index = (indexPath.row > 3 && isLocalFieldScreen) ? indexPath.row - 1 : indexPath.row;
    NSString * cellType = [[viewCells objectForKey:[fieldsL objectAtIndex:index]] objectForKey:TYPE];
	
    
    
	if([cellType isEqualToString:DROPDOWN_CELL]) // Done - used
	{        
		DropDownCell * cell1 = [[[DropDownCell alloc] init] autorelease];
        
        [cell1 setReuseIdentifier:identifier];
        [cell1 setFormDataDict:[viewCells objectForKey:[fieldsL objectAtIndex:index]]];
        
        NSDictionary * pickerDict = [[viewCells objectForKey:[fieldsL objectAtIndex:index]] objectForKey:CELL_VALUES];
        
        NSArray * pickerValues = [pickerDict allKeys];
        pickerValues = [pickerValues sortedArrayUsingFunction:sortingFunction context:nil];
        
		
        cell1.pickerData = pickerValues;
		cell1.label.text = [NSString stringWithFormat:@"  %@: ", [fieldsL objectAtIndex:index]];
		
		cell1.field = [fieldsL objectAtIndex:index];
		if([cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]])
		{
			[cell1.comboButton setTitle:[cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]] forState:UIControlStateNormal];
		} else { // adding previous data.
            NSDictionary * previousData = [cell1.formDataDict objectForKey:@"Selected"];
            if ([[previousData allKeys] count] > 0){
                [cell1.comboButton setTitle:[[previousData allKeys] objectAtIndex:0] forState:UIControlStateNormal];
            } else {
                
            }
        }
        
		cell = cell1;
	}
    
    if([cellType isEqualToString:TEXTFIELD_CELL]) // Done - used
	{
        
		TextFieldCell * cell1 = [[[TextFieldCell alloc] init] autorelease];
        [cell1 setReuseIdentifier:identifier];
        NSMutableDictionary * tempFormDict =[viewCells objectForKey:[fieldsL objectAtIndex:index]];
        [cell1 setFormDataDict:tempFormDict];
        
        NSString * lblText=[NSString stringWithFormat:@"  %@: ", [fieldsL objectAtIndex:index]];
        
        [cell1 setKeyBoardTypeForTitle:lblText];
        
		cell1.label.text = lblText;
        
		cell1.field = [fieldsL objectAtIndex:index];
		
        
        
        if([cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]])
		{
            
            [cell1.textField setText:[cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]]];
		} else { // adding previous data.
            NSDictionary * previousData = [cell1.formDataDict objectForKey:@"Selected"];
            if ([[previousData allKeys] count] > 0){
                [cell1.textField setText:[[previousData allKeys] objectAtIndex:0]];
            } else {
                
            }
        }
        
        if ([[cell1.formDataDict objectForKey:@"ReadOnly"] boolValue]){
            [cell1.textField setUserInteractionEnabled:NO];
        } else {
            [cell1.textField setUserInteractionEnabled:YES];
        }
        
        //return cell1;
        //changed recent
		cell = cell1;
	}
	
	if([cellType isEqualToString:DATETIME_CELL]) // Done
	{
        
		NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"DateTimeCell" owner:self options:nil];
		DateTimeCell * cell1 = (DateTimeCell *)[nib objectAtIndex:0];
        [cell1 setReuseIdentifier:identifier];
		[cell1 setFormDataDict:[viewCells objectForKey:[fieldsL objectAtIndex:index]]];
        
		cell1.label.text = [NSString stringWithFormat:@"  %@: ", [fieldsL objectAtIndex:index]];
        
        CLS_LOG(@"%@", cell1.formDataDict);
        //        user_entered_vale
		if([cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]])
		{
            //cell1.dateTextField.text = [cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]];
            
            NSDateFormatter * dateFormater = [[[NSDateFormatter alloc] init] autorelease];
            [dateFormater setDateFormat:@"d/L/yy"];
            
            NSString * tempDateString = [cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]];
            
            NSDate * tempDate = [dateFormater dateFromString:tempDateString];
            
            [dateFormater setDateFormat:@"d/L/yyyy"];
            
            NSString * tempDateString1 = [dateFormater stringFromDate:tempDate];
            
			cell1.dateTextField.text = tempDateString1;
            
		} else { // adding previous data.
            NSDictionary * previousData = [cell1.formDataDict objectForKey:@"Selected"];
            if ([[previousData allKeys] count] > 0){
                //[cell1.dateTextField setTitle:[[previousData allKeys] objectAtIndex:0] forState:UIControlStateNormal];
                //cell1.dateTextField.text = [[previousData allKeys] objectAtIndex:0];
                
                NSDateFormatter * dateFormater = [[[NSDateFormatter alloc] init]autorelease];
                [dateFormater setDateFormat:@"d/L/yyyy H:m:s"];
                
                
                //
                
                
                
                NSString * tempDateString = [[previousData allKeys] objectAtIndex:0];
                CLS_LOG(@"%@", tempDateString);
                NSDate * tempDate = [dateFormater dateFromString:tempDateString];
                if (!tempDate)
                {
                    [dateFormater setDateFormat:@"MM/dd/yyyy HH:mm"];
                    tempDate= [dateFormater dateFromString:tempDateString];
                }
                
                
                CLS_LOG(@"%@", tempDate);
                [dateFormater setDateFormat:@"d/L/yyyy"];
                
                NSString * tempDateString1 = [dateFormater stringFromDate:tempDate];
                CLS_LOG(@"%@", tempDateString1);
                
                
                cell1.dateTextField.text = tempDateString1;
            } else {
                
            }
        }
        
		cell1.field = [fieldsL objectAtIndex:index];
		cell = cell1;
	}
	
	if([cellType isEqualToString:CHECKBOX_CELL]) // NOT DONE
	{
        
		NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"CheckBoxCell" owner:self options:nil];
		CheckBoxCell * cell1 = (CheckBoxCell *)[nib objectAtIndex:0];
        [cell1 setReuseIdentifier:identifier];
		[cell1 setFormDataDict:[viewCells objectForKey:[fieldsL objectAtIndex:index]]];
        
        
		cell1.label.text = [NSString stringWithFormat:@"  %@: ", [fieldsL objectAtIndex:index]];
		
        if ([cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]]){
            NSString * value = [cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]];
            if ([value isEqualToString:@"YES"])
            {
                [cell1.checkboxButton setSelected:YES];
                cell1.selection = 1;
            }
            else
            {
                [cell1.checkboxButton setSelected:NO];
                cell1.selection = 0;
            }
        }
        
		cell1.checkBoxLabel.text = [[viewCells objectForKey:[fieldsL objectAtIndex:index]]objectForKey:CELL_VALUES];
		cell1.field = [fieldsL objectAtIndex:index];
		cell = cell1;
	}
	
	if([cellType isEqualToString:YES_NO_CELL]) // NOT DONE
	{
        
		NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"YesNoCell" owner:self options:nil];
		YesNoCell * cell1 = (YesNoCell *)[nib objectAtIndex:0];
        [cell1 setReuseIdentifier:identifier];
		[cell1 setFormDataDict:[viewCells objectForKey:[fieldsL objectAtIndex:index]]];
        
		cell1.label.text = [NSString stringWithFormat:@"  %@: ", [fieldsL objectAtIndex:index]];
		
        /*if([[[SingletonData sharedInstance] tempFormsData] objectForKey:[fields objectAtIndex:indexPath.row]] != nil)
         {
         if ([[[[SingletonData sharedInstance] tempFormsData] objectForKey:[fields objectAtIndex:indexPath.row]] isEqualToString:@"Yes"])
         {
         [cell1.yesButton setSelected:YES];
         }
         else
         {
         [cell1.noButton setSelected:YES];
         }
         }*/
        if([cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]])
		{
			if ([[cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]] isEqualToString:@"Yes"])
			{
				[cell1.yesButton setSelected:YES];
			}
			else
			{
				[cell1.noButton setSelected:YES];
			}
		}
		cell1.field = [fieldsL objectAtIndex:index];
		cell = cell1;
	}
    
    if([cellType isEqualToString:CHECKBOXES]) // NOT DONE - used
    {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"CheckBoxesCell" owner:self options:nil];
        CheckBoxesCell * cell1 = (CheckBoxesCell *)[nib objectAtIndex:0];
        [cell1 setReuseIdentifier:identifier];
        [cell1 setFormDataDict:[viewCells objectForKey:[fieldsL objectAtIndex:index]]];
        
        NSString * labelText = (NSString *)[fieldsL objectAtIndex:index];
        
        NSDictionary * pickerDict = [[viewCells objectForKey:[fieldsL objectAtIndex:index]] objectForKey:CELL_VALUES];
        NSArray * checkBoxLabels = [pickerDict allKeys];
        
        [cell1 setCheckBoxes:checkBoxLabels];
        
        adaptations = nil;
        adaptations = [cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]];
        
        if (adaptations == nil || ![adaptations isKindOfClass:[NSMutableDictionary class]]) {
            adaptations = [[NSMutableDictionary alloc] init];
            [cell1.formDataDict setObject:adaptations forKey:[ServerKeys userEnteredValueKey]];
            [adaptations release];
        } else if (adaptations && [adaptations isKindOfClass:[NSMutableDictionary class]]){
            //do nothing
        }else{ // adding previous data.
            NSDictionary * previousData = [cell1.formDataDict objectForKey:@"Selected"];
            if ([[previousData allKeys] count] > 0){
                adaptations = [[NSMutableDictionary alloc] init];
                
                
                for (int tt = 0; tt < [[previousData allKeys] count]; tt++){
                    NSString * tempString = [[previousData objectForKey:[[previousData allKeys] objectAtIndex:tt]] objectForKey:@"IsCheckBoxSelected"];
                    if ([tempString isEqualToString:@"True"]){
                        [adaptations setObject:@"Yes" forKey:[[previousData allKeys] objectAtIndex:tt]];
                    } else {
                        [adaptations setObject:@"No" forKey:[[previousData allKeys] objectAtIndex:tt]];
                    }
                }
                
                [cell1.formDataDict setObject:adaptations forKey:[ServerKeys userEnteredValueKey]];
                [adaptations release];
                //[cell1.dateTextField setTitle:[[previousData allKeys] objectAtIndex:0] forState:UIControlStateNormal];
                //cell1.dateTextField.text = [[previousData allKeys] objectAtIndex:0];
                //[cell1.formDataDict setObject:previousData forKey:[ServerKeys userEnteredValueKey]];
            } else {
                
            }
            
        }
        
        cell1.label.text = [NSString stringWithFormat:@"  %@: ", labelText];
        cell1.field = labelText;
        cell1.selectedCheckBoxValues = adaptations;
        
        [cell1 drawCheckBoxes];
        /*if([checkBoxLabels count] == 3)
         {
         cell1.checkBoxLabel1.text = (NSString *)[checkBoxLabels objectAtIndex:0];
         cell1.fieldCheckBox1 = (NSString *)[checkBoxLabels objectAtIndex:0];
         cell1.checkBoxLabel2.text = (NSString *)[checkBoxLabels objectAtIndex:1];
         cell1.fieldCheckBox2 = (NSString *)[checkBoxLabels objectAtIndex:1];
         cell1.checkBoxLabel3.text = (NSString *)[checkBoxLabels objectAtIndex:2];
         cell1.fieldCheckBox3 = (NSString *)[checkBoxLabels objectAtIndex:2];
         
         if([cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]])
         {
         //if([adaptations count] == 3)
         //{
         NSString * value1 = (NSString *)[adaptations objectForKey:[checkBoxLabels objectAtIndex:0]];
         NSString * value2 = (NSString *)[adaptations objectForKey:[checkBoxLabels objectAtIndex:1]];
         NSString * value3 = (NSString *)[adaptations objectForKey:[checkBoxLabels objectAtIndex:2]];
         if ([value1 isEqualToString:@"Yes"])
         {
         [cell1.checkboxButton1 setSelected:YES];
         cell1.checkbox1Selected = YES;
         }
         else
         {
         [cell1.checkboxButton1 setSelected:NO];
         cell1.checkbox1Selected = NO;
         }
         
         if ([value2 isEqualToString:@"Yes"])
         {
         [cell1.checkboxButton2 setSelected:YES];
         cell1.checkbox2Selected = YES;
         }
         else
         {
         [cell1.checkboxButton2 setSelected:NO];
         cell1.checkbox2Selected = NO;
         }
         
         if ([value3 isEqualToString:@"Yes"])
         {
         [cell1.checkboxButton3 setSelected:YES];
         cell1.checkbox3Selected = YES;
         }
         else
         {
         [cell1.checkboxButton3 setSelected:NO];
         cell1.checkbox3Selected = NO;
         }
         //}
         }
         }*/
        
        cell = cell1;
    }
	
	if([cellType isEqualToString:COMPOSITE_CELL]) //NOT DONE
	{
		NSDictionary * composite = [viewCells objectForKey:[fieldsL objectAtIndex:index]];
		NSDictionary * compositeCells = [composite objectForKey:CELL_VALUES];
		NSArray * valueKeys = [compositeCells allKeys];
        
		
		if([[composite objectForKey:COMPOSITE_CELL_TYPE] isEqualToString:DROPDOWN_DATETIME])
		{
			//NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"DropDownDateTimeCell" owner:self options:nil];
			//DropDownDateTimeCell * cell1 = (DropDownDateTimeCell *)[nib objectAtIndex:0];
            DropDownDateTimeCell *cell1 = [[[DropDownDateTimeCell alloc] init] autorelease];
            [cell1 setReuseIdentifier:identifier];
            [cell1 setFormDataDict:[viewCells objectForKey:[fieldsL objectAtIndex:index]]];
            
            if ([[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]] == nil) {
                [[cell1 formDataDict] setObject:[[[NSMutableDictionary alloc] init] autorelease] forKey:[ServerKeys userEnteredValueKey]];
            }
            
            NSArray * pickerData = nil;
			
			for(int i = 0; i < (int)[compositeCells count]; i++)
			{
				if([[[compositeCells objectForKey:[valueKeys objectAtIndex:i]] objectForKey:TYPE] isEqualToString:DROPDOWN_CELL])
				{
                    
					pickerData = [[compositeCells objectForKey:[valueKeys objectAtIndex:i]] objectForKey:CELL_VALUES];
					cell1.pickerData = pickerData;
					cell1.dropDownLabel.text = [NSString stringWithFormat:@"  %@: ", [valueKeys objectAtIndex:i]];
					cell1.fieldDropDown = [valueKeys objectAtIndex:i];
					
                    if([[[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]] objectForKey:[valueKeys objectAtIndex:i]] != nil)
					{
						[cell1.comboButton setTitle:[[[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]]objectForKey:[valueKeys objectAtIndex:i]] forState:UIControlStateNormal];
					}
				}
				
				if([[[compositeCells objectForKey:[valueKeys objectAtIndex:i]] objectForKey:TYPE] isEqualToString:DATETIME_CELL])
				{
                    
					cell1.dateTimeLabel.text = [NSString stringWithFormat:@"  %@: ", [valueKeys objectAtIndex:i]];
					cell1.fieldDateTime = [valueKeys objectAtIndex:i];
					
                    if([[[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]] objectForKey:[valueKeys objectAtIndex:i]] != nil)
					{
						cell1.dateTimeText.text = [[[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]]objectForKey:[valueKeys objectAtIndex:i]];
					}
				}
                
			}
			cell = cell1;
		}
		
		if([[composite objectForKey:COMPOSITE_CELL_TYPE] isEqualToString:DROPDOWN_CHECKBOX])
		{
			//NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"DropDownCheckBoxCell" owner:self options:nil];
			//DropDownCheckBoxCell * cell1 = (DropDownCheckBoxCell *)[nib objectAtIndex:0];
			
            DropDownCheckBoxCell * cell1 = [[[DropDownCheckBoxCell alloc] init] autorelease];
            [cell1 setReuseIdentifier:identifier];
            [cell1 setFormDataDict:[viewCells objectForKey:[fieldsL objectAtIndex:index]]];
            
            if ([[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]] == nil) {
                [[cell1 formDataDict] setObject:[[[NSMutableDictionary alloc] init] autorelease] forKey:[ServerKeys userEnteredValueKey]];
            }
            
			NSString * labelText = @"";
			NSString * checkBoxLabel = @"";
			NSArray * pickerData = nil;
			
			if([[valueKeys objectAtIndex:0] length] > 3)
			{
				labelText = (NSString *)[valueKeys objectAtIndex:0];
				checkBoxLabel = (NSString *)[[compositeCells objectForKey:[valueKeys objectAtIndex:1]] objectForKey:CELL_VALUES];
				pickerData = [[compositeCells objectForKey:[valueKeys objectAtIndex:0]] objectForKey:CELL_VALUES];
			}
			else
			{
				labelText = (NSString *)[valueKeys objectAtIndex:1];
				checkBoxLabel = (NSString *)[[compositeCells objectForKey:[valueKeys objectAtIndex:0]] objectForKey:CELL_VALUES];
				pickerData = [[compositeCells objectForKey:[valueKeys objectAtIndex:1]] objectForKey:CELL_VALUES];
			}
			
			if([[[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]] objectForKey:checkBoxLabel])
			{
				NSString * stringCheck = [[[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]] objectForKey:checkBoxLabel];
				if ([stringCheck isEqualToString:@"Yes"])
				{
					[cell1.checkboxButton setSelected:YES];
                    cell1.checkboxSelected = YES;
				}
				else
				{
					[cell1.checkboxButton setSelected:NO];
                    cell1.checkboxSelected = NO;
				}
			}
            
			if([[[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]] objectForKey:labelText])
			{
				NSString * stringCheck = [[[cell1 formDataDict] objectForKey:[ServerKeys userEnteredValueKey]] objectForKey:labelText];
				[cell1.comboButton setTitle:stringCheck forState:UIControlStateNormal];
			}
            
            
			cell1.pickerData = pickerData;
			cell1.fieldCheckBox = checkBoxLabel;
			cell1.fieldDropDown = labelText;
			cell1.checkBoxLabel.text = checkBoxLabel;
			cell1.dropDownLabel.text = [NSString stringWithFormat:@"  %@: ", labelText];
			cell = cell1;
		}
		
		if([[composite objectForKey:COMPOSITE_CELL_TYPE] isEqualToString:CHECKBOXES])
		{
			NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"CheckBoxesCell" owner:self options:nil];
			CheckBoxesCell * cell1 = (CheckBoxesCell *)[nib objectAtIndex:0];
            [cell1 setReuseIdentifier:identifier];
			[cell1 setFormDataDict:[viewCells objectForKey:[fieldsL objectAtIndex:index]]];
            
			NSString * labelText = (NSString *)[valueKeys objectAtIndex:0];
			NSArray * checkBoxLabels = [[[compositeCells objectForKey:[valueKeys objectAtIndex:0]] objectForKey:CELL_VALUES] allKeys];
			
            [cell1 setCheckBoxes:checkBoxLabels];
            
            adaptations = nil;
            adaptations = [cell1.formDataDict objectForKey:[ServerKeys userEnteredValueKey]];
            
            if (adaptations == nil) {
                adaptations = [[NSMutableDictionary alloc] init];
                [cell1.formDataDict setObject:adaptations forKey:[ServerKeys userEnteredValueKey]];
                [adaptations release];
            }
			
            cell1.label.text = [NSString stringWithFormat:@"  %@: ", labelText];
			cell1.field = labelText;
			cell1.selectedCheckBoxValues = adaptations;
            
            [cell1 drawCheckBoxes];

			
			cell = cell1;
		}
	}
	
	return cell;
}

#pragma mark - Controls methods

- (IBAction)notesButtonPressed
{
    NotesViewController * nvc = [[NotesViewController alloc] initWithNibName:@"NotesView" bundle:nil];    
    [nvc setDataDict:(NSMutableDictionary*)dictData];
    [nvc setParentTitle:self.title];
	[self.navigationController pushViewController:nvc animated:YES];
	[nvc release];
}

- (IBAction)cameraButtonPressed
{
	PhotosViewController * pvc = [[PhotosViewController alloc] initWithDictData:(NSMutableDictionary*)dictData];
	[pvc setPictureTag:formPath];
	[self.navigationController pushViewController:pvc animated:YES];
	[pvc release];
}

- (void)save
{
	if([[SingletonData sharedInstance] isDataChanged])
	{
        //[[[SingletonData sharedInstance] completedDict] setObject:@"YES" forKey:self.completeTitle];
        [(NSMutableDictionary*)dictData setObject:@"YES" forKey:[ServerKeys formCompleted]];
        
        //		[activityIndicator startAnimating];
        
    
        [[PropertySurveyAppAppDelegate appDelegate] showProgressAlertView:[self view] withTitle:@"Loading. . ."];
		
		//setup data to update to server
		[[[SingletonData sharedInstance] updateSurveyToServer] setObject:self.title forKey:[ServerKeys formName]];
		NSString * notes = [dictData objectForKey:@"Notes"];
		if(!notes)
		{
			notes = @"";
		}
        
        NSMutableDictionary * tempDict = [[[[SingletonData sharedInstance] updateSurveyToServer] objectForKey:[ServerKeys appointmentDetailsKey]] retain];
        
        [[[SingletonData sharedInstance] updateSurveyToServer] removeObjectForKey:[ServerKeys appointmentDetailsKey]];
        
		//[[[SingletonData sharedInstance] updateSurveyToServer] setObject:notes forKey:[ServerKeys formNotes]];
		
		//[[[SingletonData sharedInstance] updateSurveyToServer] setObject:formPath forKey:[ServerKeys formIndexPath]];
		
		NSString * date = [PropertySurveyHelperFunctions convertNSDateToServerDate:[NSDate date] fromForm:NO];
		[[[SingletonData sharedInstance] updateSurveyToServer] setObject:date forKey:[ServerKeys formDateSaved]];
		NSMutableArray * formFields = [[NSMutableArray alloc] init];
		//server data set
		
        // setting up the data;
        
        
        NSDictionary * viewCells = [dictData objectForKey:FIELDS];
        NSArray * fieldsL = self.fields;
        
        for (int i = 0; i < [fieldsL count]; i ++){
            if ([[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:[ServerKeys userEnteredValueKey]] != NULL){
                NSMutableDictionary * serverDict = [[NSMutableDictionary alloc] init];
                if ([[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:@"itemParamId"] != nil){
                    [serverDict setObject:[[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:@"itemParamId"] forKey:@"surveyItemParamId"];
                } else {
                    [serverDict setObject:@"0" forKey:@"surveyItemParamId"];
                }
                
                [serverDict setObject:[[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:TYPE] forKey:@"controlType"];
                
                if ([[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:@"paramId"] != nil){
                    [serverDict setObject:[[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:@"paramId"] forKey:@"surveyParameterId"];
                } else {
                    [serverDict setObject:@"0" forKey:@"surveyParameterId"];
                }
                
                [serverDict setObject:[fieldsL objectAtIndex:i] forKey:@"surveyParamName"];
                NSMutableArray * surveyParamItemField = [[NSMutableArray alloc] init];
                
                id tempValue = [[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:[ServerKeys userEnteredValueKey]];
                if (tempValue == nil){
                    tempValue = @"";
                }
                if([[[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:TYPE] isEqualToString:@"Date"]){
                    NSDateFormatter * dateFormatter = [[[NSDateFormatter alloc] init]autorelease];
                    [dateFormatter setDateFormat:@"d/M/yy 00:00:00 +0000"];
                    tempValue = [tempValue stringByAppendingFormat:@" 00:00:00 +0000"];
                    NSDate * tempDate = [dateFormatter dateFromString:tempValue];
                    tempValue = [PropertySurveyHelperFunctions convertNSDateToServerDate:tempDate fromForm:NO];
                }
                
                if ([tempValue isKindOfClass:[NSString class]]){
                    
                    NSMutableDictionary * tempParamItemFieldDict = [[NSMutableDictionary alloc] init];
                    
                    [tempParamItemFieldDict setObject:@"" forKey:@"isSelected"];
                    [tempParamItemFieldDict setObject:tempValue forKey:@"surveyParamItemFieldValue"];
                    
                    if ([[[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:@"Values"] isKindOfClass:[NSDictionary class]] || [[[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:@"Values"] isKindOfClass:[NSMutableDictionary class]]){
                        [tempParamItemFieldDict setObject:[NSNumber numberWithInt:[[[[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:@"Values"] objectForKey:tempValue] intValue]] forKey:@"surveyPramItemFieldId"];
                    }
                    
                    [surveyParamItemField addObject:tempParamItemFieldDict];
                    [tempParamItemFieldDict release];
                    
                } else if ([tempValue isKindOfClass:[NSDictionary class]]){
                    NSDictionary * tempTypeDict = (NSDictionary*)tempValue;
                    NSArray * tempTypeKeys = [tempTypeDict allKeys];
                    
                    for (int tt = 0; tt < [tempTypeKeys count]; tt ++){
                        NSMutableDictionary * tempParamItemFieldDict = [[NSMutableDictionary alloc] init];
                        NSString * tempTypeValue = [tempTypeDict objectForKey:[tempTypeKeys objectAtIndex:tt]];
                        if ([tempTypeValue isEqualToString:@"Yes"]){
                            [tempParamItemFieldDict setObject:@"Yes" forKey:@"isSelected"];
                        } else {
                            [tempParamItemFieldDict setObject:@"No" forKey:@"isSelected"];
                        }
                        
                        [tempParamItemFieldDict setObject:[tempTypeKeys objectAtIndex:tt] forKey:@"surveyParamItemFieldValue"];
                        [tempParamItemFieldDict setObject:[NSNumber numberWithInt:[[[[viewCells objectForKey:[fieldsL objectAtIndex:i]] objectForKey:@"Values"] objectForKey:[tempTypeKeys objectAtIndex:tt]] intValue]] forKey:@"surveyPramItemFieldId"];
                        
                        [surveyParamItemField addObject:tempParamItemFieldDict];
                        [tempParamItemFieldDict release];
                        
                    }
                }
                [serverDict setObject:surveyParamItemField forKey:@"surveyParamItemField"];
                [surveyParamItemField release];
                [formFields addObject:serverDict];
                [serverDict release];
            }
            
        }
        
		//save data to dict - for local saving
		NSMutableDictionary * temp = [[NSMutableDictionary alloc] initWithDictionary:[[SingletonData sharedInstance] tempFormsData]];
        [surveyDataFields setDictionary:temp];
        
        // COMENTED OUT BY HUSSAIN
		//formFields = [PropertySurveyHelperFunctions upDateFormFields:temp toArray:formFields];
		[[[SingletonData sharedInstance] updateSurveyToServer] setObject:formFields forKey:[ServerKeys formFields]];
        
        NSMutableDictionary * finalTempDict = [[SingletonData sharedInstance] updateSurveyToServer];
        //[finalTempDict setObject:serverDict forKey:@"surveyFields"];
        [finalTempDict setObject:formFields forKey:@"surveyFields"];
        if ([[dictData objectForKey:@"itemId"] stringValue]){
            [finalTempDict setObject:[[dictData objectForKey:@"itemId"] stringValue] forKey:@"itemId"];
        }
		
        [finalTempDict setObject:[tempDict objectForKey:@"appointmentId"] forKey:@"appointmentId"];
        [finalTempDict setObject:[[SingletonData sharedInstance] userId] forKey:@"completedBy"];
        
        if (![[tempDict objectForKey:CUSTOMER_LIST] isKindOfClass:[NSNull class]]){
            [finalTempDict setObject:[[[tempDict objectForKey:CUSTOMER_LIST] objectAtIndex:[self.calenderEvent.details.customerDefaultIndex intValue]] objectForKey:@"customerId"] forKey:@"customerId"];
        }

        
        [finalTempDict setObject:[[tempDict objectForKey:@"property"] objectForKey:kPropertyId] forKey:kPropertyId];
        
        //if (![notes isEqualToString:@""]){
        NSMutableDictionary * notesDict = [[NSMutableDictionary alloc] init];
        [notesDict setObject:notes forKey:@"surveyNotesDetail"];
        [notesDict setObject:date  forKey:@"surveyNotesDate"];
        
        [finalTempDict setObject:notesDict forKey:@"surveyNotes"];
        [notesDict release];
        //}
        
        
        
		jsonRequest = [finalTempDict JSONRepresentation];
		
		ServerConnection * conn = [[ServerConnection alloc] init];
		conn.requestString = jsonRequest;
		conn.httpMethod = @"POST";
		[conn postFormData:self requestSelector:@selector(saveFormData:)];
		[conn release];
		[temp release];
		[formFields release];
        
        
        [[[SingletonData sharedInstance] updateSurveyToServer] setObject:tempDict forKey:[ServerKeys appointmentDetailsKey]];
        //Added By aaban
        [[SingletonData sharedInstance] saveData];
	}
	else
	{
		UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"There were no changes made to the form" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
	}
    
	[[SingletonData sharedInstance] setDataChanged:NO];
}

- (void)checkSaves
{
	if([[SingletonData sharedInstance] isDataChanged])
	{
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"You have unsaved changes" message:@"Do you want to continue?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		[alert show];
		[alert release];
	}
	else
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}

- (void)completePressed
{
//    if ([[ticksDictionay allKeys] count]==[[dictData objectForKey:FIELDS] count])
//    {
//    
        [[PropertySurveyAppAppDelegate appDelegate] showProgressAlertView:[self view] withTitle:@"Loading. . ."];
        
        if([calenderEvent.details.surveyStatus isEqualToString:[ServerKeys pendingKey]])
        {
            startString = [NSString stringWithFormat:@"appointmentid=%@&username=%@",
                           calenderEvent.objId,
                           [[SingletonData sharedInstance] userName]];
            
            ServerConnection * conn = [[ServerConnection alloc] init];
            conn.httpMethod = @"GET";
            conn.startString = startString;
            if ([self.appointmentType isEqualToString:@"Gas"])
            {
                startString = [NSString stringWithFormat:@"appointmentid=%@&userid=%i",
                               calenderEvent.objId,
                               [[[SingletonData sharedInstance] userId] intValue]];
                [conn finishAppointmentForGas:self requestSelector:@selector(startAppointment:)];
            }
            else
                [conn finishAppointment:self requestSelector:@selector(startAppointment:)];
            [conn release];
        }
        else
        {
            NSString * message = [NSString stringWithFormat:@"The survey is already %@", calenderEvent.details.surveyStatus];
            UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [tempAlertView show];
            [tempAlertView release];
        }
//    }
//    else
//    {
//        NSString * message = [NSString stringWithFormat:@"Form Not Filled Completely"];
//		UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//		[tempAlertView show];
//		[tempAlertView release];
//    }
    
}

#pragma mark - Server CallBack

- (void)startAppointment:(NSData *)data
{
    //	[activityIndicator stopAnimating];
    

    [[PropertySurveyAppAppDelegate appDelegate] hideProgressAlertView];
    
    
	if(data == nil)
	{
		//TODO: if there is no response from server save the startString to a plist file
		UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@"" message:@"No response from server!\n Data has been saved locally" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[tempAlertView show];
		[tempAlertView release];
		
		[UpdateToServer setIsUpdatedToServer:NO];
		[UpdateToServer setAppointmentStarted:startString forAppointmentId:calenderEvent.objId];
	}
	NSString * canStart = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]autorelease];
    
    
	//if appointment can be started and this appointment has an appointmentID (sent from the server after its creation)
	if((!canStart || [canStart isEqualToString:@"true"]) &&
	   [[[SingletonData sharedInstance] appointmentSelectedForViewing] objectForKey:[ServerKeys appointmentIdKey]])
	{
		//if appointment data has been appended (for local saving)
		if([PropertySurveyHelperFunctions appendDataForAppointmentStarted:[[SingletonData sharedInstance] appointmentSelectedForViewing] withIndex:calenderEvent.arrayIndex])
		{
			//update status
			[[[SingletonData sharedInstance] appointmentSelectedForViewing] setObject:[ServerKeys pendingKey] forKey:[ServerKeys appointmentStatusKey]];
			calenderEvent.details.surveyStatus = [ServerKeys finishedKey];
			
            //NSMutableArray * a = [SingletonData sharedInstance].appointmentsData;
            
            int pending = [SingletonData sharedInstance].pendingBadgeNumber;
            pending--;
            
            
            
            [[[SingletonData sharedInstance] inprogressEvents] removeObjectAtIndex:self.selectedCalenderEventIndex];
            
            self.selectedCalenderEventIndex=[PropertySurveyHelperFunctions insertCalendarEventInSortedOrder:[[SingletonData sharedInstance] completedEvents] inCalendarEvent:self.calenderEvent];
            
            [SingletonData sharedInstance].pendingBadgeNumber = pending;
            
            for (int i = 0; i < [[SingletonData sharedInstance].appointmentsData count]; i++){
                NSMutableDictionary * tce = [[SingletonData sharedInstance].appointmentsData objectAtIndex:i];
                
                //if ([[[tce objectForKey:@"appointmentId"] stringValue] isEqualToString:calenderEvent.objId]){
                if ([[tce objectForKey:@"appointmentId"] intValue] == [calenderEvent.objId intValue]){
                    //tce.details.surveyStatus = [ServerKeys finishedKey];
                    [tce setObject:[ServerKeys finishedKey] forKey:@"appointmentStatus"];
                    break;
                }
            }
            
            
            
			
			//append with other appointments started by this user and saved locally
			if(![[SingletonData sharedInstance] customerData])
			{
				NSMutableDictionary * tempMutDict = [[NSMutableDictionary alloc] init];
				[[SingletonData sharedInstance] setCustomerData:tempMutDict];
				[tempMutDict release];
			}
			[[[SingletonData sharedInstance] customerData] setObject:[[SingletonData sharedInstance] appointmentSelectedForViewing]
															 forKey:[[[SingletonData sharedInstance] appointmentSelectedForViewing] objectForKey:[ServerKeys appointmentIdKey]]];
			UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Survey Finished" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[tempAlertView show];
			[tempAlertView release];
		}
		/*else
         {
         UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Cannot start survey!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
         [tempAlertView show];
         [tempAlertView release];
         }*/
        [self.navigationController popViewControllerAnimated:YES];
	}
    else if ([canStart isEqualToString:@"true"])
    {
        calenderEvent.details.surveyStatus = [ServerKeys finishedKey];
        
        for (int i = 0; i < [[SingletonData sharedInstance].appointmentsData count]; i++){
            NSMutableDictionary * tce = [[SingletonData sharedInstance].appointmentsData objectAtIndex:i];
            
            //if ([[[tce objectForKey:@"appointmentId"] stringValue] isEqualToString:calenderEvent.objId]){
            if ([[tce objectForKey:@"appointmentId"] intValue] == [calenderEvent.objId intValue]){
                //tce.details.surveyStatus = [ServerKeys finishedKey];
                [tce setObject:[ServerKeys finishedKey] forKey:@"appointmentStatus"];
                break;
            }
        }
        
        int pending = [SingletonData sharedInstance].pendingBadgeNumber;
        pending--;
        
        
        
        [[[SingletonData sharedInstance] inprogressEvents] removeObjectAtIndex:self.selectedCalenderEventIndex];
        
        self.selectedCalenderEventIndex=[PropertySurveyHelperFunctions insertCalendarEventInSortedOrder:[[SingletonData sharedInstance] completedEvents] inCalendarEvent:self.calenderEvent];
        
        [SingletonData sharedInstance].pendingBadgeNumber = pending;
        
        UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Survey Finished" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [tempAlertView show];
        [tempAlertView release];
        
    }
    
	else if([canStart isEqualToString:@"true"] &&
			![[[SingletonData sharedInstance] appointmentSelectedForViewing] objectForKey:[ServerKeys appointmentIdKey]])
	{
		//TODO: what to do if appointmentID is not available => new appointment not synched with server
	}
	else
	{
		UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Survey was not marked as completed, please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [tempAlertView show];
        [tempAlertView release];
	}
}

#pragma mark - AlertView delegate method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if(buttonIndex == 1)
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
}

#pragma mark - UITableView Datasource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
	if(dictData != nil)
	{
        if (isLocalFieldScreen)
        {
            //*** changed by Afnan, original line is below ***//
            //return [[dictData objectForKey:FIELDS] count] + 1;
            
            CLS_LOG(@"dictionary count: %d", [[dictData objectForKey:FIELDS] count]);
            return [[dictData objectForKey:FIELDS] count] + 1;
        }
        else
            return [[dictData objectForKey:FIELDS] count];
	}
	else
	{
		return 0;
	}
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString * identifier = @"MyCell";
    NSDictionary * viewCells = [dictData objectForKey:FIELDS];
    NSString *identifier;
    
    int index = (indexPath.row > 3 && isLocalFieldScreen) ? indexPath.row - 1 : indexPath.row;
    
    if (indexPath.row==3 && isLocalFieldScreen)
        identifier=@"localField";
    else
        identifier=[NSString stringWithFormat:@"%@%@%d", [dictData objectForKey:TYPE], [[viewCells objectForKey:[fields objectAtIndex:index]] objectForKey:TYPE], indexPath.row];
    
	UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (indexPath.row==3 && isLocalFieldScreen)
    {
        if (!cell)
        {
            cell = [[[ApplianceCountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
            cell.contentView.contentMode = UIViewContentModeScaleToFill;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        [[(ApplianceCountCell*)cell firstLabelVal] setText:@"Appliances"];
        int fontSize=(int )FONT_SIZE;
        [[(ApplianceCountCell *) cell firstLabelVal] setFont:[UIFont boldSystemFontOfSize:fontSize]];
        [[(ApplianceCountCell*)cell secondLabelVal] setText:[NSString stringWithFormat:@"%i", noOfAppliances]];
        cell.backgroundColor=[UIColor whiteColor];
    }
    else if (viewCells != nil && [viewCells count] > 0)
	{
		//NSArray * fields = [[viewCells allKeys] sortedArrayUsingFunction:sortingFunction context:nil];
		if([[dictData objectForKey:TYPE] isEqualToString:MENU_TYPE])
		{
			UIImage * updateImg;
            
            //if ([[SingletonData sharedInstance].completedDict objectForKey:[(NSString *)[fields objectAtIndex:indexPath.row] stringByAppendingFormat:@"|%@",completeTitle]]){
            NSMutableDictionary * tempDict = [[dictData objectForKey:FIELDS] objectForKey:[self.fields objectAtIndex:index]];
            
            NSString * tempString = [tempDict objectForKey:[ServerKeys formCompleted]];
            
            //Change by hussain
            //NSArray * tempArray = [[[tempDict objectForKey:FIELDS] allKeys] sortedArrayUsingFunction:sortingFunction context:nil];
            NSArray * tempArray = [tempDict objectForKey:@"Order"];
            
            int count = 0;
            
            for (int i = 0; i < [tempArray count]; i ++)
            {
                NSMutableDictionary * tDict = [[tempDict objectForKey:FIELDS] objectForKey:[tempArray objectAtIndex:i]];
                NSString* tString = [tDict objectForKey:[ServerKeys formCompleted]];
                if ([tString isEqualToString:@"YES"])
                {
                    count ++;
                }
                else
                {
                    
                }
            }
            
            if (!tempString && [tempArray count] > 0)
            {
                if (count == [tempArray count]) {
                    tempString = @"YES";
                }
            }
            
            if (!cell)
			{
				cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
				cell.contentView.contentMode = UIViewContentModeScaleToFill;
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
			}
            
            if ([tempString isEqualToString:@"YES"])
            {
                self.totalNumberOfTicks=[NSNumber numberWithInt:[self.totalNumberOfTicks intValue]+1];
                [ticksDictionay setObject:@"Yes" forKey:[self.fields objectAtIndex:index]];
                updateImg = [UIImage imageNamed:TICK_IMG];
            } else {
                updateImg = [UIImage imageNamed:CROSS_IMG];
            }
            
            //if ([self.totalNumberOfTicks intValue]==[[dictData objectForKey:FIELDS] count])
            if ([[ticksDictionay allKeys] count]==[[dictData objectForKey:FIELDS] count])
            {
                [(NSMutableDictionary*)dictData setObject:@"YES" forKey:[ServerKeys formCompleted]];
                //tempString = [dictData objectForKey:[ServerKeys formCompleted]];
            }
            
            cell.textLabel.text = [self.fields objectAtIndex:index];
            int fontSize=(int )FONT_SIZE;
            [cell.textLabel setFont:[UIFont boldSystemFontOfSize:fontSize]];
            
            //cell.textLabel.text = [NSString stringWithFormat:@"%d|%d|%@", [[tempDict objectForKey:FIELDS] count], [tempArray count], [fields objectAtIndex:indexPath.row]];
            
			//cell.imageView.image = updateImg;
			UIImageView * updateImgView = [[UIImageView alloc] initWithImage:updateImg];
            
            [updateImgView setBackgroundColor:[UIColor clearColor]];
            
			updateImgView.frame = CGRectMake(230, 10, 20, 20);
			[cell.contentView addSubview:updateImgView];
            [updateImgView release];
		}
		else if([[dictData objectForKey:TYPE] isEqualToString:FORM_TYPE])
		{
			if (!cell)
			{
				NSString * cellType = [[viewCells objectForKey:[self.fields objectAtIndex:index]] objectForKey:TYPE];
				if([self isCustomCell:cellType])
				{
					//cell = [self initCustomCell:cell withDict:viewCells andIndexPath:indexPath];
                    cell = [self initCustomCell:cell withDict:viewCells withReuseIdentifier:identifier andIndexPath:indexPath];
				}
				else
				{
					cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
					cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					cell.textLabel.text = [self.fields objectAtIndex:index];
				}
			}
		}
	}
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==3) {
        
        //  TextFieldCell * cell=
        
    }
    
    
    int index = (indexPath.row > 3 && isLocalFieldScreen) ? indexPath.row - 1 : indexPath.row;
    
    if (isLocalFieldScreen && indexPath.row==3)
    {
        ApplianceDetailsViewController *adViewController = [[ApplianceDetailsViewController alloc] initWithNibName:@"ApplianceDetailsViewController" bundle:[NSBundle mainBundle]];
        adViewController.appointmentID = [[[SingletonData sharedInstance] appointmentSelectedForViewing] objectForKey:@"appointmentId"];
        adViewController.propertyId = self.propertyId;
        [self.navigationController pushViewController:adViewController animated:YES];
        [adViewController release];
    }
    else
        if(![[dictData objectForKey:TYPE] isEqualToString:FORM_TYPE])
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSMutableDictionary * dict = [[dictData objectForKey:FIELDS] objectForKey:[self.fields objectAtIndex:index]];
            if(surveyDataFields != nil && [surveyDataFields objectForKey:[self.fields objectAtIndex:index]] != nil)
            {
                if([[surveyDataFields objectForKey:[self.fields objectAtIndex:index]] isKindOfClass:[NSDictionary class]])
                {
                    nextSurveyData = [[NSMutableDictionary dictionaryWithDictionary:[surveyDataFields objectForKey:[self.fields objectAtIndex:index]]] retain];
                }
                else
                {
                    nextSurveyData = [[surveyDataFields objectForKey:[self.fields objectAtIndex:index]] retain];
                }
            }
            if(dict != nil && [dict count] > 0)
            {
                GenericViewController * gvc = [[GenericViewController alloc] initWithTitle:(NSString *)[self.fields objectAtIndex:index] parentTitle:[self title] withViewTitles:dict withData:nextSurveyData prevData:surveyDataFields withKey:[self.fields objectAtIndex:index]];
                
                [gvc setIsMainSurveyMenu:NO];
                [gvc setCalenderEvent:calenderEvent];
                gvc.propertyId=self.propertyId;
                //changed By aaban, dAted 17-12-12
                [gvc setAppointmentType:self.appointmentType];
                [gvc setCompleteTitle:[gvc.completeTitle stringByAppendingFormat:@"|%@", self.title]];
                [[SingletonData sharedInstance] setDataChanged:NO];
                [self.navigationController pushViewController:gvc animated:YES];
                [gvc release];
            }
        }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isLocalFieldScreen && indexPath.row ==3)
        return 44;
    else if([[dictData objectForKey:TYPE] isEqualToString:FORM_TYPE])
	{
		NSArray * allKeys = self.fields;
        //allKeys = [allKeys sortedArrayUsingFunction:sortingFunction context:nil];
        
        int index = (indexPath.row > 3 && isLocalFieldScreen) ? indexPath.row - 1 : indexPath.row;
        
		if([[[[dictData objectForKey:FIELDS] objectForKey:[allKeys objectAtIndex:index]] objectForKey:TYPE] isEqualToString:COMPOSITE_CELL])
		{
            //NSString * tempString = [[[dictData objectForKey:FIELDS] objectForKey:[allKeys objectAtIndex:indexPath.row]] objectForKey:COMPOSITE_CELL_TYPE];
            //NSString * temaString = [[[dictData objectForKey:FIELDS] objectForKey:[allKeys objectAtIndex:indexPath.row]] objectForKey:TYPE];
            
            if ([[[[dictData objectForKey:FIELDS] objectForKey:[allKeys objectAtIndex:index]] objectForKey:COMPOSITE_CELL_TYPE] isEqualToString:CHECKBOXES]){
                
                NSDictionary * tempDict = [[dictData objectForKey:FIELDS] objectForKey:[allKeys objectAtIndex:index]];
                NSDictionary * tempFields = [[[tempDict objectForKey:CELL_VALUES] objectForKey:[[[tempDict objectForKey:CELL_VALUES] allKeys] objectAtIndex:0]] objectForKey:CELL_VALUES];
                NSArray * tempA = [tempFields allKeys];
                
                //NSArray * tempArray = [[[[[[dictData objectForKey:FIELDS] objectForKey:[[[dictData objectForKey:FIELDS]  allKeys] objectAtIndex:indexPath.row]] objectForKey:CELL_VALUES] objectForKey:[[[[[dictData objectForKey:FIELDS] objectForKey:[fields objectAtIndex:indexPath.row]] objectForKey:CELL_VALUES] allKeys] objectAtIndex:0]] objectForKey:CELL_VALUES] allKeys];
                float height = (60 + ([tempA count] * 25));
                return height;
                
            }
            else
            {
                return COMPOSITE_CELL_SIZE;
            }
		}
        
        if ([[[[dictData objectForKey:FIELDS] objectForKey:[allKeys objectAtIndex:index]] objectForKey:TYPE] isEqualToString:CHECKBOXES]){
            NSDictionary * tempDict = [[dictData objectForKey:FIELDS] objectForKey:[allKeys objectAtIndex:index]];
            NSDictionary * tempFields = [tempDict objectForKey:@"Values"];
            NSArray * tempA = [tempFields allKeys];
            
            //NSArray * tempArray = [[[[[[dictData objectForKey:FIELDS] objectForKey:[[[dictData objectForKey:FIELDS]  allKeys] objectAtIndex:indexPath.row]] objectForKey:CELL_VALUES] objectForKey:[[[[[dictData objectForKey:FIELDS] objectForKey:[fields objectAtIndex:indexPath.row]] objectForKey:CELL_VALUES] allKeys] objectAtIndex:0]] objectForKey:CELL_VALUES] allKeys];
            float height = (60 + ([tempA count] * 25));
            return height;
        }
		
		return CUSTOM_CELL_SIZE;
	}
    return DEFAULT_CELL_SIZE;
}

#pragma mark - Server Callback

- (void)saveFormData:(NSData *)data
{
    NSString * checkSession = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]autorelease];
    if([checkSession JSONValue])//check if session is valied
    {
        SBJSON *json =[[SBJSON new]autorelease];
        if([[json objectWithString:checkSession] isKindOfClass:[NSMutableDictionary class]]){
            NSMutableDictionary *serverResponse =[json objectWithString:checkSession];
            if ([serverResponse objectForKey:@"errorCode"]){
                if ([[serverResponse objectForKey:@"errorCode"] isEqualToString:@"100"]){
                    
                    UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@"Session Expired"
                                                                             message:@"Please re-login!"
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil];
                    [tempAlertView show];
                    [tempAlertView release];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    return;
                }
            }
        }
    }
	if(data == nil)
	{
		//save this to updatetoserver file/dict
		[UpdateToServer setIsUpdatedToServer:NO];
		[UpdateToServer setFormsData:jsonRequest];
        //		[activityIndicator stopAnimating];
        
    
        [[PropertySurveyAppAppDelegate appDelegate] hideProgressAlertView];
        
        
		return;
	} 
	NSString * formDidSave = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]autorelease];
    
	if([formDidSave isEqualToString:@"true"])
	{
		UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Form has been saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
	}
	else
	{
		UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Form could not be saved to server. Reason %@", formDidSave] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
		[UpdateToServer setIsUpdatedToServer:NO];
	}
    //	[activityIndicator stopAnimating];
    

    [[PropertySurveyAppAppDelegate appDelegate] hideProgressAlertView];
    
}

@end
