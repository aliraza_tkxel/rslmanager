//
//  PSCameraViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCameraViewController.h"
#import "PSBarButtonItem.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "PSCollectionViewCell.h"
#import "PSAddNotesViewController.h"
#import "PSCameraButtonCell.h"
#import "PSSurveyViewController.h"
#import "PSPropertyPictureManager.h"
#import "PhotographCollectionViewCell.h"
#import "PSImagePickerController.h"
#import "PropertyPicture+JSON.h"
#import "PSUploadDefectPictureService.h"

#import "MWPhotoBrowser.h"

static NSString *collectionViewCellIdentifier = @"collecitonViewCell";
static NSString *cameraButtonCellIdentifier = @"cameraButtonCell";

@interface PSCameraViewController ()
{
    
}

@end

@implementation PSCameraViewController

@synthesize propertyPictures;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self loadNavigationonBarItems];
    [self.collectionView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    
    [self.collectionView registerClass:[PhotographCollectionViewCell class] forCellWithReuseIdentifier:collectionViewCellIdentifier];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDelete:) name:kPropertyPicturesRemoveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureSave:) name:kPropertyPictureSaveNotification object:nil];
    
    
    if (!self.propertyPictures) {
        
        if (self.defect) {
            if([self.defect.defectID intValue]<0)
            {
                self.propertyPictures=[NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:self.defect.defectID andJournalID:self.defect.journalId]];
            }
            else
            {
                self.propertyPictures=[NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:self.defect.defectID]];
            }
            
        }else
        {
            self.propertyPictures=[NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] propertyPicturesWithPropertyIdentifier:self.appointment.appointmentToProperty appointmentId:self.appointment.appointmentId itemId:self.itemId]];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    
    PSBarButtonItem * cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                            style:PSBarButtonItemStyleCamera
                                                                           target:self
                                                                           action:@selector(onClickBtnCamera:)];
    self.homeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                             style:PSBarButtonItemStyleHome
                                                            target:self
                                                            action:@selector(onClickHomeButton)];
    
    [self setLeftBarButtonItems:@[backButton]];
    
    if (self.mainViewController)
    {
        [self setRightBarButtonItems:@[cameraButton ,self.homeButton]];
    }
    else
    {
        [self setRightBarButtonItems:@[cameraButton]];
    }
    [self setTitle:[NSString stringWithFormat:@"%@ Condition Survey",self.appointment.appointmentType]];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickHomeButton
{
    CLS_LOG(@"onClickHomeButton");
    if (self.mainViewController)
    {
        [self.navigationController popToViewController:self.mainViewController animated:YES];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout Protocol methods
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(90, 90);
}


#pragma mark - UICollectionView Delegate Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.propertyPictures count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PhotographCollectionViewCell *cell = (PhotographCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCellIdentifier forIndexPath:indexPath];
    
    [cell setDataObject:[self.propertyPictures objectAtIndex:[indexPath row]]];
    [cell.cellImageView setImage:nil];
    [cell loadImageView];
    
    cell.tag=indexPath.row;
    cell.gestureDelegate=self;
    
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    browser.allPhotos=self.propertyPictures;
    browser.currentPageIndex=indexPath.row;
    browser.displayActionButton = NO;
    
    [self.navigationController pushViewController:browser animated:NO];
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 0, 5);
}

#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image;
    if([info objectForKey:UIImagePickerControllerEditedImage])
    {
        image = [info valueForKey:UIImagePickerControllerEditedImage];
    }
    else
    {
        image = [info valueForKey:UIImagePickerControllerOriginalImage];
    }
    
    [self saveImageToServer:image];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: LOC(@"KEY_ALERT_SAVE_FAILURE")
                              message: LOC(@"KEY_ALERT_IMAGE_SAVE_FAILURE")
                              delegate: nil
                              cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                              otherButtonTitles:nil];
        [alert show];
    }else
    {
        
    }
}


-(void) saveImageToServer:(UIImage*) image
{
    //Setting status to modification for Appointments
    if(self.appointment)
    {
        [[PSDataPersistenceManager sharedManager] updateModificationStatusOfAppointments:[NSArray arrayWithObject:self.appointment] toModificationStatus:[NSNumber numberWithBool:YES]];
    }
    //create an instance for ProprtyPhoto
     //   [self showActivityIndicator:LOC(@"Saving Picture")];
        
        NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
        [requestParameters setObject:image forKey:kPropertyImage];
        [requestParameters setObject:@".jpg" forKey:kFileExtension];
        [requestParameters setObject:@"false" forKey:kIsDefault];
        [requestParameters setObject:[NSNumber numberWithInt:0] forKey:@"propertyPicId"];
        [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
        [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    [requestParameters setObject:isEmpty(self.appointment.appointmentToProperty.propertyId)?[NSNull null]:self.appointment.appointmentToProperty.propertyId forKey:kPropertyId];
    [requestParameters setObject:isEmpty(self.appointment.appointmentToProperty.schemeId)?[NSNull null]:self.appointment.appointmentToProperty.schemeId forKey:kSchemeID];
    [requestParameters setObject:isEmpty(self.appointment.appointmentToProperty.blockId)?[NSNull null]:self.appointment.appointmentToProperty.blockId forKey:kSchemeBlockID];
        [requestParameters setObject:[UtilityClass getUniqueIdentifierStringWithUserName:[[SettingsClass sharedObject]loggedInUser].userName] forKey:kUniqueImageIdentifier];
    [requestParameters setObject:_itemId forKey:kItemId];
    [requestParameters setObject:!isEmpty(_heatingId)?_heatingId:[NSNull null] forKey:kHeatingId];
    if (self.imageType == CameraViewImageTypeDefect) {
        
        if(self.defect) {
            [requestParameters setObject:self.defect.defectID forKey:@"faultId"];
        }
        else {
            [requestParameters setObject:@"-1" forKey:@"faultId"];
        }
        
        [[PSDataPersistenceManager sharedManager] saveDefectPictureInfo:requestParameters withProperty:self.defect];
    }else
    {
        [requestParameters setObject:self.appointment.appointmentType forKey:@"type"];
        
        [requestParameters setObject:self.appointment.appointmentId forKey:kAppointmentId];
        
        [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
        
        [[PSDataPersistenceManager sharedManager] savePropertyPictureInfo:requestParameters withProperty:self.appointment.appointmentToProperty];
        
    }
    
    
}


-(void) deleteImageFromServer:(PropertyPicture*) propertyPicture
{
    
    //Server URL
    //create an instance for ProprtyPhotos
    [self showActivityIndicator:LOC(@"Removing Picture")];
    
    //Soft Deletion
        [self onPictureDelete:nil];
    
}

#pragma mark - CameraHeaderDelagate

- (void)onClickBtnCamera:(id) sender {
    
    UIActionSheet * _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:          nil
                                                       otherButtonTitles:NSLocalizedString(@"Take a Photo", nil),
                                     NSLocalizedString(@"Choose from Library", nil),
                                     nil] ;
    
    _actionsSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    _actionsSheet.tag=1;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [_actionsSheet showFromBarButtonItem:sender animated:YES];
    } else {
        [_actionsSheet showInView:self.view];
    }
    
}

-(void) showCameraOfSourceType:(UIImagePickerControllerSourceType) type
{
    if ([UIImagePickerController isSourceTypeAvailable:type])
    {
        PSImagePickerController *imagePicker = [[PSImagePickerController alloc] init];
        
        
        imagePicker.delegate = self;
        imagePicker.sourceType = type;
        imagePicker.allowsEditing = YES;
        
        if( type == UIImagePickerControllerSourceTypeCamera)
        {
            imagePicker.showsCameraControls = YES;
        }
        
        [self presentViewController:imagePicker animated:YES completion:^{
            
        }];
        
        
    }
    
}

- (void) setAsDefault:(PropertyPicture*) propertyPicture
{
    
    NSDictionary * dict=[NSDictionary dictionaryWithObject:@"true" forKey:kPropertyPictureIsDefault];
    
    [[PSDataUpdateManager sharedManager] updatePropertyPicture:propertyPicture forDictionary:dict];
    
}


#pragma mark Gesture Delegate
-(void)handleLongPressGesture:(int)selectedIndex
{
    self.selectedIndex=selectedIndex;
    
    [self showActionSheet];
}

#pragma mark ActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    CLS_LOG(@"Clicked Index %d",buttonIndex);
    
    if (actionSheet.tag==1) {
        
        
        ///if (actionSheet == _actionsSheet)
        {
            // Actions
            if (buttonIndex != actionSheet.cancelButtonIndex) {
                if (buttonIndex == actionSheet.firstOtherButtonIndex) {
                    
                    [self showCameraOfSourceType:UIImagePickerControllerSourceTypeCamera];
                    
                } else if (buttonIndex == actionSheet.firstOtherButtonIndex + 1) {
                    [self showCameraOfSourceType:UIImagePickerControllerSourceTypePhotoLibrary]; return;
                }
                
            }
        }
        
    }else if (actionSheet.tag==2)
    {
     
        if (actionSheet.cancelButtonIndex==buttonIndex) {
            
        }else
        if(buttonIndex == actionSheet.destructiveButtonIndex) {
            
            [self deleteImageFromServer:[self.propertyPictures objectAtIndex:self.selectedIndex]];
            
        }
        else if(buttonIndex == actionSheet.firstOtherButtonIndex) {
            
            [self setAsDefault:[self.propertyPictures objectAtIndex:self.selectedIndex]];
        }
        else if(buttonIndex == actionSheet.firstOtherButtonIndex+1) {
            
            PropertyPicture * picture =[self.propertyPictures objectAtIndex:self.selectedIndex];
            [picture uploadPicture];
        }
        
    }else  if (actionSheet.tag==3)
    {
        if(buttonIndex == actionSheet.firstOtherButtonIndex) {
            
            PropertyPicture * picture =[self.propertyPictures objectAtIndex:self.selectedIndex];
            [picture uploadPicture];
        }
    }
}


#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}



#pragma mark ActionSheet Methods

-(void)showActionSheet {
    
    UIActionSheet * _actionsSheet=nil;
    
     PropertyPicture * picture=[self.propertyPictures objectAtIndex:self.selectedIndex];
    
    if (!self.defect) {
        
        if (picture.imagePath) {
            
            _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:@"Delete"
                                               otherButtonTitles:NSLocalizedString(@"Set as Default", nil),
                             nil] ;
            
        }else
        {
            
            _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:@"Delete"
                                               otherButtonTitles:NSLocalizedString(@"Set as Default", nil),NSLocalizedString(@"Synch", nil),
                             
                             nil] ;
            
        }
        
        _actionsSheet.tag=2;
        
        
    }else
    {
        if (!picture.imagePath) {
            
            _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil
                                               otherButtonTitles:NSLocalizedString(@"Synch", nil),
                             
                             nil] ;
            
        }
        
        _actionsSheet.tag=3;
    }
    
    _actionsSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [_actionsSheet showInView:self.view];
    
}


-(void) onPictureRefresh{
    
    [self.collectionView reloadData];
    
}

-(void) onPictureDelete:(NSNotification*) notification
{
    [self hideActivityIndicator];
    
    PropertyPicture * picture=[self.propertyPictures objectAtIndex:self.selectedIndex];
    //marking deleted image for user offline support
    //Dirty Marking
    [[PSDataUpdateManager sharedManager] markDeletePropertyPicture:picture];
    [self.propertyPictures removeObject:picture];
    [self.collectionView reloadData];
}

-(void) onPictureSave:(NSNotification*) notification
{
    [self hideActivityIndicator];
    
    PropertyPicture * picture=notification.object;
    if (picture) {

        if (self.defect)
        {
            if([self.defect.defectID intValue]<0)
            {
                self.propertyPictures=[NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:self.defect.defectID andJournalID:self.defect.journalId]];
            }
            else
            {
                self.propertyPictures=[NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:self.defect.defectID]];
            }
            
        }else
        {
            self.propertyPictures=[NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] propertyPicturesWithPropertyIdentifier:self.appointment.appointmentToProperty appointmentId:self.appointment.appointmentId itemId:self.itemId]];
        }
        [self.collectionView reloadData];
    }
    
}

-(void) dealloc{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPropertyPicturesRemoveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPropertyPictureSaveNotification object:nil];
}
@end
