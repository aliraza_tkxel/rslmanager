//
//  PhotographTableViewCell.m
//  ReidmarkPropertyManager
//
//  Created by My Lion on 9/27/12.
//  Copyright (c) 2012 Tkxel. All rights reserved.
//

#import "PhotographCollectionViewCell.h"
#import "PropertyPicture+JSON.h"
#import "UIImageView+WebCache.h"



@implementation PhotographCollectionViewCell
@synthesize cellImageView=_cellImageView;
@synthesize dataObject;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _cellImageView=[[FBRImageView alloc]initWithFrame:CGRectMake(0,0, 90, 90)];
        
        
        _cellImageView.contentMode=UIViewContentModeScaleAspectFit;
        
        _cellImageView.thumbnail=YES;
        
        [self addSubview:_cellImageView];
    
        [self setBackgroundColor:[UIColor lightGrayColor]];
    
        [self addGestures];
        
        // Add Gestures
        
        
        // Initialization code
    }
    
    return self;
}


-(void) loadImageView
{
    NSString * imagePath=[self.dataObject getPicturePath];
    
    if(!isEmpty(imagePath) && [imagePath rangeOfString:@"null"].location == NSNotFound) {
        [self.cellImageView setImageWithURL:[NSURL URLWithString:imagePath] andAddBorder:YES];
    }
    
}


#pragma mark Gestures

-(void)addGestures {

    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc]
                                                         initWithTarget:self action:@selector(onLongPress:)];
    longPressRecognizer.minimumPressDuration = 0.5;
    [self addGestureRecognizer:longPressRecognizer];
}

-(void)onLongPress:(UILongPressGestureRecognizer*)pGesture
{
    if (pGesture.state == UIGestureRecognizerStateRecognized)
    {
        //Do something to tell the user!
    }
    if (pGesture.state == UIGestureRecognizerStateEnded) {
        
        CLS_LOG(@"onLongPress");
        
        if(self.gestureDelegate && [self.gestureDelegate respondsToSelector:@selector(handleLongPressGesture:)]) {
            [self.gestureDelegate handleLongPressGesture:self.tag];
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
