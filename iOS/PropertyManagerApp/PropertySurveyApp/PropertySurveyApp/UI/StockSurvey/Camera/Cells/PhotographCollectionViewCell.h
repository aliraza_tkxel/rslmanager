//
//  PhotographTableViewCell.h
//  ReidmarkPropertyManager
//
//  Created by My Lion on 9/27/12.
//  Copyright (c) 2012 Tkxel. All rights reserved.
//

#define ACTIVITY_INDICATOR_TAG 123456

@protocol PhotographCellDelegate <NSObject>

-(void) handleLongPressGesture:(int) tag;

@end


@interface PhotographCollectionViewCell : UICollectionViewCell

@property(nonatomic,weak) id  dataObject;
@property (strong, nonatomic)  FBRImageView *cellImageView;
@property (weak, nonatomic) id gestureDelegate;

-(void) loadImageView;

@end
