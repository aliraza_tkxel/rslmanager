//
//  PSSurveyViewController.h
//  PropertySurveyApp
//
//  Created by TkXel on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSBarButtonItem;
@class PSSurveyCheckboxCell;
@class PSSurveyMenuCell;
@class PSSurveyDropdownCell;
@class PSSurveyTextboxCell;
@class PSSurveyDateCell;
@class PSSurveyTextViewCell;

typedef NS_ENUM(NSInteger, SurveyDataType)
{
    SurveyDataTypeNone = -1,
    
    SurveyDataTypeMenu = 0,
    SurveyDataTypeForm = 1,
    SurveyDataTypeDropDown = 2,
    SurveyDataTypeCheckBox = 3,
    SurveyDataTypeTextBox = 4,
    SurveyDataTypeDate = 5,
    SurveyDataTypeTextView = 6
};

/*!
 @discussion
 SurveyFormDelegate is used to send form field value change call backs. All Form Cells send callsbacks on this method.
 
 */
@protocol SurveyFormDelegate <NSObject>
- (void) didChangeSurveyFormWithData:(NSDictionary *)formDataDictionary;
- (void) didChangeSurveyFormNotes:(NSString *)notesString;
@end

@interface PSSurveyViewController : PSCustomViewController <UITableViewDataSource, UITableViewDelegate, SurveyFormDelegate, UINavigationControllerDelegate>
{
    NSString *surveyTitle;
    SurveyDataType dataType;
    NSMutableDictionary * surveyData;
    
    NSMutableArray * surveyDataTitles;
    NSMutableDictionary * surveyDataFields;
    BOOL isNameAddressLabel;
    BOOL isImage;

    BOOL isLandlordAppliance;
    BOOL isLocalFieldScreen;
}
- (IBAction)onClickCompleteAppointment:(id)sender;
@property (weak,   nonatomic) PSSurveyViewController *mainViewController;
@property (strong, nonatomic) PSBarButtonItem *homeButton;
@property (strong, nonatomic) PSBarButtonItem *cameraButton;
@property (strong, nonatomic) PSBarButtonItem *notesButton;
@property (strong, nonatomic) PSBarButtonItem *backButton;

@property (assign, nonatomic, setter = setMainSurveyMenu:) BOOL isMainSurveyMenu;

@property (strong,   nonatomic) Appointment *appointment;
@property (strong, nonatomic) IBOutlet UIButton *btnMarkComplete;
@property (strong, nonatomic) IBOutlet UIView *propertyInfoView;
@property (strong, nonatomic) IBOutlet UIView *markCompleteView;


@property (strong, nonatomic) NSMutableDictionary * surveyData;
@property (strong, nonatomic) IBOutlet FBRImageView * imageView;

@property (strong, nonatomic) IBOutlet UILabel *lblTenantName;
@property (strong, nonatomic) IBOutlet UILabel *lblPropertyAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblCertificateExpiry;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet PSSurveyCheckboxCell *surveyCheckboxCell;
@property (weak, nonatomic) IBOutlet PSSurveyMenuCell *surveyMenuCell;
@property (weak, nonatomic) IBOutlet PSSurveyDropdownCell *surveyDropdownCell;
@property (weak, nonatomic) IBOutlet PSSurveyTextboxCell *surveyTextboxCell;
@property (weak, nonatomic) IBOutlet PSSurveyDateCell *surveyDateCell;
@property (weak, nonatomic) IBOutlet PSSurveyTextViewCell *surveyTextViewCell;


@property (strong, nonatomic) NSDictionary * dataDictionary;

@property (strong, nonatomic) NSString * completeTitle;
@property (strong, nonatomic) NSString * startString;
@property (strong, nonatomic) NSString * parentTitle;
@property (strong, nonatomic) NSString * jsonRequest;


//SurveyData Values
@property (strong, nonatomic) NSNumber * appointmentId;
@property (strong, nonatomic) NSString * propertyId;
@property (strong, nonatomic) NSNumber * itemId;
@property (strong, nonatomic) NSString * surveyFormName; //Form Path
@property (strong, nonatomic) NSString * surveyNotesDetail;
@property (strong, nonatomic) NSDate   * surveyNotesDate;


- (id)initWithTitle:(NSString *)title surveyData:(NSMutableDictionary *)surveyDataDictionary;

#pragma mark - Utility Methods
- (SurveyDataType) dataTypeForKey:(NSString *)key;
@end
