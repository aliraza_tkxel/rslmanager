//
//  PSAppointmentDetailViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Appointment;
@class PSBarButtonItem;

@interface PSDAppointmentDetailViewController : PSCustomViewController

@property (strong,   nonatomic) Appointment *appointment;
@property (strong, nonatomic) NSMutableArray *tenantsArray;
@property (strong, nonatomic) NSMutableArray *jobDataListArray;
@property (strong, nonatomic) NSMutableArray *asbestosArray;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong, nonatomic) PSBarButtonItem *noEntryButton;
@property (strong, nonatomic) PSBarButtonItem *notesButton;
@property (strong, nonatomic) Customer *customer;
@property (strong, nonatomic) UIButton *btnStartAppointment;
- (void) onAppointmentObjectUpdate;
@end
