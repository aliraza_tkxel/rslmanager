//
//  TenantCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSDADTenantCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTenantName;

@property (strong, nonatomic) IBOutlet UILabel *lblTelephone;
@property (strong, nonatomic) IBOutlet UILabel *lblMobile;
@property (strong, nonatomic) IBOutlet UIButton *btnEditTenant;
@property (strong, nonatomic) IBOutlet UIButton *btnTelephone;
@property (strong, nonatomic) IBOutlet UIButton *btnMobile;

@property (strong, nonatomic) IBOutlet UIButton *btnDisclosureIndicator;

@property (weak, nonatomic) Customer *customer;
-(void) setTelephoneTarget:(id)owner Action:(SEL)action;
-(void) setMobileTarget:(id)owner Action:(SEL)action;
-(void) setDisclosureIndicatorTarget:(id)owner Action:(SEL)action;
-(void) setEditTenantTarget:(id)owner Action:(SEL)action;
-(void)configureCell:(id)data :(NSIndexPath*)indexPath;


@end
