//
//  PSJSNumberCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSDADJSDCell : UITableViewCell{
    
}

@property (strong, nonatomic) IBOutlet UILabel *lblJobDetail;
@property (strong, nonatomic) IBOutlet UILabel *lblApplianceDetail;
@property (strong, nonatomic) IBOutlet UIImageView *imgSlotTwo;
@property (strong, nonatomic) IBOutlet UIImageView *imgSlotOne;
@property (strong, nonatomic) IBOutlet UIImageView *imgArrow;
@property (strong, nonatomic) IBOutlet UILabel *lblClickHereJobSheet;
@property (weak,   nonatomic) Appointment *appointment;


-(void)configureCell:(id)data :(NSIndexPath *)indexPath;
@end
