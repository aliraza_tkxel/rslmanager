//
//  TypeCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSDADTypeCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
-(void) configureCell:(id)data :(NSIndexPath*) indexPath;
@end
