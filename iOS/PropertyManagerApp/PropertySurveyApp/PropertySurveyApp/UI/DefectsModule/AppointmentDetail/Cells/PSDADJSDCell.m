//
//  PSJSNumberCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDADJSDCell.h"

@implementation PSDADJSDCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
           }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) awakeFromNib{

    self.selectionStyle = UITableViewCellSelectionStyleNone;

}

-(void)configureCell:(id)data :(NSIndexPath *)indexPath
{
	[self setupLayout];
	[self setAppointment:data];
	[self populateData:indexPath];
}

-(void) setupLayout
{
	self.lblClickHereJobSheet.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT );
	self.lblJobDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblApplianceDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	
	UIView* backgroundView = [[UIView alloc ] initWithFrame:CGRectZero ];
	backgroundView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	
	CGRect jobSheetFrame = self.lblClickHereJobSheet.frame;
	jobSheetFrame.origin.x= 105;
	jobSheetFrame.origin.y=9;
	self.lblClickHereJobSheet.frame= jobSheetFrame;
	
	CGRect arrowframe = self.imgArrow.frame;
	arrowframe.origin.x= 292;
	arrowframe.origin.y=6;
	self.imgArrow.frame= arrowframe;
	
	CGRect slotOneFrame = self.imgSlotOne.frame;
	slotOneFrame.origin.x= 292;
	slotOneFrame.origin.y=30;
	self.imgSlotOne.frame= slotOneFrame;
	
	CGRect slotTwoFrame = self.imgSlotTwo.frame;
	slotTwoFrame.origin.x= 262;
	slotTwoFrame.origin.y=30;
	self.imgSlotTwo.frame= slotTwoFrame;
	
}

-(NSString *) getApplianceDetailDataOfJobSheet : (DefectJobSheet*) objJobData
{

	NSMutableString *applianceDetail = [NSMutableString stringWithString:objJobData.appliance];
	
	if (!isEmpty(objJobData.make)) {
		[applianceDetail appendString: @" : "];
		[applianceDetail appendString: objJobData.make];
	}
	
	if (!isEmpty(objJobData.model)) {
		[applianceDetail appendString: @" : "];
		[applianceDetail appendString: objJobData.model];
	}
	
	return applianceDetail;
}

- (void) populateData :(NSIndexPath *)indexPath
{
	NSMutableArray *jobDataListArray =  [NSMutableArray arrayWithArray:[self.appointment.appointmentToJobSheet allObjects]];
	DefectJobSheet *objJobData = (DefectJobSheet *)[jobDataListArray objectAtIndex:indexPath.row];
	self.lblJobDetail.text = [NSString stringWithFormat:@"%@ / %@",objJobData.jsNumber,objJobData.inspectionRef];
	NSString *applianceDetail = [self getApplianceDetailDataOfJobSheet:objJobData];
	self.lblApplianceDetail.text = applianceDetail;
	
	if(isEmpty(applianceDetail) == NO)
	{
		self.lblApplianceDetail.text = applianceDetail;
		CGRect frame = self.lblApplianceDetail.frame;
		frame.size.width = 288;
		self.lblApplianceDetail.frame = frame;
		[self.lblApplianceDetail sizeToFit];
		
	}
	
	
	self.imgSlotOne.hidden = YES;
	self.imgSlotTwo.hidden = YES;

	// Check two person visibility
	if ([objJobData.isTwoPersonsJob boolValue]==YES) {
		self.imgSlotOne.image =[UIImage imageNamed:kTwoPersonImageName];
		[self displayIcon:kTwoPersonImageName];
	}
	
	// Check job status
	NSString * jobStatus = objJobData.jobStatus;
	if ([jobStatus isEqualToString:kJobStatusPaused])
	{
		[self displayIcon:@"btn_Pause"];
	}
	else if ([jobStatus isEqualToString:kJobStatusComplete] || [jobStatus isEqualToString:kJobStatusNoEntry])
	{
		[self displayIcon:@"icon_completed"];
	}
	else if ([jobStatus isEqualToString:kJobStatusInProgress])
	{
		[self displayIcon:@"btn_Play"];
	}
	
	// Check appointment status
	if([self.appointment getStatus] == AppointmentStatusInProgress)
	{
		self.imgArrow.hidden = NO;
	}else{
		self.imgArrow.hidden = YES;
	}
	
}

- (void) displayIcon:(NSString *) imageName
{
	UIImage *img = [UIImage imageNamed:imageName];
	if (self.imgSlotOne.hidden == YES)
	{
		self.imgSlotOne.hidden = NO;
		self.imgSlotOne.image = img;
	}else if (self.imgSlotTwo.hidden == YES)
	{
		self.imgSlotTwo.hidden = NO;
		self.imgSlotTwo.image = img;
	}
}

@end
