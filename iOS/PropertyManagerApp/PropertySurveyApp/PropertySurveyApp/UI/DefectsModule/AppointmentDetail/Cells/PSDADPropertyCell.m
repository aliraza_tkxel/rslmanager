//
//  PSFaultAppointmentPropertyCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSDADPropertyCell.h"
#import "PropertyPicture+MWPhoto.h"

@implementation PSDADPropertyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    }
    
    return self;
}

-(void)onPictureDefault:(NSNotification*) notification
{
    PropertyPicture * picture=notification.object;
    
    [self.imgProperty setImageWithURL:[NSURL URLWithString:picture.imagePath] andAddBorder:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void) configureCell:(Appointment*)appointment
{
	self.lblAddress.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
	self.lblStarts.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
	self.lblEnds.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	self.lblRecordedBy.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblRecordedBy.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblRecordedByTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblRecordedByTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblRecordedDate.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblRecordedDate.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblRecordedDateTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblRecordedDateTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblTrade.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblTrade.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblTradeTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	self.lblTradeTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	
	[self setPropertyImage:appointment];
	self.lblAddress.text = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyle1];
	DefectJobSheet *objDefectJobDataList = [self getJobDataList:appointment];
	
	if (isEmpty(objDefectJobDataList)==NO) {
		Employee* objEmployee = [[PSCoreDataManager sharedManager] employeeWithIdentifier:objDefectJobDataList.defectRecordedBy inContext:[PSDatabaseContext sharedContext].managedObjectContext];
		self.lblRecordedBy.text = [NSString stringWithFormat:@"%@", (isEmpty(objEmployee.employeeName)==YES)?@"":objEmployee.employeeName];
		self.lblRecordedDate.text = [UtilityClass stringFromDate:objDefectJobDataList.defectDate dateFormat:kDateTimeStyle8];
		self.lblTrade.text = objDefectJobDataList.trade;
	}
  self.lblStartDate.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle9];
  self.lblEndDate.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle9];
  self.lblStartTime.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle3];
  self.lblEndTime.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle3];
}

-(DefectJobSheet *) getJobDataList:(Appointment*)appointment
{
	DefectJobSheet *objDefectJobDataList=nil;
	
	NSMutableArray *jobDataListArray =  [NSMutableArray arrayWithArray:[appointment.appointmentToJobSheet allObjects]];
	if (isEmpty(jobDataListArray)==NO) {
		objDefectJobDataList = [jobDataListArray objectAtIndex:0];
	}
	
	return  objDefectJobDataList;
}

-(void) setPropertyImage:(Appointment*) appointment
{

	UIImage * image=[appointment.appointmentToProperty.defaultPicture underlyingImage];
	if (image) {
		
		[self.imgProperty setImage: image];
		
	}else
	{
		[self.imgProperty setImageWithURL:[NSURL URLWithString:appointment.appointmentToProperty.defaultPicture.imagePath]
												 andAddBorder:YES];
	}

}

-(void) setAddressGestureRecognizer:(id)owner action:(SEL)action
{
        // if labelView is not set userInteractionEnabled, you must do so
    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:owner action:action];
    [self.lblAddress setUserInteractionEnabled:YES];
    [self.lblAddress addGestureRecognizer:gesture];
}

-(void) setImagePropertyGestureRecognizer:(id)owner action:(SEL)action
{
        // if labelView is not set userInteractionEnabled, you must do so
    UITapGestureRecognizer* imageGesture = [[UITapGestureRecognizer alloc] initWithTarget:owner action:action];
    [self.imgProperty setUserInteractionEnabled:YES];
    [self.imgProperty addGestureRecognizer:imageGesture];
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
