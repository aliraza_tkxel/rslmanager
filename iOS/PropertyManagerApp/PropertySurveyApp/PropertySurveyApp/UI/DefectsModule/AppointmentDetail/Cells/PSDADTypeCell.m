//
//  TypeCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDADTypeCell.h"
#import "DefectJobSheet+CoreDataClass.h"

@implementation PSDADTypeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureCell:(id)data :(NSIndexPath*) indexPath
{
	
	self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	self.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	self.lblTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
	
	Appointment *appointment = (Appointment *) data;
	NSMutableArray *jobDataListArray =  [NSMutableArray arrayWithArray:[appointment.appointmentToJobSheet allObjects]];
	DefectJobSheet *objJobData = (DefectJobSheet *)[jobDataListArray objectAtIndex:indexPath.row];
	DefectCategory *objDefectCategory = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([DefectCategory class])
																																									Field:kCategoryId
																																							WithValue:objJobData.defectCategoryId
																																								context:[PSDatabaseContext sharedContext].managedObjectContext];
	NSString *typeData = [NSString stringWithFormat:@"%@ : %@",appointment.appointmentType,objDefectCategory.categoryDescription];
	self.lblTitle.text = typeData;
	
	self.lblTitle.numberOfLines = 0;
	CGRect frame = self.lblTitle.frame;
	frame.size.height = [self heightForLabelWithText:typeData];
	self.lblTitle.frame = frame;
	[self.lblTitle sizeToFit];
	
}

- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(288, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(288, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}

@end
