//
//  PSAppointmentDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDAppointmentDetailViewController.h"
#import "PSAppointmentNotesViewController.h"
#import "PSDADPropertyCell.h"
#import "PSDADNotesCell.h"
#import "PSDADTypeCell.h"
#import "PSDADAsbestosCell.h"
#import "PSDADTenantCell.h"
#import "PSDADJSDCell.h"
#import "PSTenantDetailViewController.h"
#import "PSDJobDetailViewController.h"
#import "PSMapsViewController.h"
#import "PSImageViewController.h"
#import "PSNoEntryNoCardViewController.h"
#import "PSSectionHeaderView.h"
#import "PSSynchronizationManager.h"

#define kViewSectionHeaderHeight 20

#define kNumberOfSections 6
#define kSectionProperty 0
#define kSectionType 1
#define kSectionTenant 2
#define kSectionJSD 3
#define kSectionAsbestos 4
#define kSectionNotes 5

#define kSectionPropertyRows 1
#define kSectionTypeRows 1

#define kPropertyCellHeight 80
#define kDefectPropertyCellHeight 160

#define kTypeCellHeight 29
#define kTenantCellHeight 98
#define kAsbestosCellHeight 48
#define kJSNumberCellHeight 82
#define kNotesCellHeight 150

#define kDownloadSurveyAlertView 1


@interface PSDAppointmentDetailViewController ()
{
   AppointmentType _appointmentType;
	 AppointmentStatus _appointmentStatus;
}
@end

@implementation PSDAppointmentDetailViewController

@synthesize appointment;
@synthesize tenantsArray;
@synthesize jobDataListArray;
@synthesize asbestosArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isEditing = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerNibsForTableView];
    [self prepopulateValues];
    [self loadNavigationonBarItems];
    [self addTableHeaderView];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotifications];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.notesButton, self.noEntryButton, nil]];
    if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
        || [self.appointment getStatus] == AppointmentStatusNoEntry)
    {
        [self.noEntryButton setEnabled:NO];
        [self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
    }
    else
    {
        [self.noEntryButton setEnabled:YES];
    }
    
    [self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self deRegisterNotifications];
    [super viewWillDisappear:animated];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentStartNotificationReceive) name:kAppointmentStartNotification object:nil];
    
}
- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentStartNotification object:nil];
}

-(void) onAppointmentStartNotificationReceive{
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
                                                                style:PSBarButtonItemStyleDefault
                                                               target:self
                                                               action:@selector(onClickNoEntryButton)];
    self.notesButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                              style:PSBarButtonItemStyleNotes
                                                             target:self
                                                             action:@selector(onClickNotesButton)];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.notesButton, self.noEntryButton, nil]];
    
    if (_appointmentStatus == [UtilityClass completionStatusForAppointmentType:_appointmentType]
        || _appointmentStatus == AppointmentStatusNoEntry)
    {
        [self.noEntryButton setEnabled:NO];
    }
    else
    {
        [self.noEntryButton setEnabled:YES];
    }
    if(appointment.appointmentToProperty)
    {
        NSString *propertyTitle = @"";
        if(!isEmpty(appointment.appointmentToProperty.propertyId)){
            propertyTitle = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort];
        }
        else if(!isEmpty(appointment.appointmentToProperty.blockId)){
            propertyTitle = appointment.appointmentToProperty.blockName;
        }
        else{
            propertyTitle =appointment.appointmentToProperty.schemeName;
        }
        [self setTitle:propertyTitle];
    }
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickNoEntryButton
{
	CLS_LOG(@"onClickNoEntryButton");
	PSNoEntryNoCardViewController * noEntryView = [[PSNoEntryNoCardViewController alloc] initWithNibName:NSStringFromClass([PSNoEntryNoCardViewController class])
																																																bundle:[NSBundle mainBundle]];
	[noEntryView setAppointment:self.appointment];
	[self.navigationController pushViewController:noEntryView animated:YES];
}

- (void) onClickNotesButton
{
    CLS_LOG(@"onClickNotesButton");
    PSAppointmentNotesViewController *appointmentNotesViewConroller = [[PSAppointmentNotesViewController alloc] initWithNibName:NSStringFromClass([PSAppointmentNotesViewController class])
																																																												 bundle:[NSBundle mainBundle]];
    [appointmentNotesViewConroller setAppointment:self.appointment];
    [self.navigationController pushViewController:appointmentNotesViewConroller animated:YES];
}
#pragma mark - IBActions
- (IBAction)onClickStartAppoiontmentButton:(id)sender
{
    [self.appointment startAppointment];
	  [self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
}
- (IBAction)onClickAcceptAppoiontmentButton:(id)sender
{
    [self.appointment acceptAppointment];
    [_btnStartAppointment removeTarget:self action:@selector(onClickAcceptAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    [_btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
    [_btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSInteger numberOfSelections = kNumberOfSections;
    return numberOfSelections;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    /*
     The count check ensures that if array is empty, i.e no record exists
     for particular section, the header height is set to zero so that
     section header is not alloted any space.
     */
    
    CGFloat headerHeight = 0.0;
	
    if(section == kSectionJSD)
    {
			if ([self.jobDataListArray count])
			{
				headerHeight = 1;
				
			}
			else
			{
				headerHeight = 0.0;
			}
    }
    else if (section == kSectionAsbestos)
    {
        headerHeight = kViewSectionHeaderHeight;
    }

    else if (section == kSectionTenant)
    {
			if (isEmpty(self.tenantsArray)==NO) {
          headerHeight = kViewSectionHeaderHeight;
			}
    }
    
    else if (section == kSectionNotes)
    {
        headerHeight = kViewSectionHeaderHeight;
    }
    else
    {
        headerHeight = kViewSectionHeaderHeight;
    }
    return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	
	PSSectionHeaderView *headerView = nil;

	if (section == kSectionProperty)
	{
        NSString *propertyTitle = @"";
        if(!isEmpty(appointment.appointmentToProperty.propertyId)){
            propertyTitle = @"KEY_STRING_PROPERTY";
        }
        else if(!isEmpty(appointment.appointmentToProperty.blockId)){
            propertyTitle = @"KEY_STRING_BLOCK";
        }
        else{
            propertyTitle = @"KEY_STRING_SCHEME";
        }
		headerView = [[PSSectionHeaderView alloc] initWithTitle:LOC(propertyTitle)];
	}else if (section == kSectionType)
	{
		headerView = [[PSSectionHeaderView alloc] initWithTitle:LOC(@"KEY_STRING_TYPE")];
	}
	else if (section == kSectionTenant)
	{
		if (isEmpty(self.tenantsArray)==NO) {
			headerView = [[PSSectionHeaderView alloc] initWithTitle:LOC(@"KEY_STRING_TENANT")];
			if ([self checkCustomerRisk]) {
				[headerView setHeaderTitleImage:kRiskImageName];
			}
		}

	}
	else if (section == kSectionJSD)
	{
		headerView = [[PSSectionHeaderView alloc] initWithTitle:@""];
	}
	else if (section == kSectionAsbestos)
	{
		headerView = [[PSSectionHeaderView alloc] initWithTitle:LOC(@"KEY_STRING_ASBESTOS")];
		if ([self checkAsbestos]) {
			[headerView setHeaderTitleImage:kAsbestosImageName];
		}
	}
	else if (section == kSectionNotes)
	{
		headerView = [[PSSectionHeaderView alloc] initWithTitle:LOC(@"KEY_STRING_NOTES")];
		if(isEmpty(self.appointment.appointmentNotes)== NO)
		{
			[headerView setHeaderTitleImage:kCustomerNotesImageName];
		}
		
	}
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0.0;
    if(indexPath.section == kSectionProperty)
    {
        rowHeight = kDefectPropertyCellHeight;
    }
		else if (indexPath.section == kSectionType)
	 {
		 NSString * type = [self getAppointmentTypeDataOfJobSheet:indexPath];
		 rowHeight = [self heightForTextView:nil containingString:type];
		 if (rowHeight < kTypeCellHeight) {
       rowHeight = kTypeCellHeight;
		 }
	 }
    else if (indexPath.section == kSectionTenant)
    {
			if (isEmpty(self.tenantsArray)==NO) {
       rowHeight = kTenantCellHeight;
			}
			
    }
    
    else if (indexPath.section == kSectionAsbestos)
    {
        rowHeight = kAsbestosCellHeight;
    }
    
    else if (indexPath.section == kSectionJSD)
    {
			NSString * type = [self getApplianceDetailDataOfJobSheet:indexPath];
			CGFloat applianceDetailDefaultLabelHeight = 20;
		 rowHeight = kJSNumberCellHeight - applianceDetailDefaultLabelHeight + [self heightForTextView:nil containingString:type];
		 if (rowHeight < kJSNumberCellHeight) {
			 rowHeight = kJSNumberCellHeight;
		 }
			
    }
    else if (indexPath.section == kSectionNotes)
    {
        if (isEmpty(self.appointment.appointmentNotes)==NO)
        {
            rowHeight = [self heightForTextView:nil
															 containingString:self.appointment.appointmentNotes];
            rowHeight += 25;
            
            if (rowHeight < 50)
            {
                rowHeight = 50;
            }
        }
        
        else
        {
            rowHeight = 50;
        }
    }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger numberOfRows=0;
    
    if (section == kSectionProperty)
    {
        numberOfRows = kSectionPropertyRows;
    }
    
    else if (section == kSectionType)
    {
			numberOfRows = [self.jobDataListArray count];
    }
    
    else if (section == kSectionTenant)
    {
			if (isEmpty(self.tenantsArray)==NO) {
				numberOfRows = [self.tenantsArray count];
			}
			
    }
    
    else if (section == kSectionAsbestos)
    {
        if (isEmpty(self.asbestosArray)==NO)
        {
            numberOfRows = [self.asbestosArray count];
        }
        else
        {
            numberOfRows = 1;
        }
    }
    
    else if (section == kSectionJSD)
    {
			numberOfRows = [self.jobDataListArray count];
    }
    else if (section == kSectionNotes)
    {
			numberOfRows = 1;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *_cell = nil;
	
	if (indexPath.section == kSectionProperty)
	{
		PSDADPropertyCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDADPropertyCell class])];
		[cell configureCell:self.appointment];
		[cell setAddressGestureRecognizer:self action:@selector(onClickAddress:)];
		[cell setImagePropertyGestureRecognizer:self action:@selector(onClickImageProperty:)];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		_cell = cell;
	}else if (indexPath.section == kSectionType)
    {
        PSDADTypeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDADTypeCell class])];
        [cell configureCell:self.appointment : indexPath];
        _cell = cell;
    }
    else if (indexPath.section == kSectionTenant)
    {
        PSDADTenantCell *cell = (PSDADTenantCell *)[self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDADTenantCell class])];
			if (isEmpty(self.tenantsArray)==NO) {
				[cell configureCell:[self.tenantsArray objectAtIndex:indexPath.row]:indexPath];
				[cell setTelephoneTarget:self Action:@selector(onClickTelephoneButton:)];
				[cell setMobileTarget:self Action:@selector(onClickMobileButton:)];
				[cell setDisclosureIndicatorTarget:self Action:@selector(onClickDisclosureButton:)];
				[cell setEditTenantTarget:self Action:@selector(onClickEditTenantButton:)];
				cell.selectionStyle = UITableViewCellSelectionStyleGray;
			}else{
				[cell configureCell:nil:nil];
			}
        _cell = cell;
    }
		else if (indexPath.section == kSectionJSD)
		{
			PSDADJSDCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDADJSDCell class])];
			[cell configureCell:self.appointment : indexPath];
			_cell = cell;
			
		}
    else if (indexPath.section == kSectionAsbestos)
    {
        PSDADAsbestosCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDADAsbestosCell class])];
			  [cell configureCell:self.asbestosArray :indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell = cell;
    }
    else if (indexPath.section == kSectionNotes)
    {
        PSDADNotesCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDADNotesCell class])];
        [cell configureCell:self.appointment];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell = cell;
    }
    
    _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    return _cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   if (indexPath.section == kSectionJSD)
    {
        if([self.appointment getStatus] != AppointmentStatusNotStarted)
        {
            NSArray *objAsbestosArray = [self.appointment.appointmentToProperty.propertyToPropertyAsbestosData allObjects];
            UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DefectAppointment" bundle:nil];
            PSDJobDetailViewController * jobDetailDetailViewConroller = (PSDJobDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSDJobDetailViewController class])];
					  [jobDetailDetailViewConroller setJobData:[self.jobDataListArray objectAtIndex:indexPath.row]];
            [jobDetailDetailViewConroller setJobAsbestosArray:objAsbestosArray];
            [self.navigationController pushViewController:jobDetailDetailViewConroller animated:YES];
        }
        
    }

}

#pragma mark - Edit Tenant Button Selector
- (IBAction)onClickEditTenantButton:(id)sender
{
    CLS_LOG(@"onClickEditAddressButton");
    
    UIButton *btnEditTenant = (UIButton *) sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnEditTenant.tag];
    NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
    if (customer != nil) {
        PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:NSStringFromClass([PSTenantDetailViewController class]) bundle:[NSBundle mainBundle]];
        [tenantDetailViewConroller setAddress:address];
        [tenantDetailViewConroller setTenant:customer];
        [tenantDetailViewConroller setEditing:YES];
        [self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
    }
}

- (IBAction)onClickDisclosureButton:(id)sender
{
    UIButton *btnDisclosure = (UIButton *) sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnDisclosure.tag];
    NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
    if (customer != nil)
    {
        PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:NSStringFromClass([PSTenantDetailViewController class]) bundle:[NSBundle mainBundle]];
        [tenantDetailViewConroller setAddress:address];
        [tenantDetailViewConroller setTenant:customer];
        [self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
    }
}

- (IBAction)onClickTelephoneButton:(id)sender
{
    UIButton *btnTel = (UIButton *)sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnTel.tag];
    [self setupDirectCallToNumber:customer.telephone];
}

- (IBAction)onClickMobileButton:(id)sender
{
    // [self setupDirectCallToNumber:self.customer.mobile];
    UIButton *btnMob = (UIButton *)sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnMob.tag];
    [self setupDirectCallToNumber:customer.mobile];
}

- (void) setupDirectCallToNumber:(NSString *)number
{
    if (isEmpty(number) == NO)
    {
        number = [UtilityClass getValidNumber:number];
        CLS_LOG(@"Direct Call Number Verified: %@", number);
        NSString *url = [@"tel:" stringByAppendingString:number];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

#pragma mark - Core Data Update Events
- (void) onAppointmentObjectUpdate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self isViewLoaded] && self.view.window)
        {
            [self.tableView reloadData];
        }
    });
}

- (void)popOrCloseViewController
{
    [super popOrCloseViewController];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
    float horizontalPadding = 0;
    float verticalPadding = 8;
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                //[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
                                [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
                                nil];
    CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(288, 999999.0f)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:attributes
                                               context:nil];
    CGFloat height = boundingRect.size.height + verticalPadding;
    
#else
    CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
                        constrainedToSize:CGSizeMake(288, 999999.0f)
                            lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
    
    return height;
}

#pragma mark - Methods

-(NSString *) getApplianceDetailDataOfJobSheet : (NSIndexPath*) indexPath
{
	DefectJobSheet *objJobData = (DefectJobSheet *)[jobDataListArray objectAtIndex:indexPath.row];
    if (!isEmpty(objJobData)) {
        NSMutableString *applianceDetail = !isEmpty(objJobData.appliance) ? [NSMutableString stringWithString:objJobData.appliance] : [NSMutableString stringWithString:@""] ;
        
        if (!isEmpty(objJobData.make)) {
            [applianceDetail appendString: @" : "];
            [applianceDetail appendString: objJobData.make];
        }
        
        if (!isEmpty(objJobData.model)) {
            [applianceDetail appendString: @" : "];
            [applianceDetail appendString: objJobData.model];
        }
    }
	
	
	return @"";
}

-(NSString *) getAppointmentTypeDataOfJobSheet : (NSIndexPath*) indexPath
{
	DefectJobSheet *objJobData = (DefectJobSheet *)[jobDataListArray objectAtIndex:indexPath.row];
	DefectCategory *objDefectCategory = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([DefectCategory class])
																																									Field:kCategoryId
																																							WithValue:objJobData.defectCategoryId
																																								context:[PSDatabaseContext sharedContext].managedObjectContext];
	NSString *typeData = [NSString stringWithFormat:@"%@ : %@",appointment.appointmentType,objDefectCategory.categoryDescription];
	return typeData;
}

-(BOOL)checkCustomerRisk
{
	NSSet * customers = self.appointment.appointmentToCustomer;
	BOOL isRisk = NO;
	// Check visibility of customer vulnarity
	for(Customer * customer in customers)
	{
		if(isEmpty(customer.customerToCustomerRiskData)== NO
			 || isEmpty(customer.customerToCustomerVulunarityData) == NO)
		{
			isRisk = YES;
			break;
		}
	}
	return isRisk;
}

-(BOOL)checkAsbestos
{
	NSSet * asbestos = self.appointment.appointmentToProperty.propertyToPropertyAsbestosData;
	BOOL isAsbestos = NO;
	if(asbestos.count>0)
	{
		isAsbestos = YES;
	}
	return isAsbestos;
}

-(void)registerNibsForTableView
{
    NSString * nibName = NSStringFromClass([PSDADNotesCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    nibName = NSStringFromClass([PSDADPropertyCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    nibName = NSStringFromClass([PSDADAsbestosCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    nibName = NSStringFromClass([PSDADJSDCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    nibName = NSStringFromClass([PSDADTenantCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    nibName = NSStringFromClass([PSDADTypeCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void) prepopulateValues{

    _appointmentType = [self.appointment getType];
    _appointmentStatus = [self.appointment getStatus];
    self.tenantsArray = [NSMutableArray arrayWithArray:[appointment.appointmentToCustomer allObjects]];
    self.jobDataListArray =  [NSMutableArray arrayWithArray:[appointment.appointmentToJobSheet allObjects]];
    self.asbestosArray =  [NSMutableArray arrayWithArray:[appointment.appointmentToProperty.propertyToPropertyAsbestosData allObjects]];
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];

}

- (void) disableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = NO;
    }
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = NO;
    }
}

- (void) enableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = YES;
    }
    
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = YES;
    }
}

- (void) addTableHeaderView
{
    //NSString *btnTitle = LOC(@"KEY_STRING_ACCEPT");
	
    CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
	
    UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
    headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    
    UILabel *lblMessage = [[UILabel alloc] init];
    lblMessage.frame = CGRectMake(8, 0, 245, 29);
    lblMessage.center = CGPointMake(lblMessage.center.x, headerView.center.y);
    lblMessage.backgroundColor = [UIColor clearColor];
    [lblMessage setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lblMessage setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lblMessage setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
    lblMessage.text =LOC(@"KEY_STRING_APPOINTMENT_START_MESSAGE");
    
    _btnStartAppointment = [[UIButton alloc]init];
    _btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
    _btnStartAppointment.center = CGPointMake(_btnStartAppointment.center.x, headerView.center.y);
    //[_btnStartAppointment setTitle:btnTitle forState:UIControlStateNormal];
    [_btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
    [_btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:
     UIControlStateNormal];
    if([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame){
        [_btnStartAppointment setTitle:LOC(@"KEY_STRING_ACCEPT") forState:UIControlStateNormal];
        [_btnStartAppointment addTarget:self action:@selector(onClickAcceptAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if([self.appointment.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame){
        [_btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
        [_btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    }

    
    [headerView addSubview:lblMessage];
    [headerView addSubview:_btnStartAppointment];
    
    if ([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame || [self.appointment.appointmentStatus compare:kAppointmentStatusAccepted] == NSOrderedSame)
    {
        headerViewRect = self.tableView.tableHeaderView.frame;
        headerViewRect.size.height = 40;
        headerViewRect.size.width = 320;
        [self.tableView.tableHeaderView setFrame:headerViewRect];
        [self.tableView setTableHeaderView:headerView];
    }
}

- (void) hideTableHeaderView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.5];
    [self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
    self.tableView.tableHeaderView = nil;
    [self.tableView.tableHeaderView setHidden:YES];
    [self.tableView reloadData];
    [UIView commitAnimations];
}

- (IBAction)onClickAddress:(UIGestureRecognizer *)sender
{
    PSMapsViewController *mapViewController = [[PSMapsViewController alloc] initWithNibName:NSStringFromClass([PSMapsViewController class]) bundle:[NSBundle mainBundle]];
		[mapViewController setAddress:[self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull]];
		[mapViewController setNavigationTitle:[self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
    [self.navigationController pushViewController:mapViewController animated:YES];
}

- (IBAction)onClickImageProperty:(UIGestureRecognizer *)sender
{
	PSImageViewController *imageViewController = [[PSImageViewController alloc] initWithNibName:NSStringFromClass([PSImageViewController class]) bundle:[NSBundle mainBundle]];
	imageViewController.propertyObj=self.appointment.appointmentToProperty;
	[self.navigationController pushViewController:imageViewController animated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//
@end
