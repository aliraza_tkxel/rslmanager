//
//  PSJobStatusViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate.h"
#import "PSDCompleteAppointmentViewController.h"
#import "PSDCALabelCell.h"
#import "PSDCAPickerCell.h"
#import "PSDCATextFieldCell.h"
#import "PSDCADeliveryCell.h"
#import "PSBarButtonItem.h"
#import "PSTextView.h"
#import "PSTextViewCell.h"
#import "Appointment+JSON.h"
#import "PSRepairImagesViewController.h"
#import "PSDConfirmAppointmentViewController.h"
#import "PSRepairImagesGalleryViewController.h"
#import "PSDPauseAppointmentViewController.h"

#define kSectionHeaderHeight 13

/*!
 @discussion
 Number of Sections in Complete Data Source and their names.
 */
#define kNumberOfSection 11
#define kSectionAppliance 0
#define kSectionCategory 1
#define kSectionDefectNotes 2
#define kSectionRemedialActionTaken 3
#define kSectionRemedialNotes 4
#define kSectionWarningNotesIssued 5
#define kSectionSerialNumber 6
#define kSectionGcNumber 7
#define kSectionWarningTagFixed 8
#define kSectionApplianceCapped 9
#define kSectionPartsDetail 10

/*!
 @discussion
 Cell height for each custom cell in Complete Data Source.
 */
#define kRowHeight 44
#define kPartsDetailRowHeight 100

#define kLabelWidth 300
#define kPickerWidth 260
#define kTextFieldWidth 260
#define kTextViewWidth 260
#define kPartsTextViewWidth 169

@interface PSDCompleteAppointmentViewController ()
{
	BOOL _isInternetAvailable;
	
	DefectCategory *_defectCategory;
	NSString *_defectNotes;
	NSString *_isRemedialActionTaken;
	NSString * _remedialNotes;
	NSString *_isWarningIssued;
	NSString * _serialNumber;
	NSString * _gcNumber;
	NSString *_isStickerFixed;
	NSString *_isDisconnected;
	Employee *_partsOrderedBy;
	NSDate *_deliveryDueDate;
	NSString *_deliveryTo;
	NSString *_appliance;
	
	NSArray *_yesNoPickerOptions;
	NSArray *_yesNoNaPickerOptions;
	NSMutableArray *_defectCategoryPickerOptions;
	
	NSMutableArray *_defectCategories;
	
	NSInteger _pickerTag;
}

@end

@implementation PSDCompleteAppointmentViewController
@synthesize tableView;
@synthesize headerViewArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setupHeader];
	[self setupLayout];
	[self populatePickerOptions];
	[self loadNavigationonBarItems];
	[self registerNibsForTableView];
	[self loadDefaultValues];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if ( !isEmpty(self.jobData)
			&&  ![self.jobData.jobStatus isEqualToString:kJobStatusComplete]
			&& ![self.jobData.jobStatus isEqualToString:kJobStatusNoEntry])
	{
		if ([self.jobData.jobStatus isEqualToString:kJobStatusPaused])
		{
			[self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
			[self.completeButton setEnabled:NO];
		}
		else if ([self.jobData.jobStatus isEqualToString:kJobStatusInProgress])
		{
			[self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
			[self.completeButton setEnabled:YES];
		}
		[self addCameraButtonInRightBarButtons];
	}
	else
	{
		[self.pauseButton setEnabled:NO];
		[self.resumeButton setEnabled:NO];
		[self.completeButton setEnabled:NO];
	}

	[self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];

}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	[self setBtnConfirm:nil];
	[self setBtnCancel:nil];
	[self setTableView:nil];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	self.pauseButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														style:PSBarButtonItemStylePause
																													 target:self
																													 action:@selector(onClickPauseButton)];
	self.resumeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														 style:PSBarButtonItemStylePlay
																														target:self
																														action:@selector(onClickResumeButton)];
	self.completeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																															 style:PSBarButtonItemStyleCheckmark
																															target:self
																															action:@selector(onClickCompleteButton)];
	self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														 style:PSBarButtonItemStyleCamera
																														target:self
																														action:@selector(onClickCameraButton)];
	
	//[self.completeButton setEnabled:NO];
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	if ([self.jobData.jobStatus isEqualToString:kJobStatusComplete] == NO
			|| [self.jobData.jobStatus isEqualToString:kJobStatusNoEntry] == NO)
	{
		if ([self.jobData.jobStatus isEqualToString:kJobStatusPaused] == YES)
		{
			[self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
			[self.completeButton setEnabled:NO];
		}
		else if ([self.jobData.jobStatus isEqualToString:kJobStatusInProgress] == YES)
		{
			[self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
			[self.completeButton setEnabled:YES];
		}
		[self addCameraButtonInRightBarButtons];
	}
	
	
	NSString *title = [NSString stringWithFormat:@"%@",self.jobData.jsNumber];
	[self setTitle:title];
	
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickPauseButton
{
	
	CLS_LOG(@"onClickPauseButton");
	UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DefectAppointment" bundle:nil];
	PSDPauseAppointmentViewController * pauseViewConroller = (PSDPauseAppointmentViewController *)[storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSDPauseAppointmentViewController class])];
	[pauseViewConroller setJobData:self.jobData];
	[self.navigationController pushViewController:pauseViewConroller animated:YES];

}

- (void) onClickResumeButton
{
	CLS_LOG(@"onClickResumeButton");
	[self setRightBarButtonItems:@[self.pauseButton, self.completeButton]];
	[self.completeButton setEnabled:YES];
	[[PSDataUpdateManager sharedManager] updateJobStatus:self.jobData jobStaus:kJobStatusInProgress andPauseData:nil];
	[self addCameraButtonInRightBarButtons];
}
- (void) onClickCompleteButton
{
	
}

- (void) onClickCameraButton
{
	CLS_LOG(@"onClickCameraButton");
	PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
	if(self.jobData.jobSheetToAppointment.appointmentToProperty)
	{
		[repairImagesViewConroller setPropertyObj:self.jobData.jobSheetToAppointment.appointmentToProperty];
		[repairImagesViewConroller setSchemeObj:nil];
	}
	else
	{
		[repairImagesViewConroller setPropertyObj:nil];
		[repairImagesViewConroller setSchemeObj:self.jobData.jobSheetToAppointment.appointmentToScheme];
	}
	[repairImagesViewConroller setJobDataListObj:self.jobData];
	[self.navigationController pushViewController:repairImagesViewConroller animated:YES];
}

#pragma mark - Table View Methods

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	return [self.headerViewArray objectAtIndex:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return kNumberOfSection;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	NSInteger numberOfRows=1;
	return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	CGFloat rowHeight = 0.0;
	CGFloat bottomMargin = 15;
	
	if(indexPath.section == kSectionAppliance){
		rowHeight = [self heightForLabelWithText:_appliance :kLabelWidth :kFontFamilyHelveticaNeueBold :17];
	}else if(indexPath.section == kSectionCategory){
		rowHeight = [self heightForLabelWithText:_defectCategory.categoryDescription :kPickerWidth :kFontFamilyHelveticaNeueBold :17];
	}else if(indexPath.section == kSectionDefectNotes){
		rowHeight = [self heightForLabelWithText:_defectNotes :kTextViewWidth :kFontFamilyHelveticaNeueBold :17] + bottomMargin;
	}else if(indexPath.section == kSectionRemedialNotes){
		rowHeight = [self heightForLabelWithText:_remedialNotes :kTextViewWidth :kFontFamilyHelveticaNeueBold :17] + bottomMargin;
	}else if(indexPath.section == kSectionPartsDetail){
		rowHeight = kPartsDetailRowHeight + [self heightForLabelWithText:_remedialNotes :kPartsTextViewWidth :kFontFamilyHelveticaNeueBold :13];
	}else {
		rowHeight = kRowHeight;
	}
	
	if (rowHeight < kRowHeight)
	{
		rowHeight = kRowHeight;
	}
	
	return rowHeight;
}


- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	CGFloat rowHeight = kSectionHeaderHeight;
	if (section == kSectionPartsDetail)
	{
		rowHeight = 1;
	}
	return rowHeight;
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;
	
	if (indexPath.section == kSectionAppliance)
	{
		PSDCALabelCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDCALabelCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.section == kSectionCategory || indexPath.section == kSectionRemedialActionTaken
					 || indexPath.section == kSectionWarningNotesIssued || indexPath.section == kSectionWarningTagFixed
					 || indexPath.section == kSectionApplianceCapped )
	{
		PSDCAPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDCAPickerCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.section == kSectionSerialNumber || indexPath.section == kSectionGcNumber )
	{
		PSDCATextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDCATextFieldCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.section == kSectionRemedialNotes || indexPath.section ==  kSectionDefectNotes)
	{
		PSTextViewCell *  _cell = [[PSTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:textViewCellIdentifier];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	else if (indexPath.section == kSectionPartsDetail)
	{
		PSDCADeliveryCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDCADeliveryCell class])];
		[self configureCell:&_cell atIndexPath:indexPath];
		cell = _cell;
	}
	
	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
	
	if ([*cell isKindOfClass:[PSDCATextFieldCell class]])
	{
		PSDCATextFieldCell *_cell = (PSDCATextFieldCell *)*cell;
		[_cell.txtTitle setDelegate:self];
		
		switch (indexPath.section)
		{
			case kSectionGcNumber:
				[_cell configureCell:_gcNumber withTag:kSectionGcNumber];
				[_cell.txtTitle setKeyboardType:UIKeyboardTypeNumberPad];
				break;
				
			case kSectionSerialNumber:
				[_cell configureCell:_serialNumber
										 withTag:kSectionSerialNumber];
	
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	else  if ([*cell isKindOfClass:[PSTextViewCell class]])
	{
		PSTextViewCell *_cell = (PSTextViewCell *)*cell;
		
		PSTextView*  _textView = [[PSTextView alloc] initWithFrame:CGRectMake(5, 5, kTextViewWidth, 20)];
		[_textView setDelegate:self];
		[_textView setTag:indexPath.row];
		[_textView setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
		
		switch (indexPath.section)
		{
			case kSectionRemedialNotes:
				_textView.text = _remedialNotes;
				_textView.tag = kSectionRemedialNotes;
				
				if ([_isRemedialActionTaken boolValue]==YES) {
					[_textView setUserInteractionEnabled:YES];
					[_textView setEditable:YES];
				}else{
					[_textView setUserInteractionEnabled:NO];
					[_textView setEditable:NO];
					_textView.text = _remedialNotes;
				}
				break;
				
			case kSectionDefectNotes:
				_textView.text = _defectNotes;
				_textView.tag = kSectionDefectNotes;
				if ([_defectCategory.categoryDescription isEqualToString:@"Remedial Action Taken" ]==YES) {
					[_textView setUserInteractionEnabled:NO];
					[_textView setEditable:NO];
					_textView.text = _defectNotes;
				}else{
					[_textView setUserInteractionEnabled:YES];
					[_textView setEditable:YES];
				}
			
				break;
			default:
				break;
		}
		_cell.textView = _textView;
		_textView = nil;
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSDCAPickerCell class]])
	{
		PSDCAPickerCell *_cell = (PSDCAPickerCell *)*cell;
		//[_cell setDelegate:self];
		
		switch (indexPath.section) {
			case kSectionCategory:
				[_cell configureCell:_defectCategory.categoryDescription
						withPickerOption:_defectCategoryPickerOptions
								withSelector:@selector(onClickCategory:)
								withDelegate:self
									withTarget:self];
				break;
			case kSectionRemedialActionTaken:
				[_cell configureCell:_isRemedialActionTaken
						withPickerOption:_yesNoPickerOptions
								withSelector:@selector(onClickRemedialAction:)
								withDelegate:self
									withTarget:self];
				break;
			case kSectionWarningNotesIssued:
				[_cell configureCell:_isWarningIssued
						withPickerOption:_yesNoPickerOptions
								withSelector:@selector(onClickWarningIssued:)
								withDelegate:self
									withTarget:self];
				break;
			case kSectionWarningTagFixed:
				[_cell configureCell:_isStickerFixed
						withPickerOption:_yesNoPickerOptions
								withSelector:@selector(onClickWarningSticker:)
								withDelegate:self
									withTarget:self];
				break;
				
			case kSectionApplianceCapped:
				[_cell configureCell:_isDisconnected
						withPickerOption:_yesNoNaPickerOptions
								withSelector:@selector(onClickDisconnected:)
								withDelegate:self
									withTarget:self];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSDCALabelCell class]])
	{
		PSDCALabelCell *_cell = (PSDCALabelCell *)*cell;
		
		switch (indexPath.section) {
			case kSectionAppliance:
				[_cell configureCell:self.jobData];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	else if ([*cell isKindOfClass:[PSDCADeliveryCell class]])
	{
		PSDCADeliveryCell *_cell = (PSDCADeliveryCell *)*cell;
		switch (indexPath.section) {
			case kSectionPartsDetail:
				[_cell configureCell:self.jobData];
				break;
			default:
				break;
		}
		*cell = _cell;
	}
	
}

#pragma mark - Notifications
- (void) registerNotification
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSaveJobResumeDataFailureNotificationReceive) name:kSaveJobResumeDataFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSaveJobResumeDataSuccessNotificationReceive) name:kSaveJobResumeDataSuccessNotification object:nil];
	
	
}

- (void) deRegisterNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveJobResumeDataFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveJobResumeDataSuccessNotification object:nil];
}


- (void) onJobStartNotificationReceive
{
	[self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
}


- (void) onSaveJobResumeDataFailureNotificationReceive
{
	
	[self setRightBarButtonItems:@[self.pauseButton, self.completeButton]];
	[self.completeButton setEnabled:NO];
	[self addCameraButtonInRightBarButtons];
	[self viewWillAppear:NO];
}

- (void) onSaveJobResumeDataSuccessNotificationReceive
{
	
	[self setRightBarButtonItems:@[self.pauseButton, self.completeButton]];
	[self.completeButton setEnabled:YES];
	[self addCameraButtonInRightBarButtons];
	[self viewWillAppear:NO];
	
}


#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[self resignAllResponders];
}

#pragma mark - Picker Click

- (IBAction)onClickCategory:(id)sender
{
	CLS_LOG(@"onClickCategory");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kSectionCategory;
	
}

- (IBAction)onClickRemedialAction:(id)sender
{
	CLS_LOG(@"onClickRemedialAction");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kSectionRemedialActionTaken;
}

- (IBAction)onClickWarningIssued:(id)sender
{
	CLS_LOG(@"onClickWarningIssued");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kSectionWarningNotesIssued;
}

- (IBAction)onClickWarningSticker:(id)sender
{
	CLS_LOG(@"onClickWarningSticker");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kSectionWarningTagFixed;
}

- (IBAction)onClickDisconnected:(id)sender
{
	CLS_LOG(@"onClickDisconnected");
	self.selectedControl = (UIButton *)sender;
	[self resignAllResponders];
	_pickerTag = kSectionApplianceCapped;
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
	switch (textField.tag)
	{
		case kSectionSerialNumber:
			_serialNumber = textField.text;
			break;
		case kSectionGcNumber:
			_gcNumber = textField.text;
			break;
		default:
			break;
	}
	
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	BOOL textFieldShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
	
	switch (textField.tag)
	{
		case kSectionSerialNumber:
			
			if ([stringAfterReplacement length] > 10)
			{
				textFieldShouldReturn = NO;
			}
			break;
		case kSectionGcNumber:
		{
			if (range.location == 9 || (textField.text.length >= 9 && range.length == 0) || string.length + textField.text.length > 9 ) {
				return NO;
			}
			
			// Reject appending non-digit characters
			if (range.length == 0 &&
					![[NSCharacterSet decimalDigitCharacterSet] characterIsMember:[string characterAtIndex:0]]) {
				return NO;
			}
			
			UITextRange* selRange = textField.selectedTextRange;
			UITextPosition *currentPosition = selRange.start;
			NSInteger pos = [textField offsetFromPosition:textField.beginningOfDocument toPosition:currentPosition];
			if (range.length != 0) { //deleting
				if (range.location == 2 || range.location == 6) { //deleting a dash
					if (range.length == 1) {
						range.location--;
						pos-=2;
					}
					else {
						pos++;
					}
				}
				else {
					if (range.length > 1) {
						NSString* selectedRange = [textField.text substringWithRange:range];
						NSString* hyphenless = [selectedRange stringByReplacingOccurrencesOfString:@"-" withString:@""];
						NSInteger diff = selectedRange.length - hyphenless.length;
						pos += diff;
					}
					pos --;
				}
			}
			
			NSMutableString* changedString = [NSMutableString stringWithString:[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:@"-" withString:@""]];
			if (changedString.length > 2) {
				[changedString insertString:@"-" atIndex:2];
				if (pos == 2) {
					pos++;
				}
			}
			if (changedString.length > 6) {
				[changedString insertString:@"-" atIndex:6];
				if (pos == 6) {
					pos++;
				}
			}
			pos += string.length;
			
			textField.text = changedString;
			if (pos > changedString.length) {
				pos = changedString.length;
			}
			currentPosition = [textField positionFromPosition:textField.beginningOfDocument offset:pos];
			
			[textField setSelectedTextRange:[textField textRangeFromPosition:currentPosition toPosition:currentPosition]];
			return NO;
			
		}
			break;
		default:
			break;
	}
	
	return textFieldShouldReturn;
	
}

#pragma mark - Protocol Methods

/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerIndex
{
	switch (_pickerTag) {
		case kSectionCategory:
		{
			_defectCategory = [_defectCategories objectAtIndex:pickerIndex];
		}
			break;

		case kSectionRemedialActionTaken:
		{
			_isRemedialActionTaken = [_yesNoPickerOptions objectAtIndex:pickerIndex];
			
			/*if ([_isRemedialActionTaken boolValue]==NO) {
				_remedialNotes = nil;
			}*/
			
		}
			break;
		case kSectionWarningNotesIssued:
		{
			_isWarningIssued = [_yesNoPickerOptions objectAtIndex:pickerIndex];
		}
			break;
		case kSectionWarningTagFixed:
		{
			_isStickerFixed = [_yesNoPickerOptions objectAtIndex:pickerIndex];
		}
			break;
		case kSectionApplianceCapped:
		{
			_isDisconnected = [_yesNoNaPickerOptions objectAtIndex:pickerIndex];
		}
			break;	
		default:
			break;
	}
	[self.tableView reloadData];
}

#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	NSUInteger lengthAllowed = 200;
	if (textView.tag == kSectionRemedialNotes) {
		lengthAllowed = 200;
	}else if (textView.tag == kSectionDefectNotes){
		lengthAllowed = 200;
	}
	
	BOOL textVeiwShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextView *)self.selectedControl).text stringByAppendingString:text];
	
	if ([stringAfterReplacement length] > lengthAllowed)
	{
		textVeiwShouldReturn = NO;
	}
	else{
		
		switch (textView.tag) {
			case kSectionRemedialNotes:
				_remedialNotes = stringAfterReplacement;
				break;
			case kSectionDefectNotes:
				_defectNotes = stringAfterReplacement;
				break;
			default:
				break;
		}
		
	}
	return textVeiwShouldReturn;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	
	self.isEditing = NO;
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	
	
	switch (textView.tag) {
		case kSectionRemedialNotes:
			_remedialNotes = textView.text;
			break;
		case kSectionDefectNotes:
			_defectNotes = textView.text;
			break;
		default:
			break;
	}
	
	[self scrollToRectOfSelectedControl];
}

- (void)textViewDidChange:(UITextView *)textView
{
	self.isEditing = YES;
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
	[self scrollToRectOfSelectedControl];
}

#pragma mark - Scroll Text Field Methods
- (void)scrollToRectOfSelectedControl {
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.selectedControl.tag] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string withScale:(CGFloat)scale
{
	float horizontalPadding = 0;
	float verticalPadding = 82;
	if(OS_VERSION >= 7.0)
	{
		verticalPadding = 47;
	}
	CGFloat height = 0.0;
	if(OS_VERSION >= 7.0)
	{
		NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
																[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17],NSFontAttributeName,
																nil];
		CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(280, 999999.0f)
																							 options:NSStringDrawingUsesLineFragmentOrigin
																						attributes:attributes
																							 context:nil];
		height = boundingRect.size.height + verticalPadding;
	}
	else
	{
		height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
								constrainedToSize:CGSizeMake(280, 999999.0f)
										lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
	}
	return height * scale;
}

//#pragma mark - Keyboard Methods
//
//- (void)keyboardDidShow:(NSNotification *)notification
//{
//	
//	CGFloat bottomViewHeight = 137;
//	
//	if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone) {
//		NSDictionary *userInfo = [notification userInfo];
//		CGSize size = [[userInfo objectForKey: UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
//		CGRect newTableViewFrame = CGRectMake(self.tableView.frame.origin.x,
//																					self.tableView.frame.origin.y,
//																					self.tableView.frame.size.width,
//																					self.tableView.frame.size.height - size.height - 5 + bottomViewHeight);
//		[self.tableView setFrame:newTableViewFrame];
//		CGSize contentSize = CGSizeMake(self.tableView.contentSize.width,
//																		self.tableView.contentSize.height - size.height + bottomViewHeight);
//		self.tableView.contentSize = contentSize;
//		[self scrollToRectOfSelectedControl];
//	}
//}
//
//- (void)keyboardWillHide:(NSNotification *)notification
//{
//	NSDictionary *userInfo = [notification userInfo];
//		CGFloat bottomViewHeight = 137;
//	CGSize size = [[userInfo objectForKey: UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
//	CGRect newTableViewFrame = CGRectMake(self.tableView.frame.origin.x,
//																				self.tableView.frame.origin.y,
//																				self.tableView.frame.size.width,
//																				self.tableView.frame.size.height + size.height + 5 - bottomViewHeight);
//	[self.tableView setFrame:newTableViewFrame];
//	CGSize contentSize = CGSizeMake(self.tableView.contentSize.width,
//																	self.tableView.contentSize.height + size.height - bottomViewHeight);
//	self.tableView.contentSize = contentSize;
//}

#pragma mark - Methods

-(NSString *) getApplianceDetailDataOfJobSheet : (DefectJobSheet*) objJobData
{
	
	NSMutableString *applianceDetail = [NSMutableString stringWithString:objJobData.appliance];
	
	if (!isEmpty(objJobData.make)) {
		[applianceDetail appendString: @" : "];
		[applianceDetail appendString: objJobData.make];
	}
	
	if (!isEmpty(objJobData.model)) {
		[applianceDetail appendString: @" : "];
		[applianceDetail appendString: objJobData.model];
	}
	
	return applianceDetail;
}


-(void)addCameraButtonInRightBarButtons
{
	NSMutableArray *rightBarButtons = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
	NSInteger jobPicturesCount  = [self.jobData.jobSheetToRepairImages count];
	if (jobPicturesCount > 0)
	{
		BOOL found = FALSE;
		for (PSBarButtonItem* button in rightBarButtons) {
			if([button tag] == PSBarButtonItemStyleCamera) {
				found = TRUE;
				break;
			}
		}
		
		if(!found) {
			[rightBarButtons insertObject:self.cameraButton atIndex:[rightBarButtons count]];
			[self setRightBarButtonItems:rightBarButtons];
		}
	}
	
}

- (CGFloat) heightForLabelWithText:(NSString*)string :(CGFloat) width : (NSString *) fontFamily : (CGFloat) fontSize
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:fontFamily size:fontSize],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(width, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:fontFamily size:fontSize]
											constrainedToSize:CGSizeMake(width, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}

- (void) setupHeader{
	
	self.headerViewArray = [NSMutableArray array];
	[self loadSectionHeaderViews:@[LOC(@"KEY_STRING_DEFECT_APPLIANCE"),LOC(@"KEY_STRING_DEFECT_CATEGORY")
																 ,LOC(@"KEY_STRING_DEFECT_NOTES"),LOC(@"KEY_STRING_DEFECT_REMEDIAL_ACTION_TAKEN")
																 ,LOC(@"KEY_STRING_DEFECT_REMEDIAL_NOTES"),LOC(@"KEY_STRING_DEFECT_WARNING_NOTE_ISSUED")
																 ,LOC(@"KEY_STRING_DEFECT_SERIAL_NUMBER"),LOC(@"KEY_STRING_DEFECT_GC_NUMBER")
																 ,LOC(@"KEY_STRING_DEFECT_WARNING_TAG_FIXED"),LOC(@"KEY_STRING_DEFECT_APPLIANCE_DISCONNECTED")
																 ,LOC(@"")]   headerViews:self.headerViewArray];

}

- (BOOL) isFormValid
{
	BOOL isValid = YES;
	NSString *message= @"";
	NSString* remedialActionTaken = [@"Remedial Action Taken" lowercaseString];
	
	if (isEmpty(_defectCategory.categoryDescription.trimString)) {
		message = LOC(@"KEY_ALERT_SELECT_DEFECT_CATEGORY");
		isValid = NO;
	}else if (isEmpty(_defectNotes.trimString)  && ![[_defectCategory.categoryDescription lowercaseString] isEqualToString:remedialActionTaken]){
		message = LOC(@"KEY_ALERT_ENTER_DEFECT_NOTES");
		isValid = NO;
	}else if (isEmpty(_isRemedialActionTaken.trimString)){
		message = LOC(@"KEY_ALERT_SELECT_REMEDIAL_ACTION");
		isValid = NO;
	}else if (isEmpty(_remedialNotes.trimString) && [_isRemedialActionTaken boolValue] == YES){
		message = LOC(@"KEY_ALERT_ENTER_REMEDIAL_NOTES");
		isValid = NO;
	}else if (isEmpty(_isWarningIssued.trimString)){
		message = LOC(@"KEY_ALERT_SELECT_WARNING_ISSUED");
		isValid = NO;
	}else if (isEmpty(_serialNumber.trimString) ){
		message = LOC(@"KEY_ALERT_ENTER_SERIAL_NUMBER");
		isValid = NO;
	}else if ((isEmpty(_gcNumber.trimString)==NO)
						&& (([_gcNumber.trimString length] > 0)
								&& ([_gcNumber.trimString length] < 9))){
							
		message = LOC(@"KEY_ALERT_ENTER_GC_NUMBER");
	  isValid = NO;
	}else if (isEmpty(_isDisconnected.trimString)){
		message = LOC(@"KEY_ALERT_SELECT_DISCONNECTED");
		isValid = NO;
	}
	
	if (isValid==NO) {
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																									 message:message
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
	}
	return isValid;
}

-(void) loadDefaultValues{
	
	DefectJobSheet *objJobdata = (DefectJobSheet *) self.jobData;
	_defectCategory = [[PSCoreDataManager sharedManager] defectCategoryWithIdentifier:objJobdata.defectCategoryId context:[PSDatabaseContext sharedContext].managedObjectContext];
	_defectNotes = objJobdata.defectNotes;
	_isRemedialActionTaken = [self convertNSNumberToString:objJobdata.isRemedialActionTaken];
	_remedialNotes = objJobdata.remedialActionNotes;
	_isWarningIssued = [self convertNSNumberToString:objJobdata.isAdviceNoteIssued];
	_serialNumber = objJobdata.serialNumber;
	_gcNumber = objJobdata.gasCouncilNumber;
	_isStickerFixed = [self convertNSNumberToString:objJobdata.warningTagFixed];
	_isDisconnected = [self convertNSNumberToString:objJobdata.isDisconnected];
	_partsOrderedBy = [[PSCoreDataManager sharedManager] employeeWithIdentifier:objJobdata.partsOrderedBy inContext:[PSDatabaseContext sharedContext].managedObjectContext];
	_deliveryDueDate = objJobdata.partsDueDate;
	_deliveryTo = objJobdata.partsLocation;
	_appliance = [self getApplianceDetailDataOfJobSheet:objJobdata];
	
}

- (NSString*) convertNSNumberToString:(NSNumber*) value
{
	NSString *boolString = nil;
	if (value == nil)
	{
		boolString = LOC(@"KEY_STRING_NOTAPPLICABLE");
	}
	else if([value boolValue] == YES)
	{
		boolString = LOC(@"KEY_ALERT_YES");
	}
	else if([value boolValue] == NO)
	{
		boolString = LOC(@"KEY_ALERT_NO");
	}
	
	return boolString;
}

-(void) populatePickerOptions{
	
	_yesNoPickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
	_yesNoNaPickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO"), LOC(@"KEY_STRING_NOTAPPLICABLE")];
	[self populateDefectCategories];
}

- (void) populateDefectCategories
{
	NSString* generalCommentCategory = [@"General comment" lowercaseString];
	NSString* otherCategory = [@"Other" lowercaseString];
	
	NSArray* defectCategories = [[PSApplianceManager sharedManager] fetchAllDefectCategories:kCategoryDescription];
	_defectCategories = [NSMutableArray array];
	_defectCategoryPickerOptions = [NSMutableArray array];
	
	for (DefectCategory *defectCategory in defectCategories)
	{
		NSString * category = [defectCategory.categoryDescription lowercaseString];
		if (([category containsString:generalCommentCategory]
				 || [category containsString:otherCategory]) == NO)
		{
			[_defectCategories addObject:defectCategory];
			[_defectCategoryPickerOptions addObject:defectCategory.categoryDescription];
		}
	}
}

-(void)registerNibsForTableView
{
	NSString *nibName = NSStringFromClass([PSDCALabelCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSDCAPickerCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSDCATextFieldCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	nibName = NSStringFromClass([PSDCADeliveryCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
	
}

- (void) setupLayout{
	
	[self.view setBackgroundColor:UIColorFromHex (THEME_BG_COLOUR)];
	[self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	[self.tableView setUserInteractionEnabled:YES];
	[self.tableView setScrollEnabled:YES];
	
}

#pragma mark - IBActions
- (IBAction)onClickBtnConfirm:(id)sender
{
    [self.view endEditing:YES];
	if ([self isFormValid]) {
		
		NSMutableDictionary *_jobCompletionDictionary = [NSMutableDictionary dictionary];
		[_jobCompletionDictionary setObject:isEmpty(_defectCategory.categoryID)?[NSNull null]:_defectCategory.categoryID forKey:kDefectCategoryType];
		[_jobCompletionDictionary setObject:isEmpty(_defectNotes)?[NSNull null]:_defectNotes forKey:kDefectNotes];
		[_jobCompletionDictionary setObject:[NSNumber numberWithBool:[_isRemedialActionTaken boolValue]] forKey:kIsRemedialActionTaken];
		[_jobCompletionDictionary setObject:isEmpty(_remedialNotes)?[NSNull null]:_remedialNotes forKey:kRemedialActionNotes];
		[_jobCompletionDictionary setObject:[NSNumber numberWithBool:[_isWarningIssued boolValue]] forKey:kisAdviceNoteIssued];
		[_jobCompletionDictionary setObject:isEmpty(_serialNumber)?[NSNull null]:_serialNumber forKey:kSerialNo];
		[_jobCompletionDictionary setObject:isEmpty(_gcNumber)?[NSNull null]:_gcNumber forKey:kGasCouncilNumber];
		[_jobCompletionDictionary setObject:isEmpty(_isStickerFixed)?[NSNull null]:[NSNumber numberWithBool:[_isStickerFixed boolValue]] forKey:kWarningTagFixed];
		[_jobCompletionDictionary setObject:isEmpty(([_isDisconnected isEqualToString:LOC(@"KEY_STRING_NOTAPPLICABLE")])?nil:_isDisconnected)?[NSNull null]:[NSNumber numberWithBool:[_isDisconnected boolValue]] forKey:kIsDisconnected];
		
		UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DefectAppointment" bundle:nil];
		PSDConfirmAppointmentViewController * confirmAppointmentViewConroller = (PSDConfirmAppointmentViewController *)[storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSDConfirmAppointmentViewController class])];
		[confirmAppointmentViewConroller setJobData:self.jobData];
		[confirmAppointmentViewConroller setJobCompletionDictionary:_jobCompletionDictionary];
		[self.navigationController pushViewController:confirmAppointmentViewConroller animated:YES];
		
	}

}

- (IBAction)onClickBtnCancel:(id)sender
{
	CLS_LOG(@"onClickBtnCancel");
	[self popOrCloseViewController];
}

-(void)dealloc
{
	
}
@end
