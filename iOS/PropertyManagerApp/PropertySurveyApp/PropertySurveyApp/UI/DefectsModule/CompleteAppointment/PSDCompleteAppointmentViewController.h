//
//  PSJobStatusViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextViewCell.h"

@class PSBarButtonItem;
@class PSDCAPickerCell;

@protocol PSDCompleteAppointmentPickerOptionProtocol <NSObject>
@required
/*!
 @discussion
 Method didPickerOptionSelect Called upon selection of any picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerIndex;

@end

@interface PSDCompleteAppointmentViewController : PSCustomViewController <PSDCompleteAppointmentPickerOptionProtocol, UITextFieldDelegate,UITextViewDelegate>
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) PSBarButtonItem *completeButton;
@property (strong, nonatomic) PSBarButtonItem *pauseButton;
@property (strong, nonatomic) PSBarButtonItem *resumeButton;
@property (strong, nonatomic) PSBarButtonItem *cameraButton;
- (IBAction)onClickBtnConfirm:(id)sender;
- (IBAction)onClickBtnCancel:(id)sender;
@property (strong, nonatomic) IBOutlet PSTextViewCell *textViewCell;
@property (strong, nonatomic) DefectJobSheet *jobData;

@end
