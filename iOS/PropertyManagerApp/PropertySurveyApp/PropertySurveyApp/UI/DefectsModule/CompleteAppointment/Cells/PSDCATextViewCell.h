//
//  PSAddApplianceTextFieldCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"

@interface PSDCATextViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextView *txtTitle;
- (IBAction)onClickEditBtn:(id)sender;
- (void) configureCell:(NSString*)titleText
							 withTag:(NSInteger)tag;
@end
