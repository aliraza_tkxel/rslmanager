//
//  PSInspectAppliancePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDCALabelCell.h"

@implementation PSDCALabelCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
			
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureCell :(id) data
{
	[self.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	DefectJobSheet *objDefectJobDataList = (DefectJobSheet *) data;
	NSString *detail = [self getApplianceDetailDataOfJobSheet:objDefectJobDataList];
	
	if(isEmpty(detail) == NO)
	{
		self.lblTitle.text = detail;
		CGRect frame = self.lblTitle.frame;
		frame.size.width = 288;
		self.lblTitle.frame = frame;
		[self.lblTitle sizeToFit];
		
	}

}

-(NSString *) getApplianceDetailDataOfJobSheet : (DefectJobSheet*) objJobData
{
	
	NSMutableString *applianceDetail = [NSMutableString stringWithString:objJobData.appliance];
	
	if (!isEmpty(objJobData.make)) {
		[applianceDetail appendString: @" : "];
		[applianceDetail appendString: objJobData.make];
	}
	
	if (!isEmpty(objJobData.model)) {
		[applianceDetail appendString: @" : "];
		[applianceDetail appendString: objJobData.model];
	}
	
	return applianceDetail;
}

- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(288, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(288, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}
    
@end
