//
//  PSDefectCompleteAppointmentDeliveryCellTableViewCell.h
//  PropertySurveyApp
//
//  Created by aqib javed on 23/07/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSDCADeliveryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitleOrderedBy;
@property (strong, nonatomic) IBOutlet UILabel *lblOrderedBy;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleDeliveryDue;
@property (strong, nonatomic) IBOutlet UILabel *lblDeliveryDue;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleDeliveryTo;
@property (strong, nonatomic) IBOutlet UILabel *lblDeliveryTo;

- (void) configureCell :(id) data;

@end
