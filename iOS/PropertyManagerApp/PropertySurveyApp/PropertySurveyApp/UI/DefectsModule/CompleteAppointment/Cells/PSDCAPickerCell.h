//
//  PSInspectAppliancePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSDCompleteAppointmentViewController.h"

@interface PSDCAPickerCell : UITableViewCell
@property (weak,    nonatomic) id<PSDCompleteAppointmentPickerOptionProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (assign, nonatomic) NSInteger selectedIndex;
- (IBAction)onClickPickerBtn:(id)sender;

- (void) configureCell:(NSString*)titleText
			withPickerOption:pickerOptions
					withSelector:(SEL) selector
					withDelegate:(id<PSDCompleteAppointmentPickerOptionProtocol>) deleg
						withTarget:(id)target;
@end
