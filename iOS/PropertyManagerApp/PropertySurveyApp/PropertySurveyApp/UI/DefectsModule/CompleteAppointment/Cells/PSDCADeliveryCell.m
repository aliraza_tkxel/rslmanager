//
//  PSDefectCompleteAppointmentDeliveryCellTableViewCell.m
//  PropertySurveyApp
//
//  Created by aqib javed on 23/07/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSDCADeliveryCell.h"

@implementation PSDCADeliveryCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureCell :(id) data
{

	[self setupLayout];
	[self populateData:data];
	
}

- (void) setupLayout
{
	[self.lblTitleOrderedBy setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblOrderedBy setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	
	[self.lblTitleDeliveryDue setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblDeliveryDue setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	
	[self.lblTitleDeliveryTo setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblDeliveryTo setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
}



- (void) populateData:(id) data
{
	
	DefectJobSheet * objDefectJobDataList = (DefectJobSheet *) data;
	Employee* objEmployee = [[PSCoreDataManager sharedManager] employeeWithIdentifier:objDefectJobDataList.partsOrderedBy inContext:[PSDatabaseContext sharedContext].managedObjectContext];
	
	[self.lblOrderedBy setText:objEmployee.employeeName];
	[self.lblDeliveryDue setText:[UtilityClass stringFromDate:objDefectJobDataList.partsDueDate dateFormat:kDateTimeStyle2]];

	CGRect frame = self.lblDeliveryTo.frame;
	frame.size.height = [self heightForLabelWithText:objDefectJobDataList.partsLocation];
	self.lblDeliveryTo.frame = frame;
	self.lblDeliveryTo.text = objDefectJobDataList.partsLocation;

}


- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:13],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(158, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:13]
											constrainedToSize:CGSizeMake(158, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}

@end
