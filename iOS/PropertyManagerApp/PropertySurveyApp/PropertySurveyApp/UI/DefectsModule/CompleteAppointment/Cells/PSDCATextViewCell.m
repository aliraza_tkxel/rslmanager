//
//  PSAddApplianceTextFieldCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDCATextViewCell.h"

@implementation PSDCATextViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (IBAction)onClickEditBtn:(id)sender
{
    [self.txtTitle becomeFirstResponder];
}

#pragma mark - Methods

- (void) configureCell:(NSString*)titleText
							 withTag:(NSInteger)tag
{
	[self.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.txtTitle setKeyboardType:UIKeyboardTypeDefault];
	[self.txtTitle setTag:tag];
	
	if(isEmpty(titleText) == NO)
	{
		self.txtTitle.text = titleText;
		CGRect frame = self.txtTitle.frame;
		frame.size.width = 260;
		self.txtTitle.frame = frame;
		[self.txtTitle sizeToFit];
	}
	
}

- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(260, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(260, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}


@end
