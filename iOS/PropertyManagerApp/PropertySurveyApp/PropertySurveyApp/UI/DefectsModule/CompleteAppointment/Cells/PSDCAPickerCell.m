//
//  PSInspectAppliancePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDCAPickerCell.h"

@implementation PSDCAPickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClickPickerBtn:(id)sender {
    if(isEmpty(self.pickerOptions)==NO)
    {
        [ActionSheetStringPicker showPickerWithTitle:self.lblTitle.text
                                                rows:self.pickerOptions
                                    initialSelection:self.selectedIndex
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self.selectedIndex = [selectedIndex integerValue];
    self.lblTitle.text = [self.pickerOptions objectAtIndex:self.selectedIndex];
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectPickerOption:)])
    {
        [(PSDCompleteAppointmentViewController *)self.delegate didSelectPickerOption:[selectedIndex integerValue]];
    }
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark Methods

- (void) configureCell:(NSString*)titleText
			withPickerOption:pickerOptions
					withSelector:(SEL) selector
					withDelegate:(id<PSDCompleteAppointmentPickerOptionProtocol>) deleg
						withTarget:(id)target

{
	[self setDelegate:deleg];
	[self.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	self.pickerOptions = pickerOptions;
	[self.btnPicker addTarget:target
										 action:selector
					 forControlEvents:UIControlEventTouchUpInside];
	self.lblTitle.text = titleText;
}

- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(260, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(260, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}
    
@end
