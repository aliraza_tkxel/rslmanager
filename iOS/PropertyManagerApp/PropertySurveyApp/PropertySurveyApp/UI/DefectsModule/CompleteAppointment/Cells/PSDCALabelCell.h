//
//  PSInspectAppliancePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTestMeterViewController.h"

@interface PSDCALabelCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
- (void) configureCell :(id) data;
@end
