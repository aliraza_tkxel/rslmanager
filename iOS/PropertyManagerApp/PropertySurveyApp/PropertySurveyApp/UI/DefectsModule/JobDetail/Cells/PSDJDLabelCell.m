//
//  PSFaultNotesCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDJDLabelCell.h"

@implementation PSDJDLabelCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCell:(id)data
{
	NSString *detail = (NSString*) data;
	if(isEmpty(detail) == NO)
	{
		self.lblDetail.text = detail;
		CGRect frame = self.lblDetail.frame;
		frame.size.width = 288;
		self.lblDetail.frame = frame;
		[self.lblDetail sizeToFit];
		
	}
	self.lblDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	[self setSelectionStyle:UITableViewCellSelectionStyleNone];
	[self setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
}

- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(300, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(300, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}

@end
