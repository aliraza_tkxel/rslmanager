//
//  PSJobDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDJobDetailViewController.h"
#import "PSDJDLabelCell.h"
#import "PSDPauseAppointmentViewController.h"
#import "PSDCompleteAppointmentViewController.h"
#import "PSBarButtonItem.h"
#import "PSRepairImagesViewController.h"
#import "PSRepairImagesGalleryViewController.h"

@interface PSDJobDetailViewController ()

@end

#define kNumberOfSections 13
#define kSectionAppliance 0
#define kSectionCategory 1
#define kSectionDefectNotes 2
#define kSectionRemedialActionTaken 3
#define kSectionRemedialNotes 4
#define kSectionWarningNotesIssued 5
#define kSectionSerialNumber 6
#define kSectionGcNumber 7
#define kSectionWarningTagFixed 8
#define kSectionApplianceCapped 9
#define kSectionPartsOrderedBy 10
#define kSectionPartsDeliveryDue 11
#define kSectionPartsDeliveryTo 12

#define kSectionHeaderHeight 20
#define kDefectLabelCellHeight 44

@implementation PSDJobDetailViewController
@synthesize headerViewArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setupBackground];
	[self setupHeader];
	[self registerNibsForTableView];
	[self loadNavigationonBarItems];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if(isEmpty(self.jobData) == NO
		 && [self.jobData.jobStatus isEqualToString:kJobStatusNotStarted]==NO
		 && [self.jobData.jobStatus isEqualToString:kJobStatusAccepted]==NO
		 ) {
		[self hideTableHeaderView];
	}
	
	if (isEmpty(self.jobData) == NO
			&&  ![self.jobData.jobStatus isEqualToString:kJobStatusComplete]
			&& ![self.jobData.jobStatus isEqualToString:kJobStatusNoEntry])
	{
		if ([self.jobData.jobStatus isEqualToString:kJobStatusPaused])
		{
			[self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
		}
		else if ([self.jobData.jobStatus isEqualToString:kJobStatusInProgress])
		{
			[self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
		}
		[self addCameraButtonInRightBarButtons];
	}
	else
	{
		[self.pauseButton setEnabled:NO];
		[self.resumeButton setEnabled:NO];
		[self.completeButton setEnabled:NO];
	}
	
	[self registerNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	[self setTableView:nil];
}

-(void) setupBackground{
	
	self.contentView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	self.tableView.backgroundColor =UIColorFromHex(THEME_BG_COLOUR);
	
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	self.pauseButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														style:PSBarButtonItemStylePause
																													 target:self
																													 action:@selector(onClickPauseButton)];
	self.resumeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														 style:PSBarButtonItemStylePlay
																														target:self
																														action:@selector(onClickResumeButton)];
	self.completeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																															 style:PSBarButtonItemStyleCheckmark
																															target:self
																															action:@selector(onClickCompleteButton)];
	self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														 style:PSBarButtonItemStyleCamera
																														target:self
																														action:@selector(onClickCameraButton)];
	
	//[self.completeButton setEnabled:NO];
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	if (![self.jobData.jobStatus isEqualToString:kJobStatusComplete]
			|| ![self.jobData.jobStatus isEqualToString:kJobStatusNoEntry]
			)
	{
		if ([self.jobData.jobStatus isEqualToString:kJobStatusPaused])
		{
			[self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
			[self.completeButton setEnabled:NO];
		}
		else if ([self.jobData.jobStatus isEqualToString:kJobStatusInProgress])
		{
			[self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
			[self.completeButton setEnabled:YES];
		}
		[self addCameraButtonInRightBarButtons];
	}
	
	NSString *title = [NSString stringWithFormat:@"Job %@",self.jobData.jsNumber];
	[self setTitle:title];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

- (void) onClickPauseButton
{
	
	CLS_LOG(@"onClickPauseButton");
	UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DefectAppointment" bundle:nil];
	PSDPauseAppointmentViewController * pauseViewConroller = (PSDPauseAppointmentViewController *)[storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSDPauseAppointmentViewController class])];
	[pauseViewConroller setJobData:self.jobData];
	[self.navigationController pushViewController:pauseViewConroller animated:YES];
	
}

- (void) onClickResumeButton
{
	CLS_LOG(@"onClickResumeButton");
	[self setRightBarButtonItems:@[self.pauseButton, self.completeButton]];
	[self.completeButton setEnabled:TRUE];
	[[PSDataUpdateManager sharedManager] updateJobStatus:self.jobData jobStaus:kJobStatusInProgress andPauseData:nil];
	[self addCameraButtonInRightBarButtons];
	

}
- (void) onClickCompleteButton
{
	
	CLS_LOG(@"onClickCompleteButton");
	UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"DefectAppointment" bundle:nil];
	PSDCompleteAppointmentViewController * completeAppointmentViewConroller = (PSDCompleteAppointmentViewController *)[storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([PSDCompleteAppointmentViewController class])];
	[completeAppointmentViewConroller setJobData:self.jobData];
	[self.navigationController pushViewController:completeAppointmentViewConroller animated:YES];
	
}

- (void) onClickCameraButton
{
	CLS_LOG(@"onClickCameraButton");
	PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
	if(self.jobData.jobSheetToAppointment.appointmentToProperty)
	{
		[repairImagesViewConroller setPropertyObj:self.jobData.jobSheetToAppointment.appointmentToProperty];
		[repairImagesViewConroller setSchemeObj:nil];
	}
	else
	{
		[repairImagesViewConroller setPropertyObj:nil];
		[repairImagesViewConroller setSchemeObj:self.jobData.jobSheetToAppointment.appointmentToScheme];
	}
	[repairImagesViewConroller setJobDataListObj:self.jobData];
	[self.navigationController pushViewController:repairImagesViewConroller animated:YES];
}

#pragma mark - IBActions
- (IBAction)onClickStartJobButton:(id)sender
{
	
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_PHOTOGRAPH_A_DEFECT")
																								 message:LOC(@"KEY_ALERT_BEFORE_DEFECT_PICTURE")
																								delegate:self
																			 cancelButtonTitle:LOC(@"KEY_ALERT_YES")
																			 otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"), nil];
	alert.tag = AlertViewTagDefectPicture;
	[alert show];
}

#pragma mark - UITableView Delegate Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	UITableViewCell *_cell = nil;
	PSDJDLabelCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDJDLabelCell class])];
	NSString *detail = [self getDataForIndex:indexPath];
	[cell configureCell:detail];
	_cell = cell;
	return _cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	CGFloat rowHeight = 0.0;
	
	rowHeight = [self heightForLabelWithText:[self getDataForIndex:indexPath]];
	if (rowHeight < kDefectLabelCellHeight)
	{
		rowHeight = kDefectLabelCellHeight;
	}
	
	return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
	NSInteger numberOfRows = 1;
	
	return numberOfRows;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return kSectionHeaderHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	return kNumberOfSections;
}


#pragma mark - Notifications
- (void) registerNotification
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobStartNotificationReceive) name:kJobStartNotification object:nil];
}

- (void) deRegisterNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kJobStartNotification object:nil];
}


- (void) onJobStartNotificationReceive
{
	[self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
}

#pragma mark - Methods

-(NSString *) getApplianceDetailDataOfJobSheet : (DefectJobSheet*) objJobData
{
	
	NSMutableString *applianceDetail = [NSMutableString stringWithString:objJobData.appliance];
	
	if (!isEmpty(objJobData.make)) {
		[applianceDetail appendString: @" : "];
		[applianceDetail appendString: objJobData.make];
	}
	
	if (!isEmpty(objJobData.model)) {
		[applianceDetail appendString: @" : "];
		[applianceDetail appendString: objJobData.model];
	}
	
	return applianceDetail;
}

- (NSString*) getYesNoNaString:(NSNumber*) value
{
	NSString *boolString = nil;
	if (value == nil)
	{
		boolString = LOC(@"KEY_STRING_NOTAPPLICABLE");
	}
	else if([value boolValue] == YES)
	{
		boolString = LOC(@"KEY_ALERT_YES");
	}
	else if([value boolValue] == NO)
	{
		boolString = LOC(@"KEY_ALERT_NO");
	}
	
	return boolString;
}

-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSDJDLabelCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}



- (void) setupHeader{
	
	self.headerViewArray = [NSMutableArray array];
	[self loadSectionHeaderViews:@[LOC(@"KEY_STRING_DEFECT_APPLIANCE"),LOC(@"KEY_STRING_DEFECT_CATEGORY")
																 ,LOC(@"KEY_STRING_DEFECT_NOTES"),LOC(@"KEY_STRING_DEFECT_REMEDIAL_ACTION_TAKEN")
																 ,LOC(@"KEY_STRING_DEFECT_REMEDIAL_NOTES"),LOC(@"KEY_STRING_DEFECT_WARNING_NOTE_ISSUED")
																 ,LOC(@"KEY_STRING_DEFECT_SERIAL_NUMBER"),LOC(@"KEY_STRING_DEFECT_GC_NUMBER")
																 ,LOC(@"KEY_STRING_DEFECT_WARNING_TAG_FIXED"),LOC(@"KEY_STRING_DEFECT_APPLIANCE_DISCONNECTED")
																 ,LOC(@"KEY_STRING_DEFECT_PARTS_ORDERED_BY"),LOC(@"KEY_STRING_DEFECT_PARTS_DELIVERY_DUE")
																 ,LOC(@"KEY_STRING_DEFECT_PARTS_DELIVERY_TO")
																 ]   headerViews:self.headerViewArray];
	[self addTableHeaderView];
	
}

- (NSString *) getDataForIndex:(NSIndexPath *)indexPath{
	
	DefectJobSheet * objJobData = (DefectJobSheet *) self.jobData;
	NSString* data = nil;
	
	if(indexPath.section == kSectionAppliance){
		data = [self getApplianceDetailDataOfJobSheet:objJobData];
	}else if (indexPath.section == kSectionCategory){
		DefectCategory * objDefectCategory = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([DefectCategory class])
																																										 Field:kCategoryId
																																								 WithValue:objJobData.defectCategoryId
																																									 context:[PSDatabaseContext sharedContext].managedObjectContext];
		data = objDefectCategory.categoryDescription;
	}else if (indexPath.section == kSectionDefectNotes){
		data = objJobData.defectNotes;
	}else if (indexPath.section == kSectionRemedialActionTaken){
		data = ([objJobData.isRemedialActionTaken boolValue]==YES?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO"));
	}else if (indexPath.section == kSectionRemedialNotes){
		data = objJobData.remedialActionNotes;
	}else if (indexPath.section == kSectionWarningNotesIssued){
		data = ([objJobData.isAdviceNoteIssued boolValue]==YES?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO"));
	}else if (indexPath.section == kSectionSerialNumber){
		data = objJobData.serialNumber;
	}else if (indexPath.section == kSectionGcNumber){
		data = objJobData.gasCouncilNumber;
	}else if (indexPath.section == kSectionWarningTagFixed){
		data = ([objJobData.warningTagFixed boolValue]==YES?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO"));
	}else if (indexPath.section == kSectionApplianceCapped){
		data = [self getYesNoNaString:objJobData.isDisconnected];
	}else if (indexPath.section == kSectionPartsOrderedBy){
		Employee * objEmployee = [[PSCoreDataManager sharedManager] findRecordFrom:NSStringFromClass([Employee class])
																																				 Field:kEmployeeId
																																		 WithValue:objJobData.partsOrderedBy
																																			 context:[PSDatabaseContext sharedContext].managedObjectContext];
		data = objEmployee.employeeName;
	}else if (indexPath.section == kSectionPartsDeliveryDue){
		data = [UtilityClass stringFromDate:objJobData.partsDueDate dateFormat:kDateTimeStyle2];
	}else if (indexPath.section == kSectionPartsDeliveryTo){
		data = objJobData.partsLocation;
	}
	
	if (isEmpty(data)) {
		data = @"-";
	}
	
	return data;
}

- (void) addTableHeaderView
{
    if(![self.jobData.jobSheetToAppointment.appointmentStatus isEqualToString:kAppointmentStatusAccepted]){
        if ([self.jobData.jobStatus compare:kJobStatusNotStarted] == NSOrderedSame
						|| [self.jobData.jobStatus compare:kJobStatusAccepted] == NSOrderedSame)
        {
            CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
            UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
            headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
            
            UILabel *lblStartJob = [[UILabel alloc] init];
            lblStartJob.frame = CGRectMake(8, 0, 245, 29);
            lblStartJob.center = CGPointMake(lblStartJob.center.x, headerView.center.y);
            lblStartJob.backgroundColor = [UIColor clearColor];
            [lblStartJob setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
            [lblStartJob setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
            [lblStartJob setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
            lblStartJob.text =LOC(@"KEY_STRING_JOB_START_MESSAGE");
            
            UIButton *btnStartAppointment = [[UIButton alloc]init];
            btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
            btnStartAppointment.center = CGPointMake(btnStartAppointment.center.x, headerView.center.y);
            [btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
            [btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
            [btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:
             UIControlStateNormal];
            [btnStartAppointment addTarget:self action:@selector(onClickStartJobButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [headerView addSubview:lblStartJob];
            [headerView addSubview:btnStartAppointment];
            
            headerViewRect = self.tableView.tableHeaderView.frame;
            headerViewRect.size.height = 40;
            headerViewRect.size.width = 320;
            [self.tableView.tableHeaderView setFrame:headerViewRect];
            [self.tableView setTableHeaderView:headerView];
        }
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_ALERT")
                                                       message:LOC(@"KEY_READ_ONLY_JOB_SHEET")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil, nil];
        [alert show];
    }
	
}

- (void) hideTableHeaderView
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.5];
	[self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
	self.tableView.tableHeaderView = nil;
	[self.tableView.tableHeaderView setHidden:YES];
	[UIView commitAnimations];
	[self loadNavigationonBarItems];
	
}

-(void)addCameraButtonInRightBarButtons
{
	NSMutableArray *rightBarButtons = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
	NSInteger jobPicturesCount  = [self.jobData.jobSheetToRepairImages count];
	if (jobPicturesCount > 0)
	{
		BOOL found = FALSE;
		for (PSBarButtonItem* button in rightBarButtons) {
			if([button tag] == PSBarButtonItemStyleCamera) {
				found = TRUE;
				break;
			}
		}
		
		if(!found) {
			[rightBarButtons insertObject:self.cameraButton atIndex:[rightBarButtons count]];
			[self setRightBarButtonItems:rightBarButtons];
		}
	}
	
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForLabelWithText:(NSString*)string
{
	float horizontalPadding = 0;
	float verticalPadding = 8;
	
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
	NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
															//[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
															[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
															nil];
	CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(288, 999999.0f)
																						 options:NSStringDrawingUsesLineFragmentOrigin
																					attributes:attributes
																						 context:nil];
	CGFloat height = boundingRect.size.height + verticalPadding;
	
#else
	CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
											constrainedToSize:CGSizeMake(288, 999999.0f)
													lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
	
	return height;
}

#pragma mark UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	
 if(alertView.tag == AlertViewTagDefectPicture) {

	 if(buttonIndex == 0) { //YES
		 
		 PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
		 if(self.jobData.jobSheetToAppointment.appointmentToProperty)
		 {
			 [repairImagesViewConroller setPropertyObj:self.jobData.jobSheetToAppointment.appointmentToProperty];
			 [repairImagesViewConroller setSchemeObj:nil];
		 }
		 else
		 {
			 [repairImagesViewConroller setPropertyObj:nil];
			 [repairImagesViewConroller setSchemeObj:self.jobData.jobSheetToAppointment.appointmentToScheme];
		 }
		 [repairImagesViewConroller setJobDataListObj:self.jobData];
		 [self.navigationController pushViewController:repairImagesViewConroller animated:YES];
	 }
 }
}

@end
