//
//  PSJobStatusViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate.h"
#import "PSDPauseAppointmentViewController.h"
#import "PSDPAReasonCell.h"
#import "PSBarButtonItem.h"
#import "PSTextView.h"
#import "Appointment+JSON.h"

/*!
 @discussion
 Number of Sections in Pause Data Source and their names.
 */
#define kPauseNumberOfSections 2
#define kPauseSectionReason 0
#define kPauseSectionNotes 1

/**/
/*!
 @discussion
 Number of rows in each section of Pause Data Source.
 */

#define kSectionHeaderHeight 18

/*!
 @discussion
 Cell height for each custom cell in Pause Data Source.
 */
#define kReasonCellHeight 35
#define kPauseNotesCellHeight 235

@interface PSDPauseAppointmentViewController ()
{
	NSString *_reason;
	NSArray *_pauseReasons;
	NSMutableArray *_pauseReasonStrings;
	NSString *_pauseNotes;
	JobPauseData *_jobPauseReason;
	BOOL _isInternetAvailable;
}

@end

@implementation PSDPauseAppointmentViewController
@synthesize tableView;
@synthesize headerViewArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self loadDefaults];
	[self setupLayout];
	[self loadSectionHeader];
	[self loadNavigationonBarItems];
	[self loadPauseReason];
	[self registerNibsForTableView];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self.tableView reloadData];
	[self registerNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	[self setLblTitle:nil];
	[self setBtnConfirm:nil];
	[self setBtnCancel:nil];
	[self setTableView:nil];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	
	NSString *title = [NSString stringWithFormat:@"Job %@",self.jobData.jsNumber];
	[self setTitle:title];
	
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
	CLS_LOG(@"onClickPauseButton");
	[self popOrCloseViewController];
}

#pragma mark - Table View Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return  kPauseNumberOfSections;;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
	NSInteger numberOfRows=1;
	return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	CGFloat rowHeight = 0.0;
	CGFloat scale = 1.0;
	if (self.isEditing)
	{
		scale = 1.03;
	}
	
	if(indexPath.section == kPauseSectionReason){
		rowHeight = kReasonCellHeight;
	}
	else if (indexPath.section == kPauseSectionNotes)
	{
		rowHeight = [self heightForTextView:nil containingString:_pauseNotes withScale:scale];
		if (rowHeight < 200)
		{
			rowHeight = 200;
		}
	}
	
	return rowHeight;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return kSectionHeaderHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	return [self.headerViewArray objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *_cell = nil;
	
	if (indexPath.section == kPauseSectionReason){
		PSDPAReasonCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDPAReasonCell class])];
		[cell configureCell:LOC(@"KEY_STRING_REASON")
							withTitle:_reason
			 withPickerOption:_pauseReasonStrings
					 withDelegate:self];
		
		_cell = cell;
	}
	else if (indexPath.section == kPauseSectionNotes){
		PSTextViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:textViewCellIdentifier];
		if (_cell == nil)
		{
			cell = [[PSTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:textViewCellIdentifier];
			self.textViewCell = cell;
		}
		PSTextView * _textView = [[PSTextView alloc] initWithFrame:CGRectZero];
		_textView.delegate = self;
		_textView.tag = indexPath.section;
		[_textView setFrame:_cell.frame];
		if(isEmpty(_pauseNotes)==NO)
		{
			_textView.text = _pauseNotes;
		}
		cell.textView = _textView;
		cell.textView.editable = YES;
    cell.textView.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
		_cell = cell;
	}
	
	[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return _cell;
}

#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	[self resignAllResponders];
}

#pragma mark - Notifications
- (void) registerNotification
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobStatusUpdateNotificationReceive) name:kJobStatusUpdateNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobPauseReasonsSaveSuccessNotificationReceive) name:kFetchJobPauseReasonsSaveSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobPauseReasonsSaveFailureNotificationReceive) name:kFetchJobPauseReasonsSaveFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobPauseDataSaveSuccessNotificationReceive) name:kSaveJobPauseDataSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobPauseDataSaveFailureNotificationReceive) name:kSaveJobPauseDataFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kJobStatusUpdateNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchJobPauseReasonsSaveSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchJobPauseReasonsSaveFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveJobPauseDataSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveJobPauseDataFailureNotification object:nil];
}

- (void) onJobStatusUpdateNotificationReceive
{
	
}

- (void) onJobPauseReasonsSaveSuccessNotificationReceive
{
	_pauseReasons = [[PSFaultManager sharedManager]fetchAllJobReasons];
	[self populatePauseList];
	[self hideActivityIndicator];
	[self.tableView reloadData];
}

- (void) onJobPauseDataSaveSuccessNotificationReceive
{
	[self hideActivityIndicator];
	[self popOrCloseViewController];
}

- (void) onJobPauseDataSaveFailureNotificationReceive
{
	[self hideActivityIndicator];
}

- (void) onJobPauseReasonsSaveFailureNotificationReceive
{
	[self hideActivityIndicator];
	
}

#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	BOOL textVeiwShouldReturn = YES;
	NSString *stringAfterReplacement = [((UITextView *)self.selectedControl).text stringByAppendingString:text];
	
	if ([stringAfterReplacement length] > 1000)
	{
		textVeiwShouldReturn = NO;
	}
	
	
	return textVeiwShouldReturn;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	
	self.isEditing = NO;
	_pauseNotes = textView.text;
	[self.tableView beginUpdates];
	[self.tableView endUpdates];

}

- (void)textViewDidChange:(UITextView *)textView
{
	self.isEditing = YES;
	_pauseNotes = textView.text;
	[self.tableView beginUpdates];
	[self.tableView endUpdates];
}

#pragma mark - Scroll Text Field Methods
- (void)scrollToRectOfSelectedControl {
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.selectedControl.tag]
												atScrollPosition:UITableViewScrollPositionTop
																animated:YES];
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string withScale:(CGFloat)scale
{
	float horizontalPadding = 0;
	float verticalPadding = 82;
	if(OS_VERSION >= 7.0)
	{
		verticalPadding = 47;
	}
	CGFloat height = 0.0;
	if(OS_VERSION >= 7.0)
	{
		NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
																[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17],NSFontAttributeName,
																nil];
		CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(280, 999999.0f)
																							 options:NSStringDrawingUsesLineFragmentOrigin
																						attributes:attributes
																							 context:nil];
		height = boundingRect.size.height + verticalPadding;
	}
	else
	{
		height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
								constrainedToSize:CGSizeMake(280, 999999.0f)
										lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
	}
	return height * scale;
}

#pragma mark - Methods

-(void)registerNibsForTableView
{
	NSString * nibName = NSStringFromClass([PSDPAReasonCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

-(void) loadDefaults {
	
	_isInternetAvailable = IS_NETWORK_AVAILABLE;
	self.isEditing = NO;
	self.headerViewArray = [NSMutableArray array];
	_pauseReasonStrings = [NSMutableArray array];
	
}

- (void) loadPauseReason{
	
	NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
	[requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:@"username"];
	[requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:@"salt"];
	_pauseReasons = [[PSFaultManager sharedManager]fetchAllJobReasons];
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		
		if ([_pauseReasons count] <= 0)
		{
			if(_isInternetAvailable)
			{
				[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																		 LOC(@"KEY_ALERT_LOADING"),
																		 @"Reasons"]];
			}
			
		}
		[[PSFaultManager sharedManager]fetchJobPauseReasonList:requestParameters];
	});
	
	if ([_pauseReasons count] <= 0 && !_isInternetAvailable)
	{
		[self hideActivityIndicator];
		// _isInternetAvailable = NO;
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																									 message:LOC(@"KEY_ALERT_PAUSE_REASONS_NO_INTERNET")
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		
		[alert show];
	}
	
	[self populatePauseList];
	
}

- (void) setupLayout{
	
	[self.view setBackgroundColor:UIColorFromHex (THEME_BG_COLOUR)];
	[self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	self.lblTitle.textColor = UIColorFromHex(NAVIGATION_BAR_TITLE_COLOR);
	[self.tableView setUserInteractionEnabled:YES];
	
}

- (void) loadSectionHeader
{
	NSArray *sectionTitles = @[LOC(@"KEY_STRING_REASON"),LOC(@"KEY_STRING_NOTES")];
	[self loadSectionHeaderViews:sectionTitles headerViews:self.headerViewArray];
}

-(void) populatePauseList
{
 if (!isEmpty(_pauseReasons))
 {
	 NSSortDescriptor *_sortDescriptor;
	 NSArray *_sortDescriptors;
	 if (!_sortDescriptor)
	 {
		 _sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kPauseReasonTitle ascending:YES];
		 _sortDescriptors = @[_sortDescriptor];
	 }
	 _pauseReasons = [NSMutableArray arrayWithArray:[_pauseReasons sortedArrayUsingDescriptors:_sortDescriptors]];
	 
		for (JobPauseReason *pauseReason in _pauseReasons)
		{
			if (pauseReason != nil) {
				if (![_pauseReasonStrings containsObject:pauseReason.pauseReason])
				{
					[_pauseReasonStrings addObject:pauseReason.pauseReason];
				}
			}
		}
	}
	
}

#pragma mark - IBActions
- (IBAction)onClickBtnConfirm:(id)sender
{
	NSString *jobStatus = nil;
	
	if (!isEmpty(_reason.trimString) && !isEmpty(_pauseNotes.trimString))
	{
		[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																 @"Pausing ",
																 @"Job"]];
        NSMutableDictionary *pauseJobDictionary = [NSMutableDictionary dictionary];
        [pauseJobDictionary setObject:isEmpty(_pauseNotes)?[NSNull null]:_pauseNotes forKey:kPauseNote];
        [pauseJobDictionary setObject:isEmpty(_jobPauseReason)?[NSNull null]:_jobPauseReason forKey:kJobPauseReason];
        [pauseJobDictionary setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kPausedBy];
		[[PSDataUpdateManager sharedManager] updateJobStatus:self.jobData
                                                    jobStaus:kJobStatusPaused
                                                andPauseData:pauseJobDictionary];
		
	}
	else
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Pause Job"
																									 message:LOC(@"KEY_ALERT_EMPTY_MANDATORY_FIELDS")
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		[alert show];
	}
}

- (IBAction)onClickBtnCancel:(id)sender
{
	CLS_LOG(@"onClickBtnCancel");
	[self popOrCloseViewController];
}

#pragma mark - Protocol Methods
- (void) didSelectPickerOption:(NSInteger)pickerOption
{
	_reason = [_pauseReasonStrings objectAtIndex:pickerOption];
	_jobPauseReason = [_pauseReasons objectAtIndex:pickerOption];
	[self.tableView reloadData];
	[self.view endEditing:YES];
}

- (void) didChangeSwitchOption:(BOOL) valueChanged
{
	[self.tableView reloadData];
}

-(void)dealloc
{
	
}
@end
