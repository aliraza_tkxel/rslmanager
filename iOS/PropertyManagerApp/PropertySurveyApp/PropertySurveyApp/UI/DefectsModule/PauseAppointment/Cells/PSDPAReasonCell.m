//
//  PSReasonOrRepairCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDPAReasonCell.h"
#import "PSDPauseAppointmentViewController.h"


@interface PSDPAReasonCell()
@end

@implementation PSDPAReasonCell
@synthesize lblHeader;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)onClickBtnPicker:(id)sender {
    if(!isEmpty(self.pickerOptions))
    {
        [ActionSheetStringPicker showPickerWithTitle:self.lblHeader
                                                rows:self.pickerOptions
                                    initialSelection:self.selectedIndex
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self.selectedIndex = [selectedIndex integerValue];
    self.lblTitle.text = [self.pickerOptions objectAtIndex:self.selectedIndex];
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectPickerOption:)])
    {
        [(PSDPauseAppointmentViewController *)self.delegate didSelectPickerOption:self.selectedIndex];
    }
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

- (void) configureCell:(NSString*)headerText
						 withTitle:(NSString*) titleText
			withPickerOption:pickerOptions
					withDelegate:(id<PSDPausePickerOptionProtocol>) deleg
{

	self.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
	
	if (isEmpty(pickerOptions)==NO)
	{
		self.pickerOptions = pickerOptions;
		[self.btnPicker setEnabled:YES];
	}
	else
	{
		[self.btnPicker setEnabled:NO];
	}
	if (isEmpty(titleText)==NO)
	{
		self.lblTitle.text = titleText;
	}
	self.lblHeader = headerText;
	self.delegate = deleg;

}

@end
