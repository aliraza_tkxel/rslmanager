//
//  PSJobStatusViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextViewCell.h"

@class PSBarButtonItem;


@protocol PSDPausePickerOptionProtocol <NSObject>
@required
/*!
 @discussion
 Method didPickerOptionSelect Called upon selection of any picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerOption;
/*!
 @discussion
 Method didPickerOptionSelect Called upon changing switch value.
 */
- (void) didChangeSwitchOption:(BOOL) valueChanged;
@end

@interface PSDPauseAppointmentViewController : PSCustomViewController <PSDPausePickerOptionProtocol, UITextViewDelegate>


@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)onClickBtnConfirm:(id)sender;
- (IBAction)onClickBtnCancel:(id)sender;
@property (strong, nonatomic) IBOutlet PSTextViewCell *textViewCell;
@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (strong, nonatomic) DefectJobSheet *jobData;

@property (strong, nonatomic) PSBarButtonItem *playButton;

@end
