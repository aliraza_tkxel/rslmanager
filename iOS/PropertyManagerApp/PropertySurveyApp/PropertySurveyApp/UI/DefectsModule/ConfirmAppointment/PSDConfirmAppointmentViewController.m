//
//  PSJobStatusViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate.h"
#import "PSDConfirmAppointmentViewController.h"
#import "PSDCOAPickerCell.h"
#import "PSDCOATextFieldCell.h"
#import "PSBarButtonItem.h"
#import "PSTextView.h"
#import "PSTextViewCell.h"
#import "Appointment+JSON.h"
#import "PSRepairImagesViewController.h"
#import "PSRepairImagesGalleryViewController.h"

#define kSectionHeaderHeight 18

/*!
 @discussion
 Number of Sections in Complete Data Source and their names.
 */
#define kNumberOfSection 5
#define kSectionCustomerHeating 0
#define kSectionHeatersLeft 1
#define kSectionNumberOfHeaters 2
#define kSectionHotWater 3
#define kSectionCompletionNotes 4

/*!
 @discussion
 Cell height for each custom cell in Complete Data Source.
 */
#define kRowHeight 44

@interface PSDConfirmAppointmentViewController ()
{
    BOOL _isInternetAvailable;
    NSString *_customerHeating;
    NSString *_heatersLeft;
    NSString *_numberOfHeaters;
    NSString *_hotWater;
    NSArray *_yesNoPickerOptions;
    NSInteger _pickerTag;
    NSString *_completionNotes;
    
}

@end

@implementation PSDConfirmAppointmentViewController
@synthesize tableView;
@synthesize headerViewArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupLayout];
    [self setupHeader];
    [self populatePickerOptions];
    [self loadNavigationonBarItems];
    [self registerNibsForTableView];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self setBtnConfirm:nil];
    [self setBtnCancel:nil];
    [self setTableView:nil];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    NSString *title = [NSString stringWithFormat:@"%@",self.jobData.jsNumber];
    [self setTitle:title];
    
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    BOOL isAppointmentComplete = [[PSAppointmentsManager sharedManager]isFaultAppointmentComplete:self.jobData.jobSheetToAppointment];
    if (isAppointmentComplete)
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOC(@"KEY_ALERT_CONFIRM_POPUP_HEADING") message:LOC(@"KEY_CONFIRM_COMPLETE_ACTION") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_NO") style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            NSLog(@"Cancelled Appointment Completion");
            [self popOrCloseViewController];
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.jobData.jobSheetToAppointment getType]] appointment:self.jobData.jobSheetToAppointment];
            NSLog(@"Confirmed Appointment Completion");
            [self popOrCloseViewController];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated: YES completion: nil];
        
    }
    else{
        [self popOrCloseViewController];
    }
    CLS_LOG(@"onClickBackButton");
    
}

#pragma mark - Table View Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumberOfSection;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows=1;
    return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat rowHeight = 0.0;
    rowHeight = kRowHeight;
    return rowHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kSectionHeaderHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.section == kSectionCustomerHeating || indexPath.section == kSectionHeatersLeft || indexPath.section == kSectionHotWater)
    {
        PSDCOAPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDCOAPickerCell class])];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    else if (indexPath.section == kSectionNumberOfHeaters )
    {
        PSDCOATextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDCOATextFieldCell class])];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    else if(indexPath.section == kSectionCompletionNotes){
        PSDCOATextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDCOATextFieldCell class])];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
        
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    if ([*cell isKindOfClass:[PSDCOATextFieldCell class]])
    {
        PSDCOATextFieldCell *_cell = (PSDCOATextFieldCell *)*cell;
        [_cell.txtTitle setDelegate:self];
        
        switch (indexPath.section)
        {
            case kSectionNumberOfHeaters:
                [_cell configureCell:_numberOfHeaters withTag:kSectionNumberOfHeaters];
                break;
            case kSectionCompletionNotes:
                [_cell configureCell:_completionNotes withTag:kSectionCompletionNotes];
                [_cell.txtTitle setKeyboardType:UIKeyboardTypeDefault];
                break;
            default:
                break;
        }
        *cell = _cell;
    }
    else if ([*cell isKindOfClass:[PSDCOAPickerCell class]])
    {
        PSDCOAPickerCell *_cell = (PSDCOAPickerCell *)*cell;
        
        switch (indexPath.section) {
            case kSectionCustomerHeating:
                [_cell configureCell:_customerHeating
                    withPickerOption:_yesNoPickerOptions
                        withSelector:@selector(onClickCustomerHeating:)
                        withDelegate:self
                          withTarget:self];
                
                break;
            case kSectionHeatersLeft:
                
                [_cell configureCell:_heatersLeft
                    withPickerOption:_yesNoPickerOptions
                        withSelector:@selector(onClickHeatersLeft:)
                        withDelegate:self
                          withTarget:self];
                
                break;
            case kSectionHotWater:
                
                [_cell configureCell:_hotWater
                    withPickerOption:_yesNoPickerOptions
                        withSelector:@selector(onClickHotWater:)
                        withDelegate:self
                          withTarget:self];
                
                break;
            default:
                break;
        }
        *cell = _cell;
    }
    
}


#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag==kSectionNumberOfHeaters){
        _numberOfHeaters = textField.text;
    }
    else if(textField.tag == kSectionCompletionNotes){
        _completionNotes = textField.text;
    }
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField.tag==kSectionNumberOfHeaters){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return (([string isEqualToString:filtered])&&(newLength <= 2));
    }
    return  YES;
    
    
}

#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignAllResponders];
}

#pragma mark - Picker Click

- (IBAction)onClickCustomerHeating:(id)sender
{
    CLS_LOG(@"onClickCustomerHeating");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    _pickerTag = kSectionCustomerHeating;
}

- (IBAction)onClickHeatersLeft:(id)sender
{
    CLS_LOG(@"onClickHeatersLeft");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    _pickerTag = kSectionHeatersLeft;
}

- (IBAction)onClickHotWater:(id)sender
{
    CLS_LOG(@"onClickHotWater");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    _pickerTag = kSectionHotWater;
}


#pragma mark - Scroll Text Field Methods
- (void)scrollToRectOfSelectedControl {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.selectedControl.tag] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - Methods

-(void) populatePickerOptions{
    
    _yesNoPickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
    
}

- (void) setupHeader{
    
    self.headerViewArray = [NSMutableArray array];
    [self loadSectionHeaderViews:@[LOC(@"KEY_STRING_CUSTOMER_HAVE_HEATING"),LOC(@"KEY_STRING_HEATERS_LEFT"),LOC(@"KEY_STRING_NUMBER_OF_HEATERS")
                                   ,LOC(@"KEY_STRING_CUSTOMER_HAVE_HOT_WATER"),LOC(@"KEY_STRING_NOTES")
                                   ]   headerViews:self.headerViewArray];
    
}

-(void)registerNibsForTableView
{
    NSString * nibName = NSStringFromClass([PSDCOAPickerCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
    nibName = NSStringFromClass([PSDCOATextFieldCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void) setupLayout{
    
    [self.view setBackgroundColor:UIColorFromHex (THEME_BG_COLOUR)];
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self.tableView setUserInteractionEnabled:YES];
    [self.tableView setScrollEnabled:YES];
    
}

#pragma mark - IBActions
- (IBAction)onClickBtnConfirm:(id)sender
{
    [self.view endEditing:YES];
    if ([self isFormValid]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOC(@"KEY_ALERT_CONFIRM_POPUP_HEADING") message:LOC(@"KEY_CONFIRM_COMPLETE_ACTION") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_NO") style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            NSLog(@"Cancelled Appointment Completion");
            [self popOrCloseViewController];
        }];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_PHOTOGRAPH_A_DEFECT")
                                                           message:LOC(@"KEY_ALERT_AFTER_DEFECT_PICTURE")
                                                          delegate:self
                                                 cancelButtonTitle:LOC(@"KEY_ALERT_YES")
                                                 otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"), nil];
            alert.tag = AlertViewTagDefectPicture;
            [alert show];
            
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
}

- (IBAction)onClickBtnCancel:(id)sender
{
    CLS_LOG(@"onClickBtnCancel");
    [self popOrCloseViewController];
}

#pragma mark - Protocol Methods

/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerIndex;
{
    switch (_pickerTag) {
        case kSectionCustomerHeating:
        {
            _customerHeating = [_yesNoPickerOptions objectAtIndex:pickerIndex];
        }
            break;
        case kSectionHeatersLeft:
        {
            _heatersLeft = [_yesNoPickerOptions objectAtIndex:pickerIndex];
        }
            break;
        case kSectionHotWater:
        {
            _hotWater = [_yesNoPickerOptions objectAtIndex:pickerIndex];
        }
            break;
        default:
            break;
    }
    [self.tableView reloadData];
}

#pragma mark - Notifications

- (BOOL) isFormValid
{
    BOOL isValid = YES;
    
    if (isEmpty(_customerHeating.trimString)
        || isEmpty(_heatersLeft.trimString)
        || isEmpty(_hotWater.trimString)
        || isEmpty(_numberOfHeaters.trimString)) {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                       message:LOC(@"KEY_ALERT_ANSWER_ALL")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
        isValid = NO;
    }
    
    return isValid;
}

- (void) registerNotification
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobCompleteSuccessNotificationReceive) name:kJobCompleteSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobCompleteFailureNotificationReceive) name:kJobCompleteFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentStatusUpdateSuccessNotificationReceive) name:kAppointmentStatusUpdateSuccessNotification object:nil];
    
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kJobCompleteSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kJobCompleteFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentStatusUpdateSuccessNotification object:nil];
}


- (void) onJobCompleteSuccessNotificationReceive
{
    
    
    
}

- (void) onJobCompleteFailureNotificationReceive
{
    
}

- (void) onAppointmentStatusUpdateSuccessNotificationReceive
{
    UIViewController * viewController = [self.navigationController.viewControllers objectAtIndex:1];
    [self.navigationController popToViewController:viewController animated:YES];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(alertView.tag == AlertViewTagDefectPicture) {
        CLS_LOG(@"Clicked at Index %ld",(long)buttonIndex);
        if(buttonIndex == 0) { //YES
            
            [self.jobData setCompletionDate:[NSDate date]];
            PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
            if(self.jobData.jobSheetToAppointment.appointmentToProperty)
            {
                [repairImagesViewConroller setPropertyObj:self.jobData.jobSheetToAppointment.appointmentToProperty];
                [repairImagesViewConroller setSchemeObj:nil];
            }
            else
            {
                [repairImagesViewConroller setSchemeObj:self.jobData.jobSheetToAppointment.appointmentToScheme];
                [repairImagesViewConroller setPropertyObj:nil];
            }
            [repairImagesViewConroller setJobDataListObj:self.jobData];
            [repairImagesViewConroller setIsJobComplete:YES];
            
            [self.jobCompletionDictionary setObject:[NSNumber numberWithBool:[_customerHeating boolValue]] forKey:kIsCustomerHaveHeating];
            [self.jobCompletionDictionary setObject:[NSNumber numberWithBool:[_heatersLeft boolValue]] forKey:kIsHeatersLeft];
            [self.jobCompletionDictionary setObject:[NSNumber numberWithBool:[_hotWater boolValue]] forKey:kIsCustomerHaveHotWater];
            
            NSNumberFormatter *numberStyle = [[NSNumberFormatter alloc] init];
            numberStyle.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *numberOfHeaters = [numberStyle numberFromString:_numberOfHeaters];
            [self.jobCompletionDictionary setObject:!isEmpty(numberOfHeaters)?numberOfHeaters:[NSNull null] forKey:kNumberOfHeatersLeft];
            
            [self.jobCompletionDictionary setObject:!isEmpty(_completionNotes)?_completionNotes:[NSNull null] forKey:kDefectCompletionNotes];
            
            
            [repairImagesViewConroller setRepairCompletionDictionary:self.jobCompletionDictionary];
            [self.navigationController pushViewController:repairImagesViewConroller animated:YES];
        }
    }
    
}

-(void)dealloc
{
    
}
@end

