//
//  PSJobStatusViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextViewCell.h"

@class PSBarButtonItem;
@class PSDCOAPickerCell;

@protocol PSDConfirmAppointmentPickerOptionProtocol <NSObject>
@required
/*!
 @discussion
 Method didPickerOptionSelect Called upon selection of any picker option.
 */
- (void) didSelectPickerOption:(NSInteger)pickerOption;
@end

@interface PSDConfirmAppointmentViewController : PSCustomViewController <PSDConfirmAppointmentPickerOptionProtocol, UITextFieldDelegate,UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) NSMutableArray *headerViewArray;
- (IBAction)onClickBtnConfirm:(id)sender;
- (IBAction)onClickBtnCancel:(id)sender;
@property (strong, nonatomic) IBOutlet PSTextViewCell *textViewCell;
@property (strong, nonatomic) DefectJobSheet *jobData;
@property (strong, nonatomic) NSMutableDictionary * jobCompletionDictionary;

@end
