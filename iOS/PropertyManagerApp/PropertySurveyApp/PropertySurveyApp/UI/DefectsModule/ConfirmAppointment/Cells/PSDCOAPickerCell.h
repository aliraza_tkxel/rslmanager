//
//  PSInspectAppliancePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSDConfirmAppointmentViewController.h"

@interface PSDCOAPickerCell : UITableViewCell
@property (weak,    nonatomic) id<PSDConfirmAppointmentPickerOptionProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (assign, nonatomic) NSInteger selectedIndex;
- (IBAction)onClickPickerBtn:(id)sender;

- (void) configureCell:(NSString*)titleText
			withPickerOption:pickerOptions
					withSelector:(SEL) selector
					withDelegate:(id<PSDConfirmAppointmentPickerOptionProtocol>) deleg
						withTarget:(id)target;
@end
