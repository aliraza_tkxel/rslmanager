//
//  PSAddApplianceTextFieldCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDCOATextFieldCell.h"

@implementation PSDCOATextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
   
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (IBAction)onClickEditBtn:(id)sender
{
    [self.txtTitle becomeFirstResponder];
}

#pragma mark - Methods

- (void) configureCell:(NSString*)titleText
							 withTag:(NSInteger)tag
{
	[self.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.txtTitle setKeyboardType:UIKeyboardTypeNumberPad];
	[self.txtTitle setText:titleText];
    self.txtTitle.tag = tag;
	[self setTag:tag];
	
}
@end
