//
//  PSInspectAppliancePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDCOAPickerCell.h"

@implementation PSDCOAPickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClickPickerBtn:(id)sender {
    if(!isEmpty(self.pickerOptions))
    {
        [ActionSheetStringPicker showPickerWithTitle:self.lblTitle.text
                                                rows:self.pickerOptions
                                    initialSelection:self.selectedIndex
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self.selectedIndex = [selectedIndex integerValue];
    self.lblTitle.text = [self.pickerOptions objectAtIndex:self.selectedIndex];
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectPickerOption:)])
    {
        [(PSDConfirmAppointmentViewController *)self.delegate didSelectPickerOption:[selectedIndex integerValue]];
    }
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark Methods

- (void) configureCell:(NSString*)titleText
			withPickerOption:pickerOptions
					withSelector:(SEL) selector
					withDelegate:(id<PSDConfirmAppointmentPickerOptionProtocol>) deleg
						withTarget:(id)target

{
	[self setDelegate:deleg];
	[self.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
	[self.lblTitle setText:titleText];
	self.pickerOptions = pickerOptions;
	[self.btnPicker addTarget:target
										 action:selector
					 forControlEvents:UIControlEventTouchUpInside];
	
}
    
@end
