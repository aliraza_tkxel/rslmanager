//
//  PSGeneralHeatingChecksViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSGeneralHeatingChecksViewController.h"

#define kTotalRows  6
#define kRowRadiatorsCondition    0
#define kRowHeatingControls   1
#define kRowHeatingControlAdvice  2
#define kRowHeatingDefects    3
#define kRowAccessIssuesAndNotes   4
#define kRowConfirmation   5


@interface PSGeneralHeatingChecksViewController ()

@end

@implementation PSGeneralHeatingChecksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [self registerCells];
    [self initDefaultValues];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Init methods

-(void) initData{
    [self initDefaultValues];
    _pickerArray = @[@"YES",@"NO"];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 44.0;
    _tblView.allowsSelection = NO;
    [_tblView reloadData];
}

-(void) initDefaultValues{
    if(!isEmpty(_appointment.appointmentToProperty.propertyToGeneralHeatingChecks)){
        _radiatorCondition = !isEmpty(_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.radiatorCondition)?_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.radiatorCondition:@"";
        _heatingControl = !isEmpty(_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.heatingControl)?_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.heatingControl:@"";
        _systemUsage = !isEmpty(_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.systemUsage)?_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.systemUsage:@"";
        _generalHeating = !isEmpty(_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.generalHeating)?_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.generalHeating:@"";
        if([_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.accessIssues isEqualToString:@"YES"]){
            _accessIssues = YES;
        }
        else{
            _accessIssues = NO;
        }
        _accessIssueNotes = !isEmpty(_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.accessIssueNotes)?_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.accessIssueNotes:@"";
        _confirmation = !isEmpty(_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.confirmation)?_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.confirmation:@"";;
        _checkedDate = !isEmpty(_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.checkedDate)?_appointment.appointmentToProperty.propertyToGeneralHeatingChecks.checkedDate:[NSDate date];
    }
    else{
        _radiatorCondition = @"";
        _heatingControl = @"";
        _systemUsage = @"";
        _generalHeating = @"";
        _accessIssues = NO;
        _accessIssueNotes = @"";
        _confirmation = @"";
        _checkedDate = [NSDate date];
    }
}

-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSBoolAndTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSBoolAndTextTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSPickerViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSPickerViewTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSTextfieldTableViewCell"];
     [_tblView registerNib:[UINib nibWithNibName:@"PSAFSTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSTextViewTableViewCell"];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSaveBoiler = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                style:PSBarButtonItemStyleDefault
                                                               target:self
                                                               action:@selector(btnSaveTapped)];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.btnSaveBoiler, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"GENERAL HEATING CHECKS"];
}


#pragma mark - Actions

- (void) btnBackTapped
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update this heating type?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveGeneralChecks];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please fill all the mandatory fields"];
    }
    
    
    
}

-(void) saveGeneralChecks{
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    GeneralHeatingChecks *boilerInspection;
    if(self.appointment.appointmentToProperty.propertyToGeneralHeatingChecks!=nil){
        boilerInspection = self.appointment.appointmentToProperty.propertyToGeneralHeatingChecks;
    }
    else{
        boilerInspection = [NSEntityDescription insertNewObjectForEntityForName:kGeneralHeatingChecksEntity inManagedObjectContext:managedObjectContext];
    }
    NSString *access = (_accessIssues)?@"YES":@"NO";
    boilerInspection.radiatorCondition = isEmpty(_radiatorCondition)?nil:_radiatorCondition;
    boilerInspection.heatingControl = isEmpty(_heatingControl)?nil:_heatingControl;
    boilerInspection.systemUsage = isEmpty(_systemUsage)?nil:_systemUsage;
    boilerInspection.generalHeating = isEmpty(_generalHeating)?nil:_generalHeating;
    boilerInspection.accessIssues = isEmpty(access)?nil:access;
    boilerInspection.accessIssueNotes = isEmpty(_accessIssueNotes)?nil:_accessIssueNotes;
    boilerInspection.confirmation = isEmpty(_confirmation)?nil:_confirmation;
    boilerInspection.checkedDate = isEmpty(_checkedDate)?[NSDate date]:_checkedDate;
    
    boilerInspection.generalHeatingChecksToProperty = self.appointment.appointmentToProperty;
    self.appointment.appointmentToProperty.propertyToGeneralHeatingChecks = boilerInspection;
    
    
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    [self popOrCloseViewController];
}

-(PSAltFuelDynamicCellsConfiguartion*) getCellConfigurationForRow:(NSInteger) row andSection:(NSInteger) section{
    PSAltFuelDynamicCellsConfiguartion * config = [[PSAltFuelDynamicCellsConfiguartion alloc] init];
    config.cellRowId = row;
    config.cellSectionId = section;
    config.inputFieldTitle = [self getHeaderForRow:row];
    config.selectedValue = [self getStringValueForRow:row];
    config.selectedBoolValue = [self getBoolValueForRow:row];
    config.isNonMutableField = NO;
    if(row == kRowHeatingDefects){
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeFreeText;
        config.appliedCellFieldType = PSAFSDynamicCellsBaseTypeFreeText;
    }
    else if(row==kRowAccessIssuesAndNotes){
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeBoolAndText;
        config.appliedCellFieldType = PSAFSDynamicCellsBaseTypeFreeText;
    }
    else{
        config.dropDownSourceArray = _pickerArray;
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDropDown;
        config.isNonMutableField = YES;
        switch (row) {
            case kRowRadiatorsCondition:
                config.dropDownHeaderTitle = @"SELECT RADIATOR CONDITION";
                break;
            case kRowHeatingControls:
                config.dropDownHeaderTitle = @"HEATING CONTROLS";
                break;
            case kRowHeatingControlAdvice:
                config.dropDownHeaderTitle = @"ADVICE GIVEN";
                break;
            case kRowConfirmation:
                config.dropDownHeaderTitle = @"CONFIRM";
                break;
            default:
                break;
        }
    }
    return config;
}



#pragma mark - Utility

-(BOOL) isValidData{
    if(!isEmpty(_radiatorCondition) &&!isEmpty(_confirmation) && !isEmpty(_heatingControl) && !isEmpty(_generalHeating) && !isEmpty(_accessIssueNotes)){
        return YES;
    }
    return NO;
}


-(NSString *) getHeaderForRow:(NSInteger)row{
    NSString *header = @"";
    switch (row) {
        case kRowRadiatorsCondition:
            header = @"RADIATORS CONDITION";
            break;
        case kRowHeatingControls:
            header = @"HEATING CONTROLS";
            break;
        case kRowHeatingControlAdvice:
            header = @"HEATING CONTROLS AND SYSTEM USAGE/ENERGY SAVING ADVICE EXPLAINED/GIVEN TO CUSTOMER";
            break;
        case kRowHeatingDefects:
            header = @"GENERAL HEATING/OTHER DEFECTS";
            break;
        case kRowAccessIssuesAndNotes:
            header = @"ANY ACCESS ISSUES TO PROPERTY/HEATING COMPONENTS/OTHER";
            break;
        case kRowConfirmation:
            header = @"I CONFIRM THAT ALL HEATING ELEMENTS HAVE BEEN SERVICED IN ACCORDANCE WITH THE RELEVANT MANUFACTURERS INSTRUCTIONS";
            break;
        default:
            break;
    }
    return header;
}

-(NSString *) getStringValueForRow:(NSInteger)row{
    NSString *header = @"";
    switch (row) {
        case kRowRadiatorsCondition:
            header = _radiatorCondition;
            break;
        case kRowHeatingControls:
            header = _heatingControl;
            break;
        case kRowHeatingControlAdvice:
            header = _systemUsage;
            break;
        case kRowHeatingDefects:
            header = _generalHeating;
            break;
        case kRowAccessIssuesAndNotes:
            header = _accessIssueNotes;
            break;
        case kRowConfirmation:
            header = _confirmation;
            break;
        default:
            break;
    }
    if(isEmpty(header)==YES){
        header = @"";
    }
    return header;
}

-(void) setStringValueForBaseConfig:(PSAltFuelDynamicCellsConfiguartion *) config{
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    NSString *label = config.selectedValue;
    switch (config.cellRowId) {
        case kRowRadiatorsCondition:
            _radiatorCondition = label;
            break;
        case kRowHeatingControls:
            _heatingControl = label;
            break;
        case kRowHeatingControlAdvice:
            _systemUsage = label;
            break;
        case kRowHeatingDefects:
            _generalHeating = label;
            break;
        case kRowAccessIssuesAndNotes:
            _accessIssueNotes = label;
            break;
        case kRowConfirmation:
            _confirmation = label;
            break;
        default:
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}

-(BOOL) getBoolValueForRow:(NSInteger)row{
    BOOL header = NO;
    switch (row) {
        case kRowAccessIssuesAndNotes:
            header = _accessIssues;
            break;
        default:
            break;
    }
    return header;
}

-(void) setBoolValueForBaseConfig:(PSAltFuelDynamicCellsConfiguartion *) config{
    BOOL header = config.selectedBoolValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    switch (row) {
        case kRowAccessIssuesAndNotes:
            _accessIssues = header;
            break;
        default:
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}



#pragma mark - TableViewDelegates

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return kTotalRows;
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    PSAltFuelDynamicCellsConfiguartion *config = [self getCellConfigurationForRow:indexPath.row andSection:indexPath.section];
    
    if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeBoolAndText)
    {
         PSAFSBoolAndTextTableViewCell*manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSBoolAndTextTableViewCell class])];
        manualCell.baseConfig = config;
        manualCell.txtViewDetails.placeholder = @"Add notes to explain access issue";
        manualCell.delegate = self;
        [manualCell initWithConfig];
        [manualCell setNeedsUpdateConstraints];
        [manualCell updateConstraintsIfNeeded];
        [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        cell = manualCell;
    }
    else if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeFreeText){
        if(indexPath.row == kRowHeatingDefects){
            PSAFSTextViewTableViewCell*manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSTextViewTableViewCell class])];
            manualCell.baseConfig = config;
            manualCell.delegate = self;
            [manualCell initWithConfig];
            [manualCell setNeedsUpdateConstraints];
            [manualCell updateConstraintsIfNeeded];
            [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
            cell = manualCell;
        }
        else{
            PSAFSTextfieldTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSTextfieldTableViewCell class])];
            manualCell.baseConfig = config;
            manualCell.delegate = self;
            [manualCell initializeData];
            [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
            cell = manualCell;
        }
        
    }
    else if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeDropDown){
        PSAFSPickerViewTableViewCell *pickerCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSPickerViewTableViewCell class])];
        pickerCell.btnClearValue.hidden = YES;
        pickerCell.baseConfig = config;
        pickerCell.delegate = self;
        [pickerCell initializeData];
        [pickerCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        cell = pickerCell;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


#pragma mark - Delegates

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion*)config{
    [self setStringValueForBaseConfig:config];
}

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion *)config andIsForBoolValue:(BOOL)isForBoolVal{
    if(isForBoolVal){
        [self setBoolValueForBaseConfig:config];
    }
    else{
        [self setStringValueForBaseConfig:config];
    }
}

-(void) didClickShowDropDownFor:(PSAltFuelDynamicCellsConfiguartion *)config andSender:(id) sender{
    [self.view endEditing:YES];
    _pickerTag = config.cellRowId;
    _pickerArray = [[NSMutableArray alloc] initWithArray:config.dropDownSourceArray];
    NSInteger initialIndex = [_pickerArray indexOfObject:config.selectedValue];
    if(initialIndex>[config.dropDownSourceArray count]){
        initialIndex = 0;
    }
    [ActionSheetStringPicker showPickerWithTitle:[self getHeaderForRow:config.cellRowId]
                                            rows:_pickerArray
                                initialSelection:initialIndex
                                          target:self
                                   successAction:@selector(onOptionSelected:element:)
                                    cancelAction:@selector(onOptionPickerCancelled:)
                                          origin:sender];
}


- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    NSString *label = [_pickerArray objectAtIndex:[selectedIndex integerValue]];
    switch (_pickerTag) {
        case kRowRadiatorsCondition:
            _radiatorCondition = label;
            break;
        case kRowHeatingControls:
            _heatingControl = label;
            break;
        case kRowHeatingControlAdvice:
            _systemUsage = label;
            break;
        case kRowHeatingDefects:
            _generalHeating = label;
            break;
        case kRowAccessIssuesAndNotes:
            _accessIssueNotes = label;
            break;
        case kRowConfirmation:
            _confirmation = label;
            break;
        default:
            break;
    }
    [_tblView reloadData];
    
}

- (void) onOptionPickerCancelled:(id)sender {
    CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
