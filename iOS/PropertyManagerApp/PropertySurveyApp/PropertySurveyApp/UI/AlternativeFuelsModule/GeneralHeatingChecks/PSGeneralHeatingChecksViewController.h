//
//  PSGeneralHeatingChecksViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 30/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSTextfieldTableViewCell.h"
#import "PSAFSPickerViewTableViewCell.h"
#import "PSAFSBoolAndTextTableViewCell.h"
#import "PSAFSTextViewTableViewCell.h"
@interface PSGeneralHeatingChecksViewController : PSCustomViewController<UITableViewDataSource, UITableViewDelegate,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (strong, nonatomic) NSString *radiatorCondition;
@property (strong, nonatomic) NSString *heatingControl;
@property (strong, nonatomic) NSString *systemUsage;
@property (strong, nonatomic) NSString *generalHeating;
@property (nonatomic) BOOL accessIssues;
@property (strong, nonatomic) NSString *accessIssueNotes;
@property (strong, nonatomic) NSString *confirmation;
@property (strong, nonatomic) NSDate *checkedDate;
@property (strong, nonatomic) Appointment *appointment;
@property NSInteger pickerTag;
@property (strong,nonatomic) NSArray *pickerArray;
@property (strong, nonatomic) PSBarButtonItem *btnSaveBoiler;;
@end
