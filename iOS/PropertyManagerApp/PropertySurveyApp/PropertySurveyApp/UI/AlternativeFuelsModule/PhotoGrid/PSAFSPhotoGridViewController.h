//
//  PSAFSPhotoGridViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PhotographCollectionViewCell.h"
#import "MWPhotoBrowser.h"
@interface PSAFSPhotoGridViewController : PSCustomViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIActionSheetDelegate>
@property(assign) int selectedIndex;
@property (strong, nonatomic) PSBarButtonItem *cameraButton;
@property (strong, nonatomic) Appointment *appointment;
@property (strong, nonatomic) AlternativeHeating *altHeating;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,retain) NSMutableArray*  picturesDataSource;
@property AFSPhotoGridRoot rootVC;
@property (assign, nonatomic) CameraViewImageTypeTag imageType;
@property (strong, nonatomic) NSNumber *itemId;
-(void) showCameraOfSourceType:(UIImagePickerControllerSourceType) type;
@end
