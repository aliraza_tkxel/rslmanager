//
//  PSAirSourceSurveyViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAirSourceSurveyViewController.h"
#import "PSCameraViewController.h"

#define kSectionWater    0
#define kSectionElectrical   1
#define kSectionMaintOutdoor  2

#define kSectionWaterRowWaterSupplyOff   0
#define kSectionWaterRowWaterSupplyOn 1
#define kSectionWaterRowCheckWaterValves  2

#define kSectionElectricalRowSupplementaryBonding  0
#define kSectionElectricalRowCheckFuse  1
#define kSectionElectricalRowCheckThermo  2

#define kSectionMaintOutdoorRowCheckOilLeak 0
#define kSectionMaintOutdoorRowCheckPipework    1
#define kSectionMaintOutdoorRowCheckConnection  2


@interface PSAirSourceSurveyViewController ()

@end

@implementation PSAirSourceSurveyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [self registerCells];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [self refreshPhotosCount];
}

#pragma mark - Init methods

-(void) initData{
    [self populateDefaultValues];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 44.0;
    _tblView.allowsSelection = NO;
    [_tblView reloadData];
}

-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSBoolAndTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSBoolAndTextTableViewCell"];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSave = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                style:PSBarButtonItemStyleDefault
                                                               target:self
                                                               action:@selector(btnSaveTapped)];
    
    
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    
    self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                               style:PSBarButtonItemStyleCamera
                                                              target:self
                                                              action:@selector(onClickCameraButton)bagde:badgeText];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.cameraButton,self.btnSave, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Air Source Heat Pump"];
}

-(void) refreshPhotosCount{
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    if(!isEmpty(self.cameraButton)){
        [self.cameraButton reloadBadge:badgeText];
    }
    
}

-(void) populateDefaultValues{
    if(!isEmpty(_boiler.alternativeHeatingToAirsourceInspection)){
        _checkElectricConnection = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkElectricConnection)?[_boiler.alternativeHeatingToAirsourceInspection.checkElectricConnection boolValue]:NO;
        
        _checkFuse = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkFuse)?[_boiler.alternativeHeatingToAirsourceInspection.checkFuse boolValue]:NO;
        
        _checkOilLeak = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkOilLeak)?[_boiler.alternativeHeatingToAirsourceInspection.checkOilLeak boolValue]:NO;
        
        _checkSupplementaryBonding = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkSupplementaryBonding)?[_boiler.alternativeHeatingToAirsourceInspection.checkSupplementaryBonding boolValue]:NO;
        
        _checkValves = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkValves)?[_boiler.alternativeHeatingToAirsourceInspection.checkValves boolValue]:NO;
        
        _checkWaterPipework = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkWaterPipework)?[_boiler.alternativeHeatingToAirsourceInspection.checkWaterPipework boolValue]:NO;
        
        _checkWaterSupplyTurnedOff = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkWaterSupplyTurnedOff)?[_boiler.alternativeHeatingToAirsourceInspection.checkWaterSupplyTurnedOff boolValue]:NO;
        
        _checkWaterSupplyTurnedOn = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkWaterSupplyTurnedOn)?[_boiler.alternativeHeatingToAirsourceInspection.checkWaterSupplyTurnedOn boolValue]:NO;
        
        _checkThermostat = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.checkThermostat)?[_boiler.alternativeHeatingToAirsourceInspection.checkThermostat boolValue]:NO;
        
        _isInspected = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.isInspected)?[_boiler.alternativeHeatingToAirsourceInspection.isInspected boolValue]:NO;
        
        _connectionDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.connectionDetail)?_boiler.alternativeHeatingToAirsourceInspection.connectionDetail:@"";
        _fuseDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.fuseDetail)?_boiler.alternativeHeatingToAirsourceInspection.fuseDetail:@"";
        
        _oilLeakDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.oilLeakDetail)?_boiler.alternativeHeatingToAirsourceInspection.oilLeakDetail:@"";
        _pipeworkDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.pipeworkDetail)?_boiler.alternativeHeatingToAirsourceInspection.pipeworkDetail:@"";
        _supplementaryBondingDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.supplementaryBondingDetail)?_boiler.alternativeHeatingToAirsourceInspection.supplementaryBondingDetail:@"";
        _thermostatDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.thermostatDetail)?_boiler.alternativeHeatingToAirsourceInspection.thermostatDetail:@"";
        _turnedOffDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.turnedOffDetail)?_boiler.alternativeHeatingToAirsourceInspection.turnedOffDetail:@"";
        _turnedOnDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.turnedOnDetail)?_boiler.alternativeHeatingToAirsourceInspection.turnedOnDetail:@"";
        _valveDetail = !isEmpty(_boiler.alternativeHeatingToAirsourceInspection.valveDetail)?_boiler.alternativeHeatingToAirsourceInspection.valveDetail:@"";
    }
    else{
        _checkElectricConnection = NO;
        _checkFuse = NO;
        _checkOilLeak = NO;
        _checkSupplementaryBonding = NO;
        _checkValves = NO;
        _checkWaterPipework = NO;
        _checkWaterSupplyTurnedOff = NO;
        _checkWaterSupplyTurnedOn = NO;
        _checkThermostat =NO;
        _connectionDetail = @"";
        _fuseDetail = @"";
        _isInspected = NO;
        _oilLeakDetail = @"";
        _pipeworkDetail = @"";
        _supplementaryBondingDetail = @"";
        _thermostatDetail = @"";
        _turnedOffDetail = @"";
        _turnedOnDetail = @"";
        _valveDetail = @"";
    }
}

#pragma mark - Actions

- (void) btnBackTapped
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update this heating type?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveHeatingInspection];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please check/fill all the mandatory fields"];
    }
    
    
    
}


- (void)onClickCameraButton {
    [self.view endEditing:YES];
    CLS_LOG(@"Camera Button");
    if([self isValidData]){
        [self performSegueWithIdentifier:SEG_ASF_AIRSOURCE_SURVEY_TO_PHOTOS sender:self];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please check/fill all the mandatory fields"];
    }
    
    
}


#pragma mark - Utility

-(NSUInteger) getPhotographsCount
{
    NSArray * photographsList =[_boiler.alternativeHeatingToAirsourceInspection.airsourceInspectionToPictures allObjects];
    return !isEmpty(photographsList)?[photographsList count]:0;
}


-(BOOL) isValidData{
    
    if(!isEmpty(_connectionDetail) && !isEmpty(_fuseDetail) && !isEmpty(_oilLeakDetail)&& !isEmpty(_pipeworkDetail)&& !isEmpty(_supplementaryBondingDetail) && !isEmpty(_thermostatDetail)&& !isEmpty(_turnedOffDetail)&& !isEmpty(_turnedOnDetail)&& !isEmpty(_valveDetail)){
        return YES;
    }
    return NO;
}

-(NSString *) getHeaderForSection:(NSInteger) section{
    NSString *header = @"";
    switch (section) {
        case kSectionWater:
            header = @"MECHANICAL TASKS/MAINTENANCE TASKS UNIT/WATER & HEATING SYSTEM";
            break;
        case kSectionElectrical:
            header = @"ELECTRICAL TASKS/CHECKS";
            break;
        case kSectionMaintOutdoor:
            header = @"MAINTENANCE TASKS - OUTDOOR UNIT";
            break;
        default:
            break;
    }
    return header;
}

-(NSString *) getCellLabelForSection:(NSInteger) section andRow:(NSInteger) row{
    NSString *label = @"";
    if(section==kSectionWater){
        switch (row) {
            case kSectionWaterRowWaterSupplyOff:
                label = @"Check with the water supply turned off and hot water taps open, check the expansion vessel charge pressure and top up as necessary";
                break;
            case kSectionWaterRowWaterSupplyOn:
                label = @"Check with the water supply turned on, open the temprature relief valve and then expansion valve to check the unrestricted discharge into the tundish";
                break;
            case kSectionWaterRowCheckWaterValves:
                label = @"Check valves for freedom of movement and confirm that the water stops and both valves reseat correctly. Check at a full bore discharge from either valve that there is no back up or discharges over the tundish";
                break;
            default:
                break;
        }
    }
    else if(section==kSectionElectrical){
        switch (row) {
            case kSectionElectricalRowSupplementaryBonding:
                label = @"Visually inspect, checking for presence of supplementary bonding and that it is being maintained";
                break;
            case kSectionElectricalRowCheckFuse:
                label = @"Check correct rating and type of fuse which is fitted on electrical supply";
                break;
            case kSectionElectricalRowCheckThermo:
                label = @"Check for the correct operations and temprature settings of the Thermostats";
                break;
            default:
                break;
        }
        
    }
    else if(section==kSectionMaintOutdoor){
        switch (row) {
            case kSectionMaintOutdoorRowCheckOilLeak:
                label = @"Visually inspect for signs of oil leaks which may indicate refrigerant leak (Check for leaks if necessary)";
                break;
            case kSectionMaintOutdoorRowCheckPipework:
                label = @"Check integrity of water pipework and lagging";
                break;
            case kSectionMaintOutdoorRowCheckConnection:
                label = @"Check all electrical connections including mains isolator";
                break;
            default:
                break;
        }
    }
    return label;
}


- (void)addBottomBorderTo:(UIView*)parentView WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth {
    UIView *border = [UIView new];
    border.backgroundColor = color;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    border.frame = CGRectMake(0, parentView.frame.size.height - borderWidth, parentView.frame.size.width, borderWidth);
    [parentView addSubview:border];
}

#pragma mark - Getters and Setters

-(PSAltFuelDynamicCellsConfiguartion*) getCellConfigurationForRow:(NSInteger) row andSection:(NSInteger) section{
    PSAltFuelDynamicCellsConfiguartion * config = [[PSAltFuelDynamicCellsConfiguartion alloc] init];
    config.cellRowId = row;
    config.cellSectionId = section;
    config.inputFieldTitle = [self getCellLabelForSection:section andRow:row];
    config.selectedBoolValue = [self getBoolSelectedValueForSection:section andRow:row];
    config.selectedValue = [self getSelectedStringValueForSection:section andRow:row];
    config.isNonMutableField = NO;
    config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeBoolAndText;
    config.appliedCellFieldType = PSAFSDynamicCellsBaseTypeFreeText;
    return config;
}


-(NSString *) getSelectedStringValueForSection:(NSInteger) section andRow:(NSInteger) row{
    NSString *label = @"";
    if(section==kSectionWater){
        switch (row) {
            case kSectionWaterRowWaterSupplyOff:
                label = _turnedOffDetail;
                break;
            case kSectionWaterRowWaterSupplyOn:
                label = _turnedOnDetail;
                break;
            case kSectionWaterRowCheckWaterValves:
                label = _valveDetail;
                break;
            default:
                break;
        }
    }
    else if(section==kSectionElectrical){
        switch (row) {
            case kSectionElectricalRowSupplementaryBonding:
                label = _supplementaryBondingDetail;
                break;
            case kSectionElectricalRowCheckFuse:
                label = _fuseDetail;
                break;
            case kSectionElectricalRowCheckThermo:
                label = _thermostatDetail;
                break;
            default:
                break;
        }
        
    }
    else if(section==kSectionMaintOutdoor){
        switch (row) {
            case kSectionMaintOutdoorRowCheckOilLeak:
                label = _oilLeakDetail;
                break;
            case kSectionMaintOutdoorRowCheckPipework:
                label = _pipeworkDetail;
                break;
            case kSectionMaintOutdoorRowCheckConnection:
                label = _connectionDetail;
                break;
            default:
                break;
        }
    }
    return label;
}

-(void) setSelectedStringValueWithConfig:(PSAltFuelDynamicCellsConfiguartion*)config{
    NSString *label = config.selectedValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    if(section==kSectionWater){
        switch (row) {
            case kSectionWaterRowWaterSupplyOff:
                _turnedOffDetail =label;
                break;
            case kSectionWaterRowWaterSupplyOn:
                _turnedOnDetail = label;
                break;
            case kSectionWaterRowCheckWaterValves:
                _valveDetail = label;
                break;
            default:
                break;
        }
    }
    else if(section==kSectionElectrical){
        switch (row) {
            case kSectionElectricalRowSupplementaryBonding:
                _supplementaryBondingDetail = label;
                break;
            case kSectionElectricalRowCheckFuse:
                _fuseDetail = label;
                break;
            case kSectionElectricalRowCheckThermo:
                _thermostatDetail = label;
                break;
            default:
                break;
        }
        
    }
    else if(section==kSectionMaintOutdoor){
        switch (row) {
            case kSectionMaintOutdoorRowCheckOilLeak:
                _oilLeakDetail = label;
                break;
            case kSectionMaintOutdoorRowCheckPipework:
                _pipeworkDetail = label;
                break;
            case kSectionMaintOutdoorRowCheckConnection:
                _connectionDetail = label;
                break;
            default:
                break;
        }
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}

-(BOOL) getBoolSelectedValueForSection:(NSInteger) section andRow:(NSInteger) row{
    BOOL label = NO;
    if(section==kSectionWater){
        switch (row) {
            case kSectionWaterRowWaterSupplyOff:
                label = _checkWaterSupplyTurnedOff;
                break;
            case kSectionWaterRowWaterSupplyOn:
                label = _checkWaterSupplyTurnedOn;
                break;
            case kSectionWaterRowCheckWaterValves:
                label = _checkValves;
                break;
            default:
                break;
        }
    }
    else if(section==kSectionElectrical){
        switch (row) {
            case kSectionElectricalRowSupplementaryBonding:
                label = _checkSupplementaryBonding;
                break;
            case kSectionElectricalRowCheckFuse:
                label = _checkFuse;
                break;
            case kSectionElectricalRowCheckThermo:
                label = _checkThermostat;
                break;
            default:
                break;
        }
        
    }
    else if(section==kSectionMaintOutdoor){
        switch (row) {
            case kSectionMaintOutdoorRowCheckOilLeak:
                label = _checkOilLeak;
                break;
            case kSectionMaintOutdoorRowCheckPipework:
                label = _checkWaterPipework;
                break;
            case kSectionMaintOutdoorRowCheckConnection:
                label = _checkElectricConnection;
                break;
            default:
                break;
        }
    }
    return label;
}

-(void) setBoolSelectedValueWithConfig:(PSAltFuelDynamicCellsConfiguartion*)config{
    BOOL label = config.selectedBoolValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    if(section==kSectionWater){
        switch (row) {
            case kSectionWaterRowWaterSupplyOff:
                _checkWaterSupplyTurnedOff = label;
                break;
            case kSectionWaterRowWaterSupplyOn:
                _checkWaterSupplyTurnedOn = label;
                break;
            case kSectionWaterRowCheckWaterValves:
                _checkValves = label;
                break;
            default:
                break;
        }
    }
    else if(section==kSectionElectrical){
        switch (row) {
            case kSectionElectricalRowSupplementaryBonding:
                _checkSupplementaryBonding = label;
                break;
            case kSectionElectricalRowCheckFuse:
                _checkFuse = label;
                break;
            case kSectionElectricalRowCheckThermo:
                _checkThermostat = label;
                break;
            default:
                break;
        }
        
    }
    else if(section==kSectionMaintOutdoor){
        switch (row) {
            case kSectionMaintOutdoorRowCheckOilLeak:
                _checkOilLeak = label;
                break;
            case kSectionMaintOutdoorRowCheckPipework:
                _checkWaterPipework = label;
                break;
            case kSectionMaintOutdoorRowCheckConnection:
                _checkElectricConnection = label;
                break;
            default:
                break;
        }
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}



#pragma mark - UITableViewDelegates

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSUInteger rows = 0;
    switch (section) {
        case kSectionWater:
            rows = 3;
            break;
        case kSectionElectrical:
            rows = 3;
            break;
        case kSectionMaintOutdoor:
            rows = 3;
        default:
            break;
    }
    return rows;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 40)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [label setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    
    label.text = [self getHeaderForSection:section];
    label.textAlignment = NSTextAlignmentLeft;
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    [self addBottomBorderTo:view WithColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR) andWidth:2.0f];
    return view;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    PSAltFuelDynamicCellsConfiguartion *config = [self getCellConfigurationForRow:indexPath.row andSection:indexPath.section];
    PSAFSBoolAndTextTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSBoolAndTextTableViewCell class])];
    manualCell.baseConfig = config;
    manualCell.delegate = self;
    [manualCell initWithConfig];
    [manualCell setNeedsUpdateConstraints];
    [manualCell updateConstraintsIfNeeded];
    [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    cell = manualCell;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

#pragma mark - Delegates

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion *)config andIsForBoolValue:(BOOL)isForBoolVal{
    if(!isForBoolVal){
        [self setSelectedStringValueWithConfig:config];
    }
    else{
        [self setBoolSelectedValueWithConfig:config];
    }
}

#pragma mark - Save heating Inspection

-(void) saveAllData{
    NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
    AirSourceInspection *boilerInspection;
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
    _inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
    _inspectionId = (isEmpty(_inspectionId) || (_inspectionId == [NSNumber numberWithInt:-1]))?[NSNumber numberWithInt:-1]:_inspectionId;
    if(self.boiler.alternativeHeatingToAirsourceInspection!=nil){
        boilerInspection = self.boiler.alternativeHeatingToAirsourceInspection;
    }
    else{
        boilerInspection = [NSEntityDescription insertNewObjectForEntityForName:kAirsourceInspectionEntity inManagedObjectContext:managedObjectContext];
    }
    
    boilerInspection.isInspected = [NSNumber numberWithBool:YES];
    boilerInspection.inspectedBy = isEmpty(_inspectedBy)?nil:_inspectedBy;
    boilerInspection.inspectionDate = isEmpty(_inspectionDate)?nil:_inspectionDate;
    boilerInspection.inspectionId = isEmpty(_inspectionId)?nil:_inspectionId;
    boilerInspection.checkElectricConnection = [NSNumber numberWithBool:_checkElectricConnection];
    boilerInspection.checkFuse = [NSNumber numberWithBool:_checkFuse];
    boilerInspection.checkOilLeak = [NSNumber numberWithBool:_checkOilLeak];
    boilerInspection.checkSupplementaryBonding = [NSNumber numberWithBool:_checkSupplementaryBonding];
    boilerInspection.checkThermostat = [NSNumber numberWithBool:_checkThermostat];
    boilerInspection.checkValves = [NSNumber numberWithBool:_checkValves];
    boilerInspection.checkWaterPipework = [NSNumber numberWithBool:_checkWaterPipework];
    boilerInspection.checkWaterSupplyTurnedOff = [NSNumber numberWithBool:_checkWaterSupplyTurnedOff];
    boilerInspection.checkWaterSupplyTurnedOn = [NSNumber numberWithBool:_checkWaterSupplyTurnedOn];
    
    boilerInspection.connectionDetail = isEmpty(_connectionDetail)?nil:_connectionDetail;
    boilerInspection.fuseDetail = isEmpty(_fuseDetail)?nil:_fuseDetail;
    boilerInspection.oilLeakDetail = isEmpty(_oilLeakDetail)?nil:_oilLeakDetail;
    boilerInspection.pipeworkDetail = isEmpty(_pipeworkDetail)?nil:_pipeworkDetail;
    boilerInspection.supplementaryBondingDetail = isEmpty(_supplementaryBondingDetail)?nil:_supplementaryBondingDetail;
    boilerInspection.thermostatDetail = isEmpty(_thermostatDetail)?nil:_thermostatDetail;
    boilerInspection.turnedOffDetail = isEmpty(_turnedOffDetail)?nil:_turnedOffDetail;
    boilerInspection.turnedOnDetail = isEmpty(_turnedOnDetail)?nil:_turnedOnDetail;
    boilerInspection.valveDetail = isEmpty(_valveDetail)?nil:_valveDetail;
    self.boiler.isInspected = [NSNumber numberWithBool:YES];
    boilerInspection.airsourceInspectionToHeating = self.boiler;
    self.boiler.alternativeHeatingToAirsourceInspection = boilerInspection;
    
    self.appointment.isModified = [NSNumber numberWithBool:YES];
    self.appointment.isSurveyChanged = [NSNumber numberWithBool:YES];
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
}

-(void) saveHeatingInspection{
    [self saveAllData];
    [self popOrCloseViewController];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue identifier] isEqualToString:SEG_ASF_AIRSOURCE_SURVEY_TO_PHOTOS]){
        [self saveAllData];
        PSAFSPhotoGridViewController * dest = (PSAFSPhotoGridViewController*)[segue destinationViewController];
        dest.rootVC = AFSPhotoGridRootAirsourceInspection;
        dest.appointment = self.appointment;
        dest.altHeating = _boiler;
    }
}
@end
