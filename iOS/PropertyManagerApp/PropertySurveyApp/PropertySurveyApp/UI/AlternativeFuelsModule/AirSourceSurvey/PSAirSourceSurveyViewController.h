//
//  PSAirSourceSurveyViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSBoolAndTextTableViewCell.h"
#import "PSAFSPhotoGridViewController.h"
@interface PSAirSourceSurveyViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (strong, nonatomic) AlternativeHeating *boiler;
@property (strong, nonatomic) PSBarButtonItem *btnSave;
@property(nonatomic,strong) PSBarButtonItem * cameraButton;
@property (strong,nonatomic) Appointment *appointment;

@property (nonatomic) BOOL checkElectricConnection;
@property (nonatomic) BOOL checkFuse;
@property (nonatomic) BOOL checkOilLeak;
@property (nonatomic) BOOL checkSupplementaryBonding;
@property ( nonatomic) BOOL checkThermostat;
@property (nonatomic) BOOL checkValves;
@property ( nonatomic) BOOL checkWaterPipework;
@property ( nonatomic) BOOL checkWaterSupplyTurnedOff;
@property ( nonatomic) BOOL checkWaterSupplyTurnedOn;
@property (strong, nonatomic) NSString *connectionDetail;
@property (strong, nonatomic) NSString *fuseDetail;
@property (strong, nonatomic) NSString *oilLeakDetail;
@property (strong, nonatomic) NSString *pipeworkDetail;
@property (strong, nonatomic) NSString *supplementaryBondingDetail;
@property (strong, nonatomic) NSString *thermostatDetail;
@property (strong, nonatomic) NSString *turnedOffDetail;
@property (strong, nonatomic) NSString *turnedOnDetail;
@property (strong, nonatomic) NSString *valveDetail;

@property (strong, nonatomic) NSNumber *inspectedBy;
@property (strong, nonatomic) NSNumber *inspectionId;
@property (strong, nonatomic) NSDate *inspectionDate;
@property ( nonatomic) BOOL  isInspected;

@end
