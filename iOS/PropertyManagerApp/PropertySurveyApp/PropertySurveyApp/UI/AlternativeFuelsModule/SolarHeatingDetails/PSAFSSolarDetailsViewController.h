//
//  PSAFSSolarDetailsViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSTextfieldTableViewCell.h"
#import "PSAFSPickerViewTableViewCell.h"
@interface PSAFSSolarDetailsViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (strong, nonatomic) AlternativeHeating *boiler;
@property NSString *deviceModel;
@property NSString *deviceSerialNumber;
@property BoilerManufacturer *deviceManufacturer;
@property SolarType *deviceType;
@property (strong, nonatomic) PSBarButtonItem *btnSaveBoiler;
@property NSInteger pickerTag;
@property (strong,nonatomic) NSMutableArray *pickerArray;
@end
