//
//  PSAFSSolarDetailsViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAFSSolarDetailsViewController.h"



#define kRowManufacturer    0
#define kRowModel   1
#define kRowSerialNumber  2
#define kRowType    3
@interface PSAFSSolarDetailsViewController ()

@end

@implementation PSAFSSolarDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [self registerCells];
    [self initDefaultValues];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Init methods

-(void) initData{
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 44.0;
    _tblView.allowsSelection = NO;
    [_tblView reloadData];
}

-(void) initDefaultValues{
    _deviceManufacturer = _boiler.alternativeHeatingToManufacturer;
    _deviceModel = _boiler.model;
    _deviceType = _boiler.alternativeHeatingToSolarType;
    _deviceSerialNumber = _boiler.serialNumber;
}

-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSPickerViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSPickerViewTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSTextfieldTableViewCell"];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSaveBoiler = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                style:PSBarButtonItemStyleDefault
                                                               target:self
                                                               action:@selector(btnSaveTapped)];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.btnSaveBoiler, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Solar"];
}


#pragma mark - Actions

- (void) btnBackTapped
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update this heating type?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveBoiler];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please fill all the mandatory fields"];
    }
    
    
    
}

-(void) saveBoiler{
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _boiler.alternativeHeatingToManufacturer = _deviceManufacturer;
    _boiler.manufacturerId = _deviceManufacturer.manufacturerId;
    _boiler.model = _deviceModel;
    _boiler.serialNumber = _deviceSerialNumber;
    _boiler.alternativeHeatingToSolarType = _deviceType;
    _boiler.solarTypeId = _deviceType.lookupId;
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
    [self popOrCloseViewController];
}

-(PSAltFuelDynamicCellsConfiguartion*) getCellConfigurationForRow:(NSInteger) row andSection:(NSInteger) section{
    PSAltFuelDynamicCellsConfiguartion * config = [[PSAltFuelDynamicCellsConfiguartion alloc] init];
    config.cellRowId = row;
    config.cellSectionId = section;
    config.inputFieldTitle = [self getHeaderForRow:row];
    config.selectedValue = [self getValueForRow:row];
    config.isNonMutableField = NO;
    config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeFreeText;
    config.appliedCellFieldType = PSAFSDynamicCellsBaseTypeFreeText;
    if(row==kRowManufacturer){
        config.dropDownHeaderTitle = @"SELECT MANUFACTURER";
        config.dropDownSourceArray = [[NSArray alloc] initWithArray:[self createPresentablePickerDataForRow:row]];
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDropDown;
        config.isNonMutableField = YES;
        
    }
    else if(row==kRowType){
        config.dropDownHeaderTitle = @"SELECT TYPE";
        config.dropDownSourceArray = [[NSArray alloc] initWithArray:[self createPresentablePickerDataForRow:row]];
        config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDropDown;
        config.isNonMutableField = YES;
    }
    return config;
}

#pragma mark - PickerRows

- (NSArray *) getPickerDataForRow:(NSInteger) row
{
    NSArray *rowsData = [NSArray array];
    NSPredicate *altFuelPred = [NSPredicate predicateWithFormat:@"SELF.isAlternativeHeating==1"];
    STARTEXCEPTION
    NSArray* boilerData;
    switch (row) {
        case kRowManufacturer:
            boilerData = [CoreDataHelper getObjectsFromEntity:kBoilerManufacturer
                                                    predicate:altFuelPred
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
            
            break;
        case kRowType:
            boilerData = [CoreDataHelper getObjectsFromEntity:kSolarType
                                                    predicate:nil
                                                      sortKey:nil
                                                sortAscending:YES
                                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
            break;
        default:
            break;
    }
    if([boilerData count] > 0)
    {
        rowsData = [NSArray arrayWithArray:boilerData];
    }
    ENDEXCEPTION
    return rowsData;
}

-(NSMutableArray *) createPresentablePickerDataForRow:(NSInteger) row{
    NSArray *objectsArray = [self getPickerDataForRow:row];
    NSMutableArray *presentableData = [[NSMutableArray alloc] init];
    if(row == kRowManufacturer){
        for(BoilerManufacturer *type in objectsArray){
            [presentableData addObject:type.manufacturerDescription];
        }
    }
    else  if(row == kRowType){
        for(SolarType *type in objectsArray){
            [presentableData addObject:type.value];
        }
    }
    return presentableData;
}


#pragma mark - Utility

-(BOOL) isValidData{
    if(!isEmpty(_deviceSerialNumber) && !isEmpty(_deviceType) && !isEmpty(_deviceModel) && !isEmpty(_deviceManufacturer)){
        if([_boiler.alternativeHeatingToManufacturer.manufacturerDescription isEqualToString:@"Please Select"] ||[_boiler.alternativeHeatingToSolarType.value isEqualToString:@"Please Select"])
        {
            return NO;
        }
        return YES;
    }
    return NO;
}

-(NSString *) getHeaderForRow:(NSInteger)row{
    NSString *header = @"";
    switch (row) {
        case kRowManufacturer:
            header = @"MANUFACTURER";
            break;
        case kRowModel:
            header = @"MODEL";
            break;
        case kRowSerialNumber:
            header = @"SERIAL NUMBER";
            break;
        case kRowType:
            header = @"TYPE";
            break;
        default:
            break;
    }
    return header;
}

-(NSString *) getValueForRow:(NSInteger)row{
    NSString *header = @"";
    switch (row) {
        case kRowManufacturer:
            header = _deviceManufacturer.manufacturerDescription;
            break;
        case kRowModel:
            header = _deviceModel;
            break;
        case kRowSerialNumber:
            header = _deviceSerialNumber;
            break;
        case kRowType:
            header = _deviceType.value;
            break;
        default:
            break;
    }
    if(isEmpty(header)==YES){
        header = @"";
    }
    return header;
}
-(void) setValueForBaseConfig:(PSAltFuelDynamicCellsConfiguartion *) config{
    switch (config.cellRowId) {
        case kRowManufacturer:
            break;
        case kRowModel:
            _deviceModel = config.selectedValue;
            break;
        case kRowSerialNumber:
            _deviceSerialNumber = config.selectedValue;
            break;
            break;
        default:
            break;
    }
    [_tblView reloadData];
}

#pragma mark - TableViewDelegates

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    PSAltFuelDynamicCellsConfiguartion *config = [self getCellConfigurationForRow:indexPath.row andSection:indexPath.section];
    if(indexPath.row == kRowModel || indexPath.row == kRowSerialNumber){
        PSAFSTextfieldTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSTextfieldTableViewCell class])];
        manualCell.delegate = self;
        manualCell.baseConfig = config;
        [manualCell initializeData];
        cell = manualCell;
    }
    else{
        PSAFSPickerViewTableViewCell *pickerCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSPickerViewTableViewCell class])];
        pickerCell.delegate = self;
        pickerCell.baseConfig = config;
        pickerCell.btnClearValue.hidden = YES;
        [pickerCell initializeData];
        cell = pickerCell;
    }
    cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - Delegates

-(void) didClickClearDataForRow:(PSAltFuelDynamicCellsConfiguartion*)config{
    switch (config.cellRowId) {
        case kRowManufacturer:
            _boiler.alternativeHeatingToManufacturer = nil;
            break;
        case kRowModel:
            _deviceModel = @"";
            break;
        case kRowSerialNumber:
            _deviceSerialNumber = @"";
            break;
            break;
        default:
            break;
    }
    [_tblView reloadData];
}

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion*)config{
    [self setValueForBaseConfig:config];
}

-(void) didClickShowDropDownFor:(PSAltFuelDynamicCellsConfiguartion *)config andSender:(id) sender{
    [self.view endEditing:YES];
    _pickerTag = config.cellRowId;
    _pickerArray = [[NSMutableArray alloc] initWithArray:config.dropDownSourceArray];
    NSInteger initialIndex = [_pickerArray indexOfObject:config.selectedValue];
    if(initialIndex>[config.dropDownSourceArray count]){
        initialIndex = 0;
    }
    [ActionSheetStringPicker showPickerWithTitle:[self getHeaderForRow:config.cellRowId]
                                            rows:_pickerArray
                                initialSelection:initialIndex
                                          target:self
                                   successAction:@selector(onOptionSelected:element:)
                                    cancelAction:@selector(onOptionPickerCancelled:)
                                          origin:sender];
}


- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    NSString *value = [_pickerArray objectAtIndex:[selectedIndex integerValue]];
    [_pickerArray removeAllObjects];
    if(_pickerTag==kRowManufacturer){
        BoilerManufacturer *mfr = [self getObjectForPickerTag:_pickerTag andValue:value];
        _deviceManufacturer = mfr;
        [_tblView reloadData];
    }
    else if(_pickerTag==kRowType){
        SolarType *mfr = [self getObjectForPickerTag:_pickerTag andValue:value];
        _deviceType = mfr;
        [_tblView reloadData];
    }
}

- (void) onOptionPickerCancelled:(id)sender {
    CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

-(id) getObjectForPickerTag:(NSInteger)pickerTag andValue:(NSString *) value{
    NSArray *dataArray = [self getPickerDataForRow:pickerTag];
    if(pickerTag==kRowManufacturer){
        BoilerManufacturer *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.manufacturerDescription = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    else if(pickerTag==kRowType){
        SolarType *type;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.value = %@", value];
        NSArray *filteredArray = [dataArray filteredArrayUsingPredicate:predicate];
        if([filteredArray count]>0){
            type = [filteredArray objectAtIndex:0];
        }
        return type;
    }
    return @"";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
