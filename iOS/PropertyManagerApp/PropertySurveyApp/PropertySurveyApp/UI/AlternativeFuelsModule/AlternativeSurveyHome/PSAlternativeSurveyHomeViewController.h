//
//  PSAlternativeSurveyHomeViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 06/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"

@interface PSAlternativeSurveyHomeViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet FBRImageView *imgViewProperty;
@property (weak, nonatomic) IBOutlet UIView *locationDetailView;
@property (weak, nonatomic) IBOutlet UILabel *lblTenantName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblCertificateExpiry;
@property (weak, nonatomic) IBOutlet UIView *issueCertificateView;
@property (weak, nonatomic) IBOutlet UIButton *btnIssueCertificate;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (strong,   nonatomic) Appointment *appointment;
@property (strong, nonatomic) PSBarButtonItem *noEntryButton;
@property (strong, nonatomic) PSBarButtonItem *abortButton;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property NSArray *cellTitles;
@end
