//
//  PSAlternativeSurveyHomeViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 06/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAlternativeSurveyHomeViewController.h"
#import "PSNoEntryViewController.h"
#import "PSAbortInspectionViewController.h"
#import "PSAlternateFuelServiceSurveyTableViewCell.h"
#import "PSAFSHeatingTypesViewController.h"
#import "PSAFSIssueCertificateViewController.h"
#import "PSGeneralHeatingChecksViewController.h"
#import "PSAFSPhotoGridViewController.h"

#define kTotalNumberOfRows 4
#define kRowHeatingsInspected 0
#define kRowPhotographs 1
#define kRowGeneralHeatingChecks 2
#define kRowIssueReceivedBy 3


@interface PSAlternativeSurveyHomeViewController ()

@end

@implementation PSAlternativeSurveyHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [self registerNotifications];
    [self configureIssueCertificateButton];
    [self initModule];
}

-(void) viewWillDisappear:(BOOL)animated{
    [self deRegisterNotifications];
}

#pragma mark - Init methods

-(void) configureIssueCertificateButton{
    if ([self checkInspectionStatus] && [self.appointment.appointmentToCP12Info.inspectionCarried boolValue])
    {
        [self.btnIssueCertificate setEnabled:YES];
        [self.btnIssueCertificate setBackgroundColor:[UIColor redColor]];
    }
    else
    {
        [self.btnIssueCertificate setEnabled:NO];
        [self.btnIssueCertificate setBackgroundColor:[UIColor darkGrayColor]];
    }
}

-(BOOL) checkInspectionStatus{
    BOOL isCompletelyInspected = YES;
    for(AlternativeHeating *blr in self.appointment.appointmentToProperty.propertyToAlternativeHeating){
        if([blr.heatingFuel isEqualToString:kAFSFuelAirSource] ||[blr.heatingFuel isEqualToString:kAFSFuelSolar]||[blr.heatingFuel isEqualToString:kAFSFuelMVHR]){
            if(![blr.isInspected boolValue]){
                isCompletelyInspected = NO;
                break;
            }
        }
    }
    return isCompletelyInspected;
}
-(void) initModule{
    //[_btnIssueCertificate setEnabled:FALSE];
    [self loadNavigationonBarItems];
     [self.imgViewProperty setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
    _cellTitles = @[@"HEATING TYPES INSPECTED", @"PHOTOGRAPHS", @"GENERAL HEATING CHECKS", @"ISSUE/RECEIVED BY"];
    
    Customer *customer = [self.appointment defaultTenant];
    self.lblTenantName.text = [customer fullName];
    
    Property *property = [self.appointment appointmentToProperty];
    if(!isEmpty(property.propertyId)){
        self.lblLocationAddress.text = [property addressWithStyle:PropertyAddressStyle1];
    }
    else if(!isEmpty(property.blockId)){
        self.lblLocationAddress.text = property.blockName;
    }
    else if(!isEmpty(property.schemeId)){
        self.lblLocationAddress.text = property.schemeName;
    }
    
    if (!isEmpty(self.appointment.appointmentToProperty.certificateExpiry))
    {
        [self.lblCertificateExpiry setHidden:NO];
        [self.lblCertificateExpiry setText:[NSString stringWithFormat:@"Certificate Expiry: %@",[UtilityClass stringFromDate:self.appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]]];
    }
    else
    {
        [self.lblCertificateExpiry setHidden:NO];
        [self.lblCertificateExpiry setText:@"Certificate Expiry: N/A"];
    }
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PSAlternateFuelServiceSurveyTableViewCell class]) bundle:nil]
         forCellReuseIdentifier:NSStringFromClass([PSAlternateFuelServiceSurveyTableViewCell class])];
    [self.tableView reloadData];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    if ([self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
    {
        self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
                                                                    style:PSBarButtonItemStyleDefault
                                                                   target:self
                                                                   action:@selector(onClickNoEntryButton)];
        
        self.abortButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_ABORT")
                                                                  style:PSBarButtonItemStyleAbort
                                                                 target:self
                                                                 action:@selector(onClickAbortButton)];
        
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.noEntryButton,self.abortButton, nil]];
    }
    
    [self setTitle:@"Alternative Fuels Servicing Survey"];
}

#pragma mark - Navbar methods

-(void) onClickAbortButton
{
    CLS_LOG(@"abort clicked");
    PSAbortInspectionViewController *abortVC = [[PSAbortInspectionViewController alloc] initWithNibName:@"PSAbortInspectionViewController" bundle:nil];
    abortVC.appointment = self.appointment;
    [self.navigationController pushViewController:abortVC animated:YES];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickNoEntryButton
{
    PSNoEntryViewController *noEntryViewController = [[PSNoEntryViewController alloc] initWithNibName:@"PSNoEntryViewController" bundle:nil];
    [noEntryViewController setAppointment:self.appointment];
    [self.navigationController pushViewController:noEntryViewController animated:YES];
    
    CLS_LOG(@"onClickNoEntryButton");
}


#pragma mark - Tableview methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    /*
     The count check ensures that if array is empty, i.e no record exists
     for particular section, the header height is set to zero so that
     section header is not alloted any space.
     */
    CGFloat headerHeight = 0.0;
    
    return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kTotalNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    PSAlternateFuelServiceSurveyTableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAlternateFuelServiceSurveyTableViewCell class])];
    [self configureCell:_cell atIndexPath:indexPath];
    cell = _cell;
    cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[PSAlternateFuelServiceSurveyTableViewCell class]])
    {
        
        PSAlternateFuelServiceSurveyTableViewCell *_cell = (PSAlternateFuelServiceSurveyTableViewCell *) cell;
        [_cell.lblItemName setText:[_cellTitles objectAtIndex:indexPath.row]];
        _cell.lblItemName.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblItemName.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        BOOL isInspected = [self checkInspectionStatus];
        BOOL isCertIssued = [self.appointment.appointmentToCP12Info.inspectionCarried boolValue];
        BOOL isGeneralChecked = isEmpty(self.appointment.appointmentToProperty.propertyToGeneralHeatingChecks)?NO:YES;
        _cell.imgViewStatus.image = [UIImage imageNamed:@"icon_Cross"];
        
        if (isInspected && indexPath.row==kRowHeatingsInspected)
        {
            _cell.imgViewStatus.image = [UIImage imageNamed:@"icon_Complete"];
        }
        if(isCertIssued && indexPath.row == kRowIssueReceivedBy)
        {
            _cell.imgViewStatus.image = [UIImage imageNamed:@"icon_Complete"];
        }
        if(isGeneralChecked && indexPath.row==kRowGeneralHeatingChecks){
            _cell.imgViewStatus.image = [UIImage imageNamed:@"icon_Complete"];
        }
        
        if(indexPath.row!=kRowGeneralHeatingChecks && indexPath.row!=kRowIssueReceivedBy &&indexPath.row!=kRowHeatingsInspected){
            _cell.imgViewStatus.hidden = YES;
        }
        else
        {
            _cell.imgViewStatus.hidden = NO;
            if(indexPath.row==kRowHeatingsInspected){
                _cell.imgViewStatus.hidden = YES;
                for(AlternativeHeating *heating in self.appointment.appointmentToProperty.propertyToAlternativeHeating){
                    if([heating.heatingFuel isEqualToString:kAFSFuelAirSource]){
                        _cell.imgViewStatus.hidden = NO;
                        break;
                    }
                    else if([heating.heatingFuel isEqualToString:kAFSFuelMVHR]){
                        _cell.imgViewStatus.hidden = NO;
                        break;
                    }
                    else if([heating.heatingFuel isEqualToString:kAFSFuelSolar]){
                        _cell.imgViewStatus.hidden = NO;
                        break;
                    }
                }
            }
            
        }
        
        cell = _cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == kRowHeatingsInspected){
        [self performSegueWithIdentifier:SEG_ASF_HOME_TO_HEATING_TYPES_LIST_SCREEN sender:self];
    }
    else if(indexPath.row == kRowPhotographs){
        [self performSegueWithIdentifier:SEG_ASF_HOME_TO_PHOTO_GRID_SCREEN sender:self];
    }
    else if(indexPath.row==kRowGeneralHeatingChecks){
        [self performSegueWithIdentifier:SEG_ASF_HOME_TO_GENERAL_ELEC_CHECK_SCREEN sender:self];
    }
    else if(indexPath.row==kRowIssueReceivedBy){
        [self performSegueWithIdentifier:SEG_ASF_HOME_TO_ISSUE_CERT_SCREEN sender:self];
    }
    
}




#pragma mark - Actions
- (IBAction)btnBackTapped:(id)sender {
    [self onClickBackButton];
}


#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentSuccessNotificationReceive) name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentFailureNotificationReceive) name:kCompleteAppointmentFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentFailureNotification object:nil];
}


- (void) onCompleteAppointmentSuccessNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

- (void) onCompleteAppointmentFailureNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        self.appointment.appointmentStatus = kAppointmentStatusInProgress;
        
        [self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
                         message:LOC(@"KEY_ALERT_FAILED_TO_COMPLETE_APPOINTMENT")
                             tag:0
               cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
               otherButtonTitles:nil];
    });
}

- (IBAction)btnIssueCertificateTapped:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:LOC(@"KEY_ALERT_COMPLETE_APPOINTMENT") message:LOC(@"KEY_ALERT_CONFIRM_COMPLETE_APPOINTMENT") preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self completeAppointment];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:LOC(@"KEY_ALERT_NO") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) completeAppointment{
    self.appointment.appointmentToCP12Info.cp12Passed = [NSNumber numberWithBool:YES];
    [self.appointment.managedObjectContext save:nil];
    
    self.appointment.appointmentStatus = [UtilityClass statusStringForAppointmentStatus:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]]];
    [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]] appointment:self.appointment];
    [self popToRootViewController];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue identifier] isEqualToString:SEG_ASF_HOME_TO_HEATING_TYPES_LIST_SCREEN]){
        PSAFSHeatingTypesViewController *dest = (PSAFSHeatingTypesViewController *)[segue destinationViewController];
        dest.appointment = self.appointment;
    }
    if([[segue identifier] isEqualToString:SEG_ASF_HOME_TO_GENERAL_ELEC_CHECK_SCREEN]){
        PSGeneralHeatingChecksViewController *dest = (PSGeneralHeatingChecksViewController *)[segue destinationViewController];
        dest.appointment = self.appointment;
    }
    else if([[segue identifier] isEqualToString:SEG_ASF_HOME_TO_ISSUE_CERT_SCREEN]){
        PSAFSIssueCertificateViewController *dest = (PSAFSIssueCertificateViewController *)[segue destinationViewController];
        dest.appointment = self.appointment;
        dest.cp12Info = self.appointment.appointmentToCP12Info;
    }
    else if([[segue identifier] isEqualToString:SEG_ASF_HOME_TO_PHOTO_GRID_SCREEN]){
        PSAFSPhotoGridViewController * dest = (PSAFSPhotoGridViewController*)[segue destinationViewController];
        dest.rootVC = AFSPhotoGridRootProperty;
        dest.appointment = self.appointment;
    }
}

-(BOOL) shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:SEG_ASF_HOME_TO_ISSUE_CERT_SCREEN]){
        if(![self checkInspectionStatus]){
            [self showMessageWithHeader:@"Alert" andBody:@"Please inspect all heatings first"];
            return NO;
        }
    }
    return YES;
}

@end
