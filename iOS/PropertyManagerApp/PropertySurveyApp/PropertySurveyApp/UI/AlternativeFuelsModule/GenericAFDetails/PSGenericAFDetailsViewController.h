//
//  PSGenericAFDetailsViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 15/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSTextfieldTableViewCell.h"
#import "PSAFSPickerViewTableViewCell.h"
@interface PSGenericAFDetailsViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (weak, nonatomic) AlternativeHeating *boiler;
@property NSInteger pickerTag;
@property (strong,nonatomic) NSMutableArray *pickerArray;
@property NSString *deviceModel;
@property NSString *deviceSerialNumber;
@property NSString * deviceLocation;
@property BoilerManufacturer *deviceManufacturer;


@property NSDate* installedDate;
@property NSString* certificateName;
@property NSDate* certificateIssued;
@property NSNumber * cerificateNumber;
@property NSDate* certificateRenewal;
@property NSDate *replacementDue;
@property NSDate *lastInspected;



@property (strong, nonatomic) PSBarButtonItem *btnSaveBoiler;
@end
