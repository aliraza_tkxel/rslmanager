//
//  PSMVHRDetailsViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSTextfieldTableViewCell.h"
#import "PSAFSPickerViewTableViewCell.h"
@interface PSMVHRDetailsViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property NSString *deviceModel;
@property NSString * deviceLocation;
@property BoilerManufacturer *deviceManufacturer;
@property (strong, nonatomic) AlternativeHeating *boiler;
@property (strong, nonatomic) PSBarButtonItem *btnSaveBoiler;
@property NSInteger pickerTag;
@property (strong,nonatomic) NSMutableArray *pickerArray;
@end
