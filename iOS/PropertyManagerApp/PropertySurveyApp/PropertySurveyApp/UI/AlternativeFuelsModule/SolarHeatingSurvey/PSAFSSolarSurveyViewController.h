//
//  PSAFSSolarSurveyViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSBoolAndTextTableViewCell.h"
#import "PSCameraViewController.h"
#import "PSAFSTextfieldTableViewCell.h"
#import "PSAFSPickerViewTableViewCell.h"
#import "PSAFSPhotoGridViewController.h"
@interface PSAFSSolarSurveyViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (strong, nonatomic) AlternativeHeating *boiler;
@property (strong, nonatomic) PSBarButtonItem *btnSave;
@property(nonatomic,strong) PSBarButtonItem * cameraButton;
@property (strong,nonatomic) Appointment *appointment;
@property NSInteger pickerTag;
@property NSArray *pickerArray;
@property (nonatomic) BOOL antiScaldingControl;
@property (strong, nonatomic) NSString *antiScaldingControlDetail;
@property (nonatomic) BOOL checkDirection;
@property (strong, nonatomic) NSString *directionDetail;
@property (nonatomic) BOOL checkElectricalControl;
@property (strong, nonatomic) NSString *electricalControlDetail;
@property (strong, nonatomic) NSNumber *backPressure;
@property (strong, nonatomic) NSNumber *calculationRate;
@property (strong, nonatomic) NSNumber *deltaOff;
@property (strong, nonatomic) NSNumber *deltaOn;
@property (strong, nonatomic) NSNumber *freezingTemp;
@property (strong, nonatomic) NSNumber *inspectedBy;
@property (strong, nonatomic) NSDate *inspectionDate;
@property (strong, nonatomic) NSNumber *inspectionId;
@property (nonatomic) BOOL isInspected;
@property (strong, nonatomic) NSNumber *maxTemperature;
@property (strong, nonatomic) NSNumber *minPressure;
@property (strong, nonatomic) NSNumber *systemPressure;
@property (strong, nonatomic) NSString *thermostatTemperature;
@property (strong, nonatomic) NSNumber *vesselCapacity;
@property (strong, nonatomic) NSString *vesselProtectionInstalled;

@end
