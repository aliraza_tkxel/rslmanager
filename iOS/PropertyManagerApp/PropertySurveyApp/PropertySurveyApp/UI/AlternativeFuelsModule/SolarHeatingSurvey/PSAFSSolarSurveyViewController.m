//
//  PSAFSSolarSurveyViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAFSSolarSurveyViewController.h"

#define kSectionExpansion    0
#define kSectionPumpStation   1

#define kSectionExpansionRowVesselProtectionInstalled   0
#define kSectionExpansionRowVesselCapacity  1
#define kSectionExpansionRowMinPressure 2
#define kSectionExpansionRowFreezingTemp    3
#define kSectionExpansionRowSystemPressure  4
#define kSectionExpansionRowBackPressure    5

#define kSectionPumpStationRowDeltaOn   0
#define kSectionPumpStationRowDeltaOff  1
#define kSectionPumpStationRowMaxTemperature    2
#define kSectionPumpStationRowCalculationRate   3
#define kSectionPumpStationRowThermostatTemperature 4
#define kSectionPumpStationRowAntiScaldingControl   5
#define kSectionPumpStationRowCheckDirection    6
#define kSectionPumpStationRowCheckElectricalControl    7

typedef NS_ENUM(NSInteger, SolarCellType)
{
    SolarCellTypeSimpleText = 0,
    SolarCellTypeDropdown = 1,
    SolarCellTypeBoolAndText = 2,
    SolarCellTypeTextNumeric = 3,
    SolarCellTypeDecimal = 4,
};


@interface PSAFSSolarSurveyViewController ()

@end

@implementation PSAFSSolarSurveyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [self registerCells];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [self refreshPhotosCount];
}

#pragma mark - Init methods

-(void) initData{
    [self populateDefaultValues];
    _pickerArray = @[@"YES",@"NO"];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 44.0;
    _tblView.allowsSelection = NO;
    [_tblView reloadData];
}

-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSBoolAndTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSBoolAndTextTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSPickerViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSPickerViewTableViewCell"];
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSTextfieldTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSTextfieldTableViewCell"];
}
-(void) refreshPhotosCount{
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    if(!isEmpty(self.cameraButton)){
        [self.cameraButton reloadBadge:badgeText];
    }
    
}
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSave = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                          style:PSBarButtonItemStyleDefault
                                                         target:self
                                                         action:@selector(btnSaveTapped)];
    
    
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    
    self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                               style:PSBarButtonItemStyleCamera
                                                              target:self
                                                              action:@selector(onClickCameraButton)bagde:badgeText];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.cameraButton,self.btnSave, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Solar"];
}

-(void) populateDefaultValues{
    if(!isEmpty(_boiler.alternativeHeatingToSolarInspection)){
        _isInspected =!isEmpty(_boiler.alternativeHeatingToSolarInspection.isInspected)?[_boiler.alternativeHeatingToSolarInspection.isInspected boolValue]:NO;
        _antiScaldingControl =!isEmpty(_boiler.alternativeHeatingToSolarInspection.antiScaldingControl)?[_boiler.alternativeHeatingToSolarInspection.antiScaldingControl boolValue]:NO;
        _antiScaldingControlDetail = !isEmpty(_boiler.alternativeHeatingToSolarInspection.antiScaldingControlDetail)?_boiler.alternativeHeatingToSolarInspection.antiScaldingControlDetail:@"";
        _checkDirection = !isEmpty(_boiler.alternativeHeatingToSolarInspection.checkDirection)?[_boiler.alternativeHeatingToSolarInspection.checkDirection boolValue]:NO;
        _directionDetail = !isEmpty(_boiler.alternativeHeatingToSolarInspection.directionDetail)?_boiler.alternativeHeatingToSolarInspection.directionDetail:@"";
        _checkElectricalControl = !isEmpty(_boiler.alternativeHeatingToSolarInspection.checkElectricalControl)?[_boiler.alternativeHeatingToSolarInspection.checkElectricalControl boolValue]:NO;
        _electricalControlDetail = !isEmpty(_boiler.alternativeHeatingToSolarInspection.electricalControlDetail)?_boiler.alternativeHeatingToSolarInspection.electricalControlDetail:@"";
        _backPressure =!isEmpty(_boiler.alternativeHeatingToSolarInspection.backPressure)?_boiler.alternativeHeatingToSolarInspection.backPressure:nil;
        _systemPressure =!isEmpty(_boiler.alternativeHeatingToSolarInspection.backPressure)?_boiler.alternativeHeatingToSolarInspection.backPressure:nil;
        _calculationRate = !isEmpty(_boiler.alternativeHeatingToSolarInspection.calculationRate)?_boiler.alternativeHeatingToSolarInspection.calculationRate:nil;
        _deltaOff = !isEmpty(_boiler.alternativeHeatingToSolarInspection.deltaOff)?_boiler.alternativeHeatingToSolarInspection.deltaOff:nil;
        _inspectedBy = !isEmpty(_boiler.alternativeHeatingToSolarInspection.inspectedBy)?_boiler.alternativeHeatingToSolarInspection.inspectedBy:nil;
        _inspectionId = !isEmpty(_boiler.alternativeHeatingToSolarInspection.inspectionId)?_boiler.alternativeHeatingToSolarInspection.inspectionId:nil;
        _inspectionDate =!isEmpty(_boiler.alternativeHeatingToSolarInspection.inspectionDate)?_boiler.alternativeHeatingToSolarInspection.inspectionDate:[NSDate date];
        _deltaOn = !isEmpty(_boiler.alternativeHeatingToSolarInspection.deltaOn)?_boiler.alternativeHeatingToSolarInspection.deltaOn:nil;
        _freezingTemp = !isEmpty(_boiler.alternativeHeatingToSolarInspection.freezingTemp)?_boiler.alternativeHeatingToSolarInspection.freezingTemp:nil;
        _maxTemperature = !isEmpty(_boiler.alternativeHeatingToSolarInspection.maxTemperature)?_boiler.alternativeHeatingToSolarInspection.maxTemperature:nil;
        _minPressure = !isEmpty(_boiler.alternativeHeatingToSolarInspection.minPressure)?_boiler.alternativeHeatingToSolarInspection.minPressure:nil;
        _thermostatTemperature = !isEmpty(_boiler.alternativeHeatingToSolarInspection.thermostatTemperature)?_boiler.alternativeHeatingToSolarInspection.thermostatTemperature:nil;
        _vesselCapacity =!isEmpty(_boiler.alternativeHeatingToSolarInspection.vesselCapacity)?_boiler.alternativeHeatingToSolarInspection.vesselCapacity:nil;
        _vesselProtectionInstalled =(_boiler.alternativeHeatingToSolarInspection.vesselProtectionInstalled)?@"YES":@"NO";
    }
    else{
        _antiScaldingControl = NO;
        _antiScaldingControlDetail = @"";
        _checkDirection = NO;
        _directionDetail = @"";
        _checkElectricalControl = NO;
        _electricalControlDetail = @"";
    }
}

#pragma mark - Actions

- (void) btnBackTapped
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update this heating type?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveHeatingInspection];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please fill all the mandatory fields"];
    }
    
    
    
}


- (void)onClickCameraButton {
    [self.view endEditing:YES];
    CLS_LOG(@"Camera Button");
    if([self isValidData]){
        [self performSegueWithIdentifier:SEG_ASF_SOLAR_SURVEY_TO_PHOTOS sender:self];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please check/fill all the mandatory fields"];
    }
    
}

#pragma mark - Utility

-(NSUInteger) getPhotographsCount
{
    NSArray * photographsList =[_boiler.alternativeHeatingToSolarInspection.solarInspectionToPictures allObjects];
    return !isEmpty(photographsList)?[photographsList count]:0;
}


-(BOOL) isValidData{
    if(!isEmpty(_antiScaldingControlDetail) && !isEmpty(_directionDetail)&& !isEmpty(_electricalControlDetail) && !isEmpty(_backPressure)&& !isEmpty(_calculationRate) && !isEmpty(_deltaOn) && !isEmpty(_deltaOff) && !isEmpty(_freezingTemp)&& !isEmpty(_maxTemperature) && !isEmpty(_minPressure) && !isEmpty(_systemPressure) && !isEmpty(_thermostatTemperature) && !isEmpty(_vesselCapacity) && !isEmpty(_vesselProtectionInstalled)){
        return YES;
    }
    return NO;
}

-(NSString *) getHeaderForSection:(NSInteger) section{
    NSString *header = @"";
    switch (section) {
        case kSectionExpansion:
            header = @"EXPANSION AND PRESSURE";
            break;
        case kSectionPumpStation:
            header = @"PUMP STATION & ANCILLARY CONTROLS";
        default:
            break;
    }
    return header;
}

-(NSString *) getCellLabelForSection:(NSInteger) section andRow:(NSInteger) row{
    NSString *label = @"";
    if(section==kSectionExpansion){
        switch (row) {
            case kSectionExpansionRowVesselProtectionInstalled:
                label = @"EXPANSION VESSEL PROTECTION INSTALLED";
                break;
            case kSectionExpansionRowVesselCapacity:
                label = @"EXPANSION VESSEL CAPACITY: LITRES";
                break;
            case kSectionExpansionRowMinPressure:
                label = @"MIN OPERATING PRESSURE OF THE SYSTEM: BAR";
                break;
            case kSectionExpansionRowFreezingTemp:
                label = @"FREEZING TEMP OF HEAT TRANSFER FLUID: °C";
                break;
            case kSectionExpansionRowSystemPressure:
                label = @"SYSTEM PRESSURE SETTING WHEN FILLED: BAR";
                break;
            case kSectionExpansionRowBackPressure:
                label = @"BACK PRESSURE: LITRES";
                break;
            default:
                break;
        }
    }
    else if(section == kSectionPumpStation){
        switch (row) {
            case kSectionPumpStationRowDeltaOn:
                label = @"DELTA-T ON";
                break;
            case kSectionPumpStationRowDeltaOff:
                label = @"DELTA-T OFF";
                break;
            case kSectionPumpStationRowMaxTemperature:
                label = @"STORE MAX TEMPERATURE";
                break;
            case kSectionPumpStationRowCalculationRate:
                label = @"CALCULATION RATE SETTING: L/MIN";
                break;
            case kSectionPumpStationRowThermostatTemperature:
                label = @"DHW THERMOSTATE SETTING TEMPERATURE";
                break;
            case kSectionPumpStationRowAntiScaldingControl:
                label = @"Anti scalding control present (i.e. TMV)";
                break;
            case kSectionPumpStationRowCheckDirection:
                label = @"Direction of circulation through collector matched to sensor position";
                break;
            case kSectionPumpStationRowCheckElectricalControl:
                label = @"Electrical controls and temperature sensors working satisfactorily";
                break;
            default:
                break;
        }
    }
    return label;
}


-(SolarCellType) getCellTypeForSection:(NSInteger) section andRow:(NSInteger) row{
    SolarCellType label = SolarCellTypeSimpleText;
    if(section==kSectionExpansion){
        switch (row) {
            case kSectionExpansionRowVesselProtectionInstalled:
                label = SolarCellTypeDropdown;
                break;
            case kSectionExpansionRowVesselCapacity:
                label = SolarCellTypeDecimal;
                break;
            case kSectionExpansionRowMinPressure:
                label =SolarCellTypeDecimal;
                break;
            case kSectionExpansionRowFreezingTemp:
                label = SolarCellTypeTextNumeric;
                break;
            case kSectionExpansionRowSystemPressure:
                label = SolarCellTypeTextNumeric;
                break;
            case kSectionExpansionRowBackPressure:
                label = SolarCellTypeTextNumeric;
                break;
            default:
                break;
        }
    }
    else if(section == kSectionPumpStation){
        switch (row) {
            case kSectionPumpStationRowDeltaOn:
                label = SolarCellTypeTextNumeric;
                break;
            case kSectionPumpStationRowDeltaOff:
                label = SolarCellTypeTextNumeric;
                break;
            case kSectionPumpStationRowMaxTemperature:
                label = SolarCellTypeTextNumeric;
                break;
            case kSectionPumpStationRowCalculationRate:
                label = SolarCellTypeTextNumeric;
                break;
            case kSectionPumpStationRowThermostatTemperature:
                label = SolarCellTypeDecimal;
                break;
            case kSectionPumpStationRowAntiScaldingControl:
                label = SolarCellTypeBoolAndText;
                break;
            case kSectionPumpStationRowCheckDirection:
                label = SolarCellTypeBoolAndText;
                break;
            case kSectionPumpStationRowCheckElectricalControl:
                label = SolarCellTypeBoolAndText;
                break;
            default:
                break;
        }
    }
    return label;
}

- (void)addBottomBorderTo:(UIView*)parentView WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth {
    UIView *border = [UIView new];
    border.backgroundColor = color;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    border.frame = CGRectMake(0, parentView.frame.size.height - borderWidth, parentView.frame.size.width, borderWidth);
    [parentView addSubview:border];
}

#pragma mark - Getters and Setters
-(PSAltFuelDynamicCellsConfiguartion*) getCellConfigurationForRow:(NSInteger) row andSection:(NSInteger) section{
    PSAltFuelDynamicCellsConfiguartion * config = [[PSAltFuelDynamicCellsConfiguartion alloc] init];
    config.cellRowId = row;
    config.cellSectionId = section;
    config.inputFieldTitle = [self getCellLabelForSection:section andRow:row];
    config.selectedBoolValue = [self getBoolSelectedValueForSection:section andRow:row];
    config.selectedValue = [self getSelectedStringValueForSection:section andRow:row];
    config.isNonMutableField = NO;
    switch ([self getCellTypeForSection:section andRow:row]) {
        case SolarCellTypeBoolAndText:
            config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeBoolAndText;
            config.appliedCellFieldType = PSAFSDynamicCellsFreeTextFieldTypeText;
            break;
        case SolarCellTypeDropdown:
            config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeDropDown;
            config.isNonMutableField = YES;
            config.dropDownHeaderTitle = @"VESSEL PROTECTION INSTALLED";
            config.dropDownSourceArray = _pickerArray;
            break;
        case SolarCellTypeSimpleText:
            config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeFreeText;
            config.appliedCellFieldType = PSAFSDynamicCellsFreeTextFieldTypeText;
            break;
        case SolarCellTypeTextNumeric:
            config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeFreeText;
            config.appliedCellFieldType = PSAFSDynamicCellsFreeTextFieldNumeric;
            break;
        case SolarCellTypeDecimal:
            config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeFreeText;
            config.appliedCellFieldType = PSAFSDynamicCellsFreeTextFieldTypeDecimal;
            break;
        default:
            break;
    }
    return config;
}


-(NSString *) getSelectedStringValueForSection:(NSInteger) section andRow:(NSInteger) row{
    NSString *label = @"";
    if(section==kSectionExpansion){
        switch (row) {
            case kSectionExpansionRowVesselProtectionInstalled:
                label = _vesselProtectionInstalled;
                break;
            case kSectionExpansionRowVesselCapacity:
                label = !isEmpty(_vesselCapacity)?[_vesselCapacity stringValue]:@"";
                break;
            case kSectionExpansionRowMinPressure:
                label = !isEmpty(_minPressure)?[_minPressure stringValue]:@"";
                break;
            case kSectionExpansionRowFreezingTemp:
                label = !isEmpty(_freezingTemp)?[_freezingTemp stringValue]:@"";
                break;
            case kSectionExpansionRowSystemPressure:
                label = !isEmpty(_systemPressure)?[_systemPressure stringValue]:@"";
                break;
            case kSectionExpansionRowBackPressure:
                label = !isEmpty(_backPressure)?[_backPressure stringValue]:@"";
                break;
            default:
                break;
        }
    }
    else if(section==kSectionPumpStation){
        switch (row) {
            case kSectionPumpStationRowDeltaOn:
                label = !isEmpty(_deltaOn)?[_deltaOn stringValue]:@"";
                break;
            case kSectionPumpStationRowDeltaOff:
                label = !isEmpty(_deltaOff)?[_deltaOff stringValue]:@"";
                break;
            case kSectionPumpStationRowMaxTemperature:
                label = !isEmpty(_maxTemperature)?[_maxTemperature stringValue]:@"";
                break;
            case kSectionPumpStationRowCalculationRate:
                label = !isEmpty(_calculationRate)?[_calculationRate stringValue]:@"";
                break;
            case kSectionPumpStationRowThermostatTemperature:
                label = !isEmpty(_thermostatTemperature)?_thermostatTemperature:@"";
                break;
            case kSectionPumpStationRowAntiScaldingControl:
                label = !isEmpty(_antiScaldingControlDetail)?_antiScaldingControlDetail:@"";
                break;
            case kSectionPumpStationRowCheckDirection:
                label = !isEmpty(_directionDetail)?_directionDetail:@"";
                break;
            case kSectionPumpStationRowCheckElectricalControl:
                label = !isEmpty(_electricalControlDetail)?_electricalControlDetail:@"";
                break;
            default:
                break;
        }
    }
   
    return label;
}

-(void) setSelectedStringValueWithConfig:(PSAltFuelDynamicCellsConfiguartion*)config{
    NSString *label = config.selectedValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    if(section==kSectionExpansion){
        switch (row) {
            case kSectionExpansionRowVesselProtectionInstalled:
                break;
            case kSectionExpansionRowVesselCapacity:
                _vesselCapacity = [NSNumber numberWithDouble:[label doubleValue]];
                break;
            case kSectionExpansionRowMinPressure:
                _minPressure = [NSNumber numberWithDouble:[label doubleValue]];
                break;
            case kSectionExpansionRowFreezingTemp:
                _freezingTemp = [NSNumber numberWithInteger:[label integerValue]];
                break;
            case kSectionExpansionRowSystemPressure:
                _systemPressure = [NSNumber numberWithInteger:[label integerValue]];
                break;
            case kSectionExpansionRowBackPressure:
                _backPressure = [NSNumber numberWithInteger:[label integerValue]];
                break;
            default:
                break;
        }
    }
    else if(section==kSectionPumpStation){
        switch (row) {
            case kSectionPumpStationRowDeltaOn:
                _deltaOn = [NSNumber numberWithInteger:[label integerValue]];
                break;
            case kSectionPumpStationRowDeltaOff:
                _deltaOff = [NSNumber numberWithInteger:[label integerValue]];
                break;
            case kSectionPumpStationRowMaxTemperature:
                _maxTemperature = [NSNumber numberWithInteger:[label integerValue]];
                break;
            case kSectionPumpStationRowCalculationRate:
                _calculationRate = [NSNumber numberWithInteger:[label integerValue]];
                break;
            case kSectionPumpStationRowThermostatTemperature:
                _thermostatTemperature = label;
                break;
            case kSectionPumpStationRowAntiScaldingControl:
                _antiScaldingControlDetail = label;
                break;
            case kSectionPumpStationRowCheckDirection:
                _directionDetail = label;
                break;
            case kSectionPumpStationRowCheckElectricalControl:
                _electricalControlDetail = label;
                break;
            default:
                break;
        }
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}

-(BOOL) getBoolSelectedValueForSection:(NSInteger) section andRow:(NSInteger) row{
    BOOL label = NO;
    if(section==kSectionPumpStation){
        switch (row) {
            case kSectionPumpStationRowAntiScaldingControl:
                label = _antiScaldingControl;
                break;
            case kSectionPumpStationRowCheckDirection:
                label = _checkDirection;
                break;
            case kSectionPumpStationRowCheckElectricalControl:
                label = _checkElectricalControl;
                break;
            default:
                break;
        }
    }
    return label;
}

-(void) setBoolSelectedValueWithConfig:(PSAltFuelDynamicCellsConfiguartion*)config{
    BOOL label = config.selectedBoolValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    if(section==kSectionPumpStation){
        switch (row) {
            case kSectionPumpStationRowAntiScaldingControl:
                _antiScaldingControl = label;
                break;
            case kSectionPumpStationRowCheckDirection:
                _checkDirection = label;
                break;
            case kSectionPumpStationRowCheckElectricalControl:
                _checkElectricalControl = label;
                break;
            default:
                break;
        }
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - UITableViewDelegates

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==kSectionExpansion){
        return 6;
    }
    else if(section==kSectionPumpStation){
        return 8;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 40)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [label setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    
    label.text = [self getHeaderForSection:section];
    label.textAlignment = NSTextAlignmentLeft;
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    [self addBottomBorderTo:view WithColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR) andWidth:2.0f];
    return view;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    PSAltFuelDynamicCellsConfiguartion *config = [self getCellConfigurationForRow:indexPath.row andSection:indexPath.section];
    if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeBoolAndText)
    {
        PSAFSBoolAndTextTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSBoolAndTextTableViewCell class])];
        manualCell.baseConfig = config;
        manualCell.delegate = self;
        [manualCell initWithConfig];
        [manualCell setNeedsUpdateConstraints];
        [manualCell updateConstraintsIfNeeded];
        [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        cell = manualCell;
    }
    else if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeFreeText){
        PSAFSTextfieldTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSTextfieldTableViewCell class])];
        manualCell.baseConfig = config;
        manualCell.delegate = self;
        [manualCell initializeData];
        [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        cell = manualCell;
    }
    else if(config.baseTypeForConfig==PSAFSDynamicCellsBaseTypeDropDown){
        PSAFSPickerViewTableViewCell *pickerCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSPickerViewTableViewCell class])];
        pickerCell.btnClearValue.hidden = YES;
        pickerCell.baseConfig = config;
        pickerCell.delegate = self;
        [pickerCell initializeData];
        [pickerCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        cell = pickerCell;
    }
   
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
#pragma mark - Delegates

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion *)config andIsForBoolValue:(BOOL)isForBoolVal{
    if(!isForBoolVal){
        [self setSelectedStringValueWithConfig:config];
    }
    else{
        [self setBoolSelectedValueWithConfig:config];
    }
}
-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion *)config{
    [self setSelectedStringValueWithConfig:config];
}
-(void) didClickShowDropDownFor:(PSAltFuelDynamicCellsConfiguartion *)config andSender:(id)sender{
    [self.view endEditing:YES];
    _pickerTag = config.cellRowId;
    NSInteger initialIndex = [_pickerArray indexOfObject:config.selectedValue];
    if(initialIndex>[config.dropDownSourceArray count]){
        initialIndex = 0;
    }
    [ActionSheetStringPicker showPickerWithTitle:config.dropDownHeaderTitle
                                            rows:_pickerArray
                                initialSelection:initialIndex
                                          target:self
                                   successAction:@selector(onOptionSelected:element:)
                                    cancelAction:@selector(onOptionPickerCancelled:)
                                          origin:sender];
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
   _vesselProtectionInstalled = [_pickerArray objectAtIndex:[selectedIndex integerValue]];
    NSIndexPath *path = [NSIndexPath indexPathForRow:kSectionExpansionRowVesselProtectionInstalled inSection:kSectionExpansion];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (void) onOptionPickerCancelled:(id)sender {
    CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

#pragma mark - Save heating Inspection
-(void) saveAllData{
    NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
    SolarInspection *boilerInspection;
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
    _inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
    _inspectionId = (isEmpty(_inspectionId) || (_inspectionId == [NSNumber numberWithInt:-1]))?[NSNumber numberWithInt:-1]:_inspectionId;
    if(self.boiler.alternativeHeatingToSolarInspection!=nil){
        boilerInspection = self.boiler.alternativeHeatingToSolarInspection;
    }
    else{
        boilerInspection = [NSEntityDescription insertNewObjectForEntityForName:kSolarInspectionEntity inManagedObjectContext:managedObjectContext];
    }
    
    boilerInspection.isInspected = [NSNumber numberWithBool:YES];
    boilerInspection.inspectedBy = isEmpty(_inspectedBy)?nil:_inspectedBy;
    boilerInspection.inspectionDate = isEmpty(_inspectionDate)?nil:_inspectionDate;
    boilerInspection.inspectionId = isEmpty(_inspectionId)?nil:_inspectionId;
    boilerInspection.antiScaldingControl = [NSNumber numberWithBool:_antiScaldingControl];
    boilerInspection.checkElectricalControl = [NSNumber numberWithBool:_checkElectricalControl];
    boilerInspection.checkElectricalControl = [NSNumber numberWithBool:_checkElectricalControl];
    boilerInspection.antiScaldingControlDetail = isEmpty(self.antiScaldingControlDetail)?nil:self.antiScaldingControlDetail;
    boilerInspection.checkDirection = [NSNumber numberWithBool:_checkDirection];
    boilerInspection.directionDetail = isEmpty(self.directionDetail)?nil:self.directionDetail;
    boilerInspection.electricalControlDetail = isEmpty(self.electricalControlDetail)?nil:self.electricalControlDetail;
    boilerInspection.backPressure = isEmpty(self.backPressure)?nil:self.backPressure;
    boilerInspection.calculationRate = isEmpty(self.calculationRate)?nil:self.calculationRate;
    boilerInspection.deltaOff = isEmpty(self.deltaOff)?nil:self.deltaOff;
    boilerInspection.deltaOn = isEmpty(self.deltaOn)?nil:self.deltaOn;
    boilerInspection.freezingTemp = isEmpty(self.freezingTemp)?nil:self.freezingTemp;
    boilerInspection.minPressure = isEmpty(self.minPressure)?nil:self.minPressure;
    boilerInspection.maxTemperature = isEmpty(self.maxTemperature)?nil:self.maxTemperature;
    
    boilerInspection.systemPressure = isEmpty(self.systemPressure)?nil:self.systemPressure;
    boilerInspection.thermostatTemperature = isEmpty(self.thermostatTemperature)?nil:self.thermostatTemperature;
    boilerInspection.vesselCapacity = isEmpty(self.vesselCapacity)?nil:self.vesselCapacity;
    boilerInspection.vesselProtectionInstalled = isEmpty(self.vesselProtectionInstalled)?nil:self.vesselProtectionInstalled;
    self.boiler.isInspected = [NSNumber numberWithBool:YES];
    
    boilerInspection.solarInspectionToHeating = self.boiler;
    self.boiler.alternativeHeatingToSolarInspection = boilerInspection;
    
    self.appointment.isModified = [NSNumber numberWithBool:YES];
    self.appointment.isSurveyChanged = [NSNumber numberWithBool:YES];
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
}
-(void) saveHeatingInspection{
    [self saveAllData];
    [self popOrCloseViewController];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue identifier] isEqualToString:SEG_ASF_SOLAR_SURVEY_TO_PHOTOS]){
        [self saveAllData];
        PSAFSPhotoGridViewController * dest = (PSAFSPhotoGridViewController*)[segue destinationViewController];
        dest.rootVC = AFSPhotoGridRootSolarInspection;
        dest.appointment = self.appointment;
        dest.altHeating = _boiler;
    }
}



@end
