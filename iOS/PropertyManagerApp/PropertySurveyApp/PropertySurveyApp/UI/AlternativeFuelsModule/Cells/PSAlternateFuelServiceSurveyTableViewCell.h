//
//  PSAlternateFuelServiceSurveyTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 08/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSAlternateFuelServiceSurveyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblItemName;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewStatus;

@end
