//
//  PSAFSPickerViewTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 16/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAFSPickerViewTableViewCell.h"

@implementation PSAFSPickerViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) initializeData{
    if(!isEmpty(_baseConfig)){
        _txtFieldValue.text = !isEmpty(_baseConfig.selectedValue)?_baseConfig.selectedValue:@"";
        _txtFieldValue.userInteractionEnabled = !_baseConfig.isNonMutableField;
        _txtFieldValue.delegate = self;
        _lblTitle.text = _baseConfig.inputFieldTitle;
        switch (_baseConfig.appliedCellFieldType) {
            case PSAFSDynamicCellsFreeTextFieldEmail:
                _txtFieldValue.keyboardType = UIKeyboardTypeEmailAddress;
                break;
            case PSAFSDynamicCellsFreeTextFieldNumeric:
                _txtFieldValue.keyboardType = UIKeyboardTypeNumberPad;
                break;
            case PSAFSDynamicCellsFreeTextFieldTypeText:
                _txtFieldValue.keyboardType = UIKeyboardTypeDefault;
                break;
            case PSAFSDynamicCellsFreeTextFieldTelephone:
                _txtFieldValue.keyboardType = UIKeyboardTypePhonePad;
                break;
            case PSAFSDynamicCellsFreeTextFieldTypeDecimal:
                _txtFieldValue.keyboardType = UIKeyboardTypeDecimalPad;
                break;
            default:
                break;
        }
    }
    
}

-(void) textFieldDidEndEditing:(UITextField *)textField{
    
}
- (IBAction)tappedBtnShowPicker:(id)sender {
    if(!isEmpty(_delegate)){
        if(_baseConfig.baseTypeForConfig==PSAFSDynamicCellsBaseTypeDate){
            [ActionSheetDatePicker showPickerWithTitle:_baseConfig.dropDownHeaderTitle datePickerMode:UIDatePickerModeDate selectedDate:_baseConfig.startingDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
                NSString *val =[UtilityClass stringFromDate:selectedDate dateFormat:kDateTimeStyle8];
                _baseConfig.selectedValue = val;
                _txtFieldValue.text = val;
                if(!isEmpty(_delegate)){
                    if([_delegate respondsToSelector:@selector(didEditHeatingWithDetail:)]){
                        [_delegate didEditHeatingWithDetail:_baseConfig];
                    }
                }
                
                
                
            } cancelBlock:^(ActionSheetDatePicker *picker) {
                
            } origin:sender];
        }
        else{
            if([_delegate respondsToSelector:@selector(didClickShowDropDownFor:andSender:)]){
                [_delegate didClickShowDropDownFor:_baseConfig andSender:sender];
            }
        }
        
    }
}
- (IBAction)tappedBtnClearValue:(id)sender {
    if(!isEmpty(_delegate)){
        if([_delegate respondsToSelector:@selector(didClickClearDataForRow::)]){
            [_delegate didClickClearDataForRow:_baseConfig];
        }
    }
}

@end
