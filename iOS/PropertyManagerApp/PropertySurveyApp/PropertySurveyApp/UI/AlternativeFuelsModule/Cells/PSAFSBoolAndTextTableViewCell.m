//
//  PSAFSBoolAndTextTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 27/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAFSBoolAndTextTableViewCell.h"

@implementation PSAFSBoolAndTextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)switcherDidChangeValue:(id)sender {
    _baseConfig.selectedBoolValue = [_switcher isOn];
    if(!isEmpty(_delegate) && [_delegate respondsToSelector:@selector(didEditHeatingWithDetail:andIsForBoolValue:)]){
        [_delegate didEditHeatingWithDetail:_baseConfig andIsForBoolValue:YES];
    }
}
-(void) initWithConfig{
    _txtViewDetails.delegate = self;
    _txtViewDetails.text = !isEmpty(_baseConfig.selectedValue)?_baseConfig.selectedValue:@"";
    [_switcher setOn:_baseConfig.selectedBoolValue];
    _lblHeading.text = _baseConfig.inputFieldTitle;
    switch (_baseConfig.appliedCellFieldType) {
        case PSAFSDynamicCellsFreeTextFieldEmail:
            _txtViewDetails.keyboardType = UIKeyboardTypeEmailAddress;
            break;
        case PSAFSDynamicCellsFreeTextFieldNumeric:
            _txtViewDetails.keyboardType = UIKeyboardTypeNumberPad;
            break;
        case PSAFSDynamicCellsFreeTextFieldTypeText:
            _txtViewDetails.keyboardType = UIKeyboardTypeDefault;
            break;
        case PSAFSDynamicCellsFreeTextFieldTelephone:
            _txtViewDetails.keyboardType = UIKeyboardTypePhonePad;
            break;
        case PSAFSDynamicCellsFreeTextFieldTypeDecimal:
            _txtViewDetails.keyboardType = UIKeyboardTypeDecimalPad;
            break;
        default:
            break;
    }
}

-(void) textViewDidEndEditing:(UITextView *)textView{
    _baseConfig.selectedValue = textView.text;
    if(!isEmpty(_delegate) && [_delegate respondsToSelector:@selector(didEditHeatingWithDetail:andIsForBoolValue:)]){
        [_delegate didEditHeatingWithDetail:_baseConfig andIsForBoolValue:NO];
    }
}

@end
