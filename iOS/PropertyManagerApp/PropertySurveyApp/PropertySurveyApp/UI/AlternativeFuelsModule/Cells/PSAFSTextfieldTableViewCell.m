//
//  PSAFSTextfieldTableViewCell.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 16/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAFSTextfieldTableViewCell.h"

@implementation PSAFSTextfieldTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
 @property PSAFSDynamicCellsFreeTextFieldType appliedCellFieldType;
 @property PSAFSDynamicCellsBaseType baseTypeForConfig;
 @property NSArray *dropDownSourceArray;
 @property NSString *dropDownHeaderTitle;
 @property NSInteger cellRowId;
 @property NSInteger cellSectionId;
 @property NSString *selectedValue;
 @property NSString *inputFieldTitle;
 @property BOOL isNonMutableField;
 */

-(void) initializeData{
    if(!isEmpty(_baseConfig)){
        _txtFieldValue.text = !isEmpty(_baseConfig.selectedValue)?_baseConfig.selectedValue:@"";
        _txtFieldValue.userInteractionEnabled = !_baseConfig.isNonMutableField;
        _txtFieldValue.delegate = self;
        _lblTitle.text = _baseConfig.inputFieldTitle;
        _btnEdit.hidden = _baseConfig.isNonMutableField;
        switch (_baseConfig.appliedCellFieldType) {
            case PSAFSDynamicCellsFreeTextFieldEmail:
                _txtFieldValue.keyboardType = UIKeyboardTypeEmailAddress;
                break;
            case PSAFSDynamicCellsFreeTextFieldNumeric:
                _txtFieldValue.keyboardType = UIKeyboardTypeNumberPad;
                break;
            case PSAFSDynamicCellsFreeTextFieldTypeText:
                _txtFieldValue.keyboardType = UIKeyboardTypeDefault;
                break;
            case PSAFSDynamicCellsFreeTextFieldTelephone:
                _txtFieldValue.keyboardType = UIKeyboardTypePhonePad;
                break;
            case PSAFSDynamicCellsFreeTextFieldTypeDecimal:
                _txtFieldValue.keyboardType = UIKeyboardTypeDecimalPad;
                break;
            default:
                break;
        }
    }
    
}
- (IBAction)btnEditTapped:(id)sender {
    [_txtFieldValue becomeFirstResponder];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(void) textFieldDidEndEditing:(UITextField *)textField{
    if(!isEmpty(_delegate)){
        if([_delegate respondsToSelector:@selector(didEditHeatingWithDetail:)]){
            _baseConfig.selectedValue = _txtFieldValue.text;
            [_delegate didEditHeatingWithDetail:_baseConfig];
        }
    }
}

@end
