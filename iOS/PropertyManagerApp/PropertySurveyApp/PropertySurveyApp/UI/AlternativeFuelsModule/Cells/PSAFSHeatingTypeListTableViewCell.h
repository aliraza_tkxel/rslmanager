//
//  PSAFSHeatingTypeListTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 09/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSAFSHeatingTypeListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblBoilerNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBoilerNameDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblBoilerTypeDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblBoilerTypeTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnInspectHeating;

@end
