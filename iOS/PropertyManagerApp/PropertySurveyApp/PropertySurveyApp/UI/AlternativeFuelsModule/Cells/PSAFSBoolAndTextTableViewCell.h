//
//  PSAFSBoolAndTextTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 27/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZTextView.h"
@interface PSAFSBoolAndTextTableViewCell : UITableViewCell<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblHeading;
@property (weak, nonatomic) IBOutlet UISwitch *switcher;
@property (weak, nonatomic) IBOutlet SZTextView *txtViewDetails;
-(void) initWithConfig;
@property (weak, nonatomic) id<PSAltFuelServicingEditDelegate> delegate;
@property PSAltFuelDynamicCellsConfiguartion *baseConfig;
@end
