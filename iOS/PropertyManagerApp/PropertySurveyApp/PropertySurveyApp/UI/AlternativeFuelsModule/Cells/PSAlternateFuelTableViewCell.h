//
//  PSAlternateFuelTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 05/06/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAppointmentsViewController.h"
@interface PSAlternateFuelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (strong, nonatomic) IBOutlet UILabel *lblAppointmentTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblAppointmentStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblAppointmentDate;
@property (strong, nonatomic) IBOutlet UILabel *lblAppointmentTime;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblArrangedBy;
@property (strong, nonatomic) IBOutlet UILabel *lblCertificateExpiry;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppointmentType;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppointmentStatus;
@property (strong, nonatomic) IBOutlet UIImageView *imgAppointmentOfflineStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPropertyNotesIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnMapView;
@property (weak, nonatomic) IBOutlet UIButton *btnFailedReason;
@property (strong, nonatomic) IBOutlet UIImageView *iconAddress;
- (IBAction)viewFailedReason:(id)sender;

@property (weak,   nonatomic) Appointment *appointment;
@property (weak,    nonatomic) id<PSAppointmentsAddressMapsProtocol> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblJSGas;



#pragma mark Download Progress View
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *progressTxtLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *downloadProgressBarView;

- (void) updateProgess:(CGFloat)progress;
- (void)showProgressView:(BOOL)show animated:(BOOL)animated;
- (void) animationTest;
-(void) reloadData:(Appointment*)appointment;

#pragma mark appalert icons
@property (nonatomic,strong) IBOutlet UIImageView * iconCustomerNotes;
@property (nonatomic,strong) IBOutlet UIImageView * iconAsbestos;
@property (nonatomic,strong) IBOutlet UIImageView * iconRiskValunarability;
@end
