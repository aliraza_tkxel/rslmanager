//
//  PSAFSPickerViewTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 16/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PSAFSPickerViewTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldValue;
@property (weak, nonatomic) IBOutlet UIButton *btnShowPicker;
@property (weak, nonatomic) IBOutlet UIButton *btnClearValue;
@property (weak, nonatomic) id<PSAltFuelServicingEditDelegate> delegate;
@property PSAltFuelDynamicCellsConfiguartion *baseConfig;
-(void) initializeData;
@end
