//
//  PSAFSTextfieldTableViewCell.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 16/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSAFSTextfieldTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldValue;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) id<PSAltFuelServicingEditDelegate> delegate;
@property PSAltFuelDynamicCellsConfiguartion *baseConfig;
-(void) initializeData;
@end
