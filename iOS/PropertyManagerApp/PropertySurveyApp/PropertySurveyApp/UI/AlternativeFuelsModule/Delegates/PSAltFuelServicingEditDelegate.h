//
//  PSEditBoilerDelegate.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 11/3/16.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSAltFuelDynamicCellsConfiguartion.h"
@protocol PSAltFuelServicingEditDelegate <NSObject>
@optional
-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion*)config;
-(void) didClickClearDataForRow:(PSAltFuelDynamicCellsConfiguartion*) config;
-(void) didClickShowDropDownFor:(PSAltFuelDynamicCellsConfiguartion*) config andSender:(id) sender;
-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion*)config andIsForBoolValue:(BOOL) isForBoolVal;
@end
