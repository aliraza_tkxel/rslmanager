//
//  PSAltFuelDynamicCellsConfiguartion.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 16/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, PSAFSDynamicCellsBaseType) {
    PSAFSDynamicCellsBaseTypeDropDown,
    PSAFSDynamicCellsBaseTypeDate,
    PSAFSDynamicCellsBaseTypeFreeText,
    PSAFSDynamicCellsBaseTypeTime,
    PSAFSDynamicCellsBaseTypeBoolAndText,
    PSAFSDynamicCellsBaseTypeTextView,
};

typedef NS_ENUM(NSInteger, PSAFSDynamicCellsFreeTextFieldType) {
    PSAFSDynamicCellsFreeTextFieldTypeText,
    PSAFSDynamicCellsFreeTextFieldTelephone,
    PSAFSDynamicCellsFreeTextFieldEmail,
    PSAFSDynamicCellsFreeTextFieldNumeric,
    PSAFSDynamicCellsFreeTextFieldTypeDecimal
};


@interface PSAltFuelDynamicCellsConfiguartion : NSObject
@property PSAFSDynamicCellsFreeTextFieldType appliedCellFieldType;
@property PSAFSDynamicCellsBaseType baseTypeForConfig;
@property NSArray *dropDownSourceArray;
@property NSString *dropDownHeaderTitle;
@property NSInteger cellRowId;
@property NSInteger cellSectionId;
@property NSString *selectedValue;
@property NSString *inputFieldTitle;
@property BOOL isNonMutableField;
@property BOOL selectedBoolValue;
@property NSDate * startingDate;

@end
