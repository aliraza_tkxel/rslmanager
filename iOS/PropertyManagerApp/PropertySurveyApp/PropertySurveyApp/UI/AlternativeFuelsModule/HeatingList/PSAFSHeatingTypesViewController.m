//
//  PSAFSHeatingTypesViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSAFSHeatingTypesViewController.h"
#import "PSAFSHeatingTypeListTableViewCell.h"
#import "PSMVHRDetailsViewController.h"
#import "PSAFSSolarDetailsViewController.h"
#import "PSAirSourceDetailsViewController.h"
#import "PSGenericAFDetailsViewController.h"
#import "PSAirSourceSurveyViewController.h"
#import "PSMVHRSurveyViewController.h"
#import "PSAFSSolarSurveyViewController.h"

#define kBoilerDetailCellHeight 99
@interface PSAFSHeatingTypesViewController ()

@end

@implementation PSAFSHeatingTypesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    [self registerNibsForTableView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    _heatings = [NSArray array];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kHeatingName
                                                 ascending:YES];
    NSArray *sortedArray = [[self.appointment.appointmentToProperty.propertyToAlternativeHeating allObjects] sortedArrayUsingDescriptors:@[sortDescriptor]];
    _heatings = [[NSArray alloc] initWithArray:sortedArray] ;
    [self.tableView reloadData];
}



#pragma mark - Init Methods
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
     [self setTitle:@"Heating Types"];
    
}
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

-(void)registerNibsForTableView
{
    NSString * nibName = NSStringFromClass([PSAFSHeatingTypeListTableViewCell class]);
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
    if (_noResultsFound)
    {
        rowHeight = 40;
    }
    else
    {
        rowHeight = kBoilerDetailCellHeight;
    }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    
    if (!isEmpty(_heatings))
    {
        rows = [_heatings count];
        _noResultsFound = NO;
    }
    else
    {
        rows = 1;
        _noResultsFound = YES;
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if(_noResultsFound)
    {
        static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
        UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
        if(_cell == nil)
        {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
        }
        _cell.textLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
        _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
        _cell.textLabel.text = LOC(@"KEY_STRING_NO_BOILER");
        _cell.textLabel.textAlignment = PSTextAlignmentCenter;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        cell = _cell;
    }
    
    else
    {
        PSAFSHeatingTypeListTableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSHeatingTypeListTableViewCell class])];
        AlternativeHeating *blr = [_heatings objectAtIndex:indexPath.row];
        _cell.lblBoilerNameDesc.text = blr.heatingName;
        _cell.lblBoilerTypeDesc.text = blr.heatingFuel;
        _cell.btnInspectHeating.tag = indexPath.row;
        [_cell.lblBoilerNameTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblBoilerTypeTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblBoilerNameDesc setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblBoilerTypeDesc setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.btnInspectHeating addTarget:self action:@selector(onClickHeatingBtnInspect:) forControlEvents:UIControlEventTouchUpInside];
        
        if([blr.heatingFuel isEqualToString:kAFSFuelAirSource] ||[blr.heatingFuel isEqualToString:kAFSFuelSolar]||[blr.heatingFuel isEqualToString:kAFSFuelMVHR]){
            _cell.btnInspectHeating.hidden = NO;
        }
        else{
            _cell.btnInspectHeating.hidden = YES;
        }
        if(!isEmpty(blr.isInspected)){
            if([blr.isInspected boolValue]){
                [_cell.btnInspectHeating setBackgroundImage:[UIImage imageNamed:@"btn_Complete.png"] forState:UIControlStateNormal];
            }
            else{
                 [_cell.btnInspectHeating setBackgroundImage:[UIImage imageNamed:@"checklistIcon.png"] forState:UIControlStateNormal];
            }
        }
        
        
        cell = _cell;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedBoiler = [_heatings objectAtIndex:indexPath.row];
    if([_selectedBoiler.heatingFuel isEqualToString:kAFSFuelAirSource]){
        [self performSegueWithIdentifier:SEG_ASF_LIST_TO_AIRSOURCE_DETAILS sender:self];
    }
    else if([_selectedBoiler.heatingFuel isEqualToString:kAFSFuelMVHR]){
        [self performSegueWithIdentifier:SEG_ASF_LIST_TO_MVHR_DETAILS sender:self];
    }
    else if([_selectedBoiler.heatingFuel isEqualToString:kAFSFuelSolar]){
        [self performSegueWithIdentifier:SEG_ASF_LIST_TO_SOLAR_DETAILS sender:self];
    }
    else{
        [self performSegueWithIdentifier:SEG_ASF_LIST_TO_GENERIC_DETAILS sender:self];
    }
}

-(IBAction)onClickHeatingBtnInspect:(id)sender{
    CLS_LOG(@"onClickBoilerBtnInspect");
    UIButton *btn = (UIButton *) sender;
    _selectedBoiler = [_heatings objectAtIndex:btn.tag];
    if([_selectedBoiler.heatingFuel isEqualToString:kAFSFuelAirSource]){
        [self performSegueWithIdentifier:SEG_ASF_LIST_TO_AIRSOURCE_SURVEY sender:self];
    }
    else if([_selectedBoiler.heatingFuel isEqualToString:kAFSFuelMVHR]){
        [self performSegueWithIdentifier:SEG_ASF_LIST_TO_MVHR_SURVEY sender:self];
    }
    else if([_selectedBoiler.heatingFuel isEqualToString:kAFSFuelSolar]){
        [self performSegueWithIdentifier:SEG_ASF_LIST_TO_SOLAR_SURVEY sender:self];
    }
    else{
        [self showMessageWithHeader:@"Alert" andBody:@"This heating cannot be inspected."];
    }
}


#pragma mark - Navigation

//In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue destinationViewController] isKindOfClass:[PSAirSourceDetailsViewController class]]){
        PSAirSourceDetailsViewController *dest = [segue destinationViewController];
        dest.boiler = _selectedBoiler;
    }
    else if([[segue destinationViewController] isKindOfClass:[PSAFSSolarDetailsViewController class]]){
        PSAFSSolarDetailsViewController *dest = [segue destinationViewController];
        dest.boiler = _selectedBoiler;
    }
    else if([[segue destinationViewController] isKindOfClass:[PSMVHRDetailsViewController class]]){
        PSMVHRDetailsViewController *dest = [segue destinationViewController];
        dest.boiler = _selectedBoiler;
    }
    else if([[segue destinationViewController] isKindOfClass:[PSGenericAFDetailsViewController class]]){
        PSGenericAFDetailsViewController *dest = [segue destinationViewController];
        dest.boiler = _selectedBoiler;
    }
    else if([[segue destinationViewController] isKindOfClass:[PSAirSourceSurveyViewController class]]){
        PSAirSourceSurveyViewController *dest = [segue destinationViewController];
        dest.boiler = _selectedBoiler;
        dest.appointment = self.appointment;
    }
    else if([[segue destinationViewController] isKindOfClass:[PSMVHRSurveyViewController class]]){
        PSMVHRSurveyViewController *dest = [segue destinationViewController];
        dest.boiler = _selectedBoiler;
        dest.appointment = self.appointment;
    }
    else if([[segue destinationViewController] isKindOfClass:[PSAFSSolarSurveyViewController class]]){
        PSAFSSolarSurveyViewController *dest = [segue destinationViewController];
        dest.boiler = _selectedBoiler;
        dest.appointment = self.appointment;
    }
}


@end
