//
//  PSAFSHeatingTypesViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"

@interface PSAFSHeatingTypesViewController : PSCustomViewController
@property (strong,   nonatomic) Appointment *appointment;
@property NSArray *heatings;
@property BOOL noResultsFound;
@property AlternativeHeating *selectedBoiler;
@end
