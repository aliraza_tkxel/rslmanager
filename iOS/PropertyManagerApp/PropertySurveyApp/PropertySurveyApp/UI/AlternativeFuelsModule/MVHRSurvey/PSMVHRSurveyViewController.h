//
//  PSMVHRSurveyViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSAFSBoolAndTextTableViewCell.h"
#import "PSCameraViewController.h"
#import "PSAFSPhotoGridViewController.h"
@interface PSMVHRSurveyViewController : PSCustomViewController<UITableViewDelegate,UITableViewDataSource,PSAltFuelServicingEditDelegate>
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tblView;
@property (strong, nonatomic) AlternativeHeating *boiler;
@property (strong, nonatomic) PSBarButtonItem *btnSave;
@property(nonatomic,strong) PSBarButtonItem * cameraButton;
@property (strong,nonatomic) Appointment *appointment;

@property (strong, nonatomic) NSString *airFlowDetail;
@property (nonatomic) BOOL checkAirFlow;
@property (nonatomic) BOOL checkFilters;
@property (strong, nonatomic) NSString *ductingDetail;
@property (nonatomic) BOOL ductingInspection;
@property (strong, nonatomic) NSString *filterDetail;
@property (strong, nonatomic) NSNumber *inspectedBy;
@property (strong, nonatomic) NSDate *inspectionDate;
@property (strong, nonatomic) NSNumber *inspectionId;
@property (nonatomic) BOOL isInspected;

@end
