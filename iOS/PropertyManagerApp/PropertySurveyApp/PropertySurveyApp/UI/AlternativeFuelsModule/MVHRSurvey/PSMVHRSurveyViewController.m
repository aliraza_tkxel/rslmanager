//
//  PSMVHRSurveyViewController.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 07/08/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import "PSMVHRSurveyViewController.h"


#define kSectionMVHRInspectionDetails    0

#define kRowCheckAirflow    0
#define kRowDuctingInspection   1
#define kRowCheckFilters    2

@interface PSMVHRSurveyViewController ()

@end

@implementation PSMVHRSurveyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadNavigationonBarItems];
    [self registerCells];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [self refreshPhotosCount];
}

#pragma mark - Init methods

-(void) initData{
    [self populateDefaultValues];
    _tblView.delegate = self;
    _tblView.dataSource = self;
    _tblView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    _tblView.rowHeight = UITableViewAutomaticDimension;
    _tblView.estimatedRowHeight = 44.0;
    _tblView.allowsSelection = NO;
    [_tblView reloadData];
}
-(void) refreshPhotosCount{
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    if(!isEmpty(self.cameraButton)){
        [self.cameraButton reloadBadge:badgeText];
    }
    
}
-(void) registerCells{
    [_tblView registerNib:[UINib nibWithNibName:@"PSAFSBoolAndTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"PSAFSBoolAndTextTableViewCell"];
}

- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(btnBackTapped)];
    
    self.btnSave = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                          style:PSBarButtonItemStyleDefault
                                                         target:self
                                                         action:@selector(btnSaveTapped)];
    
    
    NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
    
    self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                               style:PSBarButtonItemStyleCamera
                                                              target:self
                                                              action:@selector(onClickCameraButton)bagde:badgeText];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.cameraButton,self.btnSave, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"MVHR"];
}

-(void) populateDefaultValues{
    if(!isEmpty(_boiler.alternativeHeatingToMVHRInspection)){
        
        _checkFilters = !isEmpty(_boiler.alternativeHeatingToMVHRInspection.checkFilters)?[_boiler.alternativeHeatingToMVHRInspection.checkFilters boolValue]:NO;
        _checkAirFlow = !isEmpty(_boiler.alternativeHeatingToMVHRInspection.checkAirFlow)?[_boiler.alternativeHeatingToMVHRInspection.checkAirFlow boolValue]:NO;
        _ductingInspection = !isEmpty(_boiler.alternativeHeatingToMVHRInspection.ductingInspection)?[_boiler.alternativeHeatingToMVHRInspection.ductingInspection boolValue]:NO;
        _airFlowDetail = !isEmpty(_boiler.alternativeHeatingToMVHRInspection.airFlowDetail)?_boiler.alternativeHeatingToMVHRInspection.airFlowDetail:@"";
        _ductingDetail = !isEmpty(_boiler.alternativeHeatingToMVHRInspection.ductingDetail)?_boiler.alternativeHeatingToMVHRInspection.ductingDetail:@"";
         _filterDetail = !isEmpty(_boiler.alternativeHeatingToMVHRInspection.filterDetail)?_boiler.alternativeHeatingToMVHRInspection.filterDetail:@"";
    }
    else{
        _checkFilters = NO;
        _checkAirFlow = NO;
        _ductingInspection = NO;
        _isInspected = NO;
        _inspectionDate = [NSDate date];
        _airFlowDetail = @"";
        _filterDetail = @"";
        _ductingDetail = @"";
    }
}


#pragma mark - Actions

- (void) btnBackTapped
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to quit updating? Your changes will not be saved if you exit now." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        CLS_LOG(@"onClickBackButton");
        [self popOrCloseViewController];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
    }];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}

-(void) btnSaveTapped{
    [self.view endEditing:YES];
    if([self isValidData]==YES){
        CLS_LOG(@"Save boiler tapped");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirm Action" message:@"Are you sure you want to update this heating type?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self saveHeatingInspection];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
        }];
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated: YES completion: nil];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please fill all the mandatory fields"];
    }
    
    
    
}


- (void)onClickCameraButton {
    [self.view endEditing:YES];
    CLS_LOG(@"Camera Button");
    if([self isValidData]){
        [self performSegueWithIdentifier:SEG_ASF_MVHR_SURVEY_TO_PHOTOS sender:self];
    }
    else{
        [self showMessageWithHeader:@"Error" andBody:@"Please check/fill all the mandatory fields"];
    }
    
    
    
}

#pragma mark - Utility

-(NSUInteger) getPhotographsCount
{
    NSArray * photographsList =[_boiler.alternativeHeatingToMVHRInspection.mvhrInspectionToPictures allObjects];
    return !isEmpty(photographsList)?[photographsList count]:0;
}


-(BOOL) isValidData{
    if(!isEmpty(_airFlowDetail) && !isEmpty(_ductingDetail) && !isEmpty(_filterDetail)){
        return YES;
    }
    return NO;
}

-(NSString *) getHeaderForSection:(NSInteger) section{
    NSString *header = @"";
    switch (section) {
        case kSectionMVHRInspectionDetails:
            header = @"MVHR INSPECTION DETAILS";
            break;
        default:
            break;
    }
    return header;
}

-(NSString *) getCellLabelForSection:(NSInteger) section andRow:(NSInteger) row{
    NSString *label = @"";
    switch (row) {
        case kRowCheckAirflow:
            label = @"Airflow rates correct";
            break;
        case kRowDuctingInspection:
            label = @"Ducting inspection";
            break;
        case kRowCheckFilters:
            label = @"Filters replaced";
            break;
        default:
            break;
    }
    return label;
}

- (void)addBottomBorderTo:(UIView*)parentView WithColor:(UIColor *)color andWidth:(CGFloat) borderWidth {
    UIView *border = [UIView new];
    border.backgroundColor = color;
    [border setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
    border.frame = CGRectMake(0, parentView.frame.size.height - borderWidth, parentView.frame.size.width, borderWidth);
    [parentView addSubview:border];
}

#pragma mark - Getters and Setters

-(NSString *) getSelectedStringValueForSection:(NSInteger) section andRow:(NSInteger) row{
    NSString *label = @"";
    switch (row) {
        case kRowCheckAirflow:
            label = _airFlowDetail;
            break;
        case kRowDuctingInspection:
            label = _ductingDetail;
            break;
        case kRowCheckFilters:
            label = _filterDetail;
            break;
        default:
            break;
    }
    return label;
}

-(void) setSelectedStringValueWithConfig:(PSAltFuelDynamicCellsConfiguartion*)config{
    NSString *label = config.selectedValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    switch (row) {
        case kRowCheckAirflow:
            _airFlowDetail =label;
            break;
        case kRowDuctingInspection:
            _ductingDetail = label;
            break;
        case kRowCheckFilters:
            _filterDetail = label;
            break;
        default:
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}


-(BOOL) getBoolSelectedValueForSection:(NSInteger) section andRow:(NSInteger) row{
    BOOL label = NO;
    switch (row) {
        case kRowCheckAirflow:
            label = _checkAirFlow;
            break;
        case kRowDuctingInspection:
            label = _ductingInspection;
            break;
        case kRowCheckFilters:
            label = _checkFilters;
            break;
        default:
            break;
    }
    return label;
}

-(void) setBoolSelectedValueWithConfig:(PSAltFuelDynamicCellsConfiguartion*)config{
    BOOL label = config.selectedBoolValue;
    NSInteger section = config.cellSectionId;
    NSInteger row = config.cellRowId;
    switch (row) {
        case kRowCheckAirflow:
            _checkAirFlow = label;
            break;
        case kRowDuctingInspection:
            _ductingInspection = label;
            break;
        case kRowCheckFilters:
            _checkFilters = label;
            break;
        default:
            break;
    }
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    [_tblView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
}

-(PSAltFuelDynamicCellsConfiguartion*) getCellConfigurationForRow:(NSInteger) row andSection:(NSInteger) section{
    PSAltFuelDynamicCellsConfiguartion * config = [[PSAltFuelDynamicCellsConfiguartion alloc] init];
    config.cellRowId = row;
    config.cellSectionId = section;
    config.inputFieldTitle = [self getCellLabelForSection:section andRow:row];
    config.selectedBoolValue = [self getBoolSelectedValueForSection:section andRow:row];
    config.selectedValue = [self getSelectedStringValueForSection:section andRow:row];
    config.isNonMutableField = NO;
    config.baseTypeForConfig = PSAFSDynamicCellsBaseTypeBoolAndText;
    config.appliedCellFieldType = PSAFSDynamicCellsBaseTypeFreeText;
    return config;
}

#pragma mark - UITableViewDelegates

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 40)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14]];
    [label setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    
    label.text = [self getHeaderForSection:section];
    label.textAlignment = NSTextAlignmentLeft;
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    [self addBottomBorderTo:view WithColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR) andWidth:2.0f];
    return view;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    PSAltFuelDynamicCellsConfiguartion *config = [self getCellConfigurationForRow:indexPath.row andSection:indexPath.section];
    PSAFSBoolAndTextTableViewCell *manualCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAFSBoolAndTextTableViewCell class])];
    manualCell.baseConfig = config;
    manualCell.delegate = self;
    [manualCell initWithConfig];
    [manualCell setNeedsUpdateConstraints];
    [manualCell updateConstraintsIfNeeded];
    [manualCell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    cell = manualCell;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

#pragma mark - Delegates

-(void) didEditHeatingWithDetail:(PSAltFuelDynamicCellsConfiguartion *)config andIsForBoolValue:(BOOL)isForBoolVal{
    if(!isForBoolVal){
        [self setSelectedStringValueWithConfig:config];
    }
    else{
        [self setBoolSelectedValueWithConfig:config];
    }
}


#pragma mark - Save heating Inspection
-(void) saveAllData{
    NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
    MVHRInspection *boilerInspection;
    NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    _inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
    _inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
    _inspectionId = (isEmpty(_inspectionId) || (_inspectionId == [NSNumber numberWithInt:-1]))?[NSNumber numberWithInt:-1]:_inspectionId;
    if(self.boiler.alternativeHeatingToMVHRInspection!=nil){
        boilerInspection = self.boiler.alternativeHeatingToMVHRInspection;
    }
    else{
        boilerInspection = [NSEntityDescription insertNewObjectForEntityForName:kMVHRInspectionEntity inManagedObjectContext:managedObjectContext];
    }
    
    boilerInspection.isInspected = [NSNumber numberWithBool:YES];
    boilerInspection.inspectedBy = isEmpty(_inspectedBy)?nil:_inspectedBy;
    boilerInspection.inspectionDate = isEmpty(_inspectionDate)?nil:_inspectionDate;
    boilerInspection.inspectionId = isEmpty(_inspectionId)?nil:_inspectionId;
    boilerInspection.checkAirFlow = [NSNumber numberWithBool:_checkAirFlow];
    boilerInspection.checkFilters = [NSNumber numberWithBool:_checkFilters];
    boilerInspection.ductingInspection = [NSNumber numberWithBool:_ductingInspection];
    
    boilerInspection.ductingDetail = isEmpty(_ductingDetail)?nil:_ductingDetail;
    boilerInspection.airFlowDetail = isEmpty(_airFlowDetail)?nil:_airFlowDetail;
    boilerInspection.filterDetail = isEmpty(_filterDetail)?nil:_filterDetail;
    
    self.boiler.isInspected = [NSNumber numberWithBool:YES];
    boilerInspection.mvhrInspectionToHeating = self.boiler;
    self.boiler.alternativeHeatingToMVHRInspection = boilerInspection;
    
    self.appointment.isModified = [NSNumber numberWithBool:YES];
    self.appointment.isSurveyChanged = [NSNumber numberWithBool:YES];
    [[PSDataPersistenceManager sharedManager] saveDataInDB:managedObjectContext];
}
-(void) saveHeatingInspection{
    [self saveAllData];
    [self popOrCloseViewController];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue identifier] isEqualToString:SEG_ASF_MVHR_SURVEY_TO_PHOTOS]){
        [self saveAllData];
        PSAFSPhotoGridViewController * dest = (PSAFSPhotoGridViewController*)[segue destinationViewController];
        dest.rootVC = AFSPhotoGridRootMVHRInspection;
        dest.appointment = self.appointment;
        dest.altHeating = _boiler;
    }
}

@end
