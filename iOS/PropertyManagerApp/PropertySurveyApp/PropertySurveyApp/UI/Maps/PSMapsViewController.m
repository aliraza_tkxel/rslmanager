//
//  PSMapsViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 29/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSMapsViewController.h"
#import "PSAnnotation.h"
#import "PSBarButtonItem.h"

@interface PSMapsViewController ()

@end

@implementation PSMapsViewController
@synthesize addressMap,address,navigationTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    [self fetchAddressCoordinates];
    [self setMapView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];

    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    [self setTitle:self.navigationTitle];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

#pragma mark - MKAnnotationView Delegate Methods
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
	
	// if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
	// try to dequeue an existing pin view first
	static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
	MKPinAnnotationView* pinView = [[MKPinAnnotationView alloc]
									 initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
	pinView.animatesDrop=YES;
	pinView.canShowCallout=YES;
	pinView.pinColor=MKPinAnnotationColorPurple;
    //	UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    //	[rightButton setTitle:annotation.title forState:UIControlStateNormal];
    //	[rightButton addTarget:self
    //					action:@selector(showDetails:)
    //		  forControlEvents:UIControlEventTouchUpInside];
    //	pinView.rightCalloutAccessoryView = rightButton;
    //
    //	UIImageView *profileIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profile.png"]];
    //	pinView.leftCalloutAccessoryView = profileIconView;
    //	[profileIconView release];
	
	return pinView;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Methods
-(void)setMapView
{
    CLLocationCoordinate2D zoomLocation;
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * latNum = [numberFormatter numberFromString:lat];
    NSNumber * lngNum = [numberFormatter numberFromString:lng];
    
    double zoomX = [latNum doubleValue];
    double zoomY = [lngNum doubleValue];
    
    zoomLocation.latitude = zoomX;
    zoomLocation.longitude= zoomY;
    addressMap.delegate=self;
   
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
  
    MKCoordinateRegion adjustedRegion = [addressMap regionThatFits:viewRegion];
 
    [addressMap setRegion:adjustedRegion animated:YES];
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = zoomLocation;
    annotationPoint.title = @"Location";
    annotationPoint.subtitle = address;
    [addressMap addAnnotation:annotationPoint];
    
}

- (void) fetchAddressCoordinates
{
    NSString *Req = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false",address];
    //NSString *Req = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false",address];
    //CLS_LOG(@"Formated address %@",Req);
    NSString *formattedReq = [Req stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    formattedReq = [formattedReq stringByReplacingOccurrencesOfString:@"\n" withString:@"%20"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:formattedReq]];
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    // Get JSON as a NSString from NSData response
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    NSArray* statuses = [(NSDictionary*)[json_string JSONValue] objectForKey:@"results"];
   
    if(isEmpty(statuses))
    {
        lat = @"52.6283";
        lng = @"1.2967";
        
    }
    else
    {
        NSDictionary* lat_lng  = [statuses objectAtIndex:0];
        NSArray *latitudeArray = (NSArray *)[[[lat_lng objectForKey:@"geometry"] objectForKey:@"location"]
                                             objectForKey:@"lat"];
        NSArray *longitudeArray = (NSArray *)[[[lat_lng objectForKey:@"geometry"] objectForKey:@"location"]
                                              objectForKey:@"lng"];
        lat = [NSString stringWithFormat:@"%@",latitudeArray];
        lng = [NSString stringWithFormat:@"%@",longitudeArray];
    }
}

@end
