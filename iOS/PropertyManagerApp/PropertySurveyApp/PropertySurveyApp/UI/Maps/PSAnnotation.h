//
//  PSAnnotation.h
//  PropertySurveyApp
//
//  Created by Yawar on 29/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface PSAnnotation : NSObject<MKAnnotation>
{
	CLLocationCoordinate2D	coordinate;
	NSString*				title;
	NSString*				subtitle;
}

@property (nonatomic, assign)               CLLocationCoordinate2D	coordinate;
@property (nonatomic, readonly, copy)		NSString*				navigationTitle;
@property (nonatomic, readonly, copy)		NSString*				subtitle;

@end
