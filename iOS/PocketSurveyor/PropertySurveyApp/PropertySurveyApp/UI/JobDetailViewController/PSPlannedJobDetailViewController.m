//
//  PSPlannedJobDetailViewController.m
//  PropertySurveyApp
//
//  Created by TkXel on 01/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSPlannedJobDetailViewController.h"

#import "PSFaultDetailsCell.h"
#import "PSFaultNotesCell.h"
#import "PSJobDetailAsbestosCell.h"
#import "PSPriorityCell.h"
#import "PSDurationCell.h"
#import "PSResponseTimeCell.h"
#import "PlannedTradeComponent+Methods.h"
#import "PSJobStatusViewController.h"

//CR by client PMM-621 Fixed. Mohsin Mahmood
//Hiding Asbestos .. Just change the kNumberofsections to 6 will show asbestos
#define kNumberOfSections 5
#define kSectionFaultDetails 0
#define kSectionFaultNotes 1
#define kSectionPriority 2
#define kSectionDuration 3
#define kSectionAsbestos 5
#define kSectionResponseTime 4

#define kSectionHeaderHeight 13

#define kSectionFaultDetailsRows 1
#define kSectionFaultNotesRows 1
#define kSectionPriorityRows 1
#define kSectionDurationRows 1
#define kSectionResponseTimeRows 1

#define kFaultDetailsCellHeight 56
#define kFaultNotesCellHeight 33
#define kPriorityCellHeight 36
#define kDurationCellHeight 34
#define kAsbestosCellHeight 42
#define kResponseTimeCellHeight 32

@interface PSPlannedJobDetailViewController ()

@end

@implementation PSPlannedJobDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) loadView
{
    [super loadView];
    CGRect frame= [[UIScreen mainScreen] bounds];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, frame.size.height-CGRectGetMaxY(self.navigationController.navigationBar.frame)) style:(UITableViewStylePlain)];
    
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    [self.view addSubview:self.tableView];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.headerViewArray = [NSMutableArray array];
    
    //Condition is pasted here becasuse of different Sections Titles with respect to AppointmentType
    
    /*
     CR by client PMM-621 Fixed. Mohsin Mahmood
     Hiding Asbestos.. UnComment portion of lines in load Section statement to Show Asbestos Title Sections
     */
    if([self.plannedComponent.tradeComponentToAppointment getType] == AppointmentTypeMiscellaneous) {
        [self loadSectionHeaderViews:@[LOC(@"KEY_STRING_PLANNED_DETAILS"),LOC(@"KEY_STRING_WORKS_REQUIRED"),LOC(@"KEY_STRING_PRIORITY"),LOC(@"KEY_STRING_DURATION"),/*LOC(@"KEY_STRING_ASBESTOS"),*/ LOC(@"KEY_STRING_RESPONSE_TIME")]
                         headerViews:self.headerViewArray];

    }
    else {
        [self loadSectionHeaderViews:@[LOC(@"KEY_STRING_PLANNED_DETAILS"),LOC(@"KEY_STRING_PLANNED_NOTES"),LOC(@"KEY_STRING_PRIORITY"),LOC(@"KEY_STRING_DURATION"),/*LOC(@"KEY_STRING_ASBESTOS"),*/ LOC(@"KEY_STRING_RESPONSE_TIME")]
                         headerViews:self.headerViewArray];

    }
    
    [self addTableHeaderView];
    [self loadNavigationonBarItems];
    self.tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    // Do any additional setup after loading the view from its nib.
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(!isEmpty(self.plannedComponent) && ![self.plannedComponent.jobStatus isEqualToString:kJobStatusNotStarted]) {
        [self hideTableHeaderView];
    }

    
    if (!isEmpty(self.plannedComponent) && ![self.plannedComponent.jobStatus isEqualToString:kJobStatusComplete] && ![self.plannedComponent.jobStatus isEqualToString:kJobStatusNoEntry])
    {
        if ([self.plannedComponent.jobStatus isEqualToString:kJobStatusPaused])
        {
            [self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
        }
        else if ([self.plannedComponent.jobStatus isEqualToString:kJobStatusInProgress] || [self.plannedComponent.jobStatus isEqualToString:@"In Progress"])
        {
            [self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
        }
    }
    else
    {
        
        [self.pauseButton setEnabled:NO];
        [self.resumeButton setEnabled:NO];
        [self.completeButton setEnabled:NO];

    }
    
}

-(void) loadNavigationonBarItems
{
    [super loadNavigationonBarItems];
    
    if (![self.plannedComponent.jobStatus isEqualToString:kJobStatusComplete] || ![self.plannedComponent.jobStatus isEqualToString:kJobStatusNoEntry])
    {
        if ([self.plannedComponent.jobStatus isEqualToString:kJobStatusPaused])
        {
            [self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
        }
        else if ([self.plannedComponent.jobStatus isEqualToString:kJobStatusInProgress])
        {
            [self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
        }
    }
    
    NSString *title = [NSString stringWithFormat:@"Job %@",self.plannedComponent.jsNumber];
    
    [self setTitle:title];
    
}


- (void) addTableHeaderView
{
    CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
    UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
    headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.frame = CGRectMake(8, 0, 245, 29);
    lbl.center = CGPointMake(lbl.center.x, headerView.center.y);
    lbl.backgroundColor = [UIColor clearColor];
    [lbl setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lbl setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lbl setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
    lbl.text =LOC(@"KEY_STRING_JOB_START_MESSAGE");
    
    UIButton *btnStartAppointment = [[UIButton alloc]init];
    btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
    btnStartAppointment.center = CGPointMake(btnStartAppointment.center.x, headerView.center.y);
    [btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
    [btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
    [btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:
     UIControlStateNormal];
    [btnStartAppointment addTarget:self action:@selector(onClickStartJobButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:lbl];
    [headerView addSubview:btnStartAppointment];

    
    if ([self.plannedComponent.jobStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame)
    {
        headerViewRect = self.tableView.tableHeaderView.frame;
        headerViewRect.size.height = 40;
        headerViewRect.size.width = 320;
        [self.tableView.tableHeaderView setFrame:headerViewRect];
        [self.tableView setTableHeaderView:headerView];
    }
}

- (void) hideTableHeaderView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.5];
    [self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
    self.tableView.tableHeaderView = nil;
    [self.tableView.tableHeaderView setHidden:YES];
    [UIView commitAnimations];
    [self loadNavigationonBarItems];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return kNumberOfSections;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *_cell = nil;
    
    if (indexPath.section == kSectionFaultDetails){
        static NSString *faultDetailsCellIdentifier = @"FaultDetailsCellIdentifier";
        PSFaultDetailsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:faultDetailsCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSFaultDetailsCell" owner:self options:nil];
            cell = self.faultDetailsCell;
            self.faultDetailsCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    else if (indexPath.section == kSectionFaultNotes){
        static NSString *faultNotesCellIdentifier = @"FaultNotesCellIdentifier";
        PSFaultNotesCell *cell = [self.tableView dequeueReusableCellWithIdentifier:faultNotesCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSFaultNotesCell" owner:self options:nil];
            cell = self.faultNotesCell;
            self.faultNotesCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.section == kSectionPriority){
        static NSString *priorityCellIdentifier = @"PriorityCellIdentifier";
        PSPriorityCell *cell = [self.tableView dequeueReusableCellWithIdentifier:priorityCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSPriorityCell" owner:self options:nil];
            cell = self.priorityCell;
            self.priorityCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.section == kSectionDuration){
        static NSString *duartionCellIdentifier = @"DurationCellIdentifier";
        PSDurationCell *cell = [self.tableView dequeueReusableCellWithIdentifier:duartionCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSDurationCell" owner:self options:nil];
            cell = self.durationCell;
            self.durationCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.section == kSectionAsbestos){
        static NSString *asbestosCellIdentifier = @"AsbestosCellIdentifier";
        PSJobDetailAsbestosCell *cell = [self.tableView dequeueReusableCellWithIdentifier:asbestosCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSJobDetailAsbestosCell" owner:self options:nil];
            cell = self.jobDetailAsbestosCell;
            self.jobDetailAsbestosCell = nil;
        }
        
        if (!isEmpty(self.jobAsbestosArray)) {
            PropertyAsbestosData *asbestosData = [self.jobAsbestosArray objectAtIndex:indexPath.row];
            if (asbestosData != nil) {
                cell.lblAsbestos.text = asbestosData.asbRiskLevelDesc;
                cell.lblAsbestosLocation.text = asbestosData.riskDesc;
                
            }
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.section == kSectionResponseTime){
        static NSString *responseTimeCellIdentifier = @"ResponseTimeCellIdentifier";
        PSResponseTimeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:responseTimeCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSResponseTimeCell" owner:self options:nil];
            cell = self.responseTimeCell;
            self.responseTimeCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
    }
    [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return _cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSFaultDetailsCell class]])
    {
        PSFaultDetailsCell *_cell = (PSFaultDetailsCell *) *cell;
        NSString *faultDescription = self.plannedComponent.componentName;
        if (!isEmpty(faultDescription))
        {
            _cell.lblFaultDetails.text = faultDescription;
        }
        _cell.lblFaultDetails.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSFaultNotesCell class]])
    {
        PSFaultNotesCell *_cell = (PSFaultNotesCell *) *cell;
        if(!isEmpty(self.plannedComponent.jsnNotes)){
            
            [_cell.lblFaultNotes setNumberOfLines:0];
            _cell.lblFaultNotes.text = self.plannedComponent.jsnNotes;
            CGRect labelFrame = _cell.lblFaultNotes.frame;
            labelFrame.size.height = [self heightForLabelWithText:self.plannedComponent.jsnNotes];
            [_cell.lblFaultNotes setFrame:labelFrame];
            [_cell.lblFaultNotes sizeToFit];
            
        }
        
        _cell.lblFaultNotes.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSPriorityCell class]])
    {
        PSPriorityCell *_cell = (PSPriorityCell *) *cell;
//        if (!isEmpty(self.plannedComponent.priority)) {
//        
//            _cell.lblPriority.text = self.plannedComponent.priority;
//        }
        _cell.lblPriority.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSDurationCell class]])
    {
        PSDurationCell *_cell = (PSDurationCell *) *cell;
        if (!isEmpty(self.plannedComponent.durationUnit)) {
            _cell.lblDuration.text = self.plannedComponent.durationUnit;
        }
        else {
            _cell.lblDuration.text = nil;
        }
        _cell.lblDuration.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSJobDetailAsbestosCell class]])
    {
        PSJobDetailAsbestosCell *_cell = (PSJobDetailAsbestosCell *) *cell;
        if (!isEmpty(self.jobAsbestosArray)) {
            PropertyAsbestosData *asbestosData = [self.jobAsbestosArray objectAtIndex:indexPath.row];
            if (asbestosData != nil) {
                _cell.lblAsbestos.text = asbestosData.asbRiskLevelDesc;
                _cell.lblAsbestosLocation.text = asbestosData.riskDesc;
                
            }
        }
        _cell.lblAsbestos.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblAsbestosLocation.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSResponseTimeCell class]])
    {
        PSResponseTimeCell *_cell = (PSResponseTimeCell *) *cell;
        if (!isEmpty(self.jobData.responseTime)) {
            _cell.lblResponseTime.text = self.jobData.responseTime;
        }
        _cell.lblResponseTime.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat rowHeight = 0.0;
    
    if(indexPath.section == kSectionFaultDetails){
        rowHeight = kFaultDetailsCellHeight;
    }
    
    else if (indexPath.section == kSectionFaultNotes) {
        rowHeight = [self heightForLabelWithText:self.plannedComponent.jsnNotes];
        if (rowHeight < kFaultNotesCellHeight)
        {
            rowHeight = kFaultNotesCellHeight;
        }
    }
    
    else if (indexPath.section == kSectionPriority){
        rowHeight = kPriorityCellHeight;
    }
    
    else if (indexPath.section == kSectionDuration){
        rowHeight = kDurationCellHeight;
    }
    
    else if (indexPath.section == kSectionAsbestos){
        rowHeight = kAsbestosCellHeight;
    }
    
    else if (indexPath.section == kSectionResponseTime){
        rowHeight = kResponseTimeCellHeight;
    }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger numberOfRows=0;
    
    if(section == kSectionFaultDetails){
        numberOfRows = kSectionFaultDetailsRows;
    }
    
    else if (section == kSectionFaultNotes){
        numberOfRows = kSectionFaultNotesRows;
    }
    
    else if (section == kSectionPriority){
        numberOfRows = kSectionPriorityRows;
    }
    
    else if (section == kSectionDuration){
        numberOfRows = kSectionDurationRows;
    }
    
    else if (section == kSectionAsbestos){
        if (!isEmpty(self.jobAsbestosArray))
        {
            numberOfRows = [self.jobAsbestosArray count];
        }
        else
        {
            numberOfRows = 1;
        }
    }
    
    else if (section == kSectionResponseTime){
        numberOfRows = kSectionResponseTimeRows;
    }
    
    return numberOfRows;
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickPauseButton
{
    CLS_LOG(@"onClickPauseButton");
    //[self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
    PSJobStatusViewController *jobStatusViewConroller = [[PSJobStatusViewController alloc] initWithNibName:@"PSJobStatusViewController" bundle:[NSBundle mainBundle]];
    [jobStatusViewConroller setPlannedComponent:self.plannedComponent];
    
    [jobStatusViewConroller setViewMode:kPauseJobViewMode];
    [self.navigationController pushViewController:jobStatusViewConroller animated:YES];
}

- (void) onClickResumeButton
{
    CLS_LOG(@"onClickResumeButton");
    [[PSDataUpdateManager sharedManager] updatePlannedJobStatus:self.plannedComponent jobStaus:kJobStatusInProgress];
    [self setRightBarButtonItems:@[self.pauseButton, self.completeButton]];
}
- (void) onClickCompleteButton
{
    CLS_LOG(@"onClickCompleteButton");
    
    PSJobStatusViewController *jobStatusViewConroller = [[PSJobStatusViewController alloc] initWithNibName:@"PSJobStatusViewController" bundle:[NSBundle mainBundle]];
    [jobStatusViewConroller setPlannedComponent:self.plannedComponent];
    
    [jobStatusViewConroller setViewMode:CompletePlannedJobViewMode];
    
    [self.navigationController pushViewController:jobStatusViewConroller animated:YES];
}




- (IBAction)onClickStartJobButton:(id)sender
{
    [self.plannedComponent startJob];
    
    NSString *message = [NSString stringWithFormat:@"%@ %@",self.plannedComponent.jsNumber, LOC(@"KEY_ALERT_DETAIL_JOB_SHEET_IN_PROGRESS")];
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_JOB_SHEET_IN_PROGRESS")
                                                   message:message
                                                  delegate:self
                                         cancelButtonTitle:LOC(@"KEY_ALERT_CONFIRM")
                                         otherButtonTitles:nil, nil];
    alert.tag = AlertViewTagStartJob;
    [alert show];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
