//
//  PSFaultDetailsCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSFaultDetailsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblFaultDetails;

@end
