//
//  PSProrityCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSPriorityCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblPriority;

@end
