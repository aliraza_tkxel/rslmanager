//
//  PSAdaptationJobDetailViewController.h
//  PropertySurveyApp
//
//  Created by Waqas Bhatti on 08/08/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSJobDetailViewController.h"

@interface PSAdaptationJobDetailViewController : PSJobDetailViewController
{
    
}
@property(nonatomic,strong) PlannedTradeComponent * plannedComponent;
@end
