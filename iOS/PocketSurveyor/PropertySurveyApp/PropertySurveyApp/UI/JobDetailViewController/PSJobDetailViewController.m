//
//  PSJobDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 21/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSJobDetailViewController.h"

#import "PSFaultDetailsCell.h"
#import "PSFaultNotesCell.h"
#import "PSJobDetailAsbestosCell.h"
#import "PSPriorityCell.h"
#import "PSDurationCell.h"
#import "PSResponseTimeCell.h"

#import "PSJobStatusViewController.h"
#import "PSBarButtonItem.h"
#import "JobDataList+Methods.h"

#import "PSRepairImagesViewController.h"
#import "PSRepairImagesGalleryViewController.h"

@interface PSJobDetailViewController ()

@end



#define kNumberOfSections 5
#define kSectionFaultDetails 0
#define kSectionFaultNotes 1
#define kSectionPriority 2
#define kSectionDuration 3
#define kSectionAsbestos 5
#define kSectionResponseTime 4

#define kSectionHeaderHeight 13

#define kSectionFaultDetailsRows 1
#define kSectionFaultNotesRows 1
#define kSectionPriorityRows 1
#define kSectionDurationRows 1
#define kSectionResponseTimeRows 1



#define kFaultDetailsCellHeight 56
#define kFaultNotesCellHeight 33
#define kPriorityCellHeight 36
#define kDurationCellHeight 34
#define kAsbestosCellHeight 42
#define kResponseTimeCellHeight 32


@implementation PSJobDetailViewController
@synthesize headerViewArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.headerViewArray = [NSMutableArray array];
    [self loadSectionHeaderViews:@[LOC(@"KEY_STRING_FAULT_DETAILS"),LOC(@"KEY_STRING_FAULT_NOTES"),LOC(@"KEY_STRING_PRIORITY"),LOC(@"KEY_STRING_DURATION"),/*LOC(@"KEY_STRING_ASBESTOS"),*/LOC(@"KEY_STRING_RESPONSE_TIME")
                             ]   headerViews:self.headerViewArray];
    [self addTableHeaderView];
    [self loadNavigationonBarItems];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(!isEmpty(self.jobData) && ![self.jobData.jobStatus isEqualToString:kJobStatusNotStarted]) {
        [self hideTableHeaderView];
    }
    
    if (!isEmpty(self.jobData) &&  ![self.jobData.jobStatus isEqualToString:kJobStatusComplete] && ![self.jobData.jobStatus isEqualToString:kJobStatusNoEntry])
    {
        if ([self.jobData.jobStatus isEqualToString:kJobStatusPaused])
        {
            [self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
        }
        else if ([self.jobData.jobStatus isEqualToString:kJobStatusInProgress])
        {
            [self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
        }
        [self addCameraButtonInRightBarButtons];
    }
    else
    {
        [self.pauseButton setEnabled:NO];
        [self.resumeButton setEnabled:NO];
        [self.completeButton setEnabled:NO];
    }

    [self registerNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [self setTableView:nil];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    self.pauseButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                              style:PSBarButtonItemStylePause
                                                             target:self
                                                             action:@selector(onClickPauseButton)];
    self.resumeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                               style:PSBarButtonItemStylePlay
                                                              target:self
                                                              action:@selector(onClickResumeButton)];
    self.completeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                 style:PSBarButtonItemStyleCheckmark
                                                                target:self
                                                                action:@selector(onClickCompleteButton)];
    self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                 style:PSBarButtonItemStyleCamera
                                                                target:self
                                                                action:@selector(onClickCameraButton)];
    
    //[self.completeButton setEnabled:NO];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];

    if (![self.jobData.jobStatus isEqualToString:kJobStatusComplete] || ![self.jobData.jobStatus isEqualToString:kJobStatusNoEntry])
    {
        if ([self.jobData.jobStatus isEqualToString:kJobStatusPaused])
        {
            [self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
            [self.completeButton setEnabled:FALSE];
        }
        else if ([self.jobData.jobStatus isEqualToString:kJobStatusInProgress])
        {
            [self setRightBarButtonItems:@[self.pauseButton,self.completeButton]];
            [self.completeButton setEnabled:TRUE];
        }
        [self addCameraButtonInRightBarButtons];
    }

    NSString *title = [NSString stringWithFormat:@"Job %@",self.jobData.jsNumber];
    [self setTitle:title];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickPauseButton
{
    CLS_LOG(@"onClickPauseButton");
    //[self setRightBarButtonItems:@[self.resumeButton, self.completeButton]];
    PSJobStatusViewController *jobStatusViewConroller = [[PSJobStatusViewController alloc] initWithNibName:@"PSJobStatusViewController" bundle:[NSBundle mainBundle]];
    [jobStatusViewConroller setJobData:self.jobData];
    
    [jobStatusViewConroller setViewMode:kPauseJobViewMode];
    [self.navigationController pushViewController:jobStatusViewConroller animated:YES];
}

- (void) onClickResumeButton
{
    CLS_LOG(@"onClickResumeButton");
    [self setRightBarButtonItems:@[self.pauseButton, self.completeButton]];
    [self.completeButton setEnabled:TRUE];
    [[PSDataUpdateManager sharedManager] updateJobStatus:self.jobData jobStaus:kJobStatusInProgress];
    [self addCameraButtonInRightBarButtons];
}
- (void) onClickCompleteButton
{
    CLS_LOG(@"onClickCompleteButton");
    
    PSJobStatusViewController *jobStatusViewConroller = [[PSJobStatusViewController alloc] initWithNibName:@"PSJobStatusViewController" bundle:[NSBundle mainBundle]];
    [jobStatusViewConroller setJobData:self.jobData];
    [jobStatusViewConroller setViewMode:kCompleteJobViewMode];
    [self.navigationController pushViewController:jobStatusViewConroller animated:YES];
}

- (void) onClickCameraButton
{
    CLS_LOG(@"onClickCameraButton");
    
    PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
    if(self.jobData.jobDataListToAppointment.appointmentToProperty)
    {
        [repairImagesViewConroller setPropertyObj:self.jobData.jobDataListToAppointment.appointmentToProperty];
        [repairImagesViewConroller setSchemeObj:nil];
    }
    else
    {
        [repairImagesViewConroller setPropertyObj:nil];
        [repairImagesViewConroller setSchemeObj:self.jobData.jobDataListToAppointment.appointmentToScheme];
    }
    [repairImagesViewConroller setJobDataListObj:self.jobData];
    [self.navigationController pushViewController:repairImagesViewConroller animated:YES];
}

#pragma mark - IBActions
- (IBAction)onClickStartJobButton:(id)sender
{
//    [self.jobData startJob];
//    NSString *message = [NSString stringWithFormat:@"%@ %@",self.jobData.jsNumber, LOC(@"KEY_ALERT_DETAIL_JOB_SHEET_IN_PROGRESS")];
//    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_JOB_SHEET_IN_PROGRESS")
//                                                   message:message
//                                                  delegate:nil
//                                         cancelButtonTitle:LOC(@"KEY_ALERT_CONFIRM")
//                                         otherButtonTitles:nil, nil];
//    alert.tag = AlertViewTagStartJob;
//    [alert show];
    
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_PHOTOGRAPH_A_FAULT")
                                                   message:LOC(@"KEY_ALERT_BEFORE_FAULT_PICTURE")
                                                  delegate:self
                                         cancelButtonTitle:LOC(@"KEY_ALERT_YES")
                                         otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"), nil];
    alert.tag = AlertViewTagFaultPicture;
    [alert show];
}

#pragma mark - UITableView Delegate Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *_cell = nil;
    
    if (indexPath.section == kSectionFaultDetails){
        static NSString *faultDetailsCellIdentifier = @"FaultDetailsCellIdentifier";
        PSFaultDetailsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:faultDetailsCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSFaultDetailsCell" owner:self options:nil];
            cell = self.faultDetailsCell;
            self.faultDetailsCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    else if (indexPath.section == kSectionFaultNotes){
        static NSString *faultNotesCellIdentifier = @"FaultNotesCellIdentifier";
        PSFaultNotesCell *cell = [self.tableView dequeueReusableCellWithIdentifier:faultNotesCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSFaultNotesCell" owner:self options:nil];
            cell = self.faultNotesCell;
            self.faultNotesCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.section == kSectionPriority){
        static NSString *priorityCellIdentifier = @"PriorityCellIdentifier";
        PSPriorityCell *cell = [self.tableView dequeueReusableCellWithIdentifier:priorityCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSPriorityCell" owner:self options:nil];
            cell = self.priorityCell;
            self.priorityCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.section == kSectionDuration){
        static NSString *duartionCellIdentifier = @"DurationCellIdentifier";
        PSDurationCell *cell = [self.tableView dequeueReusableCellWithIdentifier:duartionCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSDurationCell" owner:self options:nil];
            cell = self.durationCell;
            self.durationCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.section == kSectionAsbestos){
        static NSString *asbestosCellIdentifier = @"AsbestosCellIdentifier";
        PSJobDetailAsbestosCell *cell = [self.tableView dequeueReusableCellWithIdentifier:asbestosCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSJobDetailAsbestosCell" owner:self options:nil];
            cell = self.jobDetailAsbestosCell;
            self.jobDetailAsbestosCell = nil;
        }
        
        if (!isEmpty(self.jobAsbestosArray)) {
            PropertyAsbestosData *asbestosData = [self.jobAsbestosArray objectAtIndex:indexPath.row];
            if (asbestosData != nil) {
                cell.lblAsbestos.text = asbestosData.asbRiskLevelDesc;
                cell.lblAsbestosLocation.text = asbestosData.riskDesc;
                
            }
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.section == kSectionResponseTime){
        static NSString *responseTimeCellIdentifier = @"ResponseTimeCellIdentifier";
        PSResponseTimeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:responseTimeCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSResponseTimeCell" owner:self options:nil];
            cell = self.responseTimeCell;
            self.responseTimeCell = nil;
        }

        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
    }
    [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return _cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSFaultDetailsCell class]])
    {
        PSFaultDetailsCell *_cell = (PSFaultDetailsCell *) *cell;
        NSString *faultDescription = [self.jobData getFaultDescription];
        if (!isEmpty(faultDescription))
        {
             _cell.lblFaultDetails.text = faultDescription;
         }
         _cell.lblFaultDetails.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
         *cell = _cell;
     }
    
    else if ([*cell isKindOfClass:[PSFaultNotesCell class]])
    {
        PSFaultNotesCell *_cell = (PSFaultNotesCell *) *cell;

        if(!isEmpty(self.jobData.jsnNotes))
        {
            [_cell.lblFaultNotes setNumberOfLines:0];
            _cell.lblFaultNotes.text = self.jobData.jsnNotes;
            CGRect labelFrame = _cell.lblFaultNotes.frame;
            labelFrame.size.height = [self heightForLabelWithText:self.jobData.jsnNotes];
            [_cell.lblFaultNotes setFrame:labelFrame];
            [_cell.lblFaultNotes sizeToFit];
        }
        _cell.lblFaultNotes.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSPriorityCell class]])
    {
        PSPriorityCell *_cell = (PSPriorityCell *) *cell;
        if (!isEmpty(self.jobData.priority)) {
            _cell.lblPriority.text = self.jobData.priority;
        }
        _cell.lblPriority.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSDurationCell class]])
    {
        PSDurationCell *_cell = (PSDurationCell *) *cell;
        if (!isEmpty(self.jobData.durationUnit)) {

            _cell.lblDuration.text = self.jobData.durationUnit;
        }
        else {
            _cell.lblDuration.text = nil;
        }
        _cell.lblDuration.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSJobDetailAsbestosCell class]])
    {
        PSJobDetailAsbestosCell *_cell = (PSJobDetailAsbestosCell *) *cell;
        if (!isEmpty(self.jobAsbestosArray)) {
            PropertyAsbestosData *asbestosData = [self.jobAsbestosArray objectAtIndex:indexPath.row];
            if (asbestosData != nil) {
                _cell.lblAsbestos.text = asbestosData.asbRiskLevelDesc;
                _cell.lblAsbestosLocation.text = asbestosData.riskDesc;
            }
        }
        else
        {
            _cell.lblAsbestos.text = LOC(@"KEY_STRING_NONE");
        }
        _cell.lblAsbestos.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblAsbestosLocation.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);

        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSResponseTimeCell class]])
    {
        PSResponseTimeCell *_cell = (PSResponseTimeCell *) *cell;
        if (!isEmpty(self.jobData.responseTime)) {
            _cell.lblResponseTime.text = self.jobData.responseTime;
        }
        _cell.lblResponseTime.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        *cell = _cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat rowHeight = 0.0;
    
    if(indexPath.section == kSectionFaultDetails){
        rowHeight = kFaultDetailsCellHeight;
    }
    
    else if (indexPath.section == kSectionFaultNotes){
        rowHeight = [self heightForLabelWithText:self.jobData.jsnNotes];
        if (rowHeight < kFaultNotesCellHeight)
        {
            rowHeight = kFaultNotesCellHeight;
        }
    }
    
    else if (indexPath.section == kSectionPriority){
        rowHeight = kPriorityCellHeight;
    }
    
    else if (indexPath.section == kSectionDuration){
        rowHeight = kDurationCellHeight;
    }
    
    else if (indexPath.section == kSectionAsbestos){
        rowHeight = kAsbestosCellHeight;
    }
    
    else if (indexPath.section == kSectionResponseTime){
        rowHeight = kResponseTimeCellHeight;
    }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger numberOfRows=0;
    
    if(section == kSectionFaultDetails){
        numberOfRows = kSectionFaultDetailsRows;
    }
    
    else if (section == kSectionFaultNotes){
        numberOfRows = kSectionFaultNotesRows;
    }
    
    else if (section == kSectionPriority){
        numberOfRows = kSectionPriorityRows;
    }
    
    else if (section == kSectionDuration){
        numberOfRows = kSectionDurationRows;
    }
    
    else if (section == kSectionAsbestos){
        if (!isEmpty(self.jobAsbestosArray))
        {
            numberOfRows = [self.jobAsbestosArray count];
        }
        else
        {
            numberOfRows = 1;
        }
    }
    
    else if (section == kSectionResponseTime){
        numberOfRows = kSectionResponseTimeRows;
    }
    
    return numberOfRows;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kSectionHeaderHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return kNumberOfSections;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*PSJobStatusViewController *jobStatusViewConroller = [[PSJobStatusViewController alloc] initWithNibName:@"PSJobStatusViewController" bundle:[NSBundle mainBundle]];
    [jobStatusViewConroller setJobData:self.jobData];
    
    if (indexPath.section == kSectionPriority) {
        [jobStatusViewConroller setViewMode:0];
    }
    
    else if (indexPath.section == kSectionDuration) {
        [jobStatusViewConroller setViewMode:1];
    }
    [self.navigationController pushViewController:jobStatusViewConroller animated:YES];*/
}

#pragma mark - Notifications
- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobStartNotificationReceive) name:kJobStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveApplianceFailureNotification) name:kSaveNewAppliacneFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kJobStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveNewAppliacneFailureNotification object:nil];
}


- (void) onJobStartNotificationReceive
{
    [self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
}

#pragma mark - Methods
- (void) addTableHeaderView
{
    CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
    UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
    headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.frame = CGRectMake(8, 0, 245, 29);
    lbl.center = CGPointMake(lbl.center.x, headerView.center.y);
    lbl.backgroundColor = [UIColor clearColor];
    [lbl setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lbl setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lbl setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
    lbl.text =LOC(@"KEY_STRING_JOB_START_MESSAGE");
    
    UIButton *btnStartAppointment = [[UIButton alloc]init];
    btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
    btnStartAppointment.center = CGPointMake(btnStartAppointment.center.x, headerView.center.y);
    [btnStartAppointment setTitle:LOC(@"KEY_STRING_START") forState:UIControlStateNormal];
    [btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
    [btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:
     UIControlStateNormal];
    [btnStartAppointment addTarget:self action:@selector(onClickStartJobButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:lbl];
    [headerView addSubview:btnStartAppointment];
    
    if ([self.jobData.jobStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame)
    {
        headerViewRect = self.tableView.tableHeaderView.frame;
        headerViewRect.size.height = 40;
        headerViewRect.size.width = 320;
        [self.tableView.tableHeaderView setFrame:headerViewRect];
        [self.tableView setTableHeaderView:headerView];
    }
}

- (void) hideTableHeaderView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.5];
    [self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
    self.tableView.tableHeaderView = nil;
    [self.tableView.tableHeaderView setHidden:YES];
    [UIView commitAnimations];
    [self loadNavigationonBarItems];
    
}

-(void)addCameraButtonInRightBarButtons
{
    NSMutableArray *rightBarButtons = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
    NSInteger jobPicturesCount  = [self.jobData.jobDataListToRepairImages count];
    if (jobPicturesCount > 0)
    {
        BOOL found = FALSE;
        for (PSBarButtonItem* button in rightBarButtons) {
            if([button tag] == PSBarButtonItemStyleCamera) {
                found = TRUE;
                break;
            }
        }
        
        if(!found) {
            [rightBarButtons insertObject:self.cameraButton atIndex:[rightBarButtons count]];
            [self setRightBarButtonItems:rightBarButtons];
        }
    }
    
}

#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForLabelWithText:(NSString*)string
{
    float horizontalPadding = 0;
    float verticalPadding = 8;
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                //[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
                                [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
                                nil];
    CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(300, 999999.0f)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:attributes
                                               context:nil];
    CGFloat height = boundingRect.size.height + verticalPadding;
    
#else
    CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
                        constrainedToSize:CGSizeMake(300, 999999.0f)
                            lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
    
    return height;
}


#pragma mark UIAlertView Delegate 

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
 if(alertView.tag == AlertViewTagFaultPicture) {
        CLS_LOG(@"Clicked at Index %d",buttonIndex);
        if(buttonIndex == 0) { //YES
            
            PSRepairImagesGalleryViewController *repairImagesViewConroller = [[PSRepairImagesGalleryViewController alloc] init];
            if(self.jobData.jobDataListToAppointment.appointmentToProperty)
            {
                [repairImagesViewConroller setPropertyObj:self.jobData.jobDataListToAppointment.appointmentToProperty];
                [repairImagesViewConroller setSchemeObj:nil];
            }
            else
            {
                [repairImagesViewConroller setPropertyObj:nil];
                [repairImagesViewConroller setSchemeObj:self.jobData.jobDataListToAppointment.appointmentToScheme];
            }
            [repairImagesViewConroller setJobDataListObj:self.jobData];
            [self.navigationController pushViewController:repairImagesViewConroller animated:YES];
        }
    }
}

@end
