//
//  PSPlannedJobDetailViewController.h
//  PropertySurveyApp
//
//  Created by TkXel on 01/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSJobDetailViewController.h"
#import "PlannedTradeComponent.h"

@interface PSPlannedJobDetailViewController : PSJobDetailViewController {
    
}

@property(nonatomic,strong) PlannedTradeComponent * plannedComponent;

@end
