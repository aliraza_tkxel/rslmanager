//
//  PSRepairListViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 13/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSRepairListViewController.h"
#import "PSBarButtonItem.h"
#import "PSFaultRepairListCell.h"
#import "NSFetchedResultsControllerHelper.h"

#define kRepairViewSectionHeaderHeight 20


@interface PSRepairListViewController ()
{
    BOOL _reloadingTable;
    BOOL _noResultsFound;
    BOOL _firstTime;
    BOOL _isSearching;
    NSMutableArray *_repairListArray;
}

@end

@implementation PSRepairListViewController
@synthesize searchString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _noResultsFound = NO;
    _firstTime = YES;
    _isSearching = NO;
    _repairListArray = [[NSMutableArray alloc] init];
    if (!isEmpty(self.jobData.faultRepairDataList))
    {
        _repairListArray = [NSMutableArray arrayWithArray:[self.jobData.faultRepairDataList allObjects]];
    }
    [self loadNavigationonBarItems];
    
    if(OS_VERSION < 7.0)
    {
        CGRect frame = self.searchBarView.frame;
        self.searchBarView.frame = CGRectMake(frame.origin.x,
                                              frame.origin.y,
                                              frame.size.width,
                                              44);
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                          self.searchBarView.frame.origin.y + self.searchBarView.frame.size.height,
                                          self.tableView.frame.size.width,
                                          self.tableView.frame.size.height);
    }
    else
    {
        //Change the Section Index Backgorund Color
        self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
        self.tableView.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
    }
    self.view.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    self.txtSearch.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.txtSearch.delegate = self;
    
    [self performFetchOnNSFetchedResultsController];
    if([[self.fetchedResultsController fetchedObjects] count] <= 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
                                         LOC(@"KEY_ALERT_LOADING"),
                                         LOC(@"KEY_STRING_REPAIR_LIST")]];
        });
        
    }
    NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:@"username"];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:@"salt"];
   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[PSFaultManager sharedManager]fetchFaultRepairList:requestParameters];
    });
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotifications];
    /*self.navigationController.navigationBar.hidden = YES;
    [self.navigationItem setHidesBackButton:YES animated:NO];*/
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self deRegisterNotificaitons];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    self.saveButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                             style:PSBarButtonItemStyleDefault
                                                            target:self
                                                            action:@selector(onClickSaveButton)];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setRightBarButtonItems:@[self.saveButton]];
 
    [self setTitle:@"Repair List"];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}
- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    [[PSFaultManager sharedManager] addFaultRepairHistory:_repairListArray forJob:self.jobData];
}

#pragma mark - UITableView Delegate Methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if(_noResultsFound)
    {
        static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
        UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
        if(_cell == nil)
        {
            _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
        }
        // Configure the cell...
        
        _cell.textLabel.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);//UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
        _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
        _cell.textLabel.text = LOC(@"KEY_STRING_NO_FAULT_REPAIRS_FOUND");
        _cell.textLabel.textAlignment = PSTextAlignmentCenter;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        cell = _cell; 
    }
    else
    {
    
    static NSString *faultRepairCellIdentifier = @"faultRepairCellIdentifier";
    PSFaultRepairListCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:faultRepairCellIdentifier];
    
    
    if(_cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"PSFaultRepairListCell" owner:self options:nil];
        _cell = self.repairDetailCell;
        self.repairDetailCell = nil;
    }
    [self configureCell:&_cell atIndexPath:indexPath];
    
    _cell.backgroundColor = [UIColor clearColor];
    _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = _cell;
    }
    return cell;
}


-(BOOL)isFetchedResultsOutofBound:(NSIndexPath *)indexPath
{
    BOOL isOutofBound = YES;
    //check if indexPath is within range of fetchedResultsController limit
    if ([[self.fetchedResultsController sections] count] >= [indexPath section]){
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:[indexPath section]];
        if ([sectionInfo numberOfObjects] >= [indexPath row]){
            isOutofBound = NO;
        } else {
            isOutofBound = YES;
        }
    } else {
        isOutofBound = YES;
    }
    return isOutofBound;
}

- (void)configureCell:(PSFaultRepairListCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    _firstTime = NO;
    if (![self isFetchedResultsOutofBound:indexPath])
    {
        if (!_noResultsFound)
        {
            if (self.viewMode == RepairListViewMode)
            {
                FaultRepairData * faultRepairData = [self.fetchedResultsController objectAtIndexPath:indexPath];
                if (!isEmpty(faultRepairData))
                {
                    (*cell).lblRepairDetail.text = faultRepairData.faultRepairDescription;
                    if ([_repairListArray containsObject:faultRepairData])
                    {
                        [(*cell) makeSelected];
                    }
                    else
                    {
                        [(*cell) makeDeselected];
                    }
                }
            }
            else if (self.viewMode == RecentViewMode)
            {
                FaultRepairListHistory *jobHistory = [self.fetchedResultsController objectAtIndexPath:indexPath];
                if (!isEmpty(jobHistory))
                {
                    FaultRepairData * faultRepairData = nil;
                    faultRepairData = [[PSCoreDataManager sharedManager] faultReapirWithID:jobHistory.faultRepairID];
                    if (!isEmpty(faultRepairData))
                    {
                        (*cell).lblRepairDetail.text = faultRepairData.faultRepairDescription;
                        if ([_repairListArray containsObject:faultRepairData])
                        {
                            [(*cell) makeSelected];
                        }
                        else
                        {
                            [(*cell) makeDeselected];
                        }
                        
                    }
                }
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSInteger heightForHeader = 0;
    if (self.viewMode == RepairListViewMode)
    {
        if (!_noResultsFound && !_isSearching)
        {
            heightForHeader = kRepairViewSectionHeaderHeight;
        }
    }
    return heightForHeader;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    int sections = 1;
  //  if (self.viewMode == RepairListViewMode)
   // {
        if(_noResultsFound)
        {
            sections = 1; //For Showing the No Results Found
        }
        else
        {
            sections = [[self.fetchedResultsController sections] count];
        }
 //   }
    return sections;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *header = nil;
    if (!_noResultsFound && !_isSearching) {
        
        id <NSFetchedResultsSectionInfo> sectionInfo;
        sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        header = [sectionInfo indexTitle];
        if (header == nil)
        {
            CLS_LOG(@"nil sectiion header");
        }
    }
    return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numberOfRows;
    if(_noResultsFound)
    {
        numberOfRows = 1; //For Showing the No Results Found
    }
    else
    {
        id <NSFetchedResultsSectionInfo> sectionInfo;
        sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        numberOfRows = [sectionInfo numberOfObjects];
    }
    return numberOfRows;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(_isSearching)
    {
        return nil;
    }
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    if(_isSearching)
    {
        return -1;
    }
    return [self.fetchedResultsController.sectionIndexTitles indexOfObject:title];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_noResultsFound)
    {
        PSFaultRepairListCell *cell = (PSFaultRepairListCell*) [self.tableView cellForRowAtIndexPath:indexPath];
        FaultRepairData * faultRepairData;
        if (self.viewMode == RepairListViewMode)
        {
            faultRepairData = [self.fetchedResultsController objectAtIndexPath:indexPath];
        }
        else if (self.viewMode == RecentViewMode)
        {
            FaultRepairListHistory *jobHistory = [self.fetchedResultsController objectAtIndexPath:indexPath];
            if (!isEmpty(jobHistory))
            {
                faultRepairData = [[PSCoreDataManager sharedManager] faultReapirWithID:jobHistory.faultRepairID];
            }
            
        }
        if (!isEmpty(faultRepairData))
        {
            if ([_repairListArray containsObject:faultRepairData])
            {
                [_repairListArray removeObject:faultRepairData];
                [cell makeDeselected];
                
            }
            else
            {
                [_repairListArray addObject:faultRepairData];
                [cell makeSelected];
            }
        }
    }
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource
{
	_reloadingTable = YES;
    [self.tableView setUserInteractionEnabled:NO];
    [self.tableView reloadData];
}

- (void)doneLoadingTableViewData
{
	_reloadingTable = NO;
    [self.tableView setUserInteractionEnabled:YES];
    [self.tableView reloadData];
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    _isSearching = YES;
    self.selectedControl = (UITextField *)textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (isEmpty(self.searchString))
    {
        _isSearching = NO;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (isEmpty(self.searchString))
    {
        _isSearching = NO;
    }
	[textField resignFirstResponder];
    [self.tableView reloadData];
	return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //  BOOL isBackspaceHit = NO;
    NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
    
    if([string isEqualToString:@""] && !isEmpty(textField.text)) //Handle Backspace Here
    {
        // isBackspaceHit = YES;
        stringAfterReplacement = [textField.text substringToIndex:textField.text.length - 1];
    }
    if (isEmpty(stringAfterReplacement))
    {
        _isSearching = NO;
    }
    else
    {
        _isSearching = YES;
    }
    self.searchString = stringAfterReplacement;
    [self FilterRepairListWithString];
    CLS_LOG(@"string after replacement: %@",stringAfterReplacement);
    return YES;
}

#pragma mark - Fetched results controller
- (void) performFetchOnNSFetchedResultsController
{
    CLS_LOG(@"performFetchOnNSFetchedResultsController");
    NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		CLS_LOG(@"fetchedResultsController, Unresolved error %@, %@", error, [error userInfo]);
	}
    if (!_firstTime) {
        [self checkResultsEmptiness];
    }
    // [self checkResultsEmptiness];
    [self.tableView reloadData];
}

- (void) checkResultsEmptiness
{
    if([[self.fetchedResultsController fetchedObjects] count] <= 0)
    {
        _noResultsFound = YES;
    }
    else
    {
        _noResultsFound = NO;
    }
    //#warning PSA: FIX NO RESULTS YES BUG HERE (ON RESET SIMULATOR)
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if(self.viewMode == RepairListViewMode)
    {
        self.fetchedResultsController = [self repairFetchedResultsController];
    }
    else
    {
        self.fetchedResultsController = [self recentFetchedResultsController];
    }
    return _fetchedResultsController;
}

- (NSFetchedResultsController *)repairFetchedResultsController
{
    if (_repairFetchedResultsController != nil)
    {
        if (!isEmpty(self.searchString))
        {
            self.repairFilterPredicate = [NSPredicate predicateWithFormat:@"SELF.faultRepairDescription CONTAINS[cd] %@", self.searchString];
            [self.repairFetchRequest setPredicate:self.repairFilterPredicate];
            
        }
        else
        {
            self.repairFilterPredicate = nil;
            [self.repairFetchRequest setPredicate:self.repairFilterPredicate];
        }
        return _repairFetchedResultsController;
    }
    
    self.repairFetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:kFaultRepairData inManagedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext];
    [self.repairFetchRequest setEntity:entity];
    self.repairFetchRequest.returnsDistinctResults = YES;
    // Set the batch size to a suitable number.
    [self.repairFetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"faultRepairDescription"
                                        ascending:YES
                                        selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [self.repairFetchRequest setSortDescriptors:sortDescriptors];
    [self.repairFetchRequest setReturnsDistinctResults:YES];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:self.repairFetchRequest managedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext sectionNameKeyPath:kRepairSectionIdentifier cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    self.repairFetchedResultsController = aFetchedResultsController;
	NSError *error = nil;
	if (![self.repairFetchedResultsController performFetch:&error]) {
	    CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    if (!_firstTime) {
        [self checkResultsEmptiness];
    }
    
    return _repairFetchedResultsController;
}

- (NSFetchedResultsController *) recentFetchedResultsController
{
    if (_recentFetchedResultsController != nil)
    {
        if (!isEmpty(self.searchString))
        {
            
            self.repairFilterPredicate = [NSPredicate predicateWithFormat:@"SELF.faultRepairListHistoryToJobDataList.jsNumber LIKE[cd] %@ AND SELF.faultRepairDescription CONTAINS[cd] %@",self.jobData.jsNumber, self.searchString];
            [self.recentFetchRequest setPredicate:self.repairFilterPredicate];
        }
        else
        {
            self.repairFilterPredicate = [NSPredicate predicateWithFormat:@"SELF.faultRepairListHistoryToJobDataList.jsNumber LIKE[cd] %@",self.jobData.jsNumber];
            [self.recentFetchRequest setPredicate:self.repairFilterPredicate];
        }
        return _recentFetchedResultsController;
    }
    
    self.recentFetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:kFaultRepairListHistory inManagedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext];
    [self.recentFetchRequest setEntity:entity];
    self.recentFetchRequest.returnsDistinctResults = YES;
    // Set the batch size to a suitable number.
    [self.recentFetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"faultRepairDescription"
                                        ascending:YES
                                        selector:@selector(caseInsensitiveCompare:)];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [self.recentFetchRequest setSortDescriptors:sortDescriptors];
    [self.recentFetchRequest setReturnsDistinctResults:YES];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:self.recentFetchRequest managedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    self.recentFetchedResultsController = aFetchedResultsController;
	NSError *error = nil;
	if (![self.recentFetchedResultsController performFetch:&error]) {
	    CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
	}
    if (!_firstTime) {
        [self checkResultsEmptiness];
    }
    return _recentFetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    if((self.viewMode == RecentViewMode && controller == self.repairFetchedResultsController) ||
       (self.viewMode == RepairListViewMode && controller == self.recentFetchedResultsController))
    {
        return;
    }
    CLS_LOG(@"<<<<<<<<<<<< controllerWillChangeContent");
    
    [self.tableView beginUpdates];
    
    CLS_LOG(@">>>>>>>>>>>>>>>>>> controllerWillChangeContent");
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if((self.viewMode == RecentViewMode && controller == self.repairFetchedResultsController) ||
       (self.viewMode == RepairListViewMode && controller == self.recentFetchedResultsController))
    {
        return;
    }
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    if((self.viewMode == RecentViewMode && controller == self.repairFetchedResultsController) ||
       (self.viewMode == RepairListViewMode && controller == self.recentFetchedResultsController))
    {
        return;
    }
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView cellForRowAtIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if((self.viewMode == RecentViewMode && controller == self.repairFetchedResultsController) ||
       (self.viewMode == RepairListViewMode && controller == self.recentFetchedResultsController))
    {
        return;
    }
    [self.tableView endUpdates];
}

/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */


#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveRepairDataSaveSuccessNotification) name:kFetchFaultRepairDataSaveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveRepairDataSaveFailureNotification) name:kFetchFaultRepairDataSaveFailureNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveFaultRepairListSaveSuccessNotification) name:kSaveFaultRepairListSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveFaultRepairListSaveFailureNotification) name:kSaveFaultRepairListFailureNotification object:nil];
}

- (void) deRegisterNotificaitons
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchFaultRepairDataSaveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchFaultRepairDataSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveFaultRepairListSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveFaultRepairListFailureNotification object:nil];
}

- (void) onReceiveRepairDataSaveSuccessNotification
{
    [self hideActivityIndicator];
    [self performFetchOnNSFetchedResultsController];
    if(_reloadingTable)
    {
        [self doneLoadingTableViewData];
    }
    else
    {
        [self.tableView reloadData];
    }
}

- (void) onReceiveRepairDataSaveFailureNotification
{
    [self hideActivityIndicator];
    
    [self.tableView reloadData];
}

- (void) onReceiveFaultRepairListSaveSuccessNotification
{
    CLS_LOG(@"After Saving fault List");

    [self popOrCloseViewController];
}

- (void) onReceiveFaultRepairListSaveFailureNotification
{
    [self hideActivityIndicator];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.activityIndicator stopAnimating];
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                       message:LOC(@"KEY_ALERT_FAULT_REPAIR_SAVE_FAILURE")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
        
    });
}

#pragma mark - IBActions
- (IBAction)onClickRepairBtn:(id)sender
{
    CLS_LOG(@"onClickRepairBtn");
    [self.btnRepair setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.recentBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    [self loadViewWithMode:RepairListViewMode];
}
- (IBAction)onClickRecentBtn:(id)sender
{
    CLS_LOG(@"onClickRecentBtn");
    [self.recentBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnRepair setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self loadViewWithMode:RecentViewMode];
}
- (IBAction)onClickClearSearch:(id)sender
{
    CLS_LOG(@"onClickClearSearchs");
    NSString *searchFieldText;
    if (!isEmpty(((UITextField *)self.selectedControl).text))
    {
        searchFieldText = [((UITextField *)self.selectedControl).text substringToIndex:((UITextField *)self.selectedControl).text.length -1];
        self.txtSearch.text = searchFieldText;
        self.searchString = searchFieldText;
        _isSearching = !isEmpty(searchFieldText);
    }
    
    [self FilterRepairListWithString];
}

#pragma mark - Load View with Selected Mode

- (void) loadViewWithMode:(RepairViewMode)viewMode
{
    if([NSThread currentThread] == [NSThread mainThread])
    {
        @synchronized(self)
        {
//            if (self.viewMode == RecentViewMode)
//            {
//                self.repairFetchedResultsController = nil;
//            }
//            else if (self.viewMode == RecentViewMode)
//            {
//                self.recentFetchedResultsController = nil;
//            }
            self.viewMode = viewMode;
            [self performFetchOnNSFetchedResultsController];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadViewWithMode:viewMode];
        });
    }
}

- (IBAction)resignFirstResponder:(id)sender
{
    
}

#pragma mark - Filter/Search Methods
- (void) FilterRepairListWithString
{
    //  self.fetchedResultsController = nil;
    [self performFetchOnNSFetchedResultsController];
}
@end
