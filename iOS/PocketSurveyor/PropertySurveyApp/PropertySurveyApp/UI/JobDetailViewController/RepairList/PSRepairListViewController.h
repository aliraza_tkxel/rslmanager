//
//  PSRepairListViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 13/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
@class PSFaultRepairListCell;

typedef NS_ENUM(NSInteger, RepairViewMode)
{
    RepairListViewMode,
    RecentViewMode
};

@interface PSRepairListViewController : PSCustomViewController <UITextFieldDelegate,  NSFetchedResultsControllerDelegate>
@property (assign, nonatomic) RepairViewMode viewMode;
@property (strong, nonatomic) IBOutlet UIButton *btnRepair;
@property (strong, nonatomic) IBOutlet UIButton *recentBtn;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong,  nonatomic) NSString *searchString;
@property (strong,  nonatomic) NSPredicate *repairFilterPredicate;
@property (weak,   nonatomic) IBOutlet PSFaultRepairListCell *repairDetailCell;
@property (strong,  nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong,  nonatomic) NSFetchedResultsController *repairFetchedResultsController;
@property (strong,  nonatomic) NSFetchedResultsController *recentFetchedResultsController;
@property (strong,  nonatomic) NSFetchRequest *repairFetchRequest;
@property (strong,  nonatomic) NSFetchRequest *recentFetchRequest;
@property (strong, nonatomic) JobDataList *jobData;
- (IBAction)onClickRepairBtn:(id)sender;
- (IBAction)onClickRecentBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
- (IBAction)onClickClearSearch:(id)sender;
- (IBAction)resignFirstResponder:(id)sender;
- (void) loadViewWithMode:(RepairViewMode)viewMode;
@property (strong, nonatomic) IBOutlet UIView *searchBarView;
@end
