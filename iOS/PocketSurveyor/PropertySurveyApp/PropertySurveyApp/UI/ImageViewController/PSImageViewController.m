//
//  PSImageViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSImageViewController.h"
#import "PSBarButtonItem.h"
#import "PSImagePickerController.h"
#import "MWPhotoBrowser.h"
#import "PropertyPicture+MWPhoto.h"
@interface PSImageViewController ()
{
    
}

@end

@implementation PSImageViewController
@synthesize image;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    
    [self.saveButton setEnabled:NO];
    
    if (self.propertyObj.defaultPicture) {
     
        self.image=[self.propertyObj.defaultPicture underlyingImage];
        
        [self.imgProperty setImage:self.image];
        
        if ([self.propertyObj.defaultPicture.synchStatus integerValue]!=1) {

            [self.saveButton setTitle:@"Sync"];
            
            [self.saveButton setEnabled:YES];
        }
            
    }
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureSave:) name:kPropertyPictureSaveNotification object:nil];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    self.editButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                             style:PSBarButtonItemStyleEdit
                                                            target:self
                                                            action:@selector(onClickEditButton)];
    self.saveButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                             style:PSBarButtonItemStyleDefault
                                                            target:self
                                                            action:@selector(onClickSaveButton)];
    self.backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                             style:PSBarButtonItemStyleBack
                                                            target:self
                                                            action:@selector(onClickBackButton)];
    //[self.completeButton setEnabled:NO];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    [self setRightBarButtonItems:@[self.editButton,self.saveButton]];
    
    [self setTitle:@"Picture"];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    if(!isEmpty(self.image))
        [self saveImageToServer:self.image];
}

- (void) onClickEditButton
{
    UIActionSheet * _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:          nil
                                                       otherButtonTitles:NSLocalizedString(@"Take a Photo", nil),
                                     NSLocalizedString(@"Choose from Library", nil),
                                     nil] ;
    
    _actionsSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    _actionsSheet.tag=1;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [_actionsSheet showFromBarButtonItem:self.editButton animated:YES];
    }
    else
    {
        [_actionsSheet showInView:self.view];
    }
    
}

#pragma mark ActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    CLS_LOG(@"Clicked Index %d",buttonIndex);
    
    if (actionSheet.tag == 1)
    {
        if (buttonIndex != actionSheet.cancelButtonIndex)
        {
            if (buttonIndex == actionSheet.firstOtherButtonIndex)
            {
                [self showCameraOfSourceType:UIImagePickerControllerSourceTypeCamera];
                
            }
            else if (buttonIndex == actionSheet.firstOtherButtonIndex + 1)
            {
                [self showCameraOfSourceType:UIImagePickerControllerSourceTypePhotoLibrary]; return;
            }
            
        }
    }
}

#pragma mark - CameraHeaderDelagate

- (void)onClickBtnCamera:(id) sender {
    
    UIActionSheet * _actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                       cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:          nil
                                                       otherButtonTitles:NSLocalizedString(@"Take a Photo", nil),
                                     NSLocalizedString(@"Choose from Library", nil),
                                     nil] ;
    
    _actionsSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    _actionsSheet.tag=1;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [_actionsSheet showFromBarButtonItem:sender animated:YES];
    } else {
        [_actionsSheet showInView:self.view];
    }
    
}

-(void) showCameraOfSourceType:(UIImagePickerControllerSourceType) type
{
    if ([UIImagePickerController isSourceTypeAvailable:type])
    {
        PSImagePickerController *imagePicker = [[PSImagePickerController alloc] init];
        
        
        imagePicker.delegate = self;
        imagePicker.sourceType = type;
        imagePicker.allowsEditing = YES;
        
        if( type == UIImagePickerControllerSourceTypeCamera)
        {
            imagePicker.showsCameraControls = YES;
        }
        
        [self presentViewController:imagePicker animated:YES completion:^{
            
            [self.saveButton setEnabled:YES];
            
        }];
        
        
    }
    
}


#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if([info objectForKey:UIImagePickerControllerEditedImage])
    {
        self.image = [info valueForKey:UIImagePickerControllerEditedImage];
    }
    else
    {
        self.image = [info valueForKey:UIImagePickerControllerOriginalImage];
    }
    [self.imgProperty setImage:self.image];
     [[PSDataPersistenceManager sharedManager] updateModificationStatusOfAppointments:[NSArray arrayWithObject:self.propertyObj.propertyToAppointment] toModificationStatus:[NSNumber numberWithBool:YES]];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: LOC(@"KEY_ALERT_SAVE_FAILURE")
                              message: LOC(@"KEY_ALERT_IMAGE_SAVE_FAILURE")
                              delegate: nil
                              cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                              otherButtonTitles:nil];
        [alert show];
    }else
    {
        
    }
}

-(void) saveImageToServer:(UIImage*) _image
{
    NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
    [requestParameters setObject:_image forKey:kPropertyImage];
    [requestParameters setObject:@".jpg" forKey:kFileExtension];
    [requestParameters setObject:@"true" forKey:kPropertyPictureIsDefault];
    [requestParameters setObject:[NSNumber numberWithInt:0] forKey:@"propertyPicId"];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    [requestParameters setObject:self.propertyObj.propertyId forKey:kPropertyId];
    [requestParameters setObject:self.propertyObj.propertyToAppointment.appointmentType forKey:@"type"];
    [requestParameters setObject:[NSNumber numberWithInt:0] forKey:kItemId];
    [requestParameters setObject:[NSNumber numberWithInt:0] forKey:kAppointmentId];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
    [requestParameters setObject:[UtilityClass getUniqueIdentifierStringWithUserName:[[SettingsClass sharedObject]loggedInUser].userName] forKey:kUniqueImageIdentifier];
    [[PSDataPersistenceManager sharedManager] savePropertyPictureInfo:requestParameters withProperty:self.propertyObj];
    
}


-(void) onPictureSave:(NSNotification*) notification
{
    [self hideActivityIndicator];
    
    PropertyPicture * picture=notification.object;
    
    if (picture) {
        
        [self.saveButton setEnabled:NO];
    }
    
}

@end
