//
//  PSImageViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 20/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSCameraHeaderView.h"

@interface PSImageViewController : PSCustomViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate,PSCameraHeaderViewDelegate>

@property (strong, nonatomic) Property * propertyObj;

@property (strong, nonatomic) PSBarButtonItem *editButton;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong, nonatomic) PSBarButtonItem *backButton;
@property (strong, nonatomic) IBOutlet UIImageView *imgProperty;
@property (strong, nonatomic) UIImage *image;

@end
