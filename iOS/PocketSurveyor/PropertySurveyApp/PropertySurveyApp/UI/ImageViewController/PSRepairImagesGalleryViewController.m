//
//  PSRepairImagesGalleryViewController.m
//  PropertySurveyApp
//
//  Created by TkXel on 10/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSRepairImagesGalleryViewController.h"
#import "JobDataList+Methods.h"
#import "RepairPictures+JSON.h"
#import "Scheme+JSON.h"

#define kSyncRepairImageTag 0x123
@interface PSRepairImagesGalleryViewController ()

@end

@implementation PSRepairImagesGalleryViewController
@synthesize jobDataListObj;
@synthesize repairCompletionDictionary;
@synthesize propertyObj,schemeObj;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:@"PSCameraViewController" bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureSave:) name:kRepairPictureSaveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureSaveFailure:) name:kRepairPictureSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobStartNotificationReceive) name:kJobStartNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentStatusUpdateSuccessNotificationReceive) name:kAppointmentStatusUpdateSuccessNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onJobCompleteSuccessNotificationReceive) name:kJobCompleteSuccessNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kJobStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRepairPictureSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRepairPictureSaveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentStatusUpdateSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kJobCompleteSuccessNotification object:nil];
}

//override back button because on COmpletion JOb we have to move on JobDetail screen else just pop
-(void)popOrCloseViewController {
    if ([self.jobDataListObj.jobStatus isEqualToString:kJobStatusComplete]) {
        NSArray* viewControllers = self.navigationController.viewControllers;
        UIViewController * viewController = [viewControllers objectAtIndex:1];
        [self.navigationController popToViewController:viewController animated:YES];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BOOL  isBefore = YES;
    if (self.isJobComplete || [self.jobDataListObj.jobStatus isEqualToString:kJobStatusComplete])
    {
        isBefore = NO;
    }
    
    if(isBefore) {
        [self setTitle:@"Repair Pictures (Before)"];
    }
    else {
        [self setTitle:@"Repair Pictures (After)"];
    }
   
    [self setPropertyPictures:[NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] repairPictureListWithJSNumber:self.jobDataListObj.jsNumber beforeImage:[NSNumber numberWithBool:isBefore]]]];
    
//    for (RepairPictures* obj in self.propertyPictures) {
//        CLS_LOG(@"%@",obj.syncStatus);
//        CLS_LOG(@"%@",obj.imagePath);
//        CLS_LOG(@"%@",obj.imageId);
//        CLS_LOG(@"%@",obj.repairImagesToJobDataList.jsNumber);
//        CLS_LOG(@"%@",obj.isBeforeImage);
//
//    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Save Pictures Methods

-(void) saveImageToServer:(UIImage*) _image
{
    //create an instance for ProprtyPhoto
    //Setting status to modification for Appointments
    if(self.jobDataListObj)
    {
        [[PSDataPersistenceManager sharedManager] updateModificationStatusOfAppointments:[NSArray arrayWithObject:self.jobDataListObj.jobDataListToAppointment] toModificationStatus:[NSNumber numberWithBool:YES]];
    }
    [self showActivityIndicator:LOC(@"Saving Repair Picture")];
    
    NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
    
    [requestParameters setObject:_image forKey:kRepairImage];
    BOOL  isBefore = YES;
    if (self.isJobComplete || [self.jobDataListObj.jobStatus isEqualToString:kJobStatusComplete])
    {
        isBefore = NO;
    }
    if(self.propertyObj)
    {
        [requestParameters setObject:self.propertyObj.propertyId forKey:kPropertyId];
        [requestParameters setObject:@"0" forKey:kSchemeBlockID];//its service requirement
        [requestParameters setObject:@"0" forKey:kSchemeID];//its service requirement
    }
    else
    {
//        [requestParameters setObject:self.schemeObj.schemeId forKey:kSchemeID];
        if(!isEmpty(self.schemeObj.schemeName))
        {
            [requestParameters setObject:self.schemeObj.schemeId forKey:kSchemeID];
        }
        else
        {
            [requestParameters setObject:@"0" forKey:kSchemeID];
        }
        if(!isEmpty(self.schemeObj.blockName))
        {
            [requestParameters setObject:self.schemeObj.blockId forKey:kSchemeBlockID];
        }
        else
        {
            [requestParameters setObject:@"0" forKey:kSchemeBlockID];
        }
        [requestParameters setObject:@"" forKey:kPropertyId]; //its service requirement
    }
    [requestParameters setObject:self.jobDataListObj.jsNumber forKey:kRepairJSNumber];
    [requestParameters setObject:@".jpg" forKey:kFileExtension];
    [requestParameters setObject:[NSNumber numberWithBool:isBefore] forKey:kIsBeforeImage];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kCreatedBy];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
    [requestParameters setObject:[UtilityClass getUniqueIdentifierStringWithUserName:[[SettingsClass sharedObject]loggedInUser].userName] forKey:kUniqueImageIdentifier];
    [[PSDataPersistenceManager sharedManager] saveRepairPictureInfo:requestParameters];   
}


-(void) onPictureSave:(NSNotification*) notification
{
    [self hideActivityIndicator];
    RepairPictures * picture=notification.object;
    if (self.isJobComplete)
    {
        if ([self.jobDataListObj.jobStatus isEqualToString:kJobStatusInProgress])
        {
            [[PSDataUpdateManager sharedManager]markJobComplete:self.jobDataListObj withUpdatedValues:self.repairCompletionDictionary];
        }
    }
    else
    {
        if(![self.jobDataListObj.jobStatus isEqualToString:kJobStatusInProgress]   && ([self.jobDataListObj.jobStatus isEqualToString:kJobStatusNotStarted] || [self.jobDataListObj.jobStatus isEqualToString:kJobStatusInProgress])){
            [self.jobDataListObj startJob];
        }
    }
    
//    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_SUCCESS")
//                                                   message:LOC(@"KEY_ALERT_UPLOAD_SUCCESS_FAULT_PICTURE")
//                                                  delegate:nil
//                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
//                                         otherButtonTitles:nil,nil];
//    [alert show];
    
    if (picture) {
        [self setPropertyPictures:[NSMutableArray arrayWithArray:[[PSCoreDataManager sharedManager] repairPictureListWithJSNumber:self.jobDataListObj.jsNumber beforeImage:picture.isBeforeImage]]];
        [self.collectionView reloadData];
    }

}

-(void) onPictureSaveFailure:(NSNotification*) notification
{
    [self hideActivityIndicator];
//    [self.saveButton setEnabled:YES];
//    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
//                                                   message:LOC(@"KEY_ALERT_UPLOAD_FAILED_FAULT_PICTURE")
//                                                  delegate:nil
//                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
//                                         otherButtonTitles:nil,nil];
}


- (void) onJobStartNotificationReceive {
    
}


#pragma mark ActionSheet Methods

-(void)showActionSheet {
    
    UIActionSheet * actionsSheet=nil;
    
    RepairPictures * picture=[self.propertyPictures objectAtIndex:self.selectedIndex];
    
    if (!picture.imagePath) {
        actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil
                                           otherButtonTitles:NSLocalizedString(@"Synch", nil),
                         
                         nil] ;
        
    }
    
    actionsSheet.tag=kSyncRepairImageTag;
    actionsSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionsSheet showInView:self.view];
}


#pragma mark ActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    CLS_LOG(@"Clicked Index %d",buttonIndex);
    
    
    if (actionSheet.tag == 1) {
        if (buttonIndex != actionSheet.cancelButtonIndex) {
                if (buttonIndex == actionSheet.firstOtherButtonIndex) {
                    [self showCameraOfSourceType:UIImagePickerControllerSourceTypeCamera];
                    
                } else if (buttonIndex == actionSheet.firstOtherButtonIndex + 1) {
                    [self showCameraOfSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                }
                
            }
    }
    else if([actionSheet tag] == kSyncRepairImageTag) {
        if(buttonIndex == actionSheet.firstOtherButtonIndex) {
            RepairPictures * picture =[self.propertyPictures objectAtIndex:self.selectedIndex];
            [picture uploadPicture];
        }
    }
}


#pragma mark JobStatusNotifications

- (void) onJobCompleteSuccessNotificationReceive
{
    BOOL isAppointmentComplete = [[PSAppointmentsManager sharedManager]isFaultAppointmentComplete:self.jobDataListObj.jobDataListToAppointment];
    if (isAppointmentComplete)
    {
        [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.jobDataListObj.jobDataListToAppointment getType]] appointment:self.jobDataListObj.jobDataListToAppointment];
    }
}

- (void) onAppointmentStatusUpdateSuccessNotificationReceive
{
//    UIViewController * viewController = [self.navigationController.viewControllers objectAtIndex:1];
//    [self.navigationController popToViewController:viewController animated:YES];
    [self popToRootViewController];
}


@end
