//
//  PSAddEditDetectorProtocol.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PSAddEditDetectorProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSInteger )pickerOption;

/*!
 @discussion
 Method didSelectDate Called upon selection of date picker.
 */
- (void) didSelectDate:(NSDate *)pickerOption;

@end
