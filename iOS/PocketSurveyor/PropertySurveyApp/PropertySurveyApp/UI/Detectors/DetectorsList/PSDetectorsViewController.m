  //
  //  PSApplianceDetailViewController.m
  //  PropertySurveyApp
  //
  //  Created by Yawar on 08/11/2013.
  //  Copyright (c) 2013 TkXel. All rights reserved.
  //

#import "PSDetectorsViewController.h"
#import "PSBarButtonItem.h"
#import "PSDetectorCell.h"
#import "PSAddEditDetectorViewController.h"
#import "PSCameraViewController.h"

#define kNumberOfSections 1
#define kDetectorDetailCellHeight 150

static NSString *detectorsCellIdentifier = @"detectorsCellIdentifier";

@interface PSDetectorsViewController ()

@end

@implementation PSDetectorsViewController
{
  NSArray *_detectors;
  BOOL _noResultsFound;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
      // Custom initialization
  }
  return self;
}


- (void)viewDidLoad
{
  [super viewDidLoad];

  [self registerNibsForTableView];
	
}

- (void) viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
	[self loadNavigationonBarItems];
  _detectors = [NSArray array];
  _detectors = [self.appointment.appointmentToProperty.propertyToDetector allObjects];
	NSInteger detectorTypeId = self.detectorType;
	_detectors = [_detectors filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"detectorTypeId = %ld", (long)detectorTypeId]]];
  [self.tableView reloadData];

}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];

}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}



#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
  PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                       style:PSBarButtonItemStyleBack
                                                                      target:self
                                                                      action:@selector(onClickBackButton)];
  
  self.addDetectorBtn = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NEW")
                                                               style:PSBarButtonItemStyleAdd
                                                              target:self
                                                              action:@selector(onClickNewDetectorButton)];
	
	//Camera Button
	
	NSString * badgeText=[NSString stringWithFormat:@"%lu",(unsigned long)[self getPhotographsCount]];
	
	self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																														 style:PSBarButtonItemStyleCamera
																														target:self
																														action:@selector(onClickCameraButton)bagde:badgeText];
	
	NSMutableArray *rightBarButtonItems = [NSMutableArray array];
	[rightBarButtonItems addObjectsFromArray:@[self.addDetectorBtn, self.cameraButton]];	
  [self setRightBarButtonItems:rightBarButtonItems];
  [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	if(self.detectorType == DetectorTypeCO)
	{
		[self setTitle:@"CO Detectors"];
	}
	else
	{
		[self setTitle:@"Smoke Detectors"];
	}
	
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
  CLS_LOG(@"onClickBackButton");
  [self popOrCloseViewController];
}

- (void) onClickNewDetectorButton
{
  CLS_LOG(@"onClickNewDetectorButton");
	Detector *detector = nil;
  PSAddEditDetectorViewController *addEditDetectorViewController = [[PSAddEditDetectorViewController alloc] initWithNibName:NSStringFromClass([PSAddEditDetectorViewController class]) bundle:nil];
  [addEditDetectorViewController setAppointment:self.appointment];
	[addEditDetectorViewController setDetector:detector];
  addEditDetectorViewController.detectorNewId = [NSNumber numberWithInt:(-1)* ((int)self.appointment.appointmentToProperty.propertyToDetector.count +1)];
  [self.navigationController pushViewController:addEditDetectorViewController animated:YES];
}


#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return kNumberOfSections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  CGFloat rowHeight =kDetectorDetailCellHeight;
  return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  NSInteger rows = 0;
  
  
  if (!isEmpty(_detectors))
    {
    rows =[_detectors count];
    _noResultsFound = NO;
    }
  else
    {
    rows = 1;
    _noResultsFound = YES;
    }
  
  return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = nil;
  
  if(_noResultsFound)
    {
    static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
    UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
    if(_cell == nil)
      {
      _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
      }
    
    _cell.textLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
    _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
    _cell.textLabel.text = LOC(@"KEY_STRING_NO_DETECTOR");
    _cell.textLabel.textAlignment = PSTextAlignmentCenter;
    _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    cell = _cell;
    }
  
  else
    {
    PSDetectorCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSDetectorCell class])];
    Detector *detector;
    if (!isEmpty(_detectors))
      {
      detector = [_detectors objectAtIndex:indexPath.row];
      }
    
    [_cell configureCell:detector];
    cell = _cell;
    }

  return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  if (!_noResultsFound)
    {
    Detector *detector = [_detectors objectAtIndex:indexPath.row];
			if (detector.detectorId == [NSNumber numberWithInt:0]) {
				detector.detectorId = [NSNumber numberWithInt:(-1)* [detector.detectorTypeId intValue]];
			}
        PSAddEditDetectorViewController *editDetectorViewController = [[PSAddEditDetectorViewController alloc] initWithNibName:NSStringFromClass([PSAddEditDetectorViewController class]) bundle:nil];
        [editDetectorViewController setDetector:detector];
        [editDetectorViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:editDetectorViewController animated:YES];
    }
}

#pragma mark - Methods

-(void)registerNibsForTableView
{
  NSString * nibName = NSStringFromClass([PSDetectorCell class]);
  [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

-(NSUInteger) getPhotographsCount
{
	NSArray * photographsList =[[PSCoreDataManager sharedManager] propertyPicturesWithPropertyIdentifier:self.appointment.appointmentToProperty.propertyId appointmentId:self.appointment.appointmentId itemId:self.itemId andHeatingId:nil];
	return [photographsList count];
}

- (void)onClickCameraButton {
	CLS_LOG(@"Camera Button");
	PSCameraViewController *cameraViewController = [[PSCameraViewController alloc] initWithNibName:@"PSCameraViewController" bundle:[NSBundle mainBundle]];
	[cameraViewController setMainViewController:self.mainViewController];
	[cameraViewController setImageType:CameraViewImageTypeOther];
	[cameraViewController setAppointment:self.appointment];
	[cameraViewController setItemId:self.itemId];
	
	[self.navigationController pushViewController:cameraViewController animated:YES];
	
}

@end
