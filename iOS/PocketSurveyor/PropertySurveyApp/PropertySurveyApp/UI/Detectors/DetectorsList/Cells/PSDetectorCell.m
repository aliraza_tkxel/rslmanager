  //
  //  PSDetectorDetailCell.m
  //  PropertySurveyApp
  //
  //  Created by Yawar on 08/11/2013.
  //  Copyright (c) 2013 TkXel. All rights reserved.
  //

#import "PSDetectorCell.h"

@implementation PSDetectorCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
      // Initialization code
    
  }
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
  [super setSelected:selected animated:animated];
  
    // Configure the view for the selected state
}

- (void) configureCell: (id)data
{
  [self.lblTitleType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  [self.lblTitleLocation setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblLocation setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  [self.lblTitlePower setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.lblPower setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
  
  [self.lblLocation setText:nil];
  [self.lblType setText:nil];
  [self.lblPower setText:nil];

  Detector *detector = (Detector *) data;
  if (!isEmpty(detector))
    {
		[self.lblType setText:detector.detectorType];
		[self.lblLocation setText:detector.location];
		[self.lblPower setText:detector.detectorToPowerType.powerType];
    
        [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    }
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

@end
