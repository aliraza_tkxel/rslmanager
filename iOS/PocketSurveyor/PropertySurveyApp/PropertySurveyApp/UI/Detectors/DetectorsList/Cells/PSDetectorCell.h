//
//  PSDetectorDetailCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSDetectorCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitleType;
@property (strong, nonatomic) IBOutlet UILabel *lblType;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblTitlePower;
@property (strong, nonatomic) IBOutlet UILabel *lblPower;
- (void) configureCell: (id)data;
@end
