//
//  PSApplianceDetailViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSSurveyViewController.h"

@interface PSDetectorsViewController : PSCustomViewController
@property (strong, nonatomic) PSBarButtonItem *addDetectorBtn;
@property (weak,   nonatomic) Appointment *appointment;
@property (strong, nonatomic) NSNumber *itemId;
@property (nonatomic, assign) PropertyDetectorType detectorType;
@property (strong, nonatomic) PSBarButtonItem *cameraButton;
@property (weak,   nonatomic) PSSurveyViewController *mainViewController;

@end
