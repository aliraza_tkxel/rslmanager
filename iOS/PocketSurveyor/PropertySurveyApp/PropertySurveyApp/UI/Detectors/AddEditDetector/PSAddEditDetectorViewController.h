//
//  PSAddEditDetectorViewController.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/9/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextViewCell.h"
#import "PSBarButtonItem.h"
#import "PSAEDTextFieldCell.h"
#import "PSAEDPickerCell.h"
#import "PSAEDDatePickerCell.h"
#import "PSTextViewCell.h"
#import "PSAddEditDetectorProtocol.h"

@interface PSAddEditDetectorViewController : PSCustomViewController <PSAddEditDetectorProtocol, UITextFieldDelegate>
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutlet PSTextViewCell *textViewCell;
@property (weak,   nonatomic) Appointment *appointment;
@property (nonatomic,strong) NSNumber *detectorNewId;
@property (weak,   nonatomic) Detector *detector;
@end
