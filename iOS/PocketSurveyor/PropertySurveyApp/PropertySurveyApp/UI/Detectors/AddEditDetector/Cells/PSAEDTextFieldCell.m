//
//  PSAddApplianceTextFieldCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAEDTextFieldCell.h"

@implementation PSAEDTextFieldCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


- (IBAction)onClickEditBtn:(id)sender
{
    [self.txtTitle becomeFirstResponder];
}

#pragma mark - Methods

- (void) configureCell{

  [self.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
  [self.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];

}

- (BOOL) isValid
{
	  BOOL isvalid=YES;
	  NSString *title = self.txtTitle.text;
    if (isEmpty(title.trimString))
    {
        isvalid = NO;
		}
    
    return isvalid;
}
@end
