//
//  PSChangePasswordCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSChangePasswordCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *txtField;
@property (strong, nonatomic) IBOutlet UIImageView *imgIcon;

-(void) reloadData:(NSIndexPath*)indexPath withText:(NSString *)text;
- (BOOL) isValid;
@end
