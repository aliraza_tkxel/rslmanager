//
//  PSChangePasswordCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSChangePasswordCell.h"

#define kRowUserName 0
#define kRowOldPassword 1
#define kRowNewPassword 2
#define kRowConfirmNewPassword 3

@implementation PSChangePasswordCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void) reloadData:(NSIndexPath*)indexPath withText:(NSString *)text
{
    switch (indexPath.row)
    {
            [self.txtField layoutSubviews];
        case kRowUserName:
            [self.txtField setPlaceholder:@"Username"];
            [self.imgIcon setImage:[UIImage imageNamed:@"icon_person"]];
            [self.txtField setTag:kRowUserName];
            [self.txtField setSecureTextEntry:NO];
            [self.txtField setText:text];
            break;
        case kRowOldPassword:
            [self.txtField setPlaceholder:@"Old Password"];
            [self.imgIcon setImage:[UIImage imageNamed:@"icon_padlock_violet"]];
            [self.txtField setTag:kRowOldPassword];
            [self.txtField setSecureTextEntry:YES];
            [self.txtField setText:text];
            break;
        case kRowNewPassword:
            [self.txtField setPlaceholder:@"New Password"];
            [self.imgIcon setImage:[UIImage imageNamed:@"icon_padlock_green"]];
            [self.txtField setTag:kRowNewPassword];
            [self.txtField setSecureTextEntry:YES];
            [self.txtField setText:text];
            break;
        case kRowConfirmNewPassword:
            [self.txtField setPlaceholder:@"Confirm New Password"];
            [self.imgIcon setImage:[UIImage imageNamed:@"icon_padlock_green"]];
            [self.txtField setTag:kRowConfirmNewPassword];
            [self.txtField setSecureTextEntry:YES];
            [self.txtField setText:text];
            break;
        default:
            break;
    }
}

/*- (void) drawPlaceholderInRect:(CGRect)rect
{
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [attributes setObject:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:16] forKey:NSFontAttributeName];
    [attributes setObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [[UIColor whiteColor] setFill];
    [[self.txtField placeholder] drawInRect:rect withAttributes:attributes];
}*/

- (BOOL) isValid
{
    BOOL isValid = YES;
    if (isEmpty(self.txtField.text))
    {
        isValid = NO;
    }
    return isValid;
}
@end
