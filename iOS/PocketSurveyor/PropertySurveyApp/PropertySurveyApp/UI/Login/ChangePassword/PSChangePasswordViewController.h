//
//  PSChangePasswordViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 09/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PSChangePasswordCell;

@interface PSChangePasswordViewController : PSCustomViewController<UIGestureRecognizerDelegate, UITextFieldDelegate>
{
    BOOL isKeyboardShown;
}
@property (strong, nonatomic) IBOutlet UILabel *lblChangePassword;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (strong,  nonatomic) IBOutlet PSChangePasswordCell *changePasswordCell;
- (IBAction)onClickChangePassword:(id)sender;
- (IBAction)onClickLoginAgain:(id)sender;
@end
