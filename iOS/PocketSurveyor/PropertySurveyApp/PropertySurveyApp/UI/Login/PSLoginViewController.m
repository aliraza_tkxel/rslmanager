//
//  PSLoginViewController.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSLoginViewController.h"
#import "PSAppDelegate.h"
#import "PSForgotPasswordViewController.h"
#import "PSChangePasswordViewController.h"
#import <KLCPopup.h>
#import "PSPromoVideoPlayer.h"
@interface PSLoginViewController ()

@end

@implementation PSLoginViewController

@synthesize txtPassword;
@synthesize txtUsername;
@synthesize swtStayLoggedIn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setFrame:[[UIScreen mainScreen] bounds]];
    
    
    UIImage* bgImage = nil;
    if(IS_IPHONE_5)
    {
        bgImage = [UIImage imageNamed:@"LoginBG-568h"];
        self.containerView.center = CGPointMake(self.containerView.center.x, self.view.center.y + 30);
    }
    else
    {
        bgImage = [UIImage imageNamed:@"LoginBG"];
        self.containerView.center = self.view.center;

    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    CGRect updateViewRect = self.upDateView.frame;
    updateViewRect.origin.y = self.btnLogin.frame.origin.y + self.btnLogin.frame.size.height;
    if(OS_VERSION >= 7.0)
    {
        CLS_LOG(@"Setting Login Switch Frame");
        CGRect switchFrame = self.swtStayLoggedIn.frame;
        CGFloat xPoint =  txtPassword.frame.origin.x + txtPassword.frame.size.width;
        CGFloat switchXPoint = (xPoint - switchFrame.size.width) + 5;
        CGFloat labelXPoint = (xPoint - self.lblStayLoggedIn.frame.size.width - switchFrame.size.width);
        self.swtStayLoggedIn.frame = CGRectMake(switchXPoint,
                                                switchFrame.origin.y,
                                                switchFrame.size.width,
                                                switchFrame.size.height);
        
        self.lblStayLoggedIn.frame = CGRectMake(labelXPoint,
                                                self.lblStayLoggedIn.frame.origin.y,
                                                self.lblStayLoggedIn.frame.size.width,
                                                self.lblStayLoggedIn.frame.size.height);
        updateViewRect.origin.y +=5;
    }
    else
    {
        updateViewRect.origin.y -=10;
        
    }
   
    
    [self.upDateView setFrame: updateViewRect];
    self.lblStayLoggedIn.text = LOC(@"KEY_STRING_STAY_LOGGED_IN");
    
    UIImageView* bgImageView = [[UIImageView alloc] initWithImage:bgImage];
    [bgImageView setFrame:[[UIScreen mainScreen] bounds]];
    [bgImageView setContentMode:UIViewContentModeScaleAspectFit];
    [bgImageView setBackgroundColor:[UIColor clearColor]];
    [bgImageView setTag:kBackgroundImageTag];
    
    [self.view insertSubview:bgImageView atIndex:0];
    
    self.txtUsername.textColor= UIColorFromHex(TEXT_FIELD_TEXT_COLOUR);
    self.txtPassword.textColor= UIColorFromHex(TEXT_FIELD_TEXT_COLOUR);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapView:)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate = self;
    [self.view addGestureRecognizer:tapGesture];
    [self addVersionBuildNumberOnRightBottom];
    isKeyboardShown = NO;
    [self showBannerForMyBroadland];
}

-(void)addVersionBuildNumberOnRightBottom
{
    CGSize screen = [UIScreen mainScreen].bounds.size;
    NSString * text = [UtilityClass getAPPVersionString];
    
    UILabel *versionLabel = [[UILabel alloc]initWithFrame:CGRectMake(screen.width-160, screen.height-14, 155, 14)];
    versionLabel.text = text;
    versionLabel.numberOfLines = 1;
    versionLabel.backgroundColor = [UIColor clearColor];
    versionLabel.textColor = [UIColor whiteColor];
    versionLabel.textAlignment = NSTextAlignmentRight;
    versionLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:12];
    [self.view addSubview:versionLabel];
    [self.view bringSubviewToFront:versionLabel];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[PSAppDelegate delegate] setUserLoggedInToApplication:NO];
    [self registerNotifications];
    [self.swtStayLoggedIn setOn:[SettingsClass sharedObject].autoLogin];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [self setTxtUsername:nil];
    [self setTxtPassword:nil];
    [self setSwtStayLoggedIn:nil];
    [self setContainerView:nil];
    [self setActivityIndicator:nil];
}


#pragma mark - UIKeyboard Show/Hide Notifications
- (void)keyboardWillHide:(NSNotification *)n
{
    CGRect viewFrame = CGRectMake(self.view.frame.origin.x,
                                  0,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    isKeyboardShown = NO;
}

- (void)keyboardWillShow:(NSNotification *)n
{
    if (isKeyboardShown) {
        return;
    }
    CGRect viewFrame = CGRectMake(self.view.frame.origin.x,
                                  -50,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height);
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    // The kKeyboardAnimationDuration I am using is 0.3
    [UIView setAnimationDuration:0.3];
    [self.view setFrame:viewFrame];
    [UIView commitAnimations];
    
    isKeyboardShown = YES;
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    BOOL shouldReceiveTouch = YES;
    if(touch.view == txtUsername ||
       touch.view == txtPassword)
    {
        shouldReceiveTouch = NO;
    }
    return shouldReceiveTouch;
}

- (void) onTapView:(id)sender
{
    if(isKeyboardShown)
    {
        //Will Resign All Responders on the Screen.
        [self.view endEditing:YES];
    }
}


#pragma mark - Notifications
- (void)loginSuccess:(NSNotification *)notification {
    [self.activityIndicator stopAnimating];
    [CrashlyticsKit setUserName:txtUsername.text];
    [[PSAppDelegate delegate] performSelector:@selector(showAppointmentsView) withObject:nil afterDelay:0.5];
}

-(void)loginFailed:(NSNotification *)notification {
    NSDictionary *responseDictionary = [notification object];
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_SOMETHING_WENT_WRONG") message:[responseDictionary valueForKey:@"message"] delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
    [alert show];
    
}

-(void)unableToAccess:(NSNotification *)notification {
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:LOC(@"KEY_ALERT_UNABLE_TO_ACCESS_SERVER") delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
    [alert show];
    
}

-(void)internetUnavailable:(NSNotification *)notification {
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:LOC(@"KEY_ALERT_NO_INTERNET") delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
    [alert show];
}

-(void)onOfflineAuthenticationFailureNotificationReceive{
    
    //Replace with offline authentication failure messages
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:LOC(@"KEY_ALERT_OFFLINE_AUTHENTICATION_FAILURE") delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
    [alert show];
}
#pragma mark - IBActions
- (IBAction)loginMe:(id)sender
{
/******************************************/
//	txtUsername.text = @"760.760";
//	txtPassword.text = @"Broadland760@!";
/******************************************/
//	txtUsername.text = @"113.113";
//	txtPassword.text = @"Broadland113@!";
/******************************************/
//    txtUsername.text = @"789.789";
//    txtPassword.text = @"Broadland789@!";
	if(isEmpty(txtUsername.text))
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:LOC(@"KEY_ALERT_USERNAME_EMPTY") delegate:nil cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
		[alert show];
		return;
	}
	else if(isEmpty(txtPassword.text))
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:LOC(@"KEY_ALERT_PASSWORD_EMPTY") delegate:nil cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
		[alert show];
		return;
	}
	
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.activityIndicator startAnimating];
	});
	
	
	[[SettingsClass sharedObject] setAutoLogin:swtStayLoggedIn.on];
	if ([[ReachabilityManager sharedManager]isConnectedToInternet])
	{
        NSData *dataToEncode = [txtPassword.text dataUsingEncoding:NSUTF8StringEncoding];
        NSData *encodedData = [dataToEncode base64EncodedDataWithOptions:0];
        NSString *encodedString = [[NSString alloc] initWithData:encodedData encoding:NSUTF8StringEncoding];

		[[PSAuthenticator sharedAuthenticator] authenticateUserWithName:txtUsername.text password:encodedString];
	}
	else
	{
        NSData *dataToEncode = [txtPassword.text dataUsingEncoding:NSUTF8StringEncoding];
        NSData *encodedData = [dataToEncode base64EncodedDataWithOptions:0];
        NSString *encodedString = [[NSString alloc] initWithData:encodedData encoding:NSUTF8StringEncoding];

		[[PSAuthenticator sharedAuthenticator]authenticateUserOfflineWithName:txtUsername.text password:encodedString];
	}
}

- (IBAction)resignFirstResponder:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    [textField resignFirstResponder];    
}

- (IBAction)onClickForgotPassword:(id)sender
{
    PSForgotPasswordViewController *forgotPasswordViewController = [[PSForgotPasswordViewController alloc] initWithNibName:@"PSForgotPasswordViewController" bundle:nil];
    [self.navigationController pushViewController:forgotPasswordViewController animated:YES];
}

- (IBAction)onClickChangePassword:(id)sender
{
    PSChangePasswordViewController *changePasswordViewController = [[PSChangePasswordViewController alloc] initWithNibName:@"PSChangePasswordViewController" bundle:nil];
    [self.navigationController pushViewController:changePasswordViewController animated:YES];
}

#pragma mark Private Methods

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    
    
    // listen for success login notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginSuccess:)
                                                 name:kAuthorizationSuccessNotification
                                               object:nil];
    
    // listen for deleted document from server notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginFailed:)
                                                 name:kAuthorizationFailureNotification
                                               object:nil];
    
    // listen for deleted document from server notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(unableToAccess:)
                                                 name:kCommonUnableToAccessServices
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(internetUnavailable:)
                                                 name:kInternetUnavailable
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onOfflineAuthenticationFailureNotificationReceive)
                                                 name:kOfflineLoginFailureReceiveNotification
                                               object:nil];

}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAuthorizationSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAuthorizationFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCommonUnableToAccessServices object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kInternetUnavailable object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kOfflineLoginFailureReceiveNotification object:nil];
}

#pragma mark UIAlertViewDelegate 

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self.activityIndicator stopAnimating];
}
#pragma mark - Promo

-(void) showBannerForMyBroadland{
    PSPromoVideoPlayer *vp = [[PSPromoVideoPlayer alloc] initWithFrame:CGRectMake(0, 0, 290, 148)];
    KLCPopup* popup = [KLCPopup popupWithContentView:vp];
    [popup show];
    [vp playPromoVideoWithName:@"mb_promo_vid_2.m4v"];
}

@end
