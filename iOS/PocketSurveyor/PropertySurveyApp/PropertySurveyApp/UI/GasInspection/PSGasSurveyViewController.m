//
//  PSGasSurveyViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 04/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSGasSurveyViewController.h"
#import "PSGasSurveyCell.h"
#import "PSIssueReceivedViewController.h"
#import "PSInstallationPipeworkViewController.h"
#import "PSDefectsViewController.h"
#import "PSApplianceDetailViewController.h"
#import "PSBarButtonItem.h"
#import "Appointment+JSON.h"

#define kTotalNumberOfRows 4
#define kRowApplianceDetails 0
#define kRowGasPipeWork 1
#define kRowDefects 2
#define kRowIssued 3

@interface PSGasSurveyViewController ()
{
    NSArray *_cellLabels;
    BOOL _isApplianceInspectionComplete;
    BOOL _isInstallationPipeworkComplete;
    BOOL _isDefectInspectionComplete;
    BOOL _isIssuedReceivedByComplete;
}

@end

@implementation PSGasSurveyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadSurveyUI];
    
    [self loadNavigationonBarItems];
    [self.btnMarkComplete setEnabled:NO];
    _cellLabels = @[LOC(@"KEY_STRING_APPLIANCE_DETAILS"), LOC(@"KEY_STRING_GAS_INSTALLATION_PIPEWORK"), LOC(@"KEY_STRING_DEFECTS"), LOC(@"KEY_STRING_ISSUED_RECEIVED_BY")];
    // Do any additional setup after loading the view from its nib.
    _isApplianceInspectionComplete = NO;
    _isInstallationPipeworkComplete = NO;
    _isDefectInspectionComplete = NO;
    _isIssuedReceivedByComplete = NO;
    
    [self.imageView setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
    
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    
}



-(void)onPictureDefault:(NSNotification*) notification
{
    [self.imageView setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotifications];
    // NSNumber *inspectedAppliances = [[PSApplianceManager sharedManager] totalNumberOfAppliancesInspected:self.appointment.appointmentToProperty];
    
    [self onPictureDefault:nil];
    
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
     [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:surveyTitle];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewTagMarkComplete)
    {
       	if(buttonIndex == alertView.firstOtherButtonIndex)
        {
            //[self showActivityIndicator:LOC(@"KEY_STRING_PROCESSING")];
            
            self.appointment.appointmentStatus = [UtilityClass statusStringForAppointmentStatus:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]]];
            [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]] appointment:self.appointment];
            
//            NSDictionary *appointmentDictionary = [self.appointment JSONValue];
//            NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
//            NSMutableArray *appointmentArray = [NSMutableArray array];
//            [appointmentArray addObject:appointmentDictionary];
//            [requestParameters setObject:appointmentArray forKey:@"appointments"];
//            [requestParameters setObject:[NSNumber numberWithBool:YES] forKey:@"respondWithData"];
//            [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
//            [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
//            
//            if([[ReachabilityManager sharedManager] isConnectedToInternet])
//            {
//                [[PSAppointmentsManager sharedManager] completeAppointment:requestParameters];
//            }
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    /*
     The count check ensures that if array is empty, i.e no record exists
     for particular section, the header height is set to zero so that
     section header is not alloted any space.
     */
    CGFloat headerHeight = 0.0;
    
    return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight =44;
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kTotalNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    static NSString *gasSurveyCellIdentifier = @"gasSurveyCellIdentifier";
    PSGasSurveyCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:gasSurveyCellIdentifier];
    
    if (cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"PSGasSurveyCell" owner:self options:nil];
        _cell = self.gasSurveyCell;
        self.gasSurveyCell = nil;
    }
    [self configureCell:&_cell atIndexPath:indexPath];
    cell = _cell;
    cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSGasSurveyCell class]])
    {
        
        PSGasSurveyCell *_cell = (PSGasSurveyCell *) *cell;
        [_cell.lblTitle setText:[_cellLabels objectAtIndex:indexPath.row]];
        _cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        BOOL isInspected = NO;
        switch (indexPath.row)
        {
            case kRowApplianceDetails:
                isInspected = [[PSApplianceManager sharedManager] isApplianceInspected:self.appointment.appointmentToProperty];
                _isApplianceInspectionComplete = isInspected;
                break;
            case kRowGasPipeWork:
                isInspected = !isEmpty(self.appointment.appointmentToProperty.installationPipework);//[self.appointment.appointmentToProperty.installationPipework.isInspected boolValue];
                _isInstallationPipeworkComplete = isInspected;
                break;
            case kRowDefects:
                isInspected = [[PSApplianceManager sharedManager]isDefectAdded:self.appointment.journalId];
                _isDefectInspectionComplete = isInspected;
                break;
            case kRowIssued:
                isInspected = [self.appointment.appointmentToCP12Info.inspectionCarried boolValue];
                _isIssuedReceivedByComplete = isInspected;
                break;
            default:
                break;
        }
        
        if (isInspected)
        {
            _cell.imgStatus.image = [UIImage imageNamed:@"icon_Complete"];
        }
        else
        {
            _cell.imgStatus.image = [UIImage imageNamed:@"icon_Cross"];
        }
        if (_isIssuedReceivedByComplete && _isInstallationPipeworkComplete && _isApplianceInspectionComplete)
        {
            [self.btnMarkComplete setEnabled:YES];
            [self.btnMarkComplete setBackgroundColor:[UIColor redColor]];
        }
        else
        {
            [self.btnMarkComplete setEnabled:NO];
            [self.btnMarkComplete setBackgroundColor:[UIColor darkGrayColor]];
        }
        *cell = _cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == kRowApplianceDetails)
    {
        PSApplianceDetailViewController *applianceDetailViewController = [[PSApplianceDetailViewController alloc] initWithNibName:@"PSApplianceDetailViewController" bundle:nil];
        [applianceDetailViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:applianceDetailViewController animated:YES];
    }
    
    else if (indexPath.row == kRowGasPipeWork)
    {
        PSInstallationPipeworkViewController *pipeInstallationViewController = [[PSInstallationPipeworkViewController alloc] initWithNibName:@"PSInstallationPipeworkViewController" bundle:nil];
        [pipeInstallationViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:pipeInstallationViewController animated:YES];
    }
    
    else if (indexPath.row == kRowDefects)
    {
        PSDefectsViewController *defectsViewController = [[PSDefectsViewController alloc] initWithNibName:@"PSDefectsViewController" bundle:nil];
        [defectsViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:defectsViewController animated:YES];
    }
    
    else if (indexPath.row == kRowIssued)
    {
        PSIssueReceivedViewController *issueReceivedViewController = [[PSIssueReceivedViewController alloc] initWithNibName:@"PSIssueReceivedViewController" bundle:nil];
        [issueReceivedViewController setCp12Info:self.appointment.appointmentToCP12Info];
        [issueReceivedViewController setAppointment:self.appointment];
        [self.navigationController pushViewController:issueReceivedViewController animated:YES];
    }
}

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentSuccessNotificationReceive) name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentFailureNotificationReceive) name:kCompleteAppointmentFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentFailureNotification object:nil];
}

- (void) onCompleteAppointmentSuccessNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

- (void) onCompleteAppointmentFailureNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        self.appointment.appointmentStatus = kAppointmentStatusInProgress;

        [self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
                         message:LOC(@"KEY_ALERT_FAILED_TO_COMPLETE_APPOINTMENT")
                             tag:0
               cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
               otherButtonTitles:nil];
    });
}

#pragma mark - Methods

- (void)loadSurveyUI {
    Customer *customer = [self.appointment defaultTenant];
    self.lblTenantName.text = [customer fullName];
    
    Property *property = [self.appointment appointmentToProperty];
    self.lblAddress.text = [property addressWithStyle:PropertyAddressStyle1];
    if (!isEmpty(self.appointment.appointmentToProperty.certificateExpiry))
    {
        [self.lblCertificateExpiry setHidden:NO];
        [self.lblCertificateExpiry setText:[UtilityClass stringFromDate:self.appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]];
    }
    else
    {
        [self.lblCertificateExpiry setHidden:YES];
    }
    if ([self.appointment getType] == AppointmentTypeStock) {
        surveyTitle = @"Stock Condition Survey";
        self.lblCertificateExpiry.hidden = YES;
    }
    else if ([self.appointment getType] == AppointmentTypeGas) {
        surveyTitle = @"Gas Survey";
    }
    else if ([self.appointment getType] == AppointmentTypeFault) {
        surveyTitle = @"Fault Survey";
    }
    [self.btnBack addTarget:self action:@selector(onClickBackButton:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)onClickBackButton:(id)sender {
    [self popOrCloseViewController];
}

- (IBAction)onClickCompleteBtn:(id)sender
{
    CLS_LOG(@"on click complete button");
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_COMPLETE_APPOINTMENT") message:LOC(@"KEY_ALERT_CONFIRM_COMPLETE_APPOINTMENT") delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_NO") otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
    alert.tag = AlertViewTagMarkComplete;
    [alert show];
    //NSDictionary *appointmentDictionary = [self.appointment JSONValue];
}

-(void) dealloc
{
   // [[NSNotificationCenter defaultCenter] removeObserver:self forKeyPath:kPropertyPictureDefaultNotification];
    
}
@end
