//
//  PSPipeworkPickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSPipeworkPickerCell.h"

@implementation PSPipeworkPickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClickPickerBtn:(id)sender
{
    self.btnPicker = (UIButton *) sender;
    CLS_LOG(@"Picker button tag: %ld",(long)self.btnPicker.tag);
    // [self _showPickerView];{
    if(!isEmpty(self.pickerOptions))
    {
        [ActionSheetStringPicker showPickerWithTitle:self.lblHeading.text
                                                rows:self.pickerOptions
                                    initialSelection:self.selectedIndex
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }

}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self.selectedIndex = [selectedIndex integerValue];
    self.lblValue.text = [self.pickerOptions objectAtIndex:self.selectedIndex];
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelecPickerOption:)])
    {
        [(PSInstallationPipeworkViewController *)self.delegate didSelecPickerOption:self.lblValue.text];
    }
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

@end
