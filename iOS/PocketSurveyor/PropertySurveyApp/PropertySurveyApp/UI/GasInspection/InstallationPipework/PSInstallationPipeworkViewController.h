//
//  PSInstallationPipeworkViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"
@class PSPipeworkPickerCell;

@protocol PSPipeworkProtocol <NSObject>
@required

/*!
 @discussion
 Method didSelecPickerOption Called upon selection of spicker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption;


@end
@interface PSInstallationPipeworkViewController : PSCustomViewController <PSPipeworkProtocol>
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (weak,   nonatomic) IBOutlet PSPipeworkPickerCell *pipeworkCell;
@property (weak,   nonatomic) Appointment *appointment;
@end
