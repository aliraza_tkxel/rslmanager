//
//  PSAddDefectsViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"
@class PSAddDefectPickerCell;
@class PSAddDefectTextFieldCell;
@class PSAddDefectTextViewCell;

@protocol PSAddDefectProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSInteger)pickerOption;
@end

typedef NS_ENUM(NSInteger, ViewMode)
{
    ApplianceViewMode,
    DetectorViewMode
};

@interface PSAddDefectsViewController : PSCustomViewController <PSAddDefectProtocol, UITextFieldDelegate, UITextViewDelegate>
@property (assign, nonatomic) ViewMode viewMode;
@property (weak,   nonatomic) IBOutlet PSAddDefectPickerCell *addDefectPcikerCell;
@property (weak,   nonatomic) IBOutlet PSAddDefectTextFieldCell *addDefectTextFieldCell;
@property (weak,   nonatomic) IBOutlet PSAddDefectTextViewCell *addDefectTextViewCell;
@property (strong, nonatomic) IBOutlet PSTextViewCell *textViewCell;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (weak,   nonatomic) Appointment *appointment;
@property (weak,   nonatomic) Defect *defect;
@property ( nonatomic,strong) NSNumber *  defectNewID;
@end
