//
//  PSAddDefectPickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAddDefectsViewController.h"

@interface PSAddDefectPickerCell : UITableViewCell
@property (weak,    nonatomic) id<PSAddDefectProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
- (IBAction)onClickPickerBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (assign, nonatomic) NSInteger selectedIndex;

@end
