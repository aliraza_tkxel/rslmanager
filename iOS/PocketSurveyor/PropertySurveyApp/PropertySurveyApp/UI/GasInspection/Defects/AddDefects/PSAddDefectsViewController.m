//
//  PSAddDefectsViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddDefectsViewController.h"
#import "PSAddDefectTextViewCell.h"
#import "PSAddDefectTextFieldCell.h"
#import "PSAddDefectPickerCell.h"

#import "PSCameraViewController.h"

#import "PSTextViewCell.h"
#import "Defect+JSON.h"

#define kNumberOfRows 9
#define kRowDefectCategory 0
#define kRowDefectIdentified 1
#define kRowDefectNotes 2
#define kRowRemedialAction 3
#define kRowRemedialNotes 4
#define kRowWarningIssued 5
#define kRowSerialNumber 6
#define kRowWarningStickerFixed 7
#define kRowSelectAppliance 8

#define kDefectPickerCellHeight 44
#define kDefectTextFieldCellHeight 44
#define kDefectTextViewCellHeight 80

static NSString *pickerCellIdentifier = @"addDefectPickerCellIdentifier";
static NSString *textFieldCellIdentifier = @"textFieldCellIdentifier";

@interface PSAddDefectsViewController ()

@property(nonatomic,strong) PSBarButtonItem * cameraButton;
@end

@implementation PSAddDefectsViewController
{
    BOOL _isDefectGeneralComment;
    BOOL _isDefectIdentified;
    NSString *_isDefectIdentifiedString;
    BOOL _isActionTaken;
    NSString *_isActionTakenString;
    BOOL _isWarningIssued;
    NSString *_isWarningIssuedString;
    BOOL _isStickerFixed;
    NSString *_isStickerFixedString;
    NSString *_serialNumber;
    NSString *_remedialNotes;
    NSString *_defectNotes;
    NSString *_defectCategoryDescription;
    NSString *_applianceName;
    NSInteger _pickerTag;
    
    DefectCategory *_defectCategory;
    Appliance *_appliance;
    Detector *_detector;
    NSArray *_pickerOptions;
    NSArray *_defectCategories;
    NSArray *_appliances;
    NSArray *_detectors;
    NSMutableArray *_defectCategoriesStrings;
    NSMutableArray *_appliancesNames;
    NSMutableArray *_detectorNames;
    NSMutableDictionary *_defectDictionary;
    BOOL _isSavingFromCamera;
    //PSTextView *_textView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerKeyboardNotifications];
    [self loadNavigationonBarItems];
    _pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
    NSSortDescriptor *sortDescriptor;
    NSArray *sortDescriptors;
    _defectCategories = [[PSApplianceManager sharedManager] fetchAllDefectCategories];
    if (!sortDescriptor)
    {
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kCategoryDescription ascending:YES];
        sortDescriptors = @[sortDescriptor];
        
    }
    _defectCategories = [NSMutableArray arrayWithArray:[_defectCategories sortedArrayUsingDescriptors:sortDescriptors]];
    
    //_appliances = [self.appointment.appointmentToProperty.propertyToAppliances allObjects];
    // _detectors = [self.appointment.appointmentToProperty.propertyToDetector allObjects];
    _defectCategoriesStrings = [NSMutableArray array];
    _appliancesNames =[NSMutableArray array];
    _detectorNames = [NSMutableArray array];
    _defectDictionary = [NSMutableDictionary dictionary];
    if (!isEmpty(self.defect))
    {
        [self loadDefaultValues];
    }
    for (DefectCategory *defectCategory in _defectCategories)
    {
        if (!isEmpty(defectCategory.categoryDescription))
        {
            [_defectCategoriesStrings addObject:defectCategory.categoryDescription];
        }
    }
    if (self.viewMode == ApplianceViewMode)
    {
        _appliances = [self.appointment.appointmentToProperty.propertyToAppliances allObjects];
        for (Appliance *appliance in _appliances)
        {
            if (!isEmpty(appliance.applianceType.typeName))
            {
                [_appliancesNames addObject:appliance.applianceType.typeName];
            }
        }
    }
    else if (self.viewMode == DetectorViewMode)
    {
        _detectors = [self.appointment.appointmentToProperty.propertyToDetector allObjects];
        for (Detector *detector in _detectors)
        {
            if (!isEmpty(detector))
            {
                [_detectorNames addObject:detector.detectorType];
            }
        }
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotification];
    [self reloadPhotographsBadge];
    if(self.defect)
    {
        CLS_LOG(@"Defect ID = %@",self.defect.defectID);
    }
    else
    {
        CLS_LOG(@"NEW Defect ID = %@",self.defectNewID);
    }
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self deregisterKeyboardNotifications];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger) getPhotographsCount
{
    NSArray * photographsList = nil;
    if([self.defect.defectID intValue]<0)
    {
        photographsList =[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:self.defect.defectID andJournalID:self.defect.journalId];
    }
    else
    {
        photographsList =[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:self.defect.defectID];
    }
    
    return [photographsList count];
}

//-(NSUInteger) getPhotographsCountWithDefectID:(NSNumber*)defectID
//{
//    NSArray * photographsList =[[PSCoreDataManager sharedManager] defectPictureListWithIdentifier:defectID];
//    
//    return [photographsList count];
//}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    PSBarButtonItem *saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                                  style:PSBarButtonItemStyleDefault
                                                                                 target:self
                                                                                 action:@selector(onClickSaveButton)];
    
    if (self.defect)
    {
        //Camera Button
        NSString * badgeText=[NSString stringWithFormat:@"%d",[self getPhotographsCount]];
        
        self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                   style:PSBarButtonItemStyleCamera
                                                                  target:self
                                                                  action:@selector(onClickCameraButton)bagde:badgeText];
        
        [self setRightBarButtonItems:[NSArray arrayWithObjects: saveButton,self.cameraButton, nil]];
    }else
    {
        NSString * badgeText= @"0";
        
        self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                   style:PSBarButtonItemStyleCamera
                                                                  target:self
                                                                  action:@selector(onClickCameraButton)bagde:badgeText];
        
        [self setRightBarButtonItems:[NSArray arrayWithObjects: saveButton,self.cameraButton, nil]];
        
        
        //   [self setRightBarButtonItems:[NSArray arrayWithObjects:saveButton, nil]];
    }
    
    
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    if (self.defect)
    {
        [self setTitle:@"Defect"];
    }
    else
    {
        [self setTitle:@"Add Defect"];
    }
    
}

-(void) reloadPhotographsBadge
{
    if (self.cameraButton && self.defect) {
        
        NSString * badgeText=[NSString stringWithFormat:@"%d",[self getPhotographsCount]];
        [self.cameraButton reloadBadge:badgeText];
    }
    else
    {
        NSString * badgeText=@"";
        [self.cameraButton reloadBadge:badgeText];
    }
    
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    _isSavingFromCamera = NO;
    [self resignAllResponders];
    //    if([[ReachabilityManager sharedManager] isConnectedToInternet])
    //    {
    [self validateForm];
    //    }
    //    else
    //    {
    //        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
    //                                                       message:LOC(@"KEY_ALERT_NO_INTERNET")
    //                                                      delegate:nil
    //                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
    //                                             otherButtonTitles:nil,nil];
    //
    //        [alert show];
    //    }
}



- (void)onClickCameraButton {
    
    CLS_LOG(@"Camera Button");
    
    if(self.defect)
    {
        PSCameraViewController *cameraViewController = [[PSCameraViewController alloc] initWithNibName:@"PSCameraViewController" bundle:[NSBundle mainBundle]];
        [cameraViewController setImageType:CameraViewImageTypeDefect];
        [cameraViewController setAppointment:self.appointment];
        [cameraViewController setDefect:self.defect];
        [self.navigationController pushViewController:cameraViewController animated:YES];
    }
    else
    {
        CLS_LOG(@"Saving On Camera");
        _isSavingFromCamera = YES;
        [self resignAllResponders];
        [self validateForm];
    }
    
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
    switch (indexPath.row) {
        case kRowDefectCategory:
            rowHeight = kDefectPickerCellHeight;
            break;
        case kRowDefectIdentified:
            rowHeight = kDefectPickerCellHeight;
            break;
        case kRowDefectNotes:
            rowHeight = [self heightForTextView:nil containingString:_defectNotes];
            if (rowHeight < kDefectTextViewCellHeight)
            {
                rowHeight = kDefectTextViewCellHeight;
            }
            break;
        case kRowRemedialAction:
            rowHeight = kDefectPickerCellHeight;
            break;
        case kRowRemedialNotes:
            rowHeight = [self heightForTextView:nil containingString:_remedialNotes];
            if (rowHeight < kDefectTextViewCellHeight)
            {
                rowHeight = kDefectTextViewCellHeight;
            }
            break;
        case kRowWarningIssued:
            rowHeight = kDefectPickerCellHeight;
            break;
        case kRowSerialNumber:
            rowHeight = kDefectTextFieldCellHeight;
            break;
        case kRowWarningStickerFixed:
            rowHeight = kDefectPickerCellHeight;
            break;
        case kRowSelectAppliance:
            rowHeight = kDefectPickerCellHeight;
            break;
            
        default:
            break;
    }
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.row == kRowDefectNotes || indexPath.row == kRowRemedialNotes)
    {
        //  PSAddDefectTextViewCell *_cell = [self createCellWithIdentifier:textViewCellIdentifier];
        PSTextViewCell *_cell = [self createCellWithIdentifier:textViewCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
        
    }
    else if (indexPath.row == kRowSerialNumber)
    {
        PSAddDefectTextFieldCell *_cell = [self createCellWithIdentifier:textFieldCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    else if ((indexPath.row == kRowDefectCategory) ||
             (indexPath.row == kRowDefectIdentified) ||
             (indexPath.row == kRowRemedialAction) ||
             (indexPath.row == kRowWarningIssued) ||
             (indexPath.row == kRowWarningStickerFixed) ||
             (indexPath.row == kRowSelectAppliance))
    {
        PSAddDefectPickerCell *_cell = [self createCellWithIdentifier:pickerCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
    if([identifier isEqualToString:pickerCellIdentifier])
    {
        PSAddDefectPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:pickerCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAddDefectPickerCell" owner:self options:nil];
            _cell = self.addDefectPcikerCell;
            self.addDefectPcikerCell = nil;
        }
        return _cell;
    }
    else if ([identifier isEqualToString:textFieldCellIdentifier])
    {
        PSAddDefectTextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:textFieldCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAddDefectTextFieldCell" owner:self options:nil];
            _cell = self.addDefectTextFieldCell;
            self.addDefectTextFieldCell = nil;
        }
        return _cell;
    }
    else if([identifier isEqualToString:textViewCellIdentifier])
    {
        // / PSTextViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:textViewCellIdentifier];
        //  if (_cell == nil)
        
        PSTextViewCell *  _cell = [[PSTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:textViewCellIdentifier];
        self.textViewCell = _cell;
        
        return _cell;
    }
    
    return nil;
}


- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSTextViewCell class]])
    {
        PSTextViewCell *_cell = (PSTextViewCell *)*cell;
        UILabel *header = [[UILabel alloc] init];
        header.frame = CGRectMake(12, 3, 320,12);
        [header setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        [header setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [header setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12]];
        [_cell addSubview:header];
        
        UIImage *image = [UIImage imageNamed:@"line"];
        UIImageView *seperatorImage = [[UIImageView alloc] init];
        
        [seperatorImage setImage:image];
        
        
        PSTextView*  _textView = [[PSTextView alloc] initWithFrame:CGRectZero];
        [_textView setDelegate:self];
        [_textView setAutocorrectionType:UITextAutocorrectionTypeDefault];
        [seperatorImage setFrame:CGRectMake(0, (_textView.frame.size.height - 1), 320, 1)];
        //[_cell addSubview:seperatorImage];
        
        [_textView setTag:indexPath.row];
        [_textView setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        
        switch (indexPath.row)
        {
            case kRowDefectNotes:
                [header setText:LOC(@"DEFECT NOTES")];
                if (!isEmpty(_defectNotes))
                {
                    _textView.text = _defectNotes;
                }
                
                if (_isDefectIdentified || _isDefectGeneralComment)
                {
                    [_textView setUserInteractionEnabled:YES];
                    [_textView setEditable:YES];
                }
                else
                {
                    [_textView setUserInteractionEnabled:NO];
                    [_textView setEditable:NO];
                    _textView.text = _defectNotes;
                }
                
                break;
            case kRowRemedialNotes:
                [header setText:LOC(@"REMEDIAL NOTES")];
                _textView.tag = kRowRemedialNotes;
                if (!isEmpty(_remedialNotes))
                {
                    _textView.text = _remedialNotes;
                }
                if (_isActionTaken || _isDefectGeneralComment)
                {
                    [_textView setUserInteractionEnabled:YES];
                    [_textView setEditable:YES];
                }
                else
                {
                    [_textView setUserInteractionEnabled:NO];
                    [_textView setEditable:NO];
                    _remedialNotes = nil;
                    _textView.text = _remedialNotes;
                }
                break;
            default:
                break;
        }
        [_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        _cell.textView = _textView;
        _textView = nil;
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSAddDefectTextFieldCell class]])
    {
        PSAddDefectTextFieldCell *_cell = (PSAddDefectTextFieldCell *) *cell;
        [_cell.lblHeader setText:@"SERIAL NUMBER"];
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        _cell.txtTitle.delegate = self;
        [_cell.txtTitle setTag:kRowSerialNumber];
        
        
        if (!_isWarningIssued || _isDefectGeneralComment)
        {
            [_cell.btnEdit setHidden:YES];
            _serialNumber = @"";
            [_cell.txtTitle setEnabled:NO];
            [_cell.txtTitle setUserInteractionEnabled:NO];
        }
        else
        {
            [_cell.btnEdit setHidden:NO];
            [_cell.txtTitle setEnabled:YES];
            
            [_cell.txtTitle setUserInteractionEnabled:YES];
        }
        [_cell.txtTitle setText:_serialNumber];
        [_cell.imgSeperator setHidden:YES];
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSAddDefectPickerCell class]])
    {
        PSAddDefectPickerCell *_cell = (PSAddDefectPickerCell *) *cell;
        
        _cell.pickerOptions = _pickerOptions;
        [_cell setDelegate:self];
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        switch (indexPath.row)
        {
            case kRowDefectCategory:
                [_cell.lblHeader setText:@"DEFECT CATEGORY"];
                if (!isEmpty(_defectCategoryDescription))
                {
                    [_cell.lblTitle setText:_defectCategoryDescription];
                }
                else
                {
                    [_cell.lblTitle setText:nil];
                }
                
                _cell.pickerOptions = _defectCategoriesStrings;
                [_cell.imgSeperator setHidden:NO];
                [_cell.btnPicker addTarget:self action:@selector(onClickDefectsCategory:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case kRowDefectIdentified:
                [_cell.lblHeader setText:@"DEFECT IDENTIFIED"];
                if (_isDefectGeneralComment)
                {
                    [_cell.btnPicker setEnabled:NO];
                    _isDefectIdentifiedString = nil;
                }
                else
                {
                    [_cell.btnPicker setEnabled:YES];
                }
                if (!isEmpty(_isDefectIdentifiedString))
                {
                    [_cell.lblTitle setText:_isDefectIdentifiedString];
                }
                else
                {
                    [_cell.lblTitle setText:nil];
                }
                
                [_cell.btnPicker addTarget:self action:@selector(onClickDefectsIdentified:) forControlEvents:UIControlEventTouchUpInside]
                ;
                [_cell.imgSeperator setHidden:NO];
                break;
            case kRowRemedialAction:
                [_cell.lblHeader setText:@"REMEDIAL ACTION"];
                if (_isDefectGeneralComment)
                {
                    [_cell.btnPicker setEnabled:NO];
                    _isActionTakenString = nil;
                }
                else
                {
                    [_cell.btnPicker setEnabled:YES];
                }
                if (!isEmpty(_isActionTakenString))
                {
                    [_cell.lblTitle setText:_isActionTakenString];
                }
                else
                {
                    [_cell.lblTitle setText:nil];
                }
                
                [_cell.btnPicker addTarget:self action:@selector(onClickRemedialAction:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.imgSeperator setHidden:NO];
                break;
            case kRowWarningIssued:
                [_cell.lblHeader setText:@"WARNING/ADVICE NOTE ISSUED"];
                
                if (_isDefectGeneralComment)
                {
                    [_cell.btnPicker setEnabled:NO];
                    _isWarningIssuedString = nil;
                }
                else
                {
                    [_cell.btnPicker setEnabled:YES];
                }
                
                if (!isEmpty(_isWarningIssuedString))
                {
                    [_cell.lblTitle setText:_isWarningIssuedString];
                }
                else
                {
                    [_cell.lblTitle setText:nil];
                }
                [_cell.btnPicker addTarget:self action:@selector(onClickWarningIssued:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.imgSeperator setHidden:NO];
                break;
            case kRowWarningStickerFixed:
                [_cell.lblHeader setText:@"WARNING TAG/STICKER FIXED?"];
                if (_isDefectGeneralComment)
                {
                    [_cell.btnPicker setEnabled:NO];
                    _isStickerFixedString = nil;
                }
                else
                {
                    [_cell.btnPicker setEnabled:YES];
                }
                if (!isEmpty(_isStickerFixedString))
                {
                    [_cell.lblTitle setText:_isStickerFixedString];
                }
                else
                {
                    [_cell.lblTitle setText:nil];
                }
                
                [_cell.btnPicker addTarget:self action:@selector(onClickWarningSticker:) forControlEvents:UIControlEventTouchUpInside];
                [_cell.imgSeperator setHidden:NO];
                break;
                
            case kRowSelectAppliance:
                
                if (self.viewMode == ApplianceViewMode)
                {
                    [_cell.lblHeader setText:@"SELECT APPLIANCE"];
                    _cell.pickerOptions = _appliancesNames;
                    if (!isEmpty(_applianceName))
                    {
                        [_cell.lblTitle setText:_applianceName];
                    }
                    else
                    {
                        [_cell.lblTitle setText:nil];
                    }
                }
                else if (self.viewMode == DetectorViewMode)
                {
                    [_cell.lblHeader setText:@"SELECT DETECTOR"];
                    _cell.pickerOptions = _detectorNames;
                    if (!isEmpty(_detector))
                    {
                        [_cell.lblTitle setText:_detector.detectorType];
                    }
                    else
                    {
                        [_cell.lblTitle setText:nil];
                    }
                    
                }
                
                [_cell.btnPicker setEnabled:YES];
                
                [_cell.imgSeperator setHidden:NO];
                [_cell.btnPicker addTarget:self action:@selector(onClickSelectAppliance:) forControlEvents:UIControlEventTouchUpInside];
                
                break;
            default:
                break;
        }
        *cell = _cell;
    }
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickDefectsCategory:(id)sender
{
    CLS_LOG(@"onClickDefectsCategory");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    _pickerTag = kRowDefectCategory;
    if (isEmpty(_defectCategoriesStrings))
    {
        
    }
}

- (IBAction)onClickDefectsIdentified:(id)sender
{
    CLS_LOG(@"onClickDefectsIdentified");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    _pickerTag = kRowDefectIdentified;
}

- (IBAction)onClickRemedialAction:(id)sender
{
    CLS_LOG(@"onClickRemedialAction");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    _pickerTag = kRowRemedialAction;
}

- (IBAction)onClickWarningIssued:(id)sender
{
    CLS_LOG(@"onClickWarningIssued");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    _pickerTag = kRowWarningIssued;
}

- (IBAction)onClickWarningSticker:(id)sender
{
    CLS_LOG(@"onClickWarningSticker");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    _pickerTag = kRowWarningStickerFixed;
}

- (IBAction)onClickSelectAppliance:(id)sender
{
    CLS_LOG(@"onClickSelectAppliance");
    self.selectedControl = (UIButton *)sender;
    [self resignAllResponders];
    if (self.viewMode == ApplianceViewMode)
    {
        if (isEmpty(_appliances))
        {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_NO_APPLIANCES")
                                                           message:LOC(@"KEY_ALERT_NO_APPLIANCES_DETAIL")
                                                          delegate:nil
                                                 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                 otherButtonTitles:nil,nil];
            [alert show];
        }
    }
    _pickerTag = kRowSelectAppliance;
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.selectedControl = textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    _serialNumber = textField.text;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL textFieldShouldReturn = YES;
    NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
    
    if ([stringAfterReplacement length] > 10)
    {
        textFieldShouldReturn = NO;
    }
    return textFieldShouldReturn;
    
}

#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
    BOOL textVeiwShouldReturn = YES;
    NSString *stringAfterReplacement = [((UITextView *)self.selectedControl).text stringByAppendingString:text];
    
    if ([stringAfterReplacement length] > 200)
    {
        textVeiwShouldReturn = NO;
    }
    return textVeiwShouldReturn;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.selectedControl = (UIControl *)textView;
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.tag == kRowDefectNotes)
    {
        _defectNotes = textView.text;
    }
    else if (textView.tag == kRowRemedialNotes)
    {
        _remedialNotes = textView.text;
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.tag == kRowDefectNotes)
    {
        _defectNotes = textView.text;
    }
    else if (textView.tag == kRowRemedialNotes)
    {
        _remedialNotes = textView.text;
    }
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    [self scrollToRectOfSelectedControl];
}

#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignAllResponders];
}

#pragma mark - Scroll Cell Methods
- (void)scrollToRectOfSelectedControl {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - Notifications

- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveDefectSuccessNotification) name:kSaveDefectSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveDefectFailureNotification) name:kSaveDefectFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveDefectSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveDefectFailureNotification object:nil];
}

- (void) onReceiveSaveDefectSuccessNotification
{
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
    if(!_isSavingFromCamera)
    {
        [self popOrCloseViewController];
    }
    else
    {
       self.defect = [[PSApplianceManager sharedManager] fetchApplianceDefectWithDefectID:self.defectNewID];
        //Double check. the defect object
        if(self.defect)
        {
            CLS_LOG(@"%@",[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:[self.defect JSONValue] options:0 error:nil] encoding:NSUTF8StringEncoding]);
            [self onClickCameraButton];
        }
    }
    _isSavingFromCamera=NO;
}

- (void) onReceiveSaveDefectFailureNotification
{
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.activityIndicator stopAnimating];
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                       message:LOC(@"KEY_ALERT_DEFECT_REPAIR_SAVE_FAILURE")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
        
    });
}

#pragma mark - Protocol Methods

/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSInteger)pickerOption
{
    switch (_pickerTag) {
        case kRowDefectCategory:
        {
            _defectCategory = [_defectCategories objectAtIndex:pickerOption];
            _defectCategoryDescription = _defectCategory.categoryDescription;
            _isDefectGeneralComment = [[_defectCategoryDescription lowercaseString]isEqualToString:LOC(@"KEY_STRING_GENERAL_COMMENT")];
        }
            break;
        case kRowDefectIdentified:
        {
            _isDefectIdentifiedString = [_pickerOptions objectAtIndex:pickerOption];
            _isDefectIdentified = [_isDefectIdentifiedString boolValue];
        }
            break;
        case kRowRemedialAction:
        {
            _isActionTakenString = [_pickerOptions objectAtIndex:pickerOption];
            _isActionTaken = [_isActionTakenString boolValue];
        }
            break;
        case kRowWarningIssued:
        {
            _isWarningIssuedString = [_pickerOptions objectAtIndex:pickerOption];
            _isWarningIssued = [_isWarningIssuedString boolValue];
            if (!_isWarningIssued)
            {
                _serialNumber = nil;
            }
        }
            break;
        case kRowWarningStickerFixed:
        {
            _isStickerFixedString = [_pickerOptions objectAtIndex:pickerOption];
            _isStickerFixed = [_isStickerFixedString boolValue];
        }
            break;
        case kRowSelectAppliance:
        {
            if (self.viewMode == ApplianceViewMode)
            {
                _applianceName = [_appliancesNames objectAtIndex:pickerOption];
                _appliance = [_appliances objectAtIndex:pickerOption];
            }
            else if (self.viewMode == DetectorViewMode)
            {
                _detector = [_detectors objectAtIndex:pickerOption];
            }
        }
            break;
        default:
            break;
    }
    [self.tableView reloadData];
}

#pragma mark - Resign All Responders
- (void) resignAllResponders
{
    [self.view endEditing:YES];
}



#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
    float horizontalPadding = 0;
    float verticalPadding = 40;
    CGFloat height = 0.0;
    if(OS_VERSION >= 7.0)
    {
        NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [style setLineBreakMode:NSLineBreakByWordWrapping];
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    style, NSParagraphStyleAttributeName,
                                    
                                    [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17],NSFontAttributeName,
                                    nil];
        CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(285, 999999.0f)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:attributes
                                                   context:nil];
        height = boundingRect.size.height + verticalPadding;
    }
    else
    {
        height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
                    constrainedToSize:CGSizeMake(285, 999999.0f)
                        lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
    }
    return height * 1.05;
}

#pragma mark - Methods

- (void) loadDefaultValues
{
    _isDefectIdentified = [self.defect.isDefectIdentified boolValue];
    if (_isDefectIdentified)
    {
        _isDefectIdentifiedString = LOC(@"KEY_ALERT_YES");
    }
    else
    {
        _isDefectIdentifiedString = LOC(@"KEY_ALERT_NO");
    }
    
    _isActionTaken = [self.defect.isActionTaken boolValue];
    if (_isActionTaken)
    {
        _isActionTakenString = LOC(@"KEY_ALERT_YES");
    }
    else
    {
        _isActionTakenString = LOC(@"KEY_ALERT_NO");
    }
    
    _isWarningIssued = [self.defect.isAdviceNoteIssued boolValue];
    if (_isWarningIssued)
    {
        _isWarningIssuedString = LOC(@"KEY_ALERT_YES");
    }
    else
    {
        _isWarningIssuedString = LOC(@"KEY_ALERT_NO");
    }
    
    _isStickerFixed = [self.defect.warningTagFixed boolValue];
    if (_isStickerFixed)
    {
        _isStickerFixedString = LOC(@"KEY_ALERT_YES");
    }
    else
    {
        _isStickerFixedString = LOC(@"KEY_ALERT_NO");
    }
    _defectCategory = self.defect.defectCategory;
    _defectCategoryDescription = self.defect.defectCategory.categoryDescription;
    _isDefectGeneralComment = [[_defectCategoryDescription lowercaseString]isEqualToString:LOC(@"KEY_STRING_GENERAL_COMMENT")];
    if (self.viewMode == ApplianceViewMode)
    {
#warning ASIF: save defects to check change in applianceWithIdentifier method. Defects should be successfully saved
        _appliance = self.defect.defectToAppliance;//[[PSCoreDataManager sharedManager] applianceWithIdentifier:self.defect.applianceID inContext:nil];
        if (!isEmpty(_appliance))
        {
            _applianceName = _appliance.applianceType.typeName;
        }
    }
    else if (self.viewMode == DetectorViewMode)
    {
        _detector = [[PSCoreDataManager sharedManager]detectorWithIdentifier:self.defect.detectorTypeId forProperty:self.defect.propertyId context:[PSDatabaseContext sharedContext].managedObjectContext];
    }
    
    _remedialNotes = self.defect.remedialAction;
    _serialNumber = self.defect.serialNo;
    _defectNotes = self.defect.defectDescription;
}

- (void) disableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = NO;
    }
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = NO;
    }
}

- (void) enableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = YES;
    }
    
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = YES;
    }
}

- (void) validateForm
{
    BOOL isValid = YES;
    if (isEmpty(_defectCategory))
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                       message:LOC(@"KEY_ALERT_SELECT_DEFECT_CATEGORY")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
        isValid = NO;
    }
    else if (isEmpty(_appliance) || isEmpty(_detector))
    {
        NSString *message = @"";
        BOOL isApplianceSelected = YES;
        
        if (self.viewMode == ApplianceViewMode)
        {
            if (isEmpty(_appliance))
            {
                message = LOC(@"KEY_ALERT_SELECT_APPLIANCE");
                isApplianceSelected = NO;
            }
            
        }
        else if (self.viewMode == DetectorViewMode)
        {
            if (isEmpty(_detector))
            {
                message = LOC(@"KEY_ALERT_SELECT_DETECTOR");
                isApplianceSelected = NO;
            }
            
        }
        if (!isApplianceSelected)
        {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                           message:message
                                                          delegate:nil
                                                 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                 otherButtonTitles:nil,nil];
            [alert show];
            isValid = NO;
        }
        
    }
    
    
    if (isValid)
    {
        [self saveDefect];
    }
}
- (void) saveDefect
{
    [self showActivityIndicator:[NSString stringWithFormat:@"%@", LOC(@"KEY_ALERT_SAVING_DEFECT")]];
    
    [self disableNavigationBarButtons];
    if (isEmpty(self.defect))
    {
        CLS_LOG(@"New defect ID %@",self.defectNewID);
        [_defectDictionary setObject:self.defectNewID forKey:kDefectID];
    }
    else
    {
        [_defectDictionary setObject:isEmpty(self.defect.defectID)?[NSNull null]:self.defect.defectID forKey:kDefectID];
        [_defectDictionary setObject:self.defect.objectID forKey:@"defectObjectId"];
        
    }
    
    if (!_isStickerFixed)
    {
        _serialNumber = nil;
    }
    if (self.viewMode == ApplianceViewMode)
    {
        [_defectDictionary setObject:_appliance.objectID forKey:@"applianceObjectId"];
        [_defectDictionary setObject:[NSNull null] forKey:kDetectorTypeId];
        [_defectDictionary setObject:kDefectTypeAppliance forKey:kDefectType];
    }
    else if (self.viewMode == DetectorViewMode)
    {
        [_defectDictionary setObject:[NSNull null] forKey:kApplianceId];
        [_defectDictionary setObject:isEmpty(_detector.detectorTypeId)?[NSNull null]:_detector.detectorTypeId forKey:kDetectorTypeId];
        [_defectDictionary setObject:kDefectTypeDetector forKey:kDefectType];
    }
    
    [_defectDictionary setObject:[UtilityClass convertNSDateToServerDate:[NSDate date]] forKey:kDefectDate];
    [_defectDictionary setObject:isEmpty(_defectNotes)?[NSNull null]:_defectNotes forKey:kDefectDescription];
    [_defectDictionary setObject:isEmpty(_defectCategory.categoryID)?[NSNull null]:_defectCategory.categoryID forKey:kFaultCategory];
    [_defectDictionary setObject:[NSNumber numberWithBool:_isActionTaken] forKey:kIsActionTaken];
    [_defectDictionary setObject:[NSNumber numberWithBool:_isWarningIssued] forKey:kisAdviceNoteIssued];
    [_defectDictionary setObject:[NSNumber numberWithBool:_isDefectIdentified] forKey:kIsDefectIdentified];
    [_defectDictionary setObject:isEmpty(self.appointment.journalId)?[NSNull null]:self.appointment.journalId forKey:@"JournalId"];
    [_defectDictionary setObject:isEmpty(self.appointment.appointmentToProperty.propertyId)?[NSNull null]:self.appointment.appointmentToProperty.propertyId forKey:kPropertyId];
    [_defectDictionary setObject:isEmpty(_remedialNotes)?[NSNull null]:_remedialNotes forKey:kRemedialAction];
    [_defectDictionary setObject:isEmpty(_serialNumber)?[NSNull null]:_serialNumber forKey:kSerialNo];
    [_defectDictionary setObject:[NSNumber numberWithBool:_isStickerFixed] forKey:kWarningTagFixed];
    
    NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
    // [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:@"salt"];
    //[requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:@"username"];
    [requestParameters setObject:_defectDictionary forKey:@"defect"];
    //CLS_LOG(@"%@",[requestParameters JSONRepresentation]);
    [[PSDataPersistenceManager sharedManager]saveNewDefect:_defectDictionary];
    //[self schduleUIControlsActivation];
}
@end
