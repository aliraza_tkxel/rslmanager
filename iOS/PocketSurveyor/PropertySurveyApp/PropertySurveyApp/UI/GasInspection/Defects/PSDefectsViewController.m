//
//  PSDefectsViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDefectsViewController.h"
#import "PSBarButtonItem.h"
#import "PSDefectsCell.h"
#import "PSDefectsPickerCell.h"
#import "PSAddDefectsViewController.h"

#define kNumberOfSections 2
#define kSectionNumberOfAppliances 0
#define kSectionDefects 1

#define kApplianceViewMode 0
#define kDetectorViewMode 1

@interface PSDefectsViewController ()
{
    BOOL _noResultsFound;
    NSArray *_defects;
    NSArray *totalAppliances;
    NSNumber *_numberOfAppliancesTested;
    NSInteger _viewMode;
}

@end

@implementation PSDefectsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    self.headerViewArray = [NSMutableArray array];
    
//    _defects = [[PSApplianceManager sharedManager]fetchAllApplianceDefects:self.appointment.journalId];
//    if ([_defects count] <= 0)
//    {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [self disableNavigationBarButtons];
//            [self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
//                                         LOC(@"KEY_ALERT_LOADING"),
//                                         LOC(@"KEY_STRING_DEFECTS")]];
//        });
//    }
//    NSMutableDictionary *requestParametes = [NSMutableDictionary dictionary];
//    [requestParametes setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:@"salt"];
//    [requestParametes setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:@"username"];
//    [requestParametes setObject:self.appointment.journalId forKey:@"journalId"];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [[PSApplianceManager sharedManager] fetchApplianceDefects:requestParametes];
//    });
    
    [self schduleUIControlsActivation];
    [self loadSectionHeaderViews:@[@"",LOC(@"KEY_STRING_DEFECTS")] headerViews:self.headerViewArray];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotification];
    _numberOfAppliancesTested = [[PSApplianceManager sharedManager] totalNumberOfAppliancesInspected:self.appointment.appointmentToProperty];
     _defects = [[PSApplianceManager sharedManager]fetchAllApplianceDefects:self.appointment.journalId];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.addDefectButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_ADD_DEFECT")
                                                                      style:PSBarButtonItemStyleAdd
                                                                     target:self
                                                                     action:@selector(onClickAddDefectButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.addDefectButton, nil]];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:LOC(@"KEY_STRING_DEFECTS")];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickAddDefectButton
{
    CLS_LOG(@"onClickAddDefectButton");
    

    [self showActionSheet];

}

#pragma mark - UITableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 0;
    if (section == kSectionDefects)
    {
        height = 20;
    }
	return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [self.headerViewArray objectAtIndex:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return kNumberOfSections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 44;
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    if (section == kSectionNumberOfAppliances)
    {
        rows = 1;
    }
    else if (section == kSectionDefects)
    {
        if (!isEmpty(_defects))
        {
            rows = [_defects count];
            _noResultsFound = NO;
        }
        else
        {
            rows = 1;
            _noResultsFound = YES;
        }
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = nil;
    if (indexPath.section == kSectionNumberOfAppliances)
    {
        static NSString *appliancesCellidentifier = @"defectsPickerCellIdentifier";
        PSDefectsPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:appliancesCellidentifier];
        
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSDefectsPickerCell" owner:self options:nil];
            _cell = self.defectPcikerCell;
            self.defectPcikerCell = nil;
        }
        [self configureCell:&_cell atIndexPath:indexPath];
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = _cell;

    
    }
    else if (indexPath.section == kSectionDefects)
    {
        if(_noResultsFound)
        {
            static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
            UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
            if(_cell == nil)
            {
                _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
            }
            // Configure the cell...
            
            _cell.textLabel.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);//UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
            _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
            _cell.textLabel.text = LOC(@"KEY_STRING_NO_DEFECTS_FOUND");
            _cell.textLabel.textAlignment = PSTextAlignmentCenter;
            _cell.selectionStyle = UITableViewCellSelectionStyleNone;
            _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
            cell = _cell;
        }
        else
        {
            static NSString *defectsCellidentifier = @"defectsCellIdentifier";
            PSDefectsCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:defectsCellidentifier];
            
            if(_cell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"PSDefectsCell" owner:self options:nil];
                _cell = self.defectsCell;
                self.defectsCell = nil;
            }
            [self configureCell:&_cell atIndexPath:indexPath];
            _cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell = _cell;
        }
    }
    return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSDefectsPickerCell class]])
    {
        PSDefectsPickerCell *_cell = (PSDefectsPickerCell *)*cell;
        [_cell.btnPicker addTarget:self action:@selector(onClickAppliancesPicker:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnPicker setHidden:YES];
        [_cell.lblHeader setText:LOC(@"KEY_STRING_TOTAL_NUMBER_OF_APPLIANCES_TESTED")];
        _cell.pickerOptions = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
        [_cell.lblTitle setText:[_numberOfAppliancesTested stringValue]];
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSDefectsCell class]])
    {
        PSDefectsCell *_cell = (PSDefectsCell *)*cell;
        //NSString *defect =  [NSString stringWithFormat:@"%i. %@", indexPath.row + 1, @"Defect"];
        Defect *defect = [_defects objectAtIndex:indexPath.row];
        [_cell.lblTitle setText:defect.defectDescription];
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        *cell = _cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionDefects)
    {
        if (!_noResultsFound)
        {
            PSAddDefectsViewController *addDefectViewController = [[PSAddDefectsViewController alloc] initWithNibName:@"PSAddDefectsViewController" bundle:nil];
            [addDefectViewController setAppointment:self.appointment];
            Defect *defect = [_defects objectAtIndex:indexPath.row];
            if ([defect.defectType isEqualToString:kDefectTypeAppliance])
            {
                [addDefectViewController setViewMode:kApplianceViewMode];
            }
            else if ([defect.defectType isEqualToString:kDefectTypeDetector])
            {
                [addDefectViewController setViewMode:kDetectorViewMode];
            }
            [addDefectViewController setDefect:[_defects objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:addDefectViewController animated:YES];
        }
    }
}
#pragma mark - Cell Button Click Selectors

- (IBAction)onClickAppliancesPicker:(id)sender
{
    CLS_LOG(@"onclickappliancePicker");
}


#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.firstOtherButtonIndex)
    {
        _viewMode = kApplianceViewMode;
        [self addDefect];
    }
    else if (buttonIndex == (actionSheet.firstOtherButtonIndex + 1))
    {
        _viewMode = kDetectorViewMode;
        [self addDefect];
    }
}



#pragma mark ActionSheet Methods

-(void)showActionSheet {
    UIActionSheet * actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                delegate:self
                                                       cancelButtonTitle:LOC(@"KEY_ALERT_CANCEL")
                                                  destructiveButtonTitle:nil
                                                       otherButtonTitles:LOC(@"KEY_STRING_ADD_APPLIANCE_DEFECT"), LOC(@"KEY_STRING_ADD_DETECTOR_DEFECT"), nil] ;
    [actionSheet setCancelButtonIndex:2];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
}

#pragma mark - Notifications

- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveFetchDefectsSaveSuccessNotification) name:kFetchDefectsSaveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveFetchDefectsSaveFailureNotification) name:kFetchDefectsSaveFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchDefectsSaveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchDefectsSaveFailureNotification object:nil];
}

- (void) onReceiveFetchDefectsSaveSuccessNotification
{
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
    _defects = [[PSApplianceManager sharedManager]fetchAllApplianceDefects:self.appointment.journalId];
    [self.tableView reloadData];
}

- (void) onReceiveFetchDefectsSaveFailureNotification
{
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
    _defects = [[PSApplianceManager sharedManager]fetchAllApplianceDefects:self.appointment.journalId];
    [self.tableView reloadData];
}

#pragma mark - Protocol Methods
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption
{
    if (!isEmpty(pickerOption))
    {
       // _numberOfAppliances = pickerOption;
    }
    [self.tableView reloadData];
}

#pragma mark - Methods

- (void) disableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = NO;
    }
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = NO;
    }
}

- (void) enableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = YES;
    }
    
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = YES;
    }
}

- (void) addDefect
{
    PSAddDefectsViewController *addDefectViewController = [[PSAddDefectsViewController alloc] initWithNibName:@"PSAddDefectsViewController" bundle:nil];
    [addDefectViewController setAppointment:self.appointment];
    
    //Cattering offline scnerio
    addDefectViewController.defectNewID = [NSNumber numberWithInt:(-1)* ((int)[[PSApplianceManager sharedManager] fetchAllApplianceDefects:self.appointment.journalId].count +1)];
    
    if (_viewMode == kApplianceViewMode)
    {
        NSArray *appliances = [self.appointment.appointmentToProperty.propertyToAppliances allObjects];
        if ([appliances count] > 0)
        {
            [addDefectViewController setViewMode:kApplianceViewMode];
            [self.navigationController pushViewController:addDefectViewController animated:YES];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_NO_APPLIANCES")
                                                           message:LOC(@"KEY_ALERT_NO_APPLIANCES_DETAIL")
                                                          delegate:nil
                                                 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                 otherButtonTitles:nil,nil];
            [alert show];
        }
        
    }
    else if (_viewMode == kDetectorViewMode)
    {
        [addDefectViewController setViewMode:kDetectorViewMode];
        [self.navigationController pushViewController:addDefectViewController animated:YES];
    }
}
@end
