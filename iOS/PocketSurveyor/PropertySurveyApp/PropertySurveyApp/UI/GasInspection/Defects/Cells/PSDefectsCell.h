//
//  PSDefectsCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSDefectsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end
