//
//  PSCP12RiskVulCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSCP12RiskVulCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCategory;
@property (strong, nonatomic) IBOutlet UILabel *lblSubCategory;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator;

@end
