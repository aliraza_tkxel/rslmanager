//
//  PSCP12InfoViewController.m
//  PropertySurveyApp
//
//  Created by TkXel on 05/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCP12InfoViewController.h"
#import "PSGasSurveyViewController.h"
#import "PSBarButtonItem.h"
#import "PSCP12InfoCell.h"
#import "PSSectionHeaderView.h"
#import "PSCP12AsbestosCell.h"
#import "PSCP12RiskVulCell.h"
#import "PSNoEntryViewController.h"

@interface PSCP12InfoViewController ()

@end


#define kNumberOfSections 5

#define kSectionCertificate 0
#define kSectionInspectionForm 1
#define kSectionOtherRelevantInfo 2
#define kSectionAsbestos 5
#define kSectionRisk 4
#define kSectionVulnerability 3

#define kSectionCertificateRows 3
#define kRowCertificateIssued 0
#define kRowInspectionRef 1
#define kRowExpiry 2

#define kSectionInspectionFormRows 1
#define kRowInspectionForm 0

#define kSectionOtherRelevantInfoRows 2
#define kRowAppointments 0
#define kRowNoEntrys 1

#define kSectionRiskAndAsbestosRows 2
#define kRowRisk 0
#define kRowAsbestos 1

#define kAsbestosCellHeight 48

static NSString *asbestosCellIdentifier = @"AsbestosCellIdentifier";
static NSString *riskVulCellIdentifier = @"tenantRiskVulCellIdentifier";
static NSString *cp12InfoCellIdentifier = @"cp12InfoCellIdentifier";

@implementation PSCP12InfoViewController
@synthesize asbestosArray;
@synthesize riskArray;
@synthesize vulnerabilityArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.headerViewArray = [NSMutableArray array];
    // Do any additional setup after loading the view from its nib.
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self loadNavigationonBarItems];

    
    Customer *customer = [self.appointment defaultTenant];
    self.riskArray = [customer.customerToCustomerRiskData allObjects];
    self.vulnerabilityArray = [customer.customerToCustomerVulunarityData allObjects];
    self.asbestosArray =  [NSArray arrayWithArray:[self.appointment.appointmentToProperty.propertyToPropertyAsbestosData allObjects]];
    
    
    //  [self loadSectionHeaderViews:@[@"", @"", LOC(@"KEY_STRING_OTHER_RELEVANT_INFORMATION"),@""] headerViews:self.headerViewArray];
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self loadSectionHeaderViews:@[@"",@"",@"",LOC(@"KEY_STRING_RISK"), LOC(@"KEY_STRING_VULNERABILITY")/*,LOC(@"KEY_STRING_ASBESTOS")*/] headerViews:self.headerViewArray];
    if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]] || [self.appointment.appointmentStatus isEqualToString:kAppointmentStatusNoEntry])
    {
        [self.noEntryButton setEnabled:NO];
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    if ([self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
    {
        self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
                                                                    style:PSBarButtonItemStyleDefault
                                                                   target:self
                                                                   action:@selector(onClickNoEntryButton)];
        
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.noEntryButton, nil]];
    }
    
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:LOC(@"KEY_STRING_INSPECTION")];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickNoEntryButton
{
    PSNoEntryViewController *noEntryViewController = [[PSNoEntryViewController alloc] initWithNibName:@"PSNoEntryViewController" bundle:nil];
    [noEntryViewController setAppointment:self.appointment];
    [self.navigationController pushViewController:noEntryViewController animated:YES];
    
    CLS_LOG(@"onClickNoEntryButton");
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return kNumberOfSections;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSString *headerTitle = nil;
//    if(section == kSectionOtherRelevantInfo)
//    {
//        headerTitle = LOC(@"KEY_STRING_OTHER_RELEVANT_INFORMATION");
//    }
//    return headerTitle;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat height = 0;
    if (section == kSectionAsbestos || section == kSectionRisk || section == kSectionVulnerability)
    {
        height = 20;
    }
	return height;
}
//
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [self.headerViewArray objectAtIndex:section];
    return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 44;
    switch (indexPath.section) {
        case kSectionCertificate:
            rowHeight = 44;
            break;
        case kSectionInspectionForm:
            rowHeight = 44;
            break;
        case kSectionOtherRelevantInfo:
            rowHeight = 44;
            break;
        case kSectionAsbestos:
            rowHeight = 48;
            break;
        case kSectionRisk:
            rowHeight = 48;
            break;
        case kSectionVulnerability:
            rowHeight = 48;
            break;
        default:
            rowHeight = 0;
            break;
    }

    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rows = 1;
    switch (section) {
        case kSectionCertificate:
            rows = kSectionCertificateRows;
            break;
        case kSectionInspectionForm:
            rows = kSectionInspectionFormRows;
            break;
        case kSectionOtherRelevantInfo:
            rows = kSectionOtherRelevantInfoRows;
            break;
        case kSectionAsbestos:
            if (![self.asbestosArray count] == 0)
            {
                rows = (int)[self.asbestosArray count];
            }
            break;
        case kSectionRisk:
            if (![self.riskArray count] == 0)
            {
                rows = (int)[self.riskArray count];
            }
            
            break;
        case kSectionVulnerability:
            if (![self.vulnerabilityArray count] == 0)
            {
                rows = (int)[self.vulnerabilityArray count];
            }
            
            break;
        default:
            rows = 0;
            break;
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.section == kSectionAsbestos)
    {
        
        PSCP12AsbestosCell *_cell = [self createCellWithIdentifier:asbestosCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = _cell;
        
    }
    else if (indexPath.section == kSectionRisk || indexPath.section == kSectionVulnerability)
    {
        
        PSCP12RiskVulCell *_cell = [self createCellWithIdentifier:riskVulCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = _cell;
    }
    else
    {
        static NSString *cp12InfoCellIdentifier = @"cp12InfoCellIdentifier";
        PSCP12InfoCell *_cell = [self createCellWithIdentifier:cp12InfoCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        
        _cell.lblHeader.backgroundColor = [UIColor clearColor];
        [_cell.lblHeader setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        [_cell.lblHeader setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        [_cell.lblHeader setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12]];
        cell = _cell;
        cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
	return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
	if([identifier isEqualToString:asbestosCellIdentifier])
	{
        PSCP12AsbestosCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:asbestosCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSCP12AsbestosCell" owner:self options:nil];
            _cell = self.asbestosCell;
            self.asbestosCell = nil;
        }
		return _cell;
	}
    else if ([identifier isEqualToString:riskVulCellIdentifier])
    {
        PSCP12RiskVulCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:riskVulCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSCP12RiskVulCell" owner:self options:nil];
            _cell = self.riskVulCell;
            self.riskVulCell = nil;
        }
		return _cell;
    }
	else if([identifier isEqualToString:cp12InfoCellIdentifier])
	{
        
        PSCP12InfoCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:cp12InfoCellIdentifier];
        if (_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSCP12InfoCell" owner:self options:nil];
            _cell = self.cp12InfoCell;
            self.cp12InfoCell = nil;
        }
		return _cell;
	}
	return nil;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSCP12AsbestosCell class]])
    {
        PSCP12AsbestosCell *_cell = (PSCP12AsbestosCell *) *cell;
        
        if ([self.asbestosArray count] == 0) {
            _cell.lblAsbestos.text = LOC(@"KEY_STRING_NONE");
            [_cell.lblAsbestosLocation setHidden:YES];
            _cell.imgSeperator.hidden = NO;
        }
        else
        {
            PropertyAsbestosData *asbestosData = [self.asbestosArray objectAtIndex:indexPath.row];
            if (!isEmpty(asbestosData))
            {
                _cell.imgSeperator.hidden = YES;
                [_cell.lblAsbestosLocation setHidden:NO];
                _cell.lblAsbestos.text = asbestosData.asbRiskLevelDesc;
                _cell.lblAsbestos.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
                _cell.lblAsbestos.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
                _cell.lblAsbestosLocation.text = asbestosData.riskDesc;
                _cell.lblAsbestosLocation.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
                _cell.lblAsbestosLocation.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
                _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
            }
            if (indexPath.row == [self.asbestosArray count] -1) {
                _cell.imgSeperator.hidden = NO;
            }
        }
        
        *cell = _cell;
    }
    else if ([*cell isKindOfClass:[PSCP12RiskVulCell class]])
    {
        PSCP12RiskVulCell *_cell = (PSCP12RiskVulCell *)*cell;
        [_cell.lblCategory setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblSubCategory setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];

        if (indexPath.section == kSectionRisk)
        {
            
            if ([self.riskArray count] == 0)
            {
                [_cell.lblCategory setText:LOC(@"KEY_STRING_NONE")];
                _cell.imgSeperator.hidden = NO;
            }
            
            else
            {
                CustomerRiskData *riskData = [self.riskArray objectAtIndex:indexPath.row];
                
                if (!isEmpty(riskData)) {
                    [_cell.lblCategory setText:riskData.riskCatDesc];
                    [_cell.lblSubCategory setText:riskData.riskSubCatDesc];
                    [_cell.imgSeperator setHidden:YES];
                }
                
                if (indexPath.row == [self.riskArray count] -1) {
                    _cell.imgSeperator.hidden = NO;
                }
            }
        }
        
        else if (indexPath.section == kSectionVulnerability)
        {
            [_cell.imgSeperator setHidden:YES];
            CustomerVulnerabilityData *vulnerabilityData = [self.vulnerabilityArray count] ?[self.vulnerabilityArray objectAtIndex:indexPath.row]:nil;
            ;//[self.vulnerabilityArray objectAtIndex:0];
            if (!isEmpty(vulnerabilityData.vulCatDesc) && !isEmpty(vulnerabilityData.vulSubCatDesc))
            {
                _cell.lblCategory.text = vulnerabilityData.vulCatDesc;
                _cell.lblSubCategory.text = vulnerabilityData.vulSubCatDesc;
            }
            else
            {
                _cell.lblCategory.text = LOC(@"KEY_STRING_NONE");
                _cell.imgSeperator.hidden = NO;
            }
            
            if (indexPath.row == [self.vulnerabilityArray count] -1) {
                _cell.imgSeperator.hidden = NO;
            }
        }
        *cell = _cell;
        
    }
    
    else if ([*cell isKindOfClass:[PSCP12InfoCell class]])
    {
        PSCP12InfoCell *_cell = (PSCP12InfoCell *)*cell;
        if (indexPath.section == kSectionCertificate)
        {
            if (indexPath.row == kRowCertificateIssued) {
                _cell.lblHeader.text = LOC(@"KEY_STRING_CERTIFICATE_ISSUED");
                _cell.lblTitle.text = [UtilityClass stringFromDate:self.appointment.appointmentToCP12Info.issuedDate dateFormat:kDateTimeStyle8];
            }
            else if (indexPath.row == kRowInspectionRef) {
                _cell.lblHeader.text = LOC(@"KEY_STRING_INSPECTION_REF");
                _cell.lblTitle.text = [self.appointment.appointmentToCP12Info.jsgNumber stringValue];
            }
            else if (indexPath.row == kRowExpiry) {
                _cell.lblHeader.text = LOC(@"KEY_STRING_EXPIRY");
                _cell.lblTitle.text = [UtilityClass stringFromDate:self.appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8];
            }
            *cell = _cell;
        }
        
        else if (indexPath.section == kSectionInspectionForm)
        {
            if (indexPath.row == kRowInspectionForm) {
                _cell.lblHeader.text = LOC(@"KEY_STRING_INSPECTION_FORM");
                _cell.lblTitle.text = self.appointment.appointmentStatus;
                AppointmentStatus appointmentStatus = [self.appointment getStatus];
                AppointmentType appointmentType = [self.appointment getType];
                if (appointmentStatus == [UtilityClass completionStatusForAppointmentType:appointmentType])
                {
                    _cell.lblTitle.text = LOC(@"KEY_STRING_COMPLETED");
                }
                
                else if (appointmentStatus == AppointmentStatusInProgress)
                {
                    _cell.lblTitle.text = LOC(@"KEY_STRING_STARTED");
                    //[self.appointment stringWithAppointmentStatus:appointmentStatus];
                }
            
                if (appointmentStatus != [UtilityClass completionStatusForAppointmentType:appointmentType] && appointmentStatus != AppointmentStatusNoEntry)
                {
                    _cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                }
                else
                {
                    _cell.accessoryType = UITableViewCellAccessoryNone;
                }
                
            }
            *cell = _cell;
        }
        else if (indexPath.section == kSectionOtherRelevantInfo)
        {
            if (indexPath.row == kRowAppointments) {
                _cell.lblHeader.text = [LOC(@"KEY_STRING_APPOINTMENTS") uppercaseString];
                _cell.lblTitle.text = [self.appointment.appointmentToAppInfoData.totalAppointments stringValue];
                
            }
            else if (indexPath.row == kRowNoEntrys) {
                _cell.lblHeader.text = [LOC(@"NO ENTRY") uppercaseString];
                _cell.lblTitle.text = [self.appointment.appointmentToAppInfoData.totalNoEntries stringValue];
            }
            *cell = _cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == kSectionInspectionForm && indexPath.row == kRowInspectionForm)
    {
        if ([self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]] && [self.appointment getStatus] != AppointmentStatusNoEntry)
        {
            PSGasSurveyViewController *gasSurveyViewController = [[PSGasSurveyViewController alloc] initWithNibName:@"PSGasSurveyViewController" bundle:nil];
            [gasSurveyViewController setAppointment:self.appointment];
            [self.navigationController pushViewController:gasSurveyViewController animated:YES];
        }
    }
}

-(void)dealloc
{
}


@end
