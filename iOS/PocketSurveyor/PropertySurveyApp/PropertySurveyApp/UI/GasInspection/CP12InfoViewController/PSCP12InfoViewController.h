//
//  PSCP12InfoViewController.h
//  PropertySurveyApp
//
//  Created by TkXel on 05/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
@class PSCP12InfoCell;
@class PSCP12AsbestosCell;
@class PSCP12RiskVulCell;

@interface PSCP12InfoViewController : PSCustomViewController

@property (strong, nonatomic) NSMutableArray *headerViewArray;
@property (weak, nonatomic) IBOutlet PSCP12InfoCell *cp12InfoCell;
@property (weak,   nonatomic) IBOutlet PSCP12AsbestosCell *asbestosCell;
@property (weak,   nonatomic) IBOutlet PSCP12RiskVulCell *riskVulCell;
@property (strong, nonatomic) PSBarButtonItem *noEntryButton;
@property (weak,   nonatomic) Appointment *appointment;
@property (strong, nonatomic) NSArray *asbestosArray;
@property (strong, nonatomic) NSArray *riskArray;
@property (strong, nonatomic) NSArray *vulnerabilityArray;

@end
