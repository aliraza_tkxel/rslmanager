//
//  PSApplianceDetailViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSApplianceDetailViewController.h"
#import "PSBarButtonItem.h"
#import "PSApplianceDetailCell.h"
#import "PSDetectorDetailCell.h"
#import "PSAddApplianceViewController.h"
#import "PSInspectApplianceViewController.h"
#import "PSEditDetectorViewController.h"

#define kNumberOfSections 2
#define kSectionApplianceDetail 0
#define kSectionDetectorDetail 1

#define kApplianceDetailCellHeight 200
#define kDetectorDetailCellHeight 102

#define kApplianceViewMode 0
#define kDetectorViewMode 1

static NSString *applianceDetailCellIdentifier = @"applianceDetailCellIdentifier";
static NSString *detectorDetailCellIdentifier = @"detectorDetailCellIdentifier";

@interface PSApplianceDetailViewController ()

@end

@implementation PSApplianceDetailViewController
{
    NSArray *_appliances;
    NSArray *_detectors;
    BOOL _noResultsFound;
    BOOL isLoadedOnce_;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    // Do any additional setup after loading the view from its nib.
    isLoadedOnce_ = NO;
//    _appliances = [NSArray array];
//    _detectors = [NSArray array];
//    _appliances = [self.appointment.appointmentToProperty.propertyToAppliances allObjects];
//    _detectors = [self.appointment.appointmentToProperty.propertyToDetector allObjects];
//    
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kApplianceInstalledDate ascending:YES];
//    NSArray *sortDescriptors = @[sortDescriptor];
//    _appliances = [_appliances sortedArrayUsingDescriptors:sortDescriptors];
    
    [self.tableView reloadData];

    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _appliances = [NSArray array];
    _detectors = [NSArray array];
    _appliances = [self.appointment.appointmentToProperty.propertyToAppliances allObjects];
    _detectors = [self.appointment.appointmentToProperty.propertyToDetector allObjects];

    if (!isLoadedOnce_) {
        
        isLoadedOnce_ = YES;
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kApplianceInstalledDate ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor];
        _appliances = [_appliances sortedArrayUsingDescriptors:sortDescriptors];
    }
    
    
    
    [self.tableView reloadData];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.addApplianceBtn = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NEW")
                                                                style:PSBarButtonItemStyleAdd
                                                               target:self
                                                               action:@selector(onClickNewAppplianceButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.addApplianceBtn, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Appliance Details"];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickNewAppplianceButton
{
    CLS_LOG(@"onClickNewAppliance");
    PSAddApplianceViewController *addApplianceViewController = [[PSAddApplianceViewController alloc] initWithNibName:@"PSAddApplianceViewController" bundle:nil];
    [addApplianceViewController setAppointment:self.appointment];
    [self.navigationController pushViewController:addApplianceViewController animated:YES];
}


#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return kNumberOfSections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
    switch (indexPath.section)
    {
        case kSectionApplianceDetail:
            if (_noResultsFound)
            {
                rowHeight = 40;
            }
            else
            {
                rowHeight = kApplianceDetailCellHeight;
            }
            break;
        case kSectionDetectorDetail:
            rowHeight = kDetectorDetailCellHeight;
            break;
        default:
            break;
    }
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    switch (section)
    {
        case kSectionApplianceDetail:
            if (!isEmpty(_appliances))
            {
                rows = [_appliances count];
                _noResultsFound = NO;
            }
            else
            {
                rows = 1;
                _noResultsFound = YES;
            }
            break;
        case kSectionDetectorDetail:
            rows =[_detectors count];
            break;
        default:
            break;
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    if (indexPath.section == kSectionApplianceDetail)
    {
        if(_noResultsFound)
        {
            static NSString *noResultsCellIdentifier = @"noResultsFoundCellIdentifier";
            UITableViewCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:noResultsCellIdentifier];
            if(_cell == nil)
            {
                _cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noResultsCellIdentifier];
            }
            // Configure the cell...
            
            _cell.textLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
            _cell.textLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
            _cell.textLabel.text = LOC(@"KEY_STRING_NO_APPLIANCE");
            _cell.textLabel.textAlignment = PSTextAlignmentCenter;
            _cell.selectionStyle = UITableViewCellSelectionStyleNone;
            _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
            cell = _cell;
        }
        
        else
        {
            PSApplianceDetailCell *_cell = [self createCellWithIdentifier:applianceDetailCellIdentifier];
            [self configureCell:&_cell atIndexPath:indexPath];
            cell = _cell;
        }
        
    }
    else if (indexPath.section == kSectionDetectorDetail)
    {
        PSDetectorDetailCell *_cell = [self createCellWithIdentifier:detectorDetailCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    
    UIImageView *backgroundimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 58)];
    backgroundimage.backgroundColor = [UIColor clearColor];
    backgroundimage.opaque = NO;
    
    if(indexPath.row %2 == 0)
    {
        backgroundimage.image = [UIImage imageNamed:@"cell_white"];
    }
    else if (indexPath.row %2 == 1){
        backgroundimage.image = [UIImage imageNamed:@"cell_grey"];
    }
    
    backgroundimage.contentMode = cell.imageView.contentMode;
    cell.backgroundView = backgroundimage;

    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
	if([identifier isEqualToString:applianceDetailCellIdentifier])
	{
        PSApplianceDetailCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:applianceDetailCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSApplianceDetailCell" owner:self options:nil];
            _cell = self.applianceDetailCell;
            self.applianceDetailCell = nil;
        }
		return _cell;
	}
    else if ([identifier isEqualToString:detectorDetailCellIdentifier])
    {
        PSDetectorDetailCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:detectorDetailCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSDetectorDetailCell" owner:self options:nil];
            _cell = self.detectorDetailCell;
            self.detectorDetailCell = nil;
        }
		return _cell;
    }
	return nil;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSApplianceDetailCell class]])
    {
        PSApplianceDetailCell *_cell = (PSApplianceDetailCell *)*cell;
        dispatch_async(dispatch_get_main_queue(), ^{
        
        [_cell.lblTitleType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleMake setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleModel setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleInstalledDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleReplacementDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        
        [_cell.lblType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblMake setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblModel setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblInstalledDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblReplacementDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        
        [_cell.btnInspect addTarget:self action:@selector(onclickApplianceBtnInspect:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnInspect setTag:indexPath.row];
        Appliance *appliance = nil;
        if (!isEmpty(_appliances))
        {
            appliance = [_appliances objectAtIndex:indexPath.row];
        }
        
        [_cell.lblType setText:nil];
        [_cell.lblMake setText:nil];
        [_cell.lblModel setText:nil];
        [_cell.lblInstalledDate setText:nil];
        [_cell.lblReplacementDate setText:nil];

   
        if (!isEmpty(appliance))
        {
 
            if (!isEmpty(appliance.applianceType.typeName))
            {
                [_cell.lblType setText:appliance.applianceType.typeName];
            }
            
            if (!isEmpty(appliance.applianceManufacturer.manufacturerName))
            {
                [_cell.lblMake setText:appliance.applianceManufacturer.manufacturerName];
            }
            
            if (!isEmpty(appliance.applianceModel.modelName))
            {
                [_cell.lblModel setText:appliance.applianceModel.modelName];
            }
                        
            [_cell.lblInstalledDate setText:[UtilityClass stringFromDate:appliance.installedDate dateFormat:kDateTimeStyle8]];
            [_cell.lblReplacementDate setText:[UtilityClass stringFromDate:appliance.replacementDate dateFormat:kDateTimeStyle8]];
            
            if ([appliance.isInspected boolValue])
            {
                #warning Explicitly changed to editing functionality either it was inspected. Ticket# 6112
//                [_cell.btnInspect setEnabled:YES];
                [_cell setAccessoryType:UITableViewCellAccessoryNone];
                [_cell.btnInspect setBackgroundImage:[UIImage imageNamed:@"btn_Complete"] forState:UIControlStateNormal];
            }
            else
            {
                [_cell.btnInspect setBackgroundImage:[UIImage imageNamed:@"checklistIcon"] forState:UIControlStateNormal];
                [_cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//                [_cell.btnInspect setEnabled:YES];
            }
        }
        *cell = _cell;
    });
    }
    
    else if ([*cell isKindOfClass:[PSDetectorDetailCell class]])
    {
        PSDetectorDetailCell *_cell = (PSDetectorDetailCell *) *cell;
        [_cell.lblTitleType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitleQuantity setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblQuantity setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.btnInspect addTarget:self action:@selector(onclickDetectorBtnInspect:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnInspect setTag:indexPath.row];
        [_cell.lblQuantity setText:nil];
        [_cell.lblType setText:nil];

        Detector *detector;
        if (!isEmpty(_detectors))
        {
            detector = [_detectors objectAtIndex:indexPath.row];
        }
        if (!isEmpty(detector))
        {
            [_cell.lblQuantity setText:[detector.detectorCount stringValue]];
            [_cell.lblType setText:detector.detectorType];
            [_cell.btnInspect setBackgroundImage:nil forState:UIControlStateNormal];

            if ([detector.isInspected boolValue])
            {
#warning Explicitly changed to editing functionality either it was inspected. Ticket# 6112
//                _cell.imgStatus.hidden = NO;
//                _cell.btnInspect.hidden = YES;
//                [_cell.btnInspect setEnabled:NO];
                [_cell setAccessoryType:UITableViewCellAccessoryNone];
                [_cell.btnInspect setBackgroundImage:[UIImage imageNamed:@"btn_Complete"] forState:UIControlStateNormal];
            }
            else
            {
//                _cell.imgStatus.hidden = YES;
//                _cell.btnInspect.hidden = NO;
                [_cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
//                [_cell.btnInspect setEnabled:YES];
                [_cell.btnInspect setBackgroundImage:[UIImage imageNamed:@"checklistIcon"] forState:UIControlStateNormal];
            }
            
        }
        *cell = _cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == kSectionApplianceDetail)
    {
        if (!_noResultsFound)
        {
            Appliance *appliance = [_appliances objectAtIndex:indexPath.row];
            if (![appliance.isInspected boolValue])
            {
                PSAddApplianceViewController *addApplianceViewController = [[PSAddApplianceViewController alloc] initWithNibName:@"PSAddApplianceViewController" bundle:nil];
                [addApplianceViewController setAppliance:appliance];
                [addApplianceViewController setAppointment:self.appointment];
                [self.navigationController pushViewController:addApplianceViewController animated:YES];
            }
        }
    }
    
    else if (indexPath.section == kSectionDetectorDetail)
    {
        Detector *detector = [_detectors objectAtIndex:indexPath.row];
        if (![detector.isInspected boolValue])
        {
            PSEditDetectorViewController *editDetectorViewController = [[PSEditDetectorViewController alloc] initWithNibName:@"PSEditDetectorViewController" bundle:nil];
            [editDetectorViewController setDetector:detector];
            [self.navigationController pushViewController:editDetectorViewController animated:YES];
        }
    }
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onclickApplianceBtnInspect:(id)sender
{
    CLS_LOG(@"onclickBtnInspect");
    #warning Explicitly changed to editing functionality either it was inspected. Ticket# 6112
    UIButton *btn = (UIButton *)sender;
    Appliance *appliance = [_appliances objectAtIndex:btn.tag];
    //if (![appliance.isInspected boolValue])
    {
        PSInspectApplianceViewController *inspectApplianceViewController = [[PSInspectApplianceViewController alloc] initWithNibName:@"PSInspectApplianceViewController" bundle:nil];
        [inspectApplianceViewController setViewMode:kApplianceViewMode];
        [inspectApplianceViewController setAppliance:appliance];
        [self.navigationController pushViewController:inspectApplianceViewController animated:YES];
    }
}

- (IBAction)onclickDetectorBtnInspect:(id)sender
{
    CLS_LOG(@"onclickBtnInspect");
#warning Explicitly changed to editing functionality either it was inspected. Ticket# 6112
    UIButton *btn = (UIButton *)sender;
    PSInspectApplianceViewController *inspectApplianceViewController = [[PSInspectApplianceViewController alloc] initWithNibName:@"PSInspectApplianceViewController" bundle:nil];
    Detector *detector = [_detectors objectAtIndex:btn.tag];
    [inspectApplianceViewController setViewMode:kDetectorViewMode];
    [inspectApplianceViewController setDetector:detector];
    [self.navigationController pushViewController:inspectApplianceViewController animated:YES];
}

@end
