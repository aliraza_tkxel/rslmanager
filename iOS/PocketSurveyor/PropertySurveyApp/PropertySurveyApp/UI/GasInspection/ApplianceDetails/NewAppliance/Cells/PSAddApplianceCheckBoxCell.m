//
//  PSAddApplianceCheckBoxCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAddApplianceCheckBoxCell.h"

@implementation PSAddApplianceCheckBoxCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClickCheckBoxBtn:(id)sender {
    if (self.isCheckBoxSelected)
    {
        [self makeDeselected];
        if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectConfirmationButton:)])
        {
            [(PSAddApplianceViewController *)self.delegate didSelectConfirmationButton:self.isCheckBoxSelected];
        }
    }
    else
    {
        [self makeSelected];
        if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectConfirmationButton:)])
        {
            
            [(PSAddApplianceViewController *)self.delegate didSelectConfirmationButton:self.isCheckBoxSelected];
        }
    }
}

- (void) makeSelected
{
    
    self.isCheckBoxSelected = YES;
    [self.btnCheckBox setImage:[UIImage imageNamed:@"checkbox_marked"] forState:UIControlStateNormal];
    [self.lblTitle setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:17]];
}

- (void) makeDeselected
{
    self.isCheckBoxSelected = NO;
    [self.btnCheckBox setImage:[UIImage imageNamed:@"checkbox_unmark"] forState:UIControlStateNormal];
    [self.lblTitle setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
}

@end
