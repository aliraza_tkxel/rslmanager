//
//  PSEditDetectorViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 26/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSEditDetectorViewController.h"

@interface PSEditDetectorViewController ()
{
    NSArray *_pickerOptions;
    NSString *_title;
    NSString *_quantity;
}

@end

@implementation PSEditDetectorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadUI];
    [self loadNavigationonBarItems];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerNotification];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:self.detector.detectorType];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    //[self validateForm];
    [[PSDataUpdateManager sharedManager]updateDetector:self.detector withDetectorCount:[NSNumber numberWithInt:[_quantity integerValue]]];
}


- (IBAction)onClickBtnPicker:(id)sender
{
    if(!isEmpty(_pickerOptions))
    {
        [ActionSheetStringPicker showPickerWithTitle:self.lblTitleQuantity.text
                                                rows:_pickerOptions
                                    initialSelection:self.selectedIndex
                                              target:self
                                       successAction:@selector(onOptionSelected:element:)
                                        cancelAction:@selector(onOptionPickerCancelled:)
                                              origin:sender];
    }
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element
{
    self.selectedIndex = [selectedIndex integerValue];
    _quantity = [_pickerOptions objectAtIndex:self.selectedIndex];
    self.lblQuantity.text = _quantity;
}

- (void) onOptionPickerCancelled:(id)sender {
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}


#pragma mark - Notifications

- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveUpdateDetectorSuccessNotification) name:kUpdateDetectorSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveUpdateDetectorFailureNotification) name:kUpdateDetectorFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateDetectorSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateDetectorFailureNotification object:nil];
}

- (void) onReceiveUpdateDetectorSuccessNotification
{
    [self popOrCloseViewController];
}

- (void) onReceiveUpdateDetectorFailureNotification
{
 
}

#pragma mark - Methods

- (void)loadUI
{
    [self.lblTitleDetector setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [self.lblTitleQuantity setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
    [self.lblDetector setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    [self.lblQuantity setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    
    if (!isEmpty(self.detector))
    {
        _title = self.detector.detectorType;
        _quantity = [self.detector.detectorCount stringValue];
    }
    
    [self.lblDetector setText:_title];
    [self.lblQuantity setText:_quantity];
    _pickerOptions = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
    
    
}
@end
