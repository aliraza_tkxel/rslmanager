//
//  PSEditDetectorViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 26/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSBarButtonItem.h"
@interface PSEditDetectorViewController : PSCustomViewController
@property (weak,   nonatomic) Detector *detector;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleDetector;
@property (strong, nonatomic) IBOutlet UILabel *lblDetector;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleQuantity;
@property (strong, nonatomic) IBOutlet UILabel *lblQuantity;
- (IBAction)onClickBtnPicker:(id)sender;

@end
