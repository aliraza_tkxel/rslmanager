//
//  PSDetectorDetailCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDetectorDetailCell.h"

@implementation PSDetectorDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self.lblTitleType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [self.lblTitleQuantity setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [self.lblType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [self.lblQuantity setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
