//
//  PSApplianceDetailCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSApplianceDetailCell.h"

@implementation PSApplianceDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self.lblTitleType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [self.lblTitleMake setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [self.lblTitleModel setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [self.lblTitleInstalledDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [self.lblTitleReplacementDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        
        [self.lblType setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [self.lblMake setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [self.lblModel setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [self.lblInstalledDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [self.lblReplacementDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
