//
//  PSInspectApplianceViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSBarButtonItem.h"
@class PSInspectAppliancePickerCell;
@class PSInspectApplianceTextFieldCell;
@class PSInspectAppliancePickerTFCell;

@protocol PSInspectApplianceProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption;

@end

typedef NS_ENUM(NSInteger, ViewMode)
{
    ApplianceViewMode,
    DetectorViewMode
};
@interface PSInspectApplianceViewController : PSCustomViewController <PSInspectApplianceProtocol, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblApplianceDetail;
@property (assign, nonatomic) ViewMode viewMode;
@property (weak,   nonatomic) IBOutlet PSInspectAppliancePickerCell *appliancePickerCell;
@property (weak,   nonatomic) IBOutlet PSInspectApplianceTextFieldCell *applianceTextfieldCell;
@property (weak,   nonatomic) IBOutlet PSInspectAppliancePickerTFCell *appliancePickerTFCell;
@property (weak,   nonatomic) Appliance *appliance;
@property (weak,   nonatomic) Detector *detector;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@end
