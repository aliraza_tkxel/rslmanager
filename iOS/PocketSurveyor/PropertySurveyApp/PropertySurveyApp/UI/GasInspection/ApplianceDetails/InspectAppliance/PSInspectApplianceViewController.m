//
//  PSInspectApplianceViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSInspectApplianceViewController.h"
#import "PSBarButtonItem.h"
#import "PSInspectAppliancePickerCell.h"
#import "PSInspectApplianceTextFieldCell.h"
#import "PSInspectAppliancePickerTFCell.h"

#define kApplianceNumberOfRows 12
#define kRowApplianceInspected 0
#define kRowCombustionReading 1
#define kRowOperatingPressure 2
#define kRowSafetyDeviceOperation 3
#define kRowSpillageTest 4
#define kRowSmokePelletFlue 5
#define kRowAdequateVentillation 6
#define kRowFlueCondition 7
#define kRowSatisfactoryTermination 8
#define kRowFluePerformanceChecks 9
#define kRowApplianceServices 10
#define kRowApplianceSafeToUse 11

static NSString *inspectApplianceTextFieldCellIdentifier = @"inspectApplianceTextfieldCellIdentifier";
static NSString *inspectAppliancePickerCellIdentifier = @"inspectAppliancePickerCellIdentifier";
static NSString *inspectAppliancePickerTFCellIdentifier=@"inspectAppliancePickerTFCellIdentifier";

@interface PSInspectApplianceViewController ()
{
    NSInteger _pickerTag;
    
    NSNumber  *_isInspected;
    NSString *_combustionReading;
    NSString *_operatingPressure;
    NSString *_safetyDeviceOperational;
    NSString *_smokePellet;
    NSString *_adequateVentilation;
    NSString *_flueVisualCondition;
    NSString *_satisfactoryTermination;
    NSString *_fluePerformanceChecks;
    NSString *_applianceServiced;
    NSString *_applianceSafeToUse;
    NSNumber *_inspectionID;
    NSNumber *_inspectedBy;
    NSDate *_inspectionDate;
    NSString *_spillageTest;
    NSString *_detectorInspection;
    NSString *_operatingPressureUnit;
}

@end

@implementation PSInspectApplianceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadNavigationonBarItems];
    NSMutableString *applianceLabel =[NSMutableString stringWithString:@""];
    
    if (self.viewMode == ApplianceViewMode)
    {
        if (!isEmpty(self.appliance))
        {
            if (!isEmpty(self.appliance.applianceType.typeName))
            {
                [applianceLabel appendString:self.appliance.applianceType.typeName];
            }
            if (!isEmpty(self.appliance.applianceManufacturer.manufacturerName))
            {
                [applianceLabel appendString:[NSString stringWithFormat:@" > %@",self.appliance.applianceManufacturer.manufacturerName]];
            }
            if (!isEmpty(self.appliance.applianceModel.modelName))
            {
                [applianceLabel appendString:[NSString stringWithFormat:@" > %@",self.appliance.applianceModel.modelName]];
            }
            
            if (!isEmpty(self.appliance.applianceInspection)) {
                [self initializeAppliancesDetails];
            }
        }
    }
    
    else if (self.viewMode == DetectorViewMode)
    {
        if (!isEmpty(self.detector))
        {
            [applianceLabel appendString:@"Detector"];
            if (!isEmpty(self.detector.detectorType))
            {
                [applianceLabel appendString:[NSString stringWithFormat:@" > %@",self.detector.detectorType]];
            }
            
            if (!isEmpty(self.detector.detectorInspection)) {
                [self initializeAppliancesDetails];
            }
        }
    }
    [self.lblApplianceDetail setText:applianceLabel];
    [self.lblApplianceDetail setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
}

-(void) viewWillAppear:(BOOL)animated
{
    [self registerNotification];
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    //[self.saveButton setEnabled:NO];
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:@"Inspect Appliance"];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    if (self.viewMode == ApplianceViewMode)
    {
        [self validateForm];
    }
    else if (self.viewMode == DetectorViewMode)
    {
        [self saveInspectionForm];
    }
    //[self.navigationController popToRootViewControllerAnimated:YES];
    
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 43;
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    switch (self.viewMode) {
        case ApplianceViewMode:
            rows = kApplianceNumberOfRows;
            break;
        case DetectorViewMode:
            rows = 1;
            break;
        default:
            break;
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (self.viewMode == ApplianceViewMode)
    {
        if (indexPath.row == kRowCombustionReading)
        {
            PSInspectApplianceTextFieldCell *_cell = [self createCellWithIdentifier:inspectApplianceTextFieldCellIdentifier];
            [self configureCell:&_cell atIndexPath:indexPath];
            cell = _cell;
        }
        else if (indexPath.row == kRowOperatingPressure)
        {
            PSInspectAppliancePickerTFCell *_cell = [self createCellWithIdentifier:inspectAppliancePickerTFCellIdentifier];
            [self configureCell:&_cell atIndexPath:indexPath];
            cell = _cell;
        }
        else
        {
            PSInspectAppliancePickerCell *_cell = [self createCellWithIdentifier:inspectAppliancePickerCellIdentifier];
            [self configureCell:&_cell atIndexPath:indexPath];
            cell = _cell;
        }
    }
    
    else if (self.viewMode == DetectorViewMode)
    {
        PSInspectAppliancePickerCell *_cell = [self createCellWithIdentifier:inspectAppliancePickerCellIdentifier];
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (id) createCellWithIdentifier:(NSString*)identifier
{
    if (self.viewMode == ApplianceViewMode) {
        
        if([identifier isEqualToString:inspectApplianceTextFieldCellIdentifier])
        {
            PSInspectApplianceTextFieldCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:inspectApplianceTextFieldCellIdentifier];
            if(_cell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"PSInspectApplianceTextFieldCell" owner:self options:nil];
                _cell = self.applianceTextfieldCell;
                self.applianceTextfieldCell = nil;
            }
            return _cell;
        }
        else if ([identifier isEqualToString:inspectAppliancePickerTFCellIdentifier])
        {
            PSInspectAppliancePickerTFCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:inspectAppliancePickerTFCellIdentifier];
            if (_cell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"PSInspectAppliancePickerTFCell" owner:self options:nil];
                _cell = self.appliancePickerTFCell;
                self.appliancePickerTFCell = nil;
            }
            return _cell;
        }
        else if ([identifier isEqualToString:inspectAppliancePickerCellIdentifier])
        {
            PSInspectAppliancePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:inspectAppliancePickerCellIdentifier];
            if(_cell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"PSInspectAppliancePickerCell" owner:self options:nil];
                _cell = self.appliancePickerCell;
                self.appliancePickerCell = nil;
            }
            return _cell;
        }
    }
    
    else if (self.viewMode == DetectorViewMode)
    {
        PSInspectAppliancePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:inspectAppliancePickerCellIdentifier];
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSInspectAppliancePickerCell" owner:self options:nil];
            _cell = self.appliancePickerCell;
            self.appliancePickerCell = nil;
        }
        return _cell;
    }
	return nil;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (self.viewMode == ApplianceViewMode) {
        
        if ([*cell isKindOfClass:[PSInspectApplianceTextFieldCell class]])
        {
            PSInspectApplianceTextFieldCell *_cell = (PSInspectApplianceTextFieldCell *)*cell;
            [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
            [_cell.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
            [_cell.txtTitle setDelegate:self];
            [_cell.txtTitle setKeyboardType:UIKeyboardTypeDefault];
            //Just doing because on scrolling it is getting values from other cells, Values will be set downwards
            [_cell.txtTitle setText:nil];

            switch (indexPath.row)
            {
                case kRowCombustionReading:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_COMBUSTION_ANALYSER_READING")];
                    [_cell.txtTitle setTag:kRowCombustionReading];
                    if (!isEmpty(_combustionReading))
                    {
                        [_cell.txtTitle setText:_combustionReading];
                    }
                    break;
                default:
                    break;
            }
            *cell = _cell;
        }
        else if ([*cell isKindOfClass:[PSInspectAppliancePickerTFCell class]])
        {
            PSInspectAppliancePickerTFCell *_cell = (PSInspectAppliancePickerTFCell *)*cell;
            [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
            [_cell.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
            [_cell.txtUnit setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
            [_cell.txtTitle setDelegate:self];
            [_cell.lblHeader setText:LOC(@"KEY_STRING_OPERATING_PRESSURE_OR_HEAT_INPUT")];
            [_cell.txtTitle setTag:kRowOperatingPressure];
            [_cell.txtTitle setKeyboardType:UIKeyboardTypeDecimalPad];
            [_cell setDelegate:self];
            //Just doing because on scrolling it is getting values from other cells, Values will be set downwards
            [_cell.txtTitle setText:nil];
            [_cell.txtUnit setText:nil];

            
            if (!isEmpty(_operatingPressure))
            {
                [_cell.txtTitle setText:_operatingPressure];
            }
            if (!isEmpty(_operatingPressureUnit))
            {
                [_cell.txtUnit setText:_operatingPressureUnit];
            }
            
            _cell.pickerOptions = @[LOC(@"KEY_STRING_BTU"), LOC(@"KEY_STRING_KW"), LOC(@"KEY_STRING_MBAR")];
            [_cell.btnPicker addTarget:self action:@selector(onClickOperatingUnit:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        else if ([*cell isKindOfClass:[PSInspectAppliancePickerCell class]])
        {
            PSInspectAppliancePickerCell *_cell = (PSInspectAppliancePickerCell *)*cell;
            [_cell setDelegate:self];
            [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
            [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
            //Just doing because on scrolling it is getting values from other cells, Values will be set downwards
            [_cell.lblTitle setText:nil];
            
            switch (indexPath.row) {
                case kRowApplianceInspected:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_APPLIANCE_INSPECTED")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
                    [_cell.lblTitle setText:isEmpty(_isInspected)?nil:([_isInspected boolValue] == YES)?LOC(@"KEY_ALERT_YES"):LOC(@"KEY_ALERT_NO")];
                    [_cell.btnPicker addTarget:self action:@selector(onClickApplianceInspected:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowSafetyDeviceOperation:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_SAFETY_DEVICE_CORRECT_OPERATION")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_safetyDeviceOperational))
                    {
                        [_cell.lblTitle setText:_safetyDeviceOperational];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onclickSafetyDeviceOperation:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowSpillageTest:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_SPILLAGE_TEST")];
                    _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_spillageTest))
                    {
                        [_cell.lblTitle setText:_spillageTest];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onClickSpillageTest:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowSmokePelletFlue:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_SMOKE_PELLET_FLUE_TEST")];
                    _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_smokePellet))
                    {
                        [_cell.lblTitle setText:_smokePellet];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickSmokePelletFlue:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowAdequateVentillation:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_ADEQUATE_VENTILATION")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
                    if (!isEmpty(_adequateVentilation))
                    {
                        [_cell.lblTitle setText:_adequateVentilation];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickAdequateVentillation:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowFlueCondition:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_FLUE_VISUAL_CONDITION")];
                    _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_flueVisualCondition))
                    {
                        [_cell.lblTitle setText:_flueVisualCondition];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickFlueCondition:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowSatisfactoryTermination:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_SATISFACTORY_TERMINATION")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_satisfactoryTermination))
                    {
                        [_cell.lblTitle setText:_satisfactoryTermination];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickSatisfactoryTermination:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowFluePerformanceChecks:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_FLUE_PERFORMANCE_CHECKS")];
                    _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
                    if (!isEmpty(_fluePerformanceChecks))
                    {
                        [_cell.lblTitle setText:_fluePerformanceChecks];
                    }
                    
                    [_cell.btnPicker addTarget:self action:@selector(onCLickFluePerformance:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowApplianceServices:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_APPLIANCE_SERVICED")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
                    
                    if (!isEmpty(_applianceServiced))
                    {
                        [_cell.lblTitle setText:_applianceServiced];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickApplianceServices:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case kRowApplianceSafeToUse:
                    [_cell.lblHeader setText:LOC(@"KEY_STRING_APPLIANCE_SAFE_TO_USE")];
                    _cell.pickerOptions = @[LOC(@"KEY_ALERT_YES"), LOC(@"KEY_ALERT_NO")];
                    
                    if (!isEmpty(_applianceSafeToUse))
                    {
                        [_cell.lblTitle setText:_applianceSafeToUse];
                    }
                    [_cell.btnPicker addTarget:self action:@selector(onCLickSafeToUse:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                    
                default:
                    break;
            }
            *cell = _cell;
        }
    }
    
    else if (self.viewMode == DetectorViewMode)
    {
        PSInspectAppliancePickerCell *_cell = (PSInspectAppliancePickerCell *)*cell;
        [_cell setDelegate:self];
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblHeader setText:LOC(@"KEY_STRING_DETECTORS_TESTED")];
        _cell.pickerOptions = @[LOC(@"KEY_STRING_PASS"), LOC(@"KEY_STRING_FAIL"), LOC(@"KEY_STRING_NOT_APPLICABLE")];
        //Just doing because on scrolling it is getting values from other cells, Values will be set downwards
        [_cell.lblTitle setText:nil];

        if (!isEmpty(_detectorInspection))
        {
            [_cell.lblTitle setText:_detectorInspection];
        }
    }
}


- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignAllResponders];
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickOperatingUnit:(id)sender
{
    CLS_LOG(@"onClickApplianceInspected");
    [self resignAllResponders];
    _pickerTag = kRowOperatingPressure;
}

- (IBAction)onClickApplianceInspected:(id)sender
{
    CLS_LOG(@"onClickApplianceInspected");
    [self resignAllResponders];
    _pickerTag = kRowApplianceInspected;
}

- (IBAction)onclickSafetyDeviceOperation:(id)sender
{
    CLS_LOG(@"onclickSafetyDeviceOperation");
    [self resignAllResponders];
    _pickerTag = kRowSafetyDeviceOperation;
}

- (IBAction)onClickSpillageTest:(id)sender
{
    CLS_LOG(@"onClickSpillageTest");
    [self resignAllResponders];
    _pickerTag = kRowSpillageTest;
}

- (IBAction)onCLickSmokePelletFlue:(id)sender
{
    CLS_LOG(@"onCLickSmokePelletFlue");
    [self resignAllResponders];
    _pickerTag = kRowSmokePelletFlue;
}

- (IBAction)onCLickAdequateVentillation:(id)sender
{
    CLS_LOG(@"onCLickAdequateVentillation");
    [self resignAllResponders];
    _pickerTag = kRowAdequateVentillation;
}

- (IBAction)onCLickFlueCondition:(id)sender
{
    CLS_LOG(@"onCLickFlueCondition");
    [self resignAllResponders];
    _pickerTag = kRowFlueCondition;
}

- (IBAction)onCLickSatisfactoryTermination:(id)sender
{
    CLS_LOG(@"onCLickSatisfactoryTermination");
    [self resignAllResponders];
    _pickerTag = kRowSatisfactoryTermination;
}

- (IBAction)onCLickFluePerformance:(id)sender
{
    CLS_LOG(@"onCLickFluePerformance");
    [self resignAllResponders];
    _pickerTag = kRowFluePerformanceChecks;
}

- (IBAction)onCLickApplianceServices:(id)sender
{
    CLS_LOG(@"onCLickApplianceServices");
    [self resignAllResponders];
    _pickerTag = kRowApplianceServices;
}

- (IBAction)onCLickSafeToUse:(id)sender
{
    CLS_LOG(@"onCLickSafeToUse");
    [self resignAllResponders];
    _pickerTag = kRowApplianceSafeToUse;
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case kRowOperatingPressure: {
//            if([_operatingPressureUnit isEqualToString:LOC(@"KEY_STRING_N/A")]) {
//                _operatingPressure = @"";
//                [textField resignFirstResponder];
//            }
//            else {
                self.selectedControl = textField;
//            }
        }
            break;
        default:
            self.selectedControl = textField;
            break;
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    switch (textField.tag) {
        case kRowCombustionReading:
            _combustionReading = textField.text;
            break;
        case kRowOperatingPressure:
                _operatingPressure = textField.text;
            break;
        default:
            break;
    }
    [textField resignFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL textFieldShouldReturn = YES;
    NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
    
    if ([stringAfterReplacement length] > 15)
    {
        textFieldShouldReturn = NO;
        return textFieldShouldReturn;
    }

    switch (textField.tag) {
        case kRowCombustionReading:
            _combustionReading = stringAfterReplacement;
            break;
        case kRowOperatingPressure:
            _operatingPressure = stringAfterReplacement;
            break;
        default:
            break;
    }

    
    return textFieldShouldReturn;
    
}

#pragma mark - Protocol Methods
/*!
 @discussion
 Method didSelecPickerOption Called upon selection of picker option.
 */
- (void) didSelecPickerOption:(NSString *)pickerOption
{
    if (self.viewMode == ApplianceViewMode)
    {
        switch (_pickerTag) {
            case kRowApplianceInspected:
                _isInspected = [NSNumber numberWithBool:[pickerOption boolValue]];
                //[self.saveButton setEnabled:[_isInspected boolValue]];
                break;
            case kRowSafetyDeviceOperation:
                _safetyDeviceOperational = pickerOption;
                break;
            case kRowSpillageTest:
                _spillageTest = pickerOption;
                break;
            case kRowSmokePelletFlue:
                _smokePellet = pickerOption;
                break;
            case kRowAdequateVentillation:
                _adequateVentilation = pickerOption;
                break;
            case kRowFlueCondition:
                _flueVisualCondition = pickerOption;
                break;
            case kRowSatisfactoryTermination:
                _satisfactoryTermination = pickerOption;
                break;
            case kRowFluePerformanceChecks:
                _fluePerformanceChecks = pickerOption;
                break;
            case kRowApplianceServices:
                _applianceServiced = pickerOption;
                break;
            case kRowApplianceSafeToUse:
                _applianceSafeToUse = pickerOption;
                break;
            case kRowOperatingPressure:
            {
                _operatingPressureUnit = pickerOption;
                
//                if([pickerOption isEqualToString:LOC(@"KEY_STRING_N/A")]) {
//                    _operatingPressure = @"";
//                    [self.tableView reloadData];
//                }
            }
                break;
            default:
                break;
        }
    }
    else if (self.viewMode == DetectorViewMode)
    {
        [self.saveButton setEnabled:YES];
        _detectorInspection = pickerOption;
    }
}

#pragma mark - Notifications

- (void) registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveApplianceInspectionFormSuccessNotification) name:kSaveApplianceInspectionFormSuccessNotification object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSaveApplianceInspectionFormFailureNotification) name:kSaveApplianceInspectionFormFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveApplianceInspectionFormSuccessNotification object:nil];
    // [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveApplianceInspectionFormFailureNotification object:nil];
}

- (void) onReceiveSaveApplianceInspectionFormSuccessNotification
{
    [self popOrCloseViewController];
}

#pragma mark - Methods

- (void) validateForm
{
    NSString *alertMessage = nil;
    NSString *emptyField;
    BOOL inValid = NO;
    
   if ([_isInspected boolValue] && [self.appliance.isLandlordAppliance boolValue])
    {
        for (int cellNumber = kRowCombustionReading; cellNumber <= kRowApplianceSafeToUse; ++cellNumber)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellNumber inSection:0];
            UITableViewCell *cell = (UITableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
            emptyField = nil;
            alertMessage = nil;
            inValid = NO;
            
            if ([cell  isKindOfClass:[PSInspectApplianceTextFieldCell class]]) {
                PSInspectApplianceTextFieldCell *_cell = (PSInspectApplianceTextFieldCell *)cell;
                if (![_cell isValid])
                {
                    emptyField = [_cell.lblHeader.text lowercaseString];
                    inValid = YES;
                }
            }
            
            else if ([cell isKindOfClass:[PSInspectAppliancePickerCell class]])
            {
                PSInspectAppliancePickerCell *_cell = (PSInspectAppliancePickerCell *)cell;
                if (![_cell isValid])
                {
                    emptyField = [_cell.lblHeader.text lowercaseString];
                    inValid = YES;
                }
                
            }
            else if ([cell isKindOfClass:[PSInspectAppliancePickerTFCell class]])
            {
                PSInspectAppliancePickerTFCell *_cell = (PSInspectAppliancePickerTFCell *)cell;
                if (![_cell isValid]) {
                    emptyField = [_cell.lblHeader.text lowercaseString];
                    inValid = YES;
                }
            }
            if (inValid)
            {
                alertMessage = [NSString stringWithFormat:@"Please enter %@.", emptyField];
                break;
            }
        }
    }
    
    
    if (inValid)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:alertMessage delegate:nil cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
        [alert show];
    }
    
    else
    {
        [self saveInspectionForm];
    }
    
}

- (void) saveInspectionForm
{
    NSMutableDictionary *inspectionFormDictionary = [NSMutableDictionary dictionary];
    if (self.viewMode == ApplianceViewMode)
    {
        _inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
        _inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
        _inspectionID = (isEmpty(_inspectionID) || (_inspectionID == [NSNumber numberWithInt:-1]))?[NSNumber numberWithInt:-1]:_inspectionID;
        
        [inspectionFormDictionary setObject:isEmpty(_isInspected)?[NSNull null]:_isInspected forKey:kIsInspected];
        [inspectionFormDictionary setObject:isEmpty(_combustionReading)?[NSNull null]:_combustionReading forKey:kCombustionReading];
        [inspectionFormDictionary setObject:isEmpty(_operatingPressure)?[NSNull null]:_operatingPressure forKey:kOperatingPressure];
        [inspectionFormDictionary setObject:isEmpty(_safetyDeviceOperational)?[NSNull null]:_safetyDeviceOperational forKey:kSafetyDeviceOperational];
        [inspectionFormDictionary setObject:isEmpty(_smokePellet)?[NSNull null]:_smokePellet forKey:kSmokePellet];
        [inspectionFormDictionary setObject:isEmpty(_adequateVentilation)?[NSNull null]:_adequateVentilation forKey:kAdequateVentilation];
        [inspectionFormDictionary setObject:isEmpty(_flueVisualCondition)?[NSNull null]:_flueVisualCondition forKey:kFlueVisualCondition];
        [inspectionFormDictionary setObject:isEmpty(_satisfactoryTermination)?[NSNull null]:_satisfactoryTermination forKey:kSatisfactoryTermination];
        [inspectionFormDictionary setObject:isEmpty(_fluePerformanceChecks)?[NSNull null]:_fluePerformanceChecks forKey:kFluePerformanceChecks];
        [inspectionFormDictionary setObject:isEmpty(_applianceServiced)?[NSNull null]:_applianceServiced forKey:kApplianceServiced];
        [inspectionFormDictionary setObject:isEmpty(_applianceSafeToUse)?[NSNull null]:_applianceSafeToUse forKey:kApplianceSafeTouse];
        [inspectionFormDictionary setObject:isEmpty(_inspectionID)?[NSNull null]:_inspectionID forKey:kInspectionId];
        [inspectionFormDictionary setObject:isEmpty(_inspectedBy)?[NSNull null]:_inspectedBy forKey:kInspectedBy];
        [inspectionFormDictionary setObject:isEmpty(_inspectionDate)?[NSNull null]:_inspectionDate forKey:kInspectionDate];
        [inspectionFormDictionary setObject:isEmpty(_spillageTest)?[NSNull null]:_spillageTest forKey:kSpillageTest];
        [inspectionFormDictionary setObject:isEmpty(_operatingPressureUnit)?[NSNull null]:_operatingPressureUnit forKey:kOperatingPressureUnit];
        
        [[PSApplianceManager sharedManager]saveInspectionForm:inspectionFormDictionary forAppliance:self.appliance];
    }
    
    else if (self.viewMode == DetectorViewMode)
    {
        _inspectedBy = [[SettingsClass sharedObject]loggedInUser].userId;
        _inspectionDate = isEmpty(_inspectionDate)?[NSDate date]:_inspectionDate;
        _inspectionID = isEmpty(_inspectionID)?[NSNumber numberWithInt:-1]:_inspectionID;
        [inspectionFormDictionary setObject:isEmpty(_isInspected)?[NSNull null]:_isInspected forKey:kIsInspected];
        [inspectionFormDictionary setObject:isEmpty(_inspectionID)?[NSNull null]:_inspectionID forKey:kInspectionId];
        [inspectionFormDictionary setObject:isEmpty(_inspectedBy)?[NSNull null]:_inspectedBy forKey:kInspectedBy];
        [inspectionFormDictionary setObject:isEmpty(_detectorInspection)?[NSNull null]:_detectorInspection forKey:kDetectorTest];
        [inspectionFormDictionary setObject:isEmpty(_inspectionDate)?[NSNull null]:_inspectionDate forKey:kInspectionDate];
        [[PSApplianceManager sharedManager] saveDetectorInspectionForm:inspectionFormDictionary forDetector:self.detector];
    }
}

-(void) initializeAppliancesDetails {
    
    if (self.viewMode == ApplianceViewMode) {
        _inspectionID = self.appliance.applianceInspection.inspectionID;
        _inspectedBy = self.appliance.applianceInspection.inspectedBy;
        _inspectionDate = self.appliance.applianceInspection.inspectionDate;
        _isInspected = self.appliance.applianceInspection.isInspected;
        _combustionReading = self.appliance.applianceInspection.combustionReading;
        _operatingPressure = self.appliance.applianceInspection.operatingPressure;
        _safetyDeviceOperational = self.appliance.applianceInspection.safetyDeviceOperational;
        _smokePellet = self.appliance.applianceInspection.smokePellet;
        _adequateVentilation = self.appliance.applianceInspection.adequateVentilation;
        _flueVisualCondition = self.appliance.applianceInspection.flueVisualCondition;
        _satisfactoryTermination = self.appliance.applianceInspection.satisfactoryTermination;
        _fluePerformanceChecks = self.appliance.applianceInspection.fluePerformanceChecks;
        _applianceServiced = self.appliance.applianceInspection.applianceServiced;
        _applianceSafeToUse = self.appliance.applianceInspection.applianceSafeToUse;
        _inspectedBy = self.appliance.applianceInspection.inspectedBy;
        _spillageTest = self.appliance.applianceInspection.spillageTest;
        _operatingPressureUnit = self.appliance.applianceInspection.operatingPressureUnit;
        //[self.saveButton setEnabled:[_isInspected boolValue]];
    }
    else if (self.viewMode == DetectorViewMode) {
        _inspectedBy = self.detector.detectorInspection.inspectedBy;
        _inspectionDate = self.detector.detectorInspection.inspectionDate;
        _inspectionID = self.detector.detectorInspection.inspectionID;
        _detectorInspection = self.detector.detectorInspection.detectorsTested;
    }
    
    
}

@end
