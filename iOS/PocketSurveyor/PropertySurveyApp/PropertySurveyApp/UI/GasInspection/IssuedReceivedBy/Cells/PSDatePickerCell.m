//
//  PSDatePickerCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDatePickerCell.h"

@interface PSDatePickerCell()

@property (nonatomic, retain) ActionSheetDatePicker * picker;

@end
@implementation PSDatePickerCell
@synthesize pickerOptions;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClickPickerBtn:(id)sender
{
    if(self.selectedDate == nil)
    {
        self.selectedDate = [NSDate date];
    }
    
    self.picker = [ActionSheetDatePicker showPickerWithTitle:self.lblHeader.text
                                              datePickerMode:UIDatePickerModeDate
                                                selectedDate:self.selectedDate
                                                      target:self
                                                      action:@selector(onDateSelected:element:)
                                                      origin:sender];
}

- (void)onDateSelected:(NSDate *)selectedDate element:(id)element {
    self.selectedDate = selectedDate;
    
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectDate:)])
    {
        [(PSIssueReceivedViewController *)self.delegate didSelectDate:self.selectedDate];
    }
}


@end
