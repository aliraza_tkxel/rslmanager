//
//  PSDatePickerCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSIssueReceivedViewController.h"
@interface PSDatePickerCell : UITableViewCell
@property (weak,    nonatomic) id<PSIssueReceivedProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (strong, nonatomic) NSDate *selectedDate;
- (IBAction)onClickPickerBtn:(id)sender;

@end
