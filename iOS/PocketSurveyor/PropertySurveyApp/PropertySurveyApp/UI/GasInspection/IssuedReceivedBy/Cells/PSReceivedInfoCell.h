//
//  PSReceivedInfoCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSReceivedInfoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end
