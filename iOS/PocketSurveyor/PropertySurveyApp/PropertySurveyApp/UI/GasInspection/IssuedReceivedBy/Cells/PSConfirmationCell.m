//
//  PSConfirmationCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSConfirmationCell.h"

@implementation PSConfirmationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClickConfirmationBtn:(id)sender
{
    if (self.isCheckBoxSelected)
    {
        [self makeDeselected];
        if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectConfirmationBtn:)])
        {
            [(PSIssueReceivedViewController *)self.delegate didSelectConfirmationBtn:self.isCheckBoxSelected];
        }
    }
    else
    {
        [self makeSelected];
       if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectConfirmationBtn:)])
        {
            
            [(PSIssueReceivedViewController *)self.delegate didSelectConfirmationBtn:self.isCheckBoxSelected];
        }
    }
}

- (void) makeSelected
{
    
    self.isCheckBoxSelected = YES;
    [self.BtnCheckBox setImage:[UIImage imageNamed:@"checkbox_marked"] forState:UIControlStateNormal];
    [self.lblConfirmation setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:13]];
}

- (void) makeDeselected
{
    self.isCheckBoxSelected = NO;
    [self.BtnCheckBox setImage:[UIImage imageNamed:@"checkbox_unmark"] forState:UIControlStateNormal];
    [self.lblConfirmation setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:13]];
}
@end
