//
//  PSConfirmationCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSIssueReceivedViewController.h"
@interface PSConfirmationCell : UITableViewCell
@property (assign, nonatomic, setter=setCheckBoxSelected:) BOOL isCheckBoxSelected;
@property (weak,   nonatomic) id<PSIssueReceivedProtocol> delegate;
- (IBAction)onClickConfirmationBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *BtnCheckBox;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmation;

- (void) makeSelected;
- (void) makeDeselected;
@end
