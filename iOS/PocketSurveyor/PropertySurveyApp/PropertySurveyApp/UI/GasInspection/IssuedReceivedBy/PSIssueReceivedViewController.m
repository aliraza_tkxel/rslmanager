//
//  PSIssueReceivedViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSIssueReceivedViewController.h"
#import "PSReceivedInfoCell.h"
#import "PSReceivedPickerCell.h"
#import "PSDatePickerCell.h"
#import "PSBarButtonItem.h"
#import "PSConfirmationCell.h"
#import "PSCoreDataManager.h"

#define kTotalNumberOfRows 6
#define kRowInspectionRef 0
#define kRowIssuedBy 1
#define kRowIssuedDate 2
#define kRowReceivedBy 3
#define kRowReceivedDate 4
#define kRowConfirmation 5

static NSString *pickerCellIdentifier = @"pickerCellIdentifier";
static NSString *datePickerCellIdentifier = @"datePickerCellIdentifier";
static NSString *infoCellIdentifier = @"infoCellIdentifier";
@interface PSIssueReceivedViewController ()
{
    NSInteger _datepickerTag;
    BOOL _isCheckBoxSelected;
    Surveyor *_surveyor;
    NSDate *_issuedDate;
    NSDate *_receivedDate;
    NSString *_receivedOnBehalfOf;
}

@end

@implementation PSIssueReceivedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _surveyor =  [[PSCoreDataManager sharedManager] surveyorWithIdentifier:self.cp12Info.issuedBy];
    _isCheckBoxSelected = [self.cp12Info.inspectionCarried boolValue];
    _issuedDate = self.cp12Info.issuedDate;
    _receivedDate = self.cp12Info.receivedDate;
    _receivedOnBehalfOf = self.cp12Info.receivedOnBehalfOf;
    [self loadNavigationonBarItems];

    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [self registerNotifications];
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                      style:PSBarButtonItemStyleDefault
                                                                     target:self
                                                                     action:@selector(onClickSaveButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:LOC(@"KEY_STRING_ISSUED_RECEIVED_BY")];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    CLS_LOG(@"onClickSaveButton");
    [self validateForm];
    
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 44;
    if (indexPath.row == kRowConfirmation)
    {
        rowHeight = 59;
    }
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

        return kTotalNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = nil;
    
    if (indexPath.row == kRowInspectionRef)
    {
        static NSString *inspectionRefCellidentifier = @"infoCellIdentifier";
        PSReceivedInfoCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:inspectionRefCellidentifier];
        
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSReceivedInfoCell" owner:self options:nil];
            _cell = self.infoCell;
            self.infoCell = nil;
        }

        [self configureCell:&_cell atIndexPath:indexPath];
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell = _cell;
        
    }
    
    else if (indexPath.row == kRowIssuedBy)
    {
        static NSString *issuedbyCellidentifier = @"infoCellIdentifier";
        PSReceivedInfoCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:issuedbyCellidentifier];
        
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSReceivedInfoCell" owner:self options:nil];
            _cell = self.infoCell;
            self.infoCell = nil;
        }
        
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    
    else if (indexPath.row == kRowIssuedDate)
    {
        static NSString *issuedDateCellidentifier = @"datePickerCellIdentifier";
        PSDatePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:issuedDateCellidentifier];
        
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSDatePickerCell" owner:self options:nil];
            _cell = self.dateCell;
            self.dateCell = nil;
        }

        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;

    }
    
    else if (indexPath.row == kRowReceivedBy)
    {
        static NSString *receivedByCellidentifier = @"pickerCellIdentifier";
        PSReceivedPickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:receivedByCellidentifier];
        
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSReceivedPickerCell" owner:self options:nil];
            _cell = self.pickerCell;
            self.pickerCell = nil;
        }
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }

    else if (indexPath.row == kRowReceivedDate)
    {
        static NSString *receivedDateCellidentifier = @"datePickerCellIdentifier";
        PSDatePickerCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:receivedDateCellidentifier];
        
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSDatePickerCell" owner:self options:nil];
            _cell = self.dateCell;
            self.dateCell = nil;
        }

        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    
    else if (indexPath.row == kRowConfirmation)
    {
     static NSString *confirmationCellidentifier = @"confirmationCellIdentifier";
        PSConfirmationCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:confirmationCellidentifier];
        
        if(_cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSConfirmationCell" owner:self options:nil];
            _cell = self.confirmationCell;
            self.confirmationCell = nil;
        }
        
        [self configureCell:&_cell atIndexPath:indexPath];
        cell = _cell;
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSReceivedInfoCell class]])
    {
        PSReceivedInfoCell *_cell = (PSReceivedInfoCell *)*cell;
        if (indexPath.row == kRowInspectionRef)
        {
            [_cell.lblHeader setText:LOC(@"KEY_STRING_INSPECTION_REFERENCE")];
            if (!isEmpty(self.cp12Info.jsgNumber)) {
                [_cell.lblTitle setText:[self.cp12Info.jsgNumber stringValue]];
            }
        }
        
        else if (indexPath.row == kRowIssuedBy)
        {
            [_cell.lblHeader setText:LOC(@"KEY_STRING_ISSUED_BY")];
            [_cell.lblTitle setText:[[SettingsClass sharedObject] loggedInUser].fullName];
        }
        
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        *cell = _cell;
    }
    else if ([*cell isKindOfClass:[PSReceivedPickerCell class]])
    {
        PSReceivedPickerCell *_cell = (PSReceivedPickerCell *)*cell;
        if (!isEmpty(_receivedOnBehalfOf))
        {
            [_cell.lblTitle setText:_receivedOnBehalfOf];
        }
        _cell.pickerOptions = @[LOC(@"KEY_STRING_TENANT"), LOC(@"KEY_STRING_LANDLORD"), LOC(@"KEY_STRING_AGENT"), LOC(@"KEY_STRING_HOME_OWNER")];
        [_cell.lblHeader setText:LOC(@"KEY_STRING_RECEIVED_ON_BEHALF_OF")];
        [_cell setDelegate:self];
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        *cell = _cell;
        
    }
    
    else if ([*cell isKindOfClass:[PSDatePickerCell class]])
    {
        PSDatePickerCell *_cell = (PSDatePickerCell *)*cell;
        
        if (indexPath.row == kRowIssuedDate)
        {
            _cell.lblHeader.text = LOC(@"KEY_STRING_ISSUE_DATE");
            [_cell.btnPicker addTarget:self action:@selector(onClickIssuedDateButton:) forControlEvents:UIControlEventTouchUpInside];
            if (!isEmpty(_issuedDate))
            {
                _cell.lblTitle.text = [UtilityClass stringFromDate:_issuedDate dateFormat:kDateTimeStyle8];
            }
        }
        
        else if (indexPath.row == kRowReceivedDate)
        {
            [_cell.lblHeader setText:LOC(@"KEY_STRING_RECEIVED_DATE")];
            [_cell.btnPicker addTarget:self action:@selector(onClickReceivedDateButton:) forControlEvents:UIControlEventTouchUpInside];
            if (!isEmpty(_receivedDate))
            {
                [_cell.lblTitle setText:[UtilityClass stringFromDate:_receivedDate dateFormat:kDateTimeStyle8]];
            }
        }
        [_cell setDelegate:self];
        [_cell.lblHeader setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSConfirmationCell class]])
    {
        PSConfirmationCell *_cell = (PSConfirmationCell *)*cell;
        [_cell setDelegate:self];
        if (_isCheckBoxSelected)
        {
            [_cell makeSelected];
        }
        else
        {
            [_cell makeDeselected];
        }
        *cell = _cell;
    }
}

#pragma mark - Cell Button Selectors

- (IBAction)onClickIssuedDateButton:(id)sender
{
    CLS_LOG(@"onClickStartPickerButton");
    _datepickerTag = kRowIssuedDate;
    
}

- (IBAction)onClickReceivedDateButton:(id)sender
{
    CLS_LOG(@"onClickEndPickerButton");
    _datepickerTag = kRowReceivedDate;
    
}
#pragma mark - Protocol methods

/*!
 @discussion
 Method didSelectDateTime Called upon selection of Date picker option.
 */
- (void) didSelectDate:(NSDate *)pickerOption
{
    if (_datepickerTag == kRowIssuedDate)
    {
        if (!isEmpty(pickerOption))
        {
            _issuedDate = pickerOption;
        }
    }
    else if (_datepickerTag == kRowReceivedDate)
    {
        if (!isEmpty(pickerOption))
        {
            _receivedDate = pickerOption;
        }
    }
    [self.tableView reloadData];
}

/*!
 @discussion
 Method didSelectReceivedPickerOption Called upon selection of Received on behalf of picker option.
 */
- (void) didSelectReceivedPickerOption:(NSString *)pickerOption
{
    if (!isEmpty(pickerOption))
    {
        _receivedOnBehalfOf = pickerOption;
        [self.tableView reloadData];
    }
}

/*!
 @discussion
 Method didSelectConfirmationBtn Called upon clicking confirmation button.
 */
- (void) didSelectConfirmationBtn:(BOOL)pickerOption
{
    _isCheckBoxSelected = pickerOption;
}

#pragma mark - Notification

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveUpdateCP12InfoSaveSuccessNotification) name:kUpdateCP12InfoSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveUpdateCP12InfoSaveFailureNotification) name:kUpdateCP12InfoFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateCP12InfoSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateCP12InfoFailureNotification object:nil];
}
- (void) onReceiveUpdateCP12InfoSaveSuccessNotification
{
    [self popOrCloseViewController];
}


#pragma mark - Methods

- (void) validateForm
{
    if (_isCheckBoxSelected && _receivedOnBehalfOf && _issuedDate && _receivedDate)
    {
        [self saveInformation];
    }
    
    else
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:LOC(@"KEY_ALERT_EMPTY_MANDATORY_FIELDS") delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
     
        [alert show];
    }
}

- (void) saveInformation
{
    NSMutableDictionary *cp12InfoDictionary = [NSMutableDictionary dictionary];
    
    if (self.cp12Info.objectID)
    {
        [cp12InfoDictionary setObject:self.cp12Info.objectID forKey:@"objectId"];
    }
    if (_receivedOnBehalfOf)
    {
        [cp12InfoDictionary setObject:_receivedOnBehalfOf forKey:@"ReceivedOnBehalfOf"];
    }
    if (_receivedDate)
    {
        [cp12InfoDictionary setObject:_receivedDate forKey:@"ReceivedDate"];
    }
    if (_issuedDate)
    {
        [cp12InfoDictionary setObject:_issuedDate forKey:@"IssuedDate"];
    }
    
    [cp12InfoDictionary setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:@"IssuedBy"];
    [cp12InfoDictionary setObject:[NSNumber numberWithBool:_isCheckBoxSelected] forKey:@"InspectionCarried"];
    
    [[PSDataUpdateManager sharedManager] updateCP12Info:cp12InfoDictionary forAppointment:self.appointment];
}

@end
