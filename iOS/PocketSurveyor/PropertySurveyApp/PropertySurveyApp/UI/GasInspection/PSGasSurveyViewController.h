//
//  PSGasSurveyViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
@class PSGasSurveyCell;

@interface PSGasSurveyViewController : PSCustomViewController
{
    NSString *surveyTitle;
}
- (IBAction)onClickCompleteBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) IBOutlet UILabel *lblTenantName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblCertificateExpiry;
@property (strong, nonatomic) IBOutlet UIButton *btnMarkComplete;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (weak,   nonatomic) Appointment *appointment;
@property (weak, nonatomic) IBOutlet PSGasSurveyCell *gasSurveyCell;

@property (strong, nonatomic) IBOutlet FBRImageView * imageView;

@end
