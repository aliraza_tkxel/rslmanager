//
//  PSGasSurveyCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSGasSurveyCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgStatus;

@end
