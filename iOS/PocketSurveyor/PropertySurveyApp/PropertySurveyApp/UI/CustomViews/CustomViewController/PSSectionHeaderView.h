//
//  PSSectionHeaderView.h
//  PropertySurveyApp
//
//  Created by TkXel on 05/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSectionHeaderView : UIView
{
    @private
        UILabel *_titleLabel;
}
@property(strong, nonatomic, setter = setTitle:) NSString *title;

/*!
 @discussion
 initWithTitle Inititalizes the Section Header View With Given Title.
 */
- (id)initWithTitle:(NSString *)title;
- (void) setHeaderTitle:(NSString *)title;
@end
