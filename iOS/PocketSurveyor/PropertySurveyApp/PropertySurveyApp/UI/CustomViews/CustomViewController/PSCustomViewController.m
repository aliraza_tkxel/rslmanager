//
//  PSCustomViewController.m
//  PropertySurveyApp
//
//  Created by TkXel on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import "PSSectionHeaderView.h"
#import "PSBarButtonItem.h"


@interface PSCustomViewController ()
{
	CGRect _titleViewRect;
	UIAlertView *_alertView;
	CGSize keyboadSize;
}
@property (atomic, assign) BOOL keyboardVisible;
- (CGRect) _rectForTitleViewWithButtonCount:(NSInteger)buttonCount;
@end

@implementation PSCustomViewController
@synthesize activityIndicator;
@synthesize isEditing = _isEditing;
@synthesize isDispatchReady = _isDispatchReady;
@synthesize selectedControl = _selectedControl;
@synthesize tableView = _tableView;

#pragma mark Initialization

- (id)init {
    if (self = [super init]) {
			
    }
    return (self);
}

- (void)loadView {
	
	[super loadView];
	[self.view setFrame:[[UIScreen mainScreen] bounds]];
}

#pragma mark UIViewController
-(void) viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    //   [self.view setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(unableToAccess:)
                                                 name:kCommonUnableToAccessServices
                                               object:nil];
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if(OS_VERSION < 7.0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
    else {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCommonUnableToAccessServices object:nil];
    [self setTableView:nil];
}


#pragma mark - Navigation Bar Methods
/*!
 @discussion
 Method to Set Navigation Bar Items.
 */
- (void) loadNavigationonBarItems
{
    
}

/*!
 @discussion
 Sets the Title of the UINavigationBar. This method must be called after setting the Left and Right Bar Button Items.
 */
- (void) setTitle:(NSString *)title
{
    NSInteger rightBarButtonItemsCount = [self.navigationItem.rightBarButtonItems count];
    _titleViewRect = [self _rectForTitleViewWithButtonCount:rightBarButtonItemsCount];
    self.navigationItem.titleView = [(PSCustomNavigationController *)self.navigationController titleViewWithTitle:title rect:_titleViewRect];
    //[self.navigationItem.titleView sizeToFit];
}

- (CGRect) _rectForTitleViewWithButtonCount:(NSInteger)buttonCount
{
    
    
    CGRect rect;
    CGFloat consumedWidth = 0.0;
    CGFloat xPosition = 0.0;
    
    NSArray *rightBarButtonItems = [self.navigationItem rightBarButtonItems];
    NSArray *leftBarButtonItems = [self.navigationItem leftBarButtonItems];
    
    for(PSBarButtonItem *button in rightBarButtonItems)
    {
        consumedWidth += button.buttonSize.width;
    }
    
    for(PSBarButtonItem *button in leftBarButtonItems)
    {
        consumedWidth += button.buttonSize.width;
        xPosition += button.buttonSize.width;
    }
    
    CGFloat width = CGRectGetWidth([self.view frame]) - consumedWidth - xPosition;

    rect = CGRectMake(0,0,width,40);
    return rect;
}
/*!
 @discussion
 Sets the Left Bar Button Items
 */
- (void) setLeftBarButtonItems:(NSArray *)leftBarButtonItems
{
    if(!isEmpty(leftBarButtonItems))
    {
        [self.navigationItem setLeftBarButtonItems:leftBarButtonItems animated:YES];
    }
}

/*!
 @discussion
 Sets the Right Bar Button Items
 */
- (void) setRightBarButtonItems:(NSArray *)rightBarButtonItems
{
    if(!isEmpty(rightBarButtonItems))
    {
        [self.navigationItem setRightBarButtonItems:rightBarButtonItems animated:YES];
    }
    CGRect frame = [self _rectForTitleViewWithButtonCount:[rightBarButtonItems count]];
    self.navigationItem.titleView.frame = frame;
}

/*!
 @discussion
 Disables all Navigation Bar Button Items
 */
- (void) disableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = NO;
    }
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = NO;
    }
}

/*!
 @discussion
 Enables all Navigation Bar Button Items
 */
- (void) enableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = YES;
    }
    
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = YES;
    }
}

#pragma mark - Activity Indicator
/*!
 @discussion
 Shows the UIActivityIndicator Animated.
 */
-(void)showActivityIndicator:(NSString*)title
{
    MBProgressHUD* spinnerView = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [spinnerView  setLabelText:title];
}

/*!
 @discussion
 Hides the UIActivityIndicator Animated.
 */
-(void)hideActivityIndicator
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - Alert View
/*!
 @discussion
 Shows the UIAlertView Animated.
 */
- (void) showAlertWithTitle:(NSString*)title message:(NSString *)message tag:(NSInteger)tag cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles,... NS_REQUIRES_NIL_TERMINATION
{
	if(_alertView != nil)
	{
		[_alertView dismissWithClickedButtonIndex:-1 animated:YES];
	}
	
	_alertView = [[UIAlertView alloc] initWithTitle:title
                                            message:message
                                           delegate:self
                                  cancelButtonTitle:cancelButtonTitle
                                  otherButtonTitles:otherButtonTitles, nil];
    _alertView.tag = tag;
    [_alertView show];
}

#pragma mark Alert View Delegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}




#pragma mark - Notifications

-(void)unableToAccess:(NSNotification *)notification {
    [self.activityIndicator stopAnimating];
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
}

#pragma mark QTUIViewController

- (void)popAllModalViewControllers:(NSNotification *)notification {
    
    #if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_5_0
    while (super.presentedViewController != nil) {
    #else
    while (super.modalViewController != nil) {
    #endif
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

- (UIColor *)navigationBarTint {
    return [UIColor colorWithRed:0.9882 green:0.9804 blue:0.9843 alpha:1.0];
}

- (UIColor *)toolbarTint {
    return [UIColor colorWithRed:0.9882 green:0.9804 blue:0.9843 alpha:1.0];
}

- (UIColor *)searchBarTint {
    return [UIColor colorWithRed:0.9882 green:0.9804 blue:0.9843 alpha:1.0];
}

- (BOOL)isRootViewController {
    return [super navigationController] != nil && [[super.navigationController viewControllers] objectAtIndex:0] == self;
}

- (void)popOrCloseViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)popToRootViewController {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Child Class must override.
    return nil;
}


#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Creates an array consisting of table view section header views
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
    float horizontalPadding = 0;
    float verticalPadding = 16;
    CGFloat height = 0.0;
    if(OS_VERSION >= 7.0)
    {
        CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(320, 999999.0f)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:[NSDictionary dictionaryWithObject:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17] forKey:NSFontAttributeName]
                                                   context:nil];
        height = boundingRect.size.height + verticalPadding;
    }
    else
    {
        height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
                          constrainedToSize:CGSizeMake(320, 999999.0f)
                              lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
    }
    
    return height;
}

#pragma mark - Load Section Header

- (void) loadSectionHeaderViews:(NSArray *)sectionTitles headerViews:(NSMutableArray *)headerViews
{
    if(!isEmpty(sectionTitles))
    {
        if(headerViews == nil)
        {
            headerViews = [NSMutableArray array];
        }
        for (NSString *sectionTitle in sectionTitles)
        {
            PSSectionHeaderView *headerView = [[PSSectionHeaderView alloc] initWithTitle:sectionTitle];
            [headerViews addObject:headerView];
        }
    }
}


#pragma mark - UI Keyboard Notifications
- (void) registerKeyboardNotifications
{
	if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone) {
		self.keyboardVisible = NO;
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(keyboardDidShow:)
																								 name:UIKeyboardDidShowNotification
																							 object:self.view.window];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
																						 selector:@selector(keyboardDidHide:)
																								 name:UIKeyboardDidHideNotification
																							 object:self.view.window];
	}
}

- (void) deregisterKeyboardNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
																									name:UIKeyboardDidShowNotification
																								object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self
																									name:UIKeyboardDidHideNotification
																								object:nil];
}

#pragma mark - Keyboard Methods

- (void)keyboardDidShow:(NSNotification *)notification
{
	CLS_LOG(@"");
	if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone)
	{
		if(self.keyboardVisible == YES)
		{
			CLS_LOG(@"Keyboard already exist...");
			return;
		}
		self.keyboardVisible = YES;
		NSDictionary *userInfo = [notification userInfo];
		keyboadSize = [[userInfo objectForKey: UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
		CGRect newTableViewFrame = CGRectMake(self.tableView.frame.origin.x,
																					self.tableView.frame.origin.y,
																					self.tableView.frame.size.width,
																					self.tableView.frame.size.height - keyboadSize.height - 5);
		[self.tableView setFrame:newTableViewFrame];
		CGSize contentSize = CGSizeMake(self.tableView.contentSize.width,
																		self.tableView.contentSize.height - keyboadSize.height);
		self.tableView.contentSize = contentSize;
		[self scrollToRectOfSelectedControl];
		CLS_LOG(@"(x,%f),(y,%f),(width,%f),(height,%f)",
				 self.tableView.frame.origin.x, self.tableView.frame.origin.y,
				 self.tableView.frame.size.width, self.tableView.frame.size.height);
	}
}

- (void)keyboardDidHide:(NSNotification *)notification
{
	NSDictionary *userInfo = [notification userInfo];
//	CGSize size = [[userInfo objectForKey: UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
	CGRect newTableViewFrame = CGRectMake(self.tableView.frame.origin.x,
																				self.tableView.frame.origin.y,
																				self.tableView.frame.size.width,
																				self.tableView.frame.size.height + keyboadSize.height + 5);
	[self.tableView setFrame:newTableViewFrame];
	self.keyboardVisible = NO;
	
	CLS_LOG(@"(x,%f),(y,%f),(width,%f),(height,%f)",
			 self.tableView.frame.origin.x, self.tableView.frame.origin.y,
			 self.tableView.frame.size.width, self.tableView.frame.size.height);
}

- (void) scrollToRectOfSelectedControl
{
    CLS_LOG(@"Parent Method Invoked, Child Class must override.");
}

#pragma mark - Resign All Responders
    
- (void) resignAllResponders
{
    [self.view endEditing:YES];
}
    
#pragma mark - View Controller Visible
- (BOOL)isVisible
{
    return [self isViewLoaded] && self.view.window;
}
    
#pragma mark - UI Methods
/*!
 @discussion
 Schedules the "activateUIControls" method on mail loop for 30 sec delay.
 */
- (void) schduleUIControlsActivation
{
    if([NSThread currentThread] == [NSThread mainThread])
    {
        [NSTimer scheduledTimerWithTimeInterval:60.0
                                         target:self
                                       selector:@selector(activateUIControls)
                                       userInfo:nil
                                        repeats:NO];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(schduleUIControlsActivation) withObject:nil waitUntilDone:NO];
    }
}

/*!
 @discussion
 Remove all Activity Indicators and Enables all Navigation Controls.
 */
- (void) activateUIControls
{
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
}
    
@end
