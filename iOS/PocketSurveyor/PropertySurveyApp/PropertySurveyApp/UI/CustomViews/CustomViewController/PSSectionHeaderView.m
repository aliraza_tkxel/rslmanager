//
//  PSSectionHeaderView.m
//  PropertySurveyApp
//
//  Created by TkXel on 05/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSectionHeaderView.h"

#define kViewSectionHeaderHeight 20

@implementation PSSectionHeaderView

- (id)initWithTitle:(NSString *)title
{
    CGRect frame = CGRectZero;
    if(!isEmpty(title))
    {
        frame = CGRectMake(0, 0, 320, kViewSectionHeaderHeight);
    }
    
    if(self = [self initWithFrame:frame])
    {
        [self setHeaderTitle:title];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.frame = CGRectMake(12, 3, 320, kViewSectionHeaderHeight);
        _titleLabel.backgroundColor = [UIColor clearColor];
        [_titleLabel setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        _titleLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12];
        _titleLabel.text = self.title;
        
        CGRect imageRect = [[UIScreen mainScreen] bounds];
        imageRect.size.height=1;
        UIImage *image = [UIImage imageNamed:@"line"];
        UIImageView *headerImage = [[UIImageView alloc] init];
        
        [headerImage setImage:image];
        [headerImage setFrame:imageRect];
        
        [self addSubview:headerImage];
        [self addSubview:_titleLabel];
    }
    return self;
}

- (void) setHeaderTitle:(NSString *)title
{
    self.title = [NSString stringWithString:title];
    _titleLabel.text = self.title;
    [self layoutSubviews];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
