//
//  PSButton.m
//  PropertySurveyApp
//
//  Created by TkXel on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSButton.h"

@interface PSButton()
{
    PSBarButtonItemSeperatorPosition _seperatorPosition;
    BOOL _addSeperator;
    PSBarButtonItemStyle _buttonStyle;
}
@end

@implementation PSButton
@synthesize buttonSize = _buttonSize;

- (id) init
{
    return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        // Initialization code
    }
    return self;
}

- (PSButton *) initWithStyle:(PSBarButtonItemStyle)style
{
    _buttonStyle = style;
    _addSeperator = YES;
    CGFloat xSpace = 7.0;
    _seperatorPosition = PSBarButtonItemSeperatorPositionLeft;
    if(self = [self initWithFrame:CGRectMake(0, 0, 35, 35)])
    {
        switch (style) {
            case PSBarButtonItemStyleDefault:
            {
                _addSeperator = NO;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                CGRect frame = CGRectMake(0, 0, 52, 32);
                self.frame = frame;
                [self setBackgroundImage:[UIImage imageNamed:@"btn_Default"] forState:UIControlStateNormal];
                [self setBackgroundImage:[UIImage imageNamed:@"btn_Default_Active"] forState:UIControlStateHighlighted];
            }
                break;
            case PSBarButtonItemStyleBack:
            {
                _addSeperator = NO;
                _seperatorPosition = PSBarButtonItemSeperatorPositionLeft;
                CGRect frame = CGRectMake(0, 0, 12, 21);
                self.frame = frame;
                [self setImage:[UIImage imageNamed:@"backArrow"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"backArrow"] forState:UIControlStateHighlighted];
            }
                break;
                
            case PSBarButtonItemStyleSearch:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Search"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Search_Active"] forState:UIControlStateHighlighted];
            }
                break;
                
            case PSBarButtonItemStyleAdd:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Add"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Add_Active"] forState:UIControlStateHighlighted];
            }
                break;
                
            case PSBarButtonItemStyleTrash:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Trash"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Trash_Active"] forState:UIControlStateHighlighted];
            }
                break;
                
            case PSBarButtonItemStyleNotes:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Notes"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Notes_Active"] forState:UIControlStateHighlighted];
            }
                break;
                
            case PSBarButtonItemStyleCamera:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_BarCamera"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_BarCamera_Active"] forState:UIControlStateHighlighted];
            }
                break;
                
            case PSBarButtonItemStyleHome:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Home"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Home_Active"] forState:UIControlStateHighlighted];
            }
                break;
            case PSBarButtonItemStylePlay:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Play"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Play_Active"] forState:UIControlStateHighlighted];
            }
                break;
            case PSBarButtonItemStylePause:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Pause"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Pause_Active"] forState:UIControlStateHighlighted];
            }
                break;
            case PSBarButtonItemStyleCheckmark:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Checkmark"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Checkmark_Active"] forState:UIControlStateHighlighted];
            }
                break;
            case  PSBarButtonItemStyleEdit:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Edit"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Edit_Active"] forState:UIControlStateHighlighted];
            }
                break;
            case PSBarButtonItemStyleDownload:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionRight;
                [self setImage:[UIImage imageNamed:@"btn_Download"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Download_Active"] forState:UIControlStateHighlighted];
            }
                break;
            case PSBarButtonItemStyleLogout:
            {
                _addSeperator = YES;
                _seperatorPosition = PSBarButtonItemSeperatorPositionLeft;
                [self setImage:[UIImage imageNamed:@"btn_Logout"] forState:UIControlStateNormal];
                [self setImage:[UIImage imageNamed:@"btn_Logout_Active"] forState:UIControlStateHighlighted];
            }
                break;
            default:
                break;
        }
    }
    
    //Add Seperator Layer
    CGSize seperatorSize = CGSizeZero;
    if(_addSeperator)
    {
        UIImage *imgSeperator = [UIImage imageNamed:@"separator"];
        CALayer *seperatorLayer = [CALayer layer];
        seperatorLayer.contents = (id)imgSeperator.CGImage;
        seperatorSize = imgSeperator.size;

        if(_seperatorPosition == PSBarButtonItemSeperatorPositionLeft)
        {
            
            seperatorLayer.frame = CGRectMake(self.frame.origin.x + self.frame.size.width + 5, -1, seperatorSize.width, seperatorSize.height);
            seperatorLayer.position = CGPointMake(seperatorLayer.position.x, self.center.y);
            [self.layer addSublayer:seperatorLayer];
        }
        else if(_seperatorPosition == PSBarButtonItemSeperatorPositionRight)
        {
            seperatorLayer.frame = CGRectMake(-5, -1, seperatorSize.width, seperatorSize.height);
            seperatorLayer.position = CGPointMake(seperatorLayer.position.x, self.center.y);
            [self.layer addSublayer:seperatorLayer];
        }
    }
    
    //Set Button Size
    self.buttonSize = self.bounds.size;
    self.buttonSize = CGSizeMake(self.buttonSize.width + 5 + seperatorSize.width + xSpace
                                 , self.buttonSize.height);
    return self;
}

- (void) setTitle:(NSString *)title
{
    if(!isEmpty(title))
    {
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitle:title forState:UIControlStateHighlighted];
        UIFont *titleFont = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:16];
        [self.titleLabel setFont:titleFont];
        [self setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT) forState:UIControlStateNormal];
        [self setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT) forState:UIControlStateHighlighted];
        
        
        if(_buttonStyle == PSBarButtonItemStyleDefault)
        {
            CGSize size = CGSizeZero;
            if(OS_VERSION >= 7.0)
            {
                size = [title sizeWithAttributes:[NSDictionary dictionaryWithObject:titleFont forKey:NSFontAttributeName]];
            }
            else
            {
                size = [title sizeWithFont:titleFont];
            }
            if(size.width < 50)
            {
                size = CGSizeMake(52, self.buttonSize.height);
            }
            else
            {
                size = CGSizeMake(size.width + 7, self.buttonSize.height);
            }
            self.buttonSize = CGSizeMake(size.width, size.height);
            self.frame = CGRectMake(self.frame.origin.x,
                                    self.frame.origin.y,
                                    self.buttonSize.width,
                                    self.frame.size.height);
        }
    }
}



//- (UIEdgeInsets)alignmentRectInsets
//{
//    UIEdgeInsets insets;
//    if(OS_VERSION >= 7.0)
//    {
//        if ([self isLeftButton]) {
//            if(_buttonStyle == PSBarButtonItemStyleBack)
//            {
//                insets = UIEdgeInsetsZero;
//            }
//            else
//            {
//                insets = UIEdgeInsetsMake(0, 9.0f, 0, 0);
//            }
//            
//            
//        } else {
//            insets = UIEdgeInsetsMake(0, 0, 0, 5.0f);
//            
//        }
//        
//        if(_addSeperator)
//        {
//            
//        }
//    }
//    else {
//        insets = UIEdgeInsetsZero;
//    }
//    return insets;
//}
//
//- (BOOL)isLeftButton {
//    return self.frame.origin.x < (self.superview.frame.size.width / 2);
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end