//
//  PSCustomNavigationController.m
//  PropertySurveyApp
//
//  Created by TkXel on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCustomNavigationController.h"

@interface PSCustomNavigationController ()

@end



@implementation PSCustomNavigationController

- (id)initWithRootViewController:(UIViewController *)rootViewController {
    
    if (self = [super initWithRootViewController:rootViewController]) {
        
        if(OS_VERSION >= 7.0)
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }
        else
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
        }

        [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
        [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
        [self.navigationBar setTranslucent:NO];
        [[self navigationBar] setBackgroundImage:[UIImage imageNamed:@"bg_navbar"] forBarMetrics:UIBarMetricsDefault];
    }
    
    return (self);
    
}

#pragma mark UINavigationController

- (void)popToRootViewController:(NSNotification *)notification {
    [self popToRootViewControllerAnimated:NO];
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [super pushViewController:viewController animated:animated];
}
- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    UIViewController *popped = [super popViewControllerAnimated:animated];
    
    return (popped);
}
- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated {
    NSArray *popped = [super popToViewController:viewController animated:animated];
    return (popped);
}
- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated {
    NSArray *popped = [super popToRootViewControllerAnimated:animated];
    return (popped);
}


- (UILabel *) titleViewWithTitle:(NSString *)title
{
    return [self titleViewWithTitle:title rect:CGRectMake(20,0,180,40)];
}

- (UILabel *) titleViewWithTitle:(NSString *)title rect:(CGRect)rect
{
    UILabel* titleView = [[UILabel alloc] initWithFrame:rect];
    [titleView setBackgroundColor:[UIColor clearColor]];
    [titleView setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:20]];
    [titleView setTextColor:UIColorFromHex(NAVIGATION_BAR_TITLE_COLOR)];
    //[titleView setFont:[UIFont boldSystemFontOfSize:16]];
    titleView.textAlignment = PSTextAlignmentLeft;
    titleView.text = title;
    return titleView;
}

@end
