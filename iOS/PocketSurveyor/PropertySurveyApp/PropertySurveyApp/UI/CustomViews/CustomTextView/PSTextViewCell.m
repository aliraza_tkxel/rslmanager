//
//  PSTextViewCell.m
//  PropertySurveyApp
//
//  Created by TkXel on 11/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSTextViewCell.h"

@implementation PSTextViewCell

@synthesize textView = _textView;

- (void)setTextView:(PSTextView *)newTextView
{
    if(self.parentTableView) {
        [self registerForNotifications];
    }
    
	_textView = newTextView;
    _textView.scrollEnabled = NO;
    [_textView setAutocorrectionType:UITextAutocorrectionTypeNo];
	[self.contentView addSubview:newTextView];
    [self.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	[self layoutSubviews];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets;
        contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height-CGRectGetMinY(self.parentTableView.frame), 0);
        self.parentTableView.contentInset = contentInsets;
}
-(void)keyboardWillHide:(NSNotification *)notification
{
    self.parentTableView.contentInset = UIEdgeInsetsZero;
    self.parentTableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    [self deRegisterForNotifications];
}
-(void)registerForNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)deRegisterForNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	CGRect contentRect = [self.contentView bounds];
	self.textView.frame  = CGRectMake(contentRect.origin.x + 5.0,
									  contentRect.origin.y + 10.0,
									  contentRect.size.width - (5.0*2),
									  contentRect.size.height - (5.0*2));
    self.textView.backgroundColor = CLEAR_COLOR;
    self.textView.textAlignment = PSTextAlignmentLeft;
    self.textView.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.textView.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
    
}

-(void)dealloc
{
    [self deRegisterForNotifications];
}
@end
