//
//  PSPropertyListViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 28/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSPropertyListViewController.h"
#import "PSPropertyViewCell.h"
#import "PSLoadingCell.h"
#import "Property+CoreDataClass.h"


#define kNumberOfSections 2
#define kSectionProperties 0
#define kSectionPageLoading 1

@interface PSPropertyListViewController ()
{
	BOOL _noResultsFound;
	BOOL _firstTime;
	int _pageNumber;
	NSUInteger _totalPages;
	BOOL _isPageLoading;
	BOOL _isSearching;
	BOOL _internetUnavailable;
	PSPropertyManager * _manager;
}
@end

@implementation PSPropertyListViewController
@synthesize searchString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	_pageNumber = 1;
	_isSearching = NO;
	_noResultsFound = NO;
	_firstTime = YES;
	_isPageLoading = NO;
	_internetUnavailable = NO;
	self.searchString = @"";
	
	[self.btnBack setHidden:NO];
	[self.btnCancel setHidden:YES];
	[self.tableView registerNib:[UINib nibWithNibName:@"PSLoadingCell" bundle:nil] forCellReuseIdentifier:@"loadingCellIdentifier"];
	
	[self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	[self.view setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	
	if(OS_VERSION < 7.0)
	{
		CGRect frame = self.searchBarView.frame;
		self.searchBarView.frame = CGRectMake(frame.origin.x,
																					frame.origin.y,
																					frame.size.width,
																					44);
		self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
																			self.searchBarView.frame.origin.y + self.searchBarView.frame.size.height,
																			self.tableView.frame.size.width,
																			self.tableView.frame.size.height);
	}
	else
	{
		//Change the Section Index Backgorund Color
		self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
		self.tableView.sectionIndexTrackingBackgroundColor = [UIColor whiteColor];
	}
	
	_manager = [PSPropertyManager sharedManager];
	
	[_manager setDelegate:self];
	self.txtSearch.delegate = self;
	[self performFetchOnNSFetchedResultsController];
	[self fetchAllProperties];
	[self schduleUIControlsActivation];
	
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self registerNotifications];
	self.navigationController.navigationBar.hidden = YES;
	[self.navigationItem setHidesBackButton:YES animated:NO];
	
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self deRegisterNotifications];
	self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	[self setTxtSearch:nil];
	
}

#pragma mark - UITableView Delegate Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return kNumberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	int numberOfRows = 0;
	if (section == kSectionProperties)
	{
		if (_noResultsFound && !_isPageLoading) {
			numberOfRows = 1;
		}
		else
		{
			id <NSFetchedResultsSectionInfo> sectionInfo;
			sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
			numberOfRows = (int)[sectionInfo numberOfObjects];
		}
	}
	else if (section == kSectionPageLoading)
	{
		numberOfRows = (int)[self pageLoadingRow];
	}
	return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	CGFloat rowHeight = 80;
	if (indexPath.section == kSectionPageLoading)
	{
		rowHeight = 44;
	}
	return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionProperties)
	{
		static NSString *propertyViewCellIdentifier = @"PropertyViewCellIdentifier";
		PSPropertyViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:propertyViewCellIdentifier];
		
		if(cell == nil)
		{
			[[NSBundle mainBundle] loadNibNamed:@"PSPropertyViewCell" owner:self options:nil];
			cell = self.propertyViewCell;
			self.propertyViewCell = nil;
		}
		
		[self configureCell:cell atIndexPath:indexPath];
		return cell;
	}
	else if (indexPath.section == kSectionPageLoading)
	{
		PSLoadingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCellIdentifier"];
		[cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
		[cell.lblTitle setText:LOC(@"KEY_STRING_PROPERTIES_DOWNLOADING")];
		return cell;
	}
	return nil;
}

- (void)configureCell:(PSPropertyViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	_firstTime = NO;
	if (_noResultsFound) {
		cell.lblName.text = LOC(@"KEY_STRING_NO_RESULTS_FOUND");
		//  cell.lblName.textAlignment = NSTextAlignmentCenter;
		cell.lblName.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
		cell.lblAddress.hidden = YES;
		cell.lblExpiryDate.hidden = YES;
		cell.lblExpiryTitle.hidden = YES;
		cell.imgProperty.hidden = YES;
		cell.imgSeperator.hidden = YES;
		
	}
	else
	{
		SProperty *property;
		property = [self.fetchedResultsController objectAtIndexPath:indexPath];
		
		[cell reloadData:property];
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionProperties)
	{
		/*!
		 @discussion
		 Check for last row. if last row is reached, send request for more results.
		 */
		
		if (_isSearching) {
			return;
		}
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == kSectionProperties)
	{
		if (!_noResultsFound) {
			
			SProperty *property = [self.fetchedResultsController objectAtIndexPath:indexPath];
			if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectSProperty:)])
			{
				[(PSNewAppointmentViewController *)self.delegate didSelectSProperty:property];
			}
			
			[self popOrCloseViewController];
		}
	}
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	self.selectedControl = (UITextField *)textField;
	_isSearching = YES;
	
	[self.btnBack setHidden:YES];
	[self.btnCancel setHidden:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	_isSearching = NO;
	[self.btnBack setHidden:NO];
	[self.btnCancel setHidden:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self startSearchForTextField:textField];
	[textField resignFirstResponder];
	return YES;
}

-(void) startSearchForTextField:(UITextField*)textField{
    NSString *string = textField.text;
    _pageNumber = 1;
    self.searchString = string;
    [self filterPropertiesWithString:self.searchString];
}
    

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	return YES;
}


#pragma mark - Fetched results controller
- (void) performFetchOnNSFetchedResultsController
{
	CLS_LOG(@"performFetchOnNSFetchedResultsController");
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		CLS_LOG(@"fetchedResultsController, Unresolved error %@, %@", error, [error userInfo]);
	}
	if (!_firstTime) {
		[self checkResultsEmptiness];
	}
	[self.tableView reloadData];
}

- (void) checkResultsEmptiness
{
	if([[self.fetchedResultsController fetchedObjects] count] <= 0)
	{
		_noResultsFound = YES;
	}
	else
	{
		_noResultsFound = NO;
	}
	//#warning PSA: FIX NO RESULTS YES BUG HERE (ON RESET SIMULATOR)
}

- (NSFetchedResultsController *)fetchedResultsController
{
	if (_fetchedResultsController != nil) {
		if (!isEmpty(self.searchString))
		{
            NSPredicate *completeNamePredicate = [NSPredicate predicateWithFormat:@"SELF.%K CONTAINS[cd] %@", kCustomerCompleteName, self.searchString];
            NSPredicate *completeAddressPredicate = [NSPredicate predicateWithFormat:@"SELF.%K CONTAINS[cd] %@", kCompleteAddress, self.searchString];
			NSPredicate *searchTemplate = [NSCompoundPredicate orPredicateWithSubpredicates:@[completeNamePredicate, completeAddressPredicate]];
			/* use the compound predicate to combine them */
            self.propertyFilterPredicate = searchTemplate;
			[self.fetchRequest setPredicate:self.propertyFilterPredicate];
		}
		else
		{
			self.propertyFilterPredicate = nil;
			[self.fetchRequest setPredicate:self.propertyFilterPredicate];
		}
		return _fetchedResultsController;
	}
	
	self.fetchRequest = [[NSFetchRequest alloc] init];
	// Edit the entity name as appropriate.
	NSEntityDescription *entity = [NSEntityDescription entityForName:kSProperty inManagedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext];
	[self.fetchRequest setEntity:entity];
	self.fetchRequest.returnsDistinctResults = YES;
	// Set the batch size to a suitable number.
	[self.fetchRequest setFetchBatchSize:20];
	
	// Edit the sort key as appropriate.
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kAddress1 ascending:YES];
	NSArray *sortDescriptors = @[sortDescriptor];
	
	[self.fetchRequest setSortDescriptors:sortDescriptors];
	[self.fetchRequest setReturnsDistinctResults:YES];
	
	
	
	// Edit the section name key path and cache name if appropriate.
	// nil for section name key path means "no sections".
	
	NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:self.fetchRequest managedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext sectionNameKeyPath:nil cacheName:nil];
	
	aFetchedResultsController.delegate = self;
	self.fetchedResultsController = aFetchedResultsController;
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
		CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
	}
	if (!_firstTime)
	{
		[self checkResultsEmptiness];
	}
	return _fetchedResultsController;
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	if(_isSearching)
	{
		return;
	}
	[self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
					 atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	if(_isSearching)
	{
		return;
	}
	switch(type)
	{
		case NSFetchedResultsChangeInsert:
			[self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		default:
			break;
	}
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
			 atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
			newIndexPath:(NSIndexPath *)newIndexPath
{
	if(_isSearching)
	{
		return;
	}
	
	UITableView *tableView = self.tableView;
	
	switch(type) {
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[self tableView:tableView cellForRowAtIndexPath:indexPath];
			break;
			
		case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
	if(_isSearching)
	{
		return;
	}
	[self.tableView endUpdates];
}

#pragma mark - Notifications

- (void) registerNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceivePropertySaveSuccessNotification) name:kPropertySaveSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceivePropertySaveFailureNotification) name:kPropertySaveFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveConnectivityChangeNotification) name:NOTIF_NO_CONNECTIVITY object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveNoConnectivityNotification) name:kInternetUnavailable object:nil];
}

- (void) deRegisterNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kPropertySaveSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kPropertySaveFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIF_NO_CONNECTIVITY object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kInternetUnavailable object:nil];
}
- (void) onReceivePropertySaveSuccessNotification
{
	[self hideActivityIndicator];
    _txtSearch.enabled = YES;
	_isPageLoading = NO;
	if(_isSearching)
	{
		_isSearching = NO;
	}
    [self performFetchOnNSFetchedResultsController];
	//[self.tableView reloadData];
}

- (void) onReceivePropertySaveFailureNotification
{
	[self hideActivityIndicator];
    _txtSearch.enabled = YES;
	_isPageLoading = NO;
	[self.tableView reloadData];
}

- (void) onReceiveNoConnectivityNotification
{
	
	[self hideActivityIndicator];
    _txtSearch.enabled = YES;
	_isPageLoading = NO;
	[self checkResultsEmptiness];
	[self.tableView reloadData];
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.activityIndicator stopAnimating];
		if (_noResultsFound)
		{
			UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																										 message:LOC(@"KEY_ALERT_NO_INTERNET")
																										delegate:self
																					 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																					 otherButtonTitles:nil,nil];
			alert.tag = AlertViewTagNoInternet;
			alert.delegate = self;
			[alert show];
		}
	});
	
}

- (void) onReceiveConnectivityChangeNotification
{
	[self hideActivityIndicator];
    _txtSearch.enabled = YES;
	_isPageLoading = NO;
	[self checkResultsEmptiness];
	[self.tableView reloadData];
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.activityIndicator stopAnimating];
	});
}
#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == AlertViewTagNoInternet)
	{
		if (_noResultsFound)
		{
			//[self popOrCloseViewController];
		}
	}
}

#pragma mark - IBActions

- (IBAction)onClickCancelSearch:(id)sender
{
    CLS_LOG(@"onClickCancelSearch");
	[(UITextField *)self.selectedControl setText:@""];
	self.searchString = @"";
	_isPageLoading = NO;
    _txtSearch.enabled = YES;
	[NSObject cancelPreviousPerformRequestsWithTarget:self
																					 selector:@selector(filterPropertiesWithString:)
																						 object:nil];
	[self resignAllResponders];
	[self performFetchOnNSFetchedResultsController];
}

- (IBAction)onClickSearch:(id)sender
{
	CLS_LOG(@"onClickSearch");
}

- (IBAction)resignFirstResponder:(id)sender{
	[self resignAllResponders];
}

- (IBAction)onClickClearSearch:(id)sender
{
	CLS_LOG(@"onClickClearSearchs");
	NSString *searchFieldText;
	if (!isEmpty(((UITextField *)self.selectedControl).text))
	{
		searchFieldText = [((UITextField *)self.selectedControl).text substringToIndex:((UITextField *)self.selectedControl).text.length -1];
	}
	if (!isEmpty(searchFieldText))
	{
		self.searchString = searchFieldText;
		[(UITextField *)self.selectedControl setText:searchFieldText];
	}
	else
	{
		self.searchString = @"";
		[(UITextField *)self.selectedControl setText:@""];
	}
	[self performFetchOnNSFetchedResultsController];
}

- (IBAction)onClickBackButton:(id)sender
{
	CLS_LOG(@"onClickBackButton");
	[self popOrCloseViewController];
}

#pragma mark - Protocol Methods

/*!
 @discussion
 Method didReceivePageInformation Called upon when page information is received in server response.
 */
- (void) didReceivePageInformation:(NSDictionary *)pageInfo
{
	_totalPages = [[pageInfo valueForKey:@"TotalPages"]integerValue];
	if(_totalPages < 1)
	{
		_totalPages = 1;
	}
}


#pragma mark -  Paging & Filter and Dispatch Property Request
- (NSInteger) pageLoadingRow
{
	NSInteger row = 0;
	if(_isPageLoading)
	{
		row = 1;
	}
	return row;
}

- (void) fetchAllProperties
{
	[_manager removeAllProperties];
	
	if([[self.fetchedResultsController fetchedObjects] count] <= 0)
	{
		[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																 LOC(@"KEY_ALERT_LOADING"),
																 LOC(@"KEY_STRING_PROPERTIES")]];
	}
	
	self.parameterDictionary = [NSMutableDictionary dictionary];
	//default parameter values
	[self.parameterDictionary setValue:@"" forKey:@"searchstring"];
	[self.parameterDictionary setObject:[NSNumber numberWithInteger:20] forKey:@"pagecount"];
	[self.parameterDictionary setObject:[NSNumber numberWithInteger:_pageNumber] forKey:@"pagenumber"];
	
	[_manager fetchAllProperties:self.parameterDictionary];
}

- (void) filterPropertiesWithString:(NSString *)searchText
{
	[self sendRequestForMoreProperties];
	[self performFetchOnNSFetchedResultsController];
}

- (void) sendRequestForMoreProperties
{
	if([[ReachabilityManager sharedManager] isConnectedToInternet])
	{
		if (!_isPageLoading)
		{
			if (_pageNumber <= _totalPages)
			{
				_isPageLoading = YES;
                _txtSearch.enabled = NO;
				[self.parameterDictionary setValue:self.searchString forKey:@"searchstring"];
				[self.parameterDictionary setObject:[NSNumber numberWithInteger:20] forKey:@"pagecount"];
				[self.parameterDictionary setObject:[NSNumber numberWithInteger:_pageNumber] forKey:@"pagenumber"];
				[[PSPropertyManager sharedManager] fetchAllProperties:self.parameterDictionary];
				++_pageNumber;
				//[self.tableView reloadData];
			}
		}
		else
		{
			CLS_LOG(@"Property Request Already in Process!");
		}
	}
}

@end
