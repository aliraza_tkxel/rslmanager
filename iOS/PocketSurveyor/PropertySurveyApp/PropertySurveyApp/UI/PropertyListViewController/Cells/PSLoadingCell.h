//
//  PSLoadingCell.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 14/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSLoadingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
