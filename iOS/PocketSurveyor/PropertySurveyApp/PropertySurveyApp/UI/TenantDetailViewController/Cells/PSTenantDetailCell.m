//
//  PSTenantDetailCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSTenantDetailCell.h"
#import "PSPropertyListViewController.h"

@implementation PSTenantDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)resignFirstResponder:(id)sender{
    UITextField *textField = (UITextField *)sender;
    
    [textField resignFirstResponder];
}

@end
