//
//  PSMapsViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 29/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import <MapKit/MapKit.h>

#define METERS_PER_MILE 1609.334
@interface PSMapsViewController : PSCustomViewController <MKMapViewDelegate,MKAnnotation>
{
    MKMapView *addressMap;
    
    NSString *address;
    NSString *lat;
    NSString *lng;
}

@property (nonatomic, retain) IBOutlet MKMapView *addressMap;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *navigationTitle;

-(void)setMapView;
@end
