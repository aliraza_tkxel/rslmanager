//
//  PSAnnotation.m
//  PropertySurveyApp
//
//  Created by Yawar on 29/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSAnnotation.h"
#import <MapKit/MapKit.h>

@implementation PSAnnotation
@synthesize navigationTitle;
@synthesize subtitle;
@synthesize coordinate;
@end
