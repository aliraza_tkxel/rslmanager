//
//  PSAppointmentsViewController.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppointmentsViewController.h"
#import "PSAppointmentsFilterViewController.h"
#import "PSAppointmentDetailViewController.h"
#import "PSBarButtonItem.h"
#import "NSFetchedResultsControllerHelper.h"
#import "PSNewAppointmentViewController.h"
#import "PSEventsManager.h"
#import "Appointment+SurveyDownload.h"
#import "Appointment+JSON.h"
#import "PSAppointmentCell.h"
#import "PropertyPicture+JSON.h"
#import "RepairPictures+JSON.h"
#import "Appliance+JSON.h"
#import "Defect+JSON.h"
#import "Scheme+JSON.h"


#define kAppointmentViewSectionHeaderHeight 20
#define kFaultAppointmentCellDefaultHeight 130
#define kFaultSchemeAppointmentCellDefaultHeight 183
#define kPlannedAppointmentCellDefaultHeight 143
#define kAdaptationAppointmentCellDefaultHeight 155


@interface PSAppointmentsViewController ()
{
	EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloadingTable;
	BOOL _noResultsFound;
	BOOL _isAppointmentListEmpty;
	NSMutableArray * _completedAppointments;
	NSMutableArray * _sendingAppointments;
	NSInteger _todaySection;
	//image resending mechanism helper variables
	NSMutableArray * imagesToUpload;
	BOOL isAnyImageFail;
	int totalImages;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (UIImage *)imageForAppointmentType:(AppointmentType)type;
- (UIImage *)imageForAppointmentCompletionStatus:(AppointmentStatus)status;
@end

@implementation PSAppointmentsViewController

//NSLock * refresh_mutex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	//	refresh_mutex = [[NSLock alloc]init];
	
	imagesToUpload = [[NSMutableArray alloc] init];
	[[PSEventsManager sharedManager] requestCalendarPermissions];
	_todaySection = -1;
	
	[self.tblAppointments setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	
	
	[self loadNavigationonBarItems];
	if ([[SettingsClass sharedObject].loggedInUser.userToAppointments count] <= 0)
	{
		[self disableNavigationBarButtons];
	}
	[self addAppointmentsFilterView];
	[self addPullToRefreshControl];
	
	[self todaySection];
	if (_todaySection)
	{
		[self performSelector:@selector(scrollToTodaySection) withObject:nil afterDelay:1.0];
	}
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	_allowRelaodingTableView_ = YES;
	[[PSAppDelegate delegate] setUserLoggedInToApplication:YES];
	[self registerNotifications];
	if ([[SettingsClass sharedObject].loggedInUser.userToAppointments count] <= 0 && _fetchedResultsController == nil)
	{
		_isAppointmentListEmpty = YES;
		[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																 LOC(@"KEY_ALERT_LOADING"),
																 LOC(@"KEY_STRING_APPOINTMENTS")]];
	}
	else
	{
		_isAppointmentListEmpty = NO;
	}
	
	if (_fetchedResultsController == nil)
	{
		if( !_isAppointmentListEmpty)
		{
			[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
																	 LOC(@"KEY_ALERT_LOADING"),
																	 LOC(@"KEY_STRING_APPOINTMENTS")]];
		}
		[[PSAppointmentsManager sharedManager] fetchAllAppointsments];
		[self performFetchOnNSFetchedResultsController];
		
	}
	else
	{
		//Force full repopulation
		self.fetchedResultsController = nil;
		[self performFetchOnNSFetchedResultsController];
	}
	[self setTitle:[[SettingsClass sharedObject] userFullName]];
	[NSFetchedResultsControllerHelper filterAppointmentsWithOptions:[SettingsClass sharedObject].filterOptions
																											 controller:self.fetchedResultsController];
	[self.tblAppointments reloadData];
}

-(void)loadInprogressOldAppointmentsFromDate:(NSDate*)from toDate:(NSDate*)to
{
	[self hideAppointmentsFilterView];
	[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
															 LOC(@"KEY_ALERT_LOADING"),
															 LOC(@"KEY_STRING_APPOINTMENTS")]];
	[[PSAppointmentsManager sharedManager] fetchAllInprogressAppointsmentsFromDate:from toDate:to];
	
}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self deRegisterNotifications];
	_noResultsFound = NO;
	self.fetchedResultsController.delegate=nil;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.b
	[self setTblAppointments:nil];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
	PSBarButtonItem *logoutButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																				 style:PSBarButtonItemStyleLogout
																																				target:self
																																				action:@selector(onClickLogout)];
	
	PSBarButtonItem *addButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			style:PSBarButtonItemStyleAdd
																																		 target:self
																																		 action:@selector(onClickAddButton)];
	
	PSBarButtonItem *searchButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																				 style:PSBarButtonItemStyleSearch
																																				target:self
																																				action:@selector(onClickSearchButton)];
	
	[self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:logoutButton, nil] animated:YES];
	[self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:searchButton, addButton, nil] animated:YES];
	
	[self setTitle:[[SettingsClass sharedObject] userFullName]];
}

- (void) onClickLogout
{
	CLS_LOG(@"onClickLogout");
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_LOGOUT")
																								 message:LOC(@"KEY_ALERT_CONFIRM_LOGOUT")
																								delegate:self
																			 cancelButtonTitle:LOC(@"KEY_ALERT_YES")
																			 otherButtonTitles:LOC(@"KEY_ALERT_NO"),nil];
	alert.tag = AlertViewTagLogout;
	[alert show];
}

- (void)onClickAddButton {
	
	if([[ReachabilityManager sharedManager] isConnectedToInternet])
	{
		PSNewAppointmentViewController *addAppointment = [[PSNewAppointmentViewController alloc] initWithNibName:@"PSNewAppointmentViewController" bundle:[NSBundle mainBundle]];
		[self.navigationController pushViewController:addAppointment animated:YES];
	}
	else
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																									 message:LOC(@"KEY_ALERT_NO_INTERNET")
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		
		[alert show];
	}
	CLS_LOG(@"onClickAddButton");
}

- (void)onClickSearchButton
{
	CLS_LOG(@"onClickSearchButton");
	if (!isSearching)
	{
		[self showAppointmentsFilterView];
	}
}

#pragma mark UIAlertViewDelegate Method

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (alertView.tag == AlertViewTagLogout)
	{
		if(buttonIndex == alertView.cancelButtonIndex)
		{
			_fetchedResultsController = nil;
			[[SettingsClass sharedObject] signOut];
		}
	}
	else if (alertView.tag == AlertViewTagPostAppointmentData)
	{ // Post Appointments to server
		if(buttonIndex == alertView.cancelButtonIndex)
		{ // Pressed 'Post Data'
			//Checking here if an image is in posting
			if([PSPropertyPictureManager sharedManager].isPostingImage == FALSE)
			{
				[self postAppointmentsToServer];
			}
			else
			{
				UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																											 message:LOC(@"KEY_ALERT_DATA_POSTING_IMAGE_INPROGRESS")
																											delegate:nil
																						 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																						 otherButtonTitles:nil,nil];
				[alert show];
			}
		}
		else
		{ // Pressed 'Cancel'
			_completedAppointments = nil;
			[self doneLoadingTableViewData];
		}
	}
	else if (alertView.tag == AlertViewTagNoInternet)
	{
		_completedAppointments = nil;
		[self doneLoadingTableViewData];
	}
	else if (alertView.tag == AlertViewTagRefreshAppointments)
	{ // Reload Appointments
		if (buttonIndex == alertView.firstOtherButtonIndex)
		{ // Pressed YES
			[self reloadTableViewDataSource];
		}
		else
		{ // Pressed NO
			[MBProgressHUD hideHUDForView:self.view animated:YES];
			[self doneLoadingTableViewData];
		}
	}
	else if (alertView.tag == AlertViewTagException)
	{
		[self doneLoadingTableViewData];
	}
	else if(alertView.tag == AlertViewTagFailedToSyncFewAppointmentsOnServer)
	{
		if (buttonIndex == alertView.firstOtherButtonIndex)
		{
			// Handle Failed Appointments to View
		}
		else
		{
			[self doneLoadingTableViewData];
		}
	}
    else if(alertView.tag == AlertViewTagFailedToSyncImages || alertView.tag == AlertViewTagFailedToGenerateDocument || alertView.tag == AlertViewTagFailedtoSyncData)
    { // Post Appointments to server
        if(buttonIndex == 0)
        {
            // Pressed 'Retry'
            if([PSPropertyPictureManager sharedManager].isPostingImage == FALSE)
            {
                _completedAppointments = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] completedAppointments]];
                [self postAppointmentsToServer];
            }
            else
            {
                UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                               message:LOC(@"KEY_ALERT_DATA_POSTING_IMAGE_INPROGRESS")
                                                              delegate:nil
                                                     cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                     otherButtonTitles:nil,nil];
                [alert show];
            }
        }
        else if(buttonIndex == 1)
        {
            // Pressed 'Close'
            _completedAppointments = nil;
            _completedAppointments = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] completedAppointments]];
            [self doneLoadingTableViewData];
        }
    }

}

#pragma mark - Notifications

-(void) registerNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAppointmentsSaveSuccessNotification) name:kAppointmentsSaveSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAppointmentsSaveFailureNotification) name:kAppointmentsSaveFailureNotification object:nil];
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdateAppointmentsSuccessNotificationReceive:) name:kUpdateAppointmentsSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUpdateAppointmentsFailureNotificationReceive) name:kUpdateAppointmentsFailureNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSuccessStartSendingImagesNotificationReceive:) name:kAppointmentModificationStatusChangeSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFailureAppointmentDataNotificationReceive:) name:kAppointmentModificationStatusChangeFailureNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAppointmentsNoResultFoundNotification) name:kAppointmentsNoResultFoundNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRefreshAppointmentExceptionNotificationReceive) name:kRefreshAppointmentsExceptionReceiveNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveNoConnectivityNotification) name:kInternetUnavailable object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveRequestTimeoutNotification) name:kInternetRequestTimeOut object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveFailureForDocumentInspection) name:kAppointmentInspectionDocumentFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSuccessForDocumentInspection) name:kAppointmentInspectionDocumentSuccessNotification object:nil];
	
}

-(void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentInspectionDocumentFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentInspectionDocumentSuccessNotification object:nil];
    
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsSaveSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsSaveFailureNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateAppointmentsSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kUpdateAppointmentsFailureNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsNoResultFoundNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentModificationStatusChangeSuccessNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentModificationStatusChangeFailureNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kRefreshAppointmentsExceptionReceiveNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kInternetUnavailable object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kInternetRequestTimeOut object:nil];
	
}

- (void)onReceiveAppointmentsSaveSuccessNotification
{
	[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(methodToExecuteonReceiveAppointmentsSaveSuccessNotification) object:nil];
	[self performSelector:@selector(methodToExecuteonReceiveAppointmentsSaveSuccessNotification) withObject:nil afterDelay:1.0];
}

-(void)methodToExecuteonReceiveAppointmentsSaveSuccessNotification
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[self.activityIndicator stopAnimating];
		[self hideActivityIndicator];
		[self enableNavigationBarButtons];
		[self performFetchOnNSFetchedResultsController];
		if (_reloadingTable)
		{
			[self doneLoadingTableViewData];
		}
		else
		{
			//     [self.tblAppointments reloadData];
		}
		
		[self todaySection];
		if (_todaySection)
		{
			[self performSelector:@selector(scrollToTodaySection) withObject:nil afterDelay:1.0];
		}
		[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
		
	});
}

- (void)onReceiveAppointmentsSaveFailureNotification {
	dispatch_async(dispatch_get_main_queue(), ^{
		[self hideActivityIndicator];
		[self enableNavigationBarButtons];
		[self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
										 message:LOC(@"KEY_ALERT_FAILED_TO_DOWNLOAD_APPOINTMENT")
												 tag:AlertViewTagException
					 cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
					 otherButtonTitles:nil];
	});
}


- (void) onUpdateAppointmentsSuccessNotificationReceive:(NSNotification*)notification
{
	/*
	 set all modified appointments as not modified and then refresh appointments
	 */
	/*Appointment Data sync Success callback*/
	NSDictionary* response = [notification.object JSONValue];
	CLS_LOG(@"%@",response);
	PSAppDelegate * delgate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
	if(!delgate.postingStartedByOutbox)
	{
		
		NSDictionary* responseDictionary = [response objectForKey:kResponseTag];
		__block NSArray* savedAppointments = [responseDictionary objectForKey:@"savedAppointments"];
		__block NSArray* failedAppointments = [responseDictionary objectForKey:@"failedAppointments"];
		dispatch_async(dispatch_get_main_queue(), ^{
			[[PSDataPersistenceManager sharedManager]changeAppointmentModificationStatus:_sendingAppointments
																																 savedAppointments:savedAppointments
																																failedAppointments:failedAppointments
																																	postNotification:YES];
		});
	}
}

- (void) onUpdateAppointmentsFailureNotificationReceive
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
										 message:LOC(@"KEY_ALERT_FAILED_FEW_APPOINTMENTS_TO_SYNC_ON_SERVER")
												 tag:AlertViewTagException
					 cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
					 otherButtonTitles:nil];
	});
	[self hideActivityIndicator];
	[self enableNavigationBarButtons];
}

-(void) onReceiveAppointmentsNoResultFoundNotification
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[self hideActivityIndicator];
		[self doneLoadingTableViewData];
		[self enableNavigationBarButtons];
		NSManagedObjectContext * context = [PSDatabaseContext sharedContext].managedObjectContext;
		NSArray * appointments = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([Appointment class])
																													sortKey:NULL
																										sortAscending:TRUE
																													context:context];
		if([appointments count] == 0)
		{
			_noResultsFound = TRUE;
		}
		else
		{
			_noResultsFound = FALSE;
		}
		[_tblAppointments reloadData];
	});
	
}

- (void) onSuccessStartSendingImagesNotificationReceive:(NSNotification*)notification
{
	//All Data is posted to server, so refresh all the appointments.
	dispatch_async(dispatch_get_main_queue(), ^{
		//Updating sync status
        
        [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:[_sendingAppointments objectAtIndex:[_sendingAppointments count]-1] andStatus:AppointmentDataSynced andSuccessBlock:^(BOOL isSuccess) {
            if(isSuccess){
                //Synching images after synching appointments.
                
                CLS_LOG(@"Count images for posting to server.");
                
                isAnyImageFail=NO;
                //Registring Success and failure notification only when going to post
                if(imagesToUpload)
                {
                    [imagesToUpload removeAllObjects];
                }
                imagesToUpload = [[NSMutableArray alloc] init];
                for (Appointment *appointment in _sendingAppointments)
                {
                    //getting defaultProperty pictures for syncing
                    if(appointment.appointmentToProperty) //avoiding  Scheme object.
                    {
                        if(appointment.appointmentToProperty.defaultPicture)//Existance Verification
                        {
                            PropertyPicture * pic = appointment.appointmentToProperty.defaultPicture;
                            if(!pic.imagePath || [pic.deletedPicture isEqualToNumber:[NSNumber numberWithBool:YES]]) // image path nil means image not synced and deletePic means dirty picture
                            {
                                [imagesToUpload addObject:pic];
                            }
                        }
                    }
                    //end
                    
                    if([appointment.appointmentType isEqualToString:kAppointmentTypeStock])
                    {
                        // only Stock and gas property images can be deleted
                        for(PropertyPicture * ppic in [appointment.appointmentToProperty.propertyToPropertyPicture allObjects])
                        {
                            if(!ppic.imagePath || [ppic.deletedPicture isEqualToNumber:[NSNumber numberWithBool:YES]])  // Adding dirty images in too
                            {
                                [imagesToUpload addObject:ppic];
                            }
                        }
                    }
                }
                // Now we have send request for uploading of images that are not sent on server.
                //Remove object just as you send
                [self disableNavigationBarButtons];
                if(imagesToUpload.count>0)
                {
                    //image uploading notification
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFailureToSyncImages:) name:kAppointmentOfflineImageSaveNotificationFail object:nil];
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSuccessSendNextImage:) name:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
                    totalImages = (int)imagesToUpload.count;
                    if([[imagesToUpload objectAtIndex:0] isKindOfClass:[PropertyPicture class]])
                    {
                        [((PropertyPicture*)[imagesToUpload objectAtIndex:0]) uploadPicture];
                    }
                    else if([[imagesToUpload objectAtIndex:0] isKindOfClass:[RepairPictures class]])
                    {
                        [((RepairPictures*)[imagesToUpload objectAtIndex:0]) uploadPicture];
                    }
                    else if([[imagesToUpload objectAtIndex:0] isKindOfClass:[DefectPicture class]])
                    {
                        [((DefectPicture*)[imagesToUpload objectAtIndex:0]) uploadPicture];
                    }
                    [imagesToUpload removeObjectAtIndex:0];
                    int remaining = totalImages - (int)imagesToUpload.count;
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    MBProgressHUD * progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [progress setLabelText:[NSString stringWithFormat:@"Syncing %i of %i image",remaining,totalImages]];
                }
                else
                {
                    [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:[_sendingAppointments objectAtIndex:[_sendingAppointments count]-1] andStatus:AppointmentPhotoSynced andSuccessBlock:^(BOOL isSuccess) {
                        if(isSuccess){
                            [self postAppointmentsToServer];
                        }
                        
                    }];
                }
            }
        }];
	});
}

- (void) onFailureAppointmentDataNotificationReceive:(NSNotification*)notification
{
	
	NSArray* failedAppointments = (NSArray*)notification.object;
	
	if(!isEmpty(failedAppointments)) {
        [self showAlertViewForSyncingErrorWithTitle:LOC(@"KEY_ALERT_ERROR")
                                         andMessage:LOC(@"KEY_ALERT_FAILED_FEW_APPOINTMENTS_TO_SYNC_ON_SERVER")
                                      andCloseTitle:LOC(@"KEY_ALERT_CLOSE")
                                      andRetryTitle:LOC(@"KEY_ALERT_RETRY")
                                             andTag:AlertViewTagFailedtoSyncData];
	}
	else {
		dispatch_async(dispatch_get_main_queue(), ^{
			[self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
											 message:LOC(@"KEY_ALERT_FAILED_TO_REFRESH_APPOINTMENT")
													 tag:AlertViewTagFailedToRefreshAppointmentsOnDevice
						 cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
						 otherButtonTitles:nil];
		});
	}
	[self doneLoadingTableViewData];
	[self enableNavigationBarButtons];
	[self hideActivityIndicator];
}

- (void) onRefreshAppointmentExceptionNotificationReceive
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
										 message:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS_FAILURE")
												 tag:AlertViewTagException
					 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
					 otherButtonTitles:nil];
	});
	//  [self doneLoadingTableViewData];
	[self enableNavigationBarButtons];
}

- (void) onReceiveNoConnectivityNotification
{
	dispatch_async(dispatch_get_main_queue(), ^{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																									 message:LOC(@"KEY_ALERT_NO_INTERNET")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		alert.tag = AlertViewTagNoInternet;
		alert.delegate = self;
		[alert show];
	});
	[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void) onReceiveRequestTimeoutNotification
{
	[self doneLoadingTableViewData];
	[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)setAppointmentsTableHeader:(NSUInteger)sections {
	self.noAppointmentsLabel.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
	self.noAppointmentsLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17];
	self.noAppointmentsLabel.text = LOC(@"KEY_STRING_NO_APPOINTMENT");
	self.noAppointmentsLabel.textAlignment = PSTextAlignmentCenter;
	//self.noAppointmentsLabel.backgroundColor = [UIColor clearColor];
	[self.noAppointmentsHeaderView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
	CGRect frame = [self.noAppointmentsHeaderView frame];
	frame.size.height = CGRectGetHeight([self.tblAppointments frame]);
	[self.noAppointmentsHeaderView setFrame:frame];
	
	if(sections == 0)
	{
		[self.tblAppointments setTableHeaderView:self.noAppointmentsHeaderView];
	}
	else
	{
		[self.tblAppointments setTableHeaderView:nil];
	}
	
}

-(void) onReceiveSuccessForDocumentInspection{
    [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:[_sendingAppointments objectAtIndex:[_sendingAppointments count]-1] andStatus:AppointmentSynced andSuccessBlock:^(BOOL isSuccess) {
        if(isSuccess){
            [[PSAppointmentsManager sharedManager] removeCompleteAppointsments:_sendingAppointments];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            _completedAppointments = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] completedAppointments]];
            if(_completedAppointments.count == 0)
            {
                [self reloadAppointments];
            }
            else
            {
                PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
                delegate.postingDataObjectAppointments = delegate.postingDataObjectAppointments - 1;
                [self postAppointmentsToServer];
            }
        }
        
    }];
    
    
}


-(void) onReceiveFailureForDocumentInspection{
    
    [self showAlertViewForSyncingErrorWithTitle:LOC(@"KEY_ALERT_ERROR")
                                     andMessage:LOC(@"KEY_ALERT_FAILED_TO_GENERATE_DOCUMENT")
                                  andCloseTitle:LOC(@"KEY_ALERT_CLOSE")
                                  andRetryTitle:LOC(@"KEY_ALERT_RETRY")
                                         andTag:AlertViewTagFailedToGenerateDocument];

}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// Return the number of sections.
	NSUInteger sections = 0;
	sections = [[self.fetchedResultsController sections] count];
	
	[self setAppointmentsTableHeader:sections];
	
	return sections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 118;
	Appointment *appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	if([appointment.appointmentType compare:kAppointmentTypeGas] == NSOrderedSame)
	{
		height = 118;
	}
	else if([appointment.appointmentType compare:kAppointmentTypeStock] == NSOrderedSame)
	{
		height = 118;
	}
	else if ([appointment.appointmentType compare:kAppointmentTypeFault] == NSOrderedSame)
	{
		NSInteger jobCount = [[appointment.appointmentToJobDataList allObjects] count];
		CGFloat labelsHeight = jobCount * 18;
		if(appointment.appointmentToProperty)
		{
			height = kFaultAppointmentCellDefaultHeight + labelsHeight;
		}
		else
		{
			height = kFaultSchemeAppointmentCellDefaultHeight + labelsHeight;
		}
	}
	else if ([appointment.appointmentType compare:kAppointmentTypeAdaptations] == NSOrderedSame)
	{
		height = kAdaptationAppointmentCellDefaultHeight;
	}
	else if ([appointment.appointmentType compare:kAppointmentTypePlanned] == NSOrderedSame ||
					 [appointment.appointmentType compare:kAppointmentTypeMiscellaneous] == NSOrderedSame ||
					 [appointment.appointmentType compare:kAppointmentTypeCondition] == NSOrderedSame)
	{
		height = kPlannedAppointmentCellDefaultHeight;
	}
	else
	{
		height = 103;
	}
	return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return (_noResultsFound) ? 0.0 : kAppointmentViewSectionHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = nil;
	if(!_noResultsFound)
	{
		NSString *header = nil;
		id <NSFetchedResultsSectionInfo> sectionInfo;
		//[__NSArrayM objectAtIndex:]: index 1 beyond bounds [0 .. 0]
		if ([[_fetchedResultsController sections] count] > 1)
		{
			sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
		}
		else if ([[_fetchedResultsController sections] count] <= 1)
		{
			sectionInfo = [[_fetchedResultsController sections] objectAtIndex:0];
		}
		//sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
		NSArray *sectionLabels = [[sectionInfo name] componentsSeparatedByString:@","];
		
		if(sectionLabels && [sectionLabels count] >= 2)
		{
			headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, tableView.bounds.size.width, kAppointmentViewSectionHeaderHeight)];
			[headerView setBackgroundColor:[UIColor clearColor]];
			UIImageView *imgSectionHeaderBar = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sectionHeaderBar"]];
			[headerView addSubview:imgSectionHeaderBar];
			
			UIFont *dayFont = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:18];
			UIFont *dateFont = [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:18];
			NSString *dayString = [sectionLabels objectAtIndex:0];
			NSString *dateString = [sectionLabels objectAtIndex:1];
			
			CGSize dayStrigSize = CGSizeZero;
			CGSize dateStrigSize = CGSizeZero;
			if(OS_VERSION >= 7.0)
			{
				dayStrigSize = [dayString sizeWithAttributes:[NSDictionary dictionaryWithObject:dayFont forKey:NSFontAttributeName]];
				dateStrigSize = [dateString sizeWithAttributes:[NSDictionary dictionaryWithObject:dateFont forKey:NSFontAttributeName]];
			}
			else
			{
				dayStrigSize = [dayString sizeWithFont:dayFont];
				dateStrigSize = [dateString sizeWithFont:dateFont];
			}
			
			UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, dayStrigSize.width, kAppointmentViewSectionHeaderHeight)];
			[dayLabel setFont:dayFont];
			[dayLabel setBackgroundColor:[UIColor clearColor]];
			[dayLabel setTextColor:[UIColor whiteColor]];
			[dayLabel setText:dayString];
			
			UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(dayStrigSize.width + 7, 0, dateStrigSize.width, kAppointmentViewSectionHeaderHeight)];
			[dateLabel setFont:dateFont];
			[dateLabel setBackgroundColor:[UIColor clearColor]];
			[dateLabel setTextColor:[UIColor whiteColor]];
			[dateLabel setText:dateString];
			
			[headerView setAlpha:1.0];
			[headerView addSubview:dayLabel];
			[headerView addSubview:dateLabel];
		}
	}
	return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// Return the number of rows in the section.
	NSUInteger rows = 0;
	id <NSFetchedResultsSectionInfo> sectionInfo;
	sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
	rows = [sectionInfo numberOfObjects];
	return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = nil;
	//    Appointment *appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
	Appointment *appointment = nil;
	//check if indexPath is within range of fetchedResultsController limit
	if ([[self.fetchedResultsController sections] count] >= [indexPath section]){
		id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:[indexPath section]];
		if ([sectionInfo numberOfObjects] >= [indexPath row]){
			appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
		}
	}
	
	CLS_LOG(@"Appoinment id %@",appointment.appointmentId);
	NSString * className = NSStringFromClass([PSAppointmentCell class]);
	PSAppointmentCell *_cell = [self.tblAppointments dequeueReusableCellWithIdentifier:className];
	if(_cell == nil)
	{
		[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
		_cell = self.appointmentCell;
		self.appointmentCell = nil;
	}
	// Configure the cell...
	[self configureCell:_cell atIndexPath:indexPath];
	[_cell reloadData:appointment];
	[_cell setDelegate:self];
	cell = _cell;
	return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	
	Appointment *appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
	AppointmentStatus appointmentStatus = [appointment getStatus];
	AppointmentType appointmentType = [appointment getType];
	
	if ([cell isKindOfClass:[PSAppointmentCell class]])
	{
		PSAppointmentCell *_cell = (PSAppointmentCell *)cell;
		_cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
		_cell.lblAppointmentTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
		_cell.lblAppointmentStatus.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
		_cell.lblArrangedBy.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
		_cell.lblCertificateExpiry.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
		
		_cell.lblAppointmentTitle.text = appointment.appointmentTitle;
		_cell.lblArrangedBy.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_ASSIGNED_BY"),
																appointment.createdByPerson];
		_cell.lblAppointmentDate.text = [appointment appointmentDateWithStyle:AppointmentDateStyleComplete
																															 dateFormat:kDateTimeStyle9];
		
		_cell.lblAppointmentTime.text = [appointment appointmentDateWithStyle:AppointmentDateStyleStartToEnd
																															 dateFormat:kDateTimeStyle3];
		
		_cell.lblAddress.text = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleLong];
		_cell.lblAppointmentStatus.text = [appointment stringWithAppointmentStatus:appointmentStatus];
		
		_cell.imgAppointmentType.image = [self imageForAppointmentType:appointmentType];
		_cell.imgAppointmentStatus.image = [self imageForAppointmentCompletionStatus:appointmentStatus];
		
		if ([appointment.appointmentType compare:kAppointmentTypeGas] == NSOrderedSame)
		{
			if (!isEmpty(appointment.appointmentToProperty.certificateExpiry))
			{
				_cell.lblCertificateExpiry.hidden = NO;
				_cell.lblCertificateExpiry.text = [NSString stringWithFormat:@"%@: %@", LOC(@"KEY_STRING_CERTIFICATE_EXPIRY"), [UtilityClass stringFromDate:appointment.appointmentToProperty.certificateExpiry dateFormat:kDateTimeStyle8]];
			}
			else
			{
				_cell.lblCertificateExpiry.hidden = YES;
			}
		}
		else
		{
			_cell.lblCertificateExpiry.hidden = YES;
		}
		
		__weak typeof(self) weakSelf = self;
		
		if ([appointment isDownloadInProgress])
		{
			[_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
			[_cell showProgressView:YES animated:[self isViewLoaded]];
			[_cell updateProgess:0];
			if (![appointment progressBlock])
			{
				[appointment setProgressBlock: ^(long long totalBytesRead, long long totalBytesExpected) {
					NSIndexPath *cellIndexPath = [indexPath copy];
					NSIndexPath *indexPathOfCell = [self.tblAppointments indexPathForCell:cell];
					if ([indexPathOfCell isEqual:indexPath])
					{
						if (totalBytesRead < totalBytesExpected)
						{
							[_cell showProgressView:YES animated:[self isViewLoaded]];
							float progress = (float)totalBytesRead * 100.0 / (float)totalBytesExpected;
							[_cell updateProgess:progress];
						}
						else
						{
							[_cell showProgressView:NO animated:[self isViewLoaded]];
							[weakSelf configureCell:cell atIndexPath:indexPath];
						}
					}
				}];
			}
		}
		else if ([appointment isPreparedForOffline] && [appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[appointment getType]])
		{
			[_cell setSelectionStyle:UITableViewCellSelectionStyleGray];
			[_cell showProgressView:NO animated:NO];
			_cell.imgAppointmentOfflineStatus.image = [UIImage imageNamed:@"icon_offline_ready"];
		}
		else
		{
			[_cell showProgressView:NO animated:NO];
			_cell.imgAppointmentOfflineStatus.image = nil;
		}
		cell = _cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	if(!_noResultsFound)
	{
		
		Appointment *appointment = [self.fetchedResultsController objectAtIndexPath:indexPath];
		
		if (![appointment isDownloadInProgress])
		{
			self.appointmentDetailViewConroller = [[PSAppointmentDetailViewController alloc] initWithNibName:NSStringFromClass([PSAppointmentDetailViewController class]) bundle:[NSBundle mainBundle]];
			[(PSAppointmentDetailViewController*)self.appointmentDetailViewConroller setAppointment:appointment];
			[(PSAppointmentDetailViewController*)self.appointmentDetailViewConroller setMainAppViewController:self];//week assign
			[self.navigationController pushViewController:self.appointmentDetailViewConroller animated:YES];
		}
	}
}

#pragma mark - Pull To Refresh Methods
- (void)addPullToRefreshControl
{
	if (_refreshHeaderView == nil)
	{
		EGORefreshTableHeaderView *headerView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tblAppointments.bounds.size.height, self.view.frame.size.width, self.tblAppointments.bounds.size.height)];
		headerView.delegate = self;
		[self.tblAppointments addSubview:headerView];
		_refreshHeaderView = headerView;
	}
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource
{
	_reloadingTable = YES;
	[self disableNavigationBarButtons];
	[self.tblAppointments setUserInteractionEnabled:NO];
	[self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
															 LOC(@"KEY_ALERT_LOADING"),
															 LOC(@"KEY_STRING_APPOINTMENTS")]];
	
	[[PSAppointmentsManager sharedManager] refreshAllAppointsments];
	[self.tblAppointments reloadData];
}

- (void)doneLoadingTableViewData
{
	_reloadingTable = NO;
	[self.tblAppointments reloadData];
	[self.tblAppointments setUserInteractionEnabled:YES];
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tblAppointments];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

//This method is called to halt the scrolling on any updation that might cause
//insertion or deletion in NSfetchresultcontroller for section
- (void)killScroll
{
	self.tblAppointments.scrollEnabled = NO;
	self.tblAppointments.scrollEnabled = YES;
}


#pragma mark - Scroll Cell Methods
- (void)scrollToTodaySection {
	if (_todaySection >= 0 && _todaySection < [[self.fetchedResultsController sections] count])
	{
		id <NSFetchedResultsSectionInfo> sectionInfo;
		sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:_todaySection];
		NSUInteger rowcount = [sectionInfo numberOfObjects];
		if(rowcount > 0)
		{
			//Found bug.. it is not necessary that today always have the row
			[self.tblAppointments scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0
																																			inSection:_todaySection]
																	atScrollPosition:UITableViewScrollPositionTop
																					animated:YES];
		}
	}
}

#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view
{
	//Refresh Table View Data Source
	if([[ReachabilityManager sharedManager] isConnectedToInternet])
	{
		[self checkAppointmentsForModifications];
	}
	else
	{
		_completedAppointments = nil;
		[self doneLoadingTableViewData];
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																									 message:LOC(@"KEY_ALERT_NO_INTERNET")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil,nil];
		alert.tag = AlertViewTagNoInternet;
		[alert show];
	}
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view
{
	return _reloadingTable; // should return if data source model is reloading
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
	return [NSDate date]; // should return date data source was last changed
}

#pragma mark - Fetched results controller
- (void)performFetchOnNSFetchedResultsController
{
	CLS_LOG(@"performFetchOnNSFetchedResultsController");
	NSError *error;
	if (![[self fetchedResultsController] performFetch:&error])
	{
		// Update to handle the error appropriately.
		CLS_LOG(@"fetchedResultsController, Unresolved error %@, %@", error, [error userInfo]);
	}
	[self.tblAppointments reloadData];
}

- (void) checkResultsEmptiness
{
	if([[self.fetchedResultsController fetchedObjects] count] <= 0)
	{
		_noResultsFound = YES;
	}
	else
	{
		_noResultsFound = NO;
	}
//#warning PSA: FIX NO RESULTS YES BUG HERE (ON RESET SIMULATOR)
}

- (NSFetchedResultsController *)fetchedResultsController
{
	if (_fetchedResultsController != nil)
	{
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	// Edit the entity name as appropriate.
	NSEntityDescription *entity = [NSEntityDescription entityForName:kAppointment inManagedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext];
	[fetchRequest setEntity:entity];
	
	// Set the batch size to a suitable number.
	[fetchRequest setFetchBatchSize:20];
	
	// Edit the sort key as appropriate.
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kAppointmentStartDateTime ascending:YES];
	NSArray *sortDescriptors = @[sortDescriptor];
	
	[fetchRequest setSortDescriptors:sortDescriptors];
	
	
	// self.appointmentFilterPredicate = [NSPredicate predicateWithFormat:@"SELF.appointmentToUser.userId == %@", [SettingsClass sharedObject].loggedInUser.userId];
	self.appointmentFilterPredicate = [NSPredicate predicateWithFormat:@"(SELF.appointmentToUser.userName LIKE[cd] SELF.surveyorUserName) AND (appointmentToUser.userId == %@)", [SettingsClass sharedObject].loggedInUser.userId];
	
	[fetchRequest setPredicate:self.appointmentFilterPredicate];
	
	// Edit the section name key path and cache name if appropriate.
	// nil for section name key path means "no sections".
	NSFetchedResultsController*aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[PSDatabaseContext sharedContext].managedObjectContext sectionNameKeyPath:kAppointmentDateSectionIdentifier cacheName:nil];
	aFetchedResultsController.delegate = self;
	self.fetchedResultsController = aFetchedResultsController;
	
	[NSFetchedResultsControllerHelper filterAppointmentsWithOptions:[SettingsClass sharedObject].filterOptions
																											 controller:self.fetchedResultsController];
	[self todaySection];
	
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
		CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
	}
	
	return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
	[self.tblAppointments beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo> )sectionInfo
					 atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
	[self killScroll];
	switch (type)
	{
		case NSFetchedResultsChangeInsert:
			[self.tblAppointments insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self.tblAppointments deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
			break;
		default:
			break;
	}
	_allowRelaodingTableView_ = YES;
	[self todaySection];
	[self.tblAppointments reloadData];
	_allowRelaodingTableView_ = NO;
}

- (void)controller:(NSFetchedResultsController *)controller
	 didChangeObject:(id)anObject
			 atIndexPath:(NSIndexPath *)indexPath
		 forChangeType:(NSFetchedResultsChangeType)type
			newIndexPath:(NSIndexPath *)newIndexPath
{
	UITableView *tableView = self.tblAppointments;
	
	switch (type)
	{
		case NSFetchedResultsChangeInsert:
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeDelete:
			[self killScroll];
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
			
		case NSFetchedResultsChangeUpdate:
			[self updateAppointmentDetailOnAppointmentUpdate];
			break;
			
		case NSFetchedResultsChangeMove:
			[self killScroll];
			[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
	}
	//Reloading table view for removing duplicates and
	_allowRelaodingTableView_ = YES;
	[self todaySection];
	[self.tblAppointments reloadData];
	_allowRelaodingTableView_ = NO;
	
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	[self.tblAppointments endUpdates];
}

#pragma mark - Appointmetns Filter View
- (void)addAppointmentsFilterView {
	CGRect frame = self.view.frame;
	CGRect rect = CGRectMake(frame.size.width,
													 frame.origin.y,
													 270,
													 frame.size.height);
	filterViewController = [[PSAppointmentsFilterViewController alloc] initWithNibName:@"PSAppointmentsFilterViewController" bundle:[NSBundle mainBundle]];
	filterViewController.delegate = self;
	filterViewController.view.frame = rect;
	CGRect lblFrame = filterViewController.lblAppVersion.frame;
	lblFrame.size.width = filterViewController.view.frame.size.width;
	lblFrame.origin.x = 0;
	[filterViewController.lblAppVersion setFrame:lblFrame];
	[filterViewController.view setHidden:YES];
	[self.view addSubview:filterViewController.view];
}

- (void)showAppointmentsFilterView
{
	__block CGRect frame = filterViewController.view.frame;
	filterViewController.view.hidden = NO;
	frame.origin.x = 50;
	[UIView animateWithDuration:0.5
									 animations: ^{
										 filterViewController.view.frame = frame;
									 }
	 								 completion: ^(BOOL finished) {
										 isSearching = YES;
										 filterViewController.view.userInteractionEnabled = YES;
										 self.tblAppointments.userInteractionEnabled = NO;
									 }];
}

- (void)hideAppointmentsFilterView
{
	__block CGRect frame = filterViewController.view.frame;
	frame.origin.x = self.view.frame.size.width;
	[UIView animateWithDuration:0.5
									 animations: ^{
										 filterViewController.view.frame = frame;
									 }
									 completion: ^(BOOL finished) {
										 isSearching = NO;
										 filterViewController.view.hidden = YES;
										 filterViewController.view.userInteractionEnabled = NO;
										 self.tblAppointments.userInteractionEnabled = YES;
									 }];
}

#pragma mark - PSAppointmetnsFilterProtocol
- (void)filterAppointmentsWithOptions:(NSArray *)filterOptions
{
	[self killScroll];
	CLS_LOG(@"Filter Options: %@", filterOptions);
	[SettingsClass sharedObject].filterOptions = [NSArray arrayWithArray:filterOptions];
	[[SettingsClass sharedObject] saveSettings];
	[NSFetchedResultsControllerHelper filterAppointmentsWithOptions:filterOptions
																											 controller:self.fetchedResultsController];
	[self todaySection];
	[self checkResultsEmptiness];
	[self.tblAppointments reloadData];
	[self scrollToTodaySection];
	[self hideAppointmentsFilterView];
	
	for (NSNumber *filterOption in filterOptions)
	{
		switch ([filterOption integerValue])
		{
			case PSFilterOptionViewAll:
			{
				//[[PSAppointmentsManager sharedManager] fetchAllAppointsments];
			}
				break;
				
			case PSFilterOptionViewToday:
			{
				NSDate *startDate = [[NSDate date] dateAtStartOfDay]; //12:00:00 am current date
				NSDate *endDate = [[[[NSDate date] dateByAddingDays:1] dateAtStartOfDay] dateBySubtractingSeconds:1]; //11:59:59 pm current date
				
				[[PSAppointmentsManager sharedManager] fetchAllAppointsmentsFromDate:startDate toDate:endDate];
			}
				break;
				
			case PSFilterOptionViewNextFiveDays:
			{
				NSDate *startDate = [[NSDate date] dateAtStartOfDay]; //12:00:00 am current date
				NSDate *endDate = [[[[NSDate date] dateByAddingDays:5] dateAtStartOfDay] dateBySubtractingSeconds:1]; //11:59:59 PM 5th day next to today
				
				[[PSAppointmentsManager sharedManager] fetchAllAppointsmentsFromDate:startDate toDate:endDate];
			}
				break;
			default:
				break;
		}
	}
}

#pragma mark - PSAppointmentsAddressMapsProtocol
- (void)addressTapped:(Appointment *)appointment
{
	PSMapsViewController *mapViewController = [[PSMapsViewController alloc] initWithNibName:@"PSMapsViewController" bundle:[NSBundle mainBundle]];
	
	if(appointment.appointmentToProperty)
	{
		[mapViewController setAddress:[appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull]];
		[mapViewController setNavigationTitle:[appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
	}
	else if(appointment.appointmentToScheme)
	{
		[mapViewController setAddress:[appointment.appointmentToScheme fullAddress]];
		[mapViewController setNavigationTitle:[appointment.appointmentToScheme shortAddress]];
	}
	[self.navigationController pushViewController:mapViewController animated:YES];
}

#pragma mark - PSAppointments FailedToSyncReasonProtocol

- (void)failedReasonButtonTapped:(Appointment *)appointment
{
	
}

#pragma mark - Appointment Modifications
-(void) checkAppointmentsForModifications
{
	_completedAppointments = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] completedAppointments]];
	
	if (!isEmpty(_completedAppointments))
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_WARNING")
																									 message:LOC(@"KEY_ALERT_MODIFIED_APPOINTMENTS")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_STRING_POST_DATA") otherButtonTitles:LOC(@"KEY_ALERT_CANCEL"),nil];
		alert.tag = AlertViewTagPostAppointmentData;
		[alert show];
	}
	else
	{
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS")
																									 message:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS_MESSAGE")
																									delegate:self
																				 cancelButtonTitle:LOC(@"KEY_ALERT_NO") otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
		alert.tag = AlertViewTagRefreshAppointments;
		[alert show];
	}
}

-(void) retrySendingAppointmentsToServer{
    [self postAppointmentsToServer];
}

-(void) postAppointmentsToServer
{
	//fetching app delegate to check wether data is already in uploading or not
	CLS_LOG(@"post appointment method");
    _completedAppointments = [NSMutableArray arrayWithArray:[[PSAppointmentsManager sharedManager] completedAppointments]];
	PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
	delegate.postingDataObjectAppointments = delegate.postingDataObjectAppointments + 1;
	/*if(delegate.postingDataObjectAppointments > 1)
	{
		delegate.postingDataObjectAppointments = 1;
		CLS_LOG(@"Message already busy needs appearing");
		UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
																									 message:LOC(@"KEY_ALERT_DATA_POSTING_ALREADY_RUNNING")
																									delegate:nil
																				 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																				 otherButtonTitles:nil];
		[alert show];
		return;
	}*/
	if(_completedAppointments && _completedAppointments.count>0)
	{
		_allowRelaodingTableView_ = NO;
		delegate.postingStartedByOutbox = NO;
		MBProgressHUD * progress = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
		[self disableNavigationBarButtons];
		// Sending Appointments Data,
		[progress setLabelText:@"Uploading Appointments..."];
		
		CLS_LOG(@"Appointments data posting...");
		
		_sendingAppointments = [NSMutableArray array];
		[_sendingAppointments addObject:[_completedAppointments objectAtIndex:0]];
		[_completedAppointments removeObjectAtIndex:0];
		
		NSMutableArray *appointments = [NSMutableArray array];
		for (Appointment *appointment in _sendingAppointments)
		{
			NSDictionary *appointmentDictionary = [appointment JSONValue];
			if (!isEmpty(appointmentDictionary))
			{
				[appointments addObject:appointmentDictionary];
			}
		}
        [self appointmentPostingRouterForAppointments:appointments];
		
	}
}

-(void) appointmentPostingRouterForAppointments:(NSMutableArray*) appointments{
    Appointment *appointmentToBeSent = [_sendingAppointments objectAtIndex:0];
    if([appointmentToBeSent.syncStatus integerValue]==AppointmentNotSynced){
        if (isEmpty(appointments) == FALSE)
        {
            NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
            [requestParameters setObject:appointments forKey:@"appointments"];
            [requestParameters setObject:[NSNumber numberWithBool:NO] forKey:@"respondWithData"];
            [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
            [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
            [requestParameters setObject:[NSNumber numberWithBool:YES] forKey:@"responseWithIndividualStatus"];
            
            NSString *paramsStr = [requestParameters JSONFragment];
            
            CLS_LOG(@"\nPost Data JSON \n%@",paramsStr);
            [[PSAppointmentsManager sharedManager] updateAppointments:requestParameters];
        }
    }
    else if([appointmentToBeSent.syncStatus integerValue]==AppointmentDataSynced){
        [self onSuccessStartSendingImagesNotificationReceive:nil];
    }
    else if([appointmentToBeSent.syncStatus integerValue] ==AppointmentPhotoSynced){
        [self generateInspectionDocumentReport];
    }
    else if([appointmentToBeSent.syncStatus integerValue] ==AppointmentSynced){
        [self onReceiveSuccessForDocumentInspection];
    }
}

-(void) generateInspectionDocumentReport
{
	dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        MBProgressHUD * progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [progress setLabelText:[NSString stringWithFormat:@"Generating Stock Document"]];
        [[PSAppointmentsManager sharedManager] createInspectionDocumentForAppointments:_sendingAppointments];
		
	});
}

-(void)RollBackIfNoInternetAvailability
{
	NSLog(@"Rollback");
	[imagesToUpload removeAllObjects];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineImageSaveNotificationFail object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
	PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
	delegate.postingDataObjectAppointments =0;
	[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
	[self enableNavigationBarButtons];
	[self hideActivityIndicator];
	[self doneLoadingTableViewData];
}

-(BOOL)checkAllImagesPostedSuccessFully
{
	if(imagesToUpload.count==0 && !isAnyImageFail)
	{
		NSLog(@"image posting success");
		//Unregistring Success and failure notification only when going to post
		[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineImageSaveNotificationFail object:nil];
		[[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
		return YES;
	}
	return NO;
}

-(void)onFailureToSyncImages:(id)sender
{
	isAnyImageFail = YES;
	[self RollBackIfNoInternetAvailability];
    [self showAlertViewForSyncingErrorWithTitle:LOC(@"KEY_ALERT_ERROR")
                                     andMessage:LOC(@"KEY_ALERT_FAILED_TO_SYNC_IMAGES")
                                  andCloseTitle:LOC(@"KEY_ALERT_CLOSE")
                                  andRetryTitle:LOC(@"KEY_ALERT_RETRY")
                                         andTag:AlertViewTagFailedToSyncImages];
	
}
- (void)reloadAppointments
{
	PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
	delegate.postingDataObjectAppointments = 0;
	[self enableNavigationBarButtons];
	UIAlertView * alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS")
																									message:LOC(@"KEY_ALERT_REFRESH_APPOINTMENTS_MESSAGE")
																								 delegate:self
																				cancelButtonTitle:LOC(@"KEY_ALERT_NO")
																				otherButtonTitles:LOC(@"KEY_ALERT_YES"), nil];
	alert.tag = AlertViewTagRefreshAppointments;
	[alert show];
}

-(void)onSuccessSendNextImage:(id)Sender
{
	if([self checkAllImagesPostedSuccessFully])
	{
        [[PSDataUpdateManager sharedManager] updateAppointmentSyncStatusForAppointment:[_sendingAppointments objectAtIndex:[_sendingAppointments count]-1] andStatus:AppointmentPhotoSynced andSuccessBlock:^(BOOL isSuccess) {
            if(isSuccess){
                [self postAppointmentsToServer];
            }
        }];
		return;
	}
	if(imagesToUpload)
	{
		if(imagesToUpload.count>0)
		{
			if([[imagesToUpload objectAtIndex:0] isKindOfClass:[PropertyPicture class]])
			{
				[((PropertyPicture*)[imagesToUpload objectAtIndex:0]) uploadPicture];
			}
			else if([[imagesToUpload objectAtIndex:0] isKindOfClass:[RepairPictures class]])
			{
				[((RepairPictures*)[imagesToUpload objectAtIndex:0]) uploadPicture];
			}
			else if([[imagesToUpload objectAtIndex:0] isKindOfClass:[DefectPicture class]])
			{
				[((DefectPicture*)[imagesToUpload objectAtIndex:0]) uploadPicture];
			}
			
			[imagesToUpload removeObjectAtIndex:0];
			[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
			MBProgressHUD * progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
			int remaining = totalImages - (int)imagesToUpload.count;
			[progress setLabelText:[NSString stringWithFormat:@"Syncing %i of %i image",remaining,totalImages]];
		}
		else
		{
			[self RollBackIfNoInternetAvailability];
		}
	}
}

#pragma mark - Methods
-(UIImage *)imageForAppointmentType:(AppointmentType)type
{
	UIImage *imgAppointmentType = nil;
	@autoreleasepool
	{
		if (type == AppointmentTypeStock)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_stock"];
		}
		else if (type == AppointmentTypeGas)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_gas"];
		}
		else if (type == AppointmentTypeVoid)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_void"];
		}
		else if (type == AppointmentTypeFault)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_repair"];
		}
		else if (type == AppointmentTypePre)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_pre"];
		}
		else if (type == AppointmentTypePost)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_post"];
		}
		else if (type == AppointmentTypePlanned)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_planned"];
		}
		else if (type == AppointmentTypeAdaptations)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_adaptations"];
		}
		else if (type == AppointmentTypeMiscellaneous)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_miscellaneous"];
		}
		else if (type == AppointmentTypeCondition)
		{
			imgAppointmentType = [UIImage imageNamed:@"icon_crt"];
		}
	}
	return imgAppointmentType;
}

- (UIImage *)imageForAppointmentCompletionStatus:(AppointmentStatus)status {
	UIImage *imgAppointmentStatus = nil;
	@autoreleasepool {
		if (status == AppointmentStatusComplete) {
			imgAppointmentStatus = [UIImage imageNamed:@"icon_completed"];
		}
		else if (status == AppointmentStatusNoEntry)
		{
			imgAppointmentStatus = [UIImage imageNamed:@"icon_completed"];
		}
		else if (status == AppointmentStatusFinished) {
			imgAppointmentStatus = [UIImage imageNamed:@"icon_completed"];
		}
		else if (status == AppointmentStatusInProgress) {
			imgAppointmentStatus = [UIImage imageNamed:@"icon_progress"];
		}
		else if (status == AppointmentStatusPaused) {
			imgAppointmentStatus = [UIImage imageNamed:@"icon_pause"];
		}
		else if (status == AppointmentStatusNotStarted) {
			imgAppointmentStatus = [UIImage imageNamed:@"icon_notepad"];
		}
	}
	return imgAppointmentStatus;
}

- (void) disableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons)
	{
		button.enabled = NO;
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = NO;
	}
	
}

- (void) enableNavigationBarButtons
{
	NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
	for (PSBarButtonItem *button in navigationBarRightButtons)
	{
		button.enabled = YES;
	}
	
	NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
	for (PSBarButtonItem *button in navigationBarLeftButtons)
	{
		button.enabled = YES;
	}
}

-(void) showAlertViewForSyncingErrorWithTitle: (NSString *) title andMessage:(NSString *) message andCloseTitle:(NSString*) closeTitle andRetryTitle:(NSString *) retryTitle andTag:(NSInteger) alertTag{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:self
                                               cancelButtonTitle: retryTitle
                                               otherButtonTitles:closeTitle, nil];
        alert.tag = alertTag;
        [alert show];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    });
    
}

- (void) todaySection
{
	id <NSFetchedResultsSectionInfo> sectionInfo;
	
	NSString *currentDateString;
	NSString *tomorrowDateString;
	NSString * dayAfterTomorrowDateString;
	NSInteger tomorrowSection = -1;
	NSInteger dayAfterTomorrowSection = -1;
	
	currentDateString = [UtilityClass stringFromDate:[NSDate date] dateFormat:kDateTimeStyle18];
	tomorrowDateString = [UtilityClass stringFromDate:[[NSDate date] dateByAddingDays:1] dateFormat:kDateTimeStyle18];
	dayAfterTomorrowDateString = [UtilityClass stringFromDate:[[NSDate date] dateByAddingDays:2] dateFormat:kDateTimeStyle18];
	
	for (int sectionNumber = 0; sectionNumber < [[_fetchedResultsController sections] count]; ++sectionNumber)
	{
		sectionInfo = [[_fetchedResultsController sections] objectAtIndex:sectionNumber];
		NSArray *sectionLabels = [[sectionInfo name] componentsSeparatedByString:@","];
		NSString *dateString = [sectionLabels objectAtIndex:1];
		if ([[dateString trimWhitespace] isEqualToString:[currentDateString trimWhitespace]])
		{
			_todaySection = sectionNumber;
		}
		else if ([[dateString trimWhitespace] isEqualToString:[tomorrowDateString trimWhitespace]])
		{
			tomorrowSection = sectionNumber;
		}
		else if ([[dateString trimWhitespace] isEqualToString:[dayAfterTomorrowDateString trimWhitespace]])
		{
			dayAfterTomorrowSection = sectionNumber;
		}
	}
	if (_todaySection == - 1)
	{
		if (tomorrowSection > - 1) {
			_todaySection = tomorrowSection;
		}
		else if (dayAfterTomorrowSection > - 1)
		{
			_todaySection = dayAfterTomorrowSection;
		}
	}
}

#pragma mark - Core Data Update Events
- (void) updateAppointmentDetailOnAppointmentUpdate {
	if (self.appointmentDetailViewConroller != nil)
	{
		if([self.appointmentDetailViewConroller respondsToSelector:@selector(onAppointmentObjectUpdate)])
		{
			[self.appointmentDetailViewConroller onAppointmentObjectUpdate];
		}
	}
}

@end
