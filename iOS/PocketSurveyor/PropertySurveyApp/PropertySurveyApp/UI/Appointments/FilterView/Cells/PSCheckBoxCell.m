//
//  PSCheckBoxCell.m
//  PSAOffline
//
//  Created by My Mac on 08/08/2013.
//  Copyright (c) 2013 My Mac. All rights reserved.
//

#import "PSCheckBoxCell.h"

@implementation PSCheckBoxCell

@synthesize labelOutlet;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)submitAction:(id)sender{

    
    if (self.isCheckBoxSelected)
    {
        [self makeDeselected];
        if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didDeselectFilterOption:indexPath:)])
        {
            [(PSAppointmentsFilterViewController *)self.delegate didDeselectFilterOption:self.tag indexPath:self.indexPath];
        }
    }
    else
    {
        [self makeSelected];
        if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectFilterOption:indexPath:)])
        {
                       
            [(PSAppointmentsFilterViewController *)self.delegate didSelectFilterOption:self.tag indexPath:self.indexPath];
        }
    }
}

- (void) makeSelected
{
    
    self.isCheckBoxSelected = YES;
    [self.btnCheckBox setImage:[UIImage imageNamed:@"checkbox_marked"] forState:UIControlStateNormal];
    [self.labelOutlet setFont:[UIFont boldSystemFontOfSize:14.0F]];
}

- (void) makeDeselected
{
    self.isCheckBoxSelected = NO;
    [self.btnCheckBox setImage:[UIImage imageNamed:@"checkbox_unmark"] forState:UIControlStateNormal];
    [self.labelOutlet setFont:[UIFont systemFontOfSize:14.0F]];
}

@end
