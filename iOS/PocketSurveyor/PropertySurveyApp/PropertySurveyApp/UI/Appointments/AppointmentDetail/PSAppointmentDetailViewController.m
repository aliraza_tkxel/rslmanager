//
//  PSAppointmentDetailViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 20/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppointmentDetailViewController.h"
#import "PSPropertyCell.h"
#import "PSTypeCell.h"
#import "PSTypeCell.h"
#import "PSAsbestosCell.h"
#import "PSTenantCell.h"
#import "PSJSNumberCell.h"
#import "PSAccomodationsCell.h"
#import "PSAppDelegate.h"
#import "Appointment+CoreDataClass.h"
#import "Scheme+JSON.h"
#import "Customer+CoreDataClass.h"
#import "PSTenantDetailViewController.h"
#import "PSAccomodationViewController.h"
#import "PSBarButtonItem.h"
#import "PSJobDetailViewController.h"
#import "PSSurveyViewController.h"
#import "PSAppointmentDetailNotesCell.h"
#import "Appointment+SurveyDownload.h"
#import "Survey+JSON.h"
#import "Appointment+JSON.h"
#import "PSCP12InfoViewController.h"
#import "JobDataList+Methods.h"
#import "PSFaultNoEntryViewController.h"
#import "PSPlannedComponentCell.h"
#import "PSPlannedJobDetailViewController.h"
#import "PSAppointmentNotesViewController.h"
#import "PSFaultAppointmentPropertyCell.h"
#import "PSMapsViewController.h"
#import "PSImageViewController.h"
#import "PSAppointmentsViewController.h"
#import "PSAdaptationJobDetailViewController.h"
#import "PSFaultAppointmentSchemeCell.h"
#import "PSConditionJobDetailViewController.h"



#define kViewSectionHeaderHeight 20

@interface PSAppointmentDetailViewController ()
{
    AppointmentType _appointmentType;
    AppointmentStatus _appointmentStatus;
}
@end

@implementation PSAppointmentDetailViewController

@synthesize headerViewArray;
@synthesize appointment;
@synthesize property;
@synthesize tenantsArray;
@synthesize jobDataListArray;
@synthesize asbestosArray;
@synthesize mainAppViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isEditing = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _appointmentType = [self.appointment getType];
    _appointmentStatus = [self.appointment getStatus];
    self.headerViewArray = [NSMutableArray array];
    self.tenantsArray = [NSMutableArray arrayWithArray:[appointment.appointmentToCustomer allObjects]];
    self.jobDataListArray =  [NSMutableArray arrayWithArray:[appointment.appointmentToJobDataList allObjects]];
    self.property = appointment.appointmentToProperty;
    self.asbestosArray =  [NSMutableArray arrayWithArray:[property.propertyToPropertyAsbestosData allObjects]];
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self loadNavigationonBarItems];
    [self addTableHeaderView];
    if(appointment.appointmentToProperty)
    {
        [self loadSectionHeaderViews:@[LOC(@"KEY_STRING_PROPERTY"),LOC(@"KEY_STRING_COMPONENT"),LOC(@"KEY_STRING_TYPE"),LOC(@"KEY_STRING_TENANT"),@"",LOC(@"KEY_STRING_ASBESTOS"),@"",LOC(@"KEY_STRING_CUSTOMER_NOTES")] headerViews:self.headerViewArray];
    }
    else
    {
        [self loadSectionHeaderViews:@[@"",LOC(@"KEY_STRING_COMPONENT"),LOC(@"KEY_STRING_TYPE"),LOC(@"KEY_STRING_TENANT"),@"",LOC(@"KEY_STRING_ASBESTOS"),@"",LOC(@"KEY_STRING_CUSTOMER_NOTES")] headerViews:self.headerViewArray];
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self.appointment getType] == AppointmentTypePlanned || [self.appointment getType] == AppointmentTypeMiscellaneous || [self.appointment getType] == AppointmentTypeAdaptations || [self.appointment getType] == AppointmentTypeCondition)
    {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.noEntryButton, nil]];
        if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
            || [self.appointment getStatus] == AppointmentStatusNoEntry)
        {
            [self.noEntryButton setEnabled:NO];
            [self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
        }
        else
        {
            [self.noEntryButton setEnabled:YES];
        }
    }
    else if ([self.appointment getType] == AppointmentTypeFault)
    {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.notesButton, self.noEntryButton, nil]];
        if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
            || [self.appointment getStatus] == AppointmentStatusNoEntry)
        {
            [self.noEntryButton setEnabled:NO];
            [self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
        }
        else
        {
            [self.noEntryButton setEnabled:YES];
        }
    }
    else
    {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.downloadButton, self.trashButton, self.notesButton, nil]];
    }
    [self registerNotifications];
    [self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    // [self deRegisterNotifications];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.trashButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                              style:PSBarButtonItemStyleTrash
                                                             target:self
                                                             action:@selector(onClickTrashButton)];
    
    self.downloadButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                 style:PSBarButtonItemStyleDownload
                                                                target:self
                                                                action:@selector(onClickDownloadButton)];
    
    self.saveButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                             style:PSBarButtonItemStyleDefault
                                                            target:self
                                                            action:@selector(onClickSaveButton)];
    self.noEntryButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_NO_ENTRY")
                                                                style:PSBarButtonItemStyleDefault
                                                               target:self
                                                               action:@selector(onClickNoEntryButton)];
    self.notesButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                              style:PSBarButtonItemStyleNotes
                                                             target:self
                                                             action:@selector(onClickNotesButton)];
    _appointmentStatus = [self.appointment getStatus];
    _appointmentType = [self.appointment getType];
    //Set Download Button
    if ((_appointmentStatus == [UtilityClass completionStatusForAppointmentType:_appointmentType] ||
         (_appointmentStatus == AppointmentStatusNoEntry)) ||
        ([self.appointment isPreparedForOffline]))
    {
        [self.downloadButton setEnabled:NO];
    }
    
    if(_appointmentType == ApplicationTypeStock && _appointmentStatus == AppointmentStatusNotStarted)
    {
        [self.trashButton setEnabled:YES];
    }
    else
    {
        [self.trashButton setEnabled:NO];
    }
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    
    
    if ([self.appointment getType] == AppointmentTypeFault || [self.appointment getType] == AppointmentTypePlanned  || [self.appointment getType] == AppointmentTypeMiscellaneous || [self.appointment getType] == AppointmentTypeAdaptations || [self.appointment getType] == AppointmentTypeCondition)
    {
        
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.noEntryButton, nil]];
        if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
            || [self.appointment getStatus] == AppointmentStatusNoEntry)
        {
            [self.noEntryButton setEnabled:NO];
        }
        else
        {
            [self.noEntryButton setEnabled:YES];
        }
        
    }
    else if ([self.appointment getType] == AppointmentTypeFault)
    {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.notesButton, self.noEntryButton, nil]];
        if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]]
            || [self.appointment getStatus] == AppointmentStatusNoEntry)
        {
            [self.noEntryButton setEnabled:NO];
        }
        else
        {
            [self.noEntryButton setEnabled:YES];
        }
    }
    else
    {
        [self setRightBarButtonItems:[NSArray arrayWithObjects:self.downloadButton, self.trashButton, nil]];
        
    }
    if(appointment.appointmentToProperty)
    {
        [self setTitle:[appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
    }
    else
    {
        [self setTitle:appointment.appointmentToScheme.schemeName];
    }
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickTrashButton
{
    CLS_LOG(@"onClickTrashButton");
    if([[ReachabilityManager sharedManager] isConnectedToInternet])
    {
        [self.activityIndicator stopAnimating];
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_DELETE_APPOINTMENT")
                                                       message:LOC(@"KEY_ALERT_CONFIRM_DELETE")
                                                      delegate:self
                                             cancelButtonTitle:LOC(@"KEY_ALERT_YES")
                                             otherButtonTitles:LOC(@"KEY_ALERT_NO"), nil];
        alert.tag = AlertViewTagTrash;
        [alert show];
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                       message:LOC(@"KEY_ALERT_NO_INTERNET")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
    }
}

- (void) onClickDownloadButton
{
    CLS_LOG(@"onClickDownloadButton");
    if([[ReachabilityManager sharedManager] isConnectedToInternet])
    {
        if(![self.appointment isDownloadInProgress]) {
            [self disableNavigationBarButtons];
            [self showActivityIndicator:LOC(@"KEY_STRING_SURVEY_DOWNLOADING")];
            [self.appointment prepareForOffline];
            [self popOrCloseViewController];
            [self.tableView reloadData];
        }
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                       message:LOC(@"KEY_ALERT_NO_INTERNET")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        [alert show];
    }
}

- (void) onClickEditButton
{
    CLS_LOG(@"onClickEditButton");
    self.isEditing = YES;
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, self.downloadButton, self.trashButton, nil]];
    [self.tableView reloadData];
}

- (void) onClickSaveButton
{
    self.isEditing = NO;
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.downloadButton, self.trashButton, nil]];
    [self.tableView reloadData];
}

- (void) onClickNoEntryButton
{
    CLS_LOG(@"onClickNoEntryButton");
    PSFaultNoEntryViewController *faultNoEntryViewConroller = [[PSFaultNoEntryViewController alloc] initWithNibName:@"PSFaultNoEntryViewController" bundle:[NSBundle mainBundle]];
    [faultNoEntryViewConroller setAppointment:self.appointment];
    [self.navigationController pushViewController:faultNoEntryViewConroller animated:YES];
}

- (void) onClickNotesButton
{
    CLS_LOG(@"onClickNotesButton");
    PSAppointmentNotesViewController *appointmentNotesViewConroller = [[PSAppointmentNotesViewController alloc] initWithNibName:@"PSAppointmentNotesViewController" bundle:[NSBundle mainBundle]];
    [appointmentNotesViewConroller setAppointment:self.appointment];
    [self.navigationController pushViewController:appointmentNotesViewConroller animated:YES];
}
#pragma mark - IBActions
- (IBAction)onClickStartAppoiontmentButton:(id)sender
{
    [self.appointment startAppointment];
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    CLS_LOG(@"Appointment ID: %@", self.appointment.appointmentId);
    if (alertView.tag == AlertViewTagTrash)
    {
       	if(buttonIndex == alertView.cancelButtonIndex)
        {
            //Delete Appointment
            [self showActivityIndicator:[NSString stringWithFormat:@"%@ %@...",
                                         @"Deleting",
                                         @"Appointment"]];
            [self disableNavigationBarButtons];
            [[PSAppointmentsManager sharedManager] deleteAppointment:self.appointment];
            [self schduleUIControlsActivation];
            CLS_LOG(@"Delete Appointment");
        }
    }
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return kNumberOfSections;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    /*
     The count check ensures that if array is empty, i.e no record exists
     for particular section, the header height is set to zero so that
     section header is not alloted any space.
     */
    
    CGFloat headerHeight = 0.0;
    
    if(section == kSectionJSNumber)
    {
        if ([self.jobDataListArray count]  || self.appointment.appointmentToPlannedComponent)
        {
            headerHeight = 1;
            
        }
        else
        {
            headerHeight = 0;
        }
    }
    else if (section == kSectionPlannedComponent )
    {
        if (self.appointment.appointmentToPlannedComponent) {
            
            headerHeight = kViewSectionHeaderHeight;
        }else
        {
            headerHeight=0.0;
        }
        
    }
    else if (section == kSectionAccomodation)
    {
        headerHeight = 1;
    }
    else if (section == kSectionAsbestos)
    {
        headerHeight = kViewSectionHeaderHeight;
    }
    
    else if (section == kSectionTenant)
    {
        if([self.tenantsArray count] > 0)
        {
            headerHeight = kViewSectionHeaderHeight;
        }
    }
    
    else if (section == kSectionNotes)
    {
        headerHeight = kViewSectionHeaderHeight;
    }
    else
    {
        headerHeight = kViewSectionHeaderHeight;
    }
    return headerHeight;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [self.headerViewArray objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0.0;
    if(indexPath.section == kSectionProperty)
    {
        if(self.appointment.appointmentToProperty)
        {
            rowHeight = kFaultPropertyCellHeight;//kPropertyCellHeight;
        }
        else
        {
            rowHeight = kFaultSchemeCellHeight;
        }
    }
    
    else if (indexPath.section == kSectionPlannedComponent && self.appointment.appointmentToPlannedComponent)
    {
        
        rowHeight = [PSPlannedComponentCell getHeight:self.appointment.appointmentToPlannedComponent];
        
    }
    else if (indexPath.section == kSectionType)
    {
        rowHeight = kTypeCellHeight;
        
    }
    
    else if (indexPath.section == kSectionTenant)
    {
        rowHeight = kTenantCellHeight;
    }
    
    else if (indexPath.section == kSectionAsbestos)
    {
        rowHeight = kAsbestosCellHeight;
    }
    
    else if (indexPath.section == kSectionJSNumber)
    {
        rowHeight = kJSNumberCellHeight;
    }
    
    else if (indexPath.section == kSectionAccomodation)
    {
        if ([self.appointment getType] != AppointmentTypeFault )
        {
            rowHeight = kAccomodationCellHeight;
        }
        
    }
    
    else if (indexPath.section == kSectionNotes)
    {
        if (!isEmpty(self.appointment.appointmentNotes))
        {
            rowHeight = [self heightForTextView:nil containingString:self.appointment.appointmentNotes];
            rowHeight += 25;
            
            if (rowHeight < 50)
            {
                rowHeight = 50;
            }
        }
        
        else
        {
            rowHeight = 50;
        }
        
        
    }
    
    return rowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSInteger numberOfRows=0;
    
    if (section == kSectionProperty)
    {
        numberOfRows = kSectionPropertyRows;
    }
    else if (section == kSectionPlannedComponent  && self.appointment.appointmentToPlannedComponent)
    {
        
        numberOfRows = kSectionPlannedComponentRows;
    }
    
    else if (section == kSectionType)
    {
        numberOfRows = kSectionTypeRows;
    }
    
    else if (section == kSectionTenant)
    {
        numberOfRows = [self.tenantsArray count];
    }
    
    else if (section == kSectionAsbestos)
    {
        if (!isEmpty(self.asbestosArray))
        {
            numberOfRows = [self.asbestosArray count];
        }
        else
        {
            numberOfRows = 1;
        }
    }
    
    else if (section == kSectionJSNumber)
    {
        
        if (self.appointment.appointmentToPlannedComponent) {
            numberOfRows=1;
            
        }else{
            numberOfRows = [self.jobDataListArray count];
        }
    }
    
    else if (section == kSectionAccomodation)
        
    {
        if ([self.appointment getType] == AppointmentTypeStock || [self.appointment getType] == AppointmentTypeGas)
        {
            numberOfRows = 1;
        }
    }
    
    else if (section == kSectionNotes)
    {
        numberOfRows = 1;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *_cell = nil;
    
    if (indexPath.section == kSectionProperty)
    {
        if(self.appointment.appointmentToProperty)
        {
            static NSString *propertyCellIdentifier = @"faultPropertyCellIdentifier";
            PSFaultAppointmentPropertyCell *cell = [self.tableView dequeueReusableCellWithIdentifier:propertyCellIdentifier];
            
            if(cell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"PSFaultAppointmentPropertyCell" owner:self options:nil];
                cell = self.faultPropertyCell;
                self.propertyCell = nil;
            }
            [self configureCell:&cell atIndexPath:indexPath];
            _cell = cell;
        }
        else
        {
            static NSString *schemeCellIdentifier = @"faultSchemeCellIdentifier";
            PSFaultAppointmentSchemeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:schemeCellIdentifier];
            
            if(cell == nil)
            {
                [[NSBundle mainBundle] loadNibNamed:@"PSFaultAppointmentSchemeCell" owner:self options:nil];
                cell = self.faultSchemeCell;
                self.faultSchemeCell = nil;
            }
            [self configureCell:&cell atIndexPath:indexPath];
            _cell = cell;
        }
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    else if (indexPath.section == kSectionPlannedComponent)
    {
        static NSString *plannedCellIdentifier = @"TypePlannedComponentIdentifier";
        PSPlannedComponentCell *cell = [self.tableView dequeueReusableCellWithIdentifier:plannedCellIdentifier];
        if(cell == nil)
        {
            
            cell = [[PSPlannedComponentCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:plannedCellIdentifier];
        }
        
        [cell reloadData:self.appointment.appointmentToPlannedComponent];
        
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
    }
    
    else if (indexPath.section == kSectionType)
    {
        static NSString *typeCellIdentifier = @"TypeCellIdentifier";
        PSTypeCell *cell = [self.tableView dequeueReusableCellWithIdentifier:typeCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSTypeCell" owner:self options:nil];
            cell = self.typeCell;
            self.typeCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    else if (indexPath.section == kSectionTenant)
    {
        static NSString *tenantCellIdentifier = @"TenantCellIdentifier";
        PSTenantCell *cell = (PSTenantCell *)[self.tableView dequeueReusableCellWithIdentifier:tenantCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSTenantCell" owner:self options:nil];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell = self.tenantCell;
            self.tenantCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    
    else if (indexPath.section == kSectionAsbestos)
    {
        static NSString *asbestosCellIdentifier = @"AsbestosCellIdentifier";
        PSAsbestosCell *cell = [self.tableView dequeueReusableCellWithIdentifier:asbestosCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAsbestosCell" owner:self options:nil];
            cell = self.asbestosCell;
            self.asbestosCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    else if (indexPath.section == kSectionJSNumber)
    {
        static NSString *JSNumberCellIdentifier = @"JSNumberCellIdentifier";
        PSJSNumberCell *cell = [self.tableView dequeueReusableCellWithIdentifier:JSNumberCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSJSNumberCell" owner:self options:nil];
            cell = self.jsNumberCell;
            self.jsNumberCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    else if (indexPath.section == kSectionAccomodation)
    {
        static NSString *accomodationCellIdentifier = @"AccomodationCellIdentifier";
        PSAccomodationsCell *cell = [self.tableView dequeueReusableCellWithIdentifier:accomodationCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAccomodationsCell" owner:self options:nil];
            cell = self.accomodationCell;
            self.accomodationCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleGray;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    }
    
    else if (indexPath.section == kSectionNotes)
    {
        static NSString *notesCellIdentifier = @"notesCellIdentifier";
        PSAppointmentDetailNotesCell *cell = [self.tableView dequeueReusableCellWithIdentifier:notesCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSAppointmentDetailNotesCell" owner:self options:nil];
            cell = self.notesCell;
            self.notesCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        _cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        
    }
    return _cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSFaultAppointmentPropertyCell class]])
    {
        
        PSFaultAppointmentPropertyCell *_cell = (PSFaultAppointmentPropertyCell *) *cell;
        
        Customer *customer = [self.appointment defaultTenant];
        
        if (!isEmpty(customer))
        {
            _cell.lblName.text = [customer fullName];
        }
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickAddress:)];
        UITapGestureRecognizer* imageGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickPropertyImage:)];
        // if labelView is not set userInteractionEnabled, you must do so
        [_cell.lblAddress setUserInteractionEnabled:YES];
        [_cell.lblAddress addGestureRecognizer:gesture];
        [_cell.imgProperty setUserInteractionEnabled:YES];
        [_cell.imgProperty addGestureRecognizer:imageGesture];
        [_cell setAppointment:self.appointment];
        [_cell reloadData:self.appointment];
        *cell = _cell;
    }
    else if ([*cell isKindOfClass:[PSFaultAppointmentSchemeCell class]])
    {
        
        PSFaultAppointmentSchemeCell *_cell = (PSFaultAppointmentSchemeCell *) *cell;
        
        UITapGestureRecognizer* gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickAddress:)];
        UITapGestureRecognizer* gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickAddress:)];
        
        [_cell.lblBlockName setUserInteractionEnabled:YES];
        [_cell.lblBlockName addGestureRecognizer:gesture1];
        [_cell.lblSchemeName setUserInteractionEnabled:YES];
        [_cell.lblSchemeName addGestureRecognizer:gesture2];
        [_cell setAppointment:self.appointment];
        [_cell reloadData:self.appointment];
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSTypeCell class]])
    {
        PSTypeCell *_cell = (PSTypeCell *) *cell;
        _cell.lblDetail.text = appointment.appointmentType;
        _cell.lblDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblDetail.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        if ([self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]] && [appointment getType] != AppointmentTypeGas)
        {
            [_cell.imgStatus setHidden:NO];
        }
        else
        {
            [_cell.imgStatus setHidden:YES];
        }
        if ([self.appointment.appointmentType isEqualToString:kAppointmentTypeStock])
        {
            NSMutableDictionary *surveyJSON = [self.appointment.appointmentToSurvey.surveyJSON JSONValue];
            if(!isEmpty(surveyJSON) && [self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]] && [appointment getStatus] != AppointmentStatusNotStarted)
            {
                [_cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            
            else
            {
                [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            }
        }
        
        else if ([self.appointment.appointmentType isEqualToString:kAppointmentTypeGas])
        {
            if (([appointment isPreparedForOffline] && [appointment getStatus] != AppointmentStatusNotStarted) || [self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]] || [self.appointment getStatus] == AppointmentStatusNoEntry)
            {
                [_cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            }
            else
            {
                [_cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            
        }
        
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSTenantCell class]])
    {
        PSTenantCell *_cell = (PSTenantCell *) *cell;
        
        self.customer= [self.tenantsArray objectAtIndex:indexPath.row];
        
        NSString *customerName = [self.customer fullName];
        
        if(!isEmpty(self.customer.telephone)){
            NSMutableString *telephone = [NSMutableString stringWithFormat:@"   %@",LOC(@"KEY_STRING_TEL")];
            [telephone appendString:@". "];
            [telephone appendString:self.customer.telephone];
            _cell.btnTelephone.hidden = NO;
            [_cell.btnTelephone setTitle:telephone forState:UIControlStateNormal];
            [_cell.btnTelephone setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateNormal];
            [_cell.btnTelephone setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateHighlighted];
            _cell.btnTelephone.tag = indexPath.row;
            [_cell.btnTelephone setFrame:CGRectMake(12, 37, 240, 24)];
            [_cell.btnMobile setFrame:CGRectMake(12, 65, 240, 24)];
        }
        
        else
        {
            [_cell.lblMobile setFrame:_cell.lblTelephone.frame];
            [_cell.btnMobile setFrame:_cell.btnTelephone.frame];
        }
        
        if (!isEmpty(self.customer.mobile) && ![self.customer.mobile isEqualToString:@"(null)"]) {
            NSMutableString *mobile = [NSMutableString stringWithFormat:@"   %@",LOC(@"KEY_STRING_MOB")];
            [mobile appendString:@". "];
            [mobile appendString:self.customer.mobile];
            _cell.lblMobile.text = mobile;
            _cell.lblMobile.hidden = NO;
            _cell.btnMobile.hidden = NO;
            [_cell.btnMobile setTitle:mobile forState:UIControlStateNormal];
            [_cell.btnMobile setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateNormal];
            [_cell.btnMobile setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateHighlighted];
            _cell.btnMobile.tag = indexPath.row;
        }
        
        else {
            _cell.lblMobile.hidden = YES;
            _cell.btnMobile.hidden = YES;
        }
        _cell.lblTenantName.text = customerName;
        _cell.lblTenantName.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
        _cell.lblMobile.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblTelephone.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblTenantName.highlightedTextColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
        _cell.lblMobile.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.lblTelephone.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        [_cell.btnEditTenant addTarget:self action:@selector(onClickEditTenantButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnTelephone addTarget:self action:@selector(onClickTelephoneButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnMobile addTarget:self action:@selector(onClickMobileButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnDisclosureIndicator addTarget:self action:@selector(onClickDisclosureButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnEditTenant setTag:indexPath.row];
        [_cell.btnDisclosureIndicator setTag:indexPath.row];
        
        if (self.isEditing)
        {
            [_cell.btnEditTenant setHidden:NO];
        }
        
        else
        {
            [_cell.btnEditTenant setHidden:YES];
        }
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSAsbestosCell class]])
    {
        PSAsbestosCell *_cell = (PSAsbestosCell *) *cell;
        _cell.lblAsbestos.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.lblAsbestos.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.lblAsbestosLocation.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.lblAsbestosLocation.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        if ([self.asbestosArray count] > 0)
        {
            PropertyAsbestosData *asbestosData = [self.asbestosArray objectAtIndex:indexPath.row];
            if (!isEmpty(asbestosData))
            {
                _cell.lblAsbestos.text = asbestosData.asbRiskLevelDesc;
                _cell.lblAsbestosLocation.text = asbestosData.riskDesc;
            }
        }
        else
        {
            _cell.lblAsbestos.text = LOC(@"KEY_STRING_NONE");
        }
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSJSNumberCell class]])
    {
        PSJSNumberCell *_cell = (PSJSNumberCell *) *cell;
        
        NSString * jobStatus=nil;
        if (self.appointment.appointmentToPlannedComponent) {
            
            jobStatus=self.appointment.appointmentToPlannedComponent.jobStatus;
            _cell.lblFaultDetail.text = self.appointment.appointmentToPlannedComponent.jsNumber;
            _cell.lblClickHereJobSheet.text = LOC(@"KEY_STRING_CLICK_HERE_FOR_JOBSHEET");
            _cell.lblFaultDetail.center = _cell.center;
            _cell.lblClickHereJobSheet.center = _cell.center;
            
            CGRect labelFrame = _cell.lblFaultDetail.frame;
            labelFrame.origin.x = 12;
            [_cell.lblFaultDetail setFrame:labelFrame];
            labelFrame = _cell.lblClickHereJobSheet.frame;
            labelFrame.origin.x = CGRectGetMinX([_cell.imgStatus frame]) - CGRectGetWidth([_cell.lblClickHereJobSheet frame]);
            
            labelFrame.origin.y = (CGRectGetMinY([_cell.imgStatus frame]) + (CGRectGetHeight([_cell.imgStatus frame])/2)) - CGRectGetHeight([_cell.lblClickHereJobSheet frame])/2;
            
            [_cell.lblClickHereJobSheet setFrame:labelFrame];
        }
        else
        {
            JobDataList *jobData = [self.jobDataListArray objectAtIndex:indexPath.row];
            jobStatus = jobData.jobStatus;
            _cell.lblTitle.text = jobData.jsNumber;
            _cell.lblClickHereJobSheet.text = LOC(@"KEY_STRING_CLICK_HERE_FOR_JOBSHEET");
            [_cell.lblFaultDetail setHidden:NO];
            _cell.lblFaultDetail.text = jobData.jsnDescription;
        }
        
        if ([jobStatus isEqualToString:kJobStatusPaused])
        {
            [_cell.imgStatus setHidden:NO];
            _cell.imgStatus.image = [UIImage imageNamed:@"btn_Pause"];
        }
        else if ([jobStatus isEqualToString:kJobStatusComplete] || [jobStatus isEqualToString:kJobStatusNoEntry])
        {
            [_cell.imgStatus setHidden:NO];
            _cell.imgStatus.image = [UIImage imageNamed:@"icon_completed"];
        }
        else if ([jobStatus isEqualToString:kJobStatusInProgress])
        {
            [_cell.imgStatus setHidden:NO];
            _cell.imgStatus.image = [UIImage imageNamed:@"btn_Play"];
        }
        else if ([jobStatus isEqualToString:kJobStatusNotStarted])
        {
            [_cell.imgStatus setHidden:YES];
        }
        
        if([self.appointment getStatus] == AppointmentStatusInProgress )
        {
            [_cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        
        else
        {
            [_cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        //        if (indexPath.row == [self.jobDataListArray count] - 1)
        //        {
        //            [_cell.imgSeperator setHidden:YES];
        //        }
        //        else
        //        {
        //            [_cell.imgSeperator setHidden:NO];
        //        }
        
        
        _cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT );
        _cell.lblClickHereJobSheet.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT );
        _cell.lblFaultDetail.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.lblTitle.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.lblClickHereJobSheet.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
        UIView* backgroundView = [ [ UIView alloc ] initWithFrame:CGRectZero ];
        backgroundView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);//UIColorFromRGB(240, 241, 241);
        
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSAccomodationsCell class]])
    {
        PSAccomodationsCell *_cell = (PSAccomodationsCell *) *cell;
        _cell.lblAccommodation.text = LOC(@"KEY_STRING_ACCOMMODATIONS");
        _cell.lblAccommodation.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        _cell.lblAccommodation.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        NSMutableArray *accomodations = [NSMutableArray arrayWithArray:[self.appointment.appointmentToProperty.propertyToAccomodation allObjects]];
        
        if (!isEmpty(accomodations))
        {
            _cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        else
        {
            _cell.accessoryType = UITableViewCellAccessoryNone;
        }
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSAppointmentDetailNotesCell class]])
    {
        PSAppointmentDetailNotesCell *_cell = (PSAppointmentDetailNotesCell *) *cell;
        _cell.txtViewNotes.textAlignment = NSTextAlignmentJustified;
        _cell.txtViewNotes.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
        [_cell.txtViewNotes setScrollEnabled:NO];
        
        if (!isEmpty(self.appointment.appointmentNotes)) {
            _cell.txtViewNotes.text = self.appointment.appointmentNotes;
        }
        else
        {
            _cell.txtViewNotes.text = LOC(@"KEY_STRING_NONE");
        }
        
        *cell = _cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == kSectionType)
    {
        
        NSMutableDictionary *surveyJSON = [self.appointment.appointmentToSurvey.surveyJSON JSONValue];
        
        if ([self.appointment.appointmentType isEqualToString:kAppointmentTypeGas])
        {
            if ((([appointment isPreparedForOffline] && [appointment getStatus] != AppointmentStatusNotStarted))|| [self.appointment getStatus] == [UtilityClass completionStatusForAppointmentType:[self.appointment getType]] || [self.appointment getStatus] == AppointmentStatusNoEntry)
            {
                PSCP12InfoViewController *cp12InfoViewController = [[PSCP12InfoViewController alloc] initWithNibName:@"PSCP12InfoViewController" bundle:[NSBundle mainBundle]];
                [cp12InfoViewController setAppointment:self.appointment];
                [self.navigationController pushViewController:cp12InfoViewController animated:YES];
            }
            else
            {
                if ([appointment isPreparedForOffline]  && [self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
                {
                    
                    
                    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                                   message:LOC(@"KEY_STRING_APPOINTMENT_NOT_STARTED")
                                                                  delegate:nil
                                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                         otherButtonTitles:nil,nil];
                    [alert show];
                }
                else
                {
                    if ([self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
                    {
                        
                        
                        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                                       message:LOC(@"KEY_STRING_SURVEY_NOT_DOWNLOADED")
                                                                      delegate:nil
                                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                             otherButtonTitles:nil,nil];
                        [alert show];
                        
                    }
                }
            }
        }
        
        else if([self.appointment getType] == AppointmentTypeStock)
        {
            if ([self.appointment getStatus] != [UtilityClass completionStatusForAppointmentType:[self.appointment getType]])
            {
                if (!isEmpty(surveyJSON) && [appointment getStatus] != AppointmentStatusNotStarted)
                {
                    PSSurveyViewController *surveyViewConroller = [[PSSurveyViewController alloc] initWithTitle:nil surveyData:surveyJSON];
                    [surveyViewConroller setMainSurveyMenu:YES];
                    [surveyViewConroller setAppointment:self.appointment];
                    [surveyViewConroller setMainViewController:surveyViewConroller];
                    [self.navigationController pushViewController:surveyViewConroller animated:YES];
                }
                else
                {
                    if (!isEmpty(surveyJSON))
                    {
                        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                                       message:LOC(@"KEY_READ_ONLY_JOB_SHEET_DENIED")
                                                                      delegate:nil
                                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                             otherButtonTitles:nil,nil];
                        [alert show];

                    }
                    else
                    {
                        
                        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                                       message:LOC(@"KEY_STRING_SURVEY_NOT_DOWNLOADED")
                                                                      delegate:nil
                                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                             otherButtonTitles:nil,nil];
                        [alert show];
                    }
                }
            }
        }
    }
    else if (indexPath.section == kSectionTenant)
    {
        //Do Nothing
    }
    
    else if (indexPath.section == kSectionJSNumber)
    {
        if([self.appointment getStatus] != AppointmentStatusNotStarted)
        {
            NSArray *jobAsbestosArray = [self.appointment.appointmentToProperty.propertyToPropertyAsbestosData allObjects];
            
            if (self.appointment.appointmentToPlannedComponent) {
                
                if ([self.appointment.appointmentType isEqualToString:kAppointmentTypeAdaptations] ) {
                    
                    PSAdaptationJobDetailViewController *jobDetailViewController = [[PSAdaptationJobDetailViewController alloc] init];
                    [jobDetailViewController setPlannedComponent:self.appointment.appointmentToPlannedComponent];
                    
                    [jobDetailViewController setJobAsbestosArray:jobAsbestosArray];
                    [self.navigationController pushViewController:jobDetailViewController animated:YES];
                }
                else if ([self.appointment.appointmentType isEqualToString:kAppointmentTypeCondition] ) {
                    
                    PSConditionJobDetailViewController *jobDetailDetailViewConroller = [[PSConditionJobDetailViewController alloc] init];
                    [jobDetailDetailViewConroller setPlannedComponent:self.appointment.appointmentToPlannedComponent];
                    [jobDetailDetailViewConroller setJobAsbestosArray:jobAsbestosArray];
                    [self.navigationController pushViewController:jobDetailDetailViewConroller animated:YES];
                }
                else
                {
                    PSPlannedJobDetailViewController *jobDetailDetailViewConroller = [[PSPlannedJobDetailViewController alloc] init];
                    [jobDetailDetailViewConroller setPlannedComponent:self.appointment.appointmentToPlannedComponent];
                    [jobDetailDetailViewConroller setJobAsbestosArray:jobAsbestosArray];
                    [self.navigationController pushViewController:jobDetailDetailViewConroller animated:YES];
                    
                }
                
                
            }else
            {
                // Reactive appointment
                PSJobDetailViewController *jobDetailDetailViewConroller = [[PSJobDetailViewController alloc] initWithNibName:@"PSJobDetailViewController" bundle:[NSBundle mainBundle]];
                [jobDetailDetailViewConroller setJobData:[self.jobDataListArray objectAtIndex:indexPath.row]];
                [jobDetailDetailViewConroller setJobAsbestosArray:jobAsbestosArray];
                [self.navigationController pushViewController:jobDetailDetailViewConroller animated:YES];
            }
        }
    }
    
    else if (indexPath.section == kSectionAccomodation){
        NSMutableArray *accomodations = [NSMutableArray arrayWithArray:[self.appointment.appointmentToProperty.propertyToAccomodation allObjects]];
        
        if (!isEmpty(accomodations))
        {
            [self setAccomodationlViewConroller:[[PSAccomodationViewController alloc] initWithNibName:@"PSAccomodationViewController" bundle:[NSBundle mainBundle]]];
            [self.accomodationlViewConroller setEditing:self.isEditing];
            [self.accomodationlViewConroller setAppointment:self.appointment];
            [self.navigationController pushViewController:self.accomodationlViewConroller animated:YES];
        }
    }
}

#pragma mark - Edit Tenant Button Selector
- (IBAction)onClickEditTenantButton:(id)sender
{
    CLS_LOG(@"onClickEditAddressButton");
    
    UIButton *btnEditTenant = (UIButton *) sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnEditTenant.tag];
    NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
    if (customer != nil) {
        PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:@"PSTenantDetailViewController" bundle:[NSBundle mainBundle]];
        [tenantDetailViewConroller setAddress:address];
        [tenantDetailViewConroller setTenant:customer];
        [tenantDetailViewConroller setEditing:YES];
        [self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
    }
}

- (IBAction)onClickDisclosureButton:(id)sender
{
    UIButton *btnDisclosure = (UIButton *) sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnDisclosure.tag];
    NSString *address = [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull];
    if (customer != nil)
    {
        PSTenantDetailViewController *tenantDetailViewConroller = [[PSTenantDetailViewController alloc] initWithNibName:@"PSTenantDetailViewController" bundle:[NSBundle mainBundle]];
        [tenantDetailViewConroller setAddress:address];
        [tenantDetailViewConroller setTenant:customer];
        [self.navigationController pushViewController:tenantDetailViewConroller animated:YES];
    }
}

- (IBAction)onClickTelephoneButton:(id)sender
{
    UIButton *btnTel = (UIButton *)sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnTel.tag];
    [self setupDirectCallToNumber:customer.telephone];
}

- (IBAction)onClickMobileButton:(id)sender
{
    // [self setupDirectCallToNumber:self.customer.mobile];
    UIButton *btnMob = (UIButton *)sender;
    Customer *customer = [self.tenantsArray objectAtIndex:btnMob.tag];
    [self setupDirectCallToNumber:customer.mobile];
}

- (void) setupDirectCallToNumber:(NSString *)number
{
    if (!isEmpty(number))
    {
        number = [UtilityClass getValidNumber:number];
        CLS_LOG(@"Direct Call Number Verified: %@", number);
        NSString *url = [@"tel:" stringByAppendingString:number];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

#pragma mark - Core Data Update Events
- (void) onAppointmentObjectUpdate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if([self isViewLoaded] && self.view.window)
        {
            [self.tableView reloadData];
        }
    });
}

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentStartNotificationReceive) name:kAppointmentStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSurveyFormSaveSuccessNotificationReceive) name:kSurveyFormSaveSucessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSurveyFormSaveFailureNotificationReceive) name:kSurveyFormSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentDataRemovalSuccessNotificationReceive) name:kAppointmentsDataRemoveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppointmentDataRemovalFailureNotificationReceive) name:kAppointmentsDataRemoveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppliancesSaveSuccessNotificationReceive) name:kSaveAppliacnesSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppliancesSaveFailureNotificationReceive) name:kSaveAppliacnesFailureNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPropertyAppliancesDataDownloadNotificationReceive) name:kFetchPropertyAppliancesDataDownloadedNotification object:nil];
    
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentStartNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSurveyFormSaveSucessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSurveyFormSaveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsDataRemoveSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentsDataRemoveFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveAppliacnesSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSaveAppliacnesFailureNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFetchPropertyAppliancesDataDownloadedNotification object:nil];
}

- (void) onAppointmentStartNotificationReceive
{
    [self.trashButton setEnabled:NO];
    [self performSelector:@selector(hideTableHeaderView) withObject:nil afterDelay:0.1];
}

- (void) onSurveyFormSaveSuccessNotificationReceive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
}

- (void) onSurveyFormSaveFailureNotificationReceive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self hideActivityIndicator];
    [self enableNavigationBarButtons];
}

- (void) onAppointmentDataRemovalSuccessNotificationReceive
{
    [self enableNavigationBarButtons];
    [[PSDataUpdateManager sharedManager] deleteAppointment:self.appointment];
    [[PSDatabaseContext sharedContext] saveContext];
    [self hideActivityIndicator];
    [self popOrCloseViewController];
}

- (void)popOrCloseViewController
{
    self.mainAppViewController.appointmentDetailViewConroller = nil;
    [super popOrCloseViewController];
}

- (void) onAppointmentDataRemovalFailureNotificationReceive
{
    //KEY_ALERT_DELETE_APPOINTMENT_FAILIURE
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_DELETE_APPOINTMENT")
                                                   message:LOC(@"KEY_ALERT_DELETE_APPOINTMENT_FAILIURE")
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
    [self enableNavigationBarButtons];
    [self hideActivityIndicator];
}
- (void) onAppliancesSaveSuccessNotificationReceive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self enableNavigationBarButtons];
    [self hideActivityIndicator];
}


- (void) onAppliancesSaveFailureNotificationReceive
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self enableNavigationBarButtons];
    [self hideActivityIndicator];
}

- (void) onPropertyAppliancesDataDownloadNotificationReceive
{
    [self.appointment setIsDownloadInProgress:NO];
}


#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
    float horizontalPadding = 0;
    float verticalPadding = 8;
    
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                //[NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
                                [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17],NSFontAttributeName,
                                nil];
    CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(300, 999999.0f)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:attributes
                                               context:nil];
    CGFloat height = boundingRect.size.height + verticalPadding;
    
#else
    CGFloat height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17]
                        constrainedToSize:CGSizeMake(300, 999999.0f)
                            lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
#endif
    
    return height;
}

#pragma mark - Methods

- (void) disableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = NO;
    }
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = NO;
    }
}

- (void) enableNavigationBarButtons
{
    NSArray *navigationBarRightButtons = self.navigationItem.rightBarButtonItems;
    for (PSBarButtonItem *button in navigationBarRightButtons) {
        button.enabled = YES;
    }
    
    NSArray *navigationBarLeftButtons = self.navigationItem.leftBarButtonItems;
    for (PSBarButtonItem *button in navigationBarLeftButtons)
    {
        button.enabled = YES;
    }
}

- (void) addTableHeaderView
{
    NSString *btnTitle = @"";
    if ([self.appointment getType] == AppointmentTypeFault || [self.appointment getType] == AppointmentTypePlanned || [self.appointment getType] == AppointmentTypeMiscellaneous || [self.appointment getType] == AppointmentTypeAdaptations)
    {
        btnTitle = LOC(@"KEY_STRING_ACCEPT");
    }
    else
    {
        btnTitle = LOC(@"KEY_STRING_START");
    }
    CGRect headerViewRect = CGRectMake(0, 0, 320, 40);
    UIView *headerView = [[UIView alloc] initWithFrame:headerViewRect];
    headerView.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.frame = CGRectMake(8, 0, 245, 29);
    lbl.center = CGPointMake(lbl.center.x, headerView.center.y);
    lbl.backgroundColor = [UIColor clearColor];
    [lbl setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lbl setHighlightedTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
    [lbl setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
    lbl.text =LOC(@"KEY_STRING_APPOINTMENT_START_MESSAGE");
    
    UIButton *btnStartAppointment = [[UIButton alloc]init];
    btnStartAppointment.frame = CGRectMake(257, 0, 53, 29);
    btnStartAppointment.center = CGPointMake(btnStartAppointment.center.x, headerView.center.y);
    [btnStartAppointment setTitle:btnTitle forState:UIControlStateNormal];
    [btnStartAppointment.titleLabel setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
    [btnStartAppointment setBackgroundImage:[UIImage imageNamed:@"btn_view"] forState:
     UIControlStateNormal];
    [btnStartAppointment addTarget:self action:@selector(onClickStartAppoiontmentButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:lbl];
    [headerView addSubview:btnStartAppointment];
    
    if ([self.appointment.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame)
    {
        headerViewRect = self.tableView.tableHeaderView.frame;
        headerViewRect.size.height = 40;
        headerViewRect.size.width = 320;
        [self.tableView.tableHeaderView setFrame:headerViewRect];
        [self.tableView setTableHeaderView:headerView];
    }
}

- (void) hideTableHeaderView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.5];
    [self.tableView.tableHeaderView setFrame:CGRectMake(0, 0, 0, 0)];
    self.tableView.tableHeaderView = nil;
    [self.tableView.tableHeaderView setHidden:YES];
    //[self.tableView reloadData];
    [UIView commitAnimations];
}

- (IBAction)onClickAddress:(UIGestureRecognizer *)sender
{
    // Code to respond to gesture here
    PSMapsViewController *mapViewController = [[PSMapsViewController alloc] initWithNibName:@"PSMapsViewController" bundle:[NSBundle mainBundle]];
    if(self.appointment.appointmentToProperty)
    {
        [mapViewController setAddress:[self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull]];
        [mapViewController setNavigationTitle:[self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort]];
    }
    else
    {
        [mapViewController setAddress:[self.appointment.appointmentToScheme fullAddress]];
        [mapViewController setNavigationTitle:[self.appointment.appointmentToScheme shortAddress]];
    }
    [self.navigationController pushViewController:mapViewController animated:YES];
    
}

- (IBAction)onClickPropertyImage:(UIGestureRecognizer *)sender
{
    // Code to respond to gesture here
    PSImageViewController *imageViewController = [[PSImageViewController alloc] initWithNibName:@"PSImageViewController" bundle:[NSBundle mainBundle]];
    
    imageViewController.propertyObj=self.appointment.appointmentToProperty;
    
    [self.navigationController pushViewController:imageViewController animated:YES];
    
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//
@end
