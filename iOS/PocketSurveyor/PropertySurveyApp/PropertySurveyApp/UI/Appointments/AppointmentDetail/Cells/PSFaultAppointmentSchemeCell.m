//
//  PSAppointmentSchemeDetailViewController.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 26/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSFaultAppointmentSchemeCell.h"
#import "Scheme+JSON.h"

@implementation PSFaultAppointmentSchemeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    
    if (self) {
        
           }
    
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void) reloadData:(Appointment*)appointment
{
    self.lblSchemeName.text = appointment.appointmentToScheme.schemeName;
    self.lblBlockName.text = isEmpty(appointment.appointmentToScheme.blockName)?@"N/A":appointment.appointmentToScheme.blockName;
    self.lblReported.text = [appointment stringWithAppointmentStatus:[appointment getStatus]];
    self.lblSurveyor.text = [NSString stringWithFormat:@"%@: %@",
                             LOC(@"KEY_STRING_ASSIGNED_BY"),
                             appointment.createdByPerson];
    self.lblStartDate.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle9];
    self.lblEndDate.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle9];
    self.lblStartTime.text = [UtilityClass stringFromDate:appointment.appointmentStartTime dateFormat:kDateTimeStyle3];
    self.lblEndTime.text = [UtilityClass stringFromDate:appointment.appointmentEndTime dateFormat:kDateTimeStyle3];
       
    self.lblBlockName.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
    self.lblSchemeName.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
    self.lblReportedDate.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblReported.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblStarts.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.lblEnds.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT);
    self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
}

@end
