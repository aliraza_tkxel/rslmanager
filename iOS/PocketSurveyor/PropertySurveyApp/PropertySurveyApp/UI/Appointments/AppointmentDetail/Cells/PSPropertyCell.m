//
//  PropertyCell.m
//  PropertySurveyApp
//
//  Created by My Mac on 19/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSPropertyCell.h"
#import "PropertyPicture+MWPhoto.h"

@implementation PSPropertyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
        // Initialization code
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
    }
 
    return self;
}


-(void)onPictureDefault:(NSNotification*) notification
{
    PropertyPicture * picture=notification.object;
    
    [self.imgProperty setImageWithURL:[NSURL URLWithString:picture.imagePath] andAddBorder:YES];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) reloadData:(Property*)property
{
    if(!isEmpty(property.certificateExpiry))
    {
        self.lblExpiryDate.text = [UtilityClass stringFromDate:property.certificateExpiry dateFormat:kDateTimeStyle8];
    }
    
    self.lblAddress.text = [property addressWithStyle:PropertyAddressStyle1];
    
    //self.imgProperty.thumbnail=YES;
    
    UIImage * image=[property.defaultPicture underlyingImage];
    if (image) {
        
        [self.imgProperty setImage: image];
        
    }else
    {
        [self.imgProperty setImageWithURL:[NSURL URLWithString:property.defaultPicture.imagePath] andAddBorder:YES];
    }

    
    self.lblName.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
    self.lblAddress.textColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT );
    self.lblExpiryDate.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
    self.lblName.highlightedTextColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR);
    self.lblAddress.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_LIGHT );
    self.lblExpiryDate.highlightedTextColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
    self.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
    
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
