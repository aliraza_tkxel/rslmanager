//
//  PSAppointmentSchemeDetailViewController.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 26/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSFaultAppointmentSchemeCell: UITableViewCell
@property (strong, nonatomic) IBOutlet FBRImageView *imgProperty;
@property (strong, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (strong, nonatomic) IBOutlet UILabel *lblBlockName;
@property (strong, nonatomic) IBOutlet UILabel *lblSurveyor;
@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStarts;
@property (strong, nonatomic) IBOutlet UILabel *lblEnds;
@property (strong, nonatomic) IBOutlet UILabel *lblReported;
@property (strong, nonatomic) IBOutlet UILabel *lblReportedDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEndTime;

@property (weak,   nonatomic) Appointment *appointment;
-(void) reloadData:(Appointment*)appointment;
@end
