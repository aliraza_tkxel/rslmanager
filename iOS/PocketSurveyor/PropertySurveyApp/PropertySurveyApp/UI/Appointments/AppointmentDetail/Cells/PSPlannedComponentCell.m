//
//  PSPlannedComponentCell.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSPlannedComponentCell.h"

@interface PSPlannedComponentCell ()


@end

@implementation PSPlannedComponentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        [self setComponentTitleLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        self.componentTitleLabel.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        self.componentTitleLabel.font=[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12];
        [self.componentTitleLabel setBackgroundColor:CLEAR_COLOR];

        
        [self setComponentValueLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        self.componentValueLabel.textColor=UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        self.componentValueLabel.font=[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12];
        [self.componentValueLabel setBackgroundColor:CLEAR_COLOR];

        
        [self setPmoTitleNumber:[[UILabel alloc] initWithFrame:CGRectZero]];
        [self.pmoTitleNumber setNumberOfLines:0];
        self.pmoTitleNumber.textColor=UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        self.pmoTitleNumber.font=[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12];
        [self.pmoTitleNumber setBackgroundColor:CLEAR_COLOR];

        
        [self setPmoNumber:[[UILabel alloc] initWithFrame:CGRectZero]];
        [self.pmoNumber setNumberOfLines:0];
        self.pmoNumber.font=[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12];
        self.pmoNumber.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK );
        [self.pmoNumber setBackgroundColor:CLEAR_COLOR];

        
        [self.contentView addSubview:self.componentTitleLabel];
        [self.contentView addSubview:self.componentValueLabel];
        
        [self.contentView addSubview:self.pmoTitleNumber];
        [self.contentView addSubview:self.pmoNumber];
        
        [self setBackgroundColor:CLEAR_COLOR];
        [self setSelectionStyle:(UITableViewCellSelectionStyleNone)];
    }
    
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    
    [self.componentTitleLabel setFrame:CGRectMake(10, 10, 100, 15)];
    
    [self.componentValueLabel setFrame:CGRectMake(CGRectGetMaxX(self.componentTitleLabel.frame) +5, CGRectGetMinY(self.componentTitleLabel.frame) ,200, CGRectGetHeight(self.componentTitleLabel.frame))];
    
    
    [self.pmoTitleNumber setFrame:CGRectMake(CGRectGetMinX(self.componentTitleLabel.frame), CGRectGetMaxY(self.componentValueLabel.frame)+2, CGRectGetWidth(self.componentTitleLabel.frame), CGRectGetHeight(self.componentTitleLabel.frame)*2)];
    
    [self.pmoNumber setFrame:CGRectMake(CGRectGetMaxX(self.pmoTitleNumber.frame) +5,CGRectGetMinY(self.pmoTitleNumber.frame), 200, CGRectGetHeight(self.componentTitleLabel.frame)*2)];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) reloadData:(PlannedTradeComponent*) plannedComponent
{

    //by Waqas
    //[self.componentTitleLabel setText:LOC(@"Component")];
    [self.componentTitleLabel setText:plannedComponent.pmoDescription];
    
    if([plannedComponent.tradeComponentToAppointment getType] == AppointmentTypeMiscellaneous) {
        [self.pmoTitleNumber setText:LOC(@"MWO")];
        [self.componentValueLabel setText:plannedComponent.tradeDescription];
        [self.pmoNumber setText:plannedComponent.pmoDescription];

    }
    else if([plannedComponent.tradeComponentToAppointment getType] == AppointmentTypeAdaptations) {
        [self.pmoTitleNumber setText:plannedComponent.pmoDescription];
        [self.pmoNumber setText:plannedComponent.location];

        [self.componentTitleLabel setText:plannedComponent.jsNumber];
        [self.componentValueLabel setText:plannedComponent.adaptation];        
    }
    else {
        
        //by Waqas temp
        //[self.pmoTitleNumber setText:LOC(@"PMO")];
        [self.pmoTitleNumber setText:plannedComponent.jsNumber];
        [self.componentValueLabel setText:plannedComponent.componentName];
        //[self.pmoNumber setText:plannedComponent.pmoDescription];
        NSString *text = plannedComponent.jsnNotes;
        text = [text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        [self.pmoNumber setText:LOC(text)];
    }

    
}

+(CGFloat) getHeight:(PlannedTradeComponent*) plannedComponent
{
    return 60;
}
@end
