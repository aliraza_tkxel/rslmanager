//
//  PSFaultAppointmentPropertyCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSFaultAppointmentPropertyCell : UITableViewCell
@property (strong, nonatomic) IBOutlet FBRImageView *imgProperty;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblSurveyor;
@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStarts;
@property (strong, nonatomic) IBOutlet UILabel *lblEnds;
@property (strong, nonatomic) IBOutlet UILabel *lblReported;
@property (strong, nonatomic) IBOutlet UILabel *lblReportedDate;
@property (strong, nonatomic) IBOutlet UILabel *lblStartTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEndTime;
@property (strong, nonatomic) IBOutlet UILabel *lblCertificateExpiry;
@property (strong, nonatomic) IBOutlet UILabel *lblCertificateExpiryDate;
@property (weak,   nonatomic) Appointment *appointment;
-(void) reloadData:(Appointment*)appointment;
@end
