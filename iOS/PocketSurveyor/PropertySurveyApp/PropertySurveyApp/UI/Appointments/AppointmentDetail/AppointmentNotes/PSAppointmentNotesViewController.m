//
//  PSAppointmentNotesViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSAppointmentNotesViewController.h"
#import "PSTextView.h"
#import "PSBarButtonItem.h"

@interface PSAppointmentNotesViewController ()
{
    PSTextView *_textView;
}
@end

@implementation PSAppointmentNotesViewController
@synthesize mailViewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _textView = [[PSTextView alloc] initWithFrame:self.txtViewFeedback.frame];
	_textView.delegate = self;
	[_textView setBackgroundColor:[UIColor grayColor]];
	[_textView canBecomeFirstResponder];
	self.txtViewFeedback = _textView;
	[self.txtViewFeedback setUserInteractionEnabled:YES];
	[self.txtViewFeedback setEditable:YES];
    [self.txtViewFeedback setBackgroundColor:[UIColor whiteColor]];
	[self.txtViewFeedback setDelegate:self];
	[self.txtViewFeedback setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]];
    
	[self.view addSubview:self.txtViewFeedback];
	[self.txtViewFeedback setReturnKeyType:UIReturnKeyDefault];
    
	[self loadNavigationonBarItems];
    
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // [self registerNotification];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    // [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    PSBarButtonItem *sendButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SEND")
                                                                                  style:PSBarButtonItemStyleDefault
                                                                                 target:self
                                                                                 action:@selector(onClickSendButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:sendButton, nil]];
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
    [self setTitle:LOC(@"KEY_STRING_NOTEPAD")];
}

- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSendButton
{
    CLS_LOG(@"onClickSendButton");
    if ([MFMailComposeViewController canSendMail]) {
        mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:[NSString stringWithFormat:@"Notes from Stock Survey:%@", [self getJobNames:[self.appointment.appointmentToJobDataList allObjects]]]];
        [mailViewController setToRecipients:[NSArray arrayWithObject:LOC(@"KEY_STRING_CUSTOMER_SERVICE_EMAIL")]];
        [mailViewController setMessageBody:[self getMailBody] isHTML:NO];
        
        mailViewController.navigationController.navigationBar.translucent = NO;
        NSString *titleColourKey = @"";
        NSString *titlefontKey = @"";
        
        if(OS_VERSION >= 7.0)
        {
            titleColourKey = NSForegroundColorAttributeName;
            titlefontKey = NSFontAttributeName;
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
          
        }
        else
        {
            titleColourKey = UITextAttributeTextColor;
            titlefontKey = UITextAttributeFont;
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
            
        }
        NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   UIColorFromHex(NAVIGATION_BAR_TITLE_COLOR),titleColourKey,
                                                   [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:20], titlefontKey,
                                                   [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset, nil];
        [[mailViewController navigationBar] setTitleTextAttributes:navbarTitleTextAttributes];
        [[mailViewController navigationBar] setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        [[mailViewController navigationBar] setTintColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [[self navigationController] presentViewController:mailViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView * tempAlertView = [[UIAlertView alloc] initWithTitle:@""
                                                                 message:LOC(@"KEY_ALERT_EMAIL_COMPOSER_NOT_SUPPORTED")
                                                                delegate:nil
                                                       cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                       otherButtonTitles:nil];
        [tempAlertView show];
    }
}

#pragma mark - UITextView Delegate Methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
	return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
	self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	[self.view endEditing:YES];
}

- (void)textViewDidChange:(UITextView *)textView {
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

#warning - Move methods to helper class
#pragma mark - Mail Composer Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [mailViewController dismissViewControllerAnimated:YES completion:^{
        [self popOrCloseViewController];
    }];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    UIAlertView * tempAlertView;
    NSString *title = @"";
    NSString *message = @"";
    BOOL showTextView = NO;
    
    switch (result) {
        case MFMailComposeResultCancelled:
            [controller dismissViewControllerAnimated:YES completion:nil];
            break;
            
        case MFMailComposeResultSent:
            title = LOC(@"KEY_ALERT_MESSAGE_SENT");
            message = LOC(@"KEY_ALERT_NOTES_FORWARDED_TO_CUSTOMER_SERVICE");
            showTextView = YES;
            
            break;
        case MFMailComposeResultFailed:
            title = LOC(@"KEY_ALERT_ERROR");
            message = LOC(@"KEY_ALERT_NOTES_NOT_SENT_SUCCESSFULLY");
            showTextView = YES;
            break;
            
        case MFMailComposeResultSaved:
            title = LOC(@"KEY_ALERT_WARNING");
            message = LOC(@"KEY_ALERT_DRAFT_SAVE_NOT_SUPPORTED");
            showTextView = YES;
        default:
            break;
    }
    if (showTextView)
    {
        tempAlertView = [[UIAlertView alloc] initWithTitle:title
                                                   message:message
                                                  delegate:self
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil];
        [tempAlertView show];
        
    }
}

#pragma mark - Private Methods

-(NSString *) getMailBody
{
    NSArray *customers = [NSArray arrayWithArray:[self.appointment.appointmentToCustomer allObjects]];
    NSArray *jobs = [NSArray arrayWithArray:[self.appointment.appointmentToJobDataList allObjects]];
    NSString *jobNames = [self getJobNames:jobs];
    NSString *customerNames = [self getCustomerNames:customers];
    
    NSString *mailBody=[NSString stringWithFormat:@"Customer: %@ \nAddress: %@ \nRef: %@ \nAppointment: %@ \nOperative: %@ \n Notes:\n%@", customerNames, [self.appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleFull], jobNames,[UtilityClass stringFromDate:self.appointment.appointmentStartTime dateFormat:kDateTimeStyle12], [[SettingsClass sharedObject]loggedInUser].fullName, self.txtViewFeedback.text];
    return mailBody;
}

- (NSString *) getCustomerNames:(NSArray *)customers
{
    NSMutableString *customerNames = [NSMutableString stringWithString:@""];
    if (!isEmpty(customers))
    {
        for (NSInteger customerNumber = 0; customerNumber < [customers count]; ++customerNumber)
        {
            Customer *customer = [customers objectAtIndex:customerNumber];
            if (customerNumber != 0)
            {
                [customerNames appendFormat:@", %@", [customer fullName]];
            }
            else
            {
                [customerNames appendString:[customer fullName]];
            }
        }
    }
    else
    {
        [customerNames appendString:LOC(@"KEY_STRING_NOT_APPLICABLE")];
    }
    return customerNames;
}

- (NSString *)getJobNames:(NSArray *)jobs
{
    NSMutableString *jobNames = [NSMutableString stringWithString:@""];
    if (!isEmpty(jobs))
    {
        for (NSInteger jobNumber = 0; jobNumber < [jobs count]; ++jobNumber)
        {
            JobDataList *jobData = [jobs objectAtIndex:jobNumber];
            if (jobNumber != 0)
            {
                [jobNames appendFormat:@", %@", jobData.jsNumber];
            }
            else
            {
                [jobNames appendString:jobData.jsNumber];
            }
        }
    }
    else
    {
        [jobNames appendString:LOC(@"KEY_STRING_NOT_APPLICABLE")];
    }
    return jobNames;
}
@end
