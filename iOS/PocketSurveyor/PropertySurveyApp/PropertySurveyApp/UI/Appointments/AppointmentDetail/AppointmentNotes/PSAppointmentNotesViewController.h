//
//  PSAppointmentNotesViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSCustomViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface PSAppointmentNotesViewController : PSCustomViewController <UITextViewDelegate, MFMailComposeViewControllerDelegate>
@property(nonatomic, retain)  MFMailComposeViewController *mailViewController;
@property (strong, nonatomic) IBOutlet UITextView *txtViewFeedback;
@property (weak,   nonatomic) Appointment *appointment;
@end
