//
//  PSNewAppointmentStartEndCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSNewAppointmentViewController.h"

@interface PSNewAppointmentStartEndCell : UITableViewCell <UIActionSheetDelegate> {
    
}

@property (weak,    nonatomic) id<PSNewAppointmentProtocol> delegate;

@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (weak,   nonatomic) UIButton *btnPicker;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (strong, nonatomic) NSDate *selectedDate;
- (IBAction)onClickPickerBtn:(id)sender;
@end
