//
//  PSNewAppointmentStartEndCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSNewAppointmentStartEndCell.h"
#import "PSNewAppointmentViewController.h"

#define kRowNewAppointmentStarts 3
#define kRowNewAppointmentEnds 4

@interface PSNewAppointmentStartEndCell()

@property (nonatomic, retain) ActionSheetDatePicker * picker;

@end

@implementation PSNewAppointmentStartEndCell
@synthesize pickerOptions;
@synthesize delegate;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClickPickerBtn:(id)sender
{
    if(self.selectedDate == nil)
    {
        self.selectedDate = [NSDate date];
    }
    CLS_LOG(@"date picker will show");
    self.picker = [ActionSheetDatePicker showPickerWithTitle:self.lblHeading.text
                                datePickerMode:UIDatePickerModeDateAndTime
                                  selectedDate:self.selectedDate
                                        target:self
                                        action:@selector(onDateSelected:element:)
                                        origin:sender];
    CLS_LOG(@"date picker did show");
}

- (void)onDateSelected:(NSDate *)selectedDate element:(id)element {
    
    self.selectedDate = selectedDate;
    
    CLS_LOG(@">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didSelectStartEndDatePickerOption:)])
    {
        CLS_LOG(@"date picker will delegate value");
        [(PSNewAppointmentViewController *)self.delegate didSelectStartEndDatePickerOption:self.selectedDate];
        CLS_LOG(@"date picker did delegate value");
    }
    CLS_LOG(@"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

}
@end
