//
//  PSNewAppointmentPickerViewCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSNewAppointmentViewController.h"

@interface PSNewAppointmentPickerViewCell : UITableViewCell

@property (weak,   nonatomic) id<PSNewAppointmentProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (weak,   nonatomic) UIButton *btnPicker;
- (IBAction)onClickPickerBtn:(id)sender;

@end
