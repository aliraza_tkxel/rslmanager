//
//  PSNewAppointmentPropertyCell.m
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSNewAppointmentPropertyCell.h"
#import "Property+Methods.h"
#import "PropertyPicture+MWPhoto.h"

@implementation PSNewAppointmentPropertyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) reloadData:(SProperty*)property
{
    
    [self.lblName setText:[property fullName]];
    [self.lblAddress setText:[property SPropertyAddress]];
    [self.lblCertificateExpiry setText:[UtilityClass stringFromDate:property.certificateExpiry dateFormat:kDateTimeStyle8]];
    [self.lblName setHidden:NO];
    [self.lblAddress setHidden:NO];
    [self.lblCertificateExpiry setHidden:NO];
    [self.lblCertificateExpiryTitle setHidden:NO];
    [self.imgProperty setHidden:NO];
    
    if (isEmpty(property.certificateExpiry))
    {
        [self.lblCertificateExpiry setHidden:YES];
        [self.lblCertificateExpiryTitle setHidden:YES];
    }
    else
    {
        [self.lblCertificateExpiry setHidden:NO];
        [self.lblCertificateExpiryTitle setHidden:NO];
    }
    
    Property * propertyObj= [[PSCoreDataManager sharedManager] propertyWithIdentifier:property.propertyId];
    
    UIImage * image=[propertyObj.defaultPicture underlyingImage];
    if (image) {
        
        [self.imgProperty setImage: image];
        
    }else
    {
        [self.imgProperty setImageWithURL:[NSURL URLWithString:propertyObj.defaultPicture.imagePath] andAddBorder:YES];
    }
    
    
}



@end
