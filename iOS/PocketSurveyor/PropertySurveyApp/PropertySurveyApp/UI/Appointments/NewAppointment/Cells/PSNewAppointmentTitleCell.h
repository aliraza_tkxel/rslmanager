//
//  PSNewAppointmentTitleCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSNewAppointmentTitleCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *txtTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
- (IBAction)resignFirstResponder:(id)sender;
@end
