//
//  PSNewAppointmentAddToCalendarCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSNewAppointmentAddToCalendarCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UISwitch *swtAddToCalendar;
- (IBAction)onValueChange:(id)sender;

@end
