//
//  PSNewAppointmentPropertyCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSNewAppointmentPropertyCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
@property (strong, nonatomic) IBOutlet FBRImageView *imgProperty;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblCertificateExpiryTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblCertificateExpiry;
@property (strong, nonatomic) IBOutlet UIButton *btnSearchProperty;


-(void) reloadData:(SProperty*)property;

@end
