//
//  PSNewAppointmentSurveyorCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSNewAppointmentSurveyorCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UILabel *lblSurveyor;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;

@end
