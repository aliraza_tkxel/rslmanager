//
//  PSNewAppointmentNotesCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSNewAppointmentNotesCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextField *txtNotes;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
- (IBAction)resignFirstResponder:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgLine;

@end
