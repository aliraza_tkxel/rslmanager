//
//  PSNewAppointmentViewController.m
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSNewAppointmentViewController.h"
#import "PSBarButtonItem.h"
#import "PSNewAppointmentTitleCell.h"
#import "PSNewAppointmentPickerViewCell.h"
#import "PSNewAppointmentPropertyCell.h"
#import "PSNewAppointmentStartEndCell.h"
#import "PSNewAppointmentSurveyorCell.h"
#import "PSNewAppointmentReminderCell.h"
#import "PSNewAppointmentAddToCalendarCell.h"
#import "PSNewAppointmentNotesCell.h"
#import "PSSurveyorsViewController.h"
#import "PSPropertyListViewController.h"
#import "PSTextView.h"
#import "PSTextViewCell.h"
#import "PSAddNotesViewController.h"
#import "PSCameraViewController.h"
#import "JSON.h"
#import "PSEventsManager.h"
#import "PSBarButtonItem.h"

#define kNumberOfSections 1
#define kNumberOfRows 11

#define kRowNewAppointmentTitle 0
#define kRowNewAppointmentType 1
#define kRowNewAppointmentProperty 2
#define kRowNewAppointmentStarts 3
#define kRowNewAppointmentEnds 4
#define kRowNewAppointmentSurveyor 5
#define kRowNewAppointmentReminder 6
#define kRowNewAppointmentAddToCalendar 7
#define kRowNewAppointmentCalendarType 8
#define kRowNewAppointmentShowMeAs 9
#define kRowNewAppointmentNotes 10

#define kNewAppointmentTitleCellHeight 50
#define kNewAppointmentPickerViewCellHeight 42
#define kNewAppointmentPropertyCellHeight 91
#define kNewAppointmentStartsEndsCellHeight 46
#define kNewAppointmentSurveyorCellHeight 44
#define kNewAppointmentRemindersCellHeight 42
#define kNewAppointmentAddToCalendarCellHeight 40
#define kNewAppointmentShowMeAsCellHeight 55
#define kNewAppointmentNotesCellHeight 87

@interface PSNewAppointmentViewController ()
{
@private
    PSTextView *_textView;
    NSString *_riskNotes;
}
@end

@implementation PSNewAppointmentViewController
@synthesize appointmentType;
@synthesize reminder;
@synthesize startTime;
@synthesize endTime;
@synthesize startDate;
@synthesize endDate;
@synthesize showMeAs;
@synthesize pickerCellTag;
@synthesize startEndCellTag;
@synthesize notes;
@synthesize startDateTime;
@synthesize endDateTime;
@synthesize property;
@synthesize calendarType;
@synthesize appointmentTitle;
@synthesize surveyor = _surveyor;
@synthesize appointmentDictionary;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadNavigationonBarItems];
    self.appointmentDictionary = [NSMutableDictionary dictionary];
    self.notes = _riskNotes;
    [self.tableView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self.view setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    [self setTitle:LOC(@"KEY_STRING_NEW_APPOINTMENT")];
    
    //tableFrame = [self.tableView frame];

}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [self registerKeyboardNotifications];
    [self registerNotifications];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
//    CLS_LOG(@"%f",CGRectGetHeight([self.view frame]));
//    CLS_LOG(@"%f",CGRectGetHeight([self.navigationController.navigationBar frame]));
//    CLS_LOG(@"%f",CGRectGetMaxY([self.view frame]));
//    CLS_LOG(@"%f",CGRectGetMaxY([self.navigationController.navigationBar frame]));
//    CLS_LOG(@"%f",CGRectGetHeight([self.tableView frame]));

    [self.tableView setFrame:CGRectMake(CGRectGetMinX([self.tableView frame]),CGRectGetMinY([self.tableView frame]),CGRectGetWidth([self.view frame]), CGRectGetHeight([self.view frame]))];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self deregisterKeyboardNotifications];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    CLS_LOG(@"-----New appointment didReceiveMemoryWarning");
}


#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveNewAppointmentSuccessNotification) name:kNewAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveNewAppointmentFailureNotification) name:kNewAppointmentFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAppointmentTimeUnavailableNotification) name:kAppointmentTimeNotAvailableNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveAppointmentTimeUnavailableBankHolidayNotification) name:kAppointmentTimeNotAvailableBankHolidayNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNewAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNewAppointmentFailureNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentTimeNotAvailableNotification object:nil];
	  [[NSNotificationCenter defaultCenter] removeObserver:self name:kAppointmentTimeNotAvailableBankHolidayNotification object:nil];
}
- (void) onReceiveNewAppointmentSuccessNotification
{
    [self hideActivityIndicator];
    [self performSelector:@selector(onClickBackButton) withObject:nil afterDelay:0.5];
}

- (void) onReceiveNewAppointmentFailureNotification
{
    [self hideActivityIndicator];
    NSString *alertMessage = LOC(@"KEY_ALERT_FAILED_TO_CREATE_NEW_APPOINTMENT");
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                   message:alertMessage
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
    [self enableNavigationBarButtons];
}

- (void) onReceiveAppointmentTimeUnavailableNotification
{
    [self hideActivityIndicator];
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
                                                   message:LOC(@"KEY_ALERT_APPOINTMENT_TIME_UNAVAILABLE")
                                                  delegate:nil
                                         cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                         otherButtonTitles:nil,nil];
    [alert show];
    [self enableNavigationBarButtons];
}

- (void) onReceiveAppointmentTimeUnavailableBankHolidayNotification
{
	[self hideActivityIndicator];
	UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR")
																								 message:LOC(@"KEY_ALERT_APPOINTMENT_TIME_UNAVAILABLE_BANK_HOLIDAY")
																								delegate:nil
																			 cancelButtonTitle:LOC(@"KEY_ALERT_OK")
																			 otherButtonTitles:nil,nil];
	[alert show];
	[self enableNavigationBarButtons];
}

#pragma mark - Navigation Bar
- (void) loadNavigationonBarItems
{
    PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
                                                                         style:PSBarButtonItemStyleBack
                                                                        target:self
                                                                        action:@selector(onClickBackButton)];
    
    self.saveButton = [[PSBarButtonItem alloc]          initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
                                                                                  style:PSBarButtonItemStyleDefault
                                                                                 target:self
                                                                                 action:@selector(onClickSaveButton)];
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
    CLS_LOG(@"onClickBackButton");
    [self popOrCloseViewController];
}

- (void) onClickSaveButton
{
    
    CLS_LOG(@"onClickSaveButton");
    [self resignAllResponders];
    [self disableNavigationBarButtons];
    [self.appointmentDictionary setObject:isEmpty(self.notes)?[NSNull null]:self.notes forKey:kAppointmentNotes];
    [self validateForm];
    //[Appointment saveNewAppointment:self.appointmentDictionary];
    //[self.navigationController popToRootViewControllerAnimated:YES];
    
}

#pragma mark - UITableView Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kNumberOfRows;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNumberOfSections;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = 0;
    
    if (indexPath.row == kRowNewAppointmentTitle)
    {
        rowHeight = kNewAppointmentTitleCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentType)
    {
        rowHeight = kNewAppointmentPickerViewCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentProperty)
    {
        if (!isEmpty(self.property))
        {
            rowHeight = kNewAppointmentPropertyCellHeight;
        }
        else
        {
            rowHeight = 40;
        }
    }
    
    else if (indexPath.row == kRowNewAppointmentStarts)
    {
        rowHeight = kNewAppointmentStartsEndsCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentEnds)
    {
        rowHeight = kNewAppointmentStartsEndsCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentSurveyor)
    {
        rowHeight = kNewAppointmentSurveyorCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentReminder)
    {
        rowHeight = kNewAppointmentRemindersCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentAddToCalendar)
    {
        rowHeight = kNewAppointmentAddToCalendarCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentShowMeAs)
    {
        rowHeight = kNewAppointmentPickerViewCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentCalendarType)
    {
        rowHeight = kNewAppointmentPickerViewCellHeight;
    }
    
    else if (indexPath.row == kRowNewAppointmentNotes)
    {
        if (!isEmpty(_riskNotes))
        {
            rowHeight = [self heightForTextView:_textView containingString:_riskNotes];
            if (rowHeight < 100)
            {
                rowHeight = 100;
            }
        }
        else
        {
            rowHeight = 100;
        }
    }
    
    return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *_cell = nil;
    
    if (indexPath.row == kRowNewAppointmentTitle)
    {
        static NSString *titleCellIdentifier = @"titleCellIdentifier";
        PSNewAppointmentTitleCell *cell = [self.tableView dequeueReusableCellWithIdentifier:titleCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentTitleCell" owner:self options:nil];
            cell = self.appointmentTitleCell;
            self.appointmentTitleCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
    }
    
    else if (indexPath.row == kRowNewAppointmentType)
    {
        
        static NSString *typeCellIdentifier = @"pickerViewCellIdentifier";
        PSNewAppointmentPickerViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:typeCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentPickerViewCell" owner:self options:nil];
            cell = self.appointmentPickerViewCell;
            self.appointmentPickerViewCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
    }
    
    else if (indexPath.row == kRowNewAppointmentProperty)
    {
        static NSString *propertyCellIdentifier = @"propertyCellIdentifier";
        PSNewAppointmentPropertyCell *cell = [self.tableView dequeueReusableCellWithIdentifier:propertyCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentPropertyCell" owner:self options:nil];
            cell = self.appointmentPropertyCell;
            self.appointmentPropertyCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.row == kRowNewAppointmentStarts)
    {
        static NSString *startCellIdentifier = @"startEndCellIdentifier";
        PSNewAppointmentStartEndCell *cell = [self.tableView dequeueReusableCellWithIdentifier:startCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentStartEndCell" owner:self options:nil];
            cell = self.appointmentStartEndCell;
            self.appointmentStartEndCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        [_cell.contentView setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        _cell = cell;
        
    }
    
    else if (indexPath.row == kRowNewAppointmentEnds)
    {
        static NSString *endCellIdentifier = @"startEndCellIdentifier";
        PSNewAppointmentStartEndCell *cell = [self.tableView dequeueReusableCellWithIdentifier:endCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentStartEndCell" owner:self options:nil];
            cell = self.appointmentStartEndCell;
            self.appointmentStartEndCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        
        _cell = cell;
        
    }
    
    else if (indexPath.row == kRowNewAppointmentSurveyor)
    {
        static NSString *surveyorCellIdentifier = @"surveyorCellIdentifier";
        PSNewAppointmentSurveyorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:surveyorCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentSurveyorCell" owner:self options:nil];
            cell = self.appointmentSurveyorCell;
            self.appointmentSurveyorCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        
        _cell = cell;
    }
    
    else if (indexPath.row == kRowNewAppointmentReminder)
    {
        static NSString *reminderCellIdentifier = @"reminderCellIdentifier";
        PSNewAppointmentReminderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reminderCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentReminderCell" owner:self options:nil];
            cell = self.appointmentReminderCell;
            self.appointmentReminderCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        
        _cell = cell;
    }
    
    else if (indexPath.row == kRowNewAppointmentAddToCalendar)
    {
        static NSString *addToCalendarCellIdentifier = @"addToCalendarCellIdentifier";
        PSNewAppointmentAddToCalendarCell *cell = [self.tableView dequeueReusableCellWithIdentifier:addToCalendarCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentAddToCalendarCell" owner:self options:nil];
            cell = self.appointmentaddToCalendarCell;
            self.appointmentaddToCalendarCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        
        _cell = cell;
        
    }
    else if (indexPath.row == kRowNewAppointmentCalendarType)
    {
        static NSString *calendarTypeCellIdentifier = @"pickerViewCellIdentifier";
        PSNewAppointmentPickerViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:calendarTypeCellIdentifier];
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentPickerViewCell" owner:self options:nil];
            cell = self.appointmentPickerViewCell;
            self.appointmentPickerViewCell = nil;
        }
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
    }
    
    else if (indexPath.row == kRowNewAppointmentShowMeAs)
    {
        static NSString *showMCellIdentifier = @"pickerViewCellIdentifier";
        PSNewAppointmentPickerViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:showMCellIdentifier];
        
        if(cell == nil)
        {
            [[NSBundle mainBundle] loadNibNamed:@"PSNewAppointmentPickerViewCell" owner:self options:nil];
            cell = self.appointmentPickerViewCell;
            self.appointmentPickerViewCell = nil;
        }
        
        [self configureCell:&cell atIndexPath:indexPath];
        _cell = cell;
        
    }
    
    else if (indexPath.row == kRowNewAppointmentNotes)
    {
        PSTextViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:textViewCellIdentifier];
        if (_cell == nil)
        {
            cell = [[PSTextViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:textViewCellIdentifier];
            self.textViewCell = cell;
        }
        @synchronized(self){
        [self configureCell:&cell atIndexPath:indexPath];
        }
        _cell = cell;
    }
    
    [_cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [_cell setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
    return _cell;
}

- (void)configureCell:(UITableViewCell **)cell atIndexPath:(NSIndexPath *)indexPath
{
    if ([*cell isKindOfClass:[PSNewAppointmentTitleCell class]])
    {
        PSNewAppointmentTitleCell *_cell = (PSNewAppointmentTitleCell *) *cell;
        [_cell.txtTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.txtTitle setDelegate:self];
        [_cell.lblHeading setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        [_cell.txtTitle setTag:indexPath.row];
        [_cell.txtTitle setText:self.appointmentTitle];
        *cell = _cell;
        
    }
    
    else if ([*cell isKindOfClass:[PSNewAppointmentPickerViewCell class]])
    {
        PSNewAppointmentPickerViewCell *_cell = (PSNewAppointmentPickerViewCell *) *cell;
        _cell.delegate = self;
        if (indexPath.row == kRowNewAppointmentType)
        {
            [_cell.lblHeading setText:LOC(@"KEY_STRING_TYPE")];
            
            _cell.pickerOptions = @[kAppointmentTypeStock];
            [_cell.btnPicker addTarget:self action:@selector(onClickAppointmentTypePickerButton:) forControlEvents:UIControlEventTouchUpInside];
            [_cell.btnPicker setTag:kRowNewAppointmentType];
            [_cell.lblTitle setText:self.appointmentType];
        }
        
        else if (indexPath.row == kRowNewAppointmentShowMeAs)
        {
            [_cell.lblTitle setText:self.showMeAs];
            _cell.pickerOptions = @[/*LOC(@"KEY_STRING_NONE"),*/LOC(@"KEY_STRING_FREE"), LOC(@"KEY_STRING_TENTATIVE"), LOC(@"KEY_STRING_BUSY"), LOC(@"KEY_STRING_OUT_OF_OFFICE")];
            _cell.lblHeading.text = LOC(@"KEY_STRING_SHOW_ME_AS");
            ;
            [_cell.btnPicker addTarget:self action:@selector(onClickShowMeAsPickerButton:) forControlEvents:UIControlEventTouchUpInside];
            [_cell.btnPicker setTag:kRowNewAppointmentShowMeAs];
        }
        
        else if (indexPath.row == kRowNewAppointmentCalendarType)
        {
            [_cell.lblTitle setText:self.calendarType];
            [_cell.lblHeading setText:LOC(@"KEY_STRING_CALENDAR_TYPE")];
            [_cell.btnPicker addTarget:self action:@selector(onClickCalendarTypePickerButton:) forControlEvents:UIControlEventTouchUpInside];
          //  _cell.pickerOptions = @[LOC(@"KEY_STRING_NONE"),LOC(@"KEY_STRING_FREE")];
            _cell.pickerOptions = [[PSEventsManager sharedManager]listCalendars];
            [_cell.btnPicker setTag:kRowNewAppointmentCalendarType];
        }
        
        [_cell.lblTitle setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblHeading setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSNewAppointmentPropertyCell class]])
    {
        PSNewAppointmentPropertyCell *_cell = (PSNewAppointmentPropertyCell *) *cell;
        [_cell.btnSearchProperty addTarget:self action:@selector(onClickSearchPropertyButton:) forControlEvents:UIControlEventTouchUpInside];
        if (!isEmpty(self.property))
        {
            [_cell reloadData:self.property];
        }
        
        else
        {
            [_cell.lblName setHidden:YES];
            [_cell.lblAddress setHidden:YES];
            [_cell.lblCertificateExpiry setHidden:YES];
            [_cell.lblCertificateExpiryTitle setHidden:YES];
            [_cell.imgProperty setHidden:YES];
        }
        
        [_cell.lblName setTextColor:UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR)];
        [_cell.lblAddress setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
        [_cell.lblCertificateExpiry setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblHeading setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        *cell = _cell;
        
    }
    
    else if ([*cell isKindOfClass:[PSNewAppointmentStartEndCell class]])
    {
        PSNewAppointmentStartEndCell *_cell = (PSNewAppointmentStartEndCell *) *cell;
        
        [_cell setDelegate:self];
        
        if (indexPath.row == kRowNewAppointmentStarts)
        {
            [_cell.lblDate setText:self.startDate];
            [_cell.lblTime setText:self.startTime];
            _cell.lblHeading.text = LOC(@"KEY_STRING_STARTS");
            [_cell.btnPicker addTarget:self action:@selector(onClickStartPickerButton:) forControlEvents:UIControlEventTouchUpInside];
            [_cell.btnPicker setTag:kRowNewAppointmentStarts];
        }
        
        else if (indexPath.row == kRowNewAppointmentEnds)
        {
            [_cell.lblDate setText:self.endDate];
            [_cell.lblTime setText:self.endTime];
            _cell.lblHeading.text = LOC(@"KEY_STRING_END");
            [_cell setSelectedDate:self.endDateTime];
            [_cell.btnPicker addTarget:self action:@selector(onClickEndPickerButton:) forControlEvents:UIControlEventTouchUpInside];
            [_cell.btnPicker setTag:kRowNewAppointmentEnds];
        }
        [_cell.lblDate setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblTime setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblHeading setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        *cell = _cell;
        
    }
    
    else if ([*cell isKindOfClass:[PSNewAppointmentSurveyorCell class]])
    {
        PSNewAppointmentSurveyorCell *_cell = (PSNewAppointmentSurveyorCell *) *cell;
        [_cell.btnSearch addTarget:self action:@selector(onClickSearchSurveyorButton:) forControlEvents:UIControlEventTouchUpInside];
        [_cell.btnSearch setTag:kRowNewAppointmentTitle];
        [_cell.lblSurveyor setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblHeading setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        
        if (!isEmpty(self.surveyor)) {
            [_cell.lblSurveyor setText:_surveyor.fullName];
        }

        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSNewAppointmentReminderCell class]])
    {
        PSNewAppointmentReminderCell *_cell = (PSNewAppointmentReminderCell *) *cell;
        _cell.delegate = self;
        _cell.pickerOptions = @[LOC(@"KEY_STRING_NONE"),LOC(@"KEY_STRING_FIVE_MINUTES_BEFORE"), LOC(@"KEY_STRING_FIFTEEN_MINUTES_BEFORE"), LOC(@"KEY_STRING_ONE_HOUR_BEFORE"), LOC(@"KEY_STRING_TWO_HOURS_BEFORE"), LOC(@"KEY_STRING_ONE_DAY_BEFORE"), LOC(@"KEY_STRING_TWO_DAYS_BEFORE"), LOC(@"KEY_STRING_ON_DATE_OF_APPOINTMENT")];
        
        [_cell.lblReminder setTextColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK)];
        [_cell.lblHeading setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        [_cell.btnPicker setTag:kRowNewAppointmentReminder];
        [_cell.lblReminder setText:self.reminder];
        [_cell.btnPicker addTarget:self action:@selector(onClickReminderPickerButton:) forControlEvents:UIControlEventTouchUpInside];
        *cell = _cell;
    }
    
    else if ([*cell isKindOfClass:[PSNewAppointmentAddToCalendarCell class]])
    {
        PSNewAppointmentAddToCalendarCell *_cell = (PSNewAppointmentAddToCalendarCell *) *cell;
        [_cell.lblTitle setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        
        *cell = _cell;
        
    }
    
    else if ([*cell isKindOfClass:[PSTextViewCell class]])
    {
        PSTextViewCell *_cell = (PSTextViewCell *)*cell;
        
        UILabel *header = [[UILabel alloc] init];
        header.frame = CGRectMake(12, 3, 320,12);
        [header setBackgroundColor:UIColorFromHex(THEME_BG_COLOUR)];
        [header setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
        [header setFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:12]];
        [header setText:LOC(@"KEY_STRING_NOTES")];
        [_cell addSubview:header];
        
        _textView = [[PSTextView alloc] initWithFrame:CGRectZero];
        [_textView setAutocorrectionType:UITextAutocorrectionTypeNo];
    
        _textView.delegate = self;
        _textView.tag = indexPath.row;
        [_textView setScrollEnabled:NO];
        
        if(!isEmpty(self.notes))
        {
            _textView.text = self.notes;
        }
        _cell.textView = _textView;
        _cell.textView.editable = YES;
        
    }
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.selectedControl = (UITextField *)textField;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    /* self.notes = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
     [self.tableView beginUpdates];
     [self.tableView endUpdates];*/
    return YES;
}

#pragma mark - UITextView Delegate Methods
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.selectedControl = (UIControl *)textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.notes = _riskNotes;
}

- (void)textViewDidChange:(UITextView *)textView
{
    _riskNotes = textView.text;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    [self scrollToRectOfSelectedControl];
}


#pragma mark - UIScrollView Delegates

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resignAllResponders];
}


#pragma mark - Scroll Text Field Methods
- (void)scrollToRectOfSelectedControl {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - Cell Button Click Selectors

- (IBAction)onClickReminderPickerButton:(id)sender
{
    self.selectedControl = (UIButton *)sender;
    CLS_LOG(@"onClickReminderPickerButton");
    
}

- (IBAction)onClickShowMeAsPickerButton:(id)sender
{
    CLS_LOG(@"onClickShowMeAsPickerButton");
    self.selectedControl = (UIButton *)sender;
    self.pickerCellTag = kRowNewAppointmentShowMeAs;
}

- (IBAction)onClickCalendarTypePickerButton:(id)sender
{
    CLS_LOG(@"onClickCalendarTypePickerButton");
    self.pickerCellTag = kRowNewAppointmentCalendarType;
     self.selectedControl = (UIButton *)sender;
}
- (IBAction)onClickAppointmentTypePickerButton:(id)sender
{
    CLS_LOG(@"onClickAppointmentTypePickerButton");
    self.pickerCellTag = kRowNewAppointmentType;
     self.selectedControl = (UIButton *)sender;
}

- (IBAction)onClickSearchPropertyButton:(id)sender
{
    CLS_LOG(@"onClickSearchPropertyButton");
     self.selectedControl = (UIButton *)sender;
    PSPropertyListViewController *propertyListViewViewConroller = [[PSPropertyListViewController alloc] initWithNibName:@"PSPropertyListViewController" bundle:[NSBundle mainBundle]];
    propertyListViewViewConroller.delegate = self;
    [self.navigationController pushViewController:propertyListViewViewConroller animated:YES];
}

- (IBAction)onClickSearchSurveyorButton:(id)sender
{
    CLS_LOG(@"onClickSearchSurveyorButton");
     self.selectedControl = (UIButton *)sender;
    PSSurveyorsViewController *surveyorViewConroller = [[PSSurveyorsViewController alloc] initWithNibName:@"PSSurveyorsViewController" bundle:[NSBundle mainBundle]];
    surveyorViewConroller.delegate = self;
    [self.navigationController pushViewController:surveyorViewConroller animated:YES];
    
    
}

- (IBAction)onClickStartPickerButton:(id)sender
{
    CLS_LOG(@"onClickStartPickerButton");
     self.selectedControl = (UIButton *)sender;
    self.startEndCellTag = kRowNewAppointmentStarts;
}

- (IBAction)onClickEndPickerButton:(id)sender
{
    CLS_LOG(@"onClickEndPickerButton");
    self.startEndCellTag = kRowNewAppointmentEnds;
    self.selectedControl = (UIButton *)sender;
}

#pragma mark - Protocol Methods

/*!
 @discussion
 Method didSelectReminderPickerOption Called upon selection of Reminder picker option.
 */
- (void) didSelectReminderPickerOption:(NSString *)pickerOption
{
    self.reminder = pickerOption;
    [self.appointmentDictionary setObject:self.reminder forKey:kSurveyorAlert];
    [self.tableView reloadData];
    [self.view endEditing:YES];
}

/*!
 @discussion
 Method didSelectStartEndDatePickerOption Called upon selection of Start or End Date picker option.
 */
- (void) didSelectStartEndDatePickerOption:(NSDate *)datePickerOption
{
    if (!isEmpty(datePickerOption))
    {
      /*  NSString *dateString = [self dateString:datePickerOption];
        NSString *timeString = [self timeString:datePickerOption];*/
        
        if (self.startEndCellTag == kRowNewAppointmentStarts)
        {
            [self configureStartDate:datePickerOption];
            
            if (isEmpty(self.endDateTime) || (!([self.endDateTime compare:[self.startDateTime dateByAddingMinutes:1]] == NSOrderedDescending)))
            {
                [self configureEndDate:[datePickerOption dateByAddingMinutes:60]];
            }
        }
        
        else if (self.startEndCellTag == kRowNewAppointmentEnds)
        {
            [self configureEndDate:datePickerOption];
        }
        [self.tableView reloadData];
        [self.view endEditing:YES];
    }
}
/*!
 @discussion
 Method didSelectTypeOrShowPickerOption Called upon selection of Type or Show Me As or Calendar Type picker option.
 */
- (void) didSelectTypeOrShowPickerOption:(NSString *)pickerOption
{
    if (self.pickerCellTag == kRowNewAppointmentType)
    {
        self.appointmentType = pickerOption;
        [self.appointmentDictionary setObject:self.appointmentType forKey:kAppointmentType];
        
#warning PSA: Hardcoded values in Dictionary
        [self.appointmentDictionary setObject:kAppointmentStatusNotStarted forKey:kAppointmentStatus];
        [self.appointmentDictionary setObject:LOC(@"KEY_STRING_LOCAL") forKey:kSurveyType];
    }
    
    else if (self.pickerCellTag == kRowNewAppointmentShowMeAs)
    {
        self.showMeAs = pickerOption;
        [self.appointmentDictionary setObject:[UtilityClass removeWhiteSpacesFromString:pickerOption]  forKey:kSurveyourAvailability];
    }
    else if (self.pickerCellTag == kRowNewAppointmentCalendarType)
    {
        self.calendarType = pickerOption;
        [self.appointmentDictionary setObject:pickerOption forKey:kAppointmentCalendar];
    }
    
    [self.tableView reloadData];
    [self.view endEditing:YES];
}

/*!
 @discussion
 Method didSelectSurveyor Called upon selection of Surveyor from SurveyorsViewController.
 */
- (void) didSelectSurveyor:(Surveyor *)surveyor
{
    _surveyor = surveyor;
    [self.appointmentDictionary setObject:surveyor.userName forKey:kSurveyorUserName];
    
    [self.tableView reloadData];
}

/*!
 @discussion
 Method didSelectSProperty Called upon selection of Property from PropertyListViewController
 */
- (void) didSelectSProperty:(SProperty *)sProperty{
    self.property = sProperty;
    [self.appointmentDictionary setObject:[self.property.sPropertyJSON JSONValue] forKey:@"customer"];
    [self.appointmentDictionary setObject:@"" forKey:@"CP12Info"];
    [self.appointmentDictionary setObject:@"" forKey:@"appInfoData"];
    [self.appointmentDictionary setObject:[self.property propertyShortAddress] forKey:kAppointmentTitle];
    [self.appointmentDictionary setObject:[self.property SPropertyAddress] forKey:kAppointmentLocation];
    self.appointmentTitle = [self.property propertyShortAddress];
    [self.tableView reloadData];
}
#pragma mark - Text View Cell Height Calculation
/*!
 @discussion
 Calculates height for text view and text view cell.
 */
- (CGFloat) heightForTextView:(UITextView*)textView containingString:(NSString*)string
{
    float horizontalPadding = 0;
    float verticalPadding = 22;
    CGFloat height = 0.0;
    if(OS_VERSION >= 7.0)
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSParagraphStyle defaultParagraphStyle], NSParagraphStyleAttributeName,
                                    [UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17],NSFontAttributeName,
                                    nil];
        CGRect boundingRect = [string boundingRectWithSize:CGSizeMake(290, 999999.0f)
                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                attributes:attributes
                                                   context:nil];
        height = boundingRect.size.height + verticalPadding;
    }
    else
    {
        height = [string sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueLight size:17]
                            constrainedToSize:CGSizeMake(290, 999999.0f)
                                lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
    }
#warning move this method to parent class.
    return height * 1.05;
}

#pragma mark - Methods

- (NSString *) dateString:(NSDate *) appointmentDate
{
    NSString *date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:kDateTimeStyle8];
    date = [dateFormatter stringFromDate: appointmentDate];
    return date;
}

- (NSString *) timeString:(NSDate *) appointmentDate
{
    NSString *time;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:kDateTimeStyle3];
    time = [dateFormatter stringFromDate: appointmentDate];
    return time;
}

- (NSString *) convertNSDictionaryToJSONString
{
    return [self.appointmentDictionary JSONRepresentation];
}

- (void) validateForm
{
    NSString *alertMessage;
    
    if (isEmpty(self.appointmentType) || isEmpty(self.property) || isEmpty(self.startDateTime)
        || isEmpty(self.endDateTime) || isEmpty(self.surveyor) || isEmpty(self.reminder)
        || isEmpty(self.showMeAs) /*|| isEmpty(self.calendarType)*/)
    {
        
        if (isEmpty(self.appointmentType))
        {
            alertMessage = LOC(@"KEY_ALERT_TYPE_EMPTY");
        }
        
        else if (isEmpty(self.property))
        {
            alertMessage = LOC(@"KEY_ALERT_PROPERTY_EMPTY");
        }
        
        else if (isEmpty(self.startDateTime))
        {
            alertMessage = LOC(@"KEY_ALERT_START_TIME_EMPTY");
        }
        else if (isEmpty(self.endDateTime))
        {
            alertMessage = LOC(@"KEY_ALERT_END_TIME_EMPTY");
        }
        else if (isEmpty(self.surveyor))
        {
            alertMessage = LOC(@"KEY_ALERT_SURVEYOR_EMPTY");
        }
        else if (isEmpty(self.reminder))
        {
            alertMessage = LOC(@"KEY_ALERT_REMINDER_EMPTY");
        }
        else if (isEmpty(self.showMeAs))
        {
            alertMessage = LOC(@"KEY_ALERT_AVAILABILITY_EMPTY");
        }
        else if (isEmpty(self.notes))
        {
            alertMessage = LOC(@"KEY_ALERT_NOTES_EMPTY");
        }
        
        else if (isEmpty(self.calendarType))
        {
            alertMessage = LOC(@"KEY_ALERT_CALENDAR_EMPTY");
        }
        
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:alertMessage delegate:nil cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
        [alert show];
    }
    
    else
    {
        BOOL isDateValid = [self validateAppointmentDates];
        if (isDateValid)
        {
            [self saveAppointment];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:LOC(@"KEY_ALERT_END_TIME_INVALID") delegate:nil cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil,nil];
            [alert show];
        }
   
    }
    [self enableNavigationBarButtons];
}

- (BOOL) validateAppointmentDates
{
    BOOL isValid;
    if (!isEmpty(self.startDateTime) && !isEmpty(self.endDateTime))
    {
        if (!([self.endDateTime compare:[self.startDateTime dateByAddingMinutes:1]] == NSOrderedDescending))
        {
            isValid = NO;
        }
        else
        {
            isValid = YES;
        }
    }
    else
    {
        isValid = NO;
    }
    return isValid;
}

- (void) saveAppointment
{
    if([[ReachabilityManager sharedManager] isConnectedToInternet])
    {
        [self disableNavigationBarButtons];
        CLS_LOG(@"save button disbaled");
        
        [self showActivityIndicator:LOC(@"KEY_STRING_APPOINTMENT_CREATING")];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:kRowNewAppointmentAddToCalendar inSection:0];
        PSNewAppointmentAddToCalendarCell *cell = (PSNewAppointmentAddToCalendarCell*) [self.tableView cellForRowAtIndexPath:indexPath];
        
        BOOL addToCalendar = cell.swtAddToCalendar.on;
        [self.appointmentDictionary setObject:[NSNumber numberWithBool:addToCalendar] forKey:kAppointmentAddtoCalendar];
        [self.appointmentDictionary setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kCreatedBy];
        [[PSAppointmentsManager sharedManager] createAppointment:self.appointmentDictionary];
        [self schduleUIControlsActivation];
    }
    else
    {
        [self enableNavigationBarButtons];
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                       message:LOC(@"KEY_ALERT_NO_INTERNET")
                                                      delegate:nil
                                             cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                             otherButtonTitles:nil,nil];
        
        [alert show];
    }
}

- (void) saveAppointmentWithoutCalendarEvent
{
    [self showActivityIndicator:LOC(@"KEY_STRING_APPOINTMENT_CREATING")];
    [self.appointmentDictionary setObject:[NSNull null] forKey:kAppointmentEventIdentifier];
    //[Appointment saveNewAppointment:self.appointmentDictionary];
    [[PSAppointmentsManager sharedManager] createAppointment:self.appointmentDictionary];
  
    NSString *appointmentJSONString = [self convertNSDictionaryToJSONString];
    CLS_LOG(@"%@",appointmentJSONString);
}

- (void) configureStartDate: (NSDate *)date
{
    NSString *dateString = [self dateString:date];
    NSString *timeString = [self timeString:date];
    self.startDateTime = date;
    NSString *serverStartTime = [UtilityClass convertNSDateToServerDate:self.startDateTime];
    NSString *serverAppointmentDate = [UtilityClass convertNSDateToServerDate:[NSDate date]];
    [self.appointmentDictionary setObject:serverStartTime forKey:kAppointmentStartTime];
    [self.appointmentDictionary setObject:serverAppointmentDate forKey:kLoggedDate];
    self.startDate = dateString;
    self.startTime = timeString;
}

- (void) configureEndDate:(NSDate *)date
{

    if (!isEmpty(self.startDateTime))
    {
        if (!([date compare:[self.startDateTime dateByAddingMinutes:1]] == NSOrderedDescending))
        {
            date = [self.startDateTime dateByAddingMinutes:60];
        }
        NSString *dateString = [self dateString:date];
        NSString *timeString = [self timeString:date];
        self.endDateTime = date;
        NSString *serverEndTime = [UtilityClass convertNSDateToServerDate:self.endDateTime];
        [self.appointmentDictionary setObject:serverEndTime forKey:kAppointmentEndTime];
        self.endDate = dateString;
        self.endTime = timeString;
    }
}

- (void) fetchLoggedInSurveyor
{
    _surveyor = [[PSSurveyorsManager sharedManager] fetchLoggedInSurveyor];
}


@end
