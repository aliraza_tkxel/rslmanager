//
//  PSNewAppointmentViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 04/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSNewAppointmentTitleCell;
@class PSNewAppointmentPropertyCell;
@class PSNewAppointmentPickerViewCell;
@class PSNewAppointmentAddToCalendarCell;
@class PSNewAppointmentNotesCell;
@class PSNewAppointmentStartEndCell;
@class PSNewAppointmentReminderCell;
@class PSNewAppointmentSurveyorCell;
@class PSTextViewCell;
@class PSBarButtonItem;
@protocol PSNewAppointmentProtocol <NSObject>
@required
/*!
 @discussion
 Method didSelectReminderPickerOption Called upon selection of Reminder picker option.
 */
- (void) didSelectReminderPickerOption:(NSString *)pickerOption;

/*!
 @discussion
 Method didSelectStartEndDatePickerOption Called upon selection of Start or End Date picker option.
 */
- (void) didSelectStartEndDatePickerOption:(NSDate *)pickerOption;

/*!
 @discussion
 Method didSelectTypeOrShowPickerOption Called upon selection of Type or Show Me As Date picker option.
 */
- (void) didSelectTypeOrShowPickerOption:(NSString *)pickerOption;

/*!
 @discussion
 Method didSelectSurveyor Called upon selection of surveyor from SurveyorViewController.
 */
- (void) didSelectSurveyor:(Surveyor *)surveyor;

/*!
 @discussion
 Method didSelectSProperty Called upon selection of property from PropertyListViewController.
 */
- (void) didSelectSProperty:(SProperty *)sProperty;

@end

@interface PSNewAppointmentViewController : PSCustomViewController <PSNewAppointmentProtocol,UITextFieldDelegate, UITextViewDelegate> {
    CGRect tableFrame;
}

@property (weak,   nonatomic) IBOutlet PSNewAppointmentTitleCell *appointmentTitleCell;
@property (weak,   nonatomic) IBOutlet PSNewAppointmentNotesCell *appointmentNotesCell;
@property (weak,   nonatomic) IBOutlet PSNewAppointmentStartEndCell *appointmentStartEndCell;
@property (weak,   nonatomic) IBOutlet PSNewAppointmentReminderCell *appointmentReminderCell;
@property (weak,   nonatomic) IBOutlet PSNewAppointmentSurveyorCell *appointmentSurveyorCell;
@property (weak,   nonatomic) IBOutlet PSNewAppointmentPropertyCell *appointmentPropertyCell;
@property (weak,   nonatomic) IBOutlet PSNewAppointmentPickerViewCell *appointmentPickerViewCell;
@property (weak,   nonatomic) IBOutlet PSNewAppointmentAddToCalendarCell *appointmentaddToCalendarCell;
@property (strong, nonatomic) IBOutlet PSTextViewCell *textViewCell;
@property (strong, nonatomic) PSBarButtonItem *saveButton;
@property (strong, nonatomic) NSMutableDictionary *appointmentDictionary;

@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *endTime;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *endDate;
@property (strong, nonatomic) NSString *showMeAs;
@property (strong, nonatomic) NSString *appointmentType;
@property (strong, nonatomic) NSString *reminder;
@property (strong, nonatomic) NSString *notes;
@property (strong, nonatomic) NSString *appointmentTitle;
@property (strong, nonatomic) NSString *calendarType;
@property (strong, nonatomic) NSDate *startDateTime;
@property (strong, nonatomic) NSDate *endDateTime;
@property (strong, nonatomic) SProperty *property;
@property (strong, nonatomic) Surveyor *surveyor;

@property NSInteger pickerCellTag;
@property NSInteger startEndCellTag;

@end
