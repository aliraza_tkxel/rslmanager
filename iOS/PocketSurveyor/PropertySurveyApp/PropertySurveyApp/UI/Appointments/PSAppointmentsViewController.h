//
//  PSAppointmentsViewController.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "PSCustomViewController.h"
#import "PSMapsViewController.h"
#import "PSTableView.h"


@class PSAppointmentsFilterViewController;
@class PSAppointmentDetailViewController;
@class PSAppointmentCell;

@protocol PSAppointmentsFilterProtocol <NSObject>
@required
/*!
 @discussion
 Method "filterAppointmentsWithOptions" Called upon tapping view button. Provides the filterOptions.
 */
- (void) filterAppointmentsWithOptions:(NSArray *)filterOptions;
@required
/*!
 @discussion
 Method "loadInprogressOldAppointmentsFromDate" Called upon tapping "Load Appointment" button. Adds up old in progress appointments in the AppointmentsView
 */
-(void)loadInprogressOldAppointmentsFromDate:(NSDate*)from toDate:(NSDate*)to;

@end

@protocol PSAppointmentsAddressMapsProtocol <NSObject>
@required
/*!
 @discussion
 Method "addressTapped" Called upon tapping address button.
 */
- (void) addressTapped:(Appointment *)appointment;
@end



@interface PSAppointmentsViewController : PSCustomViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, PSAppointmentsFilterProtocol, EGORefreshTableHeaderDelegate,PSAppointmentsAddressMapsProtocol>
{
    PSAppointmentsFilterViewController *filterViewController;
    BOOL isSearching;
}
@property (strong,  nonatomic) NSPredicate *appointmentFilterPredicate;
@property (strong,  nonatomic)NSFetchedResultsController*fetchedResultsController;
@property (strong,  nonatomic) IBOutlet PSAppointmentCell *appointmentCell;

@property (strong,  nonatomic) IBOutlet PSTableView *tblAppointments;
@property (strong,  nonatomic) PSAppointmentDetailViewController *appointmentDetailViewConroller;
@property (strong, nonatomic) IBOutlet UIView *noAppointmentsHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *noAppointmentsLabel;


@end
