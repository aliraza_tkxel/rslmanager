//
//  PSAccomodationViewController.m
//  PropertySurveyApp
//
//  Created by My Mac on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAccomodationViewController.h"
#import "PSAccomodationViewCell.h"
#import "PSBarButtonItem.h"
@interface PSAccomodationViewController ()
{
	NSSortDescriptor *_sortDescriptor;
	NSArray *_sortDescriptors;
	BOOL _isModified;
}

@end

@implementation PSAccomodationViewController
@synthesize headerViewArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// Custom initialization
		self.isEditing = NO;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self registerNibsForTableView];
	
	self.headerViewArray = [NSMutableArray array];
	[self loadSectionHeader];
	[self loadNavigationonBarItems];
	_isModified = NO;
	if (!_sortDescriptor)
	{
		_sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kRoomName ascending:YES];
		_sortDescriptors = @[_sortDescriptor];
		
	}
	self.accomodationsArray = [NSMutableArray arrayWithArray:[self.appointment.appointmentToProperty.propertyToAccomodation allObjects]];
	self.accomodationsArray = [NSMutableArray arrayWithArray:[self.accomodationsArray sortedArrayUsingDescriptors:_sortDescriptors]];
}

-(void)registerNibsForTableView
{
	NSString * nibName;
	nibName = NSStringFromClass([PSAccomodationViewCell class]);
	[self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

-(void) viewWillAppear:(BOOL)animated
{
	[self registerKeyboardNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAccomodationInfoUpdateSuccessNotificationReceive) name:kAccomodationInfoUpdateSuccessNotification object:nil];
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kAccomodationInfoUpdateSuccessNotification object:nil];
	[self deregisterKeyboardNotifications];
}

#pragma mark - Notifications
- (void) onAccomodationInfoUpdateSuccessNotificationReceive
{
	//self.accomodationsArray = [NSMutableArray arrayWithArray:[self.accomodationsArray sortedArrayUsingDescriptors:_sortDescriptors]];
	// _isModified = NO;
	self.accomodationsArray = [NSMutableArray arrayWithArray:[self.appointment.appointmentToProperty.propertyToAccomodation allObjects]];
	self.accomodationsArray = [NSMutableArray arrayWithArray:[self.accomodationsArray sortedArrayUsingDescriptors:_sortDescriptors]];
	self.isDispatchReady = NO;
	[self.tableView reloadData];
}


#pragma mark - Navigation Bar

- (void) loadNavigationonBarItems
{
	PSBarButtonItem *backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																																			 style:PSBarButtonItemStyleBack
																																			target:self
																																			action:@selector(onClickBackButton)];
	
	self.editButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
																													 style:PSBarButtonItemStyleEdit
																													target:self
																													action:@selector(onClickEditButton)];
	self.saveButton = [[PSBarButtonItem alloc] initWithCustomStyle:LOC(@"KEY_STRING_SAVE")
																													 style:PSBarButtonItemStyleDefault
																													target:self
																													action:@selector(onClickSaveButton)];
	
	
	if ([self.appointment getStatus] != AppointmentStatusComplete)
	{
		if (self.isEditing)
		{
			[self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
		}
		
		else
		{
			[self setRightBarButtonItems:[NSArray arrayWithObjects:self.editButton, nil]];
		}
		
	}
	
	
	
	[self setLeftBarButtonItems:[NSArray arrayWithObjects:backButton, nil]];
	[self setTitle:LOC(@"KEY_STRING_ACCOMMODATIONS")];
}

#pragma mark Navigation Bar Button Selectors
- (void) onClickBackButton
{
	[self resignAllResponders];
	/* if (_isModified)
	 {
	 [[PSDataUpdateManager sharedManager]turnAccomodationsIntoFault:self.updatedAccomodations];
	 }
	 _isModified = NO;*/
	[self popOrCloseViewController];
}

- (void) onClickEditButton
{
	self.isEditing = YES;
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.saveButton, nil]];
	[self.tableView reloadData];
}
- (void) onClickSaveButton
{
	[self.selectedControl resignFirstResponder];
	self.isEditing = NO;
	self.isDispatchReady = YES;
	[self dispatchDataUpdates];
	[self.view endEditing:YES];
	[self setRightBarButtonItems:[NSArray arrayWithObjects:self.editButton, nil]];
	[self.tableView reloadData];
}

#pragma mark - UITableView Delegate Methods
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 30;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	
	return [self.headerViewArray objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.accomodationsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PSAccomodationViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PSAccomodationViewCell class])];
	cell.indexPath = indexPath;
	cell.delegate = self;
	[self configureCell:cell atIndexPath:indexPath];
	return cell;
}

- (void)configureCell:(PSAccomodationViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	[cell.txtArea setUserInteractionEnabled:NO];
	cell.txtArea.textColor = UIColorFromHex(FILTER_TABLEVIEW_HEADER_TEXT_COLOUR);
	cell.lblCellTitle.textColor = UIColorFromHex(APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR );
	cell.txtLength.textColor = UIColorFromHex(FILTER_TABLEVIEW_HEADER_TEXT_COLOUR );
	cell.txtWidth.textColor = UIColorFromHex(FILTER_TABLEVIEW_HEADER_TEXT_COLOUR );
	
	UIImageView *backgroundimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 277, 58)];
	backgroundimage.backgroundColor = [UIColor clearColor];
	backgroundimage.opaque = NO;
	
	if(indexPath.row %2 == 0)
	{
		backgroundimage.image = [UIImage imageNamed:@"cell_grey"];
	}
	else if (indexPath.row %2 == 1)
	{
		backgroundimage.image = [UIImage imageNamed:@"cell_white"];
	}
	
	backgroundimage.contentMode = cell.imageView.contentMode;
	cell.backgroundView = backgroundimage;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	
	if(self.isEditing)
	{
		if(indexPath.row %2 == 0)
		{
			UIImage *backgroundImage = [UIImage imageNamed:@"box_white"];
			cell.txtLength.layer.contents = (id)backgroundImage.CGImage;
			cell.txtWidth.layer.contents = (id)backgroundImage.CGImage;
			cell.txtArea.layer.contents = (id)backgroundImage.CGImage;
		}
		else if (indexPath.row %2 == 1)
		{
			UIImage *backgroundImage = [UIImage imageNamed:@"box_grey"];
			cell.txtLength.layer.contents = (id)backgroundImage.CGImage;
			cell.txtWidth.layer.contents = (id)backgroundImage.CGImage;
			cell.txtArea.layer.contents = (id)backgroundImage.CGImage;
			
		}
		[cell.txtLength setEnabled:YES];
		[cell.txtWidth setEnabled:YES];
	}
	else
	{
		UIImage *backgroundImage = nil;
		cell.txtLength.layer.contents = (id)backgroundImage.CGImage;
		cell.txtWidth.layer.contents = (id)backgroundImage.CGImage;
		cell.txtArea.layer.contents = (id)backgroundImage.CGImage;
		[cell.txtLength setEnabled:NO];
		[cell.txtWidth setEnabled:NO];
	}
	
	double area = 00.00;
	NSString *areaString = @"0.00";
	if (!isEmpty(self.accomodationsArray))
	{
		Accomodation *accomodation = [self.accomodationsArray objectAtIndex:indexPath.row];
		NSDictionary *updatedAccomodation = self.updatedAccomodations[accomodation.objectID];
		if(accomodation != nil)
		{
			cell.lblCellTitle.text = accomodation.roomName;
			if(!isEmpty(updatedAccomodation)){
				if(!isEmpty(updatedAccomodation[@"roomLength"]))
				{
					cell.txtLength.text = [updatedAccomodation[@"roomLength"] stringValue];
				}
				if(!isEmpty(updatedAccomodation[@"roomWidth"]))
				{
					cell.txtWidth.text = [updatedAccomodation[@"roomWidth"] stringValue];
				}
			}
			else
			{
				if(!isEmpty(accomodation.roomLength))
				{
					cell.txtLength.text = [accomodation.roomLength stringValue];
				}
				if(!isEmpty(accomodation.roomWidth))
				{
					cell.txtWidth.text = [accomodation.roomWidth stringValue];
				}
			}
			
			if (!isEmpty(cell.txtWidth.text) && !isEmpty(cell.txtLength.text))
			{
				area = [cell.txtWidth.text floatValue] * [cell.txtLength.text floatValue];
				areaString = [NSString stringWithFormat:@"%0.2lf",area];
				cell.txtArea.text = areaString;
			}
			else
			{
				cell.txtArea.text = areaString;
			}
		}
	}
	
	cell.txtWidth.delegate = self;
	cell.txtLength.delegate = self;
	
	cell.txtWidth.tag = indexPath.row;
	cell.txtLength.tag = indexPath.row;
	
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	CLS_LOG(@"");
	[self resignAllResponders];
}

#pragma mark - Scroll Text Field Methods
- (void) scrollToRectOfSelectedControl
{
	CLS_LOG(@"");
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}


#pragma mark - Functions

- (void) loadSectionHeader {
	
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,20)];
	view.backgroundColor = UIColorFromRGB(255, 255, 255);
	UILabel *labelMeters = [[UILabel alloc] init];
	UILabel *labelWidth = [[UILabel alloc] init];
	UILabel *labelLength = [[UILabel alloc] init];
	UILabel *labelArea = [[UILabel alloc] init];
	labelMeters.frame = CGRectMake(245, 5, 100,25);
	labelWidth.frame = CGRectMake(120, 5, 60,25);
	labelLength.frame = CGRectMake(182, 5, 60,25);
	labelArea.frame = CGRectMake(12, 5, 55,25);
	
	labelMeters.backgroundColor = [UIColor clearColor];
	labelWidth.backgroundColor = [UIColor clearColor];
	labelLength.backgroundColor = [UIColor clearColor];
	labelArea.backgroundColor = [UIColor clearColor];
	
	[labelMeters setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[labelWidth setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[labelLength setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	[labelArea setTextColor:UIColorFromHex(SECTION_HEADER_TEXT_COLOUR)];
	
	labelMeters.font = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17];
	labelWidth.font = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17];
	labelLength.font = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17];
	labelArea.font = [UIFont fontWithName:kFontFamilyHelveticaNeueBold size:17];
	
	[labelMeters setTextAlignment:PSTextAlignmentLeft];
	[labelWidth setTextAlignment:PSTextAlignmentCenter];
	[labelLength setTextAlignment:PSTextAlignmentCenter];
	[labelArea setTextAlignment:PSTextAlignmentCenter];
	
	labelMeters.text =LOC(@"KEY_STRING_METERS");
	[view addSubview:labelMeters];
	
	labelWidth.text =LOC(@"KEY_STRING_WIDTH");
	[view addSubview:labelWidth];
	
	labelLength.text = LOC(@"KEY_STRING_LENGTH");
	[view addSubview:labelLength];
	
	labelArea.text = LOC(@"KEY_STRING_AREA");
	[view addSubview:labelArea];
	
	[self.headerViewArray addObject:view];
}

#pragma mark - TextField Delegate Methods

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	CLS_LOG(@"");
	if ([textField.text isEqualToString: @"0"]) {
		textField.text = @"";
	}
	self.selectedControl = (UITextField *)textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	CLS_LOG(@"");
	if ([textField.text isEqualToString: @""]) {
		textField.text = @"0";
	}
	self.selectedControl = (UITextField *)textField;
	
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
	PSAccomodationViewCell *cell = (PSAccomodationViewCell*) [self.tableView cellForRowAtIndexPath:indexPath];
	
	Accomodation *accomodation = [self.accomodationsArray objectAtIndex:indexPath.row];
	NSNumber *roomWidth = [NSNumber numberWithDouble:[cell.txtWidth.text doubleValue]];
	NSNumber *roomLength = [NSNumber numberWithDouble:[cell.txtLength.text doubleValue]];
	NSManagedObjectID *objectId = accomodation.objectID;
	if(!_updatedAccomodations)
	{
		self.updatedAccomodations = [NSMutableDictionary dictionary];
	}
	[self.updatedAccomodations setObject:[NSDictionary dictionaryWithObjectsAndKeys:
																				roomWidth,kRoomWidth,
																				roomLength,kRoomLength,
																				nil]
																forKey:objectId];
	if (self.isDispatchReady)
	{
		[self dispatchDataUpdates];
	}
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	CLS_LOG(@"");
	[textField resignFirstResponder];
	return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	
	CGFloat areaWidth = 0;
	CGFloat areaLength = 0;
	double area = 0;
	_isModified = YES;
	BOOL isBackspaceHit = NO;
	NSIndexPath *indexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
	NSString *stringAfterReplacement = [((UITextField *)self.selectedControl).text stringByAppendingString:string];
	if([string isEqualToString:@""] && !isEmpty(textField.text)) //Handle Backspace Here
	{
		isBackspaceHit = YES;
		stringAfterReplacement = [textField.text substringToIndex:textField.text.length - 1];
	}
	if(isEmpty(stringAfterReplacement))
	{
		stringAfterReplacement = @"00.00";
	}
	
	PSAccomodationViewCell *cell = (PSAccomodationViewCell*) [self.tableView cellForRowAtIndexPath:indexPath];
	
	NSInteger stringLength = [stringAfterReplacement length];
	
	if (stringLength < 6 || isBackspaceHit)
	{
		//if(stringLength >= 3)
		{
			//Calculate Area Here
			if(cell.txtLength == self.selectedControl){
				areaLength = [stringAfterReplacement floatValue];
				areaWidth = [cell.txtWidth.text floatValue];
			}
			else if (cell.txtWidth ==self.selectedControl){
				areaWidth = [stringAfterReplacement floatValue];
				areaLength = [cell.txtLength.text floatValue];
			}
			area = areaLength * areaWidth;
			NSNumber *areaVal = [NSNumber numberWithDouble:area];
			cell.txtArea.text = [areaVal stringValue];
		}
		return YES;
	}
	return NO;
}

#pragma mark - Dispatch Data Updates
- (void) dispatchDataUpdates
{
	@synchronized(self)
	{
		if(!isEmpty(self.updatedAccomodations))
		{
			[[PSDataUpdateManager sharedManager] updateAccomodations:self.updatedAccomodations];
			self.isDispatchReady = NO;
			self.updatedAccomodations = nil;
		}
		[self.tableView reloadData];
	}
}

#pragma mark - PSAccomodationProtocol

- (void) didChangeAccomodationsAtIndexPath:(NSIndexPath *)indexPath
{
	// CLS_LOG(@"Area Changed At IndexPath: %@", indexPath);
}

@end
