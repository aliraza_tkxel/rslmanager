//
//  PSAccomodationCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSAccomodationViewController.h"

@interface PSAccomodationViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCellTitle;
@property (strong, nonatomic) IBOutlet UITextField *txtWidth;
@property (strong, nonatomic) IBOutlet UITextField *txtLength;
@property (strong, nonatomic) IBOutlet UITextField *txtArea;
@property (weak,   nonatomic) id<PSAccomodationProtocol> delegate;
@property (strong,   nonatomic) NSIndexPath *indexPath;

- (IBAction)resignFirstResponder:(id)sender;
@end
