//
//  PSCameraHeaderView.h
//  PropertySurveyApp
//
//  Created by TkXel on 11/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PSCameraHeaderViewDelegate <NSObject>

-(void) onClickBtnCamera;

@end


@interface PSCameraHeaderView : UICollectionReusableView


@property(nonatomic,weak) id<PSCameraHeaderViewDelegate> cameraDelegate;

@end
