//
//  PSCollectionViewCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 18/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImage;

@end
