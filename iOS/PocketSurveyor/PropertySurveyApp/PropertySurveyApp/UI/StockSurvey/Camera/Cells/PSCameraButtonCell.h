//
//  PSCameraButtonCell.h
//  PropertySurveyApp
//
//  Created by Yawar on 23/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSCameraButtonCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnCamera;

@end
