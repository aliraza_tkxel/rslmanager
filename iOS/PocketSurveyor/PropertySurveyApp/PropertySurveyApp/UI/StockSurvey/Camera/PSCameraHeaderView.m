//
//  PSCameraHeaderView.m
//  PropertySurveyApp
//
//  Created by TkXel on 11/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCameraHeaderView.h"

@interface PSCameraHeaderView ()



@end

@implementation PSCameraHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIButton * cameraBtn=[[UIButton alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
        
        [cameraBtn setImage:[UIImage imageNamed:@"btn_BarCamera.png"] forState:(UIControlStateNormal)];
        
        [cameraBtn addTarget:self action:@selector(cameraBtnPressed:) forControlEvents:(UIControlEventTouchUpInside)];
        
        [self addSubview:cameraBtn];
        
        //self.cameraDelegate=cameraHeaderDelegate;
        // Initialization code
    }
    
    return self;
}

-(IBAction) cameraBtnPressed:(id)sender
{
    if ([self.cameraDelegate respondsToSelector:@selector(onClickBtnCamera)]) {
     
        [self.cameraDelegate onClickBtnCamera];
        
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
