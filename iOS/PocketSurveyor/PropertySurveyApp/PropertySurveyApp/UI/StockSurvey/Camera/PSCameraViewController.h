//
//  PSCameraViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSBarButtonItem.h"
#import "PSCameraHeaderView.h"



@class PSSurveyViewController;

@interface PSCameraViewController : PSCustomViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,PSCameraHeaderViewDelegate,UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,retain) NSMutableArray*  propertyPictures;
@property(assign) int selectedIndex;

- (IBAction)onClickBtnCamera:(id)sender;
@property (weak,   nonatomic) PSSurveyViewController *mainViewController;
@property (strong, nonatomic) PSBarButtonItem *homeButton;
@property (strong, nonatomic) PSBarButtonItem *cameraButton;
@property (strong, nonatomic) PSBarButtonItem *notesButton;
@property (strong, nonatomic) Appointment *appointment;
@property (strong, nonatomic) Defect *defect;
@property (assign, nonatomic) CameraViewImageTypeTag imageType;
@property (assign, nonatomic) NSString * heatingId; /*Can be null for non-heating Items.*/
@property (strong, nonatomic) NSNumber *itemId;
-(void) showCameraOfSourceType:(UIImagePickerControllerSourceType) type;
@end
