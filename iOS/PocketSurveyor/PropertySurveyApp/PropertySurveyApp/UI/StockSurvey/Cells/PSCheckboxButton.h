//
//  PSCheckboxButton.h
//  PropertySurveyApp
//
//  Created by TkXel on 18/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

#define LeftPadding     5
#define RightPadding    5
#define CheckBoxWidth   35

#define CheckBoxButtonHeight 35

@interface PSCheckboxButton : UIButton
@property (assign, nonatomic, setter=setCheckBoxSelected:) BOOL isCheckBoxSelected;
@property (strong, nonatomic) NSNumber *itemParamId;
@property (strong, nonatomic) NSNumber *paramId;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title checkBoxSelected:(BOOL)selected;
- (void) makeSelected;
- (void) makeDeselected;
- (void) toggle;
@end

