//
//  PSSurveyDropdownCell.h
//  PropertySurveyApp
//
//  Created by TkXel on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSSurveyViewController.h"

@class FormData;
@interface PSSurveyDropdownCell : UITableViewCell

@property (weak,   nonatomic) id<SurveyFormDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblValue;

@property (strong, nonatomic) NSString *surveyParamName;
@property (strong, nonatomic) NSDictionary *dataDictionary;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (nonatomic, strong) FormData *formData;

- (void) loadCellData;
- (IBAction)onClickOptionPickerButton:(UIButton *)sender;

@end
