//
//  PSSurveyDateCell.h
//  PropertySurveyApp
//
//  Created by TkXel on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSSurveyViewController.h"

@class FormData;
@interface PSSurveyDateCell : UITableViewCell
#warning Delegate deallocation hot fix
@property (strong,   nonatomic) id<SurveyFormDelegate> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UIButton *btnDatePicker;

@property (strong, nonatomic) NSString *surveyParamName;
@property (strong, nonatomic) NSDictionary *dataDictionary;
@property (strong, nonatomic) NSMutableDictionary *formDataDictionary;
@property (nonatomic, weak) FormData *formData;

- (void) loadCellData;

- (void) reloadData:(NSDate*) date;

- (IBAction)onClickDatePickerButton:(UIButton *)sender;

@end
