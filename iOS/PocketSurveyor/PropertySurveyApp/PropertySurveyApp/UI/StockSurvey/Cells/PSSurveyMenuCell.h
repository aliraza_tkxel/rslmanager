//
//  PSSurveyMenuCell.h
//  PropertySurveyApp
//
//  Created by TkXel on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSurveyMenuCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (strong, nonatomic) IBOutlet UIImageView *imgStatus;

@end
