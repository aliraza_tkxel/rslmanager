//
//  PSSurveyDropdownCell.m
//  PropertySurveyApp
//
//  Created by TkXel on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyDropdownCell.h"

@interface PSSurveyDropdownCell ()

@property (assign, nonatomic) NSInteger selectedIndex;
@property (strong, nonatomic) NSArray *pickerOptions;

@property (strong, nonatomic) NSMutableDictionary *formDataDictionary;

@end

@implementation PSSurveyDropdownCell

int sortingFunction(id item1, id item2, void * context){
    NSString *firstItem = [(NSString *)item1 lowercaseString];
    NSString *secondItem = [(NSString *)item2 lowercaseString];
	return [firstItem  compare:secondItem];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
    
	// Configure the view for the selected state
}

#pragma mark Methods
- (void)loadCellData {
	if (!self.formDataDictionary) {
		self.formDataDictionary = [NSMutableDictionary dictionary];
	}
    
	NSMutableDictionary *selectedJSON = [self.dataDictionary objectForKey:@"Selected"];
	NSMutableDictionary *valuesJSON = [self.dataDictionary objectForKey:@"Values"];
    
    
	NSNumber *surveyItemParamId = [self.dataDictionary objectForKey:@"itemParamId"];
	NSNumber *surveyParameterId = [self.dataDictionary objectForKey:@"paramId"];
	NSString *controlType = [self.dataDictionary objectForKey:@"Type"];
    
	[self.formDataDictionary setObject:surveyItemParamId forKey:@"surveyItemParamId"];
	[self.formDataDictionary setObject:surveyParameterId forKey:@"surveyParameterId"];
	[self.formDataDictionary setObject:controlType forKey:@"controlType"];
    [self.formDataDictionary setObject:self.surveyParamName forKey:@"surveyParamName"];
    
    NSNumber *readOnly = [self.dataDictionary objectForKey:@"ReadOnly"];
    [self.btnMenu setHidden:[readOnly boolValue]];

    
    NSMutableArray * valueOptionKeys = [NSMutableArray arrayWithArray:[valuesJSON keysSortedByValueUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    valueOptionKeys = [NSMutableArray arrayWithArray:[valueOptionKeys sortedArrayUsingFunction:sortingFunction context:nil]];
   
    
    
    NSString *notAppliacable = LOC(@"KEY_STRING_N/A");
    NSString *nApplicable = LOC(@"KEY_STRING_NOT_APPLICABLE");
    NSString *pleaseSelect = LOC(@"KEY_STRING_PLEASE_SELECT");

    if ([valueOptionKeys containsObject:notAppliacable] || [valueOptionKeys containsObject:nApplicable])
    {
        #warning Moving "N/A" from last index to first index
        NSInteger valueIndex = [valueOptionKeys indexOfObject:notAppliacable];
        if (valueIndex == NSNotFound)
        {
            valueIndex = [valueOptionKeys indexOfObject:nApplicable];
        }
        
        [valueOptionKeys insertObject:[valueOptionKeys objectAtIndex:valueIndex] atIndex:0];
        [valueOptionKeys removeObjectAtIndex:valueIndex + 1];
    }
    else if ([valueOptionKeys containsObject:pleaseSelect]) {
        #warning Moving "Please Select" to first index
        NSInteger valueIndex = [valueOptionKeys indexOfObject:pleaseSelect];
        [valueOptionKeys insertObject:[valueOptionKeys objectAtIndex:valueIndex] atIndex:0];
        [valueOptionKeys removeObjectAtIndex:valueIndex + 1];
    }
    
    
    self.pickerOptions = valueOptionKeys;
	NSString *selectedValue = nil;
	if (isEmpty(selectedJSON)) {
		selectedValue = LOC(@"KEY_STRING_PLEASE_SELECT");
		self.lblValue.text = selectedValue;
	}
	else {
		NSString *surveyParamItemFieldValue = [self.dataDictionary objectForKey:@"paramId"];
		NSString *surveyPramItemFieldId = [self.dataDictionary objectForKey:@"paramId"];

        
        
        selectedValue = [[selectedJSON allKeys] firstObject];
      //  selectedValue = [[selectedJSON allKeys] objectAtIndex:0];
        
		self.lblValue.text = selectedValue;
		self.selectedIndex = [[valuesJSON allKeys] indexOfObject:selectedValue];
#warning Define Rules for selected values for Drop down and Textbox, currently wrong index is being selected

        if(self.selectedIndex == NSNotFound)
        {//
            CLS_LOG(@"Break");
            NSDictionary *selectedValueDictionary = [selectedJSON objectForKey:[[selectedJSON allKeys] objectAtIndex:0]];
            NSNumber *selectedId = [selectedValueDictionary objectForKey:@"id"];
            NSArray *objectKeys = [valuesJSON allKeysForObject:selectedId];
            if(!isEmpty(objectKeys))
            {
                selectedValue = [objectKeys objectAtIndex:0];
                self.lblValue.text = selectedValue;
                self.selectedIndex = [valueOptionKeys indexOfObject:selectedId];
            }
        }
        else
        {
                self.lblValue.text = selectedValue;
                self.selectedIndex = [[valuesJSON allKeys] indexOfObject:selectedValue];
        }
            
            
        
		[self.formDataDictionary setObject:surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
		[self.formDataDictionary setObject:surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
		[self.formDataDictionary setObject:@"" forKey:@"isSelected"];
	}
    
	self.lblTitle.text = [self.surveyParamName uppercaseString];
    
   
	if (self.formData) {
	
        self.lblValue.text = self.formData.surveyParamItemFieldValue;
		self.selectedIndex = [valueOptionKeys indexOfObject:self.formData.surveyParamItemFieldValue];
        
	}
    
}

#pragma mark Picker View
- (IBAction)onClickOptionPickerButton:(UIButton *)sender {
	if (!isEmpty(self.pickerOptions)) {
        
        self.selectedIndex = [self.pickerOptions indexOfObject:self.lblValue.text];
        if(self.selectedIndex >= 0 && (self.selectedIndex < [self.pickerOptions count]))
        {
        }
        else
        {
            self.selectedIndex = 0;
        }
		[ActionSheetStringPicker showPickerWithTitle:self.lblTitle.text
		                                        rows:self.pickerOptions
		                            initialSelection:self.selectedIndex
		                                      target:self
		                               successAction:@selector(onOptionSelected:element:)
		                                cancelAction:@selector(onOptionPickerCancelled:)
		                                      origin:sender];
	}
}

- (void)onOptionSelected:(NSNumber *)selectedIndex element:(id)element {
	self.selectedIndex = [selectedIndex integerValue];
    if(self.selectedIndex != NSNotFound)
    {
        if(self.selectedIndex >= 0 && (self.selectedIndex < [self.pickerOptions count]))
        {
            //[stringPicker selectRow:self.selectedIndex inComponent:0 animated:NO];
        }
        else
        {
            //[stringPicker selectRow:([self.data count] - 1) inComponent:0 animated:NO];
            self.selectedIndex = 0;//([self.pickerOptions count] - 1);
        }
    }else
    {
        self.selectedIndex = 0;//([self.pickerOptions count] - 1);
    }

	NSString *selectedValue = [self.pickerOptions objectAtIndex:self.selectedIndex];
	self.lblValue.text = selectedValue;
    
	NSMutableDictionary *valuesJSON = [self.dataDictionary objectForKey:@"Values"];
    
	if (!isEmpty(valuesJSON)) {
		NSString *surveyParamItemFieldValue = selectedValue;
		NSString *surveyPramItemFieldId = [valuesJSON valueForKey:selectedValue];
		[self.formDataDictionary setObject:surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
		[self.formDataDictionary setObject:surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
	}
    
	//Propogate the Change Call Back
	if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeSurveyFormWithData:)]) {
		[self.delegate performSelector:@selector(didChangeSurveyFormWithData:) withObject:self.formDataDictionary];
	}
}


- (void)onOptionPickerCancelled:(id)sender
{
	CLS_LOG(@"Delegate has been informed that ActionSheetPicker was cancelled");
}

@end
