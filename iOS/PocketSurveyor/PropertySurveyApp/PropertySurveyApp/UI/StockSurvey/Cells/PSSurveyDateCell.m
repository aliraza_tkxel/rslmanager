//
//  PSSurveyDateCell.m
//  PropertySurveyApp
//
//  Created by TkXel on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyDateCell.h"
#import "SurveyData+CoreDataClass.h"
#import "SurveyData+DataPersistance.h"
#import "FormData.h"

@interface PSSurveyDateCell ()

@property (strong, nonatomic) NSDate *selectedDate;
@property (nonatomic, retain) ActionSheetDatePicker * picker;

@end

@implementation PSSurveyDateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
		// Initialization code
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
    
	// Configure the view for the selected state
}

- (IBAction)onClickDatePickerButton:(UIButton *)sender {
    
	if (self.selectedDate == nil) {
		self.selectedDate = [NSDate date];
	}
    
    self.picker =  [ActionSheetDatePicker showPickerWithTitle:self.lblTitle.text
                                               datePickerMode:UIDatePickerModeDate
                                                 selectedDate:self.selectedDate
                                                       target:self
                                                       action:@selector(onDateSelected:element:)
                                                       origin:sender];
}

- (void)onDateSelected:(NSDate *)selectedDate element:(id)element {
	self.selectedDate = selectedDate;
	self.lblDate.text = [UtilityClass stringFromDate:self.selectedDate dateFormat:kDateTimeStyle8];
	NSString *surveyParamItemFieldValue = self.lblDate.text;
    
    [self.formDataDictionary setObject:surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
    
	//Propogate the Change Call Back
	if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeSurveyFormDateWithData:)]) {
		[self.delegate performSelector:@selector(didChangeSurveyFormDateWithData:) withObject:self.formDataDictionary];
	}
    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(didChangeSurveyFormWithData:)]) {
//		[self.delegate performSelector:@selector(didChangeSurveyFormWithData:) withObject:self.formDataDictionary];
//	}
    
}

#pragma mark Methods
- (void)loadCellData {

	if (!self.formDataDictionary) {
		self.formDataDictionary = [NSMutableDictionary dictionary];
	}
    
	NSMutableDictionary *selectedJSON = [self.dataDictionary objectForKey:@"Selected"];
//	NSMutableDictionary *valuesJSON = [self.dataDictionary objectForKey:@"Values"];
    
    
	NSNumber *surveyItemParamId = [self.dataDictionary objectForKey:@"itemParamId"];
	NSNumber *surveyParameterId = [self.dataDictionary objectForKey:@"paramId"];
	NSString *controlType = [self.dataDictionary objectForKey:@"Type"];
    
    NSNumber *readOnly = [self.dataDictionary objectForKey:@"ReadOnly"];
    [self.btnDatePicker setHidden:[readOnly boolValue]];

    
	controlType = isEmpty(controlType) ? @"" : controlType;
    
#warning Survey date cell; temporary fix itemParamId
	[self.formDataDictionary setObject:isEmpty(surveyItemParamId) ? @-1:surveyItemParamId forKey:@"surveyItemParamId"];
	[self.formDataDictionary setObject:isEmpty(surveyParameterId) ? @-1:surveyParameterId forKey:@"surveyParameterId"];
	[self.formDataDictionary setObject:controlType forKey:@"controlType"];
    [self.formDataDictionary setObject:self.surveyParamName forKey:@"surveyParamName"];
    
    
	NSString *selectedValue = nil;
	if (isEmpty(selectedJSON)) {
		selectedValue = LOC(@"KEY_STRING_PLEASE_SELECT");
		self.lblDate.text = selectedValue;
	}
	else {
		selectedValue = [[selectedJSON allKeys] objectAtIndex:0];
		//self.lblDate.text = selectedValue;
#warning Survey date cell; date time formate should be consistent to dateTimStyle16
		self.selectedDate = [UtilityClass dateFromString:selectedValue dateFormat:kDateTimeStyle16];
		self.lblDate.text = [UtilityClass stringFromDate:self.selectedDate dateFormat:kDateTimeStyle8];
        
		NSString *surveyParamItemFieldValue = selectedValue;
		NSNumber *surveyPramItemFieldId = [[selectedJSON objectForKey:selectedValue] objectForKey:@"id"];
        
		[self.formDataDictionary setObject:isEmpty(surveyPramItemFieldId) ? @"":surveyPramItemFieldId forKey:@"surveyPramItemFieldId"];
		[self.formDataDictionary setObject:isEmpty(surveyParamItemFieldValue) ? @"":surveyParamItemFieldValue forKey:@"surveyParamItemFieldValue"];
		[self.formDataDictionary setObject:@"" forKey:@"isSelected"];
	}
    
	self.lblTitle.text = [self.surveyParamName uppercaseString];
    
	//if (self.surveyData) {
	//FormData *formData = [self.surveyData formDataWithItemId:[surveyItemParamId stringValue]];
    
	if (self.formData) {
		self.lblDate.text = self.formData.surveyParamItemFieldValue;
		self.selectedDate = [UtilityClass dateFromString:self.lblDate.text dateFormat:kDateTimeStyle8];
	}
	//}
}

- (void) reloadData:(NSDate*) date
{
    self.lblDate.text=[UtilityClass stringFromDate:date dateFormat:kDateTimeStyle8];
    self.selectedDate=date;
}

@end
