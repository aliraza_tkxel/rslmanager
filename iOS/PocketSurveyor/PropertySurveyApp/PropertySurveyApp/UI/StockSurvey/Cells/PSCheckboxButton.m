//
//  PSCheckboxButton.m
//  PropertySurveyApp
//
//  Created by TkXel on 18/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCheckboxButton.h"

@interface PSCheckboxButton ()
{
@private
	UIImage *_imgMarkedCheckbox;
	UIImage *_imgUnmarkedCheckbox;
    
	NSString *_title;
}
@end

@implementation PSCheckboxButton

- (id)initWithFrame:(CGRect)frame title:(NSString *)title checkBoxSelected:(BOOL)selected {
	self = [super initWithFrame:frame];
	if (self) {
		// Initialization code
		_title = title;
		[self setTitle:_title forState:UIControlStateNormal];
		[self setTitle:_title forState:UIControlStateHighlighted];
		[self setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateNormal];
		[self setTitleColor:UIColorFromHex(THEME_TEXT_COLOUR_DARK) forState:UIControlStateHighlighted];
		self.titleLabel.font = [UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15];
        
		self.isCheckBoxSelected = selected;
        
        CGSize labelSize = CGSizeZero;
        if(OS_VERSION >= 7.0)
        {
            labelSize = [_title sizeWithAttributes:[NSDictionary dictionaryWithObject:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15] forKey:NSFontAttributeName]];
        }
        else
        {
            labelSize = [_title sizeWithFont:[UIFont fontWithName:kFontFamilyHelveticaNeueMedium size:15]];
        }
        
		CGRect frame = self.frame;
		self.frame = CGRectMake(frame.origin.x,
		                        frame.origin.y,
		                        CheckBoxWidth + LeftPadding + RightPadding + labelSize.width,
		                        CheckBoxButtonHeight);
        
		self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
		_imgMarkedCheckbox = [UIImage imageNamed:@"checkbox_marked"];
		_imgUnmarkedCheckbox = [UIImage imageNamed:@"checkbox_unmark"];
		if (self.isCheckBoxSelected) {
			[self makeSelected];
		}
		else {
			[self makeDeselected];
		}
	}
	return self;
}

- (void)makeSelected {
	self.isCheckBoxSelected = YES;
	[self setImage:_imgMarkedCheckbox forState:UIControlStateNormal];
}

- (void)makeDeselected {
	self.isCheckBoxSelected = NO;
	[self setImage:_imgUnmarkedCheckbox forState:UIControlStateNormal];
}

- (void)toggle {
	self.isCheckBoxSelected = !self.isCheckBoxSelected;
	if (self.isCheckBoxSelected) {
		[self makeSelected];
	}
	else {
		[self makeDeselected];
	}
}

@end
