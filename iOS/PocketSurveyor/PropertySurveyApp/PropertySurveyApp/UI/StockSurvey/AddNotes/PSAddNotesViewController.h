//
//  PSAddNotesViewController.h
//  PropertySurveyApp
//
//  Created by Yawar on 17/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTextView.h"
#import "PSBarButtonItem.h"
#import "PSSurveyViewController.h"

@interface PSAddNotesViewController : PSCustomViewController<UITextViewDelegate>
{
    BOOL isKeyboardShown;
}

@property (nonatomic, copy) NSString *notesText;
@property (weak,   nonatomic) id<SurveyFormDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITextView *txtViewNote;
@property (strong, nonatomic) PSBarButtonItem *homeButton;
@property (strong, nonatomic) PSBarButtonItem *cameraButton;
@property (strong, nonatomic) PSBarButtonItem *notesButton;

@end
