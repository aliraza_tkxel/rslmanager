//
//  GenericViewController.h
//  PropertySurveyApp
//
//  Created by My Mac on 6/15/11.
//  Copyright 2011 PCC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarEvent.h"

//main keys
#define	TYPE				@"Type"
#define MENU_TYPE			@"Menu"
#define FORM_TYPE			@"Form"
#define FIELDS				@"Fields"
#define LOCAL_APPLIANCE_FIELD @"localField"
#define HAS_IMAGE			@"Image"
#define HAS_LABEL			@"NameAddressLabel"

//table sizes
#define IMAGE_TABLE			@"{{  0,135},{320,325}}"
#define LABEL_TABLE			@"{{  0, 57},{320,390}}"
#define SIMPLE_TABLE		@"{{  0,  0},{320,450}}"
#define NO_TABLE			@"no table"

#define CELL_VALUES			@"Values"

//Cell types
#define DROPDOWN_CELL		@"Dropdown" //@"DropDown"
#define TEXTFIELD_CELL		@"Textbox" //@"TextField"
#define DATETIME_CELL		@"Date" //DateTime
#define CHECKBOX_CELL		@"CheckBox"
#define YES_NO_CELL			@"YesNo"
#define COMPOSITE_CELL		@"Composite"
#define COMPOSITE_CELL_TYPE	@"Composite Type"
#define DROPDOWN_DATETIME	@"DropDownDateTime"
#define DROPDOWN_CHECKBOX	@"DropDownCheckBox"
#define CHECKBOXES			@"CheckBoxes"

//cell sizes
#define DEFAULT_CELL_SIZE	 44
#define CUSTOM_CELL_SIZE	 57
#define COMPOSITE_CELL_SIZE	104

#define TICK_IMG			@"tick5.png"
#define CROSS_IMG			@"cross5.png"

@interface GenericViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>
{
	NSDictionary * dictData;
	NSMutableDictionary * surveyDataFields;
	NSMutableDictionary * surveyData;
	NSMutableDictionary * nextSurveyData;
	NSString * parentTitle;
	NSString * formPath;
	NSString * jsonRequest;
	
	NSMutableDictionary * adaptations;
	
	IBOutlet UITableView * genericTableView;

	IBOutlet UILabel * imageNameLbl;
    IBOutlet UILabel * imageAddressLbl;
    IBOutlet UILabel * stockConditionLbl;
    IBOutlet UIImageView * propertyImageView;
	
	IBOutlet UILabel * nameLbl;
    IBOutlet UILabel * addressLbl;
	IBOutlet UILabel * customerNameLbl;
    IBOutlet UILabel * customerAddressLbl;
	
	IBOutlet UIButton * notesButton;
	IBOutlet UIButton * cameraButton;
	
	IBOutlet UILabel * sectionNotAvailableLabel;
	IBOutlet UIActivityIndicatorView * activityIndicator;
    
    NSString * completeTitle;
    
    BOOL isMainSurveyMenu;
    BOOL isLocalFieldScreen;
    
    NSString * startString;
    
    NSArray * fields;
    int noOfAppliances;
    NSString * itemId;
    
    NSMutableDictionary * ticksDictionay;
}

@property (nonatomic, retain) CalendarEvent * calenderEvent;

@property (nonatomic, assign) int selectedCalenderEventIndex;
@property (nonatomic, retain) NSString * formPath;
//Changed By Aaban
@property (nonatomic, retain) NSNumber * totalNumberOfTicks;
@property (nonatomic, retain) NSString * propertyId;
//Ends
@property (nonatomic, retain) NSString * appointmentType;
@property (nonatomic, retain) NSString * jsonRequest;
@property (nonatomic, retain) NSString * completeTitle;
@property (readwrite) BOOL isMainSurveyMenu;
@property (nonatomic, retain) NSArray * fields;

- (IBAction)notesButtonPressed;
- (IBAction)cameraButtonPressed;

- (id)initWithTitle:(NSString *)viewTitle 
		parentTitle:(NSString *)parentT 
		withViewTitles:(NSMutableDictionary *)dict
		withData:(NSMutableDictionary *)surveyDataDict
		prevData:(NSMutableDictionary *)prevDataDict
		withKey:(NSString *)prevDictKey;

- (void)relocateTable:(NSString *)type;
- (void)setChecks:(NSString *)type;
- (BOOL)isCustomCell:(NSString *)type;
- (UITableViewCell *)initCustomCell:(UITableViewCell *)cell withDict:(NSDictionary *)viewCells withReuseIdentifier:(NSString*)identifier  andIndexPath:(NSIndexPath *)indexPath;
- (void)getSurveyData:(NSMutableDictionary *)dict;
- (NSString *)getFormIndexPath;

@end
