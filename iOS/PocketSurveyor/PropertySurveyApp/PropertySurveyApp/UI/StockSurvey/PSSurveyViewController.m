//
//  PSSurveyViewController.m
//  PropertySurveyApp
//
//  Created by TkXel on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyViewController.h"
#import "PSSurveyMenuCell.h"
#import "PSSurveyDropdownCell.h"
#import "PSBarButtonItem.h"

#import "PSSurveyCheckboxCell.h"
#import "PSSurveyMenuCell.h"
#import "PSSurveyDropdownCell.h"
#import "PSSurveyTextboxCell.h"
#import "PSSurveyDateCell.h"
#import "PSSurveyTextViewCell.h"

#import "PSCoreDataManager.h"
#import "Survey+DataPersistance.h"
#import "SurveyData+DataPersistance.h"
#import "PSCameraViewController.h"
#import "PSAddNotesViewController.h"
#import "Survey+JSON.h"
#import "Appointment+JSON.h"
#import "PSAppointmentDetailViewController.h"
#import "PSDetectorsViewController.h"
#import "PropertyPicture+MWPhoto.h"
#import "Component+CoreDataProperties.h"
#import "Component+CoreDataClass.h"
#import "SurveyHeatingFuelData+CoreDataClass.h"
#import "SurveyHeatingFuelData+CoreDataProperties.h"

#pragma mark - Local Macros

#define kFilledFields @"FilledFields"
#define kTotalFields @"TotalFields"

#define kComponentLifeCycleForSurvey    @"Lifecycle"
#define kComponentAccountingForSurvey   @"Component Accounting"
#define kComponentNameForSurvey         @"Component Name"
#define kSurveyKeyFields                @"Fields"
#define kSurveyDataFieldLastReplaced    @"Last Replaced"
#define kSurveyDataFieldReplacementDue  @"Replacement Due"

@interface PSSurveyViewController ()
{
    BOOL hideNotes_;
    int currentNoOfRows_;
}

@property(nonatomic,strong) Survey *currentSurvey;



@property (strong,nonatomic) NSArray *selectedComponents;

@end

@implementation PSSurveyViewController

@synthesize surveyData=surveyData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (id)initWithTitle:(NSString *)title surveyData:(NSMutableDictionary *)surveyDataDictionary {
	self = [super initWithNibName:@"PSSurveyViewController" bundle:[NSBundle mainBundle]];
	if (self) {
		// Custom initialization
        
		surveyTitle = title;
		surveyData = [NSMutableDictionary dictionaryWithDictionary:surveyDataDictionary];
		dataType = [self dataTypeForKey:[surveyData objectForKey:@"Type"]];
        
		surveyDataTitles = [NSMutableArray arrayWithArray:[surveyData objectForKey:@"Order"]];
		surveyDataFields = [NSMutableDictionary dictionaryWithDictionary:[surveyData objectForKey:@"Fields"]];
        
        
        
		isNameAddressLabel = [[surveyData objectForKey:@"NameAddressLabel"] boolValue];
		isImage = [[surveyData objectForKey:@"Image"] boolValue];
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    //[self registerForNotifications];
	// Do any additional setup after loading the view from its nib.
    hideNotes_ = NO;
    currentNoOfRows_ = 0;
	if (!self.appointment)
    {
		self.appointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:self.appointmentId context:nil];
	}
    
	[self loadSurveyData];
	[self loadSurveyUI];
	[self loadNavigationonBarItems];
    
    UIImage * image=[self.appointment.appointmentToProperty.defaultPicture underlyingImage];
    if (image) {
        
        [self.imageView setImage: image];
        
    }else
    {
        [self.imageView setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
    }
    
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPictureDefault:) name:kPropertyPictureDefaultNotification object:nil];
}

-(void)onPictureDefault:(NSNotification*) notification
{
    UIImage * image=[self.appointment.appointmentToProperty.defaultPicture underlyingImage];
    if (image) {
        
        [self.imageView setImage: image];
        
    }else
    {
        [self.imageView setImageWithURL:[NSURL URLWithString:self.appointment.appointmentToProperty.defaultPicture.imagePath] andAddBorder:YES];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[self registerKeyboardNotifications];
    [self registerNotifications];
    [self onPictureDefault:nil];
    
    [self reloadPhotographsBadge];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	[self deregisterKeyboardNotifications];
    [self deRegisterNotifications];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
	[self setBtnMarkComplete:nil];
	[self setPropertyInfoView:nil];
	[self setLblTenantName:nil];
	[self setLblPropertyAddress:nil];
	[self setLblCertificateExpiry:nil];
	[self setBtnBack:nil];
	[self setMarkCompleteView:nil];
	[self setSurveyMenuCell:nil];
	[self setSurveyDropdownCell:nil];
	[self setSurveyTextboxCell:nil];
	[self setSurveyDateCell:nil];
	[self setSurveyCheckboxCell:nil];
}

#pragma mark - Scroll Text Field Methods
- (void)scrollToRectOfSelectedControl {
    if(self.selectedControl != nil)
    {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

-(NSUInteger) getPhotographsCount
{
    NSArray * photographsList =[[PSCoreDataManager sharedManager] propertyPicturesWithPropertyIdentifier:self.appointment.appointmentToProperty.propertyId appointmentId:self.appointment.appointmentId itemId:self.itemId andHeatingId:self.surveyData[@"heatingTypeId"]];
    return [photographsList count];
}

#pragma mark - Navigation Bar
- (void)loadNavigationonBarItems {
	[self.navigationItem setHidesBackButton:YES];
	NSMutableArray *rightBarButtonItems = [NSMutableArray array];
	NSMutableArray *leftBarButtonItems = [NSMutableArray array];
    
	//Camera Button
    
    NSString * badgeText=[NSString stringWithFormat:@"%d",[self getPhotographsCount]];
    
	self.cameraButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
	                                                           style:PSBarButtonItemStyleCamera
	                                                          target:self
	                                                          action:@selector(onClickCameraButton)bagde:badgeText];
    
    
	//Notes Button
	self.notesButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil/*LOC(@"KEY_STRING_SAVE")*/
	                                                          style:PSBarButtonItemStyleNotes
	                                                         target:self
	                                                         action:@selector(onClickNotesButton)];
	//Home Button
	self.homeButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
	                                                         style:PSBarButtonItemStyleHome
	                                                        target:self
	                                                        action:@selector(onClickHomeButton)];
    
    
	//Back Button
	self.backButton = [[PSBarButtonItem alloc] initWithCustomStyle:nil
	                                                         style:PSBarButtonItemStyleBack
	                                                        target:self
	                                                        action:@selector(onClickBackButton:)];
    
	if (self.isMainSurveyMenu == NO) {
		
        
		if (dataType == SurveyDataTypeMenu) {
			//Add Menu Buttons
			[rightBarButtonItems addObject:self.homeButton];
		}
		else {
			//Add Form Buttons
			[rightBarButtonItems addObjectsFromArray:@[self.notesButton, self.cameraButton, self.homeButton]];
		}
	}
    [leftBarButtonItems addObject:self.backButton];
	[self setRightBarButtonItems:rightBarButtonItems];
	[self setLeftBarButtonItems:leftBarButtonItems];
	[self setTitle:surveyTitle];
}

-(void) reloadPhotographsBadge
{
    NSString * badgeText=[NSString stringWithFormat:@"%d",[self getPhotographsCount]];
    [self.cameraButton reloadBadge:badgeText];
}

-(BOOL)isValidConditionalRating
{
    //Checking Condition Rating selected or not Mondatory Field
    //getting condition rating field name e.g boiler condition rating, cooler condition rating
    NSDictionary * conditionRating = nil;
    for(NSString * key in [[self.surveyData objectForKey:@"Fields"] allKeys])
    {
        NSString * testStr = [key lowercaseString];
        NSString *comparatorString = @"condition rating";
        if ([testStr rangeOfString:comparatorString].location == NSNotFound) {
            continue;
        }
        else
        {
            conditionRating =[[self.surveyData objectForKey:@"Fields"] objectForKey:key];
            break;
        }
    }
    
    
    FormData *formData = [[[self.appointment appointmentToSurvey] surveyDataWithItemId:self.surveyData[@"itemId"] andHeatingId:self.surveyData[@"heatingTypeId"]] formDataWithItemId:conditionRating[@"itemParamId"] controlType:conditionRating[@"Type"]];
    if(formData )
    {
        if(formData.surveyParamItemFieldValue)
        {
            return YES;
        }
    }
    else if(!conditionRating)
    {
        return YES;
    }
    //Cheking for Field that come from server
    if(conditionRating)
    {
        if([[conditionRating objectForKey:@"Selected"] count]>0)
        {
            return YES;
        }
    }
    
    return NO;
}

- (void)onClickBackButton:(id)sender {

    CLS_LOG(@"onClickBackButton");
    PSSurveyViewController * viewController= [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2];

        if (dataType == SurveyDataTypeMenu)
        {
            NSArray *keys = [[self.surveyData objectForKey:@"Fields"]allKeys];
            BOOL flag=YES;
            for (NSString *key in keys)
            {
                NSDictionary *dictionary = [[self.surveyData objectForKey:@"Fields" ] objectForKey:key];
                if (![[dictionary objectForKey:kIsVisited]boolValue])
                {
                    flag = NO;
                
                }
            }
            if (flag)
            {
                [surveyData setObject:[NSNumber numberWithBool:YES] forKey:kIsVisited];
                if (!self.isMainSurveyMenu){
                    [[viewController.surveyData objectForKey:@"Fields"] setObject:self.surveyData forKey:surveyTitle];
                }
            }
        }
        else if (dataType == SurveyDataTypeForm)
        {
            //Checking Condition Rating selected or not Mondatory Field
           if(![self isValidConditionalRating])
           {
               UIAlertView * alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Please select value for Condition Rating" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
               [alert show];
               return;
           }
        [[viewController.surveyData objectForKey:@"Fields"] setObject:self.surveyData forKey:surveyTitle];
        }
    if (/*[viewController isKindOfClass:[PSAppointmentDetailViewController class]]*/self.isMainSurveyMenu)
    {
        //save appointment survey
        CLS_LOG(@"main menu");
        [[PSDataUpdateManager sharedManager]updateAppointmentSurvey:self.surveyData forAppointment:self.appointment];
    }

    if(isLandlordAppliance && ![self isDataFilled]) {
        return;
    }

    [self popOrCloseViewController];
    
}

-(BOOL)isDataFilled {
    NSArray *keys = [[self.surveyData objectForKey:@"Fields"]allKeys];
    BOOL filled = YES;
    NSString* message = nil;
    
    for (NSString *key in keys)
    {
        NSMutableDictionary *fieldJSON = [surveyDataFields objectForKey:key];
        SurveyDataType _fieldDataType = [self dataTypeForKey:[fieldJSON objectForKey:@"Type"]];
        NSMutableDictionary *selectedJSON = [fieldJSON objectForKey:@"Selected"];
        NSMutableDictionary *valuesJSON = [fieldJSON objectForKey:@"Values"];
        
        FormData *formData = [[[self.appointment appointmentToSurvey] surveyDataWithItemId:surveyData[@"itemId"]  andHeatingId:self.surveyData[@"heatingTypeId"]] formDataWithItemId:fieldJSON[@"itemParamId"] controlType:fieldJSON[@"Type"]];
        
        NSDictionary *dictionary = [[self.surveyData objectForKey:@"Fields" ] objectForKey:key];
        
        if(!isEmpty(key) && ![[key lowercaseString] isEqualToString:LOC(@"KEY_STRING_LANDLORD_APPLIANCE_SMALL_CASE")])
        if(!isEmpty(formData))
        {
            if(!isEmpty(formData.surveyParamItemFieldValue) && [[formData.surveyParamItemFieldValue lowercaseString] isEqualToString:@"please select"]) {
                filled = NO;
                message = [NSString stringWithFormat:@"Please %@ '%@'",(_fieldDataType == SurveyDataTypeDropDown)?@"select":@"enter",formData.surveyParamName];
                break;
            }
            else if(!isEmpty(formData.surveyParamItemFieldValue) && ![[formData.surveyParamItemFieldValue lowercaseString] isEqualToString:@"please select"]) {
                continue;
            }
            else if(isEmpty(formData.surveyParamItemFieldValue)) {
                filled = NO;
                
                message = [NSString stringWithFormat:@"Please %@ '%@'",(_fieldDataType == SurveyDataTypeDropDown)?@"select":@"enter",formData.surveyParamName];
                break;
            }
        }
        else if(isEmpty([dictionary valueForKey:@"Selected"]))
        {
            filled = NO;
            message = [NSString stringWithFormat:@"Please %@ '%@'",(_fieldDataType == SurveyDataTypeDropDown)?@"select":@"enter",key];
            break;
        }
    }
    
    if(message) {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:message delegate:nil cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil];
        [alert show];
    }
    
    return filled;
}

- (void)onClickCameraButton {
	CLS_LOG(@"Camera Button");
    PSCameraViewController *cameraViewController = [[PSCameraViewController alloc] initWithNibName:@"PSCameraViewController" bundle:[NSBundle mainBundle]];
    [cameraViewController setMainViewController:self.mainViewController];
    [cameraViewController setImageType:CameraViewImageTypeOther];
    [cameraViewController setAppointment:self.appointment];
    [cameraViewController setItemId:self.itemId];
    [cameraViewController setHeatingId:self.surveyData[@"heatingTypeId"]];
    [self.navigationController pushViewController:cameraViewController animated:YES];
    
}

- (void)onClickNotesButton {
	STARTEXCEPTION
	PSAddNotesViewController *addNotesViewController = [[PSAddNotesViewController alloc] initWithNibName:@"PSAddNotesViewController" bundle:nil];
	addNotesViewController.delegate = self;
	SurveyData *_surveyData = [[self.appointment appointmentToSurvey] surveyDataWithItemId:surveyData[@"itemId"]  andHeatingId:self.surveyData[@"heatingTypeId"]];
	addNotesViewController.notesText = [_surveyData surveyNotesDetail];
	[self.navigationController pushViewController:addNotesViewController animated:YES];
	ENDEXCEPTION
}

- (void)onClickHomeButton {
	CLS_LOG(@"Home Button");
    
    if(isLandlordAppliance && ![self isDataFilled]) {
        return;
    }

	[self.navigationController popToViewController:self.mainViewController animated:YES];
}

#pragma mark - UIAlert View Delegate Methods
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == AlertViewTagMarkComplete)
    {
       	if(buttonIndex == alertView.firstOtherButtonIndex)
        {
            //[self showActivityIndicator:LOC(@"KEY_STRING_PROCESSING")];
            CLS_LOG(@"complete appointment clicked");
            
            self.appointment.appointmentStatus = [UtilityClass statusStringForAppointmentStatus:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]]];
            
            [[PSDataUpdateManager sharedManager]updateAppointmentStatusOnly:[UtilityClass completionStatusForAppointmentType:[self.appointment getType]] appointment:self.appointment];
            [self popToRootViewController];
            
            
        }
    }
}

#pragma mark - Notifications

- (void) registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentSuccessNotificationReceive) name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onCompleteAppointmentFailureNotificationReceive) name:kCompleteAppointmentFailureNotification object:nil];
}

- (void) deRegisterNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCompleteAppointmentFailureNotification object:nil];
}

- (void) onCompleteAppointmentSuccessNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        [self.navigationController popToRootViewControllerAnimated:YES];
    });
}

- (void) onCompleteAppointmentFailureNotificationReceive
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideActivityIndicator];
        [self showAlertWithTitle:LOC(@"KEY_ALERT_ERROR")
                         message:LOC(@"KEY_ALERT_FAILED_TO_COMPLETE_APPOINTMENT")
                             tag:0
               cancelButtonTitle:LOC(@"KEY_ALERT_CLOSE")
               otherButtonTitles:nil];
    });
}

#pragma mark - Methods
- (void)loadSurveyData {
	if (self.isMainSurveyMenu) {
		NSDictionary *appointmentinfo = [surveyData objectForKey:@"appointmentinfo"];
		self.appointmentId = [appointmentinfo objectForKey:@"appointmentId"];
		self.propertyId = [appointmentinfo objectForKey:kPropertyId];
	}
	else {
		if (dataType == SurveyDataTypeForm) {
            
			self.itemId = [surveyData objectForKey:@"itemId"];
            
            //Stock component lifecycle can be displayed here
            //or we can display it with in Last Replaced
            
            //surveyDataTitles
            
            /*NSMutableDictionary *fieldJSON = [surveyDataFields objectForKey:@"Last Replaced"];
            if (fieldJSON) {
                
                self.componentLifeCycle = [fieldJSON objectForKey:@"cycle"];
            }*/
            
		}
	}
}

- (void)loadSurveyUI {
	if (self.isMainSurveyMenu) {
		[self.propertyInfoView setHidden:NO];
        
		Customer *customer = [self.appointment defaultTenant];
		self.lblTenantName.text = [customer fullName];
        
		Property *property = [self.appointment appointmentToProperty];
		self.lblPropertyAddress.text = [property addressWithStyle:PropertyAddressStyle1];
        
        
		if ([self.appointment getType] == AppointmentTypeStock) {
			surveyTitle = @"Stock Condition Survey";
			self.lblCertificateExpiry.hidden = YES;
		}
		else if ([self.appointment getType] == AppointmentTypeGas) {
			surveyTitle = @"Gas Survey";
		}
		else if ([self.appointment getType] == AppointmentTypeFault) {
			surveyTitle = @"Fault Survey";
		}
		[self.btnBack addTarget:self action:@selector(onClickBackButton:) forControlEvents:UIControlEventTouchUpInside];
	}
	else {
		[self.markCompleteView setHidden:YES];
		[self.propertyInfoView setHidden:YES];
		CGRect bounds = [self.view bounds];
		self.tableView.frame  = CGRectMake(bounds.origin.x,
		                                   bounds.origin.y,
		                                   bounds.size.width,
		                                   bounds.size.height);
	}
}

#pragma mark - UITableView Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    

	NSInteger numberOfRows = [surveyDataTitles count];
	
//    if (hideNotes_) {
//        numberOfRows--;
//    }
    currentNoOfRows_ = (int)numberOfRows;
    return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat rowHeight = 0;
	if (dataType == SurveyDataTypeMenu) {
		rowHeight = 44;
	}
	else if (dataType == SurveyDataTypeForm) {
		NSString *currentKey = [surveyDataTitles objectAtIndex:indexPath.row];
		NSMutableDictionary *fieldJSON = [surveyDataFields objectForKey:currentKey];
		SurveyDataType _fieldDataType = [self dataTypeForKey:[fieldJSON objectForKey:@"Type"]];
		NSMutableDictionary *valuesJSON = [fieldJSON objectForKey:@"Values"];
		if (_fieldDataType == SurveyDataTypeCheckBox) {
			rowHeight = [PSSurveyCheckboxCell cellHeightForCheckboxesCount:[valuesJSON count]];
		}
        else if (_fieldDataType == SurveyDataTypeTextView)
        {
            rowHeight = (hideNotes_)?50:150;
        }
		else {
			rowHeight = 50;
		}
	}
	else {
		rowHeight = 50;
	}
	return rowHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = nil;
	if (dataType == SurveyDataTypeMenu) {
		static NSString *surveyMenuCellIdentifier = @"surveyMenuCellIdentifier";
		PSSurveyMenuCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:surveyMenuCellIdentifier];
		if (cell == nil) {
			[[NSBundle mainBundle] loadNibNamed:@"PSSurveyMenuCell" owner:self options:nil];
			_cell = self.surveyMenuCell;
			self.surveyMenuCell = nil;
		}
		_cell.lblTitle.text = [surveyDataTitles objectAtIndex:indexPath.row];
        
        NSDictionary *surveyInformation = [self surveyInformationForKey:[surveyDataTitles objectAtIndex:indexPath.row] surveyData:surveyData];
        NSNumber *totalFields = [surveyInformation objectForKey:kTotalFields];
        NSNumber *filledFields = [surveyInformation objectForKey:kFilledFields];
        NSInteger visitedMenusCount = 0;
        NSDictionary *fieldsDictionary = [self.surveyData objectForKey:@"Fields"];
   
        NSDictionary *itemDictionary = [fieldsDictionary objectForKey:[surveyDataTitles objectAtIndex:indexPath.row]];
        BOOL isVisited = [[itemDictionary objectForKey:kIsVisited] boolValue];
        
        if (isVisited)
        {
            _cell.imgStatus.image = [UIImage imageNamed:@"icon_Complete"];
        }
        else
        {
            _cell.imgStatus.image = [UIImage imageNamed:@"icon_Cross"];
        }
		
		// Set count
		NSString *selectedKey = [surveyDataTitles objectAtIndex:indexPath.row];
		if([selectedKey isEqualToString:kSurveyKeySmoke]){
			NSArray *_detectors = [NSArray array];
			_detectors = [self.appointment.appointmentToProperty.propertyToDetector allObjects];
			NSInteger detectorTypeId = DetectorTypeSmoke;
			 _detectors = [_detectors filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"detectorTypeId = %ld", (long)detectorTypeId]]];
			_cell.lblCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_detectors count]];
			
		}else if([selectedKey isEqualToString:kSurveyKeyCO])
		{
			NSArray *_detectors = [NSArray array];
			_detectors = [self.appointment.appointmentToProperty.propertyToDetector allObjects];
			NSInteger detectorTypeId = DetectorTypeCO;
			_detectors = [_detectors filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"detectorTypeId = %ld", (long)detectorTypeId]]];
			_cell.lblCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)[_detectors count]];
		}
		else
		{
			_cell.lblCount.text = [NSString stringWithFormat:@"%@-%@",filledFields,totalFields];
		}
		
		cell = _cell;
	}
	else if (dataType == SurveyDataTypeForm) {
		static NSString *surveyDropdownCellIdentifier = @"surveyDropdownCellIdentifier";
		static NSString *surveyTextboxCellIdentifier = @"surveyTextboxCellIdentifier";
        static NSString *surveyTextViewCellIdentifier = @"surveyTextViewCellIdentifier";
		static NSString *surveyCheckboxCellIdentifier = @"surveyCheckboxCellIdentifier";
		static NSString *surveyDateCellIdentifier = @"surveyDateCellIdentifier";
        
		NSString *currentKey = [surveyDataTitles objectAtIndex:indexPath.row];
		NSMutableDictionary *fieldJSON = [surveyDataFields objectForKey:currentKey];
		SurveyDataType _fieldDataType = [self dataTypeForKey:[fieldJSON objectForKey:@"Type"]];
		NSMutableDictionary *selectedJSON = [fieldJSON objectForKey:@"Selected"];
		NSMutableDictionary *valuesJSON = [fieldJSON objectForKey:@"Values"];
		
        FormData *formData = [[[self.appointment appointmentToSurvey] surveyDataWithItemId:surveyData[@"itemId"]  andHeatingId:self.surveyData[@"heatingTypeId"]] formDataWithItemId:fieldJSON[@"itemParamId"] controlType:fieldJSON[@"Type"]];
        
		switch (_fieldDataType) {
			case SurveyDataTypeDropDown:
			{
				PSSurveyDropdownCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:surveyDropdownCellIdentifier];
				if (cell == nil) {
					[[NSBundle mainBundle] loadNibNamed:@"PSSurveyDropdownCell" owner:self options:nil];
					_cell = self.surveyDropdownCell;
					self.surveyDropdownCell = nil;
				}
				_cell.delegate = self;
				_cell.surveyParamName = currentKey;
				_cell.dataDictionary = fieldJSON;
				_cell.formData = formData;
                
                
                if([[currentKey lowercaseString] isEqualToString:@"condition rating"]) {
                    
                    
                    NSString* selectedValue = [[selectedJSON allKeys] firstObject];
                    
                    if(isEmpty(selectedValue)) {
                        
                        if (isEmpty(formData)) {
                             hideNotes_ = YES;
                        }
                        else if ([[formData.surveyParamItemFieldValue lowercaseString] isEqualToString:@"satisfactory"]) {
                            hideNotes_ = YES;
                        }
                    }
                    else if(!isEmpty(selectedValue)) {
                        
                        if(!isEmpty(formData))  {
                            if([[formData.surveyParamItemFieldValue lowercaseString] isEqualToString:@"satisfactory"]) {
                                hideNotes_ = YES;
                            }
                            else {
                                hideNotes_ = NO;
                            }
                        }
                        else if([[selectedValue lowercaseString] isEqualToString:@"satisfactory"])  {
                            hideNotes_ = YES;
                        }
                    }
                    
                   
                }
                
               
				[_cell loadCellData];
				_cell.lblTitle.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
				_cell.lblValue.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
				_cell.selectionStyle = UITableViewCellSelectionStyleNone;
				cell = _cell;
			}
                break;
                
			case SurveyDataTypeCheckBox:
			{
				PSSurveyCheckboxCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:surveyCheckboxCellIdentifier];
				if (cell == nil) {
					[[NSBundle mainBundle] loadNibNamed:@"PSSurveyCheckboxCell" owner:self options:nil];
					_cell = self.surveyCheckboxCell;
					self.surveyCheckboxCell = nil;
				}
                
                isLandlordAppliance = FALSE;

                if(!isEmpty(formData.controlType) && [formData.controlType isEqualToString:@"Checkboxes"])
                {
                    if(!isEmpty(formData.surveyParamName) && [[formData.surveyParamName lowercaseString] isEqualToString:LOC(@"KEY_STRING_LANDLORD_APPLIANCE_SMALL_CASE")])
                    {
                        
                        NSDictionary *selectedValues = formData.surveyParamItemFieldValue;
                        
                        CLS_LOG(@"Landlord %@", selectedValues);
                        NSArray *allKeys = [selectedValues allKeys];
                            for (NSString *key in allKeys) {
                                if(!isEmpty([[selectedValues objectForKey:key] objectForKey:@"IsCheckBoxSelected"]) && [[[selectedValues objectForKey:key] objectForKey:@"IsCheckBoxSelected"] boolValue])
                                {
                                    isLandlordAppliance = TRUE;
                                }
                            }
                        
                        
                       
                    }
                }
                
				_cell.delegate = self;
				_cell.formData = formData;
                _cell.surveyParamName = currentKey;
				[_cell loadCellWithData:fieldJSON];
				_cell.lblTitle.text = currentKey;
				_cell.lblTitle.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
				_cell.selectionStyle = UITableViewCellSelectionStyleNone;
				cell = _cell;
                
                CLS_LOG(@"isLandlordAppliance ---- %d",isLandlordAppliance);
			}
                break;
                
			case SurveyDataTypeTextBox:
			{
				PSSurveyTextboxCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:surveyTextboxCellIdentifier];
				if (cell == nil) {
					[[NSBundle mainBundle] loadNibNamed:@"PSSurveyTextboxCell" owner:self options:nil];
					_cell = self.surveyTextboxCell;
					self.surveyTextboxCell = nil;
				}
				_cell.delegate = self;
				_cell.surveyParamName = currentKey;
				_cell.dataDictionary = fieldJSON;
				_cell.formData = formData;
				[_cell loadCellData];
				_cell.txtValue.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
				_cell.lblTitle.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
				_cell.txtValue.tag = indexPath.row;
				_cell.selectionStyle = UITableViewCellSelectionStyleNone;
				cell = _cell;
			}
                break;
                
			case SurveyDataTypeDate:
			{
				PSSurveyDateCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:surveyDateCellIdentifier];
				if (cell == nil) {
                    
					[[NSBundle mainBundle] loadNibNamed:@"PSSurveyDateCell" owner:self options:nil];
					_cell = self.surveyDateCell;
					self.surveyDateCell = nil;
				}
				_cell.delegate = self;
				_cell.surveyParamName = currentKey;
				_cell.dataDictionary = fieldJSON;
				_cell.formData = formData;
				[_cell loadCellData];
				_cell.lblTitle.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
				_cell.lblDate.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
				_cell.selectionStyle = UITableViewCellSelectionStyleNone;
				cell = _cell;
                if(!isEmpty(self.selectedComponents)){
                    if([currentKey isEqualToString:@"Replacement Due"]||[currentKey isEqualToString:@"Electric Upgrade Due"]||[currentKey isEqualToString:@"Cur Due"]||[currentKey isEqualToString:@"Rewire Due"]){
                        cell.userInteractionEnabled = false;
                    }
                }
			}
                break;
            case SurveyDataTypeTextView:
			{
                PSSurveyTextViewCell *_cell = [tableView dequeueReusableCellWithIdentifier:surveyTextViewCellIdentifier];
				if (_cell == nil) {
					[[NSBundle mainBundle] loadNibNamed:@"PSSurveyTextViewCell" owner:self options:nil];
                    
					_cell = self.surveyTextViewCell;
					self.surveyTextViewCell = nil;
                    [_cell setRestorationIdentifier:surveyTextViewCellIdentifier];
				}
				_cell.delegate = self;
				_cell.surveyParamName = currentKey;
				_cell.dataDictionary = fieldJSON;
				_cell.formData = formData;
				[_cell loadCellData];
				_cell.txtValue.textColor = UIColorFromHex(THEME_TEXT_COLOUR_DARK);
				_cell.lblTitle.textColor = UIColorFromHex(SECTION_HEADER_TEXT_COLOUR);
                [_cell.txtValue setPlaceholderColor:UIColorFromHex(THEME_TEXT_COLOUR_LIGHT)];
				_cell.txtValue.tag = indexPath.row;
				_cell.selectionStyle = UITableViewCellSelectionStyleNone;
				cell = _cell;
                cell.contentView.hidden = hideNotes_;
			}
                break;
            case SurveyDataTypeMenu: {
                    static NSString *surveyMenuCellIdentifier = @"surveyMenuCellIdentifier";
                    PSSurveyMenuCell *_cell = [self.tableView dequeueReusableCellWithIdentifier:surveyMenuCellIdentifier];
                    if (cell == nil) {
                        [[NSBundle mainBundle] loadNibNamed:@"PSSurveyMenuCell" owner:self options:nil];
                        _cell = self.surveyMenuCell;
                        self.surveyMenuCell = nil;
                    }
                    _cell.lblTitle.text = [surveyDataTitles objectAtIndex:indexPath.row];
                    
                    NSDictionary *surveyInformation = [self surveyInformationForKey:[surveyDataTitles objectAtIndex:indexPath.row] surveyData:surveyData];
                    NSNumber *totalFields = [surveyInformation objectForKey:kTotalFields];
                    NSNumber *filledFields = [surveyInformation objectForKey:kFilledFields];
                    NSInteger visitedMenusCount = 0;
                    NSDictionary *fieldsDictionary = [self.surveyData objectForKey:@"Fields"];
                    
                    NSDictionary *itemDictionary = [fieldsDictionary objectForKey:[surveyDataTitles objectAtIndex:indexPath.row]];
                    BOOL isVisited = [[itemDictionary objectForKey:kIsVisited] boolValue];
                    
                    if (isVisited)
                    {
                        _cell.imgStatus.image = [UIImage imageNamed:@"icon_Complete"];
                    }
                    else
                    {
                        _cell.imgStatus.image = [UIImage imageNamed:@"icon_Cross"];
                    }
                    _cell.lblCount.text = [NSString stringWithFormat:@"%@-%@",filledFields,totalFields];
                    
							cell = _cell;
                }
                break;
			default:
				return nil;
				break;
		}
	}
	cell.backgroundColor = UIColorFromHex(THEME_BG_COLOUR);
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
	  NSString *selectedKey = [surveyDataTitles objectAtIndex:indexPath.row];
    NSString *currentKey = [surveyDataTitles objectAtIndex:indexPath.row];
    NSMutableDictionary *fieldJSON = [surveyDataFields objectForKey:currentKey];
    SurveyDataType _fieldDataType = [self dataTypeForKey:[fieldJSON objectForKey:@"Type"]];
    
    NSMutableDictionary *selectedJSON = [fieldJSON objectForKey:@"Selected"];
    NSMutableDictionary *valuesJSON = [fieldJSON objectForKey:@"Values"];
    
    dataType = [self dataTypeForKey:[surveyData objectForKey:@"Type"]];
	
    if([selectedKey isEqualToString:kSurveyKeySmoke]){
        PSDetectorsViewController *detectorListVC = [[PSDetectorsViewController alloc] initWithNibName:@"PSDetectorsViewController" bundle:nil];
        [detectorListVC setAppointment:self.appointment];
			  [detectorListVC setDetectorType:DetectorTypeSmoke];
			  [detectorListVC setMainViewController:self.mainViewController];
			  [detectorListVC setItemId:[fieldJSON objectForKey:@"itemId"]];
        [self.navigationController pushViewController:detectorListVC animated:YES];
    }
	else if ([selectedKey isEqualToString:kSurveyKeyCO] )
	 {
		 PSDetectorsViewController *detectorListVC = [[PSDetectorsViewController alloc] initWithNibName:@"PSDetectorsViewController" bundle:nil];
		 [detectorListVC setAppointment:self.appointment];
		 [detectorListVC setDetectorType:DetectorTypeCO];
		 [detectorListVC setMainViewController:self.mainViewController];
		 [detectorListVC setItemId:[fieldJSON objectForKey:@"itemId"]];
		 [self.navigationController pushViewController:detectorListVC animated:YES];
	 }
    else{
        if (dataType == SurveyDataTypeMenu || (dataType == SurveyDataTypeForm && _fieldDataType == SurveyDataTypeMenu))
        {
            NSMutableDictionary *surveyJSON = [[surveyData objectForKey:@"Fields"] objectForKey:selectedKey];
            surveyJSON = [self addComponentInfoToHeatingSurveyGeneric:surveyJSON];
            PSSurveyViewController *surveyViewConroller = [[PSSurveyViewController alloc] initWithTitle:selectedKey surveyData:surveyJSON];
            [surveyViewConroller setSelectedComponents:_selectedComponents];
            [surveyViewConroller setMainSurveyMenu:NO];
            [surveyViewConroller setMainViewController:self.mainViewController];
            [surveyViewConroller setAppointmentId:self.appointmentId];
            [surveyViewConroller setPropertyId:self.propertyId];
            [surveyViewConroller setItemId:self.itemId];
            [surveyViewConroller setAppointment:self.appointment];
            [self.navigationController pushViewController:surveyViewConroller animated:YES];
        }
    }
    
	
}

#pragma mark - Add Component Info
-(NSMutableDictionary*) addComponentInfoToHeatingSurveyGeneric:(NSMutableDictionary *) surveyFieldsss{
    NSMutableDictionary *surveyFields = [[NSMutableDictionary alloc] initWithDictionary:surveyFieldsss];
    SurveyDataType dt = [self dataTypeForKey:[surveyFields objectForKey:@"Type"]];
    if(dt==SurveyDataTypeForm){
        NSNumber *itemId = [surveyFields objectForKey:@"itemId"];
        NSMutableDictionary *fields = [surveyFields objectForKey:@"Fields"];
        NSMutableArray *order = [surveyFields objectForKey:@"Order"];
        surveyFields = [self removeComponentAccountingInfoFromSurvey:surveyFields];
        NSArray *components = [self.appointment.appointmentToSurveyComponents allObjects];
        NSMutableArray *selectedParamValueIds = [[NSMutableArray alloc] init];
        NSMutableArray *selectedSubParamValueIds = [[NSMutableArray alloc] init];
        NSString *defCompParamId = @"parameterId";
        NSString *defCompSubParamId = @"subParameterId";
        NSString *defCompValueId = @"valueId";
        NSString *defCompSubValueId = @"subValueId";
        NSString *defFieldParamAndSubParamId = @"paramId";
        NSString *defFieldValueAndSubValueId = @"id";
        NSString *defFieldSelected = @"Selected";
        
        /*Get Components with Item Id*/
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ == %@",kItemId, itemId];
        NSArray *filteredComponents = [components filteredArrayUsingPredicate:predicate];
        
        /* Get All Fields*/
        NSArray *allFieldsArray = [fields allValues];
        /*Get All Component's Parameter Ids*/
        NSArray *componentParamIds = [filteredComponents valueForKeyPath:@"@distinctUnionOfObjects.parameterId"];
        /*Match fields against component param ids*/
        NSPredicate *paramPred = [NSPredicate predicateWithFormat:@"SELF.%@ IN %@",defFieldParamAndSubParamId, componentParamIds];
        NSArray *paramFilteredFields = [allFieldsArray filteredArrayUsingPredicate:paramPred];
        if(!isEmpty(paramFilteredFields)){
            for(NSDictionary *dict in paramFilteredFields){
                if([[dict objectForKey:@"Type"] isEqualToString:@"Dropdown"]){
                    NSDictionary *selectedDict = [dict objectForKey:defFieldSelected];
                    if(!isEmpty(selectedDict)){
                        NSNumber *selectedId = [[selectedDict.allValues objectAtIndex:0] objectForKey:defFieldValueAndSubValueId];
                        if(!isEmpty(selectedId)){
                            [selectedParamValueIds addObject:selectedId];
                        }
                        else{
                            [selectedParamValueIds addObject:[NSNumber numberWithInt:0]];
                        }
                    }
                }
                
                
            }
            /*Reallocate Components to only those belonging to these params*/
            predicate = [NSPredicate predicateWithFormat:@"SELF.%@ IN %@",defCompParamId, componentParamIds];
            filteredComponents = [filteredComponents filteredArrayUsingPredicate:predicate];
            
            /*Get All Subparam ids from components*/
            NSArray *componentSubParamIds = [filteredComponents valueForKeyPath:@"@distinctUnionOfObjects.subParameterId"];
            /*Match Fields against the comp sub param ids*/
            NSPredicate*subParamPred = [NSPredicate predicateWithFormat:@"SELF.%@ IN %@",defFieldParamAndSubParamId, componentSubParamIds];
            NSArray*subParamFilteredFields = [allFieldsArray filteredArrayUsingPredicate:subParamPred];
            if(!isEmpty(subParamFilteredFields)){
               
                /*Get array of selected PAram and Subparam id. Will be only one at max for all other except electrics, which will return 3*/
                
                for(NSDictionary *dict in subParamFilteredFields){
                    if([[dict objectForKey:@"Type"] isEqualToString:@"Dropdown"]){
                        NSDictionary *selectedDict = [dict objectForKey:defFieldSelected];
                        if(!isEmpty(selectedDict)){
                            NSNumber *selectedId = [[selectedDict.allValues objectAtIndex:0] objectForKey:defFieldValueAndSubValueId];
                            if(!isEmpty(selectedId)){
                                [selectedSubParamValueIds addObject:selectedId];
                            }
                            else{
                                [selectedSubParamValueIds addObject:[NSNumber numberWithInt:0]];
                            }
                        }
                    }
                    
                }
                CLS_LOG(@"%@,%@",selectedParamValueIds,selectedSubParamValueIds);
                
                
                
            }
        }
        
        /*Got all the Selected Param and Subparam ids, now get Components against those*/
        
        NSString* paramPredStr = [NSString stringWithFormat:@"SELF.%@==null",defCompValueId];
        if(!isEmpty(selectedParamValueIds)){
            paramPredStr = [NSString stringWithFormat:@"SELF.%@ IN {%@}",defCompValueId,[[NSArray alloc] initWithArray:selectedParamValueIds]];
        }
        NSString* subParamPredStr = [NSString stringWithFormat:@"SELF.%@==null",defCompSubValueId];
        if(!isEmpty(selectedSubParamValueIds)){
            subParamPredStr = [NSString stringWithFormat:@"SELF.%@ IN {%@}",defCompSubValueId,[[NSArray alloc] initWithArray:selectedSubParamValueIds]];
        }
        NSPredicate *paramCompPred = [NSPredicate predicateWithFormat:paramPredStr];
        NSPredicate *subParamCompPred = [NSPredicate predicateWithFormat:subParamPredStr];
        NSPredicate *itemCompPred = [NSPredicate predicateWithFormat:@"SELF.%@==%@",kItemId,itemId];
        
        NSArray *f1 = [components filteredArrayUsingPredicate:itemCompPred];
        NSArray *f2 = [f1 filteredArrayUsingPredicate:paramCompPred];
        NSArray *finalFilteredComponents = [f2 filteredArrayUsingPredicate:subParamCompPred];
        CLS_LOG(@"%@",finalFilteredComponents);
        _selectedComponents = [[NSArray alloc] initWithArray:finalFilteredComponents];
        
        if(!isEmpty(_selectedComponents)){
            for(Component *component in _selectedComponents){
                NSString *lifeCycleSurveyTitle = ([_selectedComponents count]>1)?[NSString stringWithFormat:@"%@ : %@",kComponentLifeCycleForSurvey,component.componentName]:kComponentLifeCycleForSurvey;
                NSString *componentAccountingTitle = ([_selectedComponents count]>1)?[NSString stringWithFormat:@"%@ : %@",kComponentAccountingForSurvey,component.componentName]:kComponentAccountingForSurvey;
                
                [order addObject:kComponentNameForSurvey];
                [order addObject:lifeCycleSurveyTitle];
                [order addObject:componentAccountingTitle];
                
                [surveyFields setObject:order forKey:@"Order"];
                
                NSMutableDictionary *fieldsForCompName = [[NSMutableDictionary alloc]  init];
                [fieldsForCompName setObject:@1 forKey:@"ReadOnly"];
                [fieldsForCompName setObject:@0 forKey:@"itemParamId"];
                [fieldsForCompName setObject:@0 forKey:@"paramId"];
                [fieldsForCompName setObject:@"Textbox" forKey:@"Type"];
                [fieldsForCompName setObject:@{component.componentName:@{@"IsCheckBoxSelected":@"",@"id":@0,@"inputData":component}} forKey:@"Selected"];
                [fields setObject:fieldsForCompName forKey:kComponentNameForSurvey];
                
                NSMutableDictionary *fieldsForCompLC = [[NSMutableDictionary alloc]  init];
                [fieldsForCompLC setObject:@1 forKey:@"ReadOnly"];
                [fieldsForCompLC setObject:@0 forKey:@"itemParamId"];
                [fieldsForCompLC setObject:@0 forKey:@"paramId"];
                [fieldsForCompLC setObject:@"Textbox" forKey:@"Type"];
                NSString * lifeCycleStr = [NSString stringWithFormat:@"%@ %@",component.cycle,component.frequency];
                [fieldsForCompLC setObject:@{lifeCycleStr:@{@"IsCheckBoxSelected":@"",@"id":@0,@"inputData":component}} forKey:@"Selected"];
                [fields setObject:fieldsForCompLC forKey:lifeCycleSurveyTitle];
                
                NSMutableDictionary *fieldsForCompCA = [[NSMutableDictionary alloc]  init];
                [fieldsForCompCA setObject:@1 forKey:@"ReadOnly"];
                [fieldsForCompCA setObject:@"Textbox" forKey:@"Type"];
                [fieldsForCompCA setObject:@0 forKey:@"itemParamId"];
                [fieldsForCompCA setObject:@0 forKey:@"paramId"];
                NSString *accountingStatusStr = ([component.isComponentAccounting boolValue])?@"TRUE":@"FALSE";
                [fieldsForCompCA setObject:@{accountingStatusStr:@{@"IsCheckBoxSelected":@"",@"id":@0,@"inputData":component}} forKey:@"Selected"];
                [fields setObject:fieldsForCompCA forKey:componentAccountingTitle];
                
                
                
                //All done
                [surveyFields setObject:fields forKey:@"Fields"];
                [surveyFields setObject:order forKey:@"Order"];
            }
            
            
        }
    }
    return surveyFields;
}

-(NSMutableDictionary*) removeComponentAccountingInfoFromSurvey:(NSDictionary *) dict{
    NSMutableDictionary *surveyFields = [[NSMutableDictionary alloc] initWithDictionary:dict];
    NSMutableDictionary *fields = [surveyFields objectForKey:@"Fields"];
    NSMutableArray *order = [surveyFields objectForKey:@"Order"];
    NSPredicate *orderFilterPredicate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@ OR SELF BEGINSWITH[c] %@ OR SELF BEGINSWITH[c] %@",kComponentNameForSurvey,kComponentAccountingForSurvey,kComponentLifeCycleForSurvey];
    NSArray *filteredOrder = [order filteredArrayUsingPredicate:orderFilterPredicate];
    if(!isEmpty(filteredOrder)){
        for(NSString *key in filteredOrder){
            [order removeObject:key];
            [fields removeObjectForKey:key];
        }
    }
    return surveyFields;
}



#pragma mark - ScrollView Methods
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	[self resignAllResponders];
}

#pragma mark - Utility Methods
- (SurveyDataType)dataTypeForKey:(NSString *)key {
	SurveyDataType _dataType = SurveyDataTypeNone;
    key = [key lowercaseString];
	if (!isEmpty(key)) {
		if ([key isEqualToString:@"menu"]) {
			_dataType = SurveyDataTypeMenu;
		}
		else if ([key isEqualToString:@"form"]) {
			_dataType = SurveyDataTypeForm;
		}
		else if ([key isEqualToString:@"dropdown"]) {
			_dataType = SurveyDataTypeDropDown;
		}
        else if ([key isEqualToString:@"radiobutton"]) { //Radio Buttons are treated as Drop Down
			_dataType = SurveyDataTypeDropDown;
		}
		else if ([key isEqualToString:@"checkboxes"]) {
			_dataType = SurveyDataTypeCheckBox;
		}
		else if ([key isEqualToString:@"textbox"]) {
			_dataType = SurveyDataTypeTextBox;
		}
		else if ([key isEqualToString:@"date"]) {
			_dataType = SurveyDataTypeDate;
		}
        else if ([key isEqualToString:@"textarea"]) {
			_dataType = SurveyDataTypeTextView;
		}
		else {
			_dataType = SurveyDataTypeNone;
		}
	}
	//CLS_LOG(@"_dataType : %d", _dataType);
	return _dataType;
}

-(void) addCycleToReplacementDueDate:(NSDictionary *)formDataDictionary
{
    
    
    
    if([[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"Last Replaced"] || [[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"Electrical Upgrade Last Done"] || [[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"CUR Last Replaced"] || [[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"Last Rewired"])
    {
        
        // get formDataDictionary for "Replacement Due"
        // Add cycle to last replaced date and set this date to Replacement Due
        
        
        NSUInteger replacementDueIndex=0;
        if([[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"Last Replaced"]){
            replacementDueIndex = [surveyDataTitles indexOfObject:@"Replacement Due"];
        }
        else if([[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"Electrical Upgrade Last Done"]){
            replacementDueIndex = [surveyDataTitles indexOfObject:@"Electric Upgrade Due"];
        }
        else if([[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"CUR Last Replaced"]){
            replacementDueIndex = [surveyDataTitles indexOfObject:@"Cur Due"];
        }
        else if([[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"Last Rewired"]){
            replacementDueIndex = [surveyDataTitles indexOfObject:@"Rewire Due"];
        }
        
        PSSurveyDateCell * cell=(PSSurveyDateCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:replacementDueIndex inSection:0]];
        
        
        NSMutableDictionary * replacementDueFormDataDictionary=cell.formDataDictionary;
        
//        if (isEmpty([replacementDueFormDataDictionary objectForKey:@"surveyParamItemFieldValue"])) {
//            
//            return;
//        }
        if (isEmpty(self.selectedComponents))
        {
        
                    return;
        }
        
        NSDate * lastReplacedDate= [UtilityClass dateFromString:[formDataDictionary objectForKey:@"surveyParamItemFieldValue"] dateFormat:kDateTimeStyle8];
        
        NSDate * replacementDueDate = [NSDate date];
        
        NSArray *filteredComp = [[NSArray alloc] initWithArray:_selectedComponents];
        if([filteredComp count]>1){
            NSNumber * paramId = [replacementDueFormDataDictionary objectForKey:@"surveyParameterId"];
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF.parameterId == %@", paramId];
            filteredComp = [_selectedComponents filteredArrayUsingPredicate:pred];
            if (isEmpty(filteredComp))
            {
                
                return;
            }
        }
        
        
        Component *tempComp = [filteredComp objectAtIndex:0];
        
        if([tempComp.frequency isEqualToString:@"yrs"]){
            replacementDueDate = [lastReplacedDate dateByAddingYears:[tempComp.cycle intValue]];
        }
        else{
             replacementDueDate = [lastReplacedDate dateByAddingMonths:[tempComp.cycle intValue]];
        }
        NSString * replacementDueDateString=[UtilityClass stringFromDate:replacementDueDate dateFormat:kDateTimeStyle8];
        
        [replacementDueFormDataDictionary setObject:replacementDueDateString forKey:@"surveyParamItemFieldValue"];
        
        //once we set the date , we need to reload data of cell replacement due
        
        [cell reloadData:replacementDueDate];
        
        //save this to database
        
        [self.currentSurvey saveFormDataDictionary:replacementDueFormDataDictionary forSurveyDataDictionary:surveyData];
    }
}

#pragma mark - SurveyFormDelegate

- (void)didChangeSurveyFormDateWithData:(NSDictionary *)formDataDictionary {
    //    CLS_LOG(@"Form Value Changed: %@", formDataDictionary);
    [self didChangeSurveyFormWithData:formDataDictionary];
    
    [self addCycleToReplacementDueDate:formDataDictionary];
}


- (void)didChangeSurveyFormWithData:(NSDictionary *)formDataDictionary {
    
    CLS_LOG(@"Form Value Changed: %@", formDataDictionary);
    
    if (!self.appointment) {
		self.appointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:self.appointmentId context:nil];
	}
    if([[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"Heating Fuel"]){
        NSMutableDictionary *jsonDict = [self.appointment.appointmentToSurvey.surveyJSON JSONValue];
        NSNumber *selectedFuelId = [formDataDictionary objectForKey:@"surveyPramItemFieldId"];
        if(!isEmpty(selectedFuelId)){
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.fuelTypeId==%i",[selectedFuelId intValue]];
            NSArray *newSurvey = [self.appointment.appointmentToSurveyTemplates.allObjects filteredArrayUsingPredicate:predicate];
            if(!isEmpty(newSurvey)){
                //[[PSDataUpdateManager sharedManager] deleteSurveyDataForAppointment:self.appointment andHeatingTypeId:[surveyData objectForKey:@"heatingTypeId"]];
                SurveyHeatingFuelData *fuelEnt = [newSurvey objectAtIndex:0];
                NSMutableDictionary *newSurveyDict = [fuelEnt.heatingFuelSurvey JSONValue];
                
                [[[[[[[[[[[jsonDict objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle] setObject:[newSurveyDict objectForKey:@"Order"] forKey:@"Order"];
                
                [[[[[[[[[[[jsonDict objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle] setObject:[newSurveyDict objectForKey:kSurveyKeyFields] forKey:kSurveyKeyFields];
                [[PSDataUpdateManager sharedManager] updateAppointmentSurvey:jsonDict
                                                              forAppointment:self.appointment
                                                             withSurveyTitle:surveyTitle];
                
                NSDictionary *currentSurvey = [[[[[[[[[[jsonDict objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle];
                
                self.surveyData = [self addComponentInfoToHeatingSurveyGeneric:[NSMutableDictionary dictionaryWithDictionary:currentSurvey]];//[[NSMutableDictionary alloc] initWithDictionary:currentSurvey];//
                surveyDataTitles = [NSMutableArray arrayWithArray:[self.surveyData objectForKey:@"Order"]];
                surveyDataFields = [NSMutableDictionary dictionaryWithDictionary:[self.surveyData objectForKey:@"Fields"]];
                [self.tableView reloadData];
                
                
            }
            
        }
    }
    
    else{
        [surveyData setObject:[NSNumber numberWithBool:YES] forKey:kIsVisited];
        NSMutableDictionary *formDataMutableDictionary = [formDataDictionary mutableCopy];
        
        self.currentSurvey = [self.appointment appointmentToSurvey];
        [self.currentSurvey saveFormDataDictionary:formDataMutableDictionary forSurveyDataDictionary:surveyData];
        
        NSString *surveyParamId = [formDataDictionary objectForKey:@"surveyParamItemFieldValue"];
        
        
        if([[[formDataDictionary objectForKey:@"surveyParamName"] lowercaseString] isEqualToString:@"condition rating"]) {
            if ([[surveyParamId lowercaseString] isEqualToString:@"satisfactory"]) {
                hideNotes_ = YES;
            }
            else
            {
                hideNotes_ = NO;
            }
        }
        if([[formDataDictionary objectForKey:@"surveyParamName"] isEqualToString:@"Heating Type"])
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[self.appointment.appointmentToSurvey.surveyJSON JSONValue]];
            NSMutableDictionary *selectedValueDict = [[NSMutableDictionary alloc] init];
            [selectedValueDict setObject:[formDataDictionary objectForKey:@"surveyPramItemFieldId"] forKey:@"id"];
            [selectedValueDict setObject:@"" forKey:@"IsCheckBoxSelected"];
            
            
            NSMutableDictionary *selectedDict = [[NSMutableDictionary alloc] init];
            
            [selectedDict setObject:selectedValueDict forKey:[formDataDictionary objectForKey:@"surveyParamItemFieldValue"]];
            
            [[[[[[[[[[[[[dict objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle] objectForKey:kSurveyKeyFields] objectForKey:@"Heating Type"] setObject:selectedDict forKey:@"Selected"];
            [[PSDataUpdateManager sharedManager] updateAppointmentSurvey:dict
                                                          forAppointment:self.appointment
                                                         withSurveyTitle:surveyTitle];
            
            NSDictionary *currentSurvey = [[[[[[[[[[dict objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle];
            
            self.surveyData = [self addComponentInfoToHeatingSurveyGeneric:[NSMutableDictionary dictionaryWithDictionary:currentSurvey]];//[[NSMutableDictionary alloc] initWithDictionary:currentSurvey];//
            NSLog(@"Survey Data Updated: %@",[self.surveyData JSONFragment]);
            surveyDataTitles = [NSMutableArray arrayWithArray:[self.surveyData objectForKey:@"Order"]];
            surveyDataFields = [NSMutableDictionary dictionaryWithDictionary:[self.surveyData objectForKey:@"Fields"]];
        }
        [self.tableView reloadData];
    }
    
    
}

- (void)didChangeSurveyFormNotes:(NSString *)notesString {
	Survey *currentSurvey = [self.appointment appointmentToSurvey];
	[currentSurvey saveSurveyNotes:notesString forSurveyDataDictionary:surveyData];
}

#pragma mark - Survey Information
- (NSDictionary *)surveyInformationForKey:(NSString *)key surveyData:(NSDictionary *)dataDictionary {
	NSMutableDictionary *surveyInformation = [NSMutableDictionary dictionary];
	NSDictionary *data = [[dataDictionary objectForKey:@"Fields"] objectForKey:key];
    
	NSInteger filled = 0;
	NSInteger total = 0;
    
	if ([[data objectForKey:@"Type"] isEqualToString:@"Menu"]) {
		NSArray *fields = [data objectForKey:@"Order"];
		for (NSString *field in fields) {
			NSDictionary *information = [self surveyInformationForKey:field surveyData:data];
            
			filled += [[information objectForKey:kFilledFields] integerValue];
			total += [[information objectForKey:kTotalFields] integerValue];
		}
	}
	else
    {
		filled = [[self.appointment appointmentToSurvey] surveyFieldsFilled:data];
		total = [[data objectForKey:@"Order"] count];
	}
    
	[surveyInformation setObject:[NSNumber numberWithInteger:filled] forKey:kFilledFields];
	[surveyInformation setObject:[NSNumber numberWithInteger:total] forKey:kTotalFields];
    
	return surveyInformation;
}

- (IBAction)onClickCompleteAppointment:(id)sender
{
    if(!isEmpty(self.appointment.appointmentToProperty.propertyToDetector)){
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_MARK_COMPLETED") message:LOC(@"KEY_ALERT_MARK_COMPLETED_MESSAGE") delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_NO") otherButtonTitles:LOC(@"KEY_ALERT_YES"),nil];
        alert.tag = AlertViewTagMarkComplete;
        [alert show];
    }
    else{
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Please add detectors before completing the survey." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    
    
}

-(void)keyboardDidShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets;
    contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0);
    self.tableView.contentInset = contentInsets;
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:self.selectedControl.tag inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
}
-(void)keyboardWillHide:(NSNotification *)notification
{
    self.tableView.contentInset = UIEdgeInsetsZero;
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsZero;
}
-(void) dealloc
{

}
@end
