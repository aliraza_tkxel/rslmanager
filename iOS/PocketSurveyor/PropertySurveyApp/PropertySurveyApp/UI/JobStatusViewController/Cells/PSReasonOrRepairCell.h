//
//  PSReasonOrRepairCell.h
//  PropertySurveyApp
//
//  Created by My Mac on 22/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSJobStatusViewController.h"




@interface PSReasonOrRepairCell : UITableViewCell

@property (weak,    nonatomic) id<PSJobStatusPickerOptionProtocol> delegate;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong,  nonatomic) IBOutlet UIButton *btnPicker;
@property (strong, nonatomic) IBOutlet UIImageView *imgSeperator;
@property (strong, nonatomic) NSArray *pickerOptions;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (strong, nonatomic) NSString *lblHeader;
- (IBAction) onClickBtnPicker:(id)sender;

@end