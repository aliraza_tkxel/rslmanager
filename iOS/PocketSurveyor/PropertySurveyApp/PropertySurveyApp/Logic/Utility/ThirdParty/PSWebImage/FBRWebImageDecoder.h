
#import <Foundation/Foundation.h>


@protocol FBRWebImageDecoderDelegate;

@interface FBRWebImageDecoder : NSObject
{
    NSOperationQueue *imageDecodingQueue;
}

+ (FBRWebImageDecoder *)sharedImageDecoder;
- (void)decodeImage:(UIImage *)image withDelegate:(id <FBRWebImageDecoderDelegate>)delegate userInfo:(NSDictionary *)info;

@end

@protocol FBRWebImageDecoderDelegate <NSObject>

- (void)imageDecoder:(FBRWebImageDecoder *)decoder didFinishDecodingImage:(UIImage *)image userInfo:(NSDictionary *)userInfo;

@end

@interface UIImage (ForceDecode)

+ (UIImage *)decodedImageWithImage:(UIImage *)image;

@end