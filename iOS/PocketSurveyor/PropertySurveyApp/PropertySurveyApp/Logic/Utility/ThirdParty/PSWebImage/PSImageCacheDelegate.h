
@class PSImageCache;

@protocol PSImageCacheDelegate <NSObject>

@optional
- (void)imageCache:(PSImageCache *)imageCache didFindImage:(UIImage *)image forKey:(NSString *)key userInfo:(NSDictionary *)info;
- (void)imageCache:(PSImageCache *)imageCache didNotFindImageForKey:(NSString *)key userInfo:(NSDictionary *)info;

@end
