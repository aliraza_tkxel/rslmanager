//  Created by Imran on 2/11/13.
//  Copyright (c) 2013 Hirasoft Software Solutions. All rights reserved.
//

#import "UIImageView+FABCategory.h"


@implementation UIImageView (FABCategory)

+ (id) imageViewWithImageNamed:(NSString*)imageName{
	return [[[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]] autorelease];
}

+ (id) imageViewWithFrame:(CGRect)frame{
	return [[[UIImageView alloc] initWithFrame:frame] autorelease];
}

@end
