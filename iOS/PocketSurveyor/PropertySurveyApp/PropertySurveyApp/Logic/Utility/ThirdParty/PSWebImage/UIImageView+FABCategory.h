//  Created by Imran on 2/11/13.
//  Copyright (c) 2013 Hirasoft Software Solutions. All rights reserved.
//


#import <UIKit/UIKit.h>

/** Additional functionality for `UIImageView`.  */
@interface UIImageView (FABCategory)

/** Returns a image view with an image of the given image name.
 @param imageName The name of the image to create the imagename with.
 @return Returns `UIImageView` object.
 */
+ (UIImageView*) imageViewWithImageNamed:(NSString*)imageName;

/** Returns a image view with the given frame.
 @param frame The frame of the view.
 @return Returns `UIImageView` object.
 */
+ (UIImageView*) imageViewWithFrame:(CGRect)frame;

@end
