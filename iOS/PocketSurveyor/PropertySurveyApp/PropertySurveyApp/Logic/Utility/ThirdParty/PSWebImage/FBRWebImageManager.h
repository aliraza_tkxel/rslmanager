/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "FBRWebImageDownloaderDelegate.h"
#import "FBRWebImageManagerDelegate.h"
#import "PSImageCacheDelegate.h"

typedef enum
{
    FBRWebImageRetryFailed = 1 << 0,
    FBRWebImageLowPriority = 1 << 1,
    FBRWebImageCacheMemoryOnly = 1 << 2
} FBRWebImageOptions;

@interface FBRWebImageManager : NSObject <FBRWebImageDownloaderDelegate, PSImageCacheDelegate>
{
    NSMutableArray *downloadDelegates;
    NSMutableArray *downloaders;
    NSMutableArray *cacheDelegates;
    NSMutableArray *cacheURLs;
    NSMutableDictionary *downloaderForURL;
    NSMutableArray *failedURLs;
}

+ (id)sharedManager;
- (UIImage *)imageWithURL:(NSURL *)url;
- (void)downloadWithURL:(NSURL *)url delegate:(id<FBRWebImageManagerDelegate>)delegate;
- (void)downloadWithURL:(NSURL *)url delegate:(id<FBRWebImageManagerDelegate>)delegate options:(FBRWebImageOptions)options;
- (void)storeImage:(UIImage *)image forKey:(NSString *)key;


- (void)downloadWithURL:(NSURL *)url delegate:(id<FBRWebImageManagerDelegate>)delegate retryFailed:(BOOL)retryFailed __attribute__ ((deprecated)); // use options:SDWebImageRetryFailed instead
- (void)downloadWithURL:(NSURL *)url delegate:(id<FBRWebImageManagerDelegate>)delegate retryFailed:(BOOL)retryFailed lowPriority:(BOOL)lowPriority __attribute__ ((deprecated)); // use options:SDWebImageRetryFailed|SDWebImageLowPriority instead
- (void)cancelForDelegate:(id<FBRWebImageManagerDelegate>)delegate;

@end
