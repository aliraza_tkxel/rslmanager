//
//  MWPhotoBrowser.h
//  MWPhotoBrowser
//
//  Created by Michael Waterfall on 14/10/2010.
//  Copyright 2010 d3i. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "MWPhotoProtocol.h"
#import "MWCaptionView.h"

// Debug Logging
#if 0 // Set to 1 to enable debug logging
#define MWLog(x, ...) CLS_LOG(x, ## __VA_ARGS__);
#else
#define MWLog(x, ...)
#endif

// Delgate
@class SwipePhotoBrowser;
@protocol MWPhotoBrowserDelegate <NSObject>
- (NSUInteger)numberOfPhotosInPhotoBrowser:(SwipePhotoBrowser *)photoBrowser;
- (id<MWPhoto>)photoBrowser:(SwipePhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index;
@optional
- (MWCaptionView *)photoBrowser:(SwipePhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index;
@end

// MWPhotoBrowser
@interface SwipePhotoBrowser : UITableViewCell <UIScrollViewDelegate>

// Properties
@property (nonatomic,strong) NSMutableArray * allPhotos;
@property (assign) NSUInteger currentPageIndex;

@property(nonatomic,strong) MBProgressHUD * progressHUD;

-(void) refreshImage;

+(CGFloat) getHeight;

@end


