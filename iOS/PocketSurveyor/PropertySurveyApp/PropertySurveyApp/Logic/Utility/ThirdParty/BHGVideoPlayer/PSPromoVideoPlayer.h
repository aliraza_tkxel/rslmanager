//
//  PSPromoVideoPlayer.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 29/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@interface PSPromoVideoPlayer : UIView
@property (nonatomic) AVPlayer *avPlayer;
-(void) playPromoVideoWithName:(NSString *) resourceName;
@end
