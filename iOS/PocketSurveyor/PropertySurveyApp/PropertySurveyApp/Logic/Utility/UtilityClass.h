//
//  UtilityClass.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilityClass : NSObject

+ (BOOL) isUserLoggedIn;
+ (NSString *) userInfo;
+ (NSString *) userAgent;
+ (NSString *) applicationDocumentsDirectory;
+ (NSString *) statusStringForAppointmentStatus:(AppointmentStatus)status;
+ (AppointmentStatus) completionStatusForAppointmentType:(AppointmentType)type;
+(NSNumber*) statusNumberForAppointmentSyncStatus:(AppointmentSyncStatus) status;
+(AppointmentSyncStatus) syncStatusForAppointmentStatus:(NSInteger) status;
+(NSString*) getAppVersionStringForCompletedAppt;

+ (NSDate *) convertServerDateToNSDate:(NSString *)serverDate;
+ (NSString *)convertNSDateToServerDate:(NSDate *)localDate;
+ (NSString *) stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat;
+ (NSDate *) dateFromString:(NSString *)dateString dateFormat:(NSString *)dateFormat;


+ (BOOL) isEmailValid:(NSString *)email;
+(bool) isValidNumber:(NSString*)number;
+(NSString *) getValidNumber:(NSString*) str;
+ (NSString *) removeWhiteSpacesFromString:(NSString*)str;
+(NSString *)getAPPVersionString;
+(NSString *) getStringWithNewLineCharacters:(NSString*)string;
+(NSString*) getUniqueIdentifierStringWithUserName:(NSString*)username;
@end
