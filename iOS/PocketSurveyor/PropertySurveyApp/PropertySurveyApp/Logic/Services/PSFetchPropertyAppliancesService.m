//
//  PSFetchPropertyAppliancesService.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFetchPropertyAppliancesService.h"

@implementation PSFetchPropertyAppliancesService

- (id)init {
    if ((self = [super initWithServiceUrl:kFetchPropertyAppliacnesServiceURL]) != nil)
    {
       // [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}
#pragma mark PSFetchAppliancesService

- (NSData *) fetchPropertyAppliacnes:(NSDictionary *)parameters;
{
     return [super executeSyncRequest:parameters enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.fetchPropertyAppliancesDelegate != nil && [self.fetchPropertyAppliancesDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.fetchPropertyAppliancesDelegate service:self didFailWithError:nil];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification object:nil];
    }
    return NO;
}

@end
