//
//  PSDeleteAppointmentService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDeleteAppointmentService.h"

@implementation PSDeleteAppointmentService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kDeleteAppointmentServiceURL]) != nil) {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

#pragma mark PSDeleteAppointmentService


- (void)deleteAppointment:(NSMutableDictionary *)parameters  {
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];    
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.deleteAppointmentDelegate != nil && [self.deleteAppointmentDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.deleteAppointmentDelegate service:self didFailWithError:nil];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsDataRemoveFailureNotification object:nil];
    }
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    if(self.deleteAppointmentDelegate != nil && [self.deleteAppointmentDelegate respondsToSelector:@selector(service:didReceiveDeleteAppointmentResponse:)])
    {
        [self.deleteAppointmentDelegate service:self didReceiveDeleteAppointmentResponse:jsonObject];
    }
}

@end
