//
//  PSFetchAppliancesService.h
//  PropertySurveyApp
//
//  Created by TkXel on 06/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSFetchAppliancesService;
@protocol PSFetchAppliancesServiceDelegate <NSObject>
@optional
- (void) service:(PSFetchAppliancesService *)service didFinishLoadingAppliances:(id)appliancesData;
- (void) service:(PSFetchAppliancesService *)service didFailWithError:(NSError *)error;
- (void) service:(PSFetchAppliancesService *)service didReceiveDataBytes:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength;

@end

@interface PSFetchAppliancesService : PSARESTService <CoreRESTServiceDelegate>

@property (weak, nonatomic) id<PSFetchAppliancesServiceDelegate> appliacneDelegate;

- (void)fetchAllAppliacnes:(NSMutableDictionary *)parameters;

@end
