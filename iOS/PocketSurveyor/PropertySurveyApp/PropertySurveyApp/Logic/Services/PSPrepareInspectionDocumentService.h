//
//  PSPrepareInspectionDocument.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-31.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSPrepareInspectionDocumentService;

@protocol PSPrepareInspectionDocumentServiceDelegate <NSObject>
@optional
- (void) service:(PSPrepareInspectionDocumentService *)service didReceiveInspectionDocumentResponse:(id)inspectionResponse;
- (void) service:(PSPrepareInspectionDocumentService *)service didFailInspectionDocumentWithError:(NSError *)error;
@end


@interface PSPrepareInspectionDocumentService : PSARESTService <CoreRESTServiceDelegate>
@property (weak, nonatomic) id<PSPrepareInspectionDocumentServiceDelegate> prepareDocumentDelegate;
-(void)prepareStockInspectionDocument:(NSMutableDictionary *)parameters;

@end
