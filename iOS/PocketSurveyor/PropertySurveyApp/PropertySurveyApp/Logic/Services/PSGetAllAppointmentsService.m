//
//  PSGetAllAppointmentsService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSGetAllAppointmentsService.h"

@implementation PSGetAllAppointmentsService

#pragma mark Initialization

- (id)init
{
	if ((self = [super initWithServiceUrl:kGetAllStockAppointmentsServiceURL]) != nil)
	{
			[self setMethod:@"POST"];
			[self setContentType:@"application/json"];
	}
	return (self);
}

#pragma mark PSGetAllAppointmentsService


-(void)getAllAppointments:(NSMutableDictionary *)parameters
{
	[super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
}

@end
    