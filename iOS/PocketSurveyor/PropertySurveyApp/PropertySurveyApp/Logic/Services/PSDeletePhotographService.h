//
//  PSCreateAppointmentService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSDeletePhotographService;
@protocol PSDeletePhotographServiceDelegate <NSObject>
@optional

- (void) service:(PSDeletePhotographService *)service didReceiveCreateAppointmentResponse:(id)surveyorData;

- (void) service:(PSDeletePhotographService *)service didFailWithError:(NSError *)error;

@end

@interface PSDeletePhotographService : PSARESTService <CoreRESTServiceDelegate> {
    
}

@property (weak, nonatomic) id<PSDeletePhotographServiceDelegate> serviceDelegate;

- (void) deletePhotograph:(NSDictionary *)parameters;
- (NSDictionary*) deleteSyncPhotograph:(NSDictionary *)parameters;


@end
