//
//  PSCreateAppointmentService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSUploadPhotographService;
@protocol PSUploadPhotographServiceDelegate <NSObject>
@optional

- (void) service:(PSUploadPhotographService *)service didReceivePhotographData:(id)surveyorData;

- (void) service:(PSUploadPhotographService *)service didFailWithError:(NSError *)error;

@end

@interface PSUploadPhotographService : PSARESTService <CoreRESTServiceDelegate> {
    
}

@property (strong, nonatomic) id<PSUploadPhotographServiceDelegate> photographDelegate;

- (void) uploadPhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph;

- (void) updateProfilePhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph;

- (id)initForProfilePicture;


@end
