//
//  PSFetchDefectsService.h
//  PropertySurveyApp
//
//  Created by Yawar on 27/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSFetchDefectsService;
@protocol PSFetchDefectsDelegate <NSObject>
@optional
- (void) service:(PSFetchDefectsService *)service didFinishLoadingAppliancesDefects:(id)defectsData;
- (void) service:(PSFetchDefectsService *)service didFailWithError:(NSError *)error;
@end
@interface PSFetchDefectsService : PSARESTService <CoreRESTServiceDelegate>
- (NSData *) fetchApplianceDefects:(NSDictionary *)parameters;
@end
