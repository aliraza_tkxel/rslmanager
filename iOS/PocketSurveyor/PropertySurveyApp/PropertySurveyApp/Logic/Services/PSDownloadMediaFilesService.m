//
//  PSDownloadMediaFilesService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDownloadMediaFilesService.h"

@implementation PSDownloadMediaFilesService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kFetchSurveyFormServiceURL]) != nil) {
    }
    return (self);
}

#pragma mark PSDownloadMediaFilesService


- (void)downloadMediaFile:(NSMutableDictionary *)parameters  {
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];    
}



@end
