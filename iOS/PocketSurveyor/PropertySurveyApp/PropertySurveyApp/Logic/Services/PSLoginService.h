//
//  PSLoginService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSLoginService : PSARESTService {

}

- (void)loginMe:(NSMutableDictionary *)parameters;
- (NSData *) performSyncLogin:(NSMutableDictionary *)parameters;
@end
