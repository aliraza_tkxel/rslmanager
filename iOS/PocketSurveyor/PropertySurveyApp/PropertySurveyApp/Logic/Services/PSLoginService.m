//
//  PSLoginService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSLoginService.h"

@implementation PSLoginService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kLoginServiceURL]) != nil) {
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"]; //@"application/x-www-form-urlencoded"
    }
    return (self);
}

#pragma mark PSLoginService


- (void)loginMe:(NSMutableDictionary *)parameters  {
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
}

- (NSData *) performSyncLogin:(NSMutableDictionary *)parameters  {
    return [super executeSyncRequest:parameters enableCompression:NO];
}

@end
