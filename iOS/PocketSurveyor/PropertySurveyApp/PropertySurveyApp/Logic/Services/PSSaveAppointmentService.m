//
//  PSSaveAppointmentService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSaveAppointmentService.h"

@implementation PSSaveAppointmentService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kSaveAppointmentServiceURL]) != nil) {
    }
    return (self);
}

#pragma mark PSSaveAppointmentService


- (void)saveAppointment:(NSMutableDictionary *)parameters  {
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];    
}



@end
