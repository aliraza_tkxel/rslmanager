//
//  PSForgotPasswordService.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/02/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@interface PSForgotPasswordService : PSARESTService
- (NSData *) forgotPassword:(NSDictionary *)requestParameters;
@end
