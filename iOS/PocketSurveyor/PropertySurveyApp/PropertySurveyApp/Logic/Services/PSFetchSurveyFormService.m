//
//  PSFetchSurveyFormService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFetchSurveyFormService.h"

@implementation PSFetchSurveyFormService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kFetchSurveyFormServiceURL]) != nil) {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

#pragma mark PSFetchSurveyFormService

- (NSInteger) getDownloadPercentage
{
    return _downloadPercentage;
}

- (void)fetchStockSurveyForm:(NSMutableDictionary *)parameters  {
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
}

//- (void) service:(PSFetchSurveyFormService *)service didReceivePercentData:(NSInteger)percentage;


#pragma mark - CoreRESTServiceDelegate
-(void)connection:(CoreURLConnection *)connection didFailWithError:(NSError *)error
{
    [super connection:connection didFailWithError:error];
    if(self.surveyDelegate != nil && [self.surveyDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.surveyDelegate service:self didFailWithError:nil];
    }
}

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.surveyDelegate != nil && [self.surveyDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.surveyDelegate service:self didFailWithError:nil];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyFormSaveFailureNotification object:nil];
    }
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
//    NSDictionary *responseDictionary = [jsonObject JSONValue];
//    //NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
//    NSArray *appointmentsArray = [responseDictionary objectForKey:kResponseTag];
    if(self.surveyDelegate != nil && [self.surveyDelegate respondsToSelector:@selector(service:didFinishDownloadingSurveyForm:)])
    {
        [self.surveyDelegate service:self didFinishDownloadingSurveyForm:jsonObject];
    }
}

- (void)restService:(CoreRESTService *)service hasDownloaded:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength
{
    CLS_LOG(@"Download Progress: %d, expectedContentLength: %d", downloadedBytes, expectedContentLength);
    if (self.surveyDelegate && [self.surveyDelegate respondsToSelector:@selector(service:didReceiveDataBytes:expectedContentLength:)]) {
        [self.surveyDelegate service:self didReceiveDataBytes:downloadedBytes expectedContentLength:expectedContentLength];
    }
}

@end
