//
//  PSDeleteAppointmentService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSDeleteAppointmentService;
@protocol PSDeleteAppointmentServiceDelegate <NSObject>
@optional
- (void) service:(PSDeleteAppointmentService *)service didReceiveDeleteAppointmentResponse:(id)surveyorData;
- (void) service:(PSDeleteAppointmentService *)service didFailWithError:(NSError *)error;
@end

@interface PSDeleteAppointmentService : PSARESTService <CoreRESTServiceDelegate> 

@property (weak, nonatomic) id<PSDeleteAppointmentServiceDelegate> deleteAppointmentDelegate;

- (void)deleteAppointment:(NSMutableDictionary *)parameters;

@end
