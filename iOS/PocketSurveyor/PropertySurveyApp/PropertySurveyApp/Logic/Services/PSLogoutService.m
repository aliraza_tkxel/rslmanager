//
//  PSLogoutService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSLogoutService.h"

@implementation PSLogoutService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kLogoutServiceURL]) != nil) {
    }
    return (self);
}

#pragma mark PSLogoutService


- (void)logoutMe:(NSMutableDictionary *)parameters  {
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
}

@end
