//
//  PSSaveApplianceService.m
//  PropertySurveyApp
//
//  Created by Yawar on 11/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSaveApplianceService.h"

@implementation PSSaveApplianceService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kSaveAppliacneServiceURL]) != nil)
    {
        //[self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

- (NSData *) saveAppliance:(NSDictionary *)parameters
{
    return [super executeSyncRequest:parameters enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.saveApplianceDelegate != nil && [self.saveApplianceDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.saveApplianceDelegate service:self didFailWithError:nil];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
    }
    return NO;
}
@end
