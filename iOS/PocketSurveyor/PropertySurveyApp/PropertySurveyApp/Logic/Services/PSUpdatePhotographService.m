//
//  PSCreateAppointmentService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSUpdatePhotographService.h"

@implementation PSUpdatePhotographService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kSetDefaultPropertyPictureServiceURL]) != nil)
    {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/x-www-form-urlencoded"];
    }
    return (self);
}

#pragma mark PSCreateAppointmentService


- (void) updatePhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph
{
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
    
}


#pragma mark - CoreRESTServiceDelegate

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.photographDelegate != nil && [self.photographDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.photographDelegate service:self didFailWithError:nil];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentFailureNotification object:nil];
    }

    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    //    NSDictionary *responseDictionary = [jsonObject JSONValue];
    //    //NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
    //    NSArray *appointmentsArray = [responseDictionary objectForKey:kResponseTag];
  
    if(self.photographDelegate != nil && [self.photographDelegate respondsToSelector:@selector(service:didReceivePhotographData:)])
    {
        [self.photographDelegate service:self didReceivePhotographData:jsonObject];
    }
    
    
   
    
}




@end
