//
//  PSFetchJobRepairDataService.h
//  PropertySurveyApp
//
//  Created by Yawar on 02/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@interface PSFetchJobRepairDataService : PSARESTService<CoreRESTServiceDelegate>
- (NSData *) fetchJobRepairDataList:(NSDictionary *)parameters;

@end
