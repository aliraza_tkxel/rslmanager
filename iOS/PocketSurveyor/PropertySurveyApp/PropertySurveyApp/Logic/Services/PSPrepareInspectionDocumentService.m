//
//  PSPrepareInspectionDocument.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2015-07-31.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSPrepareInspectionDocumentService.h"

@implementation PSPrepareInspectionDocumentService

- (id)init
{
	if ((self = [super initWithServiceUrl:@"/appointment/prepareStockInspectionDocument"]) != nil)
	{
		[self setMethod:@"POST"];
		[self setContentType:@"application/json"];
	}
	return (self);
}

-(void)prepareStockInspectionDocument:(NSMutableDictionary *)parameters
{
	[super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.prepareDocumentDelegate != nil && [self.prepareDocumentDelegate respondsToSelector:@selector(service:didFailInspectionDocumentWithError:)])
    {
        [self.prepareDocumentDelegate service:self didFailInspectionDocumentWithError:nil];
    }
    
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    if(self.prepareDocumentDelegate != nil && [self.prepareDocumentDelegate respondsToSelector:@selector(service:didReceiveInspectionDocumentResponse:)])
    {
        [self.prepareDocumentDelegate service:self didReceiveInspectionDocumentResponse:jsonObject];
    }
}


@end
