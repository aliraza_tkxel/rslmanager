//
//  PSFetchJobPauseReasonsService.m
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSFetchJobPauseReasonsService.h"

@implementation PSFetchJobPauseReasonsService
- (id)init {
    if ((self = [super initWithServiceUrl:kFetchPauseReasonServiceURL]) != nil)
    {
        // [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}
#pragma mark PSFetchAppliancesService

- (NSData *) fetchJobPauseReasons:(NSDictionary *)parameters
{
    return [super executeSyncRequest:parameters enableCompression:NO];
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchJobPauseReasonsSaveFailureNotification object:nil];
    return NO;
}
@end
