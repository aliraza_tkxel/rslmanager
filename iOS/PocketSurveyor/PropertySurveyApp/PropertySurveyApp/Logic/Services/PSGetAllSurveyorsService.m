//
//  PSGetAllSurveyorsService.m
//  PropertySurveyApp
//
//  Created by Yawar on 14/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSGetAllSurveyorsService.h"

@implementation PSGetAllSurveyorsService

- (id)init {
    if ((self = [super initWithServiceUrl:kGetAllSurveyorsServiceURL]) != nil) {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/json"];
    }
    return (self);
}

#pragma mark PSgetSurveyorervice

- (void)getAllSurveyors:(NSMutableDictionary *)parameters  {
    [super executeRequest:parameters responseClass:nil streamLocation:nil enableCompression:NO];
    
}


#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.surveyorDelegate != nil && [self.surveyorDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyorSaveFailureNotification object:nil];
    }
    return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    //    NSDictionary *responseDictionary = [jsonObject JSONValue];
    //    //NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
    //    NSArray *appointmentsArray = [responseDictionary objectForKey:kResponseTag];
    if(self.surveyorDelegate != nil && [self.surveyorDelegate respondsToSelector:@selector(service:didFinishLoadingSurveyors:)])
    {
        [self.surveyorDelegate service:self didFinishLoadingSurveyors:jsonObject];
    }
}

#pragma mark CoreURLConnectionDelegate

- (void)connection:(CoreURLConnection *)connection didFailWithError:(NSError *)error {
	// post a notification that we can't reach the service
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyorSaveFailureNotification object:nil];
}

@end
