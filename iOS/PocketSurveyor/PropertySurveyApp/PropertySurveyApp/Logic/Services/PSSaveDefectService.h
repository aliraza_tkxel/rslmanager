//
//  PSSaveDefectService.h
//  PropertySurveyApp
//
//  Created by Yawar on 28/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"
@class PSSaveDefectService;
@protocol PSSaveDefectServiceDelegate <NSObject>
@optional

- (void) service:(PSSaveDefectService *)service didReceiveDefectDataAfterSuccess:(id)surveyorData;

- (void) service:(PSSaveDefectService *)service didFailWithError:(NSError *)error;

@end
@interface PSSaveDefectService : PSARESTService <CoreRESTServiceDelegate>
- (void) saveApplianceDefect:(NSDictionary *)parameters;
@property (strong, nonatomic) id<PSSaveDefectServiceDelegate> defectDelegate;
@end
