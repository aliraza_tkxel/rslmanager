//
//  PSGetAllPropertiesService.h
//  PropertySurveyApp
//
//  Created by Yawar on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSGetAllPropertiesService;
@protocol PSGetAllPropertiesServiceDelegate <NSObject>
@optional
- (void) service:(PSGetAllPropertiesService *)service didFinishLoadingProperties:(id)propertyData;
- (void) service:(PSGetAllPropertiesService *)service didFailWithError:(NSError *)error;
@end

@interface PSGetAllPropertiesService : PSARESTService <CoreRESTServiceDelegate>
@property (weak, nonatomic) id<PSGetAllPropertiesServiceDelegate> propertyDelegate;

- (void)getAllProperties:(NSMutableDictionary *)parameters;

@end
