//
//  PSLogoutService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@interface PSLogoutService : PSARESTService {
    
}

- (void)logoutMe:(NSMutableDictionary *)parameters;

@end
