//
//  PSCreateAppointmentService.m
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSUploadDefectPictureService.h"

@implementation PSUploadDefectPictureService

#pragma mark Initialization

- (id)init {
    if ((self = [super initWithServiceUrl:kUploadDefectPictureServiceURL]) != nil)
    {
        [self setDelegate:self];
        [self setMethod:@"POST"];
        [self setContentType:@"application/x-www-form-urlencoded"];
    }
    return (self);
}

#pragma mark PSCreateAppointmentService


- (void) uploadPhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph
{
    NSData *file;
	NSString *path;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

	path = [paths objectAtIndex:0];
    
	path = [path stringByAppendingPathComponent:kTemporaryImageFileName];
    
    [[NSFileManager defaultManager] createFileAtPath:path
                                            contents:UIImageJPEGRepresentation(photograph, 1.0)
                                          attributes:nil];
    
    [super executeRequest:parameters responseClass:nil streamLocation:path enableCompression:NO];
    
}


#pragma mark - CoreRESTServiceDelegate

- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    if(self.photographDelegate != nil && [self.photographDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.photographDelegate service:self didFailWithError:nil];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
    }

    return NO;
}

-(void)connection:(CoreURLConnection *)connection didFailWithError:(NSError *)error
{
    if(self.photographDelegate != nil && [self.photographDelegate respondsToSelector:@selector(service:didFailWithError:)])
    {
        [self.photographDelegate service:self didFailWithError:nil];
        
    }
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    //    NSDictionary *responseDictionary = [jsonObject JSONValue];
    //    //NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
    //    NSArray *appointmentsArray = [responseDictionary objectForKey:kResponseTag];
  
    if(self.photographDelegate != nil && [self.photographDelegate respondsToSelector:@selector(service:didReceivePhotographData:)])
    {
        [self.photographDelegate service:self didReceivePhotographData:jsonObject];
    }
    
    NSString *path;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [paths objectAtIndex:0];
	path = [path stringByAppendingPathComponent:kTemporaryImageFileName];
	NSError *error;
	if ([[NSFileManager defaultManager] fileExistsAtPath:path])		//Does file exist?
	{
		if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error])	//Delete it
		{
			CLS_LOG(@"Delete file error: %@", error);
		}
        
    }
    
    //[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification object:nil];
    
}




@end
