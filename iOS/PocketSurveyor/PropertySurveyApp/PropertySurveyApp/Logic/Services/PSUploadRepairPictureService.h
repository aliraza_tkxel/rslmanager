//
//  PSUploadRepairPictureService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSUploadRepairPictureService;

@protocol PSUploadRepairPictureServiceDelegate <NSObject>
@optional

- (void) service:(PSUploadRepairPictureService *)service didReceivePhotographData:(id)surveyorData;

- (void) service:(PSUploadRepairPictureService *)service didFailWithError:(NSError *)error;

@end

@interface PSUploadRepairPictureService : PSARESTService <CoreRESTServiceDelegate> {
    
}

@property (strong, nonatomic) id<PSUploadRepairPictureServiceDelegate> photographDelegate;

- (void) uploadPhotograph:(NSDictionary *)parameters streamLocation:(UIImage*) photograph;

@end
