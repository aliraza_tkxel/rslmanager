//
//  PSFetchJobPauseReasonsService.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@interface PSFetchJobPauseReasonsService : PSARESTService <CoreRESTServiceDelegate>
- (NSData *) fetchJobPauseReasons:(NSDictionary *)parameters;
@end
