//
//  PSFetchPropertyAppliancesService.h
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"
@class PSFetchPropertyAppliancesService;

@protocol PSFetchPropertyAppliancesDelegate <NSObject>
@optional
- (void) service:(PSFetchPropertyAppliancesService *)service didFinishLoadingPropertyAppliances:(id)appliancesData;
- (void) service:(PSFetchPropertyAppliancesService *)service didFailWithError:(NSError *)error;
@end

@interface PSFetchPropertyAppliancesService : PSARESTService <CoreRESTServiceDelegate>
@property (weak, nonatomic) id<PSFetchPropertyAppliancesDelegate> fetchPropertyAppliancesDelegate;

- (NSData *) fetchPropertyAppliacnes:(NSDictionary *)parameters;
@end
