//
//  PSCreateAppointmentService.h
//  PropertySurveyApp
//
//  Created by TkXel on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSARESTService.h"

@class PSCreateAppointmentService;
@protocol PSCreateAppointmentDelegate <NSObject>
@optional
- (void) service:(PSCreateAppointmentService *)service didReceiveCreateAppointmentResponse:(id)surveyorData;
- (void) service:(PSCreateAppointmentService *)service didFailWithError:(NSError *)error;
@end

@interface PSCreateAppointmentService : PSARESTService <CoreRESTServiceDelegate> {
    
}
@property (weak, nonatomic) id<PSCreateAppointmentDelegate> createAppointmentDelegate;
- (NSData *) createNewAppointment:(NSDictionary *)parameters;

@end
