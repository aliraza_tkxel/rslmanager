//
//  ReachabilityManager.m
//  Reachability
//
//  Created by Rizwan Sumra on 2/1/11.
//

#import "ReachabilityManager.h"


@implementation ReachabilityManager

static ReachabilityManager* _sharedObject;

+(ReachabilityManager*) sharedManager
{
	if (_sharedObject == nil)
	{
		_sharedObject = [[ReachabilityManager alloc] init];
	}
	return _sharedObject;
}

- (void) dealloc
{
	if (hostReach)
	{
		[hostReach stopNotifier];
		[hostReach release];
		hostReach = nil;
	}
	
	if (internetReach)
	{
		[internetReach stopNotifier];
		[internetReach release];
		internetReach = nil;
	}
	
	if (wifiReach)
	{
		[wifiReach stopNotifier];
		[wifiReach release];
		wifiReach = nil;
	}
	[super dealloc];
}

#pragma mark -
#pragma mark Main Methods
#pragma mark -

- (void) startMonitoring
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    //Change the host name here to change the server your monitoring
	hostReach = [[NReachability reachabilityWithHostName: HOST] retain];
	[hostReach startNotifier];
	
    internetReach = [[NReachability reachabilityForInternetConnection] retain];
	[internetReach startNotifier];
	
    wifiReach = [[NReachability reachabilityForLocalWiFi] retain];
	[wifiReach startNotifier];
}

- (void) stopMonintoring
{
	[hostReach stopNotifier];
    [internetReach stopNotifier];
    [wifiReach stopNotifier];
}

#pragma mark -
#pragma mark Methods
#pragma mark -

//Called by Reachability whenever status changes.
- (void) reachabilityChanged: (NSNotification* )note
{
    internetReach = note.object;
    if ([self isConnectedToInternet]) {
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:NOTIF_CONNECTIVITY_CHANGED object:note.object]];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:NOTIF_NO_CONNECTIVITY object:note.object]];
    }
}

#pragma mark -
#pragma mark Connection Methods
#pragma mark -

-(bool) isConnectedToInternet
{
	return [self isConnectedViaWiFi] || [self isConnectedViaWAN];
}

-(bool) isConnectedViaWiFi
{
	//if ([internetReach currentReachabilityStatus] == NReachableViaWiFi || [wifiReach currentReachabilityStatus] == NReachableViaWiFi)
	if ([internetReach isReachableViaWiFi] || [wifiReach isReachableViaWiFi])
	return true;
	
	return false;
}
-(bool) isConnectedViaWAN
{
	if (/*[internetReach currentReachabilityStatus] == NReachableViaWWAN ||*/ [internetReach isReachableViaWWAN] /*|| [wifiReach currentReachabilityStatus] == NReachableViaWWAN */|| [wifiReach isReachableViaWWAN])
		return true;
	return false;
}

-(bool) isHostReachable
{
	return true;
}

@end
