//
//  ReachabilityManager.h
//  Reachability
//
//  Created by Rizwan Sumra on 2/1/11.
//

#import <Foundation/Foundation.h>
#import "NReachability.h"

#define HOST @"http://www.apple.com"

@interface ReachabilityManager : NSObject {
	NReachability* hostReach;
    NReachability* internetReach;
    NReachability* wifiReach;
}

+(ReachabilityManager*) sharedManager;
- (void) startMonitoring;
- (void) stopMonintoring;

-(bool) isConnectedViaWiFi;
-(bool) isConnectedViaWAN;
-(bool) isHostReachable;
-(bool) isConnectedToInternet;
@end
