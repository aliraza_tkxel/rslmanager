//
//  NotificationNames.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//


#pragma mark -
#pragma mark Notifications

#define NOTIF_NO_CONNECTIVITY               @"NOTIF_NO_CONNECTIVITY"
#define NOTIF_CONNECTIVITY_CHANGED          @"NOTIF_CONNECTIVITY_CHANGED"


/*!
 @discussion
 Notification kCommonUnableToAccessServices is Posted on web page not found.
 */

#define kCommonUnableToAccessServices @"kCommonUnableToAccessServices"


/*!
 @discussion
 Notification kInternetUnavailable is Posted if internet is unavailable.
 */

#define kInternetUnavailable @"kInternetUnavailable"

/*!
 @discussion
 Notification kRequestTimeOutOccur is Posted if internet request timeout.
 */

#define kInternetRequestTimeOut @"kInternetRequestTimeOut"


/*!
 @discussion
 Notification kCommonUnexpectedServerError is Posted when unexpected server error occured.
 */

#define kCommonUnexpectedServerError @"kCommonUnexpectedServerError"

/*!
 @discussion
 Notification kCommonSessionTimeout is Posted when session timeout occured.
 */

#define kCommonSessionTimeout @"kCommonSessionTimeout"

/*!
 @discussion
 Notification kCoreDataResetNotification is Posted on Reset Core Data Event.
 */
#define kCoreDataResetNotification @"kCoreDataResetNotification"

/*!
 @discussion
 Notification kAuthorizationFailureNotification is Posted when authorization fails.
 */

#define kAuthorizationFailureNotification @"kAuthorizationFailureNotification"

/*!
 @discussion
 Notification kAuthorizationSuccessNotification is Posted when authorization success.
 */

#define kAuthorizationSuccessNotification @"kAuthorizationSuccessNotification"

/*!
 @discussion
 Notification kAppointmentsSaveSuccessNotification is Posted on Successfull Population of appointments in Core Database.
 */
#define kAppointmentsSaveSuccessNotification @"kAppointmentsSaveSuccessNotification"

/*!
 @discussion
 Notification kAppointmentsSaveFailureNotification is Posted, If any error is cncountered during appointsment fetching or loading.
 */
#define kAppointmentsSaveFailureNotification @"kAppointmentsSaveFailureNotification"

/*!
 @discussion
 Notification kAppointmentsNoResultFoundNotification is Posted, If no appointments are found
 */
#define kAppointmentsNoResultFoundNotification @"kAppointmentsNoResultFoundNotification"

/*!
 @discussion
 Notification kSurveyFormSaveSucessNotification is Posted on successfull download and save of survey form.
 */
#define kSurveyFormSaveSucessNotification @"kSurveyFormSaveSucessNotification"

/*!
 @discussion
 Notification kSurveyFormSaveSucessNotification is Posted on failure of suvery form download or save.
 */
#define kSurveyFormSaveFailureNotification @"kSurveyFormSaveFailureNotification"

/*!
 @discussion
 Notification kSurveyorSaveSuccessNotification is Posted on Successfull Population of surveyors in Core Database.
 */
#define kSurveyorSaveSuccessNotification @"kSurveyorSaveSuccessNotification"

/*!
 @discussion
 Notification kSurveyorSaveFailureNotification is Posted if any error is encountered during surveyor fetching or loading.
 */
#define kSurveyorSaveFailureNotification @"kSurveyorSaveFailureNotification"


/*!
 @discussion
 Notification kPropertySaveSuccessNotification is Posted on Successfull Population of properites in Core Database.
 */
#define kPropertySaveSuccessNotification @"kPropertySaveSuccessNotification"

/*!
 @discussion
 Notification kPropertySaveFailureNotification is Posted if any error is encountered during property fetching or loading.
 */
#define kPropertySaveFailureNotification @"kPropertySaveFailureNotification"


/*!
 @discussion
 Notification kPropertyPictureSaveFailureNotification is Posted if any error is encountered during property picture uploading.
 */
#define kPropertyPictureSaveFailureNotification @"kPropertyPictureSaveFailureNotification"


/*!
 @discussion
 Notification kPropertyPictureSaveNotification is Posted on successfull save of property picture uploading.
 */

#define kPropertyPictureSaveNotification @"kPropertyPictureSaveNotification"

/*!
 @discussion
 Notification kRepairPictureSaveNotification is Posted on successfull save of repair property picture uploading.
 */

#define kRepairPictureSaveNotification @"kRepairPictureSaveNotification"

/*!
 @discussion
 Notification kRepairPictureSaveFailureNotification is Posted if any error is encountered during property picture uploading.
 */
#define kRepairPictureSaveFailureNotification @"kRepairPictureSaveFailureNotification"


/*!
 @discussion
 Notification kPropertyPictureDefaulteNotification is Posted if any error is encountered during property picture set as default.
 */

#define kPropertyPictureDefaultNotification @"kPropertyPictureDefaulteNotification"


/*!
 @discussion
 Notification kPropertyPicturesRemoveNotification is Posted if any error is encountered during property picture uploading.
 */
#define kPropertyPicturesRemoveNotification @"kPropertyPicturesRemoveNotification"


/*!
 @discussion
 Notification kUserInfoSaveCompleted is Posted on Successfull Saving the LoggedIn User Data.
 */
#define kUserInfoSaveCompleted @"kUserInfoSaveCompleted"

/*!
 @discussion
 Notification kUserSignOutNotification is Posted When User Authentication Fails or User Explicitly Signs Out.
 */
#define kUserSignOutNotification @"kUserSignOutNotification"

/*!
 @discussion
 Notification kAccomodationInfoUpdateSuccessNotification is Posted on Successfull Update of Accomodation roomWidth and roomLength.
 */
#define kAccomodationInfoUpdateSuccessNotification @"kAccomodationInfoUpdateSuccessNotification"

/*!
 @discussion
 Notification kTenantInfoUpdateSuccessNotification is Posted on Successfull Update of Tenant Email, Telephone and Mobile.
 */
#define kTenantInfoUpdateSuccessNotification @"kTenantInfoUpdateSuccessNotification"


/*!
 @discussion
 Notification kAppointmentStartNotification is Posted on Successfull Starting the appointment.
 */
#define kAppointmentStartNotification @"kAppointmentStartNotification"


/*!
 @discussion
 Notification kAppointmentsDataRemoveSuccessNotification is Posted on Successfull Removal of Appointments Data.
 */
#define kAppointmentsDataRemoveSuccessNotification @"kAppointmentsDataRemoveSuccessNotification"

/*!
 @discussion
 Notification kAppointmentsDataRemoveFailureNotification is Posted on Successfull Removal of Appointments Data.
 */
#define kAppointmentsDataRemoveFailureNotification @"kAppointmentsDataRemoveFailureNotification"

/*!
 @discussion
 Notification kNewAppointmentSuccessNotification is Posted on Successfull Creation of New Appointment.
 */
#define kNewAppointmentSuccessNotification @"kNewAppointmentSuccessNotification"

/*!
 @discussion
 Notification kNewAppointmentFailureNotification is Posted on Unsuccessfull Creation of New Appointment.
 */
#define kNewAppointmentFailureNotification @"kNewAppointmentFailureNotification"

/*!
 @discussion
 Notification kCompleteAppointmentSuccessNotification is Posted on Successfull Completion of Appointment.
 */
#define kCompleteAppointmentSuccessNotification @"kCompleteAppointmentSuccessNotification"

/*!
 @discussion
 Notification kCompleteAppointmentFailureNotification is Posted on Unsuccessfull Completeion of Appointment.
 */
#define kCompleteAppointmentFailureNotification @"kCompleteAppointmentFailureNotification"


/*!
 @discussion
 Notification kUpdateAppointmentsSuccessNotification is Posted on Successfull Completion of Appointments.
 */
#define kUpdateAppointmentsSuccessNotification @"kUpdateAppointmentsSuccessNotification"

/*!
 @discussion
 Notification kUpdateAppointmentsFailureNotification is Posted on Unsuccessfull Update of Appointments.
 */
#define kUpdateAppointmentsFailureNotification @"kUpdateAppointmentsFailureNotification"

/*!
 @discussion
 Notification kSaveAppliacnesSuccessNotification is Posted on Successfull Download and Save/Update of Appliacnes Data.
 */
#define kSaveAppliacnesSuccessNotification @"kSaveAppliacnesSuccessNotification"

/*!
 @discussion
 Notification kSaveAppliacnesFailureNotification is Posted on Unsuccessfull Download and Save/Update of Appliacnes Data.
 */
#define kSaveAppliacnesFailureNotification @"kSaveAppliacnesFailureNotification"
/*!
 @discussion
 Notification kSaveNewAppliacneSuccessNotification is Posted on Successfull Saving of New Appliance.
 */
#define kSaveNewAppliacneSuccessNotification @"kSaveNewAppliacneSuccessNotification"

/*!
 @discussion
 Notification kSaveNewAppliacneFailureNotification is Posted on Unsuccessfull Saving of New Appliance.
 */
#define kSaveNewAppliacneFailureNotification @"kSaveNewAppliacneFailureNotification"

/*!
 @discussion
 Notification kUpdateCP12InfoSuccessNotification is Posted on Successfull Update of Appointment CP12Info.
 */
#define kUpdateCP12InfoSuccessNotification @"kUpdateCP12InfoSuccessNotification"

/*!
 @discussion
 Notification kUpdateCP12InfoFailureNotification is Posted on Unsuccessfull Update of Appointment CP12Info.
 */
#define kUpdateCP12InfoFailureNotification @"kUpdateCP12InfoFailureNotification"

/*!
 @discussion
 Notification kSaveApplianceInspectionFormSuccessNotification is posted on successful save of appliance inspection form.
 */
#define kSaveApplianceInspectionFormSuccessNotification @"kSaveApplianceInspectionFormSuccessNotification"

/*!
 @discussion
 Notification kSaveApplianceInspectionFormFailureNotification is posted on unsuccessful save of appliance inspection form.
 */
#define kSaveApplianceInspectionFormFailureNotification @"kSaveApplianceInspectionFormFailureNotification"

/*!
 @discussion
 Notification kSavePropertyPipeworkFormSuccessNotification is posted on successful save of property pipework form.
 */
#define kSavePropertyPipeworkFormSuccessNotification @"kSavePropertyPipeworkFormSuccessNotification"

/*!
 @discussion
 Notification kSavePropertyPipeworkFormFailureNotification is posted on unsuccessful save of property pipework form.
 */
#define kSavePropertyPipeworkFormFailureNotification @"kSavePropertyPipeworkFormFailureNotification"

/*!
 @discussion
 Notification kFetchPropertyAppliancesSuccessNotification is posted on successful save of property appliances.
 */
#define kFetchPropertyAppliancesSuccessNotification @"kFetchPropertyAppliancesSuccessNotification"

/*!
 @discussion
 Notification kFetchPropertyAppliancesDataDownloadedNotification is posted on successful download of property appliances to hide progress bar on appointments view
 */
#define kFetchPropertyAppliancesDataDownloadedNotification @"kFetchPropertyAppliancesDataDownloadedNotification"

/*!
 @discussion
 Notification kFetchPropertyAppliancesFailureNotification is posted on unsuccessful save of property appliances.
 */
#define kFetchPropertyAppliancesFailureNotification @"kFetchPropertyAppliancesFailureNotification"

/*!
 @discussion
 Notification kUpdateAppointmentWithNoEntrySuccessNotification is posted on successful update of no entry.
 */
#define kUpdateAppointmentWithNoEntrySuccessNotification @"kUpdateAppointmentWithNoEntrySuccessNotification"

/*!
 @discussion
 Notification kUpdateAppointmentWithNoEntryFailureNotification is posted on unsuccessful update of no entry.
 */
#define kUpdateAppointmentWithNoEntryFailureNotification @"kUpdateAppointmentWithNoEntryFailureNotification"

/*!
 @discussion
 Notification kFetchDefectsSaveSuccessNotification is posted on successful save of appliance defects.
 */
#define kFetchDefectsSaveSuccessNotification @"kFetchDefectsSaveSuccessNotification"

/*!
 @discussion
 Notification kFetchDefectsSaveFailureNotification is posted on unsuccessful save of appliance defects.
 */
#define kFetchDefectsSaveFailureNotification @"kFetchDefectsSaveFailureNotification"

/*!
 @discussion
 Notification kFetchDefectCategoriesSaveSuccessNotification is posted on successful save of defect categories.
 */
#define kFetchDefectCategoriesSaveSuccessNotification @"kFetchDefectCategoriesSaveSuccessNotification"

/*!
 @discussion
 Notification kFetchDefectCategoriesSaveFailureNotification is posted on unsuccessful save of defect categories.
 */
#define kFetchDefectCategoriesSaveFailureNotification @"kFetchDefectCategoriesSaveFailureNotification"

/*!
 @discussion
 Notification kSaveDefectSuccessNotification is posted on successful save of new sefect.
 */
#define kSaveDefectSuccessNotification @"kSaveDefectSuccessNotification"

/*!
 @discussion
 Notification kSaveDefectFailureNotification is posted on unsuccessful save of new defect.
 */
#define kSaveDefectFailureNotification @"kSaveDefectFailureNotification"

/*!
 @discussion
 Notification kFetchFaultRepairDataSaveSuccessNotification is posted on successful save of fault repair data.
 */
#define kFetchFaultRepairDataSaveSuccessNotification @"kFetchFaultRepairDataSaveSuccessNotification"

/*!
 @discussion
 Notification kFetchFaultRepairDataSaveFailureNotification is posted on unsuccessful save of fault repair data.
 */
#define kFetchFaultRepairDataSaveFailureNotification @"kFetchFaultRepairDataSaveFailureNotification"

/*!
 @discussion
 Notification kJobStartNotification is Posted on Successfull Starting the (fault) job.
 */
#define kJobStartNotification @"kJobStartNotification"

/*!
 @discussion
 Notification kJobStatusUpdateNotification is posted on job status update.
 */
#define kJobStatusUpdateNotification @"kJobStatusUpdateNotification"

/*!
 @discussion
 Notification kFetchJobPauseReasonsSaveSuccessNotification is poseted on successfull save of job pause reasons.
 */
#define kFetchJobPauseReasonsSaveSuccessNotification @"kFetchJobPauseReasonsSaveSuccessNotification"

/*!
 @discussion
 Notification kFetchJobPauseReasonsSaveFailureNotification is posted on unsuccessfull save of job pause reasons.
 */
#define kFetchJobPauseReasonsSaveFailureNotification @"kFetchJobPauseReasonsSaveFailureNotification"


/*!
 @discussion
 Notification kSavePauseJobDataSuccessNotification is posted on successfull save of job pause data.
 */
#define kSavePauseJobDataSuccessNotification @"kSavePauseJobDataSuccessNotification"

/*!
 @discussion
 Notification kSaveJobPauseDataSuccessNotification is posted on successfull save of job pause data when job is paused.
 */
#define kSaveJobPauseDataSuccessNotification @"kSaveJobPauseDataSuccessNotification"

/*!
 @discussion
 Notification kSaveJobPauseDataFailureNotification is posted on unsuccessfull save of job pause data when job is paused.
 */
#define kSaveJobPauseDataFailureNotification @"kSaveJobPauseDataFailureNotification"

/*!
 @discussion
 Notification kUpdateFaultAppointmentWithNoEntrySuccessNotification is posted on successful update of no entry for fault appointment.
 */
#define kUpdateFaultAppointmentWithNoEntrySuccessNotification @"kUpdateFaultAppointmentWithNoEntrySuccessNotification"

/*!
 @discussion
 Notification kUpdateFaultAppointmentWithNoEntryFailureNotification is posted on unsuccessful update of no entry for fault appointment.
 */
#define kUpdateFaultAppointmentWithNoEntryFailureNotification @"kUpdateFaultAppointmentWithNoEntryFailureNotification"

/*!
 @discussion
 Notification kSaveFaultRepairListSuccessNotification is posted on successful save of fault repair list in job data.
 */
#define kSaveFaultRepairListSuccessNotification @"kSaveFaultRepairListSuccessNotification"

/*!
 @discussion
 Notification kSaveFaultRepairListFailureNotification is posted on unsuccessful save of fault repair list in job data.
 */
#define kSaveFaultRepairListFailureNotification @"kSaveFaultRepairListFailureNotification"

/*!
 @discussion
 Notification kJobCompleteSuccessNotification is posted on successful complettion ofjob data.
 */
#define kJobCompleteSuccessNotification @"kJobCompleteSuccessNotification"

/*!
 @discussion
 Notification kJobCompleteFailureNotification is posted on unsuccessful complettion ofjob data.
 */
#define kJobCompleteFailureNotification @"kJobCompleteFailureNotification"

/*!
 @discussion
 Notification kAppointmentTimeNotAvailableNotification is posted if appointment time 
 for new appointment is unavailable.
 */
#define kAppointmentTimeNotAvailableNotification @"kAppointmentTimeNotAvailableNotification"

/*!
 @discussion
 Notification kAppointmentTimeNotAvailableBankHolidayNotification is posted if appointment time
 for new appointment is unavailable.
 */
#define kAppointmentTimeNotAvailableBankHolidayNotification @"kAppointmentTimeNotAvailableBankHolidayNotification"

/*!
 @discussion
 Notification kUpdateDetectorSuccessNotification is posted on successful update of detector.
 */
#define kUpdateDetectorSuccessNotification @"kUpdateDetectorSuccessNotification"

/*!
 @discussion
 Notification kUpdateDetectorFailureNotification is posted on unsuccessful update of detector.
 */
#define kUpdateDetectorFailureNotification @"kUpdateDetectorFailureNotification"

/*!
 @discussion
 Notification kAppointmentStatusUpdateSuccessNotification is posted on successful update of appointment status.
 */
#define kAppointmentStatusUpdateSuccessNotification @"kAppointmentStatusUpdateSuccessNotification"

/*!
 @discussion
 Notification kAppointmentStatusUpdateFailureNotification is posted on unsuccessful update of appointment status.
 */
#define kAppointmentStatusUpdateFailureNotification @"kAppointmentStatusUpdateFailureNotification"

/*!
 
 @discussion
 Notification kUpdatePasswordSuccessNotification is posted on successful update of user's password.
 */
#define kUpdatePasswordSuccessNotification @"kUpdatePasswordSuccessNotification"
/*!
 
 @discussion
 Notification kUpdatePasswordFailureNotification is posted on unsuccessful update of user's password.
 */
#define kUpdatePasswordFailureNotification @"kUpdatePasswordFailureNotification"

/*!
 @discussion
 Notification kForgotPasswordSuccessNotification is posted on success response
 */
#define kForgotPasswordSuccessNotification @"kForgotPasswordSuccessNotification"

/*!
 @discussion
 Notification kForgotPasswordFailureNotification is posted on failure response.
 */
#define kForgotPasswordFailureNotification @"kForgotPasswordFailureNotification"

/*!
 @discussion
 Notification kAppointmentModificationStatusChangeSuccessNotification is posted on successfully changing appointment modification false.
 */
#define kAppointmentModificationStatusChangeSuccessNotification @"kAppointmentModificationStatusChangeSuccessNotification"

/*!
 @discussion
 Notification kAppointmentModificationStatusChangeFailureNotification is posted on unsuccessful changing appointment modification false.
 */
#define kAppointmentModificationStatusChangeFailureNotification @"kAppointmentModificationStatusChangeFailureNotification"


/*!
 @discussion
 Notification kRefreshAppointmentsExceptionReceiveNotification is posted if an exception occurs on refreshing appointments.
 */
#define kRefreshAppointmentsExceptionReceiveNotification @"kRefreshAppointmentsExceptionReceiveNotification"

/*!
 @discussion
 Notification kOfflineLoginFailureReceiveNotification is posted if offline authentication fails.
 */
#define kOfflineLoginFailureReceiveNotification @"kOfflineLoginFailureReceiveNotification"

/*!
 @discussion
 Notification kAppointmentInspectionDocumentSuccessNotification is Posted on Successfull creation of inspection reportt.
 */
#define kAppointmentInspectionDocumentSuccessNotification @"kAppointmentInspectionDocumentSuccessNotification"

/*!
 @discussion
 Notification kAppointmentInspectionDocumentFailureNotification is Posted on failed creation of inspection report.
 */
#define kAppointmentInspectionDocumentFailureNotification @"kAppointmentInspectionDocumentFailureNotification"
