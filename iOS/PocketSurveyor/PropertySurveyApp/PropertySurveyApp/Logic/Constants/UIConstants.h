//
//  UIConstants.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

//Target: PropertySurveyApp

/*
 * Application Default Font Family Name
 */


//HelveticaNeue-Bold,
//HelveticaNeue-CondensedBlack,
//HelveticaNeue-Medium,
//HelveticaNeue,
//HelveticaNeue-Light,
//HelveticaNeue-CondensedBold,
//HelveticaNeue-LightItalic,
//HelveticaNeue-UltraLightItalic,
//HelveticaNeue-UltraLight,
//HelveticaNeue-BoldItalic,
//HelveticaNeue-Italic

#define     kFontFamilyHelveticaNeueLight           @"HelveticaNeue-Light"
#define     kFontFamilyHelveticaNeueMedium          @"HelveticaNeue-Medium"
#define     kFontFamilyHelveticaNeueBold            @"HelveticaNeue-Bold"

/*
 * Navigation Bar
 */
#define     NAVIGATION_BAR_BG_COLOR                 0xFAF6F6
#define     NAVIGATION_BAR_TITLE_COLOR              0x706666



/*
 * Common
 */
#define     THEME_BG_COLOUR                         0xF7F4F1
#define     THEME_TEXT_COLOUR_DARK                  0x3B3B3B //MODIFIED FROM 000000
//#define     THEME_TEXT_COLOUR_LIGHT               0xD1B5B4
#define     THEME_TEXT_COLOUR_LIGHT                 0xA9A9A9
//#define     SECTION_HEADER_TEXT_COLOUR            0x9D9999
#define     SECTION_HEADER_TEXT_COLOUR              0xA9A9A9


/*
 * FilterView
 */
#define     FILTER_TABLEVIEW_BG_COLOUR                  0x6F6A6A
#define     FILTER_TABLEVIEW_HEADER_COLOUR              0X615A5A
#define     FILTER_TABLEVIEW_HEADER_TEXT_COLOUR         0xD1B5B4
#define     FILTER_TABLEVIEW_TOP_HEADER_BG_COLOUR       0x454545
#define     FILTER_TABLEVIEW_SEPERATOR_COLOUR           0x676362
#define     FILTER_LINE_SEPERATOR_COLOUR                0x4F4949



/*
 * LoginView
 */

#define     TEXT_FIELD_TEXT_COLOUR                      0xFFFFFF


/*
 * AppointmentViewController
 */


/*
 * AppointmentDetailViewController
 */

#define     APPOINTMENT_DETAIL_PROPERTY_NAME_COLOUR                   0x424242


