//
//  WebURLs.h
//  Logic
//
//  Created by Ahmad Ansari on 26/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

// Server Info
#pragma mark -
#pragma mark Server Path

/*
 *	HTTP_PROTOCOL Value
 *	1 = HTTP
 *	0 = HTTPS
 */
#define HTTP_PROTOCOL 0
#if HTTP_PROTOCOL
#define kURLScheme @"http:"
#else
#define kURLScheme @"https:"
#endif

/*!
 @discussion
 Server Domain Name
 */


#if DEV
    #define kDomainName @"devcrm.broadlandhousinggroup.org/PropertySurveyAPI"
#elif TEST
    #define kDomainName @"testcrm.broadlandhousinggroup.org/PropertySurveyAPI"
#elif UAT
    #define kDomainName @"uatcrm.broadlandhousinggroup.org/PropertySurveyAPI"
#else
    #define kDomainName @"crm.broadlandhousinggroup.org/PropertySurveyAPI"
#endif

/*!
 @discussion
 Server URL. Formed Using Server Domain Name
 */
#define kServerUrl [NSString stringWithFormat:@"%@//%@", kURLScheme, kDomainName]


/*!
 @discussion
 Request URL for login service
 */
#define kLoginServiceURL @"/authenticate/user"

/*!
 @discussion
 Request URL for logout service
 */
#define kLogoutServiceURL @"/iphonesurveyapp/authenticate/logout"


/*!
 @discussion
 Request URL for get all appointments service
 */
#define kGetAllStockAppointmentsServiceURL @"/appointment/getStockAppointments"

/*!
 @discussion
 Request URL for get all upload defect picture service
 */

#define kUploadDefectPictureServiceURL @"/faults/saveDefectImage"

/*!
 @discussion
 Request URL for get all upload defect picture service
 */

#define kUploadRepairPictureServiceURL @"/faults/saveFaultRepairImage"


/*!
 @discussion
 Request URL for get all upload picture service
 */
#define kUploadPropertyPictureServiceURL @"/property/uploadPropertyImages"

/*!
 @discussion
 Request URL for get all upload picture service
 */
#define kUploadPropertyProfilePictureServiceURL @"/property/updatePropertyDefaultImage"

/*!
 @discussion
 Request URL for get all upload picture service
 */
#define kSetDefaultPropertyPictureServiceURL @"/property/uploadPropertyImages"

/*!
 @discussion
 Request URL for get all delete picture service
 */
#define kDeletePropertyPictureServiceURL @"/property/deletePropertyImage"

//devcrm.broadlandhousinggroup.org/StockConditionOfflineAPI/property/uploadPropertyImages?

/*!
 @discussion
 Request URL for Complete appointment service
 */
#define kUpdateCompleteAppointmentServiceURL @"/appointment/CompleteAppointment"


/*!
 @discussion
 Request URL for create new appointment service
 */
#define kCreateAppointmentServiceURL @"/appointment/saveStockAppointment"

/*!
 @discussion
 Request URL for save appointment service
 */
#define kSaveAppointmentServiceURL @""


/*!
 @discussion
 Request URL for delete appointment service
 */
#define kDeleteAppointmentServiceURL @"/appointment/deleteAppointment"

/*!
 @discussion
 Request URL for fetch survey form service
 */
#define kFetchSurveyFormServiceURL @"/appscreen/getSurveys"

/*!
 @discussion
 Request URL for download media files service
 */
#define kDownloadMediaFilesServiceURL @""

/*!
 @discussion
 Request URL for download surveyors service
 */
#define kGetAllSurveyorsServiceURL @"/appointment/surveyors"

/*!
 @discussion
 Request URL for downloading property service
 */
#define kGetAllPropertiesServiceURL @"/property/searchProperty"

/*!
 @discussion
 Request URL for fetching appliances data
 */
#define kGetAppliacnesServiceURL @"/Appliances/getAppliancesData"

/*!
 @discussion
 Request URL for Saving new appliance data
 */
#define kSaveAppliacneServiceURL @"/appliances/saveAnAppliance"

/*!
 @discussion
 Request URL for fetching property appliances
 */
#define kFetchPropertyAppliacnesServiceURL @"/appliances/getApplianceByPropertyId"

/*!
 @discussion
 Request URL for fetching Gas Appointment Defects
 */
 #define kGetGasDefectsServiceURL @"/faults/fetchGasDefects"
//////////////////////
/*!
 @discussion
 Request URL for Saving New Gas Appointment Defect
 */
#define kSaveDefectsServiceURL @"/faults/saveFaultsGas"

/*!
 @discussion
 Request URL for fetching job repair data list
 */
#define kFetchFaultDataServiceURL @"/faults/fetchFaultRepairList"

/*!
 @discussion
 Request URL for fetching job pause reasons list
 */
#define kFetchPauseReasonServiceURL @"/faults/fetchPauseReasonList"

/*!
 @discussion
 Request URL for updating user's password
 */
#define kUpdatePasswordServiceURL @"/authenticate/updatePassword"

/*!
 @discussion
 Request URL for forgotten password email
 */
#define kForgotPasswordServiceURL @"/authenticate/emailForgottenPassword"

/*!
 @li OLD CALL
 */
#define kGetGasDefects @"/iphonesurveyapp/faults/get/gas" // ... Journalid=5312&username=adnan.mirza&salt=NOKQJPTPSLQP-NOKQJPTP

/*!
 @discussion
 Request URL for fetching Defect Categories

 @li OLD CALL
 */
#define kGetDefectsCategories @"/iphonesurveyapp/faults/get/defects" // ... username=adnan.mirza&salt=WOPHNGMQGAGW-WOPHNGMQ


