//
//  Constants.h
//  Logic
//
//  Created by Ahmad Ansari on 26/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//



/*!
 @discussion
 Localization File Path.
 */
#define LOC(key)    NSLocalizedStringFromTable((key), @"Localizable", @"")


/*!
 @discussion
 Key "kLogsEnabled" is used to control the application logging on console. Key is used in info.plist.
 */
#define kLogsEnabled @"LogsEnabled"

#define kSurveyKeySystem @"Systems"
#define kSurveyKeyCO @"CO"
#define kSurveyKeySmoke @"Smoke"

/*!
 @discussion
 ApplicationType represents the Target Application type. Like Stock Application, Gas Application, etc.
 */
typedef NS_ENUM(NSInteger, ApplicationType) {
    ApplicationTypeStock = 0,
    ApplicationTypeGas = 1,
    ApplicationTypeFault = 2,
    ApplicationTypeMaintenance = 3,
};


/*!
 @discussion
 Appointment Types Constant String Values.
 */
static NSString * kApplicationTypeStock         __attribute__((unused)) = @"Stock";
static NSString * kApplicationTypeGas           __attribute__((unused)) = @"Gas";
static NSString * kApplicationTypeFault         __attribute__((unused)) = @"Fault";
static NSString * kApplicationTypeMaintenance   __attribute__((unused)) = @"Maintenance";

/*!
 @discussion
 AuthStatus: Represent the current user authentication status received from server on posting an authentication call.
 */
typedef NS_ENUM(NSInteger, AuthStatus) {
	AuthStatusSuccess = 0,
	AuthStatusInvalidUsername = 1,
	AuthStatusInvalidPassword = 2,
	AuthStatusInactiveUser = 3,
    AuthStatusUnregisteredUser = 4
};

/*!
 @discussion
 PSFilterOption: Appointments Filter Options. Used to filter the appointment list on schdule view.
 */
typedef NS_ENUM(NSInteger, PSFilterOption)
{
    PSFilterOptionViewAll = 0,
    PSFilterOptionViewToday = 1,
    PSFilterOptionViewNextFiveDays = 2,
    PSFilterOptionSortByName = 3,
    PSFilterOptionSortByDate = 4,
    PSFilterOptionShowOnlyStock = 5,
    PSFilterOptionShowOnlyInProgress = 6,
    PSFilterOptionShowOnlyPrePost = 7,
    PSFilterOptionShowOnlyArranged = 8,
    PSFilterOptionShowOnlyGas = 9,
    PSFilterOptionShowOnlyCompleted = 10,
    PSFilterOptionShowOnlyVoid = 11,
    PSFilterOptionShowOnlyFault = 12,
    PSFilterOptionShowOnlyPlanned = 13,
    PSFilterOptionShowOnlyAdaptations = 14,
    PSFilterOptionShowOnlyMiscellaneous = 15,
    PSFilterOptionShowOnlyConditionRatingTool = 16
};

/*!
 @discussion
 PSTextAlignment Definitions according iOS Vesion.
 */
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_6_0
    #define PSTextAlignmentLeft         NSTextAlignmentLeft // Visually left aligned
    #define PSTextAlignmentCenter       NSTextAlignmentCenter // Visually centered
    #define PSTextAlignmentRight        NSTextAlignmentRight // Visually right aligned
    #define PSTextAlignmentJustified    NSTextAlignmentJustified // Fully-justified. The last line in a paragraph is natural-aligned.
    #define PSTextAlignmentNatural      NSTextAlignmentNatural // Indicates the default alignment for script
#else
    #define PSTextAlignmentCenter UITextAlignmentCenter

    #define PSTextAlignmentLeft         UITextAlignmentLeft // Visually left aligned
    #define PSTextAlignmentCenter       UITextAlignmentCenter // Visually centered
    #define PSTextAlignmentRight        UITextAlignmentRight // Visually right aligned

#endif


/*!
 @discussion
 kFilterOptions Key used in Filter Options Dictionary.
 */
#define kFilterOptions  @"kFilterOptions"

/*!
 @discussion
 Appointment Types Constant String Values.
 */
static NSString * kAppointmentTypeStock         __attribute__((unused)) = @"Stock";
static NSString * kAppointmentTypeGas           __attribute__((unused)) = @"Gas";
static NSString * kAppointmentTypeVoid          __attribute__((unused)) = @"Void";
static NSString * kAppointmentTypeFault         __attribute__((unused)) = @"Fault";
static NSString * kAppointmentTypePre           __attribute__((unused)) = @"Pre";
static NSString * kAppointmentTypePost          __attribute__((unused)) = @"Post";
static NSString * kAppointmentTypePlanned       __attribute__((unused)) = @"Planned";
//by Waqas
static NSString * kAppointmentTypeAdaptations   __attribute__((unused)) = @"Adaptation";
static NSString * kAppointmentTypeMiscellaneous __attribute__((unused)) = @"Miscellaneous";
static NSString * kAppointmentTypeCondition     __attribute__((unused)) = @"Condition";

/*!
 @discussion
 Appointment Staus Constant String Values.
 */
static NSString * kAppointmentStatusPaused     __attribute__((unused)) = @"Paused";
static NSString * kAppointmentStatusInProgress __attribute__((unused)) = @"InProgress";
static NSString * kAppointmentStatusComplete   __attribute__((unused)) = @"Complete";
static NSString * kAppointmentStatusNotStarted __attribute__((unused)) = @"NotStarted";
static NSString * kAppointmentStatusFinished   __attribute__((unused)) = @"Finished";
static NSString * kAppointmentStatusNoEntry    __attribute__((unused)) = @"No Entry";

/*!
 @discussion
 Job Staus Constant String Values.
 */
static NSString * kJobStatusPaused      __attribute__((unused)) = @"Paused";
static NSString * kJobStatusInProgress  __attribute__((unused)) = @"InProgress";
static NSString * kJobStatusComplete    __attribute__((unused)) = @"Complete";
static NSString * kJobStatusNotStarted  __attribute__((unused)) = @"NotStarted";
static NSString * kJobStatusFinished    __attribute__((unused)) = @"Finished";
static NSString * kJobStatusNoEntry     __attribute__((unused)) = @"No Entry";


static NSString * kLocalNotificationStringInProgress = @"You have a job \"In Progress\", Please check if this should be set to 'Paused' or 'Complete'";


/*!
 @discussion
 Defect Type Constant String Values.
 */
static NSString * kDefectTypeAppliance      __attribute__((unused)) = @"applianceDefect";
static NSString * kDefectTypeDetector       __attribute__((unused)) = @"detectorDefect";


/*!
 @discussion
 Date Time Styles.
 */
#define kDateTimeStyleLong  @"yyyy-MM-dd HH:mm:ss Z"
#define kDateTimeStyle1     @"dd MMM yyyy"
#define kDateTimeStyle2     @"EEE MMM dd, YYYY"
#define kDateTimeStyle3     @"HH:mm"
#define kDateTimeStyle4     @"HH:mm:ss"
#define kDateTimeStyle5     @"MM/dd/yyyy HH:mm"//@"dd/MM/yyyy HH:mm"
#define kDateTimeStyle6     @"E MMM d yyyy HH:mm:ss"
#define kDateTimeStyle7     @"HH:mm dd/MM/yyyy"
#define kDateTimeStyle8     @"dd/MM/yyyy"
#define kDateTimeStyle9     @"E MMM d, yyyy"
#define kDateTimeStyle10    @"E, MMM d yyyy"
#define kDateTimeStyle11    @"MMM d, yyyy"
#define kDateTimeStyle12    @"MMM dd,yyyy, HH:mm"
#define kDateTimeStyle13    @"MMM dd,yyyy, h:mm a"
#define kDateTimeStyle14    @"MMM d, HH:mm a"
#define kDateTimeStyle15    @"MM/dd/yyyy hh:mm:ss a"
#define kDateTimeStyle16    @"dd/MM/yyyy HH:mm:ss"
#define kDateTimeStyle17    @"EEEE, dd MMM yyyy"
#define kDateTimeStyle18    @"MMM d yyyy"

#define kBackgroundImageTag 1001

/*!
 @discussion
 Service SuccessCode
 */
#define kSuccessCode        200

/*!
 @discussion
 Appointment Time Unavailable Code
 */
#define kAppointmentTimeUnavailableCode  31
#define kAppointmentTimeUnavailableBankHolidayCode  41

/*!
 @discussion
 Appointment Days Limit
 */
#define kAppointmentDaysLimit 28

/*!
 @discussion
 JSON Response Keys
 */

#define kStatusTag          @"status"
#define kResponseTag        @"response"
#define kCodeTag            @"code"
#define kMessageTag         @"message"
#define kErrorCodeTag       @"errorCode"
#define kAppointmentListTag @"appointmentList"
#define kCommonDataTag      @"commonData"
#define kDetectorTypesTag   @"detectorTypes"
#define kPowerTypesTag      @"powerTypes"
#define kPropertyInfoTag    @"propertyInfo"
#define kDetectorsTag       @"propertyDetectors"
#define kSurveyCommonTag    @"common"
#define kSurveyPlannedComponentsTag @"plannedComponents"
#define kSurveyCertificatesTag  @"certificates"
#define kSurveyHeatingFuelDataTag   @"fuelFieldsDataset"


#pragma mark - Alert View Tags
/*!
 @discussion
 Alert view tags.
 */

typedef NS_ENUM(NSInteger, AlertViewTag) {
    AlertViewTagLogout,
    AlertViewTagPostAppointmentData,
    AlertViewTagTrash,
    AlertViewTagMarkComplete,
    AlertViewTagNoEntry,
    AlertViewTagStartAppointment,
    AlertViewTagStartJob,
    AlertViewTagNoInternet,
    AlertViewTagRefreshAppointments,
    AlertViewTagChangePassword,
    AlertViewTagForgotPassword,
    AlertViewTagException,
    AlertViewTagFaultPicture,
    AlertViewTagFaultRepairsEmpty,
    AlertViewTagFailedToSyncFewAppointmentsOnServer,
    AlertViewTagFailedToRefreshAppointmentsOnDevice,
    AlertViewTagFailedToGenerateDocument,
    AlertViewTagFailedToSyncImages,
    AlertViewTagFailedtoSyncData

};

#pragma mark - CameraView Images Types Tags
/*!
 @discussion
 CameraView Images Types Tags.
 */

typedef NS_ENUM(NSInteger, CameraViewImageTypeTag) {
    CameraViewImageTypeDefect,
    CameraViewImageTypeOther
    
};

#pragma mark imageNames for AppointmentCellIcons
#define kCustomerNotesImageName @"editCustomerNotes.png"
#define kAsbestosImageName @"alert-yellow.png"
#define kRiskImageName @"alert-red.png"



