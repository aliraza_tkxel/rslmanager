//
//  PSLocalNotificationManager.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 11/03/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSLocalNotificationManager : NSObject

+(id)sharedManager;
-(void)scheduleInProgressFor12HourNotificationForAppointmentID:(NSNumber *)appointmentID;
-(void)cancelInProgressFor12HourNotificationForAppointmentID:(NSNumber*)appointmentID;
@end
