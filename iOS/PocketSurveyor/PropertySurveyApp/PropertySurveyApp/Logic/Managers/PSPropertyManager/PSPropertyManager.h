//
//  PSPropertyManager.h
//  PropertySurveyApp
//
//  Created by Yawar on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSGetAllPropertiesService.h"
#import "PSPropertyListViewController.h"
@interface PSPropertyManager : NSObject <CoreRESTServiceDelegate, PSGetAllPropertiesServiceDelegate>

@property (nonatomic, strong) PSGetAllPropertiesService *getPropertiesService;
@property (nonatomic, strong) id<PSPropertyListProtocol> delegate;
+ (PSPropertyManager *) sharedManager;
- (void) fetchAllProperties:(NSMutableDictionary *) requesParameters;
-(void) removeAllProperties;
@end
