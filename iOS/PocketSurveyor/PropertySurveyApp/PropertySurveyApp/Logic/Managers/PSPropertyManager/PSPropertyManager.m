//
//  PSPropertyManager.m
//  PropertySurveyApp
//
//  Created by Yawar on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSPropertyManager.h"

@implementation PSPropertyManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSPropertyManager *sharedPropertyManager = nil;
+(PSPropertyManager*) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPropertyManager = [[PSPropertyManager alloc] init];
        // Do any other initialisation stuff here

        sharedPropertyManager.getPropertiesService = [[PSGetAllPropertiesService alloc] init];
        sharedPropertyManager.getPropertiesService.propertyDelegate = sharedPropertyManager;
    });
    return sharedPropertyManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPropertyManager = [super allocWithZone:zone];
    });
    return sharedPropertyManager;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

#pragma mark - Methods

-(void) removeAllProperties
{
	NSManagedObjectContext * context = [PSDatabaseContext sharedContext].managedObjectContext;
	NSArray * allProperties = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([SProperty class]) sortKey:NULL sortAscending:TRUE context:context];
	for(SProperty * property in allProperties)
	{
		[PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[property objectID]];
	}
	[[PSDatabaseContext sharedContext] saveContext];
}

-(void) fetchAllProperties:(NSMutableDictionary *) requestParameters
{
	if([UtilityClass isUserLoggedIn])
	{
		@synchronized(self)
		{
			[self.getPropertiesService getAllProperties:requestParameters];
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertySaveFailureNotification object:nil];
	}
}

#pragma mark - PSGetAllPropertiesServiceDelegate
- (void) service:(PSGetAllPropertiesService *)service didFinishLoadingProperties:(id)propertyData
{
    NSDictionary *propertyDictionary = [propertyData JSONValue];
    NSDictionary *statusDictionary = [propertyDictionary objectForKey:kStatusTag];
    NSDictionary *responseDictionary = [propertyDictionary objectForKey:kResponseTag];
    NSInteger statusCode = [[statusDictionary objectForKey:kCodeTag] integerValue];
    if (statusCode == kSuccessCode)
    {
        NSDictionary *pageInfo = [responseDictionary objectForKey:@"PageInfo"];
        NSUInteger pageNumber = [[pageInfo valueForKey:@"PageNumber"] integerValue];
        if (!isEmpty(pageInfo) /* && pageNumber == 1 */)
        {
            if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didReceivePageInformation:)])
            {
                [(PSPropertyListViewController *)self.delegate didReceivePageInformation:pageInfo];
            }
        }
        else
        {
            if(self.delegate && [(NSObject *)self.delegate respondsToSelector:@selector(didReceivePageInformation:)])
            {
                [(PSPropertyListViewController *)self.delegate didReceivePageInformation:pageInfo];
            }
        }
        NSArray *propertyArray = [responseDictionary objectForKey:@"Properties"];
        if(!isEmpty(propertyArray))
        {
            // CLS_LOG(@"UserInfo : %@", propertyArray);
            [[PSDataPersistenceManager sharedManager] saveProperties:propertyArray];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertySaveSuccessNotification object:nil];
        }
    }
}

- (void) service:(PSGetAllPropertiesService *)service didFailWithError:(NSError *)error
{
    CLS_LOG(@"didFailWithError : %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertySaveFailureNotification object:nil];
    
}



@end
