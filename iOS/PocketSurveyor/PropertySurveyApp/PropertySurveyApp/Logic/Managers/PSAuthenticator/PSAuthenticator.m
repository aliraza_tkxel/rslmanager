//
//  PSAuthenticator.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAuthenticator.h"


/*
 * AuthStatus:	Represent the current user authentication status received from server on
 *				posting an authentication call.
 */
typedef NS_ENUM(NSInteger, AuthCode)
{
	AuthCodeSuccess = 200,
	AuthCodeInvalidUsername = 1,
	AuthCodeInvalidPassword = 2,
	AuthCodeInactiveUser = 3,
    AuthCodeUnregisteredUser = 4
};


@implementation PSAuthenticator

#pragma mark -
#pragma mark ARC Singleton Implementation

static PSAuthenticator *sharedPSAuthenticator = nil;
+ (PSAuthenticator *) sharedAuthenticator
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPSAuthenticator = [[PSAuthenticator alloc] init];
        // Do any other initialisation stuff here
        [sharedPSAuthenticator setLoginService:[[PSLoginService alloc]init]];
        [sharedPSAuthenticator.loginService setDelegate:sharedPSAuthenticator];
        [sharedPSAuthenticator setUpdatePasswordService:[[PSUpdatePasswordService alloc]init]];
        [sharedPSAuthenticator.updatePasswordService setDelegate:sharedPSAuthenticator];
        [sharedPSAuthenticator setForgotPasswordService:[[PSForgotPasswordService alloc]init]];

    });
    return sharedPSAuthenticator;
}

+ (id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPSAuthenticator = [super allocWithZone:zone];
    });
    return sharedPSAuthenticator;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

#pragma mark - Methods
- (void) authenticateUserWithName:(NSString *)userName password:(NSString *)password
{
    if(!isEmpty(userName) && !isEmpty(password))
    {
        NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
        [requestParameters setValue:userName forKey:@"username"];
        [requestParameters setValue:[SettingsClass sharedObject].deviceToken forKey:@"devicetoken"];
        [requestParameters setValue:password forKey:@"password"];
        [SettingsClass sharedObject].currentUserPassword = password;
        [self.loginService loginMe:requestParameters];
    }
}

- (BOOL) isUserAuthenticated
{
    return YES;
}

- (void) forgotPassword:(NSDictionary *)requestParameters
{
    NSData *responseData = [self.forgotPasswordService forgotPassword:requestParameters];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    if (!isEmpty(responseString))
    {
        NSDictionary *JSONDictionary = [responseString JSONValue];
        NSDictionary *statusDictionary = [JSONDictionary objectForKey:kStatusTag];
        NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
        NSInteger statusCode = [[statusDictionary objectForKey:kCodeTag] integerValue];
        if (statusCode == kSuccessCode)
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kForgotPasswordSuccessNotification object:responseDictionary];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kForgotPasswordFailureNotification object:statusDictionary];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kForgotPasswordFailureNotification object:nil];
    }

}

- (void) updatePassword:(NSDictionary *)requestParameters
{
    NSData *responseData = [self.updatePasswordService updatePassword:requestParameters];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    if (!isEmpty(responseString))
    {
        NSDictionary *JSONDictionary = [responseString JSONValue];
        NSDictionary *statusDictionary = [JSONDictionary objectForKey:kStatusTag];
        NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
        NSInteger statusCode = [[statusDictionary objectForKey:kCodeTag] integerValue];
        if (statusCode == kSuccessCode)
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdatePasswordSuccessNotification object:responseDictionary];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdatePasswordFailureNotification object:statusDictionary];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdatePasswordFailureNotification object:nil];
    }
}

- (void) authenticateUser
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        User *loggedInUser = [SettingsClass sharedObject].loggedInUser;
        if(loggedInUser)
        {
            BOOL isSessionTimeLimitPassed = [[SettingsClass sharedObject] isSessionTimeLimitPassed];

            if(isSessionTimeLimitPassed)
            {
                //Promt for Relogin.
                NSString *message = LOC(@"KEY_ALERT_SESSION_EXPIRED");
                [self _promptForReloginWithMessage:message];
            }
            else
            {
                NSString *userName = [SettingsClass sharedObject].loggedInUser.userName;
                NSString *password = [SettingsClass sharedObject].loggedInUser.password;
                
                if(!isEmpty(userName) && !isEmpty(password))
                {
                    NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
                    [requestParameters setValue:userName forKey:@"username"];
                    [requestParameters setValue:[SettingsClass sharedObject].deviceToken forKey:@"devicetoken"];
                    [requestParameters setValue:password forKey:@"password"];
                    [SettingsClass sharedObject].currentUserPassword = password;
                    NSData *responseData = [self.loginService performSyncLogin:requestParameters];
                    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
                    CLS_LOG(@"Auth Response: %@", responseString);
                    if(!isEmpty(responseString                                                                ))
                    {
                        NSDictionary *JSONDictionary = [responseString JSONValue];
                        NSDictionary *statusDictionary = [JSONDictionary objectForKey:kStatusTag];
                        NSInteger authCode = [[statusDictionary valueForKey:kCodeTag] intValue];
                        NSString *message = [statusDictionary valueForKey:kMessageTag];
                        switch (authCode) {
                            case AuthCodeSuccess:
                            {
                                NSDictionary *userInfo = [JSONDictionary objectForKey:kResponseTag];
                                [SettingsClass sharedObject].currentUserId = [userInfo valueForKey:kUserId];
                                [[PSDataPersistenceManager sharedManager] saveUserInfo:userInfo];
                            }
                                break;
                            case AuthCodeInvalidUsername:
                            {
                                //Username is invalid, intimate user and ask for relogin.
                                [self _promptForReloginWithMessage:message];
                            }
                                break;
                            case AuthCodeInvalidPassword:
                            {
                                //Password is invalid (wrong or changed), intimate user and ask for relogin.
                                [self _promptForReloginWithMessage:message];
                            }
                                break;
                            case AuthCodeInactiveUser:
                            {
                                //Non Registered User.
                                [self _promptForReloginWithMessage:message];
                            }
                                break;
                            case AuthCodeUnregisteredUser:
                            {
                                //Non Registered User.
                                [self _promptForReloginWithMessage:message];
                            }
                                break;
                                
                            default:
                            {
                                [self _promptForReloginWithMessage:message];
                            }
                                break;
                        }
                    }
                }
            }
        }
        else
        {
            //Promt for Relogin.
            NSString *message = @"Please Login";
            [self _promptForReloginWithMessage:message];
        }
    });
}

- (void) _promptForReloginWithMessage:(NSString *)message {
    if([NSThread currentThread] == [NSThread mainThread])
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:LOC(@"KEY_STRING_APP_TITLE")
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:LOC(@"KEY_ALERT_OK")
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
        [self performSelectorOnMainThread:@selector(_promptForReloginWithMessage:) withObject:message waitUntilDone:NO];
    }
}

#pragma mark UIAlertViewDelegate Method
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 0) {
		//Sign Out
		[[SettingsClass sharedObject] signOut];
	}
}

- (void) authenticateUserOfflineWithName:(NSString *)userName password:(NSString *)password
{
    BOOL isAuthenticationSuccess = NO;
    isAuthenticationSuccess = [[PSCoreDataManager sharedManager] authenticateUserOffline:userName password:password];
    if (isAuthenticationSuccess)
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAuthorizationSuccessNotification object:nil];
    }
    else
    {
        User *user = [[PSCoreDataManager sharedManager]userWithUserName:userName];
        if (user != nil)
        {
            NSMutableDictionary *responseDictionary = [NSMutableDictionary dictionary];
            [responseDictionary setObject:LOC(@"KEY_ALERT_INVALID_CREDENTIALS") forKey:@"message"];
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAuthorizationFailureNotification object:responseDictionary];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kOfflineLoginFailureReceiveNotification object:nil];
        }
        
    }
}

#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
    [SettingsClass sharedObject].currentUserPassword = nil;
    if(isEmpty(jsonObject)) {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAuthorizationFailureNotification object:nil];
    }
    else {
        NSDictionary *responseDictionary = [jsonObject JSONValue];
        NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAuthorizationFailureNotification object:statusDictionary];
    }
    return (YES);
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
    CLS_LOG(@"responseString %@",jsonObject);
    
    if(!isEmpty(jsonObject))
    {
        NSDictionary *responseDictionary = [jsonObject JSONValue];
        NSDictionary *userInfo = [responseDictionary objectForKey:kResponseTag];

        [SettingsClass sharedObject].currentUserId = [userInfo valueForKey:kUserId];
        [[PSDataPersistenceManager sharedManager] saveUserInfo:userInfo];
        
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAuthorizationSuccessNotification object:nil];
    }
}

@end
