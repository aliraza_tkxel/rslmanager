//
//  PSApplianceManager.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSSaveApplianceService.h"
#import "PSFetchPropertyAppliancesService.h"
#import "PSFetchDefectsService.h"
#import "PSSaveDefectService.h"

@interface PSApplianceManager : NSObject <PSSaveApplianceDelegate>

@property (nonatomic, strong) PSSaveApplianceService *saveApplianceService;
@property (nonatomic, strong) PSFetchPropertyAppliancesService *fetchPropertyAppliancesService;
@property (nonatomic, strong) PSFetchDefectsService *fetchApplianceDefectsService;
@property (nonatomic, strong) PSSaveDefectService *saveDefectService;

+ (PSApplianceManager *) sharedManager;

- (void) saveAppliance:(NSDictionary *)appliance forProperty:(NSManagedObjectID *)propertyId;

/**
 @brief saves data in offline
 */
- (void) saveAppliancePropertiesOffline:(NSArray *)locationArray typeArray:(NSArray *)typeArray manufecturerArray:(NSArray *)manufecturerArray modelArray:(NSArray *)modelArray;

/**
 @brief Returns All Appliacnces (Fetched from Core Database)
 */
- (NSArray *) fetchAllAppliances;

/**
 @brief Returns All Appliacnce Locations (Fetched from Core Database)
 */
- (NSArray *) fetchApplianceLoactions;
/**
 @brief Returns All Appliacnce Manufacturers (Fetched from Core Database)
 */
- (NSArray *) fetchApplianceManufacturers;
/**
 @brief Returns All Appliacnce Models (Fetched from Core Database)
 */
- (NSArray *) fetchApplianceModels;
/**
 @brief Returns All Appliacnce Types (Fetched from Core Database)
 */
- (NSArray *) fetchApplianceTypes;

/**
 @brief Returns YES if all appliances of a property have been inspected.
 */
- (BOOL) isApplianceInspected :(Property *)property;

/**
 @brief Returns count of inspected appliances of the property.
 */
- (NSNumber *) totalNumberOfAppliancesInspected :(Property *)property;

/**
 @brief Saves appliance's inspection form .
 */

- (void) saveInspectionForm:(NSDictionary *) inspectionFormDictionary forAppliance:(Appliance *) appliance;

/**
 @brief Saves property's pipework installation form .
 */
- (void) saveInstallationPipework:(NSDictionary *) pipeworkDictionary forProperty:(Property *)property;

/**
 @brief Fetches all appliances in property.
 */
- (void) fetchPropertyAppliances:(NSDictionary *)requestParameters forProperty:(NSManagedObjectID *)propertyId;

/**
 @brief Fetches all appliances' defects in property.
 */
- (void) fetchApplianceDefects:(NSDictionary *)requestParameters;
/**
 @brief Fetches appliance defect in property with defect ID.
 */
- (Defect *)fetchApplianceDefectWithDefectID:(NSNumber *)defectID;
/**
 @brief Returns all appliances' defects in property (Fetched from Core Database).
 */
- (NSArray *)fetchAllApplianceDefects:(NSNumber *)journalId;

/**
 @brief Returns all defect categores (Fetched from Core Database).
 */
- (NSArray *)fetchAllDefectCategories;

/**
 @brief saves new defect. responses will be catter in PSSavedewfect Service delegates
 */
- (void) saveApplianceDefect:(NSDictionary *)requestParameters;

/**
 @brief Saves detector's inspection form.
 */
- (void) saveDetectorInspectionForm:(NSDictionary *) inspectionFormDictionary forDetector:(Detector *) detector;

/**
 @brief Returns true if defects exist against a property, no otherwise.
 */
- (BOOL) isDefectAdded:(NSNumber *)journalId;

- (DetectorType *)fetchDetectorTypeWithDetectorTypeId:(NSNumber *)detectorTypeId;

- (PowerType *)fetchPowerTypeWithPowerTypeId:(NSNumber *)powerTypeId;

- (NSArray *) fetchPowerTypes:(NSString*) sortColumn;

- (NSArray *) fetchDetectorTypes:(NSString*) sortColumn;
@end
