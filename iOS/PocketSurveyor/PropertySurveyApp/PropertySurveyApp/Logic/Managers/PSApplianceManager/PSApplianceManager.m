//
//  PSApplianceManager.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSApplianceManager.h"
#import "CoreDataHelper.h"


@implementation PSApplianceManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSApplianceManager *sharedAppliacnceManager = nil;
+ (PSApplianceManager *) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAppliacnceManager = [[PSApplianceManager alloc] init];
        // Do any other initialisation stuff here
        sharedAppliacnceManager.saveApplianceService = [[PSSaveApplianceService alloc] init];
        sharedAppliacnceManager.fetchPropertyAppliancesService = [[PSFetchPropertyAppliancesService alloc] init];
        sharedAppliacnceManager.fetchApplianceDefectsService = [[PSFetchDefectsService alloc] init];
        sharedAppliacnceManager.saveDefectService = [[PSSaveDefectService alloc] init];
    });
    return sharedAppliacnceManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAppliacnceManager = [super allocWithZone:zone];
    });
    return sharedAppliacnceManager;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}


#pragma mark - Methods

- (void) fetchPropertyAppliances:(NSDictionary *)requestParameters forProperty:(NSManagedObjectID *)propertyId
{
    if([UtilityClass isUserLoggedIn])
    {
        //GET Request to download Property Appliances
        NSData *responseData = [self.fetchPropertyAppliancesService fetchPropertyAppliacnes:requestParameters];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        if(!isEmpty(responseString))
        {
            NSDictionary *JSONDictionary = [responseString JSONValue];
            NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
            if(statusCode == kSuccessCode)
            {
                NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
                if(!isEmpty(responseDictionary))
                {
                    NSArray *appliancesArray = [responseDictionary valueForKey:@"appliancesList"];
                    NSArray *detectorsArray = [responseDictionary valueForKey:@"detectorsList"];
                    
                    //CLS_LOG(@"Appliances Count %d---Detectors Count %d",[appliancesArray count],[detectorsArray count]);
                    
                    [[PSDataPersistenceManager sharedManager] saveFetchedApplianceData:appliancesArray forProperty:propertyId isAddedOffline:NO];
                    [[PSDataPersistenceManager sharedManager] saveFetchedDetectorData:detectorsArray forProperty:propertyId];
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification
                                                                                    object:nil];
            }
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesFailureNotification
                                                                                object:nil];
        }
    }
}

- (void) saveAppliance:(NSDictionary *)appliance forProperty:(NSManagedObjectID *)propertyId
{
    if([UtilityClass isUserLoggedIn])
    {
        NSMutableDictionary *applianceDictionary = [NSMutableDictionary dictionary];// [appliance mutableCopy];
        
        [applianceDictionary setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
        [applianceDictionary setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
        [applianceDictionary setObject:appliance forKey:@"ApplianceData"];
        
        //Synchronous Call
        NSData *responseData = [self.saveApplianceService saveAppliance:applianceDictionary];
        
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        if(!isEmpty(responseString))
        {
            NSDictionary *JSONDictionary = [responseString JSONValue];
            NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
            if(statusCode == kSuccessCode)
            {
                NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
                if(!isEmpty(responseDictionary))
                {
                    NSArray *applianceArray = @[responseDictionary];
                    [[PSDataPersistenceManager sharedManager] saveApplianceData:applianceArray forProperty:propertyId isAddedOffline:NO];
                    
                    
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveNewAppliacneFailureNotification
                                                                                    object:nil];
            }
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveNewAppliacneFailureNotification
                                                                                object:nil];
        }
    }
}


- (void) saveAppliancePropertiesOffline:(NSArray *)locationArray typeArray:(NSArray *)typeArray manufecturerArray:(NSArray *)manufecturerArray modelArray:(NSArray *)modelArray
{
    [[PSDataPersistenceManager sharedManager] saveAppliancePropertiesOffline:locationArray typeArray:typeArray manufecturerArray:manufecturerArray modelArray:modelArray];
}

- (void) service:(PSSaveApplianceService *)service didFailWithError:(NSError *)error
{
    CLS_LOG(@"didFailWithError : %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
    
}

/**
 @brief Returns All Appliacnces (Fetched from Core Database)
 */
- (NSArray *) fetchAllAppliances
{
    NSArray *appliances = nil;
    STARTEXCEPTION
    if([UtilityClass isUserLoggedIn])
    {
        NSArray* fetchedAppliances = [CoreDataHelper getObjectsFromEntity:kAppliance
                                                                 predicate:[NSPredicate predicateWithValue:YES]
                                                                   sortKey:nil
                                                             sortAscending:YES
                                                            context:[PSDatabaseContext sharedContext].managedObjectContext];
        if([fetchedAppliances count] > 0)
        {
            appliances = [NSArray arrayWithArray:fetchedAppliances];
        }
    }
    ENDEXCEPTION
    return appliances;
}


- (NSArray *) fetchApplianceLoactions
{
    NSArray *applianceLoactions = nil;
    STARTEXCEPTION
    if([UtilityClass isUserLoggedIn])
    {
        NSArray* fetchedLocations = [CoreDataHelper getObjectsFromEntity:kApplianceLocation
                                                                 predicate:[NSPredicate predicateWithValue:YES]
                                                                   sortKey:nil
                                                             sortAscending:YES
                                                            context:[PSDatabaseContext sharedContext].managedObjectContext];
        if([fetchedLocations count] > 0)
        {
            applianceLoactions = [NSArray arrayWithArray:fetchedLocations];
        }
    }
    ENDEXCEPTION
    return applianceLoactions;
}

- (NSArray *) fetchApplianceManufacturers
{
	NSArray *applianceManufacturers = nil;
	STARTEXCEPTION
	NSArray* fetchedManufacturers = [CoreDataHelper getObjectsFromEntity:kApplianceManufacturer
																														 predicate:[NSPredicate predicateWithValue:YES]
																															 sortKey:nil
																												 sortAscending:YES
																															 context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedManufacturers count] > 0)
	{
		applianceManufacturers = [NSArray arrayWithArray:fetchedManufacturers];
	}
	ENDEXCEPTION
	return applianceManufacturers;
}

- (NSArray *) fetchApplianceModels
{
	NSArray *applianceModels = nil;
	STARTEXCEPTION
	NSArray* fetchedModels = [CoreDataHelper getObjectsFromEntity:kApplianceModel
																											predicate:[NSPredicate predicateWithValue:YES]
																												sortKey:nil
																									sortAscending:YES
																												context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedModels count] > 0)
	{
		applianceModels = [NSArray arrayWithArray:fetchedModels];
	}
	ENDEXCEPTION
	return applianceModels;
}

- (NSArray *) fetchApplianceTypes
{
	NSArray *applianceTypes = nil;
	STARTEXCEPTION
	NSArray* fetchedTypes = [CoreDataHelper getObjectsFromEntity:kApplianceType
																										 predicate:[NSPredicate predicateWithValue:YES]
																											 sortKey:nil
																								 sortAscending:YES
																											 context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedTypes count] > 0)
	{
		applianceTypes = [NSArray arrayWithArray:fetchedTypes];
	}
	ENDEXCEPTION
	return applianceTypes;
}

- (BOOL) isApplianceInspected :(Property *)property
{
    BOOL isInspected = NO;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isInspected == 0"];
    NSSet *unInspectedAppliances = [property.propertyToAppliances filteredSetUsingPredicate:predicate];
    NSSet *unInspectedDetectors = [property.propertyToDetector filteredSetUsingPredicate:predicate];
    if (isEmpty(unInspectedAppliances) && isEmpty(unInspectedDetectors))
    {
        isInspected = YES;
    }
    
    return isInspected;
}

- (NSNumber *) totalNumberOfAppliancesInspected :(Property *)property
{
    NSNumber *totalAppliancesInspected = [NSNumber numberWithInt:0];
    if (!isEmpty(property))
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isInspected == 1"];
        NSSet *inspectedAppliances = [property.propertyToAppliances filteredSetUsingPredicate:predicate];
        
        if (!isEmpty(inspectedAppliances))
        {
            totalAppliancesInspected = [NSNumber numberWithInt:[inspectedAppliances count]];
        }
    }
    return totalAppliancesInspected;
}


- (void) saveInspectionForm:(NSDictionary *) inspectionFormDictionary forAppliance:(Appliance *) appliance
{
    [[PSDataPersistenceManager sharedManager]saveInspectionForm:inspectionFormDictionary forAppliance:appliance];
}

- (void) saveDetectorInspectionForm:(NSDictionary *) inspectionFormDictionary forDetector:(Detector *) detector
{
    [[PSDataPersistenceManager sharedManager]saveDetectorInspectionForm:inspectionFormDictionary forDetector:detector];
}

- (void) saveInstallationPipework:(NSDictionary *) pipeworkDictionary forProperty:(Property *)property
{
    [[PSDataPersistenceManager sharedManager]saveInstallationPipework:pipeworkDictionary forProperty:property];
}

- (void) fetchApplianceDefects:(NSDictionary *)requestParameters
{
    if ([UtilityClass isUserLoggedIn])
    {
        NSData *responseData = [self.fetchApplianceDefectsService fetchApplianceDefects:requestParameters];
        NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        // NSString *filePath = [[NSBundle mainBundle] pathForResource:@"defects" ofType:@"json"];
        // NSString *responseString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        if(!isEmpty(responseString))
        {
            NSDictionary *JSONDictionary = [responseString JSONValue];
            NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
            if(statusCode == kSuccessCode)
            {
                NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
                if(!isEmpty(responseDictionary))
                {
                    NSArray *applianceDefects = [responseDictionary valueForKey:@"defects"];
                    NSArray *detectorDefects = [responseDictionary valueForKey:@"detectorDefects"];
                    NSArray *defectCategories = [responseDictionary valueForKey:@"defectCategories"];
                    NSMutableDictionary *defectsDictionary = [NSMutableDictionary dictionary];
                    [defectsDictionary setObject:detectorDefects forKey:@"detectorDefects"];
                    [defectsDictionary setObject:applianceDefects forKey:@"defects"];
                    [[PSDataPersistenceManager sharedManager] saveFetchedDefectCategories:defectCategories defects:defectsDictionary];
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification
                                                                                    object:nil];
            }
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification
                                                                                object:nil];
        }
    }
}


- (NSArray *)fetchAllApplianceDefects:(NSNumber *)journalId
{
    NSArray* defects = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.journalId =%@", journalId];
    STARTEXCEPTION
	defects = [CoreDataHelper getObjectsFromEntity:kDefect
                                                         predicate:predicate
                                                           sortKey:nil
                                                     sortAscending:YES
                                                    context:[PSDatabaseContext sharedContext].managedObjectContext];
	ENDEXCEPTION
	return defects;

}

- (Defect *)fetchApplianceDefectWithDefectID:(NSNumber *)defectID
{
    NSArray* defects = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.defectID =%@", defectID];
    STARTEXCEPTION
    defects = [CoreDataHelper getObjectsFromEntity:kDefect
                                           predicate:predicate
                                             sortKey:nil
                                       sortAscending:YES
                                      context:[PSDatabaseContext sharedContext].managedObjectContext];
    ENDEXCEPTION
    if(defects.count>0)
    {
        return [defects firstObject];
    }
    return nil;
}

- (BOOL) isDefectAdded:(NSNumber *)journalId
{
    BOOL isDefect = NO;
    NSArray *defects = [self fetchAllApplianceDefects:journalId];
    if ([defects count] > 0)
    {
        isDefect = YES;
    }
    return isDefect;
}

- (NSArray *)fetchAllDefectCategories
{
    NSArray *defectCategories = nil;
    STARTEXCEPTION
	NSArray* fetchedCategoriess = [CoreDataHelper getObjectsFromEntity:kDefectCategory
                                                         predicate:[NSPredicate predicateWithValue:YES]
                                                           sortKey:nil
                                                     sortAscending:YES
                                                    context:[PSDatabaseContext sharedContext].managedObjectContext];
	if([fetchedCategoriess count] > 0)
	{
		defectCategories = [NSArray arrayWithArray:fetchedCategoriess];
	}
	ENDEXCEPTION
	return defectCategories;
}

- (void) saveApplianceDefect:(NSDictionary *)requestParameters
{
    [self.saveDefectService saveApplianceDefect:requestParameters];
}

- (DetectorType *)fetchDetectorTypeWithDetectorTypeId:(NSNumber *)detectorTypeId
{
    NSArray* detectorTypes = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.detectorTypeId =%@", detectorTypeId];
    STARTEXCEPTION
    detectorTypes = [CoreDataHelper getObjectsFromEntity:kDetectorType
                                               predicate:predicate
                                                 sortKey:nil
                                           sortAscending:YES
                                                 context:[PSDatabaseContext sharedContext].managedObjectContext];
    ENDEXCEPTION
    if(detectorTypes.count>0)
    {
        return [detectorTypes firstObject];
    }
    return nil;
}

- (PowerType *)fetchPowerTypeWithPowerTypeId:(NSNumber *)powerTypeId
{
    NSArray* powerTypes = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.powerTypeId =%@", powerTypeId];
    STARTEXCEPTION
    powerTypes = [CoreDataHelper getObjectsFromEntity:kPowerType
                                            predicate:predicate
                                              sortKey:nil
                                        sortAscending:YES
                                              context:[PSDatabaseContext sharedContext].managedObjectContext];
    ENDEXCEPTION
    if(powerTypes.count>0)
    {
        return [powerTypes firstObject];
    }
    return nil;
}

- (NSArray *) fetchDetectorTypes:(NSString*) sortColumn
{
    NSArray *detectorTypes = nil;
    
    STARTEXCEPTION
    NSArray* fetchedTypes = [CoreDataHelper getObjectsFromEntity:kDetectorType
                                                       predicate:[NSPredicate predicateWithValue:YES]
                                                         sortKey:sortColumn
                                                   sortAscending:YES
                                                         context:[PSDatabaseContext sharedContext].managedObjectContext];
    if([fetchedTypes count] > 0)
    {
        detectorTypes = [NSArray arrayWithArray:fetchedTypes];
    }
    ENDEXCEPTION
    
    return detectorTypes;
}

- (NSArray *) fetchPowerTypes:(NSString*) sortColumn
{
    NSArray *powerTypes = nil;
    
    STARTEXCEPTION
    NSArray* fetchedTypes = [CoreDataHelper getObjectsFromEntity:kPowerType
                                                       predicate:[NSPredicate predicateWithValue:YES]
                                                         sortKey:sortColumn
                                                   sortAscending:YES
                                                         context:[PSDatabaseContext sharedContext].managedObjectContext];
    if([fetchedTypes count] > 0)
    {
        powerTypes = [NSArray arrayWithArray:fetchedTypes];
    }
    ENDEXCEPTION
    
    return powerTypes;
}


@end
