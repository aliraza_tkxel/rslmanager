//
//  PSSurveyorsManager.m
//  PropertySurveyApp
//
//  Created by Yawar on 14/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSSurveyorsManager.h"
#import "PSCoreDataManager.h"

@implementation PSSurveyorsManager
@synthesize getSurveyorsService;

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSSurveyorsManager *sharedSurveyorManager = nil;
+(PSSurveyorsManager*) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSurveyorManager = [[PSSurveyorsManager alloc] init];
        // Do any other initialisation stuff here
        sharedSurveyorManager.getSurveyorsService = [[PSGetAllSurveyorsService alloc] init];
        sharedSurveyorManager.getSurveyorsService.surveyorDelegate = sharedSurveyorManager;
    });
    return sharedSurveyorManager;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSurveyorManager = [super allocWithZone:zone];
    });
    return sharedSurveyorManager;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}


#pragma mark - Methods
-(void) fetchAllSurveyors:(NSMutableDictionary *)requestParameters
{
    if([UtilityClass isUserLoggedIn])
    {
        @synchronized(self)
        {
					
            [self.getSurveyorsService getAllSurveyors:requestParameters];
        }
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyorSaveFailureNotification object:nil];
    }
}

-(void) removeAllSurveyors
{
	NSManagedObjectContext * context = [PSDatabaseContext sharedContext].managedObjectContext;
	NSArray * allSurveyors = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([Surveyor class]) sortKey:NULL sortAscending:TRUE context:context];
	for(Surveyor * surveyor in allSurveyors)
	{
		[PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[surveyor objectID]];
	}
	[[PSDatabaseContext sharedContext] saveContext];
}

- (Surveyor *) fetchLoggedInSurveyor
{
    Surveyor *surveyor = [[PSCoreDataManager sharedManager] fetchLoggedInSurveyor];
    return surveyor;
}

#pragma mark - PSGetAllSurveyorsServiceDelegate

- (void) service:(PSGetAllSurveyorsService *)service didFinishLoadingSurveyors:(id)jsonObject
{

    NSDictionary *responseDictionary = [jsonObject JSONValue];
    NSArray *surveyorsArray = [responseDictionary objectForKey:kResponseTag];
    if(!isEmpty(surveyorsArray))
    {
        [[PSDataPersistenceManager sharedManager] saveSurveyors:surveyorsArray];
    }
}

- (void) service:(PSFetchSurveyFormService *)service didFailWithError:(NSError *)error
{
    CLS_LOG(@"didFailWithError : %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyorSaveFailureNotification object:nil];
}


@end
