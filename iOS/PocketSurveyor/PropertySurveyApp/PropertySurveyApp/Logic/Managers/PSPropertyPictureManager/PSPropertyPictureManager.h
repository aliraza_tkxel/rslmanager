//
//  PSPropertyManager.h
//  PropertySurveyApp
//
//  Created by Yawar on 16/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSUploadPhotographService.h"
#import "PSUpdatePhotographService.h"
#import "PSDeletePhotographService.h"
#import "PSPropertyListViewController.h"
#import "PSUploadRepairPictureService.h"

#import "PSUploadDefectPictureService.h"

@interface PSPropertyPictureManager : NSObject <PSUploadPhotographServiceDelegate,PSDeletePhotographServiceDelegate>

@property (nonatomic, strong) PSUploadPhotographService * uploadPhotographService;

@property (nonatomic, strong) PSUploadDefectPictureService * uploadDefectPhotographService;

@property (nonatomic, strong) PSUpdatePhotographService * updatePhotographService;

@property (nonatomic, strong) PSDeletePhotographService * deletePhotographService;

@property (nonatomic, strong) PSUploadRepairPictureService * uploadRepairPhotographService;
@property (nonatomic, readwrite) BOOL isPostingImage;

@property (nonatomic, strong) id<PSPropertyListProtocol> delegate;
+ (PSPropertyPictureManager *) sharedManager;

- (void) uploadPhotograph:(NSDictionary *) requestParameters withImage:(UIImage*) image;
- (void) uploadDefectPhotograph:(NSDictionary *) requestParameters withImage:(UIImage*) image;

- (void) updateDefaultPhotograph:(NSDictionary *) requestParameters withImage:(UIImage*) image;

@end
