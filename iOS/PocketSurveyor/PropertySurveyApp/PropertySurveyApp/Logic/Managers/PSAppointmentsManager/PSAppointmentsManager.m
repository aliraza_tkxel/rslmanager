//
//  PSAppointmentsManager.m
//  PropertySurveyApp
//
//  Created by TkXel on 08/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppointmentsManager.h"
#import "CoreDataHelper.h"
#import "User+Methods.h"

@implementation PSAppointmentsManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSAppointmentsManager * sharedAppointmentManager = nil;

+(PSAppointmentsManager*) sharedManager
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedAppointmentManager = [[PSAppointmentsManager alloc] init];
		// Do any other initialisation stuff here
		sharedAppointmentManager.getAppointmentService = [[PSGetAllAppointmentsService alloc] init];
		sharedAppointmentManager.getAppointmentService.delegate = sharedAppointmentManager;
		
		sharedAppointmentManager.inspectionDocumentService = [[PSPrepareInspectionDocumentService alloc]init];
        sharedAppointmentManager.inspectionDocumentService.delegate = sharedAppointmentManager.inspectionDocumentService;
		sharedAppointmentManager.inspectionDocumentService.prepareDocumentDelegate = sharedAppointmentManager;
		
		sharedAppointmentManager.deleteAppointmentService = [[PSDeleteAppointmentService alloc] init];
		sharedAppointmentManager.deleteAppointmentService.delegate = sharedAppointmentManager.deleteAppointmentService;
		sharedAppointmentManager.deleteAppointmentService.deleteAppointmentDelegate = sharedAppointmentManager;
		
		sharedAppointmentManager.fetchSurveyFormService = [[PSFetchSurveyFormService alloc] init];
		sharedAppointmentManager.fetchSurveyFormService.surveyDelegate = sharedAppointmentManager;
		
		sharedAppointmentManager.createAppointmentService = [[PSCreateAppointmentService alloc] init];
		//sharedAppointmentManager.createAppointmentService.createAppointmentDelegate = sharedAppointmentManager;
		
		sharedAppointmentManager.completeAppointmentService = [[PSCompleteAppointmentService alloc] init];
		sharedAppointmentManager.completeAppointmentService.completeAppointmentDelegate = sharedAppointmentManager;
		
	});
	return sharedAppointmentManager;
}

+(id)allocWithZone:(NSZone *)zone
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedAppointmentManager = [super allocWithZone:zone];
	});
	return sharedAppointmentManager;
}

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}


#pragma mark - Methods

- (void) createAppointment:(NSDictionary *)appointment
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		if([UtilityClass isUserLoggedIn])
		{
			NSMutableDictionary *appointmentDictionary = [appointment mutableCopy];
			[appointmentDictionary setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
			[appointmentDictionary setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
            [appointmentDictionary setObject:@0 forKey:@"syncStatus"];
			
			NSData *responseData = [self.createAppointmentService createNewAppointment:appointmentDictionary];
			NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
			if(!isEmpty(responseString))
			{
				NSMutableDictionary *JSONDictionary = [responseString JSONValue];
                
                
				NSInteger statusCode = [[[JSONDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
				NSInteger errorCode = [[JSONDictionary objectForKey:kErrorCodeTag] integerValue];
				if(statusCode == kSuccessCode)
				{
					NSArray *responseArray = [JSONDictionary objectForKey:kResponseTag];
					if (!isEmpty(responseArray))
					{
						
						NSMutableDictionary *responseDictionary = [responseArray objectAtIndex:0];
                        //Testing sync status value
                        [responseDictionary setValue:@0 forKey:@"syncStatus"];
						if(!isEmpty(responseDictionary))
						{
							/* [responseDictionary setObject:[appointmentDictionary valueForKey:kAppointmentEventIdentifier] forKey:kAppointmentEventIdentifier];*/
							[[PSDataPersistenceManager sharedManager] createNewAppointment:responseDictionary];
						}
					}
					else
					{
						[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentFailureNotification
																																								object:nil];
					}
				}
				else if (errorCode == kAppointmentTimeUnavailableCode)
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentTimeNotAvailableNotification
																																							object:nil];
				}
				else if (errorCode == kAppointmentTimeUnavailableBankHolidayCode)
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentTimeNotAvailableBankHolidayNotification
																																							object:nil];
				}
				else
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentFailureNotification
																																							object:nil];
				}
			}
			else
			{
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentFailureNotification
																																						object:nil];
			}
		}
	});
}

- (void) deleteAppointment:(Appointment *)appointment
{
	if([UtilityClass isUserLoggedIn])
	{
		NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
		[requestParameters setObject:appointment.appointmentId forKey:@"appointmentid"];
		[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
		[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
		[self.deleteAppointmentService deleteAppointment:requestParameters];
	}
}

- (void) fetchAllAppointsments
{
	if([UtilityClass isUserLoggedIn] == TRUE)
	{
		@synchronized(self)
		{
			NSDate * startDate = [NSDate date];
			startDate = [startDate dateBySubtractingDays:kAppointmentDaysLimit];
			NSDate * endDate = [NSDate date];
			endDate = [endDate dateByAddingDays:kAppointmentDaysLimit];
			
			NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
			[requestParameters setValue:[UtilityClass stringFromDate:startDate dateFormat:kDateTimeStyle16] forKey:@"startdate"];
			[requestParameters setValue:[UtilityClass stringFromDate:endDate dateFormat:kDateTimeStyle16] forKey:@"enddate"];
			[requestParameters setValue:[NSNumber numberWithInt:1] forKey:@"includeoverdue"];
			[requestParameters setValue:[NSNumber numberWithInt:1] forKey:@"fetchall"];
			[requestParameters setObject:[[SettingsClass sharedObject] userInfo] forKey:@"userinfo"];
			
			NSManagedObjectContext * context = [PSDatabaseContext sharedContext].managedObjectContext;
            
			NSArray * appointments = [CoreDataHelper getObjectsFromEntity:NSStringFromClass([Appointment class])
                                                                  sortKey:NULL
                                                            sortAscending:FALSE
                                                                  context:context];
            
			NSArray * appointmentIds = [appointments valueForKeyPath:@"appointmentId"];
			[requestParameters setObject:appointmentIds forKey:@"appointmentIds"];
			
			[self.getAppointmentService getAllAppointments:requestParameters];
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) fetchAllAppointsmentsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
	if([UtilityClass isUserLoggedIn] && [[SettingsClass sharedObject].loggedInUser.userToAppointments count] <= 0)
	{
		@synchronized(self)
		{
			NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
			[requestParameters setValue:[UtilityClass stringFromDate:startDate dateFormat:kDateTimeStyle5] forKey:@"startdate"];
			[requestParameters setValue:[UtilityClass stringFromDate:endDate dateFormat:kDateTimeStyle5] forKey:@"enddate"];
			[requestParameters setValue:[NSNumber numberWithInt:1] forKey:@"includeoverdue"];
			[requestParameters setValue:[NSNumber numberWithInt:0] forKey:@"fetchall"];
			[requestParameters setObject:[[SettingsClass sharedObject] userInfo] forKey:@"userinfo"];
			[self.getAppointmentService getAllAppointments:requestParameters];
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) fetchAllInprogressAppointsmentsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate
{
	if([UtilityClass isUserLoggedIn] )
	{
		@synchronized(self)
		{
			NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
			[requestParameters setValue:[UtilityClass stringFromDate:startDate dateFormat:kDateTimeStyle5] forKey:@"startdate"];
			[requestParameters setValue:[UtilityClass stringFromDate:endDate dateFormat:kDateTimeStyle5] forKey:@"enddate"];
			[requestParameters setValue:[NSNumber numberWithInt:1] forKey:@"includeoverdue"];
			[requestParameters setValue:[NSNumber numberWithInt:0] forKey:@"fetchall"];
			
			[requestParameters setObject:[[SettingsClass sharedObject] userInfo] forKey:@"userinfo"];
			[self.getAppointmentService getAllAppointments:requestParameters];
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) refreshAllAppointsments
{
	if([UtilityClass isUserLoggedIn])
	{
		[self fetchAllAppointsments];
	}
}

- (void) removeCompleteAppointsments:(NSArray *)appointments
{
	if([UtilityClass isUserLoggedIn])
	{
		[[SettingsClass sharedObject].loggedInUser removeCompleteAppointments:appointments];
		[[PSDatabaseContext sharedContext] saveContext];
	}
}

-(void) createInspectionDocumentForAppointments:(NSArray *) appointments{
    [self prepareStockInspectionDocument:appointments];
}

-(void)prepareStockInspectionDocument:(NSArray *)completeAppointments
{
	NSMutableDictionary * serviceParams = [NSMutableDictionary dictionary];
	NSMutableArray * appointmentList = [[NSMutableArray alloc]init];
	for (Appointment * appointment in completeAppointments)
	{
		NSMutableDictionary * param = [NSMutableDictionary dictionary];
		[param setValue:appointment.appointmentId forKey:@"appointmentId"];
		[param setValue:appointment.appointmentToProperty.propertyId forKey:@"propertyId"];
		[appointmentList addObject:param];
	}
	[serviceParams setObject:appointmentList forKey:@"appointmentList"];
	[serviceParams setObject:[SettingsClass sharedObject].loggedInUser.userId forKey:@"userId"];
	
	CLS_LOG(@"\n Prepare Stock Inspection Document: \n%@",[serviceParams JSONFragment]);
	[self.inspectionDocumentService prepareStockInspectionDocument:serviceParams];
}

- (NSArray *) modifiedAppointments
{
	if ([UtilityClass isUserLoggedIn])
	{
		return [[SettingsClass sharedObject].loggedInUser modifiedAppointments];
	}
	return nil;
}

- (NSArray *) completedAppointments
{
	if ([UtilityClass isUserLoggedIn])
	{
		return [[SettingsClass sharedObject].loggedInUser completedAppointments];
	}
	return nil;
}

- (void) fetchSurveyFormForAppointment:(Appointment *)appointment
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appointment.appointmentToProperty))
	{
		@synchronized(self)
		{
			NSMutableDictionary* requestParameters = [NSMutableDictionary dictionary];
			[requestParameters setObject:appointment.appointmentId forKey:@"appointmentid"];
			[requestParameters setObject:appointment.appointmentToProperty.propertyId forKey:kPropertyId];
			[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
			[requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
			[self.fetchSurveyFormService fetchStockSurveyForm:requestParameters];
		}
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) completeAppointment:(NSMutableDictionary *) requestParameters
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		if([UtilityClass isUserLoggedIn])
		{
			NSData *responseData = [self.completeAppointmentService performSyncAppointmentUpdate:requestParameters];
			NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
			CLS_LOG(@"Mark Complete Appointment Response %@",responseString);
			if(!isEmpty(responseString))
			{
				NSDictionary *JSONDictionary = [responseString JSONValue];
				NSDictionary *statusDictionary = [JSONDictionary objectForKey:kStatusTag];
				NSInteger statusCode = [[statusDictionary valueForKey:kCodeTag] intValue];
				if(statusCode == kSuccessCode)
				{
					NSDictionary *responseDictionary = [JSONDictionary objectForKey:kResponseTag];
					// for (NSDictionary *responseDictionary in responseArray) {
					NSDictionary *appointment = [responseDictionary valueForKey:@"Appointment"];
					NSNumber *appointmentId = [appointment valueForKey:@"appointmentId"];
					if(!isEmpty(appointmentId))
					{
						Appointment *appointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:appointmentId context:nil];
						if(appointment)
						{
							appointment.isModified = [NSNumber numberWithBool:NO];
							appointment.isSurveyChanged = [NSNumber numberWithBool:NO];
							[self deleteCalendarEventForAppointment:appointment];
							AppointmentStatus completionStatus = [UtilityClass completionStatusForAppointmentType:[appointment getType]];
							[[PSDataUpdateManager sharedManager]
							 updateAppointmentStatus:completionStatus appointment:appointment];
						}
					}
					//}
				}
				else
				{
					[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCompleteAppointmentFailureNotification object:nil];
				}
			}
			
		}
	});
}

- (void) updateAppointments:(NSMutableDictionary *) requestParameters
{
	if([UtilityClass isUserLoggedIn])
	{
		[self.completeAppointmentService updateAppointments:requestParameters];
	}
}
#pragma mark - CoreRESTServiceDelegate
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject
{
	if(service == self.getAppointmentService)
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveFailureNotification object:nil];
	}
	else if(service == self.fetchSurveyFormService)
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyFormSaveFailureNotification object:nil];
	}
	
	else if (service == self.completeAppointmentService)
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCompleteAppointmentFailureNotification object:nil];
	}
	return NO;
}

- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject
{
	
	NSDictionary *responseDictionary = [[jsonObject JSONValue] objectForKey:kResponseTag];
	//NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
	NSArray *appointmentsArray = [responseDictionary objectForKey:kAppointmentListTag];
    NSDictionary *commonData = [responseDictionary objectForKey:kCommonDataTag];
    PSDataPersistenceManager * persistenceMgr = [PSDataPersistenceManager sharedManager];
    [persistenceMgr saveCommonData:commonData];
	if(!isEmpty(appointmentsArray))
	{
		//CLS_LOG(@"UserInfo : %@", appointmentsArray);
		[[PSDataPersistenceManager sharedManager] saveAppointments:appointmentsArray];
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsNoResultFoundNotification object:nil];
	}
}

#pragma mark - PSCreateInspectionDocument Delegate

-(void) service:(PSPrepareInspectionDocumentService *)service didReceiveInspectionDocumentResponse:(id)inspectionResponse{
    NSDictionary *response = [inspectionResponse JSONValue];
    NSDictionary *statusDictionary = [response objectForKey:kStatusTag];
    
    if([[statusDictionary valueForKey:kCodeTag] intValue] == kSuccessCode)
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentInspectionDocumentSuccessNotification object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentInspectionDocumentFailureNotification object:nil];
    }
}

-(void) service:(PSPrepareInspectionDocumentService *)service didFailInspectionDocumentWithError:(NSError *)error{
    CLS_LOG(@"didFailWithError : %@", error);
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentInspectionDocumentFailureNotification object:nil];
}

#pragma mark - PSDeleteAppointmentServiceDelegate
- (void) service:(PSDeleteAppointmentService *)service didReceiveDeleteAppointmentResponse:(id)jsonObject
{
	NSDictionary *response = [jsonObject JSONValue];
	NSDictionary *statusDictionary = [response objectForKey:kStatusTag];
	
	if([[statusDictionary valueForKey:kCodeTag] intValue] == kSuccessCode)
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsDataRemoveSuccessNotification object:nil];
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsDataRemoveFailureNotification object:nil];
	}
}

#pragma mark - PSFetchSurveyFormServiceDelegate
- (void) service:(PSFetchSurveyFormService *)service didReceivePercentData:(NSInteger)percentage
{
	CLS_LOG(@"didReceivePercentData : %ld", (long)percentage);
}

- (void) service:(PSFetchSurveyFormService *)service didFinishDownloadingSurveyForm:(id)surveyData
{
	CLS_LOG(@"didFinishDownloadingSurveyForm : %@", surveyData);
	NSDictionary * responseDictionary = [surveyData JSONValue];
	NSDictionary * surveyDataDictionary = [responseDictionary objectForKey:kResponseTag];
	[[PSDataPersistenceManager sharedManager] saveAppointmentSurveyData:surveyDataDictionary];
}

- (void) service:(PSFetchSurveyFormService *)service didFailWithError:(NSError *)error
{
	CLS_LOG(@"didFailWithError : %@", error);
}

#pragma mark - PSCompleteAppointmentServiceDelegate
- (void) service:(PSCompleteAppointmentService *)service didReceiveCompleteAppointmentResponse:(id)completeAppointmentData
{
	[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateAppointmentsSuccessNotification object:completeAppointmentData];
}

- (void) service:(PSCompleteAppointmentService *)service didFailCompleteAppointmentWithError:(NSError *)error
{
	[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateAppointmentsFailureNotification object:nil];
}

#pragma mark - Methods
- (void) deleteCalendarEventForAppointment:(Appointment *)appointment
{
	NSString *appointmentTitle = [NSString stringWithFormat:@"%@: %@ (%@)",appointment.appointmentType, appointment.appointmentTitle, appointment.appointmentId];
	NSString *existingEventIdentifier = [[PSEventsManager sharedManager] eventIdentifierForExistingEvent:appointmentTitle WithStartDate:appointment.appointmentStartTime AndEndDate:appointment.appointmentEndTime];
	if (existingEventIdentifier != nil)
	{
		CLS_LOG(@"Delete Calendar Event Identifier %@\n%@",appointmentTitle,existingEventIdentifier);
		[[PSEventsManager sharedManager] removeEventWithIdentifier:existingEventIdentifier completion:^(bool success, NSError *error) {
		}];
	}
}

- (BOOL) isFaultAppointmentComplete:(Appointment *) appointment
{
	BOOL isAppointmentComplete = NO;
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.jobStatus LIKE[cd] %@",kJobStatusComplete];
	NSSet *completeAppointments = [appointment.appointmentToJobDataList filteredSetUsingPredicate:predicate];
	
	if ([completeAppointments count] == [appointment.appointmentToJobDataList count])
	{
		isAppointmentComplete = YES;
	}
	return isAppointmentComplete;
}
@end
