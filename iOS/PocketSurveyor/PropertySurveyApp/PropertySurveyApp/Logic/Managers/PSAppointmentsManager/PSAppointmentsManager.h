//
//  PSAppointmentsManager.h
//  PropertySurveyApp
//
//  Created by TkXel on 08/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PSGetAllAppointmentsService.h"
#import "PSFetchSurveyFormService.h"
#import "PSDeleteAppointmentService.h"
#import "PSCreateAppointmentService.h"
#import "PSCompleteAppointmentService.h"
#import "PSPrepareInspectionDocumentService.h"

@interface PSAppointmentsManager : NSObject <CoreRESTServiceDelegate, PSFetchSurveyFormServiceDelegate, PSDeleteAppointmentServiceDelegate, PSCompleteAppointmentServiceDelegate,PSPrepareInspectionDocumentServiceDelegate>

@property (nonatomic, strong) PSGetAllAppointmentsService        * getAppointmentService;
@property (nonatomic, strong) PSPrepareInspectionDocumentService * inspectionDocumentService;
@property (nonatomic, strong) PSFetchSurveyFormService           * fetchSurveyFormService;
@property (nonatomic, retain) PSDeleteAppointmentService         * deleteAppointmentService;
@property (nonatomic, retain) PSCreateAppointmentService         * createAppointmentService;
@property (nonatomic, retain) PSCompleteAppointmentService       * completeAppointmentService;

+ (PSAppointmentsManager*) sharedManager;
- (void) fetchAllAppointsments;
- (void) fetchAllAppointsmentsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate;

- (void) refreshAllAppointsments;

- (void) removeCompleteAppointsments:(NSArray *)appointments;

- (NSArray *) modifiedAppointments;
- (NSArray *) completedAppointments;

- (void) fetchSurveyFormForAppointment:(Appointment *)appointment;

- (void) deleteAppointment:(Appointment *)appointment;
- (void) createAppointment:(NSDictionary *)appointmentJSON;
- (void) completeAppointment:(NSMutableDictionary *) requestParameters; //For mark complete appointment only
- (void) updateAppointments:(NSMutableDictionary *) requestParameters; //For bulk appointment update

- (void) fetchAllInprogressAppointsmentsFromDate:(NSDate *)startDate toDate:(NSDate *)endDate;
-(void) createInspectionDocumentForAppointments:(NSArray *) appointments;

/**
 @brief checks if a fault appointment has completed or not.
 */
- (BOOL) isFaultAppointmentComplete:(Appointment *) appointment;

/**
 @brief Deletes calendar event for completed appointment.
 */
- (void) deleteCalendarEventForAppointment:(Appointment *)appointment;
@end