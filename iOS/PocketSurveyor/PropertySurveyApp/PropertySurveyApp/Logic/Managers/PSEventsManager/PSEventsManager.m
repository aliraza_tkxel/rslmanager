//
//  PSEventsManager.m
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 01/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSEventsManager.h"

@implementation PSEventsManager

#define kDefaultCalendar @"Calendar"

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSEventsManager * _sharedManager = nil;
+ (PSEventsManager *)sharedManager {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
	    _sharedManager = [[PSEventsManager alloc] init];
	    // Do any other initialisation stuff here
	});
	return _sharedManager;
}

+ (id)allocWithZone:(NSZone *)zone {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
	    _sharedManager = [super allocWithZone:zone];
	});
	return _sharedManager;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

#pragma mark -
- (void) requestCalendarPermissions
{
	__block EKEventStore *store = [[EKEventStore alloc] init];
	[store requestAccessToEntityType:EKEntityTypeEvent completion: ^(BOOL granted, NSError *error)
    {
        //Do Something
	}];
    [store requestAccessToEntityType:EKEntityTypeReminder
                                completion:^(BOOL granted, NSError *error) {
                                    if (!granted) {
                                        CLS_LOG(@"Access to store not granted");
                                    }
                                }];
}

- (void)createNewEventWithDictionary:(NSDictionary *)eventDictionary completion:(void (^)(NSString *eventIdentifer, NSError *error))block {
	[self createNewEventWithDictionary:eventDictionary inCalendar:kDefaultCalendar completion:block];
}

- (void)createNewEventWithDictionary:(NSDictionary *)eventDictionary inCalendar:(NSString *)calendarName completion:(void (^)(NSString *eventIdentifer, NSError *error))block {
	__block EKEventStore *store = [[EKEventStore alloc] init];
    
	[store requestAccessToEntityType:EKEntityTypeEvent completion: ^(BOOL granted, NSError *error) {
	    if (granted) {
	        EKEvent *event = [EKEvent eventWithEventStore:store];
            
	        event.title = [eventDictionary objectForKey:kAppointmentTitle];
	        event.startDate = [eventDictionary objectForKey:kAppointmentStartTime];
	        event.endDate = [eventDictionary objectForKey:kAppointmentEndTime];
	        event.notes = [eventDictionary objectForKey:kAppointmentNotes];
	        event.location = [eventDictionary objectForKey:kAppointmentLocation];
            
	        NSString *availability = [eventDictionary objectForKey:kSurveyourAvailability];
            
	        if ([availability isEqualToString:LOC(@"KEY_STRING_AVAILABLE")]) {
	            event.availability = EKEventAvailabilityFree;
			}
	        else if ([availability isEqualToString:LOC(@"KEY_STRING_FREE")]) {
	            event.availability = EKEventAvailabilityFree;
			}
	        else if ([availability isEqualToString:LOC(@"KEY_STRING_TENTATIVE")]) {
	            event.availability = EKEventAvailabilityTentative;
			}
	        else if ([availability isEqualToString:LOC(@"KEY_STRING_BUSY")]) {
	            event.availability = EKEventAvailabilityBusy;
			}
	        else if ([availability isEqualToString:LOC(@"KEY_STRING_OUT_OF_OFFICE")]) {
	            event.availability = EKEventAvailabilityUnavailable;
			}
            
            
	        NSString *alert = [eventDictionary objectForKey:kSurveyorAlert];
	        if ([alert isEqualToString:LOC(@"KEY_STRING_NONE")]) {
			}
	        else if ([alert isEqualToString:LOC(@"KEY_STRING_FIVE_MINUTES_BEFORE")]) {
	            [event addAlarm:[EKAlarm alarmWithRelativeOffset:-300]];
			}
	        else if ([alert isEqualToString:LOC(@"KEY_STRING_FIFTEEN_MINUTES_BEFORE")]) {
	            [event addAlarm:[EKAlarm alarmWithRelativeOffset:-900]];
			}
	        else if ([alert isEqualToString:LOC(@"KEY_STRING_ONE_HOUR_BEFORE")]) {
	            [event addAlarm:[EKAlarm alarmWithRelativeOffset:-3600]];
			}
	        else if ([alert isEqualToString:LOC(@"KEY_STRING_TWO_HOURS_BEFORE")]) {
	            [event addAlarm:[EKAlarm alarmWithRelativeOffset:-7200]];
			}
	        else if ([alert isEqualToString:LOC(@"KEY_STRING_ONE_DAY_BEFORE")]) {
	            [event addAlarm:[EKAlarm alarmWithRelativeOffset:-86400]];
			}
	        else if ([alert isEqualToString:LOC(@"KEY_STRING_TWO_DAYS_BEFORE")]) {
	            [event addAlarm:[EKAlarm alarmWithRelativeOffset:-172800]];
			}
	        else if ([alert isEqualToString:LOC(@"KEY_STRING_ON_DATE_OF_APPOINTMENT")]) {
	            [event addAlarm:[EKAlarm alarmWithRelativeOffset:0]];
			}
            
	        [event setCalendar:[self calendarForString:calendarName forEventStore:store] ? :[store defaultCalendarForNewEvents]];
            
	        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&error];
            
	        if (error) {
	            CLS_LOG(@"Event Creation Error: %@", error);
			}
            
	        if (block) {
	            block(event.eventIdentifier, error);
			}
		}
	    else {
	        if (block) {
	            block(nil, error);
			}
		}
	}];
}

- (void)removeEventWithIdentifier:(NSString *)eventIdentifier completion:(void (^)(bool success, NSError *error))block {
	__block EKEventStore *store = [[EKEventStore alloc] init];
    
	[store requestAccessToEntityType:EKEntityTypeEvent completion: ^(BOOL granted, NSError *error) {
	    if (granted) {
	        EKEvent *event = [store eventWithIdentifier:eventIdentifier];
            if(isEmpty(event)) {
                CLS_LOG(@"Event is nil");
            }
            else {
                CLS_LOG(@"removeEventWithIdentifier %@\n%@",event.title,event.eventIdentifier);
	            [store removeEvent:event span:EKSpanThisEvent error:&error];

            }

	        if (error) {
	            CLS_LOG(@"Event Deletion Error: %@", error);
			}
            
	        if (block) {
	            block(error == nil, error);
			}
		}
	    else {
	        if (block) {
	            block(NO, error);
			}
		}
	}];
}

- (NSArray *)listCalendars {
	EKEventStore *eventDB = [[EKEventStore alloc] init];
	NSArray *calendars = [eventDB calendarsForEntityType:EKEntityTypeEvent];
	NSMutableArray *allCalendars = [NSMutableArray array];
	NSString *typeString = @"";
    
	for (EKCalendar *thisCalendar in calendars) {
		if (thisCalendar.allowsContentModifications) {
			[allCalendars addObject:thisCalendar.title];
		}
	}
	return allCalendars;
}

- (NSString *) eventIdentifierForExistingEvent:(NSString*)titleToLookUp WithStartDate:(NSDate*)startDate AndEndDate:(NSDate*)endDate
{
    NSString *eventIdentifier = nil;
    if (!isEmpty(startDate) && !isEmpty(endDate))
    {
        EKEventStore *eventStore = [[EKEventStore alloc] init];
        NSPredicate *predicate =
        [eventStore predicateForEventsWithStartDate:startDate
                                            endDate:endDate
                                          calendars:nil];
        NSArray *eventList = [eventStore eventsMatchingPredicate:predicate];
        for (EKEvent *event in eventList)
        {
            if (!isEmpty(event.title))
            {
                if([event.title isEqualToString:titleToLookUp])
                {
                    eventIdentifier  = event.eventIdentifier;
                }
            }
        }
    }
    return eventIdentifier;
}

#pragma mark - Private Method
- (EKCalendar *)calendarForString:(NSString *)calendarName forEventStore:(EKEventStore *)eventStore {
	NSArray *calendars = [eventStore calendarsForEntityType:EKEntityTypeEvent];
    
	for (EKCalendar *thisCalendar in calendars) {
		if ([thisCalendar.title isEqualToString:calendarName]) {
			return thisCalendar;
		}
	}
	return nil;
}
@end
