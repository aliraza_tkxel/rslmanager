//
//  PSEventsManager.h
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 01/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSEventsManager : NSObject
+ (PSEventsManager *) sharedManager;

#pragma mark -
- (void) requestCalendarPermissions;
- (void) createNewEventWithDictionary:(NSDictionary *)eventDictionary completion:(void (^)(NSString *eventIdentifer, NSError *error))block;
- (void) createNewEventWithDictionary:(NSDictionary *)eventDictionary inCalendar:(NSString *)calendarName
                           completion:(void (^)(NSString *eventIdentifer, NSError *error))block;

- (void) removeEventWithIdentifier:(NSString *)eventIdentifier completion:(void (^)(bool success, NSError *error))block;
- (NSArray *)listCalendars;
- (NSString *) eventIdentifierForExistingEvent:(NSString*)titleToLookUp WithStartDate:(NSDate*)startDate AndEndDate:(NSDate*)endDate;
@end
