//
//  PSDefectManager.h
//  PropertySurveyApp
//
//  Created by Yawar on 08/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDefectManager : NSObject
+(PSDefectManager *) sharedManager;

/**
 @brief Returns All Defects (Fetched from Core Database)
 */
- (NSArray *) fetchAllDefects;
@end
