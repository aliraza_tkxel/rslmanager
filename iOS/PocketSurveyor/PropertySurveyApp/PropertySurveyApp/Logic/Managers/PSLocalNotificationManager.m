//
//  PSLocalNotificationManager.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 11/03/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "PSLocalNotificationManager.h"

#define kInprogressLocalNotificationFiringTime 12*60*60  //Time is in Secords e.g 12*60*60 for 12 hours
@interface PSLocalNotificationManager() //Private Methods

-(NSString*) scheduleNotificationWithText:(NSString*)textStr;
-(void) cancelNotificationWithUdid:(NSString*)udidString;
-(void)removeNotificationUIDsThatAreFiredFromUserDefaults;
@end

static NSString * kMessageRecordDictionaryName = @"InProgressSchedulingRecord";
static PSLocalNotificationManager * _sharedManager_=nil;
static NSString * kLocalNotificationIDKeyName = @"uid";


@implementation PSLocalNotificationManager

+(id)sharedManager
{
    if(!_sharedManager_)
    {
        _sharedManager_ = [[PSLocalNotificationManager alloc] init];
    }
    return _sharedManager_;
}

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary * recordDict = [defaults objectForKey:kMessageRecordDictionaryName];
        if(recordDict)
        {
            [self removeNotificationUIDsThatAreFiredFromUserDefaults];
        }
        else  //First time initialization of Application
        {
            recordDict = [[NSMutableDictionary alloc] init];
            [defaults setObject:recordDict forKey:kMessageRecordDictionaryName];
            [defaults synchronize];
        }
    }
    return  self;
}

-(NSString*) scheduleNotificationWithText:(NSString*)textStr
{
    UILocalNotification* n1 = [[UILocalNotification alloc] init];
    
//Setting Bundle Usage and Actual Code Setting for Live and other Versions where no bundle used
#if TEST
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int val = (int)[defaults integerForKey:@"firetime_preference"];
    n1.fireDate = [NSDate dateWithTimeIntervalSinceNow:val*60];
#else
    n1.fireDate = [NSDate dateWithTimeIntervalSinceNow:kInprogressLocalNotificationFiringTime];
#endif
    n1.alertBody = textStr;
    n1.applicationIconBadgeNumber = [[UIApplication sharedApplication] scheduledLocalNotifications].count+1;
    NSString * notificationIdentifier = [NSString stringWithFormat:@"%f",[NSDate timeIntervalSinceReferenceDate]];
    //Populating User Info
    NSDictionary * infoDict = [[NSDictionary alloc] initWithObjectsAndKeys:notificationIdentifier,kLocalNotificationIDKeyName, nil];
    n1.userInfo = infoDict;
    [[UIApplication sharedApplication] scheduleLocalNotification: n1];
    return notificationIdentifier;
}

-(void) cancelNotificationWithUdid:(NSString*)udidString
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (UILocalNotification* oneEvent in eventArray)
    {
        NSString *uid= [oneEvent.userInfo objectForKey:kLocalNotificationIDKeyName];
        if ([uid isEqualToString:udidString])
        {
            [app cancelLocalNotification:oneEvent];
            break;
        }
    }
}

-(void)removeNotificationUIDsThatAreFiredFromUserDefaults
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary * scheduleRecordDictionay = [defaults objectForKey:kMessageRecordDictionaryName];
    UIApplication * app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    NSArray * schedulingDictKeys = [scheduleRecordDictionay allKeys];
    NSString * notificationID = nil;
    NSMutableDictionary * scheduledDictionary = [[NSMutableDictionary alloc] init];
    for(NSString * uidKey in schedulingDictKeys)
    {
        notificationID = [scheduleRecordDictionay objectForKey:uidKey];
        for (UILocalNotification * oneEvent in eventArray)
        {
            NSString *uid= [oneEvent.userInfo objectForKey:kLocalNotificationIDKeyName];
            if ([uid isEqualToString:notificationID])
            {
                [scheduledDictionary setObject:notificationID forKey:uidKey];
                break;
            }
        }
    }
    [defaults setObject:scheduledDictionary forKey:kMessageRecordDictionaryName];
    [defaults synchronize];
}

-(void)scheduleInProgressFor12HourNotificationForAppointmentID:(NSNumber*)appointmentID
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * scheduleRecordDictionay = [defaults objectForKey:kMessageRecordDictionaryName];
    NSString * notificationUID = [scheduleRecordDictionay objectForKey:[appointmentID stringValue]];
    if(notificationUID)
    {
        [self cancelNotificationWithUdid:notificationUID];
    }
    notificationUID =  [self scheduleNotificationWithText:kLocalNotificationStringInProgress];
    NSMutableDictionary * updatedScheduledDictionary = [NSMutableDictionary dictionaryWithDictionary:scheduleRecordDictionay];
    [updatedScheduledDictionary setObject:notificationUID forKey:[appointmentID stringValue]];
    [defaults setObject:updatedScheduledDictionary forKey:kMessageRecordDictionaryName];
    [defaults synchronize];
}

-(void)cancelInProgressFor12HourNotificationForAppointmentID:(NSNumber*)appointmentID
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary * scheduleRecordDictionay = [defaults objectForKey:kMessageRecordDictionaryName];
    NSString * notificationUID = [scheduleRecordDictionay objectForKey:[appointmentID stringValue]];
    if(notificationUID)
    {
        [self cancelNotificationWithUdid:notificationUID];
    }
    NSMutableDictionary * updatedScheduledDictionary = [NSMutableDictionary dictionaryWithDictionary:scheduleRecordDictionay];
    [updatedScheduledDictionary removeObjectForKey:[appointmentID stringValue]];
    [defaults setObject:updatedScheduledDictionary forKey:kMessageRecordDictionaryName];
    [defaults synchronize];
}


@end
