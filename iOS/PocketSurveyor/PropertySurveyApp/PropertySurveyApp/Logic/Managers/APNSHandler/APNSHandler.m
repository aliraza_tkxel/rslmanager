//
//  APNSHandler.m
//  PropertySurveyApp
//
//  Created by TkXel on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "APNSHandler.h"

@implementation APNSHandler

#pragma mark -
#pragma mark ARC Singleton Implementation
static APNSHandler *sharedAPNSHandler = nil;
+ (APNSHandler *) sharedHandler
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPNSHandler = [[APNSHandler alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedAPNSHandler;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedAPNSHandler = [super allocWithZone:zone];
    });
    return sharedAPNSHandler;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

#pragma mark -
#pragma mark Notification Methods
- (void) handlePushNotificationWithPayload:(NSDictionary*)payload notificationMode:(APNSMode)mode {
    if (mode == APNSModeLaunch) {
        [self performSelectorOnMainThread:@selector(performActionOnNotificationWithPayload:)
                               withObject:payload
                            waitUntilDone:NO];
    } else if (mode == APNSModeRemote) {
        [self performSelectorOnMainThread:@selector(promptForNotificationWithPayload:)
                               withObject:payload
                            waitUntilDone:NO];
    }
}

- (void) performActionOnNotificationWithPayload:(NSDictionary*)payload {

    CLS_LOG(@"payload %@",payload);
	NSDictionary* elements = [payload objectForKey:kAPS];
    //CLS_LOG(@"payload %@",elements);

    if(!isEmpty([elements objectForKey:kAPSAlert]))
    {
        NSString * str =  [UtilityClass getStringWithNewLineCharacters:[elements objectForKey:kAPSAlert]];
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
        [alert show];
    }
}

- (void) promptForNotificationWithPayload:(NSDictionary*)payload
{
    CLS_LOG(@"payload %@",payload);
	NSDictionary* elements = [payload objectForKey:kAPS];
    //CLS_LOG(@"payload %@",elements);
    
    if(!isEmpty([elements objectForKey:kAPSAlert]))
    {
        NSString * str =  [UtilityClass getStringWithNewLineCharacters:[elements objectForKey:kAPSAlert]];
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:str delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
        [alert show];
    }
}
@end
