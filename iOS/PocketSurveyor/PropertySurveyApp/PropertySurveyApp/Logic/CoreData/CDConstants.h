//
//  CDConstants.h
//  VLogic
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

/*!
 @discussion
 Unique identifier that will represent an image saving.
 */
#define kUniqueImageIdentifier @"imageIdentifier"

#pragma mark - Core Data Store Constants

/*!
 @discussion
 XCDataModel File Name.
 */
#define kXCDataModelFile @"PSAOfflineStock"

/*!
 @discussion
 XCDataModel type. 
 momd describes as versioned Model.
 */
#define kXCDataModelType @"momd"

/*!
 @discussion
 Persistence Data Store Type.
 */
#define kXCDataStoreType @"sqlite"

/*!
 @discussion
 Persistence Data File Name with extension.
 */
#define kXCDataSQLiteFile [NSString stringWithFormat:@"%@.sqlite", kXCDataModelFile]





#pragma mark - Core Data Entities

/*!
 @discussion
 Accomodation Entity Name.
 */
#define kAccomodation @"Accomodation"

/*!
 @discussion
 Address Entity Name.
 */
#define kAddress @"Address"

/*!
 @discussion
 AppInfoData Entity Name.
 */
#define kAppInfoData @"AppInfoData"

/*!
 @discussion
 Appointment Entity Name.
 */
#define kAppointment @"Appointment"

/*!
 @discussion
 CP12Info Entity Name.
 */
#define kCP12Info @"CP12Info"

/*!
 @discussion
 Customer Entity Name.
 */
#define kCustomer @"Customer"

/*!
 @discussion
 CustomerRiskData Entity Name.
 */
#define kCustomerRiskData @"CustomerRiskData"

/*!
 @discussion
 CustomerVulnerabilityData Entity Name.
 */
#define kCustomerVulnerabilityData @"CustomerVulnerabilityData"

/*!
 @discussion
 FaultRepairData Entity Name.
 */
#define kFaultRepairData @"FaultRepairData"

/*!
 @discussion
 FaultRepairListHistory Entity Name.
 */
#define kFaultRepairListHistory @"FaultRepairListHistory"

/*!
 @discussion
 FormData Entity Name.
 */
#define kFormData @"FormData"

/*!
 @discussion
 JobDataList Entity Name.
 */
#define kJobDataList @"JobDataList"

/*!
 @discussion
 JobDataList Entity Name.
 */
#define kTradeComponent @"PlannedTradeComponent"

/*!
 @discussion
 Journal Entity Name.
 */
#define kJournal @"Journal"

/*!
 @discussion
 Outbox Entity Name.
 */
#define kOutbox @"Outbox"

/*!
 @discussion
 Property Entity Name.
 */
#define kProperty @"Property"

/*!
 @discussion
 Property Entity Name.
 */
#define kScheme @"Scheme"

/*!
 @discussion
 PropertyAsbestosData Entity Name.
 */
#define kPropertyAsbestosData @"PropertyAsbestosData"

/*!
 @discussion
 PropertyPicture Entity Name.
 */
#define kPropertyPicture @"PropertyPicture"

/*!
 @discussion
 SurveyData Entity Name.
 */
#define kSurveyData @"SurveyData"

/*!
 @discussion
 User Entity Name.
 */
#define kUser @"User"

/*!
 @discussion
 Surveyor Entity Name.
 */
#define kSurveyor @"Surveyor"

/*!
 @discussion
 SProperty Entity Name.
 */
#define kSProperty @"SProperty"

/*!
 @discussion
 Survey Entity Name.
 */
#define kSurvey @"Survey"


#define kSurveyHeatingFuelDataEntity    @"SurveyHeatingFuelData"
#define kHeatingFuelSurveyDataEntFuelTypeId   @"fuelTypeId"
#define kHeatingFuelSurveyDataEntHeatingFuelSurvey  @"heatingFuelSurvey"


#define kSurveyComponentEntity  @"Component"
#define kComponentEntComponentId    @"componentId"
#define kComponentEntComponentName    @"componentName"
#define kComponentEntCycle          @"cycle"
#define kComponentEntFrequency      @"frequency"
#define kComponentEntItemId         @"itemId"
#define kComponentEntParameterId    @"parameterId"
#define kComponentEntSubParameterId @"subParameterId"
#define kComponentEntSubValueId     @"subValueId"
#define kComponentEntValueId        @"valueId"
#define kComponentEntIsComponentAccounting  @"isComponentAccounting"

#define kSurveyHeatingFuelCertificateEntity @"SurveyHeatingFuelCertificate"
#define kHeatingCertificateEntCertificateId @"certificateId"
#define kHeatingCertificateEntCertificateName   @"certificateName"
#define kHeatingCertificateEntFuelTypeId    @"fuelTypeId"

/*!
 @discussion
 Appliance Entity Name.
 */
#define kAppliance @"Appliance"

/*!
 @discussion
 ApplianceLocation Entity Name.
 */
#define kApplianceLocation @"ApplianceLocation"

/*!
 @discussion
 ApplianceManufacturer  Entity Name.
 */
#define kApplianceManufacturer @"ApplianceManufacturer"

/*!
 @discussion
 ApplianceModel Entity Name.
 */
#define kApplianceModel @"ApplianceModel"

/*!
 @discussion
 ApplianceType Entity Name.
 */
#define kApplianceType @"ApplianceType"

/*!
 @discussion
 ApplianceInspection Entity Name.
 */
#define kApplianceInspection @"ApplianceInspection"

/*!
 @discussion
 kApplianceDefects Entity Name.
 */
#define kApplianceDefects @"ApplianceDefects"


/*!
 @discussion
 Property Pipework Installation Entity Name.
 */
#define kInstallationPipework @"InstallationPipework"

/*!
 @discussion
 Appliance Defect Entity Name.
 */
#define kDefect @"Defect"

/*!
 @discussion
 Appliance DefectPicture Entity Name.
 */
#define kDefectPicture @"DefectPicture"

/*!
 @discussion
 Defect Category Entity Name.
 */
#define kDefectCategory @"DefectCategory"

/*!
 @discussion
 Pause Reason Entity Name.
 */
#define kJobPauseReason @"JobPauseReason"

/*!
 @discussion
Job Pause Data Entity Name.
 */
#define kJobPauseData @"JobPauseData"

/*!
 @discussion
 Job Detector Entity Name.
 */
#define kDetector @"Detector"

/*!
 @discussion
 Job Detector Inspection Entity Name.
 */
#define kDetectorInspection @"DetectorInspection"

/*!
 @discussion
 Repair pictures Entity Name.
 */
#define kRepairPictures @"RepairPictures"

#pragma mark - Property Address Styles
/*!
 @discussion
 Property Address Styles
 */
typedef NS_ENUM(NSInteger, PropertyAddressStyle)
{
    PropertyAddressStyleShort,
    PropertyAddressStyleLong,
    PropertyAddressStyleFull,
    PropertyAddressStyle1,
    PropertyAddressStyleAddress2Onwards
};


#pragma mark - Appointment Date Style
/*!
 @discussion
 Appointment Date Style. 
 @result
 Appointment String Date formed using appointment start time and end time, or appointmentData in case of long style.
 */
typedef NS_ENUM(NSInteger, AppointmentDateStyle)
{
    AppointmentDateStyleStartTime,
    AppointmentDateStyleEndTime,
    AppointmentDateStyleStartToEnd,
    AppointmentDateStyleComplete,
};

/*!
 @discussion
 AppointmentStatus represents the appointment's completion status.
 */
typedef NS_ENUM(NSInteger, AppointmentStatus) {
    AppointmentStatusPaused,
    AppointmentStatusInProgress,
    AppointmentStatusComplete,
    AppointmentStatusNotStarted,
    AppointmentStatusFinished,
    AppointmentStatusNoEntry,
};


/*!
 @discussion
 AppointmentType represents the appointment's type like Stock, Gas, Fault.
 */
typedef NS_ENUM(NSInteger, AppointmentType) {
    AppointmentTypeStock,
    AppointmentTypeGas,
    AppointmentTypeVoid,
    AppointmentTypeFault,
    AppointmentTypePre,
    AppointmentTypePost,
    AppointmentTypePlanned,
    AppointmentTypeAdaptations,
    AppointmentTypeMiscellaneous,
    AppointmentTypeCondition
};

/*!
 @discussion
 JobStatus represents the job completion status.
 */
typedef NS_ENUM(NSInteger, JobStatus) {
    JobStatusPaused,
    JobStatusInProgress,
    JobStatusComplete,
    JobStatusNotStarted,
    JobStatusFinished,
    JobStatusNoEntry,
};

/*!
 Appointment sync status*/
typedef NS_ENUM(NSInteger, AppointmentSyncStatus)
{
    AppointmentNotSynced,
    AppointmentDataSynced,
    AppointmentPhotoSynced,
    AppointmentSynced,
};

/*!
 Detector Types*/
typedef NS_ENUM(NSInteger, PropertyDetectorType)
{
	DetectorTypeSmoke = 1,
	DetectorTypeCO = 2
};

#pragma mark - Appointment Entity Attributes
/*!
 @discussion
 Appointment Entity Attributes.
 */
#define kAppointmentCalendar                @"appointmentCalendar"
#define kAppointmentDate                    @"appointmentDate"
#define kAppointmentStartTime               @"appointmentStartDateTime"
#define kAppointmentEndTime                 @"appointmentEndDateTime"
#define kAppointmentId                      @"appointmentId"
#define kAppointmentLocation                @"appointmentLocation"
#define kAppointmentNotes                   @"appointmentNotes"
#define kAppointmentOverdue                 @"appointmentOverdue"
#define kAppointmentShift                   @"appointmentShift"
#define kAppointmentStatus                  @"appointmentStatus"
#define kAppointmentTitle                   @"appointmentTitle"
#define kAppointmentType                    @"appointmentType"
#define kAssignedTo                         @"assignedTo"
#define kCreatedBy                          @"createdBy"
#define kDefaultCustomerId                  @"defaultCustomerId"
#define kDefaultCustomerIndex               @"defaultCustomerIndex"
#define kJournalHistoryId                   @"journalHistoryId"
#define kJournalId                          @"journalId"
#define kJsgNumber                          @"jsgNumber"
#define kLoggedDate                         @"loggedDate"
#define kNoEntryNotes                       @"noEntryNotes"
#define kRepairCompletionDateTime           @"repairCompletionDateTime"
#define kRepairNotes                        @"repairNotes"
#define kSurveyFormJSON                     @"surveyFormJSON"
#define kSurveyorAlert                      @"surveyorAlert"
#define kSurveyorUserName                   @"surveyorUserName"
#define kSurveyourAvailability              @"surveyourAvailability"
#define kSurveyType                         @"surveyType"
#define kTenancyId                          @"tenancyId"
#define kAppointmentToCP12Info              @"CP12Info"
#define kAppointmentToAppInfoData           @"appInfoData"
#define kAppointmentToCustomer              @"customerList"
#define kAppointmentToJobDataList           @"jobDataList"
#define kAppointmentToComponentTrade        @"componentTrade"
#define kAppointmentToJournal               @"journal"
#define kAppointmentToProperty              @"property"
#define kAppointmentToScheme                @"scheme"
#define kAppointmentEventIdentifier         @"appointmentEventIdentifier"
#define kAppointmentDateSectionIdentifier   @"appointmentDateSectionIdentifier"
#define kAppointmentIsModified              @"isModified"
#define kAppointmentAddtoCalendar           @"addToCalendar"
#define kAppointmentStartDateTime           @"appointmentStartTime"
#define kCreatedByPerson                    @"createdByPerson"
#define kIsMiscAppointment                  @"isMiscAppointment"
#define kAppointmentFailedReason            @"failureReason"
#define kAppointmentSyncStatus              @"syncStatus"
#define kAptCompletedAppVersionInfo         @"appointmentCompletedAppVersion"
#define kAptCurrentAppVersionInfo           @"appointmentCurrentAppVersion"

#pragma mark - User Entity Attributes
/*!
 @discussion
 User Entity Attributes.
 */
#define kFullName                   @"fullName"
#define kIsActive                   @"isActive"
#define kLastLoggedInDate           @"lastLoggedInDate"
#define kPassword                   @"password"
#define kSalt                       @"salt"
#define kUserId                     @"userId"
#define kUserName                   @"userName"

/*!
 @discussion
 Surveyor Entity Attributes. 
 (Other the "SurveyorType" All Entity Attributes are similar to User's Entity Attributes)
 */
#define kSurveyorType                       @"surveyorType"
#define kSurveyorNameSectionIdentifier      @"surveyorNameSectionIdentifier"

#pragma mark - JobDataList Entity Attributes
/*!
 @discussion
 JobDataList Entity Attributes.
 */
#define kDuration                       @"duration"
#define kDurationUnit                   @"durationUnit"
#define kFaultLogID                     @"faultLogID"
#define kFollowOnNotes                  @"followOnNotes"
#define kJobStatus                      @"jobStatus"
#define kJSNDescription                 @"JSNDescription"
#define kJSNLocation                    @"JSNLocation"
#define kJSNNotes                       @"JSNNotes"
#define kJSNumber                       @"JSNumber"
#define kPriority                       @"priority"
#define kJobRepairNotes                 @"repairNotes"
#define kReportedDate                   @"reportedDate"
#define kResponseTime                   @"responseTime"


#pragma mark - Accomodation Entity Attributes
/*!
 @discussion
 Accomodation Entity Attributes.
 */
#define kRoomHeight                 @"roomHeight"
#define kRoomLength                 @"roomLength"
#define kRoomName                   @"roomName"
#define kRoomWidth                  @"roomWidth"
#define kUpdatedBy                  @"updatedBy"
#define kUpdatedOn                  @"updatedOn"


#pragma mark - Tenant Entity Attributes
/*!
 @discussion
 Tenant(Customer) Entity Attributes.
 */
#define kCustomerId                 @"customerId"
#define kCustomerAddress            @"address"
#define kCustomerEmail              @"email"
#define kCustomerFirstName          @"firstName"
#define kCustomerLastName           @"lastName"
#define kCustomerMiddleName         @"middleName"
#define kCustomerMobile             @"mobile"
#define kCustomerFax                @"fax"
#define kCustomerProperty           @"property"
#define kCustomerTelephone          @"telephone"
#define kCustomerTitle              @"title"
#define kCustomerCompleteName       @"completeName"


#pragma mark - SProperty Entity Attributes
/*!
 @discussion
 SProperty Entity Attributes.
 */
#define kSPropertyNameSectionIdentifier     @"sPropertyNameSectionIdentifier"

#pragma mark - Property Entity Attributes
/*!
 @discussion
 Property Entity Attributes.
 */
#define kAddress1                   @"address1"
#define kAddress2                   @"address2"
#define kAddress3                   @"address3"
#define kCertificateExpiry          @"certificateExpiry"
#define kCounty                     @"county"
#define kFlatNumber                 @"flatNumber"
#define kHouseNumber                @"houseNumber"
#define kLastSurveyDate             @"lastSurveyDate"
#define kPostCode                   @"postCode"
#define kPropertyId                 @"propertyId"
#define kTenancyId                  @"tenancyId"
#define kTownCity                   @"townCity"
#define kCompleteAddress            @"completeAddress"

#pragma mark - Scheme Entity Attributes
/*!
 @discussion
 Scheme Entity Attributes.
 */
#define kSchemeBlockID      @"blockId"
#define kSchemeID           @"schemeId"
#define kSchemeName         @"schemeName"
#define kSchemeBlockName    @"blockName"
#define kSchemCityTown      @"towncity"
#define kSchemePostCode     @"postcode"
#define kSchemeCounty       @"county"

#pragma mark - PropertyPicture Entity Attributes

#define kPropertyPictureHeatingId   @"heatingId"
/*!
 @discussion
 PropertyPicture Entity Attributes.
 */
#define kDefectPictureId                @"defectPictureId"

#define kPropertyPictureId              @"propertyPictureId"
#define kItemId                         @"itemId"
#define kPropertyPicturePath            @"imagePath"
#define kPropertyPictureName            @"propertyPictureName"
#define kPropertyPictureIsDefault       @"isDefault"
#define kPropertyPicturesSynchStatus    @"synchStatus"
#define kPropertyImage                  @"propertyImage"
#define kFileExtension                  @"fileExt"
#define kIsDefault                      @"isDefault"

#pragma mark - Appliances Entity(ies) Attributes
/*!
 @discussion
 Appliance Entity Attributes.
 */
#define kApplianceId                       @"ApplianceID"
#define kApplianceFlueType                 @"FluType"
#define kApplianceInstalledDate            @"InstalledDate"
#define kApplianceReplacementDate          @"ReplacementDate"
#define kApplianceSerialNumber             @"SerialNumber"
#define kApplianceGCNumber                 @"GCNumber"
#define kApplianceIsLandlordAppliance      @"isLandlordAppliance"
#define kApplianceIsInspected              @"isInspected"


#pragma mark - RepairPicture Entity Attributes
/*!
 @discussion
 RepairPicture Entity Attributes.
 */
#define kIsBeforeImage                  @"isBeforeImage"
#define kFaultRepairImageId                  @"FaultRepairImageId"
#define kRepairImagePath                @"imagePath"
#define kRepairImage                    @"image"
#define kPropertyPictureIsDefault       @"isDefault"
#define kRepairImageSyncStatus          @"syncStatus"
#define kRepairJSNumber                 @"jsNumber"
#define kTemporaryImageFileName     @"temporaryImageFileToUpload.png"


/*!
 @discussion
 ApplianceLocation Entity Attributes.
 */
#define kApplianceLocationName      @"Location"
#define kApplianceLocationID        @"LocationID"

/*!
 @discussion
 ApplianceManufacturer Entity Attributes.
 */
#define kApplianceManufacturerName      @"Manufacturer"
#define kApplianceManufacturerID        @"ManufacturerID"

/*!
 @discussion
 ApplianceModel Entity Attributes.
 */
#define kApplianceModelName      @"ApplianceModel"
#define kApplianceModelID    @"ApplianceModelID"

/*!
 @discussion
 ApplianceType Entity Attributes.
 */
#define kApplianceTypeName      @"ApplianceType"
#define kApplianceTypeID        @"ApplianceTypeID"

/*!
 @discussion
 Detector Entity Attributes.
 */

#define kDetectorCount          @"detectorCount"
#define kDetectorTypeId         @"detectorTypeId"
#define kDetectorTypeDesc       @"detectorType"
#define kDetectorType           @"DetectorType"
#define kPowerType              @"PowerType"

#define kDetectorsType          @"detectorType"
#define kDetectorTypeId         @"detectorTypeId"
#define kInstalledDate          @"installedDate"
#define kIsLandlordsDetector    @"isLandlordsDetector"
#define kLocation               @"location"
#define kManufacturer           @"manufacturer"
#define kDetectorPropertyID     @"propertyId"
#define kSerialNumber           @"serialNumber"
#define kPowerTypeId            @"powerTypeId"
#define kDetectorId             @"detectorId"
#define kPowerTypeDesc          @"powerType"
#define kDetectorNotes          @"notes"
#define kDetectorInstalledBy    @"installedBy"

#pragma mark - Survey Entity Attributes
/*!
 @discussion
 Property Survey Attributes.
 */
#define kSurveyJSON                 @"surveyJSON"
#define kIsVisited                  @"isVisited"

#pragma mark - New Appointment Entity Attributes
/*!
 @discussion
 New Appointment Attributes.
 */


#pragma mark - Appliance Inspection Entity Attributes
/*!
 @discussion
 Appliance Inspection Attributes.
 */

#define kIsInspected                  @"isInspected"
#define kCombustionReading            @"combustionReading"
#define kOperatingPressure            @"operatingPressure"
#define kSafetyDeviceOperational      @"safetyDeviceOperational"
#define kSmokePellet                  @"smokePellet"
#define kAdequateVentilation          @"adequateVentilation"
#define kFlueVisualCondition          @"flueVisualCondition"
#define kSatisfactoryTermination      @"satisfactoryTermination"
#define kFluePerformanceChecks        @"fluePerformanceChecks"
#define kApplianceServiced            @"applianceServiced"
#define kApplianceSafeTouse           @"applianceSafeToUse"
#define kInspectionId                 @"inspectionID"
#define kInspectedBy                  @"inspectedBy"
#define kInspectionDate               @"inspectionDate"
#define kSpillageTest                 @"spillageTest"
#define kOperatingPressureUnit        @"operatingPressureUnit"

#pragma mark - Appliance Detector Entity Attributes
/*!
 @discussion
 Appliance Detector Attributes.
 */
#define kDetectorTest                  @"detectorTest"


#pragma mark - Property Installation Pipework Entity Attributes
/*!
 @discussion
Property Installation PipeworkA ttributes.
 */

#define kEmergencyControl         @"emergencyControl"
#define kEquipotentialBonding     @"equipotentialBonding"
#define kGasTightnessTest          @"gasTightnessTest"
#define KVisualInspection          @"visualInspection"

#pragma mark - Defect Entity Attributes
/*!
 @discussion
 Defect Entity Attributes.
 */

#define kDefectDate                 @"DefectDate"
#define kDefectDescription          @"DefectDesc"
#define kDefectID                   @"ID"
#define kFaultCategory              @"FaultCategory"
#define kIsActionTaken              @"IsActionTaken"
#define kisAdviceNoteIssued         @"isAdviceNoteIssued"
#define kIsDefectIdentified         @"IsDefectIdentified"
#define kRemedialAction             @"RemedialAction"
#define kSerialNo                   @"SerialNo"
#define kWarningTagFixed            @"WarningTagFixed"
#define kDefectType                 @"defectType"

#pragma mark - Defect Category Entity Attributes
/*!
 @discussion
 Defect Category Entity Attributes.
 */
#define kDefectCategoryDescription        @"CatDescription"
#define kDefectCategoryID                 @"CatId"
#define kCategoryDescription              @"categoryDescription"

#pragma mark - Fault Repair Entity Attributes
/*!
 @discussion
 Fault Repair Entity Attributes.
 */
#define kFaultRepairDescription             @"Description"
#define kFaultRepairID                      @"FaultRepairID"
#define kRepairSectionIdentifier            @"repairSectionIdentifier"

#pragma mark - Job Pause Data Entity Attributes
/*!
 @discussion
 Job Pause Data Entity Attributes.
 */
#define kPauseDate                  @"pauseDate"
#define kPauseNote                  @"pauseNote"



#pragma mark - JobDataList Entity Attributes
/*!
 @discussion
 PlannedTradeComponent Entity Attributes.
 */
#define kTradeDescription           @"trade"
#define kTradeId                    @"tradeId"
#define kSortingOrder               @"sorder"
#define kComponentId                @"componentId"
#define kComponentName              @"componentName"
#define kComponentTradeId           @"componentTradeId"
#define kPMODescription             @"PMO"
#define kCompletionDate             @"completionDate"
#define kLocation                   @"location"
#define kLocationId                 @"locationId"
#define kAdaptation                 @"adaptation"
#define kAdaptationId               @"adaptationId"
