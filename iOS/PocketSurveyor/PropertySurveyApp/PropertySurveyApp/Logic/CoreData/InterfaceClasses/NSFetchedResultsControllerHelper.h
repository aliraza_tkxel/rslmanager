//
//  NSFetchedResultsControllerHelper.h
//  TkXel_iOS
//
//  Created by Ansari on 3/28/13.
//
//

#import <Foundation/Foundation.h>


@interface NSFetchedResultsControllerHelper : NSObject

+ (NSSortDescriptor *) getSortDescriptersForOption:(PSFilterOption)filterOption;
+ (void) filterAppointmentsWithOptions:(NSArray *)filterOptions controller:(NSFetchedResultsController *)_fetchedResultsController;

@end
