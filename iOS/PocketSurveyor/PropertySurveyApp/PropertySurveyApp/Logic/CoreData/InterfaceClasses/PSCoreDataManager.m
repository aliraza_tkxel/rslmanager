//
//  PSCoreDataManager.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 27/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSCoreDataManager.h"
#import "CoreDataHelper.h"

@implementation PSCoreDataManager

#pragma mark -
#pragma mark ARC Singleton Implementation

static PSCoreDataManager *sharedCoreDataManager = nil;
+ (PSCoreDataManager *) sharedManager
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedCoreDataManager = [[PSCoreDataManager alloc] init];
		// Do any other initialisation stuff here
		
	});
	return sharedCoreDataManager;
}

+ (id)allocWithZone:(NSZone *)zone {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedCoreDataManager = [super allocWithZone:zone];
	});
	return sharedCoreDataManager;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

#pragma mark - User Entity Data Control Methods
- (User *) userWithIdentifier:(NSNumber *)userId
{
	User *user = nil;
	if (!isEmpty(userId))
	{
		STARTEXCEPTION
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId = %@",userId];
		NSArray* users = [CoreDataHelper getObjectsFromEntity:kUser
																								predicate:predicate
																									sortKey:nil
																						sortAscending:YES
																									context:[PSDatabaseContext sharedContext].managedObjectContext];
		if([users count] > 0)
		{
			user = [users firstObject];
		}
		ENDEXCEPTION
	}
	return user;
}

-(BOOL) authenticateUserOffline:(NSString *)userName password:(NSString *)password
{
	BOOL isAuthenticationSuccess = NO;
	if (!isEmpty(userName) && !isEmpty(password))
	{
		STARTEXCEPTION
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.userName LIKE[cd] %@ AND SELF.password LIKE %@",userName,password];
		NSArray* users = [CoreDataHelper getObjectsFromEntity:kUser
																								predicate:predicate
																									sortKey:nil
																						sortAscending:YES
																									context:[PSDatabaseContext sharedContext].managedObjectContext];
		if([users count] > 0)
		{
			User *user = [users firstObject];
			[SettingsClass sharedObject].currentUserId = user.userId;
			[SettingsClass sharedObject].currentUserPassword = user.password;
			[SettingsClass sharedObject].loggedInUser = user;
			isAuthenticationSuccess = YES;
		}
		ENDEXCEPTION
	}
	return isAuthenticationSuccess;
}

-(User *) userWithUserName:(NSString *)userName
{
	User *user = nil;
	
	if (!isEmpty(userName))
	{
		STARTEXCEPTION
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.userName LIKE[cd] %@",userName];
		NSArray* users = [CoreDataHelper getObjectsFromEntity:kUser
																								predicate:predicate
																									sortKey:nil
																						sortAscending:YES
																									context:[PSDatabaseContext sharedContext].managedObjectContext];
		if([users count] > 0)
		{
			user = [users firstObject];
		}
		ENDEXCEPTION
	}
	
	return user;
}
- (BOOL) isUserExistsWithIdentifier:(NSNumber *)userId
{
	BOOL userExists = NO;
	@autoreleasepool
	{
		if(userId != nil)
		{
			User *user = [[PSCoreDataManager sharedManager] userWithIdentifier:userId];
			if(user != nil)
			{
				userExists = YES;
			}
		}
	}
	return userExists;
}


#pragma mark - Surveyor Entity Data Control Methods
- (Surveyor *) surveyorWithIdentifier:(NSNumber *)userId
{
	Surveyor *surveyor = nil;
 
	if (!isEmpty(userId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			
			
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId = %@",userId];
			NSArray* surveyors = [CoreDataHelper getObjectsFromEntity:kSurveyor
																											predicate:predicate
																												sortKey:nil
																									sortAscending:YES
																												context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([surveyors count] > 0)
			{
				surveyor = [surveyors firstObject];
			}
		}
		ENDEXCEPTION
	}
	return surveyor;
}

- (Surveyor *)fetchLoggedInSurveyor
{
	Surveyor *surveyor = nil;
	User *loggedInUser = [SettingsClass sharedObject].loggedInUser;
	if (!isEmpty(loggedInUser))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userName = %@",loggedInUser.userName];
			NSArray* surveyors = [CoreDataHelper getObjectsFromEntity:kSurveyor
																											predicate:predicate
																												sortKey:nil
																									sortAscending:YES
																												context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([surveyors count] > 0)
			{
				surveyor = [surveyors firstObject];
			}
		}
		ENDEXCEPTION
	}
	return surveyor;
	
}
- (BOOL) isSurveyorExistsWithIdentifier:(NSNumber *)surveyorId
{
	BOOL surveyorExists = NO;
	@autoreleasepool
	{
		
		
		if(surveyorId != nil)
		{
			Surveyor *surveyor = [[PSCoreDataManager sharedManager] surveyorWithIdentifier:surveyorId];
			if(surveyor != nil)
			{
				surveyorExists = YES;
			}
		}
	}
	return surveyorExists;
}

#pragma mark - Property Entity Data Control Methods
- (SProperty *) sPropertyWithIdentifier:(NSString *)propertyId
{
	SProperty *property = nil;
	
	if (!isEmpty(propertyId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"propertyId = %@",propertyId];
			NSArray* properties = [CoreDataHelper getObjectsFromEntity:kSProperty
																											 predicate:predicate
																												 sortKey:nil
																									 sortAscending:YES
																												 context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([properties count] > 0)
			{
				property = [properties firstObject];
			}
		}
		ENDEXCEPTION
	}
	
	return property;
}

//unused
- (BOOL) isPropertyExistsWithIdentifier:(NSString *)propertyId
{
	BOOL propertyExists = NO;
	@autoreleasepool {
		
		
		if(propertyId != nil)
		{
			SProperty *property = [[PSCoreDataManager sharedManager] sPropertyWithIdentifier:propertyId];
			if(property != nil)
			{
				propertyExists = YES;
			}
		}
	}
	return propertyExists;
}

- (id) findRecordFrom:(NSString *)table Field:(NSString *)field WithValue:(id)value context:(NSManagedObjectContext *)dbContext
{
	id record = nil;
	if (!isEmpty(value))
	{
		STARTEXCEPTION
		@autoreleasepool
		{
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@", field, value];
			NSArray* fetchedAppointments = [CoreDataHelper getObjectsFromEntity:table
																																predicate:predicate
																																	sortKey:nil
																														sortAscending:YES
																																	context:dbContext];
			if([fetchedAppointments count] > 0)
			{
				record = [fetchedAppointments firstObject];
			}
		}
		ENDEXCEPTION
	}
	return record;
}

- (id) findRecordFrom:(NSString *)table Field1:(NSString *)field1 WithValue1:(id)value1 AndField2:(NSString *)field2 WithValue2:(id)value2 context:(NSManagedObjectContext *)dbContext
{
	id record = nil;
	STARTEXCEPTION
	@autoreleasepool
	{
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@ AND SELF.%@ = %@", field1, value1, field2, value2];
		NSArray* fetchedAppointments = [CoreDataHelper getObjectsFromEntity:table
																															predicate:predicate
																																sortKey:nil
																													sortAscending:YES
																																context:dbContext];
		if([fetchedAppointments count] > 0)
		{
			record = [fetchedAppointments firstObject];
		}
	}
	ENDEXCEPTION
	return record;
}

#pragma mark - Appointment Entity Data Control Methods
- (Appointment *) appointmentWithIdentifier:(NSNumber *)appointmentId context:(NSManagedObjectContext *)managedObjectContext
{
	Appointment *appointment = nil;
	
	if (isEmpty(managedObjectContext))
	{
		managedObjectContext = [PSDatabaseContext sharedContext].managedObjectContext;
		
	}
	
	if (!isEmpty(appointmentId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@", kAppointmentId, appointmentId];
			NSArray* fetchedAppointments = [CoreDataHelper getObjectsFromEntity:kAppointment
																																predicate:predicate
																																	sortKey:nil
																														sortAscending:YES
																																	context:managedObjectContext];
			if([fetchedAppointments count] > 0)
			{
				appointment = [fetchedAppointments firstObject];
			}
		}
		ENDEXCEPTION
	}
	
	return appointment;
}

- (Appointment *) appointmentWithIdentifier:(NSNumber *)appointmentId propertyId:(NSNumber *)propertyId
{
	Appointment *appointment = nil;
	
	if (!isEmpty(appointmentId))
	{
		STARTEXCEPTION
		@autoreleasepool
		{
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@ AND SELF.appointmentToProperty.%@ = %@", kAppointmentId, appointmentId, kPropertyId, propertyId];
			NSArray* fetchedAppointments = [CoreDataHelper getObjectsFromEntity:kAppointment
																																predicate:predicate
																																	sortKey:nil
																														sortAscending:YES
																																	context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([fetchedAppointments count] > 0)
			{
				appointment = [fetchedAppointments firstObject];
			}
		}
		ENDEXCEPTION
	}
	return appointment;
}


- (Appointment *) appointmentWithPropertyIdentifier:(NSNumber *)propertyId appointmentId:(NSNumber *)appointmentId
{
	Property *property = nil;
	
	Appointment *appointment = nil;
	
	if (!isEmpty(propertyId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@ AND SELF.propertyToAppointment.%@ = %@", kPropertyId, propertyId, kAppointmentId, appointmentId];
			NSArray* fetchedProperties = [CoreDataHelper getObjectsFromEntity:kProperty
																															predicate:predicate
																																sortKey:nil
																													sortAscending:YES
																																context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([fetchedProperties count] > 0)
			{
				property = [fetchedProperties firstObject];
				if(property != nil)
				{
					appointment = property.propertyToAppointment;
				}
			}
		}
		ENDEXCEPTION
	}
	return appointment;
}

#pragma mark - Property Entity Data Control Methods
- (Property *)propertyWithIdentifier:(NSString *)propertyId
{
	return [self propertyWithIdentifier:propertyId context:[PSDatabaseContext sharedContext].managedObjectContext];
}

- (Property *)propertyWithIdentifier:(NSString *)propertyId context:(NSManagedObjectContext *)managedObjectContext
{
	Property *property = nil;
	
	if (!isEmpty(propertyId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.propertyId MATCHES[cd] %@",propertyId];
			NSArray* properties = [CoreDataHelper getObjectsFromEntity:kProperty
																											 predicate:predicate
																												 sortKey:nil
																									 sortAscending:YES
																												 context:managedObjectContext];
			if([properties count] > 0)
			{
				property = [properties firstObject];
			}
		}
		ENDEXCEPTION
	}
	return property;
}

#pragma mark - Defect Picture Entity Data Control Methods
- (DefectPicture *)defectPictureWithIdentifier:(NSNumber *)propertyPictureId
{
	if (!propertyPictureId) {
		
		return nil;
	}
	return [self defectPictureWithIdentifier:propertyPictureId context:[PSDatabaseContext sharedContext].managedObjectContext];
}

- (DefectPicture *)defectPictureWithIdentifier:(NSNumber *)propertyPictureId context:(NSManagedObjectContext *)managedObjectContext
{
	DefectPicture *propertyPicture = nil;
	
	if (!isEmpty(propertyPictureId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.defectPictureId = %@",propertyPictureId];
			NSArray* propertyPictures = [CoreDataHelper getObjectsFromEntity:kDefectPicture
																														 predicate:predicate
																															 sortKey:nil
																												 sortAscending:YES
																															 context:managedObjectContext];
			if([propertyPictures count] > 0)
			{
				propertyPicture = [propertyPictures firstObject];
			}
		}
		ENDEXCEPTION
	}
	return propertyPicture;
}

- (NSArray *)defectPictureListWithIdentifier:(NSNumber *)propertyPictureId
{
	NSArray* propertyPictures = nil;
	
	if (!isEmpty(propertyPictureId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.defectPictureToDefect.defectID = %@",propertyPictureId];
			propertyPictures = [CoreDataHelper getObjectsFromEntity:kDefectPicture
																										predicate:predicate
																											sortKey:nil
																								sortAscending:YES
																											context:[PSDatabaseContext sharedContext].managedObjectContext];
			
		}
		ENDEXCEPTION
	}
	return propertyPictures;
}

- (NSArray *)defectPictureListWithIdentifier:(NSNumber *)propertyPictureId andJournalID:(NSNumber*)journalID
{
	NSArray* propertyPictures = nil;
	
	if (!isEmpty(propertyPictureId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.defectPictureToDefect.defectID = %@ AND self.defectPictureToDefect.journalId = %@",propertyPictureId,journalID];
			propertyPictures = [CoreDataHelper getObjectsFromEntity:kDefectPicture
																										predicate:predicate
																											sortKey:nil
																								sortAscending:YES
																											context:[PSDatabaseContext sharedContext].managedObjectContext];
			
		}
		ENDEXCEPTION
	}
	return propertyPictures;
}

#pragma mark - Property Picture Entity Data Control Methods
- (PropertyPicture *)propertyPictureWithIdentifier:(NSNumber *)propertyPictureId
{
	if (!propertyPictureId) {
		
		return nil;
	}
	return [self propertyPictureWithIdentifier:propertyPictureId context:[PSDatabaseContext sharedContext].managedObjectContext];
}

- (PropertyPicture *)propertyPictureWithIdentifier:(NSNumber *)propertyPictureId context:(NSManagedObjectContext *)managedObjectContext
{
	
	PropertyPicture *propertyPicture = nil;
	
	if (!isEmpty(propertyPictureId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.propertyPictureId = %@",propertyPictureId];
			NSArray* propertyPictures = [CoreDataHelper getObjectsFromEntity:kPropertyPicture
																														 predicate:predicate
																															 sortKey:nil
																												 sortAscending:YES
																															 context:managedObjectContext];
			if([propertyPictures count] > 0)
			{
				propertyPicture = [propertyPictures firstObject];
			}
		}
		ENDEXCEPTION
	}
	return propertyPicture;
}

/*- (NSArray *) propertyPicturesWithPropertyIdentifier:(NSString *)propertyId appointmentId:(NSNumber *)appointmentId itemId:(NSNumber*) itemId
{
	Property *property = nil;
	NSArray* fetchedProperties=nil;
	
	if (!isEmpty(propertyId) && !isEmpty(appointmentId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.propertyPictureToProperty.%@ = %@ AND SELF.propertyPictureToProperty.propertyToAppointment.%@ = %@ AND SELF.itemId= %@ AND SELF.isDefault == NO AND SELF.deletedPicture == NO"
																, kPropertyId, propertyId, kAppointmentId, appointmentId, itemId];
			fetchedProperties = [CoreDataHelper getObjectsFromEntity:kPropertyPicture
																										 predicate:predicate
																											 sortKey:nil
																								 sortAscending:YES
																											 context:[PSDatabaseContext sharedContext].managedObjectContext];
		}
		ENDEXCEPTION
	}
	return fetchedProperties;
}*/

- (NSArray *) propertyPicturesWithPropertyIdentifier:(NSString *)propertyId appointmentId:(NSNumber *)appointmentId itemId:(NSNumber*) itemId andHeatingId:(NSString *) heatingId
{
    Property *property = nil;
    NSArray* fetchedProperties=nil;
    
    if (!isEmpty(propertyId) && !isEmpty(appointmentId))
    {
        STARTEXCEPTION
        @autoreleasepool {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.propertyPictureToProperty.%@ = %@ AND SELF.propertyPictureToProperty.propertyToAppointment.%@ = %@ AND SELF.itemId= %@ AND SELF.isDefault == NO AND SELF.deletedPicture == NO AND SELF.%@ == %@"
                                      , kPropertyId, propertyId, kAppointmentId, appointmentId, itemId,kPropertyPictureHeatingId,heatingId];
            fetchedProperties = [CoreDataHelper getObjectsFromEntity:kPropertyPicture
                                                           predicate:predicate
                                                             sortKey:nil
                                                       sortAscending:YES
                                                             context:[PSDatabaseContext sharedContext].managedObjectContext];
        }
        ENDEXCEPTION
    }
    return fetchedProperties;
}

#pragma mark - Customer Entity Data Control Methods
- (Customer *) customerWithIdentifier:(NSNumber *)customerId
{
	return [self customerWithIdentifier:customerId context:[PSDatabaseContext sharedContext].managedObjectContext];
}

- (Customer *) customerWithIdentifier:(NSNumber *)customerId context:(NSManagedObjectContext *)managedObjectContext
{
	Customer *customer = nil;
	
	if (!isEmpty(customerId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.customerId = %@",customerId];
			NSArray* customers = [CoreDataHelper getObjectsFromEntity:kCustomer
																											predicate:predicate
																												sortKey:nil
																									sortAscending:YES
																												context:managedObjectContext];
			if([customers count] > 0)
			{
				customer = [customers firstObject];
			}
		}
		ENDEXCEPTION
	}
	return customer;
}

#pragma mark - Appliance Entity(ies) Data Control Methods

- (Appliance *) applianceWithIdentifier:(NSNumber *)applianceId inContext:(NSManagedObjectContext *)managedObjectContext
{
	Appliance *appliance = nil;
	if (isEmpty(managedObjectContext))
	{
		managedObjectContext = [PSDatabaseContext sharedContext].managedObjectContext;
		
	}
	if (!isEmpty(applianceId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.applianceID = %@", applianceId];
			NSArray* fetchedAppliances = [CoreDataHelper getObjectsFromEntity:kAppliance
																															predicate:predicate
																																sortKey:nil
																													sortAscending:YES
																																context:managedObjectContext];
			if ([fetchedAppliances count] > 0)
			{
				appliance = [fetchedAppliances firstObject];
			}
		}
		ENDEXCEPTION
	}
	
	return appliance;
}

- (Appliance *) applianceWithIdentifier:(NSNumber *)applianceId context:(NSManagedObjectContext *)managedObjectContext
{
	Appliance *appliance = nil;
	if (!isEmpty(applianceId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			if (!isEmpty(applianceId))
			{
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.applianceID = %@", applianceId];
				NSArray* fetchedAppliances = [CoreDataHelper getObjectsFromEntity:kAppliance
																																predicate:predicate
																																	sortKey:nil
																														sortAscending:YES
																																	context:managedObjectContext];
				if ([fetchedAppliances count] > 0)
				{
					appliance = [fetchedAppliances firstObject];
				}
			}
		}
		ENDEXCEPTION
	}
	return appliance;
}


- (ApplianceLocation *) applianceLocationWithName:(NSString *)locationName inContext:(NSManagedObjectContext*)managedObjectContext
{
	@synchronized(self)
	{
		ApplianceLocation *applianceLocation = nil;
		STARTEXCEPTION
		@autoreleasepool {
			if (!isEmpty(locationName))
			{
				
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.locationName LIKE[cd] %@", locationName];
				NSArray* fetchedLocations = [CoreDataHelper getObjectsFromEntity:kApplianceLocation
																															 predicate:predicate
																																 sortKey:nil
																													 sortAscending:YES
																																 context:managedObjectContext];
				if(!isEmpty(fetchedLocations))
				{
					applianceLocation = [fetchedLocations firstObject];
				}
			}
		}
		ENDEXCEPTION
		return applianceLocation;
	}
}

- (ApplianceManufacturer *) applianceManufacturerWithName:(NSString *)manufacturerName inContext:(NSManagedObjectContext*)managedObjectContext
{
	ApplianceManufacturer *applianceManufacturer = nil;
	@autoreleasepool {
		
		
		if (!isEmpty(manufacturerName))
		{
			STARTEXCEPTION
			@autoreleasepool {
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.manufacturerName LIKE[cd] %@", manufacturerName];
				NSArray* fetchedManufacturers = [CoreDataHelper getObjectsFromEntity:kApplianceManufacturer
																																	 predicate:predicate
																																		 sortKey:nil
																															 sortAscending:YES
																																		 context:managedObjectContext];
				if([fetchedManufacturers count] > 0)
				{
					applianceManufacturer = [fetchedManufacturers firstObject];
				}
			}
			ENDEXCEPTION
		}
	}
	return applianceManufacturer;
}

- (ApplianceModel *) applianceModelWithName:(NSString *)modelName inContext:(NSManagedObjectContext*)managedObjectContext
{
	ApplianceModel *applianceModel = nil;
	if (!isEmpty(modelName))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.modelName LIKE[cd] %@", modelName];
			NSArray* fetchedModels = [CoreDataHelper getObjectsFromEntity:kApplianceModel
																													predicate:predicate
																														sortKey:nil
																											sortAscending:YES
																														context:managedObjectContext];
			if([fetchedModels count] > 0)
			{
				applianceModel = [fetchedModels firstObject];
			}
		}
		ENDEXCEPTION
	}
	
	return applianceModel;
}

- (ApplianceType *) applianceTypeWithName:(NSString *)typeName inContext:(NSManagedObjectContext*)managedObjectContext
{
	ApplianceType *applianceType = nil;
	if (!isEmpty(typeName))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.typeName LIKE[cd] %@", typeName];
			NSArray* fetchedTypes = [CoreDataHelper getObjectsFromEntity:kApplianceType
																												 predicate:predicate
																													 sortKey:nil
																										 sortAscending:YES
																													 context:managedObjectContext];
			if([fetchedTypes count] > 0)
			{
				applianceType = [fetchedTypes firstObject];
			}
		}
		ENDEXCEPTION
	}
	return applianceType;
}

//unused
- (ApplianceLocation *) applianceLocationWithIdentifier:(NSNumber *)locationID
{
	ApplianceLocation *applianceLocation = nil;
	@autoreleasepool {
		
		
		if (!isEmpty(locationID))
		{
			STARTEXCEPTION
			@autoreleasepool {
				NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.locationID = %@", locationID];
				NSArray* fetchedLocations = [CoreDataHelper getObjectsFromEntity:kApplianceLocation
																															 predicate:predicate
																																 sortKey:nil
																													 sortAscending:YES
																																 context:[PSDatabaseContext sharedContext].managedObjectContext];
				if([fetchedLocations count] > 0)
				{
					applianceLocation = [fetchedLocations firstObject];
				}
			}
			ENDEXCEPTION
		}
	}
	return applianceLocation;
}

//unused
- (ApplianceManufacturer *) applianceManufacturerWithIdentifier:(NSNumber *)manufacturerID
{
	ApplianceManufacturer *applianceManufacturer = nil;
	if (!isEmpty(manufacturerID))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.manufacturerID = %@", manufacturerID];
			NSArray* fetchedManufacturers = [CoreDataHelper getObjectsFromEntity:kApplianceManufacturer
																																 predicate:predicate
																																	 sortKey:nil
																														 sortAscending:YES
																																	 context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([fetchedManufacturers count] > 0)
			{
				applianceManufacturer = [fetchedManufacturers firstObject];
			}
		}
		ENDEXCEPTION
	}
	return applianceManufacturer;
}

//unused
- (ApplianceModel *) applianceModelWithIdentifier:(NSNumber *)modelID
{
	ApplianceModel *applianceModel = nil;
	
	STARTEXCEPTION
	@autoreleasepool {
		if (!isEmpty(modelID))
		{
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.modelID = %@", modelID];
			NSArray* fetchedModels = [CoreDataHelper getObjectsFromEntity:kApplianceModel
																													predicate:predicate
																														sortKey:nil
																											sortAscending:YES
																														context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([fetchedModels count] > 0)
			{
				applianceModel = [fetchedModels firstObject];
			}
		}
	}
	ENDEXCEPTION
	return applianceModel;
}

//unused
- (ApplianceType *) applianceTypeWithIdentifier:(NSNumber *)typeID
{
	ApplianceType *applianceType = nil;
	STARTEXCEPTION
	@autoreleasepool {
		if (!isEmpty(typeID))
		{
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.typeID = %@", typeID];
			NSArray* fetchedTypes = [CoreDataHelper getObjectsFromEntity:kApplianceType
																												 predicate:predicate
																													 sortKey:nil
																										 sortAscending:YES
																													 context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([fetchedTypes count] > 0)
			{
				applianceType = [fetchedTypes firstObject];
			}
		}
	}
	ENDEXCEPTION
	return applianceType;
}

#pragma mark - Defect Entity(ies) Data Control Methods

- (Defect *) defectWithIdentifier:(NSNumber *)defectId context:(NSManagedObjectContext *)managedObjectContext
{
	Defect *defect = nil;
	if (!isEmpty(defectId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.defectID = %@", defectId];
			NSArray* fetchedDefects = [CoreDataHelper getObjectsFromEntity:kDefect
																													 predicate:predicate
																														 sortKey:nil
																											 sortAscending:YES
																														 context:managedObjectContext];
			if(!isEmpty(fetchedDefects))
			{
				defect = [fetchedDefects firstObject];
			}
		}
		ENDEXCEPTION
	}
	return defect;
}

- (DefectCategory *) defectCategoryWithIdentifier:(NSNumber *)defectCategoryId context:(NSManagedObjectContext *)managedObjectContext
{
	DefectCategory *defectCategory = nil;
	if (!isEmpty(defectCategoryId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.categoryID = %@", defectCategoryId];
			NSArray* fetchedDefectCategories = [CoreDataHelper getObjectsFromEntity:kDefectCategory
																																		predicate:predicate
																																			sortKey:nil
																																sortAscending:YES
																																			context:managedObjectContext];
			if([fetchedDefectCategories count] > 0)
			{
				defectCategory = [fetchedDefectCategories firstObject];
			}
		}
		ENDEXCEPTION
	}
	return defectCategory;
}

- (DefectCategory *) defectCategoryWithIdentifier:(NSNumber *)defectCategoryId
{
	DefectCategory *defectCategory = nil;
	if (!isEmpty(defectCategoryId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.categoryID = %@", defectCategoryId];
			NSArray* fetchedDefectCategories = [CoreDataHelper getObjectsFromEntity:kDefectCategory
																																		predicate:predicate
																																			sortKey:nil
																																sortAscending:YES
																																			context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([fetchedDefectCategories count] > 0)
			{
				defectCategory = [fetchedDefectCategories firstObject];
			}
		}
		ENDEXCEPTION
	}
	return defectCategory;
}

- (DefectCategory *) defectCategoryWithName:(NSString *)defectCategoryName context:(NSManagedObjectContext *)managedObjectContext
{
	DefectCategory *defectCategory = nil;
	if (!isEmpty(defectCategoryName))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.categoryID LIKE[cd] %@", defectCategoryName];
			NSArray* fetchedDefectCategories = [CoreDataHelper getObjectsFromEntity:kDefectCategory
																																		predicate:predicate
																																			sortKey:nil
																																sortAscending:YES
																																			context:managedObjectContext];
			if([fetchedDefectCategories count] > 0)
			{
				defectCategory = [fetchedDefectCategories firstObject];
			}
		}
		ENDEXCEPTION
	}
	return defectCategory;
}

#pragma mark - Fault Repair Entity(ies) Data Control Methods
- (FaultRepairData *) faultReapirWithID:(NSNumber *)faultRepairId
{
	FaultRepairData *faultRepairData = nil;
	if (!isEmpty(faultRepairId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.faultRepairID = %@",faultRepairId];
			NSArray* faultRepairs = [CoreDataHelper getObjectsFromEntity:kFaultRepairData
																												 predicate:predicate
																													 sortKey:nil
																										 sortAscending:YES
																													 context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([faultRepairs count] > 0)
			{
				faultRepairData = [faultRepairs firstObject];
			}
		}
		ENDEXCEPTION
	}
	return faultRepairData;
}

- (FaultRepairData *) faultRepairDataWithID:(NSNumber *)faultRepairDataId inContext:(NSManagedObjectContext *)managedObjectContext
{
	FaultRepairData *faultRepairData = nil;
	if (!isEmpty(faultRepairDataId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.faultRepairID = %@",faultRepairDataId];
			NSArray* faultRepairDataList = [CoreDataHelper getObjectsFromEntity:kFaultRepairData
																																predicate:predicate
																																	sortKey:nil
																														sortAscending:YES
																																	context:managedObjectContext];
			if([faultRepairDataList count] > 0)
			{
				faultRepairData = [faultRepairDataList firstObject];
			}
		}
		ENDEXCEPTION
	}
	return faultRepairData;
}

- (FaultRepairListHistory *) faultRepairHistoryWithID:(NSNumber *)faultRepairHistoryId forJobData:(NSString *)jobDataId inContext:(NSManagedObjectContext *)managedObjectContext
{
	FaultRepairListHistory *faultRepairHistory = nil;
	if (!isEmpty(faultRepairHistoryId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.faultRepairID = %@ AND SELF.faultRepairListHistoryToJobDataList.jsNumber LIKE[cd] %@",faultRepairHistoryId, jobDataId];
			NSArray* faultRepairHistoryList = [CoreDataHelper getObjectsFromEntity:kFaultRepairListHistory
																																	 predicate:predicate
																																		 sortKey:nil
																															 sortAscending:YES
																																		 context:managedObjectContext];
			if([faultRepairHistoryList count] > 0)
			{
				faultRepairHistory = [faultRepairHistoryList firstObject];
			}
		}
		ENDEXCEPTION
	}
	return faultRepairHistory;
}

- (FaultRepairData *) faultReapirWithDescription:(NSString *)faultRepairDescription
{
	FaultRepairData *faultRepairData = nil;
	if (!isEmpty(faultRepairDescription))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.faultRepairID LIKE[cd] %@",faultRepairDescription];
			NSArray* faultRepairs = [CoreDataHelper getObjectsFromEntity:kFaultRepairData
																												 predicate:predicate
																													 sortKey:nil
																										 sortAscending:YES
																													 context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([faultRepairs count] > 0)
			{
				faultRepairData = [faultRepairs firstObject];
			}
		}
		ENDEXCEPTION
	}
	return faultRepairData;
}

- (JobPauseReason *) jobPauseReasonWithName:(NSString *)jobPauseReason
{
	JobPauseReason *pauseReason = nil;
	if (!isEmpty(jobPauseReason))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.pauseReason LIKE[cd] %@",jobPauseReason];
			NSArray* pauseReasons = [CoreDataHelper getObjectsFromEntity:kJobPauseReason
																												 predicate:predicate
																													 sortKey:nil
																										 sortAscending:YES
																													 context:[PSDatabaseContext sharedContext].managedObjectContext];
			if([pauseReasons count] > 0)
			{
				pauseReason = [pauseReasons firstObject];
			}
		}
		ENDEXCEPTION
	}
	return pauseReason;
}

#pragma mark - Detector Entity(ies) Data Control Methods

- (Detector *)detectorWithIdentifier:(NSNumber *)detectorTypeId forProperty:(NSString *)propertyId context:(NSManagedObjectContext *)managedObjectContext
{
	Detector *detector = nil;
	if (!isEmpty(detectorTypeId) && !isEmpty(propertyId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.detectorTypeId = %@ AND self.propertyID LIKE[cd] %@", detectorTypeId,propertyId];
			NSArray* detectors = [CoreDataHelper getObjectsFromEntity:kDetector
																											predicate:predicate
																												sortKey:nil
																									sortAscending:YES
																												context:managedObjectContext];
			if(!isEmpty(detectors))
			{
				detector = [detectors firstObject];
			}
		}
		ENDEXCEPTION
	}
	return detector;
}

#pragma mark - Appliance Inspection Data Control Methods

- (ApplianceInspection *) applianceInspectionWithInspectionIdAndApplianceId:(NSNumber *)applianceId inspectionId:(NSNumber *)inspectionId managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	ApplianceInspection *applianceInspection = nil;
	if (!isEmpty(applianceId) && !isEmpty(inspectionId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.inspectionID = %@ AND SELF.appliance.applianceID = %@",inspectionId,applianceId];
			NSArray* fetchedAppliances = [CoreDataHelper getObjectsFromEntity:kApplianceInspection
																															predicate:predicate
																																sortKey:nil
																													sortAscending:YES
																																context:managedObjectContext];
			if (!isEmpty(fetchedAppliances))
			{
				applianceInspection = [fetchedAppliances firstObject];
			}
		}
		ENDEXCEPTION
	}
	
	return applianceInspection;
}

#pragma mark - Detector Inspection Data Control Methods

- (DetectorInspection *) detectorInspectionWithInspectionId:(NSNumber *)inspectionId detectorId:(NSNumber *)detectorId propertyId:(NSString *)propertyId managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	DetectorInspection *detectorInspection = nil;
	if (!isEmpty(detectorId) && !isEmpty(inspectionId) && !isEmpty(propertyId))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.inspectionID = %@ AND SELF.detector.detectorTypeId = %@ AND SELF.detector.detectorToProperty.propertyId LIKE[cd] %@",inspectionId,detectorId,propertyId];
			
			
			NSArray* fetchedInspections = [CoreDataHelper getObjectsFromEntity:kDetectorInspection
																															 predicate:predicate
																																 sortKey:nil
																													 sortAscending:YES
																																 context:managedObjectContext];
			if ([fetchedInspections count] > 0)
			{
				detectorInspection = [fetchedInspections firstObject];
			}
		}
		ENDEXCEPTION
	}
	
	return detectorInspection;
}

#pragma mark - Repair Images Methods

- (BOOL) isRepairCompletionImageExists:(JobDataList *)jobData
{
	BOOL isRepairCompletionImage = NO;
	
	if (!isEmpty(jobData))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isBeforeImage = %@",0];
			
			
			NSArray* fetchedImages = [[jobData.jobDataListToRepairImages filteredSetUsingPredicate:predicate] allObjects];
			if ([fetchedImages count] > 0)
			{
				isRepairCompletionImage = YES;
			}
		}
		ENDEXCEPTION
	}
	
	return isRepairCompletionImage;
}



- (JobDataList *) jobDataWithJSN:(NSString *)JSNumber inContext:(NSManagedObjectContext *)managedObjectContext
{
	JobDataList *jobDataList = nil;
	if (isEmpty(managedObjectContext))
	{
		managedObjectContext = [PSDatabaseContext sharedContext].managedObjectContext;
		
	}
	if (!isEmpty(JSNumber))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.jsNumber = %@", JSNumber];
			NSArray* fetchedJobs = [CoreDataHelper getObjectsFromEntity:kJobDataList
																												predicate:predicate
																													sortKey:nil
																										sortAscending:YES
																													context:managedObjectContext];
			if ([fetchedJobs count] > 0)
			{
				jobDataList = [fetchedJobs firstObject];
			}
		}
		ENDEXCEPTION
	}
	
	return jobDataList;
}

- (NSArray *)repairPictureListWithJSNumber:(NSString*)jsNumber beforeImage:(NSNumber*)isBeforeImage
{
	
	NSArray* repairPictures = nil;
	if (!isEmpty(jsNumber))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.repairImagesToJobDataList.jsNumber = %@ AND self.isBeforeImage = %@",jsNumber,isBeforeImage];
			repairPictures = [CoreDataHelper getObjectsFromEntity:kRepairPictures
																									predicate:predicate
																										sortKey:nil
																							sortAscending:YES
																										context:[PSDatabaseContext sharedContext].managedObjectContext];
			
		}
		ENDEXCEPTION
	}
	return repairPictures;
}

- (NSArray *)repairPictureAfterAndBeforeListWithJSNumber:(NSString*)jsNumber
{
	
	NSArray* repairPictures = nil;
	if (!isEmpty(jsNumber))
	{
		STARTEXCEPTION
		@autoreleasepool {
			NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.repairImagesToJobDataList.jsNumber = %@",jsNumber];
			repairPictures = [CoreDataHelper getObjectsFromEntity:kRepairPictures
																									predicate:predicate
																										sortKey:nil
																							sortAscending:YES
																										context:[PSDatabaseContext sharedContext].managedObjectContext];
			
		}
		ENDEXCEPTION
	}
	return repairPictures;
}
@end
