//
//  PSCoreDataManager.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 27/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSCoreDataManager : NSObject

+ (PSCoreDataManager *) sharedManager;

#pragma mark - User Entity Data Control Methods
- (User *) userWithIdentifier:(NSNumber *)userId;
-(User *) userWithUserName:(NSString *)userName;
- (BOOL) isUserExistsWithIdentifier:(NSNumber *)userId;
-(BOOL) authenticateUserOffline:(NSString *)userName password:(NSString *)password;

#pragma mark - Appointment Entity Data Control Methods
- (Appointment *) appointmentWithIdentifier:(NSNumber *)appointmentId context:(NSManagedObjectContext *)managedObjectContext;
- (Appointment *) appointmentWithIdentifier:(NSNumber *)appointmentId propertyId:(NSNumber *)propertyId;
- (Appointment *) appointmentWithPropertyIdentifier:(NSNumber *)propertyId appointmentId:(NSNumber *)appointmentId;

#pragma mark - Property Entity Data Control Methods
- (Property *)propertyWithIdentifier:(NSString *)propertyId;
- (Property *)propertyWithIdentifier:(NSString *)propertyId context:(NSManagedObjectContext *)managedObjectContext;

#pragma mark - Property Picture Entity Data Control Methods
- (PropertyPicture *)propertyPictureWithIdentifier:(NSNumber *)propertyPictureId;
- (PropertyPicture *)propertyPictureWithIdentifier:(NSNumber *)propertyPictureId context:(NSManagedObjectContext *)managedObjectContext;
- (NSArray *) propertyPicturesWithPropertyIdentifier:(NSString *)propertyId appointmentId:(NSNumber *)appointmentId itemId:(NSNumber*) itemId andHeatingId:(NSString *) heatingId;
#pragma mark - Customer Entity Data Control Methods
- (Customer *) customerWithIdentifier:(NSNumber *)customerId;
- (Customer *) customerWithIdentifier:(NSNumber *)customerId context:(NSManagedObjectContext *)managedObjectContext;

#pragma mark - Survyeor Entity Data Control Methods
- (Surveyor *) surveyorWithIdentifier:(NSNumber *)surveyorId;
- (BOOL) isSurveyorExistsWithIdentifier:(NSNumber *)surveyorId;
- (Surveyor *)fetchLoggedInSurveyor;

#pragma mark - Property Entity Data Control Methods
- (SProperty *) sPropertyWithIdentifier:(NSString *)propertyId;
- (BOOL) isPropertyExistsWithIdentifier:(NSString *)propertyId;


#pragma mark - Appliance Entity(ies) Data Control Methods
- (Appliance *) applianceWithIdentifier:(NSNumber *)applianceId inContext:(NSManagedObjectContext *)managedObjectContext;
- (ApplianceLocation *) applianceLocationWithName:(NSString *)locationName inContext:(NSManagedObjectContext*)managedObjectContext;
- (ApplianceManufacturer *) applianceManufacturerWithName:(NSString *)manufacturerName inContext:(NSManagedObjectContext*)managedObjectContext;
- (ApplianceModel *) applianceModelWithName:(NSString *)modelName inContext:(NSManagedObjectContext*)managedObjectContext;
- (ApplianceType *) applianceTypeWithName:(NSString *)typeName inContext:(NSManagedObjectContext*)managedObjectContext;

- (ApplianceLocation *) applianceLocationWithIdentifier:(NSNumber *)locationID;
- (ApplianceManufacturer *) applianceManufacturerWithIdentifier:(NSNumber *)manufacturerID;
- (ApplianceModel *) applianceModelWithIdentifier:(NSNumber *)modelID;
- (ApplianceType *) applianceTypeWithIdentifier:(NSNumber *)typeID;

#pragma mark - Defect Entity(ies) Data Control Methods
- (DefectCategory *) defectCategoryWithIdentifier:(NSNumber *)defectCategoryId;
- (DefectCategory *) defectCategoryWithIdentifier:(NSNumber *)defectCategoryId context:(NSManagedObjectContext *)managedObjectContext;
- (DefectCategory *) defectCategoryWithName:(NSString *)defectCategoryName context:(NSManagedObjectContext *)managedObjectContext;
- (Defect *) defectWithIdentifier:(NSNumber *)defectId context:(NSManagedObjectContext *)managedObjectContext;

- (DefectPicture *)defectPictureWithIdentifier:(NSNumber *)propertyPictureId;

- (NSArray *)defectPictureListWithIdentifier:(NSNumber *)propertyPictureId;
- (NSArray *)defectPictureListWithIdentifier:(NSNumber *)propertyPictureId andJournalID:(NSNumber*)journalID;

#pragma mark - Fault Repair Entity(ies) Data Control Methods
- (FaultRepairData *) faultReapirWithID:(NSNumber *)faultRepairId;
- (FaultRepairData *) faultRepairDataWithID:(NSNumber *)faultRepairDataId inContext:(NSManagedObjectContext *)managedObjectContext;
- (FaultRepairListHistory *) faultRepairHistoryWithID:(NSNumber *)faultRepairHistoryId forJobData:(NSString *)jobDataId inContext:(NSManagedObjectContext *)managedObjectContext;
- (FaultRepairData *) faultReapirWithDescription:(NSString *)faultRepairDescription;
- (JobPauseReason *) jobPauseReasonWithName:(NSString *)jobPauseReason;

#pragma mark - Detector Entity(ies) Data Control Methods
- (Detector *)detectorWithIdentifier:(NSNumber *)detectorTypeId forProperty:(NSString *)propertyId context:(NSManagedObjectContext *)managedObjectContext;

#pragma mark - Appliance Inspection Data Control Methods
- (ApplianceInspection *) applianceInspectionWithInspectionIdAndApplianceId:(NSNumber *)applianceId inspectionId:(NSNumber *)inspectionId managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

#pragma mark - Detector Inspection Data Control Methods
- (DetectorInspection *) detectorInspectionWithInspectionId:(NSNumber *)inspectionId detectorId:(NSNumber *)detectorId propertyId:(NSString *)propertyId managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

#pragma mark - Repair Images Methods
- (BOOL) isRepairCompletionImageExists:(JobDataList *)jobData;
- (JobDataList *) jobDataWithJSN:(NSString *)JSNumber inContext:(NSManagedObjectContext *)managedObjectContext;
- (NSArray *)repairPictureListWithJSNumber:(NSString*)jsNumber beforeImage:(NSNumber*)isBeforeImage;
- (NSArray *)repairPictureAfterAndBeforeListWithJSNumber:(NSString*)jsNumber;

#pragma mark - Generic Record Fetching Methods
- (id) findRecordFrom:(NSString *)table Field:(NSString *)field WithValue:(id)value context:(NSManagedObjectContext *)dbContext;
- (id) findRecordFrom:(NSString *)table Field1:(NSString *)field1 WithValue1:(id)value1 AndField2:(NSString *)field2 WithValue2:(id)value2 context:(NSManagedObjectContext *)dbContext;

@end
