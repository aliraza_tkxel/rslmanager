//
//  FaultRepairData+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 16/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "FaultRepairData+JSON.h"

@implementation FaultRepairData (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *faultRepairDictionary = [NSMutableDictionary dictionary];
    [faultRepairDictionary setObject:isEmpty(self.faultRepairID)?[NSNull null]:self.faultRepairID forKey:kFaultRepairID];
    [faultRepairDictionary setObject:isEmpty(self.faultRepairDescription)?[NSNull null]:self.faultRepairDescription forKey:kFaultRepairDescription];
    return faultRepairDictionary;
}
@end
