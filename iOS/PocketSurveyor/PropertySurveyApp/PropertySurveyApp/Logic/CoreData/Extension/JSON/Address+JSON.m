//
//  Address+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Address+JSON.h"

@implementation Address (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *addressDictionary = [NSMutableDictionary dictionary];
    
    [addressDictionary setObject:isEmpty(self.address)?[NSNull null]:self.address forKey:@"address"];
    [addressDictionary setObject:isEmpty(self.addressID)?[NSNull null]:self.addressID forKey:@"addressID"];
    [addressDictionary setObject:isEmpty(self.houseNo)?[NSNull null]:self.houseNo forKey:@"houseNo"];
    [addressDictionary setObject:isEmpty(self.postCode)?[NSNull null]:self.postCode forKey:@"postCode"];
    [addressDictionary setObject:isEmpty(self.townCity)?[NSNull null]:self.townCity forKey:@"townCity"];
    
    return addressDictionary;
}
@end
