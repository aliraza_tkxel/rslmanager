//
//  Journal+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Journal.h"

@interface Journal (JSON)

- (NSDictionary *) JSONValue;
@end
