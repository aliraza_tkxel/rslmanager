//
//  InstallationPipework+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "InstallationPipework+JSON.h"

@implementation InstallationPipework (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *pipeWorkDictionary = [NSMutableDictionary dictionary];
    NSString *inspectionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.inspectionDate])?nil:[UtilityClass convertNSDateToServerDate:self.inspectionDate];
    
    [pipeWorkDictionary setObject:isEmpty(self.emergencyControl)?[NSNull null]:self.emergencyControl forKey:kEmergencyControl];
    [pipeWorkDictionary setObject:isEmpty(self.equipotentialBonding)?[NSNull null]:self.equipotentialBonding forKey:kEquipotentialBonding];
    [pipeWorkDictionary setObject:isEmpty(self.gasTightnessTest)?[NSNull null]:self.gasTightnessTest forKey:kGasTightnessTest];
    [pipeWorkDictionary setObject:isEmpty(self.visualInspection)?[NSNull null]:self.visualInspection forKey:KVisualInspection];
    [pipeWorkDictionary setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kIsInspected];
    [pipeWorkDictionary setObject:isEmpty(inspectionDate)?[NSNull null]:inspectionDate forKey:kInspectionDate];
    [pipeWorkDictionary setObject:isEmpty(self.inspectionID)?[NSNull null]:self.inspectionID forKey:kInspectionId];
    return pipeWorkDictionary;
}
@end
