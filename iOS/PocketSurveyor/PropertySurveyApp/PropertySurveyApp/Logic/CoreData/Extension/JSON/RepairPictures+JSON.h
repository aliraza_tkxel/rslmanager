//
//  RepairPictures+JSON.h
//  PropertySurveyApp
//
//  Created by TkXel on 09/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "RepairPictures.h"

@interface RepairPictures (JSON)<PSUploadRepairPictureServiceDelegate> {
    
}

-(void) uploadRepairPicture:(NSDictionary*) pictureDictionary withImage:propertyImage;

- (NSDictionary *) JSONValue;
-(void) uploadPicture;
-(NSString *) getPicturePath;

@end
