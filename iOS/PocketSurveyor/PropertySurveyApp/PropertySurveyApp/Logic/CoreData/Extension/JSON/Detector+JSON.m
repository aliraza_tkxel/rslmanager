//
//  Detector+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 26/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Detector+JSON.h"
#import "Defect+JSON.h"
#import "DetectorInspection+JSON.h"

@implementation Detector (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *detectorDictionary = [NSMutableDictionary dictionary];
    
    [detectorDictionary setObject:isEmpty(self.detectorId)?[NSNull null]:self.detectorId forKey:kDetectorId];
    [detectorDictionary setObject:isEmpty(self.detectorType)?[NSNull null]:self.detectorType forKey:kDetectorTypeDesc];
    [detectorDictionary setObject:isEmpty(self.detectorTypeId)?[NSNull null]:self.detectorTypeId forKey:kDetectorTypeId];
    [detectorDictionary setObject:isEmpty(self.installedBy)?[NSNull null]:self.installedBy forKey:kDetectorInstalledBy];
    [detectorDictionary setObject:isEmpty(self.isLandlordsDetector)?[NSNull null]:self.isLandlordsDetector forKey:kIsLandlordsDetector];
    [detectorDictionary setObject:isEmpty(self.location)?[NSNull null]:self.location forKey:kLocation];
    [detectorDictionary setObject:isEmpty(self.manufacturer)?[NSNull null]:self.manufacturer forKey:kManufacturer];
    [detectorDictionary setObject:isEmpty(self.powerTypeId)?[NSNull null]:self.powerTypeId forKey:kPowerTypeId];
    [detectorDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [detectorDictionary setObject:isEmpty(self.serialNumber)?[NSNull null]:self.serialNumber forKey:kSerialNumber];
    [detectorDictionary setObject:isEmpty(self.installedDate)?[NSNull null]:[UtilityClass convertNSDateToServerDate:self.installedDate] forKey:kInstalledDate];
    
    
    [detectorDictionary setObject:isEmpty(self.detectorCount)?[NSNull null]:self.detectorCount forKey:kDetectorCount];
    [detectorDictionary setObject:isEmpty(self.isInspected)?[NSNull null]:self.isInspected forKey:kApplianceIsInspected];
    
    NSMutableArray *detectorDefectsArray = [NSMutableArray array];
    for (Defect *defect in self.detectorDefects)
    {
        [detectorDefectsArray addObject:[defect JSONValue]];
    }
    
    NSDictionary *detectorInspectionDictionary = [NSDictionary dictionaryWithDictionary:[self.detectorInspection JSONValue]];
    
    [detectorDictionary setObject:detectorDefectsArray forKey:@"detectorDefects"];
    [detectorDictionary setObject:detectorInspectionDictionary forKey:@"detectorInspection"];
    
    return detectorDictionary;
}
@end
