//
//  Survey+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Survey+CoreDataClass.h"

@interface Survey (JSON)

- (NSDictionary *) JSONValue;
@end
