//
//  JobPauseReason+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 16/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobPauseReason.h"

@interface JobPauseReason (JSON)
- (NSDictionary *) JSONValue;
@end
