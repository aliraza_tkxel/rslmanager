//
//  Customer+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Customer+JSON.h"
#import "CustomerVulnerabilityData+JSON.h"
#import "CustomerRiskData+JSON.h"
#import "Address+JSON.h"

@implementation Customer (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *customerDictionary = [NSMutableDictionary dictionary];
    [customerDictionary setObject:isEmpty(self.address)?[NSNull null]:self.address forKey:kCustomerAddress];
    [customerDictionary setObject:isEmpty(self.customerId)?[NSNull null]:self.customerId forKey:kCustomerId];
    [customerDictionary setObject:isEmpty(self.email)?[NSNull null]:self.email forKey:kCustomerEmail];
    [customerDictionary setObject:isEmpty(self.fax)?[NSNull null]:self.fax forKey:kCustomerFax];
    [customerDictionary setObject:isEmpty(self.firstName)?[NSNull null]:self.firstName forKey:kCustomerFirstName];
    [customerDictionary setObject:isEmpty(self.lastName)?[NSNull null]:self.lastName forKey:kCustomerLastName];
    [customerDictionary setObject:isEmpty(self.middleName)?[NSNull null]:self.middleName forKey:kCustomerMiddleName];
    [customerDictionary setObject:isEmpty(self.mobile)?[NSNull null]:self.mobile forKey:kCustomerMobile];
    [customerDictionary setObject:isEmpty(self.property)?[NSNull null]:self.property forKey:kCustomerProperty];
    [customerDictionary setObject:isEmpty(self.telephone)?[NSNull null]:self.telephone forKey:kCustomerTelephone];
    [customerDictionary setObject:isEmpty(self.title)?[NSNull null]:self.title forKey:kCustomerTitle];
    
    
    NSMutableArray *customerVulnerabilityArray = [NSMutableArray array];
    for (CustomerVulnerabilityData *vulnerabilityData in self.customerToCustomerVulunarityData)
    {
        [customerVulnerabilityArray addObject:[vulnerabilityData JSONValue]];
    }
    NSMutableArray *customerRiskArray = [NSMutableArray array];
    for (CustomerRiskData *customerRisk in self.customerToCustomerRiskData)
    {
        [customerRiskArray addObject:[customerRisk JSONValue]];
    }
    NSDictionary *customerAddressDictionary = [NSDictionary dictionaryWithDictionary:[self.customerToAddress JSONValue]];
    
    [customerDictionary setObject:customerVulnerabilityArray forKey:@"customerVulnerabilityData"];
    [customerDictionary setObject:customerRiskArray forKey:@"customerRiskData"];
    [customerDictionary setObject:customerAddressDictionary forKey:@"Address"];
    
    return customerDictionary;
}
@end
