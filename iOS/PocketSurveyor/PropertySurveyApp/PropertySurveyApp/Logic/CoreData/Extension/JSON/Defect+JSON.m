//
//  Defect+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Defect+JSON.h"
#import "DefectCategory+JSON.h"
@implementation Defect (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *defectDictionary = [NSMutableDictionary dictionary];
    
    NSString *defectDate = isEmpty([UtilityClass convertNSDateToServerDate:self.defectDate])?nil:[UtilityClass convertNSDateToServerDate:self.defectDate];
    [defectDictionary setObject:isEmpty(self.applianceID)?[NSNull null]:self.applianceID forKey:kApplianceId];
    [defectDictionary setObject:isEmpty(self.detectorTypeId)?[NSNull null]:self.detectorTypeId forKey:kDetectorTypeId];
    [defectDictionary setObject:isEmpty(defectDate)?[NSNull null]:defectDate forKey:kDefectDate];
    [defectDictionary setObject:isEmpty(self.defectDescription)?[NSNull null]:self.defectDescription forKey:kDefectDescription];
    [defectDictionary setObject:isEmpty(self.defectID)?[NSNull null]:self.defectID forKey:kDefectID];
    [defectDictionary setObject:isEmpty(self.faultCategory)?[NSNull null]:self.faultCategory forKey:kFaultCategory];
    [defectDictionary setObject:isEmpty(self.isActionTaken)?[NSNull null]:self.isActionTaken forKey:kIsActionTaken];
    [defectDictionary setObject:isEmpty(self.isAdviceNoteIssued)?[NSNull null]:self.isAdviceNoteIssued forKey:kisAdviceNoteIssued];
    [defectDictionary setObject:isEmpty(self.isDefectIdentified)?[NSNull null]:self.isDefectIdentified forKey:kIsDefectIdentified];
    [defectDictionary setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:@"JournalId"];
    [defectDictionary setObject:isEmpty(self.propertyId)?[NSNull null]:self.propertyId forKey:kPropertyId];
    [defectDictionary setObject:isEmpty(self.remedialAction)?[NSNull null]:self.remedialAction forKey:kRemedialAction];
    [defectDictionary setObject:isEmpty(self.serialNo)?[NSNull null]:self.serialNo forKey:kSerialNo];
    [defectDictionary setObject:isEmpty(self.warningTagFixed)?[NSNull null]:self.warningTagFixed forKey:kWarningTagFixed];
    
    /* NSDictionary *defectCategoryDictionary = [NSDictionary dictionaryWithDictionary:[self.defectCategory JSONValue]];
     [defectDictionary setObject:defectCategoryDictionary forKey:@"defectcategory"];*/
    return defectDictionary;
}

-(void)updateDefectValuesWithDictionary:(NSDictionary*)defectDictionary
{
    NSDictionary * parameterDictionary = [defectDictionary objectForKey:kResponseTag];
    [[PSDataPersistenceManager sharedManager] updateDefectObject:self withDictionary:parameterDictionary];
}

-(void)saveDefectOnServer
{
    NSMutableDictionary * parameterDict = [[NSMutableDictionary alloc] init];
    [parameterDict setObject:[self JSONValue] forKey:@"defect"];
    [parameterDict setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
    [parameterDict setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
    [PSApplianceManager sharedManager].saveDefectService.defectDelegate = self;
    [[PSApplianceManager sharedManager] saveApplianceDefect:parameterDict];
}

#pragma mark PSSaveDefectServiceDelegate
- (void) service:(PSSaveDefectService *)service didReceiveDefectDataAfterSuccess:(id)surveyorData {
    
    NSString * responseString=surveyorData;
    
    if(!isEmpty(responseString))
    {
        NSDictionary *JSONDictionary = [responseString JSONValue];
        [self updateDefectValuesWithDictionary:JSONDictionary];
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineDefectsSaveNotificationSuccess object:nil];
        return;
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineDefectsSaveNotificationFail object:nil];
        return;
    }
}

- (void) service:(PSSaveDefectService *)service didFailWithError:(NSError *)error {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineDefectsSaveNotificationFail object:error];
}

@end