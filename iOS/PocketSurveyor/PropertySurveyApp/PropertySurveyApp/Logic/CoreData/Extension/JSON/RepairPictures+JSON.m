//
//  RepairPictures+JSON.m
//  PropertySurveyApp
//
//  Created by TkXel on 09/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "RepairPictures+JSON.h"
#import "RepairPictures+MWPhoto.h"
#import "Scheme+JSON.h"

@implementation RepairPictures (JSON)

-(NSString *) getPicturePath
{
    if (self.imagePath) {
        
        return self.imagePath;
    }
    
    return [NSString stringWithFormat:@"%@-%@",[self.syncStatus stringValue],[UtilityClass stringFromDate:self.createdOn dateFormat:@"HH-mm-ss"]];
}

-(void) uploadPicture
{
    self.repairImage = [[self underlyingImage] scaleToSize:CGSizeMake(500, 500)];
    [PSPropertyPictureManager sharedManager].uploadRepairPhotographService.photographDelegate = self;
    [[PSPropertyPictureManager sharedManager].uploadRepairPhotographService uploadPhotograph:[self getJSON] streamLocation:self.repairImage];
}

-(void) uploadRepairPicture:(NSDictionary*) pictureDictionary withImage:(UIImage*) propertyImage
{
    
    [self setRepairImage:[propertyImage scaleToSize:CGSizeMake(500, 500)]];
    
    FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
    [manager storeImage:self.repairImage forKey:[self getPicturePath]];
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kRepairPictureSaveNotification object:self];
    
//    PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
//    if(delegate.postingDataObjectAppointments==0)
//    {
//        if([PSPropertyPictureManager sharedManager].isPostingImage==NO)
//        {
//            [PSPropertyPictureManager sharedManager].isPostingImage=YES;
//            [PSPropertyPictureManager sharedManager].uploadRepairPhotographService.photographDelegate = self;
//            [[[PSPropertyPictureManager sharedManager] uploadRepairPhotographService] uploadPhotograph:pictureDictionary streamLocation:propertyImage];
//        }
//    }
}

-(NSMutableDictionary*) getJSON
{
    NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
    
    if (self.image) {
        
        [requestParameters setObject:self.image forKey:kRepairImage];
    }
    
    BOOL  isBefore = YES;
    if(self.isBeforeImage)
    {
        if([self.isBeforeImage intValue]==0)
        {
            isBefore=NO;
        }
    }
    else
    {
        if (/*self.isJobComplete ||*/ [self.repairImagesToJobDataList.jobStatus isEqualToString:kJobStatusComplete])
        {
            isBefore = NO;
        }
    }
    
    if(self.repairImagesToJobDataList.jobDataListToAppointment.appointmentToProperty)
    {
        [requestParameters setObject:self.repairImagesToJobDataList.jobDataListToAppointment.appointmentToProperty.propertyId forKey:kPropertyId];
        [requestParameters setObject:@"0" forKey:kSchemeBlockID];//its service requirement
        [requestParameters setObject:@"0" forKey:kSchemeID];//its service requirement
    }
    else
    {
        [requestParameters setObject:self.repairImagesToJobDataList.jobDataListToAppointment.appointmentToScheme.schemeId forKey:kSchemeID];
        if(!isEmpty(self.repairImagesToJobDataList.jobDataListToAppointment.appointmentToScheme.blockName))
        {
            [requestParameters setObject:self.repairImagesToJobDataList.jobDataListToAppointment.appointmentToScheme.blockId forKey:kSchemeBlockID];
        }
        else
        {
            [requestParameters setObject:@"0" forKey:kSchemeBlockID];
        }
        [requestParameters setObject:@"" forKey:kPropertyId]; //its service requirement
        
    }
    [requestParameters setObject:self.repairImagesToJobDataList.jsNumber forKey:kRepairJSNumber];
    [requestParameters setObject:@".jpg" forKey:kFileExtension];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kCreatedBy];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
    [requestParameters setValue:isBefore?@"TRUE":@"FALSE" forKey:kIsBeforeImage];
    [requestParameters setObject:self.imageIdentifier forKey:kUniqueImageIdentifier];
    
    
    return requestParameters;
    
}

#pragma mark PSUploadRepairPictureServiceDelegate

- (void) service:(PSUploadRepairPictureService *)service didReceivePhotographData:(id)surveyorData
{
    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    NSString * responseString=surveyorData;
    if(!isEmpty(responseString))
    {
        NSDictionary *JSONDictionary = [responseString JSONValue];
        
        [[PSDataUpdateManager sharedManager] updateRepairPicture:self forDictionary:[JSONDictionary objectForKey:@"response"]];
        
        if (self.repairImage) {
            
            FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
            
            [manager storeImage:self.repairImage forKey:[[JSONDictionary objectForKey:@"response"] objectForKey:kRepairImagePath]];
        }
        
        NSDictionary * dict=[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:1] forKey:kRepairImageSyncStatus];
        
        [[PSDataUpdateManager sharedManager] updateRepairPicture:self forDictionary:dict];
        self.repairImage=nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:nil];
    }
    
}

- (void) service:(PSUploadPhotographService *)service didFailWithError:(NSError *)error {
    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    self.repairImage=nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:error];
}

@end
