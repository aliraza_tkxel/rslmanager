//
//  JobPauseData+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 16/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobPauseData+JSON.h"
#import "JobPauseReason+JSON.h"

@implementation JobPauseData (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *jobPauseDataDictionary = [NSMutableDictionary dictionary];
    NSString *pauseDate = isEmpty([UtilityClass convertNSDateToServerDate:self.pauseDate])?nil:[UtilityClass convertNSDateToServerDate:self.pauseDate];
    [jobPauseDataDictionary setObject:isEmpty(self.actionType)?[NSNull null]:self.actionType forKey:@"actionType"];
    [jobPauseDataDictionary setObject:isEmpty(pauseDate)?[NSNull null]:pauseDate forKey:@"pauseDate"];
    [jobPauseDataDictionary setObject:isEmpty(self.pauseNote)?[NSNull null]:self.pauseNote forKey:@"pauseNote"];
    if(self.pauseReason)
    {
        [jobPauseDataDictionary setObject:isEmpty(self.pauseReason.pauseReason)?[NSNull null]:self.pauseReason.pauseReason forKey:@"pauseReason"];
    }
    else
    {
        [jobPauseDataDictionary setObject:[NSNull null] forKey:@"pauseReason"];
    }
    [jobPauseDataDictionary setObject:isEmpty(self.pausedBy)?[NSNull null]:self.pausedBy forKey:@"pausedBy"];
    /* NSDictionary *pauseReasonDictionary = [NSDictionary dictionaryWithDictionary:[self.pauseReason JSONValue]];
     [jobPauseDataDictionary setObject:pauseReasonDictionary forKey:@"pauseReason"];*/
    return jobPauseDataDictionary;
}
@end
