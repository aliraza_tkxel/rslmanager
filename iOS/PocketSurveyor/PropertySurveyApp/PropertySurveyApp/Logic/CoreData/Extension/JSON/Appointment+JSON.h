//
//  Appointment+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Appointment+CoreDataClass.h"

@interface Appointment (JSON)

- (NSDictionary *) JSONValue;


#pragma mark - Methods
- (void) startAppointment;
- (void) completeAppointment;
- (BOOL) isPreparedForOffline;
- (AppointmentStatus) getStatus;
- (AppointmentType) getType;
- (Customer *) defaultTenant;
- (NSString *) stringWithAppointmentStatus:(AppointmentStatus)status;
- (NSString *) appointmentDateWithStyle:(AppointmentDateStyle)style dateFormat:(NSString *)dateFormat;
-(AppointmentSyncStatus) getSyncStatus;

@end
