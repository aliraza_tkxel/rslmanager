//
//  DefectPicture+JSON.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 01/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "DefectPicture.h"
#import "PSPropertyPictureManager.h"
@interface DefectPicture (JSON)<PSUploadDefectPictureServiceDelegate>
-(void) uploadPropertyPicture:(NSDictionary*) pictureDictionary withImage:(UIImage*) propertyImage;

-(NSString *) getPicturePath;

-(void) uploadPicture;
@end
