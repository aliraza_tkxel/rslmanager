//
//  JobDataList+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobDataList.h"

@interface JobDataList (JSON)

- (NSDictionary *) JSONValue;
@end
