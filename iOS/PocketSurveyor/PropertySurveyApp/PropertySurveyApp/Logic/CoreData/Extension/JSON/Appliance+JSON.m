////
//  Appliance+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Appliance+JSON.h"
#import "ApplianceLocation+JSON.h"
#import "ApplianceType+JSON.h"
#import "ApplianceManufacturer+JSON.h"
#import "ApplianceModel+JSON.h"
#import "ApplianceInspection+JSON.h"
#import "Defect+JSON.h"

@implementation Appliance (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *applianceDictionary = [NSMutableDictionary dictionary];
    
    NSString *installationDate = isEmpty([UtilityClass convertNSDateToServerDate:self.installedDate])?nil:[UtilityClass convertNSDateToServerDate:self.installedDate];
    NSString *replacementDate = isEmpty([UtilityClass convertNSDateToServerDate:self.replacementDate])?nil:[UtilityClass convertNSDateToServerDate:self.replacementDate];
    
    [applianceDictionary setObject:isEmpty(self.fluType)?[NSNull null]:self.fluType forKey:kApplianceFlueType];
    [applianceDictionary setObject:isEmpty(installationDate)?[NSNull null]:installationDate forKey:kApplianceInstalledDate];
    [applianceDictionary setObject:isEmpty(replacementDate)?[NSNull null]:replacementDate forKey:kApplianceReplacementDate];
    [applianceDictionary setObject:isEmpty(self.serialNumber)?[NSNull null]:self.serialNumber forKey:kApplianceSerialNumber];
    [applianceDictionary setObject:isEmpty(self.gcNumber)?[NSNull null]:self.gcNumber forKey:kApplianceGCNumber];
    [applianceDictionary setObject:isEmpty(self.isLandlordAppliance)?[NSNull null]:self.isLandlordAppliance forKey:kApplianceIsLandlordAppliance];
    [applianceDictionary setObject:isEmpty(self.applianceID)?[NSNull null]:self.applianceID forKey:kApplianceId];
    [applianceDictionary setObject:isEmpty(self.isInspected)?[NSNumber numberWithBool:NO]:self.isInspected forKey:kApplianceIsInspected];
    [applianceDictionary setObject:isEmpty(self.propertyID)?[NSNull null]:self.propertyID forKey:kPropertyId];
    
    NSDictionary *applianceLocationDictionary = [NSDictionary dictionaryWithDictionary:[self.applianceLocation JSONValue]];
    NSDictionary *applianceManufecturerDictionary = [NSDictionary dictionaryWithDictionary:[self.applianceManufacturer JSONValue]];
    NSDictionary *applianceModelDictionary = [NSDictionary dictionaryWithDictionary:[self.applianceModel JSONValue]];
    NSDictionary *applianceTypeDictionary = [NSDictionary dictionaryWithDictionary:[self.applianceType JSONValue]];
    NSDictionary *applianceInspectionDictionary = [NSDictionary dictionaryWithDictionary:[self.applianceInspection JSONValue]];

    
    NSMutableArray *applianceDefectsArray = [NSMutableArray array];
    for (Defect *defect in self.applianceDefects)
    {
        [applianceDefectsArray addObject:[defect JSONValue]];
    }
    
    [applianceDictionary setObject:applianceLocationDictionary forKey:kApplianceLocation];
    [applianceDictionary setObject:applianceManufecturerDictionary forKey:kApplianceManufacturer];
    [applianceDictionary setObject:applianceModelDictionary forKey:kApplianceModel];
    [applianceDictionary setObject:applianceTypeDictionary forKey:kApplianceType];
    [applianceDictionary setObject:applianceInspectionDictionary forKey:kApplianceInspection];
    [applianceDictionary setObject:applianceDefectsArray forKey:kApplianceDefects];
    
    return applianceDictionary;
}
@end
