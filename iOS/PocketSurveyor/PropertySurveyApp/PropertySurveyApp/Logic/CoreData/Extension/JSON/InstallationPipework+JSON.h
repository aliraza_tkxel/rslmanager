//
//  InstallationPipework+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "InstallationPipework.h"

@interface InstallationPipework (JSON)
- (NSDictionary *) JSONValue;
@end
