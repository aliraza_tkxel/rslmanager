//
//  Appointment+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Appointment+JSON.h"
#import "Customer+JSON.h"
#import "Property+JSON.h"
#import "AppInfoData+JSON.h"
#import "CP12Info+JSON.h"
#import "JobDataList+JSON.h"
#import "Journal+JSON.h"
#import "Survey+JSON.h"
#import "Appointment+CoreDataClass.h"
#import "Scheme+JSON.h"

@implementation Appointment (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *appointmentDictionary = [NSMutableDictionary dictionary];
    
    NSString *appointmentDate =  isEmpty([UtilityClass convertNSDateToServerDate:self.appointmentDate])?nil:[UtilityClass convertNSDateToServerDate:self.appointmentDate];
    NSString *appointmentStartTime =  isEmpty([UtilityClass convertNSDateToServerDate:self.appointmentStartTime])?nil:[UtilityClass convertNSDateToServerDate:self.appointmentStartTime];
    NSString *appointmentEndTime =  isEmpty([UtilityClass convertNSDateToServerDate:self.appointmentEndTime])?nil:[UtilityClass convertNSDateToServerDate:self.appointmentEndTime];
    NSString *loggedDate =  isEmpty([UtilityClass convertNSDateToServerDate:self.loggedDate])?nil:[UtilityClass convertNSDateToServerDate:self.loggedDate];
    NSString *repairCompletionDateTime =  isEmpty([UtilityClass convertNSDateToServerDate:self.repairCompletionDateTime])?nil:[UtilityClass convertNSDateToServerDate:self.repairCompletionDateTime];
    
    id surveyCheckListForm = nil;
    /*
     NSString * surveyJSON = [appointmentData valueForKey:kSurveyFormJSON];*/
    
    [appointmentDictionary setObject:isEmpty(appointmentDate)?[NSNull null]:appointmentDate forKey:kAppointmentDate];
    [appointmentDictionary setObject:isEmpty(appointmentStartTime)?[NSNull null]:appointmentStartTime forKey:kAppointmentStartTime];
    [appointmentDictionary setObject:isEmpty(appointmentEndTime)?[NSNull null]:appointmentEndTime forKey:kAppointmentEndTime];
    
    [appointmentDictionary setObject:isEmpty(self.appointmentId)?[NSNull null]:self.appointmentId forKey:kAppointmentId];
    [appointmentDictionary setObject:isEmpty(self.appointmentLocation)?[NSNull null]:self.appointmentLocation forKey:kAppointmentLocation];
    [appointmentDictionary setObject:isEmpty(self.appointmentNotes)?[NSNull null]:self.appointmentNotes forKey:kAppointmentNotes];
    [appointmentDictionary setObject:isEmpty(self.appointmentOverdue)?[NSNull null]:self.appointmentOverdue forKey:kAppointmentOverdue];
    [appointmentDictionary setObject:isEmpty(self.appointmentShift)?[NSNull null]:self.appointmentShift forKey:kAppointmentShift];
    [appointmentDictionary setObject:isEmpty(self.appointmentStatus)?[NSNull null]:self.appointmentStatus forKey:kAppointmentStatus];
    [appointmentDictionary setObject:isEmpty(self.appointmentTitle)?[NSNull null]:self.appointmentTitle forKey:kAppointmentTitle];
    [appointmentDictionary setObject:isEmpty(self.appointmentType)?[NSNull null]:self.appointmentType forKey:kAppointmentType];
    [appointmentDictionary setObject:isEmpty(self.assignedTo)?[NSNull null]:self.assignedTo forKey:kAssignedTo];
    [appointmentDictionary setObject:isEmpty(self.createdBy)?[NSNull null]:self.createdBy forKey:kCreatedBy];
    
    [appointmentDictionary setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
    
    [appointmentDictionary setObject:isEmpty(self.defaultCustomerId)?[NSNull null]:self.defaultCustomerId forKey:kDefaultCustomerId];
    [appointmentDictionary setObject:isEmpty(self.defaultCustomerIndex)?[NSNull null]:self.defaultCustomerIndex forKey:kDefaultCustomerIndex];
    [appointmentDictionary setObject:isEmpty(self.journalHistoryId)?[NSNull null]:self.journalHistoryId forKey:kJournalHistoryId];
    [appointmentDictionary setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:kJournalId];
    [appointmentDictionary setObject:isEmpty(self.jsgNumber)?[NSNull null]:self.jsgNumber forKey:kJsgNumber];
    [appointmentDictionary setObject:isEmpty(loggedDate)?[NSNull null]:loggedDate forKey:kLoggedDate];
    [appointmentDictionary setObject:isEmpty(self.noEntryNotes)?[NSNull null]:self.noEntryNotes forKey:kNoEntryNotes];
    [appointmentDictionary setObject:isEmpty(repairCompletionDateTime)?[NSNull null]:repairCompletionDateTime forKey:kRepairCompletionDateTime];
    [appointmentDictionary setObject:isEmpty(self.repairNotes)?[NSNull null]:self.repairNotes forKey:kRepairNotes];
    [appointmentDictionary setObject:isEmpty(self.surveyorAlert)?[NSNull null]:self.surveyorAlert forKey:kSurveyorAlert];
    [appointmentDictionary setObject:isEmpty(self.surveyorUserName)?[NSNull null]:self.surveyorUserName forKey:kSurveyorUserName];
    [appointmentDictionary setObject:isEmpty(self.surveyourAvailability)?[NSNull null]:self.surveyourAvailability forKey:kSurveyourAvailability];
    [appointmentDictionary setObject:isEmpty(self.surveyType)?[NSNull null]:self.surveyType forKey:kSurveyType];
    [appointmentDictionary setObject:isEmpty(self.tenancyId)?[NSNull null]:self.tenancyId forKey:kTenancyId];
    [appointmentDictionary setObject:isEmpty(self.isMiscAppointment)?[NSNull null]:self.isMiscAppointment forKey:kIsMiscAppointment];
    [appointmentDictionary setObject:isEmpty(self.addToCalendar)?[NSNull null]:self.addToCalendar forKey:kAppointmentAddtoCalendar];
    [appointmentDictionary setObject:isEmpty(self.syncStatus)?[NSNull null]:self.syncStatus forKey:kAppointmentSyncStatus];
    
    NSDictionary *appInfo = [NSDictionary dictionaryWithDictionary:[self.appointmentToAppInfoData JSONValue]];
    NSDictionary *appCP12 = [NSDictionary dictionaryWithDictionary:[self.appointmentToCP12Info JSONValue]];
    
    NSMutableArray *customerList = [NSMutableArray array];
    for (Customer *customer in self.appointmentToCustomer) {
        [customerList addObject:[customer JSONValue]];
    }
    
    if(self.appointmentToProperty)
        
    {
        NSDictionary *propertyData = [NSDictionary dictionaryWithDictionary:[self.appointmentToProperty JSONValue]];
        [appointmentDictionary setObject:propertyData forKey:kAppointmentToProperty];
    }
    else
    {
        NSDictionary * schemeData = [NSDictionary dictionaryWithDictionary:[self.appointmentToScheme JSONValue]];
        [appointmentDictionary setObject:schemeData forKey:kAppointmentToScheme];
    }
    NSMutableArray *jobDataListArray = [NSMutableArray array];
    for (JobDataList *jobData in self.appointmentToJobDataList) {
        
        [jobDataListArray addObject:[jobData JSONValue]];
        
    }
    
    NSDictionary *tradeComponent = [NSDictionary dictionaryWithDictionary:[self.appointmentToPlannedComponent JSONValue]];
    [appointmentDictionary setObject:tradeComponent forKey:kAppointmentToComponentTrade];
    
    NSDictionary *journalData = [NSDictionary dictionaryWithDictionary:[self.appointmentToJournal JSONValue]];
    NSDictionary *surveyDicitionary = [NSDictionary dictionaryWithDictionary:[self.appointmentToSurvey JSONValue]];
    [appointmentDictionary setObject:appInfo forKey:kAppointmentToAppInfoData];
    [appointmentDictionary setObject:appCP12 forKey:kAppointmentToCP12Info];
    [appointmentDictionary setObject:customerList forKey:kAppointmentToCustomer];
    [appointmentDictionary setObject:jobDataListArray forKey:kAppointmentToJobDataList];
    [appointmentDictionary setObject:journalData forKey:kAppointmentToJournal];
    
    [appointmentDictionary setObject:surveyDicitionary forKey:kSurvey];
    [self addBuildAndCompletionInfoToAppointmentDic:appointmentDictionary];
    
    return appointmentDictionary;
}


- (NSString *) description
{
    return [NSString stringWithFormat:@"appointmentCalendar: %@,appointmentDate: %@,appointmentEndTime: %@,appointmentId: %@,appointmentLocation: %@,appointmentNotes: %@,appointmentShift: %@,appointmentStartTime: %@,appointmentStatus: %@,appointmentTitle: %@,appointmentType: %@,assignedTo: %@,createdBy: %@,defaultCustomerId: %@,defaultCustomerIndex: %@,journalHistoryId: %@,journalId: %@,jsgNumber: %@,loggedDate: %@,noEntryNotes: %@,repairCompletionDateTime: %@,repairNotes: %@,surveyCheckListForm: %@,surveyorAlert: %@,surveyorUserName: %@,surveyourAvailability: %@,surveyType: %@,tenancyId: %@",
            self.appointmentCalendar,
            self.appointmentDate,
            self.appointmentEndTime,
            self.appointmentId,
            self.appointmentLocation,
            self.appointmentNotes,
            self.appointmentShift,
            self.appointmentStartTime,
            self.appointmentStatus,
            self.appointmentTitle,
            self.appointmentType,
            self.assignedTo,
            self.createdBy,
            self.defaultCustomerId,
            self.defaultCustomerIndex,
            self.journalHistoryId,
            self.journalId,
            self.jsgNumber,
            self.loggedDate,
            self.noEntryNotes,
            self.repairCompletionDateTime,
            self.repairNotes,
            self.surveyCheckListForm,
            self.surveyorAlert,
            self.surveyorUserName,
            self.surveyourAvailability,
            self.surveyType,
            self.tenancyId];
}

#pragma mark - Methods
- (void) startAppointment
{
    CLS_LOG(@"Start Appointment Here!");
    [[PSDataUpdateManager sharedManager] startAppointment:self];
}

- (void) completeAppointment
{
    CLS_LOG(@"Complete Appointment Here");
    
}
#pragma mark - Create New Appointment
+ (void) saveNewAppointment: (NSDictionary *) appointment
{
    CLS_LOG(@"Creating New Appointment");
    [[PSDataPersistenceManager sharedManager] createNewAppointment:appointment];
    
    if (!isEmpty([appointment valueForKey:kAppointmentEventIdentifier])) {
        CLS_LOG(@"event exists");
    }
    else
    {
        CLS_LOG(@"event doesnt exist");
    }
}

#pragma mark - Prepare For Offline

- (BOOL) isPreparedForOffline
{
    BOOL isPrepared = NO;
    if ([self.isOfflinePrepared boolValue])
    {
        isPrepared = YES;
    }
    return isPrepared;
}

#pragma mark - Methods

- (AppointmentStatus) getStatus
{
    AppointmentStatus status = AppointmentStatusComplete;
    if([self.appointmentStatus compare:kAppointmentStatusComplete] == NSOrderedSame)
    {
        status = AppointmentStatusComplete;
    }
    else if([self.appointmentStatus compare:kAppointmentStatusFinished] == NSOrderedSame)
    {
        status = AppointmentStatusFinished;
    }
    else if([self.appointmentStatus compare:kAppointmentStatusInProgress] == NSOrderedSame)
    {
        status = AppointmentStatusInProgress;
    }
    else if([self.appointmentStatus compare:kAppointmentStatusNotStarted] == NSOrderedSame)
    {
        status = AppointmentStatusNotStarted;
    }
    else if([self.appointmentStatus compare:kAppointmentStatusNoEntry] == NSOrderedSame)
    {
        status = AppointmentStatusNoEntry;
    }
    return status;
}

-(AppointmentSyncStatus) getSyncStatus{
    if([self.syncStatus integerValue]==0){
        return AppointmentNotSynced;
    }
    else if([self.syncStatus integerValue]==1){
        return AppointmentDataSynced;
    }
    else if([self.syncStatus integerValue]==2){
        return AppointmentPhotoSynced;
    }
    return AppointmentSynced;
}

-(void) addBuildAndCompletionInfoToAppointmentDic:(NSMutableDictionary *) dict{
    if(self.aptCompletedAppVersionInfo!=nil){
        [dict setObject:self.aptCompletedAppVersionInfo forKey:kAptCompletedAppVersionInfo];
        [dict setObject:[UtilityClass getAppVersionStringForCompletedAppt] forKey:kAptCurrentAppVersionInfo];
    }
    else{
        [dict setObject:[NSNull null] forKey:kAptCompletedAppVersionInfo];
        [dict setObject:[NSNull null] forKey:kAptCurrentAppVersionInfo];
    }
    
    if(self.loggedDate!=nil){
        [dict setObject:[UtilityClass convertNSDateToServerDate:self.loggedDate] forKey:kLoggedDate];
    }
    else{
        [dict setObject:[NSNull null] forKey:kLoggedDate];
    }
}

- (AppointmentType) getType
{
    AppointmentType type = AppointmentTypeStock;
    if([self.appointmentType compare:kAppointmentTypeStock] == NSOrderedSame)
    {
        type = AppointmentTypeStock;
    }
    else if([self.appointmentType compare:kAppointmentTypeGas] == NSOrderedSame)
    {
        type = AppointmentTypeGas;
    }
    else if([self.appointmentType compare:kAppointmentTypeVoid] == NSOrderedSame)
    {
        type = AppointmentTypeVoid;
    }
    else if([self.appointmentType compare:kAppointmentTypeFault] == NSOrderedSame)
    {
        type = AppointmentTypeFault;
    }
    else if([self.appointmentType compare:kAppointmentTypePre] == NSOrderedSame)
    {
        type = AppointmentTypePre;
    }
    else if([self.appointmentType compare:kAppointmentTypePost] == NSOrderedSame)
    {
        type = AppointmentTypePost;
    }
    else if([self.appointmentType compare:kAppointmentTypePlanned] == NSOrderedSame)
    {
        type = AppointmentTypePlanned;
    }
    else if([self.appointmentType compare:kAppointmentTypeMiscellaneous] == NSOrderedSame)
    {
        type = AppointmentTypeMiscellaneous;
    }
    else if([self.appointmentType compare:kAppointmentTypeAdaptations] == NSOrderedSame)
    {
        type = AppointmentTypeAdaptations;
    }
    else if([self.appointmentType compare:kAppointmentTypeCondition] == NSOrderedSame)
    {
        type = AppointmentTypeCondition;
    }
    
    
    return type;
}

- (NSString *) stringWithAppointmentStatus:(AppointmentStatus)status
{
    NSString *statusString = @"";
    NSString *statusTitle = @"";
    NSString *dateFormat = @"";
    if(status == AppointmentStatusComplete)
    {
        statusTitle = LOC(@"KEY_STRING_COMPLETED");
    }
    else if(status == AppointmentStatusFinished)
    {
        statusTitle = LOC(@"KEY_STRING_COMPLETED");
    }
    else if(status == AppointmentStatusInProgress)
    {
        statusTitle = LOC(@"KEY_STRING_IN_PROGRESS");//KEY_STRING_STARTED
    }
    else if(status == AppointmentStatusPaused)
    {
        statusTitle = LOC(@"KEY_STRING_IN_PROGRESS");//KEY_STRING_STARTED
    }
    else if(status == AppointmentStatusNotStarted)
    {
        statusTitle = LOC(@"KEY_STRING_ARRANGED");
    }
    else if (status == AppointmentStatusNoEntry)
    {
        statusTitle = LOC(@"KEY_STRING_NO_ENTRY");
    }
    if (!isEmpty(self.loggedDate))
    {
        statusString = [NSString stringWithFormat:@"%@ %@",
                        statusTitle,
                        [UtilityClass stringFromDate:self.loggedDate dateFormat:kDateTimeStyle11]];
    }
    else
    {
        statusString = statusTitle;
    }
    return statusString;
}

- (NSString *) appointmentDateWithStyle:(AppointmentDateStyle)style dateFormat:(NSString *)dateFormat
{
    NSString *appointmentDateString = @"";
    switch (style) {
        case AppointmentDateStyleStartTime:
        {
            if(self.appointmentStartTime)
            {
                appointmentDateString = [UtilityClass stringFromDate:self.appointmentStartTime
                                                          dateFormat:dateFormat];
            }
        }
            break;
        case AppointmentDateStyleEndTime:
        {
            if(self.appointmentEndTime)
            {
                appointmentDateString = [UtilityClass stringFromDate:self.appointmentEndTime
                                                          dateFormat:dateFormat];
            }
        }
            break;
        case AppointmentDateStyleStartToEnd:
        {
            if(self.appointmentStartTime && self.appointmentEndTime)
            {
                NSString *startTimeString = [UtilityClass stringFromDate:self.appointmentStartTime
                                                              dateFormat:dateFormat];
                NSString *endTimeString = [UtilityClass stringFromDate:self.appointmentEndTime
                                                            dateFormat:dateFormat];
                if(!isEmpty(startTimeString) && !isEmpty(endTimeString))
                {
                    appointmentDateString = [NSString stringWithFormat:@"%@ %@ %@",
                                             startTimeString,
                                             LOC(@"KEY_STRING_TO"),
                                             endTimeString];
                }
            }
        }
            break;
        case AppointmentDateStyleComplete:
        {
            if(self.appointmentDate)
            {
                appointmentDateString = [UtilityClass stringFromDate:/*self.appointmentDate*/self.appointmentStartTime
                                                          dateFormat:dateFormat];
            }
        }
            break;
        default:
        {
            appointmentDateString = @"";
        }
            break;
    }
    return appointmentDateString?appointmentDateString:@"";
}

- (Customer *) defaultTenant
{
    Customer *customer =nil;
    if(self && !isEmpty(self.appointmentToCustomer))
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.customerId == %d", [self.defaultCustomerId intValue]];
        NSSet *filteredTenantsSet = [self.appointmentToCustomer filteredSetUsingPredicate:predicate];
        if(!isEmpty(filteredTenantsSet))
        {
            customer = [[filteredTenantsSet allObjects] objectAtIndex:0];
        }
        else
        {
            customer = [[self.appointmentToCustomer allObjects] objectAtIndex:0];
        }
    }
    return customer;
}


/*!
 @discussion
 Appointment List Section Identifier, Generated at Runtime. FetchedResultsController uses this idetifier to form the appointment sections.
 */
- (NSString *) appointmentDateSectionIdentifier {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:kAppointmentDateSectionIdentifier];
    NSString *sectionIdentifier = nil;//[self primitiveAppointmentDateSectionIdentifier];
    [self didAccessValueForKey:kAppointmentDateSectionIdentifier];
    
    if (!sectionIdentifier)
    {
        sectionIdentifier = [UtilityClass stringFromDate:self.appointmentDate
                                              dateFormat:kDateTimeStyle10];
        // [self setPrimitiveAppointmentDateSectionIdentifier:sectionIdentifier];
    }
    
    return sectionIdentifier;
}

#pragma mark -
#pragma mark Key path Dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionIdentifier {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:kAppointmentDate];
}

@end
