//
//  Scheme+JSON.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 23/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//
#import "Scheme.h"
#import "Scheme+JSON.h"

@implementation Scheme (JSON)
-(NSDictionary *)JSONValue
{
    NSMutableDictionary *schemeDict = [NSMutableDictionary dictionary];
    [schemeDict setObject:isEmpty(self.blockId)?[NSNull null]:self.blockId forKey:kSchemeBlockID];
    [schemeDict setObject:isEmpty(self.schemeId)?[NSNull null]:self.schemeId forKey:kSchemeID];
    [schemeDict setObject:isEmpty(self.blockName)?[NSNull null]:self.blockName forKey:kSchemeBlockName];
    [schemeDict setObject:isEmpty(self.schemeName)?[NSNull null]:self.schemeName forKey:kSchemeName];
    [schemeDict setObject:isEmpty(self.postCode)?[NSNull null]:self.postCode forKey:kSchemePostCode];
    [schemeDict setObject:isEmpty(self.townCity)?[NSNull null]:self.townCity forKey:kSchemCityTown];
    [schemeDict setObject:isEmpty(self.county)?[NSNull null]:self.county forKey:kSchemeCounty];
    
    return schemeDict;
}

- (NSString *) fullAddress
{
    NSMutableString *schemeAddress = [NSMutableString stringWithString:@""];
    
    if (!isEmpty(self.blockName)) {
        [schemeAddress appendString:self.blockName];
        [schemeAddress appendString:@", "];
    }

    if (!isEmpty(self.schemeName)) {
        [schemeAddress appendString:self.schemeName];
    }
    
    if (!isEmpty(self.county)) {
        [schemeAddress appendString:@"\n"];
        [schemeAddress appendString:self.county];
    }
    
    if (!isEmpty(self.townCity))
    {
        [schemeAddress appendString:@", "];
        [schemeAddress appendString:self.townCity];
    }
    
    if (!isEmpty(self.postCode)) {
        [schemeAddress appendString:@", "];
        [schemeAddress appendString:self.postCode];
    }
    
    return schemeAddress;
}

- (NSString *) shortAddress
{
    NSMutableString *schemeAddress = [NSMutableString stringWithString:@""];
    
    if (!isEmpty(self.blockName)) {
        [schemeAddress appendString:self.blockName];
        [schemeAddress appendString:@", "];
    }
    
    if (!isEmpty(self.schemeName)) {

        [schemeAddress appendString:self.schemeName];
    }
    return schemeAddress;
}
@end
