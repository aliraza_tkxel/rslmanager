//
//  AppInfoData+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "AppInfoData.h"

@interface AppInfoData (JSON)

- (NSDictionary *) JSONValue;
@end
