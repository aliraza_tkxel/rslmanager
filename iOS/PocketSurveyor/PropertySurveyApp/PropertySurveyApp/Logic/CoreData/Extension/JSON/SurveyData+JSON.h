//
//  SurveyData+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "SurveyData+CoreDataClass.h"

@interface SurveyData (JSON)
- (NSDictionary *) JSONValue;
@end
