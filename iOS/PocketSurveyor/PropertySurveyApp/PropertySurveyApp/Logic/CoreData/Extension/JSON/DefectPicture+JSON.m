//
//  DefectPicture+JSON.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 01/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "DefectPicture+JSON.h"
#import "DefectPicture+MWPhoto.h"

@implementation DefectPicture (JSON)

-(void) uploadPropertyPicture:(NSDictionary*) pictureDictionary withImage:(UIImage*) propertyImage
{
    self.image =[propertyImage scaleToSize:CGSizeMake(500, 500)];
    
    FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
    [manager storeImage:self.image forKey:[self getPicturePath]];
    if([self.defectPictureToDefect.defectID intValue]>0)
    {
        //Check for if background thread is posting images
        PSAppDelegate * delegate = (PSAppDelegate*)[UIApplication sharedApplication].delegate;
        if(delegate.postingDataObjectAppointments==0)
        {
            if([PSPropertyPictureManager sharedManager].isPostingImage==NO)
            {
                [PSPropertyPictureManager sharedManager].isPostingImage=YES;                [PSPropertyPictureManager sharedManager].uploadDefectPhotographService.photographDelegate = self;
                [[PSPropertyPictureManager sharedManager] uploadDefectPhotograph:pictureDictionary withImage:self.image];
            }
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification
                                                                        object:self];
}

-(void) uploadPicture
{
    self.image= [self underlyingImage];
    NSMutableDictionary * requestParameters=[[NSMutableDictionary alloc] init];
    
    if (self.image)
		{
        [requestParameters setObject:self.image forKey:kPropertyImage];
    }
    [requestParameters setObject:@".jpg" forKey:kFileExtension];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].salt forKey:kSalt];
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userName forKey:kUserName];
    
    [requestParameters setObject:[[SettingsClass sharedObject]loggedInUser].userId forKey:kUpdatedBy];
    [requestParameters setObject:self.defectPictureToDefect.defectID forKey:@"faultId"];
    [requestParameters setObject:@"false" forKey:kIsDefault];
    [requestParameters setObject:@"0" forKey:@"propertyPicId"];
    [requestParameters setObject:self.imageIdentifier forKey:kUniqueImageIdentifier];
    
    if(self.defectPictureToDefect.defectToDetector)
    {
        [requestParameters setObject:self.defectPictureToDefect.defectToDetector.detectorToProperty.propertyId forKey:kPropertyId];
    }
    else if(self.defectPictureToDefect.defectToAppliance)
    {
        [requestParameters setObject:self.defectPictureToDefect.defectToAppliance.applianceToProperty.propertyId forKey:kPropertyId];
    }
    
    [PSPropertyPictureManager sharedManager].uploadDefectPhotographService.photographDelegate = self;
    
    [[PSPropertyPictureManager sharedManager] uploadDefectPhotograph:requestParameters withImage:self.image];
    
}

-(NSString *) getPicturePath
{
    if (self.imagePath) {
        
        return self.imagePath;
    }
    NSString *   resultantPath = [NSString stringWithFormat:@"%@-%@",[UtilityClass stringFromDate:self.createdOn dateFormat:@"HH-mm-ss"],[self.synchStatus stringValue]];

    return resultantPath;
}

#pragma mark PSUPloadDefectpictutreServiceDelegate
- (void) service:(PSUploadDefectPictureService *)service didReceivePhotographData:(id)surveyorData {
          [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    NSString * responseString=surveyorData;
    if(!isEmpty(responseString))
    {
        NSDictionary *JSONDictionary = [responseString JSONValue];
        
        [[PSDataUpdateManager sharedManager] updateDefectPicture:self forDictionary:[JSONDictionary objectForKey:@"response"]];
        
        FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
        [manager storeImage:self.image forKey:[[JSONDictionary objectForKey:@"response"] objectForKey:kPropertyPicturePath]];
        
        NSDictionary * dict=[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:1] forKey:kPropertyPicturesSynchStatus];
        
        [[PSDataUpdateManager sharedManager] updateDefectPicture:self forDictionary:dict];
        self.image=nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationSuccess object:nil];
        return;
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:nil];
        return;
    }
    
    
}

- (void) service:(PSUploadDefectPictureService *)service didFailWithError:(NSError *)error {
    

    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    self.image=nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppointmentOfflineImageSaveNotificationFail object:error];
}

@end
