//
//  CP12Info+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 01/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "CP12Info+JSON.h"

@implementation CP12Info (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *cp12InfoDictionary = [NSMutableDictionary dictionary];
    
    NSString *dateTimeStamp =  isEmpty([UtilityClass convertNSDateToServerDate:self.dateTimeStamp])?nil:[UtilityClass convertNSDateToServerDate:self.dateTimeStamp];
    NSString *issuedDate =  isEmpty([UtilityClass convertNSDateToServerDate:self.issuedDate])?nil:[UtilityClass convertNSDateToServerDate:self.issuedDate];
    NSString *receivedDate =  isEmpty([UtilityClass convertNSDateToServerDate:self.receivedDate])?nil:[UtilityClass convertNSDateToServerDate:self.receivedDate];
    
    [cp12InfoDictionary setObject:isEmpty(self.cp12Document)?[NSNull null]:self.cp12Document forKey:@"CP12Document"];
    [cp12InfoDictionary setObject:isEmpty(self.cp12Number)?[NSNull null]:self.cp12Number forKey:@"CP12Number"];
    [cp12InfoDictionary setObject:isEmpty(dateTimeStamp)?[NSNull null]:dateTimeStamp forKey:@"DTimeStamp"];
    [cp12InfoDictionary setObject:isEmpty(self.documentType)?[NSNull null]:self.documentType forKey:@"DocumentType"];
    [cp12InfoDictionary setObject:isEmpty(self.inspectionCarried)?[NSNull null]:self.inspectionCarried forKey:@"InspectionCarried"];
    [cp12InfoDictionary setObject:isEmpty(self.issuedBy)?[NSNull null]:self.issuedBy forKey:@"IssuedBy"];
    [cp12InfoDictionary setObject:isEmpty(issuedDate)?[NSNull null]:issuedDate forKey:@"IssuedDate"];
    [cp12InfoDictionary setObject:isEmpty(self.issuedDateString)?[NSNull null]:self.issuedDateString forKey:@"IssuedDateString"];
    [cp12InfoDictionary setObject:isEmpty(self.journalId)?[NSNull null]:self.journalId forKey:@"JournalId"];
    [cp12InfoDictionary setObject:isEmpty(self.jsgNumber)?[NSNull null]:self.jsgNumber forKey:@"jsgNumber"];
    [cp12InfoDictionary setObject:isEmpty(self.lsgrid)?[NSNull null]:self.lsgrid forKey:@"LGSRID"];
    [cp12InfoDictionary setObject:isEmpty(self.notes)?[NSNull null]:self.notes forKey:@"Notes"];
    [cp12InfoDictionary setObject:isEmpty(self.propertyID)?[NSNull null]:self.propertyID forKey:kPropertyId];
    [cp12InfoDictionary setObject:isEmpty(receivedDate)?[NSNull null]:receivedDate forKey:@"ReceivedDate"];
    [cp12InfoDictionary setObject:isEmpty(self.receivedDateString)?[NSNull null]:self.receivedDateString forKey:@"ReceivedDateString"];
    [cp12InfoDictionary setObject:isEmpty(self.receivedOnBehalfOf)?[NSNull null]:self.receivedOnBehalfOf forKey:@"ReceivedOnBehalfOf"];
    [cp12InfoDictionary setObject:isEmpty(self.tenantHanded)?[NSNull null]:self.tenantHanded forKey:@"TenantHanded"];
    return cp12InfoDictionary;
}
@end
