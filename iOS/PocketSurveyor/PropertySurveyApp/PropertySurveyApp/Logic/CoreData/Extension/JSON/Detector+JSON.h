//
//  Detector+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 26/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Detector+CoreDataClass.h"

@interface Detector (JSON)
- (NSDictionary *) JSONValue;
@end
