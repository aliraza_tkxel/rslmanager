//
//  PlannedTradeComponent+JSON.m
//  PropertySurveyApp
//
//  Created by TkXel on 02/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PlannedTradeComponent+JSON.h"
#import "JobPauseData+JSON.h"

@implementation PlannedTradeComponent (JSON)

- (NSDictionary *) JSONValue
{
    NSMutableDictionary *jobDataDictionary = [NSMutableDictionary dictionary];
    
    NSString *completionDate = isEmpty([UtilityClass convertNSDateToServerDate:self.completionDate])?nil:[UtilityClass convertNSDateToServerDate:self.completionDate];
    NSString *reportedDate = isEmpty([UtilityClass convertNSDateToServerDate:self.reportedDate])?nil:[UtilityClass convertNSDateToServerDate:self.reportedDate];
    
    [jobDataDictionary setObject:isEmpty(completionDate)?[NSNull null]:completionDate forKey:@"completionDate"];
    [jobDataDictionary setObject:isEmpty(self.duration)?[NSNull null]:self.duration forKey:kDuration];
    [jobDataDictionary setObject:isEmpty(self.durationUnit)?[NSNull null]:self.durationUnit forKey:kDurationUnit];
    [jobDataDictionary setObject:isEmpty(self.jobStatus)?[NSNull null]:self.jobStatus forKey:kJobStatus];
    [jobDataDictionary setObject:isEmpty(self.componentName)?[NSNull null]:self.componentName forKey:kJSNDescription];
    [jobDataDictionary setObject:isEmpty(self.jsnNotes)?[NSNull null]:self.jsnNotes forKey:kJSNNotes];
    [jobDataDictionary setObject:isEmpty(self.jsNumber)?[NSNull null]:self.jsNumber forKey:kJSNumber];
    [jobDataDictionary setObject:isEmpty(self.componentTradeId)?[NSNull null]:self.componentTradeId forKey:kComponentTradeId];
    [jobDataDictionary setObject:isEmpty(self.componentId)?[NSNull null]:self.componentId forKey:kComponentId];
    [jobDataDictionary setObject:isEmpty(self.tradeId)?[NSNull null]:self.tradeId forKey:kTradeId];

    [jobDataDictionary setObject:isEmpty(self.location)?[NSNull null]:self.location forKey:kLocation];
    [jobDataDictionary setObject:isEmpty(self.locationId)?[NSNull null]:self.locationId forKey:kLocationId];
    [jobDataDictionary setObject:isEmpty(self.adaptation)?[NSNull null]:self.adaptation forKey:kAdaptation];
    [jobDataDictionary setObject:isEmpty(self.adaptationId)?[NSNull null]:self.adaptationId forKey:kAdaptationId];

    NSMutableArray *jobPauseHistoryArray = [NSMutableArray array];
    for (JobPauseData *jobPauseData in self.jobPauseData)
    {
        [jobPauseHistoryArray addObject:[jobPauseData JSONValue]];
    }
    
    [jobDataDictionary setObject:jobPauseHistoryArray forKey:@"jobPauseHistory"];
    
    return jobDataDictionary;
    
}

@end
