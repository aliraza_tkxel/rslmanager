//
//  ApplianceType+JSON.m
//  PropertySurveyApp
//
//  Created by Yawar on 20/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "ApplianceType+JSON.h"

@implementation ApplianceType (JSON)
- (NSDictionary *) JSONValue
{
    NSMutableDictionary *applianceTypeDictionary = [NSMutableDictionary dictionary];
    [applianceTypeDictionary setObject:isEmpty(self.typeID)?[NSNull null]:self.typeID forKey:kApplianceTypeID];
    [applianceTypeDictionary setObject:isEmpty(self.typeName)?[NSNull null]:self.typeName forKey:kApplianceTypeName];

    return applianceTypeDictionary;
}
@end
