//
//  Scheme+JSON.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 23/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "Scheme.h"

@interface Scheme (JSON)
- (NSDictionary *) JSONValue;
- (NSString *) fullAddress;
- (NSString *) shortAddress;
@end
