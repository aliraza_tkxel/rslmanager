//
//  JobPauseData+JSON.h
//  PropertySurveyApp
//
//  Created by Yawar on 16/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobPauseData.h"

@interface JobPauseData (JSON)
- (NSDictionary *) JSONValue;
@end
