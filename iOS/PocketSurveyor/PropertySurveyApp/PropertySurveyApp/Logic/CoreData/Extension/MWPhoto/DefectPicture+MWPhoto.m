//
//  DefectPicture+MWPhoto.m
//  PropertySurveyApp
//
//  Created by TkXel on 03/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "DefectPicture+MWPhoto.h"
#import "DefectPicture+JSON.h"

@implementation DefectPicture (MWPhoto)

-(UIImage *) underlyingImage
{
    return [[FBRWebImageManager sharedManager] imageWithURL:[NSURL URLWithString:[self getPicturePath]]];
}

-(void) loadUnderlyingImageAndNotify
{
    FBRWebImageManager *manager = [FBRWebImageManager sharedManager];
    [manager downloadWithURL:[NSURL URLWithString:self.imagePath] delegate:self];
}

- (void)unloadUnderlyingImage
{
    
}

- (void)webImageManager:(FBRWebImageManager *)imageManager didFinishWithImage:(UIImage *)image
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                        object:self];
    //notify here with image
    
}

- (void)webImageManager:(FBRWebImageManager *)imageManager didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                        object:nil];
}

-(void) deletePhotograph
{
    [[PSDataUpdateManager sharedManager] deleteDefectPicture:self];
}



@end
