//
//  PropertyPicture+MWPhoto.h
//  PropertySurveyApp
//
//  Created by TkXel on 03/01/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PropertyPicture+CoreDataClass.h"
#import "MWPhotoProtocol.h"

@interface PropertyPicture (MWPhoto)<MWPhoto,FBRWebImageManagerDelegate>

@end
