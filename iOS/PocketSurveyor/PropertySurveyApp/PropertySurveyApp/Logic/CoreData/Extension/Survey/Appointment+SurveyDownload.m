//
//  Appointment+SurveyDownload.m
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 24/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Appointment+SurveyDownload.h"


#import <objc/runtime.h>

@implementation Appointment (SurveyDownload)

@dynamic fetchSurveyFormService;
//@dynamic fetchAppliancesService;
@dynamic isDownloadInProgress;
@dynamic progressBlock;

static char PSA_FETCH_SURVEY_FORM_SERVICE_KEY;
static char PSA_FETCH_APPLIANCES_SERVICE_KEY;
static char PSA_IS_DOWNLOAD_IN_PROGRESS_KEY;
static char PSA_PROGRESS_BLOCK_KEY;

#pragma mark - Accessor Methods for Fetch Survey Form Service
- (void)setFetchSurveyFormService:(PSFetchSurveyFormService *)fetchSurveyFormService {
	objc_setAssociatedObject(self, &PSA_FETCH_SURVEY_FORM_SERVICE_KEY, fetchSurveyFormService, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (PSFetchSurveyFormService *)fetchSurveyFormService {
	return objc_getAssociatedObject(self, &PSA_FETCH_SURVEY_FORM_SERVICE_KEY);
}

#pragma mark - Accessor Methods for Get Appliacnes Service
- (void)setFetchAppliancesService:(PSFetchAppliancesService *)fetchAppliancesService {
	objc_setAssociatedObject(self, &PSA_FETCH_APPLIANCES_SERVICE_KEY, fetchAppliancesService, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (PSFetchAppliancesService *)fetchAppliancesService {
	return objc_getAssociatedObject(self, &PSA_FETCH_APPLIANCES_SERVICE_KEY);
}

#pragma mark - Download Progress
- (void)setIsDownloadInProgress:(BOOL)isDownloadInProgress {
	objc_setAssociatedObject(self, &PSA_IS_DOWNLOAD_IN_PROGRESS_KEY, [NSNumber numberWithBool:isDownloadInProgress], OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)isDownloadInProgress {
	return [(NSNumber *)objc_getAssociatedObject(self, &PSA_IS_DOWNLOAD_IN_PROGRESS_KEY) boolValue];
}

- (void)setProgressBlock:(AppointmentSurveyDataProgressiveOperationProgressBlock)progressBlock {
	objc_setAssociatedObject(self, &PSA_PROGRESS_BLOCK_KEY, progressBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (AppointmentSurveyDataProgressiveOperationProgressBlock)progressBlock {
	return objc_getAssociatedObject(self, &PSA_PROGRESS_BLOCK_KEY);
}

#pragma mark - Methods

- (void)prepareForOffline {
	[self setIsDownloadInProgress:YES];
    
	if ([UtilityClass isUserLoggedIn] && !isEmpty(self.appointmentToProperty)) {
		@synchronized(self)
		{
            if([self getType] == AppointmentTypeGas)
            {
                if (![self fetchAppliancesService]) {
                    [self setFetchAppliancesService:[[PSFetchAppliancesService alloc] init]];
                    [self fetchAppliancesService].appliacneDelegate = self;
                }

                NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
                [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
                [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
                
                [[self fetchAppliancesService] fetchAllAppliacnes:requestParameters];
            }
            else
            {
                if (![self fetchSurveyFormService]) {
                    [self setFetchSurveyFormService:[[PSFetchSurveyFormService alloc] init]];
                    [self fetchSurveyFormService].surveyDelegate = self;
                }
                
                NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
                [requestParameters setObject:self.appointmentId forKey:@"appointmentid"];
                [requestParameters setObject:self.appointmentToProperty.propertyId forKey:kPropertyId];
                [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
                [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.salt forKey:@"salt"];
                [[self fetchSurveyFormService] fetchStockSurveyForm:requestParameters];
            }
		}
	}
	else
    {
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
	}
}

- (void) fetchPropertyApplianceData
{
    NSMutableDictionary *requestParameters = [NSMutableDictionary dictionary];
    [requestParameters setObject:[SettingsClass sharedObject].loggedInUser.userName forKey:@"username"];
    [requestParameters setObject:self.appointmentId forKey:@"appointmentid"];
    [requestParameters setObject:self.appointmentToProperty.propertyId forKey:kPropertyId];
    [requestParameters setObject:self.journalId forKey:kJournalId];
    [[PSApplianceManager sharedManager] fetchPropertyAppliances:requestParameters forProperty:self.appointmentToProperty.objectID];
}

#pragma mark - Delegates Download Progress Method

- (void)service:(id)service didReceiveDataBytes:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength {
	if (downloadedBytes >= expectedContentLength) {
        [self setIsDownloadInProgress:NO];
    }
    
    if ([self progressBlock]) {
		AppointmentSurveyDataProgressiveOperationProgressBlock block = [self progressBlock];
        block(downloadedBytes, expectedContentLength);
	}
    
	CLS_LOG(@"Progess downloaded: %d, expected: %d", downloadedBytes, expectedContentLength);
}

#pragma mark - PSFetchSurveyFormServiceDelegate
- (void)service:(PSFetchSurveyFormService *)service didFinishDownloadingSurveyForm:(id)surveyData {
	NSDictionary *responseDictionary = [surveyData JSONValue];
    NSInteger statusCode = [[[responseDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
    if(statusCode == kSuccessCode)
    {
        NSMutableDictionary *surveyDataDictionary = [[NSMutableDictionary alloc] initWithDictionary:[responseDictionary objectForKey:kResponseTag]];
        NSDictionary *detectors = [[NSDictionary alloc] initWithDictionary:[surveyDataDictionary objectForKey:kPropertyInfoTag]];
        
        [surveyDataDictionary removeObjectForKey:kPropertyInfoTag];
        
        [[PSDataPersistenceManager sharedManager] saveAppointmentSurveyData:surveyDataDictionary];
        
        NSArray *detectorsList = [detectors objectForKey:kDetectorsTag];
        [[PSDataPersistenceManager sharedManager] saveSurveyPropertyDetectors:detectorsList];
    }
    else
    {
         [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
    }
    //    if (self.progressBlock) {
    //		self.progressBlock(1, 1); // to indicate that download has completed!
    //	}
}

#pragma mark - PSFetchAppliancesServiceDelegate
- (void) service:(PSFetchAppliancesService *)service didFinishLoadingAppliances:(id)appliancesData
{
    NSDictionary *responseDictionary = [appliancesData JSONValue];
    NSInteger statusCode = [[[responseDictionary objectForKey:kStatusTag] objectForKey:kCodeTag] integerValue];
    if(statusCode == kSuccessCode)
    {
        NSDictionary *surveyDataDictionary = [responseDictionary objectForKey:kResponseTag];
        [[PSDataPersistenceManager sharedManager] saveAppliances:surveyDataDictionary];
        [self performSelector:@selector(fetchPropertyApplianceData) withObject:nil afterDelay:5.0];

    }
    else
    {
         [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesFailureNotification object:nil];
    }
}

#pragma mark - Delegates Error Method
- (void) service:(id)service didFailWithError:(NSError *)error
{
    [self setIsDownloadInProgress:NO];
}

@end
