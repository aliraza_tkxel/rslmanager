//
//  Survey+DataPersistance.m
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 09/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Survey+DataPersistance.h"
#import "SurveyData+CoreDataClass.h"
#import "UtilityClass.h"
#import "SettingsClass.h"
#import "SurveyData+DataPersistance.h"

#define kSurveyDataItemId @"itemId"
#define kSurveyDataHeatingTypeId    @"heatingTypeId"

@implementation Survey (DataPersistance)

- (void)saveFormDataDictionary:(NSDictionary *)formDataDictionary forSurveyDataDictionary:(NSDictionary *)surveyDataDictionary
{
	__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
	
    SurveyData *surveyData = [self surveyDataWithItemId:surveyDataDictionary[kSurveyDataItemId] andHeatingId:[surveyDataDictionary objectForKey:@"heatingTypeId"] inManagedObjectContext:managedObjectContext];
	self.surveyToAppointment.isModified = [NSNumber numberWithBool:YES];
	self.surveyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
	surveyData.appointmentId = self.surveyToAppointment.appointmentId;
	surveyData.customerId = [[SettingsClass sharedObject] currentUserId];
	surveyData.itemId = [surveyDataDictionary objectForKey:kSurveyDataItemId];
	surveyData.propertyId = surveyData.surveyDataToSurvey.surveyToAppointment.appointmentToProperty.propertyId;
	surveyData.surveyFormName = [formDataDictionary objectForKey:@"surveyName"];
    surveyData.heatingTypeId = [surveyDataDictionary objectForKey:kSurveyDataHeatingTypeId];
    
    
	[surveyData saveFormDataDictionary:formDataDictionary inManagedObjectContext:managedObjectContext];
    
	[managedObjectContext performBlock: ^{
		// Save the context.
		NSError *error = nil;
		CLS_LOG(@"Saving to PSC");
		if (![managedObjectContext save:&error])
		{
			CLS_LOG(@"Error in Saving MOC: %@", [error description]);
		}
	}]; // main
}

- (void)saveSurveyNotes:(NSString *)notesString forSurveyDataDictionary:(NSDictionary *)surveyDataDictionary
{
	__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];

	SurveyData *surveyData = [self surveyDataWithItemId:surveyDataDictionary[kSurveyDataItemId] andHeatingId:[surveyDataDictionary objectForKey:@"heatingTypeId"] inManagedObjectContext:managedObjectContext];
	self.surveyToAppointment.isModified = [NSNumber numberWithBool:YES];
	surveyData.appointmentId = self.surveyToAppointment.appointmentId;
	surveyData.customerId = [[SettingsClass sharedObject] currentUserId];
	surveyData.propertyId = surveyData.surveyDataToSurvey.surveyToAppointment.appointmentToProperty.propertyId;
	surveyData.itemId = [surveyDataDictionary objectForKey:kSurveyDataItemId];
	surveyData.surveyNotesDetail = notesString;
	surveyData.surveyNotesDate = [NSDate date];
    surveyData.heatingTypeId = [surveyDataDictionary objectForKey:kSurveyDataHeatingTypeId];
    
	[managedObjectContext performBlock: ^{
		// Save the context.
		NSError *error = nil;
		CLS_LOG(@"Saving to PSC");
		if (![managedObjectContext save:&error])
		{
			CLS_LOG(@"Error in Saving MOC: %@", [error description]);
		}
	}]; // main
}

- (SurveyData *)surveyDataWithItemId:(NSString *)itemId  andHeatingId:(NSString *) heatingId
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.%@ = %@ && SELF.%@ = %@", kSurveyDataItemId, itemId,kSurveyDataHeatingTypeId,heatingId];
    
	NSSet *filteredObjects = [[self surveyToSurveyData] filteredSetUsingPredicate:predicate];
	SurveyData *surveyData = nil;
    
	if ([filteredObjects count] > 0)
	{
		surveyData = [[filteredObjects allObjects] objectAtIndex:0];
	}
	return surveyData;
}

- (NSInteger)surveyFieldsFilled:(NSDictionary *)surveyDataDictionary
{
    
    //NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    SurveyData *surveyData = [self surveyDataWithItemId:surveyDataDictionary[kSurveyDataItemId] andHeatingId:surveyDataDictionary[kSurveyDataHeatingTypeId]];
    
    NSInteger filled = 0;
    NSArray *fields = [surveyDataDictionary objectForKey:@"Order"];
    for (NSString *field in fields) {
        NSDictionary *information = [[surveyDataDictionary objectForKey:@"Fields"] objectForKey:field];
        
        FormData * form=[surveyData formDataWithItemId:[information objectForKey:@"itemParamId"] controlType:[information objectForKey:@"Type"]];
        if (!isEmpty(form.surveyParamItemFieldValue) || [[information objectForKey:@"Selected"] count] > 0) {
            filled++;
        } else {
            if([self checkForSelectedKeyInDictionary:information]) {
                filled++;
            }
        }
    }

    return filled;
}

- (BOOL)checkForSelectedKeyInDictionary:(NSDictionary *)dictionaryToCheck {
    if ([[dictionaryToCheck objectForKey:@"Selected"] count] > 0) {
        return YES;
    }
    else {
        for (NSString *keyStrings in [dictionaryToCheck allKeys]) {
            if ([[dictionaryToCheck objectForKey:keyStrings] isKindOfClass:[NSDictionary class]]) {
                return [self checkForSelectedKeyInDictionary:[dictionaryToCheck objectForKey:keyStrings]];
            }
        }
        return NO;
    }
}

#pragma mark - Private Methods
- (SurveyData *)surveyDataWithItemId:(NSString *)itemId andHeatingId:(NSString *) heatingId inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext {
	SurveyData *surveyData = [self surveyDataWithItemId:itemId andHeatingId:heatingId];
    
	if (!surveyData) { // object is not found so we need to add a new one.
		surveyData = [NSEntityDescription insertNewObjectForEntityForName:@"SurveyData" inManagedObjectContext:managedObjectContext];
		Survey *survey = (Survey *)[managedObjectContext objectWithID:self.objectID];
		[survey addSurveyToSurveyDataObject:surveyData];
	}
    
	return surveyData;
}
@end
