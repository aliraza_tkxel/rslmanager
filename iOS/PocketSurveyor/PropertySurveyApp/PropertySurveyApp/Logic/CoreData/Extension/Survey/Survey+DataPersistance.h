//
//  Survey+DataPersistance.h
//  PropertySurveyApp
//
//  Created by Afnan Ahmad on 09/10/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//


@class SurveyData;

@interface Survey (DataPersistance)

- (void)saveFormDataDictionary:(NSDictionary *)formDataDictionary forSurveyDataDictionary:(NSDictionary *)surveyDataDictionary;
- (void)saveSurveyNotes:(NSString *)notesString forSurveyDataDictionary:(NSDictionary *)surveyDataDictionary;

- (SurveyData *)surveyDataWithItemId:(NSString *)itemId andHeatingId:(NSString *) heatingId;

- (NSInteger)surveyFieldsFilled:(NSDictionary *)surveyDataDictionary;

@end
