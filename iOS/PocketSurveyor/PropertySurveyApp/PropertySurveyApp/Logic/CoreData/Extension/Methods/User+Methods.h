//
//  User+Methods.h
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2016-04-08.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "User.h"

@interface User (Methods)

- (void) removeCompleteAppointments:(NSArray *)appointments;
- (NSArray *) modifiedAppointments;
- (NSArray *) completedAppointments;

@end
