//
//  Property+Methods.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Property+Methods.h"

@interface Property ()
- (NSString *) _fullAddress;
- (NSString *) _longAddress;
- (NSString *) _shortAddress;
- (NSString *) _propertyAddressStyle1;
@end


@implementation Property (Methods)



- (NSString *) addressWithStyle:(PropertyAddressStyle)addressStyle
{
    NSString *address = nil;
    if(addressStyle == PropertyAddressStyleShort)
    {
        address = [self _shortAddress];
    }
    else if(addressStyle == PropertyAddressStyleLong)
    {
        address = [self _longAddress];
    }
    else if(addressStyle == PropertyAddressStyleFull)
    {
        address = [self _fullAddress];
    }
    else if(addressStyle == PropertyAddressStyle1)
    {
        address = [self _propertyAddressStyle1];
    }
    else if (addressStyle == PropertyAddressStyleAddress2Onwards)
    {
        address = [self _propertyAddress2Onwards];
    }
    
    return address;
}

#pragma mark - Private Methods
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

- (NSString *) _fullAddress
{
    NSMutableString *propertyAddress = [NSMutableString stringWithString:@""];
    
    if (!isEmpty(self.houseNumber)) {
        [propertyAddress appendString:self.houseNumber];
    }
    
    if(!isEmpty(self.address1)){
        [propertyAddress appendString:@" "];
        [propertyAddress appendString:self.address1];
    }
    
    if (!isEmpty(self.address2)) {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.address2];
        
    }
    
    if(!isEmpty(self.address3)){
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.address3];
    }
    
    
    if (!isEmpty(self.county)) {
        [propertyAddress appendString:@"\n"];
        [propertyAddress appendString:self.county];
    }
    
    if (!isEmpty(self.townCity))
    {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.townCity];
    }
    
    if (!isEmpty(self.postCode)) {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.postCode];
    }
    
    return propertyAddress;
}

- (NSString *) _longAddress
{
    NSMutableString *propertyAddress = [NSMutableString stringWithString:@""];
    if (!isEmpty(self.houseNumber)) {
        [propertyAddress appendString:self.houseNumber];
    }
    
    if(!isEmpty(self.address1)){
        [propertyAddress appendString:@" "];
        [propertyAddress appendString:self.address1];
    }
    
    if (!isEmpty(self.address2)) {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.address2];
        
    }
    
    if (!isEmpty(self.townCity)){
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.townCity];
    }
    
    if (!isEmpty(self.county)) {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.county];
    }
    
    return propertyAddress;
}

- (NSString *) _shortAddress
{
    return [NSString stringWithFormat:@"%@ %@",self.houseNumber, self.address1];
}

- (NSString *) _propertyAddressStyle1
{
    NSMutableString *propertyAddress = [NSMutableString stringWithString:@""];
    
    if (!isEmpty(self.houseNumber)) {
        [propertyAddress appendString:self.houseNumber];
    }
    
    if(!isEmpty(self.address1)){
        [propertyAddress appendString:@" "];
        [propertyAddress appendString:self.address1];
    }
    
    if (!isEmpty(self.address2)) {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.address2];
        
    }
    
    if(!isEmpty(self.address3)){
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.address3];
    }
    
    if (!isEmpty(self.townCity))
    {
        [propertyAddress appendString:@"\n"];
        [propertyAddress appendString:self.townCity];
    }
    
    if (!isEmpty(self.postCode)) {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.postCode];
    }
    
    return propertyAddress;
    
}


- (NSString *) _propertyAddress2Onwards
{
    NSMutableString *propertyAddress = [NSMutableString stringWithString:@""];
    
    if (!isEmpty(self.address2)) {
        [propertyAddress appendString:self.address2];
    }
    
    if (!isEmpty(self.townCity))
    {
        if(!isEmpty(propertyAddress))
        {
            [propertyAddress appendString:@", "];
        }
        [propertyAddress appendString:self.townCity];
    }
    
    if (!isEmpty(self.postCode))
    {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.postCode];
    }
    
    return propertyAddress;
    
}

#pragma clang diagnostic pop
@end
