//
//  JobDataList+Methods.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobDataList.h"

@interface JobDataList (Methods)

- (void) startJob;
- (JobStatus) getStatus;
- (NSString *) stringForJSNLabel;
@end
