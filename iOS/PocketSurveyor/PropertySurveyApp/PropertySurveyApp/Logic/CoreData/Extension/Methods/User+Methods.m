//
//  User+Methods.m
//  PropertySurveyApp
//
//  Created by Omer Nasir on 2016-04-08.
//  Copyright © 2016 TkXel. All rights reserved.
//

#import "User+Methods.h"

@implementation User (Methods)

- (void) removeCompleteAppointments:(NSArray *)appointments
{
	if(!isEmpty(self.userToAppointments))
	{
		if(appointments == NULL)
		{
			for(Appointment *appointment in self.userToAppointments)
			{
				if ([appointment.appointmentStatus isEqualToString:@"Complete"] == TRUE)
				{
					[[PSDataUpdateManager sharedManager] deleteAppointment:appointment];
				}
			}
		}
		else
		{
			for(Appointment * appointment in appointments)
			{
				[[PSDataUpdateManager sharedManager] deleteAppointment:appointment];
			}
		}
	}
}


- (NSArray *) modifiedAppointments
{
	NSArray *modifiedAppointments = nil;
	if(!isEmpty(self.userToAppointments))
	{
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.isModified == 1"];
		NSSet *modifiedAppointmentsSet = [self.userToAppointments filteredSetUsingPredicate:predicate];
		if(!isEmpty(modifiedAppointmentsSet))
		{
			modifiedAppointments = [modifiedAppointmentsSet allObjects];
		}
	}
	return modifiedAppointments;
}

- (NSArray *) completedAppointments
{
	NSArray *completedAppointments = nil;
	if(!isEmpty(self.userToAppointments))
	{
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == 'Complete'"];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == 'Finished'"];
        NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"SELF.appointmentStatus == 'No Entry'"];
        NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1, predicate2,predicate3]];
        NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"syncStatus" ascending:NO];
		NSSet *completedAppointmentsSet = [self.userToAppointments filteredSetUsingPredicate:predicate];
		if(!isEmpty(completedAppointmentsSet))
		{
            completedAppointments = [completedAppointmentsSet sortedArrayUsingDescriptors:[[NSArray alloc] initWithObjects:sorter,nil]];
		}
	}
	return completedAppointments;
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"User: [ %@, %@, %@ ]",
					self.userName,
					self.password,
					self.salt];
}


@end
