//
//  JobDataList+Methods.m
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobDataList+Methods.h"

@implementation JobDataList (Methods)

#pragma mark - Methods
- (void) startJob
{
    CLS_LOG(@"Start Job Here!");
    [[PSDataUpdateManager sharedManager] startJob:self];
}

- (JobStatus) getStatus
{
    JobStatus status = JobStatusComplete;
    if([self.jobStatus compare:kJobStatusComplete] == NSOrderedSame)
    {
        status = JobStatusComplete;
    }
    else if([self.jobStatus compare:kJobStatusFinished] == NSOrderedSame)
    {
        status = JobStatusFinished;
    }
    else if([self.jobStatus compare:kJobStatusInProgress] == NSOrderedSame)
    {
        status = JobStatusInProgress;
    }
    else if([self.jobStatus compare:kJobStatusNotStarted] == NSOrderedSame)
    {
        status = JobStatusNotStarted;
    }
    else if([self.jobStatus compare:kJobStatusNoEntry] == NSOrderedSame)
    {
        status = JobStatusNoEntry;
    }
    return status;
}

- (NSString *) stringForJSNLabel
{
    NSString *JSNString = [NSString stringWithFormat:@"%@: %@",self.jsNumber,self.priority];
    return JSNString;
}

@end
