//
//  JobPauseReason.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface JobPauseReason : NSManagedObject

@property (nonatomic, retain) NSString * pauseReason;

@end
