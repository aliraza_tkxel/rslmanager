//
//  FaultRepairData.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobDataList;

@interface FaultRepairData : NSManagedObject

@property (nonatomic, retain) NSNumber * faultRepairID;
@property (nonatomic, retain) NSString * faultRepairDescription;
@property (nonatomic, retain) JobDataList *faultRepairDataToJobDataList;
@property (nonatomic, retain) NSString *repairSectionIdentifier;
@property (nonatomic, retain) NSString *primitiveRepairSectionIdentifier;
- (NSString *) repairSectionIdentifier;
@end
