//
//  Address+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer;

NS_ASSUME_NONNULL_BEGIN

@interface Address : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Address+CoreDataProperties.h"
