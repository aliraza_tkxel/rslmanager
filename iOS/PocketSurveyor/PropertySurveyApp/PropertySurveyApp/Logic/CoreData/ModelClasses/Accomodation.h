//
//  Accomodation.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Property;

@interface Accomodation : NSManagedObject

@property (nonatomic, retain) NSString * propertyId;
@property (nonatomic, retain) NSNumber * roomHeight;
@property (nonatomic, retain) NSNumber * roomLength;
@property (nonatomic, retain) NSString * roomName;
@property (nonatomic, retain) NSNumber * roomWidth;
@property (nonatomic, retain) NSNumber * updatedBy;
@property (nonatomic, retain) NSDate * updatedOn;
@property (nonatomic, retain) Property *accomodationToProperty;

@end
