//
//  JobDataList.m
//  PropertySurveyApp
//
//  Created by Yawar on 02/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "JobDataList.h"
#import "Appointment+CoreDataClass.h"
#import "FaultRepairData.h"
#import "FaultRepairListHistory.h"
#import "JobPauseData.h"
#import "RepairPictures.h"


@implementation JobDataList

@dynamic completionDate;
@dynamic duration;
@dynamic durationUnit;
@dynamic faultLogID;
@dynamic followOnNotes;
@dynamic jobStatus;
@dynamic jsnDescription;
@dynamic jsnLocation;
@dynamic jsnNotes;
@dynamic jsNumber;
@dynamic priority;
@dynamic repairNotes;
@dynamic reportedDate;
@dynamic responseTime;
@dynamic faultRepairDataList;
@dynamic faultRepairHistoryList;
@dynamic jobDataListToAppointment;
@dynamic jobPauseData;
@dynamic jobDataListToRepairImages;

- (NSString *)getFaultDescription
{
    NSMutableString *faultDescription = [NSMutableString stringWithFormat:@""];
    if (!isEmpty(self.jsnLocation))
    {
        [faultDescription appendString:[NSString stringWithFormat:@"%@ > ",self.jsnLocation]];
    }
    if (!isEmpty(self.jsnDescription))
    {
        [faultDescription appendString:self.jsnDescription];
    }
    
    return faultDescription;
}
@end
