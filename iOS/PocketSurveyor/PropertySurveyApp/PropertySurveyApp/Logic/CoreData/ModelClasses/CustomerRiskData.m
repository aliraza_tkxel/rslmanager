//
//  CustomerRiskData.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 02/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "CustomerRiskData.h"
#import "Customer+CoreDataClass.h"


@implementation CustomerRiskData

@dynamic riskCatDesc;
@dynamic riskSubCatDesc;
@dynamic customerRiskDataToCustomer;

@end
