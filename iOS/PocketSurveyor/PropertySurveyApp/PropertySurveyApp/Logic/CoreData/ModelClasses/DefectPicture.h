//
//  DefectPicture.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 25/02/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Defect;

@interface DefectPicture : NSManagedObject

@property (nonatomic, retain) NSNumber * createdBy;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSNumber * defectPictureId;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSNumber * synchStatus;
@property (nonatomic, retain) NSString * imageIdentifier;
@property (nonatomic, retain) Defect *defectPictureToDefect;
@property (nonatomic, retain) UIImage *image;

@end
