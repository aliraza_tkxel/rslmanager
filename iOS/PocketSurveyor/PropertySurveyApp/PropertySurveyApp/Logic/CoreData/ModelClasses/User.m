//
//  User.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "User.h"
#import "Appointment+CoreDataClass.h"


@implementation User

@dynamic fullName;
@dynamic isActive;
@dynamic lastLoggedInDate;
@dynamic password;
@dynamic salt;
@dynamic userId;
@dynamic userName;
@dynamic userToAppointments;

@end
