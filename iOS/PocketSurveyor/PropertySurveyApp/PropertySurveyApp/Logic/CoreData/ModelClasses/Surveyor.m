//
//  Surveyor.m
//  PropertySurveyApp
//
//  Created by Yawar on 10/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Surveyor.h"


@implementation Surveyor

@dynamic fullName;
@dynamic surveyorType;
@dynamic userId;
@dynamic userName;
@dynamic surveyorNameSectionIdentifier;
@synthesize primitiveSurveyorNameSectionIdentifier;

/*!
 @discussion
 Surveyor List Section Identifier, Generated at Runtime. FetchedResultsController uses this idetifier to form the surveyor sections.
 */
- (NSString *) surveyorNameSectionIdentifier {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:kSurveyorNameSectionIdentifier];
    NSString *sectionIdentifier = [self primitiveSurveyorNameSectionIdentifier];
    [self didAccessValueForKey:kSurveyorNameSectionIdentifier];
    
    if (!sectionIdentifier)
    {
        sectionIdentifier = [[self.fullName substringToIndex:1] uppercaseString];
        if(![sectionIdentifier isAlphabeticChar])
        {
            sectionIdentifier = @"#";
        }
        [self setPrimitiveSurveyorNameSectionIdentifier:sectionIdentifier];
    }
    return sectionIdentifier;
}

#pragma mark -
#pragma mark Key path Dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionIdentifier {
    // If the value of surveyName changes, the section identifier may change as well.
    return [NSSet setWithObject:kFullName];
}

@end
