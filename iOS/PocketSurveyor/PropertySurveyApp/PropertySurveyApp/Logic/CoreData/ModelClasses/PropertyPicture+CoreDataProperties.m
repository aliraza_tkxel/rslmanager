//
//  PropertyPicture+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/04/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "PropertyPicture+CoreDataProperties.h"

@implementation PropertyPicture (CoreDataProperties)

+ (NSFetchRequest<PropertyPicture *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PropertyPicture"];
}

@dynamic appointmentId;
@dynamic createdBy;
@dynamic createdOn;
@dynamic deletedPicture;
@dynamic imageIdentifier;
@dynamic imagePath;
@dynamic isDefault;
@dynamic itemId;
@dynamic propertyId;
@dynamic propertyPictureId;
@dynamic propertyPictureName;
@dynamic surveyId;
@dynamic synchStatus;
@dynamic heatingId;
@dynamic image;
@dynamic propertyPictureToProperty;

@end
