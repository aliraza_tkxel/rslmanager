//
//  DefectCategory.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DefectCategory : NSManagedObject

@property (nonatomic, retain) NSString * categoryDescription;
@property (nonatomic, retain) NSNumber * categoryID;

@end
