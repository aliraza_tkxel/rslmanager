//
//  Detector+CoreDataClass.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Detector+CoreDataClass.h"
#import "Defect.h"

#import "DetectorInspection.h"

#import "PowerType+CoreDataClass.h"

#import "Property+CoreDataClass.h"

@implementation Detector

@end
