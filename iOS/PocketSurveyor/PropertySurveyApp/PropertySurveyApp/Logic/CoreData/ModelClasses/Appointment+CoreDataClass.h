//
//  Appointment+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AppInfoData, CP12Info, Component, Customer, JobDataList, Journal, NSObject, PlannedTradeComponent, Property, Scheme, Survey, SurveyHeatingFuelCertificate, SurveyHeatingFuelData, User;

NS_ASSUME_NONNULL_BEGIN

@interface Appointment : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Appointment+CoreDataProperties.h"
