//
//  SurveyHeatingFuelData+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SurveyHeatingFuelData+CoreDataProperties.h"

@implementation SurveyHeatingFuelData (CoreDataProperties)

+ (NSFetchRequest<SurveyHeatingFuelData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SurveyHeatingFuelData"];
}

@dynamic fuelTypeId;
@dynamic heatingFuelSurvey;
@dynamic heatingFuelSurveyToAppointment;

@end
