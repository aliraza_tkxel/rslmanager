//
//  Property+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Property+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Property (CoreDataProperties)

+ (NSFetchRequest<Property *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address1;
@property (nullable, nonatomic, copy) NSString *address2;
@property (nullable, nonatomic, copy) NSString *address3;
@property (nullable, nonatomic, copy) NSDate *certificateExpiry;
@property (nullable, nonatomic, copy) NSString *county;
@property (nullable, nonatomic, copy) NSString *flatNumber;
@property (nullable, nonatomic, copy) NSString *houseNumber;
@property (nullable, nonatomic, copy) NSDate *lastSurveyDate;
@property (nullable, nonatomic, copy) NSString *postCode;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSNumber *tenancyId;
@property (nullable, nonatomic, copy) NSString *townCity;
@property (nullable, nonatomic, copy) NSString *completeAddress;
@property (nullable, nonatomic, retain) PropertyPicture *defaultPicture;
@property (nullable, nonatomic, retain) InstallationPipework *installationPipework;
@property (nullable, nonatomic, retain) NSSet<Accomodation *> *propertyToAccomodation;
@property (nullable, nonatomic, retain) NSSet<Appliance *> *propertyToAppliances;
@property (nullable, nonatomic, retain) Appointment *propertyToAppointment;
@property (nullable, nonatomic, retain) NSSet<Detector *> *propertyToDetector;
@property (nullable, nonatomic, retain) NSSet<PropertyAsbestosData *> *propertyToPropertyAsbestosData;
@property (nullable, nonatomic, retain) NSSet<PropertyPicture *> *propertyToPropertyPicture;

@end

@interface Property (CoreDataGeneratedAccessors)

- (void)addPropertyToAccomodationObject:(Accomodation *)value;
- (void)removePropertyToAccomodationObject:(Accomodation *)value;
- (void)addPropertyToAccomodation:(NSSet<Accomodation *> *)values;
- (void)removePropertyToAccomodation:(NSSet<Accomodation *> *)values;

- (void)addPropertyToAppliancesObject:(Appliance *)value;
- (void)removePropertyToAppliancesObject:(Appliance *)value;
- (void)addPropertyToAppliances:(NSSet<Appliance *> *)values;
- (void)removePropertyToAppliances:(NSSet<Appliance *> *)values;

- (void)addPropertyToDetectorObject:(Detector *)value;
- (void)removePropertyToDetectorObject:(Detector *)value;
- (void)addPropertyToDetector:(NSSet<Detector *> *)values;
- (void)removePropertyToDetector:(NSSet<Detector *> *)values;

- (void)addPropertyToPropertyAsbestosDataObject:(PropertyAsbestosData *)value;
- (void)removePropertyToPropertyAsbestosDataObject:(PropertyAsbestosData *)value;
- (void)addPropertyToPropertyAsbestosData:(NSSet<PropertyAsbestosData *> *)values;
- (void)removePropertyToPropertyAsbestosData:(NSSet<PropertyAsbestosData *> *)values;

- (void)addPropertyToPropertyPictureObject:(PropertyPicture *)value;
- (void)removePropertyToPropertyPictureObject:(PropertyPicture *)value;
- (void)addPropertyToPropertyPicture:(NSSet<PropertyPicture *> *)values;
- (void)removePropertyToPropertyPicture:(NSSet<PropertyPicture *> *)values;

@end

NS_ASSUME_NONNULL_END
