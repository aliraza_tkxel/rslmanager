//
//  CustomerRiskData.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 02/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Customer;

@interface CustomerRiskData : NSManagedObject

@property (nonatomic, retain) NSString * riskCatDesc;
@property (nonatomic, retain) NSString * riskSubCatDesc;
@property (nonatomic, retain) Customer *customerRiskDataToCustomer;

@end
