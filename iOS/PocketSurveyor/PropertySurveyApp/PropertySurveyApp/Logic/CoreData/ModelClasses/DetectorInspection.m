//
//  DetectorInspection.m
//  PropertySurveyApp
//
//  Created by Yawar on 23/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "DetectorInspection.h"
#import "Detector+CoreDataClass.h"


@implementation DetectorInspection

@dynamic detectorsTested;
@dynamic inspectionID;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic detector;

@end
