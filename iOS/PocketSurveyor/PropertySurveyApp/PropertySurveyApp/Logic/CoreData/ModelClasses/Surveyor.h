//
//  Surveyor.h
//  PropertySurveyApp
//
//  Created by Yawar on 10/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Surveyor : NSManagedObject

@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSString * surveyorType;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * userName;

@property (nonatomic, retain) NSString *surveyorNameSectionIdentifier;
@property (nonatomic, retain) NSString *primitiveSurveyorNameSectionIdentifier;

@end
