//
//  Appliance.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 19/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ApplianceInspection, ApplianceLocation, ApplianceManufacturer, ApplianceModel, ApplianceType, Defect, Property;

@interface Appliance : NSManagedObject

@property (nonatomic, retain) NSNumber * applianceID;
@property (nonatomic, retain) NSString * fluType;
@property (nonatomic, retain) NSString * gcNumber;
@property (nonatomic, retain) NSDate * installedDate;
@property (nonatomic, retain) NSNumber * isInspected;
@property (nonatomic, retain) NSNumber * isLandlordAppliance;
@property (nonatomic, retain) NSString * model;
@property (nonatomic, retain) NSString * propertyID;
@property (nonatomic, retain) NSDate * replacementDate;
@property (nonatomic, retain) NSString * serialNumber;
@property (nonatomic, retain) NSSet *applianceDefects;
@property (nonatomic, retain) ApplianceLocation *applianceLocation;
@property (nonatomic, retain) ApplianceManufacturer *applianceManufacturer;
@property (nonatomic, retain) ApplianceModel *applianceModel;
@property (nonatomic, retain) Property *applianceToProperty;
@property (nonatomic, retain) ApplianceType *applianceType;
@property (nonatomic, retain) ApplianceInspection *applianceInspection;
@end

@interface Appliance (CoreDataGeneratedAccessors)

- (void)addApplianceDefectsObject:(Defect *)value;
- (void)removeApplianceDefectsObject:(Defect *)value;
- (void)addApplianceDefects:(NSSet *)values;
- (void)removeApplianceDefects:(NSSet *)values;

@end
