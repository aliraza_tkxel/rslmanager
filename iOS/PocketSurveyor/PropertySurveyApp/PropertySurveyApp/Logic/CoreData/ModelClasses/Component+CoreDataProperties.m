//
//  Component+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Component+CoreDataProperties.h"

@implementation Component (CoreDataProperties)

+ (NSFetchRequest<Component *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Component"];
}

@dynamic componentId;
@dynamic componentName;
@dynamic cycle;
@dynamic frequency;
@dynamic isComponentAccounting;
@dynamic itemId;
@dynamic parameterId;
@dynamic subParameterId;
@dynamic subValueId;
@dynamic valueId;
@dynamic componentToAppointment;

@end
