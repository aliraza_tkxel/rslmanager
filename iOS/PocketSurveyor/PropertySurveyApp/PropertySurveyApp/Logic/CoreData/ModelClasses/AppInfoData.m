//
//  AppInfoData.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "AppInfoData.h"
#import "Appointment+CoreDataClass.h"

@implementation AppInfoData

@dynamic totalAppointments;
@dynamic totalNoEntries;
@dynamic appInfoDataToAppointment;
@dynamic isCardLeft;
@end
