//
//  SProperty+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "SProperty+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SProperty (CoreDataProperties)

+ (NSFetchRequest<SProperty *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address1;
@property (nullable, nonatomic, copy) NSString *address2;
@property (nullable, nonatomic, copy) NSString *address3;
@property (nullable, nonatomic, copy) NSDate *certificateExpiry;
@property (nullable, nonatomic, copy) NSString *county;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *flatNumber;
@property (nullable, nonatomic, copy) NSString *fullName;
@property (nullable, nonatomic, copy) NSString *houseNumber;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, copy) NSString *middleName;
@property (nullable, nonatomic, copy) NSString *postCode;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSString *sPropertyJSON;
@property (nullable, nonatomic, copy) NSString *sPropertyNameSectionIdentifier;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *townCity;
@property (nullable, nonatomic, copy) NSString *completeName;
@property (nullable, nonatomic, copy) NSString *completeAddress;
@property (nonatomic, retain) NSString *primitiveSPropertyNameSectionIdentifier;

- (NSString *) fullName;
- (NSString *) fullNameWithoutTitle;
- (NSString *) SPropertyAddress;
- (NSString *) propertyShortAddress;

@end

NS_ASSUME_NONNULL_END
