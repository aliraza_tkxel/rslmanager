//
//  FaultRepairData.m
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "FaultRepairData.h"
#import "JobDataList.h"


@implementation FaultRepairData

@dynamic faultRepairID;
@dynamic faultRepairDescription;
@dynamic faultRepairDataToJobDataList;
@dynamic repairSectionIdentifier;
@synthesize primitiveRepairSectionIdentifier;

/*!
 @discussion
 Repair List Section Identifier, Generated at Runtime. FetchedResultsController uses this idetifier to form the repair list sections.
 */
- (NSString *) repairSectionIdentifier {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:kRepairSectionIdentifier];
    NSString *sectionIdentifier = [self primitiveRepairSectionIdentifier];
    [self didAccessValueForKey:kRepairSectionIdentifier];
    
    if (!sectionIdentifier)
    {
        sectionIdentifier = [[[self.faultRepairDescription trimWhitespace] substringToIndex:1] uppercaseString];
        if(![sectionIdentifier isAlphabeticChar])
        {
            sectionIdentifier = @"#";
        }
        [self setPrimitiveRepairSectionIdentifier:sectionIdentifier];
    }
    return sectionIdentifier;
}
@end
