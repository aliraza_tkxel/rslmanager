//
//  ApplianceLocation.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "ApplianceLocation.h"


@implementation ApplianceLocation

@dynamic locationID;
@dynamic locationName;

@end
