//
//  Customer+CoreDataClass.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Customer+CoreDataClass.h"
#import "Address+CoreDataClass.h"

#import "Appointment+CoreDataClass.h"

#import "CustomerRiskData.h"

#import "CustomerVulnerabilityData.h"

@implementation Customer

@end
