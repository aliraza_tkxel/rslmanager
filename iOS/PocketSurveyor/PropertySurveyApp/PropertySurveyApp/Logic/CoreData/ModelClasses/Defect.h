//
//  Defect.h
//  PropertySurveyApp
//
//  Created by Yawar on 24/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appliance, DefectCategory, DefectPicture, Detector;

@interface Defect : NSManagedObject

@property (nonatomic, retain) NSNumber * applianceID;
@property (nonatomic, retain) NSDate * defectDate;
@property (nonatomic, retain) NSString * defectDescription;
@property (nonatomic, retain) NSNumber * defectID;
@property (nonatomic, retain) NSString * defectType;
@property (nonatomic, retain) NSNumber * faultCategory;
@property (nonatomic, retain) NSNumber * isActionTaken;
@property (nonatomic, retain) NSNumber * isAdviceNoteIssued;
@property (nonatomic, retain) NSNumber * isDefectIdentified;
@property (nonatomic, retain) NSNumber * journalId;
@property (nonatomic, retain) NSString * propertyId;
@property (nonatomic, retain) NSString * remedialAction;
@property (nonatomic, retain) NSString * serialNo;
@property (nonatomic, retain) NSNumber * detectorTypeId;
@property (nonatomic, retain) NSNumber * warningTagFixed;
@property (nonatomic, retain) DefectCategory *defectCategory;
@property (nonatomic, retain) NSSet *defectPictures;
@property (nonatomic, retain) Appliance *defectToAppliance;
@property (nonatomic, retain) Detector *defectToDetector;
@end

@interface Defect (CoreDataGeneratedAccessors)

- (void)addDefectPicturesObject:(DefectPicture *)value;
- (void)removeDefectPicturesObject:(DefectPicture *)value;
- (void)addDefectPictures:(NSSet *)values;
- (void)removeDefectPictures:(NSSet *)values;

@end
