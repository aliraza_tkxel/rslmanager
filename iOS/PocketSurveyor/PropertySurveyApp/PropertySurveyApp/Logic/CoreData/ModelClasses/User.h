//
//  User.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSDate * lastLoggedInDate;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * salt;
@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSSet *userToAppointments;

@end

@interface User (CoreDataGeneratedAccessors)

- (void) addUserToAppointmentsObject:(Appointment *)value;
- (void) removeUserToAppointmentsObject:(Appointment *)value;
- (void) addUserToAppointments:(NSSet *)values;
- (void) removeUserToAppointments:(NSSet *)values;

@end
