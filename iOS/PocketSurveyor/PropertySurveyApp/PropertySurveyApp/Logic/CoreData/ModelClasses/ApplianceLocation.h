//
//  ApplianceLocation.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ApplianceLocation : NSManagedObject

@property (nonatomic, retain) NSNumber * locationID;
@property (nonatomic, retain) NSString * locationName;

@end
