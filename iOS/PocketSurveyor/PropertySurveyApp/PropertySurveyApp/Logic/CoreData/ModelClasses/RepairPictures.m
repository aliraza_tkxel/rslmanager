//
//  RepairPictures.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 25/02/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "RepairPictures.h"
#import "JobDataList.h"


@implementation RepairPictures

@dynamic createdBy;
@dynamic createdOn;
@dynamic image;
@dynamic imageId;
@dynamic imagePath;
@dynamic isBeforeImage;
@dynamic syncStatus;
@dynamic imageIdentifier;
@dynamic repairImagesToJobDataList;
@synthesize repairImage;
@end
