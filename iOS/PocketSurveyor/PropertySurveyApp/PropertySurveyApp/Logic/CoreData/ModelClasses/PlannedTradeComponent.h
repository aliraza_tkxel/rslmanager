//
//  PlannedTradeComponent.h
//  PropertySurveyApp
//
//  Created by Asif Nazir on 8/29/14.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, JobPauseData;

@interface PlannedTradeComponent : NSManagedObject

@property (nonatomic, retain) NSDate * completionDate;
@property (nonatomic, retain) NSNumber * componentId;
@property (nonatomic, retain) NSString * componentName;
@property (nonatomic, retain) NSNumber * componentTradeId;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSString * durationUnit;
@property (nonatomic, retain) NSString * jobStatus;
@property (nonatomic, retain) NSString * jsnNotes;
@property (nonatomic, retain) NSString * jsNumber;
@property (nonatomic, retain) NSString * pmoDescription;
@property (nonatomic, retain) NSDate * reportedDate;
@property (nonatomic, retain) NSNumber * sortingOrder;
@property (nonatomic, retain) NSString * tradeDescription;
@property (nonatomic, retain) NSNumber * tradeId;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * adaptation;
@property (nonatomic, retain) NSNumber * adaptationId;
@property (nonatomic, retain) NSNumber * locationId;
@property (nonatomic, retain) NSSet *jobPauseData;
@property (nonatomic, retain) Appointment *tradeComponentToAppointment;
@end

@interface PlannedTradeComponent (CoreDataGeneratedAccessors)

- (void)addJobPauseDataObject:(JobPauseData *)value;
- (void)removeJobPauseDataObject:(JobPauseData *)value;
- (void)addJobPauseData:(NSSet *)values;
- (void)removeJobPauseData:(NSSet *)values;

@end
