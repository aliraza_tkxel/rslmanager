//
//  Scheme.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 23/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

@interface Scheme : NSManagedObject

@property (nonatomic, retain) NSNumber * blockId;
@property (nonatomic, retain) NSNumber * schemeId;
@property (nonatomic, retain) NSString * blockName;
@property (nonatomic, retain) NSString * schemeName;
@property (nonatomic, retain) NSString * townCity;
@property (nonatomic, retain) NSString * postCode;
@property (nonatomic, retain) NSString * county;
@property (nonatomic, retain) Appointment *schemeToAppointment;

@end
