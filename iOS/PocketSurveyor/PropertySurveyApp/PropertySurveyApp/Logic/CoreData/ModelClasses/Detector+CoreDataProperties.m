//
//  Detector+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Detector+CoreDataProperties.h"

@implementation Detector (CoreDataProperties)

+ (NSFetchRequest<Detector *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Detector"];
}

@dynamic detectorCount;
@dynamic detectorId;
@dynamic detectorType;
@dynamic detectorTypeId;
@dynamic installedBy;
@dynamic installedDate;
@dynamic isInspected;
@dynamic isLandlordsDetector;
@dynamic location;
@dynamic manufacturer;
@dynamic notes;
@dynamic powerTypeId;
@dynamic propertyId;
@dynamic serialNumber;
@dynamic detectorDefects;
@dynamic detectorInspection;
@dynamic detectorToPowerType;
@dynamic detectorToProperty;

@end
