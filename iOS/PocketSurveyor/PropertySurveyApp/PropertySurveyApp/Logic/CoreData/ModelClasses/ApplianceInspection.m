//
//  ApplianceInspection.m
//  PropertySurveyApp
//
//  Created by TkXel on 26/03/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "ApplianceInspection.h"
#import "Appliance.h"


@implementation ApplianceInspection

@dynamic adequateVentilation;
@dynamic applianceSafeToUse;
@dynamic applianceServiced;
@dynamic combustionReading;
@dynamic fluePerformanceChecks;
@dynamic flueVisualCondition;
@dynamic inspectedBy;
@dynamic inspectionDate;
@dynamic inspectionID;
@dynamic isInspected;
@dynamic operatingPressure;
@dynamic operatingPressureUnit;
@dynamic safetyDeviceOperational;
@dynamic satisfactoryTermination;
@dynamic smokePellet;
@dynamic spillageTest;
@dynamic appliance;

@end
