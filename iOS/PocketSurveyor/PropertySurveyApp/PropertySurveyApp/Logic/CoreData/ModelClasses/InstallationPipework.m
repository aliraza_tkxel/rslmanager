//
//  InstallationPipework.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 19/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "InstallationPipework.h"
#import "Property+CoreDataClass.h"


@implementation InstallationPipework

@dynamic emergencyControl;
@dynamic equipotentialBonding;
@dynamic gasTightnessTest;
@dynamic visualInspection;
@dynamic isInspected;
@dynamic inspectionDate;
@dynamic inspectionID;
@dynamic property;

@end
