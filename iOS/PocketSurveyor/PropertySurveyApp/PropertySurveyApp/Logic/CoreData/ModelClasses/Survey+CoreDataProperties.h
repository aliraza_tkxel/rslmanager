//
//  Survey+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Survey+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Survey (CoreDataProperties)

+ (NSFetchRequest<Survey *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *surveyJSON;
@property (nullable, nonatomic, retain) Appointment *surveyToAppointment;
@property (nullable, nonatomic, retain) NSSet<SurveyData *> *surveyToSurveyData;

@end

@interface Survey (CoreDataGeneratedAccessors)

- (void)addSurveyToSurveyDataObject:(SurveyData *)value;
- (void)removeSurveyToSurveyDataObject:(SurveyData *)value;
- (void)addSurveyToSurveyData:(NSSet<SurveyData *> *)values;
- (void)removeSurveyToSurveyData:(NSSet<SurveyData *> *)values;

@end

NS_ASSUME_NONNULL_END
