//
//  Outbox.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Outbox : NSManagedObject

@property (nonatomic, retain) NSString * encodingType;
@property (nonatomic, retain) NSString * queryString;
@property (nonatomic, retain) NSDate * requestDateTime;
@property (nonatomic, retain) NSString * requestHeader;
@property (nonatomic, retain) NSString * requestType;
@property (nonatomic, retain) NSString * requestURL;

@end
