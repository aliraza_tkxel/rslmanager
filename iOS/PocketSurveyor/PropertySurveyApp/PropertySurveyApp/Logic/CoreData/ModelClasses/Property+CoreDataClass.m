//
//  Property+CoreDataClass.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Property+CoreDataClass.h"
#import "Accomodation.h"

#import "Appliance.h"

#import "Appointment+CoreDataClass.h"

#import "Detector+CoreDataClass.h"

#import "InstallationPipework.h"

#import "PropertyAsbestosData.h"

#import "PropertyPicture+CoreDataClass.h"

@implementation Property

@end
