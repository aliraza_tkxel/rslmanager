//
//  JobPauseData.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 12/12/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "JobPauseData.h"
#import "JobDataList.h"
#import "JobPauseReason.h"


@implementation JobPauseData

@dynamic pauseDate;
@dynamic pausedBy;
@dynamic pauseNote;
@dynamic actionType;
@dynamic jobData;
@dynamic pauseReason;

@end
