//
//  Scheme.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 23/01/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "Scheme.h"
#import "Appointment+CoreDataClass.h"


@implementation Scheme

@dynamic blockId;
@dynamic schemeId;
@dynamic blockName;
@dynamic schemeName;
@dynamic townCity;
@dynamic postCode;
@dynamic county;
@dynamic schemeToAppointment;

@end
