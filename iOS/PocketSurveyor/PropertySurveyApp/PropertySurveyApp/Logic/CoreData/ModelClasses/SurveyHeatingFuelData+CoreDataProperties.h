//
//  SurveyHeatingFuelData+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SurveyHeatingFuelData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SurveyHeatingFuelData (CoreDataProperties)

+ (NSFetchRequest<SurveyHeatingFuelData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *fuelTypeId;
@property (nullable, nonatomic, copy) NSString *heatingFuelSurvey;
@property (nullable, nonatomic, retain) Appointment *heatingFuelSurveyToAppointment;

@end

NS_ASSUME_NONNULL_END
