//
//  ApplianceType.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "ApplianceType.h"


@implementation ApplianceType

@dynamic typeID;
@dynamic typeName;

@end
