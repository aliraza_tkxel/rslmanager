//
//  PropertyAsbestosData.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Property;

@interface PropertyAsbestosData : NSManagedObject

@property (nonatomic, retain) NSNumber * asbestosId;
@property (nonatomic, retain) NSString * asbRiskLevelDesc;
@property (nonatomic, retain) NSString * riskDesc;
@property (nonatomic, retain) Property *propertyAsbestosDataToProperty;

@end
