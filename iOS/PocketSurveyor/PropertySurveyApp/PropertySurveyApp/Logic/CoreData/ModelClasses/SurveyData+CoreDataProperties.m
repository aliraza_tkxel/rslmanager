//
//  SurveyData+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 24/04/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SurveyData+CoreDataProperties.h"

@implementation SurveyData (CoreDataProperties)

+ (NSFetchRequest<SurveyData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SurveyData"];
}

@dynamic appointmentId;
@dynamic customerId;
@dynamic heatingTypeId;
@dynamic itemId;
@dynamic propertyId;
@dynamic surveyFormName;
@dynamic surveyNotesDate;
@dynamic surveyNotesDetail;
@dynamic surveyDataToFormData;
@dynamic surveyDataToSurvey;

@end
