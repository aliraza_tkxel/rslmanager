//
//  Outbox.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Outbox.h"


@implementation Outbox

@dynamic encodingType;
@dynamic queryString;
@dynamic requestDateTime;
@dynamic requestHeader;
@dynamic requestType;
@dynamic requestURL;

@end
