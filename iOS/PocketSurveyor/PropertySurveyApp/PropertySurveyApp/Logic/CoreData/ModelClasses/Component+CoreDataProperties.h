//
//  Component+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Component+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Component (CoreDataProperties)

+ (NSFetchRequest<Component *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *componentId;
@property (nullable, nonatomic, copy) NSString *componentName;
@property (nullable, nonatomic, copy) NSNumber *cycle;
@property (nullable, nonatomic, copy) NSString *frequency;
@property (nullable, nonatomic, copy) NSNumber *isComponentAccounting;
@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, copy) NSNumber *parameterId;
@property (nullable, nonatomic, copy) NSNumber *subParameterId;
@property (nullable, nonatomic, copy) NSNumber *subValueId;
@property (nullable, nonatomic, copy) NSNumber *valueId;
@property (nullable, nonatomic, retain) Appointment *componentToAppointment;

@end

NS_ASSUME_NONNULL_END
