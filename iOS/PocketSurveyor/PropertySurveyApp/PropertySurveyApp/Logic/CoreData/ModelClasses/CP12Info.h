	//
//  CP12Info.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

@interface CP12Info : NSManagedObject

@property (nonatomic, retain) NSString * cp12Document;
@property (nonatomic, retain) NSString * cp12Number;
@property (nonatomic, retain) NSDate * dateTimeStamp;
@property (nonatomic, retain) NSString * documentType;
@property (nonatomic, retain) NSNumber * inspectionCarried;
@property (nonatomic, retain) NSNumber * issuedBy;
@property (nonatomic, retain) NSDate * issuedDate;
@property (nonatomic, retain) NSString * issuedDateString;
@property (nonatomic, retain) NSNumber * journalId;
@property (nonatomic, retain) NSNumber * jsgNumber;
@property (nonatomic, retain) NSNumber * lsgrid;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSString * propertyID;
@property (nonatomic, retain) NSDate * receivedDate;
@property (nonatomic, retain) NSString * receivedDateString;
@property (nonatomic, retain) NSString * receivedOnBehalfOf;
@property (nonatomic, retain) NSNumber * tenantHanded;
@property (nonatomic, retain) Appointment *cp12InfoToAppointment;

@end
