//
//  PropertyAsbestosData.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PropertyAsbestosData.h"
#import "Property+CoreDataClass.h"


@implementation PropertyAsbestosData

@dynamic asbestosId;
@dynamic asbRiskLevelDesc;
@dynamic riskDesc;
@dynamic propertyAsbestosDataToProperty;

@end
