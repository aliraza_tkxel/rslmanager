//
//  SurveyHeatingFuelData+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

NS_ASSUME_NONNULL_BEGIN

@interface SurveyHeatingFuelData : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SurveyHeatingFuelData+CoreDataProperties.h"
