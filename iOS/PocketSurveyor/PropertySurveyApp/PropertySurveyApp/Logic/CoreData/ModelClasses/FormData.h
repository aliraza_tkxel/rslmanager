//
//  FormData.h
//  PropertySurveyApp
//
//  Created by TkXel on 18/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SurveyData;

@interface FormData : NSManagedObject

@property (nonatomic, retain) NSString * controlType;
@property (nonatomic, retain) NSNumber * isSelected;
@property (nonatomic, retain) NSNumber * surveyItemParamId;
@property (nonatomic, retain) NSNumber * surveyParameterId;
@property (nonatomic, retain) id surveyParamItemFieldValue;
@property (nonatomic, retain) NSString * surveyParamName;
@property (nonatomic, retain) NSNumber * surveyPramItemFieldId;
@property (nonatomic, retain) SurveyData *formDataToSurveyData;

@end
