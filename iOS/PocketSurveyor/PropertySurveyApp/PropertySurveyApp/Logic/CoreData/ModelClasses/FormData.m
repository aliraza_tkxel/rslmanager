//
//  FormData.m
//  PropertySurveyApp
//
//  Created by TkXel on 18/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "FormData.h"
#import "SurveyData+CoreDataClass.h"


@implementation FormData

@dynamic controlType;
@dynamic isSelected;
@dynamic surveyItemParamId;
@dynamic surveyParameterId;
@dynamic surveyParamItemFieldValue;
@dynamic surveyParamName;
@dynamic surveyPramItemFieldId;
@dynamic formDataToSurveyData;

@end
