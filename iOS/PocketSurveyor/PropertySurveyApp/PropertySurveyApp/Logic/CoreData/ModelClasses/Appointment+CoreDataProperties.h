//
//  Appointment+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Appointment+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Appointment (CoreDataProperties)

+ (NSFetchRequest<Appointment *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *addToCalendar;
@property (nullable, nonatomic, copy) NSString *appointmentCalendar;
@property (nullable, nonatomic, copy) NSDate *appointmentDate;
@property (nullable, nonatomic, copy) NSString *appointmentDateSectionIdentifier;
@property (nullable, nonatomic, copy) NSDate *appointmentEndTime;
@property (nullable, nonatomic, copy) NSString *appointmentEventIdentifier;
@property (nullable, nonatomic, copy) NSNumber *appointmentId;
@property (nullable, nonatomic, copy) NSString *appointmentLocation;
@property (nullable, nonatomic, copy) NSString *appointmentNotes;
@property (nullable, nonatomic, copy) NSNumber *appointmentOverdue;
@property (nullable, nonatomic, copy) NSString *appointmentShift;
@property (nullable, nonatomic, copy) NSDate *appointmentStartTime;
@property (nullable, nonatomic, copy) NSString *appointmentStatus;
@property (nullable, nonatomic, copy) NSString *appointmentTitle;
@property (nullable, nonatomic, copy) NSString *appointmentType;
@property (nullable, nonatomic, copy) NSString *aptCompletedAppVersionInfo;
@property (nullable, nonatomic, copy) NSNumber *assignedTo;
@property (nullable, nonatomic, copy) NSNumber *createdBy;
@property (nullable, nonatomic, copy) NSString *createdByPerson;
@property (nullable, nonatomic, copy) NSNumber *defaultCustomerId;
@property (nullable, nonatomic, copy) NSNumber *defaultCustomerIndex;
@property (nullable, nonatomic, copy) NSString *failedReason;
@property (nullable, nonatomic, copy) NSNumber *isMiscAppointment;
@property (nullable, nonatomic, copy) NSNumber *isModified;
@property (nullable, nonatomic, copy) NSNumber *isOfflinePrepared;
@property (nullable, nonatomic, copy) NSNumber *isSurveyChanged;
@property (nullable, nonatomic, copy) NSNumber *journalHistoryId;
@property (nullable, nonatomic, copy) NSNumber *journalId;
@property (nullable, nonatomic, copy) NSNumber *jsgNumber;
@property (nullable, nonatomic, copy) NSDate *loggedDate;
@property (nullable, nonatomic, copy) NSString *noEntryNotes;
@property (nullable, nonatomic, copy) NSDate *repairCompletionDateTime;
@property (nullable, nonatomic, copy) NSString *repairNotes;
@property (nullable, nonatomic, retain) NSObject *surveyCheckListForm;
@property (nullable, nonatomic, copy) NSString *surveyorAlert;
@property (nullable, nonatomic, copy) NSString *surveyorUserName;
@property (nullable, nonatomic, copy) NSString *surveyourAvailability;
@property (nullable, nonatomic, copy) NSString *surveyType;
@property (nullable, nonatomic, copy) NSNumber *syncStatus;
@property (nullable, nonatomic, copy) NSNumber *tenancyId;
@property (nullable, nonatomic, retain) AppInfoData *appointmentToAppInfoData;
@property (nullable, nonatomic, retain) CP12Info *appointmentToCP12Info;
@property (nullable, nonatomic, retain) NSSet<Customer *> *appointmentToCustomer;
@property (nullable, nonatomic, retain) NSSet<JobDataList *> *appointmentToJobDataList;
@property (nullable, nonatomic, retain) Journal *appointmentToJournal;
@property (nullable, nonatomic, retain) PlannedTradeComponent *appointmentToPlannedComponent;
@property (nullable, nonatomic, retain) Property *appointmentToProperty;
@property (nullable, nonatomic, retain) Scheme *appointmentToScheme;
@property (nullable, nonatomic, retain) Survey *appointmentToSurvey;
@property (nullable, nonatomic, retain) User *appointmentToUser;
@property (nullable, nonatomic, retain) NSSet<Component *> *appointmentToSurveyComponents;
@property (nullable, nonatomic, retain) NSSet<SurveyHeatingFuelCertificate *> *appointmentToCertificateTemplates;
@property (nullable, nonatomic, retain) NSSet<SurveyHeatingFuelData *> *appointmentToSurveyTemplates;

@end

@interface Appointment (CoreDataGeneratedAccessors)

- (void)addAppointmentToCustomerObject:(Customer *)value;
- (void)removeAppointmentToCustomerObject:(Customer *)value;
- (void)addAppointmentToCustomer:(NSSet<Customer *> *)values;
- (void)removeAppointmentToCustomer:(NSSet<Customer *> *)values;

- (void)addAppointmentToJobDataListObject:(JobDataList *)value;
- (void)removeAppointmentToJobDataListObject:(JobDataList *)value;
- (void)addAppointmentToJobDataList:(NSSet<JobDataList *> *)values;
- (void)removeAppointmentToJobDataList:(NSSet<JobDataList *> *)values;

- (void)addAppointmentToSurveyComponentsObject:(Component *)value;
- (void)removeAppointmentToSurveyComponentsObject:(Component *)value;
- (void)addAppointmentToSurveyComponents:(NSSet<Component *> *)values;
- (void)removeAppointmentToSurveyComponents:(NSSet<Component *> *)values;

- (void)addAppointmentToCertificateTemplatesObject:(SurveyHeatingFuelCertificate *)value;
- (void)removeAppointmentToCertificateTemplatesObject:(SurveyHeatingFuelCertificate *)value;
- (void)addAppointmentToCertificateTemplates:(NSSet<SurveyHeatingFuelCertificate *> *)values;
- (void)removeAppointmentToCertificateTemplates:(NSSet<SurveyHeatingFuelCertificate *> *)values;

- (void)addAppointmentToSurveyTemplatesObject:(SurveyHeatingFuelData *)value;
- (void)removeAppointmentToSurveyTemplatesObject:(SurveyHeatingFuelData *)value;
- (void)addAppointmentToSurveyTemplates:(NSSet<SurveyHeatingFuelData *> *)values;
- (void)removeAppointmentToSurveyTemplates:(NSSet<SurveyHeatingFuelData *> *)values;

@end

NS_ASSUME_NONNULL_END
