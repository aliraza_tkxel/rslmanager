//
//  CP12Info.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "CP12Info.h"
#import "Appointment+CoreDataClass.h"


@implementation CP12Info

@dynamic cp12Document;
@dynamic cp12Number;
@dynamic dateTimeStamp;
@dynamic documentType;
@dynamic inspectionCarried;
@dynamic issuedBy;
@dynamic issuedDate;
@dynamic issuedDateString;
@dynamic journalId;
@dynamic jsgNumber;
@dynamic lsgrid;
@dynamic notes;
@dynamic propertyID;
@dynamic receivedDate;
@dynamic receivedDateString;
@dynamic receivedOnBehalfOf;
@dynamic tenantHanded;
@dynamic cp12InfoToAppointment;

@end
