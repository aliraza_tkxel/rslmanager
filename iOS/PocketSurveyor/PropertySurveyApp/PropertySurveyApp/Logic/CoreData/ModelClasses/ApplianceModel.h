//
//  ApplianceModel.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ApplianceModel : NSManagedObject

@property (nonatomic, retain) NSNumber * modelID;
@property (nonatomic, retain) NSString * modelName;

@end
