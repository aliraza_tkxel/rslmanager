//
//  PowerType+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PowerType+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PowerType (CoreDataProperties)

+ (NSFetchRequest<PowerType *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *powerType;
@property (nullable, nonatomic, copy) NSNumber *powerTypeId;

@end

NS_ASSUME_NONNULL_END
