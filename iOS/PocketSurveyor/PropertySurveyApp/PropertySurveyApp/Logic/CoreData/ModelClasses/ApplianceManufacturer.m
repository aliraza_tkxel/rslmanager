//
//  ApplianceManufacturer.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 07/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "ApplianceManufacturer.h"


@implementation ApplianceManufacturer

@dynamic manufacturerID;
@dynamic manufacturerName;

@end
