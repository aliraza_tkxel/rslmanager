//
//  DetectorInspection.h
//  PropertySurveyApp
//
//  Created by Yawar on 23/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Detector;

@interface DetectorInspection : NSManagedObject

@property (nonatomic, retain) NSString * detectorsTested;
@property (nonatomic, retain) NSNumber * inspectionID;
@property (nonatomic, retain) NSNumber * inspectedBy;
@property (nonatomic, retain) NSDate * inspectionDate;
@property (nonatomic, retain) Detector *detector;

@end
