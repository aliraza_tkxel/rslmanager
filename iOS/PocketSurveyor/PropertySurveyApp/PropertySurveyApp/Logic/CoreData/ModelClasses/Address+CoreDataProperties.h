//
//  Address+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Address+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Address (CoreDataProperties)

+ (NSFetchRequest<Address *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSNumber *addressID;
@property (nullable, nonatomic, copy) NSString *houseNo;
@property (nullable, nonatomic, copy) NSString *postCode;
@property (nullable, nonatomic, copy) NSString *townCity;
@property (nullable, nonatomic, copy) NSString *completeAddress;
@property (nullable, nonatomic, retain) Customer *addressToCustomer;

@end

NS_ASSUME_NONNULL_END
