//
//  Journal.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Journal.h"
#import "Appointment+CoreDataClass.h"


@implementation Journal

@dynamic actionId;
@dynamic creationBy;
@dynamic creationDate;
@dynamic inspectionTypeId;
@dynamic isCurrent;
@dynamic journalId;
@dynamic propertyId;
@dynamic statusId;
@dynamic journalToAppointment;

@end
