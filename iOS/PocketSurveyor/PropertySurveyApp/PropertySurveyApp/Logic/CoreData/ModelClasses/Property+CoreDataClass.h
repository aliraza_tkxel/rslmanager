//
//  Property+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Accomodation, Appliance, Appointment, Detector, InstallationPipework, PropertyAsbestosData, PropertyPicture;

NS_ASSUME_NONNULL_BEGIN

@interface Property : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Property+CoreDataProperties.h"
