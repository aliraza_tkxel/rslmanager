//
//  FaultRepairListHistory.h
//  PropertySurveyApp
//
//  Created by Yawar on 14/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobDataList;

@interface FaultRepairListHistory : NSManagedObject

@property (nonatomic, retain) NSString * faultRepairDescription;
@property (nonatomic, retain) NSNumber * faultRepairID;
@property (nonatomic, retain) JobDataList *faultRepairListHistoryToJobDataList;

@end
