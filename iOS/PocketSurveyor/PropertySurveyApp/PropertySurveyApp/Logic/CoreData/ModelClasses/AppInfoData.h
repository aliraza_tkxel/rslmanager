//
//  AppInfoData.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

@interface AppInfoData : NSManagedObject

@property (nonatomic, retain) NSNumber * totalAppointments;
@property (nonatomic, retain) NSNumber * totalNoEntries;
@property (nonatomic, retain) NSNumber * isCardLeft;
@property (nonatomic, retain) Appointment *appInfoDataToAppointment;

@end
