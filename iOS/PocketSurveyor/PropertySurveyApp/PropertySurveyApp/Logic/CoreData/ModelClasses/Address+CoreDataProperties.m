//
//  Address+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Address+CoreDataProperties.h"

@implementation Address (CoreDataProperties)

+ (NSFetchRequest<Address *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Address"];
}

@dynamic address;
@dynamic addressID;
@dynamic houseNo;
@dynamic postCode;
@dynamic townCity;
@dynamic completeAddress;
@dynamic addressToCustomer;

@end
