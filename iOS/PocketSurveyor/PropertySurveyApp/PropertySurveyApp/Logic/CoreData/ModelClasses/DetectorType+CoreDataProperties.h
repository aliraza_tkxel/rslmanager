//
//  DetectorType+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DetectorType+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DetectorType (CoreDataProperties)

+ (NSFetchRequest<DetectorType *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *detectorType;
@property (nullable, nonatomic, copy) NSNumber *detectorTypeId;

@end

NS_ASSUME_NONNULL_END
