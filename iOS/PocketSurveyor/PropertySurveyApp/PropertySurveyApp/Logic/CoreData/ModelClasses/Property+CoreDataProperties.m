//
//  Property+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Property+CoreDataProperties.h"

@implementation Property (CoreDataProperties)

+ (NSFetchRequest<Property *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Property"];
}

@dynamic address1;
@dynamic address2;
@dynamic address3;
@dynamic certificateExpiry;
@dynamic county;
@dynamic flatNumber;
@dynamic houseNumber;
@dynamic lastSurveyDate;
@dynamic postCode;
@dynamic propertyId;
@dynamic tenancyId;
@dynamic townCity;
@dynamic completeAddress;
@dynamic defaultPicture;
@dynamic installationPipework;
@dynamic propertyToAccomodation;
@dynamic propertyToAppliances;
@dynamic propertyToAppointment;
@dynamic propertyToDetector;
@dynamic propertyToPropertyAsbestosData;
@dynamic propertyToPropertyPicture;

@end
