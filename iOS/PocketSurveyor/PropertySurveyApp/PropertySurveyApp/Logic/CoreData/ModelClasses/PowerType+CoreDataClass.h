//
//  PowerType+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PowerType : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "PowerType+CoreDataProperties.h"
