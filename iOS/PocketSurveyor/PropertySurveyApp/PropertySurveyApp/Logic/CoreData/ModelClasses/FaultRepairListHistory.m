//
//  FaultRepairListHistory.m
//  PropertySurveyApp
//
//  Created by Yawar on 14/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "FaultRepairListHistory.h"
#import "JobDataList.h"


@implementation FaultRepairListHistory

@dynamic faultRepairDescription;
@dynamic faultRepairID;
@dynamic faultRepairListHistoryToJobDataList;

@end
