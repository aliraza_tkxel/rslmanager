//
//  InstallationPipework.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 19/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Property;

@interface InstallationPipework : NSManagedObject

@property (nonatomic, retain) NSString * emergencyControl;
@property (nonatomic, retain) NSString * equipotentialBonding;
@property (nonatomic, retain) NSString * gasTightnessTest;
@property (nonatomic, retain) NSString * visualInspection;
@property (nonatomic, retain) NSNumber * isInspected;
@property (nonatomic, retain) NSDate * inspectionDate;
@property (nonatomic, retain) NSNumber * inspectionID;
@property (nonatomic, retain) Property *property;

@end
