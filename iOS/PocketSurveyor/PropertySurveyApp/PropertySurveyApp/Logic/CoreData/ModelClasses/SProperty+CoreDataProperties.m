//
//  SProperty+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "SProperty+CoreDataProperties.h"

@implementation SProperty (CoreDataProperties)

+ (NSFetchRequest<SProperty *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SProperty"];
}

@dynamic address1;
@dynamic address2;
@dynamic address3;
@dynamic certificateExpiry;
@dynamic county;
@dynamic firstName;
@dynamic flatNumber;
@dynamic fullName;
@dynamic houseNumber;
@dynamic lastName;
@dynamic middleName;
@dynamic postCode;
@dynamic propertyId;
@dynamic sPropertyJSON;
@dynamic sPropertyNameSectionIdentifier;
@dynamic title;
@dynamic townCity;
@dynamic completeName;
@dynamic completeAddress;
@dynamic primitiveSPropertyNameSectionIdentifier;

- (NSString *) fullName
{
    NSMutableString *customerName =[NSMutableString stringWithString:@""];
    if(!isEmpty(self.title)){
        [customerName appendString:self.title];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.firstName)){
        [customerName appendString:self.firstName];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.middleName)){
        [customerName appendString:self.middleName];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.lastName))
        [customerName appendString:self.lastName];
    return customerName;
}

- (NSString *) fullNameWithoutTitle
{
    NSMutableString *customerName =[NSMutableString stringWithString:@""];
    
    if(!isEmpty(self.firstName)){
        [customerName appendString:self.firstName];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.middleName)){
        [customerName appendString:self.middleName];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.lastName))
        [customerName appendString:self.lastName];
    return customerName;
}

- (NSString *) SPropertyAddress
{
    NSMutableString *propertyAddress = [NSMutableString stringWithString:@""];
    
    if (!isEmpty(self.houseNumber)) {
        [propertyAddress appendString:self.houseNumber];
    }
    
    if(!isEmpty(self.address1)){
        [propertyAddress appendString:@" "];
        [propertyAddress appendString:self.address1];
    }
    
    if (!isEmpty(self.address2)) {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.address2];
        
    }
    
    if(!isEmpty(self.address3)){
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.address3];
    }
    
    if (!isEmpty(self.townCity))
    {
        [propertyAddress appendString:@"\n"];
        [propertyAddress appendString:self.townCity];
    }
    
    if (!isEmpty(self.postCode)) {
        [propertyAddress appendString:@", "];
        [propertyAddress appendString:self.postCode];
    }
    
    return propertyAddress;
}

- (NSString *) propertyShortAddress
{
    NSString *address = nil;
    if (!isEmpty(self.houseNumber) && !isEmpty(self.address1))
    {
        address = [NSString stringWithFormat:@"%@ %@",self.houseNumber, self.address1];
    }
    return address;
}

/*!
 @discussion
 Surveyor List Section Identifier, Generated at Runtime. FetchedResultsController uses this idetifier to form the surveyor sections.
 */
- (NSString *) sPropertyNameSectionIdentifier {
    
    // Create and cache the section identifier on demand.
    
    [self willAccessValueForKey:kSPropertyNameSectionIdentifier];
    NSString *sectionIdentifier = [self primitiveSPropertyNameSectionIdentifier];
    [self didAccessValueForKey:kSPropertyNameSectionIdentifier];
    NSString *fullName = [self fullNameWithoutTitle];
    if (!sectionIdentifier)
    {
        sectionIdentifier = [[fullName substringToIndex:1] uppercaseString];
        [self setPrimitiveSPropertyNameSectionIdentifier:sectionIdentifier];
    }
    return sectionIdentifier;
}

#pragma mark -
#pragma mark Key path Dependencies

+ (NSSet *)keyPathsForValuesAffectingSectionIdentifier {
    // If the value of timeStamp changes, the section identifier may change as well.
    return [NSSet setWithObject:kFullName];
}


@end
