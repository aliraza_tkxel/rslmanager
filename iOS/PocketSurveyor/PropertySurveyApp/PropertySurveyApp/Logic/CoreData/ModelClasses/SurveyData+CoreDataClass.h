//
//  SurveyData+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 24/04/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FormData, Survey;

NS_ASSUME_NONNULL_BEGIN

@interface SurveyData : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "SurveyData+CoreDataProperties.h"
