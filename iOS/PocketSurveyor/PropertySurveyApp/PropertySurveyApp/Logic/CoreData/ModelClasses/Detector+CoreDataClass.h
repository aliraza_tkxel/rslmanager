//
//  Detector+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/17/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Defect, DetectorInspection, PowerType, Property;

NS_ASSUME_NONNULL_BEGIN

@interface Detector : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Detector+CoreDataProperties.h"
