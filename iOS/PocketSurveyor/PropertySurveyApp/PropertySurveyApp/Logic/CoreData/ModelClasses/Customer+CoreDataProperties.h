//
//  Customer+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Customer+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Customer (CoreDataProperties)

+ (NSFetchRequest<Customer *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSNumber *customerId;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSString *fax;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, copy) NSString *middleName;
@property (nullable, nonatomic, copy) NSString *mobile;
@property (nullable, nonatomic, copy) NSString *property;
@property (nullable, nonatomic, copy) NSString *telephone;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *completeName;
@property (nullable, nonatomic, retain) Address *customerToAddress;
@property (nullable, nonatomic, retain) Appointment *customerToAppointment;
@property (nullable, nonatomic, retain) NSSet<CustomerRiskData *> *customerToCustomerRiskData;
@property (nullable, nonatomic, retain) NSSet<CustomerVulnerabilityData *> *customerToCustomerVulunarityData;

@end

@interface Customer (CoreDataGeneratedAccessors)

- (void)addCustomerToCustomerRiskDataObject:(CustomerRiskData *)value;
- (void)removeCustomerToCustomerRiskDataObject:(CustomerRiskData *)value;
- (void)addCustomerToCustomerRiskData:(NSSet<CustomerRiskData *> *)values;
- (void)removeCustomerToCustomerRiskData:(NSSet<CustomerRiskData *> *)values;

- (void)addCustomerToCustomerVulunarityDataObject:(CustomerVulnerabilityData *)value;
- (void)removeCustomerToCustomerVulunarityDataObject:(CustomerVulnerabilityData *)value;
- (void)addCustomerToCustomerVulunarityData:(NSSet<CustomerVulnerabilityData *> *)values;
- (void)removeCustomerToCustomerVulunarityData:(NSSet<CustomerVulnerabilityData *> *)values;

- (NSString *) fullName;

@end

NS_ASSUME_NONNULL_END
