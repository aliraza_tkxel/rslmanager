//
//  Defect.m
//  PropertySurveyApp
//
//  Created by Yawar on 24/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Defect.h"
#import "Appliance.h"
#import "DefectCategory.h"
#import "DefectPicture.h"
#import "Detector+CoreDataClass.h"


@implementation Defect

@dynamic applianceID;
@dynamic defectDate;
@dynamic defectDescription;
@dynamic defectID;
@dynamic defectType;
@dynamic faultCategory;
@dynamic isActionTaken;
@dynamic isAdviceNoteIssued;
@dynamic isDefectIdentified;
@dynamic journalId;
@dynamic propertyId;
@dynamic remedialAction;
@dynamic serialNo;
@dynamic detectorTypeId;
@dynamic warningTagFixed;
@dynamic defectCategory;
@dynamic defectPictures;
@dynamic defectToAppliance;
@dynamic defectToDetector;

@end
