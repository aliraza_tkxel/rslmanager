//
//  SurveyData+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 24/04/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SurveyData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SurveyData (CoreDataProperties)

+ (NSFetchRequest<SurveyData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *appointmentId;
@property (nullable, nonatomic, copy) NSNumber *customerId;
@property (nullable, nonatomic, copy) NSNumber *heatingTypeId;
@property (nullable, nonatomic, copy) NSNumber *itemId;
@property (nullable, nonatomic, copy) NSString *propertyId;
@property (nullable, nonatomic, copy) NSString *surveyFormName;
@property (nullable, nonatomic, copy) NSDate *surveyNotesDate;
@property (nullable, nonatomic, copy) NSString *surveyNotesDetail;
@property (nullable, nonatomic, retain) NSSet<FormData *> *surveyDataToFormData;
@property (nullable, nonatomic, retain) Survey *surveyDataToSurvey;

@end

@interface SurveyData (CoreDataGeneratedAccessors)

- (void)addSurveyDataToFormDataObject:(FormData *)value;
- (void)removeSurveyDataToFormDataObject:(FormData *)value;
- (void)addSurveyDataToFormData:(NSSet<FormData *> *)values;
- (void)removeSurveyDataToFormData:(NSSet<FormData *> *)values;

@end

NS_ASSUME_NONNULL_END
