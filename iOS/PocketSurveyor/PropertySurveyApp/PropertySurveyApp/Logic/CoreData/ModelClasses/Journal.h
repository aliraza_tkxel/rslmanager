//
//  Journal.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment;

@interface Journal : NSManagedObject

@property (nonatomic, retain) NSNumber * actionId;
@property (nonatomic, retain) NSNumber * creationBy;
@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) NSNumber * inspectionTypeId;
@property (nonatomic, retain) NSNumber * isCurrent;
@property (nonatomic, retain) NSNumber * journalId;
@property (nonatomic, retain) NSString * propertyId;
@property (nonatomic, retain) NSNumber * statusId;
@property (nonatomic, retain) Appointment *journalToAppointment;

@end
