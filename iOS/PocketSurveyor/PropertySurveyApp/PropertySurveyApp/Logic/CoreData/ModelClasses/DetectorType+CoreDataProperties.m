//
//  DetectorType+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "DetectorType+CoreDataProperties.h"

@implementation DetectorType (CoreDataProperties)

+ (NSFetchRequest<DetectorType *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"DetectorType"];
}

@dynamic detectorType;
@dynamic detectorTypeId;

@end
