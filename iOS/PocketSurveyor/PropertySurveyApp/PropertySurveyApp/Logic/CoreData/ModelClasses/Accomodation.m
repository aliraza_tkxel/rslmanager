//
//  Accomodation.m
//  PropertySurveyApp
//
//  Created by Yawar on 03/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Accomodation.h"
#import "Property+CoreDataClass.h"


@implementation Accomodation

@dynamic propertyId;
@dynamic roomHeight;
@dynamic roomLength;
@dynamic roomName;
@dynamic roomWidth;
@dynamic updatedBy;
@dynamic updatedOn;
@dynamic accomodationToProperty;

@end
