//
//  JobPauseData.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 12/12/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobDataList, JobPauseReason;

@interface JobPauseData : NSManagedObject

@property (nonatomic, retain) NSDate * pauseDate;
@property (nonatomic, retain) NSNumber * pausedBy;
@property (nonatomic, retain) NSString * pauseNote;
@property (nonatomic, retain) NSString * actionType;
@property (nonatomic, retain) JobDataList *jobData;
@property (nonatomic, retain) JobPauseReason *pauseReason;

@end
