//
//  PowerType+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 8/10/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "PowerType+CoreDataProperties.h"

@implementation PowerType (CoreDataProperties)

+ (NSFetchRequest<PowerType *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PowerType"];
}

@dynamic powerType;
@dynamic powerTypeId;

@end
