//
//  SurveyHeatingFuelCertificate+CoreDataProperties.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SurveyHeatingFuelCertificate+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SurveyHeatingFuelCertificate (CoreDataProperties)

+ (NSFetchRequest<SurveyHeatingFuelCertificate *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *certificateId;
@property (nullable, nonatomic, copy) NSString *certificateName;
@property (nullable, nonatomic, copy) NSNumber *fuelTypeId;
@property (nullable, nonatomic, retain) Appointment *heatingFuelCertificateToAppointment;

@end

NS_ASSUME_NONNULL_END
