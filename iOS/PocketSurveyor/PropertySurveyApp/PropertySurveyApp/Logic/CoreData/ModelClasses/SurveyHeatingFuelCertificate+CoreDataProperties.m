//
//  SurveyHeatingFuelCertificate+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "SurveyHeatingFuelCertificate+CoreDataProperties.h"

@implementation SurveyHeatingFuelCertificate (CoreDataProperties)

+ (NSFetchRequest<SurveyHeatingFuelCertificate *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"SurveyHeatingFuelCertificate"];
}

@dynamic certificateId;
@dynamic certificateName;
@dynamic fuelTypeId;
@dynamic heatingFuelCertificateToAppointment;

@end
