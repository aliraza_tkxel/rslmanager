//
//  Customer+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import "Customer+CoreDataProperties.h"

@implementation Customer (CoreDataProperties)

+ (NSFetchRequest<Customer *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Customer"];
}

@dynamic address;
@dynamic customerId;
@dynamic email;
@dynamic fax;
@dynamic firstName;
@dynamic lastName;
@dynamic middleName;
@dynamic mobile;
@dynamic property;
@dynamic telephone;
@dynamic title;
@dynamic completeName;
@dynamic customerToAddress;
@dynamic customerToAppointment;
@dynamic customerToCustomerRiskData;
@dynamic customerToCustomerVulunarityData;
- (NSString *)  fullName
{
    /*NSMutableString *customerName =[NSMutableString stringWithString:@""];
    if(!isEmpty(self.title)){
        [customerName appendString:self.title];
        [customerName appendString:@"."];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.firstName)){
        [customerName appendString:self.firstName];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.middleName)){
        [customerName appendString:self.middleName];
        [customerName appendString:@" "];
    }
    if(!isEmpty(self.lastName))
        [customerName appendString:self.lastName];*/
    return !isEmpty(self.completeName)?self.completeName:@"";
}

@end
