//
//  DefectPicture.m
//  PropertySurveyApp
//
//  Created by M.Mahmood on 25/02/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import "DefectPicture.h"
#import "Defect.h"


@implementation DefectPicture

@dynamic createdBy;
@dynamic createdOn;
@dynamic defectPictureId;
@dynamic imagePath;
@dynamic synchStatus;
@dynamic imageIdentifier;
@dynamic defectPictureToDefect;
@synthesize image;

@end
