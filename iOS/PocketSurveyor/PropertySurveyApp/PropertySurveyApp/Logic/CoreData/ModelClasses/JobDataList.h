//
//  JobDataList.h
//  PropertySurveyApp
//
//  Created by Yawar on 02/04/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appointment, FaultRepairData, FaultRepairListHistory, JobPauseData, RepairPictures;

@interface JobDataList : NSManagedObject

@property (nonatomic, retain) NSDate * completionDate;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSString * durationUnit;
@property (nonatomic, retain) NSNumber * faultLogID;
@property (nonatomic, retain) NSString * followOnNotes;
@property (nonatomic, retain) NSString * jobStatus;
@property (nonatomic, retain) NSString * jsnDescription;
@property (nonatomic, retain) NSString * jsnLocation;
@property (nonatomic, retain) NSString * jsnNotes;
@property (nonatomic, retain) NSString * jsNumber;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSString * repairNotes;
@property (nonatomic, retain) NSDate * reportedDate;
@property (nonatomic, retain) NSString * responseTime;
@property (nonatomic, retain) NSSet *faultRepairDataList;
@property (nonatomic, retain) NSSet *faultRepairHistoryList;
@property (nonatomic, retain) Appointment *jobDataListToAppointment;
@property (nonatomic, retain) NSSet *jobPauseData;
@property (nonatomic, retain) NSSet *jobDataListToRepairImages;

- (NSString *)getFaultDescription;

@end

@interface JobDataList (CoreDataGeneratedAccessors)

- (void)addFaultRepairDataListObject:(FaultRepairData *)value;
- (void)removeFaultRepairDataListObject:(FaultRepairData *)value;
- (void)addFaultRepairDataList:(NSSet *)values;
- (void)removeFaultRepairDataList:(NSSet *)values;

- (void)addFaultRepairHistoryListObject:(FaultRepairListHistory *)value;
- (void)removeFaultRepairHistoryListObject:(FaultRepairListHistory *)value;
- (void)addFaultRepairHistoryList:(NSSet *)values;
- (void)removeFaultRepairHistoryList:(NSSet *)values;

- (void)addJobPauseDataObject:(JobPauseData *)value;
- (void)removeJobPauseDataObject:(JobPauseData *)value;
- (void)addJobPauseData:(NSSet *)values;
- (void)removeJobPauseData:(NSSet *)values;

- (void)addJobDataListToRepairImagesObject:(RepairPictures *)value;
- (void)removeJobDataListToRepairImagesObject:(RepairPictures *)value;
- (void)addJobDataListToRepairImages:(NSSet *)values;
- (void)removeJobDataListToRepairImages:(NSSet *)values;

@end
