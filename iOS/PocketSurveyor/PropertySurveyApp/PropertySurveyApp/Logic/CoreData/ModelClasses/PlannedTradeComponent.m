//
//  PlannedTradeComponent.m
//  PropertySurveyApp
//
//  Created by Asif Nazir on 8/29/14.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import "PlannedTradeComponent.h"
#import "Appointment+CoreDataClass.h"
#import "JobPauseData.h"


@implementation PlannedTradeComponent

@dynamic completionDate;
@dynamic componentId;
@dynamic componentName;
@dynamic componentTradeId;
@dynamic duration;
@dynamic durationUnit;
@dynamic jobStatus;
@dynamic jsnNotes;
@dynamic jsNumber;
@dynamic pmoDescription;
@dynamic reportedDate;
@dynamic sortingOrder;
@dynamic tradeDescription;
@dynamic tradeId;
@dynamic location;
@dynamic adaptation;
@dynamic adaptationId;
@dynamic locationId;
@dynamic jobPauseData;
@dynamic tradeComponentToAppointment;

@end
