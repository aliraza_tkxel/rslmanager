//
//  Appliance.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 19/11/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "Appliance.h"
#import "ApplianceInspection.h"
#import "ApplianceLocation.h"
#import "ApplianceManufacturer.h"
#import "ApplianceModel.h"
#import "ApplianceType.h"
#import "Defect.h"
#import "Property+CoreDataClass.h"


@implementation Appliance

@dynamic applianceID;
@dynamic fluType;
@dynamic gcNumber;
@dynamic installedDate;
@dynamic isInspected;
@dynamic isLandlordAppliance;
@dynamic model;
@dynamic propertyID;
@dynamic replacementDate;
@dynamic serialNumber;
@dynamic applianceDefects;
@dynamic applianceLocation;
@dynamic applianceManufacturer;
@dynamic applianceModel;
@dynamic applianceToProperty;
@dynamic applianceType;
@dynamic applianceInspection;

@end
