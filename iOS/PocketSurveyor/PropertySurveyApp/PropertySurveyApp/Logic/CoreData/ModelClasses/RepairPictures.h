//
//  RepairPictures.h
//  PropertySurveyApp
//
//  Created by M.Mahmood on 25/02/2015.
//  Copyright (c) 2015 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobDataList;

@interface RepairPictures : NSManagedObject

@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSNumber * imageId;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSNumber * isBeforeImage;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSString * imageIdentifier;
@property (nonatomic, retain) JobDataList *repairImagesToJobDataList;
@property (nonatomic, retain) UIImage *repairImage;


@end
