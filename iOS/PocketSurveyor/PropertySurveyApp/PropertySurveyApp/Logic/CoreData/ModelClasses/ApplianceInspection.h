//
//  ApplianceInspection.h
//  PropertySurveyApp
//
//  Created by TkXel on 26/03/2014.
//  Copyright (c) 2014 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Appliance;

@interface ApplianceInspection : NSManagedObject

@property (nonatomic, retain) NSString * adequateVentilation;
@property (nonatomic, retain) NSString * applianceSafeToUse;
@property (nonatomic, retain) NSString * applianceServiced;
@property (nonatomic, retain) NSString * combustionReading;
@property (nonatomic, retain) NSString * fluePerformanceChecks;
@property (nonatomic, retain) NSString * flueVisualCondition;
@property (nonatomic, retain) NSNumber * inspectedBy;
@property (nonatomic, retain) NSDate * inspectionDate;
@property (nonatomic, retain) NSNumber * inspectionID;
@property (nonatomic, retain) NSNumber * isInspected;
@property (nonatomic, retain) NSString * operatingPressure;
@property (nonatomic, retain) NSString * operatingPressureUnit;
@property (nonatomic, retain) NSString * safetyDeviceOperational;
@property (nonatomic, retain) NSString * satisfactoryTermination;
@property (nonatomic, retain) NSString * smokePellet;
@property (nonatomic, retain) NSString * spillageTest;
@property (nonatomic, retain) Appliance *appliance;

@end
