//
//  Appointment+CoreDataProperties.m
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 13/03/2018.
//  Copyright © 2018 TkXel. All rights reserved.
//
//

#import "Appointment+CoreDataProperties.h"

@implementation Appointment (CoreDataProperties)

+ (NSFetchRequest<Appointment *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Appointment"];
}

@dynamic addToCalendar;
@dynamic appointmentCalendar;
@dynamic appointmentDate;
@dynamic appointmentDateSectionIdentifier;
@dynamic appointmentEndTime;
@dynamic appointmentEventIdentifier;
@dynamic appointmentId;
@dynamic appointmentLocation;
@dynamic appointmentNotes;
@dynamic appointmentOverdue;
@dynamic appointmentShift;
@dynamic appointmentStartTime;
@dynamic appointmentStatus;
@dynamic appointmentTitle;
@dynamic appointmentType;
@dynamic aptCompletedAppVersionInfo;
@dynamic assignedTo;
@dynamic createdBy;
@dynamic createdByPerson;
@dynamic defaultCustomerId;
@dynamic defaultCustomerIndex;
@dynamic failedReason;
@dynamic isMiscAppointment;
@dynamic isModified;
@dynamic isOfflinePrepared;
@dynamic isSurveyChanged;
@dynamic journalHistoryId;
@dynamic journalId;
@dynamic jsgNumber;
@dynamic loggedDate;
@dynamic noEntryNotes;
@dynamic repairCompletionDateTime;
@dynamic repairNotes;
@dynamic surveyCheckListForm;
@dynamic surveyorAlert;
@dynamic surveyorUserName;
@dynamic surveyourAvailability;
@dynamic surveyType;
@dynamic syncStatus;
@dynamic tenancyId;
@dynamic appointmentToAppInfoData;
@dynamic appointmentToCP12Info;
@dynamic appointmentToCustomer;
@dynamic appointmentToJobDataList;
@dynamic appointmentToJournal;
@dynamic appointmentToPlannedComponent;
@dynamic appointmentToProperty;
@dynamic appointmentToScheme;
@dynamic appointmentToSurvey;
@dynamic appointmentToUser;
@dynamic appointmentToSurveyComponents;
@dynamic appointmentToCertificateTemplates;
@dynamic appointmentToSurveyTemplates;

@end
