//
//  Customer+CoreDataClass.h
//  PropertySurveyApp
//
//  Created by Afaq Hussain on 9/26/17.
//  Copyright © 2017 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Address, Appointment, CustomerRiskData, CustomerVulnerabilityData;

NS_ASSUME_NONNULL_BEGIN

@interface Customer : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Customer+CoreDataProperties.h"
