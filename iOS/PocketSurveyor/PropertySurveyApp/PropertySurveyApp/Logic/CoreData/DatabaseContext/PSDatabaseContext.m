//
//  PSDatabaseContext.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 05/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDatabaseContext.h"

@implementation PSDatabaseContext
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


#pragma mark -
#pragma mark ARC Singleton Implementation
static PSDatabaseContext *sharedDatabaseContext = nil;
+ (PSDatabaseContext *) sharedContext
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDatabaseContext = [[PSDatabaseContext alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedDatabaseContext;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDatabaseContext = [super allocWithZone:zone];
    });
    return sharedDatabaseContext;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

#pragma mark - Methods
- (void)saveContext
{
	NSError *error = nil;
	__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
	// Save the context.
	if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
	{
		CLS_LOG(@"Error in Saving MOC: %@",[error description]);
	}
}

- (void) resetCoreDataStack
{
    CLS_LOG(@"resetCoreDataStack");
    NSString *storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:kXCDataSQLiteFile];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:storePath])
    {
        NSError* error = nil;
        [fileManager removeItemAtPath:storePath error:&error];
    }
    
    _managedObjectContext = nil;
    _persistentStoreCoordinator = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCoreDataResetNotification object:nil];
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
	if (_managedObjectContext != nil)
	{
		return _managedObjectContext;
	}

	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if(coordinator != NULL)
	{
		_managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
		[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	}
	return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
	if (_managedObjectModel != nil)
	{
		return _managedObjectModel;
	}
	NSString *path = [[NSBundle mainBundle] pathForResource:kXCDataModelFile ofType:kXCDataModelType];
	NSURL *momURL = [NSURL fileURLWithPath:path];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
	return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
		{
        return _persistentStoreCoordinator;
    }
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    NSString * storePath = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:kXCDataSQLiteFile];
    
//	/*
//	 Set up the store.
//	 For the sake of illustration, provide a pre-populated default store.
//	 */
	NSFileManager *fileManager = [NSFileManager defaultManager];
	// If the expected store doesn't exist, copy the default store.
	if (![fileManager fileExistsAtPath:storePath]) {
		NSString *defaultStorePath = [[NSBundle mainBundle] pathForResource:kXCDataModelFile ofType:kXCDataStoreType];
		if (defaultStorePath) {
			[fileManager copyItemAtPath:defaultStorePath toPath:storePath error:NULL];
		}
	}
    else { //Item exists at path
        CLS_LOG(@"DB File Exists %@",storePath);
        #warning Handle DB Contents Removal
    }
	NSURL *storeUrl = [NSURL fileURLWithPath:storePath];
	//Lightweight Migration Options
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                                                       [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                                    [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    NSError * error=nil;
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
	
    return _persistentStoreCoordinator;

}

#pragma mark -
#pragma mark Application's documents directory

/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark -
#pragma mark Faulting

+ (void) turnObjectIntoFault:(NSManagedObjectID *) objectID
{
    //Remove Object From CoreData
    STARTEXCEPTION
    if(objectID != nil)
    {
        NSError *error = nil;
        NSManagedObject *_object = [[PSDatabaseContext sharedContext].managedObjectContext existingObjectWithID:objectID error:&error];
        if(_object != nil)
        {
            //CLS_LOG(@"Turning Managed Object into Fault: %@", _object);
            [[PSDatabaseContext sharedContext].managedObjectContext refreshObject:_object mergeChanges:NO];
        }
    }
    ENDEXCEPTION
}

+ (void) turnObjectIntoFault:(NSManagedObjectID *) objectID context:(NSManagedObjectContext *)managedObjectContext
{
    //Remove Object From CoreData
    STARTEXCEPTION
    if(objectID != nil)
    {
        NSError *error = nil;
        NSManagedObject *_object = [managedObjectContext existingObjectWithID:objectID error:&error];
        if(_object != nil)
        {
            //CLS_LOG(@"Turning Managed Object into Fault: %@", _object);
            [managedObjectContext refreshObject:_object mergeChanges:NO];
        }
    }
    ENDEXCEPTION
}

#pragma mark - NSManagedObject Deletion

+ (void) deleteManagedObjectFromCoreDataWithID:(NSManagedObjectID *) objectID;
{
    //Remove Object From CoreData
    STARTEXCEPTION
    if(objectID != nil)
    {
        // Creating managed objects
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        NSError *error = nil;
       // NSManagedObject *_object = [managedObjectContext existingObjectWithID:objectID error:&error];
         NSManagedObject *_object = [managedObjectContext objectWithID:objectID];
        if(_object != nil)
        {
            [managedObjectContext deleteObject:_object];
        }
    }
    ENDEXCEPTION
}

@end
