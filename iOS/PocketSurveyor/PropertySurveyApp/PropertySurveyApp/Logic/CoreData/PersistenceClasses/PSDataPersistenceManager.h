//
//  PSDataPersistenceManager.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDataPersistenceManager : NSObject

+ (PSDataPersistenceManager *)sharedManager;
- (void) saveUserInfo:(NSDictionary *)userInfo;
- (void) saveAppointments:(NSArray *)appointmentsArray;
- (void) changeAppointmentModificationStatus:(NSArray *)appointmentsArray savedAppointments:(NSArray*)savedAppointments failedAppointments:(NSArray*)failedAppointments postNotification:(BOOL)flag;
- (void) createNewAppointment: (NSDictionary *)appointmentDictionary;

- (void) savePropertyPictureInfo:(NSDictionary *)pictureDictionary withProperty:(Property*) property;
- (void) saveDefectPictureInfo:(NSDictionary *)pictureDictionary withProperty:(Defect*) propertyParam;

- (void) saveAppointmentSurveyData:(NSDictionary *)surveyData;
- (void) saveSurveyors:(NSArray *)surveyorsArray;
- (void) saveProperties:(NSArray *)propertyArray;
- (void) saveAppliances:(NSDictionary *)appliancesData;
- (void) saveApplianceData:(NSArray *) appliancesData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline;
- (void) saveFetchedApplianceData:(NSArray *) appliancesData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline;
- (void) saveAppliancePropertiesOffline:(NSArray *)locationArray typeArray:(NSArray *)typeArray manufecturerArray:(NSArray *)manufecturerArray modelArray:(NSArray *)modelArray;
- (void) saveInspectionForm:(NSDictionary *) inspectionFormDictionary forAppliance:(Appliance *)appliance;
- (void) saveInstallationPipework:(NSDictionary *) pipeworkDictionary forProperty:(Property *)property;
- (void) saveFetchedDefects:(NSDictionary *) defectsDictionary;
- (void) saveNewDefect:(NSDictionary *)defect;
//- (void) saveFetchedDefectCategories:(NSArray *)defectCategories defects:(NSArray *)defects;
- (void) saveFetchedDefectCategories:(NSArray *)defectCategories defects:(NSDictionary *)defectsDictionary;
- (void) saveFetchedFaultRepairData:(NSArray *)faultRepairData;
- (void) savefetchedJobPauseReasons:(NSArray *)jobPauseReasons;
- (void) saveFaultRepairHistory:(NSArray *)faultRepairHistory forJob:(JobDataList *)jobData;
- (void) saveFetchedDetectorData:(NSArray *) detectorData forProperty:(NSManagedObjectID *)propertyId;
- (void) saveDetectorInspectionForm:(NSDictionary *) inspectionFormDictionary forDetector:(Detector *)detector;
- (void) updateModificationStatusOfAppointments:(NSArray *)appointmentsArray  toModificationStatus:(NSNumber*)modificationStatus;
- (void) updateDefectObject:(Defect *)defectObj  withDictionary:(NSDictionary*)defectDictionary;
- (void) saveRepairPictureInfo:(NSDictionary *)pictureDictionary;
- (void) loadAppointmentSchemeData:(NSDictionary *)SchemeData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (BOOL) saveDetector:(NSDictionary *)detector forProperty:(NSManagedObjectID *)propertyId;
- (void) saveCommonData:(NSDictionary *)commonDataArray;
-(void) saveSurveyPropertyDetectors:(NSArray *) detectors;

@end
