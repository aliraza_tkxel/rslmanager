//
//  PSDataPersistenceManager.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 31/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDataPersistenceManager.h"
#import "PSDatabaseContext.h"
#import "PSCoreDataManager.h"
#import "Survey+CoreDataClass.h"
#import "PSPropertyPictureManager.h"
#import "PropertyPicture+JSON.h"
#import "RepairPictures+JSON.h"
#import "Scheme+JSON.h"
#import "Component+CoreDataClass.h"
#import "SurveyHeatingFuelData+CoreDataClass.h"
#import "SurveyHeatingFuelCertificate+CoreDataClass.h"

@interface PSDataPersistenceManager ()

- (void) loadAppointmentData:(NSDictionary *)appointmentData managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadSurveyorData:(NSDictionary *)surveyorData managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadAppointmentAppInfoData:(NSDictionary *)appInfoDictionary appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadAppointmentCP12Info:(NSDictionary *)CP12InfoDictionary appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadAppointmentCustomerData:(NSArray *)customerList appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadAppointmentJobDataList:(NSArray *)jobDataList appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadAppointmentJournalData:(NSDictionary *)journalData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadAppointmentPropertyData:(NSDictionary *)propertyData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

- (void) loadCustomerRiskData:(NSArray *)customerRiskDataList customer:(Customer *)customer managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadCustomerVulunarityData:(NSArray *)customerVulunarityDataList customer:(Customer *)customer managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadApplianceLocationData:(NSArray *)applianceLocationData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadApplianceManufacturerData:(NSArray *)applianceManufacturerData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadApplianceModelData:(NSArray *)applianceModelData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadApplianceTypeData:(NSArray *)applianceTypeData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadInspectionForm:(NSDictionary *)inspectionFormDictionary forAppliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadPipeworkForm:(NSDictionary *)pipeworkDictionary forProperty:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadApplianceDefectsData:(NSArray *)applianceDefectsList managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadNewDefectData:(NSDictionary *)defectDictionary managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadDefectCategoryData:(NSArray *)defectCategoryList managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadFaultRepairData:(NSArray *)faultRepairDataList jobData:(JobDataList *)jobData managedObjectContext:(NSManagedObjectContext *)managedObjectContext;

- (void) loadJobPauseReasonData:(NSArray *)jobPauseReasonList managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadFaultRepairHistoryData:(NSArray *)faultRepairHistoryList jobData:(JobDataList *)jobData managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadDetectorData:(NSArray *)detectorsData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
- (void) loadDetectorInspectionForm:(NSDictionary *)inspectionFormDictionary forDetector:(Detector *)detector managedObjectContext:(NSManagedObjectContext *)managedObjectContext;
@end

@implementation PSDataPersistenceManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSDataPersistenceManager *sharedManagerObject = nil;
+ (PSDataPersistenceManager *) sharedManager
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedManagerObject = [[PSDataPersistenceManager alloc] init];
		// Do any other initialisation stuff here
	});
	return sharedManagerObject;
}

+(id)allocWithZone:(NSZone *)zone {
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedManagerObject = [super allocWithZone:zone];
	});
	return sharedManagerObject;
}

- (id)copyWithZone:(NSZone *)zone {
	return self;
}

#pragma mark - Methods

-(BOOL)saveDataInDB:(NSManagedObjectContext *)dbContext
{
	BOOL status = TRUE;
	// Save the context.
	NSError *error = nil;
	if (![dbContext save:&error])
	{
		CLS_LOG(@"Error in Saving MOC: %@",[error description]);
		status = FALSE;
	}
	return status;
}

#pragma mark - PropertyPicture Persistence
- (void) saveDefectPictureInfo:(NSDictionary *)pictureDictionary withProperty:(Defect*) propertyParam
{
	if(!isEmpty(pictureDictionary))
	{
		__block DefectPicture *existingPicture = [[PSCoreDataManager sharedManager] defectPictureWithIdentifier:[pictureDictionary valueForKey:kPropertyPictureId]];
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		[managedObjectContext performBlock:^{
			
			Defect* property = nil;
			
			if(propertyParam) {
				property = (Defect*)[managedObjectContext objectWithID:propertyParam.objectID];
			}
			
			DefectPicture *propertyPicture = nil;
			
			if(existingPicture != nil)
			{
				//User already exists in database, so just update the existing object.
				propertyPicture = (DefectPicture *)[managedObjectContext existingObjectWithID:existingPicture.objectID error:nil];
			}
			else
			{
				//User does not exists in database, so create new user entity.
				propertyPicture = (DefectPicture *)[NSEntityDescription insertNewObjectForEntityForName:kDefectPicture inManagedObjectContext:managedObjectContext];
			}
			
			
			UIImage * propertyImage = [pictureDictionary valueForKey:kPropertyImage];
			
			
			NSString *imagePath = [pictureDictionary valueForKey:kPropertyPicturePath];
			NSNumber *createdBy = [pictureDictionary valueForKey:kCreatedBy];
			NSString * uniqueIdentifier = [pictureDictionary valueForKey:kUniqueImageIdentifier];
			
			
			propertyPicture.createdBy = isEmpty(createdBy)?nil:createdBy;
			
			if(!isEmpty(property)) {
				propertyPicture.defectPictureToDefect=property;
			}
			
			propertyPicture.createdOn=[NSDate date];
			propertyPicture.imagePath= isEmpty(imagePath)?nil:imagePath;
			propertyPicture.synchStatus=[self generateIdentifier];
			propertyPicture.imageIdentifier = uniqueIdentifier;
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUserInfoSaveCompleted object:nil];
		}]; // parent
	}
}

#pragma mark - PropertyPicture Persistence
- (void) savePropertyPictureInfo:(NSDictionary *)pictureDictionary withProperty:(Property*) propertyParam
{
	if(!isEmpty(pictureDictionary))
	{
		__block PropertyPicture *existingPicture = [[PSCoreDataManager sharedManager] propertyPictureWithIdentifier:[pictureDictionary valueForKey:kPropertyPictureId]];
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			Property* property = (Property*)[managedObjectContext objectWithID:propertyParam.objectID];
			if (!property) {
				property = propertyParam;
			}
			PropertyPicture *propertyPicture = nil;
			
			if(existingPicture != nil)
			{
				//User already exists in database, so just update the existing object.
				propertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:existingPicture.objectID];
			}
			else
			{
				//User does not exists in database, so create new user entity.
				propertyPicture = (PropertyPicture *)[NSEntityDescription insertNewObjectForEntityForName:kPropertyPicture inManagedObjectContext:managedObjectContext];
			}
            NSString *heatingId = [pictureDictionary valueForKey:kPropertyPictureHeatingId];
			NSNumber *propertyPictureId = [pictureDictionary valueForKey:kPropertyPictureId];
			NSString * propertyId = [pictureDictionary valueForKey:kPropertyId];
			NSNumber *itemId = [pictureDictionary valueForKey:kItemId];
			NSNumber *appointmentId = [pictureDictionary valueForKey:kAppointmentId];
			NSString *imagePath = [pictureDictionary valueForKey:kPropertyPictureId];
			NSString *createdBy = [pictureDictionary valueForKey:kCreatedBy];
			NSString * isDefault=[pictureDictionary valueForKey:kPropertyPictureIsDefault];
			UIImage * propertyImage = [pictureDictionary valueForKey:kPropertyImage];
			NSString * uniqueIdentifier = [pictureDictionary valueForKey:kUniqueImageIdentifier];
			
			propertyPicture.propertyPictureId = isEmpty(propertyPictureId)?nil:propertyPictureId;
			propertyPicture.propertyId = isEmpty(propertyId)?nil:propertyId;
			
			propertyPicture.itemId = itemId;
			propertyPicture.appointmentId = appointmentId;
            propertyPicture.image = isEmpty(propertyImage)?nil:UIImageJPEGRepresentation(propertyImage, 1);
			//propertyPicture.createdBy = isEmpty(createdBy)?nil:createdBy;
            propertyPicture.heatingId = [pictureDictionary valueForKey:kPropertyPictureHeatingId];
			propertyPicture.propertyPictureToProperty = property;
			
			propertyPicture.createdOn = [NSDate date];
			propertyPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
			propertyPicture.synchStatus=[self generateIdentifier];
			propertyPicture.imageIdentifier = uniqueIdentifier;
			
			if ([isDefault isEqualToString:@"true"]) {
				
				propertyPicture.isDefault=[NSNumber numberWithInt:YES];
				property.defaultPicture=propertyPicture;
			}
			else
			{
				propertyPicture.isDefault = [NSNumber numberWithInt:NO];
			}
			
			if([self saveDataInDB:managedObjectContext] == TRUE)
			{
				// if property picture is saved then need to update it on server
				PropertyPicture *propertyPicture1 = (PropertyPicture *)[managedObjectContext objectWithID:propertyPicture.objectID];
				[propertyPicture1 uploadPropertyPicture:pictureDictionary withImage:propertyImage];
			}
		}]; // parent
	}
}


#pragma mark - RepairPicture Persistence
- (void) saveRepairPictureInfo:(NSDictionary *)pictureDictionary
{
	if(!isEmpty(pictureDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			NSString *jobSheetNo = [pictureDictionary valueForKey:kRepairJSNumber];
			
			JobDataList* jobDataListObject = (JobDataList*)[[PSCoreDataManager sharedManager] jobDataWithJSN:jobSheetNo inContext:managedObjectContext];
			
			//Repair pircure does not exists in database, so create new user entity.
			RepairPictures* repairPicture = (RepairPictures *)[NSEntityDescription insertNewObjectForEntityForName:kRepairPictures inManagedObjectContext:managedObjectContext];
			
			NSNumber *imageId = [pictureDictionary valueForKey:kFaultRepairImageId];
			NSNumber *appointmentId = [pictureDictionary valueForKey:kAppointmentId];
			NSString *imagePath = [pictureDictionary valueForKey:kRepairImagePath];
			NSString *createdBy = [pictureDictionary valueForKey:kCreatedBy];
			NSNumber *isBeforeImage=[pictureDictionary valueForKey:kIsBeforeImage];
			UIImage  *repairImage = [pictureDictionary valueForKey:kRepairImage];
			NSString * uniqueIdentifier = [pictureDictionary valueForKey:kUniqueImageIdentifier];
			//NSDate *createdOn = ([[pictureDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[pictureDictionary valueForKey:kInspectionDate]]:[pictureDictionary valueForKey:kInspectionDate];
			
			[pictureDictionary setValue:(isBeforeImage == [NSNumber numberWithBool:YES])?@"TRUE":@"FALSE" forKey:kIsBeforeImage];
			
			repairPicture.imageId = isEmpty(imageId)?nil:imageId;
			repairPicture.createdOn = [NSDate date];
			repairPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
			repairPicture.isBeforeImage=isEmpty(isBeforeImage)?nil:isBeforeImage;
			repairPicture.repairImage = isEmpty(repairImage)?nil:repairImage;
			repairPicture.syncStatus=[self generateIdentifier];
			repairPicture.repairImagesToJobDataList = jobDataListObject;
			repairPicture.imageIdentifier = uniqueIdentifier;
			
			[jobDataListObject addJobDataListToRepairImagesObject:repairPicture];
			
			if([self saveDataInDB:managedObjectContext] == TRUE)
			{
				// if property picture is saved then need to update it on server
				RepairPictures *repairPicture1 = (RepairPictures *)[managedObjectContext objectWithID:repairPicture.objectID];
				[repairPicture1 uploadRepairPicture:pictureDictionary withImage:repairImage];
			}
		}]; // parent
	}
}

- (void) loadRepairPictureData:(NSDictionary *)pictureDictionary inContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(pictureDictionary) && !isEmpty(managedObjectContext))
	{
		NSString *jobSheetNo = [pictureDictionary valueForKey:kRepairJSNumber];
		
		JobDataList* jobDataListObject = (JobDataList*)[[PSCoreDataManager sharedManager] jobDataWithJSN:jobSheetNo inContext:managedObjectContext];
		
		//Repair pircure does not exists in database, so create new user entity.
		RepairPictures* repairPicture = (RepairPictures *)[NSEntityDescription insertNewObjectForEntityForName:kRepairPictures inManagedObjectContext:managedObjectContext];
		
		NSNumber *imageId = [pictureDictionary valueForKey:kFaultRepairImageId];
		NSNumber *appointmentId = [pictureDictionary valueForKey:kAppointmentId];
		NSString *imagePath = [pictureDictionary valueForKey:kRepairImagePath];
		NSString *createdBy = [pictureDictionary valueForKey:kCreatedBy];
		NSNumber *isBeforeImage=[pictureDictionary valueForKey:kIsBeforeImage];
		UIImage  *repairImage = [pictureDictionary valueForKey:kRepairImage];
		NSDate *createdOn = ([[pictureDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[pictureDictionary valueForKey:kInspectionDate]]:[pictureDictionary valueForKey:kInspectionDate];
		
		[pictureDictionary setValue:(isBeforeImage == [NSNumber numberWithBool:YES])?@"TRUE":@"FALSE" forKey:kIsBeforeImage];
		repairPicture.imageId = isEmpty(imageId)?nil:imageId;
		repairPicture.createdOn = isEmpty(createdOn)?nil:createdOn;;
		repairPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
		repairPicture.isBeforeImage=isEmpty(isBeforeImage)?nil:isBeforeImage;
		repairPicture.repairImage = isEmpty(repairImage)?nil:repairImage;
		repairPicture.syncStatus=[self generateIdentifier];
		
		repairPicture.repairImagesToJobDataList = jobDataListObject;
		[jobDataListObject addJobDataListToRepairImagesObject:repairPicture];
		
	}
}
-(NSNumber*) generateIdentifier
{
	int a=arc4random();
	if (!a) {
		
		while (a) {
			
			a=arc4random();
			
			break;
		}
		
	}
	
	NSNumber *temp=[NSNumber numberWithInt:a];
	
	return temp;
	
}


#pragma mark - UserInfo Persistence
- (void) saveUserInfo:(NSDictionary *)userInfo
{
	if(!isEmpty(userInfo))
	{
		__block User *existingUser = [[PSCoreDataManager sharedManager] userWithIdentifier:[userInfo valueForKey:kUserId]];
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			User *user = nil;
			if(existingUser != nil)
			{
				//User already exists in database, so just update the existing object.
				user = (User *)[managedObjectContext existingObjectWithID:existingUser.objectID error:nil];
			}
			else
			{
				//User does not exists in database, so create new user entity.
				user = (User *)[NSEntityDescription insertNewObjectForEntityForName:kUser inManagedObjectContext:managedObjectContext];
			}
			
			NSString *salt = [userInfo valueForKey:kSalt];
			NSNumber *userId = [userInfo valueForKey:kUserId];
			NSString *userName = [userInfo valueForKey:kUserName];
			NSString *password = [SettingsClass sharedObject].currentUserPassword;
			NSString *fullName = [userInfo valueForKey:kFullName];
			NSNumber *isActive = [userInfo valueForKey:kIsActive];
			NSDate *lastLoggedInDate = [UtilityClass convertServerDateToNSDate:[userInfo valueForKey:kLastLoggedInDate]];
			
			user.salt = isEmpty(salt)?nil:salt;
			user.userId = isEmpty(userId)?nil:userId;
			user.userName = isEmpty(userName)?nil:userName;
			user.password = isEmpty(password)?nil:password;
			user.fullName = isEmpty(fullName)?nil:fullName;
			user.isActive = isEmpty(isActive)?nil:isActive;
			user.lastLoggedInDate = isEmpty(lastLoggedInDate)?nil:lastLoggedInDate;
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUserInfoSaveCompleted object:nil];
		}]; // parent
	}
}
#pragma mark - Appliances Data Persistence

- (void) saveFetchedApplianceData:(NSArray *) appliancesData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline
{
	if([UtilityClass isUserLoggedIn])
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		__block NSArray * applinceLocationBlock = appliancesData;
		__block Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
		[managedObjectContext performBlock:^{
			
			if(!isEmpty(applinceLocationBlock))
			{
				[[PSDataPersistenceManager sharedManager] loadApplianceData:applinceLocationBlock property:property managedObjectContext:managedObjectContext isAddedOffline:offline];
			}
			property.propertyToAppointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
		}]; // parent
	}
	
}

- (void) saveApplianceData:(NSArray *) appliancesData forProperty:(NSManagedObjectID *)propertyId isAddedOffline:(BOOL)offline
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appliancesData))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		[managedObjectContext performBlock:^{
			Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
			[[PSDataPersistenceManager sharedManager] loadApplianceData:appliancesData property:property managedObjectContext:managedObjectContext isAddedOffline:offline];
			
			property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			property.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveNewAppliacneSuccessNotification object:nil];
		}]; // parent
	}
}

- (void) saveAppliancePropertiesOffline:(NSArray *)locationArray typeArray:(NSArray *)typeArray manufecturerArray:(NSArray *)manufecturerArray modelArray:(NSArray *)modelArray
{
	if([UtilityClass isUserLoggedIn])
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			[[PSDataPersistenceManager sharedManager]loadApplianceLocationData:locationArray appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceManufacturerData:manufecturerArray appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceModelData:modelArray appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceTypeData:typeArray appliance:nil managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
		}]; // parent
	}
}

- (void) saveAppliances:(NSDictionary *)appliancesData
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appliancesData))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			NSArray *applianceLocations = [appliancesData valueForKey:@"ApplianceLocation"];
			NSArray *applianceManufacturers = [appliancesData valueForKey:@"ApplianceManufacturer"];
			NSArray *applianceModels = [appliancesData valueForKey:@"ApplianceModel"];
			NSArray *applianceTypes = [appliancesData valueForKey:@"ApplianceType"];
			NSArray *defectCategories = [appliancesData valueForKey:@"defectCategories"];
			
			
			[[PSDataPersistenceManager sharedManager]loadApplianceLocationData:applianceLocations appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceManufacturerData:applianceManufacturers appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceModelData:applianceModels appliance:nil managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceTypeData:applianceTypes appliance:nil managedObjectContext:managedObjectContext];
			
			[[PSDataPersistenceManager sharedManager] loadDefectCategoryData:defectCategories managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			
			// [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveAppliacnesSuccessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - Detectors Persistence

- (void) saveFetchedDetectorData:(NSArray *) detectorData forProperty:(NSManagedObjectID *)propertyId
{
	if([UtilityClass isUserLoggedIn])
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			Property *property;
			if(!isEmpty(detectorData) && !isEmpty(propertyId))
			{
				Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
				[[PSDataPersistenceManager sharedManager]loadDetectorData:detectorData property:property managedObjectContext:managedObjectContext];
			}
			if (property != nil)
			{
				property.propertyToAppointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
				// property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			}
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchPropertyAppliancesSuccessNotification object:nil];
		}]; // parent
	}
	
}


#pragma mark - Inspection Form Data Persistence

- (void) saveInspectionForm:(NSDictionary *) inspectionFormDictionary forAppliance:(Appliance *)appliance
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectID* _applianceId = appliance.objectID;
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			Appliance *existingAppliance = (Appliance *)[managedObjectContext objectWithID:_applianceId];
			[[PSDataPersistenceManager sharedManager] loadInspectionForm:inspectionFormDictionary forAppliance:existingAppliance managedObjectContext:managedObjectContext];
			
			
			existingAppliance.isInspected = [NSNumber numberWithBool:YES];
			existingAppliance.applianceToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			existingAppliance.applianceToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveApplianceInspectionFormSuccessNotification object:nil];
		}]; // parent
	}
	
}

- (void) saveDetectorInspectionForm:(NSDictionary *) inspectionFormDictionary forDetector:(Detector *)detector
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(inspectionFormDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectID* _detectorId = detector.objectID;
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager]loadDetectorInspectionForm:inspectionFormDictionary forDetector:detector managedObjectContext:managedObjectContext];
			Detector *existingDetector = (Detector *)[managedObjectContext objectWithID:_detectorId];
			existingDetector.isInspected = [NSNumber numberWithBool:YES];
			existingDetector.detectorToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			existingDetector.detectorToProperty.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveApplianceInspectionFormSuccessNotification object:nil];
		}]; // parent
	}
	
}
#pragma mark - Pipework Installation Data Persistence

- (void) saveInstallationPipework:(NSDictionary *) pipeworkDictionary forProperty:(Property *)property
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(pipeworkDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectID* _propertyId = property.objectID;
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			//  Property *_property = (Property *)[temporaryContext objectWithID:_propertyId];
			
			Property *_property = (Property *)[managedObjectContext objectWithID:_propertyId];
			[[PSDataPersistenceManager sharedManager] loadPipeworkForm:pipeworkDictionary forProperty:_property managedObjectContext:managedObjectContext];
			_property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			_property.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSavePropertyPipeworkFormSuccessNotification object:nil];
		}]; // parent
	}
	
}

#pragma mark - Defects Persistence
- (void) saveFetchedDefects:(NSDictionary *)defectsDictionary
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(defectsDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			NSArray *defects = [defectsDictionary valueForKey:@"defects"];
			[[PSDataPersistenceManager sharedManager] loadApplianceDefectsData:defects managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			NSArray *detectorDefects = [defectsDictionary valueForKey:@"detectorDefects"];
			[[PSDataPersistenceManager sharedManager] saveFetchedDetectorDefects:detectorDefects];
		}]; // parent
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification object:nil];
	}
}

- (void) saveFetchedDetectorDefects:(NSArray *)defects
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(defects))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager] loadDetectorDefectsData:defects managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveSuccessNotification object:nil];
		}]; // parent
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchDefectsSaveFailureNotification object:nil];
	}
	
}
- (void) saveNewDefect:(NSDictionary *)defect
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(defect))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager] loadNewDefectData:defect managedObjectContext:managedObjectContext];
			Property *property = [[PSCoreDataManager sharedManager]propertyWithIdentifier:[defect valueForKey:kPropertyId] context:managedObjectContext];
			property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
			property.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveDefectSuccessNotification object:nil];
		}]; // parent
	}
	else
	{
		[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveDefectFailureNotification object:nil];
	}
}

#pragma mark - Defect Categories Persistence
- (void) saveFetchedDefectCategories:(NSArray *)defectCategories defects:(NSDictionary *)defectsDictionary
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(defectCategories))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager] loadDefectCategoryData:defectCategories managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			[[PSDataPersistenceManager sharedManager]saveFetchedDefects:defectsDictionary];
		}]; // parent
	}
	
}

#pragma mark - Job Data Persistence
- (void) saveFetchedFaultRepairData:(NSArray *)faultRepairDataList
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(faultRepairDataList))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager]loadFaultRepairData:faultRepairDataList jobData:nil managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchFaultRepairDataSaveSuccessNotification object:nil];
		}]; // parent
	}
	
}

- (void) savefetchedJobPauseReasons:(NSArray *)jobPauseReasons
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(jobPauseReasons))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			[[PSDataPersistenceManager sharedManager]loadJobPauseReasonData:jobPauseReasons managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kFetchJobPauseReasonsSaveSuccessNotification object:nil];
		}]; // parent
	}
}

- (void) saveFaultRepairHistory:(NSArray *)faultRepairHistory forJob:(JobDataList *)jobData
{
	if([UtilityClass isUserLoggedIn] /*&& !isEmpty(faultRepairHistory)*/ && !isEmpty(jobData))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectID *jobDataId = jobData.objectID;
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			JobDataList *existingJobData = (JobDataList *)[managedObjectContext objectWithID:jobDataId];
			if (!isEmpty(existingJobData))
			{
				[[PSDataPersistenceManager sharedManager]loadFaultRepairHistoryData:faultRepairHistory jobData:existingJobData managedObjectContext:managedObjectContext];
			}
			else
			{
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveFaultRepairListFailureNotification object:nil];
			}
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveFaultRepairListSuccessNotification object:nil];
		}]; // parent
	}
	
}

#pragma mark - Surveyors Data Persistence
- (void) saveSurveyors:(NSArray *)surveyorsArray
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(surveyorsArray))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			int counter = 1;
			for (NSDictionary *surveyorData in surveyorsArray)
			{
				[[PSDataPersistenceManager sharedManager] loadSurveyorData:surveyorData managedObjectContext:managedObjectContext];
				
				if(counter % 5 == 0)
				{
					// Save the context.
					NSError *error = nil;
					if (![managedObjectContext save:&error]) {
						CLS_LOG(@"Unresolved error %@, %@", error, [error userInfo]);
					}
				}
				counter++;
			}
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyorSaveSuccessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - SProperties Data Persistence
- (void) saveProperties:(NSArray *)propertyArray
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(propertyArray))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlockAndWait:^{
			
			int counter = 1;
			for (NSDictionary *propertyData in propertyArray)
			{
				[[PSDataPersistenceManager sharedManager] loadSPropertyData:propertyData managedObjectContext:managedObjectContext];
				
				if(counter % 5 == 0)
				{
					// Save the context.
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:managedObjectContext];
				}
				counter++;
			}
			
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertySaveSuccessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - New Appointment Persistence

- (void) createNewAppointment: (NSDictionary *)appointmentDictionary
{
	if ([UtilityClass isUserLoggedIn] && !isEmpty(appointmentDictionary)) {
		
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			
			[[PSDataPersistenceManager sharedManager] loadAppointmentData:appointmentDictionary managedObjectContext:managedObjectContext];
			
			[self saveDataInDB:managedObjectContext];
			
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kNewAppointmentSuccessNotification object:nil];
		}]; // parent
	}
	
}

#pragma mark - Appointment Persistence

/*!
 @discussion
 This method is called to set appointment 'isModified' attribute false after modified appointments are posted
 to the server, so that same data is not posted again and again to server. Flag determines whether to post
 notifification or not.
 */
- (void) changeAppointmentModificationStatus:(NSArray *)appointmentsArray
													 savedAppointments:(NSArray *)savedAppointments
													failedAppointments:(NSArray *)failedAppointments
														postNotification:(BOOL)flag
{
	if(!isEmpty(appointmentsArray))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		for (Appointment *appointment in appointmentsArray)
		{
			NSNumber *appointmentId = appointment.appointmentId;
			if (!isEmpty(appointmentId))
			{
				__block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:appointmentId context:nil];
				if (existingAppointment != nil)
				{
					// we have to only check if some appointments failed
					if(!isEmpty(failedAppointments))
					{
						for (NSDictionary* object in failedAppointments)
						{
							if(!isEmpty([object objectForKey:kAppointmentId]) && [appointment.appointmentId isEqualToNumber:[object objectForKey:kAppointmentId]]) {
								existingAppointment.isModified = [NSNumber numberWithBool:YES];
								existingAppointment.failedReason = isEmpty([object objectForKey:kAppointmentFailedReason])?nil:[object objectForKey:kAppointmentFailedReason];
								break;
							}
						}
					}
					else
					{
						existingAppointment.isModified = [NSNumber numberWithBool:NO];
						existingAppointment.failedReason = nil;
					}
				}
			}
		}
		
		[self saveDataInDB:managedObjectContext];
		
		if (flag)
		{
			if(!isEmpty(savedAppointments) && [savedAppointments count] == [appointmentsArray count])
			{
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentModificationStatusChangeSuccessNotification object:nil];
			}
			else
			{
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentModificationStatusChangeFailureNotification object:failedAppointments];
			}
		}
		
	}
}

- (void) updateModificationStatusOfAppointments:(NSArray *)appointmentsArray  toModificationStatus:(NSNumber*)modificationStatus
{
	if(!isEmpty(appointmentsArray))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		__block NSNumber * blockModficationStatus = modificationStatus;
		__block NSArray * appointmentsArrayForBlock = appointmentsArray;
		
		[managedObjectContext performBlock:^{
			
			for (Appointment *appointment in appointmentsArrayForBlock)
			{
				NSNumber *appointmentId = appointment.appointmentId;
				if (!isEmpty(appointmentId))
				{
					__block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:appointmentId context:nil];
					if (existingAppointment)
					{
						existingAppointment.isModified = blockModficationStatus;
					}
				}
			}
			
			[self saveDataInDB:managedObjectContext];
		}]; // parent
	}
}

- (void) updateDefectObject:(Defect *)defectObj  withDictionary:(NSDictionary*)defectDictionary
{
	if(!isEmpty(defectDictionary))
	{
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		__block NSManagedObjectID * defectObjID = defectObj.objectID;
		__block NSDictionary * defectDict = defectDictionary;
		[managedObjectContext performBlock:^{
			
			Defect * defect = (Defect *)[managedObjectContext objectWithID:defectObjID];
			NSNumber * defectID = [defectDict valueForKey:kDefectID];
			
			NSNumber *applianceId = [defectDict valueForKey:kApplianceId];
			NSDate *defectDate = [UtilityClass convertServerDateToNSDate:[defectDict valueForKey:kDefectDate]];
			NSString *defectDescription = [defectDict valueForKey:kDefectDescription];
			NSNumber *faultCategory  = [defectDict valueForKey:kFaultCategory];
			NSNumber *isActionTaken = [defectDict valueForKey:kIsActionTaken];
			NSNumber *isAdviceNoteIssued = [defectDict valueForKey:kisAdviceNoteIssued];
			NSNumber *isDefectidentified = [defectDict valueForKey:kIsDefectIdentified];
			NSString *remedialAction = [defectDict valueForKey:kRemedialAction];
			NSString *serialNumber = [defectDict valueForKey:kSerialNo];
			NSNumber *warningTagFixed = [defectDict valueForKey:kWarningTagFixed];
			NSNumber *detectorId = [defectDict valueForKey:kDetectorTypeId];
			
			defect.defectDate = isEmpty(defectDate)?nil:defectDate;
			defect.defectDescription = isEmpty(defectDescription)?nil:defectDescription;
			defect.faultCategory = isEmpty(faultCategory)?nil:faultCategory;
			defect.isActionTaken = isEmpty(isActionTaken)?nil:isActionTaken;
			defect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
			defect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
			defect.remedialAction = isEmpty(remedialAction)?nil:remedialAction;
			defect.serialNo = isEmpty(serialNumber)?nil:serialNumber;
			defect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
			defect.defectID = isEmpty(defectID)?nil:defectID;
			
			[self saveDataInDB:managedObjectContext];
			
		}]; // parent
	}
}

- (void) saveAppointments:(NSArray *)appointmentsArray
{
	if([UtilityClass isUserLoggedIn] && !isEmpty(appointmentsArray))
	{
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			int counter = 1;
			for (NSDictionary *appointmentData in appointmentsArray)
			{
				[[PSDataPersistenceManager sharedManager] loadAppointmentData:appointmentData managedObjectContext:managedObjectContext];
				if(counter % 5 == 0)
				{
					// Save the context.
					CLS_LOG(@"Saving to PSC, %u", counter);
					[self saveDataInDB:managedObjectContext];
				}
				counter++;
			}
			[self saveDataInDB:managedObjectContext];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsSaveSuccessNotification object:nil];
		}]; // parent
	}
}

#pragma mark - Appointment Survey Data Persistence
- (void) saveAppointmentSurveyData:(NSDictionary *)surveyData
{
	if(!isEmpty(surveyData))
	{
		NSDictionary *appointmentInfo = [surveyData objectForKey:@"appointmentinfo"];
		
		__block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithPropertyIdentifier:[appointmentInfo valueForKey:kPropertyId]
																																																			appointmentId:[appointmentInfo valueForKey:kAppointmentId]];
		CLS_LOG(@"Property ID: %@, Appointment ID: %@",
				 [appointmentInfo valueForKey:kPropertyId],
				 [appointmentInfo valueForKey:kAppointmentId]);
		
		// Creating managed objects
		CLS_LOG(@"Creating managed objects");
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			
			Appointment *appointment = nil;
			if(existingAppointment != nil)
			{
				appointment = (Appointment *)[managedObjectContext objectWithID:existingAppointment.objectID];
				
				Survey *survey = [NSEntityDescription insertNewObjectForEntityForName:kSurvey inManagedObjectContext:managedObjectContext];
				NSString *surveyJSON = [surveyData JSONRepresentation];
				survey.surveyJSON = isEmpty(surveyJSON)?nil:surveyJSON;
				appointment.appointmentToSurvey = survey;
				appointment.isOfflinePrepared = [NSNumber numberWithBool:YES];
				//  appointment.isModified = [NSNumber numberWithBool:YES];
				survey.surveyToAppointment = appointment;
			}
			else
			{
				//Appointment Does Not Exists.
				[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyFormSaveFailureNotification object:nil];
			}
			
			[self saveDataInDB:managedObjectContext];
            [self saveCertificatesForAppointmentSurvey:surveyData];
            [self saveHeatingDataFormsForAppointmentSurvey:surveyData];
            [self saveComponentsForAppointmentSurvey:surveyData];
			[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSurveyFormSaveSucessNotification object:nil];
		}]; // parent
	}
}

-(void) saveCertificatesForAppointmentSurvey:(NSDictionary*)surveyData{
    if(!isEmpty(surveyData)){
        NSDictionary *appointmentInfo = [surveyData objectForKey:@"appointmentinfo"];
        
        __block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithPropertyIdentifier:[appointmentInfo valueForKey:kPropertyId]
                                                                                                          appointmentId:[appointmentInfo valueForKey:kAppointmentId]];
        CLS_LOG(@"Property ID: %@, Appointment ID: %@",
                [appointmentInfo valueForKey:kPropertyId],
                [appointmentInfo valueForKey:kAppointmentId]);
        
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            Appointment *appointment = nil;
            if(existingAppointment != nil)
            {
                appointment = (Appointment *)[managedObjectContext objectWithID:existingAppointment.objectID];
                
                NSDictionary *common = [surveyData objectForKey:kSurveyCommonTag];
                if(!isEmpty(common)){
                    NSArray *certificates = [common objectForKey:kSurveyCertificatesTag];
                    if(!isEmpty(certificates)){
                        NSMutableArray *certificatesEntities = [[NSMutableArray alloc] init];
                        for(NSDictionary *cert in certificates){
                            SurveyHeatingFuelCertificate *certEnt = [NSEntityDescription insertNewObjectForEntityForName:kSurveyHeatingFuelCertificateEntity inManagedObjectContext:managedObjectContext];
                            certEnt.certificateId = [cert objectForKey:kHeatingCertificateEntCertificateId];
                            certEnt.certificateName = [cert objectForKey:kHeatingCertificateEntCertificateName];
                            certEnt.fuelTypeId = [cert objectForKey:kHeatingCertificateEntFuelTypeId];
                            certEnt.heatingFuelCertificateToAppointment = appointment;
                            [certificatesEntities addObject:certEnt];
                        }
                        if(!isEmpty(certificatesEntities)){
                            appointment.appointmentToCertificateTemplates = [NSSet setWithArray:certificatesEntities];
                        }
                    }
                }
                
            }
            
            
            [self saveDataInDB:managedObjectContext];
        }]; // parent
    }
}

-(void) saveHeatingDataFormsForAppointmentSurvey:(NSDictionary*)surveyData{
    if(!isEmpty(surveyData)){
        NSDictionary *appointmentInfo = [surveyData objectForKey:@"appointmentinfo"];
        
        __block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithPropertyIdentifier:[appointmentInfo valueForKey:kPropertyId]
                                                                                                          appointmentId:[appointmentInfo valueForKey:kAppointmentId]];
        CLS_LOG(@"Property ID: %@, Appointment ID: %@",
                [appointmentInfo valueForKey:kPropertyId],
                [appointmentInfo valueForKey:kAppointmentId]);
        
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            Appointment *appointment = nil;
            if(existingAppointment != nil)
            {
                appointment = (Appointment *)[managedObjectContext objectWithID:existingAppointment.objectID];
                NSDictionary *common = [surveyData objectForKey:kSurveyCommonTag];
                if(!isEmpty(common)){
                    NSArray *heatingData = [common objectForKey:kSurveyHeatingFuelDataTag];
                    if(!isEmpty(heatingData)){
                        NSMutableArray *heatingEntities = [[NSMutableArray alloc] init];
                        for(NSDictionary *heatingDataDict in heatingData){
                            SurveyHeatingFuelData *heatEnt = [NSEntityDescription insertNewObjectForEntityForName:kSurveyHeatingFuelDataEntity inManagedObjectContext:managedObjectContext];
                            heatEnt.fuelTypeId = [heatingDataDict objectForKey:kHeatingFuelSurveyDataEntFuelTypeId];
                            heatEnt.heatingFuelSurvey = [heatingDataDict JSONRepresentation];
                            heatEnt.heatingFuelSurveyToAppointment = appointment;
                            [heatingEntities addObject:heatEnt];
                        }
                        if(!isEmpty(heatingEntities)){
                            appointment.appointmentToSurveyTemplates = [NSSet setWithArray:heatingEntities];
                        }
                    }
                }
                
            }
            
            
            [self saveDataInDB:managedObjectContext];
        }]; // parent
    }
}

-(void) saveComponentsForAppointmentSurvey:(NSDictionary*)surveyData{
    if(!isEmpty(surveyData)){
        NSDictionary *appointmentInfo = [surveyData objectForKey:@"appointmentinfo"];
        
        __block Appointment *existingAppointment = [[PSCoreDataManager sharedManager] appointmentWithPropertyIdentifier:[appointmentInfo valueForKey:kPropertyId]
                                                                                                          appointmentId:[appointmentInfo valueForKey:kAppointmentId]];
        CLS_LOG(@"Property ID: %@, Appointment ID: %@",
                [appointmentInfo valueForKey:kPropertyId],
                [appointmentInfo valueForKey:kAppointmentId]);
        
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            Appointment *appointment = nil;
            if(existingAppointment != nil)
            {
                appointment = (Appointment *)[managedObjectContext objectWithID:existingAppointment.objectID];
                
                NSDictionary *common = [surveyData objectForKey:kSurveyCommonTag];
                if(!isEmpty(common)){
                    NSArray *plannedComps = [common objectForKey:kSurveyPlannedComponentsTag];
                    if(!isEmpty(plannedComps)){
                        NSMutableArray *plannedCompsEnts = [[NSMutableArray alloc] init];
                        for(NSDictionary * compDict in plannedComps){
                            Component *compEnt = [NSEntityDescription insertNewObjectForEntityForName:kSurveyComponentEntity inManagedObjectContext:managedObjectContext];
                            compEnt.componentId = (isEmpty([compDict objectForKey:kComponentEntComponentId]))?nil:[compDict objectForKey:kComponentEntComponentId];
                            compEnt.componentName = (isEmpty([compDict objectForKey:kComponentEntComponentName]))?nil:[compDict objectForKey:kComponentEntComponentName];
                            compEnt.cycle = (isEmpty([compDict objectForKey:kComponentEntCycle]))?nil:[compDict objectForKey:kComponentEntCycle];
                            compEnt.frequency = (isEmpty([compDict objectForKey:kComponentEntFrequency]))?nil:[compDict objectForKey:kComponentEntFrequency];
                            compEnt.itemId = (isEmpty([compDict objectForKey:kComponentEntItemId]))?nil:[compDict objectForKey:kComponentEntItemId];
                            compEnt.parameterId = (isEmpty([compDict objectForKey:kComponentEntParameterId]))?nil:[compDict objectForKey:kComponentEntParameterId];
                            compEnt.subParameterId = (isEmpty([compDict objectForKey:kComponentEntSubParameterId]))?nil:[compDict objectForKey:kComponentEntSubParameterId];
                            compEnt.subValueId = (isEmpty([compDict objectForKey:kComponentEntSubValueId]))?nil:[compDict objectForKey:kComponentEntSubValueId];
                            compEnt.isComponentAccounting =  (isEmpty([compDict objectForKey:kComponentEntIsComponentAccounting]))?nil:[compDict objectForKey:kComponentEntIsComponentAccounting];
                            compEnt.valueId  = (isEmpty([compDict objectForKey:kComponentEntValueId]))?nil:[compDict objectForKey:kComponentEntValueId];
                            compEnt.componentToAppointment = appointment;
                            [plannedCompsEnts addObject:compEnt];
                        }
                        if(!isEmpty(plannedCompsEnts)){
                            appointment.appointmentToSurveyComponents = [NSSet setWithArray:plannedCompsEnts];
                        }
                    }
                }
                
            }
            
            
            [self saveDataInDB:managedObjectContext];
        }]; // parent
    }
}

#pragma mark - Private Methods
#pragma mark - AppointmentData
- (void) loadAppointmentData:(NSDictionary *)appointmentData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(appointmentData))
	{
		NSNumber * appointmentId = [appointmentData valueForKey:kAppointmentId];
		
		__block Appointment *appointment = [[PSCoreDataManager sharedManager] appointmentWithIdentifier:appointmentId context:nil];
		
		if (appointment == nil)
		{
			appointment = [NSEntityDescription insertNewObjectForEntityForName:kAppointment inManagedObjectContext:managedObjectContext];
			
			NSString * appointmentCalendar = [appointmentData valueForKey:kAppointmentCalendar];
			
			//CLS_LOG(@"%@----%@----%@",[appointmentData valueForKey:kAppointmentDate],[appointmentData valueForKey:kAppointmentStartTime],[appointmentData valueForKey:kAppointmentEndTime]);
			
			
			NSDate * appointmentDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kAppointmentDate]];
			
			NSDate * appointmentStartTime = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kAppointmentStartTime]];
			NSDate * appointmentEndTime = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kAppointmentEndTime]];
			
			NSString * appointmentLocation = [appointmentData valueForKey:kAppointmentLocation];
			NSString * appointmentNotes = [appointmentData valueForKey:kAppointmentNotes];
			NSNumber * appointmentOverdue = [appointmentData valueForKey:kAppointmentOverdue];
			NSString * appointmentShift = [appointmentData valueForKey:kAppointmentShift];
			NSString * appointmentStatus = [appointmentData valueForKey:kAppointmentStatus];
			NSString * appointmentTitle = [appointmentData valueForKey:kAppointmentTitle];
			NSString * appointmentType = [appointmentData valueForKey:kAppointmentType];
			NSNumber * assignedTo = [appointmentData valueForKey:kAssignedTo];
			NSNumber * createdBy = [appointmentData valueForKey:kCreatedBy];
			NSNumber * defaultCustomerId = [appointmentData valueForKey:kDefaultCustomerId];
			NSNumber * defaultCustomerIndex = [appointmentData valueForKey:kDefaultCustomerIndex];
			NSNumber * journalHistoryId = [appointmentData valueForKey:kJournalHistoryId];
			NSNumber * journalId = [appointmentData valueForKey:kJournalId];
			NSNumber * jsgNumber = [appointmentData valueForKey:kJsgNumber];
			NSDate * loggedDate = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kLoggedDate]];
			NSString * noEntryNotes = [appointmentData valueForKey:kNoEntryNotes];
			NSDate * repairCompletionDateTime = [UtilityClass convertServerDateToNSDate:[appointmentData valueForKey:kRepairCompletionDateTime]];
			NSString * repairNotes = [appointmentData valueForKey:kRepairNotes];
			id surveyCheckListForm = nil;
			NSString * surveyJSON = [appointmentData valueForKey:kSurveyFormJSON];
			NSString * surveyorAlert = [appointmentData valueForKey:kSurveyorAlert];
			NSString * surveyorUserName = [appointmentData valueForKey:kSurveyorUserName];
			NSString * surveyourAvailability = [appointmentData valueForKey:kSurveyourAvailability];
			NSString * createdByPerson = [appointmentData valueForKey:kCreatedByPerson];
			NSString * surveyType = [appointmentData valueForKey:kSurveyType];
			NSNumber * isMiscAppointment = [appointmentData valueForKey:kIsMiscAppointment];
			NSNumber * tenancyId = [appointmentData valueForKey:kTenancyId];
			NSNumber *addToCalendar = [appointmentData valueForKey:kAppointmentAddtoCalendar];
            NSNumber *syncStatus = [appointmentData valueForKey:@"syncStatus"];
            
            
			appointment.appointmentCalendar = isEmpty(appointmentCalendar)?nil:appointmentCalendar;
			appointment.appointmentEndTime = isEmpty(appointmentEndTime)?nil:appointmentEndTime;
			appointment.appointmentId = isEmpty(appointmentId)?nil:appointmentId;
			appointment.appointmentLocation = isEmpty(appointmentLocation)?nil:appointmentLocation;
			appointment.appointmentNotes = isEmpty(appointmentNotes)?nil:appointmentNotes;
			appointment.appointmentOverdue = isEmpty(appointmentOverdue)?nil:appointmentOverdue;
			appointment.appointmentShift = isEmpty(appointmentShift)?nil:appointmentShift;
			appointment.appointmentStartTime = isEmpty(appointmentStartTime)?nil:appointmentStartTime;
			appointment.appointmentDate = isEmpty(appointmentStartTime)?nil: appointmentStartTime;
			appointment.appointmentStatus = isEmpty(appointmentStatus)?nil:appointmentStatus;
			appointment.appointmentTitle = isEmpty(appointmentTitle)?nil:appointmentTitle;
			appointment.appointmentType = isEmpty(appointmentType)?nil:appointmentType;
			appointment.assignedTo = isEmpty(assignedTo)?nil:assignedTo;
			appointment.createdBy = isEmpty(createdBy)?nil:createdBy;
			appointment.defaultCustomerId = isEmpty(defaultCustomerId)?nil:defaultCustomerId;
			appointment.defaultCustomerIndex = isEmpty(defaultCustomerIndex)?nil:defaultCustomerIndex;
			appointment.journalHistoryId = isEmpty(journalHistoryId)?nil:journalHistoryId;
			appointment.journalId = isEmpty(journalId)?nil:journalId;
			appointment.jsgNumber = isEmpty(jsgNumber)?nil:jsgNumber;
			appointment.loggedDate = isEmpty(loggedDate)?nil:loggedDate;
			appointment.noEntryNotes = isEmpty(noEntryNotes)?nil:noEntryNotes;
			appointment.repairCompletionDateTime = isEmpty(repairCompletionDateTime)?nil:repairCompletionDateTime;
			appointment.repairNotes = isEmpty(repairNotes)?nil:repairNotes;
			appointment.surveyCheckListForm = isEmpty(surveyCheckListForm)?nil:surveyCheckListForm;
			appointment.appointmentToSurvey.surveyJSON = isEmpty(surveyJSON)?nil:surveyJSON;
			appointment.surveyorAlert = isEmpty(surveyorAlert)?nil:surveyorAlert;
			appointment.surveyorUserName = isEmpty(surveyorUserName)?nil:surveyorUserName;
			appointment.surveyourAvailability = isEmpty(surveyourAvailability)?nil:surveyourAvailability;
			appointment.surveyType = isEmpty(surveyType)?nil:surveyType;
			appointment.tenancyId = isEmpty(tenancyId)?nil:tenancyId;
			appointment.createdByPerson = isEmpty(createdByPerson)?nil:createdByPerson;
			appointment.isModified = [NSNumber numberWithBool:NO];
			appointment.isSurveyChanged = [NSNumber numberWithBool:NO];
			appointment.addToCalendar = /*[NSNumber numberWithInt:1];*/ isEmpty(addToCalendar)?nil:addToCalendar;
			appointment.isMiscAppointment = isEmpty(isMiscAppointment)?[NSNumber numberWithBool:NO]:isMiscAppointment;
			appointment.failedReason = nil;
			appointment.syncStatus = isEmpty(syncStatus)?@0:syncStatus;
			//Appointment Releationsship Keys
			NSDictionary *appInfoData = [appointmentData valueForKey:kAppointmentToAppInfoData];
			[[PSDataPersistenceManager sharedManager] loadAppointmentAppInfoData:appInfoData appointment:appointment managedObjectContext:managedObjectContext];
			
			NSDictionary *CP12Info = [appointmentData valueForKey:kAppointmentToCP12Info];
			[[PSDataPersistenceManager sharedManager] loadAppointmentCP12Info:CP12Info appointment:appointment managedObjectContext:managedObjectContext];
			
			NSArray *customerList = [appointmentData valueForKey:kAppointmentToCustomer];
			[[PSDataPersistenceManager sharedManager] loadAppointmentCustomerData:customerList appointment:appointment managedObjectContext:managedObjectContext];
			
			NSArray *jobDataList = [appointmentData valueForKey:kAppointmentToJobDataList];
			[[PSDataPersistenceManager sharedManager] loadAppointmentJobDataList:jobDataList appointment:appointment managedObjectContext:managedObjectContext];
			
			NSDictionary * tradeComponent = [appointmentData valueForKey:kAppointmentToComponentTrade];
			[[PSDataPersistenceManager sharedManager] loadAppointmentTradeComponent:tradeComponent appointment:appointment managedObjectContext:managedObjectContext];
			
			
			NSDictionary *journalData = [appointmentData valueForKey:kAppointmentToJournal];
			[[PSDataPersistenceManager sharedManager] loadAppointmentJournalData:journalData appointment:appointment managedObjectContext:managedObjectContext];
			
			NSDictionary *propertyData = [appointmentData valueForKey:kAppointmentToProperty];
			NSDictionary *schemeData = [appointmentData valueForKey:kAppointmentToScheme];
			if([propertyData isEqual:[NSNull null]])
			{
				propertyData = nil;
			}
			if(propertyData)
			{
				[[PSDataPersistenceManager sharedManager] loadAppointmentPropertyData:propertyData appointment:appointment managedObjectContext:managedObjectContext];
				appointment.appointmentTitle = [appointment.appointmentToProperty addressWithStyle:PropertyAddressStyleShort];
			}
			else
			{
				NSDictionary *schemeData = [appointmentData valueForKey:kAppointmentToScheme];
				[[PSDataPersistenceManager sharedManager] loadAppointmentSchemeData:schemeData appointment:appointment managedObjectContext:managedObjectContext];
				appointment.appointmentTitle = appointment.appointmentToScheme.schemeName;
			}
			//Add Appointment into logged in user.
			User *loggedInUser = (User *)[managedObjectContext existingObjectWithID:[[SettingsClass sharedObject].loggedInUser objectID] error:nil];
			if(loggedInUser)
			{
				[loggedInUser addUserToAppointmentsObject:appointment];
				appointment.appointmentToUser = loggedInUser;
			}
			
			//Populate Aggregate Values
			
			__block NSString *appointmentEventIdentifier;
			if ([appointment.addToCalendar boolValue] && appointment.appointmentStatus !=[UtilityClass statusStringForAppointmentStatus: [UtilityClass completionStatusForAppointmentType:[appointment getType]]])
			{
				NSString *appointmentTitle = [NSString stringWithFormat:@"%@: %@ (%@)",appointment.appointmentType, appointment.appointmentTitle, appointment.appointmentId];
				[[PSAppointmentsManager sharedManager] deleteCalendarEventForAppointment:appointment];
				NSMutableDictionary *appointmentEventDictionary = [NSMutableDictionary dictionary];
				
				[appointmentEventDictionary setObject:isEmpty(appointment.appointmentEndTime)?[NSNull null]:appointment.appointmentEndTime forKey:kAppointmentEndTime];
				[appointmentEventDictionary setObject:isEmpty(appointment.appointmentStartTime)?[NSNull null]:appointment.appointmentStartTime forKey:kAppointmentStartTime];
				[appointmentEventDictionary setObject:isEmpty(appointmentTitle)?[NSNull null]:appointmentTitle forKey:kAppointmentTitle];
				[appointmentEventDictionary setObject:isEmpty(appointment.surveyorAlert)?[NSNull null]:appointment.surveyorAlert forKey:kSurveyorAlert];
				[appointmentEventDictionary setObject:isEmpty(appointment.appointmentNotes)?[NSNull null]:appointment.appointmentNotes forKey:kAppointmentNotes];
				[appointmentEventDictionary setObject:isEmpty(appointment.appointmentLocation)?[NSNull null]:appointment.appointmentLocation forKey:kAppointmentLocation];
			}
		}
	}
}

- (NSString *) createAppointmentEvent:(NSDictionary *)calendarEventDictionary inCalendar:(NSString *)calendar
{
	__block NSString *appointmentEventIdentifier;
	[[PSEventsManager sharedManager] createNewEventWithDictionary:calendarEventDictionary inCalendar:calendar completion:^(NSString *eventIdentifer, NSError *error) {
		appointmentEventIdentifier = eventIdentifer;
	}];
	
	return appointmentEventIdentifier;
}
#pragma mark - SurveyorData
- (void) loadSurveyorData:(NSDictionary *)surveyorData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(surveyorData))
	{
		Surveyor *surveyor = [[PSCoreDataManager sharedManager] surveyorWithIdentifier:[surveyorData valueForKey:kUserId]];
		
		if(surveyor != nil)
		{
			//User already exists in database, so just update the existing object.
			surveyor = (Surveyor *)[managedObjectContext existingObjectWithID:surveyor.objectID error:nil];
		}
		else
		{
			//User does not exists in database, so create new user entity.
			surveyor = (Surveyor *)[NSEntityDescription insertNewObjectForEntityForName:kSurveyor inManagedObjectContext:managedObjectContext];
		}
		
		NSString * fullName = [surveyorData valueForKey:kFullName];
		NSNumber * userId = [surveyorData valueForKey:kUserId];
		NSString * userName = [surveyorData valueForKey:kUserName];
		NSString * surveyorType = [[SettingsClass sharedObject] applicationTypeName];
		
		surveyor.fullName = isEmpty(fullName)?nil:fullName;
		surveyor.userId = isEmpty(userId)?nil:userId;
		surveyor.userName = isEmpty(userName)?nil:userName;
		surveyor.surveyorType = isEmpty(surveyorType)?nil:surveyorType;
		surveyor.surveyorNameSectionIdentifier = [surveyor surveyorNameSectionIdentifier];
	}
}

#pragma mark - SurveyData
- (void) loadAppointmentSurvey:(NSDictionary *)surveyData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	
}

#pragma mark - SPropertyData
- (void) loadSPropertyData:(NSDictionary *)propertyData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(propertyData))
	{
		NSDictionary *propertyDictionary = [NSDictionary dictionaryWithDictionary:[propertyData objectForKey:@"property"]];
		SProperty *property = [[PSCoreDataManager sharedManager] sPropertyWithIdentifier:[propertyDictionary valueForKey:kPropertyId]];
		
		if(property != nil)
		{
			//User already exists in database, so just update the existing object.
			property = (SProperty *)[managedObjectContext existingObjectWithID:property.objectID error:nil];
		}
		else
		{
			//User does not exists in database, so create new user entity.
			property = (SProperty *)[NSEntityDescription insertNewObjectForEntityForName:kSProperty inManagedObjectContext:managedObjectContext];
		}
		
		NSString *address1 = [propertyDictionary valueForKey:kAddress1];
		NSString *address2 = [propertyDictionary valueForKey:kAddress2];
		NSString *address3 = [propertyDictionary valueForKey:kAddress3];
		NSDate * certificateExpiry = [UtilityClass convertServerDateToNSDate:[propertyDictionary valueForKey:kCertificateExpiry]];
		NSString *county = [propertyDictionary valueForKey:kCounty];
		NSString *flatNumber = [propertyDictionary valueForKey:kFlatNumber];
		NSString *houseNumber = [propertyDictionary valueForKey:kHouseNumber];
		NSString *postCode = [propertyDictionary valueForKey:kPostCode];
		NSString *propertyId = [propertyDictionary valueForKey:kPropertyId];
		NSString *townCity = [propertyDictionary valueForKey:kTownCity];
		NSString *firstName = [propertyData valueForKey:kCustomerFirstName];
		NSString *lastName = [propertyData valueForKey:kCustomerLastName];
		NSString *middleName = [propertyData valueForKey:kCustomerMiddleName];
		NSString *title = [propertyData valueForKey:kCustomerTitle];
		NSString *sPropertyJSON = [propertyData JSONRepresentation];
		
		property.address1 = isEmpty(address1)?nil:address1;
		property.address2 = isEmpty(address2)?nil:address2;
		property.address3 = isEmpty(address3)?nil:address3;
		property.certificateExpiry = isEmpty(certificateExpiry) ? nil : certificateExpiry;
		property.county = isEmpty(county)?nil:county;
		property.flatNumber = isEmpty(flatNumber)?nil:flatNumber;
		property.houseNumber = isEmpty(houseNumber)?nil:houseNumber;
		property.postCode = isEmpty(postCode)?nil:postCode;
		property.propertyId = isEmpty(propertyId)?nil:propertyId;
		property.townCity = isEmpty(townCity)?nil:townCity;
		property.firstName = isEmpty(firstName)?nil:firstName;
		property.middleName = isEmpty(middleName)?nil:middleName;
		property.lastName = isEmpty(lastName)?nil:lastName;
		property.title = isEmpty(title)?nil:[title stringByAppendingString:@"."];
		property.sPropertyJSON = isEmpty(sPropertyJSON)?nil:sPropertyJSON;
		property.sPropertyNameSectionIdentifier = [property sPropertyNameSectionIdentifier];
		property.fullName = [property fullNameWithoutTitle];
        
        NSMutableString *compAdd =[NSMutableString stringWithString:@""];
        if(!isEmpty(property.houseNumber)){
            [compAdd appendString:property.houseNumber];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.address1)){
            [compAdd appendString:property.address1];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.address2)){
            [compAdd appendString:property.address2];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.address3)){
            [compAdd appendString:property.address3];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.townCity)){
            [compAdd appendString:property.townCity];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.postCode)){
            [compAdd appendString:property.postCode];
        }
        property.completeAddress = compAdd;
        
        NSMutableString *customerName =[NSMutableString stringWithString:@""];
        if(!isEmpty(property.title)){
            [customerName appendString:property.title];
            [customerName appendString:@"."];
            [customerName appendString:@" "];
        }
        if(!isEmpty(property.firstName)){
            [customerName appendString:property.firstName];
            [customerName appendString:@" "];
        }
        if(!isEmpty(property.middleName)){
            [customerName appendString:property.middleName];
            [customerName appendString:@" "];
        }
        if(!isEmpty(property.lastName)){
            [customerName appendString:property.lastName];
        }
        property.completeName = [NSString stringWithFormat:@"%@",customerName];
		
	}
}

#pragma mark - AppInfoData
- (void) loadAppointmentAppInfoData:(NSDictionary *)appInfoDictionary appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(appInfoDictionary))
	{
		AppInfoData *appInfoData = [NSEntityDescription insertNewObjectForEntityForName:kAppInfoData inManagedObjectContext:managedObjectContext];
		
		NSNumber * totalAppointments = [appInfoDictionary valueForKey:@"TotalAppointments"];
		NSNumber * totalNoEntries = [appInfoDictionary valueForKey:@"TotalNoEntries"];
		
		totalAppointments = isEmpty(totalAppointments)?nil:totalAppointments;
		totalNoEntries = isEmpty(totalNoEntries)?nil:totalNoEntries;
		
		appInfoData.totalAppointments = totalAppointments;
		appInfoData.totalNoEntries = totalNoEntries;
		
		//Set the Inverse Relationship
		appInfoData.appInfoDataToAppointment = appointment;
		appointment.appointmentToAppInfoData = appInfoData;
	}
}

#pragma mark - CP12Info
- (void) loadAppointmentCP12Info:(NSDictionary *)CP12InfoDictionary appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(CP12InfoDictionary))
	{
		CP12Info *cp12Info = [NSEntityDescription insertNewObjectForEntityForName:kCP12Info inManagedObjectContext:managedObjectContext];
		
		NSString * cp12Document = [CP12InfoDictionary valueForKey:@"CP12Document"];
		NSString * cp12Number = [CP12InfoDictionary valueForKey:@"CP12Number"];
		NSDate * dateTimeStamp = [UtilityClass convertServerDateToNSDate:[CP12InfoDictionary valueForKey:@"DTimeStamp"]];
		NSString * documentType = [CP12InfoDictionary valueForKey:@"DocumentType"];
		NSNumber * inspectionCarried = [CP12InfoDictionary valueForKey:@"InspectionCarried"];
		NSNumber * issuedBy = [CP12InfoDictionary valueForKey:@"IssuedBy"];
		NSDate * issuedDate = [UtilityClass convertServerDateToNSDate:[CP12InfoDictionary valueForKey:@"IssuedDate"]];
		NSString * issuedDateString = [CP12InfoDictionary valueForKey:@"IssuedDateString"];
		NSNumber * journalId = [CP12InfoDictionary valueForKey:@"JournalId"];
		NSNumber * jsgNumber = [CP12InfoDictionary valueForKey:@"jsgNumber"];
		NSNumber * lsgrid = [CP12InfoDictionary valueForKey:@"LGSRID"];
		NSString * notes = [CP12InfoDictionary valueForKey:@"Notes"];
		NSString * propertyID = [CP12InfoDictionary valueForKey:kPropertyId];
		NSDate * receivedDate = [UtilityClass convertServerDateToNSDate:[CP12InfoDictionary valueForKey:@"ReceivedDate"]];
		NSString * receivedDateString = [CP12InfoDictionary valueForKey:@"ReceivedDateString"];
		NSString * receivedOnBehalfOf = [CP12InfoDictionary valueForKey:@"ReceivedOnBehalfOf"];
		NSNumber * tenantHanded = [CP12InfoDictionary valueForKey:@"TenantHanded"];
		
		cp12Document = isEmpty(cp12Document)?nil:cp12Document;
		cp12Number = isEmpty(cp12Number)?nil:cp12Number;
		dateTimeStamp = isEmpty(dateTimeStamp)?nil:dateTimeStamp;
		documentType = isEmpty(documentType)?nil:documentType;
		inspectionCarried = isEmpty(inspectionCarried)?nil:inspectionCarried;
		issuedBy = isEmpty(issuedBy)?nil:issuedBy;
		issuedDate = isEmpty(issuedDate)?nil:issuedDate;
		issuedDateString = isEmpty(issuedDateString)?nil:issuedDateString;
		journalId = isEmpty(journalId)?nil:journalId;
		jsgNumber = isEmpty(jsgNumber)?nil:jsgNumber;
		lsgrid = isEmpty(lsgrid)?nil:lsgrid;
		notes = isEmpty(notes)?nil:notes;
		propertyID = isEmpty(propertyID)?nil:propertyID;
		receivedDate = isEmpty(receivedDate)?nil:receivedDate;
		receivedDateString = isEmpty(receivedDateString)?nil:receivedDateString;
		receivedOnBehalfOf = isEmpty(receivedOnBehalfOf)?nil:receivedOnBehalfOf;
		tenantHanded = isEmpty(tenantHanded)?nil:tenantHanded;
		
		cp12Info.cp12Document = cp12Document;
		cp12Info.cp12Number = cp12Number;
		cp12Info.dateTimeStamp = dateTimeStamp;
		cp12Info.documentType = documentType;
		cp12Info.inspectionCarried =inspectionCarried ;
		cp12Info.issuedBy = issuedBy;
		cp12Info.issuedDate = issuedDate;
		cp12Info.issuedDateString = issuedDateString;
		cp12Info.journalId = journalId;
		cp12Info.jsgNumber = jsgNumber;
		cp12Info.lsgrid = lsgrid;
		cp12Info.notes = notes;
		cp12Info.propertyID = propertyID;
		cp12Info.receivedDate = receivedDate;
		cp12Info.receivedDateString = receivedDateString;
		cp12Info.receivedOnBehalfOf = receivedOnBehalfOf;
		cp12Info.tenantHanded = tenantHanded;
		
		//Set the Inverse Relationship
		cp12Info.cp12InfoToAppointment = appointment;
		appointment.appointmentToCP12Info = cp12Info;
	}
}

#pragma mark - CustomerList
- (void) loadAppointmentCustomerData:(NSArray *)customerList appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(customerList))
	{
		for(NSDictionary *customerDictionary in customerList)
		{
			if(!isEmpty(customerDictionary))
			{
				Customer *customer = [NSEntityDescription insertNewObjectForEntityForName:kCustomer inManagedObjectContext:managedObjectContext];
				
				NSNumber * customerId = [customerDictionary valueForKey:kCustomerId];
				NSString * address = [customerDictionary valueForKey:kCustomerAddress];
				NSString * email = [customerDictionary valueForKey:kCustomerEmail];
				NSString * fax = [customerDictionary valueForKey:kCustomerFax];
				NSString * firstName = [customerDictionary valueForKey:kCustomerFirstName];
				NSString * lastName = [customerDictionary valueForKey:kCustomerLastName];
				NSString * middleName = [customerDictionary valueForKey:kCustomerMiddleName];
				NSString * mobile = [customerDictionary valueForKey:kCustomerMobile];
				NSString * property = [customerDictionary valueForKey:kCustomerProperty];
				NSString * telephone = [customerDictionary valueForKey:kCustomerTelephone];
				NSString * title = [customerDictionary valueForKey:kCustomerTitle];
				
				address = isEmpty(address)?nil:address;
				customerId = isEmpty(customerId)?nil:customerId;
				email = isEmpty(email)?nil:email;
				fax = isEmpty(fax)?nil:fax;
				firstName = isEmpty(firstName)?nil:firstName;
				lastName = isEmpty(lastName)?nil:lastName;
				middleName = isEmpty(middleName)?nil:middleName;
				mobile = isEmpty(mobile)?nil:mobile;
				property = isEmpty(property)?nil:property;
				telephone = isEmpty(telephone)?nil:telephone;
				title = isEmpty(title)?nil:title;
				
				customer.address = address;
				customer.customerId = customerId;
				customer.email = email;
				customer.fax = fax;
				customer.firstName = firstName;
				customer.lastName = lastName;
				customer.middleName = middleName;
				customer.mobile = mobile;
				customer.property = property;
				customer.telephone = telephone;
				customer.title = title;
				
                NSMutableString *customerName =[NSMutableString stringWithString:@""];
                if(!isEmpty(customer.title)){
                    [customerName appendString:customer.title];
                    [customerName appendString:@"."];
                    [customerName appendString:@" "];
                }
                if(!isEmpty(customer.firstName)){
                    [customerName appendString:customer.firstName];
                    [customerName appendString:@" "];
                }
                if(!isEmpty(customer.middleName)){
                    [customerName appendString:customer.middleName];
                    [customerName appendString:@" "];
                }
                if(!isEmpty(customer.lastName)){
                    [customerName appendString:customer.lastName];
                }
                customer.completeName = [NSString stringWithFormat:@"%@",customerName];
                
				//Customer Releationsship Keys
				NSDictionary *customerAddressData = [customerDictionary valueForKey:@"Address"];
				[[PSDataPersistenceManager sharedManager] loadCustomerAddressData:customerAddressData customer:customer managedObjectContext:managedObjectContext];
				
				NSArray *customerRiskData = [customerDictionary valueForKey:@"customerRiskData"];
				[[PSDataPersistenceManager sharedManager] loadCustomerRiskData:customerRiskData customer:customer managedObjectContext:managedObjectContext];
				
				NSArray *customerVulnerabilityData = [customerDictionary valueForKey:@"customerVulnerabilityData"];
				[[PSDataPersistenceManager sharedManager] loadCustomerVulunarityData:customerVulnerabilityData customer:customer managedObjectContext:managedObjectContext];
				
				//Set the Inverse Relationship
				//customer.customerToAppointment = appointment;
				[appointment addAppointmentToCustomerObject:customer];
			}
		}
		
	}
}

- (void) loadCustomerAddressData:(NSDictionary *)customerAddressDataDictionary customer:(Customer *)customer managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(customer && !isEmpty(customerAddressDataDictionary))
	{
		Address *customerAddress = [NSEntityDescription insertNewObjectForEntityForName:kAddress inManagedObjectContext:managedObjectContext];
		
		NSString * address = [customerAddressDataDictionary valueForKey:@"address"];
		NSNumber * addressID = [customerAddressDataDictionary valueForKey:@"addressID"];
		NSString * houseNo = [customerAddressDataDictionary valueForKey:@"houseNo"];
		NSString * postCode = [customerAddressDataDictionary valueForKey:@"postCode"];
		NSString * townCity = [customerAddressDataDictionary valueForKey:@"townCity"];
		
		
		address = isEmpty(address)?nil:address;
		addressID = isEmpty(addressID)?nil:addressID;
		houseNo = isEmpty(houseNo)?nil:houseNo;
		postCode = isEmpty(postCode)?nil:postCode;
		townCity = isEmpty(townCity)?nil:townCity;
		
		customerAddress.address = address;
		customerAddress.addressID = addressID;
		customerAddress.houseNo = houseNo;
		customerAddress.postCode = postCode;
		customerAddress.townCity = townCity;
        
        NSMutableString *addressComp =[NSMutableString stringWithString:@""];
        if(!isEmpty(customerAddress.houseNo)){
            [addressComp appendString:customerAddress.houseNo];
            [addressComp appendString:@" "];
        }
        if(!isEmpty(customerAddress.address)){
            [addressComp appendString:customerAddress.address];
            [addressComp appendString:@" "];
        }
        if(!isEmpty(customerAddress.townCity)){
            [addressComp appendString:customerAddress.townCity];
            [addressComp appendString:@" "];
        }
        if(!isEmpty(customerAddress.postCode)){
            [addressComp appendString:customerAddress.postCode];
        }
        
        customerAddress.completeAddress = [NSString stringWithFormat:@"%@",addressComp];
		
		//Set the Inverse Relationship
		customerAddress.addressToCustomer = customer;
		customer.customerToAddress = customerAddress;
	}
}

- (void) loadCustomerRiskData:(NSArray *)customerRiskDataList customer:(Customer *)customer managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(customer && !isEmpty(customerRiskDataList))
	{
		for(NSDictionary *customerRiskDataDictionary in customerRiskDataList)
		{
			if(!isEmpty(customerRiskDataDictionary))
			{
				CustomerRiskData *customerRiskData = [NSEntityDescription insertNewObjectForEntityForName:kCustomerRiskData inManagedObjectContext:managedObjectContext];
				
				NSString * riskCatDesc = [customerRiskDataDictionary valueForKey:@"riskCatDesc"];;
				NSString * riskSubCatDesc = [customerRiskDataDictionary valueForKey:@"riskSubCatDesc"];
				
				riskCatDesc = isEmpty(riskCatDesc)?nil:riskCatDesc;
				riskSubCatDesc = isEmpty(riskSubCatDesc)?nil:riskSubCatDesc;
				
				customerRiskData.riskCatDesc = riskCatDesc;
				customerRiskData.riskSubCatDesc = riskSubCatDesc;
				
				customerRiskData.customerRiskDataToCustomer = customer;
				[customer addCustomerToCustomerRiskDataObject:customerRiskData];
			}
		}
	}
}

- (void) loadCustomerVulunarityData:(NSArray *)customerVulnerabilityDataList customer:(Customer *)customer managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(customer && !isEmpty(customerVulnerabilityDataList))
	{
		for (NSDictionary *customerVulnerabilityDataDictionary in customerVulnerabilityDataList)
		{
			if (!isEmpty(customerVulnerabilityDataDictionary)) {
				CustomerVulnerabilityData *customerVulnerabilityData = [NSEntityDescription insertNewObjectForEntityForName:kCustomerVulnerabilityData inManagedObjectContext:managedObjectContext];
				
				NSString * vulCatDesc = [customerVulnerabilityDataDictionary valueForKey:@"vulCatDesc"];
				NSNumber * vulHistoryId = [customerVulnerabilityDataDictionary valueForKey:@"vulHistoryId"];
				NSString * vulSubCatDesc = [customerVulnerabilityDataDictionary valueForKey:@"vulSubCatDesc"];
				
				vulCatDesc = isEmpty(vulCatDesc)?nil:vulCatDesc;
				vulHistoryId = isEmpty(vulHistoryId)?nil:vulHistoryId;
				vulSubCatDesc = isEmpty(vulSubCatDesc)?nil:vulSubCatDesc;
				
				customerVulnerabilityData.vulCatDesc = vulCatDesc;
				customerVulnerabilityData.vulHistoryId = vulHistoryId;
				customerVulnerabilityData.vulSubCatDesc = vulSubCatDesc;
				
				//Set the Inverse Relationship
				customerVulnerabilityData.customerVulunarityDataToCustomer = customer;
				[customer addCustomerToCustomerVulunarityDataObject:customerVulnerabilityData];
			}
		}
	}
}

#pragma mark - JobDataList
- (void) loadAppointmentJobDataList:(NSArray *)jobDataList appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(jobDataList))
	{
		for(NSDictionary *jobDataDictionary in jobDataList)
		{
			if(!isEmpty(jobDataDictionary))
			{
				JobDataList *jobData = [NSEntityDescription insertNewObjectForEntityForName:kJobDataList inManagedObjectContext:managedObjectContext];
				NSDate * completionDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:@"completionDate"]];
				NSNumber * duration = [jobDataDictionary valueForKey:kDuration];
				NSString * durationUnit = [jobDataDictionary valueForKey:kDurationUnit];
				NSNumber * faultLogID = [jobDataDictionary valueForKey:kFaultLogID];
				NSString * followOnNotes = [jobDataDictionary valueForKey:kFollowOnNotes];
				NSString * jobStatus = [jobDataDictionary valueForKey:kJobStatus];
				
				NSString * jsnDescription = [jobDataDictionary valueForKey:kJSNDescription];
				NSString * jsnLocation = [jobDataDictionary valueForKey:kJSNLocation];
				NSString * jsnNotes = [jobDataDictionary valueForKey:kJSNNotes];
				NSString * jsNumber = [jobDataDictionary valueForKey:kJSNumber];
				NSString * priority = [jobDataDictionary valueForKey:kPriority];
				NSString * repairNotes = [jobDataDictionary valueForKey:kJobRepairNotes];
				NSDate * reportedDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kReportedDate]];
				NSString * responseTime = [jobDataDictionary valueForKey:kResponseTime];
				jobData.completionDate = isEmpty(completionDate)?nil:completionDate;
				jobData.duration = isEmpty(duration)?nil:duration;
				jobData.faultLogID = isEmpty(faultLogID)?nil:faultLogID;
				jobData.followOnNotes = isEmpty(followOnNotes)?nil:followOnNotes;
				jobData.jobStatus = isEmpty(jobStatus)?nil:jobStatus;
				jobData.jsnDescription = isEmpty(jsnDescription)?nil:jsnDescription;
				jobData.jsnLocation = isEmpty(jsnLocation)?nil:jsnLocation;
				jobData.jsnNotes = isEmpty(jsnNotes)?nil:jsnNotes;
				jobData.jsNumber = isEmpty(jsNumber)?nil:jsNumber;
				jobData.priority = isEmpty(priority)?nil:priority;
				jobData.repairNotes = isEmpty(repairNotes)?nil:repairNotes;
				jobData.reportedDate = isEmpty(reportedDate)?nil:reportedDate;
				jobData.responseTime = isEmpty(responseTime)?nil:responseTime;
				jobData.durationUnit = isEmpty(durationUnit)?nil:durationUnit;
				
				//Job Data Relationship Keys
				NSArray *faultRepairDataList = [jobDataDictionary valueForKey:@"faultRepairList"];
				[[PSDataPersistenceManager sharedManager] loadFaultRepairDataList:faultRepairDataList jobData:jobData managedObjectContext:managedObjectContext];
				NSArray *repairImages = [jobDataDictionary valueForKey:@"repairImageList"];
				if (!isEmpty(repairImages) && [repairImages count] > 0)
				{
					for (NSDictionary *repairImageDictionary in repairImages)
					{
						[self loadRepairPictureData:repairImageDictionary inContext:managedObjectContext];
					}
				}
				
				//Set the Inverse Relationship
				jobData.jobDataListToAppointment = appointment;
				[appointment addAppointmentToJobDataListObject:jobData];
			}
		}
	}
}


#pragma mark - PlannedTradeComponent
- (void) loadAppointmentTradeComponent:(NSDictionary *)jobDataDictionary appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(jobDataDictionary))
	{
		if(!isEmpty(jobDataDictionary))
		{
			PlannedTradeComponent *jobData = [NSEntityDescription insertNewObjectForEntityForName:kTradeComponent inManagedObjectContext:managedObjectContext];
			
			NSString * jsNumber = [jobDataDictionary valueForKey:kJSNumber];
			NSString * jsnNotes = [jobDataDictionary valueForKey:kJSNNotes];
			NSString * pmoDescription = [jobDataDictionary valueForKey:kPMODescription];
			NSNumber * componentTradeId = [jobDataDictionary valueForKey:kComponentTradeId];
			NSDate * completionDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kCompletionDate]];
			NSNumber * componentId = [jobDataDictionary valueForKey:kComponentId];
			NSString * componentName = [jobDataDictionary valueForKey:kComponentName];
			NSString * jobStatus = [jobDataDictionary valueForKey:kJobStatus];
			NSNumber * duration = [jobDataDictionary valueForKey:kDuration];
			NSString * durationUnit = [jobDataDictionary valueForKey:kDurationUnit];
			NSDate * reportedDate = [UtilityClass convertServerDateToNSDate:[jobDataDictionary valueForKey:kReportedDate]];
			NSNumber * sortingOrder = [jobDataDictionary valueForKey:kSortingOrder];
			NSString * tradeDescription = [jobDataDictionary valueForKey:kTradeDescription];
			NSNumber * tradeId = [jobDataDictionary valueForKey:kTradeId];
			NSString * location = [jobDataDictionary valueForKey:kLocation];
			NSNumber * locationId = [jobDataDictionary valueForKey:kLocationId];
			NSString * adaptation = [jobDataDictionary valueForKey:kAdaptation];
			NSNumber * adaptationId = [jobDataDictionary valueForKey:kAdaptationId];
			
			jobData.jsNumber = isEmpty(jsNumber)?nil:jsNumber;
			jobData.jsnNotes = isEmpty(jsnNotes)?nil:jsnNotes;
			jobData.pmoDescription = isEmpty(pmoDescription)?nil:pmoDescription;
			jobData.componentTradeId = isEmpty(componentTradeId)?nil:componentTradeId;
			jobData.completionDate = isEmpty(completionDate)?nil:completionDate;
			jobData.componentId = isEmpty(componentId)?nil:componentId;
			jobData.componentName = isEmpty(componentName)?nil:componentName;
			jobData.duration = isEmpty(duration)?nil:duration;
			jobData.durationUnit = isEmpty(durationUnit)?nil:durationUnit;
			jobData.reportedDate = isEmpty(reportedDate)?nil:reportedDate;
			jobData.sortingOrder = isEmpty(sortingOrder)?nil:sortingOrder;
			jobData.tradeDescription = isEmpty(tradeDescription)?nil:tradeDescription;
			jobData.tradeId = isEmpty(tradeId)?nil:tradeId;
			jobData.location = isEmpty(location)?nil:location;
			jobData.locationId = isEmpty(locationId)?nil:locationId;
			jobData.adaptation = isEmpty(adaptation)?nil:adaptation;
			jobData.adaptationId = isEmpty(adaptationId)?nil:adaptationId;
			
			jobData.jobStatus= isEmpty(jobStatus)?nil:jobStatus;
			
			//Set the Inverse Relationship
			jobData.tradeComponentToAppointment = appointment;
			
		}
	}
}


- (void) loadFaultRepairDataList:(NSArray *)faultRepairDataList jobData:(JobDataList *)jobData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(jobData && !isEmpty(faultRepairDataList))
	{
		for(NSDictionary *faultRepairDictionary in faultRepairDataList)
		{
			if(!isEmpty(faultRepairDictionary))
			{
				/* FaultRepairData *faultRepairData = [NSEntityDescription insertNewObjectForEntityForName:kFaultRepairData inManagedObjectContext:managedObjectContext];*/
				NSString * faultRepairDescription = [faultRepairDictionary valueForKey:kFaultRepairDescription];
				NSNumber * faultRepairID = [faultRepairDictionary valueForKey:kFaultRepairID];
				
				FaultRepairData *faultRepairData = [[PSCoreDataManager sharedManager]faultRepairDataWithID:faultRepairID inContext:managedObjectContext];
				if (faultRepairData != nil)
				{
					faultRepairData = (FaultRepairData *)[managedObjectContext objectWithID:faultRepairData.objectID];
				}
				else
				{
					faultRepairData = [NSEntityDescription insertNewObjectForEntityForName:kFaultRepairData inManagedObjectContext:managedObjectContext];
				}
				
				
				
				
				faultRepairData.faultRepairDescription = isEmpty([faultRepairDescription trimWhitespace])?nil:[faultRepairDescription trimWhitespace];
				faultRepairData.faultRepairID = isEmpty(faultRepairID)?nil:faultRepairID;
				faultRepairData.repairSectionIdentifier = [faultRepairData repairSectionIdentifier];
				
				//Set the Inverse Relationship
				faultRepairData.faultRepairDataToJobDataList = jobData;
				[jobData addFaultRepairDataListObject:faultRepairData];
			}
		}
	}
}

#pragma mark - JournalData
- (void) loadAppointmentJournalData:(NSDictionary *)journalData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(journalData))
	{
		Journal *journal = [NSEntityDescription insertNewObjectForEntityForName:kJournal inManagedObjectContext:managedObjectContext];
		
		NSNumber * actionId = [journalData valueForKey:@"actionId"];
		NSNumber * creationBy = [journalData valueForKey:@"creationBy"];
		NSDate * creationDate = [UtilityClass convertServerDateToNSDate:[journalData valueForKey:@"creationDate"]];
		NSNumber * inspectionTypeId = [journalData valueForKey:@"inspectionTypeId"];
		NSNumber * isCurrent = [journalData valueForKey:@"isCurrent"];
		NSNumber * journalId = [journalData valueForKey:@"journalId"];
		NSString * propertyId = [journalData valueForKey:kPropertyId];
		NSNumber * statusId = [journalData valueForKey:@"statusId"];
		
		
		journal.actionId = isEmpty(actionId)?nil:actionId;
		journal.creationBy = isEmpty(creationBy)?nil:creationBy;
		journal.creationDate = isEmpty(creationDate)?nil:creationDate;
		journal.inspectionTypeId = isEmpty(inspectionTypeId)?nil:inspectionTypeId;
		journal.isCurrent = isEmpty(isCurrent)?nil:isCurrent;
		journal.journalId = isEmpty(journalId)?nil:journalId;
		journal.propertyId = isEmpty(propertyId)?nil:propertyId;
		journal.statusId = isEmpty(statusId)?nil:statusId;
		
		//Set the Inverse Relationship
		journal.journalToAppointment = appointment;
		appointment.appointmentToJournal = journal;
	}
}
#pragma mark - SchemeData
- (void) loadAppointmentSchemeData:(NSDictionary *)schemeData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(schemeData))
	{
		Scheme *scheme = [NSEntityDescription insertNewObjectForEntityForName:kScheme inManagedObjectContext:managedObjectContext];
		
		NSNumber * schemeID = [schemeData valueForKey:kSchemeID];
		NSString * county = [schemeData valueForKey:kSchemeCounty];
		NSString * townCity = [schemeData valueForKey:kSchemCityTown];
		NSNumber * blockId = [schemeData valueForKey:kSchemeBlockID];
		NSString * postCode = [schemeData valueForKey:kSchemePostCode];
		NSString * schemeName = [schemeData valueForKey:kSchemeName];
		NSString * blockName = [schemeData valueForKey:kSchemeBlockName];
		
		scheme.schemeId = isEmpty(schemeID)?nil:schemeID;
		scheme.blockId = isEmpty(blockId)?nil:blockId;
		scheme.schemeName = isEmpty(schemeName)?nil:schemeName;
		scheme.postCode = isEmpty(postCode)?nil:postCode;
		scheme.blockName = isEmpty(blockName)?nil:blockName;
		scheme.county = isEmpty(county)?nil:county;
		scheme.townCity = isEmpty(townCity)?nil:townCity;
		
		//Set the Inverse Relationship
		scheme.schemeToAppointment = appointment;
		appointment.appointmentToScheme = scheme;
	}
}
#pragma mark - PropertyData
- (void) loadAppointmentPropertyData:(NSDictionary *)propertyData appointment:(Appointment *)appointment managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(appointment && !isEmpty(propertyData))
	{
		Property *property = [NSEntityDescription insertNewObjectForEntityForName:kProperty inManagedObjectContext:managedObjectContext];
		
		NSString * propertyId = [propertyData valueForKey:kPropertyId];
		
		
		
		NSString * address1 = [propertyData valueForKey:kAddress1];
		NSString * address2 = [propertyData valueForKey:kAddress2];
		NSString * address3 = [propertyData valueForKey:kAddress3];
		NSDate * certificateExpiry = [UtilityClass convertServerDateToNSDate:[propertyData valueForKey:kCertificateExpiry]];
		NSString * county = [propertyData valueForKey:kCounty];
		NSString * flatNumber = [propertyData valueForKey:kFlatNumber];
		NSString * houseNumber = [propertyData valueForKey:kHouseNumber];
		NSDate * lastSurveyDate = [UtilityClass convertServerDateToNSDate:[propertyData valueForKey:kLastSurveyDate]];
		NSString * postCode = [propertyData valueForKey:kPostCode];
		
		NSNumber * tenancyId = [propertyData valueForKey:kTenancyId];
		NSString * townCity = [propertyData valueForKey:kTownCity];
		
		property.address1 = isEmpty(address1)?nil:address1;
		property.address2 = isEmpty(address2)?nil:address2;
		property.address3 = isEmpty(address3)?nil:address3;
		property.certificateExpiry = isEmpty(certificateExpiry)?nil:certificateExpiry;
		property.county = isEmpty(county)?nil:county;
		property.flatNumber = isEmpty(flatNumber)?nil:flatNumber;
		property.houseNumber = isEmpty(houseNumber)?nil:houseNumber;
		property.lastSurveyDate = isEmpty(lastSurveyDate)?nil:lastSurveyDate;
		property.postCode = isEmpty(postCode)?nil:postCode;
		property.propertyId = isEmpty(propertyId)?nil:propertyId;
		property.tenancyId = isEmpty(tenancyId)?nil:tenancyId;
		property.townCity = isEmpty(townCity)?nil:townCity;
		
        
        NSMutableString *compAdd =[NSMutableString stringWithString:@""];
        if(!isEmpty(property.houseNumber)){
            [compAdd appendString:property.houseNumber];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.address1)){
            [compAdd appendString:property.address1];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.address2)){
            [compAdd appendString:property.address2];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.address3)){
            [compAdd appendString:property.address3];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.townCity)){
            [compAdd appendString:property.townCity];
            [compAdd appendString:@" "];
        }
        if(!isEmpty(property.postCode)){
            [compAdd appendString:property.postCode];
        }
        property.completeAddress = compAdd;
        
        
		//Property Releationsship Keys
		NSArray *propertyAccomodationsList = [propertyData valueForKey:@"Accommodations"];
		[[PSDataPersistenceManager sharedManager] loadPropertyAccomodationsData:propertyAccomodationsList property:property managedObjectContext:managedObjectContext];
		
		NSArray *propertyAsbestosDataList = [propertyData valueForKey:@"propertyAsbestosData"];
		[[PSDataPersistenceManager sharedManager] loadPropertyAsbestosData:propertyAsbestosDataList property:property managedObjectContext:managedObjectContext];
		
		NSNumber * defaultPicId=[propertyData valueForKey:@"defaultPropertyPicId"];
		
		NSArray *propertyPicturesList = [propertyData valueForKey:@"propertyPicture"];
		[[PSDataPersistenceManager sharedManager] loadPropertyPicturesData:propertyPicturesList property:property defaultPictureId:defaultPicId managedObjectContext:managedObjectContext];
		
		/*
		 Setup default pic to property default pic
		 */
		
		
		
		
		NSMutableDictionary *installationPipework = [propertyData valueForKey:@"installationpipework"];
		
		NSString *dateString = [installationPipework valueForKey:kInspectionDate];
		if (!isEmpty(dateString))
		{
			NSDate *date = [UtilityClass convertServerDateToNSDate:dateString];
			[installationPipework setObject:date forKey:kInspectionDate];
		}
		[[PSDataPersistenceManager sharedManager]loadPipeworkForm:installationPipework forProperty:property managedObjectContext:managedObjectContext];
		//Set the Inverse Relationship
		property.propertyToAppointment = appointment;
		appointment.appointmentToProperty = property;
	}
}

- (void) loadPropertyAccomodationsData:(NSArray *)propertyAccomodationsList property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(property && !isEmpty(propertyAccomodationsList))
	{
		for(NSDictionary *propertyAccomodationsDictionary in propertyAccomodationsList)
		{
			if(!isEmpty(propertyAccomodationsDictionary))
			{
				Accomodation *accomodation = [NSEntityDescription insertNewObjectForEntityForName:kAccomodation inManagedObjectContext:managedObjectContext];
				
				NSString *propertyId = [propertyAccomodationsDictionary valueForKey:kPropertyId];
				NSString *roomName = [propertyAccomodationsDictionary valueForKey:kRoomName];
				NSNumber *roomHeight = [propertyAccomodationsDictionary valueForKey:kRoomHeight];
				NSNumber *roomWidth = [propertyAccomodationsDictionary valueForKey:kRoomWidth];
				NSNumber *roomLength = [propertyAccomodationsDictionary valueForKey:kRoomLength];
				NSNumber *updatedBy = [propertyAccomodationsDictionary valueForKey:kUpdatedBy];
				//NSDate *updatedOn = [propertyAccomodationsDictionary valueForKey:kUpdatedOn];
				NSDate *updatedOn = [UtilityClass convertServerDateToNSDate:[propertyAccomodationsDictionary valueForKey:kUpdatedOn]];
				accomodation.propertyId = isEmpty(propertyId)?nil:propertyId;
				accomodation.roomHeight = isEmpty(roomHeight)?nil:roomHeight;
				accomodation.roomLength = isEmpty(roomLength)?nil:roomLength;
				accomodation.roomName = isEmpty(roomName)?nil:roomName;
				accomodation.roomWidth = isEmpty(roomWidth)?nil:roomWidth;
				accomodation.updatedBy = isEmpty(updatedBy)?nil:updatedBy;
				accomodation.updatedOn = isEmpty(updatedOn)?nil:updatedOn;
				
				
				
				//Set the Inverse Relationship
				accomodation.accomodationToProperty = property;
				[property addPropertyToAccomodationObject:accomodation];
			}
		}
	}
}

- (void) loadPropertyAsbestosData:(NSArray *)propertyAsbestosDataList property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(property && !isEmpty(propertyAsbestosDataList))
	{
		for(NSDictionary *propertyAsbestosDataDictionary in propertyAsbestosDataList)
		{
			if(!isEmpty(propertyAsbestosDataDictionary))
			{
				PropertyAsbestosData *propertyAsbestosData = [NSEntityDescription insertNewObjectForEntityForName:kPropertyAsbestosData inManagedObjectContext:managedObjectContext];
				
				NSNumber * asbestosId = [propertyAsbestosDataDictionary valueForKey:@"asbestosId"];
				NSString * asbRiskLevelDesc = [propertyAsbestosDataDictionary valueForKey:@"asbRiskLevelDesc"];
				NSString * riskDesc = [propertyAsbestosDataDictionary valueForKey:@"riskDesc"];
				
				propertyAsbestosData.asbestosId = isEmpty(asbestosId)?nil:asbestosId;
				propertyAsbestosData.asbRiskLevelDesc = isEmpty(asbRiskLevelDesc)?nil:asbRiskLevelDesc;
				propertyAsbestosData.riskDesc = isEmpty(riskDesc)?nil:riskDesc;
				
				//Set the Inverse Relationship
				propertyAsbestosData.propertyAsbestosDataToProperty = property;
				[property addPropertyToPropertyAsbestosDataObject:propertyAsbestosData];
			}
		}
	}
}

- (void) loadPropertyPicturesData:(NSArray *)propertyPicturesList property:(Property *)property defaultPictureId:(NSNumber*) defaultPictureId managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(property && !isEmpty(propertyPicturesList))
	{
		
		for(NSDictionary *propertyPictureDictionary in propertyPicturesList)
		{
			if(!isEmpty(propertyPictureDictionary))
			{
				PropertyPicture *   propertyPicture = [NSEntityDescription insertNewObjectForEntityForName:kPropertyPicture inManagedObjectContext:managedObjectContext];
				
				NSNumber * propertyPictureId = [propertyPictureDictionary valueForKey:@"propertyPictureId"];
				NSNumber * appointmentId = [propertyPictureDictionary valueForKey:@"appointmentId"];
				NSNumber * createdBy = [propertyPictureDictionary valueForKey:@"createdBy"];
				NSDate *createdOn = [UtilityClass convertServerDateToNSDate:[propertyPictureDictionary valueForKey:@"createdOn"]];
				NSString * imagePath = [propertyPictureDictionary valueForKey:@"imagePath"];
				
				if (!isEmpty(defaultPictureId) && [defaultPictureId integerValue]==[propertyPictureId integerValue]) {
					
					property.defaultPicture=propertyPicture;
					propertyPicture.isDefault=[NSNumber numberWithBool:YES];
				}
				else
				{
					propertyPicture.isDefault = [NSNumber numberWithBool:NO];
				}
				NSNumber * itemId = [propertyPictureDictionary valueForKey:@"itemId"];
				NSString * propertyId = [propertyPictureDictionary valueForKey:kPropertyId];
				NSString * propertyPictureName = [propertyPictureDictionary valueForKey:@"propertyPictureName"];
				NSNumber * surveyId = [propertyPictureDictionary valueForKey:@"surveyId"];
				
				propertyPicture.imagePath = isEmpty(imagePath)?nil:imagePath;
				
				propertyPicture.appointmentId = isEmpty(appointmentId)?nil:appointmentId;
				propertyPicture.createdBy = isEmpty(createdBy)?nil:createdBy;
				propertyPicture.createdOn = isEmpty(createdOn)?nil:createdOn;
				propertyPicture.itemId = isEmpty(itemId)?nil:itemId;
				propertyPicture.propertyId = isEmpty(propertyId)?nil:propertyId;
				propertyPicture.propertyPictureId = isEmpty(propertyPictureId)?nil:propertyPictureId;
				propertyPicture.propertyPictureName = isEmpty(propertyPictureName)?nil:propertyPictureName;
				propertyPicture.surveyId = isEmpty(surveyId)?nil:surveyId;
				
				//Set the Inverse Relationship
				propertyPicture.propertyPictureToProperty = property;
				[property addPropertyToPropertyPictureObject:propertyPicture];
			}
		}
	}
}

#pragma mark - Appliances Data

- (void) loadApplianceData:(NSArray *)appliancesData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext isAddedOffline:(BOOL)offline
{
	if(!isEmpty(appliancesData))
	{
		
		for (NSDictionary * applianceDictionary in appliancesData)
		{
			
			NSDate * installedDate = [UtilityClass convertServerDateToNSDate:[applianceDictionary valueForKey:kApplianceInstalledDate]];
			
			NSDate * replacementDate = [UtilityClass convertServerDateToNSDate:[applianceDictionary valueForKey:kApplianceReplacementDate]];
			
			NSDictionary *locationDictionary = [applianceDictionary valueForKey:kApplianceLocation];
			NSDictionary *manufecturerDictionary = [applianceDictionary valueForKey:kApplianceManufacturer];
			NSDictionary *modelDictionary = [applianceDictionary valueForKey:kApplianceModel];
			NSDictionary *typeDictionary = [applianceDictionary valueForKey:kApplianceType];
			NSDictionary *applianceInspectionDictionary = [applianceDictionary valueForKey:kApplianceInspection];
			NSArray* applianceDefectsArray = [applianceDictionary valueForKey:kApplianceDefects];
			
			NSArray *applianceLocations = @[locationDictionary];
			NSArray *applianceManufacturers = @[manufecturerDictionary];
			NSArray *applianceModels = @[modelDictionary];
			NSArray *applianceTypes =@[typeDictionary];
			
			NSNumber * applianceId = [applianceDictionary valueForKey:kApplianceId];
			NSString * fluType = [applianceDictionary valueForKey:kApplianceFlueType];
			NSString * gcNumber = [applianceDictionary valueForKey:kApplianceGCNumber];
			NSString * propertyId = [applianceDictionary valueForKey:kPropertyId];
			NSNumber * isInspected = [applianceDictionary valueForKey:kApplianceIsInspected];
			NSNumber * isLandlordAppliance = [applianceDictionary valueForKey:kApplianceIsLandlordAppliance];
			NSString * serialNumber = [applianceDictionary valueForKey:kApplianceSerialNumber];
			NSManagedObjectID *applianceObjectId = [applianceDictionary valueForKey:@"applianceObjectId"];
			
			CLS_LOG(@"----------------------------------------------------------------");
			CLS_LOG(@"applianceObjectId %@",applianceObjectId);
			CLS_LOG(@"----------------------------------------------------------------");
			
			CLS_LOG(@"applianceId %@",applianceId);
			CLS_LOG(@"propertyId %@",propertyId);
			CLS_LOG(@"isInspected %@",isInspected);
			CLS_LOG(@"isLandlordAppliance %@",isLandlordAppliance);
			
			Appliance *appliance = nil;//[[PSCoreDataManager sharedManager] applianceWithIdentifier:applianceId inContext:managedObjectContext];
			
			if(offline) {
				if (!isEmpty(applianceObjectId))
				{
					appliance = (Appliance *)[managedObjectContext objectWithID:applianceObjectId];
				}
			}
			else
			{
				appliance = [[PSCoreDataManager sharedManager] applianceWithIdentifier:applianceId inContext:managedObjectContext];
			}
			
			if(appliance != nil)
			{
				//appliance = (Appliance *)[managedObjectContext objectWithID:appliance.objectID];
				CLS_LOG(@"Appliance UPDATED");
			}
			else
			{
				appliance = [NSEntityDescription insertNewObjectForEntityForName:kAppliance inManagedObjectContext:managedObjectContext];
				CLS_LOG(@"New Appliance ADDED");
			}
			
			appliance.applianceID = isEmpty(applianceId)?nil:applianceId;
			appliance.fluType = isEmpty(fluType)?nil:fluType;
			appliance.gcNumber = isEmpty(gcNumber)?nil:gcNumber;
			appliance.isInspected = isEmpty(isInspected)?nil:isInspected;
			appliance.isLandlordAppliance = isEmpty(isLandlordAppliance)?nil:isLandlordAppliance;
			appliance.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
			appliance.propertyID = isEmpty(propertyId)?nil:propertyId;
			appliance.installedDate = isEmpty(installedDate)?nil:installedDate;
			appliance.replacementDate = isEmpty(replacementDate)?nil:replacementDate;
			
			[[PSDataPersistenceManager sharedManager]loadApplianceLocationData:applianceLocations appliance:appliance managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceManufacturerData:applianceManufacturers appliance:appliance managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager] loadApplianceModelData:applianceModels appliance:appliance managedObjectContext:managedObjectContext];
			[[PSDataPersistenceManager sharedManager]loadApplianceTypeData:applianceTypes appliance:appliance managedObjectContext:managedObjectContext];
			
			[[PSDataPersistenceManager sharedManager]loadApplianceDefectsData:applianceDefectsArray managedObjectContext:managedObjectContext];
			
			if ([appliance.isInspected boolValue])
			{
				CLS_LOG(@"appliance.isInspected TRUE");
				[[PSDataPersistenceManager sharedManager]loadInspectionForm:applianceInspectionDictionary forAppliance:appliance managedObjectContext:managedObjectContext];
			}
			else {
				appliance.applianceInspection = nil;
			}
			//Set the Inverse Relationship
			if (!isEmpty(property))
			{
				appliance.applianceToProperty = property;
				[property addPropertyToAppliancesObject:appliance];
			}
		}
	}
	
}

- (void) loadApplianceLocationData:(NSArray *)applianceLocationData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceLocationData))
	{
		for(NSDictionary *applianceLocationDictionary in applianceLocationData)
		{
			if(!isEmpty(applianceLocationDictionary))
			{
				//CLS_LOG(@"applianceLocationDictionary --------------%@",applianceLocationDictionary);
				
				NSString * locationName = [applianceLocationDictionary valueForKey:kApplianceLocationName];
				
				if(!isEmpty(locationName)) {
					//CLS_LOG(@"locationName --------------(%@)-----------%d",locationName,[locationName length]);
					locationName = [locationName trimString];
					//CLS_LOG(@"locationName trim --------------(%@)-----------%d",locationName,[locationName length]);
				}
				
				NSNumber * locationID = [applianceLocationDictionary valueForKey:kApplianceLocationID];
				if (!isEmpty(locationName))
				{
					ApplianceLocation *applianceLocation = [[PSCoreDataManager sharedManager] applianceLocationWithName:locationName inContext:managedObjectContext] ;
					//                    if ([[locationName lowercaseString] isEqualToString:@"kitchen"]) {
					//                        CLS_LOG(@"--------------%@-----------%@",locationName,locationID);
					//                    }
					if(applianceLocation != nil)
					{
						//applianceLocation = (ApplianceLocation *)[managedObjectContext objectWithID:applianceLocation.objectID];
						//CLS_LOG(@"UPDATE --------------%@-----------%@",locationName,locationID);
						
					}
					else
					{
						applianceLocation = [NSEntityDescription insertNewObjectForEntityForName:kApplianceLocation inManagedObjectContext:managedObjectContext];
						//CLS_LOG(@"INSERT --------------%@-----------%@",locationName,locationID);
					}
					
					applianceLocation.locationName = isEmpty(locationName)?nil:locationName;
					applianceLocation.locationID = isEmpty(locationID)?nil:locationID;
					
					if (appliance != nil)
					{
						appliance.applianceLocation = applianceLocation;
					}
				}
			}
		}
		//CLS_LOG(@"loadApplianceLocationData --------------End ");
	}
}

- (void) loadApplianceManufacturerData:(NSArray *)applianceManufacturerData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceManufacturerData))
	{
		for(NSDictionary *applianceManufacturerDictionary in applianceManufacturerData)
		{
			if(!isEmpty(applianceManufacturerDictionary))
			{
				
				//CLS_LOG(@"applianceLocationDictionary --------------%@",applianceManufacturerDictionary);
				
				NSString * manufacturerName = [applianceManufacturerDictionary valueForKey:kApplianceManufacturerName];
				
				if(!isEmpty(manufacturerName)) {
					//CLS_LOG(@"manufacturerName --------------(%@)-----------%d",manufacturerName,[manufacturerName length]);
					manufacturerName = [manufacturerName trimString];
					//CLS_LOG(@"manufacturerName trim --------------(%@)-----------%d",manufacturerName,[manufacturerName length]);
				}
				
				NSNumber * manufacturerID = [applianceManufacturerDictionary valueForKey:kApplianceManufacturerID];
				if (!isEmpty(manufacturerName))
				{
					ApplianceManufacturer *applianceManufacturer = [[PSCoreDataManager sharedManager] applianceManufacturerWithName:manufacturerName inContext:managedObjectContext];
					if(applianceManufacturer != nil)
					{
						//applianceManufacturer = (ApplianceManufacturer *)[managedObjectContext objectWithID:applianceManufacturer.objectID];
					}
					else
					{
						applianceManufacturer = [NSEntityDescription insertNewObjectForEntityForName:kApplianceManufacturer inManagedObjectContext:managedObjectContext];
					}
					
					applianceManufacturer.manufacturerName = isEmpty(manufacturerName)?nil:manufacturerName;
					applianceManufacturer.manufacturerID = isEmpty(manufacturerID)?nil:manufacturerID;
					if (appliance != Nil && applianceManufacturer!=Nil) {
						appliance.applianceManufacturer = applianceManufacturer;
					}
				}
			}
		}
	}
}

- (void) loadApplianceModelData:(NSArray *)applianceModelData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceModelData))
	{
		for(NSDictionary *applianceModelDictionary in applianceModelData)
		{
			if(!isEmpty(applianceModelDictionary))
			{
				
				
				//CLS_LOG(@"applianceLocationDictionary --------------%@",applianceModelDictionary);
				
				NSString * modelName = [applianceModelDictionary valueForKey:kApplianceModelName];
				
				if(!isEmpty(modelName)) {
					//CLS_LOG(@"modelName --------------(%@)-----------%d",modelName,[modelName length]);
					modelName = [modelName trimString];
					//CLS_LOG(@"modelName trim --------------(%@)-----------%d",modelName,[modelName length]);
				}
				
				NSNumber * modelID = [applianceModelDictionary valueForKey:kApplianceModelID];
				if (!isEmpty(modelName))
				{
					ApplianceModel *applianceModel = [[PSCoreDataManager sharedManager] applianceModelWithName:modelName inContext:managedObjectContext];
					if(applianceModel != nil)
					{
						//applianceModel = (ApplianceModel *)[managedObjectContext objectWithID:applianceModel.objectID];
					}
					else
					{
						applianceModel = [NSEntityDescription insertNewObjectForEntityForName:kApplianceModel inManagedObjectContext:managedObjectContext];
					}
					
					applianceModel.modelName = isEmpty(modelName)?nil:modelName;
					applianceModel.modelID = isEmpty(modelID)?nil:modelID;
					if (appliance != nil) {
						appliance.applianceModel = applianceModel;
					}
				}
			}
		}
	}
}

- (void) loadApplianceTypeData:(NSArray *)applianceTypeData appliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceTypeData))
	{
		for(NSDictionary *applianceTypeDictionary in applianceTypeData)
		{
			if(!isEmpty(applianceTypeDictionary))
			{
				//CLS_LOG(@"applianceTypeDictionary --------------%@",applianceTypeDictionary);
				NSString * typeName = [applianceTypeDictionary valueForKey:kApplianceTypeName];
				
				if(!isEmpty(typeName)) {
					//CLS_LOG(@"typeName --------------(%@)-----------%d",typeName,[typeName length]);
					typeName = [typeName trimString];
					//CLS_LOG(@"typeName trim --------------(%@)-----------%d",typeName,[typeName length]);
				}
				
				NSNumber * typeID = [applianceTypeDictionary valueForKey:kApplianceTypeID];
				if (!isEmpty(typeName))
				{
					ApplianceType *applianceType = [[PSCoreDataManager sharedManager] applianceTypeWithName:typeName inContext:managedObjectContext];
					if(applianceType != nil)
					{
						//applianceType = (ApplianceType *)[managedObjectContext objectWithID:applianceType.objectID];
					}
					else
					{
						applianceType = [NSEntityDescription insertNewObjectForEntityForName:kApplianceType inManagedObjectContext:managedObjectContext];
					}
					
					applianceType.typeName = isEmpty(typeName)?nil:typeName;
					applianceType.typeID = isEmpty(typeID)?nil:typeID;
					if (appliance != nil) {
						appliance.applianceType = applianceType;
					}
				}
			}
		}
	}
}

- (void) loadInspectionForm:(NSDictionary *)inspectionFormDictionary forAppliance:(Appliance *)appliance managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(inspectionFormDictionary) && !isEmpty(appliance))
	{
		NSNumber  *isInspected = [inspectionFormDictionary valueForKey:kIsInspected];
		NSString *combustionReading = [inspectionFormDictionary valueForKey:kCombustionReading];
		NSString *operatingPressure = [inspectionFormDictionary valueForKey:kOperatingPressure];
		NSString *safetyDeviceOperational = [inspectionFormDictionary valueForKey:kSafetyDeviceOperational];
		NSString *smokePellet = [inspectionFormDictionary valueForKey:kSmokePellet];
		NSString *adequateVentilation = [inspectionFormDictionary valueForKey:kAdequateVentilation];
		NSString *flueVisualCondition = [inspectionFormDictionary valueForKey:kFlueVisualCondition];
		NSString *satisfactoryTermination = [inspectionFormDictionary valueForKey:kSatisfactoryTermination];
		NSString *fluePerformanceChecks = [inspectionFormDictionary valueForKey:kFluePerformanceChecks];
		NSString *applianceServiced = [inspectionFormDictionary valueForKey:kApplianceServiced];
		NSString *applianceSafeToUse = [inspectionFormDictionary valueForKey:kApplianceSafeTouse];
		NSNumber *inspectionID = [inspectionFormDictionary valueForKey:kInspectionId];
		NSNumber *inspectedBy = [inspectionFormDictionary valueForKey:kInspectedBy];
		NSDate *inspectionDate = ([[inspectionFormDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kInspectionDate]]:[inspectionFormDictionary valueForKey:kInspectionDate];
		NSString *spillageTest = [inspectionFormDictionary valueForKey:kSpillageTest];
		NSString *operatingPressureUnit = [inspectionFormDictionary valueForKey:kOperatingPressureUnit];
		
		CLS_LOG(@"isInspected %@",isInspected);
		CLS_LOG(@"combustionReading %@",combustionReading);
		
		ApplianceInspection *applianceInspection = [[PSCoreDataManager sharedManager] applianceInspectionWithInspectionIdAndApplianceId:appliance.applianceID inspectionId:inspectionID managedObjectContext:managedObjectContext];
		if ( applianceInspection != nil) {
			CLS_LOG(@"Appliance Inspection UPDATED");
			//applianceInspection = (ApplianceInspection *)[managedObjectContext objectWithID:applianceInspection.objectID];
		}
		else {
			applianceInspection = [NSEntityDescription insertNewObjectForEntityForName:kApplianceInspection inManagedObjectContext:managedObjectContext];
			CLS_LOG(@"New Appliance Inspection ADDED");
		}
		
		
		applianceInspection.isInspected = isEmpty(isInspected)?nil:isInspected;
		applianceInspection.combustionReading = isEmpty(combustionReading)?nil:combustionReading;
		applianceInspection.operatingPressure = isEmpty(operatingPressure)?nil:operatingPressure;
		applianceInspection.safetyDeviceOperational = isEmpty(safetyDeviceOperational)?nil:safetyDeviceOperational;
		applianceInspection.smokePellet = isEmpty(smokePellet)?nil:smokePellet;
		
		applianceInspection.adequateVentilation = isEmpty(adequateVentilation)?nil:adequateVentilation;
		
		applianceInspection.flueVisualCondition = isEmpty(flueVisualCondition)?nil:flueVisualCondition;
		applianceInspection.satisfactoryTermination = isEmpty(satisfactoryTermination)?nil:satisfactoryTermination;
		applianceInspection.fluePerformanceChecks = isEmpty(fluePerformanceChecks)?nil:fluePerformanceChecks;
		applianceInspection.applianceServiced = isEmpty(applianceServiced)?nil:applianceServiced;
		applianceInspection.applianceSafeToUse = isEmpty(applianceSafeToUse)?nil:applianceSafeToUse;
		applianceInspection.inspectionID = isEmpty(inspectionID)?nil:inspectionID;
		applianceInspection.inspectedBy = isEmpty(inspectedBy)?nil:inspectedBy;
		applianceInspection.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
		applianceInspection.spillageTest = isEmpty(spillageTest)?nil:spillageTest;
		applianceInspection.operatingPressureUnit = isEmpty(operatingPressureUnit)?nil:operatingPressureUnit;
		
		applianceInspection.appliance = appliance;
		appliance.applianceInspection = applianceInspection;
	}
}

- (void) loadPipeworkForm:(NSDictionary *)pipeworkDictionary forProperty:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(pipeworkDictionary) && !isEmpty(property))
	{
		NSString * emergencyControl = [pipeworkDictionary valueForKey:kEmergencyControl];
		NSString * equipotentialBonding = [pipeworkDictionary valueForKey:kEquipotentialBonding];
		NSString * gasTightnessTest = [pipeworkDictionary valueForKey:kGasTightnessTest];
		NSString * visualInspection = [pipeworkDictionary valueForKey:KVisualInspection];
		NSNumber * isInspected = [pipeworkDictionary valueForKey:kIsInspected];
		NSDate * inspectionDate = [pipeworkDictionary valueForKey:kInspectionDate];
		NSNumber * inspectionID = [pipeworkDictionary valueForKey:kInspectionId];
		
		InstallationPipework *installationPipework = property.installationPipework;
		if ( installationPipework != nil)
		{
			installationPipework = (InstallationPipework *)[managedObjectContext objectWithID:installationPipework.objectID];
		}
		else
		{
			installationPipework = [NSEntityDescription insertNewObjectForEntityForName:kInstallationPipework inManagedObjectContext:managedObjectContext];
		}
		
		installationPipework.emergencyControl = isEmpty(emergencyControl)?nil:emergencyControl;
		installationPipework.equipotentialBonding = isEmpty(equipotentialBonding)?nil:equipotentialBonding;
		installationPipework.gasTightnessTest = isEmpty(gasTightnessTest)?nil:gasTightnessTest;
		installationPipework.visualInspection = isEmpty(visualInspection)?nil:visualInspection;
		installationPipework.isInspected = isEmpty(isInspected)?nil:isInspected;
		installationPipework.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
		installationPipework.inspectionID = isEmpty(inspectionID)?nil:inspectionID;
		
		//  installationPipework.property = property;
		property.installationPipework = installationPipework;
	}
}

#pragma mark - Detectors Data

- (void) loadDetectorData:(NSArray *)detectorsData property:(Property *)property managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(detectorsData))
	{
		for (NSDictionary * detectorDictionary in detectorsData)
		{
            NSManagedObjectID *objectIdForDetector = [detectorDictionary valueForKey:@"detectorObjectId"];
            
			NSNumber * detectorCount = [detectorDictionary valueForKey:kDetectorCount];
			
			
			
			NSNumber * isInspected = [detectorDictionary valueForKey:kApplianceIsInspected];
			NSDictionary* inspectionForm = [detectorDictionary valueForKey:@"detectorInspection"];
			NSArray* detectorDefects = [detectorDictionary valueForKey:@"detectorDefects"];
            
            
            NSNumber *detectorId = [detectorDictionary valueForKey:kDetectorId];
            NSString * detectorType = [detectorDictionary valueForKey:kDetectorTypeDesc];
            NSNumber * detectorTypeId = [detectorDictionary valueForKey:kDetectorTypeId];
            NSNumber *installedBy = [detectorDictionary valueForKey:kDetectorInstalledBy];
            NSNumber *isLandLordDevice = [detectorDictionary valueForKey:kIsLandlordsDetector];
            NSString *location = [detectorDictionary valueForKey:kLocation];
            NSString *manufacture = [detectorDictionary valueForKey:kManufacturer];
            NSNumber *powerTypeId = [detectorDictionary valueForKey:kPowerTypeId];
            NSString * propertyId = [detectorDictionary valueForKey:kPropertyId];
            NSString *serialNumber = [detectorDictionary valueForKey:kSerialNumber];
            NSDate *installedDate = [UtilityClass convertServerDateToNSDate:[detectorDictionary valueForKey:kInstalledDate]];
            
			
			Detector *detector;
            if(!isEmpty(objectIdForDetector)){
                detector = (Detector *)[managedObjectContext objectWithID:objectIdForDetector];
            }
            else{
                detector = [NSEntityDescription insertNewObjectForEntityForName:kDetector inManagedObjectContext:managedObjectContext];
            }
			[[PSDataPersistenceManager sharedManager] loadDetectorDefectsData:detectorDefects managedObjectContext:managedObjectContext];
			
            detector.detectorId = isEmpty(detectorId)?nil:detectorId;
            detector.detectorType = isEmpty(detectorType)?nil:detectorType;
            detector.detectorTypeId = isEmpty(detectorTypeId)?nil:detectorTypeId;
            detector.installedBy = isEmpty(installedBy)?nil:installedBy;
            detector.isLandlordsDetector = isEmpty(isLandLordDevice)?nil:isLandLordDevice;
            detector.location = isEmpty(location)?nil:location;
            detector.manufacturer = isEmpty(manufacture)?nil:manufacture;
            detector.powerTypeId = isEmpty(powerTypeId)?nil:powerTypeId;
            detector.serialNumber = isEmpty(serialNumber)?nil:serialNumber;
            detector.installedDate = isEmpty(installedDate)?nil:installedDate;
            detector.propertyId = isEmpty(propertyId)?nil:propertyId;
            
            
            
			detector.detectorTypeId = isEmpty(detectorTypeId)?nil:detectorTypeId;
			detector.detectorCount = isEmpty(detectorCount)?nil:detectorCount;
			detector.detectorType = isEmpty(detectorType)?nil:detectorType;
			detector.isInspected = isEmpty(isInspected)?nil:isInspected;
			
			if ([detector.isInspected boolValue])
			{
				[[PSDataPersistenceManager sharedManager]loadDetectorInspectionForm:inspectionForm forDetector:detector managedObjectContext:managedObjectContext];
			}
            //Set the Inverse Relationship
			if (!isEmpty(property))
			{
				detector.detectorToProperty = property;
				[property addPropertyToDetectorObject:detector];
			}
            if(!isEmpty(detector.powerTypeId)){
                PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
                PowerType * powerType = [coreDataMgr findRecordFrom:kPowerType
                                                              Field:kPowerTypeId
                                                          WithValue:detector.powerTypeId
                                                            context:managedObjectContext];
                if(!isEmpty(powerType)){
                    detector.detectorToPowerType = powerType;
                }
            }
		}
	}
	/**/
}

- (void) loadDetectorInspectionForm:(NSDictionary *)inspectionFormDictionary forDetector:(Detector *)detector managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(inspectionFormDictionary) && !isEmpty(detector))
	{
		NSString *detectorTested = [inspectionFormDictionary valueForKey:kDetectorTest];
		NSNumber *inspectionID = [inspectionFormDictionary valueForKey:kInspectionId];
		NSNumber *inspectedBy = [inspectionFormDictionary valueForKey:kInspectedBy];
		NSDate *inspectionDate = ([[inspectionFormDictionary valueForKey:kInspectionDate] isKindOfClass:[NSString class]])?[UtilityClass convertServerDateToNSDate:[inspectionFormDictionary valueForKey:kInspectionDate]]:[inspectionFormDictionary valueForKey:kInspectionDate];
		
		
		
		DetectorInspection *detectorInspection = [[PSCoreDataManager sharedManager]detectorInspectionWithInspectionId:inspectionID detectorId:detector.detectorTypeId propertyId:detector.detectorToProperty.propertyId managedObjectContext:managedObjectContext];
		if (detectorInspection != nil)
		{
			detectorInspection = (DetectorInspection *)[managedObjectContext objectWithID:detectorInspection.objectID];
		}
		else
		{
			detectorInspection = [NSEntityDescription insertNewObjectForEntityForName:kDetectorInspection inManagedObjectContext:managedObjectContext];
			
		}
		detectorInspection.detectorsTested = isEmpty(detectorTested)?nil:detectorTested;
		detectorInspection.inspectionID = isEmpty(inspectionID)?nil:inspectionID;
		detectorInspection.inspectionDate = isEmpty(inspectionDate)?nil:inspectionDate;
		detectorInspection.inspectedBy = isEmpty(inspectedBy)?nil:inspectedBy;
		
		detectorInspection.detector = detector;
		detector.detectorInspection = detectorInspection;
	}
}

#pragma mark Defects Data
- (void) loadApplianceDefectsData:(NSArray *)applianceDefectsList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(applianceDefectsList))
	{
		for(NSDictionary *defectDictionary in applianceDefectsList)
		{
			if(!isEmpty(defectDictionary))
			{
				NSNumber *defectId = [defectDictionary valueForKey:kDefectID];
				Defect *defect = [[PSCoreDataManager sharedManager] defectWithIdentifier:defectId context:managedObjectContext];
				
				if (defect != nil)
				{
					//defect = (Defect *)[managedObjectContext objectWithID:defect.objectID];
				}
				else
				{
					defect = [NSEntityDescription insertNewObjectForEntityForName:kDefect inManagedObjectContext:managedObjectContext];
				}
				
				NSNumber *applianceId = [defectDictionary valueForKey:kApplianceId];
				NSDate *defectDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kDefectDate]];
				NSString *defectDescription = [defectDictionary valueForKey:kDefectDescription];
				NSNumber *faultCategory  = [defectDictionary valueForKey:kFaultCategory];
				NSNumber *isActionTaken = [defectDictionary valueForKey:kIsActionTaken];
				NSNumber *isAdviceNoteIssued = [defectDictionary valueForKey:kisAdviceNoteIssued];
				NSNumber *isDefectidentified = [defectDictionary valueForKey:kIsDefectIdentified];
				NSNumber *journalId = [defectDictionary valueForKey:@"JournalId"];
				NSString *propertyId = [defectDictionary valueForKey:kPropertyId];
				NSString *remedialAction = [defectDictionary valueForKey:kRemedialAction];
				NSString *serialNumber = [defectDictionary valueForKey:kSerialNo];
				NSNumber *warningTagFixed = [defectDictionary valueForKey:kWarningTagFixed];
				
				defect.applianceID = isEmpty(applianceId)?nil:applianceId;
				defect.defectDate = isEmpty(defectDate)?nil:defectDate;
				defect.defectDescription = isEmpty(defectDescription)?nil:defectDescription;
				defect.faultCategory = isEmpty(faultCategory)?nil:faultCategory;
				defect.isActionTaken = isEmpty(isActionTaken)?nil:isActionTaken;
				defect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
				defect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
				defect.journalId = isEmpty(journalId)?nil:journalId;
				defect.propertyId = isEmpty(propertyId)?nil:propertyId;
				defect.remedialAction = isEmpty(remedialAction)?nil:remedialAction;
				defect.serialNo = isEmpty(serialNumber)?nil:serialNumber;
				defect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
				defect.defectID = isEmpty(defectId)?nil:defectId;
				defect.defectType = kDefectTypeAppliance;
				
				DefectCategory *defectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:faultCategory];
				if (!isEmpty(defectCategory))
				{
					DefectCategory *defectCategory1 = (DefectCategory *)[managedObjectContext objectWithID:defectCategory.objectID];
					if (!isEmpty(defectCategory1))
					{
						defect.defectCategory = defectCategory1;
					}
				}
				Appliance *appliance = [[PSCoreDataManager sharedManager]applianceWithIdentifier:applianceId inContext:managedObjectContext];
				if (!isEmpty(appliance))
				{
					Appliance *appliance1 = (Appliance *)[managedObjectContext objectWithID:appliance.objectID];
					if (!isEmpty(appliance1))
					{
						[appliance1 addApplianceDefectsObject:defect];
					}
				}
			}
		}
	}
}


- (void) loadDetectorDefectsData:(NSArray *)detectorDefectsList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(detectorDefectsList))
	{
		for(NSDictionary *defectDictionary in detectorDefectsList)
		{
			if(!isEmpty(defectDictionary))
			{
				NSNumber *defectId = [defectDictionary valueForKey:kDefectID];
				Defect *defect = [[PSCoreDataManager sharedManager] defectWithIdentifier:defectId context:managedObjectContext];
				
				if (defect != nil)
				{
					//defect = (Defect *)[managedObjectContext objectWithID:defect.objectID];
				}
				else
				{
					defect = [NSEntityDescription insertNewObjectForEntityForName:kDefect inManagedObjectContext:managedObjectContext];
				}
				
				NSNumber *applianceId = [defectDictionary valueForKey:kApplianceId];
				NSDate *defectDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kDefectDate]];
				NSString *defectDescription = [defectDictionary valueForKey:kDefectDescription];
				NSNumber *faultCategory  = [defectDictionary valueForKey:kFaultCategory];
				NSNumber *isActionTaken = [defectDictionary valueForKey:kIsActionTaken];
				NSNumber *isAdviceNoteIssued = [defectDictionary valueForKey:kisAdviceNoteIssued];
				NSNumber *isDefectidentified = [defectDictionary valueForKey:kIsDefectIdentified];
				NSNumber *journalId = [defectDictionary valueForKey:@"JournalId"];
				NSString *propertyId = [defectDictionary valueForKey:kPropertyId];
				NSString *remedialAction = [defectDictionary valueForKey:kRemedialAction];
				NSString *serialNumber = [defectDictionary valueForKey:kSerialNo];
				NSNumber *warningTagFixed = [defectDictionary valueForKey:kWarningTagFixed];
				NSNumber *detectorId = [defectDictionary valueForKey:kDetectorTypeId];
				
				defect.applianceID = isEmpty(applianceId)?nil:applianceId;
				defect.defectDate = isEmpty(defectDate)?nil:defectDate;
				defect.defectDescription = isEmpty(defectDescription)?nil:defectDescription;
				defect.faultCategory = isEmpty(faultCategory)?nil:faultCategory;
				defect.isActionTaken = isEmpty(isActionTaken)?nil:isActionTaken;
				defect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
				defect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
				defect.journalId = isEmpty(journalId)?nil:journalId;
				defect.propertyId = isEmpty(propertyId)?nil:propertyId;
				defect.remedialAction = isEmpty(remedialAction)?nil:remedialAction;
				defect.serialNo = isEmpty(serialNumber)?nil:serialNumber;
				defect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
				defect.defectID = isEmpty(defectId)?nil:defectId;
				defect.defectType = kDefectTypeDetector;
				DefectCategory *defectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:faultCategory];
				
				if (!isEmpty(defectCategory))
				{
					DefectCategory *defectCategory1 = (DefectCategory *)[managedObjectContext objectWithID:defectCategory.objectID];
					if (!isEmpty(defectCategory1))
					{
						defect.defectCategory = defectCategory1;
					}
				}
				defect.detectorTypeId = isEmpty(detectorId)?nil:detectorId;
				Detector *detector = [[PSCoreDataManager sharedManager] detectorWithIdentifier:detectorId forProperty:propertyId context:managedObjectContext];
				if (!isEmpty(detector))
				{
					[detector addDetectorDefectsObject:defect];
				}
				
			}
		}
	}
}

- (void) loadNewDefectData:(NSDictionary *)defectDictionary managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(defectDictionary))
	{
		NSNumber *defectId = [defectDictionary valueForKey:kDefectID];
		NSManagedObjectID *defectObjectId = [defectDictionary valueForKey:@"defectObjectId"];
		//Defect *defect = [[PSCoreDataManager sharedManager] defectWithIdentifier:defectId context:managedObjectContext];
		Defect *defect = nil;
		if (!isEmpty(defectObjectId))
		{
			defect = (Defect *)[managedObjectContext objectWithID:defectObjectId];
		}
		
		
		if (defect != nil)
		{
			//defect = (Defect *)[managedObjectContext objectWithID:defect.objectID];
		}
		else
		{
			defect = [NSEntityDescription insertNewObjectForEntityForName:kDefect inManagedObjectContext:managedObjectContext];
		}
		
		NSManagedObjectID *applianceObjectId = [defectDictionary valueForKey:@"applianceObjectId"];
		NSDate *defectDate = [UtilityClass convertServerDateToNSDate:[defectDictionary valueForKey:kDefectDate]];
		NSString *defectDescription = [defectDictionary valueForKey:kDefectDescription];
		NSNumber *faultCategory  = [defectDictionary valueForKey:kFaultCategory];
		NSNumber *isActionTaken = [defectDictionary valueForKey:kIsActionTaken];
		NSNumber *isAdviceNoteIssued = [defectDictionary valueForKey:kisAdviceNoteIssued];
		NSNumber *isDefectidentified = [defectDictionary valueForKey:kIsDefectIdentified];
		NSNumber *journalId = [defectDictionary valueForKey:@"JournalId"];
		NSString *propertyId = [defectDictionary valueForKey:kPropertyId];
		NSString *remedialAction = [defectDictionary valueForKey:kRemedialAction];
		NSString *serialNumber = [defectDictionary valueForKey:kSerialNo];
		NSNumber *warningTagFixed = [defectDictionary valueForKey:kWarningTagFixed];
		NSString *defectType = [defectDictionary valueForKey:kDefectType];
		NSNumber *detectorId = [defectDictionary valueForKey:kDetectorTypeId];
		
		
		defect.defectDate = isEmpty(defectDate)?nil:defectDate;
		defect.defectDescription = isEmpty(defectDescription)?nil:defectDescription;
		defect.faultCategory = isEmpty(faultCategory)?nil:faultCategory;
		defect.isActionTaken = isEmpty(isActionTaken)?nil:isActionTaken;
		defect.isAdviceNoteIssued = isEmpty(isAdviceNoteIssued)?nil:isAdviceNoteIssued;
		defect.isDefectIdentified = isEmpty(isDefectidentified)?nil:isDefectidentified;
		defect.journalId = isEmpty(journalId)?nil:journalId;
		defect.propertyId = isEmpty(propertyId)?nil:propertyId;
		defect.remedialAction = isEmpty(remedialAction)?nil:remedialAction;
		defect.serialNo = isEmpty(serialNumber)?nil:serialNumber;
		defect.warningTagFixed = isEmpty(warningTagFixed)?nil:warningTagFixed;
		defect.defectID = isEmpty(defectId)?nil:defectId;
		defect.defectType = defectType;
		
		DefectCategory *defectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:faultCategory context:managedObjectContext];
		
		if (!isEmpty(defectCategory))
		{
			defect.defectCategory = defectCategory;
		}
		if ([defectType isEqualToString:kDefectTypeAppliance])
		{
			Appliance *appliance = nil;
			if (!isEmpty(applianceObjectId))
			{
				appliance = (Appliance *)[managedObjectContext objectWithID:applianceObjectId];
			}
			
			
			// defect.applianceID = isEmpty(applianceId)?nil:applianceId;
			// Appliance *appliance = [[PSCoreDataManager sharedManager]applianceWithIdentifier:applianceId inContext:managedObjectContext];
			
			if (!isEmpty(appliance))
			{
				// Appliance *appliance1 = (Appliance *)[managedObjectContext objectWithID:appliance.objectID];
				// if (!isEmpty(appliance1))
				//  {
				[appliance addApplianceDefectsObject:defect];
				//  }
			}
			
		}
		else if ([defectType isEqualToString:kDefectTypeDetector])
		{
			defect.detectorTypeId = isEmpty(detectorId)?nil:detectorId;
			Detector *detector = [[PSCoreDataManager sharedManager] detectorWithIdentifier:detectorId forProperty:propertyId context:managedObjectContext];
			if (!isEmpty(detector))
			{
				[detector addDetectorDefectsObject:defect];
			}
		}
	}
}

- (void) loadDefectCategoryData:(NSArray *)defectCategoryList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(defectCategoryList))
	{
		for (NSDictionary *categoryDictionary in defectCategoryList)
		{
			NSString *categoryDescription = [categoryDictionary valueForKey:kDefectCategoryDescription];
			NSNumber *categoryId  = [categoryDictionary valueForKey:kDefectCategoryID];
			
			DefectCategory *defectCategory = [[PSCoreDataManager sharedManager]defectCategoryWithIdentifier:categoryId];
			if (defectCategory != nil)
			{
				defectCategory = (DefectCategory *)[managedObjectContext objectWithID:defectCategory.objectID];
			}
			else
			{
				defectCategory = [NSEntityDescription insertNewObjectForEntityForName:kDefectCategory inManagedObjectContext:managedObjectContext];
			}
			
			defectCategory.categoryDescription = isEmpty(categoryDescription)?nil:categoryDescription;
			defectCategory.categoryID = isEmpty(categoryId)?nil:categoryId;
		}
	}
}

#pragma mark - Fault Data
- (void) loadFaultRepairData:(NSArray *)faultRepairDataList jobData:(JobDataList *)jobData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(faultRepairDataList))
	{
		for(NSDictionary *repairDataDictionary in faultRepairDataList)
		{
			if(!isEmpty(repairDataDictionary))
			{
				
				NSString * faultRepairDescription = [repairDataDictionary valueForKey:kFaultRepairDescription];
				NSNumber * faultRepairID = [repairDataDictionary valueForKey:kFaultRepairID];
				
				FaultRepairData *faultRepair = [[PSCoreDataManager sharedManager] faultReapirWithID:faultRepairID];
				if(faultRepair != nil)
				{
					faultRepair = (FaultRepairData *)[managedObjectContext objectWithID:faultRepair.objectID];
				}
				else
				{
					faultRepair = [NSEntityDescription insertNewObjectForEntityForName:kFaultRepairData inManagedObjectContext:managedObjectContext];
				}
				faultRepair.faultRepairID = isEmpty(faultRepairID)?nil:faultRepairID;
				faultRepair.faultRepairDescription = isEmpty([faultRepairDescription trimWhitespace])?nil:[faultRepairDescription trimWhitespace];
				faultRepair.repairSectionIdentifier = [faultRepair repairSectionIdentifier];
				//faultRepair.faultRepairDataToJobDataList = jobData;
				//[jobData addFaultRepairDataListObject:faultRepair];
				
			}
		}
	}
}

- (void) loadJobPauseReasonData:(NSArray *)jobPauseReasonList managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if(!isEmpty(jobPauseReasonList))
	{
		for (int pauseReason = 0; pauseReason < [jobPauseReasonList count]; ++pauseReason) {
			{
				NSString *reason = [jobPauseReasonList objectAtIndex:pauseReason];
				JobPauseReason *jobPauseReason = [[PSCoreDataManager sharedManager] jobPauseReasonWithName:reason];
				if (jobPauseReason != nil)
				{
					jobPauseReason = (JobPauseReason *)[managedObjectContext objectWithID:jobPauseReason.objectID];
				}
				else
				{
					jobPauseReason = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseReason inManagedObjectContext:managedObjectContext];
				}
				jobPauseReason.pauseReason = reason;
			}
		}
	}
}

- (void) loadFaultRepairHistoryData:(NSArray *)faultRepairHistoryList jobData:(JobDataList *)jobData managedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	if (!isEmpty(jobData))
	{
		[jobData removeFaultRepairDataList:jobData.faultRepairDataList];
		
		if (!isEmpty(faultRepairHistoryList))
		{
			for (FaultRepairData *faultRepairData in faultRepairHistoryList)
			{
				if (!isEmpty(faultRepairData)) {
					FaultRepairListHistory *faultRepairHistory = [[PSCoreDataManager sharedManager]faultRepairHistoryWithID:faultRepairData.faultRepairID forJobData:jobData.jsNumber inContext:managedObjectContext];
					if (faultRepairHistory != nil)
					{
						faultRepairHistory = (FaultRepairListHistory *)[managedObjectContext objectWithID:faultRepairHistory.objectID];
					}
					else
					{
						faultRepairHistory = [NSEntityDescription insertNewObjectForEntityForName:kFaultRepairListHistory inManagedObjectContext:managedObjectContext];
					}
					
					faultRepairHistory.faultRepairID = faultRepairData.faultRepairID;
					faultRepairHistory.faultRepairDescription = faultRepairData.faultRepairDescription;
					FaultRepairData *existingFaultRepairData = (FaultRepairData *)[managedObjectContext objectWithID:faultRepairData.objectID];
					if (!isEmpty(jobData))
					{
						if (![jobData.faultRepairDataList containsObject:faultRepairData])
						{
							[jobData addFaultRepairDataListObject:existingFaultRepairData];
						}
						if (![jobData.faultRepairHistoryList containsObject:faultRepairHistory])
						{
							[jobData addFaultRepairHistoryListObject:faultRepairHistory];
						}
						jobData.jobDataListToAppointment.isModified = [NSNumber numberWithBool:YES];
					}
				}
			}
		}
	}
}

#pragma mark - Detectors

-(Property *) getPropertyForPropertyId:(NSString *) propId{
    PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
    __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    Property * property = [coreDataMgr findRecordFrom:kProperty
                                                Field:kPropertyId
                                            WithValue:propId
                                              context:dbContext];
    return property;
}

- (BOOL) saveDetector:(NSDictionary *)detector forProperty:(NSManagedObjectID *)propertyId
{
    BOOL isSaved = NO;
    
    if([UtilityClass isUserLoggedIn] && !isEmpty(detector))
    {
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        Property *property = (Property *)[managedObjectContext objectWithID:propertyId];
        [[PSDataPersistenceManager sharedManager] loadDetectorData:@[detector] property:property managedObjectContext:managedObjectContext];
        property.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        property.propertyToAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
        [self saveDataInDB:managedObjectContext];
        isSaved = YES;
    }
    return isSaved;
    
}

- (BOOL) saveDetector:(NSDictionary *)detector forPropertyId:(NSString *)propertyId
{
    BOOL isSaved = NO;
    
    if([UtilityClass isUserLoggedIn] && !isEmpty(detector))
    {
        // Creating managed objects
        CLS_LOG(@"Creating managed objects");
        NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        Property *property = (Property *)[managedObjectContext objectWithID:[self getPropertyForPropertyId:propertyId].objectID];
        [[PSDataPersistenceManager sharedManager] loadDetectorData:@[detector] property:property managedObjectContext:managedObjectContext];
        [self saveDataInDB:managedObjectContext];
        isSaved = YES;
    }
    return isSaved;
    
}


-(void) saveSurveyPropertyDetectors:(NSArray *) detectors{
    if(!isEmpty(detectors)){
        PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
        __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        for(NSDictionary * propertyDetector in detectors){
            [self saveDetector:propertyDetector forPropertyId:[propertyDetector objectForKey:@"propertyId"]];
        }
    }
    
}

#pragma mark - Common Data
- (void) saveCommonData:(NSDictionary *)commonDataArray
{
    CLS_LOG(@"save ApptCommonData");
    if([UtilityClass isUserLoggedIn] && !isEmpty(commonDataArray))
    {
        __block NSManagedObjectContext * dbContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        NSArray * detectorTypesList = [commonDataArray valueForKey:@"detectorTypes"];
        [self loadDetectorTypes:detectorTypesList dbContext:dbContext];
        NSArray * powerTypesList = [commonDataArray valueForKey:@"powerTypes"];
        [self loadPowerTypes:powerTypesList dbContext:dbContext];
        
        
    }
}


-(void) loadDetectorTypes:(NSArray *)detectorTypeList dbContext:(NSManagedObjectContext *)dbContext
{
    if(isEmpty(detectorTypeList) == NO)
    {
        int counter = 0;
        PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
        for(NSDictionary * detectorType in detectorTypeList)
        {
            NSNumber * detectorTypeId = [detectorType valueForKey:kDetectorTypeId];
            DetectorType * detectorTypeData = [coreDataMgr findRecordFrom:kDetectorType
                                                                    Field:kDetectorTypeId
                                                                WithValue:detectorTypeId
                                                                  context:dbContext];
            if(detectorTypeData == NULL)
            {
                detectorTypeData = [NSEntityDescription insertNewObjectForEntityForName:kDetectorType
                                                                 inManagedObjectContext:dbContext];
                detectorTypeData.detectorTypeId = detectorTypeId;
                detectorTypeData.detectorType = [detectorType valueForKey:kDetectorsType];
                
                counter ++;
                if(counter % 5 == 0)
                {
                    CLS_LOG(@"Saving to PSC, %u", counter);
                    [self saveDataInDB:dbContext];
                    counter = 0;
                }
            }
        }
        if(counter > 0)
        {
            [self saveDataInDB:dbContext];
        }
    }
}

-(void) loadPowerTypes:(NSArray *)powerTypeList dbContext:(NSManagedObjectContext *)dbContext
{
    if(isEmpty(powerTypeList) == NO)
    {
        PSCoreDataManager * coreDataMgr = [PSCoreDataManager sharedManager];
        for(NSDictionary * powerType in powerTypeList)
        {
            NSNumber * powerTypeId = [powerType valueForKey:kPowerTypeId];
            NSString * powerTypeDesc =[powerType valueForKey:kPowerTypeDesc];
            
            PowerType * powerTypeData = [coreDataMgr findRecordFrom:kPowerType
                                                              Field:kPowerTypeId
                                                          WithValue:powerTypeId
                                                            context:dbContext];
            if(powerTypeData != nil)
            {
                powerTypeData = (PowerType *)[dbContext objectWithID:powerTypeData.objectID];
            }else{
                
                powerTypeData = [NSEntityDescription insertNewObjectForEntityForName:kPowerType
                                                              inManagedObjectContext:dbContext];
            }
            
            powerTypeData.powerTypeId = powerTypeId;
            powerTypeData.powerType = powerTypeDesc;
            [self saveDataInDB:dbContext];
        }
    }
}

@end
