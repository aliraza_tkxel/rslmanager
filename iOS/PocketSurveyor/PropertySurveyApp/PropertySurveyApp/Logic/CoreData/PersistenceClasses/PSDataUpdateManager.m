//
//  PSDataUpdateManager.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 06/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSDataUpdateManager.h"
#import "PSEventsManager.h"
#import "PropertyPicture+JSON.h"

@implementation PSDataUpdateManager

#pragma mark -
#pragma mark ARC Singleton Implementation
static PSDataUpdateManager *sharedManagerObject = nil;
+ (PSDataUpdateManager *) sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManagerObject = [[PSDataUpdateManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedManagerObject;
}

+(id)allocWithZone:(NSZone *)zone {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManagerObject = [super allocWithZone:zone];
    });
    return sharedManagerObject;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

#pragma mark - Remove Methods

- (void) deleteAppointment:(Appointment *)appointment
{
    if(appointment != nil)
    {
        if ([appointment.addToCalendar boolValue])
        {
            [[PSAppointmentsManager sharedManager]deleteCalendarEventForAppointment:appointment];
        }
        [self deleteAppInfoData:appointment.appointmentToAppInfoData];
        [self deleteCP12Info:appointment.appointmentToCP12Info];
        [self deleteJournal:appointment.appointmentToJournal];
        [self deleteProperty:appointment.appointmentToProperty];
        [self deleteSurvey:appointment.appointmentToSurvey];
        
        for(Customer *customer in appointment.appointmentToCustomer)
        {
            [self deleteCustomer:customer];
        }
        
        for(JobDataList *jobData in appointment.appointmentToJobDataList)
        {
            [self deleteJobDataList:jobData];
        }
        
        [self deletePlannedTradeComponent:appointment.appointmentToPlannedComponent];
        
        [appointment removeAppointmentToCustomer:appointment.appointmentToCustomer];
        
        [appointment removeAppointmentToJobDataList:appointment.appointmentToJobDataList];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[appointment objectID]];
        //[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentsDataRemoveSuccessNotification object:nil];
    }
}

- (void) deleteSurvey:(Survey *)appointmentSurvey
{
    if (!isEmpty(appointmentSurvey))
    {
        for (SurveyData *surveyData in appointmentSurvey.surveyToSurveyData)
        {
            [self deleteSurveyData:surveyData];
        }
        [appointmentSurvey removeSurveyToSurveyData:appointmentSurvey.surveyToSurveyData];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:appointmentSurvey.objectID];
    }
}

- (void) deleteSurveyData:(SurveyData *)surveyData
{
    if (surveyData != nil)
    {
        for (FormData *formData in surveyData.surveyDataToFormData)
        {
            [self deleteFormData:formData];
        }
        [surveyData removeSurveyDataToFormData:surveyData.surveyDataToFormData];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:surveyData.objectID];
    }
}

-(void) deleteSurveyDataForAppointment:(Appointment *) appointment andHeatingTypeId:(NSString *)heatingTypeId{
    STARTEXCEPTION
    __block NSManagedObjectID* appointmentId = appointment.objectID;
    // Creating managed objects
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    [managedObjectContext performBlock:^{
        Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
        NSPredicate*pred = [NSPredicate predicateWithFormat:@"SELF.%@ = %@",@"heatingTypeId",heatingTypeId];
        
        NSSet *filteredSet = [appointment.appointmentToSurvey.surveyToSurveyData filteredSetUsingPredicate:pred];
        if(!isEmpty(filteredSet)){
            if([filteredSet count]>0){
                SurveyData *data = [[filteredSet allObjects] objectAtIndex:0];
                [self deleteSurveyData:data];
            }
        }
        
        [managedObjectContext performBlock:^{
            // Save the context.
            NSError *error = nil;
            CLS_LOG(@"Saving to PSC");
            if (![managedObjectContext save:&error]) {
                CLS_LOG(@"Error in Saving MOC: %@",[error description]);
            }
        }]; // mainobject:nil];
    }]; // parent
    ENDEXCEPTION
}


- (void) deleteFormData:(FormData *)formData
{
    if (formData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:formData.objectID];
    }
}
- (void) deleteCustomer:(Customer *)customer
{
    if(customer != nil)
    {
        [self deleteAddress:customer.customerToAddress];
        
        for (CustomerVulnerabilityData * vulunarityData in customer.customerToCustomerVulunarityData) {
            [self deleteCustomerVulnerabilityData:vulunarityData];
        }
        [customer removeCustomerToCustomerRiskData:customer.customerToCustomerVulunarityData];
        //[self deleteCustomerVulnerabilityData:customer.customerToCustomerVulunarityData];
        for(CustomerRiskData *customerRiskData in customer.customerToCustomerRiskData)
        {
            [self deleteCustomerRiskData:customerRiskData];
        }
        [customer removeCustomerToCustomerVulunarityData:customer.customerToCustomerVulunarityData];
        [customer removeCustomerToCustomerRiskData:customer.customerToCustomerRiskData];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[customer objectID]];
    }
}

- (void) deleteAddress:(Address *)address
{
    if(address != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[address objectID]];
    }
}

- (void) deleteAppInfoData:(AppInfoData *)appInfoData
{
    if(appInfoData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[appInfoData objectID]];
    }
}

- (void) deleteCP12Info:(CP12Info *)cp12Info
{
    if(cp12Info != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[cp12Info objectID]];
    }
}

- (void) deleteCustomerVulnerabilityData:(CustomerVulnerabilityData *)customerVulnerabilityData
{
    if(customerVulnerabilityData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[customerVulnerabilityData objectID]];
    }
}

- (void) deleteCustomerRiskData:(CustomerRiskData *)customerRiskData
{
    if(customerRiskData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[customerRiskData objectID]];
    }
}

- (void) deleteFaultRepairData:(FaultRepairData *)faultRepairData
{
    if(faultRepairData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[faultRepairData objectID]];
    }
}

- (void) deleteFaultRepairHistoryData:(FaultRepairListHistory *)faultRepairHistoryData
{
    if(faultRepairHistoryData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[faultRepairHistoryData objectID]];
    }
}

- (void) deleteJobPauseData:(JobPauseData *)jobPauseData
{
    if(jobPauseData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[jobPauseData objectID]];
    }
}


- (void) deletePlannedTradeComponent:(PlannedTradeComponent *)plannedTradeComponent
{
    if(plannedTradeComponent != nil)
    {
        //        for (JobPauseData *jobPauseData in jobDataList.jobPauseData)
        //        {
        //            [self deleteJobPauseData:jobPauseData];
        //        }
        //
        [plannedTradeComponent removeJobPauseData:plannedTradeComponent.jobPauseData];
        
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[plannedTradeComponent objectID]];
    }
}

- (void) deleteJobDataList:(JobDataList *)jobDataList
{
    if(jobDataList != nil)
    {
        for(FaultRepairData *faultRepairData in jobDataList.faultRepairDataList)
        {
            [self deleteFaultRepairData:faultRepairData];
        }
        for (FaultRepairListHistory *faultRepairHistory in jobDataList.faultRepairHistoryList)
        {
            [self deleteFaultRepairHistoryData:faultRepairHistory];
        }
        for (JobPauseData *jobPauseData in jobDataList.jobPauseData)
        {
            [self deleteJobPauseData:jobPauseData];
        }
        for (RepairPictures *repairImage in jobDataList.jobDataListToRepairImages)
        {
            [self deleteRepairImage:repairImage];
        }
        
        [jobDataList removeJobDataListToRepairImages:jobDataList.jobDataListToRepairImages];
        [jobDataList removeJobPauseData:jobDataList.jobPauseData];
        [jobDataList removeFaultRepairHistoryList:jobDataList.faultRepairHistoryList];
        [jobDataList removeFaultRepairDataList:jobDataList.faultRepairDataList];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[jobDataList objectID]];
    }
}

- (void) deleteRepairImage:(RepairPictures *)repairPicture
{
    if(repairPicture != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[repairPicture objectID]];
    }
}


- (void) deleteJournal:(Journal *)journal
{
    if(journal != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[journal objectID]];
    }
}

- (void) deleteProperty:(Property *)property
{
    if(property != nil)
    {
        for(Accomodation *accommodation in property.propertyToAccomodation)
        {
            [self deleteAccommodation:accommodation];
        }
        for(PropertyAsbestosData *propertyAsbestosData in property.propertyToPropertyAsbestosData)
        {
            [self deletePropertyAsbestosData:propertyAsbestosData];
        }
        for(PropertyPicture *propertyPicture in property.propertyToPropertyPicture)
        {
            [self deletePropertyPicture:propertyPicture];
        }
        for (Appliance *appliance in property.propertyToAppliances)
        {
            [self deleteAppliance:appliance];
        }
        for (Detector *detector in property.propertyToDetector)
        {
            [self deleteDetector:detector];
        }
        
        [self deleteInstallationPipework:property.installationPipework];
        [self deletePicture:property.defaultPicture];
        [property removePropertyToDetector:property.propertyToDetector];
        [property removePropertyToAppliances:property.propertyToAppliances];
        [property removePropertyToAccomodation:property.propertyToAccomodation];
        [property removePropertyToPropertyAsbestosData:property.propertyToPropertyAsbestosData];
        [property removePropertyToPropertyPicture:property.propertyToPropertyPicture];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[property objectID]];
    }
}

- (void) deleteInstallationPipework:(InstallationPipework *)installationPipework
{
    if(installationPipework != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[installationPipework objectID]];
    }
}

- (void) deletePicture:(PropertyPicture *)picture
{
    if(picture != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[picture objectID]];
    }
}

- (void) deleteAccommodation:(Accomodation *)accommodation
{
    if(accommodation != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[accommodation objectID]];
    }
}

- (void) deletePropertyAsbestosData:(PropertyAsbestosData *)propertyAsbestosData
{
    if(propertyAsbestosData != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[propertyAsbestosData objectID]];
    }
}

- (void) deletePropertyPicture:(PropertyPicture *)propertyPicture
{
    if(propertyPicture != nil)
    {
        if (propertyPicture.propertyPictureToProperty.defaultPicture.objectID==propertyPicture.objectID) {
            
            propertyPicture.propertyPictureToProperty.defaultPicture=nil;
            
        }
        
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[propertyPicture objectID]];
    }
}

- (void) markDeletePropertyPicture:(PropertyPicture *)propertyPicture
{
    if(propertyPicture != nil)
    {
        if (propertyPicture.propertyPictureToProperty.defaultPicture.objectID==propertyPicture.objectID) {
            
            propertyPicture.propertyPictureToProperty.defaultPicture=nil;
            
        }
        propertyPicture.deletedPicture = [NSNumber numberWithBool:YES];
        propertyPicture.propertyPictureToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
        NSError * error = nil;
        [[PSDataUpdateManager sharedManager] persistPropertyPicture:propertyPicture];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[propertyPicture objectID]];
    }
}

- (void) deleteDefectPicture:(DefectPicture *)defectPicture
{
    if(defectPicture != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[defectPicture objectID]];
    }
}


- (void) deleteAppliance:(Appliance *)appliance
{
    if(appliance != nil)
    {
        for (Defect *defect in appliance.applianceDefects)
        {
            [self deleteDefect:defect];
        }
        //   [self deleteApplianceLocation:appliance.applianceLocation];
        //  [self deleteApplianceManufacturer:appliance.applianceManufacturer];
        // [self deleteApplianceModel:appliance.applianceModel];
        //[self deleteApplianceType:appliance.applianceType];
        [self deleteApplianceInspection:appliance.applianceInspection];
        [appliance removeApplianceDefects:appliance.applianceDefects];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[appliance objectID]];
    }
}

- (void) deleteDetector:(Detector *)detector
{
    if(detector != nil)
    {
        for (Defect *defect in detector.detectorDefects)
        {
            [self deleteDefect:defect];
        }
        [detector removeDetectorDefects:detector.detectorDefects];
        [self deleteDetectorInspection:detector.detectorInspection];
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[detector objectID]];
    }
}

- (void) deleteApplianceInspection:(ApplianceInspection *)applianceInspection
{
    if(applianceInspection != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceInspection objectID]];
    }
}


- (void) deleteDetectorInspection:(DetectorInspection *)detectorInspection
{
    if(detectorInspection != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[detectorInspection objectID]];
    }
}
- (void) deleteDefect:(Defect *)defect
{
    if(defect != nil)
    {
        for (DefectPicture *defectPicture in defect.defectPictures)
        {
            [self deleteDefectPicture:defectPicture];
        }
        [self deleteDefectCategory:defect.defectCategory];
        
        [defect removeDefectPictures:defect.defectPictures];
        
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[defect objectID]];
    }
}

- (void) deleteDefectCategory:(DefectCategory *)defectCategory
{
    if(defectCategory != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[defectCategory objectID]];
    }
}

- (void) deleteApplianceLocation:(ApplianceLocation *)applianceLocation
{
    if(applianceLocation != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceLocation objectID]];
    }
}

- (void) deleteApplianceManufacturer:(ApplianceManufacturer *)applianceManufacturer
{
    if(applianceManufacturer != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceManufacturer objectID]];
    }
}

- (void) deleteApplianceModel:(ApplianceModel *)applianceModel
{
    if(applianceModel != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceModel objectID]];
    }
}

- (void) deleteApplianceType:(ApplianceType *)applianceType
{
    if(applianceType != nil)
    {
        [PSDatabaseContext deleteManagedObjectFromCoreDataWithID:[applianceType objectID]];
    }
}

#pragma mark - Update Methods
-(NSMutableDictionary*) removeComponentAccountingInfoFromWholeSurvey:(NSDictionary *)dict andSurveyTitle:(NSString *) surveyTitle{
    NSString* kComponentLifeCycleForSurvey    = @"Lifecycle";
    NSString* kComponentAccountingForSurvey   = @"Component Accounting";
    NSString* kSurveyKeyFields                = @"Fields";
    NSMutableDictionary *surveyFields = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    NSMutableDictionary *fields = [[[[[[[[[[[surveyFields objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle] objectForKey:@"Fields"];
    
    NSMutableArray *order = [[[[[[[[[[[surveyFields objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle] objectForKey:@"Order"];
    
    NSPredicate *orderFilterPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@ OR SELF contains[c] %@",kComponentAccountingForSurvey,kComponentLifeCycleForSurvey];
    NSArray *filteredOrder = [order filteredArrayUsingPredicate:orderFilterPredicate];
    if(!isEmpty(filteredOrder)){
        [order removeObject:kComponentLifeCycleForSurvey];
        [order removeObject:kComponentAccountingForSurvey];
        [fields removeObjectForKey:kComponentLifeCycleForSurvey];
        [fields removeObjectForKey:kComponentAccountingForSurvey];
        
        [[[[[[[[[[[surveyFields objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle] setObject:order forKey:@"Order"];
        
        [[[[[[[[[[[surveyFields objectForKey:kSurveyKeyFields] objectForKey:@"Internals"] objectForKey:kSurveyKeyFields] objectForKey:@"Services"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating & Water Supply"] objectForKey:kSurveyKeyFields] objectForKey:@"Heating"] objectForKey:kSurveyKeyFields] objectForKey:surveyTitle] setObject:fields forKey:kSurveyKeyFields];
    }
    return surveyFields;
}

- (void) updateAppointmentSurvey:(NSDictionary *)surveyData forAppointment:(Appointment *)appointment
                 withSurveyTitle: (NSString *)surveyTitle
{
    STARTEXCEPTION
    if(surveyData != nil && appointment != nil)
    {
        __block NSManagedObjectID* appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
            NSMutableDictionary* surveyDataMutable = [self removeComponentAccountingInfoFromWholeSurvey:surveyData andSurveyTitle:surveyTitle];
            NSString *jsonStr = [(NSObject *)surveyDataMutable JSONRepresentation];
            NSLog(@"Survey Data Updated: %@",[surveyDataMutable JSONFragment]);
            
            if(existingAppointment != nil && !isEmpty(jsonStr))
            {
                existingAppointment.appointmentToSurvey.surveyJSON = jsonStr;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                CLS_LOG(@"Saving to PSC");
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                }
            }]; // mainobject:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) updateAppointmentSurvey:(NSDictionary *)surveyData forAppointment:(Appointment *)appointment
{
	STARTEXCEPTION
	if(surveyData != nil && appointment != nil)
	{
		__block NSManagedObjectID* appointmentId = appointment.objectID;
		// Creating managed objects
		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
		
		[managedObjectContext performBlock:^{
			Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
            NSString *jsonStr = [(NSObject *)surveyData JSONRepresentation];
            NSLog(@"Survey Data Updated: %@",[surveyData JSONFragment]);
            
			if(existingAppointment != nil && !isEmpty(jsonStr))
			{
				existingAppointment.appointmentToSurvey.surveyJSON = jsonStr;
			}

			[managedObjectContext performBlock:^{
				// Save the context.
				NSError *error = nil;
				CLS_LOG(@"Saving to PSC");
				if (![managedObjectContext save:&error]) {
					CLS_LOG(@"Error in Saving MOC: %@",[error description]);
				}
			}]; // mainobject:nil];
		}]; // parent
	}
	ENDEXCEPTION
}

- (void) updateAppointmentWithNoEntry:(NSDictionary *)noEntryDictionary
{
    STARTEXCEPTION
    if(noEntryDictionary != nil)
    {
        __block NSManagedObjectID* appointmentId = [noEntryDictionary valueForKey:@"objectId"];
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
            
            if(existingAppointment != nil)
            {
                AppInfoData *appInfo = existingAppointment.appointmentToAppInfoData;
                if (!isEmpty(appInfo))
                {
                    NSNumber *isCardLeft = [noEntryDictionary valueForKey:@"isCardLeft"];
                    NSInteger totalNoEntries = [appInfo.totalNoEntries integerValue];
                    ++totalNoEntries;
                    appInfo.totalNoEntries = [NSNumber numberWithInt:totalNoEntries];
                    appInfo.isCardLeft = isEmpty(isCardLeft)?nil:isCardLeft;
                    appInfo.appInfoDataToAppointment.appointmentStatus = kAppointmentStatusNoEntry;
                    //appInfo.appInfoDataToAppointment.appointmentEndTime = [NSDate date];
                    [PSDatabaseContext turnObjectIntoFault:appInfo.appInfoDataToAppointment.objectID];
                    existingAppointment.isModified=[NSNumber numberWithBool:YES];
                    existingAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateAppointmentWithNoEntryFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error])
							{
								CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateAppointmentWithNoEntrySuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}


- (void) updateFaultAppointmentWithNoEntry:(NSDictionary *)noEntryDictionary
{
    STARTEXCEPTION
    if(noEntryDictionary != nil)
    {
        __block NSManagedObjectID* appointmentId = [noEntryDictionary valueForKey:@"objectId"];
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
            
            if(existingAppointment != nil)
            {
                NSString *noEntryNotes = [noEntryDictionary valueForKey:kNoEntryNotes];
                existingAppointment.noEntryNotes = isEmpty(noEntryNotes)?nil:noEntryNotes;
                //   existingAppointment.appointmentEndTime = [NSDate date];
                existingAppointment.appointmentStatus = kAppointmentStatusNoEntry;
                
                if (existingAppointment.appointmentToPlannedComponent) {
                    
                    existingAppointment.appointmentToPlannedComponent.jobStatus=kJobStatusComplete;
                    
                }else
                {
                    NSArray *jobs = [existingAppointment.appointmentToJobDataList allObjects];
                    [self updateFaultJobWithNoEntry:jobs inManagedObjectContext:managedObjectContext];
                }
                
                existingAppointment.isModified=[NSNumber numberWithBool:YES];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateFaultAppointmentWithNoEntryFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error])
							{
								CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateFaultAppointmentWithNoEntrySuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) updateFaultJobWithNoEntry:(NSArray *)JobDataListArray inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    if (!isEmpty(JobDataListArray))
    {
        for (JobDataList *jobData in JobDataListArray)
        {
            JobDataList *existingJobData = (JobDataList *)[managedObjectContext objectWithID:jobData.objectID];
            if (!isEmpty(existingJobData))
            {
                existingJobData.jobStatus = kJobStatusNoEntry;
                existingJobData.completionDate = [NSDate date];
            }
        }
    }
}

- (void) updateAccomodations:(NSDictionary *)accomodationsInfo
{
    STARTEXCEPTION
    if(!isEmpty(accomodationsInfo))
    {
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            NSArray *accomodationIDs = [accomodationsInfo allKeys];
            for(NSManagedObjectID *accomodationID in accomodationIDs)
            {
                NSDictionary *accomodationValues = (NSDictionary *)[accomodationsInfo objectForKey:accomodationID];
                Accomodation *accomodation;
                if (!isEmpty(accomodationID)) {
                    accomodation = (Accomodation *)[managedObjectContext objectWithID:accomodationID];
                }
                
                if(accomodation != nil)
                {
                    accomodation.roomWidth = [accomodationValues objectForKey:kRoomWidth];
                    accomodation.roomLength = [accomodationValues objectForKey:kRoomLength];
                    accomodation.accomodationToProperty.propertyToAppointment.isModified = [NSNumber numberWithBool:YES];
                    accomodation.updatedOn = [NSDate date];
                    accomodation.updatedBy = [[SettingsClass sharedObject]loggedInUser].userId;
                }
                //[PSDatabaseContext turnObjectIntoFault:accomodation.accomodationToProperty.objectID];
                //[PSDatabaseContext turnObjectIntoFault:accomodation.accomodationToProperty.propertyToAppointment.objectID];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error])
							{
								CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAccomodationInfoUpdateSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) turnAccomodationsIntoFault:(NSDictionary *)accomodationsInfo
{
    STARTEXCEPTION
    if(!isEmpty(accomodationsInfo))
    {
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            NSArray *accomodationIDs = [accomodationsInfo allKeys];
            for(NSManagedObjectID *accomodationID in accomodationIDs)
            {
                // NSDictionary *accomodationValues = (NSDictionary *)[accomodationsInfo objectForKey:accomodationID];
                Accomodation *accomodation;
                if (!isEmpty(accomodationID)) {
                    accomodation = (Accomodation *)[managedObjectContext objectWithID:accomodationID];
                }
                
                [PSDatabaseContext turnObjectIntoFault:accomodation.objectID];
                [PSDatabaseContext turnObjectIntoFault:accomodation.accomodationToProperty.propertyToAppointment.objectID];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
								CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAccomodationInfoUpdateSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
    
}
- (void) updateTenant:(NSDictionary *)tenantInfo
{
    STARTEXCEPTION
    if(!isEmpty(tenantInfo))
    {
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            NSArray *tenantIDs = [tenantInfo allKeys];
            for(NSManagedObjectID *tenantID in tenantIDs)
            {
                NSDictionary *tenantDetails = (NSDictionary *)[tenantInfo objectForKey:tenantID];
                Customer *customer = nil;
                if (!isEmpty(tenantID)) {
                    customer = (Customer *)[managedObjectContext objectWithID:tenantID];
                }
                
                if(customer != nil)
                {
                    customer.customerToAppointment.isModified = [NSNumber numberWithBool:YES];
                    NSString *email = [tenantDetails objectForKey:kCustomerEmail];
                    NSString *mobileNumber = [tenantDetails objectForKey:kCustomerMobile];
                    NSString *telephoneNumber = [tenantDetails objectForKey:kCustomerTelephone];
                    customer.email = isEmpty(email)?nil:email;
                    customer.telephone = isEmpty(telephoneNumber)?nil:telephoneNumber;
                    customer.mobile = isEmpty(mobileNumber)?nil:mobileNumber;
                }
                [PSDatabaseContext turnObjectIntoFault:customer.customerToAppointment.objectID];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kTenantInfoUpdateSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) startAppointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        // Creating managed objects
        __block NSManagedObjectID *appointmentId = appointment.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            if (!isEmpty(appointmentId))
            {
                Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:appointmentId];
                if(existingAppointment != nil)
                {
                    existingAppointment.appointmentStatus = kAppointmentStatusInProgress;
                    //    existingAppointment.appointmentStartTime = [NSDate date];
                    existingAppointment.isModified = [NSNumber numberWithBool:YES];
                    
                }
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentStartNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

/*!
 @discussion
 Update Repair Picture.
 */
- (void) updateRepairPicture:(RepairPictures*)repairPicture forDictionary:(NSDictionary *) repairPictureDetails
{
    STARTEXCEPTION
    if(repairPicture != nil)
    {
        
        __block NSManagedObjectID* repairPictureId = repairPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            RepairPictures *existingRepairPicture = (RepairPictures *)[managedObjectContext objectWithID:repairPictureId];
            
            if(existingRepairPicture != nil)
            {
                
                
                if ([repairPictureDetails objectForKey:kRepairImagePath])
                {
                    existingRepairPicture.imagePath = [repairPictureDetails objectForKey:kRepairImagePath];
                }
                
                if ([repairPictureDetails objectForKey:kFaultRepairImageId]) {
                    existingRepairPicture.imageId = [repairPictureDetails objectForKey:kFaultRepairImageId];
                }
                
                if ([repairPictureDetails objectForKey:kRepairImageSyncStatus]) {
                    existingRepairPicture.syncStatus = [repairPictureDetails objectForKey:kRepairImageSyncStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kRepairPictureSaveFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
        }]; // parent
    }
    ENDEXCEPTION
}

/*!
 @discussion
 Update property picture
 */
- (void) updatePropertyPicture:(PropertyPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails
{
    STARTEXCEPTION
    if(propertyPicture != nil)
    {
//			@try
//			{
        __block NSManagedObjectID* propertyPictureId = propertyPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
//        [managedObjectContext performBlock:^{
            
            PropertyPicture *existingPropertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if(existingPropertyPicture != nil)
            {
                
                if ([[propertyPictureDetails objectForKey:kPropertyPictureIsDefault] isEqualToString:@"true"]) {
                    
                    existingPropertyPicture.propertyPictureToProperty.defaultPicture=existingPropertyPicture;
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturePath])
                {
                    existingPropertyPicture.imagePath = [propertyPictureDetails objectForKey:kPropertyPicturePath];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPictureId]) {
                    existingPropertyPicture.propertyPictureId = [propertyPictureDetails objectForKey:kPropertyPictureId];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus]) {
                    existingPropertyPicture.synchStatus = [propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
                return;
            }
					
						// Save the context.
						NSError *error = nil;
						CLS_LOG(@"Saving to PSC");
						if (![managedObjectContext save:&error]) {
								CLS_LOG(@"Error in Saving MOC: %@",[error description]);
						}
				/*
						else
						{
								PropertyPicture *propertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
								
								if (propertyPicture. propertyPictureToProperty.defaultPicture==propertyPicture) {
										
										[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureDefaultNotification
																																												object:propertyPicture];
										
										// if property picture is saved then need to update it on server
										
										if([propertyPicture.synchStatus integerValue]!=1)
										{
												
												PropertyPicture *propertyPicture1 = (PropertyPicture *)[managedObjectContext objectWithID:propertyPicture.objectID];
												
												[propertyPicture1 uploadPicture];
												
										}
										
										
								}
								else if([propertyPicture.synchStatus integerValue]!=1)
								{
										
										[[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification object:propertyPicture];
								}
						}
				 */

//        }]; // parent
//			}
//			@catch (NSException *e)
//			{
//				CLS_LOG(@"Error finding object: %@: %@",[e name], [e reason]);
//			}
        
    }
    ENDEXCEPTION
}

/*!
 @discussion
 persist  property picture Data
 */
- (void) persistPropertyPicture:(PropertyPicture*)propertyPicture
{
	STARTEXCEPTION
	if(propertyPicture != nil)
	{

		__block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];

		[managedObjectContext performBlock:^{
			// Save the context.
			NSError *error = nil;
			if (![managedObjectContext save:&error])
			{
				CLS_LOG(@"Error in Saving MOC: %@",[error description]);

			}
		}]; // main

	}
	ENDEXCEPTION
}

/*!
 @discussion
   save Updated records in core data entity
 */
- (void) updatePropertyProfilePicture:(PropertyPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails

{
    STARTEXCEPTION
    if(propertyPicture != nil)
    {
        
        __block NSManagedObjectID* propertyPictureId = propertyPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            PropertyPicture *existingPropertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if(existingPropertyPicture != nil)
            {
                existingPropertyPicture.propertyPictureToProperty.defaultPicture=existingPropertyPicture;
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturePath])
                {
                    existingPropertyPicture.imagePath = [propertyPictureDetails objectForKey:kPropertyPicturePath];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPictureId]) {
                    existingPropertyPicture.propertyPictureId = [propertyPictureDetails objectForKey:kPropertyPictureId];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus]) {
                    existingPropertyPicture.synchStatus = [propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            
            PropertyPicture *propertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if (propertyPicture. propertyPictureToProperty.defaultPicture==propertyPicture) {
                
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureDefaultNotification
                                                                                    object:propertyPicture];
                
                [[PSPropertyPictureManager sharedManager] updateDefaultPhotograph:[propertyPicture JSONValue] withImage:nil];
                
            }
						else if([propertyPicture.synchStatus integerValue]!=1)
            {
                
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification
                                                                                    object:propertyPicture];
            }
            
        }]; // parent
        
    }
    
    ENDEXCEPTION
}


/*!
 @discussion
 Update property picture
 */
- (void) updateDefectPicture:(DefectPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails
{
    STARTEXCEPTION
    if(propertyPicture != nil)
    {
        
        __block NSManagedObjectID* propertyPictureId = propertyPicture.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            
            DefectPicture *existingPropertyPicture = (DefectPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if(existingPropertyPicture != nil)
            {
                
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturePath])
                {
                    existingPropertyPicture.imagePath = [propertyPictureDetails objectForKey:kPropertyPicturePath];
                }
                
                if ([propertyPictureDetails objectForKey:kDefectPictureId]) {
                    existingPropertyPicture.defectPictureId = [propertyPictureDetails objectForKey:kDefectPictureId];
                }
                
                if ([propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus]) {
                    existingPropertyPicture.synchStatus = [propertyPictureDetails objectForKey:kPropertyPicturesSynchStatus];
                }
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            
            PropertyPicture *propertyPicture = (PropertyPicture *)[managedObjectContext objectWithID:propertyPictureId];
            
            if([propertyPicture.synchStatus integerValue]!=1)
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kPropertyPictureSaveNotification
                                                                                    object:propertyPicture];
            }
            
            
        }]; // parent
        
    }
    ENDEXCEPTION
}


/*!
 @discussion
 Mark Appointment as Complete.
 */
- (void) updateAppointmentStatus:(AppointmentStatus)status appointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        
        __block NSManagedObjectID* _appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:_appointmentId];
            
            if(existingAppointment != nil)
            {
                /* NSString *statusString = [UtilityClass statusStringForAppointmentStatus:status];
                 if(!isEmpty(statusString))
                 {
                 existingAppointment.appointmentStatus = statusString;
                 }*/
                
                // existingAppointment.appointmentEndTime = [NSDate date];
                existingAppointment.isModified = [NSNumber numberWithBool:NO];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCompleteAppointmentFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kCompleteAppointmentSuccessNotification object:nil];
        }]; // parent
        
    }
    ENDEXCEPTION
}

- (void) updateAppointmentStatusOnly:(AppointmentStatus)status appointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        __block NSManagedObjectID* _appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            if (!isEmpty(_appointmentId))
            {
                Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:_appointmentId];
                
                if(existingAppointment != nil)
                {
                    NSString *statusString = [UtilityClass statusStringForAppointmentStatus:status];
                    if(!isEmpty(statusString))
                    {
                        existingAppointment.appointmentStatus = statusString;
                        existingAppointment.isModified = [NSNumber numberWithBool:YES];
                    }
                    // existingAppointment.appointmentEndTime = [NSDate date];
                    [[PSAppointmentsManager sharedManager]deleteCalendarEventForAppointment:existingAppointment];
                   
                    if(status == AppointmentStatusNoEntry || status == AppointmentStatusComplete)
                    {
                        existingAppointment.aptCompletedAppVersionInfo = [UtilityClass getAppVersionStringForCompletedAppt];
                        existingAppointment.loggedDate = [NSDate date];
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kAppointmentStatusUpdateSuccessNotification object:nil];
        }]; // parent
        
    }
    
    ENDEXCEPTION
}
- (void) updateCP12Info:(NSDictionary *)cp12InfoDictionary forAppointment:(Appointment *)appointment
{
    STARTEXCEPTION
    if(cp12InfoDictionary != nil)
    {
        __block NSManagedObjectID* appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            
            Appointment *existingAppointment = (Appointment *) [managedObjectContext objectWithID:appointmentId];
            if(existingAppointment != nil)
            {
                NSDate *issuedDate = [cp12InfoDictionary valueForKey:@"IssuedDate"];
                NSDate *receivedDate = [cp12InfoDictionary valueForKey:@"ReceivedDate"];
                
                NSString *receivedOnBehalfOf = [cp12InfoDictionary valueForKey:@"ReceivedOnBehalfOf"];
                NSNumber *issuedBy = [cp12InfoDictionary valueForKey:@"IssuedBy"];
                NSNumber *inspectionCarried = [cp12InfoDictionary valueForKey:@"InspectionCarried"];
                
                existingAppointment.appointmentToCP12Info.issuedDate = isEmpty(issuedDate)?nil:issuedDate;
                existingAppointment.appointmentToCP12Info.receivedDate = isEmpty(receivedDate)?nil:receivedDate;
                existingAppointment.appointmentToCP12Info.receivedOnBehalfOf = isEmpty(receivedOnBehalfOf)?nil:receivedOnBehalfOf;
                existingAppointment.appointmentToCP12Info.issuedDateString = isEmpty([UtilityClass stringFromDate:issuedDate dateFormat:kDateTimeStyle17])?nil:[UtilityClass stringFromDate:issuedDate dateFormat:kDateTimeStyle17];
                existingAppointment.appointmentToCP12Info.receivedDateString = isEmpty([UtilityClass stringFromDate:receivedDate dateFormat:kDateTimeStyle17])?nil:[UtilityClass stringFromDate:receivedDate dateFormat:kDateTimeStyle17];
                existingAppointment.appointmentToCP12Info.inspectionCarried = isEmpty(inspectionCarried)?nil:inspectionCarried;
                existingAppointment.appointmentToCP12Info.issuedBy = isEmpty(issuedBy)?nil:issuedBy;
                
                existingAppointment.isModified = [NSNumber numberWithBool:YES];
                existingAppointment.isSurveyChanged = [NSNumber numberWithBool:YES];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateCP12InfoFailureNotification object:nil];
                return;
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateCP12InfoSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

#pragma Planned Jobs

- (void) startPlannedJob:(PlannedTradeComponent *)jobData
{
    STARTEXCEPTION
    if(jobData != nil)
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block PlannedTradeComponent * currentJob = jobData;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
            if(existingJob != nil)
            {
                existingJob.jobStatus = kJobStatusInProgress;
                existingJob.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
            }

            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [self performSelector:@selector(addPlannedStartResumeforJob:) withObject:currentJob];
            [[PSLocalNotificationManager sharedManager] scheduleInProgressFor12HourNotificationForAppointmentID:currentJob.tradeComponentToAppointment.appointmentId];
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobStartNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) updatePlannedJobStatus:(PlannedTradeComponent *)jobData jobStaus:(NSString *)status
{
    STARTEXCEPTION
    if(jobData != nil)
    {
        // Creating managed objects
        __block NSString * jobstatus = status;
        __block PlannedTradeComponent * currentJobData = jobData;
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
			
        [managedObjectContext performBlock:^{
            if (!isEmpty(jobDataId))
            {
                PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
                if(existingJob != nil)
                {
                    existingJob.jobStatus = status;
                    existingJob.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
                }
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            
            if([jobstatus isEqualToString:kJobStatusInProgress])
            {
                [self performSelector:@selector(addPlannedStartResumeforJob:) withObject:currentJobData];
                //Scheduling Local Notification
                [[PSLocalNotificationManager sharedManager] scheduleInProgressFor12HourNotificationForAppointmentID:currentJobData.tradeComponentToAppointment.appointmentId];
            }
            else
            {
                //unschedule Local Notification
                [[PSLocalNotificationManager sharedManager] cancelInProgressFor12HourNotificationForAppointmentID:currentJobData.tradeComponentToAppointment.appointmentId];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobStatusUpdateNotification object:nil];
            
        }]; // parent
    }
    ENDEXCEPTION
}

- (void)addPlannedJobPauseData:(NSDictionary *)jobPauseData forJob:(PlannedTradeComponent *)jobData
{
    
    STARTEXCEPTION
    if(!isEmpty(jobPauseData))
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
            //JobDataList *existingJob1 = (JobDataList *)[managedObjectContext existingObjectWithID:existingJob.objectID error:nil];
            PlannedTradeComponent *existingJob1 = (PlannedTradeComponent *)[managedObjectContext objectWithID:existingJob.objectID];
            if(existingJob1 != nil)
            {
                NSString *pauseNotes = [jobPauseData valueForKey:kPauseNote];
                NSNumber *pausedBy = [jobPauseData valueForKey:@"pausedBy"];
                JobPauseReason *jobPauseReason = [jobPauseData valueForKey:kJobPauseReason];
                // JobPauseReason *jobPauseReason1 = (JobPauseReason *)[managedObjectContext existingObjectWithID:jobPauseReason.objectID error:nil];
                JobPauseReason *jobPauseReason1 = (JobPauseReason *)[managedObjectContext objectWithID:jobPauseReason.objectID];
                JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
                jobPauseData.pauseDate = [NSDate date];
                jobPauseData.actionType = @"Paused";
                jobPauseData.pauseNote = isEmpty(pauseNotes)?nil:pauseNotes;
                jobPauseData.pausedBy = isEmpty(pausedBy)?nil:pausedBy;
                if (!isEmpty(jobPauseReason1))
                {
                    jobPauseData.pauseReason = isEmpty(jobPauseReason1)?nil:jobPauseReason1;
                }
                [existingJob1 addJobPauseDataObject:jobPauseData];
                
                existingJob1.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobPauseDataFailureNotification object:nil];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobPauseDataSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void)addPlannedStartResumeforJob:(PlannedTradeComponent *)jobData
{
    STARTEXCEPTION
    // Creating managed objects
    __block NSManagedObjectID* jobDataId = jobData.objectID;
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    
    [managedObjectContext performBlock:^{
        PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
        //JobDataList *existingJob1 = (JobDataList *)[managedObjectContext existingObjectWithID:existingJob.objectID error:nil];
        PlannedTradeComponent *existingJob1 = (PlannedTradeComponent *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = @"InProgress";
            
            jobPauseData.pauseNote = nil;
            jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
            jobPauseData.pauseReason = nil;
            [existingJob1 addJobPauseDataObject:jobPauseData];
            existingJob1.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
            
        }
        
        [managedObjectContext performBlock:^{
					// Save the context.
					NSError *error = nil;
					CLS_LOG(@"Saving to PSC");
					if (![managedObjectContext save:&error]) {
							CLS_LOG(@"Error in Saving MOC: %@",[error description]);
					}
        }]; // main
    }]; // parent
    
    ENDEXCEPTION
}

- (void)addPlannedJobComplete:(NSManagedObjectID *)jobDataID
{
    STARTEXCEPTION
    // Creating managed objects
    __block NSManagedObjectID* jobDataId = jobDataID;
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    
    [managedObjectContext performBlock:^{
        PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
        //JobDataList *existingJob1 = (JobDataList *)[managedObjectContext existingObjectWithID:existingJob.objectID error:nil];
        PlannedTradeComponent *existingJob1 = (PlannedTradeComponent *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = @"Complete";
            jobPauseData.pauseNote = nil;
            jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
            jobPauseData.pauseReason = nil;
            [existingJob1 addJobPauseDataObject:jobPauseData];
            existingJob1.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
        }
        
        [managedObjectContext performBlock:^{
					// Save the context.
					NSError *error = nil;
					CLS_LOG(@"Saving to PSC");
					if (![managedObjectContext save:&error]) {
							CLS_LOG(@"Error in Saving MOC: %@",[error description]);
					}
        }]; // main
    }]; // parent
    
    ENDEXCEPTION
}

- (void) markPlannedJobComplete:(JobDataList *)jobData withUpdatedValues:(NSDictionary *)jobDataListDictionary
{
    //
    STARTEXCEPTION
    if(jobData != nil && !isEmpty(jobDataListDictionary))
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            if (!isEmpty(jobDataId))
            {
                PlannedTradeComponent *existingJob = (PlannedTradeComponent *)[managedObjectContext objectWithID:jobDataId];
                if(existingJob != nil)
                {
                    NSString *repairNotes = [jobDataListDictionary valueForKey:kJobRepairNotes];
                    existingJob.tradeComponentToAppointment.isModified = [NSNumber numberWithBool:YES];
                    existingJob.tradeComponentToAppointment.appointmentStatus = kAppointmentStatusComplete;
                    existingJob.jsnNotes = repairNotes;
                    existingJob.completionDate = [NSDate date];
                    existingJob.jobStatus = kJobStatusComplete;
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteFailureNotification object:nil];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [self performSelector:@selector(addPlannedJobComplete:) withObject:jobDataId];
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteSuccessNotification object:nil];
        }]; // parent
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteFailureNotification object:nil];
    }
    ENDEXCEPTION
}

#pragma Fault Job

- (void) startJob:(JobDataList *)jobData
{
    STARTEXCEPTION
    if(jobData != nil)
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block JobDataList * jobList = jobData;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            JobDataList *existingJob = (JobDataList *)[managedObjectContext objectWithID:jobDataId];
            if(existingJob != nil)
            {
                existingJob.jobStatus = kJobStatusInProgress;
                existingJob.jobDataListToAppointment.isModified = [NSNumber numberWithBool:YES];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [self performSelector:@selector(addJobStartResumeDataForJob:) withObject:jobList];
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobStartNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void) updateJobStatus:(JobDataList *)jobData jobStaus:(NSString *)status
{
    STARTEXCEPTION
    if(jobData != nil)
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block JobDataList * dataList = jobData;
        __block NSString * jobStatus = status;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            if (!isEmpty(jobDataId))
            {
                JobDataList *existingJob = (JobDataList *)[managedObjectContext objectWithID:jobDataId];
                if(existingJob != nil)
                {
                    existingJob.jobStatus = status;
                    existingJob.jobDataListToAppointment.isModified = [NSNumber numberWithBool:YES];
                }
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            if([jobStatus isEqualToString:kJobStatusInProgress])
            {
                [self performSelector:@selector(addJobStartResumeDataForJob:) withObject:dataList];
            }
            else //Always Pause
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobStatusUpdateNotification object:nil];
            }
        }]; // parent
    }
    ENDEXCEPTION
}

- (void)addJobPauseData:(NSDictionary *)jobPauseData forJob:(JobDataList *)jobData
{
    
    STARTEXCEPTION
    if(!isEmpty(jobPauseData))
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            JobDataList *existingJob = (JobDataList *)[managedObjectContext objectWithID:jobDataId];
            JobDataList *existingJob1 = (JobDataList *)[managedObjectContext objectWithID:existingJob.objectID];
            if(existingJob1 != nil)
            {
                NSString *pauseNotes = [jobPauseData valueForKey:kPauseNote];
                NSNumber *pausedBy = [jobPauseData valueForKey:@"pausedBy"];
                JobPauseReason *jobPauseReason = [jobPauseData valueForKey:kJobPauseReason];
                // JobPauseReason *jobPauseReason1 = (JobPauseReason *)[managedObjectContext existingObjectWithID:jobPauseReason.objectID error:nil];
                JobPauseReason *jobPauseReason1 = (JobPauseReason *)[managedObjectContext objectWithID:jobPauseReason.objectID];
                JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
                jobPauseData.pauseDate = [NSDate date];
                jobPauseData.actionType = @"Paused";
                jobPauseData.pauseNote = isEmpty(pauseNotes)?nil:pauseNotes;
                jobPauseData.pausedBy = isEmpty(pausedBy)?nil:pausedBy;
                if (!isEmpty(jobPauseReason1))
                {
                    jobPauseData.pauseReason = isEmpty(jobPauseReason1)?nil:jobPauseReason1;
                }
                [existingJob1 addJobPauseDataObject:jobPauseData];
                existingJob1.jobDataListToAppointment.isModified = [NSNumber numberWithBool:YES];
                
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobPauseDataFailureNotification object:nil];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kSaveJobPauseDataSuccessNotification object:nil];
        }]; // parent
    }
    ENDEXCEPTION
}

- (void)addJobStartResumeDataForJob:(JobDataList *)jobData
{
    STARTEXCEPTION
    // Creating managed objects
    __block NSManagedObjectID* jobDataId = jobData.objectID;
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    
    [managedObjectContext performBlock:^{
        JobDataList *existingJob = (JobDataList *)[managedObjectContext objectWithID:jobDataId];
        //JobDataList *existingJob1 = (JobDataList *)[managedObjectContext existingObjectWithID:existingJob.objectID error:nil];
        JobDataList *existingJob1 = (JobDataList *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = @"InProgress";
            jobPauseData.pauseNote = nil;
            jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
            jobPauseData.pauseReason = nil;
            
            [existingJob1 addJobPauseDataObject:jobPauseData];
            existingJob1.jobDataListToAppointment.isModified = [NSNumber numberWithBool:YES];
            
        }
        
        [managedObjectContext performBlock:^{
					// Save the context.
					NSError *error = nil;
					CLS_LOG(@"Saving to PSC");
					if (![managedObjectContext save:&error]) {
							CLS_LOG(@"Error in Saving MOC: %@",[error description]);
					}
        }]; // main
        
    }]; // parent
    ENDEXCEPTION
}

- (void)addJobDataForComplete:(JobDataList *)jobData
{
    
    STARTEXCEPTION
    // Creating managed objects
    __block NSManagedObjectID* jobDataId = jobData.objectID;
    __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
    
    [managedObjectContext performBlock:^{
        JobDataList *existingJob = (JobDataList *)[managedObjectContext objectWithID:jobDataId];
        //JobDataList *existingJob1 = (JobDataList *)[managedObjectContext existingObjectWithID:existingJob.objectID error:nil];
        JobDataList *existingJob1 = (JobDataList *)[managedObjectContext objectWithID:existingJob.objectID];
        if(existingJob1 != nil)
        {
            JobPauseData *jobPauseData = [NSEntityDescription insertNewObjectForEntityForName:kJobPauseData inManagedObjectContext:managedObjectContext];
            jobPauseData.pauseDate = [NSDate date];
            jobPauseData.actionType = @"Complete";
            jobPauseData.pauseNote = nil;
            jobPauseData.pausedBy = [[SettingsClass sharedObject]loggedInUser].userId;
            jobPauseData.pauseReason = nil;
            
            [existingJob1 addJobPauseDataObject:jobPauseData];
            existingJob1.jobDataListToAppointment.isModified = [NSNumber numberWithBool:YES];
            
        }
        
        [managedObjectContext performBlock:^{
					// Save the context.
					NSError *error = nil;
					CLS_LOG(@"Saving to PSC");
					if (![managedObjectContext save:&error]) {
							CLS_LOG(@"Error in Saving MOC: %@",[error description]);
					}
        }]; // main
        
    }]; // parent
    ENDEXCEPTION
}


- (void) markJobComplete:(JobDataList *)jobData withUpdatedValues:(NSDictionary *)jobDataListDictionary
{
    //
    STARTEXCEPTION
    if(jobData != nil/* && !isEmpty(jobDataListDictionary)*/)
    {
        // Creating managed objects
        __block NSManagedObjectID* jobDataId = jobData.objectID;
        __block JobDataList * jobDataList = jobData;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            if (!isEmpty(jobDataId))
            {
                JobDataList *existingJob = (JobDataList *)[managedObjectContext objectWithID:jobDataId];
                if(existingJob != nil)
                {
                    NSString *repairNotes = [jobDataListDictionary valueForKey:kJobRepairNotes];
                    NSString *followOnNotes = [jobDataListDictionary valueForKey:kFollowOnNotes];
                    existingJob.repairNotes = isEmpty(repairNotes)?nil:repairNotes;
                    existingJob.followOnNotes = isEmpty(followOnNotes)?nil:followOnNotes;
                    existingJob.jobDataListToAppointment.isModified = [NSNumber numberWithBool:YES];
                    existingJob.completionDate = [NSDate date];
                    existingJob.jobStatus = kJobStatusComplete;
                    existingJob.jobDataListToAppointment.isModified = [NSNumber numberWithBool:YES];
                    // [PSDatabaseContext turnObjectIntoFault:existingJob.jobDataListToAppointment.objectID];
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteFailureNotification object:nil];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
									CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [self performSelector:@selector(addJobDataForComplete:) withObject:jobDataList];
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteSuccessNotification object:nil];
        }]; // parent
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kJobCompleteFailureNotification object:nil];
    }
    ENDEXCEPTION
}

- (void) updateDetector:(Detector *)detector withDetectorCount:(NSNumber *)detectorCount
{
    STARTEXCEPTION
    if(detector != nil && !isEmpty(detectorCount))
    {
        // Creating managed objects
        __block NSManagedObjectID *detectorId = detector.objectID;
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            if (!isEmpty(detectorId))
            {
                Detector *existingDetector = (Detector *)[managedObjectContext objectWithID:detectorId];
                if(existingDetector != nil)
                {
                    existingDetector.detectorCount = detectorCount;
                    [PSDatabaseContext turnObjectIntoFault:existingDetector.detectorToProperty.propertyToAppointment.objectID];
                }
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateDetectorFailureNotification object:nil];
            }
            
            [managedObjectContext performBlock:^{
							// Save the context.
							NSError *error = nil;
							CLS_LOG(@"Saving to PSC");
							if (![managedObjectContext save:&error]) {
								CLS_LOG(@"Error in Saving MOC: %@",[error description]);
							}
            }]; // main
            [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateDetectorSuccessNotification object:nil];
        }]; // parent
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationOnMainThreadWithName:kUpdateDetectorFailureNotification object:nil];
    }
    ENDEXCEPTION
    
}

#pragma mark - Sync Update

-(void) updateAppointmentSyncStatusForAppointment:(Appointment *) appointment andStatus:(AppointmentSyncStatus) status andSuccessBlock:(void (^)(BOOL isSuccess))successBlock {
    
    [self updateAppointmentSyncStatusOnly:status appointment:appointment andSuccessBlock:^(BOOL isSuccess) {
        successBlock(isSuccess);
    }];

}

- (void) updateAppointmentSyncStatusOnly:(AppointmentSyncStatus)status appointment:(Appointment *)appointment andSuccessBlock:(void (^)(BOOL isSuccess))successBlock
{
    STARTEXCEPTION
    if(appointment != nil)
    {
        __block NSManagedObjectID* _appointmentId = appointment.objectID;
        // Creating managed objects
        __block NSManagedObjectContext *managedObjectContext = [[PSDatabaseContext sharedContext] managedObjectContext];
        
        [managedObjectContext performBlock:^{
            if (!isEmpty(_appointmentId))
            {
                Appointment *existingAppointment = (Appointment *)[managedObjectContext objectWithID:_appointmentId];
                
                if(existingAppointment != nil)
                {
                    NSNumber *statusString = [UtilityClass statusNumberForAppointmentSyncStatus:status];
                    if(!isEmpty(statusString))
                    {
                        existingAppointment.syncStatus = statusString;
                
                    }
            
                }
                else
                {
                    successBlock(NO);
                    return;
                }
            }
            else
            {
                successBlock(NO);
                return;
            }
            
            [managedObjectContext performBlock:^{
                // Save the context.
                NSError *error = nil;
                CLS_LOG(@"Saving to PSC");
                if (![managedObjectContext save:&error]) {
                    CLS_LOG(@"Error in Saving MOC: %@",[error description]);
                    successBlock(NO);
                }
                else{
                    successBlock(YES);
                }
            }]; // main
        }]; // parent
        
    }
    
    ENDEXCEPTION
}



@end
