//
//  PSDataUpdateManager.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 06/09/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSDataUpdateManager : NSObject

/*!
 @discussion
 Shared Object
 */
+ (PSDataUpdateManager *)sharedManager;


/*!
 @discussion
 deleteAppointment Method Removes Appointment and All its relationship objects from Core Data.
 */
- (void) deleteAppointment:(Appointment *)appointment;


/*!
 @discussion
 updateAppointmentWithNoEntry Method updates Appointment when no entry option is selected.
 */
- (void) updateAppointmentWithNoEntry:(NSDictionary *)noEntryDictionary;

/*!
 @discussion
 updateAppointmentSurvey Method updates Appointment survey to keep track of visited forms in stock survey.
 */
- (void) updateAppointmentSurvey:(NSDictionary *)surveyData forAppointment:(Appointment *)appointment;

/*!
 @discussion
 updateFaultAppointmentWithNoEntry Method updates Fault Appointment when no entry option is selected.
 */
- (void) updateFaultAppointmentWithNoEntry:(NSDictionary *)noEntryDictionary;

/*!
 @discussion
 updateAccomodations Method Updates the Accomodation Room Width and Length.
 */
- (void) updateAccomodations:(NSDictionary *)accomodationsInfo;

/*!
 @discussion
 turnAccomodationsIntoFault Method discards local changes of Accomodation Room Width and Length.
 */
- (void) turnAccomodationsIntoFault:(NSDictionary *)accomodationsInfo;
/*!
 @discussion
 updateTenant Method Updates the tenant details displayed on Tenant Detail UI .
 */
- (void) updateTenant:(NSDictionary *)tenantInfo;

/*!
 @discussion
 startAppointment Method changes appointment's status to started.
 */
- (void) startAppointment:(Appointment *)appointment;

/*!
 @discussion
 Mark Appointment as Complete.
 */
- (void) updateAppointmentStatus:(AppointmentStatus)status appointment:(Appointment *)appointment;
/*!
 @discussion
 Updates appointment status.
 */
- (void) updateAppointmentStatusOnly:(AppointmentStatus)status appointment:(Appointment *)appointment;
/*!
 @discussion
 deleteApplianceLocation Method Removes Appliance Location.
 */
- (void) deleteApplianceLocation:(ApplianceLocation *)applianceLocation;

/*!
 @discussion
 deleteApplianceManufacturer Method Removes Appliance Manufacturer.
 */
- (void) deleteApplianceManufacturer:(ApplianceManufacturer *)applianceManufacturer;

/*!
 @discussion
 deleteApplianceModel Method Removes Appliance Model.
 */
- (void) deleteApplianceModel:(ApplianceModel *)applianceModel;

/*!
 @discussion
 deleteApplianceType Method Removes Appliance Type.
 */
- (void) deleteApplianceType:(ApplianceType *)applianceType;

/*!
 @discussion
 markdeletePropertyPicture Method marks an removed image for offline support
 */
- (void) markDeletePropertyPicture:(PropertyPicture *)propertyPicture;
/*!
 @discussion
 deletePropertyPicture Method Removes deletePropertyPicture .
 */
- (void) deletePropertyPicture:(PropertyPicture *)propertyPicture;
/*!
 @discussion
deleteDefectPicture Method Removes deleteDefectPicture .
*/
- (void) deleteDefectPicture:(DefectPicture *)defectPicture;
/*!
 @discussion
 updateCP12Info Method Updates CP12Information.
 */
//- (void) updateCP12Info:(NSDictionary *)cp12InfoDictionary;
- (void) updateCP12Info:(NSDictionary *)cp12InfoDictionary forAppointment:(Appointment *)appointment;
- (void) updateAppointmentSurvey:(NSDictionary *)surveyData forAppointment:(Appointment *)appointment
                 withSurveyTitle: (NSString *)surveyTitle;
/*!
 @discussion
 Sets job data status to in progress.
 */
- (void) startJob:(JobDataList *)jobData;
-(void) deleteSurveyDataForAppointment:(Appointment *) appointment andHeatingTypeId:(NSString *)heatingTypeId;
/*!
 @discussion
 Updates job data status.
 */
- (void) updateJobStatus:(JobDataList *)jobData jobStaus:(NSString *)status;

/*!
 @discussion
 add jobs pause data in job.
 */
- (void)addJobPauseData:(NSDictionary *)jobPauseData forJob:(JobDataList *)jobData;

/*!
 @discussion
Marks job complete.
 */
- (void) markJobComplete:(JobDataList *)jobData withUpdatedValues:(NSDictionary *)jobDataListDictionary;



/*!
 @discussion
 Sets job data status to in progress.
 */
- (void) startPlannedJob:(PlannedTradeComponent *)jobData;

/*!
 @discussion
 Updates job data status.
 */
- (void) updatePlannedJobStatus:(PlannedTradeComponent *)jobData jobStaus:(NSString *)status;

/*!
 @discussion
 add jobs pause data in job.
 */
- (void)addPlannedJobPauseData:(NSDictionary *)jobPauseData forJob:(PlannedTradeComponent *)jobData;


/*!
 @discussion
 Marks job complete.
 */
- (void) markPlannedJobComplete:(PlannedTradeComponent *)jobData withUpdatedValues:(NSDictionary *)jobDataListDictionary;

/*!
 @discussion
 persist  property picture Data
 */
- (void) persistPropertyPicture:(PropertyPicture*)propertyPicture;
/*!
 @discussion
Update Property Picture.
 */
- (void) updatePropertyPicture:(PropertyPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails;

/*!
 @discussion
 Update Repair Picture.
 */
- (void) updateRepairPicture:(RepairPictures*)repairPicture forDictionary:(NSDictionary *) repairPictureDetails;


/*!
 @discussion
 Update Property Detector.
 */
- (void) updateDetector:(Detector *)detector withDetectorCount:(NSNumber *)detectorCount;


- (void) updateDefectPicture:(DefectPicture*)propertyPicture forDictionary:(NSDictionary *) propertyPictureDetails;

/*Update Sync Status*/
-(void) updateAppointmentSyncStatusForAppointment:(Appointment *) appointment andStatus:(AppointmentSyncStatus) status andSuccessBlock:(void (^)(BOOL isSuccess))successBlock;

@end
