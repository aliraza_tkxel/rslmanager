//
//  NSString_URLArgumentAdditions.h
//

#import <Foundation/Foundation.h>

/// Utilities for encoding and decoding URL arguments.
@interface NSString (NSString_URLArgumentAdditions)

// Returns a string that is escaped properly to be a URL argument.

/// This will also escape '%', so this should not be used on a string that has
/// already been escaped unless double-escaping is the desired result.
- (NSString*) stringByEscapingForURLArgument;
- (NSString *) stringByEscapingForURLArgument:(CFStringRef)ref;
/// Returns the unescaped version of a URL argument
//
/// This has the same behavior as stringByReplacingPercentEscapesUsingEncoding:,
/// except that it will also convert '+' to space.
- (NSString*) stringByUnescapingFromURLArgument;
- (NSString *) stringByAddingPercentEscapesUsingPriorityEncoding;
-(BOOL) isEmpty;
-(BOOL) isNumeric;
-(BOOL) isAlphabetic;
-(BOOL) isAlphabeticChar;

-(NSString*) trimString;
@end
