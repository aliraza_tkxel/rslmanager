//
//  UITextView+DisablePaste.h
//  BTEL_POC
//
//  Created by Adnan Ahmad on 20/05/2013.
//  Copyright (c) 2013 Adnan Ahmad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (DisablePaste)

- (BOOL)canBecomeFirstResponder;

@end
