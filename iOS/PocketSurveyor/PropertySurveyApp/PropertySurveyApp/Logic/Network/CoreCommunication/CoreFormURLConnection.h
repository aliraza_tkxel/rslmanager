/* Copyright (c) 2013 Tkxel. All rights reserved. */

#import <Foundation/Foundation.h>
#import "CoreURLConnection.h"


@interface CoreFormURLConnection : CoreURLConnection
{

}
- (void)invoke:(NSDictionary *)parameters streamLocation:(NSString *)streamLocation;
- (NSData *)invokeSync:(NSDictionary *)parameters;
@end
