/* Copyright (c) 2013 Tkxel. All rights reserved. */

#import "PSARESTService.h"

@implementation PSARESTService

@synthesize serviceUrl;

#pragma mark Initializaton

- (id)initWithServiceUrl:(NSString *)_serviceUrl
{
    if (self = [super initWithRootUrl:kServerUrl serviceUrl:_serviceUrl])
		{
    }
    return (self);
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setServiceUrl:nil];
    [super dealloc];
}

#pragma mark CoreRESTService

- (BOOL)isErrorResponse:(id)jsonObject
{

    if(!isEmpty(jsonObject))
    {
        NSDictionary *responseDictionary = [jsonObject JSONValue];
        NSDictionary *statusDictionary = [responseDictionary objectForKey:kStatusTag];
        
        if([[statusDictionary valueForKey:kCodeTag] intValue] == 200)
            return (NO);
    }
    
    return (YES);
}
- (void)didReceiveError:(id)xmlObject
{
    // grab the status object
//    RFStatus *status = (RFStatus *)[xmlObject performSelector:@selector(status)];
    
    // check for a session timeout...
//    if ([[status code] isEqual:@"403"]) {
//        // post a notification that the user's session timed out
//        [[NSNotificationCenter defaultCenter] postNotificationName:kCommonSessionTimeout
//                                                            object:nil];
//    } else if (([[status code] isEqual:@"400"] && [[status subCode] isEqual:@"3-1"]) || [[status code] isEqual:@"401"] ) {
//        // post a notification that requested entuty not found
//        [[NSNotificationCenter defaultCenter] postNotificationName:kAuthorizationFailureNotification
//                                                            object:nil];
//    } 
//    // all other errors are concerened unhandled and unexpected
//    else {
//        // post a notification that the server returned an error
//        [[NSNotificationCenter defaultCenter] postNotificationName:kCommonUnexpectedServerError
//                                                            object:nil];
//    }
}

#pragma mark CoreURLConnectionDelegate

- (void)connection:(CoreURLConnection *)connection didFailWithError:(NSError *)error {
	// post a notification that we can't reach the service

    switch ([error code]) {
        case -1009:
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:kInternetUnavailable
                                                                    object:nil];
            }
            break;
        case -1005:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kInternetUnavailable
                                                                object:nil];
        }
            break;
        case -1004:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kInternetUnavailable
                                                                object:nil];
        }
            break;
        case -1001:
        {
            UIAlertView* alert = [[UIAlertView alloc]initWithTitle:LOC(@"KEY_ALERT_ERROR") message:@"Request Timeout!" delegate:self cancelButtonTitle:LOC(@"KEY_ALERT_OK") otherButtonTitles:nil];
            [alert show];
            [[NSNotificationCenter defaultCenter] postNotificationName:kInternetRequestTimeOut
                                                                object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kCommonUnableToAccessServices object:nil];
        }
            break;
        default: {
            [[NSNotificationCenter defaultCenter] postNotificationName:kCommonUnableToAccessServices
                                                                object:nil];
        }
            break;
    }
}

@end
