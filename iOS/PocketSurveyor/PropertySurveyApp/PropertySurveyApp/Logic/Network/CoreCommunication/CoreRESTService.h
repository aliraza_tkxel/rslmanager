/* Copyright (c) 2013 Tkxel. All rights reserved. */

#import <Foundation/Foundation.h>

#import "CoreFormURLConnection.h"

@class CoreRESTService;


@protocol CoreRESTServiceDelegate < NSObject >

@optional

- (void)restService:(CoreRESTService *)service hasDownloaded:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength;
- (void)restService:(CoreRESTService *)service didFinishStreaming:(NSString *)filePath;
- (BOOL)restService:(CoreRESTService *)service didReceiveError:(id)jsonObject;
- (void)restService:(CoreRESTService *)service didReceiveResponse:(id)jsonObject;
- (void)restService:(CoreRESTService *)service didReceiveResponseHeader:(NSDictionary *)responseHeader;
@end


@interface CoreRESTService : NSObject < CoreURLConnectionDelegate > {
    
    NSString *rootURL;
    id<CoreRESTServiceDelegate> delegate;
    CoreFormURLConnection *connection;
    NSString *method;
    NSString *contentType;
    Class responseClass;
    
}

@property (nonatomic, retain) NSString *rootURL;
@property (nonatomic, assign) id<CoreRESTServiceDelegate> delegate;
@property (nonatomic, retain) CoreFormURLConnection *connection;
@property (nonatomic, retain) NSString *method;
@property (nonatomic, retain) NSString *contentType;
@property (nonatomic, retain) Class responseClass;

- (id)initWithRootUrl:(NSString *)rootUrl serviceUrl:(NSString *)url;
- (BOOL)active;
- (void)cancel;
- (BOOL)isErrorResponse:(id)jsonObject;
- (void)didReceiveError:(id)jsonObject;


- (void) executeRequest:(NSDictionary *)parameters responseClass:(Class)responseClass streamLocation:streamLocation enableCompression:(BOOL)enableCompression;

- (NSData *) executeSyncRequest:(NSDictionary *)parameters enableCompression:(BOOL)enableCompression;

- (void)setUrl:(NSString *)rootUrl serviceUrl:(NSString *)serviceUrl;

@end


