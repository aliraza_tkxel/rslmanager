/* Copyright (c) 2013 Tkxel. All rights reserved. */

#import <Foundation/Foundation.h>

typedef enum {
    
    RFURLConnectionCacheModeNone,
    
    // Caches responses application wide, so multiple instances of the service use the same
    // cache backing.
    RFURLConnectionCacheModeApplication  
} RFURLConnectionCacheMode;

@class CoreURLConnection;

@protocol CoreURLConnectionDelegate < NSObject >

@optional
- (void)connection:(CoreURLConnection *)connection hasDownloaded:(NSInteger)downloadedBytes expectedContentLength:(NSInteger)expectedContentLength;
- (void)connection:(CoreURLConnection *)connection didFinishStreaming:(NSString *)filePath;
- (void)connection:(CoreURLConnection *)connection didReceiveData:(NSData *)data;
- (void)connection:(CoreURLConnection *)connection didFailWithError:(NSError *)error;
- (void)connection:(CoreURLConnection *)connection didReceiveResponseHeader:(NSDictionary *)responseHeader;

@end

@interface CoreURLConnection : NSObject {
    id<CoreURLConnectionDelegate> delegate;
    NSArray *bufferedContentTypes;
    NSString *url;
    NSURLConnection *connection;
    BOOL connectionActive;
    NSMutableData *responseData;
    BOOL responseBuffered;
    NSString *filePath;
    NSFileHandle *file;
    RFURLConnectionCacheMode cacheMode;
    BOOL enableCompression;
    NSString *method;
    NSString *contentType;
}

@property (nonatomic, assign) id<CoreURLConnectionDelegate> delegate;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSArray *bufferedContentTypes;
@property (nonatomic, retain) NSURLConnection *connection;
@property (nonatomic, assign) BOOL connectionActive;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, assign) BOOL responseBuffered;
@property (nonatomic, retain) NSString *filePath;
@property (nonatomic, retain) NSFileHandle *file;
@property (nonatomic, assign) BOOL enableCompression;
@property (nonatomic, assign) RFURLConnectionCacheMode cacheMode;
@property (nonatomic, retain) NSString *method;
@property (nonatomic, retain) NSString *contentType;

- (id)initWithDelegate:(id<CoreURLConnectionDelegate>)delegate url:(NSString *)url;
- (BOOL)active;
//- (void)invoke:(NSString *)query content:(NSData *)content contentType:(NSString *)contentType streamLocation:(NSString *)streamLocation;
- (void)invoke:(NSString *)query content:(NSData *)content streamLocation:(NSString *)streamLocation;
- (NSData *) invokeSync:(NSString *)query content:(NSData *)content;
- (void)cancel;
- (void)connectionDidFinishLoading:(NSURLConnection *)pConnection;

@end
