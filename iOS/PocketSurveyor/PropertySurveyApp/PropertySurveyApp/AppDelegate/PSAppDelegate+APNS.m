//
//  PSAppDelegate+APNS.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate+APNS.h"

@implementation PSAppDelegate (APNS)

- (void) registerForAPNS
{
    if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        CLS_LOG(@"Requesting permission for push notifications...iOS8+");
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |
                                                UIUserNotificationTypeSound categories:nil];
        [UIApplication.sharedApplication registerUserNotificationSettings:settings];
    } else {
        NSLog(@"Registering device for push notifications...iOS7 and below");
        [UIApplication.sharedApplication registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |
         UIRemoteNotificationTypeSound];
        
    }
    
}
- (void) unregisterForAPNS
{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

#pragma mark -
#pragma mark PushNotification Methods

- (void) handlePushNotificationOnAppLaunch:(NSDictionary*)userInfo {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    NSDictionary* payload = [userInfo objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(payload != nil) {
        [self handlePushNotificationWithPayload:payload notificationMode:APNSModeLaunch];
    }
    return;
}

- (void) handlePushNotificationWithPayload:(NSDictionary*)payload notificationMode:(APNSMode)mode {
    CLS_LOG(@"didReceiveRemoteNotification %@",payload);
    
    [[APNSHandler sharedHandler] handlePushNotificationWithPayload:payload notificationMode:mode];
}


- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    application.applicationIconBadgeNumber = 0;
    NSString *deviceTokenStr = [[[deviceToken description]
                                 stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                                stringByReplacingOccurrencesOfString:@" "
                                withString:@""];
    NSLog(@"APNS DTK: %@",deviceTokenStr);
    if(!isEmpty(deviceTokenStr))
    {
        [[SettingsClass sharedObject] setDeviceToken:deviceTokenStr];
    }
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    application.applicationIconBadgeNumber = 0;
    CLS_LOG(@"payload %@",notification.alertBody);
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:APP_NAME message:notification.alertBody delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}


- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    CLS_LOG(@"%@",[error description]);
    if ([error code] != 3010) // 3010 is for the iPhone Simulator
    {
        [[SettingsClass sharedObject] setDeviceToken:@""];
    }
    
}



- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if(state == UIApplicationStateActive){
        CLS_LOG(@"UIApplicationStateActive");
        [self handlePushNotificationWithPayload:userInfo notificationMode:APNSModeRemote];
    }
    else if(state == UIApplicationStateInactive){
        CLS_LOG(@"UIApplicationStateInactive");
        [self handlePushNotificationWithPayload:userInfo notificationMode:APNSModeLaunch];
    } else if(state == UIApplicationStateBackground){
        CLS_LOG(@"UIApplicationStateBackground");
    }
}


- (void)application:(UIApplication *)application
didRegisterUserNotificationSettings:(UIUserNotificationSettings *)settings
{
    NSLog(@"Registering device for push notifications..."); // iOS 8
    [application registerForRemoteNotifications];
}




@end
