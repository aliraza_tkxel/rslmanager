//
//  PSAppDelegate.m
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 30/07/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate.h"
#import "PSAppDelegate+Window.h"
#import "PSAppDelegate+APNS.h"
#import "PSAppDelegate+Version.h"
#import "PSAppointmentsViewController.h"
#import "PSLoginViewController.h"
#import "PSCustomNavigationController.h"
#import "PSForgotPasswordViewController.h"
#import "PSChangePasswordViewController.h"

@implementation PSAppDelegate
@synthesize userLoggedInToApplication,postingDataObjectAppointments;

+ (PSAppDelegate *) delegate
{
    return (PSAppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FIRApp configure];
    [Fabric with:@[[Crashlytics class]]];
    self.postingDataObjectAppointments = 0;
    application.applicationIconBadgeNumber = 0;
    //Register For Notifications
    [self registerNotifications];
    [self registerForAPNS];
    [self setUserLoggedInToApplication:FALSE];
    [self loadAppVersionInfo];
    [self registringSettingBundleFields];
    //Start Reachability Monitoring
    
    [[ReachabilityManager sharedManager] startMonitoring];
    
    
    //Create Read MOC ..Dont Remove Below 2 lines . These are for coredata movement + storage
    [[PSDatabaseContext sharedContext] persistentStoreCoordinator];
    [[PSDatabaseContext sharedContext] managedObjectContext];
    
    [[SettingsClass sharedObject] loadSettings];
    
    CLS_LOG(@"%@", [SettingsClass sharedObject].loggedInUser);
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    if([UtilityClass isUserLoggedIn] && [SettingsClass sharedObject].autoLogin)
    {
        
        [[PSAuthenticator sharedAuthenticator] authenticateUser];
        self.appointmentsViewController = [[PSAppointmentsViewController alloc] initWithNibName:@"PSAppointmentsViewController" bundle:nil];
        PSCustomNavigationController *navigationController = [[PSCustomNavigationController alloc] initWithRootViewController:self.appointmentsViewController];
        self.window.rootViewController = navigationController;
    }
    else
    {
        self.loginViewController = [[PSLoginViewController alloc] initWithNibName:@"PSLoginViewController" bundle:nil];
        PSCustomNavigationController *navigationController = [[PSCustomNavigationController alloc] initWithRootViewController:self.self.loginViewController];
        [navigationController setNavigationBarHidden:YES];
        self.window.rootViewController = navigationController;
    }
    
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    return YES;
}

-(void)registringSettingBundleFields
{
#if TEST
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:5]
                                                            forKey:@"firetime_preference"];
    [defaults registerDefaults:appDefaults];
    [defaults synchronize];
#endif
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[SettingsClass sharedObject] saveSettings];
    
    //Stop Reachability Monitoring
    /*[[ReachabilityManager sharedManager] stopMonintoring];
     [[PSOutBox sharedOutbox]stopMonintoring];*/
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if([UtilityClass isUserLoggedIn] && [SettingsClass sharedObject].autoLogin)
    {
        [[PSAuthenticator sharedAuthenticator] authenticateUser];
    }
    self.postingDataObjectAppointments = 0;
    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    self.postingDataObjectAppointments =0;
    [PSPropertyPictureManager sharedManager].isPostingImage=NO;
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[SettingsClass sharedObject] saveSettings];

    //Stop Reachability Monitoring
    [[ReachabilityManager sharedManager] stopMonintoring];

    [self deRegisterNotifications];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}


#pragma mark Notifications
- (void) registerNotifications {
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReceiveSignOutNotification) name:kUserSignOutNotification object:nil];
	//[Crashlytics startWithAPIKey:@"f8e1dd31430ee8fe926d9ad14c80d63cace6eef7"];//bhg
	//[Crashlytics startWithAPIKey:@"0b5adcf0be87c3837e31b94a4a43f5874dda8eae"]; //tkxel

    CLS_LOG(@"-------------------------->registerNotifications------------------------");
}

- (void) deRegisterNotifications {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kUserSignOutNotification object:nil];
    CLS_LOG(@"*************************deRegisterNotifications************************");
}

- (void) onReceiveSignOutNotification
{
    [self registerForAPNS];
    self.postingDataObjectAppointments = 0;
    [[PSAppDelegate delegate] showLoginView];
}

@end
