//
//  PSAppDelegate+APNS.h
//  PropertySurveyApp
//
//  Created by Ahmad Ansari on 26/08/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "PSAppDelegate.h"

@interface PSAppDelegate (APNS)

- (void) registerForAPNS;
- (void) unregisterForAPNS;

@end
