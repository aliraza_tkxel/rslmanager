//
//  FaultRepairData.m
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "FaultRepairData.h"
#import "JobDataList.h"


@implementation FaultRepairData

@dynamic faultRepairID;
@dynamic faultRepairDescription;
@dynamic faultRepairDataToJobDataList;

@end
