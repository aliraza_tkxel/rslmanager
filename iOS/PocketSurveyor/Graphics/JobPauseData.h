//
//  JobPauseData.h
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class JobDataList, JobPauseReason;

@interface JobPauseData : NSManagedObject

@property (nonatomic, retain) NSDate * pauseDate;
@property (nonatomic, retain) NSString * pauseNote;
@property (nonatomic, retain) JobPauseReason *pauseReason;
@property (nonatomic, retain) JobDataList *jobData;

@end
