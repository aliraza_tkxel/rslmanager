//
//  JobPauseData.m
//  PropertySurveyApp
//
//  Created by Yawar on 03/12/2013.
//  Copyright (c) 2013 TkXel. All rights reserved.
//

#import "JobPauseData.h"
#import "JobDataList.h"
#import "JobPauseReason.h"


@implementation JobPauseData

@dynamic pauseDate;
@dynamic pauseNote;
@dynamic pauseReason;
@dynamic jobData;

@end
