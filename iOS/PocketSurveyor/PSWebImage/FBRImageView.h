//
//  FBRFBRImageView.h
//  Faber
//
//  Created by Imran on 3/29/13.
//  Copyright (c) 2013 Hira Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FBRImageView : UIImageView {
    
}

@property (nonatomic, assign) BOOL thumbnail;
@property (nonatomic, retain) NSString *thumbnailKey;

- (BOOL)createThumbnails;

@end
