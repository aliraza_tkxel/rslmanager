/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */



@class FBRWebImageDownloader;

@protocol FBRWebImageDownloaderDelegate <NSObject>

@optional

- (void)imageDownloaderDidFinish:(FBRWebImageDownloader *)downloader;
- (void)imageDownloader:(FBRWebImageDownloader *)downloader didFinishWithImage:(UIImage *)image;
- (void)imageDownloader:(FBRWebImageDownloader *)downloader didFailWithError:(NSError *)error;

@end
