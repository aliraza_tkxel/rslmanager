<?xml version="1.0"?>
<project name="MyWebApp" default="build" basedir=".">
	<description>Builds the MyWebApp project</description>
	<!--
      -  Property definitions
      -->

	<property name="debug" value="false" overwrite="false"/>
	<property name="dotnet" value="C:\Windows\Microsoft.NET\Framework\v4.0.30319" overwrite="false"/>
	<property name="devenvpath" value="C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE" overwrite="false"/>	
	<property name="nunit" value="C:\Program Files\NUnit 2.5.9\bin\net-2.0" overwrite="false"/>

	<!--
	<property name="dotnet" value="c:/windows/Microsoft.NET/Framework/v2.0.50727" overwrite="false"/>
        -->

	<property name="source" value="source\rsl_master_site" overwrite="false" />
	<property name="target" value="deploy\rsl_master_site" overwrite="true" />		

	
	<!-- new VInet dev properties -->	
	<if test="${not property::exists('buildoutput')}">
		<fail message="Buildoutput property not specified!"></fail>
	</if>

  <if test="${buildoutput=='dev'}">
    <echo>dev build</echo>
    <property name="IIS_ROOT" value="U:\" overwrite="true" />
    <property name="SITE_NAME" value="RSLBHAManager" overwrite="true" />
    <property name="DB_NAME.target" value="RSLBHAManager" overwrite="false"/>
    <property name="DB_HOST.target" value="10.0.1.75" overwrite="false" />
  </if>

  <if test="${buildoutput=='test'}">
    <echo>test build</echo>
    <property name="IIS_ROOT" value="T:\" overwrite="true" />
    <property name="SITE_NAME" value="BRSLBHAManager" overwrite="true" />
    <property name="DB_NAME.target" value="RSLBHAManager" overwrite="false"/>
    <property name="DB_HOST.target" value="10.0.1.73" overwrite="false" />
  </if>

  <if test="${buildoutput=='live'}">
    <echo>live build</echo>
    <property name="IIS_ROOT" value="L:\" overwrite="true" />
    <property name="SITE_NAME" value="RSLBHAManager" overwrite="true" />
    <property name="DB_NAME.target" value="RSLBHAManager" overwrite="false"/>
    <property name="DB_HOST.target" value="10.0.1.66" overwrite="false" />
  </if>

	<!--
      -  'clean' Target
      -->
	<target name="clean" description="Remove all target directory">
		<delete dir="${buildoutput}/${target}" if="${directory::exists(target)}" />
	</target>

	<!--
      - 'build' Target 
      -
      - Precompiles this ASP.Net application
      - into the ../MyWebApp.deploy directory.
      -->
	<target name="build" description="builds the RSL master site">
		<!--
         -  The precompiler will not compile the app if files
         -  exist in the output directory, so we will delete
         -  it before we run the precompiler.
         -->
		<!--
         -  Precompile the web site
         -->
		<echo message="Building FaultLocator binaries ..." />
		<exec basedir="${devenvpath}"
			   program="devenv.com"
			   commandline="/build release ${buildoutput}\${rsl_master_site.FaultLocator.source}\Broadland.TenantsOnline.sln"
			   workingdir="."
			   failonerror="true" />

		<echo message="Building FaultLocator..." />
		<exec basedir="."
			   program="${dotnet}/aspnet_compiler.exe"
			   commandline="-nologo -f -v /FaultLocator -p ${buildoutput}\${rsl_master_site.FaultLocator.source} ${buildoutput}\${rsl_master_site.FaultLocator.target}"
			   workingdir="."
			   failonerror="true" />


		<echo message="Building RSL_Master_Site..." />
		<exec basedir="."
			  program="${dotnet}/aspnet_compiler.exe"
			  commandline="-nologo -f -v /rsl_master_site -p ${buildoutput}/${source} ${buildoutput}/${target}"
			  workingdir="."
			  failonerror="true" />
		<!-- 
         - If the precompiler doesn't recognize the function of a file
         - it finds in the directory being precompiled, it copies that
         - file to the target folder. Therefore, it is necessary to do
         - a bit of cleanup after the precompiler to remove files we
         - do not want to deploy to the production server.
         -->
		<delete>
			<fileset>
				<include name="${buildoutput}/${target}/*.vss" />
			</fileset>
		</delete>
	</target>

	<target name="connectionStrings.IIS6" description="Copies the database connection strings over">

		<!-- RSL master site Web.config files -->
		<copy file="${buildoutput}\${source}\web.template.config" tofile="${IIS_ROOT}\rsl_master_site\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
          <token key="SITE_NAME" value="${SITE_NAME}"></token>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${buildoutput}\${source}\global.template.asa" tofile="${IIS_ROOT}\rsl_master_site\global.asa" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${buildoutput}\${source}\live_connections\db_connection.template.asp" tofile="${IIS_ROOT}\rsl_master_site\live_connections\db_connection.asp" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
	</target>

	<target name="connectionStrings.IIS7" description="Copies the database connection strings over">

		<!-- RSL master site Web.config files -->
		<copy file="${buildoutput}\${source}\web.iis7.template.config" tofile="${IIS_ROOT}\rsl_master_site\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="SITE_NAME" value="${SITE_NAME}"></token>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${buildoutput}\${source}\global.template.asa" tofile="${IIS_ROOT}\rsl_master_site\global.asa" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${buildoutput}\${source}\live_connections\db_connection.template.asp" tofile="${IIS_ROOT}\rsl_master_site\live_connections\db_connection.asp" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
	</target>

	<!--<target name="connectionStrings.IIS7.test" description="Copies the database connection strings over">

		--><!-- RSL master site Web.config files --><!--
		<copy file="${buildoutput}\${source}\web.iis7.template.config" tofile="${buildoutput}\${target}\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="SITE_NAME" value="${SITE_NAME}"></token>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${buildoutput}\${source}\global.template.asa" tofile="${buildoutput}\${target}\global.asa" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${buildoutput}\${source}\live_connections\db_connection.template.asp" tofile="${buildoutput}\${target}\live_connections\db_connection.asp" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
	</target>-->

</project>
