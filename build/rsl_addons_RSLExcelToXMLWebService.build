<?xml version="1.0"?>
<project name="MyWebApp" default="build" basedir=".">
	<description>Builds the RSL manager project</description>
	<!--
      -  Property definitions
      -->
	<property name="debug" value="false" overwrite="false"/>
	<property name="dotnet" value="c:/windows/Microsoft.NET/Framework/v2.0.50727" overwrite="false"/>
	<property name="dotnet4" value="c:/windows/Microsoft.NET/Framework/v4.0.30319" overwrite="false"/>
	<property name="devenvpath" value="C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE" overwrite="false"/>	
	<property name="nunit" value="C:\Program Files\NUnit 2.5.9\bin\net-2.0" overwrite="false"/>

	<property name="rsl_addons.target" value="deploy\rsl_addons" overwrite="false" />

	<property name="rsl_addons.RSLExcelToXMLWebService.source" value="source\rsl_addons\RSLExcelToXMLWebService" overwrite="false" />
	<property name="rsl_addons.RSLExcelToXMLWebService.target" value="deploy\rsl_addons\RSLExcelToXMLWebService" overwrite="true" />

	<!-- Development Database connection string entries -->	

	<property name="DB_USER.target" value="bhg\sqlservice" overwrite="false" />
	<property name="DB_PWD.target" value="" overwrite="false" />


	<if test="${not property::exists('buildoutput')}">
		<fail message="Buildoutput property not specified!"></fail>
	</if>

  <!-- new VInet dev properties -->  
  <if test="${buildoutput=='dev'}">
    <echo>dev build</echo>    
    <property name="IIS_ROOT" value="U:\rsl_addons" overwrite="true" />
    <property name="SITE_NAME" value="RSLBHAManager" overwrite="true" />
    <property name="DB_NAME.target" value="RSLBHAManager" overwrite="true" />
    <property name="DB_HOST.target" value="10.0.1.75" overwrite="true" />
  </if>

  <if test="${buildoutput=='test'}">
    <echo>test build</echo>
    <property name="IIS_ROOT" value="T:\rsl_addons" overwrite="true" />
    <property name="SITE_NAME" value="RSLBHAManager" overwrite="true" />
    <property name="DB_NAME.target" value="RSLBHAManager" overwrite="true" />
    <property name="DB_HOST.target" value="10.0.1.73" overwrite="true" />
  </if>

  <if test="${buildoutput=='live'}">
    <echo>live build</echo>
    <property name="IIS_ROOT" value="L:\rsl_addons" overwrite="true" />
    <property name="SITE_NAME" value="RSLBHAManager" overwrite="true" />
    <property name="DB_NAME.target" value="RSLBHAManager" overwrite="true" />
    <property name="DB_HOST.target" value="10.0.1.66" overwrite="true" />
  </if>
 

	<!--
      -  'clean' Target
      -->
	<target name="clean" description="Remove all target directories">
		<delete dir="${buildoutput}\${rsl_addons.RSLExcelToXMLWebService.target}" />
	</target>

	<!--
      - 'build' Targets      
      -->
	<target name="build" description="builds the RSL assemblies and addons">
		
		<!--
	 -  Precompile the web site
	 -->

		<echo message="Building RSLExcelToXMLWebService..." />
		<exec basedir="."
			   program="${dotnet4}/aspnet_compiler.exe"
			   commandline="-nologo -f -v /RSLExcelToXMLWebService -p ${buildoutput}\${rsl_addons.RSLExcelToXMLWebService.source} ${buildoutput}\${rsl_addons.RSLExcelToXMLWebService.target}"
			   workingdir="."
			   failonerror="true" />

		<!-- 
	 - If the precompiler doesn't recognize the function of a file
	 - it finds in the directory being precompiled, it copies that
	 - file to the target folder. Therefore, it is necessary to do
	 - a bit of cleanup after the precompiler to remove files we
	 - do not want to deploy to the production server.
	 -->
		<delete>
			<fileset>
				<include name="${buildoutput}/${rsl_addons.RSLExcelToXMLWebService.target}/*.vss" />
			</fileset>
		</delete>
	</target>

	<target name="connectionStrings.IIS7" description="Copies the database connection strings over">

		<!-- RSL Addons Web.config files -->
		<copy file="${buildoutput}\${rsl_addons.RSLExcelToXMLWebService.target}\web.template.config" tofile="${IIS_ROOT}\RSLExcelToXMLWebService\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>

	</target>

	<target name="connectionStrings.IIS6" description="Copies the database connection strings over">

		<!-- RSL Addons Web.config files for IIS7 which is on the new VINet servers. Live system is still on Win2k3 and IIS6.-->
		<copy file="${buildoutput}\${rsl_addons.RSLExcelToXMLWebService.target}\web.iis7.template.config" tofile="${IIS_ROOT}\RSLExcelToXMLWebService\web.config" overwrite="true">
			<filterchain>
				<replacetokens>					
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>

	</target>		

	<target name="seleniumTests" description="Runs the selenium UI smoke tests">
		<!-- Use the correct file first dev/test-->
		<copy file="${rsl_addons.SeleniumTests.source}\rsl_addons.SeleniumTests\app.dev.config" tofile="${rsl_addons.SeleniumTests.source}\rsl_addons.SeleniumTests\app.config" overwrite="true">			
		</copy>
		
		<exec program="${nunit}\nunit-console.exe" failonerror="true"  >
			<arg value="${rsl_addons.SeleniumTests.source}\rsl_addons.SeleniumTests\bin\release\rsl_addons.SeleniumTests.dll" />
			<arg value="/xml=rsl_addons.SeleniumTests-Results.xml" />
		</exec>

	</target>

</project>
