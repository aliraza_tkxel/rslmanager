<?xml version="1.0"?>
<project name="RSL Manager Setup" default="build" basedir=".">
	<description>Builds the demo version of the site including synchronisation with the dev database</description>
	<!--
      -  Property definitions
      -->
	
	<!--<property name="rsl_master_site.source" value="source\rsl_master_site\" overwrite="true" />-->
	<!-- Get the version string from the test deploy area NOT the source as this is what is used by
	     the dev for new integrations -->
	<property name="nunit" value="C:\Program Files\NUnit 2.5.9\bin\net-2.0" overwrite="false"/>
	
	<property name="rsl_master_site.source" value="deploy\rsl_master_site" overwrite="true" />
	<property name="rsl_master_site.deploy" value="T:\rsl_master_site\" overwrite="true" />
  
	<property name="rsl_tenants_online.deploy" value="rsl_tenants_online\" overwrite="true" />
	<property name="rsl_tenants_online.deploy" value="T:\TenantsOnline\" overwrite="true" />
	
	<property name="rsl_addons.deploy" value="T:\rsl_addons" overwrite="true" />

	<property name="RedGateTools" value="c:\program files (x86)\Red Gate" overwrite="false"/>
	<property name="SqlCmdPath" value="c:\program files\Microsoft SQL Server\100\tools\binn" overwrite="false"/>

	<!-- Development Scripts Folder  -->
	<property name ="dbschema.buildstem" value="C:\builds\BHA.rslmanager\" />
	<property name="dbschema.source" value="test\source\db_schema" overwrite="false" />	

	<!-- 
	  - Connection Strings/properties
	-->

	<property name="SITE_NAME" value="RSLBHALive" overwrite="true" />	
	
	<!-- Test Database -->
	<property name="DB_NAME.source" value="RSLBHALive" overwrite="true" />
	<property name="DB_HOST.source" value="10.0.1.73" overwrite="true" />
	<property name="DB_USER.source" value="bhg\SQLService" overwrite="false" />
	<property name="DB_PWD.source" value="" overwrite="false" />

	<!-- Target Database -->
	<property name="DB_NAME.target" value="RSLBHALive" overwrite="true" />
	<property name="DB_HOST.target" value="(local)" overwrite="true" />
	<property name="DB_USER.target" value="SQL DB USERNAME" overwrite="false" />
	<property name="DB_PWD.target" value="SQL DB PASSWORD" overwrite="false" />

	<!-- Release Forms -->
	<property name="rsl_master_site.releaseForms.source" value="C:\My Dropbox\Housing\BHA\RSLManager\Software Release Forms\" />
	<property name="rsl_tenants_online.releaseForms.source" value="C:\My Dropbox\Housing\BHA\TenantsOnline\Software Release Forms\" />

	<property name="rsl_addons.SeleniumTests.source" value="source\rsl_addons\rsl_addons.SeleniumTests" overwrite="true" />
	
	<property name="releaseForms.folder" value="VersionHistory" />
	<property name="releaseFile.source" value="Release_Version.template.htm" />
	<property name="releaseFile.target" value="Release_Version.htm" />

  <if test="${not property::exists('buildoutput')}">
    <fail message="Buildoutput property not specified!"></fail>
  </if>
  
	<!-- Test build for RSL manager -->
	<target name="connectionStrings" description="Copies the database connection strings over">

		<!-- RSL Addons Web.config files -->
		<copy file="${rsl_addons.deploy}\RSLApplications\web.template.config" tofile="${rsl_addons.deploy}\RSLApplications\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${rsl_addons.deploy}\RSLFaultLocator\web.template.config" tofile="${rsl_addons.deploy}\RSLFaultLocator\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>

		<!-- RSL master site Web.config files -->
		<copy file="${rsl_master_site.deploy}\global.template.asa" tofile="${rsl_master_site.deploy}\RSLApplications\global.asa" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="SITE_NAME" value="${SITE_NAME}"></token>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${rsl_master_site.deploy}\live_connections\db_connection.template.asp" tofile="${rsl_master_site.deploy}\live_connections\db_connection.asp" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${rsl_master_site.deploy}\web.template.config" tofile="${rsl_master_site.deploy}\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		

	</target>

	<target name="connectionStrings.IIS7" description="Copies the database connection strings over">

		<!-- RSL Addons Web.config files -->
		<copy file="${rsl_addons.deploy}\RSLApplications\web.iis7.template.config" tofile="${rsl_addons.deploy}\RSLApplications\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${rsl_addons.deploy}\RSLFaultLocator\web.iis7.template.config" tofile="${rsl_addons.deploy}\RSLFaultLocator\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		
		<copy file="${rsl_addons.deploy}\RSLPlannedMaintenance\web.iis7.template.config" tofile="${rsl_addons.deploy}\RSLPlannedMaintenance\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${rsl_addons.deploy}\RSLArrearsManagement\web.iis7.template.config" tofile="${rsl_addons.deploy}\RSLArrearsManagement\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="SITE_NAME" value="${SITE_NAME}"></token>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>

		<!-- RSL master site Web.config files -->
		<copy file="${rsl_master_site.deploy}\web.iis7.template.config" tofile="${rsl_master_site.deploy}\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="SITE_NAME" value="${SITE_NAME}"></token>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${rsl_master_site.deploy}\global.template.asa" tofile="${rsl_master_site.deploy}\global.asa" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		<copy file="${rsl_master_site.deploy}\live_connections\db_connection.template.asp" tofile="${rsl_master_site.deploy}\live_connections\db_connection.asp" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>
		


	</target>
	
	<!--<target name="synchroniseDatabase" description="Synchronises whatever is in the scripts folder with the dev database">

		<echo message="Synchronising databases..." />
		<exec basedir="." 
			  program="${RedGateTools}\SQL Compare 8\sqlcompare.exe" 
			  commandline="/scripts1:${dbschema.buildstem}\${dbschema.source} /db2:${DB_NAME.source} /s2:${DB_HOST.source} /u2:${DB_USER.source} /p2:${DB_PWD.source} /sync /allowIdenticalDatabases"  
			  workingdir="." 
			  failonerror="true" />
	</target>-->
	
	<target name="connectionStringsTenantsOnline" description="Copies the database connection strings over">

		<!-- RSL Tenants Online Web.config files -->
		<copy file="${rsl_tenants_online.deploy}\web.iis7.template.config" tofile="${rsl_tenants_online.deploy}\web.config" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="DB_NAME" value="${DB_NAME.target}"></token>
					<token key="DB_HOST" value="${DB_HOST.target}"></token>
				</replacetokens>
			</filterchain>
		</copy>		

	</target>

	<target name="updateReleaseVersion">
		<property name="releaseVersionString" value="" />
		<loadfile file="${buildoutput}\${rsl_master_site.source}\RELEASE_VERSION.TXT" property="releaseVersionFile" />
		<property name="releaseVersionString" value="${string::trim(releaseVersionFile)}" />
		<echo message="${releaseVersionString}" />

		<copy file="${rsl_master_site.releaseForms.source}\${releaseVersionString}.pdf" todir="${rsl_master_site.deploy}\${releaseForms.folder}" overwrite="true" />

		<copy file="${rsl_master_site.deploy}\${releaseFile.source}" tofile="${rsl_master_site.deploy}\${releaseFile.target}" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="RELEASE_VERSION" value="${releaseVersionString}"></token>
					<token key="RELEASE_VERSION_FILE" value="${releaseForms.folder}\${releaseVersionString}.pdf"></token>
				</replacetokens>
			</filterchain>
		</copy>
	</target>
	
	<target name="updateReleaseVersionTenantsOnline">
		<property name="releaseVersionString" value="" />
		<loadfile file="${rsl_tenants_online.deploy}\RELEASE_VERSION.TXT" property="releaseVersionFile" />
		<property name="releaseVersionString" value="${string::trim(releaseVersionFile)}" />
		<echo message="${releaseVersionString}" />

		<copy file="${rsl_tenants_online.releaseForms.source}\${releaseVersionString}.pdf" todir="${rsl_tenants_online.deploy}\${releaseForms.folder}" overwrite="true" />

		<!--<copy file="${rsl_tenants_online.deploy}\${releaseFile.source}" tofile="${rsl_tenants_online.deploy}\${releaseFile.target}" overwrite="true">
			<filterchain>
				<replacetokens>
					<token key="RELEASE_VERSION" value="${releaseVersionString}"></token>
					<token key="RELEASE_VERSION_FILE" value="${releaseForms.folder}\${releaseVersionString}.pdf"></token>
				</replacetokens>
			</filterchain>
		</copy>-->
	</target>

	<target name="seleniumTests" description="Runs the selenium UI smoke tests">
		<!-- Use the correct file first dev/test-->
		<copy file="${rsl_addons.SeleniumTests.source}\rsl_addons.SeleniumTests\app.test.config" tofile="${rsl_addons.SeleniumTests.source}\rsl_addons.SeleniumTests\app.config" overwrite="true">
		</copy>

		<exec program="${nunit}\nunit-console.exe" failonerror="true"  >
			<arg value="${rsl_addons.SeleniumTests.source}\rsl_addons.SeleniumTests\bin\release\rsl_addons.SeleniumTests.dll" />
			<arg value="/xml=rsl_addons.SeleniumTests-Results.xml" />
		</exec>

	</target>

</project>
