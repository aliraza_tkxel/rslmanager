Imports System

Imports System.Web
Imports System.Net.Mail
Imports System.Web.Configuration
Imports System.Configuration
Imports System.Net.Mime
Imports System.Web.HttpContext
Imports System.IO
Imports System.Xml.Serialization

Public Class Utility

#Region "Functions"

#Region "Send email"
    ''This utitilty method reads the email configuration from web.config and send email to specified address
    Public Shared Function SendMail(ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String) As Boolean

        'send the email
        Dim config As Configuration = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath)
        Dim settings As System.Net.Configuration.MailSettingsSectionGroup = _
            CType(config.GetSectionGroup("system.net/mailSettings"),  _
            System.Net.Configuration.MailSettingsSectionGroup)

        Dim message As MailMessage = New MailMessage()
        message.From = New MailAddress(settings.Smtp.From)
        message.To.Add(New MailAddress(strTo))
        message.Subject = strSubject

        'AttachImageToEmail(message, "images/broadloand_email_logo.gif", strBody)
        message.Attachments.Add(New Attachment(Current.Server.MapPath("images/broadloand_email_logo.GIF")))
        message.Attachments.Add(New Attachment(Current.Server.MapPath("images/broadlandLogo.jpg")))

        strBody = strBody.Replace("#IMAGE_HEADING#", "<img src='cid:" & message.Attachments(0).ContentId & "' /><br>")
        strBody = strBody.Replace("#IMAGE_LOGO#", "<img src='cid:" & message.Attachments(1).ContentId & "' /><br>")

        message.IsBodyHtml = True
        message.Body = strBody

        Dim client As SmtpClient = New SmtpClient()
        client.Send(message)
        Return True

    End Function

#Region "Attach email files"

    Private Shared Sub AttachImageToEmail(ByRef message As MailMessage, ByVal imagePath As String, ByRef mailContents As String)
        Dim pathTenantOnline As String = Current.Server.MapPath(imagePath)
    End Sub

#End Region

#Region "ReadTextFile"
    Public Shared Function GetFileContents(ByVal FullPath As String) As String

        Dim strContents As String = ""
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath)
            strContents = objReader.ReadToEnd()
            objReader.Close()
            Return strContents
        Catch Ex As Exception

        End Try
        Return strContents

    End Function


#End Region

#End Region

#Region "Conversion functions"

    ''This utility method converts the given string value into DateTime
    Public Shared Function StringToDate(ByVal str As String) As DateTime

        Dim MyDateTime As DateTime = New DateTime()
        Return DateTime.ParseExact(str, "dd/MM/yyyy", Nothing)

        Return Nothing

    End Function

    ''this utility function appends ENQ0000 with equiryID
    Public Shared Function GetEnquiryString(ByVal enquiryID As String) As String
        Dim tempEnqry As String = enquiryID.ToString()
        Dim i As Integer
        Dim tmpStr As String = String.Empty
        For i = 0 To 5 - tempEnqry.Length
            tmpStr = tmpStr & "0"
        Next i
        Return "ENQ" & tmpStr & tempEnqry
    End Function

    'this utility function joins firstname and lastname
    Public Shared Function FormatName(ByVal firstName As String, ByVal lastName As String) As String
        Return firstName(0) & " " & lastName
    End Function

    'this utility function joins firstname and lastname
    Public Shared Function GetFullName(ByVal firstName As String, ByVal lastName As String) As String
        Return firstName & " " & lastName
    End Function

    'this utility functions converts date from mm/dd/yyyy formate to dd/mm/yyyy
    Public Shared Function FormatDate(ByVal str As String) As String
        Dim strArr As String() = str.Split(" ")
        Return Convert.ToDateTime(strArr(0)).ToString("dd/MM/yyyy")
    End Function

    'this utility functions converts Duedate from mm/dd/yyyy formate to dd/mm/yyyy
    Public Shared Function FormatDueDate(ByVal str As String) As String
        If Not str.Equals("") And Not (str Is Nothing) Then
            Dim strArr As String() = str.Split(" ")
            Return Convert.ToDateTime(strArr(0)).ToString("dd/MM/yyyy")
        Else
            Return ("TBA")
        End If
    End Function

    Public Function ConvertToValue(ByVal value As Integer) As String

        Dim result As String = String.Empty

        If value <> 0 Then
            result = value.ToString
        End If

        Return result

    End Function

    Public Function ConvertToValue(ByVal value As Nullable(Of Integer)) As String

        Dim result As String = String.Empty

        If value IsNot Nothing AndAlso value.HasValue = True Then
            result = value.Value.ToString
        End If

        Return result

    End Function

    Public Function ConvertToValue(ByVal value As String) As String

        Dim result As String = String.Empty

        If String.IsNullOrEmpty(value) = False Then
            result = value
        End If

        Return result

    End Function

    Public Function ConvertToNullableInt(ByVal value As Object) As Nullable(Of Integer)

        Dim result As New Nullable(Of Integer)

        If value IsNot Nothing Then

            result = Convert.ToInt32(value)

            If result <= 0 Then
                result = New Nullable(Of Integer)
            End If

        End If

        Return result

    End Function

    Public Function ConvertToNullableShort(ByVal value As Object) As Nullable(Of Short)

        Dim result As New Nullable(Of Short)

        If value IsNot Nothing Then

            result = Convert.ToInt16(value)

            If result <= 0 Then
                result = New Nullable(Of Short)
            End If

        End If

        Return result

    End Function

    Public Function ConvertToNullableString(ByVal value As String) As String

        Dim result As String = Nothing

        If String.IsNullOrWhiteSpace(value) = False Then
            result = value
        End If

        Return result

    End Function

#End Region

#Region "HTMLtoPlainText"

    ''This utility method removes HTML tags from a given string and convert it into a plain text string
    Public Shared Function StripHTML(ByVal source As String) As String
        Try

            Dim result As String

            '' Remove HTML Development formatting
            '' Replace line breaks with space
            '' because browsers inserts space
            result = source.Replace("\r", " ")
            '' Replace line breaks with space
            '' because browsers inserts space
            result = result.Replace("\n", " ")
            '' Remove step-formatting
            result = result.Replace("\t", String.Empty)
            '' Remove repeating spaces because browsers ignore them
            result = System.Text.RegularExpressions.Regex.Replace(result, "( )+", " ")

            '' Remove the header (prepare first by clearing attributes)
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*head([^>])*>", "<head>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "(<( )*(/)( )*head( )*>)", "</head>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "(<head>).*(</head>)", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            '' remove all scripts (prepare first by clearing attributes)
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*script([^>])*>", "<script>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "(<( )*(/)( )*script( )*>)", "</script>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            ''result = System.Text.RegularExpressions.Regex.Replace(result,
            ''         @"(<script>)([^(<script>\.</script>)])*(</script>)",
            ''         string.Empty,
            ''         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            result = System.Text.RegularExpressions.Regex.Replace(result, "(<script>).*(</script>)", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            '' remove all styles (prepare first by clearing attributes)
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*style([^>])*>", "<style>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "(<( )*(/)( )*style( )*>)", "</style>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "(<style>).*(</style>)", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            '' insert tabs in spaces of <td> tags
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*td([^>])*>", "\t", System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            '' insert line breaks in places of <BR> and <LI> tags
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*br( )*>", "\r", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*li( )*>", "\r", System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            '' insert line paragraphs (double line breaks) in place
            '' if <P>, <DIV> and <TR> tags
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*div([^>])*>", "\r\r", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*tr([^>])*>", "\r\r", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "<( )*p([^>])*>", "\r\r", System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            '' Remove remaining tags like <a>, links, images,
            '' comments etc - anything that's enclosed inside < >
            result = System.Text.RegularExpressions.Regex.Replace(result, "<[^>]*>", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            '' replace special characters:
            result = System.Text.RegularExpressions.Regex.Replace(result, " ", " ", System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            result = System.Text.RegularExpressions.Regex.Replace(result, "&bull;", " * ", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "&lsaquo;", "<", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "&rsaquo;", ">", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "&trade;", "(tm)", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "&frasl;", "/", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "&lt;", "<", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "&gt;", ">", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "&copy;", "(c)", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "&reg;", "(r)", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            '' Remove all others. More can be added, see

            result = System.Text.RegularExpressions.Regex.Replace(result, "&(.{2,6});", String.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase)

            '' for testing
            ''System.Text.RegularExpressions.Regex.Replace(result,
            ''       this.txtRegex.Text,string.Empty, 
            ''       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            '' make line breaking consistent
            result = result.Replace("\n", "\r")

            '' Remove extra line breaks and tabs:
            '' replace over 2 breaks with 2 and over 4 tabs with 4.
            '' Prepare first to remove any whitespaces in between
            '' the escaped characters and remove redundant tabs in between line breaks
            result = System.Text.RegularExpressions.Regex.Replace(result, "(\r)( )+(\r)", "\r\r", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "(\t)( )+(\t)", "\t\t", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "(\t)( )+(\r)", "\t\r", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            result = System.Text.RegularExpressions.Regex.Replace(result, "(\r)( )+(\t)", "\r\t", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            '' Remove redundant tabs
            result = System.Text.RegularExpressions.Regex.Replace(result, "(\r)(\t)+(\r)", "\r\r", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            '' Remove multiple tabs following a line break with just one tab
            result = System.Text.RegularExpressions.Regex.Replace(result, "(\r)(\t)+", "\r\t", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            '' Initial replacement target string for line breaks

            Dim breaks As String = "\r"
            '' Initial replacement target string for tabs
            Dim tabs As String = "\t"
            Dim index As Integer

            For index = 0 To result.Length Step 1

                result = result.Replace(breaks, " ")
                result = result.Replace(tabs, " ")

            Next


            ''That's it.
            Return result
        Catch ex As Exception

            Return source

        End Try

    End Function

#End Region

#Region "ValidateSession"
    'Private Function AuthenticateUser(ByRef objSession As System.Web.Session) As Boolean

    'End Function
    'Private Function AuthenticateUser() As Boolean

    'End Function
#End Region

#End Region

End Class
