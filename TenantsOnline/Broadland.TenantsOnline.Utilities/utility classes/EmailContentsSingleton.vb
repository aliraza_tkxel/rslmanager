''This is a singleton class which loads the contents of the 
''Broadland password assistance email template contents
Imports System
Imports Broadland.TenantsOnline.Utilities
Imports System.Web.HttpContext

Public Class EmailContentsSingleton
    Private strContents As String
    Private Shared objSingleton As EmailContentsSingleton = Nothing

    Private Sub New()

    End Sub

    Private Shared Function getInstance() As EmailContentsSingleton
        If objSingleton Is Nothing Then
            objSingleton = New EmailContentsSingleton
            ''Read the email contents from the email contents text file
            Dim strEmailFile As String = ApplicationConstants.EmailTemplateFileName
            objSingleton.strContents = Utility.GetFileContents(Current.Server.MapPath(strEmailFile))
            Return objSingleton
        Else
            Return objSingleton
        End If

    End Function

    Public Shared Function GetEmailTemplateContents() As String
        Dim objMe As EmailContentsSingleton = getInstance()
        Return objMe.strContents
    End Function

End Class
