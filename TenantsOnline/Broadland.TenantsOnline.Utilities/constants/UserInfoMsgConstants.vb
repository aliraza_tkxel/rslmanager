Public Class UserInfoMsgConstants

    ''Customer registration page messages
    Public Shared CustomerRegistrationInvalidInformation As String = "Invalid information provided"
    Public Shared CustomerRegistrationUserAlreadyExists As String = "You have already registered"
    Public Shared CustomerRegistrationInvalidDateOfBirth As String = "Invalid date of birth"

    ''Password Assistance page messages
    Public Shared PasswordAssistanceIncorrectEmail As String = "Provided email address cannot be found within our records "
    Public Shared PasswordAssistanceEmailSent As String = "An email has been sent to "
    Public Shared PasswordAssistanceEmailSendErrorMsg As String = "An error occured while sending email to "

    ''Welcome page messages
    Public Shared WelcomeCustomerAccountBlancePrefixMsg As String = "My Account Balance is £{0} in {1}"
    Public Shared WelcomeCustomerRentBlancePrefixMsg As String = "£{0} in {1}"
    Public Shared TextCredit As String = "credit"
    Public Shared TextArrears As String = "arrears"

    Public Shared WelcomeCustomerGasServiceExistMsg As String = "My Gas Servicing appointment is on "
    Public Shared WelcomeCustomerGasServiceNotExistMsg As String = "No Gas Servicing appointment exists"

    Public Shared NoMyAppointmentsExistMsg As String = "No Appointments exist"

    ''No previous Asb
    Public Shared NoPreviousAsbExists As String = "No previous ASB exists"

    Public Shared HousingAssociationName As String = "Broadland Housing Association"
    Public Shared HousingAssociationAddressTel As String = "0845 331 2323"

    ''Login page message
    Public Shared LoginIncorrectMsg As String = "Incorrect login information provided"

    '' Change email message
    Public Shared IncorrectEmailMsg As String = "Incorrect information provided"

    '' How much rent do i owe/view my statement page
    Public Shared RecordNotExist As String = "No record exists"

    '' Terminate Tenancy page
    Public Shared IncorrectMoveOutDateFormat As String = "Incorrect move out date provided"
    Public Shared RequestSendingFailed As String = "Request sending failed"
    Public Shared DataSaveFailed As String = "Data save failed"
    Public Shared IncorrectMoveOutDate As String = "Invalid date: Date should be ahead of current date"

    '' Message when Enquiry gets closed or deleted
    Public Shared CloseEnquiryMsg As String = "Enquiry Closed: If you would like further information please call " & UserInfoMsgConstants.HousingAssociationAddressTel & " to speak to a member of our customer services team"

    ''Message when Fault Saving Failed
    Public Shared FaultSavingFailed As String = "Fault saving has been failed"

    ''Message when Fault Deletion Successful
    Public Shared FaultDeletionSuccess As String = "Fault has been deleted successfully"

    ''Message when Fault Deletion unSuccessful
    Public Shared FaultDeletionFailure As String = "Unable to delete the fault. Try again"

    ''Message when Fault Deletion Successful
    Public Shared WithNoFaultUpdateError As String = "You should add some faults to update"

    ''Message when Fault Updation Successful
    Public Shared FaultUpdationSuccess As String = "Fault quantity has been updated successfully"

    ''Message when Fault Deletion unSuccessful
    Public Shared FaultSubmissionFailureWithNoFaults As String = "You should add some faults to submit"

End Class

