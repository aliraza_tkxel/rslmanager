Imports System


Namespace Broadland.TenantsOnline.Utilities

    Public Class SprocNameConstants

        ' Executed for Signin usecase
        Public Shared AuthenticateCustomer As String = "TO_C_CUSTOMER_CUSTOMERLOGINAUTHENTICATE"

        ' If Signin successful, gets executed to fill session info.i.e. CustomerHeaderBO
        Public Shared SelectCustomerHeaderBO As String = "TO_C_CUSTOMER_SELECTCUSTOMERHEADER"

        ' Executed for Pasword Assistance scenario
        Public Shared PasswordAssistance As String = "TO_LOGIN_PASSWORDASSISTANCE"

        ' Executed for Email (username) Change scenario
        Public Shared ChangeEmail As String = "TO_C_CUSTOMER_EMAILCHANGE"

        ' Executed for Password Change scenario
        Public Shared ChangePassword As String = "TO_C_CUSTOMER_PasswordChange"

        '  Executed to obtain account\rent balance showed on "Home page", "How much rent do I owe" and 
        REM on "Help me manage my arrears" 
        Public Shared CalculateAccountBalance As String = "TO_F_RENTJOURNAL_CALCULATEACCOUNTBALANCE"

        '  Executed to obtain arrears action status and notes/comments entered by CRM, displayed on 
        REM "Help me manage my arrears"
        Public Shared ManageArrears As String = "TO_C_ARREARS_MANAGEARREARS"

        ' Executed for Customer Registration usecase, if registration successfull,
        REM insert value in TO_LOGIN table

        Public Shared RegisterCustomer As String = "TO_CUSTOMER_REGISTRATION"

        Public Shared LogSessionStartDate As String = "TO_LOGSESSION_DATETIME"
        Public Shared LogSessionEndDate As String = "TO_LOGSESSION_END_DATETIME"

        ' Executed for Retrieving data for welcome screen

        Public Shared GetCustomerInfo As String = "TO_CUSTOMER_GETCUSTOMERINFO"
        Public Shared GetCustomerAccountBalance As String = "TO_F_RENTJOURNAL_CALCULATEACCOUNTBALANCE"
        Public Shared GetServicingAppointmentDate As String = "TO_CUSTOMER_GASSERVICINGAPPOINTMENTDATE"
        Public Shared GetCustomerVulnerability As String = "TO_CUSTOMER_VULNERABILITY"

        ' Disability, Communication Info for My Details Page
        Public Shared GetDisabilityInfo As String = "TO_CUSTOMER_GETDISABILITYINFO"
        Public Shared GetCommunicationInfo As String = "TO_CUSTOMER_GETCOMMUNICATIONINFO"
        Public Shared UpdateCustomerDetails As String = "TO_CUSTOMER_UPDATEDETAILS"

        'Rent statement 
        Public Shared GetRentStatement As String = "F_GET_CUSTOMER_ACCOUNT"

        Public Shared GetRentStatementForPeriod As String = "TO_CUSTOMER_GETRENTSTATEMENT"

        'Public Shared GetRentStatementForAll As String = "F_GET_CUSTOMER_ACCOUNT"
        Public Shared GetRentStatementForAll As String = "F_GET_CUSTOMER_ACCOUNT_DATE_ASC"

        Public Shared GetSingleCustomerHB As String = "ARREARS_GET_SINGLE_CUSTOMER_HB"

        '******************************************************************************************
        'Public Shared GetPeriodRentStatement As String = "TO_F_RENTJOURNAL_PERIOD_ACCOUNT_BALANCE"
        ' It was decided that Rent Balance would always be of total transactions irrespective of any period filters
        Public Shared GetPeriodRentStatement As String = "TO_F_RENTJOURNAL_CALCULATEACCOUNTBALANCE"
        '******************************************************************************************

        'Rent Recharge statement 
        Public Shared GetRentRechargeStatement As String = "F_GET_CUSTOMER_RechargeACCOUNT"

        Public Shared GetRentRechargeStatementForPeriod As String = "TO_CUSTOMER_GETRENTRechargeSTATEMENT"

        Public Shared GetRentRechargeStatementForAll As String = "F_GET_CUSTOMER_RechargeACCOUNT_DATE_ASC"

        ''  ???? '''' Public Shared GetSingleCustomerHB As String = "ARREARS_GET_SINGLE_CUSTOMER_HB"

        Public Shared GetPeriodRentRechargeStatement As String = "TO_F_RENTJOURNAL_CALCULATERechargeACCOUNTBALANCE"


        'Tenancy termination
        Public Shared AddTermination As String = "TO_TEMINATION_AddTermination"

        'Broadland Complaint
        Public Shared AddComplaint As String = "TO_COMPLAINT_AddComplaint"

        'Broadland request_garage_parking
        Public Shared AddGarageParkingRequest As String = "TO_PARKING_SPACE_AddRequest"

        'Broadland report_asb
        Public Shared ReportASB As String = "TO_ASB_AddASB"

        'Broadland GetPrevious_asb
        Public Shared GetPreviousASB As String = "TO_ASB_GetPreviousASB"

        'Broadland GetPreviousComplaints
        Public Shared GetPreviousComplaints As String = "TO_COMPLAINT_GetPreviousComplaints"

        'Broadland report_abandoned
        Public Shared ReportAbandonedRequest As String = "TO_ABANDONED_AddAbandonRequest"

        'Broadland report_abandoned
        Public Shared ReportHouseMove As String = "TO_HOUSE_MOVE_AddRequest"

        'Broadland Broadland News
        Public Shared GetBroadlandNews As String = "TO_I_NEWS_GetNews"

        'Broadland Broadland latest News
        Public Shared GetBroadlandLatestNews As String = "TO_I_NEWS_GetLatestNews"

#Region "EnquiryLogStoredProcedures"

        'Customer ENQUIRYLOG 
        Public Shared GetRowCount As String = "TO_GETROWCOUNT"
        Public Shared EnquiryLogSelectAllList As String = "TO_ENQUIRY_LOG_SELECTLIST"
        Public Shared EnquiryLogSearch As String = "TO_ENQUIRY_LOG_SEARCHENQUIRY"
        Public Shared GetCustomerAddressTermination As String = "TO_TERMINATION_C_ADDRESS_VIEWTERMINATION"
        Public Shared GetCustomerAddressComplaint As String = "TO_COMPLAINT_ENQUIRYLOG_VIEWCOMPLAINT"
        Public Shared GetCustomerAddressASB As String = "TO_ASB_ENQUIRYLOG_VIEWASB"
        Public Shared GetCustomerAddressGarageParking As String = "TO_GARAGEPARKING_ENQUIRYLOG_REQUESTGARAGEPARKING"
        Public Shared GetCustomerAddressAbandon As String = "TO_ABANDON_ENQUIRYLOG_REPORTABANDON"
        Public Shared GetCustomerAddressTransfer As String = "TO_HOUSE_MOVE_ENQUIRYLOG_REQUESTTRANSFER"
        Public Shared DeleteCustomerEnquiry As String = "TO_ENQUIRY_LOG_DELETE"
        Public Shared GetSearchRowCount As String = "TO_ENQUIRY_LOG_COUNTSEARCHRESULTS"
        Public Shared GetCustomerIDByEnquiryLog As String = "TO_ENQUIRY_LOG_GET_CUSTOMERID"
        Public Shared GetCustomerNature As String = "TO_ENQUIRY_LOG_GET_C_REPORT_NATURE"
        Public Shared GetCustomerStatus As String = "TO_ENQUIRY_LOG_GET_C_REPORT_STATUS"

        Public Shared GetClearArrearInfo As String = "TO_COMPLAINT_C_ADDRESS_GET_CLEARARREARS"
        Public Shared GetDirectDebitRentInfo As String = "TO_COMPLAINT_C_ADDRESS_GET_DIRECTDEBITRENT"
        Public Shared GetCstRequestInfo As String = "TO_CST_REQUEST_ENQUIRYLOG_GETCSTREQUEST"

        'Constants for message alert system, Number of request
        Public Shared GetCustomerEnquiryCount As String = "TO_ENQUIRY_LOG_GetCustomersEqnuiriesCount"
        'Constants for message alert system, Number of responses
        Public Shared GetEnquiryResponseCount As String = "TO_ENQUIRY_LOG_GetEqnuiryResponseCount"
        'Constants for message alert system, Number of Terminations
        Public Shared GetTerminationCount As String = "TO_ENQUIRY_LOG_GetTerminationCount"
        'Constants for message alert system, Date of Current Termination
        Public Shared GetTerminationDate As String = "TO_ENQUIRY_LOG_GetTerminationDate"

        'Constants for message alert system,enquires customer made
        Public Shared GetCustomerEnquiries As String = "TO_ENQUIRY_LOG_GetCustomersEqnuiries"

        'Constants for message alert system,responses to customer enquiries
        Public Shared GetEnquiryResponse As String = "TO_ENQUIRY_LOG_GetEqnuiriesResponses"

        'Constants for message alert system,responses to customer enquiry response
        Public Shared AddCutomerResponse As String = "TO_ENQUIRY_LOG_AddCustomerResponse"

#End Region

#Region "NavigationAreaScreens"

        'Broadland Broadland latest News
        Public Shared AddClearArrears As String = "TO_ENQUIRY_LOG_AddClearArrears"

        'Broadland Broadland latest News
        Public Shared AddDirectDebit As String = "TO_ENQUIRY_LOG_AddDirectDebit"

        'Broadland Broadland Customer Service Request
        Public Shared AddCustomerServiceRequest As String = "TO_CST_REQUEST_AddRequest"

#End Region

#Region "LookUpStoredProcedures"

        Public Shared getComplaintCategory As String = "TO_C_SERVICECOMPLAINT_CATEGORY_GetCategory"
        Public Shared getGarageParkingLookup As String = "TO_LOOKUP_VALUE_GetParkingLookUpValue"
        Public Shared getAsbCategories As String = "TO_C_ASB_CATEGORY_GetAsbCategories"
        Public Shared getAbandonedPropertyVehicle As String = "TO_LOOKUP_VALUE_GetAbandonLookUpValue"
        Public Shared getEconomicStatus As String = "TO_C_ECONOMICSTATUS_GetLookup"
        Public Shared getEmploymentStatusLookup As String = "TO_C_EMPLOYMENTSTATUS_GetLookup"
        Public Shared getGenderLookup As String = "TO_C_GENDER_GetLookup"
        Public Shared getBenifitLookup As String = "TO_E_BENTYPE_GetBenifitLookup"
        Public Shared getMaritalStatusLookup As String = "TO_G_MARITALSTATUS_GetMaritalStatusLookup"
        Public Shared getEthnicityLookup As String = "TO_G_ETHNICITY_GetEthnicityLookup"
        Public Shared getReligionLookup As String = "TO_G_RELIGION_GetReligionLookup"
        Public Shared getSexualOrientationLookup As String = "TO_G_SEXUALORIENTATION_GetSexualOrientationLookup"
        Public Shared getCommunicationLookup As String = "TO_G_COMMUNICATION_GetLookup"
        Public Shared getTitleLookup As String = "TO_G_TITLE_GetTitleLookup"
        Public Shared getPreferredContactLookup As String = "TO_G_PREFEREDCONTACT_GetContactLookup"
        Public Shared getNatureLookup As String = "TO_C_NATURE_GetNatureLookup"
        Public Shared getStatusLookup As String = "TO_C_STATUS_GetStatusLookup"
        Public Shared getLocalAuthorityLookup As String = "TO_G_LOCALAUTHORITY_GetLookUpValue"
        Public Shared getDevelopmentLookup As String = "TO_P_DEVELOPMENT_GetLookUpValue"
        Public Shared getCstRequestReasonLookup As String = "TO_C_NATURE_GetCstRequestReasonLookup"
        Public Shared getReportedFaultStatusLookup As String = "FL_GETFAULTSTATUS"
        Public Shared getFaultStatusLookupForFaultLog As String = "FL_GET_FAULT_STATUS_FOR_FAULT_LOG"
        Public Shared getNationalityLookUp As String = "TO_G_NATIONALITY_GetLookup"
        Public Shared getYesNo As String = "TO_G_YESNO_GetLookup"
        Public Shared getRentstatementTypeLookup As String = "TO_C_RentStatementType_GetLookup"
        Public Shared getDisabilityLookup As String = "TO_G_DISABILITY_GetLookup"
        Public Shared getSupportAgencyLookup As String = "TO_G_SUPPORTAGENCIES_GetLookup"
        Public Shared getInternetAccessLookup As String = "TO_G_INTERNETACCESS_GetLookup"
        Public Shared getBankFacilityLookup As String = "TO_G_BANKFACILITY_GetLookup"
        Public Shared getBenefitLookup As String = "TO_G_BENEFIT_GetLookup"
        Public Shared getHouseholdIncomeLookup As String = "TO_G_HOUSEINCOME_GetLookup"
        Public Shared getAppointments As String = "TO_GET_APPOINTMENTS"

#End Region

#Region "Customer enquiry response letters stored procedure constants"

        'Broadland arrears letter
        Public Shared GetArrearLetter As String = "TO_C_CUSTOMERLETTERS_GetArrearLetter"
        Public Shared GetAsbLetter As String = "TO_C_ASBLETTER_GetAsbLetter"
        Public Shared GetRentLetter As String = "TO_C_GENERALCUSTOMERLETTERS_GetRentLetter"
        Public Shared GetServiceComplaintsLetter As String = "TO_C_LETTERS_SAVED_GetServiceComplaintLetter"

#End Region

#Region "Fault Location Procedure Constants"

        Public Shared GetLocationName As String = "FL_GET_LOCATION_NAME"
        Public Shared GetElements As String = "FL_GET_ELEMENTS"
        Public Shared GetFaults As String = "FL_GET_FAULTS"
        Public Shared GetCustomerFaults As String = "FL_FAULT_LOG_GET_CUSTOMER_Faults"
        Public Shared GetFaultQuestion As String = "FL_GET_FAULT_QUESTION"
        Public Shared SaveFaultTemporarily As String = "FL_SAVE_FAULT_TEMPORARILY"
        Public Shared TempFaultBasket As String = "FL_TEMP_FAULT_BASKET"
        Public Shared DeleteTempFault As String = "FL_DELETE_TEMP_FAULT"
        Public Shared saveFinalFaultBasket As String = "FL_SAVE_FINAL_FAULT_BASKET"
        Public Shared GetCustomerContactDetail As String = "FL_GET_CUSTOMER_CONTACT_DETAIL"
        Public Shared UpdateCustomerContactDetail As String = "FL_UPDATE_CUSTOMER_CONTACT_DETAIL"
        Public Shared UpdateFaultQuantity As String = "FL_UPDATE_FAULT_QUANTITY"
        Public Shared GetCustomerUnreadFaultsCount As String = "FL_FAULT_GetCustomerUnreadFaultsCount"
        Public Shared SaveSQuestions As String = "FL_FAULT_SAVESQUESTIONS"
        Public Shared LoadBreadCrumb As String = "FL_LOAD_BREADCRUMB"

#End Region

#Region "Paythru"

        Public Shared GetPaythruConfigValue As String = "F_PAYTHRU_GET_CONFIG"

#End Region

#Region "Fault LookUp"
        'Broadland fault status lookup stored procedure
        Public Shared getFaultStatusLookup As String = "FL_STATUS_GetFaultStatusLookup"
#End Region



    End Class

End Namespace

