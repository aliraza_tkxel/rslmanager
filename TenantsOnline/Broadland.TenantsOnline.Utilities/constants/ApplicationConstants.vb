Public Class ApplicationConstants

    '' constant for customer enquiry management
    '' number of rows to be fetched from db at one time before next qury
    Public Shared CustEnquiryManagRowCount As Integer = 10
    ''SortBy column 
    Public Shared CustEnquiryManagSortBy As String = "EnquiryLogID"
    ''SortOrder
    Public Shared CustEnquiryManagDESCSortOrder As String = "DESC"
    Public Shared CustEnquiryManagASECSortOrder As String = "ASC"
    '' mod number
    Public Shared CustEnquiryManagModNumber As Integer = 1
    ''constant for initial page index
    Public Shared CustEnquiryInitialPageIndex As Integer = 0
    '' results per page
    Public Shared CustEnquiryManagResultPerPage As Integer = 10
    ''style options for panel holding popover
    Public Shared CustEnquiryShowPanelStyleValue As String = "background-color:White;display:block"
    Public Shared CustEnquiryHidePanelStyleValue As String = "background-color:White;display:none"
    ''viewstate variable names in customer enquiry management
    ''ket name for dataset in viewstate
    Public Shared CustEnquiryDataSetViewState As String = "enquiryDS"
    ''key name for searchoptions in viewstate
    Public Shared CustEnquirySearchOptionsViewState As String = "SearchOptions"
    ''key name for sortby in viewstate
    Public Shared CustEnquirySortByViewState As String = "SortBy"
    ''key name for sortorder in viewstate
    Public Shared CustEnquirySortOrderViewSate As String = "SortOrder"
    ''key holding gridview data state
    Public Shared CustEnquiryIsSearchViewState As String = "IsSearch"
    ''key holding summary of results
    Public Shared CustEnquiryResultSummary As String = "ResultSummary"

    ''strings for exporting dataset to excel
    ''constant for default file name
    Public Shared CustEnquiryAttachment As String = "attachment; filename=EnquirLog.xls"
    ''constant for ContentType which in this case is excel
    Public Shared CustEnquiryContentType As String = "application/vnd.ms-excel"
    ''constant for HTTP Request header
    Public Shared CustEnquiryHTTPHeader As String = "content-disposition"


    ''constant email template file name
    Public Shared EmailTemplateFileName As String = "ForgotPasswordEmail.html"


    ''strings for different types of complaints/reports/request
    Public Shared CustEnquiryTerminationRequest As String = "Termination (TO)"
    Public Shared CustEnquiryServiceComplaint As String = "Service Complaints (TO)"
    Public Shared CustEnquiryGarageParkingRequest As String = "Garage/Car Parking (TO)"
    Public Shared CustEnquiryASBComplaint As String = "ASB - Complainant (TO)"
    Public Shared CustEnquiryAbandonedReport As String = "Abandoned Property/Vehicle (TO)"
    Public Shared CustEnquiryTransferRequest As String = "House Move (TO)"
    Public Shared CustEnquiryClearArrearsRequest As String = "Arrears (TO)"
    Public Shared CustEnquiryGeneralRequest As String = "General"
    Public Shared CustEnquiryDirectDebitRentRequest As String = "Rent"
    Public Shared CustEnquiryCstRequest As String = "CST Request (TO)"
    Public Shared CustomerEnquiryStatusNew As String = "26"
    Public Shared CustomerEnquiryStatusDeleted As String = "28"
   


End Class
