Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.DataAccess

Namespace Broadland.TenantsOnline.BusinessLogic

    Public Class EnquiryManager

#Region "Functions"

#Region "sendTerminationRequest"

        Public Function SendTerminationRequest(ByRef termBO As TerminationBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As EnquiryDAL = New EnquiryDAL()

            ''Call DAL method to varify provided information
            objEnqDAL.SendTerminationRequest(termBO)

            ''Check success scenario
            If termBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function
#End Region

#Region "SendCstRequest"

        Public Function SendCstRequest(ByRef cstReqBO As CstBO) As Boolean
            ''Instantiate DAL object
            Dim objEnqDAL As EnquiryDAL = New EnquiryDAL()

            ''Call DAL method to varify provided information
            objEnqDAL.SendCstRequest(cstReqBO)

            ''Check success scenario
            If cstReqBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function

#End Region

#Region "SendCopmalaintRequest"
        Public Function SendComplaint(ByRef compBO As ComplaintBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As EnquiryDAL = New EnquiryDAL()

            ''Call DAL method to varify provided information
            objEnqDAL.SendComplaint(compBO)

            ''Check success scenario
            If compBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function
#End Region

#Region "SendGaragePrakingRequest"
        Public Function SendParkingRequest(ByRef parkBO As GarageParkingBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As EnquiryDAL = New EnquiryDAL()

            ''Call DAL method to varify provided information
            objEnqDAL.SendParkingRequest(parkBO)

            ''Check success scenario
            If parkBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function
#End Region

#Region "ReportASB"

        Public Function ReportASB(ByRef objAsbBO As AsbBO) As Boolean

            Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
            enqryDAL.ReportASB(objAsbBO)

            ''Note: we'll check whether exception is generated over here or not and log it
            ''Check success scenario
            If objAsbBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function


#End Region

#Region "GetPreviousASB"

        Public Function GetPreviousASB(ByRef objAsbBO As AsbBO) As AsbList
            Dim enqryDAL As EnquiryDAL = New EnquiryDAL()

            Dim lstAsbBO As AsbList

            lstAsbBO = enqryDAL.GetPreviousASB(objAsbBO)

            ''Note: we'll check whether exception is generated over here or not and log it
            ''Check success scenario
            If objAsbBO.IsFlagStatus Then
                Return lstAsbBO
            End If

            Return Nothing

        End Function


#End Region

#Region "GetPreviousComplaints"

        Public Function GetPreviousComplaints(ByRef objCompBO As ComplaintBO) As ComplaintList

            Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
            Dim lstCompBO As ComplaintList

            lstCompBO = enqryDAL.GetPreviousComplaints(objCompBO)

            ''Note: we'll check whether exception is generated over here or not and log it
            ''Check success scenario
            If objCompBO.IsFlagStatus Then
                Return lstCompBO
            End If

            Return Nothing

        End Function


#End Region

#Region "ReportAbandonedPropertyVehicle"
        Public Function reportAbandoned(ByRef objAbandBO As AbandonBO) As Boolean

            Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
            enqryDAL.reportAbandoned(objAbandBO)

            ''Note: we'll check whether exception is generated over here or not and log it
            ''Check success scenario
            If objAbandBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function
#End Region

#Region "SendHouseMoveRequest"

        Public Function SendHouseMoveRequest(ByRef moveHouseBO As HouseMoveBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As EnquiryDAL = New EnquiryDAL()

            ''Call DAL method to varify provided information
            objEnqDAL.SendHouseMoveRequest(moveHouseBO)

            ''Check success scenario
            If moveHouseBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function

#End Region

#Region "LookUpListsFunctions"
        Public Function getAsbCategory() As DataSet

            Return (Nothing)

        End Function

#End Region

#Region "SendClearArrearRequest"
        Public Function SendClearArrearRequest(ByRef enqBO As EnquiryLogBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As EnquiryDAL = New EnquiryDAL()

            ''Call DAL method to varify provided information
            objEnqDAL.SendClearArrearRequest(enqBO)

            ''Check success scenario
            If enqBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function
#End Region

#Region "SendDirectDebitRequest"
        Public Function SendDirectDebitRequest(ByRef enqBO As EnquiryLogBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As EnquiryDAL = New EnquiryDAL()

            ''Call DAL method to varify provided information
            objEnqDAL.SendDirectDebitRequest(enqBO)

            ''Check success scenario
            If enqBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function
#End Region


#Region "MessageAlertSystem"

#Region "GetCustomerEnquiryCount"

        '' Function gets called to retrieve the number of enquiries customer made
        Public Function GetCustomerEnquiryCount(ByRef enqLogBO As EnquiryLogBO) As Boolean

                ''Instantiate DAL object
                Dim objEnqDAL As New EnquiryDAL

                ''Call DAL method to retrieve information
                objEnqDAL.GetCustomerEnquiryCount(enqLogBO)

                If (enqLogBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
            End If

        End Function

#End Region

#Region "GetCustomerEnquiries"

        '' Function gets called to retrieve the enquiries customer made
        Public Function GetCustomerEnquiries(ByVal customerID As Integer, ByVal responseStatus As String) As DataSet

            ''Instantiate DAL object
            Dim objEnqDAL As New EnquiryDAL

            ''Call DAL method to retrieve information
            Dim dsEnquiries As DataSet = objEnqDAL.GetCustomerEnquiries(customerID, responseStatus)
            Return dsEnquiries

        End Function


#End Region

#Region "GetEnquiryResponses"

        '' Function gets called to retrieve the responses
        Public Function GetEnquiryResponses(ByRef enqLogBO As EnquiryLogBO) As JournalList
            ''Instantiate DAL object
            Dim objEnqDAL As New EnquiryDAL

            ''Call DAL method to retrieve information
            Dim lstJournal As JournalList = objEnqDAL.GetEnquiryResponses(enqLogBO)
            Return lstJournal

        End Function


#End Region

#Region "GetEnquiryResponseCount"


        '' Function gets called to retrieve the number of responses against enquiries customer made
        Public Function GetEnquiryResponseCount(ByRef enqLogBO As EnquiryLogBO) As Boolean

                ''Instantiate DAL object
                Dim objEnqDAL As New EnquiryDAL

                ''Call DAL method to retrieve information
                objEnqDAL.GetEnquiryResponseCount(enqLogBO)

                If (enqLogBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

        End Function

#End Region

#Region "GetTerminationCount"

        '' Function gets called to retrieve the number of responses against enquiries customer made
        Public Function GetTerminationCount(ByRef enqLogBO As EnquiryLogBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As New EnquiryDAL

                ''Call DAL method to retrieve information
                objEnqDAL.GetTerminationCount(enqLogBO)

                If (enqLogBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

        End Function

#End Region

#Region "GetTerminationDate"

        '' Function gets called to retrieve the number of responses against enquiries customer made
        Public Function GetTerminationDate(ByRef enqLogBO As EnquiryLogBO) As Boolean

            ''Instantiate DAL object
                Dim objEnqDAL As New EnquiryDAL

                ''Call DAL method to retrieve information
                objEnqDAL.GetTerminationDate(enqLogBO)

                If (enqLogBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

        End Function

#End Region

#Region "GetLetter"

        Public Function GetLetter(ByVal storedProcedure As String, ByRef objLetterBO As LetterBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As New EnquiryDAL
            ''Call DAL method to retrieve information
            objEnqDAL.GetLetter(storedProcedure, objLetterBO)

        End Function

#End Region

#Region "AddCustomerResponse"

        Public Function AddCustomerResponse(ByRef custResponseBO As CustomerResponseBO) As Boolean

            ''Instantiate DAL object
            Dim objEnqDAL As EnquiryDAL = New EnquiryDAL()

            ''Call DAL method to varify provided information
            objEnqDAL.AddCustomerResponse(custResponseBO)

            ''Check success scenario
            If custResponseBO.IsFlagStatus Then
                Return True
            End If

            Return False
        End Function

#End Region

#End Region

#End Region

#Region "Methods"

#Region "Enquiry Log"
        Public Sub EnquiryLogData(ByRef enquiryDS As DataSet, ByRef enqryLogSearchBO As EnquiryLogSearchBO)

                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
                enqryDAL.EnquiryLogData(enquiryDS, enqryLogSearchBO)

        End Sub

        Public Function GetEnquiryLogRowCount(ByRef enqryLogSearchBO As EnquiryLogSearchBO) As Integer
            Dim rowCount As Integer = 0

            Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
            rowCount = enqryDAL.GetEnquiryLogRowCount()

            Return rowCount
        End Function

        Public Function GetEnquiryLogSearchRowCount(ByRef enqryLogSearchBO As EnquiryLogSearchBO) As Integer
            Dim rowCount As Integer = 0
            Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
            rowCount = enqryDAL.GetEnquiryLogSearchRowCount(enqryLogSearchBO)

            Return rowCount
        End Function

        Public Sub DeleteCustomerEnquiry(ByRef termBO As TerminationBO)

            Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
            enqryDAL.DeleteCustomerEnquiry(termBO)

        End Sub

        Public Sub GetCustomerIDByEnquiryLog(ByRef enqryLog As EnquiryLogBO)

            Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
            enqryDAL.GetCustomerIDByEnquiryLog(enqryLog)

        End Sub
#End Region


#End Region

    End Class

End Namespace
