Imports System

Imports System.ComponentModel

#Region " Imports "

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.DataAccess

#End Region

Namespace Broadland.TenantsOnline.BusinessLogic

    Public Class CustomerManager

 

#Region "AuthenticateCustomer Method"

        '' Function gets called when user tries to SignIn
        Public Sub AuthenticateCustomer(ByRef custBO As CustomerHeaderBO)
            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.AuthenticateCustomer(custBO)
        End Sub

#End Region

#Region "LogSessionStart Method"

        '' Function gets called when user tries to SignIn
        Public Sub LogSessionStart(ByRef custBO As CustomerHeaderBO)

            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.LogSessionStart(custBO)

        End Sub

#End Region

#Region "LogSessionEnd Method"

        '' Function gets called when user tries to SignIn
        Public Sub LogSessionEnd(ByVal custSession As Integer)

            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.LogSessionEnd(custSession)

        End Sub

#End Region

#Region "PasswordAssitance Method"

        '' Function gets called when user forgot his/her password
        Public Sub PasswordAssitance(ByRef custBO As CustomerHeaderBO)

            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.CustomerPasswordAssitance(custBO)

        End Sub

#End Region

#Region "ChangeEmail Method"


        '' Function gets called when user attempts to change his/her email(userName)
        Public Sub ChangeEmail(ByRef custHeadBO As CustomerHeaderBO)
            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.ChangeEmail(custHeadBO)

        End Sub

#End Region

#Region "Password Method"


        '' Function gets called when user attempts to change his/her password
        Public Sub ChangePassword(ByRef custHeadBO As CustomerHeaderBO)
            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.ChangePassword(custHeadBO)

        End Sub

#End Region

#Region "ManageFinancialInfo Function"

        '' Function gets called when user attempts to change his/her email(userName)
        Public Sub ManageFinancialInfo(ByRef custFinBO As CustomerFinancialBO, ByRef custHeadBO As CustomerHeaderBO, ByVal accBalOnly As Boolean)

            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.ManageFinancialInfo(custFinBO, custHeadBO, False)

        End Sub


#End Region

#Region "RentStatement Methods"


        Public Sub RentStatement(ByRef rentDS As DataSet, ByVal custRentBO As CustomerRentBO)

            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.RentAccount(rentDS, custRentBO)

            '' If no rent/account statemnet found, not caluclating housing balance
            'If custRentBO.IsFlagStatus Then


                custDAL.GetCustomerBalances(custRentBO)
                '' call to Me.CalculateHousingBalance
                'Me.CalcualteHousingBalance(custRentBO)
                'custDAL.GetCustomerPeriodAccountBalance(custRentBO)
               ' Me.CalcualteAccountBalance(custRentBO)
'
            'End If

        End Sub

        '' This will calculate Housing balance
        Private Sub CalcualteHousingBalance(ByRef custRentBO As CustomerRentBO)

            '' If AntHB greater than 0, we set HousingBalance to it 
            '' Otherwise AdvHb willl be set to HousingBalance

            If custRentBO.AntHB > 0 Then

                custRentBO.HousingBalance = custRentBO.AntHB

            Else

                custRentBO.HousingBalance = custRentBO.AdvHB

            End If

        End Sub

        '' This will calculate Housing balance
        Private Sub CalcualteAccountBalance(ByRef custRentBO As CustomerRentBO)

            '' If AntHB greater than 0, we set AccountBalance to it 
            '' Otherwise AdvHb willl be set to AccountBalance

            If custRentBO.AntHB > 0 Then

                custRentBO.AccountBalance = custRentBO.AccountBalance - custRentBO.AntHB

            Else

                custRentBO.AccountBalance = custRentBO.AccountBalance + custRentBO.AdvHB

            End If

        End Sub


#End Region

#Region "RentRechargeStatement Methods"


        Public Sub RentRechargeStatement(ByRef rentDS As DataSet, ByVal custRentRechargeBO As CustomerRentRechargeBO)

            Dim custDAL As CustomerDAL = New CustomerDAL()
            custDAL.RentRechargeAccount(rentDS, custRentRechargeBO)

            '' If no rent/account statemnet found, not caluclating housing balance
            If custRentRechargeBO.IsFlagStatus Then

                '' call to Me.CalculateHousingBalance
                'Me.CalcualteHousingBalance(custRentRechargeBO)
                'custDAL.GetCustomerPeriodAccountBalance(custRentRechargeBO)
                'Me.CalcualteRechargeAccountBalance(custRentRechargeBO)

            End If

        End Sub


        '' This will calculate Housing balance
        Private Sub CalcualteRechargeAccountBalance(ByRef custRentRechargeBO As CustomerRentRechargeBO)

            '' If AntHB greater than 0, we set AccountBalance to it 
            '' Otherwise AdvHb willl be set to AccountBalance

            If custRentRechargeBO.AntHB > 0 Then

                custRentRechargeBO.AccountBalance = custRentRechargeBO.AccountBalance - custRentRechargeBO.AntHB

            Else

                custRentRechargeBO.AccountBalance = custRentRechargeBO.AccountBalance + custRentRechargeBO.AdvHB

            End If

        End Sub
     


#End Region

#Region "Functions"

#Region "CustomerRegistration"

        '' Function gets called when user needs to register
        Public Function CustomerRegistration(ByRef custBO As CustomerBO) As Boolean

            ''Instantiate DAL object
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

            ''Call DAL method to varify provided information
            objCustomerDAL.CustomerRegistration(custBO)

            ''Check success scenario
            If custBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function

#End Region

#Region "CustomerUpdateDetails"

        '' Function gets called when user needs to register
        Public Function UpdateCustomerDetails(ByRef custDetailsBO As CustomerDetailsBO, ByRef custHeadBO As CustomerHeaderBO) As Boolean

                ''Instantiate DAL object
                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

                ''Call DAL method to update customer information
                objCustomerDAL.CustomerUpdate(custDetailsBO, custHeadBO)

                ''Check success scenario
                If custDetailsBO.IsFlagStatus Then
                    Return True
                End If

            Return False

        End Function

#End Region

#Region "HomePageRelated"

        '' Function gets called to retrieve information to be displayed on home page
        Public Function GetCustomerInfo(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailsInfo As Boolean) As Boolean

            ''Instantiate DAL object
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

            ''Call DAL method to retrieve information
            objCustomerDAL.GetCustomerInfo(custDetailsBO, getDetailsInfo)

            If (custDetailsBO.IsFlagStatus) Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "GetCustomerDetails"

        '' Function gets called to retrieve information to be displayed on customer details page
        Public Function GetCustomerDetails(ByRef custDetailsBO As CustomerDetailsBO) As Boolean

            ''Instantiate DAL object
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

            ''Call DAL method to retrieve information
            objCustomerDAL.GetCustomerDetailsInfo(custDetailsBO, True)

            ' '' Call DAL method to retrieve Disability information (from comma seperated to rows)
            'objCustomerDAL.GetCustomerDisabilityInfo(custDetailsBO, True)

            ' '' Call DAL method to retrieve Communication information (from comma seperated to rows)
            'objCustomerDAL.GetCustomerCommuniucationInfo(custDetailsBO, True)


            If (custDetailsBO.IsFlagStatus) Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "GetTerminationInfo"

        Public Function GetTerminationInfo(ByRef custAddressBO As CustomerAddressBO, ByRef terminBO As TerminationBO) As Boolean
         
                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetTerminationInfo(custAddressBO, terminBO)

                If (terminBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

        End Function

#End Region

#Region "GetComplaintInfo"

        Public Function GetComplaintInfo(ByRef custAddressBO As CustomerAddressBO, ByRef cmpltBO As ComplaintBO) As Boolean

            ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetComplaintInfo(custAddressBO, cmpltBO)

                If (cmpltBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

        End Function

#End Region

#Region "GetASBInfo"

        Public Function GetASBInfo(ByRef custAddressBO As CustomerAddressBO, ByRef abBO As AsbBO) As Boolean
         
                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetASBInfo(custAddressBO, abBO)

                If (abBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

        End Function

#End Region

#Region "GetGarageParkingInfo"

        Public Function GetGarageParkingInfo(ByRef custAddressBO As CustomerAddressBO, ByRef gpBO As GarageParkingBO) As Boolean

            ''Instantiate DAL object
            Dim objCustDAL As CustomerDAL = New CustomerDAL

            ''Call DAL method to retrieve information
            objCustDAL.GetGarageParkingInfo(custAddressBO, gpBO)

            If (gpBO.IsFlagStatus) Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "GetAbandonInfo"
        Public Function GetAbandonInfo(ByRef custAddressBO As CustomerAddressBO, ByRef abndBO As AbandonBO) As Boolean

            ''Instantiate DAL object
            Dim objCustDAL As CustomerDAL = New CustomerDAL

            ''Call DAL method to retrieve information
            objCustDAL.GetAbandonInfo(custAddressBO, abndBO)

            If (abndBO.IsFlagStatus) Then
                Return True
            Else
                Return False
            End If

        End Function
#End Region

#Region "GetTransferInfoInfo"
        Public Function GetTransferInfo(ByRef custAddressBO As CustomerAddressBO, ByRef houseTransBO As HouseMoveBO) As Boolean

            ''Instantiate DAL object
            Dim objCustDAL As CustomerDAL = New CustomerDAL

            ''Call DAL method to retrieve information
            objCustDAL.GetTransferInfo(custAddressBO, houseTransBO)

            If (houseTransBO.IsFlagStatus) Then
                Return True
            Else
                Return False
            End If

        End Function
#End Region

#Region "GetClearDirectDebitRentInfo"
        Public Function GetClearDirectDebitRentInfo(ByRef custAddressBO As CustomerAddressBO, ByRef enqBO As EnquiryLogBO) As Boolean

            ''Instantiate DAL object
            Dim objCustDAL As CustomerDAL = New CustomerDAL

            ''Call DAL method to retrieve information
            objCustDAL.GetClearDirectDebitRentInfo(custAddressBO, enqBO)

            If (enqBO.IsFlagStatus) Then
                Return True
            Else
                Return False
            End If

        End Function
#End Region

#Region "GetCstRequestInfo"
        Public Function GetCstRequestInfo(ByRef custAddressBO As CustomerAddressBO, ByRef cstReqBO As CstBO) As Boolean

            ''Instantiate DAL object
            Dim objCustDAL As CustomerDAL = New CustomerDAL

            ''Call DAL method to retrieve information
            objCustDAL.GetCstRequestInfo(custAddressBO, cstReqBO)

            If (cstReqBO.IsFlagStatus) Then
                Return True
            Else
                Return False
            End If

        End Function
#End Region

#Region "GetClearArrearInfo"
        Public Function GetClearArrearInfo(ByRef custAddressBO As CustomerAddressBO, ByRef enqBO As EnquiryLogBO) As Boolean
          
                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetClearArrearInfo(custAddressBO, enqBO)

                If (enqBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

        End Function
#End Region

#Region "GetCustomerContactDetail For Fault Locator"

        '' Function gets called to retrieve information to be displayed on customer details page
        Public Function GetCustomerContactDetail(ByRef custDetailsBO As CustomerDetailsBO) As Boolean

                ''Instantiate DAL object
                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

                ''Call DAL method to retrieve information
                objCustomerDAL.GetCustomerContactDetail(custDetailsBO)

                If (custDetailsBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

        End Function

#End Region

#Region "SaveCustomerContactDetail"

        Public Function UpdateCustomerContactDetail(ByRef cusBO As CustomerBO)

            ''Instantiate DAL object
                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

                ''Call DAL method to retrieve information
                objCustomerDAL.UpdateCustomerContactDetail(cusBO)

                If (cusBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If

            Return Nothing
        End Function

#End Region

#End Region

    End Class

End Namespace
