Imports System

Imports System.ComponentModel

#Region " Imports "

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.DataAccess

#End Region

Namespace Broadland.TenantsOnline.BusinessLogic

    Public Class PaythruManager

#Region "Functions"

#Region "CustomerRegistration"

        '' Function gets called when user needs to register
        Public Function GetPaythruUrl() As String

            ''Instantiate DAL object
            Dim objPaythruDAL As PaythruDAL = New PaythruDAL()
            Dim objPaythruBO As PaythruBO = New PaythruBO

            objPaythruBO.ConfigSettingName = "PaythruUrl"

            ''Call DAL method to varify provided information
            objPaythruDAL.GetConfigValue(objPaythruBO)

            Return objPaythruBO.ConfigSettingValue

        End Function

#End Region

#End Region

    End Class

End Namespace
