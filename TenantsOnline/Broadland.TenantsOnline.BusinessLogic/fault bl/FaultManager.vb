﻿Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.DataAccess

'' Class Name -- Fault Manager
'' Created By  -- Noor Muhammad
'' Create on -- 13th Oct,2008

Namespace Broadland.TenantsOnline.BusinessLogic

    Public Class FaultManager

#Region "Functions"

#Region "GetLocationName"
        Function GetLocationName(ByRef locationBO As LocationBO)
            'Instanticale the DAL object
            Dim objFaultDAL As FaultDAL = New FaultDAL()

            'Call DAL method to get the location name against location id
            objFaultDAL.GetLocationName(locationBO)

            'Check the status
            If locationBO.IsFlagStatus Then
                Return True
            End If

            Return Nothing
        End Function

#End Region

#Region "GetCustomerFault"

        'Function gets called to retrieve the fault 
        Public Function GetCustomerFault(ByVal customerID As Integer, ByVal faultStatus As String) As DataSet

            ''Instantiate DAL object
            Dim faultDAL As FaultDAL = New FaultDAL()

            ''Call DAL method to retrieve information
            Dim dsFaults As DataSet = faultDAL.GetCustomerFaults(customerID, faultStatus)
            Return dsFaults

        End Function


#End Region

#Region "GetUnreadFaultCount"
        '' Function gets called to retrieve the number of unread faults
        Public Function GetUnreadFaultCount(ByRef faultLogBO As FaultLogBO, ByVal faultStatus As String) As Boolean

                ''Instantiate DAL object
                Dim faultDAL As FaultDAL = New FaultDAL()

                ''Call DAL method to retrieve information
                faultDAL.GetUnreadFaultCount(faultLogBO, faultStatus)
                If (faultLogBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
         
        End Function

#End Region

#Region "SaveSQuestions"

        '' Function gets called to retrieve the number of unread faults
        Public Sub SaveSQuestions(ByRef sQuestionBO As SQuestionBO, ByRef sAnswerBO As SQuestionBO, ByVal faultLogID As Integer)

            'Instantiate DAL object
            Dim faultDAL As FaultDAL = New FaultDAL()

            'Call DAL method to store information
            faultDAL.SaveSQuestions(sQuestionBO, sAnswerBO, faultLogID)

        End Sub

#End Region

#Region "GetElements"
        Public Function GetElements(ByRef elementBO As ElementBO, ByRef elementList As FaultElementList)
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetElements(elementBO, elementList)

            Return Nothing
        End Function
#End Region

#Region "GetFaults"
        Public Function GetFaults(ByRef faultBO As FaultBO, ByRef faultList As FaultList)
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetFaults(faultBO, faultList)

            'Note: we'll check whether exception is generated over here or not and log it
            'Check success scenario

            If faultBO.IsFlagStatus Then
                Return True
            End If

            Return False

        End Function
#End Region

#Region "GetFaultQuestion"
        Public Function GetFaultQuestion(ByRef faultQBO As FaultQuestionBO)
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.GetFaultQuestion(faultQBO)

            Return Nothing
        End Function
#End Region

#Region "SaveFaultTemporarily"

        Public Function SaveFaultTemporarily(ByRef tempFaultBO As TempFaultBO)
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.SaveFaultTemporarily(tempFaultBO)

            Return Nothing
        End Function
#End Region

#Region "GetTempFaultBasket"
        Public Function GetTempFaultBasket(ByRef tempFaultBO As TempFaultBO, ByRef tempFaultDS As DataSet)

            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.GetTempFaultBasket(tempFaultBO, tempFaultDS)

            Return Nothing
        End Function
#End Region

#Region "DeleteTempFault"
        Public Function DeleteTempFault(ByRef tempFaultBO As TempFaultBO)
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.DeleteTempFault(tempFaultBO)

            Return Nothing
        End Function
#End Region

#Region "saveFinalFaultBasket"
        Public Function saveFinalFaultBasket(ByRef finalFaultBO As FinalFaultBasketBO)
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.saveFinalFaultBasket(finalFaultBO)

            Return Nothing
        End Function

#End Region

#Region "UpdateFaultQuantity"

        Public Function UpdateFaultQuantity(ByRef tempFaultBO As TempFaultBO)

            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.UpdateFaultQuantity(tempFaultBO)

            Return Nothing
        End Function
#End Region

#Region "LoadBreadCrumb"
        Public Function LoadBreadCrumb(ByRef breadBO As BreadCrumbBO)

            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.LoadBreadCrumb(breadBO)

            Return Nothing
        End Function

#End Region

#End Region

    End Class

End Namespace