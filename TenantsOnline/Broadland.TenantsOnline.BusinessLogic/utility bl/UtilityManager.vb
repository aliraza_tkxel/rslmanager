Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.DataAccess

Namespace Broadland.TenantsOnline.BusinessLogic

    Public Class UtilityManager

#Region "Methods"

#Region "getLookUpList"

        Public Sub getLookUpList(ByVal spName As String, ByRef lstLookup As LookUpList)
            Dim objDAL As New UtilityDAL
            objDAL.getLookUpList(spName, lstLookup)

        End Sub

        Public Sub getLookUpList(ByVal spName As String, ByRef lstLookup As LookUpList, ByVal inParam As String)
            Dim objDAL As New UtilityDAL
            objDAL.getLookUpList(spName, lstLookup, inParam)
        End Sub

#End Region

#End Region
        
#Region "Functions"

#Region "SendEmail"

        Public Function SendMail(ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String) As Boolean

            Dim flagEmailSendSuccess As Boolean = False
            flagEmailSendSuccess = Utility.SendMail(strTo, strSubject, strBody)
            If (flagEmailSendSuccess) Then
                Return True
            End If

            Return False

        End Function

#End Region

#Region "getBroadlandNews"


        Public Function getNewsList() As NewsList
            Dim spName As String = SprocNameConstants.getBroadlandNews
            Dim lstNews As New NewsList

            Dim objDAL As New UtilityDAL
            objDAL.getNewsList(spName, lstNews)

            lstNews.RemoveAt(0)
            Return lstNews

        End Function

        Public Function getLatestNews() As NewsList

            Dim spName As String = SprocNameConstants.getBroadlandLatestNews
            Dim lstNews As New NewsList

            Dim objDAL As New UtilityDAL
            objDAL.getNewsList(spName, lstNews)

            Return lstNews

        End Function

#End Region

#End Region

    End Class

End Namespace
