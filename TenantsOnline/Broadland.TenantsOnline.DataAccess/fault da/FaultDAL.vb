﻿
'' Class Name -- FaultDAL
'' Base Class -- BaseDAL
'' Summary --    Contains Functionality related to Fault Locator
'' Created By  -- Noor Muhammad
'' Create on -- 13th Oct,2008
''---------------------------------
Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess

    Public Class FaultDAL : Inherits BaseDAL

#Region "Functions"

#End Region

#Region "Methods"
#Region "GetLocationName"
        Sub GetLocationName(ByRef locationBO As LocationBO)

            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetLocationName

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim locationId As ParameterBO = New ParameterBO("LocationID", locationBO.LocationId, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(locationId)


            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ' If record found 
            If myReader.Read Then

                If Not myReader.IsDBNull(myReader.GetOrdinal("LocationName")) Then
                    locationBO.LocationName = myReader.GetOrdinal("LocationName")
                End If
                locationBO.IsFlagStatus = True

            Else
                ' If No record found
                locationBO.IsFlagStatus = False

            End If

        End Sub
#End Region

#Region "GetCustomerFaults"

        Public Function GetCustomerFaults(ByVal customerID As Integer, ByVal faultStatus As String) As DataSet

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerFaults

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", customerID, DbType.Int32)

            '' Create EnquiryLogBO parameters
            Dim faultStatuseParam As ParameterBO
            faultStatuseParam = New ParameterBO("FaultStatus", faultStatus, DbType.String)

            '' Adding object to paramList
            inParamList.Add(customerIdParam)
            inParamList.Add(faultStatuseParam)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            Dim dsEnq As DataSet = New DataSet
            MyBase.LoadDataSet(dsEnq, inParamList, sprocName)

            Return dsEnq

        End Function

#End Region

#Region "GetUnreadFaultCount"

        Public Sub GetUnreadFaultCount(ByRef faultLogBO As FaultLogBO, ByVal faultStatus As String)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerUnreadFaultsCount

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", faultLogBO.CustomerId, DbType.Int32)

            '' Create EnquiryLogBO parameters
            Dim faultStatuseParam As ParameterBO
            faultStatuseParam = New ParameterBO("FaultStatus", faultStatus, DbType.String)

            '' Adding object to paramList
            inParamList.Add(customerIdParam)
            inParamList.Add(faultStatuseParam)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("UnreadFaultCount")) Then
                    faultLogBO.UnreadfaultCount = myDataReader.GetInt32(myDataReader.GetOrdinal("UnreadFaultCount"))
                End If
                faultLogBO.IsFlagStatus = True
            Else
                faultLogBO.IsFlagStatus = False
            End If




        End Sub

#End Region

#Region "SaveSQuestions"

        Public Sub SaveSQuestions(ByRef sQuestionBO As SQuestionBO, ByRef sAnswerBO As SQuestionBO, ByVal faultLogID As Integer)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.SaveSQuestions

            Dim count As Integer

            For count = 0 To sQuestionBO.Count - 1
                Dim questionId As ParameterBO = New ParameterBO("QUESTIONID", sQuestionBO.Item(count), DbType.Int32)
                Dim answer As ParameterBO = New ParameterBO("ANSWER", sAnswerBO.Item(count), DbType.String)
                Dim fLogID As ParameterBO = New ParameterBO("FAULTLOGID", faultLogID, DbType.Int32)
                Dim tempSQID As ParameterBO = New ParameterBO("SAID", Nothing, DbType.Int32)

                ''
                '' This List will hold instances of type ParameterBO
                Dim inParamList As ParameterList = New ParameterList()
                Dim outParamList As ParameterList = New ParameterList()

                inParamList.Add(questionId)
                inParamList.Add(answer)
                inParamList.Add(fLogID)
                outParamList.Add(tempSQID)

                '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
                MyBase.SaveRecord(inParamList, outParamList, sprocName)

                ''Out parameter will return the TempFaultId in case of success or -1 otherwise
                Dim qryResult As Integer = outParamList.Item(0).Value

                ''In the case of invalid information provided
                If qryResult = -1 Then
                    count = sQuestionBO.Count
                    Exit For
                End If
            Next
        End Sub

#End Region

#Region "GetElements"
        Public Sub GetElements(ByRef elementBO As ElementBO, ByRef elementList As FaultElementList)

            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetElements

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim areaId As ParameterBO = New ParameterBO("AreaId", elementBO.AreaId, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(areaId)

            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)


            ''Iterate the dataReader
            While (myReader.Read)

                Dim elementId As Integer
                Dim elementName As String = String.Empty

                If Not myReader.IsDBNull(myReader.GetOrdinal("ElementId")) Then
                    elementId = myReader.GetInt32(myReader.GetOrdinal("ElementId"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("ElementName")) Then
                    elementName = myReader.GetString(myReader.GetOrdinal("ElementName"))
                End If

                Dim objElementBO As New ElementBO(elementId, elementName)

                elementList.Add(objElementBO)

            End While

            If elementList.Count <= 0 Then
                ' If No record found
                elementBO.IsFlagStatus = False

            End If

        End Sub
#End Region

#Region "GetFaults"
        Public Sub GetFaults(ByRef faultBO As FaultBO, ByRef faultList As FaultList)


            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetFaults

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim elementId As ParameterBO = New ParameterBO("ElementId", faultBO.ElementId, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(elementId)

            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            Dim counter As Integer = New Integer()

            counter = 0
            ''Iterate the dataReader
            While (myReader.Read)

                Dim faultId As Integer
                Dim description As String = String.Empty

                If Not myReader.IsDBNull(myReader.GetOrdinal("FaultId")) Then
                    faultId = myReader.GetInt32(myReader.GetOrdinal("FaultId"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("description")) Then
                    description = myReader.GetString(myReader.GetOrdinal("description"))
                End If

                Dim objFaultBO As New FaultBO(faultId, description)

                faultList.Add(objFaultBO)

                counter = counter + 1
            End While

            If Not counter Then
                faultBO.IsFlagStatus = False
            End If

        End Sub
#End Region

#Region "SaveFaultTemporarily"
        Public Sub SaveFaultTemporarily(ByRef tempFaultBO As TempFaultBO)
            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.SaveFaultTemporarily

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TempFaultBO parameters
            Dim faultId As ParameterBO = New ParameterBO("FaultId", tempFaultBO.FaultId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", tempFaultBO.CustomerId, DbType.String)
            Dim quantity As ParameterBO = New ParameterBO("Quantity", tempFaultBO.Quantity, DbType.Int32)
            Dim problemDays As ParameterBO = New ParameterBO("ProblemDays", tempFaultBO.ProblemDays, DbType.Int32)
            Dim recuringProblem As ParameterBO = New ParameterBO("RecuringProblem", tempFaultBO.RecuringProblem, DbType.Int32)
            Dim communalProblem As ParameterBO = New ParameterBO("CommunalProblem", tempFaultBO.CommunalProblem, DbType.Int32)
            Dim recharge As ParameterBO = New ParameterBO("Recharge", tempFaultBO.Recharge, DbType.Int32)
            Dim notes As ParameterBO = New ParameterBO("Notes", tempFaultBO.Notes, DbType.String)

            Dim tempFaultId As ParameterBO = New ParameterBO("TempFaultId", Nothing, DbType.Int32)


            '' Adding object to paramList
            inParamList.Add(faultId)
            inParamList.Add(customerId)
            inParamList.Add(quantity)
            inParamList.Add(problemDays)
            inParamList.Add(recuringProblem)
            inParamList.Add(communalProblem)
            inParamList.Add(recharge)
            inParamList.Add(notes)
            outParamList.Add(tempFaultId)

            '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                tempFaultBO.IsFlagStatus = False

                tempFaultBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else

                tempFaultBO.IsFlagStatus = True

            End If
        End Sub

#End Region

#Region "GetFaultQuestion"
        Public Sub GetFaultQuestion(ByRef faultQBO As FaultQuestionBO)

            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetFaultQuestion

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim questionId As ParameterBO = New ParameterBO("QuestionId", faultQBO.QuestionId, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(questionId)

            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ' If record found 
            If myReader.Read Then


                If Not myReader.IsDBNull(myReader.GetOrdinal("FQuestion")) Then
                    faultQBO.Question = myReader.GetString(myReader.GetOrdinal("FQuestion"))
                End If

                faultQBO.IsFlagStatus = True

            Else
                ' If No record found
                faultQBO.IsFlagStatus = False


            End If
        End Sub
#End Region

#Region "GetTempFaultBasket"

        Public Sub GetTempFaultBasket(ByRef tempFaultBO As TempFaultBO, ByRef tempFaultDS As DataSet)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.TempFaultBasket

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TerminationBO parameters
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", tempFaultBO.CustomerId, DbType.Int32)


            '' Adding object to paramList           
            inParamList.Add(customerId)

            MyBase.LoadDataSet(tempFaultDS, inParamList, sprocName)

            '' If no record found means no account/rent statement exist and w'll display user msg
            If tempFaultDS.Tables(0).Rows.Count = 0 Then

                tempFaultBO.IsFlagStatus = False

                tempFaultBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                tempFaultBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "DeleteTempFault"
        Public Sub DeleteTempFault(ByRef tempFaultBO As TempFaultBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.DeleteTempFault

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type
            'Create TerminationBO parameters
            Dim tempFaultId As ParameterBO = New ParameterBO("TempFaultID", tempFaultBO.TempFaultId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", tempFaultBO.CustomerId, DbType.Int32)
            Dim countRecords As ParameterBO = New ParameterBO("CountRecords", Nothing, DbType.Int32)


            '' Adding object to paramList           
            inParamList.Add(tempFaultId)
            inParamList.Add(customerId)
            outParamList.Add(countRecords)


            'Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                tempFaultBO.IsFlagStatus = False
            Else
                tempFaultBO.CountRecords = qryResult

                tempFaultBO.IsFlagStatus = True

            End If


        End Sub
#End Region

#Region "UpdateFaultQuantity"
        Public Sub UpdateFaultQuantity(ByRef tempFaultBO As TempFaultBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.UpdateFaultQuantity

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TempFaultBO parameters           
            Dim tempFaultId As ParameterBO = New ParameterBO("TempFaultId", tempFaultBO.TempFaultId, DbType.Int32)
            Dim quantity As ParameterBO = New ParameterBO("Quantity", tempFaultBO.Quantity, DbType.Int32)
            Dim tmpFault As ParameterBO = New ParameterBO("TmpFault", tempFaultBO.TempFaultId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(tempFaultId)
            inParamList.Add(quantity)
            outParamList.Add(tmpFault)

            '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                tempFaultBO.IsFlagStatus = False

                tempFaultBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else

                tempFaultBO.IsFlagStatus = True

            End If
        End Sub
#End Region

#Region "saveFinalFaultBasket"
        Public Sub saveFinalFaultBasket(ByRef finalFaultBO As FinalFaultBasketBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.saveFinalFaultBasket

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TempFaultBO parameters           
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", finalFaultBO.CustomerId, DbType.Int32)
            Dim prefContactTime As ParameterBO = New ParameterBO("PreferredContactDetails", finalFaultBO.PrefContactTime, DbType.String)
            Dim submitDate As ParameterBO = New ParameterBO("SubmitDate", finalFaultBO.SubmitDate, DbType.DateTime)
            Dim iamHappy As ParameterBO = New ParameterBO("IamHappy", finalFaultBO.IamHappy, DbType.Int32)

            Dim faultBasketId As ParameterBO = New ParameterBO("FaultBasketId", Nothing, DbType.Int32)


            '' Adding object to paramList
            inParamList.Add(customerId)
            inParamList.Add(prefContactTime)
            inParamList.Add(submitDate)
            inParamList.Add(iamHappy)
            outParamList.Add(faultBasketId)

            '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                finalFaultBO.IsFlagStatus = False

                finalFaultBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else

                finalFaultBO.IsFlagStatus = True

            End If
        End Sub

#End Region

#Region "LoadBreadCrumb"
        Public Sub LoadBreadCrumb(ByRef breadBO As BreadCrumbBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.LoadBreadCrumb

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TerminationBO parameters
            Dim curPageId As ParameterBO = New ParameterBO("CurPageId", breadBO.CurPageId, DbType.Int32)
            Dim curPageIdType As ParameterBO = New ParameterBO("CurPageIdType", breadBO.CurPageIdType, DbType.String)


            'Adding object to paramList           
            inParamList.Add(curPageId)
            inParamList.Add(curPageIdType)


            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ' If record found 
            If myReader.Read Then


                If Not myReader.IsDBNull(myReader.GetOrdinal("LocationID")) Then
                    breadBO.LocationId = myReader.GetInt32(myReader.GetOrdinal("LocationID"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("LocationName")) Then
                    breadBO.LocationName = myReader.GetString(myReader.GetOrdinal("LocationName"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("AreaID")) Then
                    breadBO.AreaId = myReader.GetInt32(myReader.GetOrdinal("AreaID"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("AreaName")) Then
                    breadBO.AreaName = myReader.GetString(myReader.GetOrdinal("AreaName"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("ElementID")) Then
                    breadBO.ElementId = myReader.GetInt32(myReader.GetOrdinal("ElementID"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("ElementName")) Then
                    breadBO.ElementName = myReader.GetString(myReader.GetOrdinal("ElementName"))
                End If

                If curPageIdType.Value.ToString = "Fault" Then
                    If Not myReader.IsDBNull(myReader.GetOrdinal("FaultID")) Then
                        breadBO.FaultId = myReader.GetInt32(myReader.GetOrdinal("FaultID"))
                    End If

                    If Not myReader.IsDBNull(myReader.GetOrdinal("Description")) Then
                        breadBO.FaultName = myReader.GetString(myReader.GetOrdinal("Description"))
                    End If
                End If

                breadBO.IsFlagStatus = True

            Else
                ' If No record found
                breadBO.IsFlagStatus = False


            End If

        End Sub

#End Region
#End Region



    End Class

End Namespace