Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess

    Public Class UtilityDAL : Inherits BaseDAL

#Region "Functions"

#Region "getLookUpList"

        Public Function getLookUpList(ByVal spName As String, ByRef lstLookUp As LookUpList) As LookUpList

            Dim sprocName As String = spName
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, sprocName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookupBO(id, name)
                lstLookUp.Add(objLookUp)
            End While

            Return lstLookUp

        End Function

        Public Function getLookUpList(ByVal spName As String, ByRef lstLookUp As LookUpList, ByVal inParam As String) As LookUpList

            Dim sprocName As String = spName
            Dim inParamList As New ParameterList()
            Dim inParamBO As New ParameterBO("DEVELOPMENTID", Integer.Parse(inParam), DbType.Int32)
            inParamList.Add(inParamBO)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookupBO(id, name)
                lstLookUp.Add(objLookUp)
            End While

            Return lstLookUp

        End Function

#End Region

#Region "getNewsList"

        Public Function getNewsList(ByVal spName As String, ByRef lstNews As NewsList) As NewsList

            Dim sprocName As String = spName
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, sprocName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                ''Call utility method to parse attribute values,construct 
                ''and add  NewsBO from provided datareader to list

                Me.FillNewsBO(myDataReader, lstNews)

            End While

            Return lstNews

        End Function
#End Region

#End Region

#Region "Methods"

        Private Sub FillNewsBO(ByRef myDataReader As IDataReader, ByRef lstNews As NewsList)
            Dim newsId As Integer
            Dim headline As String = String.Empty
            Dim imagePath As String = String.Empty
            Dim newsCreationDate As DateTime = Nothing
            Dim newsContent As String = String.Empty

            ''Parse datareader for values
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NewsID")) Then
                newsId = myDataReader.GetInt32(myDataReader.GetOrdinal("NewsID"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Headline")) Then
                headline = myDataReader.GetString(myDataReader.GetOrdinal("Headline"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ImagePath")) Then
                imagePath = myDataReader.GetString(myDataReader.GetOrdinal("ImagePath"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("DateCreated")) Then
                newsCreationDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("DateCreated"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NewsContent")) Then
                newsContent = myDataReader.GetString(myDataReader.GetOrdinal("NewsContent"))
            End If


            Dim objNews As New NewsBO()

            ''Set attributes of news BO
            objNews.NewsID = newsId
            objNews.HeadLine = headline
            objNews.ImagePath = imagePath
            objNews.DateCreated = newsCreationDate
            objNews.NewsContent = newsContent

            ''Add news BO to list
            lstNews.Add(objNews)
        End Sub

#End Region

    End Class

End Namespace
