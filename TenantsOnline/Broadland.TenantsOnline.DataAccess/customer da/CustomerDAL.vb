'' Class Name -- CustomerDAL
'' Base Class -- BaseDAL
'' Summary --    Contains Functionality related to Customer/Tenant
'' Methods --    AuthenticateCustomer
'' Created By  -- Muanwar Nadeem

''---------------------------------
Imports System

Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Diagnostics


Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess

    Public Class CustomerDAL : Inherits BaseDAL

#Region "Functions"

#Region "Signin Related Functions"
        '' This region contains All function used to authenticate customer and If authenticate,
        '' getting some Customer Information eventually stored in Session to display across pages


#Region "AuthenticateCustomer Function"



        '' NOTE:-
        ''       We'll send back exception to BLL and will set flags + log exceptions there

        Public Sub AuthenticateCustomer(ByRef custHeadBO As CustomerHeaderBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AuthenticateCustomer

            '' This List will hold instances of type ParameterBO
            Dim paramList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type
            '' ParameterBO for userName
            Dim userParam As ParameterBO = New ParameterBO("TenancyId", custHeadBO.TenancyID, DbType.String)

            '' Adding object to paramList
            paramList.Add(userParam)

            '' Parameter for password
            Dim pwdParam As ParameterBO = New ParameterBO("password", custHeadBO.Password, DbType.String)
            paramList.Add(pwdParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If authenticated, customerID is returned
            If myReader.Read Then

                custHeadBO.CustomerID = GetCustomerID(myReader).ToString
                custHeadBO.IsFlagStatus = True

                '' Call to GetCustomerHeaderBO that will execute sproc to get CustomerHeaderBO relatedInfo
                Me.SelectCustomerHeaderBO(custHeadBO)

            End If

        End Sub

#End Region

#Region "GetCustomerID Function"

        '' Private method used to return CustomerID from DataReader
        '' This method gets called inside of AuthenticateCustoemr 
        Private Function GetCustomerID(ByRef myDataRecord As IDataRecord) As Integer

            Return myDataRecord.GetInt32(myDataRecord.GetOrdinal("CUSTOMERID"))

        End Function

#End Region

#Region "SelectCustomerHeaderBO Function"

        '' This method gets called form inside AuthenticateCustomer once customer credentials verified
        '' This will execute sproc to get CustomerHeaderBO related Info eventually going to store in session
        Private Sub SelectCustomerHeaderBO(ByRef custHeadBO As CustomerHeaderBO)
          
            '' SprocNameConstants contains stored-procedures names, inside  project Utilities\constants
            Dim sprocName As String = SprocNameConstants.SelectCustomerHeaderBO

            '' This List will hold instances of type ParameterBO
            Dim paramList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type
            '' ParameterBO for CustomerID
            Dim custIdParam As ParameterBO = New ParameterBO("customerID", custHeadBO.CustomerID, DbType.String)

            '' Adding object to paramList
            paramList.Add(custIdParam)


            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)


            '' If record found i.e. Title, First, Middle and Last Name, TenancyId
            If myReader.Read Then

                Me.FillCustomerHeaderBO(myReader, custHeadBO)
                custHeadBO.IsFlagStatus = True
            Else
                '' If No record found
                custHeadBO.IsFlagStatus = False
            End If


        End Sub

#End Region

#Region "FillCustomerHeaderBO Function"

        '' Private method used to Fill Single Business Object of class CustomerBO
        '' This method gets called inside of AuthenticateCustoemr 
        Private Sub FillCustomerHeaderBO(ByRef myDataRecord As IDataRecord, ByRef custHeadBO As CustomerHeaderBO)



            '' Ensuring value isn't NULL in Table.
            '' Setting Description into custHeadBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("DESCRIPTION")) Then
                custHeadBO.Title = myDataRecord.GetString(myDataRecord.GetOrdinal("DESCRIPTION"))
            End If


            '' Setting FirstName into custHeadBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("FirstName")) Then
                custHeadBO.FirstName = myDataRecord.GetString(myDataRecord.GetOrdinal("FirstName"))
            End If


            '' Setting MiddleName into custHeadBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("MiddleName")) Then
                custHeadBO.MiddleName = myDataRecord.GetString(myDataRecord.GetOrdinal("MiddleName"))
            End If

            '' Setting LastName into custHeadBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("LastName")) Then
                custHeadBO.LastName = myDataRecord.GetString(myDataRecord.GetOrdinal("LastName"))
            End If

            '' Setting TenancyID into custHeadBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("TENANCYID")) Then
                custHeadBO.TenancyID = myDataRecord.GetInt32(myDataRecord.GetOrdinal("TENANCYID")).ToString
            End If



           
          

        End Sub

#End Region


#End Region

#Region "PasswordAssistance Related Functions"

        '' This region contains functions related to Password Assistance
        '' Customer's Password is returned once he/she provided Email used as user-name

#Region "CustomerPasswordAssistance Function"

        Public Sub CustomerPasswordAssitance(ByRef custHeadBO As CustomerHeaderBO)

        
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.PasswordAssistance

            '' This List will hold instances of type ParameterBO
            Dim paramList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type
            '' ParameterBO for Email i.e. user-name
            Dim emailParam As ParameterBO = New ParameterBO("email", custHeadBO.UserName, DbType.String)

            '' Adding object to paramList
            paramList.Add(emailParam)


            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)


            '' If record found i.e. Email exists, Login active then Password returned after 
            '' wrapped inside CustomerHeaderBO object
            If myReader.Read Then

                custHeadBO.Password = Me.GetPassword(myReader)
                custHeadBO.IsFlagStatus = True

            End If

            

        End Sub

#End Region

#Region "GetPassword Function"

        '' Private method used to return Password from DataReader
        '' This method gets called inside of CustomerPasswordAssistance
        Private Function GetPassword(ByRef myDataRecord As IDataRecord) As String
            Return myDataRecord.GetString(myDataRecord.GetOrdinal("Password"))

        End Function

#End Region

#End Region

#Region "Customers Arrears/FinancialInfo Related Functions"

        '' This region contains single function used to Fill CustomerFinancialBO Info 

        '' CustomerFinancialBO contains 3 attributes, _balance, arrearsPosition & _crmComments
        '' Here Account/rent balance is Calcualted using a stored procedure
        '' While arrears position and comments entered by CRM is obtained by executing separate sproc

#Region "ManageFinancialInfo Function"

        Public Sub ManageFinancialInfo(ByRef custFinBO As CustomerFinancialBO, ByRef custHeadBO As CustomerHeaderBO, ByVal accBalOnly As Boolean)

          
            '' custHeadBO here used to obtain CustomerID and TenancyID. Both are set when customer logs in

            '' accBalOnly boolean is passed as parameter to this function. When True, only account/rent balance
            '' is calcualted otherwise all attributes of CustomerFinancialBO will be obtained

            '' First we'll calculate account/rent balance and set to custFinBO.Balance

            Dim sprocName As String = Nothing

            '' used to hold ParameterBOs , supplied to sproc as IN-parameters 
            Dim inParamList As ParameterList = Nothing

            '' setting sproc name from SprpcNameConstants file
            sprocName = SprocNameConstants.CalculateAccountBalance

            '' Creating parameterBO type List, used as IN parameters supplied to storedprocedure
            inParamList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type
            '' ParameterBO for TenancyID i.e. tenancyID
            Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyID", custHeadBO.TenancyID, DbType.String)

            '' Adding object to paramList
            inParamList.Add(tenancyIdParam)


            '' Passing List of IN-Parameters, sproc-name to base-class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            '' If record found, we'll get calculated account/rent balance
            If myReader.Read Then

                Me.FillCustomerFinancialBO(myReader, custFinBO, True)
                custFinBO.IsFlagStatus = True

            End If

            ''================================================================================

            '' Checking accBalOnly, enters into IF Block if accBalOnly=False
            If Not accBalOnly Then

                '' Enters only If we need to obtain values for remining two attributes of custFinBO

                '' setting sproc name from SprpcNameConstants file
                sprocName = SprocNameConstants.ManageArrears

                '' Clearing already stored ParameterBOs
                inParamList.Clear()

                '' Creating ParameterBO objects and passing prameters Name, Value and Type
                '' ParameterBO for customerID 
                Dim customerIdParam As ParameterBO = New ParameterBO("tenancyId", custHeadBO.TenancyID, DbType.String)

                '' Adding object to paramList
                inParamList.Add(customerIdParam)

                '' Passing List of IN-Parameters, sproc-name to base-class method: SelectRecord
                myReader = MyBase.SelectRecord(inParamList, sprocName)

                '' If record found, we'll get action status, and comments entered by CRM
                If myReader.Read Then

                    Me.FillCustomerFinancialBO(myReader, custFinBO, False)
                    custFinBO.IsFlagStatus = True

                Else

                    '' If no record found, setting IsFlagStatus to False
                    custFinBO.IsFlagStatus = False

                End If


            End If

        End Sub


#End Region

#Region "FillCustomerFinancialBO Function"

        '' Gets called from inside of MangeCustomerFinancialInfo function

        Private Sub FillCustomerFinancialBO(ByRef myDataRecord As IDataRecord, ByRef custFinBO As CustomerFinancialBO, ByVal balOnly As Boolean)


                '' If balOnly true, only balance will be read from dataset and set to custFinBO.Balance

                If balOnly Then

                    '' Ensuring value isn't NULL in Table.
                    '' Setting accountBalance into custFinBO if value isn't Null
                    If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("AccountBalance")) Then
                        custFinBO.Balance = myDataRecord.GetDecimal(myDataRecord.GetOrdinal("AccountBalance"))
                    End If

                Else

                    '' enters if balOnly=False, means we obtain arreas Action status & CRM comments/notes

                    '' Ensuring value isn't NULL in Table.
                    '' Setting accountBalance into custFinBO if value isn't Null
                    If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("Action")) Then
                        custFinBO.ArrearsAction = myDataRecord.GetString(myDataRecord.GetOrdinal("Action"))
                    End If

            End If

        End Sub

#End Region



#End Region

#Region "CustomerRegistration function"

        Public Sub CustomerRegistration(ByRef custBO As CustomerBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.RegisterCustomer

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' ParameterBO for userName
            Dim firstNameParam As ParameterBO = New ParameterBO("CustomerFirstName", custBO.FirstName, DbType.String)
            Dim lastNameParam As ParameterBO = New ParameterBO("CustomerLastName", custBO.LastName, DbType.String)
            Dim tenancyIdParam As ParameterBO = New ParameterBO("CustomerTenancyId", custBO.Tenancy, DbType.String)
            Dim emailParam As ParameterBO = New ParameterBO("CustomerEmail", custBO.Address.Email, DbType.String)
            Dim postcodeParam As ParameterBO = New ParameterBO("CustomerPostcode", custBO.Address.PostCode, DbType.String)
            Dim passwordNameParam As ParameterBO = New ParameterBO("Password", custBO.Password, DbType.String)
            Dim dobParam As ParameterBO = New ParameterBO("dob", custBO.DateOfBirth, DbType.DateTime)

            Dim outParamCustomerId As ParameterBO = New ParameterBO("CustomerId", Nothing, DbType.Int16)
            'Debugger.Break()
            Dim outParamErrorMessage As ParameterBO = New ParameterBO("ErrMessage", Nothing, DbType.String)

            '' Adding object to paramList
            inParamList.Add(firstNameParam)
            inParamList.Add(lastNameParam)
            inParamList.Add(tenancyIdParam)
            inParamList.Add(emailParam)
            inParamList.Add(postcodeParam)
            inParamList.Add(passwordNameParam)
            inParamList.Add(dobParam)

            outParamList.Add(outParamCustomerId)
            outParamList.Add(outParamErrorMessage)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            MyBase.SelectRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the customer Id in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value


            ''In the case of invalid information provided
            If qryResult = -1 Then
                custBO.IsFlagStatus = False
                custBO.UserMsg = outParamList.Item(1).Value.ToString

                ''In the case of user already registered
            ElseIf qryResult = -2 Then

                custBO.IsFlagStatus = False
                custBO.UserMsg = outParamList.Item(1).Value.ToString

                ''In the case of user registered successfully
            Else

                custBO.IsFlagStatus = True

            End If

        End Sub
#End Region

#Region "LogSessionStart function"

        Public Sub LogSessionStart(ByRef custHeadBO As CustomerHeaderBO)
            Dim sprocName As String = SprocNameConstants.LogSessionStartDate
            Dim cust As Integer
            cust = CType(custHeadBO.CustomerID, Integer)

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            Dim customerId As ParameterBO = New ParameterBO("CustomerId", cust, DbType.Int32)

            Dim outParamLogSessionId As ParameterBO = New ParameterBO("SessionId", Nothing, DbType.Int32)


            '' Adding object to paramList
            inParamList.Add(customerId)
            outParamList.Add(outParamLogSessionId)


            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            MyBase.SelectRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the customer Id in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value


            ''In the case of invalid information provided
            If qryResult = -1 Then
                custHeadBO.sessionId = qryResult
            Else
                custHeadBO.sessionId = qryResult
            End If





        End Sub

#End Region

#Region "LogSessionEnd function"
        Public Sub LogSessionEnd(ByVal custSession As Integer)
            Dim sprocName As String = SprocNameConstants.LogSessionEndDate

            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            Dim sessionId As ParameterBO = New ParameterBO("SessionId", custSession.ToString(), DbType.String)


            '' Adding object to paramList
            inParamList.Add(sessionId)


            MyBase.SaveRecord(inParamList, outParamList, sprocName)

        End Sub

#End Region

#Region "CustomerUpdateDetails function"

        Public Sub CustomerUpdate(ByRef custDetailsBO As CustomerDetailsBO, ByRef custHeadBO As CustomerHeaderBO)

            Dim sprocName As String = SprocNameConstants.UpdateCustomerDetails

            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim sexualorientationotherParam As ParameterBO = New ParameterBO("SEXUALORIENTATIONOTHER", custDetailsBO.SexualOrientationOther, DbType.StringFixedLength)
            inParamList.Add(sexualorientationotherParam)
            Dim consentParam As ParameterBO = New ParameterBO("CONSENT", custDetailsBO.Consent, DbType.Int32)
            inParamList.Add(consentParam)
            Dim customeridParam As ParameterBO = New ParameterBO("CUSTOMERID", custDetailsBO.Id, DbType.Int32)
            inParamList.Add(customeridParam)
            Dim employmentstatusParam As ParameterBO = New ParameterBO("EMPLOYMENTSTATUS", custDetailsBO.EmploymentStatusId, DbType.Int32)
            inParamList.Add(employmentstatusParam)
            Dim ethnicoriginParam As ParameterBO = New ParameterBO("ETHNICORIGIN", custDetailsBO.EthnicityId, DbType.Int32)
            inParamList.Add(ethnicoriginParam)
            Dim maritalstatusParam As ParameterBO = New ParameterBO("MARITALSTATUS", custDetailsBO.MaritalStatusId, DbType.Int32)
            inParamList.Add(maritalstatusParam)
            Dim nationalityParam As ParameterBO = New ParameterBO("NATIONALITY", custDetailsBO.NationalityId, DbType.Int32)
            inParamList.Add(nationalityParam)
            Dim occupantsadultsParam As ParameterBO = New ParameterBO("OCCUPANTSADULTS", custDetailsBO.NoOfOccupantsAbove18, DbType.Int32)
            inParamList.Add(occupantsadultsParam)
            Dim occupantschildrenParam As ParameterBO = New ParameterBO("OCCUPANTSCHILDREN", custDetailsBO.NoOfOccupantsBelow18, DbType.Int32)
            inParamList.Add(occupantschildrenParam)
            Dim religionParam As ParameterBO = New ParameterBO("RELIGION", custDetailsBO.ReligionId, DbType.Int32)
            inParamList.Add(religionParam)
            Dim sexualorientationParam As ParameterBO = New ParameterBO("SEXUALORIENTATION", custDetailsBO.SexualOrientationId, DbType.Int32)
            inParamList.Add(sexualorientationParam)
            Dim takehomepayParam As ParameterBO = New ParameterBO("TAKEHOMEPAY", custDetailsBO.TakeHomePay, DbType.Int32)
            inParamList.Add(takehomepayParam)
            Dim bankfacilityParam As ParameterBO = New ParameterBO("BANKFACILITY", custDetailsBO.BankFacilityIdString, DbType.String)
            inParamList.Add(bankfacilityParam)
            Dim bankfacilityotherParam As ParameterBO = New ParameterBO("BANKFACILITYOTHER", custDetailsBO.BankFacilityOther, DbType.String)
            inParamList.Add(bankfacilityotherParam)
            Dim benefitParam As ParameterBO = New ParameterBO("BENEFIT", custDetailsBO.BenefitIdString, DbType.String)
            inParamList.Add(benefitParam)
            Dim communicationParam As ParameterBO = New ParameterBO("COMMUNICATION", custDetailsBO.CommunicationIdString, DbType.String)
            inParamList.Add(communicationParam)
            Dim communicationotherParam As ParameterBO = New ParameterBO("COMMUNICATIONOTHER", custDetailsBO.CommunicationOther, DbType.String)
            inParamList.Add(communicationotherParam)
            Dim disabilityParam As ParameterBO = New ParameterBO("DISABILITY", custDetailsBO.DisabilityIdString, DbType.String)
            inParamList.Add(disabilityParam)
            Dim disabilityotherParam As ParameterBO = New ParameterBO("DISABILITYOTHER", custDetailsBO.DisabilityOther, DbType.String)
            inParamList.Add(disabilityotherParam)
            Dim empaddress1Param As ParameterBO = New ParameterBO("EMPADDRESS1", custDetailsBO.EmployerAddress1, DbType.String)
            inParamList.Add(empaddress1Param)
            Dim empaddress2Param As ParameterBO = New ParameterBO("EMPADDRESS2", custDetailsBO.EmployerAddress2, DbType.String)
            inParamList.Add(empaddress2Param)
            Dim employernameParam As ParameterBO = New ParameterBO("EMPLOYERNAME", custDetailsBO.EmployerName, DbType.String)
            inParamList.Add(employernameParam)
            Dim emppostcodeParam As ParameterBO = New ParameterBO("EMPPOSTCODE", custDetailsBO.EmployerPostcode, DbType.String)
            inParamList.Add(emppostcodeParam)
            Dim emptowncityParam As ParameterBO = New ParameterBO("EMPTOWNCITY", custDetailsBO.EmployerTown, DbType.String)
            inParamList.Add(emptowncityParam)
            Dim ethnicoriginotherParam As ParameterBO = New ParameterBO("ETHNICORIGINOTHER", custDetailsBO.EthnicityOther, DbType.String)
            inParamList.Add(ethnicoriginotherParam)
            Dim firstlanguageParam As ParameterBO = New ParameterBO("FIRSTLANGUAGE", custDetailsBO.MainLanguage, DbType.String)
            inParamList.Add(firstlanguageParam)
            Dim genderParam As ParameterBO = New ParameterBO("GENDER", custDetailsBO.Gender, DbType.String)
            inParamList.Add(genderParam)
            Dim internetaccessParam As ParameterBO = New ParameterBO("INTERNETACCESS", custDetailsBO.InternetAccessIdString, DbType.String)
            inParamList.Add(internetaccessParam)
            Dim internetaccessotherParam As ParameterBO = New ParameterBO("INTERNETACCESSOTHER", custDetailsBO.InternetAccessOther, DbType.String)
            inParamList.Add(internetaccessotherParam)
            Dim nationalityotherParam As ParameterBO = New ParameterBO("NATIONALITYOTHER", custDetailsBO.NationalityOther, DbType.String)
            inParamList.Add(nationalityotherParam)
            Dim occupationParam As ParameterBO = New ParameterBO("OCCUPATION", custDetailsBO.Occupation, DbType.String)
            inParamList.Add(occupationParam)
            Dim preferedcontactParam As ParameterBO = New ParameterBO("PREFEREDCONTACT", custDetailsBO.PreferedContactId, DbType.String)
            inParamList.Add(preferedcontactParam)
            Dim religionotherParam As ParameterBO = New ParameterBO("RELIGIONOTHER", custDetailsBO.ReligionOther, DbType.String)
            inParamList.Add(religionotherParam)
            Dim supportagenciesParam As ParameterBO = New ParameterBO("SUPPORTAGENCIES", custDetailsBO.SupportAgencyIdString, DbType.String)
            inParamList.Add(supportagenciesParam)
            Dim supportagenciesotherParam As ParameterBO = New ParameterBO("SUPPORTAGENCIESOTHER", custDetailsBO.SupportAgencyOther, DbType.String)
            inParamList.Add(supportagenciesotherParam)
            Dim carerParam As ParameterBO = New ParameterBO("CARER", custDetailsBO.LiveInCarer, DbType.Int16)
            inParamList.Add(carerParam)
            Dim emailyesnoParam As ParameterBO = New ParameterBO("EMAILYESNO", custDetailsBO.ConsentToEmail, DbType.Int16)
            inParamList.Add(emailyesnoParam)
            Dim houseincomeParam As ParameterBO = New ParameterBO("HOUSEINCOME", custDetailsBO.HouseIncomeId, DbType.Int16)
            inParamList.Add(houseincomeParam)
            Dim textyesnoParam As ParameterBO = New ParameterBO("TEXTYESNO", custDetailsBO.ConsentToText, DbType.Int16)
            inParamList.Add(textyesnoParam)
            Dim wheelchairParam As ParameterBO = New ParameterBO("WHEELCHAIR", custDetailsBO.Wheelchair, DbType.Int16)
            inParamList.Add(wheelchairParam)
            Dim ownTransportParam As ParameterBO = New ParameterBO("OWNTRANSPORT", custDetailsBO.OwnTransport, DbType.Int16)
            inParamList.Add(ownTransportParam)

            Dim emailParam As ParameterBO = New ParameterBO("EMAIL", custDetailsBO.Address.Email, DbType.String)
            inParamList.Add(emailParam)
            Dim mobileParam As ParameterBO = New ParameterBO("MOBILE", custDetailsBO.Address.TelephoneMobile, DbType.String)
            inParamList.Add(mobileParam)
            Dim mobile2Param As ParameterBO = New ParameterBO("MOBILE2", custDetailsBO.Address.TelephoneMobile2, DbType.String)
            inParamList.Add(mobile2Param)
            Dim telParam As ParameterBO = New ParameterBO("TEL", custDetailsBO.Address.Telephone, DbType.String)
            inParamList.Add(telParam)
            Dim telrelationshipParam As ParameterBO = New ParameterBO("TELRELATIONSHIP", custDetailsBO.Address.AlternativeContact, DbType.String)
            inParamList.Add(telrelationshipParam)
            Dim telrelativeParam As ParameterBO = New ParameterBO("TELRELATIVE", custDetailsBO.Address.TelephoneAlternativeContact, DbType.String)
            inParamList.Add(telrelativeParam)
            Dim telworkParam As ParameterBO = New ParameterBO("TELWORK", custDetailsBO.Address.TelephoneWork, DbType.String)
            inParamList.Add(telworkParam)

            Dim rowCount As Integer = 0

            ' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            rowCount = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            rowCount = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ' If Row-count is greater than 0, means email successfully changed
            If rowCount > 0 Then

                custDetailsBO.IsFlagStatus = True

                Me.SelectCustomerHeaderBO(custHeadBO)

            End If

        End Sub

#End Region

#Region "GetCustomerInformation Function"

#Region "GetCustomerInfo"

        Public Function GetCustomerInfo(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailsInfo As Boolean) As CustomerDetailsBO

            Dim customerAddressBO As CustomerAddressBO = New CustomerAddressBO()
            custDetailsBO.Address = customerAddressBO

            ''Get customer Info as Name and address
            GetCustomerDetailsInfo(custDetailsBO, getDetailsInfo)

            ''Get customer Account Balance to be displayed on welcome screen account section
            GetCustomerAccountBalance(custDetailsBO)

            ''Get customer Gas Servicing appointment date to be displayed on welcome screen
            GasServicingAppointmentDate(custDetailsBO)

            '' check Vulnerability of customer
            VulnerabilityCheck(custDetailsBO)

            custDetailsBO.IsFlagStatus = True

            Return custDetailsBO

        End Function

        Public Sub GetCustomerDetailsInfo(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailsInfo As Boolean)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerInfo

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' ParameterBO for userName
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", custDetailsBO.Id, DbType.Int16)

            '' Adding object to paramList
            inParamList.Add(customerIdParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ''Set BO attributes by reading from data reader
            If getDetailsInfo Then

                ''Get Customer information of first record returned
                If (myDataReader.Read) Then
                    Me.FillCustomerDetailsBODetailed(myDataReader, custDetailsBO)
                End If

            Else

                ''Get Customer information of first record returned
                If (myDataReader.Read) Then
                    Me.FillCustomerDetailsBOBasic(myDataReader, custDetailsBO)
                End If

            End If

            custDetailsBO.IsFlagStatus = True

        End Sub

#Region "GetCustomerDetail Funciton for Fault Locator"

        Public Sub GetCustomerContactDetail(ByRef custDetailsBO As CustomerDetailsBO)


            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerContactDetail

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' ParameterBO for userName
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", custDetailsBO.Id, DbType.Int16)

            '' Adding object to paramList
            inParamList.Add(customerIdParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ''Get Customer information of first record returned
            If (myDataReader.Read) Then
                Me.FillCustomerContactDetails(myDataReader, custDetailsBO)
            End If

            custDetailsBO.IsFlagStatus = True

        End Sub

#End Region

#Region "SaveCustomerContactDetail"

        Public Sub UpdateCustomerContactDetail(ByRef cusBO As CustomerBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.UpdateCustomerContactDetail


            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Saving all this in  parameter bo
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", cusBO.Id, DbType.String)
            Dim firstName As ParameterBO = New ParameterBO("FirstName", cusBO.FirstName, DbType.String)
            Dim middleName As ParameterBO = New ParameterBO("MiddleName", cusBO.MiddleName, DbType.String)
            Dim lastName As ParameterBO = New ParameterBO("LastName", cusBO.LastName, DbType.String)
            Dim houseNumber As ParameterBO = New ParameterBO("HouseNumber", cusBO.Address.HouseNumber, DbType.String)
            Dim address1 As ParameterBO = New ParameterBO("Address1", cusBO.Address.Address1, DbType.String)
            Dim address2 As ParameterBO = New ParameterBO("Address2", cusBO.Address.Address2, DbType.String)
            Dim address3 As ParameterBO = New ParameterBO("Address3", cusBO.Address.Address3, DbType.String)
            Dim townCity As ParameterBO = New ParameterBO("TownCity", cusBO.Address.TownCity, DbType.String)
            Dim county As ParameterBO = New ParameterBO("County", cusBO.Address.County, DbType.String)
            Dim postCode As ParameterBO = New ParameterBO("PostCode", cusBO.Address.PostCode, DbType.String)
            Dim telephone As ParameterBO = New ParameterBO("Telephone", cusBO.Address.Telephone, DbType.String)
            Dim telephoneMobile As ParameterBO = New ParameterBO("TelephoneMobile", cusBO.Address.TelephoneMobile, DbType.String)
            Dim email As ParameterBO = New ParameterBO("Email", cusBO.Address.Email, DbType.String)
            Dim custId As ParameterBO = New ParameterBO("CustId", Nothing, DbType.String)


            'Adding object to paramList
            inParamList.Add(customerId)
            inParamList.Add(firstName)
            inParamList.Add(middleName)
            inParamList.Add(lastName)
            inParamList.Add(houseNumber)
            inParamList.Add(address1)
            inParamList.Add(address2)
            inParamList.Add(address3)
            inParamList.Add(townCity)
            inParamList.Add(county)
            inParamList.Add(postCode)
            inParamList.Add(telephone)
            inParamList.Add(telephoneMobile)
            inParamList.Add(email)
            outParamList.Add(custId)


            'Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            Dim rowCount As Integer = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            'If Row-count is greater than 0, means email successfully changed
            If rowCount > 0 Then

                'customerID output parameter is stored in paramlist at zero(0) index
                'Retrieving value of customerID output parameter after execution
                cusBO.Id = outParamList(0).Value
                cusBO.IsFlagStatus = True

            End If

        End Sub

#End Region

#Region " PrivateUtilityMethod "

        ''' <summary>
        ''' Populates basic customer details
        ''' </summary>
        ''' <param name="myDataReader"></param>
        ''' <param name="custDetailsBO"></param>
        ''' <remarks></remarks>
        Private Sub FillCustomerDetailsBOBasic(ByRef myDataReader As IDataReader, ByRef custDetailsBO As CustomerDetailsBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("FIRSTNAME")) Then
                custDetailsBO.FirstName = myDataReader.GetString(myDataReader.GetOrdinal("FIRSTNAME"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LASTNAME")) Then
                custDetailsBO.LastName = myDataReader.GetString(myDataReader.GetOrdinal("LASTNAME"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("DOB")) Then
                custDetailsBO.DateOfBirth = myDataReader.GetDateTime(myDataReader.GetOrdinal("DOB"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ADDRESS1")) Then
                custDetailsBO.Address.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("ADDRESS1"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ADDRESS2")) Then
                custDetailsBO.Address.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("ADDRESS2"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ADDRESS3")) Then
                custDetailsBO.Address.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("ADDRESS3"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("COUNTY")) Then
                custDetailsBO.Address.County = myDataReader.GetString(myDataReader.GetOrdinal("COUNTY"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMAIL")) Then
                custDetailsBO.Address.Email = myDataReader.GetString(myDataReader.GetOrdinal("EMAIL"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HOUSENUMBER")) Then
                custDetailsBO.Address.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HOUSENUMBER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MOBILE")) Then
                custDetailsBO.Address.TelephoneMobile = myDataReader.GetString(myDataReader.GetOrdinal("MOBILE"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MOBILE2")) Then
                custDetailsBO.Address.TelephoneMobile2 = myDataReader.GetString(myDataReader.GetOrdinal("MOBILE2"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("POSTCODE")) Then
                custDetailsBO.Address.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("POSTCODE"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEL")) Then
                custDetailsBO.Address.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("TEL"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TELRELATIONSHIP")) Then
                custDetailsBO.Address.AlternativeContact = myDataReader.GetString(myDataReader.GetOrdinal("TELRELATIONSHIP"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TELRELATIVE")) Then
                custDetailsBO.Address.TelephoneAlternativeContact = myDataReader.GetString(myDataReader.GetOrdinal("TELRELATIVE"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TELWORK")) Then
                custDetailsBO.Address.TelephoneWork = myDataReader.GetString(myDataReader.GetOrdinal("TELWORK"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TOWNCITY")) Then
                custDetailsBO.Address.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TOWNCITY"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("GENDER")) Then
                custDetailsBO.Gender = myDataReader.GetString(myDataReader.GetOrdinal("GENDER"))
            End If

        End Sub

        ''' <summary>
        ''' Populates customer details
        ''' </summary>
        ''' <param name="myDataReader"></param>
        ''' <param name="custDetailsBO"></param>
        ''' <remarks></remarks>
        Private Sub FillCustomerDetailsBODetailed(ByRef myDataReader As IDataReader, ByRef custDetailsBO As CustomerDetailsBO)

            ''fill the BO for basic attributes
            FillCustomerDetailsBOBasic(myDataReader, custDetailsBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("SEXUALORIENTATIONOTHER")) Then
                custDetailsBO.SexualOrientationOther = myDataReader.GetString(myDataReader.GetOrdinal("SEXUALORIENTATIONOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CONSENT")) Then
                custDetailsBO.Consent = Convert.ToBoolean(myDataReader.GetInt32(myDataReader.GetOrdinal("CONSENT")))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CUSTOMERID")) Then
                custDetailsBO.Id = myDataReader.GetInt32(myDataReader.GetOrdinal("CUSTOMERID"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMPLOYMENTSTATUS")) Then
                custDetailsBO.EmploymentStatusId = myDataReader.GetInt32(myDataReader.GetOrdinal("EMPLOYMENTSTATUS"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ETHNICORIGIN")) Then
                custDetailsBO.EthnicityId = myDataReader.GetInt32(myDataReader.GetOrdinal("ETHNICORIGIN"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LOCALAUTHORITY")) Then
                custDetailsBO.LocalAuthorityId = myDataReader.GetInt32(myDataReader.GetOrdinal("LOCALAUTHORITY"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MARITALSTATUS")) Then
                custDetailsBO.MaritalStatusId = myDataReader.GetInt32(myDataReader.GetOrdinal("MARITALSTATUS"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NATIONALITY")) Then
                custDetailsBO.NationalityId = myDataReader.GetInt32(myDataReader.GetOrdinal("NATIONALITY"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("OCCUPANTSADULTS")) Then
                custDetailsBO.NoOfOccupantsAbove18 = myDataReader.GetInt32(myDataReader.GetOrdinal("OCCUPANTSADULTS"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("OCCUPANTSCHILDREN")) Then
                custDetailsBO.NoOfOccupantsBelow18 = myDataReader.GetInt32(myDataReader.GetOrdinal("OCCUPANTSCHILDREN"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("RELIGION")) Then
                custDetailsBO.ReligionId = myDataReader.GetInt32(myDataReader.GetOrdinal("RELIGION"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("SEXUALORIENTATION")) Then
                custDetailsBO.SexualOrientationId = myDataReader.GetInt32(myDataReader.GetOrdinal("SEXUALORIENTATION"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TAKEHOMEPAY")) Then
                custDetailsBO.TakeHomePay = myDataReader.GetInt32(myDataReader.GetOrdinal("TAKEHOMEPAY"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TITLE")) Then
                custDetailsBO.TitleId = myDataReader.GetInt32(myDataReader.GetOrdinal("TITLE"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("BANKFACILITY")) Then
                custDetailsBO.BankFacilityIdString = myDataReader.GetString(myDataReader.GetOrdinal("BANKFACILITY"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("BANKFACILITYOTHER")) Then
                custDetailsBO.BankFacilityOther = myDataReader.GetString(myDataReader.GetOrdinal("BANKFACILITYOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("BENEFIT")) Then
                custDetailsBO.BenefitIdString = myDataReader.GetString(myDataReader.GetOrdinal("BENEFIT"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("COMMUNICATION")) Then
                custDetailsBO.CommunicationIdString = myDataReader.GetString(myDataReader.GetOrdinal("COMMUNICATION"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("COMMUNICATIONOTHER")) Then
                custDetailsBO.CommunicationOther = myDataReader.GetString(myDataReader.GetOrdinal("COMMUNICATIONOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("DISABILITY")) Then
                custDetailsBO.DisabilityIdString = myDataReader.GetString(myDataReader.GetOrdinal("DISABILITY"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("DISABILITYOTHER")) Then
                custDetailsBO.DisabilityOther = myDataReader.GetString(myDataReader.GetOrdinal("DISABILITYOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMPADDRESS1")) Then
                custDetailsBO.EmployerAddress1 = myDataReader.GetString(myDataReader.GetOrdinal("EMPADDRESS1"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMPADDRESS2")) Then
                custDetailsBO.EmployerAddress2 = myDataReader.GetString(myDataReader.GetOrdinal("EMPADDRESS2"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMPLOYERNAME")) Then
                custDetailsBO.EmployerName = myDataReader.GetString(myDataReader.GetOrdinal("EMPLOYERNAME"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMPPOSTCODE")) Then
                custDetailsBO.EmployerPostcode = myDataReader.GetString(myDataReader.GetOrdinal("EMPPOSTCODE"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMPTOWNCITY")) Then
                custDetailsBO.EmployerTown = myDataReader.GetString(myDataReader.GetOrdinal("EMPTOWNCITY"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ETHNICORIGINOTHER")) Then
                custDetailsBO.EthnicityOther = myDataReader.GetString(myDataReader.GetOrdinal("ETHNICORIGINOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("FIRSTLANGUAGE")) Then
                custDetailsBO.MainLanguage = myDataReader.GetString(myDataReader.GetOrdinal("FIRSTLANGUAGE"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("INTERNETACCESS")) Then
                custDetailsBO.InternetAccessIdString = myDataReader.GetString(myDataReader.GetOrdinal("INTERNETACCESS"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("INTERNETACCESSOTHER")) Then
                custDetailsBO.InternetAccessOther = myDataReader.GetString(myDataReader.GetOrdinal("INTERNETACCESSOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MIDDLENAME")) Then
                custDetailsBO.MiddleName = myDataReader.GetString(myDataReader.GetOrdinal("MIDDLENAME"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NATIONALITYOTHER")) Then
                custDetailsBO.NationalityOther = myDataReader.GetString(myDataReader.GetOrdinal("NATIONALITYOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NINUMBER")) Then
                custDetailsBO.NINumber = myDataReader.GetString(myDataReader.GetOrdinal("NINUMBER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("OCCUPATION")) Then
                custDetailsBO.Occupation = myDataReader.GetString(myDataReader.GetOrdinal("OCCUPATION"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PREFEREDCONTACT")) Then
                custDetailsBO.PreferedContactId = myDataReader.GetString(myDataReader.GetOrdinal("PREFEREDCONTACT"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("RELIGIONOTHER")) Then
                custDetailsBO.ReligionOther = myDataReader.GetString(myDataReader.GetOrdinal("RELIGIONOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("SUPPORTAGENCIES")) Then
                custDetailsBO.SupportAgencyIdString = myDataReader.GetString(myDataReader.GetOrdinal("SUPPORTAGENCIES"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("SUPPORTAGENCIESOTHER")) Then
                custDetailsBO.SupportAgencyOther = myDataReader.GetString(myDataReader.GetOrdinal("SUPPORTAGENCIESOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TITLEOTHER")) Then
                custDetailsBO.TitleOther = myDataReader.GetString(myDataReader.GetOrdinal("TITLEOTHER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CARER")) Then
                custDetailsBO.LiveInCarer = myDataReader.GetInt16(myDataReader.GetOrdinal("CARER"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EMAILYESNO")) Then
                custDetailsBO.ConsentToEmail = myDataReader.GetInt16(myDataReader.GetOrdinal("EMAILYESNO"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HOUSEINCOME")) Then
                custDetailsBO.HouseIncomeId = myDataReader.GetInt16(myDataReader.GetOrdinal("HOUSEINCOME"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEXTYESNO")) Then
                custDetailsBO.ConsentToText = myDataReader.GetInt16(myDataReader.GetOrdinal("TEXTYESNO"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("WHEELCHAIR")) Then
                custDetailsBO.Wheelchair = myDataReader.GetInt16(myDataReader.GetOrdinal("WHEELCHAIR"))
            End If
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("OWNTRANSPORT")) Then
                custDetailsBO.OwnTransport = myDataReader.GetInt16(myDataReader.GetOrdinal("OWNTRANSPORT"))
            End If

        End Sub

        ''service method with takes a datareader and parse it for
        ''values to be set for customerDetailsObject
        Private Sub FillCustomerContactDetails(ByRef myDataReader As IDataReader, ByRef custDetailsBO As CustomerDetailsBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerFirstName")) Then
                custDetailsBO.FirstName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerFirstName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerMiddleName")) Then
                custDetailsBO.MiddleName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerMiddleName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerLastName")) Then
                custDetailsBO.LastName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerLastName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Email")) Then
                custDetailsBO.Address.Email = myDataReader.GetString(myDataReader.GetOrdinal("Email"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Telephone")) Then
                custDetailsBO.Address.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("Telephone"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("mobile")) Then
                custDetailsBO.Address.TelephoneMobile = myDataReader.GetString(myDataReader.GetOrdinal("mobile"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
                custDetailsBO.Address.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
                custDetailsBO.Address.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
                custDetailsBO.Address.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
                custDetailsBO.Address.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
                custDetailsBO.Address.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("County")) Then
                custDetailsBO.Address.County = myDataReader.GetString(myDataReader.GetOrdinal("County"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Postcode")) Then
                custDetailsBO.Address.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("Postcode"))
            End If

        End Sub


        Private Sub FillCustomerDetailsBOCommunication(ByRef myDataReader As IDataReader, ByRef custDetailsBO As CustomerDetailsBO)

            Dim count As Integer = 0

            Do
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Communication")) Then
                    count = count + 1

                    If count = 1 Then
                        custDetailsBO.CommunicationIdString = myDataReader.GetString(myDataReader.GetOrdinal("Communication"))
                    Else
                        custDetailsBO.CommunicationIdString = custDetailsBO.CommunicationIdString & ", " & myDataReader.GetString(myDataReader.GetOrdinal("Communication"))
                    End If
                End If

            Loop While myDataReader.Read

        End Sub

        ''service method with takes a datareader and parse it for
        ''values to be set for customerDetailsObject
        Private Sub FillCustomerDetailsBODisability(ByRef myDataReader As IDataReader, ByRef custDetails As CustomerDetailsBO)

            Dim count As Integer = 0

            Do
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Disability")) Then
                    count = count + 1

                    If count = 1 Then
                        custDetails.DisabilityIdString = myDataReader.GetString(myDataReader.GetOrdinal("Disability"))
                    Else
                        custDetails.DisabilityIdString = custDetails.DisabilityIdString & ", " & myDataReader.GetString(myDataReader.GetOrdinal("Disability"))
                    End If
                End If



            Loop While myDataReader.Read

        End Sub

#End Region

#End Region

#Region "GetTerminationInfo"
        'Public Sub GetTerminationInfo(ByRef custAddressBO As CustomerAddressBO, ByRef terminBO As TerminationBO)
        '    '' NOTE:-
        '    ''       We'll send back exception to BLL and will set flags + log exceptions there

        '    Dim sprocName As String = SprocNameConstants.GetCustomerAddressTermination

        '    '' 
        '    '' This List will hold instances of type ParameterBO
        '    Dim inParamList As ParameterList = New ParameterList()


        '    '' Creating ParameterBO objects and passing prameters Name, Value and Type

        '    '' Create TerminationBO parameters
        '    Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", terminBO.EnquiryLogId, DbType.Int32)

        '    '' Adding object to paramList
        '    inParamList.Add(enquiryLogID)



        '    ''Out parameter will return the TerminationId in case of success or -1 otherwise
        '    Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

        '    If myDataReader.Read Then
        '        Me.FillTerminationBO(myDataReader, terminBO)

        '        ''  TO DO: GetCustomerAddress method is already exists. Using Polymorphism, need to move 
        '        '' this code on
        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
        '            custAddressBO.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
        '        End If

        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
        '            custAddressBO.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
        '        End If


        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
        '            custAddressBO.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
        '        End If


        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
        '            custAddressBO.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
        '        End If


        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
        '            custAddressBO.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
        '        End If


        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PostCode")) Then
        '            custAddressBO.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("PostCode"))
        '        End If


        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("COUNTY")) Then
        '            custAddressBO.Coutnty = myDataReader.GetString(myDataReader.GetOrdinal("COUNTY"))
        '        End If


        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEL")) Then
        '            custAddressBO.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("TEL"))
        '        End If

        '        terminBO.IsFlagStatus = True
        '    Else
        '        terminBO.IsFlagStatus = False
        '    End If


        'End Sub

        'Private Sub FillTerminationBO(ByRef myDataReader As IDataReader, ByRef terminBO As TerminationBO)

        '    If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
        '        terminBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
        '    End If

        '    If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MovingOutDate")) Then
        '        terminBO.MovingOutDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("MovingOutDate"))
        '    End If

        'End Sub
#End Region

#End Region

#Region "sendTerminationRequest"
        Public Sub SendTerminationRequest(ByRef termBO As TerminationBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.AddTermination

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", termBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", termBO.Description, DbType.String)
            '  Dim removeFlag As ParameterBO = New ParameterBO("RemoveFlag", termBO.RemoveFlag, DbType.Int32)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", termBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", termBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", termBO.CustomerId, DbType.Int32)
            Dim journalId As ParameterBO = New ParameterBO("JournaalID", termBO.JournalId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", termBO.ItemNatureId, DbType.Int32)
            Dim movingOutDate As ParameterBO = New ParameterBO("MovingOutDate", termBO.MovingOutDate, DbType.DateTime)

            Dim TerminationId As ParameterBO = New ParameterBO("TerminationId", Nothing, DbType.Int32)
            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            ' inParamList.Add(removeFlag)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(journalId)
            inParamList.Add(itemNatureId)
            inParamList.Add(movingOutDate)

            outParamList.Add(TerminationId)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                termBO.IsFlagStatus = False
                termBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                termBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#End Region

#Region "Methods"

#Region "ChangeEmail Related Functions"


        '' This region contains single function used to change customer email (usernaem) provided that, 
        '' customer supplied current email & password correctly

        '' Besides that, once Email changed, CustomerHeaderBO is filled to be stored in session
        '' Functions SelectCustomerHeaderBO can be found under Region "Signin Related Functions". This
        '' method gets called inside of ChangeEmail

        Public Sub ChangeEmail(ByRef custHeadBO As CustomerHeaderBO)



            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.ChangeEmail

            '' Creating parameterBO type List, used as IN parameters supplied to storedprocedure
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type
            '' ParameterBO for Email i.e. current user-name
            Dim currEmailParam As ParameterBO = New ParameterBO("currEmail", custHeadBO.UserName, DbType.String)

            '' Adding object to paramList
            inParamList.Add(currEmailParam)


            '' ParameterBO for NewEmail i.e. New username 
            Dim newEmailParam As ParameterBO = New ParameterBO("newEmail", custHeadBO.NewUserName, DbType.String)
            inParamList.Add(newEmailParam)

            '' ParameterBO for password
            Dim passwordParam As ParameterBO = New ParameterBO("password", custHeadBO.Password, DbType.String)
            inParamList.Add(passwordParam)

            '' Creating parameterBO type List, used as OUT parameters supplied to storedprocedure
            Dim outParamList As ParameterList = New ParameterList()

            '' Output parameter wrapped as ParameterBO for customerID
            Dim custIdParam As ParameterBO = New ParameterBO("customerID", custHeadBO.TenancyID, DbType.Int32)
            outParamList.Add(custIdParam)

            Dim rowCount As Integer = 0

            '' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            rowCount = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            '' If Row-count is greater than 0, means email successfully changed
            If rowCount > 0 Then

                '' customerID output parameter is stored in paramlist at zero(0) index
                '' Retrieving value of customerID output parameter after execution
                custHeadBO.CustomerID = outParamList(0).Value
                custHeadBO.IsFlagStatus = True

                '' Call to GetCustomerHeaderBO that will execute sproc to get CustomerHeaderBO relatedInfo
                Me.SelectCustomerHeaderBO(custHeadBO)

            End If

        End Sub

#End Region

#Region "ChangePassword Related Functions"


        '' This region contains single function used to change customer passwordprovided that, 

        Public Sub ChangePassword(ByRef custHeadBO As CustomerHeaderBO)


            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.ChangePassword

            '' Creating parameterBO type List, used as IN parameters supplied to storedprocedure
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type
            '' ParameterBO for Email i.e. current user-name
            Dim currEmailParam As ParameterBO = New ParameterBO("Email", custHeadBO.UserName, DbType.String)

            '' Adding object to paramList
            inParamList.Add(currEmailParam)


            '' ParameterBO for NewEmail i.e. New password 
            Dim newEmailParam As ParameterBO = New ParameterBO("Password", custHeadBO.Password, DbType.String)
            inParamList.Add(newEmailParam)

            '' ParameterBO for NewPassword
            Dim passwordParam As ParameterBO = New ParameterBO("@Newpassword", custHeadBO.NewPassword, DbType.String)
            inParamList.Add(passwordParam)

            '' Creating parameterBO type List, used as OUT parameters supplied to storedprocedure
            Dim outParamList As ParameterList = New ParameterList()

            '' Output parameter wrapped as ParameterBO for customerID
            Dim custIdParam As ParameterBO = New ParameterBO("customerID", custHeadBO.TenancyID, DbType.Int32)
            outParamList.Add(custIdParam)

            Dim rowCount As Integer = 0

            '' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            rowCount = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            '' If Row-count is greater than 0, means email successfully changed
            If rowCount > 0 Then

                '' customerID output parameter is stored in paramlist at zero(0) index
                '' Retrieving value of customerID output parameter after execution
                custHeadBO.CustomerID = outParamList(0).Value
                custHeadBO.IsFlagStatus = True

                '' Call to GetCustomerHeaderBO that will execute sproc to get CustomerHeaderBO relatedInfo
                Me.SelectCustomerHeaderBO(custHeadBO)

            End If

        End Sub

#End Region

#Region "Home Page relatd method"

        Private Sub GetCustomerAccountBalance(ByRef custDetailsBO As CustomerDetailsBO)
            Dim sprocName As String = SprocNameConstants.GetCustomerAccountBalance

            ''Instantiate the input parameters list
            Dim inParamList As ParameterList = New ParameterList

            ''Instantiate the ParameterBO wich will hold the parameter info
            Dim tenancyIdParam As ParameterBO = New ParameterBO("TenancyId", custDetailsBO.Tenancy, DbType.String)

            ''Add input parameters to parameter list
            inParamList.Add(tenancyIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If (myDataReader.Read) Then
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("RentBalance")) Then
                    custDetailsBO.AccountBalance = myDataReader.GetDecimal(myDataReader.GetOrdinal("RentBalance"))
                End If
            End If


        End Sub

        Private Sub GasServicingAppointmentDate(ByRef custDetailsBO As CustomerDetailsBO)

            Dim sprocName As String = SprocNameConstants.GetServicingAppointmentDate

            ''Instantiate the input parameters list
            Dim inParamList As ParameterList = New ParameterList

            ''Instantiate the ParameterBO wich will hold the parameter info
            Dim tenancyIdParam As ParameterBO = New ParameterBO("TenancyId", custDetailsBO.Tenancy, DbType.String)

            ''Add input parameters to parameter list
            inParamList.Add(tenancyIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If (myDataReader.Read) Then
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("GasServicingAppointmentDate")) Then
                    custDetailsBO.GasServicingAppointmentDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("GasServicingAppointmentDate"))
                Else
                    custDetailsBO.GasServicingAppointmentDate = Nothing
                End If
            End If

        End Sub

        Private Sub VulnerabilityCheck(ByRef custDetailsBO As CustomerDetailsBO)

            Dim sprocName As String = SprocNameConstants.GetCustomerVulnerability

            ''Instantiate the input parameters list
            Dim inParamList As ParameterList = New ParameterList

            ''Instantiate the ParameterBO wich will hold the parameter info
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", custDetailsBO.Id, DbType.Int16)

            ''Add input parameters to parameter list
            inParamList.Add(customerIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            custDetailsBO.vulnerabilityDataReader = myDataReader
        End Sub
#End Region

#Region "RentStatement Related Functions"

        Public Sub RentAccount(ByRef rentDS As DataSet, ByVal custRentBO As CustomerRentBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String

            '' This List will hold instances of type ParameterBO
            Dim paramList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type
            '' ParameterBO for TenancyID
            Dim tenancyParam As ParameterBO = New ParameterBO("TENANCYID", CType(custRentBO.TenancyID, Int32), DbType.Int32)

            '' Adding object to paramList
            paramList.Add(tenancyParam)

            If custRentBO.StartDate Is Nothing And custRentBO.EndDate Is Nothing Then
                sprocName = SprocNameConstants.GetRentStatementForAll
            Else
                sprocName = SprocNameConstants.GetRentStatementForPeriod

                tenancyParam = New ParameterBO("PAYMENTSTARTDATE", CType(custRentBO.StartDate, Date), DbType.Date)

                '' Adding object to paramList
                paramList.Add(tenancyParam)

                tenancyParam = New ParameterBO("PAYMENTENDDATE", CType(custRentBO.EndDate, Date), DbType.Date)

                '' Adding object to paramList
                paramList.Add(tenancyParam)
            End If

            '' Call to baseclass method
            Me.LoadDataSet(rentDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and w'll display user msg
            If rentDS.Tables(0).Rows.Count = 0 Then
                custRentBO.IsFlagStatus = False
                custRentBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                custRentBO.IsFlagStatus = True

                ''' Call the Me.GetHousingBenifitBalance method
                ''' This will set the antHB & advHB attributes of custRentBO, that will help in 
                ''' calculating Housing Benefit Balance
                'Me.GetHousingBenifitBalance(custRentBO)

            End If

        End Sub

        Public Sub GetCustomerBalances(ByRef custRentBO As CustomerRentBO)

            Dim sprocName As String = SprocNameConstants.GetPeriodRentStatement

            ''Instantiate the input parameters list
            Dim inParamList As ParameterList = New ParameterList

            ''Instantiate the ParameterBO wich will hold the parameter info
            Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyID", custRentBO.TenancyID, DbType.String)

            ''Add input parameters to parameter list
            inParamList.Add(tenancyIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If (myDataReader.Read) Then
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("RentBalance")) Then
                    custRentBO.AccountBalance = myDataReader.GetDecimal(myDataReader.GetOrdinal("RentBalance"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("EstimatedHBDue")) Then
                    custRentBO.EstimatedHB = myDataReader.GetDecimal(myDataReader.GetOrdinal("EstimatedHBDue"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("owedtoBHA")) Then
                    custRentBO.ToPay = myDataReader.GetDecimal(myDataReader.GetOrdinal("owedtoBHA"))
                End If
            End If

        End Sub

        'Public Sub GetCustomerPeriodAccountBalance(ByRef custRentBO As CustomerRentBO)
        '    Dim sprocName As String = SprocNameConstants.GetPeriodRentStatement

        '    ''Instantiate the input parameters list
        '    Dim inParamList As ParameterList = New ParameterList

        '    ''Instantiate the ParameterBO wich will hold the parameter info
        '    Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyID", custRentBO.TenancyID, DbType.String)

        '    ''Add input parameters to parameter list
        '    inParamList.Add(tenancyIdParam)

        '    'If custRentBO.StartDate IsNot Nothing And custRentBO.EndDate IsNot Nothing Then
        '    '    tenancyIdParam = New ParameterBO("PAYMENTSTARTDATE", CType(custRentBO.StartDate, Date), DbType.Date)

        '    '    '' Adding  PAYMENTSTARTDATE object to paramList
        '    '    inParamList.Add(tenancyIdParam)

        '    '    tenancyIdParam = New ParameterBO("PAYMENTENDDATE", CType(custRentBO.EndDate, Date), DbType.Date)

        '    '    '' Adding  PAYMENTENDDATE object to paramList
        '    '    inParamList.Add(tenancyIdParam)
        '    'End If

        '    Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

        '    If (myDataReader.Read) Then
        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("AccountBalance")) Then
        '            custRentBO.AccountBalance = myDataReader.GetDecimal(myDataReader.GetOrdinal("AccountBalance"))
        '        End If
        '    End If


        'End Sub

#Region "GetHousingBenifitBalance function"

        'Private Sub GetHousingBenifitBalance(ByRef custRentBO As CustomerRentBO)

        '    '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
        '    Dim sprocName As String = SprocNameConstants.GetSingleCustomerHB

        '    '' This List will hold instances of type ParameterBO
        '    Dim paramList As ParameterList = New ParameterList()

        '    '' Creating ParameterBO objects and passing prameters Name, Value and Type
        '    '' ParameterBO for TenancyID
        '    Dim tenancyParam As ParameterBO = New ParameterBO("TENANCYID", CType(custRentBO.TenancyID, Int32), DbType.Int32)

        '    '' Adding object to paramList
        '    paramList.Add(tenancyParam)

        '    '' Passing List of IN-Parameters, sproc-name to base-class method: SelectRecord
        '    Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

        '    '' If record found, we'll get calculated account/rent balance
        '    If myReader.Read Then

        '        Me.FillCustomerRentBO(myReader, custRentBO)
        '        custRentBO.IsFlagStatus = True

        '    End If

        'End Sub

#End Region


#Region "FillCustomerRentBO Function"

        '' Gets called from inside of GetHousingBenifitBalance function

        Private Sub FillCustomerRentBO(ByRef myDataRecord As IDataRecord, ByRef custRentBO As CustomerRentBO)

            '' Ensuring value isn't NULL in Table.
            '' Setting antHB into custRentBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("ANTHB")) Then
                custRentBO.AntHB = myDataRecord.GetDecimal(myDataRecord.GetOrdinal("ANTHB"))
            End If

            '' Setting advHB into custRentBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("ADVHB")) Then
                custRentBO.AdvHB = myDataRecord.GetDecimal(myDataRecord.GetOrdinal("ADVHB"))
            End If


        End Sub

#End Region


#End Region

#Region "RentRechargeStatement Related Functions"

        Public Sub RentRechargeAccount(ByRef rentRechargeDS As DataSet, ByVal custRentRechargeBO As CustomerRentRechargeBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String

            '' This List will hold instances of type ParameterBO
            Dim paramList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type
            '' ParameterBO for TenancyID
            Dim tenancyParam As ParameterBO = New ParameterBO("TENANCYID", CType(custRentRechargeBO.TenancyID, Int32), DbType.Int32)

            '' Adding object to paramList
            paramList.Add(tenancyParam)

            If custRentRechargeBO.StartDate Is Nothing And custRentRechargeBO.EndDate Is Nothing Then
                sprocName = SprocNameConstants.GetRentRechargeStatementForAll
            Else
                sprocName = SprocNameConstants.GetRentRechargeStatementForPeriod

                tenancyParam = New ParameterBO("PAYMENTSTARTDATE", CType(custRentRechargeBO.StartDate, Date), DbType.Date)

                '' Adding object to paramList
                paramList.Add(tenancyParam)

                tenancyParam = New ParameterBO("PAYMENTENDDATE", CType(custRentRechargeBO.EndDate, Date), DbType.Date)

                '' Adding object to paramList
                paramList.Add(tenancyParam)
            End If

            '' Call to baseclass method
            Me.LoadDataSet(rentRechargeDS, paramList, sprocName)


            If rentRechargeDS.Tables(0).Rows.Count = 0 Then
                custRentRechargeBO.IsFlagStatus = False
                custRentRechargeBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                custRentRechargeBO.IsFlagStatus = True


              '  Me.GetHousingBenifitBalance(custRentRechargeBO)

            End If

        End Sub

        'Public Sub GetCustomerPeriodAccountBalance(ByRef custRentRechargeBO As CustomerRentRechargeBO)
        '    Dim sprocName As String = SprocNameConstants.GetPeriodRentRechargeStatement

        '    ''Instantiate the input parameters list
        '    Dim inParamList As ParameterList = New ParameterList

        '    ''Instantiate the ParameterBO wich will hold the parameter info
        '    Dim tenancyIdParam As ParameterBO = New ParameterBO("tenancyID", custRentRechargeBO.TenancyID, DbType.String)

        '    ''Add input parameters to parameter list
        '    inParamList.Add(tenancyIdParam)

          

        '    Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

        '    If (myDataReader.Read) Then
        '        If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("AccountBalance")) Then
        '            custRentRechargeBO.AccountBalance = myDataReader.GetDecimal(myDataReader.GetOrdinal("AccountBalance"))
        '        End If
        '    End If


        'End Sub

#Region "GetHousingBenifitBalance function"

        'Private Sub GetHousingBenifitBalance(ByRef custRentRechargeBO As CustomerRentRechargeBO)

        '    '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
        '    Dim sprocName As String = SprocNameConstants.GetSingleCustomerRechargeHB

        '    '' This List will hold instances of type ParameterBO
        '    Dim paramList As ParameterList = New ParameterList()

        '    '' Creating ParameterBO objects and passing prameters Name, Value and Type
        '    '' ParameterBO for TenancyID
        '    Dim tenancyParam As ParameterBO = New ParameterBO("TENANCYID", CType(custRentRechargeBO.TenancyID, Int32), DbType.Int32)

        '    '' Adding object to paramList
        '    paramList.Add(tenancyParam)

        '    '' Passing List of IN-Parameters, sproc-name to base-class method: SelectRecord
        '    Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

        '    '' If record found, we'll get calculated account/rent balance
        '    If myReader.Read Then

        '        Me.FillCustomerRentBO(myReader, custRentRechargeBO)
        '        custRentRechargeBO.IsFlagStatus = True

        '    End If

        'End Sub

#End Region


#Region "FillCustomerRentRechargeBO Function"

        '' Gets called from inside of GetHousingBenifitBalance function

        Private Sub FillCustomerRentRechargeBO(ByRef myDataRecord As IDataRecord, ByRef custRentRechargeBO As CustomerRentRechargeBO)

            '' Ensuring value isn't NULL in Table.
            '' Setting antHB into custRentBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("ANTHB")) Then
                custRentRechargeBO.AntHB = myDataRecord.GetDecimal(myDataRecord.GetOrdinal("ANTHB"))
            End If

            '' Setting advHB into custRentBO if value isn't Null
            If Not myDataRecord.IsDBNull(myDataRecord.GetOrdinal("ADVHB")) Then
                custRentRechargeBO.AdvHB = myDataRecord.GetDecimal(myDataRecord.GetOrdinal("ADVHB"))
            End If


        End Sub

#End Region


#End Region

#Region "GetTerminationInfo"
        Public Sub GetTerminationInfo(ByRef custAddressBO As CustomerAddressBO, ByRef terminBO As TerminationBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressTermination

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", terminBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillTerminationBO(myDataReader, terminBO)

                '' TO DO: GetCustomerAddress method is already exists. Using Polymorphism, need to move
                '' this code on
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)

                terminBO.IsFlagStatus = True
            Else
                terminBO.IsFlagStatus = False
            End If


        End Sub
        Private Sub FillTerminationBO(ByRef myDataReader As IDataReader, ByRef terminBO As TerminationBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                terminBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MovingOutDate")) Then
                terminBO.MovingOutDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("MovingOutDate"))
            End If

        End Sub
#End Region

#Region "GetComplaintInfo"
        Public Sub GetComplaintInfo(ByRef custAddressBO As CustomerAddressBO, ByRef cmpltBO As ComplaintBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressComplaint

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", cmpltBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillComplaintBO(myDataReader, cmpltBO)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                cmpltBO.IsFlagStatus = True
            Else
                cmpltBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetASBInfo"
        Public Sub GetASBInfo(ByRef custAddressBO As CustomerAddressBO, ByRef abBO As AsbBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressASB

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", abBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillASBBO(myDataReader, abBO)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                abBO.IsFlagStatus = True
            Else
                abBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "Get GarageParking Info"
        Public Sub GetGarageParkingInfo(ByRef custAddressBO As CustomerAddressBO, ByRef gpBO As GarageParkingBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressGarageParking

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", gpBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillGarageParkingBO(myDataReader, gpBO)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                gpBO.IsFlagStatus = True
            Else
                gpBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetAbandonInfo"
        Public Sub GetAbandonInfo(ByRef custAddressBO As CustomerAddressBO, ByRef abndBO As AbandonBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressAbandon

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", abndBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillAbandonBO(myDataReader, abndBO)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                abndBO.IsFlagStatus = True
            Else
                abndBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetTransferInfo"
        Public Sub GetTransferInfo(ByRef custAddressBO As CustomerAddressBO, ByRef houseTrans As HouseMoveBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressTransfer

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", houseTrans.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillHouseMoveBO(myDataReader, houseTrans)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                houseTrans.IsFlagStatus = True
            Else
                houseTrans.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetCstRequestInfo"
        Public Sub GetCstRequestInfo(ByRef custAddressBO As CustomerAddressBO, ByRef cstReqBO As CstBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCstRequestInfo

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", cstReqBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                    cstReqBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NatureDescription")) Then
                    cstReqBO.RequestNature = myDataReader.GetString(myDataReader.GetOrdinal("NatureDescription"))
                End If

                cstReqBO.IsFlagStatus = True
            Else
                cstReqBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetClearDirectDebitRentInfo"
        Public Sub GetClearDirectDebitRentInfo(ByRef custAddressBO As CustomerAddressBO, ByRef enqBO As EnquiryLogBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetClearArrearInfo

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", enqBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                    enqBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
                End If
                enqBO.IsFlagStatus = True
            Else
                enqBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetClearArrearInfo"
        Public Sub GetClearArrearInfo(ByRef custAddressBO As CustomerAddressBO, ByRef enqBO As EnquiryLogBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetClearArrearInfo

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", enqBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                    enqBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
                End If
                enqBO.IsFlagStatus = True
            Else
                enqBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "Fill Complaint BO"
        Private Sub FillComplaintBO(ByRef myDataReader As IDataReader, ByRef cmpltBO As ComplaintBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                cmpltBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CategoryText")) Then
                cmpltBO.CategoryText = myDataReader.GetString(myDataReader.GetOrdinal("CategoryText"))
            End If

        End Sub
#End Region

#Region "Fill ASB BO"
        Private Sub FillASBBO(ByRef myDataReader As IDataReader, ByRef abBO As AsbBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                abBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CategoryText")) Then
                abBO.CategoryText = myDataReader.GetString(myDataReader.GetOrdinal("CategoryText"))
            End If

        End Sub
#End Region

#Region "Fill GarageParking BO"
        Private Sub FillGarageParkingBO(ByRef myDataReader As IDataReader, ByRef gpBO As GarageParkingBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                gpBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NameText")) Then
                gpBO.LookUpValueText = myDataReader.GetString(myDataReader.GetOrdinal("NameText"))
            End If

        End Sub
#End Region

#Region "FillAbandonBO"
        Private Sub FillAbandonBO(ByRef myDataReader As IDataReader, ByRef abndBO As AbandonBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                abndBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NameText")) Then
                abndBO.LookUpValueText = myDataReader.GetString(myDataReader.GetOrdinal("NameText"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LOCATION")) Then
                abndBO.Location = myDataReader.GetString(myDataReader.GetOrdinal("LOCATION"))
            End If

        End Sub
#End Region

#Region "FillHouseMoveBO"
        Private Sub FillHouseMoveBO(ByRef myDataReader As IDataReader, ByRef houseTransBO As HouseMoveBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                houseTransBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LocalAuthority")) Then
                houseTransBO.LocalAuthority = myDataReader.GetString(myDataReader.GetOrdinal("LocalAuthority"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Development")) Then
                houseTransBO.Development = myDataReader.GetString(myDataReader.GetOrdinal("Development"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NOOFBEDROOMS")) Then
                houseTransBO.NoOfBedrooms = myDataReader.GetInt32(myDataReader.GetOrdinal("NOOFBEDROOMS"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("OCCUPANTSNOGREATER18")) Then
                houseTransBO.OccupantsNoGreater18 = myDataReader.GetInt32(myDataReader.GetOrdinal("OCCUPANTSNOGREATER18"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("OCCUPANTSNOBELOW18")) Then
                houseTransBO.OccupantsNoBelow18 = myDataReader.GetInt32(myDataReader.GetOrdinal("OCCUPANTSNOBELOW18"))

            End If

        End Sub

#End Region

#Region "Fill Customer Address BO for enquiry log"
        Private Sub FillCustomerAddressBO(ByRef myDataReader As IDataReader, ByRef custAddressBO As CustomerAddressBO)
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
                custAddressBO.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
                custAddressBO.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
                custAddressBO.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
                custAddressBO.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
                custAddressBO.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PostCode")) Then
                custAddressBO.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("PostCode"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("COUNTY")) Then
                custAddressBO.County = myDataReader.GetString(myDataReader.GetOrdinal("COUNTY"))

            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEL")) Then
                custAddressBO.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("TEL"))
            End If
        End Sub
#End Region

#End Region

    End Class

End Namespace

