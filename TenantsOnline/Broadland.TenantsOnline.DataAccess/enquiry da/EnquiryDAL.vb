'' Class Name -- CustomerDAL
'' Base Class -- BaseDAL
'' Summary --    Contains Functionality related to Customer/Tenant
'' Methods --    AuthenticateCustomer
'' Created By  -- Muanwar Nadeem

''---------------------------------
Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess

    Public Class EnquiryDAL : Inherits BaseDAL

#Region "Methods"

#Region "SendCstRequest"

        Public Sub SendCstRequest(ByRef cstReqBO As CstBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.AddCustomerServiceRequest

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", cstReqBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", cstReqBO.Description, DbType.String)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", cstReqBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", cstReqBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", cstReqBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", cstReqBO.ItemNatureId, DbType.Int32)
            Dim requestNatureId As ParameterBO = New ParameterBO("RequestNatureID", cstReqBO.RequestNatuerID, DbType.Int32)

            Dim cstRequestId As ParameterBO = New ParameterBO("CstRequestId", Nothing, DbType.Int32)
            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)
            inParamList.Add(requestNatureId)

            outParamList.Add(cstRequestId)

            '' Passing Array of CstBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the CstRequestId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                cstReqBO.IsFlagStatus = False
                cstReqBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                cstReqBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "SendTerminationRequest"
        Public Sub SendTerminationRequest(ByRef termBO As TerminationBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.AddTermination

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", termBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", termBO.Description, DbType.String)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", termBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", termBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", termBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", termBO.ItemNatureId, DbType.Int32)
            Dim movingOutDate As ParameterBO = New ParameterBO("MovingOutDate", termBO.MovingOutDate, DbType.DateTime)

            Dim TerminationId As ParameterBO = New ParameterBO("TerminationId", Nothing, DbType.Int32)
            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)
            inParamList.Add(movingOutDate)

            outParamList.Add(TerminationId)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                termBO.IsFlagStatus = False
                termBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                termBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "SendComplaint"
        Public Sub SendComplaint(ByRef compBO As ComplaintBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.AddComplaint

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", compBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", compBO.Description, DbType.String)
            '   Dim removeFlag As ParameterBO = New ParameterBO("RemoveFlag", compBO.RemoveFlag, DbType.Int32)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", compBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", compBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", compBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", compBO.ItemNatureId, DbType.Int32)
            Dim categoryId As ParameterBO = New ParameterBO("CategoryId", compBO.CategoryId, DbType.Int32)

            Dim complaintId As ParameterBO = New ParameterBO("complaintId", Nothing, DbType.Int32)
            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)
            inParamList.Add(categoryId)

            outParamList.Add(complaintId)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                compBO.IsFlagStatus = False
                compBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                compBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "SendGarageParkingRequest"
        Public Sub SendParkingRequest(ByRef parkBO As GarageParkingBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.AddGarageParkingRequest

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", parkBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", parkBO.Description, DbType.String)
            '  Dim removeFlag As ParameterBO = New ParameterBO("RemoveFlag", parkBO.RemoveFlag, DbType.Int32)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", parkBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", parkBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", parkBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", parkBO.ItemNatureId, DbType.Int32)
            Dim lookUpValueID As ParameterBO = New ParameterBO("LookUPValueID", parkBO.LookUpValueID, DbType.Int32)

            Dim ParkingSpaceId As ParameterBO = New ParameterBO("ParkingSpaceId", Nothing, DbType.Int32)
            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)
            inParamList.Add(lookUpValueID)

            outParamList.Add(ParkingSpaceId)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                parkBO.IsFlagStatus = False
                parkBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                parkBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "ReportASB"

        Public Sub ReportASB(ByRef objAsbBO As AsbBO)
            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.ReportASB

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", objAsbBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", objAsbBO.Description, DbType.String)
            '   Dim removeFlag As ParameterBO = New ParameterBO("RemoveFlag", objAsbBO.RemoveFlag, DbType.Int32)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", objAsbBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", objAsbBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", objAsbBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", objAsbBO.ItemNatureId, DbType.Int32)

            Dim categoryID As ParameterBO = New ParameterBO("categoryID", objAsbBO.CategoryID, DbType.Int32)

            Dim asbID As ParameterBO = New ParameterBO("AsbID", Nothing, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)
            inParamList.Add(categoryID)

            outParamList.Add(asbID)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                objAsbBO.IsFlagStatus = False
                objAsbBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                objAsbBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "GetPreviousASB"

        Public Function GetPreviousASB(ByRef objAsbBO As AsbBO) As AsbList
            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetPreviousASB

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create AsbBO parameters
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", objAsbBO.CustomerId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(customerId)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            Dim dr As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            Dim lstAsbBO As New AsbList
            While dr.Read
                Dim objAsb As New AsbBO
                FillAsbBO(dr, objAsb)
                lstAsbBO.Add(objAsb)
            End While

            objAsbBO.IsFlagStatus = True
            Return lstAsbBO

        End Function

#End Region

#Region "FillAsbBO"

        Private Sub FillAsbBO(ByRef dr As IDataReader, ByRef asbBO As AsbBO)


            If Not dr.IsDBNull(dr.GetOrdinal("CreationDate")) Then
                asbBO.CreationDate = dr.GetDateTime(dr.GetOrdinal("CreationDate"))
            End If

            If Not dr.IsDBNull(dr.GetOrdinal("Description")) Then
                asbBO.Description = dr.GetString(dr.GetOrdinal("Description"))
            End If

            If Not dr.IsDBNull(dr.GetOrdinal("STATUS")) Then
                asbBO.ItemStatus = dr.GetString(dr.GetOrdinal("STATUS"))
            End If

            If Not dr.IsDBNull(dr.GetOrdinal("Category")) Then
                asbBO.CategoryText = dr.GetString(dr.GetOrdinal("Category"))
            End If
            
        End Sub

#End Region

#Region "GetPreviousComplaints"

        Public Function GetPreviousComplaints(ByRef objComBO As ComplaintBO) As ComplaintList
            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetPreviousComplaints

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create ComplaintBO parameters
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", objComBO.CustomerId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(customerId)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            Dim dr As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            Dim lstComplaintBO As New ComplaintList
            While dr.Read
                Dim objComplaintBO As New ComplaintBO
                FillComplaintBO(dr, objComplaintBO)
                lstComplaintBO.Add(objComplaintBO)
            End While

            objComBO.IsFlagStatus = True
            Return lstComplaintBO

        End Function

#End Region

#Region "FillComplaintBO"

        Private Sub FillComplaintBO(ByRef dr As IDataReader, ByRef compBO As ComplaintBO)

            If Not dr.IsDBNull(dr.GetOrdinal("CreationDate")) Then
                compBO.CreationDate = dr.GetDateTime(dr.GetOrdinal("CreationDate"))
            End If

            If Not dr.IsDBNull(dr.GetOrdinal("Description")) Then
                compBO.Description = dr.GetString(dr.GetOrdinal("Description"))
            End If


            If Not dr.IsDBNull(dr.GetOrdinal("Category")) Then
                compBO.CategoryText = dr.GetString(dr.GetOrdinal("Category"))
            End If

          
        End Sub

#End Region

#Region "ReportAbandonedPropertyVehicle"

        Public Sub reportAbandoned(ByRef objAbandBO As AbandonBO)
            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.ReportAbandonedRequest

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", objAbandBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", objAbandBO.Description, DbType.String)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", objAbandBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", objAbandBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", objAbandBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", objAbandBO.ItemNatureId, DbType.Int32)

            Dim Loacation As ParameterBO = New ParameterBO("Location", objAbandBO.Location, DbType.String)
            Dim LookUpValueID As ParameterBO = New ParameterBO("LookUpValueID", objAbandBO.LookUpValueID, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)
            inParamList.Add(Loacation)
            inParamList.Add(LookUpValueID)

            ''Output parameters
            Dim asbID As ParameterBO = New ParameterBO("AbandonedId", Nothing, DbType.Int32)
            outParamList.Add(asbID)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                objAbandBO.IsFlagStatus = False
                objAbandBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                objAbandBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "GetTerminationInfo"
        Public Sub GetTerminationInfo(ByRef custAddressBO As CustomerAddressBO, ByRef terminBO As TerminationBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressTermination

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", terminBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillTerminationBO(myDataReader, terminBO)

                '' TO DO: GetCustomerAddress method is already exists. Using Polymorphism, need to move
                '' this code on
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
                    custAddressBO.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
                    custAddressBO.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
                    custAddressBO.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
                    custAddressBO.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
                    custAddressBO.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PostCode")) Then
                    custAddressBO.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("PostCode"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("COUNTY")) Then
                    custAddressBO.County = myDataReader.GetString(myDataReader.GetOrdinal("COUNTY"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEL")) Then
                    custAddressBO.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("TEL"))
                End If

                terminBO.IsFlagStatus = True
            Else
                terminBO.IsFlagStatus = False
            End If

        End Sub
        Private Sub FillTerminationBO(ByRef myDataReader As IDataReader, ByRef terminBO As TerminationBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                terminBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MovingOutDate")) Then
                terminBO.MovingOutDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("MovingOutDate"))
            End If

        End Sub
#End Region

#Region "SendHouseMoveRequest"

        Public Sub SendHouseMoveRequest(ByRef moveHouseBO As HouseMoveBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.ReportHouseMove

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create MoveHouseBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", moveHouseBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", moveHouseBO.Description, DbType.String)
            ' Dim removeFlag As ParameterBO = New ParameterBO("RemoveFlag", moveHouseBO.RemoveFlag, DbType.Int32)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", moveHouseBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", moveHouseBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", moveHouseBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", moveHouseBO.ItemNatureId, DbType.Int32)

            Dim MoveToTown As ParameterBO = New ParameterBO("LocalAuthorityID", moveHouseBO.LocalAuthorityID, DbType.Int32)
            Dim StreetName As ParameterBO = New ParameterBO("DevelopmentID", moveHouseBO.DevelopmentID, DbType.Int32)
            Dim NoOfBedrooms As ParameterBO = New ParameterBO("NoOfBedrooms", moveHouseBO.NoOfBedrooms, DbType.Int32)
            Dim OccupantsNoGreater18 As ParameterBO = New ParameterBO("OccupantsNoGreater18", moveHouseBO.OccupantsNoGreater18, DbType.Int32)
            Dim OccupantsNoBelow18 As ParameterBO = New ParameterBO("OccupantsNoBelow18", moveHouseBO.OccupantsNoBelow18, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)
            inParamList.Add(MoveToTown)
            inParamList.Add(StreetName)
            inParamList.Add(NoOfBedrooms)
            inParamList.Add(OccupantsNoGreater18)
            inParamList.Add(OccupantsNoBelow18)

            ''Output parameters
            Dim ParkingSpaceId As ParameterBO = New ParameterBO("TransferID", Nothing, DbType.Int32)
            outParamList.Add(ParkingSpaceId)

            '' Passing Array of HouseMoveBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TransferId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                moveHouseBO.IsFlagStatus = False
                moveHouseBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                moveHouseBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "SendClearArrearRequest"
        Public Sub SendClearArrearRequest(ByRef enqLogBO As EnquiryLogBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.AddClearArrears

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", enqLogBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", enqLogBO.Description, DbType.String)
            ' Dim removeFlag As ParameterBO = New ParameterBO("RemoveFlag", enqLogBO.RemoveFlag, DbType.Int32)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", enqLogBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", enqLogBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", enqLogBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", enqLogBO.ItemNatureId, DbType.Int32)

            Dim enqLogId As ParameterBO = New ParameterBO("EnquiryLogId", Nothing, DbType.Int32)
            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            '   inParamList.Add(removeFlag)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)

            outParamList.Add(enqLogId)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                enqLogBO.IsFlagStatus = False
                enqLogBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                enqLogBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "SendDirectDebitRequest"
        Public Sub SendDirectDebitRequest(ByRef enqLogBO As EnquiryLogBO)

            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.AddDirectDebit

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim creationDate As ParameterBO = New ParameterBO("CreationDate", enqLogBO.CreationDate, DbType.Date)
            Dim description As ParameterBO = New ParameterBO("Description", enqLogBO.Description, DbType.String)
            '  Dim removeFlag As ParameterBO = New ParameterBO("RemoveFlag", enqLogBO.RemoveFlag, DbType.Int32)
            Dim itemStatusId As ParameterBO = New ParameterBO("ItemStatusID", enqLogBO.ItemStatusId, DbType.Int32)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyID", enqLogBO.TenancyId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", enqLogBO.CustomerId, DbType.Int32)
            Dim itemNatureId As ParameterBO = New ParameterBO("ItemNatureID", enqLogBO.ItemNatureId, DbType.Int32)

            Dim enqLogId As ParameterBO = New ParameterBO("EnquiryLogId", Nothing, DbType.Int32)
            '' Adding object to paramList
            inParamList.Add(creationDate)
            inParamList.Add(description)
            ' inParamList.Add(removeFlag)
            inParamList.Add(itemStatusId)
            inParamList.Add(tenancyId)
            inParamList.Add(customerId)
            inParamList.Add(itemNatureId)

            outParamList.Add(enqLogId)

            '' Passing Array of TerminationBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                enqLogBO.IsFlagStatus = False
                enqLogBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                enqLogBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "Load Enquiry Log DataSet"
        Public Sub EnquiryLogData(ByRef enquiryDS As DataSet, ByRef enqryLogSearchBO As EnquiryLogSearchBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim tenancyParam As ParameterBO

            '' rowindex, pgaeindex and sortby will be common to both type of searches
            tenancyParam = New ParameterBO("noOfRows", enqryLogSearchBO.RowCount, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("offSet", enqryLogSearchBO.PageIndex, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortColumn", enqryLogSearchBO.SortBy, DbType.String)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortOrder", enqryLogSearchBO.SortOrder, DbType.String)
            paramList.Add(tenancyParam)


            '' if search is based on username tenancyid date nature etc thn we will get
            '' these values from  EnquiryLogSearchBO object
            If enqryLogSearchBO.IsSearch Then
                If enqryLogSearchBO.TenancyID <> "" Then
                    tenancyParam = New ParameterBO("tenancyID", CType(enqryLogSearchBO.TenancyID, Int32), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                tenancyParam = New ParameterBO("firstName", enqryLogSearchBO.FirstName, DbType.String)
                paramList.Add(tenancyParam)
                tenancyParam = New ParameterBO("lastName", enqryLogSearchBO.LastName, DbType.String)
                paramList.Add(tenancyParam)

                If enqryLogSearchBO.DateValue <> "12:00:00 AM" Then
                    tenancyParam = New ParameterBO("date", enqryLogSearchBO.DateValue, DbType.Date)
                    paramList.Add(tenancyParam)
                End If

                If enqryLogSearchBO.Nature <> "" Then
                    tenancyParam = New ParameterBO("nature", CType(enqryLogSearchBO.Nature, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If enqryLogSearchBO.Status <> "" Then
                    tenancyParam = New ParameterBO("status", CType(enqryLogSearchBO.Status, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                tenancyParam = New ParameterBO("text", enqryLogSearchBO.TextValue, DbType.String)
                paramList.Add(tenancyParam)
                sprocName = SprocNameConstants.EnquiryLogSearch
            Else
                sprocName = SprocNameConstants.EnquiryLogSelectAllList
            End If


            MyBase.LoadDataSet(enquiryDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and w'll display user msg
            If enquiryDS.Tables(0).Rows.Count = 0 Then

                enqryLogSearchBO.IsFlagStatus = False
                enqryLogSearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                enqryLogSearchBO.IsFlagStatus = True
            End If
        End Sub

        Public Function GetEnquiryLogRowCount() As Integer

            Dim sprocName As String = SprocNameConstants.GetRowCount

            Dim paramList As ParameterList = New ParameterList()

            '' used to return no. of results found
            Dim rowCount As Integer = 0

            Dim tenancyParam As ParameterBO

            tenancyParam = New ParameterBO("tableName", "TO_ENQUIRY_LOG", DbType.String)
            paramList.Add(tenancyParam)


            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)


            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
                ''  custHeadBO.isFlagStatus = True
            Else
                '' If No record found
                '' custHeadBO.IsFlagStatus = False

            End If


            Return rowCount

        End Function

        Public Function GetEnquiryLogSearchRowCount(ByRef enqryLogSearchBO As EnquiryLogSearchBO) As Integer

            Dim sprocName As String = SprocNameConstants.GetSearchRowCount

            Dim paramList As ParameterList = New ParameterList()

            '' used to return no. of results found
            Dim rowCount As Integer = 0

            Dim tenancyParam As ParameterBO

            If enqryLogSearchBO.TenancyID <> "" Then
                tenancyParam = New ParameterBO("tenancyID", CType(enqryLogSearchBO.TenancyID, Int32), DbType.Int32)
                paramList.Add(tenancyParam)
            End If

            tenancyParam = New ParameterBO("firstName", enqryLogSearchBO.FirstName, DbType.String)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("lastName", enqryLogSearchBO.LastName, DbType.String)
            paramList.Add(tenancyParam)

            If enqryLogSearchBO.DateValue <> "12:00:00 AM" Then
                tenancyParam = New ParameterBO("date", enqryLogSearchBO.DateValue, DbType.Date)
                paramList.Add(tenancyParam)
            End If

            If enqryLogSearchBO.Nature <> "" Then
                tenancyParam = New ParameterBO("nature", CType(enqryLogSearchBO.Nature, Integer), DbType.Int32)
                paramList.Add(tenancyParam)
            End If

            If enqryLogSearchBO.Status <> "" Then
                tenancyParam = New ParameterBO("status", CType(enqryLogSearchBO.Status, Integer), DbType.Int32)
                paramList.Add(tenancyParam)
            End If

            tenancyParam = New ParameterBO("text", enqryLogSearchBO.TextValue, DbType.String)
            paramList.Add(tenancyParam)


            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)


            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
                ''  custHeadBO.isFlagStatus = True
            Else
                '' If No record found
                '' custHeadBO.IsFlagStatus = False

            End If


            Return rowCount

        End Function

#End Region

#Region "GetRowCount Function"

        '' Private method used to return no. of results
        Private Function GetRowCount(ByRef myDataRecord As IDataRecord) As Integer

            Return myDataRecord.GetInt32(myDataRecord.GetOrdinal("numOfRows"))

        End Function

#End Region

#Region "Delete Customer Record"
        Public Sub DeleteCustomerEnquiry(ByRef termBO As TerminationBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.DeleteCustomerEnquiry

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters enquiryLogID
            Dim enqryLogID As ParameterBO = New ParameterBO("enquiryLogID", termBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enqryLogID)

            Dim outParamList As ParameterList = New ParameterList()

            Dim rowCount As Integer = 0

            '' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            rowCount = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            termBO.IsFlagStatus = True
        End Sub
#End Region

#Region "GetCustomerIDByEnquiryLog"
        Public Sub GetCustomerIDByEnquiryLog(ByRef enqryLog As EnquiryLogBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.GetCustomerIDByEnquiryLog

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters enquiryLogID
            Dim enqryLogID As ParameterBO = New ParameterBO("enquiryLogID", enqryLog.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enqryLogID)

            Dim outParamList As ParameterList = New ParameterList()

            Dim rowCount As Integer = 0

            '' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerID")) Then
                    enqryLog.CustomerId = myDataReader.GetInt32(myDataReader.GetOrdinal("CustomerID"))
                End If

                enqryLog.IsFlagStatus = True
            Else
                enqryLog.IsFlagStatus = False
            End If
        End Sub
#End Region

#Region "MessageAlertSystem"

#Region "GetCustomerEnquiryCount"

        Public Sub GetCustomerEnquiryCount(ByRef enqBO As EnquiryLogBO)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerEnquiryCount

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerID As ParameterBO = New ParameterBO("CustomerId", enqBO.CustomerId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(customerID)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerEnquiries")) Then
                    enqBO.CustomerEnquiryCount = myDataReader.GetInt32(myDataReader.GetOrdinal("CustomerEnquiries"))
                End If
                enqBO.IsFlagStatus = True
            Else
                enqBO.IsFlagStatus = False
            End If

        End Sub

#End Region

#Region "GetEnquiryResponseCount"

        Public Sub GetEnquiryResponseCount(ByRef enqBO As EnquiryLogBO)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetEnquiryResponseCount

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerID As ParameterBO = New ParameterBO("CustomerId", enqBO.CustomerId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(customerID)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ResponseCount")) Then
                    enqBO.EnquiryResponseCount = myDataReader.GetInt32(myDataReader.GetOrdinal("ResponseCount"))
                End If
                enqBO.IsFlagStatus = True
            Else
                enqBO.IsFlagStatus = False
            End If

        End Sub

#End Region

#Region "GetTerminationCount"

        Public Sub GetTerminationCount(ByRef enqBO As EnquiryLogBO)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetTerminationCount

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerID As ParameterBO = New ParameterBO("CustomerId", enqBO.CustomerId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(customerID)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TerminationCount")) Then
                    enqBO.TerminationCount = myDataReader.GetInt32(myDataReader.GetOrdinal("TerminationCount"))
                End If
                enqBO.IsFlagStatus = True
            Else
                enqBO.IsFlagStatus = False
            End If

        End Sub

#End Region

#Region "GetTerminationDate"

        Public Sub GetTerminationDate(ByRef enqBO As EnquiryLogBO)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetTerminationDate

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerID As ParameterBO = New ParameterBO("CustomerId", enqBO.CustomerId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(customerID)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TerminationDate")) Then
                    enqBO.TerminationDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("TerminationDate"))
                End If
                enqBO.IsFlagStatus = True
            Else
                enqBO.IsFlagStatus = False
            End If

        End Sub

#End Region

#Region "GetCustomerEnquiries"

        Public Function GetCustomerEnquiries(ByVal customerID As Integer, ByVal responseStatus As String) As DataSet

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerEnquiries

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", customerID, DbType.Int32)

            '' Create EnquiryLogBO parameters
            Dim responseStatuseParam As ParameterBO
            If responseStatus.Equals("All") Then
                responseStatuseParam = New ParameterBO("ResponseStatus", Nothing, DbType.String)
            Else
                responseStatuseParam = New ParameterBO("ResponseStatus", responseStatus, DbType.String)
            End If

            '' Adding object to paramList
            inParamList.Add(customerIdParam)
            inParamList.Add(responseStatuseParam)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            Dim dsEnq As DataSet = New DataSet
            MyBase.LoadDataSet(dsEnq, inParamList, sprocName)

            Return dsEnq

        End Function

#End Region

#Region "GetEnquiryResponses"

        Public Function GetEnquiryResponses(ByRef enqLogBO As EnquiryLogBO) As JournalList

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetEnquiryResponse

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim enqIdParam As ParameterBO = New ParameterBO("EnquiryLogID", enqLogBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enqIdParam)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)
            Dim lstJournal As New JournalList

            While myDataReader.Read
                Dim jBo As New JournalBO

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LASTACTIONDATE")) Then
                    jBo.LastActionDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("LASTACTIONDATE"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NOTES")) Then
                    jBo.Notes = myDataReader.GetString(myDataReader.GetOrdinal("NOTES"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HistoryId")) Then
                    jBo.HistoryID = myDataReader.GetInt32(myDataReader.GetOrdinal("HistoryId"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ENQID")) Then
                    jBo.EnquiryId = myDataReader.GetInt32(myDataReader.GetOrdinal("ENQID"))
                End If

                If myDataReader.IsDBNull(myDataReader.GetOrdinal("LastActionUser")) Then
                    jBo.LastActionUser = -1
                Else
                    jBo.LastActionUser = myDataReader.GetInt32(myDataReader.GetOrdinal("LastActionUser"))
                End If

                lstJournal.Add(jBo)

            End While

            Return lstJournal

        End Function

#End Region

#Region "AddCustomerResponse"

        Public Sub AddCustomerResponse(ByRef custResponse As CustomerResponseBO)
           '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.AddCutomerResponse

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            ''Input params
            Dim enqId As ParameterBO = New ParameterBO("EnquiryID", custResponse.EnquiryId, DbType.Int32)
            Dim notes As ParameterBO = New ParameterBO("Notes", custResponse.Notes, DbType.String)
            Dim creationDate As ParameterBO = New ParameterBO("ResponseDate", custResponse.ResponseDate, DbType.Date)
            ''Output params
            Dim custResponseId As ParameterBO = New ParameterBO("CustomerResponseId", Nothing, DbType.Int32)

            '' Adding object to inParamList
            inParamList.Add(enqId)
            inParamList.Add(notes)
            inParamList.Add(creationDate)

            ''Adding object outParmaList
            outParamList.Add(custResponseId)

            '' Passing Array of params and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                custResponse.IsFlagStatus = False
                custResponse.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else
                custResponse.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "GetLetter"

        Public Sub GetLetter(ByVal storedProcedure As String, ByRef objLetterBO As LetterBO)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = storedProcedure

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create LetterBO parameters
            Dim enqIdParam As ParameterBO = New ParameterBO("HistoryID", objLetterBO.HistoryID, DbType.Int32)


            '' Adding object to paramList
            inParamList.Add(enqIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LetterContents")) Then
                    objLetterBO.LetterContents = myDataReader.GetString(myDataReader.GetOrdinal("LetterContents"))
                End If
            End If

        End Sub

#End Region

#End Region

#End Region

    End Class

End Namespace

