﻿
'' Class Name -- PayThruDAL
'' Base Class -- BaseDAL
'' Summary --    Contains Functionality related to Paythru
'' Created By  -- Simon Rogers
'' Created on --  9th September 2014
''---------------------------------
Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess

    Public Class PaythruDAL : Inherits BaseDAL

#Region "Functions"

#End Region

#Region "Methods"
#Region "GetConfigValue"
        Sub GetConfigValue(ByRef PaythruBO As PaythruBO)

            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetPaythruConfigValue

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim settingName As ParameterBO = New ParameterBO("settingName", PaythruBO.ConfigSettingName, DbType.String)

            ' Adding object to paramList
            inParamList.Add(settingName)


            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ' If record found 
            If myReader.Read Then

                If Not myReader.IsDBNull(myReader.GetOrdinal("SettingValue")) Then
                    PaythruBO.ConfigSettingValue = myReader.GetString(myReader.GetOrdinal("SettingValue"))
                End If
                PaythruBO.IsFlagStatus = True

            Else
                ' If No record found
                PaythruBO.IsFlagStatus = False

            End If

        End Sub
#End Region

#End Region



    End Class

End Namespace