Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class ChangeEmail
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        ''Set the attribute so that when user presses enter key the specified event for the mentioned button will be fired
        txtNewEmail.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSignIn.UniqueID + "').click();return false;}} else {return true}; ")

        Me.txtOldEmail.Focus()

    End Sub

    Protected Sub btnSignIn_Click(sender As Object, e As EventArgs) Handles btnSignIn.Click
        '' If customer wishes to signin as well as change his/her email (username), this block gets fired
        Me.ChangeEmail()
    End Sub

#End Region

#Region "Methods"

#Region "ChangeEmail"

    Private Sub ChangeEmail()

        ''  User will provide current username(old email), password and new username (new email)
        REM that will be wrapped inside CustomerHeaderBO


        Dim custHeadBO As CustomerHeaderBO = New CustomerHeaderBO()

        '' Setting current username i.e. oldEmail
        '' This will be used for verification, if gets verified then user alloed to change his/her email
        custHeadBO.UserName = txtOldEmail.Text.Trim()

        '' Setting current password, also used for verification
        custHeadBO.Password = txtPassword.Text

        '' Setting new email, If user authenticated, username changed to this email
        custHeadBO.NewUserName = txtNewEmail.Text.Trim()


        '' Call to Business Layer method  
        Dim custMngr As CustomerManager = New CustomerManager()
        custMngr.ChangeEmail(custHeadBO)

        Dim flagStatus As Boolean = False

        flagStatus = custHeadBO.IsFlagStatus

        '' If Email changed, customer info stored in session and redirected to Home page
        If (flagStatus) Then

            Me.StoreInSession(custHeadBO)
        Else
            Me.lblErrorMessage.Text = UserInfoMsgConstants.IncorrectEmailMsg
        End If


    End Sub

#End Region

#Region "StoreInSession"

    Private Sub StoreInSession(ByRef custHeadBO As CustomerHeaderBO)

        '' storing custHeadBO object in Session so that it can be used across pages
        Session("custHeadBO") = custHeadBO

        Response.Redirect("~/secure/home.aspx")


    End Sub

#End Region

#End Region

#Region "Functions"

    ''No function defined yet...

#End Region

End Class