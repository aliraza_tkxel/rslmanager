﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    CodeFile="display_fault_list.aspx.vb" Inherits="DisplayFaultList" 
    Title="Tenants Online :: Fault Locator" EnableViewState="true" %>
<%@ Register Src="../../user control/BreadCrumbControl.ascx" TagName="BreadCrumbControl"
    TagPrefix="uc2" %>  
  
  
  <%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>
    
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">   
 
    <table cellpadding="3" class="table_content">
        <tr>
            <td align="left" colspan="2" valign="top">
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Text="I would like to report a fault" style="float:left"></asp:Label>
                    <asp:Button ID="btnBack" runat="server" Text=" < Back" style="float:right;"/>
                <br />
                    <hr style="width: 538px" />
            </td>

            <td valign="top" style ="background-color:White; border:0px;" rowspan="7">
                 </td>            

        </tr>
        <tr>
            <td align="left" style="width: 432px">
                <asp:Label ID="lblYouHaveSelected" runat="server" Text="You have selected:" Width="447px" CssClass="caption"></asp:Label></td>
            <td align="left" style="width: 75px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" style="color: #ffffff; background-color: black">
                <uc2:BreadCrumbControl ID="BreadCrumbControl" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 432px; height: 25px;">
                <asp:Label ID="lblSelectProblem" runat="server" Text="Now select the problem from the list:" Width="455px" CssClass="caption"></asp:Label></td>
            <td align="left" style="width: 75px; height: 25px;">
            </td>
        </tr>
        <tr>            
            <td align="left" style="width: 75px" colspan="2">
                <asp:RequiredFieldValidator ID="valRequiredFault" runat="server" ControlToValidate="rdBtnFaultList"
                    Display="Dynamic" ErrorMessage="Select the fault below" SetFocusOnError="True"
                    ToolTip="Select the fault below" CssClass="caption" Width="164px"></asp:RequiredFieldValidator></td>
        </tr>        
        <tr>
            <td align="left" style="width: 432px; height: 51px;">                
                <asp:RadioButtonList ID="rdBtnFaultList" runat="server" CssClass="caption">
                </asp:RadioButtonList>&nbsp;
            </td>
            <td align="left" style="width: 75px; height: 51px;"></td>
            
        </tr>
        <tr>
            <td align="left" style="width: 432px">
                &nbsp;</td>
            <td align="left" style="width: 75px">
                <asp:Button ID="btnNext" runat="server" Text="Next > " UseSubmitBehavior="True" /></td>
        </tr>        
    </table> 
</asp:Content>
