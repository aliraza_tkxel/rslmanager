﻿Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.Data

Partial Public Class SubmitFaultBasket
    Inherits System.Web.UI.Page

#Region "Page_Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

        Response.Cache.SetNoStore()

        'Get the Total Faults Submitted by user
        Dim totalReportedFaults As Integer = CType(Session(FaultConstants.TotalReportedFaults), Integer)

        'If session doesnot contain any record that means there's no fault in fault basket 
        'then redirect the user
        If totalReportedFaults <= 0 Then

            Response.Redirect("~/secure/faultlocator/report_fault.aspx")

        End If



    End Sub
#End Region

#Region "Page_Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Me.LoadTempFaultBasket()

            Me.getCustomerInfo()

        End If

    End Sub

#End Region

#Region "Events"

#Region "btnSubmit_Click"

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Me.saveFinalFaultBasket()

    End Sub

#End Region

#End Region

#Region "LoadTempFaultBasket"
    Protected Sub LoadTempFaultBasket()

        'Create the TempFaultBO
        Dim tempFaultBO As TempFaultBO = New TempFaultBO()

        'Creat the object of business layer
        Dim faultBL As FaultManager = New FaultManager()

        'Create the obj of customer header bo
        Dim headerBO As CustomerHeaderBO = Nothing

        Dim tempFaultDS As DataSet = New DataSet()
        Try
            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            tempFaultBO.CustomerId = headerBO.CustomerID


            faultBL.GetTempFaultBasket(tempFaultBO, tempFaultDS)
        Catch ex As Exception
            ' Do nothing
        End Try

        'Save the number of records in text field
        If TempFaultBO.IsFlagStatus = True And tempFaultDS.Tables(0).Rows.Count > 0 Then

            grdTempFaultBasket.DataSource = tempFaultDS
            grdTempFaultBasket.DataBind()
        Else
            Response.Redirect("~/secure/faultlocator/report_fault.aspx")
        End If


    End Sub

#End Region

#Region "GetCustomerInfo"

    ''Get the customer Info to be displayed on welcome page
    Private Sub getCustomerInfo()
        Try

            Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()

            '' Obtaining object from Session
            Dim custHeadBO As CustomerHeaderBO = Nothing
            custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)


            '' Passing info to custDetailsInfo, that will be used to populate customer's Home page
            custDetailsBO.Id = CType(custHeadBO.CustomerID, Integer)
            custDetailsBO.Tenancy = custHeadBO.TenancyID

            Dim custAddressBO As New CustomerAddressBO
            custDetailsBO.Address = custAddressBO

            Dim objBL As New CustomerManager()

            Dim resultStatus As Boolean = objBL.GetCustomerContactDetail(custDetailsBO)

            If resultStatus Then

                setPresentationLables(custHeadBO, custDetailsBO)

            End If

        Catch ex As Exception
            ' Do nothing
        End Try

    End Sub
#End Region

#Region "setPresentationLables"

    Private Sub setPresentationLables(ByRef custHeadBO As CustomerHeaderBO, ByRef custDetailsBO As CustomerDetailsBO)
        'Set Presentation lables from CustomerDetailsBO object
        Me.lblTenantName.Text = custHeadBO.Title & " " & custDetailsBO.FirstName & " " & custDetailsBO.MiddleName & " " & custDetailsBO.LastName
        Me.lblAddress.Text = IIf(custDetailsBO.Address.HouseNumber <> "", custDetailsBO.Address.HouseNumber, "")
        Me.lblAddress.Text &= " " & IIf(custDetailsBO.Address.Address1 <> "", custDetailsBO.Address.Address1, "")
        Me.lblTown.Text = IIf(custDetailsBO.Address.TownCity <> "", custDetailsBO.Address.TownCity, "")
        Me.lblCountry.Text = IIf(custDetailsBO.Address.County <> "", custDetailsBO.Address.County, "")
        Me.lblTelephone.Text = IIf(custDetailsBO.Address.Telephone <> "", custDetailsBO.Address.Telephone, "")
        Me.lblMobile.Text = IIf(custDetailsBO.Address.TelephoneMobile <> "", custDetailsBO.Address.TelephoneMobile, "")
        Me.lblEmail.Text = IIf(custDetailsBO.Address.Email <> "", custDetailsBO.Address.Email, "")

    End Sub

#End Region

#Region "saveFinalFaultBasket"

    Private Sub saveFinalFaultBasket()
        'Create the TempFaultBO
        Dim finalFaultBO As FinalFaultBasketBO = New FinalFaultBasketBO()

        'Creat the object of business layer
        Dim faultBL As FaultManager = New FaultManager()

        'Create the obj of customer header bo
        Dim headerBO As CustomerHeaderBO = Nothing

        Try
            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            finalFaultBO.CustomerId = headerBO.CustomerID

            finalFaultBO.PrefContactTime = Me.txtPreferredContactTime.Text

            finalFaultBO.SubmitDate = DateTime.Now

            finalFaultBO.IamHappy = IIf(Me.chkIamHappy.Checked = True, 1, 0)

            faultBL.saveFinalFaultBasket(finalFaultBO)
        Catch ex As Exception
            ' Do nothing
        End Try

        If Not finalFaultBO.IsFlagStatus Then
            ' Do nothing
        Else
            'Now Faults has been submitted successufully. Clear the total count of 
            'fualt from session and redirec to message page
            Session.Remove(FaultConstants.TotalReportedFaults)
            Response.Redirect("~/secure/faultlocator/fault_submit_success_msg.aspx")
        End If


    End Sub

#End Region

End Class