﻿Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.Data

Partial Public Class TempFaultBasket
    Inherits System.Web.UI.Page

#Region "Page_Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init


        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub
#End Region

#Region "Page_Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Not IsPostBack Then
            'Call the function to get Fault List 
            Me.LoadTempFaultBasket()
        End If

    End Sub

#End Region

#Region "Events"

#Region "btnSubmit_Click"

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Me.txtCountRecords.Text > 0 Then

            Session(FaultConstants.TotalReportedFaults) = Me.txtCountRecords.Text
            Response.Redirect("~/secure/faultlocator/submit_fault_basket.aspx")

        Else

            Me.lblMessage.Text = UserInfoMsgConstants.FaultSubmissionFailureWithNoFaults

        End If

    End Sub

#End Region

#Region "btnAddMore_Click"

    Protected Sub btnAddMore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddMore.Click

        'Create the obj of customer header bo
        Dim headerBO As CustomerHeaderBO = Nothing

        'Get CustomerHeaderBO from session to get CustomerId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Response.Redirect(FaultConstants.LoactionUrlString)

    End Sub

#End Region

#Region "btnDelete_Click"

    Protected Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'casting sender of this event into button object before getting command arguments from
        'button object
        Dim btn As Button = DirectCast(sender, Button)

        'creating array of command arguments send by delete enquiry button
        Dim strArr As String() = btn.CommandArgument.Split(",")

        Dim tempFaultId As Integer = CType(strArr(0), Integer)

        'Call the funciton to delete the record
        Me.deleteTempFault(tempFaultId)

    End Sub

#End Region

#Region "btnUpdate_Click"

    Protected Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        If Me.txtCountRecords.Text > 0 Then

            Me.UpdateFaultQuantity()

        Else

            Me.lblMessage.Text = UserInfoMsgConstants.WithNoFaultUpdateError

        End If



    End Sub

#End Region

#End Region

#Region "LoadTempFaultBasket"
    Protected Sub LoadTempFaultBasket()
        Try
            'Create the TempFaultBO
            Dim tempFaultBO As TempFaultBO = New TempFaultBO()

            'Creat the object of business layer
            Dim faultBL As FaultManager = New FaultManager()

            'Create the obj of customer header bo
            Dim headerBO As CustomerHeaderBO = New CustomerHeaderBO()

            Dim tempFaultDS As DataSet = New DataSet()

            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            tempFaultBO.CustomerId = headerBO.CustomerID

            faultBL.GetTempFaultBasket(tempFaultBO, tempFaultDS)

            grdTempFaultBasket.DataSource = tempFaultDS
            grdTempFaultBasket.DataBind()

            'Save the number of records in text field
            If tempFaultBO.IsFlagStatus Then
                Me.txtCountRecords.Text = tempFaultDS.Tables(0).Rows.Count
            End If

        Catch ex As Exception
            ' Do nothing
        End Try

    End Sub

#End Region

#Region "deleteTempFault"

    Private Sub deleteTempFault(ByRef tempFaultId As Integer)
        Try

            'Create the TempFaultBO
            Dim tempFaultBO As TempFaultBO = New TempFaultBO()

            'Creat the object of business layer
            Dim faultBL As FaultManager = New FaultManager()

            'Create the obj of customer header bo
            Dim headerBO As CustomerHeaderBO = New CustomerHeaderBO()

            Dim tempFaultDS As DataSet = New DataSet()

            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            tempFaultBO.CustomerId = headerBO.CustomerID

            tempFaultBO.TempFaultId = tempFaultId

            faultBL.DeleteTempFault(tempFaultBO)

            Me.LoadTempFaultBasket()

            If tempFaultBO.IsFlagStatus Then

                Me.lblMessage.Text = UserInfoMsgConstants.FaultDeletionSuccess

                Me.txtCountRecords.Text = tempFaultBO.CountRecords

            Else

                tempFaultBO.UserMsg = UserInfoMsgConstants.FaultDeletionFailure

            End If

        Catch ex As Exception
            ' Do nothing
        End Try

    End Sub

#End Region

#Region "UpdateFaultQuantity"

    Private Sub UpdateFaultQuantity()
        Try
            'Create the TempFaultBO
            Dim tempFaultBO As TempFaultBO = New TempFaultBO()

            'Creat the object of business layer
            Dim faultBL As FaultManager = New FaultManager()

            Dim ddlQuantity As DropDownList = New DropDownList()

            Dim lblTempFaultId

            Dim tempFaultId As Integer = New Integer()

            Dim row As GridViewRow

            For Each row In grdTempFaultBasket.Rows

                ddlQuantity = row.FindControl("ddlQuantity")
                lblTempFaultId = row.FindControl("lblTempFaultId")
                tempFaultBO.TempFaultId = lblTempFaultId.Text()
                tempFaultBO.Quantity = ddlQuantity.SelectedItem.Value
                faultBL.UpdateFaultQuantity(tempFaultBO)

                If Not tempFaultBO.IsFlagStatus Then

                    Me.lblMessage.CssClass = "error_message_label"
                    Me.lblMessage.Text = tempFaultBO.UserMsg
                    Me.LoadTempFaultBasket()
                    Exit For
                End If

            Next

            If tempFaultBO.IsFlagStatus Then

                Me.lblMessage.CssClass = "info_message_label"
                Me.lblMessage.Text = UserInfoMsgConstants.FaultUpdationSuccess
                Me.LoadTempFaultBasket()

            End If

        Catch ex As Exception
            ' Do nothing
        End Try

    End Sub

#End Region

End Class