﻿<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="bedroom_fault.aspx.vb" Inherits="bedroom_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>

<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 

<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">

<script type="text/javascript" language ="javascript">
    var imageList = new Array(10);
    //Load all images on page load
    var bedroomMainImage = new Image();
    bedroomMainImage.src = "../../../../images/propertyImages/internal/bedroom/fl_bedroom_main.gif";
    var ceilingImage = new Image();
    ceilingImage.src = "../../../../images/propertyImages/internal/bedroom/ceiling_01.gif";
    imageList[0] = ceilingImage;
    var smokedetectorImage = new Image();
    smokedetectorImage.src = "../../../../images/propertyImages/internal/bedroom/smokedetector_01.gif";
    imageList[1] = smokedetectorImage;
    var windowImage = new Image();
    windowImage.src = "../../../../images/propertyImages/internal/bedroom/window_01.gif";
    imageList[2] = windowImage;
    var electricsImage = new Image();
    electricsImage.src = "../../../../images/propertyImages/internal/bedroom/electrics_01.gif";
    imageList[3] = electricsImage;
    var wallImage = new Image();
    wallImage.src = "../../../../images/propertyImages/internal/bedroom/wall_01.gif";
    imageList[4] = wallImage;
    var alarmsensorImage = new Image();
    alarmsensorImage.src = "../../../../images/propertyImages/internal/bedroom/alarmsensor_01.gif";
    imageList[5] = alarmsensorImage;
    var lightingImage = new Image();
    lightingImage.src = "../../../../images/propertyImages/internal/bedroom/lighting_01.gif";
    imageList[6] = lightingImage;
    var radiatorImage = new Image();
    radiatorImage.src = "../../../../images/propertyImages/internal/bedroom/radiator_01.gif";
    imageList[7] = radiatorImage;
    var doorImage = new Image();
    doorImage.src = "../../../../images/propertyImages/internal/bedroom/door_01.gif";
    imageList[8] = doorImage;
    var floorImage = new Image();
    floorImage.src = "../../../../images/propertyImages/internal/bedroom/floor.gif";
    imageList[9] = floorImage;




    //function called on click event of listbox
    function changeImageSourceUsingList(sender) {
        changeImageSource(sender.selectedIndex);
    }

    //function called onmouseover of image map and change the image source 
    //according to input paramater		
    function changeImageSource(index) {
        document.getElementById("bedroom").src = imageList[index].src;
    }

    //function called onmouseout of image map and set the main bedroom image 
    function changeToMain() {
        document.getElementById("bedroom").src = bedroomMainImage.src;
    }
</script>

<table cellpadding="3" cellspacing="0" border="0">
 <tr><td class="elementimages_btnback" colspan="2">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
    <td style="height: 435px">
        <img src="../../../../images/propertyImages/internal/bedroom/fl_bedroom_main.gif"  width="546" height="431" border="0" usemap="#Map" id="bedroom" alt=""  />
        <map name="Map" id="bedroomMap" >
            <area shape="poly" coords="382,61,388,58,390,55,392,58,399,60,399,70,398,95,395,97,388,97,383,95,382,63" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>"  />
            
            <area shape="poly" coords="0,16,0,0,542,4,532,15,388,49,264,49,266,42,262,34,255,30,242,28,232,29,223,33,217,41,218,49,74,49" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
            
            <area shape="poly" coords="531,39,523,38,456,49,457,429,470,429,532,430" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(8).ToString() %>" onmouseover="changeImageSource('8')" onmouseout="changeToMain()" alt="<%= GetElementName(8).ToString() %>" title="<%= GetElementName(8).ToString()  %>" />
            
            <area shape="poly" coords="82,260,102,261,101,281,81,280" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(3).ToString() %>" onmouseover="changeImageSource('3')" onmouseout="changeToMain()" alt="<%= GetElementName(3).ToString() %>" title="<%= GetElementName(3).ToString()  %>" />
            
            <area shape="poly" coords="458,430,0,430,1,374,81,316,82,280,102,281,102,297,104,300,110,299,111,291,118,292,114,297,114,304,99,355,107,351,115,350,120,350,120,375,333,375,333,348,345,334,351,334,355,336,356,328,353,314,348,303,341,292,348,292,348,298,356,297,356,283,384,284,395,291,394,295,458,337" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(9).ToString() %>" onmouseover="changeImageSource('9')" onmouseout="changeToMain()" alt="<%= GetElementName(9).ToString() %>" title="<%= GetElementName(9).ToString()  %>" />
            
            <area shape="poly" coords="114,188,151,190,149,210,135,210,134,215,141,216,141,239,136,242,123,242,124,215,130,215,130,210,114,209" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>" />
            <area shape="poly" coords="315,189,350,189,350,211,334,210,333,214,339,215,340,242,327,242,322,239,323,215,330,215,330,210,315,209" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>" />
       
            <area shape="poly" coords="457,226,421,217,394,218,395,296,457,337" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>" />
            
            <area shape="poly" coords="219,39,223,35,231,31,243,29,253,31,262,35,266,42,265,49,257,52,245,54,236,54,228,53,222,50,219,46" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" />
            
            <area shape="poly" coords="531,16,391,50,265,50,254,53,240,54,227,53,220,50,75,49,77,251,85,251,85,255,81,257,81,260,102,260,103,245,115,239,123,240,125,215,129,215,129,211,115,210,115,189,150,189,149,210,136,210,135,215,141,215,141,240,152,240,163,234,170,226,175,224,209,225,230,221,230,226,240,218,250,225,285,225,299,238,300,243,296,245,308,258,307,241,321,238,322,216,327,214,327,211,315,209,315,190,350,188,349,210,334,211,334,215,340,216,340,243,343,237,356,245,357,284,387,283,388,99,382,94,382,62,390,59,400,60,399,96,389,100,388,285,396,290,394,220,422,217,457,227,455,187,439,184,439,166,455,166,457,50,532,38" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>" />
        
            <area shape="poly" coords="1,35,70,58,69,77,66,88,65,99,71,109,68,117,69,125,73,134,70,144,70,152,67,161,74,176,70,184,76,194,75,228,70,242,69,250,62,249,56,249,44,249,37,249,1,262" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(2).ToString() %>" onmouseover="changeImageSource('2')" onmouseout="changeToMain()" alt="<%= GetElementName(2).ToString() %>" title="<%= GetElementName(2).ToString()  %>"  />
        </map>
   </td>
<td valign="top" style ="background-color:White; border:0px;" rowspan="3">
                     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox>
                     </td>
  </tr>
     </table>
</asp:Content>