﻿<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="kitchen_fault.aspx.vb" Inherits="kitchen_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>

<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 

<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">
<script type="text/javascript" language ="javascript">
    var imageList = new Array(17);
    //Load all images on page load
    var kitchenMainImage = new Image();
    kitchenMainImage.src = "../../../../images/propertyImages/internal/kitchen/fl_kitchen_main.gif";
    var sinkunitImage = new Image();
    sinkunitImage.src = "../../../../images/propertyImages/internal/kitchen/sinkunit_01.gif";
    imageList[0] = sinkunitImage;
    var doorImage = new Image();
    doorImage.src = "../../../../images/propertyImages/internal/kitchen/door_01.gif";
    imageList[1] = doorImage;
    var baseunitImage = new Image();
    baseunitImage.src = "../../../../images/propertyImages/internal/kitchen/baseunit_01.gif";
    imageList[2] = baseunitImage;
    var boilerImage = new Image();
    boilerImage.src = "../../../../images/propertyImages/internal/kitchen/boiler_01.gif";
    imageList[3] = boilerImage;
    var ceilingImage = new Image();
    ceilingImage.src = "../../../../images/propertyImages/internal/kitchen/ceiling_01.gif";
    imageList[4] = ceilingImage;
    var cookerImage = new Image();
    cookerImage.src = "../../../../images/propertyImages/internal/kitchen/cooker_01.gif";
    imageList[5] = cookerImage;
    var cookerhoodImage = new Image();
    cookerhoodImage.src = "../../../../images/propertyImages/internal/kitchen/cookerhood_01.gif";
    imageList[6] = cookerhoodImage;
    var electricImage = new Image();
    electricImage.src = "../../../../images/propertyImages/internal/kitchen/electrics_01.gif";
    imageList[7] = electricImage;
    var extractorImage = new Image();
    extractorImage.src = "../../../../images/propertyImages/internal/kitchen/extractor_01.gif";
    imageList[8] = extractorImage;
    var floorImage = new Image();
    floorImage.src = "../../../../images/propertyImages/internal/kitchen/floor_01.gif";
    imageList[9] = floorImage;
    var freezerImage = new Image();
    freezerImage.src = "../../../../images/propertyImages/internal/kitchen/freezer_01.gif";
    imageList[10] = freezerImage;
    var lightingImage = new Image();
    lightingImage.src = "../../../../images/propertyImages/internal/kitchen/lighting_01.gif";
    imageList[11] = lightingImage;
    var refrigeratorImage = new Image();
    refrigeratorImage.src = "../../../../images/propertyImages/internal/kitchen/refrigerator_01.gif";
    imageList[12] = refrigeratorImage;
    var wallImage = new Image();
    wallImage.src = "../../../../images/propertyImages/internal/kitchen/wall_01.gif";
    imageList[13] = wallImage;
    var walltileImage = new Image();
    walltileImage.src = "../../../../images/propertyImages/internal/kitchen/walltiles_01.gif";
    imageList[14] = walltileImage;
    var wallunitImage = new Image();
    wallunitImage.src = "../../../../images/propertyImages/internal/kitchen/wallunit_01.gif";
    imageList[15] = wallunitImage;
    var windowsImage = new Image();
    windowsImage.src = "../../../../images/propertyImages/internal/kitchen/windows_01.gif";
    imageList[16] = windowsImage;


    //function called on click event of listbox
    function changeImageSourceUsingList(sender) {
        changeImageSource(sender.selectedIndex);
    }

    //function called onmouseover of image map and change the image source 
    //according to input paramater		

    function changeImageSource(index) {
        document.getElementById("kitchen").src = imageList[index].src;
    }

    //function called onmouseout of image map and set the main kitchen image 
    function changeToMain() {
        document.getElementById("kitchen").src = kitchenMainImage.src;
    }
</script>
 <table cellpadding="3" cellspacing="0" border="0">
<tr><td class="elementimages_btnback" colspan="2">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
     <tr>
    <td style="height: 435px">
    <img src="../../../../images/propertyImages/internal/kitchen/fl_kitchen_main.gif"  width="546" height="431" border="0" usemap="#Map" id="kitchen" alt=""  />
    <map name="Map" id="kitchenMap" >
         
        <area shape="rect" coords="273,68,314,133" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(3).ToString() %>" onmouseover="changeImageSource('3')" onmouseout="changeToMain()" alt="<%= GetElementName(3).ToString() %>" title="<%= GetElementName(3).ToString()  %>"  /> 
          
        <area shape="poly" coords="186,106,186,135,273,132,272,67,202,62,201,106" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(15).ToString() %>" onmouseover="changeImageSource('15')" onmouseout="changeToMain()" alt="<%= GetElementName(15).ToString() %>" title="<%= GetElementName(15).ToString()  %>"  />

        <area shape="poly" coords="202,63,138,54,138,106,202,106" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()"  alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>"/>

        <area shape="poly" coords="81,146,81,155,255,137,253,155,330,162,330,157,336,161,337,153,325,150,323,61,543,32,542,11,254,51,80,17,81,48,254,66,301,69,301,63,288,62,287,49,301,49,307,52,307,62,312,68,313,133,186,134,186,106,139,106,139,141" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(13).ToString() %>" onmouseover="changeImageSource('13')" onmouseout="changeToMain()" alt="<%= GetElementName(13).ToString() %>" title="<%= GetElementName(13).ToString()  %>"/>
        <area shape="poly" coords="338,153,342,159,343,164,414,172,413,159" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(13).ToString() %>" onmouseover="changeImageSource('13')" onmouseout="changeToMain()" alt="<%= GetElementName(13).ToString() %>" title="<%= GetElementName(13).ToString()  %>"  />

        <area shape="poly" coords="323,61,325,151,413,160,415,50" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(16).ToString() %>" onmouseover="changeImageSource('16')" onmouseout="changeToMain()"  alt="<%= GetElementName(16).ToString() %>" title="<%= GetElementName(16).ToString()  %>"/>
        
        <area shape="poly" coords="81,154,80,162,95,161,95,176,80,177,81,186,122,178,123,166,187,155,189,166,253,155,253,138,193,143,196,148,220,145,220,157,195,161,195,151,188,143,82,155" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(14).ToString() %>" onmouseover="changeImageSource('14')" onmouseout="changeToMain()" alt="<%= GetElementName(14).ToString() %>" title="<%= GetElementName(14).ToString()  %>" />

        <area shape="poly" coords="310,168,290,176,289,258,354,282,355,194,354,186,375,177,344,171,343,160,337,154,334,160,329,157,329,162,339,170" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()"  alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>"/>
        
        <area shape="poly" coords="414,171,528,181,528,337,531,338,531,353,414,312" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(12).ToString() %>" onmouseover="changeImageSource('12')" onmouseout="changeToMain()"  alt="<%= GetElementName(12).ToString() %>" title="<%= GetElementName(12).ToString()  %>"/>
        
        <area shape="poly" coords="80,162,95,161,95,175,81,178" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(11).ToString() %>" onmouseover="changeImageSource('11')" onmouseout="changeToMain()" alt="<%= GetElementName(11).ToString() %>" title="<%= GetElementName(11).ToString()  %>" />
        
        <area shape="poly" coords="414,108,531,108,531,173,414,164" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(10).ToString() %>" onmouseover="changeImageSource('10')" onmouseout="changeToMain()" alt="<%= GetElementName(10).ToString() %>" title="<%= GetElementName(10).ToString()  %>" />
        
        <area shape="poly" coords="257,251,413,302,414,312,531,353,545,353,544,418,67,429,80,420,81,316,99,333,158,302,159,307,223,273,223,268" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(9).ToString() %>" onmouseover="changeImageSource('9')" onmouseout="changeToMain()" alt="<%= GetElementName(9).ToString() %>" title="<%= GetElementName(9).ToString()  %>" />
        <area shape="poly" coords="67,429,6,393,0,393,0,429" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(9).ToString() %>" onmouseover="changeImageSource('9')" onmouseout="changeToMain()" alt="<%= GetElementName(9).ToString() %>" title="<%= GetElementName(9).ToString()  %>" />
       
        <area shape="poly" coords="307,62,307,50,301,50,287,50,287,62,301,63" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(8).ToString() %>" onmouseover="changeImageSource('8')" onmouseout="changeToMain()" alt="<%= GetElementName(8).ToString() %>" title="<%= GetElementName(8).ToString()  %>" />
       
        <area shape="poly" coords="196,148,219,145,219,158,195,161" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>" />

        <area shape="poly" coords="67,428,79,421,80,10,0,-1,0,391" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" />

        <area shape="poly" coords="124,166,187,155,188,174,222,184,223,273,156,308,156,202,124,188" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()"  alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>"/>
        
        <area shape="poly" coords="24,0,81,11,81,16,253,50,545,10,542,4" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>" />
        
        <area shape="poly" coords="154,205,154,294,154,304,100,331,79,317,82,210,102,217" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(2).ToString() %>" onmouseover="changeImageSource('2')" onmouseout="changeToMain()" alt="<%= GetElementName(2).ToString() %>" title="<%= GetElementName(2).ToString()  %>" />
        
        <area shape="poly" coords="81,48,91,48,137,54,139,140,87,144,80,144" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(15).ToString() %>" onmouseover="changeImageSource('15')" onmouseout="changeToMain()" alt="<%= GetElementName(15).ToString() %>" title="<%= GetElementName(15).ToString()  %>"  />
        
     </map>
    </td>
   <td valign="top" style ="background-color:White; border:0px;" rowspan="3">
                     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox>
                     </td>
  </tr>
     </table>
</asp:Content>