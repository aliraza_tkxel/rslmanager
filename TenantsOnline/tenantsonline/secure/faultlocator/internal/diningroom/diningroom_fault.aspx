﻿<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="diningroom_fault.aspx.vb" Inherits="diningroom_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>

<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 

<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">

<script type="text/javascript" language ="javascript">
    var imageList = new Array(8);
    //Load all images on page load
    var diningroomMainImage = new Image();
    diningroomMainImage.src = "../../../../images/propertyImages/internal/diningroom/fl_diningroom_main.gif";
    var ceilingImage = new Image();
    ceilingImage.src = "../../../../images/propertyImages/internal/diningroom/ceiling_01.gif";
    imageList[0] = ceilingImage;
    var alarmsensorImage = new Image();
    alarmsensorImage.src = "../../../../images/propertyImages/internal/diningroom/alarmsensor_01.gif";
    imageList[1] = alarmsensorImage;
    var windowImage = new Image();
    windowImage.src = "../../../../images/propertyImages/internal/diningroom/window_01.gif";
    imageList[2] = windowImage;
    var lightingImage = new Image();
    lightingImage.src = "../../../../images/propertyImages/internal/diningroom/lighting_01.gif";
    imageList[3] = lightingImage;
    var wallImage = new Image();
    wallImage.src = "../../../../images/propertyImages/internal/diningroom/wall_01.gif";
    imageList[4] = wallImage;
    var electricsImage = new Image();
    electricsImage.src = "../../../../images/propertyImages/internal/diningroom/electrics_01.gif";
    imageList[5] = electricsImage;
    var floorImage = new Image();
    floorImage.src = "../../../../images/propertyImages/internal/diningroom/floor_01.gif";
    imageList[6] = floorImage;
    var doorImage = new Image();
    doorImage.src = "../../../../images/propertyImages/internal/diningroom/door_01.gif";
    imageList[7] = doorImage;

    //function called on click event of listbox
    function changeImageSourceUsingList(sender) {
        changeImageSource(sender.selectedIndex);
    }

    //function called onmouseover of image map and change the image source 
    //according to input paramater		
    function changeImageSource(index) {
        document.getElementById("diningroom").src = imageList[index].src;
    }

    //function called onmouseout of image map and set the main diningroom image 
    function changeToMain() {
        document.getElementById("diningroom").src = diningroomMainImage.src;
    }
</script>

<table cellpadding="3" cellspacing="0" border="0">
 <tr><td class="elementimages_btnback" colspan="2">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
    <td style="height: 435px">
        <img src="../../../../images/propertyImages/internal/diningroom/fl_diningroom_main.gif"  width="546" height="431" border="0" usemap="#Map" id="diningroom" alt=""  />
        <map name="Map" id="diningroomMap" >
        
            <area shape="poly" coords="209,51,215,50,215,45,217,50,222,51,222,80,218,81,213,83,209,79" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" />
            
            <area shape="poly" coords="26,0,544,4,544,23,538,23,215,40,27,27" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
             
            <area shape="poly" coords="538,43,535,40,461,52,463,430,534,427,537,420" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>" /> 
            
            <area shape="poly" coords="420,247,440,251,440,270,419,266" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>" />
        
            <area shape="poly" coords="56,266,65,318,68,349,68,368,68,410,76,409,77,373,119,385,119,429,16,430,27,422,26,276" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>" />
            <area shape="poly" coords="131,387,132,428,463,429,462,280,412,267,403,322,401,323,403,353,399,356,393,354,393,327,371,340,372,371,368,376,362,373,363,342,358,337,356,353,358,383,355,388,349,382,348,355,321,371,324,407,319,408,313,408,313,372,286,351,284,367,279,369,275,369,275,344,251,327,248,390,243,391,240,393,239,400,230,406,218,401,216,273,213,274,214,330,210,335,204,330,203,278,194,284,194,311,195,319,193,326,196,397,191,400,184,397,181,332,170,344,160,353" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>"/>
            <area shape="poly" coords="181,284,146,295,182,305" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>"/>
            <area shape="poly" coords="239,270,236,272,237,339,240,321,240,301" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>"/>
            <area shape="poly" coords="261,272,278,276,280,287,262,292" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>"/>
        
            <area shape="poly" coords="2,147,2,168,22,167,20,146" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(3).ToString() %>" onmouseover="changeImageSource('3')" onmouseout="changeToMain()" alt="<%= GetElementName(3).ToString() %>" title="<%= GetElementName(3).ToString()  %>"/>
        
            <area shape="poly" coords="537,22,537,41,460,52,462,280,439,274,440,268,440,249,420,248,421,267,438,269,435,274,412,267,426,183,421,180,392,188,385,195,381,195,345,203,350,163,342,157,296,154,278,203,264,201,271,190,271,175,264,170,257,174,256,187,251,186,244,187,246,199,239,198,240,181,233,179,224,181,225,198,220,199,219,197,214,192,216,83,221,80,221,51,215,51,216,46,209,53,209,80,215,83,214,192,205,194,204,204,193,204,193,188,193,59,198,59,197,45,37,34,28,35,27,28,214,39" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>" />
            <area shape="poly" coords="0,143,1,1,26,0,25,35,29,36,27,48,36,49,44,50,44,148,43,202,40,265,42,266,53,260,56,268,27,275,27,278,26,279,27,279,27,421,15,429,1,430,1,169,22,166,22,148,3,148,2,167,-1,168,1,142,1,127" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>"/>
            
            <area shape="poly" coords="27,34,37,34,197,45,198,57,192,57,192,188,188,187,186,171,176,156,132,164,143,213,149,218,145,220,133,225,120,172,114,165,73,173,66,178,74,208,41,206,39,265,54,261,42,207,42,49,36,48,28,48" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(2).ToString() %>" onmouseover="changeImageSource('2')" onmouseout="changeToMain()" alt="<%= GetElementName(2).ToString() %>" title="<%= GetElementName(2).ToString()  %>" />
        
        </map>
   </td>
<td valign="top" style ="background-color:White; border:0px;" rowspan="3">
                     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox>
                     </td>
  </tr>
     </table>
</asp:Content>