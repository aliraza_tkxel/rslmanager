﻿<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="basement_fault.aspx.vb" Inherits="basement_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>

<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 

<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">
<script type="text/javascript" language ="javascript">
    var imageList = new Array(6);
    //Load all images on page load
    var basementMainImage = new Image();
    basementMainImage.src = "../../../../images/propertyImages/internal/basement/fl_basement_main.gif";
    var wallImage = new Image();
    wallImage.src = "../../../../images/propertyImages/internal/basement/wall_01.gif";
    imageList[0] = wallImage;
    var ceilingImage = new Image();
    ceilingImage.src = "../../../../images/propertyImages/internal/basement/ceiling_01.gif";
    imageList[1] = ceilingImage;
    var stairsImage = new Image();
    stairsImage.src = "../../../../images/propertyImages/internal/basement/stairs_01.gif";
    imageList[2] = stairsImage;
    var floorImage = new Image();
    floorImage.src = "../../../../images/propertyImages/internal/basement/floor_01.gif";
    imageList[3] = floorImage;
    var lightingImage = new Image();
    lightingImage.src = "../../../../images/propertyImages/internal/basement/lighting_01.gif";
    imageList[4] = lightingImage;
    var electricsImage = new Image();
    electricsImage.src = "../../../../images/propertyImages/internal/basement/electrics.gif";
    imageList[5] = electricsImage;

    //function called on click event of listbox
    function changeImageSourceUsingList(sender) {
        changeImageSource(sender.selectedIndex);
    }

    //function called onmouseover of image map and change the image source 
    //according to input paramater		
    function changeImageSource(index) {
        document.getElementById("basement").src = imageList[index].src;
    }

    //function called onmouseout of image map and set the main basement image 
    function changeToMain() {
        document.getElementById("basement").src = basementMainImage.src;
    }
</script>
 <table cellpadding="3" cellspacing="0" border="0">
 <tr><td class="elementimages_btnback" colspan="2">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
    <td style="height: 435px">
    <img src="../../../../images/propertyImages/internal/basement/fl_basement_main.gif"  width="546" height="431" border="0" usemap="#Map" id="basement" alt=""  />
    <map name="Map" id="basementMap" >
         <area shape="poly" coords="1,0,243,1,243,22,71,22,70,17" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" />
        
        <area shape="poly" coords="447,228,544,228,544,254,448,254" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>" />
        <area shape="poly" coords="276,227,288,227,287,254,276,254" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>"/>
        
        <area shape="poly" coords="0,430,0,346,18,331,20,346,48,348,88,350,134,352,147,305,230,304,242,358,276,359,276,254,287,254,285,330,290,337,396,337,400,370,446,383,458,360,501,373,516,343,519,298,476,294,447,301,447,254,545,254,544,420,534,429" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(3).ToString() %>" onmouseover="changeImageSource('3')" onmouseout="changeToMain()" alt="<%= GetElementName(3).ToString() %>" title="<%= GetElementName(3).ToString()  %>" />
        
        <area shape="poly" coords="416,4,416,24,412,36,414,37,411,43,412,50,416,55,422,53,424,50,424,43,419,37,423,36,420,28,419,6" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>" />
        <area shape="poly" coords="307,94,327,94,327,115,307,114" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>" />
        
        <area shape="poly" coords="133,351,146,304,146,268,143,243,147,234,182,232,183,173,242,112,243,58,232,57,233,72,231,72,198,76,198,107,166,109,165,115,169,115,169,141,137,142,136,147,139,149,138,161,69,161,69,185,41,204,44,232,34,242,36,267,26,279,31,305,19,320,20,346" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(2).ToString() %>" onmouseover="changeImageSource('2')" onmouseout="changeToMain()" alt="<%= GetElementName(2).ToString() %>" title="<%= GetElementName(2).ToString()  %>" />
        
        <area shape="poly" coords="243,0,242,57,232,57,231,72,197,74,196,79,199,79,198,106,166,109,165,113,169,114,169,142,138,143,136,148,141,150,141,159,69,162,70,185,42,206,44,231,34,241,36,267,26,280,31,306,19,319,19,332,2,343,1,346,0,1" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
        <area shape="poly" coords="146,305,144,244,148,231,182,233,183,174,243,113,243,1,312,4,307,95,307,115,327,114,327,94,307,93,313,6,416,4,416,25,412,36,415,38,412,42,411,51,417,55,424,51,424,42,419,38,423,35,419,26,418,5,542,5,543,124,535,123,534,20,465,19,465,126,542,124,545,207,534,210,535,220,544,221,544,227,447,227,448,199,446,188,444,177,439,156,298,155,294,159,287,191,287,227,276,226,276,359,242,359,230,305" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
        
    </map>
    </td>
<td valign="top" style ="background-color:White; border:0px;" rowspan="3">
                     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox>
                     </td>
  </tr>
     </table>
</asp:Content>