﻿Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject


Partial Public Class ReportFaultDetail
    'Inherits System.Web.UI.Page
    Inherits System.Web.UI.Page

#Region "Page Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

        'Get the User submitted fault 
        Dim faultId As Integer = CType(Session(FaultConstants.SelectedFaultId), Integer)

        'If fault id not found in session then move to the start page
        If faultId <= 0 Then
            'Create the obj of customer header bo
            Dim headerBO As CustomerHeaderBO = Nothing

            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            Response.Redirect(FaultConstants.LoactionUrlString)
        End If

    End Sub
#End Region

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        btnNext.Attributes.Add("onClick", "fillPopup()")

    End Sub
#End Region

#Region "GetFaultQuestin"
    Protected Function GetFaultQuestion(ByVal questionId As Integer)

        'Create fault Question BO        
        Dim faultQBO As FaultQuestionBO = New FaultQuestionBO()

        Try
            'Get the area ID from Query String
            If questionId > 0 Then

                faultQBO.QuestionId = questionId

                Dim faultBL As FaultManager = New FaultManager()
                faultBL.GetFaultQuestion(faultQBO)
            End If
        Catch ex As Exception
            ' Do nothing
        End Try

        Return faultQBO.Question

    End Function
#End Region

#Region "btn Continue Popup Click"

    Protected Sub btnContinuePopup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Me.SaveFaultTemporarily()

    End Sub
#End Region

#Region "SaveFaultTemporarily"

    Private Sub SaveFaultTemporarily()


        'Create the fault BO
        Dim tempFaultBO As TempFaultBO = New TempFaultBO()
        Dim faultBL = New FaultManager()
        Dim headerBO As CustomerHeaderBO = Nothing
        Try
            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            'Get the User submitted fault 
            Dim faultId As Integer = CType(Session(FaultConstants.SelectedFaultId), Integer)

            tempFaultBO.FaultId = faultId
            tempFaultBO.CustomerId = headerBO.CustomerID
            tempFaultBO.Quantity = Me.ddlQuantity.SelectedValue
            tempFaultBO.ProblemDays = Me.ddlDays.SelectedValue
            tempFaultBO.RecuringProblem = Me.ddlRecProb.SelectedValue
            tempFaultBO.CommunalProblem = Me.ddlComProb.SelectedValue
            tempFaultBO.Notes = Me.txtNotes.Text

        Catch ex As Exception
            ' Do nothing
        End Try

        faultBL.SaveFaultTemporarily(tempFaultBO)

        If tempFaultBO.IsFlagStatus Then
            'Remove the fault id from sesssion
            Session.Remove(FaultConstants.SelectedFaultId)
            Response.Redirect("~/secure/faultlocator/temp_fault_basket.aspx")

        Else
            Me.lblErrorMessage.CssClass = "error_message_label"
            Me.lblErrorMessage.Text = UserInfoMsgConstants.FaultSavingFailed
        End If

    End Sub
#End Region

#Region "btn Top Back Click"

    Protected Sub btnTopBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTopBack.Click

        Response.RedirectLocation = FaultConstants.FaultListUrlString + CType(Session(FaultConstants.SelectedElementId), String)
        Response.Redirect(Response.RedirectLocation)

    End Sub
#End Region
End Class