﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    CodeFile="update_customer_detail.aspx.vb" Inherits="UpdateCustomerDetail" ValidateRequest="false"
    EnableEventValidation="false" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>
<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <table>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Font-Bold="True"
                    Font-Names="Arial" Text="Update My Detail" Width="520px"></asp:Label>&nbsp;
                <hr style="width: 575px" />
                <asp:Label ID="lblMessage" runat="server" CssClass="caption"></asp:Label></td>
            <td valign="top" style="background-color: White; border: 0px;">

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>
                    <asp:Label ID="lblPersonalDetail" runat="server" CssClass="caption" Text="Personal Detail"></asp:Label></strong></td>
        </tr>
        <tr>
            <td style="width: 200px">   
                <asp:Label ID="lblCaptionFirstName" runat="server" CssClass="caption" Width="81px">First Name:</asp:Label></td>
            <td>
                <asp:TextBox ID="txtFirstName" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionMiddleName" runat="server" CssClass="caption" Width="95px">Middle Name:</asp:Label></td>
            <td>
                <asp:TextBox ID="txtMiddleName" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px; height: 26px;">
                <asp:Label ID="lblCaptionLastName" runat="server" CssClass="caption" Width="90px">Last Name:</asp:Label></td>
            <td style="height: 26px;">
                <asp:TextBox ID="txtLastName" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionHouseNumber" runat="server" CssClass="caption" Text="House Number:"
                    Width="103px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtHouseNo" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionAddressLine1" runat="server" CssClass="caption" Text="Address Line 1:"
                    Width="103px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtAddressLine1" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionAddressLine2" runat="server" CssClass="caption" Text="Address Line 2:"
                    Width="103px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtAddressLine2" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionAddressLine3" runat="server" CssClass="caption" Text="Address Line 3:"
                    Width="103px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtAddressLine3" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionTownCity" runat="server" CssClass="caption" Text="Town/City:"
                    Width="73px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtTownCity" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionCounty" runat="server" CssClass="caption" Text="County:"
                    Width="59px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtCounty" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionPostcode" runat="server" CssClass="caption" Text="Postcode:"
                    Width="59px"></asp:Label></td>
            <td class="caption">
                <asp:TextBox ID="txtPostCode" runat="server" CssClass="input_normal" Enabled="False"></asp:TextBox><asp:RequiredFieldValidator
                    ID="rvPostCode" runat="server" ControlToValidate="txtPostCode" Display="Dynamic"
                    ErrorMessage="Post Code is required" SetFocusOnError="True">* Post Code is required</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="rePostCode" runat="server" ControlToValidate="txtPostCode" Display="Dynamic"
                        ErrorMessage="Invalid Post Code" SetFocusOnError="True" ValidationExpression='<%$ Resources:TextResources, PostcodeRegEx %>'>* Invalid Post Code</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionTelHome" runat="server" CssClass="caption" Text="Telephone:"
                    Width="79px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtTelephone" runat="server" CssClass="input_normal"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px">
                <asp:Label ID="lblCaptionTelMobile1" runat="server" CssClass="caption" Text="Tel(Mobile):"
                    Width="93px"></asp:Label></td>
            <td>
                <asp:TextBox ID="txtTelMobile" runat="server" CssClass="input_normal"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 1px; height: 21px">
                <asp:Label ID="lblCaptionEmail" runat="server" CssClass="caption" Text="Email:" Width="43px"></asp:Label></td>
            <td style="height: 21px;" class="caption">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="input_normal"></asp:TextBox>
                <asp:RegularExpressionValidator ID="rvEmail" runat="server" ControlToValidate="txtEmail"
                    Display="Dynamic" ErrorMessage="Invalid Email address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    SetFocusOnError="True">* Invalid Email address</asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td style="width: 1px; height: 21px">
            </td>
            <td style="height: 21px;">
                &nbsp;<asp:Button ID="btnBack" runat="server" PostBackUrl="~/secure/faultlocator/submit_fault_basket.aspx"
                    Text="< Back" />&nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save >" /></td>
        </tr>
    </table>
</asp:Content>
