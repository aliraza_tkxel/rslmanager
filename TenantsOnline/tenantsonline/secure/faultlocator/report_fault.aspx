﻿<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="report_fault.aspx.vb" Inherits="report_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
    
<asp:Content  ID = "Content" runat = "server" ContentPlaceHolderID ="page_area" >
    <div class="sectionHead" style="width: 98%">I would like to report a fault</div>
    
    From the illustrations below, first click on the area of your property where the fault is located.
    <br /><br />
    <table cellpadding="3" style="border-right: darkgray thin solid; border-top: darkgray thin solid;
        border-left: darkgray thin solid; width: 587px; border-bottom: darkgray thin solid;
        background-color: lightgrey">
        <tr>
            <td style="height: 21px; width: 649px;">
    <table cellpadding="2" cellspacing="3" style="width: 508px; background-color: lightgrey" >
        <tr>
            <td align="left" colspan="4" style="position: static; height: 45px">
                <table cellpadding="5" style="border-right: darkgray thin solid; border-top: darkgray thin solid;
                    border-left: darkgray thin solid; width: 630px; border-bottom: darkgray thin solid;
                     background-color: #eff1f2">
                    <tr>
                        <td align="left" colspan="4" style=" background-color: #eff1f2; width: 492px; height: 38px; background-image: url(../../images/propertyImages/External/external.bmp);">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px;">
                <asp:ImageButton ID="btnPathFence" runat="server" ImageUrl="~/images/propertyImages/External/Path_Fence.bmp" PostBackUrl="~/secure/faultlocator/external/path_fence/path_fence.aspx?areaId=16" /></td>
            <td style="width: 100px; height: 19px;">
                <asp:ImageButton ID="btnWindows" runat="server" ImageUrl="~/images/propertyImages/External/windows.bmp" PostBackUrl="~/secure/faultlocator/external/windows/windows.aspx?areaId=17" />
            </td>
            <td style="width: 81px; height: 19px;">
                <asp:ImageButton ID="btnDoor" runat="server" ImageUrl="~/images/propertyImages/External/door.bmp" PostBackUrl="~/secure/faultlocator/external/doors/door.aspx?areaId=18" /></td>
            <td style="width: 125px; height: 19px;">
                <asp:ImageButton ID="btnGarden" runat="server" ImageUrl="~/images/propertyImages/External/garden.bmp" PostBackUrl="~/secure/faultlocator/external/gardens/gardens.aspx?areaId=19" /></td>
        </tr>
        <tr>
            <td style="width: 101px; height: 6px">
                <asp:ImageButton ID="btnWalls" runat="server" ImageUrl="~/images/propertyImages/External/walls.bmp" PostBackUrl="~/secure/faultlocator/external/walls/walls.aspx?areaId=20" /></td>
            <td style="width: 100px; height: 6px">
                <asp:ImageButton ID="btnRoofChimney" runat="server" ImageUrl="~/images/propertyImages/External/roof_chimney.bmp" PostBackUrl="~/secure/faultlocator/external/roof_chimney/roof_chimney.aspx?areaId=21" /></td>
            <td style="width: 81px; height: 6px">
                <asp:ImageButton ID="btnDrive" runat="server" ImageUrl="~/images/propertyImages/External/drive.bmp" PostBackUrl="~/secure/faultlocator/external/drive/drive.aspx?areaId=22" /></td>
            <td style="width: 125px; height: 6px">
                <asp:ImageButton ID="btnShed" runat="server" ImageUrl="~/images/propertyImages/External/shed.bmp" PostBackUrl="~/secure/faultlocator/external/shed/shed.aspx?areaId=23" /></td>
        </tr>
    </table>
            </td>

            <td valign="top" style ="background-color:White; border:0px;">
                </td>
            
        </tr>
    </table>
    <br />
    
    <table cellpadding="3" style="border-right: darkgray thin solid; border-top: darkgray thin solid;
        border-left: darkgray thin solid; width: 587px; border-bottom: darkgray thin solid;
        background-color: lightgrey">
        <tr>
            <td colspan="3" style="height: 21px">
    <table cellpadding="2" cellspacing="3" style="width: 508px; background-color: lightgrey" >
        <tr>
            <td align="left" colspan="4" style="position: static; height: 52px"><table cellpadding="5" style="border-right: darkgray thin solid; border-top: darkgray thin solid;
                    border-left: darkgray thin solid; width: 630px; border-bottom: darkgray thin solid;
                     background-color: #eff1f2">
                <tr>
                    <td align="left" colspan="4" style=" background-color: #eff1f2; width: 492px; height: 38px; background-image: url(../../images/propertyImages/Internal/internal.bmp);">
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px;">
                <asp:ImageButton ID="btnHallStairLanding" runat="server" ImageUrl="~/images/propertyImages/Internal/hall_stairs_landing.bmp" PostBackUrl="~/secure/faultlocator/internal/hallstairs/hallstairs_fault.aspx?areaId=24" /></td>
            <td style="width: 100px; height: 19px;">
                <asp:ImageButton ID="btnLivingRoom" runat="server" ImageUrl="~/images/propertyImages/Internal/living_room.bmp" PostBackUrl="~/secure/faultlocator/internal/livingroom/livingroom_fault.aspx?areaId=25" />
            </td>
            <td style="width: 81px; height: 19px;">
                <asp:ImageButton ID="btnDiningRoom" runat="server" ImageUrl="~/images/propertyImages/Internal/dining_room.bmp" PostBackUrl="~/secure/faultlocator/internal/diningroom/diningroom_fault.aspx?areaId=26" /></td>
            <td style="width: 125px; height: 19px;">
                <asp:ImageButton ID="btnBasement" runat="server" ImageUrl="~/images/propertyImages/Internal/basement.bmp" PostBackUrl="~/secure/faultlocator/internal/basement/basement_fault.aspx?areaId=27" /></td>
        </tr>
        <tr>
            <td style="width: 101px; height: 6px">
                <asp:ImageButton ID="btnKitchen" runat="server" ImageUrl="~/images/propertyImages/Internal/kitchen.bmp" PostBackUrl="~/secure/faultlocator/internal/kitchen/kitchen_fault.aspx?areaId=28" /></td>
            <td style="width: 100px; height: 6px">
                <asp:ImageButton ID="btnBathroom_WC" runat="server" ImageUrl="~/images/propertyImages/Internal/bathroom_WC.bmp" PostBackUrl="~/secure/faultlocator/internal/bathroom/bathroom_fault.aspx?areaId=29" /></td>
            <td style="width: 81px; height: 6px">
                <asp:ImageButton ID="btnBedroom" runat="server" ImageUrl="~/images/propertyImages/Internal/bedroom.bmp" PostBackUrl="~/secure/faultlocator/internal/bedroom/bedroom_fault.aspx?areaId=30" /></td>
            <td style="width: 125px; height: 6px">
                <asp:ImageButton ID="btnLoft" runat="server" ImageUrl="~/images/propertyImages/Internal/loft.bmp" PostBackUrl="~/secure/faultlocator/internal/loft/loft_fault.aspx?areaId=31" /></td>
        </tr>
    </table>
            </td>
        </tr>
    </table>

</asp:Content>