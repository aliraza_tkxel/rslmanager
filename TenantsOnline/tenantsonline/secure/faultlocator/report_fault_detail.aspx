﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    CodeFile="report_fault_detail.aspx.vb" Inherits="ReportFaultDetail"
    Title="Tenants Online :: Fault Locator" EnableViewState="true" ValidateRequest="false"
    EnableEventValidation="false" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../../user control/BreadCrumbControl.ascx" TagName="BreadCrumbControl"
    TagPrefix="uc2" %>
<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">

    <script type="text/javascript" language="javascript">
        function fillPopup() {


            document.getElementById('popupQuantity').innerHTML = document.getElementById('<%=ddlQuantity.ClientID%>').value

            document.getElementById('popupDays').innerHTML = document.getElementById('<%=ddlDays.ClientID%>').value + ' day(s)'

            if (document.getElementById('<%=ddlRecProb.ClientID%>').value == 0) {

                document.getElementById('popupRecProb').innerHTML = 'This is not a recuring problem'
            }
            else {

                document.getElementById('popupRecProb').innerHTML = 'This is a recuring problem'
            }

            if (document.getElementById('<%=ddlComProb.ClientID%>').value == 0) {

                document.getElementById('popupComProb').innerHTML = 'Its not in communal area'
            }
            else {

                document.getElementById('popupComProb').innerHTML = 'Its in communal area'
            }

            document.getElementById('popupNotes').innerHTML = document.getElementById('<%=txtNotes.ClientID%>').value

            //            document.getElementById("dives").innerHTML= "abc"
            //            alert(document.getElementById('<%=ddlDays.ClientID%>').value)
            //            document.getElementById("popupQuantity").innerHTML = "abc"
        }

    </script>

    <table cellpadding="3" class="table_content" width="100%">
        <tr>
            <td align="left">
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Text="I would like to report a fault"></asp:Label>
            </td>
            <td align="right">
                <asp:Button ID="btnTopBack" runat="server" Text=" < Back" />
            </td>
            <td valign="top" style="background-color: White; border: 0px;" rowspan="13">

            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server"
                    Text=""></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2" rowspan="1" style="height: 25px">
                <asp:Label ID="Label1" runat="server" CssClass="cellData" Text="You have selected:"
                    Width="147px"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" style="color: #ffffff; background-color: black; height: 25px;" colspan="2">
                <uc2:BreadCrumbControl ID="BreadCrumbControl" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" style="height: 44px">
                <asp:Label ID="Label2" runat="server" CssClass="cellData" Text="If you have choosen job in error you can go back to previous page by clicking on the button above or the name of the area you have selected"
                    Width="480px"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="width: 160px" valign="top" class="body_caption_bold">
                <%=GetFaultQuestion(1).ToString()%>
            </td>
            <td align="left" style="width: 7px">
                <asp:DropDownList ID="ddlQuantity" runat="server" CssClass="select_normal">
                    <asp:ListItem Selected="True" Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                    <asp:ListItem Value="6">6</asp:ListItem>
                    <asp:ListItem Value="7">7</asp:ListItem>
                    <asp:ListItem Value="8">8</asp:ListItem>
                    <asp:ListItem Value="9">9</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="body_caption_bold">
                <%=GetFaultQuestion(2).ToString()%>
            </td>
            <td>
                <asp:DropDownList ID="ddlDays" runat="server" CssClass="select_normal">
                    <asp:ListItem Selected="True" Value="1">1 day</asp:ListItem>
                    <asp:ListItem Value="2">2 days</asp:ListItem>
                    <asp:ListItem Value="3">3 days</asp:ListItem>
                    <asp:ListItem Value="4">4 days</asp:ListItem>
                    <asp:ListItem Value="5">5 days</asp:ListItem>
                    <asp:ListItem Value="6">6 days</asp:ListItem>
                    <asp:ListItem Value="7">7 days</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="body_caption_bold">
                <%=GetFaultQuestion(3).ToString()%>
            </td>
            <td align="left" style="width: 7px">
                <asp:DropDownList ID="ddlRecProb" runat="server" CssClass="select_normal">
                    <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="body_caption_bold">
                <%=GetFaultQuestion(4).ToString()%>
            </td>
            <td align="left" style="width: 7px">
                <asp:DropDownList ID="ddlComProb" runat="server" CssClass="select_normal">
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="body_caption_bold">
                <%=GetFaultQuestion(5).ToString()%>
            </td>
            <td align="left" style="width: 7px">
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtNotes" runat="server" Width="400px" Rows="6" TextMode="MultiLine"
                    CssClass="input_normal"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                &nbsp;</td>
            <td>
            </td>
        </tr>
    </table>
    <div id="divPopup">
        <asp:ScriptManager ID="scriptMangaer" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="pnlUpdate" runat="server">
            <contenttemplate> <div style="float:right"><asp:Button ID="btnNext" runat="server" Text="Next > "/></div>
                        &nbsp;<cc2:ModalPopupExtender ID="mdlPopup" runat="server"  BackgroundCssClass="modalBackground" TargetControlID="btnNext" CancelControlID="btnBackPopup" PopupControlID="pnlResponsePopup" Drag="false">
                        </cc2:ModalPopupExtender>
                        <asp:Panel  ID="pnlResponsePopup" runat="server" BackColor="White" BorderColor="Transparent" CssClass="caption"> 
                            <table style="WIDTH:350px">
                                <tbody>
                                    <tr>
                                        <td style="BACKGROUND-COLOR: #CC0000" colspan="2">
                                          &nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <strong>Please Confirm Fault Details</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 182px">
                                            Quantity
                                        </td>
                                        <td>
                                            <div id="popupQuantity"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 182px">
                                            You have had this problem for
                                        </td>
                                        <td>
                                            <div id="popupDays" ></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             <div id="popupRecProb" ></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             <div id="popupComProb" ></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 182px" >
                                             Notes
                                        </td>
                                        <td>
                                             <div id="popupNotes" ></div><br />
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan="2">
                                             &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <asp:Button ID="btnBackPopup" runat="server" Text="< Back" /> <asp:Button ID="btnContinuePopup" runat="server" Text="Continue >" OnClick="btnContinuePopup_Click"  />  
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="BACKGROUND-COLOR: #CC0000" colspan="2">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                  </tbody> 
                            </table>
                       </asp:Panel>
                    </contenttemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
