﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    CodeFile="submit_fault_basket.aspx.vb" Inherits="SubmitFaultBasket" ValidateRequest="false"
    EnableEventValidation="false" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <table style="width: 100%">
        <tr>
            <td colspan="3">
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Font-Bold="True"
                    Font-Names="Arial" Text="I would like to report a fault"></asp:Label>
                <asp:Button ID="btnBack" runat="server" PostBackUrl="~/secure/faultlocator/temp_fault_basket.aspx"
                    Text=" < Back" />
                <hr />
            </td>
            <td valign="top" style="background-color: White; border: 0px;" rowspan="5">
                
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblRequestConfirmation" runat="server" CssClass="header_black" Font-Bold="True"
                    Font-Names="Arial" Text="Request Summary and Confirmation"></asp:Label>
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblCheckDetails" runat="server" Font-Names="Arial" Text="Thank you for your request. Please check the details are correct and update the contact details before confirming your request."></asp:Label>
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblFaultBasket" runat="server" Font-Bold="True" Font-Names="Arial"
                    Text="My Faults(s):"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 21px">
                <asp:Label ID="lblMessage" runat="server" CssClass="info_message_label"></asp:Label></td>
        </tr>
    </table>
    <table class="table_box">
        <tr>
            <td colspan="3">
                <asp:GridView ID="grdTempFaultBasket" runat="server" CellPadding="4" AutoGenerateColumns="False"
                    EmptyDataText="No record found" ForeColor="#333333" GridLines="None" Font-Names="Arial"
                    Font-Size="Small">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#CC0000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField InsertVisible="False" HeaderText="Area/Item/Element" SortExpression="CreationDate">
                            <ItemTemplate>
                                <asp:Label ID="lblLocationName" runat="server" CssClass="cellData" Text='<%# Bind("LocationName") %>' />_<asp:Label
                                    ID="lblAreaName" runat="server" CssClass="cellData" Text='<%# Bind("AreaName") %>' />_<asp:Label
                                        ID="lblElementName" runat="server" CssClass="cellData" Text='<%# Bind("ElementName") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField InsertVisible="False" HeaderText="Description">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField InsertVisible="False" HeaderText="Qty">
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" CssClass="cellData" Text='<%# Bind("Quantity") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField InsertVisible="False" HeaderText="Expected Delivery Timeframe">
                            <ItemTemplate>
                                <asp:Label ID="lblExpectedTime" runat="server" CssClass="cellData" Text="<%# Bind('FResponseTime') %>" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 21px">
                <asp:Label ID="lblMyDetails" runat="server" CssClass="header_black" Font-Bold="True"
                    Font-Names="Arial" Text="My Details:"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 122px">
                <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                    border-bottom: black thin solid;">
                    <tr>
                        <td>
                            <asp:Label ID="lblTenantName" runat="server" Text="" CssClass="caption"></asp:Label></td>
                        <td style="" class="caption">
                            <asp:Label ID="lblTelephoneLabel" runat="server" EnableViewState="False" Text="Telephone:"></asp:Label></td>
                        <td colspan="2">
                            <asp:Label ID="lblTelephone" runat="server" Text="" CssClass="caption"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAddress" runat="server" Text="" CssClass="caption"></asp:Label></td>
                        <td style="" class="caption">
                            <asp:Label ID="lblMobileLabel" runat="server" EnableViewState="False" Text="Mobile:"></asp:Label></td>
                        <td colspan="2">
                            <asp:Label ID="lblMobile" runat="server" CssClass="caption" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTown" runat="server" Text="" CssClass="caption"></asp:Label></td>
                        <td style="height: 21px;" class="caption">
                            <asp:Label ID="lblEmailLabel" runat="server" Text="Email:"></asp:Label></td>
                        <td colspan="2" style="height: 21px">
                            <asp:Label ID="lblEmail" runat="server" Text="" CssClass="caption"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCountry" runat="server" Text="" CssClass="caption"></asp:Label></td>
                        <td style="" class="caption">
                        </td>
                        <td colspan="2" style="">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Button ID="btnUpdateMyDetail" runat="server" CausesValidation="False" EnableViewState="False"
                                Text="Update My Details" PostBackUrl="~/secure/faultlocator/update_customer_detail.aspx" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblPreferredContactTime" runat="server" CssClass="caption" Text="If you have any preferred contact time or ways in which you would like us to contact you, please enter them in the box below:"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:TextBox ID="txtPreferredContactTime" runat="server" Rows="4" Width="300px" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 46px; float: left;">
                <asp:CheckBox ID="chkIamHappy" runat="server" CssClass="caption" Text=" I would like to be emailed about my level of satisfaction with repairs i report online?" />&nbsp;&nbsp;
                <div style="">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" /></div>
            </td>
        </tr>
    </table>
</asp:Content>
