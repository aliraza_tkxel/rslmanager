﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    CodeFile="door.aspx.vb" Inherits="door" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>
<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %>
<asp:Content ID="Content" runat="server" ContentPlaceHolderID="page_area">
    <table>
        <tr>
            <td class="elementimages_btnback" colspan="2">
                <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <img src="../../../../images/propertyImages/external/externalFaultImages/door_01.gif"
                    width="546" height="431" border="0" usemap="#Map" id="door" alt="" />
                <map name="Map" id="doorMap">
                    <area shape="POLY" coords="132,256,171,242,169,316,175,320,175,326,139,340,138,335,133,331"
                        href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"
                        alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
                </map>
            </td>
            <td valign="top" style="background-color: White; border: 0px;" rowspan="3">

            </td>
    </table>
</asp:Content>
