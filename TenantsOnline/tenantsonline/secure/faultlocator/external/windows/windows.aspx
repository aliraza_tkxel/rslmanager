﻿<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="windows.aspx.vb" Inherits="windows" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>

<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 

<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">
<table>
<tr><td class="elementimages_btnback" colspan="2">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
    <tr>
        <td>
   <img src="../../../../images/propertyImages/external/externalFaultImages/windows_01.gif"  width="546" height="431" border="0" usemap="#Map" id="windows" alt=""  />
    <map name="Map" id="windowsMap" >        
        <area shape="poly" coords="216,151,250,142,249,183,253,187,217,198,216,194" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
        <area shape="poly" coords="134,172,171,162,172,208,174,212,134,225,134,221" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>"/>
        <area shape="poly" coords="216,216,250,204,250,247,252,251,217,266,216,262" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>"/>
        <area shape="poly" coords="34,98,33,121,68,144,69,119" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"  alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>"/>
        <area shape="poly" coords="34,218,68,241,67,283,66,288,31,262,34,259" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"  alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>"/>
        
     </map>
     </td>
    
        
            <td valign="top" style ="background-color:White; border:0px;" rowspan="3">                  
                     </td>
                </tr> 
                
        </table>     
</asp:Content>
