﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    CodeFile="gardens.aspx.vb" Inherits="gardens" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Src="~/user control/elementimages_back_button_control.ascx"
    TagName="elementimages_back_button_control" TagPrefix="uc2" %>
<asp:Content ID="Content" runat="server" ContentPlaceHolderID="page_area">
    <table style="width: 100%">
        <tr>
            <td class="elementimages_btnback">
                <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <img src="../../../../images/propertyImages/external/externalFaultImages/gardens.gif"
                    width="546" height="431" border="0" usemap="#Map" id="garden" alt="" /></td>
        </tr>
    </table>
    <map name="Map" id="gardenMap">
        <area shape="poly" coords="279, 271, 173, 314, 175, 319, 176, 327, 203, 346, 225, 337, 236, 341, 244, 342, 263, 343, 280, 331, 295, 314, 300, 317, 297, 311, 299, 306, 340, 291, 443, 248, 393, 196, 386, 196, 373, 201, 328, 167, 328, 149, 345, 158, 343, 164, 347, 174, 353, 175, 363, 192, 372, 201, 373, 192, 375, 187, 385, 196, 395, 196, 397, 187, 388, 182, 399, 171, 392, 172, 389, 166, 383, 167, 380, 164, 381, 160, 373, 155, 393, 157, 406, 148, 398, 142, 400, 135, 394, 137, 390, 134, 390, 125, 398, 126, 392, 114, 400, 114, 395, 107, 422, 116, 423, 110, 411, 103, 424, 103, 418, 96, 429, 91, 430, 88, 430, 87, 425, 87, 428, 80, 423, 75, 411, 79, 408, 74, 407, 67, 406, 64, 419, 63, 420, 55, 414, 48, 423, 45, 425, 30, 420, 33, 419, 26, 412, 37, 404, 39, 397, 39, 404, 28, 404, 18, 398, 26, 395, 18, 386, 14, 375, 14, 370, 10, 383, 10, 394, 2, 232, 0, 234, 3, 243, 8, 243, 25, 250, 29, 250, 78, 286, 129, 290, 130, 281, 136"
            href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"
            alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
        <area shape="poly" coords="223, 2, 222, 6, 221, 22, 214, 23, 215, 29, 47, 51, 3, 118, 11, 125, 10, 276, 0, 272, 1, 0"
            href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"
            alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
        <area shape="poly" coords="342, 353, 401, 397, 398, 403, 399, 410, 394, 413, 392, 403, 386, 397, 381, 407, 382, 416, 376, 419, 376, 411, 370, 404, 365, 411, 366, 422, 361, 422, 359, 415, 355, 408, 348, 417, 350, 427, 346, 428, 344, 421, 337, 413, 332, 422, 332, 430, 2, 430, 0, 344, 68, 396, 42, 391, 27, 392, 15, 397, 8, 401, 6, 405, 11, 410, 19, 414, 34, 417, 53, 416, 65, 413, 72, 411, 74, 406, 73, 401, 100, 422, 172, 391, 203, 413"
            href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"
            alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
    </map>
</asp:Content>
