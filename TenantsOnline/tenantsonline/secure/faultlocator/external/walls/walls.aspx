﻿<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="walls.aspx.vb" Inherits="wall" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>

<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 

<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">
<table>
<tr><td class="elementimages_btnback" colspan="2">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
    <tr>
        <td>
    <img src="../../../../images/propertyImages/external/externalFaultImages/walls_01.gif"  width="546" height="431" border="0" usemap="#Map" id="walls" alt=""  />
    <map name="Map" id="wallsMap" >   
       <area shape="poly" coords="287,128,280,135,278,271,263,276,261,275,257,280,238,286,226,292,226,287,221,292,215,291,209,291,204,294,197,295,191,305,171,315,171,242,133,255,132,330,137,335,137,340,128,332,113,338,110,178,133,173,133,222,135,224,172,214,175,212,172,208,172,160,215,150,214,195,216,197,251,189,250,205,216,215,215,264,217,266,252,253,250,248,250,204,250,140" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />     
       <area shape="poly" coords="48,68,29,95,34,100,69,119,68,140,33,121,33,100,28,98,15,126,10,123,9,276,27,287,30,262,33,258,33,218,68,241,67,283,67,289,30,264,27,289,100,342,108,341,107,179,94,174,96,164" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
     </map>
     </td>
    
        
            <td valign="top" style ="background-color:White; border:0px;" rowspan="3">                
                     </td>
                </tr> 
                
        </table>     
</asp:Content>
