﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    CodeFile="shed.aspx.vb" Inherits="shed" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>
<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %>
<asp:Content ID="Content" runat="server" ContentPlaceHolderID="page_area">
    <table>
        <tr>
            <td class="elementimages_btnback" colspan="2">
                <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <img src="../../../../images/propertyImages/external/externalFaultImages/shed_garage_01.gif"
                    width="546" height="431" border="0" usemap="#Map" id="shed" alt="" />
                <map name="Map" id="shedMap">
                    <area shape="poly" coords="430,73,425,75,427,86,431,86,433,89,420,98,425,101,420,104,414,105,423,110,422,117,412,112,399,108,394,108,401,113,399,115,394,116,398,126,390,128,391,136,394,135,401,136,396,144,403,147,404,152,393,156,374,156,380,160,382,167,387,168,392,170,396,171,398,171,388,181,396,188,396,192,408,188,461,166,460,126,466,123"
                        href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"
                        alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
                    <area shape="poly" coords="330,151,344,158,342,163,344,172,348,175,353,175,356,182,359,184,361,190,360,192,328,167"
                        href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"
                        alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
                    <area shape="poly" coords="372,200,374,196,373,192,376,192,382,196,373,201,379,200,376,196,380,193"
                        href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>"
                        alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
                </map>
            </td>
            <td valign="top" style="background-color: White; border: 0px;" rowspan="3">
            </td>
        </tr>
    </table>
</asp:Content>
