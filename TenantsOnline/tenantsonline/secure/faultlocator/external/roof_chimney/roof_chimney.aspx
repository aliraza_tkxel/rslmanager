﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="roof_chimney.aspx.vb" Inherits="roof_chimney" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc2" %>

<%@ Register Src="~/user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %>

<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">

<script type="text/javascript" language ="javascript">
    //Load all images on page load
    var chimneyHighlightedImage = new Image();
    chimneyHighlightedImage.src = "../../../../images/propertyImages/external/externalFaultImages/chimney_01.gif";
    var roofHightlightedImage = new Image();
    roofHightlightedImage.src = "../../../../images/propertyImages/external/externalFaultImages/roof_01.gif";

    //function called on click event of listbox
    function changeImageSourceUsingList(sender) {
        if (sender.selectedIndex == 1)
            changeToMain();
        else
            changeImageSource();
    }

    //function called onmouseover of image map and change the image source 
    //according to input paramater		
    function changeImageSource() {
        document.getElementById("chimney_roof").src = roofHightlightedImage.src;
    }

    //function called onmouseout of image map and set the roof highlighted image 
    function changeToMain() {
        document.getElementById("chimney_roof").src = chimneyHighlightedImage.src;
    }
</script>

<table cellpadding="3" cellspacing="0" border="0">
<tr><td class="elementimages_btnback" colspan="2">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
 
 <tr>
    <td style="height: 435px">
        <img src="../../../../images/propertyImages/external/externalFaultImages/chimney_01.gif"  width="546" height="431" border="0" usemap="#Map" id="chimney_roof" alt=""  />
        <map name="Map" id="chimneyRoof" >
            <area shape="poly" coords="221,4,225,2,228,2,231,3,232,5,235,6,240,6,241,9,240,25,248,28,247,82,231,85,215,65,215,24,221,22" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" />
            <area shape="poly" coords="48,53,214,30,214,64,231,85,247,83,248,77,285,128,109,174,99,171,47,65" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" onmouseover="changeImageSource()" onmouseout="changeToMain()"  />
            
        </map>
 </td>
<td valign="top" style ="background-color:White; border:0px;" rowspan="3">
                     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox>
                     </td>
  </tr>
     </table>
</asp:Content>