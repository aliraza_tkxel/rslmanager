﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeFile="fault_log.aspx.vb" Inherits="fault_log" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">
<script language ="javascript" type="text/javascript">

    var i = 0, j = 0;

    function enableQSeven() {
        if (i < 1) {
            document.getElementById("<%= rdbQSevenOptionOne.ClientID %>").checked = true;

            document.getElementById("<%= rdbQSevenOptionOne.ClientID %>").disabled = false;
            document.getElementById("<%= rdbQSevenOptionTwo.ClientID %>").disabled = false;
            document.getElementById("<%= rdbQSevenOptionThree.ClientID %>").disabled = false;
            document.getElementById("<%= rdbQSevenOptionFour.ClientID %>").disabled = false;
            document.getElementById("<%= rdbQSevenOptionFive.ClientID %>").disabled = false;
            document.getElementById("<%= rdbQSevenOptionSix.ClientID %>").disabled = false;

            document.getElementById("hiddenRepairSatisfactoryReason").value = document.getElementById("<%= rdbQSevenOptionOne.ClientID %>").value;
            i = 1;
        }
    }

    function enableQNine() {
        if (j < 1) {
            document.getElementById("<%= rdbQNineOptionOne.ClientID %>").checked = true;

            document.getElementById("<%= rdbQNineOptionOne.ClientID %>").disabled = false;
            document.getElementById("<%= rdbQNineOptionTwo.ClientID %>").disabled = false;
            document.getElementById("<%= rdbQNineOptionThree.ClientID %>").disabled = false;
            document.getElementById("<%= rdbQNineOptionFour.ClientID %>").disabled = false;

            document.getElementById("hiddenAdministratorRepairSatisfactoryReason").value = document.getElementById("<%= rdbQNineOptionOne.ClientID %>").value;
            j = 1;
        }
    }

    function disableQSeven() {
        i = 0;
        document.getElementById("<%= rdbQSevenOptionOne.ClientID %>").checked = false;
        document.getElementById("<%= rdbQSevenOptionTwo.ClientID %>").checked = false;
        document.getElementById("<%= rdbQSevenOptionThree.ClientID %>").checked = false;
        document.getElementById("<%= rdbQSevenOptionFour.ClientID %>").checked = false;
        document.getElementById("<%= rdbQSevenOptionFive.ClientID %>").checked = false;
        document.getElementById("<%= rdbQSevenOptionSix.ClientID %>").checked = false;

        document.getElementById("<%= rdbQSevenOptionOne.ClientID %>").disabled = true;
        document.getElementById("<%= rdbQSevenOptionTwo.ClientID %>").disabled = true;
        document.getElementById("<%= rdbQSevenOptionThree.ClientID %>").disabled = true;
        document.getElementById("<%= rdbQSevenOptionFour.ClientID %>").disabled = true;
        document.getElementById("<%= rdbQSevenOptionFive.ClientID %>").disabled = true;
        document.getElementById("<%= rdbQSevenOptionSix.ClientID %>").disabled = true;

        document.getElementById("hiddenRepairSatisfactoryReason").value = "";
    }

    function disableQNine() {
        j = 0;
        document.getElementById("<%= rdbQNineOptionOne.ClientID %>").checked = false;
        document.getElementById("<%= rdbQNineOptionTwo.ClientID %>").checked = false;
        document.getElementById("<%= rdbQNineOptionThree.ClientID %>").checked = false;
        document.getElementById("<%= rdbQNineOptionFour.ClientID %>").checked = false;

        document.getElementById("<%= rdbQNineOptionOne.ClientID %>").disabled = true;
        document.getElementById("<%= rdbQNineOptionTwo.ClientID %>").disabled = true;
        document.getElementById("<%= rdbQNineOptionThree.ClientID %>").disabled = true;
        document.getElementById("<%= rdbQNineOptionFour.ClientID %>").disabled = true;

        document.getElementById("hiddenAdministratorRepairSatisfactoryReason").value = "";

    }

    function setRepairSatisfactoryReason(sender) {
        document.getElementById("hiddenRepairSatisfactoryReason").value = sender.value;
    }

    function setAdministratorRepairSatisfactoryReason(sender) {
        document.getElementById("hiddenAdministratorRepairSatisfactoryReason").value = sender.value;
    }

    function printSQuestionaire() {
        window.print();
    }

</script>
   <table style="width: 100%">
        <tr>
            <td>
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Text="Fault Log"></asp:Label>
                <hr />
            </td>
        </tr>
   </table>
   <cc2:toolkitscriptmanager id="Registration_ToolkitScriptManager" runat="server" EnablePartialRendering="true"> </cc2:toolkitscriptmanager>
   <table class="table_box" style="display:block">
        <tr>
            <td>Sorry! This area is not available</td>
        </tr>
    </table>
   <table class="table_box" style="display:none">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblSentEnquiries" runat="server" CssClass="body_caption_bold"></asp:Label>
            </td>            
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblUnreadResponses" runat="server" CssClass="body_caption_bold"></asp:Label></td>
            <td align="right">
                <asp:Label ID="lblStatus" runat="server" CssClass="caption">Status : </asp:Label><asp:DropDownList ID="ddlStatus" runat="server" CssClass="select_normal"  AppendDataBoundItems="True" AutoPostBack = "true">  
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <asp:UpdatePanel ID="UpdatePanelPopup" runat="server">
             
               <ContentTemplate>
                        <cc2:ModalPopupExtender
                            ID="mdlQuestionairPopup" runat="server" BackgroundCssClass="modalBackground" 
                            Drag="True" PopupControlID="pnlQuestionairPopup" TargetControlID="lnkBtnPseudo" X="90" Y="0" CancelControlID="btnClose">
                      </cc2:ModalPopupExtender>
                <asp:GridView ID="grdFaultLog" runat="server" CellPadding="4"
                    EmptyDataText="No record found" Font-Names="Arial" Font-Size="Small" ForeColor="Black"
                    AutoGenerateColumns="False" PageSize="1" GridLines="None" OnSelectedIndexChanged="grdFaultLog_SelectedIndexChanged">
                    <FooterStyle BackColor="#990000" BorderStyle="None" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" />
                    <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#CC0000" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    
                    <Columns>
                    <asp:TemplateField InsertVisible="False" HeaderText="Date" SortExpression="CreationDate" >
                        <ItemTemplate>
                         <asp:Label ID="faultSentDate" runat="server" CssClass="cellData" Text='<%# Format(Eval("SubmitDate"),"dd/MM/yy") %>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField InsertVisible="False" HeaderText="Details">
                        <ItemTemplate>
                         <asp:Label ID="faultDetail" runat="server" CssClass="cellData" Text='<%# Format(Eval("Description").ToString()) %>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField InsertVisible="False" HeaderText="Status">
                        <ItemTemplate>
                         <asp:Label ID="faultStatus" runat="server" CssClass="cellData" Text='<%# Format(Eval("Status").ToString) %>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField InsertVisible="False" HeaderText="Response">
                        <ItemTemplate>
                         <asp:Label ID="faultResponse" runat="server" CssClass="cellData" Text='<%# Format(Eval("FResponseTime").ToString()) %>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Appointment" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAppointmentTime" runat="server" Text='<%# Eval("AppointmentDate") %>'></asp:Label>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contractor" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblContractor" runat="server" Text='<%# Eval("ContractorName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Completed" InsertVisible="False">
                            <ItemTemplate>
                             <asp:Label ID="faultDueDate" runat="server" CssClass="cellData" Text='<%# Utility.FormatDueDate(Eval("DueDate").ToString()) %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>                    
                    <asp:TemplateField InsertVisible="False">
                        <ItemTemplate>
                         <asp:LinkButton  runat="server" ID="lnkbtnQuestionair" Text="?" OnClick="lnkbtnQuestionair_Click" CommandArgument ='<%# Format(Eval("FaultLogID").ToString()) %>'  Visible="<%# True %>" ></asp:LinkButton> 
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    </Columns>
                    <PagerSettings Visible="False" />
                </asp:GridView>
                <asp:Panel ID="pnlQuestionairPopup" runat="server" BackColor="White" BorderColor="Transparent" Height="525px" HorizontalAlign="center" CssClass="staifaction_popup">
                    <table cellpadding="1" cellspacing="1">
                        <tbody align ="left">
                        <tr>
                            <td align="left" colspan="13" style="height: 21px;; background-color: #c00000">
                                <input type="hidden" id="hiddenRepairSatisfactoryReason" name="hiddenRepairSatisfactoryReason" />
                                <input type="hidden" id="hiddenAdministratorRepairSatisfactoryReason" name="hiddenAdministratorRepairSatisfactoryReason" />
                                <asp:Label ID="lblQuestionnairHeading" runat="server" BackColor="#C00000" Font-Bold="True"
                                    Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Satisfaction Questionnaire"
                                    Width="35%"></asp:Label>
                        <asp:LinkButton ID="lnkBtnPseudo" runat="server"></asp:LinkButton></td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="2">
                                <asp:Label ID="lblQOne" runat="server" CssClass="caption" Text="How did you report this repair?"></asp:Label></td>
                            <td align="left" colspan="9">
                                <asp:TextBox ID="txtQOne" runat="server" Width="185px"></asp:TextBox></td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblQTwo" runat="server" CssClass="caption" Text="Was it dealt with promptly and courteously?"></asp:Label></td>
                            <td align="left">
                                <asp:RadioButton ID="rdbQTwoYes" runat="server" CssClass="caption" GroupName="rdbQTwo"
                                    Text="Y" Width="37px" /></td>
                            <td align="left" colspan="7">
                                <asp:RadioButton ID="rdbQTwoNo" runat="server" CssClass="caption" GroupName="rdbQTwo"
                                    Text="N" Width="42px" Checked="True" /></td>
                        </tr>
                        <tr>                           
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblQThree" runat="server" CssClass="caption" Text="Was the operative wearing company unifom?"></asp:Label></td>
                            <td align="left">
                                <asp:RadioButton ID="rdbQThreeYes" runat="server" CssClass="caption" GroupName="rdbQThree"
                                    Text="Y" Width="39px" /></td>
                            <td align="left" colspan="7">
                                <asp:RadioButton ID="rdbQThreeNo" runat="server" CssClass="caption" GroupName="rdbQThree"
                                    Text="N" Width="42px" Checked="True" /></td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblQFour" runat="server" CssClass="caption" Text="Did he show you his Identity badge?"></asp:Label></td>
                            <td align="left">
                                <asp:RadioButton ID="rdbQFourYes" runat="server" CssClass="caption" GroupName="rdbQFour"
                                    Text="Y" Width="41px" /></td>
                            <td align="left" colspan="7">
                                <asp:RadioButton ID="rdbQFourNo" runat="server" CssClass="caption" GroupName="rdbQFour"
                                    Text="N" Width="42px" Checked="True" /></td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblQFive" runat="server" CssClass="caption" Text="Did he explain repair to you?"></asp:Label></td>
                            <td align="left" >
                                <asp:RadioButton ID="rdbQFiveYes" runat="server" CssClass="caption" GroupName="rdbQFive"
                                    Text="Y" Width="39px" /></td>
                            <td align="left" colspan="7">
                                <asp:RadioButton ID="rdbQFiveNo" runat="server" CssClass="caption" GroupName="rdbQFive"
                                    Text="N" Width="42px" Checked="True" /></td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblQSix" runat="server" CssClass="caption" Text="Was the repair carried out satisfactorily?"></asp:Label></td>
                            <td align="left">
                                <asp:RadioButton ID="rdbQSixYes" runat="server" CssClass="caption" GroupName="rdbQSix"
                                    Text="Y" Width="39px" Checked="True"  /></td>
                            <td align="left" colspan="7">
                                <asp:RadioButton ID="rdbQSixNo" runat="server" CssClass="caption" GroupName="rdbQSix"
                                    Text="N" Width="44px" /></td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="11">
                                <asp:Label ID="lblQSeven" runat="server" CssClass="caption" Text="If no, Why? (Please tick one of the following)"></asp:Label></td>
                        </tr>
                        <tr>
                            
                            <td align="left">
                            </td>
                            <td align="left">
                            </td>
                            <td align="left"><asp:RadioButton ID="rdbQSevenOptionOne"   runat="server" CssClass="caption" GroupName="rdbQSeven"
                                    Text="Work not completed on time" /></td>
                            <td align="left">
                            </td>
                            <td align="left" colspan="8">
                                <asp:RadioButton ID="rdbQSevenOptionTwo" runat="server" CssClass="caption" GroupName="rdbQSeven"
                                    Text="Fault not remedied" /></td>
                        </tr>
                        <tr>                           
                            <td align="left">
                            </td>
                            <td align="left">
                            </td>
                            <td align="left">
                                <asp:RadioButton ID="rdbQSevenOptionThree" runat="server" CssClass="caption" GroupName="rdbQSeven"
                                    Text="Workmanship unsatisfactory" /></td>
                            <td align="left" style="height: 22px">
                            </td>
                            <td align="left" colspan="8" style="height: 22px">
                                <asp:RadioButton ID="rdbQSevenOptionFour" runat="server" CssClass="caption" GroupName="rdbQSeven"
                                    Text="Mess left by workman"/></td>
                        </tr>
                        <tr>                            
                            <td align="left">
                            </td>
                            <td align="left">
                            </td>
                            <td align="left">
                                <asp:RadioButton ID="rdbQSevenOptionFive" runat="server" CssClass="caption" GroupName="rdbQSeven"
                                    Text="Work not completed "/></td>
                            <td align="left">
                            </td>
                            <td align="left" colspan="8">
                                <asp:RadioButton ID="rdbQSevenOptionSix" runat="server" GroupName="rdbQSeven" Text="Workman rude/impolite" CssClass="caption"/></td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblQEight" runat="server" CssClass="caption" Text="Was the administration of your repair satisfactory?" Width="311px"></asp:Label></td>
                            <td align="left" colspan="1">
                                <asp:RadioButton ID="rdbQEightYes" runat="server" CssClass="caption" GroupName="rdbQEight"
                                    Text="Y" Width="39px" Checked="True" /></td>
                            <td align="left" colspan="2">
                                <asp:RadioButton ID="rdbQEightNo" runat="server" CssClass="caption" GroupName="rdbQEight"
                                    Text="N" Width="46px"  /></td>
                            <td colspan="7"></td>
                        </tr>
                        <tr>
                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="2" style="height: 21px">
                                <asp:Label ID="lblQNine" runat="server" CssClass="caption" Text="If no, why? (Please tick one of the following)"
                                    Width="280px"></asp:Label></td>
                            <td align="left" style="height: 21px">
                            </td>
                            <td colspan="5">
                            </td>
                            <td colspan="1">
                            </td>
                            <td colspan="1">
                            </td>
                            <td colspan="1" style="width: 4px">
                            </td>
                        </tr>
                        <tr>
                            
                            <td align="left">
                            </td>
                            <td align="left">
                            </td>
                            <td align="left" colspan="2">
                                <asp:RadioButton ID="rdbQNineOptionOne" runat="server" CssClass="caption" Text="Contractor did not keep appt" GroupName="rdbNine"/></td>
                            <td colspan="5" align="left">
                                <asp:RadioButton ID="rdbQNineOptionTwo" runat="server" CssClass="caption" Text="No appointment made" GroupName="rdbNine" Width="216px"/></td>
                            <td colspan="1">
                            </td>
                            <td colspan="1">
                            </td>
                            <td colspan="1" style="width: 4px">
                            </td>
                        </tr>
                        <tr>                            
                            <td align="left">
                            </td>
                            <td align="left">
                            </td>
                            <td align="left" colspan="2"><asp:RadioButton ID="rdbQNineOptionFour" runat="server" CssClass="caption" Text="Service Total staff were  unhelpful" GroupName="rdbNine"/>&nbsp;</td>
                            <td align="left" colspan="7">
                                <asp:RadioButton ID="rdbQNineOptionThree" runat="server" CssClass="caption" Text="Incorrect details on order" GroupName="rdbNine" Width="213px"/></td>
                            <td colspan="1" style="width: 4px">
                            </td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1" >
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblComment" runat="server" CssClass="caption" Text="Comments:"></asp:Label></td>
                            <td align="left" colspan="5">
                            </td>
                            <td colspan="1">
                            </td>
                            <td colspan="1">
                            </td>
                            <td colspan="1" style="width: 4px">
                            </td>
                        </tr>
                        <tr>                            
                            <td>
                            </td>
                            <td align="left" colspan="10">
                                <asp:TextBox ID="txtComment" runat="server" Height="34px" TextMode="MultiLine" Width="497px"></asp:TextBox></td>
                            <td colspan="1" style="width: 4px">
                            </td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblAction" runat="server" CssClass="caption" Text="Action:"></asp:Label></td>
                            <td align="left" colspan="5">
                            </td>
                            <td colspan="1">
                            </td>
                            <td colspan="1">
                            </td>
                            <td colspan="1" style="width: 4px">
                            </td>
                        </tr>
                        <tr>                            
                            <td align="left" colspan="1">
                            </td>
                            <td align="left" colspan="10">
                                <asp:TextBox ID="txtAction" runat="server" Height="34px" TextMode="MultiLine" Width="497px"></asp:TextBox></td>
                            <td colspan="1" style="width: 4px">
                            </td>
                        </tr>
                        <tr>                            
                            <td align="left">
                            </td>
                            <td align="left" >
                            </td>
                            <td align="left" colspan="2">
                            </td>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnPrint" runat="server" Text="Print a copy" CssClass="noprint" /></td>
                            <td align="left" colspan="1">
                                <asp:Button ID="btnSend"  runat="server" Text="Send" OnClick="btnSend_Click" CssClass="noprint" />
                                <asp:Button ID="btnClose"  runat="server" Text="Close" CssClass="noprint" />
                                </td>
                            <td colspan="6">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </asp:Panel> 
                </ContentTemplate> 
               </asp:UpdatePanel>  
                                
            </td>        
        </tr>
   </table>
   <table style="float:right; display:none">
        <tr>            
            <td  align="right">
                <asp:ImageButton ID="ibtnReport" runat="server" ImageUrl="~/images/buttons/add-fault.png" Height="16px" /></td>
        </tr>
   </table>
   
</asp:Content>