﻿Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.Data

Partial Public Class UpdateCustomerDetail
    Inherits System.Web.UI.Page

#Region "Page_Init"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

#End Region

#Region "Page_Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Me.getCustomerInfo()
        End If
    End Sub

#End Region

#Region "Events"

#Region "btnSave_Click"

    Protected Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Me.UpdateCustomerContactDetail()

    End Sub

#End Region

#End Region

#Region "GetCustomerInfo"

    ''Get the customer Info to be displayed on welcome page
    Private Sub getCustomerInfo()
        Try
            Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()

            '' Obtaining object from Session
            Dim custHeadBO As CustomerHeaderBO = Nothing
            custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            '' Passing info to custDetailsInfo, that will be used to populate customer's Home page
            custDetailsBO.Id = CType(custHeadBO.CustomerID, Integer)
            custDetailsBO.Tenancy = custHeadBO.TenancyID

            Dim custAddressBO As New CustomerAddressBO
            custDetailsBO.Address = custAddressBO

            Dim objBL As New CustomerManager()

            Dim resultStatus As Boolean = objBL.GetCustomerContactDetail(custDetailsBO)

            If resultStatus Then
                setPresentationLables(custHeadBO, custDetailsBO)
            End If

        Catch ex As Exception
            ' Do nothing
        End Try

    End Sub


#End Region

#Region "PresentationUtilityMethod"

    Private Sub setPresentationLables(ByRef custHeadBO As CustomerHeaderBO, ByRef custDetailsBO As CustomerDetailsBO)

        'Set Presentation lables from CustomerDetailsBO object
        Me.txtFirstName.Text = IIf(custDetailsBO.FirstName <> "", custDetailsBO.FirstName, "")
        Me.txtMiddleName.Text = IIf(custDetailsBO.MiddleName <> "", custDetailsBO.MiddleName, "")
        Me.txtLastName.Text = IIf(custDetailsBO.LastName <> "", custDetailsBO.LastName, "")
        Me.txtHouseNo.Text = IIf(custDetailsBO.Address.HouseNumber <> "", custDetailsBO.Address.HouseNumber, "")
        Me.txtAddressLine1.Text = IIf(custDetailsBO.Address.Address1 <> "", custDetailsBO.Address.Address1, "")
        Me.txtAddressLine2.Text = IIf(custDetailsBO.Address.Address2 <> "", custDetailsBO.Address.Address2, "")
        Me.txtAddressLine3.Text = IIf(custDetailsBO.Address.Address3 <> "", custDetailsBO.Address.Address3, "")
        Me.txtTownCity.Text = IIf(custDetailsBO.Address.TownCity <> "", custDetailsBO.Address.TownCity, "")
        Me.txtCounty.Text = IIf(custDetailsBO.Address.County <> "", custDetailsBO.Address.County, "")
        Me.txtPostCode.Text = IIf(custDetailsBO.Address.PostCode <> "", custDetailsBO.Address.PostCode, "")
        Me.txtTelephone.Text = IIf(custDetailsBO.Address.Telephone <> "", custDetailsBO.Address.Telephone, "")
        Me.txtTelMobile.Text = IIf(custDetailsBO.Address.TelephoneMobile <> "", custDetailsBO.Address.TelephoneMobile, "")
        Me.txtEmail.Text = IIf(custDetailsBO.Address.Email <> "", custDetailsBO.Address.Email, "")


    End Sub

#End Region

#Region "SaveCustomerContactDetail"

    Private Sub UpdateCustomerContactDetail()

        If Page.IsValid Then

            'Create the customer BO obj
            Dim cusBO As CustomerBO = New CustomerBO()

            'Create the customer Address BO obj
            Dim cusAddrBO As CustomerAddressBO = New CustomerAddressBO()

            'Obtaining object from Session
            Dim custHeadBO As CustomerHeaderBO = Nothing

            'Create the customer BL obj
            Dim cusBL As CustomerManager = New CustomerManager()

            Dim resultStatus As Boolean

            Try

                custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

                'Passing info to custDetailsInfo, that will be used to populate customer's Home page
                cusBO.Id = CType(custHeadBO.CustomerID, Integer)

                'Fill the customer BO and customer Address BO
                Me.FillCustomerContactDetail(cusBO, cusAddrBO)

                resultStatus = cusBL.UpdateCustomerContactDetail(cusBO)

            Catch ex As Exception
                ' Do nothing
            End Try

            If resultStatus Then
                Me.lblMessage.CssClass = "info_message_label"
                Me.lblMessage.Text = "Customer detail has been updated successfully."
            Else
                Me.lblMessage.CssClass = "error_message_label"
                Me.lblMessage.Text = "Customer detail updation has been failed."
            End If

        End If

    End Sub

#End Region

#Region "FillCustomerContactDetail"

    Private Sub FillCustomerContactDetail(ByRef cusBO As CustomerBO, ByRef cusAddrBO As CustomerAddressBO)

        'Set the Customer BO obj
        cusBO.FirstName = Me.txtFirstName.Text.Trim()
        cusBO.MiddleName = Me.txtMiddleName.Text.Trim()
        cusBO.LastName = Me.txtLastName.Text.Trim()

        cusBO.Address = cusAddrBO

        'Set the Customer Address BO obj
        cusBO.Address.HouseNumber = Me.txtHouseNo.Text.Trim()
        cusBO.Address.Address1 = Me.txtAddressLine1.Text.Trim()
        cusBO.Address.Address2 = Me.txtAddressLine2.Text.Trim()
        cusBO.Address.Address3 = Me.txtAddressLine3.Text.Trim()
        cusBO.Address.TownCity = Me.txtTownCity.Text.Trim()
        cusBO.Address.County = Me.txtCounty.Text.Trim()
        cusBO.Address.PostCode = Me.txtPostCode.Text.Trim()
        cusBO.Address.Telephone = Me.txtTelephone.Text.Trim()
        cusBO.Address.TelephoneMobile = Me.txtTelMobile.Text.Trim()
        cusBO.Address.Email = Me.txtEmail.Text.Trim()
    End Sub

#End Region

End Class