﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/MenuMaster.Master" CodeBehind="report_a_fault.aspx.vb" Inherits="tenantsonline.report_a_fault" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="page_header_area">
    <div id="head" class="rightheading px-3 py-6 border border-grey bg-white flex">
                 <div class="toggle-button burger" onclick="myFunction(this)">
                                    <div class="bar1"></div>
                                    <div class="bar2"></div>
                                    <div class="bar3"></div>
                                </div>
<h1 class="header uppercase text-grey text-base p-2 w-full">Repairs</h1>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
    <div id="faultPage" class="leading  m-3 mb-8 border border-grey rounded bg-white w-full">
        <div class="flex flex-wrap w-full border-grey">
            <div class="uppercase text-orange font-bold p-3 w-1/5 text-center border-r border-grey">Repairs</div>
            <div class="uppercase p-3 border-b border-grey w-4/5 text-grey-light font-bold">Schedule a Repair</div>
        </div>
        <div class="p-3 mt-6">

            <p>
                For more information about our repairs service <a href="https://www.broadlandgroup.org/existing-customers/repairs/report-a-repair/">click here</a>.
            </p>
        </div>
    </div>
    <script>
        jQuery('#ctl00_LeftMenuControl1_lnkReportFault').addClass('active');
    </script>
</asp:Content>