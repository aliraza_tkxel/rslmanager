<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="change_password.aspx.vb" Inherits="tenantsonline.ChangePassword" Title="Tenants Online :: Change Password" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="page_header_area">
    <div id="head" class="rightheading px-3 py-6 border border-grey bg-white flex">
                 <div class="toggle-button burger" onclick="myFunction(this)">
                                    <div class="bar1"></div>
                                    <div class="bar2"></div>
                                    <div class="bar3"></div>
                                </div>
<h1 class="header uppercase text-grey text-base p-2 w-full">Change Password</h1>
    </div>
</asp:Content>
<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <div id="accountDetailsPage" class="leading p-3 m-3 mb-8 border border-grey rounded bg-white w-full">
<div class="header uppercase text-grey text-base p-2" style="width: 98%">Change my password</div>
    <table cellpadding="3" class=" w-full">
      

        <tr>
                <td colspan="3">
                    <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" />
                </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="Label1" runat="server" Text="Please enter your current and new password" CssClass="sub_header_black white" /></td>
        </tr>
        
        <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblOldPassword" runat="server" CssClass="caption" 
                        Text="Current Password:" Width="141px" />
                    </td>
                <td style="width: 109px">
                                <asp:TextBox ID="txtOldPassword" runat="server"  MaxLength="150" TextMode="Password" />
                </td>
                
                <td style="width: 371px">
                    <asp:RequiredFieldValidator ID="valRequireCurrentPassword" runat="server" ControlToValidate="txtOldPassword"
                        Display="Dynamic" ErrorMessage="Current password missing" SetFocusOnError="True" ToolTip="Current password missing">*</asp:RequiredFieldValidator>
                    </td>
        </tr>
        
        <tr>
                <td style="text-align: right">
                    &nbsp;</td>
                
                <td>
                    &nbsp;</td>
                
                <td style="width: 371px">
                    &nbsp;</td>
                
        </tr>
        
        <tr>
                <td align="right">
                    <asp:Label ID="lblPassword" runat="server" CssClass="caption" Text="New Password:" Width="122px" /></td>
                <td>
                    <asp:TextBox ID="txtNewPassword" runat="server"  TextMode="Password" MaxLength="150" /></td>
                <td style="width: 371px">
                    <asp:RequiredFieldValidator ID="valRequireNewPassword" runat="server" ControlToValidate="txtNewPassword"
                        Display="Dynamic" ErrorMessage="New password missing" SetFocusOnError="True" ToolTip="Password missing">*</asp:RequiredFieldValidator></td>
        </tr>
    
        <tr>
                <td style="text-align: right" align="right">
                    <asp:Label ID="lblNewEmail" runat="server" CssClass="caption" Text="Confirm New Password:" Width="155px" />
                </td>

                <td>
                    <asp:TextBox ID="txtConfirmNewPassword" runat="server" MaxLength="150" TextMode="Password" />
                </td>
                
                <td style="width: 371px">
                    <asp:RequiredFieldValidator ID="valRequireConfirmNewPassword" runat="server" ControlToValidate="txtConfirmNewPassword"
                        Display="Dynamic" ErrorMessage="Confirm new password missing" SetFocusOnError="True" ToolTip="New Email missing">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="valCompareConfirmPassword" runat="server" ControlToCompare="txtNewPassword"
                        ControlToValidate="txtConfirmNewPassword" ErrorMessage="Password does not match"
                        SetFocusOnError="True" ToolTip="Password does not match">!</asp:CompareValidator></td>
        </tr>
        
        <tr>
                <td>
                </td>
                
                <td align="right">
                       </td>
                
                <td style="width: 371px">

                </td>
        </tr>
    </table>
        <div class="w-full flex flex-wrap p-3">
            <div class="w-4/5"></div>
            <div class="w-1/5"> <asp:LinkButton ID="btnContinue" class="w-full mt-6 bg-orange text-white p-3 rounded uppercase mt-6" runat="server">Continue</asp:LinkButton>
            </div>
        </div>
    </div>
                    <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valCompareConfirmNewPsw" runat="server" TargetControlID="valCompareConfirmPassword" />
                    
                    <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valRequireConfirmNewPassword" runat="server" TargetControlID="valRequireConfirmNewPassword" />

                    <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valRequireNewPassword" runat="server" TargetControlID="valRequireNewPassword" />
                    
                    <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valRequireCurrentPassword" runat="server" TargetControlID="valRequireCurrentPassword" />                  <ajaxToolkit:ToolkitScriptManager
                        ID="ToolkitScriptManager_ChangePassword" runat="server">
                    </ajaxToolkit:ToolkitScriptManager>
    <script>
        jQuery('#ctl00_LeftMenuControl1_lnkMyDetails').addClass('active');
    </script>
    
</asp:Content>
