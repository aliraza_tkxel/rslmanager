<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="Report_Asb.aspx.vb" Inherits="tenantsonline.ReportAsb" Title="Tenants Online :: Report Anti Social Behaviour" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">

    <table cellpadding="3" class="table_content">
        <tr>
            <td colspan="3" align="left">
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Text="I am a victim of Anti Social Behaviour"></asp:Label>
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label"></asp:Label></td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <table class="table_box">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="22px"
                                Text="Name: " Width="626px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" Height="21px"
                                Text="Tenancy Reference Number: " Width="628px"></asp:Label></td>
                    </tr>
                </table>
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lblCategory" runat="server" Text="Category:" Width="56px" CssClass="caption"></asp:Label></td>
            <td style="width: 220px">
                <asp:DropDownList ID="ddlASBCategory" runat="server" CssClass="select_normal" EnableTheming="True">
                </asp:DropDownList></td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right" style="height: 91px">
                <asp:Label ID="lblExplaination" runat="server" CssClass="caption" Text="Explain your problem:"
                    Width="134px"></asp:Label></td>
            <td colspan="2" rowspan="1" style="height: 91px">
                <asp:TextBox ID="txtExplaination" runat="server" CssClass="input_normal" Height="77px"
                    TextMode="MultiLine" Width="352px" MaxLength="4000"></asp:TextBox></td>
            <td colspan="1" rowspan="1" style="height: 91px" valign="top">
                <asp:RequiredFieldValidator ID="valRequireDesc" runat="server" ControlToValidate="txtExplaination"
                    ErrorMessage="Problem explanation missing">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="right" valign="top">
                &nbsp;</td>
            <td style="width: 220px">
                &nbsp;</td>
            <td align="right">
                &nbsp;</td>
            <td align="right">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" valign="top">
            </td>
            <td rowspan="1" style="width: 220px">
                By submitting this form, I verify that the information I've povided is true and 
                accurate to the best of your knowledge.</td>
            <td rowspan="1" align="right">
                <asp:LinkButton ID="btnReportASB" runat="server">Report ASB</asp:LinkButton>
            </td>
            <td align="right" rowspan="1">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="pnlText" runat="server" Height="50px" Width="420px" CssClass="caption">
                  <b>You will need to provide details of:</b>
                    <ul>
                    <li>Date and time of the incident</li>
                    <li>Description of the incident</li>
                    <li>Who is involved (and their address)</li>
                    <li>Any witnesses</li>
                    <li>What impact is this incident having on you?</li>
                    <li>What action have you taken? (e.g. contacted the police/spoken with your neighbour</li>
                    <li>Any crime reference numbers</li>
                    </ul>
                Upon receiving your complaint, we will allocate a grade, which will be confirmed to you and where appropriate, arrange the first appointment with a Neighbourhood Officer.  The complaint will be graded on a scale of 1 to 4, with grade 1 cases being the most serious.  The Officer will not usually take any formal action on cases graded 4, other than to note them on file.
                    <br />
                <br />
                    Please be assured that we take any complaints seriously and your information 
                    will be dealt with in a sensitive manner and confidentially. Each complaint will 
                    have a named Officer who will be dealing with it. Help and support is available 
                    from a range of agencies.
                </asp:Panel>
            </td>
        </tr>
        
        <tr>
            <td colspan="3" rowspan="1">
                <hr />
               </td>
            <td colspan="1" rowspan="1">
            </td>
        </tr>
        <tr>
            <td colspan="2" rowspan="1">
                <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager_reportAbandoned" runat="server">
                </ajaxToolkit:ToolkitScriptManager>
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valRequireExplaination"
                    runat="server" TargetControlID="valRequireDesc">
                </ajaxToolkit:ValidatorCalloutExtender>
            </td>
            <td rowspan="1">
                &nbsp;
            </td>
            <td rowspan="1">
            </td>
        </tr>
        <tr>
            <td colspan="4" rowspan="1">
    <asp:UpdatePanel ID="UpdatePanel_Popup" runat="server">
        <ContentTemplate>
            &nbsp;<asp:UpdateProgress ID="UpdateProgress_asb" runat="server" DisplayAfter="5">
                <ProgressTemplate>
                    <asp:Panel ID="pnlProgress" runat="server" BackColor="Silver" Font-Names="Arial"
                        Font-Size="Small" Height="40px" HorizontalAlign="Center" Width="221px">
                        <asp:Image ID="imgProgressBar" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" /><br />
                        Loading ...
                    </asp:Panel>
                    &nbsp;
                    <cc2:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender_progress" runat="server"
                        HorizontalSide="Center" TargetControlID="pnlProgress" VerticalSide="Middle">
                    </cc2:AlwaysVisibleControlExtender>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
            <asp:LinkButton ID="lnkPrevASB" runat="server" CssClass="link_item" Width="231px">> Show me my previous ASB</asp:LinkButton>
            <asp:Panel ID="pnlPopup" runat="server" BackColor="White" BorderColor="Transparent">
            
                <table border="0">
                    <tr>
                            <td colspan="1" style="height: 21px; background-color: #c00000">
                                <asp:Label ID="lblDragHandle" runat="server" BackColor="#C00000" Font-Bold="True"
                                    Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Previous ASB"
                                    Width="97%"></asp:Label></td>
                    </tr>
                    <tr>
                            <td align="right" colspan="1" rowspan="1" style="background-color: white" valign="top">
                                &nbsp;<asp:LinkButton ID="btnClose" runat="server" CssClass="link_item" Width="55px">< Back</asp:LinkButton></td>
                    </tr>
                    <tr>
                            <td colspan="1" style="width: 100px; background-color: white">
                                <asp:FormView ID="FormViewPreviousASB" runat="server" AllowPaging="True" EmptyDataText="No ASB exists" OnPageIndexChanging="FormViewPreviousASB_PageIndexChanging">
                                    <PagerStyle Font-Italic="False" Font-Names="Arial" Font-Size="Small" />
                                    <ItemTemplate>
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td align="right" colspan="1" rowspan="1" style="height: 25px; background-color: white"
                                                        valign="top">
                        <asp:Label ID="lblCrDate" runat="server" CssClass="caption" Text="Creation date:" Width="94px"></asp:Label></td>
                                                    <td align="left" colspan="1" rowspan="1" style="height: 25px; background-color: white"
                                                        valign="top">
                        <asp:Label ID="lblPopupAsbCreationDate" runat="server" CssClass="caption"
                            Text='<%# Utility.FormatDate(Eval("CreationDate")) %>' Width="385px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="1" rowspan="1" style="width: 100px; height: 26px; background-color: white">
                        <asp:Label ID="lblStat" runat="server" CssClass="caption" Text="Status:" Width="94px"></asp:Label></td>
                                                    <td colspan="1" rowspan="1" style="width: 100px; height: 26px; background-color: white">
                        <asp:Label ID="lblPopupStatus" runat="server" CssClass="caption" Text='<%# Eval("ItemStatus") %>'
                            Width="392px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="1" rowspan="1" style="width: 100px; height: 26px; background-color: white">
                        <asp:Label ID="lblcat" runat="server" CssClass="caption" Text="Category:" Width="94px"></asp:Label></td>
                                                    <td colspan="1" rowspan="1" style="width: 100px; height: 26px; background-color: white">
                        <asp:Label ID="lblPopupAsbCategory" runat="server" CssClass="caption" Text='<%# Eval("CategoryText") %>'
                            Width="392px"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="1" style="width: 100px; background-color: white" valign="top">
                        <asp:Label ID="lblExp" runat="server" CssClass="caption" Text="Explanation:" Width="94px"></asp:Label></td>
                                                    <td colspan="1" style="width: 100px; background-color: white">
                                                        <div style="overflow: auto; width: 400px; height: 250px">
                            <asp:Label ID="lblPopupAsbExplaination" runat="server" CssClass="caption" Text='<%# Bind("Description") %>'></asp:Label></div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ItemTemplate>
                                </asp:FormView>
                            </td>
                    </tr>
                </table>
                <cc2:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="btnClose" Drag="True" PopupControlID="pnlPopup" PopupDragHandleControlID="lblDragHandle"
                    TargetControlID="lnkPrevASB">
                </cc2:ModalPopupExtender>
        </asp:Panel>
            </td>
        </tr>
    </table>
    <br />

</asp:Content>
