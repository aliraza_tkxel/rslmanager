Imports System

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.Utilities


Partial Public Class MoveHouse
    Inherits System.Web.UI.Page

#Region "Event"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.DisplayCustomerInfo()
            Me.GetLocalAuthorityLookUp()
        End If

    End Sub

    Private Sub btnSendRequest_Click(sender As Object, e As System.EventArgs) Handles btnSendRequest.Click
        Me.SendMoveHouseRequest()
    End Sub

    Protected Sub ddlLocalAuthority_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlLocalAuthority.SelectedIndexChanged
        Me.GetDevelopmentLookUp()
    End Sub

#End Region

#Region "Functions"


#End Region

#Region "Methods"

#Region "DisplayCustomerInfo Function"

    '' Used to display customer name, address and tenanacy ref no
    Private Sub DisplayCustomerInfo()

        Dim custHeadBO As CustomerHeaderBO = Nothing

        '' Obtaining object from seesion and casting to CustomerHeaderBO
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Me.lblName.Text &= custHeadBO.Title & " " & custHeadBO.FullName

        Me.lblTenancyRefNo.Text &= custHeadBO.TenancyID

        '' To display customer address
        Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()
        custDetailsBO.Id = custHeadBO.CustomerID
        Dim objBL As CustomerManager = New CustomerManager()
        Dim statusFlag As Boolean = objBL.GetCustomerInfo(custDetailsBO, False)

        If statusFlag Then
            Me.txtAddressDetails.Text = custDetailsBO.Address.CustomerAddress
        End If

    End Sub

#End Region

#Region "sendMoveHouseRequest"

    Private Sub SendMoveHouseRequest()

        Dim moveHouseBO As New HouseMoveBO()

        Dim headerBO As CustomerHeaderBO = Nothing

        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        moveHouseBO.CreationDate = Date.Now.Date
        moveHouseBO.Description = Me.txtMoveReason.Text
        ''moveHouseBO.RemoveFlag = 0

        ''itemStatusId for "New" in C_STATUS Table
        moveHouseBO.ItemStatusId = Integer.Parse(ApplicationConstants.CustomerEnquiryStatusNew)
        moveHouseBO.TenancyId = headerBO.TenancyID
        moveHouseBO.CustomerId = headerBO.CustomerID
        'termBO.JournalId = Nothing

        ''itemNatureId for "House Move Request" in C_NATURE Table
        moveHouseBO.ItemNatureId = 55
        moveHouseBO.LocalAuthorityID = Me.ddlLocalAuthority.SelectedValue
        moveHouseBO.DevelopmentID = Me.ddlDevelopment.SelectedValue
        moveHouseBO.NoOfBedrooms = Me.ddlBedroomCount.SelectedValue
        moveHouseBO.OccupantsNoGreater18 = Me.ddlOccoupantsCountMore18.SelectedValue
        moveHouseBO.OccupantsNoBelow18 = Me.ddlOccoupantsCountLess18.SelectedValue

        Dim objBL As New EnquiryManager()
        objBL.SendHouseMoveRequest(moveHouseBO)

        If moveHouseBO.IsFlagStatus Then
            Response.Redirect("~/secure/home.aspx")
        Else
            Me.lblErrorMessage.Text = UserInfoMsgConstants.RequestSendingFailed
        End If


    End Sub

#End Region

#Region "GetLookUpValues"

#Region "GetLocalAuthorityLookUp"

    Private Sub GetLocalAuthorityLookUp()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getLocalAuthorityLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If Not lstLookUp Is Nothing Then
            Me.ddlLocalAuthority.DataSource = lstLookUp
            Me.ddlLocalAuthority.DataValueField = "LookUpValue"
            Me.ddlLocalAuthority.DataTextField = "LookUpName"
            Me.ddlLocalAuthority.DataBind()
        End If
    End Sub

#End Region

#Region "GetDevelopmentLookUp"

    Private Sub GetDevelopmentLookUp()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getDevelopmentLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp, Me.ddlLocalAuthority.SelectedValue)

        If Not lstLookUp Is Nothing Then
            Me.ddlDevelopment.Items.Clear()
            Me.ddlDevelopment.Items.Add(New ListItem("Please select", "-1"))
            Me.ddlDevelopment.DataSource = lstLookUp
            Me.ddlDevelopment.DataValueField = "LookUpValue"
            Me.ddlDevelopment.DataTextField = "LookUpName"
            Me.ddlDevelopment.DataBind()
        End If
    End Sub

#End Region

#End Region

#End Region

End Class