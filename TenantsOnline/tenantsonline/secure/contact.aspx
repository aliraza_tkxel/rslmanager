﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/MenuMaster.Master"
    CodeBehind="contact.aspx.vb" Inherits="tenantsonline.contact" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="page_header_area">
    <div id="head" class="rightheading px-3 py-6 border border-grey bg-white flex">
               <div class="toggle-button burger" onclick="myFunction(this)">
                                  <div class="bar1"></div>
                                  <div class="bar2"></div>
                                  <div class="bar3"></div>
                              </div>
  <h1 class="header uppercase text-grey text-base p-2 w-full">Contact Us</h1>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
    <div id="contactPage" class="leading p-3 m-3 mb-8 border border-grey rounded bg-white w-full">
        <h2 class="header uppercase text-grey text-base p-2">Contact Us</h2>

<div>
    <div>
<label class="w-full">What would you like to contact us about?</label>
  <select class="w-1/3">
      <option>Request Garage Parking</option>
      <option>Report vandalism / neglect</option>
      <option>Report Anti Social Behaviour</option>
      <option>I'd like to end my tenancy</option>
      <option>House Move</option>
      <option>Make a complaint</option>
  </select>
    </div>
    <div>
        <label class="mt-3">Your message</label>
        <textarea class="w-full bg-grey-light" rows="30"></textarea>
    </div>
    <div class="w-full mt-3 flex flex-wrap"><div class="w-4/5"></div><a href="" class="w-1/5 btn bg-orange text-white p-3 rounded uppercase">Contact us</a></div>

</div>

    </div>
    <script>
        jQuery('#ctl00_LeftMenuControl1_lnkOffices').addClass('active');
    </script>
</asp:Content>
