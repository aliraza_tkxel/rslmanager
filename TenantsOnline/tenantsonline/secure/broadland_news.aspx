<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="broadland_news.aspx.vb" Inherits="tenantsonline.broadland_news" Title="Tenants Online :: Broadland News" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
 <div class="sectionHead" style="width: 98%">Broadland Housing News</div>
    <table cellpadding="3" class="table_content" width="100%">
        <tr>
            <td align="left" colspan="3" style="height: 60px">
           
                
              
                <cc2:ToolkitScriptManager ID="ToolkitScriptManager_BroadlandNews" runat="server">
                </cc2:ToolkitScriptManager>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="height: 25px">
                <asp:UpdatePanel ID="UpdatePanel_latestNews" runat="server">
                    <ContentTemplate>
                <asp:FormView ID="FormView_latestNews" runat="server" DataSourceID="ObjectDataSource_latestNews">
                    <ItemTemplate>
                        <table width="100%">
                            <tr>
                                <td style="width: 398px">
                                    <asp:Label ID="lblHeadingHeadLine" runat="server" CssClass="sub_header_black" Text='<%# Bind("HeadLine") %>'></asp:Label></td>
                                <td rowspan="3" style="width: 3px" valign="top">
                                    <asp:Image ID="imgHeadingNewsImage" runat="server" ImageUrl='<%# Bind("ImagePath") %>' OnDataBinding="imgHeadingNewsImage_DataBinding" AlternateText="No image" /></td>
                            </tr>
                            <tr>
                                <td style="width: 398px">
                                    <div style="overflow:hidden;width:400px;height:145px">
                                        <asp:Label ID="lblHeadingNewsContent" runat="server" CssClass="caption" Text='<%# Bind("NewsContent") %>' OnDataBinding="lblHeadingNewsContent_DataBinding"></asp:Label></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 398px; height: 21px;">
                                    <asp:LinkButton ID="lnkNewsStoryDetails" runat="server" CssClass="link_item">Go to story ></asp:LinkButton></td>
                            </tr>
                        </table>
                        <asp:Panel ID="pnlPopup" runat="server" BorderColor="Transparent" BackColor="White">
                            <table border="0">
                                <tbody>
                                    <tr>
                                        <td colspan="2" style="height: 21px; background-color: #c00000">
                                            <asp:Label ID="lblViewNewsDetails" runat="server" BackColor="#C00000" Font-Bold="True"
                                                Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Broadland Housing News"
                                                Width="97%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="right" rowspan="1" style="background-color: white" valign="top" colspan="2">
                                            <asp:LinkButton ID="btnClose" runat="server" CssClass="link_item" Width="147px">< Back to News Page</asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td align="right" rowspan="3" style="width: 100px; background-color: white; border-left-color: white; border-bottom-color: white; border-top-color: white; border-right-color: white;" valign="top">
                                            <asp:Image ID="imgPopupImage" runat="server" ImageUrl='<%# Bind("ImagePath") %>' OnDataBinding="imgPopupImage_DataBinding" AlternateText="No image" /></td>
                                        <td colspan="1" rowspan="2" style="width: 100px; background-color: white;">
                                            <asp:Label ID="lblPopupNewsHeading" runat="server" CssClass="sub_header_black" Text='<%# Bind("HeadLine") %>' Width="392px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td colspan="1" style="width: 100px; background-color: white">
                                            <div style="overflow:auto;width:400px;height:250px">
                                                <asp:Label ID="lblPopupNewsContents" runat="server" CssClass="caption" Text='<%# Bind("NewsContent") %>'></asp:Label></div>
                                            </td>
                                    </tr>
                                </tbody>
                            </table>
                                        <cc2:modalpopupextender
                                                id="mdlPopup" runat="server" backgroundcssclass="modalBackground" cancelcontrolid="btnClose"
                                                drag="True" popupcontrolid="pnlPopup" popupdraghandlecontrolid="lblViewNewsDetails"
                                                targetcontrolid="lnkNewsStoryDetails"></cc2:modalpopupextender>
                        </asp:Panel>
                    </ItemTemplate>
                </asp:FormView>
                <asp:ObjectDataSource ID="ObjectDataSource_latestNews" runat="server" SelectMethod="getLatestNews"
                    TypeName="Broadland.TenantsOnline.BusinessLogic.UtilityManager"></asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="height: 25px">
                <hr />
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="height: 25px">
                <asp:UpdatePanel ID="UpdatePanelNews" runat="server">
                    <ContentTemplate>
                <asp:DataList ID="DataList_news" runat="server" DataSourceID="ObjectDataSource_News">
                    <ItemTemplate><table width="100%">
                        <tr>
                            <td style="width: 398px">
                                <asp:Label ID="lblHeadingHeadLine" runat="server" CssClass="sub_header_black" Text='<%# Bind("HeadLine") %>'></asp:Label></td>
                            <td rowspan="1" style="width: 3px" valign="top">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                    <asp:Label ID="lblNewsContent"  runat="server" CssClass="caption" Text='<%# Bind("NewsContent") %>' OnDataBinding="lblNewsContent_DataBinding"></asp:Label></td>
                            <td rowspan="1" style="width: 110px" valign="top">
                                <asp:LinkButton ID="lnkNewsStoryDetails" runat="server" CssClass="link_item">Go to story ></asp:LinkButton></td>
                        </tr>
                    </table>
                        <asp:Panel ID="pnlPopup" runat="server" BorderColor="Transparent" BackColor="White">
                            <table border="0">
                                <tbody>
                                    <tr>
                                        <td colspan="2" style="height: 21px; background-color: #c00000">
                                            <asp:Label ID="lblViewNewsDetails" runat="server" BackColor="#C00000" Font-Bold="True"
                                                Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Broadland Housing News"
                                                Width="97%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="right" rowspan="1" style="background-color: white" valign="top" colspan="2">
                                            <asp:LinkButton ID="btnClose" runat="server" CssClass="link_item" Width="147px">< Back to News Page</asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td align="right" rowspan="3" style="width: 100px; background-color: white; border-left-color: white; border-bottom-color: white; border-top-color: white; border-right-color: white;" valign="top">
                                            <asp:Image ID="imgPopupImage" runat="server" ImageUrl='<%# Bind("ImagePath") %>' OnDataBinding="imgPopupImage_DataBinding1" AlternateText="No image" /></td>
                                        <td colspan="1" rowspan="2" style="width: 100px; background-color: white;">
                                            <asp:Label ID="lblPopupNewsHeading" runat="server" CssClass="sub_header_black" Text='<%# Bind("HeadLine") %>'
                                                Width="392px"></asp:Label></td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td colspan="1" style="width: 100px; background-color: white">
                                            <div style="overflow:auto;width:400px;height:300px">
                                                <asp:Label ID="lblPopupNewsContents" runat="server" CssClass="caption" Text='<%# Bind("NewsContent") %>'></asp:Label></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <cc2:modalpopupextender
                                                id="mdlPopup" runat="server" backgroundcssclass="modalBackground" cancelcontrolid="btnClose"
                                                drag="True" popupcontrolid="pnlPopup" popupdraghandlecontrolid="lblViewNewsDetails"
                                                targetcontrolid="lnkNewsStoryDetails">
                            </cc2:ModalPopupExtender>
                        </asp:Panel>
                        
                    </ItemTemplate>
                </asp:DataList>
                <asp:ObjectDataSource ID="ObjectDataSource_News" runat="server" SelectMethod="getNewsList"
                    TypeName="Broadland.TenantsOnline.BusinessLogic.UtilityManager"></asp:ObjectDataSource>
                    </ContentTemplate>
                </asp:UpdatePanel>
                &nbsp;&nbsp;
            </td>
        </tr>
    </table>
  </asp:Content>
