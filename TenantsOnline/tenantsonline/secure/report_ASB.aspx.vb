Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class ReportAsb
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        ''Get the tenant name and his/her tenancy id from session variables
        Me.lblName.Text &= custHeadBO.Title & " " & custHeadBO.FullName
        Me.lblTenancyRefNo.Text &= custHeadBO.TenancyID

    End Sub

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.GetLookUpValues()
            Me.GetPreviousASB()
        End If

        Me.txtExplaination.Focus()

    End Sub

    Protected Sub btnReportASB_Click(sender As Object, e As EventArgs) Handles btnReportASB.Click
        Me.reportASB()
    End Sub

    Protected Sub FormViewPreviousASB_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.FormViewPageEventArgs)
        Me.FormViewPreviousASB.PageIndex = e.NewPageIndex
        Dim objAsbList As AsbList = CType(ViewState.Item("myAsbList"), AsbList)
        Me.FormViewPreviousASB.DataSource = objAsbList
        Me.FormViewPreviousASB.DataBind()
        Me.mdlPopup.Show()
    End Sub

#End Region

#Region "Methods"

#Region "SendAsbReport"

    Private Sub reportASB()

        Dim objAsbBO As New AsbBO()

        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        objAsbBO.CreationDate = Date.Now.Date
        objAsbBO.Description = Me.txtExplaination.Text
        ''objAsbBO.RemoveFlag = 0

        ''itemStatusId for "New" in C_STATUS Table
        objAsbBO.ItemStatusId = Integer.Parse(ApplicationConstants.CustomerEnquiryStatusNew)
        objAsbBO.TenancyId = headerBO.TenancyID
        objAsbBO.CustomerId = headerBO.CustomerID


        ''itemNatureId for "Service Complaints" in C_NATURE Table
        objAsbBO.ItemNatureId = 53
        objAsbBO.CategoryID = Me.ddlASBCategory.SelectedValue

        Dim objBL As New EnquiryManager()
        objBL.ReportASB(objAsbBO)

        If objAsbBO.IsFlagStatus Then

            Response.Redirect("~/secure/home.aspx")

        Else

            Me.lblErrorMessage.Text = UserInfoMsgConstants.RequestSendingFailed

        End If

    End Sub

#End Region

#Region "GetLookUpValues"

    Private Sub GetLookUpValues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getAsbCategories
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If Not lstLookUp Is Nothing Then
            Me.ddlASBCategory.DataSource = lstLookUp
            Me.ddlASBCategory.DataValueField = "LookUpValue"
            Me.ddlASBCategory.DataTextField = "LookUpName"
            Me.ddlASBCategory.DataBind()
        End If

    End Sub

#End Region

#Region "GetPreviousASB"

    Private Sub GetPreviousASB()

        Dim objAsbBO As New AsbBO()

        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        objAsbBO.CustomerId = headerBO.CustomerID

        Dim objBL As New EnquiryManager()

        Dim lstAsbBO As AsbList

        lstAsbBO = objBL.GetPreviousASB(objAsbBO)
        ViewState.Add("myAsbList", lstAsbBO)

        Me.FormViewPreviousASB.DataSource = lstAsbBO
        Me.FormViewPreviousASB.DataBind()

    
    End Sub

#End Region

#End Region

#Region "Functions"

    ''No function defined yet...

#End Region

End Class
