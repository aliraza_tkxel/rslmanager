Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class BroadlandComplaint
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        ''Get the tenant name and his/her tenancy id from session variables
        Me.lblName.Text &= custHeadBO.Title & " " & custHeadBO.FullName
        Me.lblTenancyRefNo.Text &= custHeadBO.TenancyID

    End Sub

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.GetLookUpValues()
            Me.GetPreviousComplaints()
        End If
        Me.txtComplaintDesc.Focus()

    End Sub

    Protected Sub FormViewPreviousComplaints_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.FormViewPageEventArgs) Handles FormViewPreviousComplaints.PageIndexChanging
        Me.FormViewPreviousComplaints.PageIndex = e.NewPageIndex
        Dim objCompList As ComplaintList = CType(ViewState.Item("myCompList"), ComplaintList)
        Me.FormViewPreviousComplaints.DataSource = objCompList
        Me.FormViewPreviousComplaints.DataBind()
        Me.mdlPopup.Show()
    End Sub

    Protected Sub btnSendRequest_Click(sender As Object, e As EventArgs) Handles btnSendRequest.Click
        Me.SendComplaint()
    End Sub

#End Region

#Region "Methods"

#Region "SendComplaintRequest"

    Private Sub SendComplaint()

        Dim complBO As New ComplaintBO()

        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        complBO.CreationDate = Date.Now.Date
        complBO.Description = Me.txtComplaintDesc.Text
        ''complBO.RemoveFlag = 0

        ''itemStatusId for "New" in C_STATUS Table
        complBO.ItemStatusId = Integer.Parse(ApplicationConstants.CustomerEnquiryStatusNew)
        complBO.TenancyId = headerBO.TenancyID
        complBO.CustomerId = headerBO.CustomerID
        'termBO.JournalId = Nothing

        ''itemNatureId for "Service Complaints" in C_NATURE Table
        complBO.ItemNatureId = 51
        complBO.CategoryId = Me.ddlComplaintCategory.SelectedValue

        Dim objBL As New EnquiryManager()
        objBL.SendComplaint(complBO)

        If complBO.IsFlagStatus Then

            Response.Redirect("~/secure/home.aspx")

        Else

            Me.lblErrorMessage.Text = UserInfoMsgConstants.RequestSendingFailed

        End If


    End Sub

#End Region

#Region "GetLookUpValues"

    Private Sub GetLookUpValues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getComplaintCategory
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If Not lstLookUp Is Nothing Then
            Me.ddlComplaintCategory.DataSource = lstLookUp
            Me.ddlComplaintCategory.DataValueField = "LookUpValue"
            Me.ddlComplaintCategory.DataTextField = "LookUpName"
            Me.ddlComplaintCategory.DataBind()
        End If
    End Sub

#End Region

#Region "GetPreviousComplaints"

    Private Sub GetPreviousComplaints()

        Dim objCompBO As New ComplaintBO

        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        objCompBO.CustomerId = headerBO.CustomerID

        Dim objBL As New EnquiryManager()

        Dim lstCompBO As ComplaintList

        lstCompBO = objBL.GetPreviousComplaints(objCompBO)
        ViewState.Add("myCompList", lstCompBO)

        Me.FormViewPreviousComplaints.DataSource = lstCompBO
        Me.FormViewPreviousComplaints.DataBind()

    End Sub

#End Region

#End Region

End Class