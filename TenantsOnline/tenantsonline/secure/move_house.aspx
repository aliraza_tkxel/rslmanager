<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="move_house.aspx.vb" Inherits="tenantsonline.MoveHouse" Title="Tenants Online :: Move House" %>

<%@ Register Assembly="Validators" Namespace="Sample.Web.UI.Compatibility" TagPrefix="cc1" %>

<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">

    <table cellpadding="3" class="table_content" style="width: 504px">
        <tr>
            <td colspan="3" align="left">
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Text="I would like to move house"></asp:Label>
    <asp:ScriptManager ID="ScriptManager_moveHouse" runat="server">
    </asp:ScriptManager>
    </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="height: 43px">
                <table class="table_box">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="22px"
                                Text="Name: " Width="300px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" Height="21px"
                                Text="Tenancy Reference Number: " Width="300px"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px" valign="top">
                <asp:Label ID="lblAddressDetails" runat="server" Text="Current address details:"
                    Width="148px" CssClass="caption"></asp:Label></td>
            <td style="width: 285px">
                <asp:TextBox ID="txtAddressDetails" runat="server" CssClass="input_normal" Height="40px"
                    TextMode="MultiLine" Width="280px" ReadOnly="True"></asp:TextBox></td>
            <td valign="top">
                </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px" valign="top">
                <asp:Label ID="lblMoveReason" runat="server" CssClass="caption" Text="I'd like to move because:"
                    Width="159px"></asp:Label></td>
            <td rowspan="2" style="width: 285px">
                <asp:TextBox ID="txtMoveReason" runat="server" CssClass="input_normal" Height="50px"
                    TextMode="MultiLine" Width="280px" MaxLength="4000"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px" valign="top">
                <asp:Label ID="lblOpt" runat="server" CssClass="footnote_item" Text="(optional)"
                    Width="50px"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px">
                </td>
            <td style="width: 285px">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px">
                <asp:Label ID="lblMoveTo" runat="server" CssClass="caption" Text="I'd like to move to:"
                    Width="142px"></asp:Label></td>
            <td style="width: 285px">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px">
                <asp:Label ID="lblTown" runat="server" CssClass="caption" Text="Local Authority:" Width="99px"></asp:Label></td>
            <td style="width: 285px"><asp:UpdatePanel ID="UpdatePanel_LocalAuthority" runat="server">
                <ContentTemplate>
                <asp:DropDownList ID="ddlLocalAuthority" runat="server" CssClass="select_normal" AutoPostBack="True" OnSelectedIndexChanged="ddlLocalAuthority_SelectedIndexChanged" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">Please select</asp:ListItem>
                </asp:DropDownList>
                    <cc1:RequiredFieldValidator ID="valRequireLocalAuthority" runat="server" ControlToValidate="ddlLocalAuthority"
                        ErrorMessage="Please select local authority" InitialValue="-1">*</cc1:RequiredFieldValidator><ajaxToolkit:ValidatorCalloutExtender
                            ID="ValidatorCalloutExtender_valLocalAuthority" runat="server" TargetControlID="valRequireLocalAuthority">
                        </ajaxToolkit:ValidatorCalloutExtender>
                    <asp:UpdateProgress ID="UpdateProgress_LoadddlDevelopment" runat="server" DisplayAfter="10">
                        <ProgressTemplate>
                            <asp:Image ID="imgProgressBar" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </ContentTemplate>
            </asp:UpdatePanel>
            </td>
            <td valign="top">
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px">
                <asp:Label ID="lblStreetName" runat="server" CssClass="caption" Text="Development:"
                    Width="99px"></asp:Label></td>
            <td style="width: 285px"><asp:UpdatePanel ID="UpdatePanel_Development" runat="server">
                <ContentTemplate>
                <asp:DropDownList ID="ddlDevelopment" runat="server" CssClass="select_normal" AppendDataBoundItems="True">
                    <asp:ListItem Value="-1">Please select</asp:ListItem>
                </asp:DropDownList>
                    <cc1:RequiredFieldValidator ID="valRequiredDevelopment" runat="server" ControlToValidate="ddlDevelopment"
                        ErrorMessage="Please select development" InitialValue="-1">*</cc1:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valRequireExplaination"
                        runat="server" TargetControlID="valRequiredDevelopment">
                    </ajaxToolkit:ValidatorCalloutExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px">
                <asp:Label ID="lblBedroomCount" runat="server" CssClass="caption" Text="Number of bedrooms:"
                    Width="156px"></asp:Label></td>
            <td style="width: 285px">
                <asp:DropDownList ID="ddlBedroomCount" runat="server" CssClass="select_normal">
                    <asp:ListItem Selected="True">0</asp:ListItem>
                    <asp:ListItem Value="1"></asp:ListItem>
                    <asp:ListItem Value="2"></asp:ListItem>
                    <asp:ListItem Value="3"></asp:ListItem>
                    <asp:ListItem Value="4"></asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                </asp:DropDownList></td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px">
                <asp:Label ID="lblNumberOfOccupants" runat="server" CssClass="caption" Text="Number of occupants:"
                    Width="156px"></asp:Label></td>
            <td align="left" style="width: 285px">
                &nbsp;
                <asp:Label ID="Label10" runat="server" CssClass="caption" Text="18+" Width="29px"></asp:Label>
                &nbsp;
                <asp:DropDownList ID="ddlOccoupantsCountMore18" runat="server" CssClass="select_small">
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem Value="1"></asp:ListItem>
                    <asp:ListItem Value="2"></asp:ListItem>
                    <asp:ListItem Value="3"></asp:ListItem>
                    <asp:ListItem Value="4"></asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                </asp:DropDownList></td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 162px">
            </td>
            <td style="width: 285px">
                &nbsp;
                <asp:Label ID="Label11" runat="server" CssClass="caption" Text="<18" Width="28px"></asp:Label>
                &nbsp;&nbsp;<asp:DropDownList ID="ddlOccoupantsCountLess18" runat="server" CssClass="select_small">
                    <asp:ListItem>0</asp:ListItem>
                    <asp:ListItem Value="1"></asp:ListItem>
                    <asp:ListItem Value="2"></asp:ListItem>
                    <asp:ListItem Value="3"></asp:ListItem>
                    <asp:ListItem Value="4"></asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                </asp:DropDownList></td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right">
                &nbsp; &nbsp; &nbsp;<asp:LinkButton ID="btnSendRequest" runat="server">Send Request</asp:LinkButton>
            </td>
        </tr>
    </table>
    
</asp:Content>
