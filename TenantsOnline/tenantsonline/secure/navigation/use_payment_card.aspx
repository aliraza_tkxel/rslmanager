<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="use_payment_card.aspx.vb" Inherits="tenantsonline.use_payment_card" %>


<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">
   
    <table cellpadding="3" class="table_content">
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblHeader" runat="server" CssClass="header_black" Text="Can I pay using a Payment Card?" />
            </td>

            <td align="left" colspan="1">
            </td>
            
        </tr>
        <tr>
            <td align="left" colspan="3">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" />
            </td>
            
        </tr>
        
        <tr>
            <td align="left" colspan="2">
                <table class="table_box">
                    <tr>
                        <td>
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="16px"
                                Text="Name: " Width="300px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" Height="16px" Text="Tenancy Reference Number: " Width="300px" />
                        </td>
                        
                    </tr>
                </table>
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
            &nbsp;
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblInstructions1" runat="server" CssClass="caption" Height="16px"
                    Text="Payment cards are a quick and easy way to pay your rent."
                    Width="500px"></asp:Label></td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="1">
              <asp:Label ID="lblInstructions2" runat="server" CssClass="caption" Height="16px"
                    Text="If you would like a payment card then send an email with the following details to this address " Width="562px"></asp:Label>
                 <asp:HyperLink ID="HyperLink2" runat="server" CssClass="caption"  NavigateUrl="mailto:payments@broadlandhousing.org">payments@broadlandhousing.org</asp:HyperLink>
                
             </td>
           
        </tr>
        
       
        <tr>
            <td>
            </td>
            <td align="right" colspan="1">
                &nbsp;</td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
               
               <td>
                <asp:Label ID="lblCaptionNumber" runat="server" CssClass="caption" Height="16px" Text="Tenant Number"
                    Width="497px"></asp:Label></td>
           </tr>         
           <tr>
                           <td>
                <asp:Label ID="lblCaptionName" runat="server" CssClass="caption" Height="16px" Text="Tenant Name"
                    Width="497px"></asp:Label></td>
           </tr>
           <tr>
                           <td>
                <asp:Label ID="lblCaptionAddress" runat="server" CssClass="caption" Height="16px" Text="Address"
                    Width="497px"></asp:Label></td>
             </tr>
             <tr>       
                           <td>
                <asp:Label ID="lblCaptionPostCode" runat="server" CssClass="caption" Height="16px" Text="Post Code"
                    Width="497px"></asp:Label></td>
        </tr>
       <%-- <tr>
            <td>
            </td>
            <td colspan="1">
                <asp:HyperLink ID="HyperLink1" runat="server" CssClass="link_item_small" NavigateUrl="https://www.allpayments.net/allpayments/"
                    Target="_blank">click here</asp:HyperLink>
                <asp:Label ID="lblInstruction4" runat="server" CssClass="caption" Height="16px" Text="to go to AllPayments.net ( "></asp:Label>
                <asp:HyperLink ID="lnkAllpayments" runat="server" NavigateUrl="https://www.allpayments.net/allpayments/"
                    Target="_blank" CssClass="link_item_small">https://www.allpayments.net/allpayments/</asp:HyperLink><asp:Label ID="lblInstruction5" runat="server" CssClass="caption" Height="16px" Text=")"
                    Width="1px"></asp:Label></td>
            <td colspan="1">
            </td>
        </tr>--%>
    </table>
    
</asp:Content>
