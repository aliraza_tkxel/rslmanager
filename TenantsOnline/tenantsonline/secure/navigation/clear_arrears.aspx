﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="clear_arrears.aspx.vb" Inherits="tenantsonline.clear_arrears" %>

<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">
   
    <table cellpadding="3" class="table_content">
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblHeader" runat="server" CssClass="header_black" Text="I need help clearing my arrears" />
            </td>

            <td align="left" colspan="1">
            </td>
            
        </tr>
        <tr>
            <td align="left" colspan="3">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" />
            </td>
            
        </tr>
        
        <tr>
            <td align="left" colspan="2">
                <table class="table_box">
                    <tr>
                        <td>
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="16px"
                                Text="Name: " Width="620px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" 
                                Height="16px" Text="Tenancy Reference Number: " Width="625px" />
                        </td>
                        
                    </tr>
                </table>
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table class="table_box">
                    <tr>
                        <td>
                            <asp:Label ID="lblCurrentOweAmount" runat="server" CssClass="body_caption_bold" Height="16px"
                                Text="* You currently owe" Width="178px"></asp:Label></td>
                        <td style="width: 7px">
                            </td>
                        <td style="width: 7px">
                            <asp:TextBox ID="txtCurrentArrearAmount" runat="server" CssClass="input_small" Font-Bold="True" ReadOnly="True"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblInstructions1" runat="server" CssClass="caption" Height="16px"
                    Text="If you would like help with your arrears, type in the box below to help explain"
                    Width="500px"></asp:Label></td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblInstructions2" runat="server" CssClass="caption" Height="16px"
                    Text="your situation and we will be in touch as soon as we can:" Width="497px"></asp:Label></td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtDetails" runat="server" CssClass="input_normal" 
                    Height="77px" TextMode="MultiLine" Width="628px" MaxLength="4000" />
            </td>
            <td colspan="1" valign="top">
                <asp:RequiredFieldValidator ID="valRequireDetails" runat="server" ControlToValidate="txtDetails"
                    ErrorMessage="Details missing">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" colspan="1">
                <asp:LinkButton ID="btnSend" runat="server">Send</asp:LinkButton>
                &nbsp;
            </td>
            <td align="left" colspan="1">
                </td>
        </tr>
        
                 <tr>
            <td colspan="3">
                <asp:Panel ID="pnlText" runat="server" Height="50px" Width="420px" CssClass="caption">
                    &nbsp;* Please note this figure does not include any anticipated housing benefit.  
                    <br />
Have you spoken to your Income Recovery Officer about making a payment arrangement?  Please contact the Customer Services team who will be able to make an appointment for you.
                    <br />
If you are currently experiencing problems in paying your rent, please contact your local <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.citizensadvice.org.uk">Citizens Advice Bureau</asp:HyperLink> for additional guidance and support. *</asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
                
                <ajaxToolkit:ToolkitScriptManager ID="TenancyTerminationToolkitScriptManager" runat="server" />
                
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_terminationDetails"
                    runat="server" TargetControlID="valRequireDetails"/>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="1">
                &nbsp;</td>
            <td colspan="1">
            </td>
        </tr>
    </table>
    
</asp:Content>
