Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class use_payment_card
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        '' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then

            ''Get the CustomerHeaderBO object from session 
            Dim headerBO As CustomerHeaderBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            ''Displays the customer information from session variables
            Me.lblName.Text &= headerBO.FullName
            Me.lblTenancyRefNo.Text &= headerBO.TenancyID

        End If

    End Sub


#End Region

End Class