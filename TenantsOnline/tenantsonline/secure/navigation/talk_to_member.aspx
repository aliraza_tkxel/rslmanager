<%@ Page Language="vb"  MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="talk_to_member.aspx.vb" Inherits="tenantsonline.talk_to_member" %>

<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">
   
    <table cellpadding="3" class="table_content">
        <tr>
            <td align="left" colspan="3">
                <asp:Label ID="lblHeader" runat="server" CssClass="header_black" Text="I would like to contact the Customer Services Team" Width="640px" />
            </td>
            
        </tr>
        <tr>
            <td align="left" colspan="3">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" />
            </td>
            
        </tr>
        
        <tr>
            <td align="left" colspan="2">
                <table class="table_box">
                    <tr>
                        <td>
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="16px"
                                Text="Name: " Width="300px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" Height="16px" Text="Tenancy Reference Number: " Width="300px" />
                        </td>
                        
                    </tr>
                </table>
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblInstructions1" runat="server" CssClass="caption" Height="16px"
                    Text="The Customer Services team are always happy to talk to you." Width="500px"></asp:Label></td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="Label1" runat="server" CssClass="caption" Height="16px"
                    Text="To help with your enquiry please select one of the options from the drop down"
                    Width="500px"></asp:Label></td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:DropDownList ID="ddlReason" runat="server" CssClass="select_normal">
                </asp:DropDownList></td>
            <td align="left" colspan="1" style="height: 25px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblInstructions3" runat="server" CssClass="caption" Height="16px"
                    Text="and then type in a short description of what you'd like to talk about."
                    Width="497px"></asp:Label></td>
            <td align="left" colspan="1" style="height: 5px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtDetails" runat="server" CssClass="input_normal" Height="77px" TextMode="MultiLine" Width="495px" MaxLength="4000" /></td>
            <td colspan="1" valign="top">
                <asp:RequiredFieldValidator ID="valRequireDetails" runat="server" ControlToValidate="txtDetails"
                    ErrorMessage="Details missing">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" colspan="1">
                <asp:LinkButton ID="btnSend" runat="server">Send</asp:LinkButton>
                &nbsp;&nbsp;
                &nbsp; &nbsp;&nbsp; &nbsp;</td>
            <td align="left" colspan="1">
                </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
                
                <ajaxToolkit:ToolkitScriptManager ID="TenancyTerminationToolkitScriptManager" runat="server" />
                
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_terminationDetails"
                    runat="server" TargetControlID="valRequireDetails"/>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="1">
                &nbsp;</td>
            <td colspan="1">
            </td>
        </tr>
    </table>
    
</asp:Content>