Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class clear_arrears
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        '' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Me.DoProcessRequest()
        End If

    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Me.SendClearArrearRequest()
    End Sub

#End Region

#Region "Methods"

#Region "DoProcessRequest Function"

    Private Sub DoProcessRequest()

        Dim custHeadBO As CustomerHeaderBO = Nothing

        ''Obtaining object from Session
        custHeadBO = Me.GetFromSession()

        '' Displaying customer Name  & Tenancy ID
        Me.DisplayHeader(custHeadBO)

        '' Call to dispaly account\rent balance, arrears action status and notes entered by CRM
        Me.ManageFinancialInfo(custHeadBO)


    End Sub

#End Region

#Region "ManageFinancialInfo Function"

    Private Sub ManageFinancialInfo(ByRef custHeadBO As CustomerHeaderBO)

        Dim custFinBO As CustomerFinancialBO = New CustomerFinancialBO()

        '' Call to Business Layer method to display financial Info 
        Dim custMngr As CustomerManager = New CustomerManager()
        custMngr.ManageFinancialInfo(custFinBO, custHeadBO, False)

        Session("custFinBO") = custFinBO

        Me.DisplayFinancialInfo(custFinBO)

    End Sub


#Region "DisplayFinancialInfo Function"

    Private Sub DisplayFinancialInfo(ByRef custFinBO As CustomerFinancialBO)

        Dim poundSign As String = Nothing

        If custFinBO.Balance < 0 Then

            poundSign = poundSign & "-£"

        Else
            poundSign = poundSign & "£"

        End If


        '' Displaying current rent/account balance
        txtCurrentArrearAmount.Text = poundSign & Decimal.Round(custFinBO.Balance, 2)

    End Sub

#End Region

#End Region

#Region "GetFromSession Function"

    Private Function GetFromSession() As CustomerHeaderBO

        '' Obtaining CustomerHeaderBO object from Session 
        Return CType(Session("custHeadBO"), CustomerHeaderBO)

    End Function

#End Region

#Region "DisplayHeader Function"

    Private Sub DisplayHeader(ByRef custHeadBO As CustomerHeaderBO)

        '' Displaying customer Name  & Tenancy ID

        Dim title As String = custHeadBO.Title

        '' Not displaying title if it is "Other"
        If Not title.Equals("Other", StringComparison.OrdinalIgnoreCase) Then

            '' concatenating label text, title, customer full name and setting it to label
            lblName.Text = lblName.Text & " " & title & " " & custHeadBO.FullName


            '' concatenating label text, Tenancy ID
            lblTenancyRefNo.Text = lblTenancyRefNo.Text & " " & custHeadBO.TenancyID


        End If

    End Sub

#End Region


#Region "SendClearArrearRequest"

    Private Sub SendClearArrearRequest()

        Dim enqLogBO As New EnquiryLogBO

        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        enqLogBO.CreationDate = Date.Now.Date
        enqLogBO.Description = "My arrears:" & Me.txtCurrentArrearAmount.Text & System.Environment.NewLine & Me.txtDetails.Text
        '  enqLogBO.RemoveFlag = 0

        ''itemStatusId for "New" in C_STATUS Table
        enqLogBO.ItemStatusId = Integer.Parse(ApplicationConstants.CustomerEnquiryStatusNew)
        enqLogBO.TenancyId = headerBO.TenancyID
        enqLogBO.CustomerId = headerBO.CustomerID
        'termBO.JournalId = Nothing

        ''itemNatureId for "Arrear" in C_NATURE Table
        enqLogBO.ItemNatureId = 56

        Dim objBL As New EnquiryManager()
        objBL.SendClearArrearRequest(enqLogBO)

        If enqLogBO.IsFlagStatus Then

            Response.Redirect("~/secure/home.aspx")

        Else

            Me.lblErrorMessage.Text = UserInfoMsgConstants.RequestSendingFailed

        End If


    End Sub

#End Region

#End Region

#Region "Functions"

    ''No function defined yet...

#End Region

End Class


