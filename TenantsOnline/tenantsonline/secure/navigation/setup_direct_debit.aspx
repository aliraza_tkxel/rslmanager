﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="setup_direct_debit.aspx.vb" Inherits="tenantsonline.setup_direct_debit" %>

<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">
   
    <table cellpadding="3" class="table_content">
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblHeader" runat="server" CssClass="header_black" Text="How do I set up a direct debit?" />
            </td>

            <td align="left" colspan="1" style="width: 150px">
            </td>
            
        </tr>
        <tr>
            <td align="left" colspan="3">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" />
            </td>
            
        </tr>
        
        <tr>
            <td align="left" colspan="2">
                <table class="table_box">
                    <tr>
                        <td>
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="16px"
                                Text="Name: " Width="567px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" 
                                Height="16px" Text="Tenancy Reference Number: " Width="567px" />
                        </td>
                        
                    </tr>
                </table>
            </td>
            <td align="left" colspan="1" style="width: 150px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
            &nbsp;
            </td>
            <td align="left" colspan="1" style="width: 150px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblInstructions1" runat="server" CssClass="caption" Height="16px"
                    Text="Please complete the Direct Debit Mandate below and send to Broadland Housing"
                    Width="500px"></asp:Label></td>
            <td align="left" colspan="1" style="width: 150px">
            </td>
        </tr>
               
        <!--
        BH REQUESTED TO REMOVE THIS TEXTBOX AND BUTTON
        MODIFIED BY ADNAN MIRZA
        MODIFIED ON 13/8/2008
         -->
        
        <tr>
        
            <td align="left" colspan="2">
                <%--<asp:TextBox ID="txtDetails" runat="server" CssClass="input_normal" Height="77px" TextMode="MultiLine" Width="495px" MaxLength="4000" BorderStyle="Outset" />--%>
            </td>
           
           
            <td colspan="1" valign="top" style="width: 150px">
                
                <%--<asp:RequiredFieldValidator ID="valRequireDetails" runat="server"
                    ErrorMessage="Details missing" ControlToValidate="txtDetails">*</asp:RequiredFieldValidator>--%>
                
            </td>
           
           
        </tr>
        <tr>
            <td>
            </td>
     <%--       <td align="right" colspan="1">
                <asp:ImageButton ID="ibtnSend" runat="server" ImageUrl="~/images/buttons/send.gif" />&nbsp;
            </td>--%>
            <td align="left" colspan="1" style="width: 673px">
                </td>
        </tr>
        
        <tr>
            <td>
            </td>
            <td align="left" colspan="1" valign="middle" style="width: 673px">
                <asp:Image ID="imgAdobeLogo" runat="server" ImageUrl="~/images/adobe_logo.png" />&nbsp;
                <asp:HyperLink ID="lnkDirectDebitForm" runat="server" CssClass="link_item" NavigateUrl="~/download_forms/DD form.pdf" Target="_blank">Direct_Debit_Mandate.pdf (approx 345kb)</asp:HyperLink></td>
            <td align="left" colspan="1" style="width: 150px">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left" colspan="1" style="width: 673px">
                <asp:Label ID="lblDownloadDesc1" runat="server" CssClass="foot_note" Height="16px"
                    Text="You will require Adobe Acrobat reader to view this document." Width="379px"></asp:Label></td>
            <td align="left" colspan="1" style="width: 150px">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left" colspan="1" valign="middle" style="width: 673px">
                <asp:Label ID="lblDownloadDesc2" runat="server" CssClass="foot_note" Height="16px"
                    Text="To download a free copy of Acrobat go to "></asp:Label>
                &nbsp;<asp:HyperLink ID="lnkAdobe" runat="server" NavigateUrl="http://www.adobe.com" Target="_blank" CssClass="link_item_tiny">www.adobe.com</asp:HyperLink></td>
            <td align="left" colspan="1" style="width: 150px">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" colspan="1" style="width: 673px">
            </td>
            <td align="left" colspan="1" style="width: 150px">
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
         
            <td style="width: 673px">
                
                <ajaxToolkit:ToolkitScriptManager ID="TenancyTerminationToolkitScriptManager" runat="server" />
                
               
                <%--<ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_terminationDetails"
                    runat="server" TargetControlID="valRequireDetails"/>--%>
              
            </td>
            
            <td style="width: 150px">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="1" style="width: 673px">
                &nbsp;</td>
            <td colspan="1" style="width: 150px">
            </td>
        </tr>
    </table>
    
</asp:Content>
