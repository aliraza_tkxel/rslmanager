Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class talk_to_member
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        '' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then

            ''Get ddlReason drop down values
            Me.GetCstRequestReasonLookUp()

            ''Get the CustomerHeaderBO object from session 
            Dim headerBO As CustomerHeaderBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            ''Displays the customer information from session variables
            Me.lblName.Text &= headerBO.FullName
            Me.lblTenancyRefNo.Text &= headerBO.TenancyID

            Me.ddlReason.Focus()

        End If

    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Me.SendCstRequest()
    End Sub

#End Region

#Region "Functions"

    ''No functions defined yet...

#End Region

#Region "Methods"

#Region "SendCstRequest"

    Private Sub SendCstRequest()

        Dim cstReqBO As New CstBO

        cstReqBO.RequestNatuerID = Me.ddlReason.SelectedValue
        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        cstReqBO.CreationDate = Date.Now.Date
        cstReqBO.Description = Me.txtDetails.Text
        ' cstReqBO.RemoveFlag = 0

        ''itemStatusId for "New" in C_STATUS Table
        cstReqBO.ItemStatusId = Integer.Parse(ApplicationConstants.CustomerEnquiryStatusNew)
        cstReqBO.TenancyId = headerBO.TenancyID
        cstReqBO.CustomerId = headerBO.CustomerID
        cstReqBO.JournalId = Nothing

        ''itemNatureId for "CST Request" in C_NATURE Table
        cstReqBO.ItemNatureId = 58


        Dim objBL As New EnquiryManager()
        objBL.SendCstRequest(cstReqBO)

        If cstReqBO.IsFlagStatus Then

            Response.Redirect("~/secure/home.aspx")

        Else

            Me.lblErrorMessage.Text = UserInfoMsgConstants.RequestSendingFailed

        End If


    End Sub


#End Region

#Region "LookupMethos"

#Region "GetLocalAuthorityLookUp"

    Private Sub GetCstRequestReasonLookUp()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getCstRequestReasonLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If Not lstLookUp Is Nothing Then
            Me.ddlReason.DataSource = lstLookUp
            Me.ddlReason.DataValueField = "LookUpValue"
            Me.ddlReason.DataTextField = "LookUpName"
            Me.ddlReason.DataBind()
            Me.ddlReason.SelectedValue = "59"
        End If
    End Sub

#End Region

#End Region

#End Region

End Class