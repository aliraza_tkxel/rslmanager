Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class TenancyTermination
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        '' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            ''Get the CustomerHeaderBO object from session 
            Dim headerBO As CustomerHeaderBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            ''Displays the customer information from session variables
            Me.lblName.Text &= headerBO.FullName
            Me.lblTenancyRefNo.Text &= headerBO.TenancyID

            'Add in the comparitor for the 28 days in the future
            Dim currentDate As Date
            currentDate = DateTime.Now.AddDays(27).ToShortDateString
            Comparevalidator1.ValueToCompare = currentDate

            Me.txtDetails.Focus()
        End If

    End Sub

  Protected Sub btnSendRequest_Click(sender As Object, e As EventArgs) Handles btnSendRequest.Click
        Me.SendTerminationRequest()
    End Sub

#End Region

#Region "Functions"

#End Region

#Region "Methods"

    Private Sub SendTerminationRequest()

        Dim termBO As New TerminationBO()
        ''Date conversion function might through an exception so handeld here
        Try
            ''convert the move out date string to date
            Dim moveOutDate As DateTime = Utility.StringToDate(Me.txtTenancyMoveDate.Text)

            termBO.MovingOutDate = moveOutDate

        Catch ex As Exception
            ''Show error message and return from the usecase
            Me.lblErrorMessage.Text = UserInfoMsgConstants.IncorrectMoveOutDateFormat
            Return
        End Try

        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        termBO.CreationDate = Date.Now.Date
        termBO.Description = Me.txtDetails.Text
        ''termBO.RemoveFlag = 0

        ''itemStatusId for "New" in C_STATUS Table
        termBO.ItemStatusId = Integer.Parse(ApplicationConstants.CustomerEnquiryStatusNew)
        termBO.TenancyId = headerBO.TenancyID
        termBO.CustomerId = headerBO.CustomerID
        termBO.JournalId = Nothing

        ''itemNatureId for "Termination" in C_NATURE Table
        termBO.ItemNatureId = 50


        Dim objBL As New EnquiryManager()
        objBL.SendTerminationRequest(termBO)

        If termBO.IsFlagStatus Then
            Response.Redirect("~/secure/home.aspx")
        Else
            Me.lblErrorMessage.Text = UserInfoMsgConstants.RequestSendingFailed
        End If

    End Sub

#End Region

    
End Class