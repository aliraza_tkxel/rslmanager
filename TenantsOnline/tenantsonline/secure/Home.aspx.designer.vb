'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Home
    
    '''<summary>
    '''LinkButton1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LinkButton1 As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lblName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTenantName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTenantName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblEmailCaption control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmailCaption As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmail As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTelCaption control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTelCaption As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTelephone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTelephone As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAdd As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddress As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAddress2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddress2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAddress3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddress3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTown As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCounty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCounty As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPostCode As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblDateOfBirth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDateOfBirth As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblTenancyRef control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTenancyRef As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''imgVul control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgVul As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''pnlVulnerability control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlVulnerability As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblVul control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblVul As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''gvCatList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvCatList As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''lblVulInfo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblVulInfo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ibtnViewVulnerability control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ibtnViewVulnerability As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''btnViewAccountDetails control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnViewAccountDetails As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''lblBalance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBalance As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAccountBalance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAccountBalance As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblText As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''btnViewRentAccount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnViewRentAccount As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnViewFaultDetails control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnViewFaultDetails As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''AppointmentsGridView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AppointmentsGridView As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''btnViewFaultDetails2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnViewFaultDetails2 As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''btnViewEnquiries control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnViewEnquiries As Global.System.Web.UI.WebControls.LinkButton
    
    '''<summary>
    '''pnlEnquiry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlEnquiry As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblRequest1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRequest1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtRequestCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRequestCount As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblRequest2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRequest2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblResponse1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblResponse1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtResponseCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtResponseCount As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''lblResponse2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblResponse2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblNoEnquiry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNoEnquiry As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''btnViewEnquiries2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnViewEnquiries2 As Global.System.Web.UI.WebControls.LinkButton
End Class
