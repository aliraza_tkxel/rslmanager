﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/MenuMaster.Master" CodeBehind="result.aspx.vb" Inherits="tenantsonline.result" %>
<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
 
   <div class="sectionHead" style="width: 98%; margin-bottom: 10px;">Details Updated</div>

    <asp:PlaceHolder ID="phMessage" runat="server">
    Your details have been successfully updated.
    </asp:PlaceHolder>
    <br /><br />
    <asp:LinkButton ID="btnContinue" runat="server" CssClass="btnLink">Continue</asp:LinkButton>
  
     
</asp:Content>
