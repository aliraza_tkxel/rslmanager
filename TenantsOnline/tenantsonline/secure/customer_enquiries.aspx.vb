Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class customer_enquiries
    Inherits System.Web.UI.Page

#Region "Class Level Variables"
    ' Class level variable to hold the current enquiry nature
    Dim EnquiryNature As String
#End Region

#Region "Event"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.GetCustomerEnquiryCount()
            Me.GetEnquiryResponseCount()
            Me.GetCustomerEnquiries()
        Else
            Me.EnquiryNature = ViewState.Item("enqNature")
        End If
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        Me.GetCustomerEnquiryCount()
        Me.GetEnquiryResponseCount()
        Me.GetCustomerEnquiries()
    End Sub

    Protected Sub grdEnquiries_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdEnquiries.PageIndexChanging
        Me.grdEnquiries.PageIndex = e.NewPageIndex
        Dim ds As DataSet = CType(ViewState.Item("myDataSet"), DataSet)
        Me.grdEnquiries.DataSource = ds
        Me.grdEnquiries.DataBind()
    End Sub

    Protected Sub FormView_Responses_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.FormViewPageEventArgs)
        Me.FormView_Responses.PageIndex = e.NewPageIndex
        Dim objJournalList As JournalList = CType(ViewState.Item("myJournalList"), JournalList)
        Me.FormView_Responses.DataSource = objJournalList
        Me.FormView_Responses.DataBind()
        Me.mdlPopup.Show()
    End Sub

    Protected Sub viewDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.HandleViewResponse(sender)
    End Sub

    Protected Sub lnkViewLetter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        Dim historyID As Integer = Integer.Parse(btn.CommandArgument)
        Me.HandleViewLetter(EnquiryNature, historyID)
    End Sub

    Protected Sub btnHideLetterPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.pnlLetter.Visible = False
        Me.mdlPopup.Show()
    End Sub


    Protected Sub btnSendResponse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GetEnquiryResponseCount(sender)
    End Sub

#End Region

#Region "Methods"

#Region "GetCustomerEnquiryCount"

    ' Get the number of enquiries customer made
    Private Sub GetCustomerEnquiryCount()

        Dim enqLogBO As New EnquiryLogBO
        ' Obtaining object from Session

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        enqLogBO.CustomerId = CType(custHeadBO.CustomerID, Integer)

        Dim objEnqDAL As New EnquiryManager

        Dim resultStatus As Boolean = objEnqDAL.GetCustomerEnquiryCount(enqLogBO)

        If enqLogBO.CustomerEnquiryCount > 0 Then
            Me.lblSentEnquiries.Text = "You have sent " & enqLogBO.CustomerEnquiryCount & " enquiries"
        Else
            Me.lblSentEnquiries.Text = "You have sent 0 enquiries"
        End If

    End Sub

#End Region

#Region "GetEnquiryResponseCount"

    ' Get the number of responses against enquiries customer made
    Private Sub GetEnquiryResponseCount()

        Dim enqLogBO As New EnquiryLogBO
        ' Obtaining object from Session

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        enqLogBO.CustomerId = CType(custHeadBO.CustomerID, Integer)

        Dim objEnqDAL As New EnquiryManager

        Dim resultStatus As Boolean = objEnqDAL.GetEnquiryResponseCount(enqLogBO)

        If enqLogBO.EnquiryResponseCount > 0 Then
            Me.lblUnreadResponses.Text = "You have " & enqLogBO.EnquiryResponseCount & " responses"
        Else
            Me.lblUnreadResponses.Text = "You have 0 unread response"
        End If

    End Sub

#End Region

#Region "GetCustomerEnquiries"

    ' Get the enquiries customer made
    Private Sub GetCustomerEnquiries()

        Dim enqLogBO As New EnquiryLogBO
        ' Obtaining object from Session

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        enqLogBO.CustomerId = CType(custHeadBO.CustomerID, Integer)

        Dim objEnqBL As New EnquiryManager

        Dim ds As DataSet = objEnqBL.GetCustomerEnquiries(custHeadBO.CustomerID, Me.ddlStatus.SelectedValue)
        ViewState.Add("myDataSet", ds)
        Me.grdEnquiries.DataSource = ds
        Me.grdEnquiries.DataBind()

    End Sub

#End Region

#Region "GetEnquiryResponses"

    ' Get the responses against customer enquiries
    Private Sub GetEnquiryResponses(ByVal enqLogId As Integer)

        Dim enqLogBO As New EnquiryLogBO
        ' Obtaining object from Session

        enqLogBO.EnquiryLogId = enqLogId

        Dim objEnqBL As New EnquiryManager
        Dim lstJournal As JournalList = objEnqBL.GetEnquiryResponses(enqLogBO)

        ' Removes the last entery of the response list
        Dim lstLength As Integer = lstJournal.Count
        'If lstLength > 1 Then
        'lstJournal.RemoveAt(lstLength - 1)
        'End If

        ViewState.Add("myJournalList", lstJournal)
        Me.FormView_Responses.DataSource = lstJournal
        Me.FormView_Responses.DataBind()

    End Sub

#End Region

#Region "UtilityMethods"

    Private Sub HandleViewResponse(ByRef sender As Object)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        Dim cmdArr As String() = btn.CommandArgument.Split("#")
        Dim enqId As Integer = Integer.Parse(cmdArr(0))
        Dim enqNature As String = cmdArr(3).ToString
        Me.GetEnquiryResponses(enqId)
        Me.SetFormView(cmdArr)
        Me.ViewDetailsHandle(sender)
        Me.mdlPopup.Show()
    End Sub

    Private Sub SetFormView(ByVal cmdArr As String())
        Dim custHeadBo As CustomerHeaderBO = CType(Session("custHeadBO"), CustomerHeaderBO)
        Dim enqId As Integer = Integer.Parse(cmdArr(0))
        Dim enqCreationDate As String = cmdArr(2).ToString
        Dim enqNature As String = cmdArr(3).ToString
        Dim enqItem As String = cmdArr(4).ToString
        Dim enqResponse As String = cmdArr(5).ToString

        Me.EnquiryNature = enqNature
        ViewState.Add("enqNature", enqNature)
        Me.lblDate.Text = enqCreationDate

        ' Get customer information
        Dim custDetailsBO As New CustomerDetailsBO
        custDetailsBO.Id = custHeadBo.CustomerID
        Dim custBL As CustomerManager = New CustomerManager()
        custBL.GetCustomerInfo(custDetailsBO, False)

        ' Set the labels values
        lblDateVal.Text = IIf(enqCreationDate <> "", enqCreationDate, "")
        lblEnquiryNoVal.Text = IIf(Utility.GetEnquiryString(enqId) <> "", Utility.GetEnquiryString(enqId), "")
        lblTenancyNoVal.Text = IIf(custHeadBo.TenancyID <> "", custHeadBo.TenancyID, "")
        lblNameVal.Text = IIf(custHeadBo.FullName <> "", custHeadBo.FullName, "")
        lblEnqryItemVal.Text = IIf(enqItem <> "", enqItem, "")
        lblEnqryNatureVal.Text = IIf(enqNature <> "", enqNature, "")

        lblTelVal.Text = IIf(custDetailsBO.Address.Telephone <> "", custDetailsBO.Address.Telephone, "")
        lblAddressVal.Text = IIf(custDetailsBO.Address.HouseNumber <> "", custDetailsBO.Address.HouseNumber, "")
        lblAddressVal.Text &= IIf(custDetailsBO.Address.Address1 <> "", custDetailsBO.Address.Address1, "")
        lblAddress3.Text = IIf(custDetailsBO.Address.Address3 <> "", custDetailsBO.Address.Address3, "")
        lblCounty.Text = IIf(custDetailsBO.Address.County <> "", custDetailsBO.Address.County, "")
        lblPostCode.Text = IIf(custDetailsBO.Address.PostCode <> "", custDetailsBO.Address.PostCode, "")

        ' For closed enquiries, display the message
        If enqResponse = "Closed" Then
            lblCloseEnquiry.Text = UserInfoMsgConstants.CloseEnquiryMsg
        Else
            lblCloseEnquiry.Text = ""
        End If

        ' Show/Hide view letter on the basis of the enquiry nature
        Me.ShowLetterLink()
    End Sub

    Private Sub HandleViewLetter(ByVal enqNature As String, ByVal historyId As Integer)

        Dim objLetterBO As New LetterBO
        Dim strStoredProcedure As String = String.Empty
        Dim lettercontents As String = String.Empty

        objLetterBO.HistoryID = historyId

        Select Case enqNature
            Case ApplicationConstants.CustEnquiryClearArrearsRequest
                strStoredProcedure = SprocNameConstants.GetArrearLetter

            Case ApplicationConstants.CustEnquiryASBComplaint
                strStoredProcedure = SprocNameConstants.GetAsbLetter

            Case ApplicationConstants.CustEnquiryDirectDebitRentRequest
                strStoredProcedure = SprocNameConstants.GetRentLetter

            Case ApplicationConstants.CustEnquiryServiceComplaint
                strStoredProcedure = SprocNameConstants.GetServiceComplaintsLetter
        End Select

        Me.GetLetter(strStoredProcedure, objLetterBO)
        Me.pnlLetter.Visible = True

        If objLetterBO.LetterContents Is Nothing Or objLetterBO.LetterContents.Equals("") Then
            Me.dvLetter.InnerHtml = "No letter attached"
            Session.Add("svletter", "No letter attached")
        Else
            lettercontents = "<table>" + objLetterBO.LetterContents + "</table>"
            lettercontents = lettercontents.Replace("/Customer/Images/BHALOGOLETTER.gif", "../images/logo_old.jpg")
            Session.Add("svletter", lettercontents)
            Me.dvLetter.InnerHtml = lettercontents
        End If

        Me.mdlPopup.Show()
    End Sub

    Public Sub ShowLetterLink()
        Dim lnkLetter As Control = Me.FormView_Responses.FindControl("lnkViewLetter")
        If Not lnkLetter Is Nothing Then

            Select Case Me.EnquiryNature
                Case ApplicationConstants.CustEnquiryClearArrearsRequest
                    lnkLetter.Visible = True

                Case ApplicationConstants.CustEnquiryASBComplaint
                    lnkLetter.Visible = True

                Case ApplicationConstants.CustEnquiryDirectDebitRentRequest
                    lnkLetter.Visible = True

                Case ApplicationConstants.CustEnquiryServiceComplaint
                    lnkLetter.Visible = True
                Case Else
                    lnkLetter.Visible = False
            End Select

        End If
        
    End Sub

#End Region

#Region "GeLetter"

    Private Sub GetLetter(ByVal storedProcedure As String, ByRef objLetterBO As LetterBO)
        Dim objEnqBL As New EnquiryManager

        Dim resultStatus As Boolean = objEnqBL.GetLetter(storedProcedure, objLetterBO)

    End Sub

#End Region

#Region "AddCustomerResponse"


    Private Sub GetEnquiryResponseCount(ByRef sender As Object)
        Dim btn As Button = CType(sender, Button)
        Dim enqId = btn.CommandArgument

        Dim custResponseBO As New CustomerResponseBO

        custResponseBO.EnquiryId = enqId
        custResponseBO.ResponseDate = Date.Now.Date
        Dim txtNotes As TextBox = CType(Me.FormView_Responses.FindControl("txtResponse"), TextBox)
        custResponseBO.Notes = txtNotes.Text

        Dim objEnqDAL As New EnquiryManager

        Dim resultStatus As Boolean = objEnqDAL.AddCustomerResponse(custResponseBO)


        Dim lblErrMsg As Label = CType(Me.FormView_Responses.FindControl("lblCustResponseMessage"), Label)
        If (resultStatus) Then
            lblErrMsg.Text = ""
        Else
            lblErrMsg.Text = "Response not saved"
        End If

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "UtilityFunctions"

    Public Function GetImageIcon(ByVal imgKey As Integer) As String
        Select Case imgKey
            Case 0
                Return "~/images/buttons/im_sent.gif"
            Case 1
                Return "~/images/buttons/im_read.gif"
            Case 2
                Return "~/images/buttons/im_responded.gif"
        End Select

        Return ""

    End Function
    
#End Region

#Region "ShowResponseFrom"

    Public Function ShowResponseFrom(ByVal lastActionUser As Integer) As String

        If lastActionUser = -1 Then

            Return "&nbsp;&nbsp;I wrote"
        Else
            Return "&nbsp;&nbsp;Broadland Housing wrote"
        End If

    End Function

#End Region

#End Region

#Region "View Details Handler"

    Public Sub ViewDetailsHandle(ByVal sender As Object)
        ' Casting object as a button before further processing
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        ' Getting command arguments from button and storing them in string aray
        Dim strArr As String() = btn.CommandArgument.Split("#")
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()

        If strArr(3).Equals(ApplicationConstants.CustEnquiryTerminationRequest) Then
            ' If passed argument equals termination request thn GetTerminationInfo is called
            ' that will show termination details of this particular enquiry in popup window
            GetTerminationInfo(strArr)
        ElseIf strArr(3).Equals(ApplicationConstants.CustEnquiryServiceComplaint) Then
            ' If passed argument equals ServiceComplaint thn GetComplaintInfo is called
            ' that will show ServiceComplaint details of this particular enquiry in popup window
            GetComplaintInfo(strArr)
        ElseIf strArr(3).Equals(ApplicationConstants.CustEnquiryGarageParkingRequest) Then
            ' If passed argument equals GarageParkingRequest thn GetGarageParkingInfo is called
            ' that will show GarageParkingRequest details of this particular enquiry in popup window
            GetGarageParkingInfo(strArr)
        ElseIf strArr(3).Equals(ApplicationConstants.CustEnquiryASBComplaint) Then
            ' If passed argument equals ASBComplaint thn GetASBInfo is called
            ' that will show ASBComplaint details of this particular enquiry in popup window
            GetASBInfo(strArr)
        ElseIf strArr(3).Equals(ApplicationConstants.CustEnquiryAbandonedReport) Then
            ' If passed argument equals AbandonedReport thn GetAbandonInfo is called
            ' that will show AbandonedReport details of this particular enquiry in popup window
            GetAbandonInfo(strArr)
        ElseIf strArr(3).Equals(ApplicationConstants.CustEnquiryTransferRequest) Then
            ' If passed argument equals TransferRequest thn GetTransferInfo is called
            ' that will show TransferRequest details of this particular enquiry in popup window
            GetTransferInfo(strArr)
        ElseIf strArr(3).Equals(ApplicationConstants.CustEnquiryClearArrearsRequest) Then
            ' If passed argument equals ClearArrearsRequest thn GetClearArrearInfo is called
            ' that will show ClearArrearsRequest details of this particular enquiry in popup window
            GetClearArrearInfo(strArr)
        ElseIf strArr(3).Equals(ApplicationConstants.CustEnquiryDirectDebitRentRequest) Then
            ' If passed argument equals DirectDebitRentRequest thn GetClearDirectDebitRentInfo is called
            ' that will show DirectDebitRentRequest details of this particular enquiry in popup window
            GetClearDirectDebitRentInfo(strArr)
        ElseIf strArr(3).Equals(ApplicationConstants.CustEnquiryCstRequest) Then
            ' If passed argument equals CstRequest thn GetCstRequestInfo is called
            ' that will show CstRequest details of this particular enquiry in popup window
            GetCstRequestInfo(strArr)
        End If

        ' Show the modal popup
        Me.mdlPopup.Show()
 End Sub
#End Region

#Region "GetASBInfo Anti Social Behaviour"
    Private Sub GetASBInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim abBO As AsbBO = New AsbBO()
        abBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        ' Pass custAddBO and abBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetASBInfo(custAddBO, abBO)
       
        Me.txtEnqDescription.Text = "Category: " & abBO.CategoryText & System.Environment.NewLine & "Explain your problem: " & abBO.Description()

    End Sub
#End Region

#Region "Get Termination Info"
    Private Sub GetTerminationInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim termBO As TerminationBO = New TerminationBO()
        ' Get EnquiryLogId and set the EnquiryLogId property of TerminationBO
        termBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        ' Pass custAddBO and termBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetTerminationInfo(custAddBO, termBO)

        Me.txtEnqDescription.Text = "I would like end my tenancy on: " & termBO.MovingOutDate & System.Environment.NewLine & termBO.Description()
    End Sub
#End Region

#Region "Get Complaint Info"
    Private Sub GetComplaintInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim cmpltBO As ComplaintBO = New ComplaintBO()
        cmpltBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        ' Pass custAddBO and cmpltBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetComplaintInfo(custAddBO, cmpltBO)
        
        Me.txtEnqDescription.Text = "I'm complaining about: " & cmpltBO.CategoryText & System.Environment.NewLine & "My complaint is: " & cmpltBO.Description()
    End Sub
#End Region

#Region "GetGarageParkingInfo"
    Private Sub GetGarageParkingInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim gpBO As GarageParkingBO = New GarageParkingBO()
        gpBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        ' Pass custAddBO and gpBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetGarageParkingInfo(custAddBO, gpBO)

        Me.txtEnqDescription.Text = "I'd like a: " & gpBO.LookUpValueText & System.Environment.NewLine & gpBO.Description()
    End Sub
#End Region

#Region "GetAbandonInfo"

    Private Sub GetAbandonInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim abndBO As AbandonBO = New AbandonBO()
        abndBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        ' Pass custAddBO and abndBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetAbandonInfo(custAddBO, abndBO)

        Me.txtEnqDescription.Text = "There is an abandoned: " & abndBO.LookUpValueText & " at " & abndBO.Location & System.Environment.NewLine & abndBO.Description()
    End Sub

#End Region

#Region "GetTransferInfo"

    Private Sub GetTransferInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim houseTransBO As HouseMoveBO = New HouseMoveBO
        houseTransBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        ' Pass custAddBO and houseTransBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetTransferInfo(custAddBO, houseTransBO)
      
        Me.txtEnqDescription.Text = "Reason to move house: " & houseTransBO.Description & System.Environment.NewLine
        Me.txtEnqDescription.Text &= "Local Authority: " & houseTransBO.LocalAuthority & System.Environment.NewLine
        Me.txtEnqDescription.Text &= "Development: " & houseTransBO.Development & System.Environment.NewLine
        Me.txtEnqDescription.Text &= "Number of Bedrooms :" & houseTransBO.NoOfBedrooms & System.Environment.NewLine
        Me.txtEnqDescription.Text &= "Occupants above 18 :" & houseTransBO.OccupantsNoBelow18 & System.Environment.NewLine
        Me.txtEnqDescription.Text &= "Occupants below 18 :" & houseTransBO.OccupantsNoGreater18 & System.Environment.NewLine

    End Sub
#End Region

#Region "GetClearArrearInfo"

    Private Sub GetClearArrearInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim enqBO As EnquiryLogBO = New EnquiryLogBO
        enqBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and enqBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetClearArrearInfo(custAddBO, enqBO)
       
        Me.txtEnqDescription.Text = "Details: " & enqBO.Description
    End Sub

#End Region

#Region "GetCstRequestInfo"

    Private Sub GetCstRequestInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim cstReqBO As CstBO = New CstBO
        cstReqBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        ' Pass custAddBO and cstReqBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetCstRequestInfo(custAddBO, cstReqBO)
       
        Me.txtEnqDescription.Text = "Details: " & cstReqBO.Description & System.Environment.NewLine
        Me.txtEnqDescription.Text &= "Request reason :" & cstReqBO.RequestNature

    End Sub

#End Region

#Region "GetClearDirectDebitRentInfo"

    Private Sub GetClearDirectDebitRentInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim enqBO As EnquiryLogBO = New EnquiryLogBO
        enqBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        ' Pass custAddBO and enqBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetClearDirectDebitRentInfo(custAddBO, enqBO)
       
        Me.txtEnqDescription.Text = "Details: " & enqBO.Description
    End Sub

#End Region

    
   
End Class