﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="report_fault.aspx.vb"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">

    <div class="sectionHead" style="width: 98%;">Report a Fault</div>
    <p>
        For more information about our repairs service <a href="https://www.broadlandgroup.org/existing-customers/repairs/report-a-repair/">click here</a>.
    </p>
</asp:Content>