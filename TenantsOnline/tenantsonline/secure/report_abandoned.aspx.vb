Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class ReportAbandoned
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        ''Get the tenant name and his/her tenancy id from session variables
        Me.lblName.Text &= custHeadBO.Title & " " & custHeadBO.FullName
        Me.lblTenancyRefNo.Text &= custHeadBO.TenancyID

        ''Set the report type drop down vaule.Drop down value is determined from query string value
        If Not Request.QueryString("reportType") Is Nothing Then
            Me.ddlAbdType.SelectedIndex = Request.QueryString("reportType")
        End If

    End Sub


    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.GetLookUpValues()
        End If

        Me.txtDescLocation.Focus()

    End Sub

    Private Sub btnSend_Click(sender As Object, e As System.EventArgs) Handles btnSend.Click
        Me.reportAbandoned()
    End Sub

#End Region

#Region "Methods"

#Region "SendAbandonReport"

    Private Sub reportAbandoned()

        Dim objAbandBO As New AbandonBO()

        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        objAbandBO.CreationDate = Date.Now.Date
        objAbandBO.Description = Me.txtOtherInfo.Text
        ''objAbandBO.RemoveFlag = 0

        ''itemStatusId for "New" in C_STATUS Table
        objAbandBO.ItemStatusId = Integer.Parse(ApplicationConstants.CustomerEnquiryStatusNew)
        objAbandBO.TenancyId = headerBO.TenancyID
        objAbandBO.CustomerId = headerBO.CustomerID


        ''itemNatureId for "Service Complaints" in C_NATURE Table
        objAbandBO.ItemNatureId = 54
        objAbandBO.LookUpValueID = Me.ddlAbdType.SelectedValue
        objAbandBO.Location = Me.txtDescLocation.Text


        Dim objBL As New EnquiryManager()
        objBL.reportAbandoned(objAbandBO)

        If objAbandBO.IsFlagStatus Then

            Response.Redirect("home.aspx")

        Else

            Me.lblErrorMessage.Text = UserInfoMsgConstants.RequestSendingFailed

        End If

    End Sub



#End Region

#Region "GetLookUpValues"

    Private Sub GetLookUpValues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getAbandonedPropertyVehicle
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If Not lstLookUp Is Nothing Then
            Me.ddlAbdType.DataSource = lstLookUp
            Me.ddlAbdType.DataValueField = "LookUpValue"
            Me.ddlAbdType.DataTextField = "LookUpName"
            Me.ddlAbdType.DataBind()
        End If
    End Sub

#End Region

#End Region

#Region "Functions"
    ''No function defined yet...
#End Region

End Class