Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class broadland_news
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then

            ''Get broadland headline news
            Me.GetLatestNews()
            ''Get broadland news except 
            Me.GetNews()

        End If

    End Sub

#Region "Databindings"

    Protected Sub lblHeadingNewsContent_DataBinding(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Me.WrapLableText(sender, 500)

    End Sub

    Protected Sub lblNewsContent_DataBinding(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Me.WrapLableText(sender, 60)

    End Sub

    Protected Sub imgPopupImage_DataBinding(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim img As Image = CType(sender, Image)
        Me.setNewsImagePath(img)

    End Sub

    Protected Sub imgHeadingNewsImage_DataBinding(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim img As Image = CType(sender, Image)
        Me.setNewsImagePath(img)

    End Sub

    Protected Sub imgPopupImage_DataBinding1(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim img As Image = CType(sender, Image)
        Me.setNewsImagePath(img)

    End Sub

#End Region

#End Region
    
#Region "Methods"

#Region "WrapLabelTest"

    ''This method truncate the text of given label to given length
    Private Sub WrapLableText(ByRef sender As Label, ByVal length As Integer)

        Dim lbl As Label = CType(sender, Label)
        ''Convert the HTML text in the provided string to plain text
        Dim str As String = Utility.StripHTML(lbl.Text)
        If str.Length > length Then
            lbl.Text = str.Substring(0, length) & "....."
        End If

    End Sub

#End Region

#Region "GetBroadlandNews"

    Private Sub GetNews()

        ObjectDataSource_News.Select()
        Me.DataList_news.DataBind()

    End Sub

    Private Sub GetLatestNews()

        ObjectDataSource_latestNews.Select()
        Me.FormView_latestNews.DataBind()

    End Sub

#End Region

#Region "SetNewsPath"

    Private Sub setNewsImagePath(ByRef img As Image)
        ''Append the image path to the image file name by reading from configuration file
        img.ImageUrl = System.Configuration.ConfigurationManager.AppSettings("news_image_path").ToString() & img.ImageUrl
    End Sub


#End Region

#End Region

#Region "Functions"
    ''No function defined yet...
#End Region

End Class