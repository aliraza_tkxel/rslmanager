<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="request_garage_parking.aspx.vb" Inherits="tenantsonline.RequestGarageParking"
    Title="Tenants Online :: Request Garage/Car Parking" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">
    
    <div class="sectionHead" style="width: 98%">I would like a garage/car parking space</div>
    <table cellpadding="3" class="table_content">
       
        <tr>
            <td align="left" colspan="3">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <table class="table_box">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" 
                                Height="19px" Text="Name: " Width="536px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" 
                                Height="19px" Text="Tenancy Reference Number: " Width="533px"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 160px" valign="top">
                <asp:Label ID="lblCurrentAddressDetails" runat="server" Text="Current address details:" Width="143px" CssClass="caption"></asp:Label>
            </td>
            <td style="width: 342px">
                <asp:TextBox ID="txtAddress" runat="server" CssClass="input_normal" Height="31px" TextMode="MultiLine" Width="333px" ReadOnly="True"></asp:TextBox>
            </td>
            <td style="width: 7px">
            </td>
        </tr>
        <tr>
            <td valign="top" align="right" style="width: 159px">
                <asp:Label ID="lblType" runat="server" CssClass="caption" Text="I'd like a:" Width="94px"></asp:Label></td>
            <td rowspan="1" style="width: 342px">
                <asp:DropDownList ID="ddlRequestCategory" runat="server" CssClass="select_normal" EnableTheming="True">
                </asp:DropDownList></td>
            <td rowspan="1" style="width: 7px">
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" style="width: 159px">
                <asp:Label ID="lblAdditionalInfo" runat="server" CssClass="caption" Text="Additional information" Width="135px"></asp:Label>
            </td>
            <td rowspan="1" style="width: 342px">
                <asp:TextBox ID="txtDetails" runat="server" CssClass="input_normal" Height="73px"
                    TextMode="MultiLine" Width="333px" MaxLength="4000"></asp:TextBox></td>
            <td rowspan="1" style="width: 30px">
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" style="width: 159px">
            </td>
            <td align="right" rowspan="1" style="width: 342px">
                <asp:LinkButton ID="btnSend" runat="server" Cssclass="btnLink">Send Request</asp:LinkButton>
            </td>
            <td rowspan="1" style="width: 7px">
            </td>
        </tr>
                <tr>
            <td colspan="2">
                <asp:Panel ID="pnlText" runat="server" Height="50px" Width="420px" CssClass="caption">
                   * Please note that not all Broadland Housing Association properties have provision for garages or parking.  Should your property have available spaces, you can apply in writing or via Tenants Online for a space.  It is at Broadland Housing Association&#8217;s discretion as to the allocation.  There may also be a monthly charge payable; this will be confirmed in writing to you if you are successful.
                    <br />
                    Copies of current vehicle ownership details will be required.
                    <br />
                    No vans, commercial vehicles or caravans are permitted on Broadland Housing Associations properties
                    <br />
                    Should you wish to end your garage or car parking agreement, we will require one week's notice in writing *
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="width: 159px">
            </td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;</td>
        </tr>
    </table>

</asp:Content>
