Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities


Partial Public Class ManageArrear
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        '' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Me.DoProcessRequest()
        End If

    End Sub

#End Region

#Region "Private Methods"

#Region "DoProcessRequest Function"

    Private Sub DoProcessRequest()

        Dim custHeadBO As CustomerHeaderBO = Nothing

        ''Obtaining object from Session
        custHeadBO = Me.GetFromSession()

        '' Displaying customer Name  & Tenancy ID
        Me.DisplayHeader(custHeadBO)

        '' Call to dispaly account\rent balance, arrears action status and notes entered by CRM
        Me.ManageFinancialInfo(custHeadBO)


    End Sub

#End Region

#Region "GetFromSession Function"

    Private Function GetFromSession() As CustomerHeaderBO

        '' Obtaining CustomerHeaderBO object from Session 
        Return CType(Session("custHeadBO"), CustomerHeaderBO)

    End Function

#End Region

#Region "DisplayHeader Function"

    Private Sub DisplayHeader(ByRef custHeadBO As CustomerHeaderBO)

        '' Displaying customer Name  & Tenancy ID

        Dim title As String = custHeadBO.Title

        '' Not displaying title if it is "Other"
        If Not title.Equals("Other", StringComparison.OrdinalIgnoreCase) Then

            '' concatenating label text, title, customer full name and setting it to label
            lblName.Text = lblName.Text & " " & title & " " & custHeadBO.FullName


            '' concatenating label text, Tenancy ID
            lblTenancyRefNo.Text = lblTenancyRefNo.Text & " " & custHeadBO.TenancyID


        End If


    End Sub

#End Region

#Region "ManageFinancialInfo Function"

    Private Sub ManageFinancialInfo(ByRef custHeadBO As CustomerHeaderBO)

        Dim custFinBO As CustomerFinancialBO = New CustomerFinancialBO()

        '' Call to Business Layer method to display financial Info 
        Dim custMngr As CustomerManager = New CustomerManager()
        custMngr.ManageFinancialInfo(custFinBO, custHeadBO, False)

        Session("custFinBO") = custFinBO

        Me.DisplayFinancialInfo(custFinBO)

    End Sub

#End Region

#Region "DisplayFinancialInfo Function"

    Private Sub DisplayFinancialInfo(ByRef custFinBO As CustomerFinancialBO)

        Dim visiblePosition As Boolean = Not String.IsNullOrEmpty(custFinBO.ArrearsAction)

        If custFinBO.Balance < 0 Then
            Me.lblRentBalance.Text = String.Format(UserInfoMsgConstants.WelcomeCustomerRentBlancePrefixMsg, Math.Abs(Decimal.Round(custFinBO.Balance, 2)), UserInfoMsgConstants.TextCredit)
        Else
            Me.lblRentBalance.Text = String.Format(UserInfoMsgConstants.WelcomeCustomerRentBlancePrefixMsg, Math.Abs(Decimal.Round(custFinBO.Balance, 2)), UserInfoMsgConstants.TextArrears)
        End If

        '' Displaying current arrears action status
        ' Me.lblActionStatus.Text = custFinBO.ArrearsAction

        ' Me.lblCaptionActionStatus.Visible = visiblePosition
        ' Me.lblActionStatus.Visible = visiblePosition

    End Sub

#End Region


#End Region

End Class