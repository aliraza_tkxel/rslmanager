<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="rent_account.aspx.vb" Inherits="tenantsonline.RentAccount" Title="Tenants Online :: Rent Account" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="page_header_area">
    <div id="head" class="rightheading px-3 py-6 border border-grey bg-white flex">
                 <div class="toggle-button burger" onclick="myFunction(this)">
                                    <div class="bar1"></div>
                                    <div class="bar2"></div>
                                    <div class="bar3"></div>
                                </div>
<h1 class="header uppercase text-grey text-base p-2 w-full">Statement</h1>
    </div>
</asp:Content>
<script runat="server">
    Dim TotalBalance As Decimal = 0.0
    Function GetBalance(ByVal Balance As Decimal) As Decimal
        TotalBalance += Balance
        Return Balance
    End Function
    Function GetTotal() As Decimal
        Return TotalBalance
    End Function


    Function GetPeriod(ByVal startDate As String, ByVal endDate As String) As String
        If startDate.Equals("") Then
            Return ""
        ElseIf endDate.Equals("") Then
            Return ""
        Else
            Return startDate & " to " & endDate
        End If
    End Function

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">

    <script language="javascript" type="text/javascript">
    function openwindow(abc, bc)
    {

        //var w = window.open(abc + document.forms[0].item(5).value, 'display', 'width=900,height=600,left=100,top=50,scrollbars=yes,toolbar=no,location=no');
        var e = document.getElementById('ddlRentPeriod');
        var w = window.open(abc + e.options[e.selectedIndex].value, 'display', 'width=900,height=600,left=100,top=50,scrollbars=yes,toolbar=no,location=no');
        w.focus();
    }
    </script>
<div id="statementPage" class="leading  m-3 mb-8 border border-grey rounded bg-white">
    <div class="flex flex-wrap w-full border-grey"><div class="uppercase text-orange font-bold p-3 w-1/5 text-center border-r border-grey">Statements</div> <div class="p-3 border-b border-grey w-4/5"> <a href="/secure/recharge_account.aspx" class="uppercase text-grey-light font-bold">Recharge Statements</a></div></div>
  <div class="p-3 mt-6">  <table cellpadding="5" class="table_content">

        <tr>
            <td align="left" colspan="3">
              <!--  <table class="table_box">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTenantName" runat="server" CssClass="sub_header_black" Width="610px"
                                Font-Bold="True"  Font-Size="Small" Height="19px">Name: </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 18px">
                            <asp:Label ID="lblTenancyNumber" runat="server" CssClass="sub_header_black" Font-Bold="True"
                                 Font-Size="Small" Text="Tenancy Reference Number: " Width="607px"
                                Height="13px"></asp:Label></td>
                    </tr>
                </table>-->
<div class="flex flex-wrap w-2/5 mb-6">

    
    <div class="w-1/2 px-3 text-right">         
        <asp:Label ID="Label3" runat="server" Text="Rent Account Balance" CssClass="caption"></asp:Label>
    </div>
    <div class="w-1/2">        
        �<asp:Label ID="lblRentAccountBalance" runat="server" Text="Rent Account Balance" CssClass="body_caption_bold"></asp:Label>
    </div>
    
    <div class="w-1/2 px-3 text-right">         
        <asp:Label ID="Label2" runat="server" Text="Estimated Housing Benefit" CssClass="caption"></asp:Label>
    </div>
    <div class="w-1/2">       
        �<asp:Label ID="lblEstimatedHB" runat="server" Text="Estimated Housing Benefit" CssClass="body_caption_bold"></asp:Label>
    </div>
    
    <div class="w-1/2 px-3 text-right">         
        <asp:Label ID="Label4" runat="server" Text="To Pay (After Housing Benefit)" CssClass="caption"></asp:Label>
    </div>
    <div class="w-1/2">          
        �<asp:Label ID="lblToPay" runat="server" Text="To Pay (After Housing Benefit)" CssClass="body_caption_bold"></asp:Label>
    </div>
    
    

</div>
        </td>
        <tr>
            <td  class="border-t border-b w-full py-3 border-grey" valign="top">
                &nbsp;

                 <div class="form-group">
                   <asp:Label ID="lblRentStatement" runat="server" CssClass="caption w-full" Text="Period"
                                    ></asp:Label>&nbsp;
                <asp:DropDownList ID="ddlRentPeriod" runat="server"
                    AutoPostBack="True" ClientIDMode="Static" class="form-control-lg">
                    <asp:ListItem Selected="True" Value="1">Transaction within the last 4 weeks</asp:ListItem>
                    <asp:ListItem Value="2">Previous 4 weeks</asp:ListItem>
                    <asp:ListItem Value="3">Previous 3 months</asp:ListItem>
                    <asp:ListItem Value="6">Previous 6 months</asp:ListItem>
                    <asp:ListItem Value="0">All</asp:ListItem>
                </asp:DropDownList></div></td>
                <td style="height: 10px; width: 126px;">
                   <!-- <asp:LinkButton ID="btnPrint" runat="server" Cssclass="btn bg-orange text-white p-3 rounded uppercase" onclientclick="javascript:openwindow('print/printRentAccount.aspx?selectedValue=')">Print</asp:LinkButton>-->
            </td>
        </tr>
        <tr>
            <td colspan="3" valign="top" align="right">

                &nbsp;<asp:GridView ID="GVRentStatement" CssClass=" w-full" runat="server" AutoGenerateColumns="False" GridLines="None">
                    <Columns>
                        <asp:TemplateField HeaderText="Date" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("F_TRANSACTIONDATE_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" CssClass="p-3" />
                            <HeaderStyle HorizontalAlign="Center" CssClass="p-3"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("ITEMTYPE") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" CssClass="p-3"  />
                            <HeaderStyle HorizontalAlign="Center" CssClass="p-3"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Transaction type" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("PAYMENTTYPE") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" CssClass="p-3"  />
                            <HeaderStyle HorizontalAlign="Center" CssClass="p-3"  />
                        </asp:TemplateField>
                   

                        <asp:TemplateField HeaderText="Debit" InsertVisible="False">
                            <ItemTemplate>
                                &pound;<asp:Label ID="Label1" runat="server" Text='<%# Bind("DEBIT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" CssClass="p-3"  />
                            <HeaderStyle HorizontalAlign="Center" CssClass="p-3"  />
                            <FooterStyle  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Credit" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("CREDIT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" CssClass="p-3"  />
                            <HeaderStyle HorizontalAlign="Center" CssClass="p-3"  />
                            <FooterStyle HorizontalAlign="Center" CssClass="p-3" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Balance">
                            <ItemTemplate>
                                &pound;<asp:Label ID="Label1" runat="server" Text='<%# Bind("BREAKDOWN") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"  CssClass="p-3" />
                            <HeaderStyle HorizontalAlign="Center" CssClass="p-3"  />
                            <FooterStyle Font-Bold="True" HorizontalAlign="Center" CssClass="p-3"  />
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" BackColor="Transparent" ForeColor="Black" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
                        NextPageText="&gt;" PageButtonCount="5" PreviousPageText="&lt;" />
                    <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle  Font-Bold="True" HorizontalAlign="Center" ForeColor="black" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="height: 28px">
                <asp:Label ID="lblNoRecordFound" runat="server" CssClass="body_caption_bold" Visible="False"></asp:Label></td>
            <td align="right" style="width: 3345px; height: 28px;">
            </td>
            <td align="right" style="width: 126px; height: 28px;">
            </td>
        </tr>


         <tr>
            <td colspan="3">
            <div style="margin-bottom: 10px; text-align: right">
                <asp:Button ID="btnPayRent" runat="server" Text="Pay rent"
                    CssClass="btn bg-orange text-white p-3 rounded uppercase" />


            </div>
           <!--     <asp:Panel ID="pnlText" runat="server" Height="50px" Width="420px" CssClass="caption">
                <div style="text-align: right;"><a href="payment_terms_and_conditions.aspx">Online Payment Terms and Conditions</a></div>
                <br />
                 <p style="text-align: center; vertical-align: top;">
                 <img src="../images/BHG_Logo_Image.png" width="145" height="113" alt="BHG Logo" />
            </p>

        <p style="text-align: center; vertical-align: top; text-decoration: underline; font-weight: bold;">
            UNDERSTANDING YOUR RENT STATEMENT</p>
        <p style="margin-bottom: 10px;">
            <b>Tenancy Reference</b> - This is the unique number we use to identify your tenancy
            and needs to be quoted whenever you contact us.
        </p>
        <p style="margin-bottom: 10px;">
            <b>Estimated Housing Benefit due</b> - For those in receipt of Housing Benefit,
            this is the amount estimated to be received up-to the end of the month, in which
            the statement is dated.
            <br />
            Please note this is only an estimate which is based upon the last amount received,
            and assumes your circumstances have not changed. Housing Benefit may vary, therefore
            changing your final balance.
        </p>
        <p style="margin-bottom: 10px;">
            <b>Owed to BHA (in red):</b>This is the amount owed to BHA by you. It takes into
            account any Housing Benefit estimated as still due to the end of the month in which
            the statement is dated.
        </p>
        <p style="margin-bottom: 10px;">
            <b>Owed from BHA (in Black):</b>This means your account is in credit and you may
            be due a refund.
            <br />
            Credits are not always refundable to you; where Housing Benefit is in payment, the
            Local Authority must be contacted to confirm if there are any overpayments owed
            to them.
        </p>
        <p style="font-weight: bold; ">
            Additional Information</p>
        <ul>
            <li style="margin-bottom: 20px;"> Your rent is due on the first day of each month and is payable in advance. </li>
            <li style="margin-bottom: 20px;">To set up a Direct Debit, please call us on 0303 303 0003. It`s quick, simple, and makes paying your rent hassle free. </li>
            <li style="margin-bottom: 20px;">If you are unable to pay via Direct Debit, we have a variety of ways in which you can pay your rent, please contact us for further information if you need to arrange an alternative method.</li>
            <li style="margin-bottom: 20px;"><b>If you have any problems paying your rent or you do not understand any part of this statement, please contact our Customer Services Team on 0303 303 0003 immediately as we can offer a range of help and advice. </b></li>
        </ul>

                </asp:Panel> -->
            </td>
        </tr>



  </table> </div>
</div>
    <script>
        jQuery('#ctl00_LeftMenuControl1_lnkRentManagement').addClass('active');
    </script>
</asp:Content>