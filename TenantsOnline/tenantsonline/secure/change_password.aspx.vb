Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class ChangePassword
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        '' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        ''Set the attribute so that when user presses enter key the specified event for the mentioned button will be fired
        Me.txtConfirmNewPassword.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + Me.btnContinue.UniqueID + "').click();return false;}} else {return true}; ")

        Me.txtOldPassword.Focus()

    End Sub

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Me.ChangePassword()
    End Sub

#End Region

#Region "Methods"

#Region "ChangePassword"

    Private Sub ChangePassword()

        Dim custHBO As CustomerHeaderBO = Nothing
        custHBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        

        ''  User will provide current username(old password)
        Dim custHeadBO As CustomerHeaderBO = New CustomerHeaderBO()

        '' Setting current username i.e. Email
        '' This will be used for verification, if gets verified then user alloed to change his/her password
        custHeadBO.UserName = custHBO.UserName

        '' Setting current password, also used for verification
        custHeadBO.Password = Me.txtOldPassword.Text.Trim

        '' Setting new password, If user authenticated, password changed to this password
        custHeadBO.NewPassword = Me.txtConfirmNewPassword.Text


        '' Call to Business Layer method  
        Dim custMngr As CustomerManager = New CustomerManager()
        custMngr.ChangePassword(custHeadBO)

        Dim flagStatus As Boolean = False

        flagStatus = custHeadBO.IsFlagStatus

        '' If Email changed, customer info stored in session and redirected to Home page
        If (flagStatus) Then

            Me.StoreInSession(custHeadBO)
        Else
            Me.lblErrorMessage.Text = UserInfoMsgConstants.IncorrectEmailMsg
        End If


    End Sub

#End Region

#Region "StoreInSession"

    Private Sub StoreInSession(ByRef custHeadBO As CustomerHeaderBO)

        '' storing custHeadBO object in Session so that it can be used across pages
        Session("custHeadBO") = custHeadBO

        Response.Redirect("~/secure/home.aspx")


    End Sub

#End Region


#End Region

#Region "Functions"

    ''No function defined yet...

#End Region

End Class