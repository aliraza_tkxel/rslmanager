<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="customer_enquiries.aspx.vb" Inherits="tenantsonline.customer_enquiries" Title="Tenants Online :: My Enquiries"  ValidateRequest="false"%>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
Namespace="System.Web.UI" TagPrefix="asp" %>


<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="page_header_area">
    <div id="head" class="rightheading px-3 py-6 border border-grey bg-white flex">
                <div class="toggle-button burger" onclick="myFunction(this)">
                                   <div class="bar1"></div>
                                   <div class="bar2"></div>
                                   <div class="bar3"></div>
                               </div>
 <h1 class="header uppercase text-grey text-base p-2 w-full">Enquiries</h1>
    </div>
</asp:Content>
<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <script type="text/javascript" language="javascript" src="../js/print.js"></script>
    <script language="javascript" type="text/javascript">


        function showResponseDiv() {
            document.getElementById("divComplaintResponse").style.display = "none";
            document.getElementById("divResponse").style.display = "block";
            document.getElementById("divBack").style.display = "block";
            document.getElementById("divResponseList").style.display = "none";
            document.getElementById("divCustomerResponse").style.display = "block";
        }

        function showComplaintResponse() {
            document.getElementById("divResponse").style.display = "none";
            document.getElementById("divBack").style.display = "none";
            document.getElementById("divCustomerResponse").style.display = "none";
            //document.getElementById("ctl00_page_area_lblViewNewsDetails").innerHTML="Enquiry History";
            document.getElementById("divComplaintResponse").style.display = "block";
            document.getElementById("divResponseList").style.display = "block";

        }
    </script>
    <div class="leading p-3 m-3 mb-8 border border-grey rounded bg-white w-full">

        <h2 class="header uppercase text-grey text-base p-2">Make a new Enquiry</h2>
       <div class="px-2"> <p>
            Got a question for us? Just ask.........We're here to help!
        </p>
        <p>
            Let us know below what it is you need answering and we'll get back to you as soon as possible.
        </p>
       </div>
        <div class="w-full mt-3 flex flex-wrap">
            <div class="w-4/5"></div>
            <a href="" class="w-2/5 btn bg-orange text-white p-3 rounded uppercase">Make Enquiry</a></div>
    </div>
    <div id="enquiriesPage" class="leading p-3 m-3 mb-8 border border-grey rounded bg-white w-full">

        <h2 class="header uppercase text-grey text-base p-2">Enquires</h2>
        <table style="width: 100%">

            <tr>
                <td colspan="3">
                    <hr/>
                    <cc2:toolkitscriptmanager id="Registration_ToolkitScriptManager" runat="server"
                                              EnablePartialRendering="true"></cc2:toolkitscriptmanager>

                </td>
            </tr>
        </table>
        <table class="table_box" style="width: 99%">
            <!-- <tr>
                 <td colspan="2" style="height: 20px">
                     <asp:Label ID="lblSentEnquiries" runat="server" CssClass="body_caption_bold"
                                Width="195px"></asp:Label>
                 </td>
                 <td style="width: 200px; height: 20px">
                 </td>
             </tr>-->
            <tr>
                <!--  <td style="width: 55%">
                      <asp:Label ID="lblUnreadResponses" runat="server" CssClass="body_caption_bold" Width="295px"
                                 Height="17px"></asp:Label>
                  </td>
                  <td align="right" style="width: 165px">
                      <asp:Label ID="lblStatus" runat="server" CssClass="caption" Width="38px">Status:</asp:Label>
                  </td>
                  <td align="left" style="width: 200px">
                      <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select_normal" AutoPostBack="True">
                          <asp:ListItem Value="All" Text="All"></asp:ListItem>
                          <asp:ListItem Value="Awaiting Response" Text="Awaiting Response"></asp:ListItem>
                          <asp:ListItem Value="Response Received" Text="Response Received"></asp:ListItem>
                          <asp:ListItem Value="Response Sent" Text="Response Sent"></asp:ListItem>
                          <asp:ListItem Value="Closed"></asp:ListItem>
                      </asp:DropDownList>
                  </td>-->
            </tr>
            <tr>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanelPopup" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkBtnPseudo" runat="server"></asp:LinkButton>
                            <cc2:ModalPopupExtender
                                    ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
                                    CancelControlID="btnClose"
                                    Drag="True" PopupControlID="pnlResponsePopup"
                                    PopupDragHandleControlID="lblViewNewsDetails"
                                    TargetControlID="lnkBtnPseudo">
                            </cc2:ModalPopupExtender>
                            <asp:GridView ID="grdEnquiries" runat="server" AllowPaging="True" CellPadding="4"
                                          AutoGenerateColumns="false"
                                          EmptyDataText="No record found" CssClass="w-full">
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                                <RowStyle BackColor="#EFF3FB" ForeColor="#333333"/>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"/>
                                <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left"/>
                                <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White"/>
                                <AlternatingRowStyle BackColor="White"/>
                                <Columns>
                                    <asp:TemplateField InsertVisible="False" HeaderText="Sent:"
                                                       SortExpression="CreationDate">
                                        <ItemTemplate>
                                            <asp:Label ID="enqSentDate" runat="server" CssClass="cellData"
                                                       Text='<%# Format(Eval("CreationDate"),"dd/MM/yy") %>'/>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>

                                    <asp:TemplateField InsertVisible="False" HeaderText="Reference:">
                                        <ItemTemplate>
                                            <asp:Label ID="enqId" runat="server" CssClass="cellData"
                                                       Text='<%# Utility.GetEnquiryString(Eval("EnquiryLogID").ToString()) %>'/>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>

                                    <asp:TemplateField InsertVisible="False" HeaderText="RE:">
                                        <ItemTemplate>
                                            <asp:Label ID="enqNature" runat="server" CssClass="cellData"
                                                       Text='<%# Eval("NATURE") %>'/>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>

                                    <asp:TemplateField InsertVisible="False" HeaderText="Status:">
                                        <ItemTemplate>
                                            <asp:Label ID="enqResponseStatus" runat="server" CssClass="cellData"
                                                       Text='<%# Eval("ResponseStatus") %>'/>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left"/>
                                    </asp:TemplateField>

                                    <asp:TemplateField InsertVisible="False">
                                        <ItemTemplate>
                                            <asp:Image ID="imgIcon" runat="server" CssClass="cellData"
                                                       ImageUrl='<%# GetImageIcon(Eval("ImageKeyFlag")) %>'/>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField InsertVisible="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="viewDetails" runat="server" CssClass="link_item_small"
                                                            CommandArgument='<%# String.Concat( Eval("EnquiryLogID"),"#",Eval("EnqDescription"),"#",Format(Eval("CreationDate"),"dd/MM/yy"),"#",Eval("NATURE"),"#",Eval("Item"),"#",Eval("ResponseStatus"))  %>'
                                                            OnClick="viewDetails_Click">View Details
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            <asp:Panel ID="pnlResponsePopup" runat="server" BackColor="White" BorderColor="Transparent">
                                <table border="0" style="width: 530px;">
                                    <tbody>
                                    <tr>
                                        <td colspan="2" style="height: 21px; background-color: #c00000">
                                            <asp:Label ID="lblViewNewsDetails" runat="server" BackColor="#C00000"
                                                       Font-Bold="True"
                                                       Font-Names="Arial" Font-Size="Small" ForeColor="White"
                                                       Height="19px" Text="Enquiry Details"
                                                       Width="99%"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="2" rowspan="1" style="background-color: white"
                                            valign="top">
                                            <asp:LinkButton ID="btnClose" runat="server" CssClass="link_item"
                                                            Width="47px" Height="19px">Close
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="2" rowspan="1" style="background-color: white"
                                            valign="top">
                                            <table style="width: 100%">
                                                <tbody>
                                                <tr>
                                                    <td align="right" style="width: 100px">
                                                        <asp:Label ID="lblPODate" runat="server" CssClass="caption"
                                                                   Text="Date:"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 150px">
                                                        <asp:Label ID="lblDateVal" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                    </td>
                                                    <td style="width: 5px">
                                                    </td>
                                                    <td align="right" style="width: 80px">
                                                        <asp:Label ID="lblPOName" runat="server" CssClass="caption"
                                                                   Text="Name:"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 150px">
                                                        <asp:Label ID="lblNameVal" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 100px">
                                                        <asp:Label ID="lblPOEnqryNo" runat="server" CssClass="caption"
                                                                   Text="Enquiry No:"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 150px">
                                                        <asp:Label ID="lblEnquiryNoVal" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                    </td>
                                                    <td style="width: 5px">
                                                    </td>
                                                    <td align="right" style="width: 80px">
                                                        <asp:Label ID="lblPOAddress" runat="server" CssClass="caption"
                                                                   Text="Address:" Width="60px"></asp:Label>
                                                    </td>
                                                    <td align="left" rowspan="1" style="width: 150px" valign="top">
                                                        <asp:Label ID="lblAddressVal" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 100px;">
                                                        <asp:Label ID="lblPOTenancy" runat="server" CssClass="caption"
                                                                   Text="Tenancy No:"></asp:Label>
                                                    </td>
                                                    <td style="width: 150px;" align="left">
                                                        <asp:Label ID="lblTenancyNoVal" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                    </td>
                                                    <td style="width: 5px">
                                                    </td>
                                                    <td align="right" style="width: 80px;">
                                                    </td>
                                                    <td align="left" rowspan="3" style="width: 150px" valign="top">
                                                        <asp:Label ID="lblAddress2" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                        <asp:Label ID="lblAddress3" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                        <asp:Label ID="lblCounty" runat="server" CssClass="caption"
                                                                   Width="150px"></asp:Label>
                                                        <asp:Label ID="lblPostCode" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 100px">
                                                    </td>
                                                    <td style="width: 150px" align="left">
                                                    </td>
                                                    <td style="width: 5px">
                                                    </td>
                                                    <td align="right" style="width: 80px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 100px;">
                                                    </td>
                                                    <td style="width: 150px;" align="left">
                                                    </td>
                                                    <td style="width: 5px">
                                                    </td>
                                                    <td align="right" style="width: 80px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 100px">
                                                        <asp:Label ID="lblPOEnqryItem" runat="server" CssClass="caption"
                                                                   Text="Enquiry Item:"
                                                                   Width="82px"></asp:Label>
                                                    </td>
                                                    <td style="width: 150px" align="left">
                                                        <asp:Label ID="lblEnqryItemVal" runat="server"
                                                                   CssClass="caption" Width="151px"></asp:Label>
                                                    </td>
                                                    <td style="width: 5px">
                                                    </td>
                                                    <td align="right" style="width: 80px">
                                                    </td>
                                                    <td align="left" style="width: 150px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 100px">
                                                        <asp:Label ID="lblPOEnqryNature" runat="server"
                                                                   CssClass="caption" Height="21px"
                                                                   Text="Enquiry Nature:" Width="96px"></asp:Label>
                                                    </td>
                                                    <td style="width: 150px" align="left">
                                                        <asp:Label ID="lblEnqryNatureVal" runat="server"
                                                                   CssClass="caption" Width="154px"></asp:Label>
                                                    </td>
                                                    <td style="width: 5px">
                                                    </td>
                                                    <td align="right" style="width: 80px">
                                                        <asp:Label ID="lblPOTel" runat="server" CssClass="caption"
                                                                   Text="Telephone:" Width="75px"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 150px">
                                                        <asp:Label ID="lblTelVal" runat="server"
                                                                   CssClass="caption"></asp:Label>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="height: 21px; background-color: #c00000">
                                            <asp:Label ID="lblMyEnquiry" runat="server" BackColor="#C00000"
                                                       Font-Bold="True" Font-Names="Arial"
                                                       Font-Size="Small" ForeColor="White" Height="19px"
                                                       Text="My Enquiry" Width="97%"></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="right" colspan="1"
                                            style="width: 100px; height: 21px; background-color: white">
                                            <asp:Label ID="lblCaptionDate" runat="server" CssClass="caption">Date:
                                            </asp:Label>
                                        </td>
                                        <td colspan="1" style="width: 477px; background-color: white; height: 21px;"
                                            align="left">
                                            <asp:Label ID="lblDate" runat="server" CssClass="caption"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="1" style="width: 100px; background-color: white"
                                            valign="top">
                                            <asp:Label ID="lblCaptionReson" runat="server" CssClass="caption">
                                                Description:
                                            </asp:Label>
                                        </td>
                                        <td align="left" colspan="1" style="width: 477px; background-color: white">
                                            <asp:TextBox ID="txtEnqDescription" runat="server"
                                                         CssClass="textarea_simple"
                                                         Height="80px" ReadOnly="True" TextMode="MultiLine"
                                                         Width="373px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 21px"></td>
                                        <td style="height: 21px; width: 477px;">
                                            <asp:Label ID="lblCloseEnquiry" runat="server" BackColor="Transparent"
                                                       Font-Bold="True" Font-Names="Arial"
                                                       Font-Size="Small" ForeColor="Black"></asp:Label>
                                        </td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="1" style="background-color: white; height: 176px;">
                                            <hr/>
                                            <table style="width: 525px">
                                                <tr>
                                                    <td style="width: 113px">
                                                        <div id="divResponseList">
                                                            <asp:Label ID="lblResponseList" runat="server"
                                                                       BackColor="Transparent" Font-Bold="True"
                                                                       Font-Names="Arial"
                                                                       Font-Size="Small" ForeColor="Black" Height="19px"
                                                                       Text="Enquiry History" Width="271px"></asp:Label>
                                                        </div>
                                                        <div id="divCustomerResponse" style="display:none">
                                                            <asp:Label ID="lblCustomerResponse" runat="server"
                                                                       BackColor="Transparent" Font-Bold="True"
                                                                       Font-Names="Arial"
                                                                       Font-Size="Small" ForeColor="Black" Height="19px"
                                                                       Text="My Response" Width="271px"></asp:Label>
                                                        </div>

                                                    </td>
                                                    <td style="width: 100px" align="right">
                                                        <div id="divBack" style="display:none">
                                                            <a href="#" id="lnkBack" onclick="showComplaintResponse()"
                                                               class="link_item">< Back </a>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </table>
                                            <hr/>
                                            <asp:FormView ID="FormView_Responses" runat="server" AllowPaging="True"
                                                          OnPageIndexChanging="FormView_Responses_PageIndexChanging"
                                                          EmptyDataText="No response exists">
                                                <ItemTemplate>
                                                    <div id="divComplaintResponse" style="width: 100%; height: 73%">
                                                        <table style="width: 100%; height: 142px;">
                                                            <tr>
                                                                <td colspan="2" style="height: 21px">
                                                                    <asp:Label ID="lblLastActionDate" runat="server"
                                                                               Text='<%# Format(Eval("LASTACTIONDATE"),"dd/MM/yy") %>'
                                                                               Font-Bold="True"></asp:Label>
                                                                    <asp:Label ID="lblResponseNature" runat="server"
                                                                               Font-Bold="True"
                                                                               Text='<%# ShowResponseFrom(Eval("LASTACTIONUSER"))  %>'
                                                                               Width="171px"></asp:Label>
                                                                </td>
                                                                <td style="width: 40px; height: 21px" align="right">
                                                                    <asp:LinkButton ID="lnkViewLetter" runat="server"
                                                                                    Width="77px"
                                                                                    OnClick="lnkViewLetter_Click"
                                                                                    CommandArgument='<%# Eval("HistoryID") %>'>
                                                                        View Letter
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <div style="overflow:auto;width:525px;height:80px; padding-bottom: 0px;">
                                                                        <asp:Label ID="lblNotes" runat="server"
                                                                                   Text='<%# Eval("NOTES") %>'
                                                                                   Width="100%"
                                                                                   CssClass="caption"></asp:Label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 48px">
                                                                </td>
                                                                <td style="width: 117px" align="right">
                                                                </td>
                                                                <td align="right">
                                                                    <input id="btnReply" size="25" type="button"
                                                                           value="Reply" onclick="showResponseDiv()"/>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </div>

                                                    <div id="divResponse"
                                                         style="display: none; width: 100%; height: 90%">
                                                        <table style="width: 529px; height: 147px">
                                                            <tr>
                                                                <td align="left" style="height: 21px">
                                                                    <asp:Label ID="lblCustResponseMessage"
                                                                               runat="server"
                                                                               CssClass="error_message_label"
                                                                               Font-Bold="True"
                                                                               Width="403px"></asp:Label>
                                                                </td>
                                                                <td align="left" style="height: 21px">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="2"
                                                                    style="padding-bottom: 0px; height: 21px"
                                                                    valign="top">
                                                                    <asp:TextBox ID="txtResponse" runat="server"
                                                                                 Height="81px" TextMode="MultiLine"
                                                                                 Width="516px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left"
                                                                    style="padding-bottom: 0px; height: 21px"
                                                                    valign="top">
                                                                </td>
                                                                <td align="right"
                                                                    style="padding-bottom: 0px; height: 21px"
                                                                    valign="top">
                                                                    <asp:Button ID="btnSendResponse" runat="server"
                                                                                OnClick="btnSendResponse_Click"
                                                                                Text="Send"
                                                                                CommandArgument='<%# Eval("EnquiryId") %>'/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ItemTemplate>


                                                <PagerStyle Font-Names="Arial" Font-Size="Small"/>
                                            </asp:FormView>
                                            <br/>
                                            <asp:Panel ID="pnlLetter" runat="server" Height="203px" Width="450px"
                                                       BackColor="White" Visible="false">
                                                <table style="background-color: white;" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="width: 100px; height: 19px; background-color: #c00000;">
                                                            <asp:Label ID="lblLetterPanelDrag" runat="server"
                                                                       BackColor="#C00000" Font-Bold="True"
                                                                       Font-Names="Arial" Font-Size="Small"
                                                                       ForeColor="White" Height="19px" Text="Letter"
                                                                       Width="491px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px;" align="right">
makwe                                                                            CssClass="link_item"
                                                                            OnClick="btnHideLetterPanel_Click"
                                                                            Width="70px">Close
                                                            </asp:LinkButton>
                                                            <a href="#" onclick="Print()"> Print
                                                                <asp:Label ID="lblDragHide" runat="server"
                                                                           Enabled="False"></asp:Label></td>


                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px;" valign="top">
                                                            <div runat="server" id="dvLetter"
                                                                 style="overflow:scroll;width:500px;height:300px">
                                                                <%--
                                                                <asp:Label ID="lblLetterContents"
                                                                           runat="server"></asp:Label>
                                                                --%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <cc2:DragPanelExtender ID="DragPanelExtenderLetter" runat="server"
                                                                   TargetControlID="pnlLetter"
                                                                   DragHandleID="lblDragHide">
                                            </cc2:DragPanelExtender>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress_enquiries" runat="server" DisplayAfter="10">
                        <ProgressTemplate>
                            <asp:Panel ID="pnlProgress" runat="server" BackColor="Silver" Height="40px" Width="221px"
                                       HorizontalAlign="Center" Font-Names="Arial" Font-Size="Small">
                                <asp:Image ID="imgProgressBar" runat="server"
                                           ImageUrl="~/images/buttons/ajax-loader.gif"/>
                                <br/>
                                Loading ...
                            </asp:Panel>
                            &nbsp;
                            <cc2:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender_progress" runat="server"
                                                              HorizontalSide="Center" TargetControlID="pnlProgress"
                                                              VerticalSide="Middle">
                            </cc2:AlwaysVisibleControlExtender>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
            </tr>
        </table>
        <br/>
        <!--  <table class="table_box" style="width: 60%">
              <tr>
                  <td style="width: 1px">
                      <asp:Label ID="lblKey" runat="server" CssClass="caption">Key:</asp:Label>
                  </td>
                  <td style="width: 100px">
                  </td>
              </tr>
              <tr>
                  <td style="width: 1px">
                      <asp:Image ID="imgSent" runat="server" ImageUrl="~/images/buttons/im_sent.gif"/>
                  </td>
                  <td style="width: 100px">
                      <asp:Label ID="lblSent" runat="server" CssClass="caption" Width="290px">Enquiries I have sent
                      </asp:Label>
                  </td>
              </tr>
              <tr>
                  <td style="width: 1px">
                      <asp:Image ID="imgOpened" runat="server" ImageUrl="~/images/buttons/im_responded.gif"/>
                  </td>
                  <td style="width: 100px">
                      <asp:Label ID="lblOpened" runat="server" CssClass="caption" Width="288px">Unopened responses to my
                          enquiry
                      </asp:Label>
                  </td>
              </tr>
              <tr>
                  <td style="width: 1px">
                      <asp:Image ID="imgUnopened" runat="server" ImageUrl="~/images/buttons/im_read.gif"/>
                  </td>
                  <td style="width: 100px">
                      <asp:Label ID="lblRespond" runat="server" CssClass="caption" Width="289px">Opened responses to my
                          enquiry
                      </asp:Label>
                  </td>
              </tr>
          </table>-->
    </div>
    <script>
        jQuery('#ctl00_LeftMenuControl1_HyperLink1').addClass('active');
    </script>
</asp:Content>

