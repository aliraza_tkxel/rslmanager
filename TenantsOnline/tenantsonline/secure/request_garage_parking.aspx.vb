Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class RequestGarageParking
    Inherits System.Web.UI.Page


#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If


        ''Set the Request category drop down vaule.Drop down value is determined from query string value
        If Not Request.QueryString("requestType") Is Nothing Then

            Me.ddlRequestCategory.SelectedIndex = Request.QueryString("requestType")

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Me.GetLookUpValues()
        End If

        Me.DisplayCustomerInfo()
        Me.txtDetails.Focus()

    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Me.SendGarageParkingRequest()
    End Sub

#End Region

#Region "Methods"

#Region "DisplayCustomerInfo Function"

    '' Used to display customer name, address and tenanacy ref no
    Private Sub DisplayCustomerInfo()

        Dim custHeadBO As CustomerHeaderBO = Nothing

        '' Obtaining object from seesion and casting to CustomerHeaderBO
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Me.lblName.Text &= custHeadBO.Title & " " & custHeadBO.FullName

        Me.lblTenancyRefNo.Text &= custHeadBO.TenancyID

        '' To display customer address
        Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()
        custDetailsBO.Id = custHeadBO.CustomerID
        Dim objBL As CustomerManager = New CustomerManager()
        Dim statusFlag As Boolean = objBL.GetCustomerInfo(custDetailsBO, False)

        If statusFlag Then
            Me.txtAddress.Text = custDetailsBO.Address.CustomerAddress
        End If

    End Sub

#End Region

#Region "SendGarageParkingRequest"

    Private Sub SendGarageParkingRequest()

        Dim parkBO As New GarageParkingBO()

        Dim headerBO As CustomerHeaderBO = Nothing
        ''Get CustomerHeaderBO from session to get CustomerId and TenancyId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        parkBO.CreationDate = Date.Now.Date
        parkBO.Description = Me.txtDetails.Text
        ''parkBO.RemoveFlag = 0

        ''itemStatusId for "New" in C_STATUS Table
        parkBO.ItemStatusId = Integer.Parse(ApplicationConstants.CustomerEnquiryStatusNew)
        parkBO.TenancyId = headerBO.TenancyID
        parkBO.CustomerId = headerBO.CustomerID
        'termBO.JournalId = Nothing

        ''itemNatureId for "Garage Parking Request" in C_NATURE Table
        parkBO.ItemNatureId = 52
        parkBO.LookUpValueID = Me.ddlRequestCategory.SelectedValue

        Dim objBL As New EnquiryManager()
        objBL.SendParkingRequest(parkBO)

        If parkBO.IsFlagStatus Then
            Response.Redirect("~/secure/home.aspx")
        Else
            Me.lblErrorMessage.Text = UserInfoMsgConstants.RequestSendingFailed
        End If


    End Sub



#End Region

#Region "GetLookUpValues"

    Private Sub GetLookUpValues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getGarageParkingLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If Not lstLookUp Is Nothing Then
            Me.ddlRequestCategory.DataSource = lstLookUp
            Me.ddlRequestCategory.DataValueField = "LookUpValue"
            Me.ddlRequestCategory.DataTextField = "LookUpName"
            Me.ddlRequestCategory.DataBind()
        End If
    End Sub

#End Region

#End Region

#Region "Functions"

    ''No function defined yet...

#End Region

End Class