﻿<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="true"
Codebehind="Home.aspx.vb" Inherits="tenantsonline.Home" Title="Tenants Online :: Welcome" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="page_header_area">
    <div id="head" class="rightheading px-3 py-6 border border-grey bg-white flex">
                 <div class="toggle-button burger" onclick="myFunction(this)">
                                    <div class="bar1"></div>
                                    <div class="bar2"></div>
                                    <div class="bar3"></div>
                                </div>
<h1 class="header uppercase text-grey text-base p-2 w-full">Dashboard</h1>
    </div>
</asp:Content>
<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">
    <div class="w-full flex flex-wrap">
        <div id="accountDetails" class="leading p-3 m-3 mb-8 border border-grey rounded bg-white">
            <div class="icon account"></div>
            <h2 class="header uppercase text-grey text-base p-2">Account Overview</h2>
            <asp:LinkButton ID="LinkButton1" runat="server"
                            PostBackUrl="~/secure/view_tenant_details.aspx"
                            CssClass="btn bg-orange text-white p-3 rounded uppercase">Edit Details
            </asp:LinkButton>

            <div class="details flex flex-wrap w-full">
                <div class="w-2/5">
                    <asp:Label ID="lblName" runat="server" CssClass="title1" Text="Name"></asp:Label>
                </div>
                <div class="w-3/5">
                    <asp:Label ID="lblTenantName" runat="server" CssClass="value1"></asp:Label>
                </div>
                <div class="w-2/5">
                    <asp:Label ID="lblEmailCaption" runat="server" CssClass="title2" Text="Contact Email"></asp:Label>
                </div>
                <div class="w-3/5">
                    <asp:Label ID="lblEmail" runat="server" CssClass="value2"></asp:Label>
                </div>
                <div class="w-2/5">
                    <asp:Label ID="lblTelCaption" runat="server" CssClass="title3" Text="Contact Number"></asp:Label>
                </div>
                <div class="w-3/5">
                    <asp:Label ID="lblTelephone" runat="server" CssClass="value3"></asp:Label>
                </div>
                <div class="w-2/5">
                    <asp:Label ID="lblAdd" runat="server" CssClass="title4" Text="Address"></asp:Label>
                </div>
                <div class="w-3/5">
                    <div id="address">
                        <asp:Label ID="lblAddress" runat="server" CssClass="caption"></asp:Label>
                        <asp:Label ID="lblAddress2" runat="server" CssClass="hidden"></asp:Label>
                        ,
                        <asp:Label ID="lblAddress3" runat="server" CssClass="caption"></asp:Label>
                        <asp:Label ID="lblTown" runat="server" CssClass="caption"></asp:Label>
                        ,
                        <asp:Label ID="lblCounty" runat="server" CssClass="hidden"></asp:Label>
                        <asp:Label ID="lblPostCode" runat="server" CssClass="caption"></asp:Label>
                    </div>
                </div>
            </div>

            <asp:Label ID="lblDateOfBirth" runat="server" CssClass="hidden"></asp:Label>
            <asp:Label ID="lblTenancyRef" runat="server" CssClass="hidden"></asp:Label>
            <!--
                    <table cellpadding="3" class="table_content">
                        <tr>
                            <td valign="top">
                                <asp:Image ID="imgVul" runat="server" Visible="false" ImageUrl="~/images/Vulnerability.gif"/>
                            </td>
                            <td colspan="2">
                                <asp:Panel ID="pnlVulnerability" runat="server" Visible="False">
                                    <table cellpadding="3" class="table_content">
                                        <tr>
                                            <td colspan="3">
                                                <asp:Label ID="lblVul" runat="server" CssClass="caption"
                                                           Text="You currently have the following vulnerability information in your records"
                                                ></asp:Label>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:GridView ID="gvCatList" runat="server" AutoGenerateColumns="False"
                                                              Visible="False" CellPadding="4"
                                                              ForeColor="#333333" GridLines="None" CssClass="caption"
                                                >
                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                                                    <Columns>
                                                        <asp:BoundField DataField="CATDESC" HeaderText="Category"/>
                                                        <asp:BoundField DataField="SUBCATDESC" HeaderText="Sub Category"/>
                                                    </Columns>
                                                    <RowStyle BackColor="#E9E9E9" ForeColor="#333333"/>
                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"/>
                                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"/>
                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
                                                    <AlternatingRowStyle BackColor="#E9E9E9"/>
                                                </asp:GridView>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>

                                            <td colspan="3">
                                                <asp:Label ID="lblVulInfo" runat="server" CssClass="caption"
                                                           Text="If any of the information is incorrect then please contact a member of our Customer Services team on 0303 303 0003"
                                                ></asp:Label>
                                            </td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td align="right">
                                                <asp:ImageButton ID="ibtnViewVulnerability" runat="server"
                                                                 ImageUrl="~/images/buttons/view_my_statement.png"
                                                                 PostBackUrl="~/secure/customer_vulnerability.aspx"/>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <hr/>
                                            </td>
                                        </tr>
                                    </table>

                                </asp:Panel>

                            </td>

                        </tr>
                    </table>-->
        </div>
    </div>
    <div class="w-full md:w-1/2">
        <div id="balance" class="area1 p-3 m-3 border border-grey bg-white rounded  flex flex-wrap">
            <h2 class="header uppercase text-grey text-base p-2 w-5/6">Rent Account</h2>

            <asp:LinkButton ID="btnViewAccountDetails" runat="server"
                            PostBackUrl="~/secure/rent_account.aspx" CssClass="btn bg-white arrow  w-1/6"
            >
            </asp:LinkButton>

            <table cellpadding="3" class="table_content">
                <tr>
                    <td align="left" colspan="2">
                    <div>Current Balance</div>
                    <div class="text-4xl">£     <asp:Label ID="lblBalance" runat="server" CssClass="caption">
                    </asp:Label></div>
                      <!--
                         Current Balance
                        - £0.00 in arrears/credit (It would need to change dynamically from ‘in arrears’ to ‘in credit’ when applicable)
                        - *Please note this figure does not include any anticipated housing benefit*
                        - Current monthly rent charge £0.00
                        -->
                      <!--  <asp:Label ID="lblAccountBalance" runat="server" CssClass="body_caption_bold"
                                   Font-Bold="True"></asp:Label>-->
                    </td>
                </tr>

                <tr>
                    <td align="left" colspan="3">
                        <asp:Label ID="lblText" runat="server" CssClass="caption">
                            *Please note this figure does not include any anticipated housing benefit*
                        </asp:Label>
                    </td>


                </tr>

                    <tr >
                                    <td align="left" colspan="3" class="pt-3 mt-3 border-t border-grey-dark">
                                    Current monthly rent charge £0.00
                                    </td>


                                </tr>
            </table>
        </div>
    </div>


        <div class="w-full md:w-1/2">
            <div id="payments" class="area4 p-3 m-3 border border-grey bg-white rounded  flex flex-wrap">
                <h2 class="header uppercase text-grey text-base p-2 w-5/6">Payments</h2>

                <asp:LinkButton ID="btnViewRentAccount" runat="server"
                                PostBackUrl="~/secure/rent_account.aspx" CssClass="btn bg-white arrow  w-1/6"
                >
                </asp:LinkButton>

                <table cellpadding="3" class="table_content w-full">
                   <th>
                      <td colspan="3" class="font-bold text-center pb-2">Amount</td>
                      <td colspan="3" class="font-bold text-center pb-2">Month</td>
                   </th>
                   <tr>
                        <td colspan="3" class="font-bold p-1">£xxx.xx</td>
                        <td colspan="3" class="text-right p-1">10 Match 2018</td>
                   </tr>
   					<tr>
                        <td colspan="3" class="font-bold p-1">£xxx.xx</td>
                        <td colspan="3" class="text-right p-1">10 Match 2018</td>
                   </tr>
                </table>

                <a
                                href="/secure/rent_account.aspx"
                                class="w-full btn bg-orange text-white p-3 rounded uppercase mt-3"
                >Make a payment
                </a>
            </div>
        </div>
    <div class="w-full md:w-1/2">
        <div id="repairs" class="area4 p-3 m-3 border border-grey bg-white rounded  flex flex-wrap">
            <h2 class="header uppercase text-grey text-base p-2 w-5/6">Repairs</h2>

            <asp:LinkButton ID="btnViewFaultDetails" runat="server"
                            PostBackUrl="~/secure/faultlocator/report_a_fault.aspx" CssClass="btn bg-white arrow  w-1/6"
            >
            </asp:LinkButton>

            <table cellpadding="3" class="table_content">
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="AppointmentsGridView" runat="server"
                                      EmptyDataText="There are no current appointments"
                                      GridLines="None">
                            <AlternatingRowStyle BackColor="#EAEAEA"/>
                            <EmptyDataRowStyle/>
                            <HeaderStyle BackColor="#CCCCCC" HorizontalAlign="Left"/>
                        </asp:GridView>
                    </td>
                </tr>
            </table>

            <asp:LinkButton ID="btnViewFaultDetails2" runat="server"
                            PostBackUrl="~/secure/faultlocator/report_a_fault.aspx"
                            CssClass="w-full btn bg-red text-white p-3 rounded uppercase mt-3"
            >Report A Repair
            </asp:LinkButton>
        </div>
    </div>
    <div class="w-full md:w-1/2">
        <div id="enquiries" class="area3 p-3 m-3 border border-grey bg-white rounded  flex flex-wrap">
            <h2 class="header uppercase text-grey text-base p-2 w-5/6">Enquiries</h2>

            <asp:LinkButton ID="btnViewEnquiries" runat="server"
                            PostBackUrl="~/secure/customer_enquiries.aspx" CssClass="btn bg-white arrow  w-1/6"
            >
            </asp:LinkButton>

            <table cellpadding="3" class="table_content">
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="pnlEnquiry" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRequest1" runat="server" CssClass="caption" Text="I have sent"
                                        ></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRequestCount" runat="server" Font-Bold="True"
                                                     ReadOnly="True"
                                        ></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblRequest2" runat="server" CssClass="caption"
                                                   Text="enquiries to Broadland"
                                        ></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblResponse1" runat="server" CssClass="caption" Text="I have"
                                        ></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtResponseCount" runat="server" Font-Bold="True"
                                                     ReadOnly="True"
                                        ></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblResponse2" runat="server" CssClass="caption"
                                                   Text="Responses to my enquiries"
                                        ></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Label ID="lblNoEnquiry" runat="server" CssClass="caption"
                                   Text="No enquiries made via Tenant's Online"
                        ></asp:Label>
                    </td>
                </tr>
            </table>


            <asp:LinkButton ID="btnViewEnquiries2" runat="server"
                            PostBackUrl="~/secure/customer_enquiries.aspx"
                            CssClass="w-full btn bg-orange text-white p-3 rounded uppercase mt-3"
            >Make Enquiry
            </asp:LinkButton>

        </div>
    </div>
</asp:Content>