<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="report_abandoned.aspx.vb" Inherits="tenantsonline.ReportAbandoned" Title="Tenants Online :: Report Abandoned Property/Vehicle" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">

<div class="sectionHead" style="width: 98%">Report vandalism and neglect</div>
    <table cellpadding="3" class="table_content" style="width: 617px">
       
        <tr>
            <td align="left" colspan="3" style="height: 29px">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="height: 29px">
                <table class="table_box">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="22px"
                                Text="Name: " Width="300px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" Height="21px"
                                Text="Tenancy Reference Number: " Width="300px"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 180px">
                <asp:Label ID="lblReportType" runat="server" Text="I'd like to report:" Width="167px"
                    CssClass="caption"></asp:Label></td>
            <td style="width: 336px">
                <asp:DropDownList ID="ddlAbdType" runat="server" CssClass="select_normal" EnableTheming="True">
                </asp:DropDownList></td>
            <td>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right" style="width: 180px">
                <asp:Label ID="lblLocationDesc" runat="server" CssClass="caption" Text="Describe location:"
                    Width="126px"></asp:Label></td>
            <td rowspan="1" style="width: 336px">
                <asp:TextBox ID="txtDescLocation" runat="server" CssClass="input_normal" Height="62px"
                    TextMode="MultiLine" Width="340px" MaxLength="4000"></asp:TextBox></td>
            <td rowspan="1" valign="top">
                <asp:RequiredFieldValidator ID="valDescLocation" runat="server" ErrorMessage="Location description missing"
                    ControlToValidate="txtDescLocation">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="right" style="width: 180px; height: 76px;" valign="top">
                <asp:Label ID="lblOtherInfo" runat="server" CssClass="caption" Text="Other information:"
                    Width="135px"></asp:Label>
                <asp:Label ID="lblOpt" runat="server" CssClass="footnote_item" Text="(optional)"
                    Width="135px"></asp:Label></td>
            <td rowspan="1" style="width: 336px; height: 76px;">
                <asp:TextBox ID="txtOtherInfo" runat="server" CssClass="input_normal" Height="62px"
                    TextMode="MultiLine" Width="340px" MaxLength="4000"></asp:TextBox></td>
            <td rowspan="1" style="height: 76px">
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" style="width: 180px">
            </td>
            <td align="right" rowspan="1" style="width: 336px">
                <asp:LinkButton ID="btnSend" runat="server" Cssclass="btnLink">Send</asp:LinkButton>
            </td>
            <td rowspan="1">
            </td>
        </tr>
         <tr>
            <td colspan="3">
                <asp:Panel ID="pnlText" runat="server" Height="50px" Width="420px" CssClass="caption">
                   
                 </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td style="width: 180px">
            </td>
            <td colspan="2">
                <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager_reportAbandoned" runat="server">
                </ajaxToolkit:ToolkitScriptManager>
                <ajaxToolkit:ValidatorCalloutExtender ID="valRequireDesc" runat="server" TargetControlID="valDescLocation">
                </ajaxToolkit:ValidatorCalloutExtender>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                </td>
        </tr>
    </table>

</asp:Content>
