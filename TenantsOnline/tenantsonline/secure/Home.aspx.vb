
'Option Strict On
Option Infer On


Imports System
Imports System.ComponentModel
Imports System.Data.EntityClient
Imports System.Data.Objects
Imports System.Data.Objects.DataClasses
Imports System.Linq
Imports System.Runtime.Serialization
Imports System.Xml.Serialization
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.DataAccess
Imports AppointmentsModel


Partial Public Class Home
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        '' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        Dim hBo As CustomerHeaderBO = CType(Session("custHeadBO"), CustomerHeaderBO)
        If IsNothing(hBo) Then
            Response.Redirect("~/signin.aspx")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Not Me.IsPostBack Then
            Me.getCustomerInfo()
            Me.CustomerEnquiries()
            Me.GetCustomerAppointments()
        End If

    End Sub

#End Region
    
#Region "Methods"

#Region "GetCustomerInfo"

    ''Get the customer Info to be displayed on welcome page
    Private Sub getCustomerInfo()
        Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()

        '' Obtaining object from Session
        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)


        '' Passing info to custDetailsInfo, that will be used to populate customer's Home page
        custDetailsBO.Id = CType(custHeadBO.CustomerID, Integer)
        custDetailsBO.Tenancy = custHeadBO.TenancyID

        Dim objBL As New CustomerManager()

        Dim resultStatus As Boolean = objBL.GetCustomerInfo(custDetailsBO, False)

        If resultStatus Then
            Me.setPresentationLables(custHeadBO, custDetailsBO)
        End If
        
        Dim custRentBO As CustomerRentBO = New CustomerRentBO()

        Dim rentDS As RentDataset = New RentDataset()
        custRentBO.TenancyID = custHeadBO.TenancyID

        Dim selectedDuration As Int16 = CType(1, Int16)
        Dim startDate As String = String.Empty
        Dim endDate As String = Date.Now().ToShortDateString()

        If selectedDuration > 0 Then
            startDate = DateAdd("m", selectedDuration * -1, Date.Now).ToShortDateString
        Else
            startDate = Nothing
            endDate = Nothing
        End If

        custRentBO.EndDate = nothing
        custRentBO.StartDate = nothing
        Dim custMngr As CustomerManager = New CustomerManager()
        custMngr.RentStatement(rentDS, custRentBO)

        lblBalance.Text = custRentBO.AccountBalance

    End Sub

    Private Sub setPresentationLables(ByRef custHeadBO As CustomerHeaderBO, ByRef custDetailsBO As CustomerDetailsBO)

        'Set Presentation lables from CustomerDetailsBO object
        Me.lblTenantName.Text = custHeadBO.Title & " " & custHeadBO.FullName

        Me.lblAddress.Text = IIf(custDetailsBO.Address.HouseNumber <> "", custDetailsBO.Address.HouseNumber, "")
        Me.lblAddress.Text &= " " & IIf(custDetailsBO.Address.Address1 <> "", custDetailsBO.Address.Address1, "")
        Me.lblAddress2.Text = IIf(custDetailsBO.Address.Address2 <> "", custDetailsBO.Address.Address2, "")
        Me.lblAddress3.Text = IIf(custDetailsBO.Address.Address3 <> "", custDetailsBO.Address.Address3, "")
        Me.lblTown.Text = IIf(custDetailsBO.Address.TownCity <> "", custDetailsBO.Address.TownCity, "")
        Me.lblCounty.Text = IIf(custDetailsBO.Address.County <> "", custDetailsBO.Address.County, "")
        Me.lblPostCode.Text = IIf(custDetailsBO.Address.PostCode <> "", custDetailsBO.Address.PostCode, "")
        Me.lblTenancyRef.Text = custDetailsBO.Tenancy


        Me.lblDateOfBirth.Text = custDetailsBO.DateOfBirth
        Me.lblTelephone.Text = custDetailsBO.Address.Telephone
        Me.lblEmail.Text = custDetailsBO.Address.Email

        If custDetailsBO.AccountBalance < 0 Then
            Me.lblAccountBalance.Text = String.Format(UserInfoMsgConstants.WelcomeCustomerAccountBlancePrefixMsg, Math.Abs(Decimal.Round(custDetailsBO.AccountBalance, 2)), UserInfoMsgConstants.TextCredit)
        Else
            Me.lblAccountBalance.Text = String.Format(UserInfoMsgConstants.WelcomeCustomerAccountBlancePrefixMsg, Math.Abs(Decimal.Round(custDetailsBO.AccountBalance, 2)), UserInfoMsgConstants.TextArrears)
        End If

        'If custDetailsBO.GasServicingAppointmentDate = Nothing Then
        '    'Me.lblGasServiceDate.Text = UserInfoMsgConstants.NoMyAppointmentsExistMsg
        'Else
        '    Me.lblGasServiceDate.Text = UserInfoMsgConstants.WelcomeCustomerGasServiceExistMsg & custDetailsBO.GasServicingAppointmentDate
        'End If

        ' CUSTOMER VULNERABILITY PART
        ' START OF COMMENTS

        Me.gvCatList.DataSource = custDetailsBO.vulnerabilityDataReader
        Me.gvCatList.DataBind()

        'if Customer has Vulnerability, then show panel
        If gvCatList.Rows.Count > 0 Then
            Me.pnlVulnerability.Visible = True
            Me.imgVul.Visible = True
        Else
            Me.pnlVulnerability.Visible = False
        End If
        ' END OF COMMENTS (CUSTOMER VULNERABILITY PART)


    End Sub

    Private Sub GetCustomerAppointments()

        Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()
        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)
        custDetailsBO.Id = CType(custHeadBO.CustomerID, Integer)
        custDetailsBO.Tenancy = custHeadBO.TenancyID

        Dim context = New AppointmentRSLEntities
        Dim query = From a In context.Appointments
                                  Where (a.TenancyId = custDetailsBO.Tenancy)
                                  Order By a.appstart
                                  Select New With {a.Type, a.AppointmentDate, a.AppointmentSlot}


        Dim appointments = query.ToList()
        AppointmentsGridView.DataSource = appointments
        AppointmentsGridView.AutoGenerateColumns = False

        Dim type As New BoundField
        type.HeaderText = "Type"
        type.DataField = "Type"
        AppointmentsGridView.Columns.Add(type)

        Dim AppointmentDate As New BoundField
        AppointmentDate.HeaderText = "AppointmentDate"
        AppointmentDate.DataField = "AppointmentDate"
        AppointmentDate.DataFormatString = "{0:dd/MM/yyyy}"
        AppointmentDate.HtmlEncode = False
        AppointmentsGridView.Columns.Add(AppointmentDate)

        Dim AppointmentSlot As New BoundField
        AppointmentSlot.HeaderText = "AppointmentSlot"
        AppointmentSlot.DataField = "AppointmentSlot"
        AppointmentsGridView.Columns.Add(AppointmentSlot)

        AppointmentsGridView.DataBind()


    End Sub

#End Region

#Region "CustomerEnquiries"

    Private Sub CustomerEnquiries()
        Dim enquiryCount As Integer
        Dim responseCount As Integer
        enquiryCount = Me.GetCustomerEnquiryCount
        responseCount = Me.GetEnquiryResponseCount

        If enquiryCount > 0 Then
            Me.lblNoEnquiry.Visible = False
            Me.pnlEnquiry.Visible = True
            Me.txtRequestCount.Text = enquiryCount
            Me.txtResponseCount.Text = responseCount
        Else
            Me.lblNoEnquiry.Visible = True
            Me.pnlEnquiry.Visible = False
        End If

    End Sub

#Region "GetCustomerEnquiryCount"

    ''Get the number of enquiries customer made
    Private Function GetCustomerEnquiryCount() As Integer

        Dim enqLogBO As New EnquiryLogBO
        '' Obtaining object from Session

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        enqLogBO.CustomerId = CType(custHeadBO.CustomerID, Integer)

        Dim objEnqDAL As New EnquiryManager

        Dim resultStatus As Boolean = objEnqDAL.GetCustomerEnquiryCount(enqLogBO)

        If enqLogBO.CustomerEnquiryCount > 0 Then
            Return enqLogBO.CustomerEnquiryCount
        Else
            Return 0
        End If

    End Function

#End Region

#Region "GetEnquiryResponseCount"

    ''Get the number of responses against enquiries customer made
    Private Function GetEnquiryResponseCount() As Integer

        Dim enqLogBO As New EnquiryLogBO
        '' Obtaining object from Session

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        enqLogBO.CustomerId = CType(custHeadBO.CustomerID, Integer)

        Dim objEnqDAL As New EnquiryManager

        Dim resultStatus As Boolean = objEnqDAL.GetEnquiryResponseCount(enqLogBO)

        If enqLogBO.EnquiryResponseCount > 0 Then
            Return enqLogBO.EnquiryResponseCount
        Else
            Return 0
        End If

    End Function

#End Region

#End Region

#End Region

#Region "Functions"

    ''No function defined yet...

#End Region


End Class