
#Region " Options "

Option Strict On
Option Explicit On
Option Infer On

#End Region

#Region " Imports "

Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.Linq

#End Region

Partial Public Class ViewTenantDetails
    Inherits System.Web.UI.Page

#Region " Event Handlers "

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        '' If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack = False Then
            Me.PopulateLookUp("LookUpListTitle", SprocNameConstants.getTitleLookup)
            Me.PopulateLookUp("LookUpListLocalAuthority", SprocNameConstants.getLocalAuthorityLookup)
            Me.PopulateLookUp(Me.ddlMaritalStatus, SprocNameConstants.getMaritalStatusLookup)
            Me.PopulateLookUp(Me.ddlEthnicity, SprocNameConstants.getEthnicityLookup)
            Me.PopulateLookUp(Me.ddlSexualOrientation, SprocNameConstants.getSexualOrientationLookup)
            Me.PopulateLookUp(Me.ddlReligion, SprocNameConstants.getReligionLookup)
            Me.PopulateLookUp(Me.ddlNationality, SprocNameConstants.getNationalityLookUp)
            Me.PopulateLookUp(Me.ddlConsentText, SprocNameConstants.getYesNo)
            Me.PopulateLookUp(Me.ddlConsentEmail, SprocNameConstants.getYesNo)
            Me.PopulateLookUp(Me.ddlOwnTransport, SprocNameConstants.getYesNo)
            Me.PopulateLookUp(Me.ddlWheelchair, SprocNameConstants.getYesNo)
            Me.PopulateLookUp(Me.ddlCarer, SprocNameConstants.getYesNo)
            Me.PopulateLookUp(Me.chklDisability, SprocNameConstants.GetDisabilityLookup)
            Me.PopulateLookUp(Me.chklSupportAgency, SprocNameConstants.getSupportAgencyLookup)
            Me.PopulateLookUp(Me.chklInternetAccess, SprocNameConstants.getInternetAccessLookup)
            Me.PopulateLookUp(Me.chklBenefit, SprocNameConstants.getBenefitLookup)
            Me.PopulateLookUp(Me.chklBankingFacility, SprocNameConstants.getBankFacilityLookup)
            Me.PopulateLookUp(Me.ddlPreferedContact, SprocNameConstants.getPreferredContactLookup)
            Me.PopulateLookUp(Me.ddlEmploymentStatus, SprocNameConstants.getEmploymentStatusLookup)
            Me.PopulateLookUp(Me.ddlGender, SprocNameConstants.getGenderLookup)
            Me.PopulateLookUp(Me.chklCommunication, SprocNameConstants.getCommunicationLookup)
            Me.PopulateLookUp(Me.ddlHouseholdIncome, SprocNameConstants.getHouseholdIncomeLookup)
            Me.GetCustomerInfo()
        End If

    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        Me.UpdateCustomerDetails()
        Me.GetCustomerInfo()
        Server.Transfer("result.aspx")
    End Sub

#End Region

#Region " Methods "

#Region " Get Customer Info "

    Private Sub GetCustomerInfo()

        Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()

        '' Obtaining object from Session
        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        '' Passing info to custDetailsInfo, that will be used to populate customer's Home page
        custDetailsBO.Id = CType(custHeadBO.CustomerID, Integer)
        custDetailsBO.Tenancy = custHeadBO.TenancyID

        Dim custAddressBO As New CustomerAddressBO
        custDetailsBO.Address = custAddressBO

        Dim objBL As New CustomerManager()

        Dim resultStatus As Boolean = objBL.GetCustomerDetails(custDetailsBO)

        If resultStatus Then
            Me.SetPresentationLabels(custHeadBO, custDetailsBO)
        End If

    End Sub

    Private Sub SetPresentationLabels(ByVal custHeadBO As CustomerHeaderBO, ByVal custDetailsBO As CustomerDetailsBO)

        Dim util As New Utility

        Dim nameParts() As String = New String() {Me.GetTitle(custDetailsBO).Trim,
                                                         util.ConvertToValue(custHeadBO.FirstName).Trim,
                                                         util.ConvertToValue(custHeadBO.MiddleName).Trim,
                                                         util.ConvertToValue(custHeadBO.LastName).Trim}

        nameParts = (From item In nameParts Where Not String.IsNullOrEmpty(item)).ToArray



        Dim addressParts() As String = New String() {String.Join(" ", util.ConvertToValue(custDetailsBO.Address.HouseNumber).Trim, util.ConvertToValue(custDetailsBO.Address.Address1).Trim).Trim,
                                                                                util.ConvertToValue(custDetailsBO.Address.Address2).Trim,
                                                                                util.ConvertToValue(custDetailsBO.Address.Address3).Trim,
                                                                                util.ConvertToValue(custDetailsBO.Address.TownCity).Trim,
                                                                                util.ConvertToValue(custDetailsBO.Address.County).Trim,
                                                                                util.ConvertToValue(custDetailsBO.Address.PostCode).Trim
                                                                                }

        addressParts = (From item In addressParts Where Not String.IsNullOrEmpty(item)).ToArray

        ' Read only
        Me.lblName.Text = String.Join(" ", nameParts)

        Me.SetLocalAuthority(custDetailsBO)
        
        Me.lblAddress.Text = String.Join("<br />", addressParts)

        Me.lblNINumber.Text = util.ConvertToValue(custDetailsBO.NINumber)
        Me.lblDob.Text = custDetailsBO.DateOfBirthDDMMYY

        ' Read/Write
        Me.ddlGender.SelectedIndex() = Me.ddlGender.Items.IndexOf(Me.ddlGender.Items.FindByText(custDetailsBO.Gender))

        Me.ddlEmploymentStatus.SelectedIndex() = Me.ddlEmploymentStatus.Items.IndexOf(Me.ddlEmploymentStatus.Items.FindByValue(custDetailsBO.EmploymentStatusId.GetValueOrDefault.ToString))

        Me.ddlSexualOrientation.SelectedIndex() = Me.ddlSexualOrientation.Items.IndexOf(Me.ddlSexualOrientation.Items.FindByValue(custDetailsBO.SexualOrientationId.GetValueOrDefault.ToString))
        Me.txtSexualOrientationOther.Text = util.ConvertToValue(custDetailsBO.SexualOrientationOther)

        Me.ddlEthnicity.SelectedIndex() = Me.ddlEthnicity.Items.IndexOf(Me.ddlEthnicity.Items.FindByValue(custDetailsBO.EthnicityId.GetValueOrDefault.ToString))
        Me.txtEthnicityOther.Text = util.ConvertToValue(custDetailsBO.EthnicityOther)

        Me.ddlReligion.SelectedIndex() = Me.ddlReligion.Items.IndexOf(Me.ddlReligion.Items.FindByValue(custDetailsBO.ReligionId.GetValueOrDefault.ToString))
        Me.txtReligionOther.Text = util.ConvertToValue(custDetailsBO.ReligionOther)

        Me.ddlNationality.SelectedIndex() = Me.ddlNationality.Items.IndexOf(Me.ddlNationality.Items.FindByValue(custDetailsBO.NationalityId.GetValueOrDefault.ToString))
        Me.txtNationalityOther.Text = util.ConvertToValue(custDetailsBO.NationalityOther)

        Me.ddlMaritalStatus.SelectedIndex() = Me.ddlMaritalStatus.Items.IndexOf(Me.ddlMaritalStatus.Items.FindByValue(custDetailsBO.MaritalStatusId.GetValueOrDefault.ToString))

        Me.ddlPreferedContact.SelectedIndex() = Me.ddlPreferedContact.Items.IndexOf(Me.ddlPreferedContact.Items.FindByValue(custDetailsBO.PreferedContactId.GetValueOrDefault.ToString))
        Me.ddlOwnTransport.SelectedIndex() = Me.ddlOwnTransport.Items.IndexOf(Me.ddlOwnTransport.Items.FindByValue(custDetailsBO.OwnTransport.GetValueOrDefault.ToString))
        Me.ddlConsentEmail.SelectedIndex() = Me.ddlConsentEmail.Items.IndexOf(Me.ddlConsentEmail.Items.FindByValue(custDetailsBO.ConsentToEmail.GetValueOrDefault.ToString))
        Me.ddlConsentText.SelectedIndex() = Me.ddlConsentText.Items.IndexOf(Me.ddlConsentText.Items.FindByValue(custDetailsBO.ConsentToText.GetValueOrDefault.ToString))
        Me.ddlWheelchair.SelectedIndex() = Me.ddlWheelchair.Items.IndexOf(Me.ddlWheelchair.Items.FindByValue(custDetailsBO.Wheelchair.GetValueOrDefault.ToString))
        Me.ddlCarer.SelectedIndex() = Me.ddlCarer.Items.IndexOf(Me.ddlCarer.Items.FindByValue(custDetailsBO.LiveInCarer.GetValueOrDefault.ToString))
        Me.ddlHouseholdIncome.SelectedIndex() = Me.ddlHouseholdIncome.Items.IndexOf(Me.ddlHouseholdIncome.Items.FindByValue(custDetailsBO.HouseIncomeId.GetValueOrDefault.ToString))

        Me.txtMainLanguage.Text = util.ConvertToValue(custDetailsBO.MainLanguage)
        Me.txtTelephone.Text = util.ConvertToValue(custDetailsBO.Address.Telephone)
        Me.txtTelWork.Text = util.ConvertToValue(custDetailsBO.Address.TelephoneWork)
        Me.txtTelMobile.Text = util.ConvertToValue(custDetailsBO.Address.TelephoneMobile)
        Me.txtTelMobileOther.Text = util.ConvertToValue(custDetailsBO.Address.TelephoneMobile2)
        Me.txtTelAlternativeContact.Text = util.ConvertToValue(custDetailsBO.Address.TelephoneAlternativeContact)
        Me.txtAlternativeContact.Text = util.ConvertToValue(custDetailsBO.Address.AlternativeContact)
        Me.txtEmail.Text = util.ConvertToValue(custDetailsBO.Address.Email)
        Me.txtOccupation.Text = util.ConvertToValue(custDetailsBO.Occupation)
        Me.txtTakeHomePay.Text = util.ConvertToValue(custDetailsBO.TakeHomePay)

        Me.txtEmployerName.Text = util.ConvertToValue(custDetailsBO.EmployerName)
        Me.txtEmployerAddress1.Text = util.ConvertToValue(custDetailsBO.EmployerAddress1)
        Me.txtEmployerAddress2.Text = util.ConvertToValue(custDetailsBO.EmployerAddress2)
        Me.txtEmployerTownCity.Text = util.ConvertToValue(custDetailsBO.EmployerTown)
        Me.txtEmployerPostcode.Text = util.ConvertToValue(custDetailsBO.EmployerPostcode)

        Me.lblAdult.Text = custDetailsBO.NoOfOccupantsAbove18.GetValueOrDefault.ToString
        Me.lblChildren.Text = custDetailsBO.NoOfOccupantsBelow18.GetValueOrDefault.ToString

        Me.SetCommunicationChecks(custDetailsBO)
        Me.txtCommunicationOther.Text = util.ConvertToValue(custDetailsBO.CommunicationOther)

        Me.SetDisabilityChecks(custDetailsBO)
        Me.txtDisabilityOther.Text = util.ConvertToValue(custDetailsBO.DisabilityOther)

        Me.SetInternetAccessChecks(custDetailsBO)
        Me.txtInternetAccessOther.Text = util.ConvertToValue(custDetailsBO.InternetAccessOther)

        Me.SetBenefitChecks(custDetailsBO)

        Me.SetSupportAgencyChecks(custDetailsBO)
        Me.txtSupportAgencyOther.Text = util.ConvertToValue(custDetailsBO.SupportAgencyOther)

        Me.SetBankingFacilityChecks(custDetailsBO)
        Me.txtBankingFacilityOther.Text = util.ConvertToValue(custDetailsBO.BankFacilityOther)

    End Sub

    Private Function GetTitle(ByVal customerDetails As CustomerDetailsBO) As String

        Dim title As String = String.Empty

        If String.IsNullOrWhiteSpace(customerDetails.TitleOther) Then
            Dim list As LookUpList = CType(Me.ViewState("LookUpListTitle"), LookUpList)
            title = (From item In list Where item.LookUpValue = customerDetails.TitleId Select item.LookUpName).FirstOrDefault
        Else
            title = customerDetails.TitleOther
        End If

        Return title

    End Function

    Private Sub SetLocalAuthority(ByVal customerDetails As CustomerDetailsBO)

        Dim list As LookUpList = CType(Me.ViewState("LookUpListLocalAuthority"), LookUpList)
        Dim localAuthority = (From item In list Where item.LookUpValue = customerDetails.LocalAuthorityId Select item.LookUpName).FirstOrDefault
        Me.lblLocalAuthority.Text = localAuthority

    End Sub

    Private Sub SetCommunicationChecks(ByVal customerDetails As CustomerDetailsBO)

        Dim comItems As String() = customerDetails.CommunicationIdString.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)

        Me.chklCommunication.Items.Cast(Of ListItem)().ToList().ForEach(Sub(item) item.Selected = comItems.Contains(item.Value))

    End Sub

    Private Sub SetDisabilityChecks(ByVal customerDetails As CustomerDetailsBO)

        Dim comItems As String() = customerDetails.DisabilityIdString.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)

        Me.chklDisability.Items.Cast(Of ListItem)().ToList().ForEach(Sub(item) item.Selected = comItems.Contains(item.Value))

    End Sub

    Private Sub SetInternetAccessChecks(ByVal customerDetails As CustomerDetailsBO)

        Dim comItems As String() = customerDetails.InternetAccessIdString.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)

        Me.chklInternetAccess.Items.Cast(Of ListItem)().ToList().ForEach(Sub(item) item.Selected = comItems.Contains(item.Value))

    End Sub

    Private Sub SetSupportAgencyChecks(ByVal customerDetails As CustomerDetailsBO)

        Dim comItems As String() = customerDetails.SupportAgencyIdString.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)

        Me.chklSupportAgency.Items.Cast(Of ListItem)().ToList().ForEach(Sub(item) item.Selected = comItems.Contains(item.Value))

    End Sub

    Private Sub SetBenefitChecks(ByVal customerDetails As CustomerDetailsBO)

        Dim comItems As String() = customerDetails.BenefitIdString.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)

        Me.chklBenefit.Items.Cast(Of ListItem)().ToList().ForEach(Sub(item) item.Selected = comItems.Contains(item.Value))

    End Sub

    Private Sub SetBankingFacilityChecks(ByVal customerDetails As CustomerDetailsBO)

        Dim comItems As String() = customerDetails.BankFacilityIdString.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)

        Me.chklBankingFacility.Items.Cast(Of ListItem)().ToList().ForEach(Sub(item) item.Selected = comItems.Contains(item.Value))

    End Sub

#End Region

#Region " Update Customer Info "

    Private Sub UpdateCustomerDetails()

        If Page.IsValid Then

            Dim util As New Utility

            ' Instantiate business objects wich will hold the customer data
            Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()

            ' Obtaining object from Session
            Dim custHeadBO As CustomerHeaderBO = Nothing
            custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            Dim custAddressBO As New CustomerAddressBO
            custDetailsBO.Address = custAddressBO

            ' Passing info to custDetailsInfo, that will be used to populate customer's Home page
            custDetailsBO.Id = CType(custHeadBO.CustomerID, Integer)

            If util.ConvertToNullableInt(Me.ddlGender.SelectedValue).HasValue = False Then
                custDetailsBO.Gender = Nothing
            Else
                custDetailsBO.Gender = Me.ddlGender.SelectedItem.Text
            End If

            custDetailsBO.EmploymentStatusId = util.ConvertToNullableInt(Me.ddlEmploymentStatus.SelectedValue)
            custDetailsBO.MaritalStatusId = util.ConvertToNullableInt(Me.ddlMaritalStatus.SelectedValue)

            Dim communication As String = Me.GenerateString(Me.chklCommunication.Items)
            custDetailsBO.CommunicationIdString = util.ConvertToNullableString(communication)
            custDetailsBO.CommunicationOther = util.ConvertToNullableString(Me.txtCommunicationOther.Text.Trim)

            custDetailsBO.Occupation = util.ConvertToNullableString(Me.txtOccupation.Text.Trim)

            If Not txtTakeHomePay.Text.Trim = String.Empty Then
                Dim takeHomePay As Integer = 0
                If Integer.TryParse(Me.txtTakeHomePay.Text.Trim, takeHomePay) Then
                    custDetailsBO.TakeHomePay = takeHomePay
                End If
            End If

            custDetailsBO.EthnicityId = util.ConvertToNullableInt(Me.ddlEthnicity.SelectedValue)
            custDetailsBO.EthnicityOther = util.ConvertToNullableString(Me.txtEthnicityOther.Text.Trim)
            custDetailsBO.NationalityId = util.ConvertToNullableInt(Me.ddlNationality.SelectedValue)
            custDetailsBO.NationalityOther = util.ConvertToNullableString(Me.txtNationalityOther.Text.Trim)
            custDetailsBO.ReligionId = util.ConvertToNullableInt(Me.ddlReligion.SelectedValue)
            custDetailsBO.ReligionOther = util.ConvertToNullableString(Me.txtReligionOther.Text.Trim)
            custDetailsBO.SexualOrientationId = util.ConvertToNullableInt(Me.ddlSexualOrientation.SelectedValue)
            custDetailsBO.SexualOrientationOther = util.ConvertToNullableString(Me.txtSexualOrientationOther.Text.Trim)
            custDetailsBO.MainLanguage = util.ConvertToNullableString(Me.txtMainLanguage.Text.Trim)
            custDetailsBO.PreferedContactId = util.ConvertToNullableInt(Me.ddlPreferedContact.SelectedValue)

            custDetailsBO.EmployerName = util.ConvertToNullableString(Me.txtEmployerName.Text.Trim)
            custDetailsBO.EmployerAddress1 = util.ConvertToNullableString(Me.txtEmployerAddress1.Text.Trim)
            custDetailsBO.EmployerAddress2 = util.ConvertToNullableString(Me.txtEmployerAddress2.Text.Trim)
            custDetailsBO.EmployerTown = util.ConvertToNullableString(Me.txtEmployerTownCity.Text.Trim)
            custDetailsBO.EmployerPostcode = util.ConvertToNullableString(Me.txtEmployerPostcode.Text.Trim)

            custDetailsBO.ConsentToText = util.ConvertToNullableShort(Me.ddlConsentText.SelectedValue)
            custDetailsBO.ConsentToEmail = util.ConvertToNullableShort(Me.ddlConsentEmail.SelectedValue)
            custDetailsBO.OwnTransport = util.ConvertToNullableShort(Me.ddlOwnTransport.SelectedValue)

            Dim disability As String = Me.GenerateString(Me.chklDisability.Items)
            custDetailsBO.DisabilityIdString = util.ConvertToNullableString(disability)
            custDetailsBO.DisabilityOther = util.ConvertToNullableString(Me.txtDisabilityOther.Text.Trim)

            custDetailsBO.Wheelchair = util.ConvertToNullableShort(Me.ddlWheelchair.SelectedValue)
            custDetailsBO.LiveInCarer = util.ConvertToNullableShort(Me.ddlCarer.SelectedValue)

            Dim supportAgency As String = Me.GenerateString(Me.chklSupportAgency.Items)
            custDetailsBO.SupportAgencyIdString = util.ConvertToNullableString(supportAgency)
            custDetailsBO.SupportAgencyOther = util.ConvertToNullableString(Me.txtSupportAgencyOther.Text.Trim)

            Dim internetAccess As String = Me.GenerateString(Me.chklInternetAccess.Items)
            custDetailsBO.InternetAccessIdString = util.ConvertToNullableString(internetAccess)
            custDetailsBO.InternetAccessOther = util.ConvertToNullableString(Me.txtInternetAccessOther.Text.Trim)

            Dim bankFacility As String = Me.GenerateString(Me.chklBankingFacility.Items)
            custDetailsBO.BankFacilityIdString = util.ConvertToNullableString(bankFacility)
            custDetailsBO.BankFacilityOther = util.ConvertToNullableString(Me.txtBankingFacilityOther.Text.Trim)

            Dim benefit As String = Me.GenerateString(Me.chklBenefit.Items)
            custDetailsBO.BenefitIdString = util.ConvertToNullableString(benefit)

            custDetailsBO.HouseIncomeId = util.ConvertToNullableShort(Me.ddlHouseholdIncome.SelectedValue)

            custDetailsBO.Address.Email = util.ConvertToNullableString(Me.txtEmail.Text.Trim)
            custDetailsBO.Address.Telephone = util.ConvertToNullableString(Me.txtTelephone.Text.Trim)
            custDetailsBO.Address.TelephoneMobile = util.ConvertToNullableString(Me.txtTelWork.Text.Trim)
            custDetailsBO.Address.TelephoneMobile2 = util.ConvertToNullableString(Me.txtTelMobileOther.Text.Trim)
            custDetailsBO.Address.TelephoneWork = util.ConvertToNullableString(Me.txtTelWork.Text.Trim)
            custDetailsBO.Address.TelephoneAlternativeContact = util.ConvertToNullableString(Me.txtTelAlternativeContact.Text.Trim)
            custDetailsBO.Address.AlternativeContact = util.ConvertToNullableString(Me.txtAlternativeContact.Text.Trim)

            ' Flag variable to varify the success scenario of the registration usecase
            Dim flagStatus As Boolean = False

            Dim objBL As New CustomerManager()
            flagStatus = objBL.UpdateCustomerDetails(custDetailsBO, custHeadBO)
            Session("custHeadBO") = custHeadBO

        End If

    End Sub

    Private Function GenerateString(ByVal itemCollection As ListItemCollection) As String

        Dim itemString As String = Nothing

        Dim items() As Integer = itemCollection.Cast(Of ListItem)() _
                                   .Where(Function(item) item.Selected = True) _
                                   .Select(Function(item) Integer.Parse(item.Value)) _
                                   .OrderBy(Function(item) item).ToArray()

        If items IsNot Nothing AndAlso items.Length > 0 Then
            itemString = String.Format("{0},", String.Join(",", items.Select(Function(item) item.ToString()).ToArray()))
        End If

        Return itemString

    End Function


#End Region

#Region " Lookup Methods "

    Private Sub PopulateLookUp(ByVal control As ListControl, ByVal procedureName As String)

        Dim objBL As New UtilityManager
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(procedureName, lstLookUp)

      If Not lstLookUp Is Nothing Then
            control.DataSource = lstLookUp
            control.DataValueField = "LookUpValue"
            control.DataTextField = "LookUpName"
            control.DataBind()
        End If

    End Sub

    Private Sub PopulateLookUp(ByVal viewStateName As String, ByVal procedureName As String)

        Dim objBL As New UtilityManager
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(procedureName, lstLookUp)

        Me.ViewState.Add(viewStateName, lstLookUp)

    End Sub

#End Region

#End Region

End Class