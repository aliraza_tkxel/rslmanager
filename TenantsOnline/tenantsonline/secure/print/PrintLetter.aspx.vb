Public Partial Class PrintLetter
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("svletter") Is Nothing Then
            lblMain.Text = CType(Session("svletter"), String).Replace("../images/logo_old.jpg", "../../images/logo_old.jpg")
        End If
    End Sub

End Class