<%@ Page Language="vb" MasterPageFile="~/master pages/PrintMaster.Master"  AutoEventWireup="false" CodeBehind="print_financial_info.aspx.vb" Inherits="tenantsonline.PrintFinancialInfo" Title="Tenants Online :: Financial Information" %>

 
        <asp:Content ContentPlaceHolderID="ContentPlaceHolder_top" runat="server">
        <script runat="server">

    Function GetPeriod(ByVal startDate As String, ByVal endDate As String) As String
        If startDate.Equals("") Then
            Return ""
        ElseIf endDate.Equals("") Then
            Return ""
        Else
            Return startDate & " to " & endDate
        End If
    End Function

</script>


<script language="javascript" type="text/javascript">
    function PrintRentStatement()
    {
        print()

    }
</script>
       
            <span class="style3" style="font-family: Verdana; text-decoration: underline"><b> <font
                color="#133e71" style="font-size: 14px">
                <asp:Label ID="lblHeading" runat="server" Font-Bold="True" Font-Names="Arial" Text="Financial Status" Width="230px"></asp:Label><br />
            </font></b></span>
                <br />
                <asp:Label ID="lblTenantName" runat="server" CssClass="caption" Text="Name:"
                    Width="351px"></asp:Label><br />
    <br />
    <asp:Label ID="lblTenancyNumber" runat="server" CssClass="caption" Font-Bold="False" Text="Tenancy Reference Number: " Width="353px" Font-Underline="False"></asp:Label><br />
                <br />
                    <asp:Label ID="lblAddress" runat="server" Font-Bold="False"
                        Text="Address" Width="357px" CssClass="caption"></asp:Label><br />
                   
        </asp:Content>
      
<asp:Content ContentPlaceHolderID="ContentPlaceHolder_subBody" runat="server">
      <table style="width: 100%">
        <tr>
            <td style="width: 60px;">
                <asp:Label ID="lblCaptionRentBalance" runat="server" CssClass="caption" Text="Current Rent Balance:" Width="140px"></asp:Label>
    </td>
            <td>
    <asp:Label ID="lblBalance" runat="server" Font-Bold="False"
        Width="81px" CssClass="caption"></asp:Label></td>
            <td align="right" style="width: 100px; height: 30px">
                <asp:LinkButton ID="btnPrint" runat="server" OnClientClick="javascript:PrintRentStatement()">Print</asp:LinkButton>
            </td>
        </tr>
    </table>
    &nbsp;
</asp:Content>

 
<asp:Content ContentPlaceHolderID="ContentPlaceHolder3_mainBody" runat="server">

<asp:PlaceHolder ID="pchArrearAction" runat="server">
<table style="width: 100%">
    <tr>
        <td style="width: 173px;">
            <asp:Label ID="lblCaptionArrearAction" runat="server" CssClass="caption" Text="Current Arrears Action:" Width="153px"></asp:Label></td>
        <td>
            <asp:Label ID="lblArrearsAction" runat="server" Font-Bold="False" Width="71px" CssClass="caption"></asp:Label>
        </td>
    </tr>
</table>

    <br />
</asp:PlaceHolder>

</asp:Content>

