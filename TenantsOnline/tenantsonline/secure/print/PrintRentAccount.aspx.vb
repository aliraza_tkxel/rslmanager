
Imports System.Data.SqlClient
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject

Partial Public Class PrintRentAccount
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/secure/signin.aspx")
        End If
        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)
        lblTenantName.Text = lblTenantName.Text & custHeadBO.Title & " " & custHeadBO.FullName
        lblTenancyNumber.Text = lblTenancyNumber.Text & custHeadBO.TenancyID

        Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()
        custDetailsBO.Id = custHeadBO.CustomerID

        Dim objBL As CustomerManager = New CustomerManager()

        Dim statusFlag As Boolean = objBL.GetCustomerInfo(custDetailsBO, False)

        If statusFlag Then
            Me.lblAddress.Text = custDetailsBO.Address.CustomerAddress
        End If



    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim selectedValue As Integer = CType(Request.QueryString("selectedValue"), Integer)

        Dim startDate As String = String.Empty
        Dim endDate As String = Date.Now().ToShortDateString()

        If selectedValue = 1 Then
            startDate = DateAdd("m", -1, Date.Now).ToShortDateString()
        ElseIf selectedValue = 3 Then
            startDate = DateAdd("m", -3, Date.Now).ToShortDateString()
        ElseIf selectedValue = 6 Then
            startDate = DateAdd("m", -6, Date.Now).ToShortDateString()
        Else
            startDate = Nothing
            endDate = Nothing
        End If
        PopulateGridView(startDate, endDate)


    End Sub


    Protected Sub PopulateGridView(ByVal startdate As String, ByVal enddate As String)
        Dim custHeadBO As CustomerHeaderBO = Nothing
        Dim custRentBO As CustomerRentBO = New CustomerRentBO()
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Dim rentDS As RentAccountDS = New RentAccountDS()

        Dim custMngr As CustomerManager = New CustomerManager()

        custRentBO.EndDate = enddate
        custRentBO.StartDate = startdate
        custRentBO.TenancyID = custHeadBO.TenancyID

        custMngr.RentStatement(rentDS, custRentBO)

        Dim hbcbTotal As Decimal = custRentBO.AccountBalance + custRentBO.HousingBalance
        hbcbTotal = Decimal.Round(hbcbTotal, 2)
        lblTotal.Text = "£" & hbcbTotal.ToString()

        GVRentStatement.DataSource = rentDS
        GVRentStatement.DataBind()

        '' Call to display Housing Benefit Balance
        Me.DisplayHousingBalance(custRentBO)

    End Sub
#Region "DisplayHousingBalance Function"

    Private Sub DisplayHousingBalance(ByRef custRentBO As CustomerRentBO)

        '' Displaying Housing Benefit Balance
        Dim hbTotal As Decimal = custRentBO.HousingBalance
        hbTotal = Decimal.Round(hbTotal, 2)

        lblHbBalance.Text = "£" & hbTotal

    End Sub


#End Region

End Class