Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject


Partial Public Class PrintTenancyTermination
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then

            Response.Redirect("~/signin.aspx")

        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            '' Call to display customer name, address and tenancy ref no.
            Me.DisplayTerminationInfo()

        End If

    End Sub

#End Region


#Region "Methods"

#Region "DisplayTerminationInfo Function"

    '' Used to display customer name, address and tenanacy ref no
    Private Sub DisplayTerminationInfo()

        Dim custHeadBO As CustomerHeaderBO = Nothing

        '' Obtaining object from seesion and casting to CustomerHeaderBO
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Me.lblTenantName.Text = lblTenantName.Text & custHeadBO.Title & " " & custHeadBO.FullName

        Me.lblTenancyNumber.Text = lblTenancyNumber.Text & custHeadBO.TenancyID


        Me.lblMoveOutDate.Text = Request.QueryString("MoveOutDate").ToString
        Me.lblTenancyTerminationDetails.Text = Request.QueryString("terminationDetails").ToString

    End Sub

#End Region

#End Region

End Class