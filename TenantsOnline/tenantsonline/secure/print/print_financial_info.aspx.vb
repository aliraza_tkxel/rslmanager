

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject


Partial Public Class PrintFinancialInfo
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            '' Call to display customer name, address and tenancy ref no.
            Me.DisplayCustomerInfo()

            '' Call to display customer account balance, arreas action status and comments by CRM
            Me.DisplayFinancialInfo()

        End If

    End Sub

#End Region

#Region "Methods"

#Region "DisplayCustomerInfo Function"

    '' Used to display customer name, address and tenanacy ref no
    Private Sub DisplayCustomerInfo()

        Dim custHeadBO As CustomerHeaderBO = Nothing

        '' Obtaining object from seesion and casting to CustomerHeaderBO
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        lblTenantName.Text = lblTenantName.Text & custHeadBO.Title & " " & custHeadBO.FullName

        lblTenancyNumber.Text = lblTenancyNumber.Text & custHeadBO.TenancyID

        '' To display address
        Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()
        custDetailsBO.Id = custHeadBO.CustomerID
        Dim objBL As CustomerManager = New CustomerManager()
        Dim statusFlag As Boolean = objBL.GetCustomerInfo(custDetailsBO, False)

        If statusFlag Then
            Me.lblAddress.Text = custDetailsBO.Address.CustomerAddress
        End If

    End Sub

#End Region

#Region "DisplayFinancialInfo Function"

    '' Used to display account/rent balance, arrears action status and comments entered by CRM
    Private Sub DisplayFinancialInfo()

        Dim custFinBO As CustomerFinancialBO = Nothing

        '' Obtaining object from seesion and casting to CustomerFinancialBO
        custFinBO = CType(Session("custFinBO"), CustomerFinancialBO)

        Dim visiblePosition As Boolean = Not String.IsNullOrEmpty(custFinBO.ArrearsAction)

        '' displaying account/rent balance
        lblBalance.Text = Decimal.Round(custFinBO.Balance, 2).ToString

        '' displaying arrears action status
        lblArrearsAction.Text = custFinBO.ArrearsAction

        Me.pchArrearAction.Visible = visiblePosition

    End Sub

#End Region

#End Region

End Class