<%@ Page Language="vb" MasterPageFile="~/master pages/PrintMaster.Master" AutoEventWireup="false"
    Codebehind="PrintRentAccount.aspx.vb" Inherits="tenantsonline.PrintRentAccount" Title="Tenants Online :: Rent Account" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder_top">

    <script runat="server">

        Function GetPeriod(ByVal startDate As String, ByVal endDate As String) As String
            If startDate.Equals("") Then
                Return ""
            ElseIf endDate.Equals("") Then
                Return ""
            Else
                Return startDate & " to " & endDate
            End If
        End Function

    </script>

    <script language="javascript" type="text/javascript">
    function PrintRentStatement()
    {
        print()

    }
    </script>

    <span class="style3" style="font-family: Verdana; text-decoration: underline"><b><font
        color="#133e71" style="font-size: 14px">
        <asp:Label ID="lblHeading" runat="server" Font-Bold="True" Font-Names="Arial" Text="Rent Statement"
            Width="230px"></asp:Label><br />
        <br />
    </font></b></span>
    <asp:Label ID="lblTenantName" runat="server" CssClass="caption" Width="231px" Font-Bold="False">Name: </asp:Label><br />
    <br />
    <asp:Label ID="lblTenancyNumber" runat="server" CssClass="caption" Text="Tenancy Reference Number: "
        Width="230px"></asp:Label><br />
    <br />
    <asp:Label ID="lblAddress" runat="server" Width="228px" CssClass="caption"></asp:Label>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder_subBody" runat="server">
    &nbsp;
    <table style="width: 623px" id="TABLE1">
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 355px">
            </td>
            <td align="right" style="width: 100px">
                <asp:LinkButton ID="btnPrint" runat="server" OnClientClick="javascript:PrintRentStatement()">Print</asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder3_mainBody" runat="server">
    &nbsp; &nbsp;
    <table style="width: 576px">
        <tr>
            <td colspan="3">
                <asp:GridView ID="GVRentStatement" runat="server" AutoGenerateColumns="False" ShowFooter="True"
                    Width="623px" Font-Names="Arial" Font-Size="10pt" Font-Strikeout="False" BorderColor="White"
                    BorderStyle="None" BorderWidth="0px">
                    <Columns>
                        <asp:TemplateField HeaderText="Date" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("F_TRANSACTIONDATE_TEXT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <HeaderStyle HorizontalAlign="Center" Font-Names="Arial" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("ITEMTYPE") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <HeaderStyle HorizontalAlign="Center" Font-Names="Arial" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment type" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("PAYMENTTYPE") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <HeaderStyle HorizontalAlign="Center" Font-Names="Arial" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Period" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%#GetPeriod( Eval("PAYMENTSTARTDATE").ToString() , Eval("PAYMENTENDDATE").ToString() )%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <HeaderStyle HorizontalAlign="Center" Font-Names="Arial" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Debit" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("DEBIT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <HeaderStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <FooterStyle Font-Names="Arial" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Credit" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("CREDIT") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <HeaderStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <FooterStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Balance">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("BREAKDOWN") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <HeaderStyle HorizontalAlign="Center" Font-Names="Arial" />
                            <FooterStyle Font-Bold="True" HorizontalAlign="Center" Font-Names="Arial" />
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="width: 5348px">
            </td>
            <td align="left" style="width: 442px">
                <asp:Label ID="lblCaptionHB" runat="server" CssClass="caption" Text="HB Still Due:"></asp:Label></td>
            <td align="left" style="width: 100px">
                <asp:Label ID="lblHbBalance" runat="server" CssClass="body_caption_bold" Font-Bold="False">?</asp:Label></td>
        </tr>
        <tr>
            <td style="width: 5348px">
            </td>
            <td align="left" style="width: 442px">
                <asp:Label ID="lblBalance" runat="server" CssClass="caption" Text="Balance:"></asp:Label></td>
            <td align="left" style="width: 100px">
                <asp:Label ID="lblTotal" runat="server" Font-Bold="True" Font-Names="Arial"></asp:Label></td>
        </tr>
    </table>
</asp:Content>
