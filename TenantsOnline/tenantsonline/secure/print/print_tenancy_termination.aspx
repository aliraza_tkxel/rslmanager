<%@ Page Language="vb" MasterPageFile="~/master pages/PrintMaster.Master" AutoEventWireup="false"
    Codebehind="print_tenancy_termination.aspx.vb" Inherits="tenantsonline.PrintTenancyTermination" Title="Tenants Online :: Tenancy Termination"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder_top" runat="server">

    <script language="javascript" type="text/javascript">
    function PrintTenancyTermination()
    {
        print();

    }
    </script>

    <span class="style3" style="font-family: Verdana; text-decoration: underline"><b><font
        color="#133e71" style="font-size: 14px">
        <asp:Label ID="lblHeading" runat="server" Font-Bold="True" Font-Names="Arial" Text="Tenancy Termination"
            Width="230px"></asp:Label><br />
    </font></b></span>
    <br />
    <asp:Label ID="lblTenantName" runat="server" CssClass="caption" Text="Name:" Width="351px"></asp:Label><br />
    <br />
    <asp:Label ID="lblTenancyNumber" runat="server" CssClass="caption" Font-Bold="False"
        Text="Tenancy Reference Number: " Width="353px" Font-Underline="False"></asp:Label><br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_subBody" runat="server">
    <table style="width: 100%">
        <tr>
            <td style="width: 60px;">
                &nbsp;<asp:Label ID="lblDate" runat="server" CssClass="caption" Text="I'd like to move out on:"
                    Width="140px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblMoveOutDate" runat="server" Font-Bold="False" Width="81px" CssClass="caption"></asp:Label></td>
            <td align="right" style="width: 100px; height: 30px">
                <asp:LinkButton ID="btnPrint" runat="server"  
                    OnClientClick="javascript:PrintTenancyTermination()">Print</asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3_mainBody" runat="server">
    <asp:Label ID="lblCaptionTerminationDetails" runat="server" CssClass="caption" Text="Tenancy termination details:"
        Width="242px"></asp:Label><br />
    <br />
    <asp:Label ID="lblTenancyTerminationDetails" runat="server" BorderStyle="Solid" BorderWidth="0px"
        Font-Names="Arial" Font-Size="Small" Width="608px" Height="115px"></asp:Label><br />
</asp:Content>
