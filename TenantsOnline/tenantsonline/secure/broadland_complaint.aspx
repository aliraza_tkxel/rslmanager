<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="broadland_complaint.aspx.vb" Inherits="tenantsonline.BroadlandComplaint" title="Tenants Online :: Complaint" %>



<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">
    <div class="sectionHead" style="width: 98%">I wish to make a complaint</div>
    <table cellpadding="3" class="table_content" 
        style="width: 572px; margin-right: 86px;">
        
        <tr>
            <td align="left" colspan="3">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <table class="table_box">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="22px"
                                Text="Name: " Width="543px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" Height="21px"
                                Text="Tenancy Reference Number: " Width="543px"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" >
                <asp:Label ID="lblComplaintType" runat="server" Text="I am complaining about:" Width="150px"
                    CssClass="caption" Visible="false"></asp:Label></td>
            <td style="width: 293px">
                <asp:DropDownList ID="ddlComplaintCategory" runat="server" CssClass="select_normal"
                    EnableTheming="True" Visible="false">
                </asp:DropDownList></td>
            <td style="width: 25px">
            </td>
        </tr>
        <tr>
            <td align="right" valign="top" style="width: 30%;">
                <asp:Label ID="lblComplaintText" runat="server" CssClass="caption" Text="My complaint is:"
                    Width="134px"></asp:Label></td>
            <td rowspan="1" style="width: 293px; height: 87px">
                <asp:TextBox ID="txtComplaintDesc" runat="server" CssClass="input_normal" Height="73px"
                    TextMode="MultiLine" Width="97%" MaxLength="4000"></asp:TextBox></td>
            <td rowspan="1" style="width: 25px" valign="top">
                <asp:RequiredFieldValidator ID="valDesc" runat="server" ControlToValidate="txtComplaintDesc"
                    ErrorMessage="Complaint description missing">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td align="right" valign="top">
            </td>
            <td align="right" rowspan="1" style="width: 293px">
                <asp:LinkButton ID="btnSendRequest" runat="server" Cssclass="btnLink">Send</asp:LinkButton>
            </td>
            <td rowspan="1" style="width: 25px">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlText" runat="server" Height="50px" Width="420px" CssClass="caption">
                    Please see our leaflet <a href="http://broadlandgroup.org/uploads/publications/tenant_leaflets/Customer%20Complaints.pdf" target="_blank">Making a complaint and customer feedback</a> which 
                    explains our complaints procedure.
                    <br />
                    <br />
                    We will acknowledge in writing your complaint within three working days of 
                    receiving it. You will receive a full response within ten working days. Should 
                    we need to investigate your issue further and not be able to provide you with a 
                    response within ten working days, we will advise you as to when to expect a 
                    response.<br />&nbsp;
                    <br />
&nbsp;Please contact our Customer Services team in the first instance as they will endeavour to 
                    assist you with any problems you may have.
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="2">
                <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager_complaint" runat="server">
                </ajaxToolkit:ToolkitScriptManager>
                <ajaxToolkit:ValidatorCalloutExtender ID="valRequireDesc" runat="server" TargetControlID="valDesc">
                </ajaxToolkit:ValidatorCalloutExtender>
                </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:UpdatePanel ID="UpdatePanel_Popup" runat="server">
                    <ContentTemplate>
                &nbsp;
                        <asp:UpdateProgress ID="UpdateProgress_complaints" runat="server" DisplayAfter="5">
                            <ProgressTemplate>
                                <asp:Panel ID="pnlProgress" runat="server" BackColor="Silver" Font-Names="Arial"
                                    Font-Size="Small" Height="40px" HorizontalAlign="Center" Width="221px">
                                    <asp:Image ID="imgProgressBar" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" /><br />
                                    Loading ...
                                </asp:Panel>
                                &nbsp;
                                <cc2:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender_progress" runat="server"
                                    HorizontalSide="Center" TargetControlID="pnlProgress" VerticalSide="Middle">
                                </cc2:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:LinkButton ID="lnkPrevComplaint" runat="server" Width="231px" CssClass="link_item" style="text-decoration: underline">>Show me my previous complaints</asp:LinkButton>
    <asp:Panel ID="pnlPopup" runat="server" BackColor="White" BorderColor="Transparent">
        <table border="0">
            <tbody>
                <tr>
                    <td colspan="1" style="height: 21px; background-color: #c00000">
                        <asp:Label ID="lblDragHandle"  runat="server" BackColor="#C00000" Font-Bold="True"
                            Font-Names="Arial"  ForeColor="White" Height="19px" Text="My previous complaints"
                            Width="97%"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="1" rowspan="1" style="background-color: white" valign="top">
                        &nbsp;<asp:LinkButton ID="btnClose" runat="server" CssClass="link_item" Width="55px">< Back</asp:LinkButton></td>
                </tr><tr>
                    <td colspan="1" style="width: 100px; background-color: white">
                        <asp:FormView ID="FormViewPreviousComplaints" runat="server" AllowPaging="True" EmptyDataText="No complaint exists">
                            <PagerStyle Font-Italic="False" Font-Names="Arial" />
                            <ItemTemplate>
                                <table border="0">
                                    <tbody>
                <tr>
                    <td align="right" colspan="1" rowspan="1" style="background-color: white; height: 25px;" valign="top">
                        <asp:Label ID="lblCrDate" runat="server" CssClass="caption" Text="Creation date:"
                            Width="86px"></asp:Label></td>
                    <td align="left" colspan="1" rowspan="1" style="background-color: white; height: 25px;" valign="top">
                        <asp:Label ID="lblPopupComplaintCreationDate" runat="server" CssClass="caption"
                            Text='<%# Utility.FormatDate(Eval("CreationDate")) %>' Width="385px"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="1" rowspan="1" style="width: 100px; background-color: white; height: 26px;" align="right">
                        <asp:Label ID="lblcat" runat="server" CssClass="caption" Text="Category:" Width="74px"></asp:Label></td>
                    <td colspan="1" rowspan="1" style="width: 100px; background-color: white; height: 26px;">
                        <asp:Label ID="lblPopupComplaintCategory" runat="server" CssClass="caption" Text='<%# Bind("CategoryText") %>'
                            Width="392px"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="1" style="width: 100px; background-color: white" valign="top" align="right">
                        <asp:Label ID="lblExp" runat="server" CssClass="caption" Text="Explanation:" Width="88px"></asp:Label></td>
                    <td colspan="1" style="width: 100px; background-color: white">
                        <div style="overflow: auto; width: 400px; height: 250px">
                            <asp:Label ID="lblPopupComplaintExplaination" runat="server" CssClass="caption" Text='<%# Bind("Description") %>'></asp:Label></div>
                    </td>
                </tr>
            </tbody>
        </table>
                            </ItemTemplate>
                        </asp:FormView>
                    </td>
                </tr>
            </tbody>
        </table>
        <cc2:modalpopupextender id="mdlPopup" runat="server" backgroundcssclass="modalBackground"
            cancelcontrolid="btnClose" drag="True" popupcontrolid="pnlPopup" popupdraghandlecontrolid="lblDragHandle"
            targetcontrolid="lnkPrevComplaint"></cc2:modalpopupextender>
        &nbsp;
    </asp:Panel>
            </td>
        </tr>
    </table>

</asp:Content>
