Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.IO
Imports System.Net
Imports System.Xml.Linq

Partial Public Class RentAccount
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        If IsNothing(Session("custHeadBO")) Then
            Response.Redirect("~/signin.aspx")
        End If

        Dim custHeadBO As CustomerHeaderBO = Nothing
        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        'lblTenantName.Text = lblTenantName.Text & custHeadBO.Title & " " & custHeadBO.FullName
        'lblTenancyNumber.Text = lblTenancyNumber.Text & custHeadBO.TenancyID

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            
            ' ********************************************************************************************
            ' Now on page load, start and end date would be same. It will return 5 recent records
            Dim sDate As String = Date.Now().ToShortDateString()
            Dim eDate As String = Date.Now().ToShortDateString()
             PopulateGridView(sDate, eDate)
            ' *********************************************************************************************

            'PopulateGridView(Nothing, Nothing)
        End If

    End Sub


    Protected Sub GVRentStatement_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVRentStatement.PageIndexChanging

        GVRentStatement.PageIndex = e.NewPageIndex

        Dim selectedDuration As Int16 = CType(ddlRentPeriod.SelectedItem.Value, Int16)
        Dim startDate As String = String.Empty
        Dim endDate As String = Date.Now().ToShortDateString()

        If selectedDuration > 0 Then
            startDate = DateAdd("m", selectedDuration * -1, Date.Now).ToShortDateString
        Else
            startDate = Nothing
            endDate = Nothing
        End If

        PopulateGridView(startDate, endDate)

    End Sub


    Protected Sub PopulateGridView(ByRef startdate As String, ByRef enddate As String)
        
        Dim custHeadBO As CustomerHeaderBO = Nothing
        Dim custRentBO As CustomerRentBO = New CustomerRentBO()

        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Dim rentDS As RentDataset = New RentDataset()

        custRentBO.EndDate = enddate
        custRentBO.StartDate = startdate
        custRentBO.TenancyID = custHeadBO.TenancyID

        Dim custMngr As CustomerManager = New CustomerManager()
        custMngr.RentStatement(rentDS, custRentBO)

        '' if exception from BLL

        If Not custRentBO.IsFlagStatus Then

            Me.lblNoRecordFound.Text = custRentBO.UserMsg

            Me.toggleVisible(False)

        Else
            Me.toggleVisible(True)

            'Dim hbcbTotal As Decimal = custRentBO.AccountBalance
            'hbcbTotal = Decimal.Round(hbcbTotal, 2)
            'If hbcbTotal < 0 Then
            '    lblTotal.Text = "CR " & "£" & (hbcbTotal * -1).ToString()
            '    lblCaptionBalance.Text = "Balance owed from BHA"
            'Else
            '    lblTotal.Text = "£" & hbcbTotal.ToString()
            '    lblCaptionBalance.Text = "Balance owed to BHA"
            'End If
            Dim RentAccountBalance As Decimal = custRentBO.AccountBalance
            Dim EstimatedHB As Decimal = custRentBO.EstimatedHB
            Dim ToPay As Decimal = custRentBO.ToPay

            lblRentAccountBalance.Text = RentAccountBalance
            lblEstimatedHB.Text = EstimatedHB
            lblToPay.Text = ToPay

            GVRentStatement.DataSource = rentDS
            GVRentStatement.DataBind()

            '' Call to display Housing Benefit Balance
            'Me.DisplayHousingBalance(custRentBO)

        End If




    End Sub

#Region "DisplayHousingBalance Function"

    'Private Sub DisplayHousingBalance(ByRef custRentBO As CustomerRentBO)

    '    '' Displaying Housing Benefit Balance
    '    Dim hbTotal As Decimal = custRentBO.HousingBalance
    '    hbTotal = Decimal.Round(hbTotal, 2)

    '    lblHbBalance.Text = "£" & hbTotal

    'End Sub


#End Region

    Protected Sub ddlRentPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRentPeriod.SelectedIndexChanged
        Dim selectedDuration As Int16 = CType(ddlRentPeriod.SelectedItem.Value, Int16)
        Dim startDate As String = String.Empty
        Dim endDate As String = Date.Now().ToShortDateString()

        If selectedDuration = 1 Then
            startDate = Date.Now().ToShortDateString()
        ElseIf selectedDuration = 2 Then
            startDate = DateAdd("ww", -4, Date.Now).ToShortDateString()
        ElseIf selectedDuration = 3 Then
            startDate = DateAdd("m", -3, Date.Now).ToShortDateString()
        ElseIf selectedDuration = 6 Then
            startDate = DateAdd("m", -6, Date.Now).ToShortDateString()
        Else
            startDate = Nothing
            endDate = Nothing
        End If

        PopulateGridView(startDate, endDate)

    End Sub

    Private Sub toggleVisible(ByVal flag As Boolean)

        'Me.lblCaptionBalance.Visible = flag
        'Me.lblCaptionHB.Visible = flag

        'Me.lblHbBalance.Visible = flag
        'Me.lblTotal.Visible = flag

       lblEstimatedHB.Visible = flag
       lblToPay.Visible = flag


        Me.GVRentStatement.Visible = flag
        Me.btnPrint.Visible = flag
        Me.lblNoRecordFound.Visible = Not (flag)

    End Sub

    Protected Sub btnPayRent_Click(sender As Object, e As EventArgs) Handles btnPayRent.Click

        Dim custHeadBO As CustomerHeaderBO = Nothing

        custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Dim url As String = Me.GetUrl(custHeadBO.CustomerID, custHeadBO.TenancyID)
        Dim result As Uri = Nothing

        If Uri.TryCreate(url, UriKind.Absolute, result) Then
            Response.Redirect(url)
        Else
            Throw New Exception(String.Format("Paythru url {0} is not valid.", url))
        End If

    End Sub

    Private Function GetUrl(ByVal customerId As String, ByVal tenancyId As String) As String

        Try
            Dim paythruManager As PaythruManager = New PaythruManager
            Dim requestUrl As String = String.Format(paythruManager.GetPaythruUrl(), tenancyId, customerId)

            Trace.Write(requestUrl)

            Dim webRequest As System.Net.HttpWebRequest = _
              System.Net.HttpWebRequest.Create(requestUrl)

            webRequest.Method = "get"
            webRequest.ContentType = "text/xml; encoding='utf-8'"

            Dim webResponse As System.Net.HttpWebResponse = webRequest.GetResponse
            Dim webResponseStatus As String = (CType(webResponse, HttpWebResponse).StatusDescription)
            Dim stream As IO.Stream = webResponse.GetResponseStream()

            Dim responseContent As String = Nothing

            Using reader As New StreamReader(stream)

                responseContent = reader.ReadToEnd()
                reader.Close()

            End Using

            webResponse.Close()

            Return XDocument.Parse(responseContent).Root.Value
        Catch ex As Exception
            Trace.Write(ex.ToString())
            Throw
        End Try

    End Function

End Class