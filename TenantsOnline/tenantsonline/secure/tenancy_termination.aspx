<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="tenancy_termination.aspx.vb" Inherits="tenantsonline.TenancyTermination" Title="Tenants Online :: Tenancy Termination"  %>

<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">

        <script language="javascript" type="text/javascript">
            function openwindow(param)
            {
                var moveDate=document.getElementById("ctl00_page_area_txtTenancyMoveDate").value;
                var terminationDetails=document.getElementById("ctl00_page_area_txtDetails").value;
              
                if(moveDate.trim().length>0 && terminationDetails.trim().length>0){
               var w = window.open(param + moveDate+"&terminationDetails="+terminationDetails,'display','width=900,height=600,left=100,top=50,scrollbars=yes,toolbar=no,location=no');
               w.focus();
                }           
            }
    </script>
    <div class="sectionHead" style="width: 98%">I'd like to end my tenancy</div>
    <table cellpadding="3" class="table_content">
        
        <tr>
            <td align="left" colspan="3">
                <hr />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" />
            </td>
            
        </tr>
        
        <tr>
            <td align="left" colspan="2">
                <table class="table_box">
                    <tr>
                        <td>
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="16px"
                                Text="Name: " Width="300px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" Height="16px" Text="Tenancy Reference Number: " Width="300px" />
                        </td>
                        
                    </tr>
                </table>
            </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <asp:Label ID="lblCaption1" runat="server" CssClass="caption" Text="Enter details in the box below explaining the reason why" Width="441px"/>
            </td>
            <td colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:Label ID="lblCaption2" runat="server" CssClass="caption" Text=" you wish to end your tenancy:" Width="214px" />
            </td>
            <td colspan="1">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtDetails" runat="server" CssClass="input_normal" Height="77px" TextMode="MultiLine" Width="495px" MaxLength="4000" />
            </td>
            <td colspan="1" valign="top">
                <asp:RequiredFieldValidator ID="valRequireTerminationDetails" runat="server" ControlToValidate="txtDetails"
                    ErrorMessage="Reason details missing">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" valign="middle">
                <ajaxToolkit:CalendarExtender ID="CalendarExtender_MoveDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalander" TargetControlID="txtTenancyMoveDate" />
                
                <ajaxToolkit:ToolkitScriptManager ID="TenancyTerminationToolkitScriptManager" runat="server" />

            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 25%" align="left">
                <asp:Label ID="lblDate" runat="server" CssClass="caption" Text="I'd like to move out on:" Width="140px" />
            </td>
            
            <td align="left" colspan="1">
                <asp:TextBox ID="txtTenancyMoveDate" runat="server" CssClass="input_small" MaxLength="10" />
                <asp:ImageButton ID="btnCalander" runat="server" CausesValidation="False" ImageUrl="~/images/buttons/Calendar_button.png" />
                <asp:RequiredFieldValidator ID="valMovOutDate" runat="server" ControlToValidate="txtTenancyMoveDate" ErrorMessage="The move out date missing" Display=Dynamic>The move out date missing</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="Comparevalidator1" runat="server" ErrorMessage="The date must be a minumum of 28 days in the future"
                        Operator="GreaterThan" ControlToValidate="txtTenancyMoveDate" Type="date" Display="Dynamic" />
                <asp:Label ID="lblDateFormat" runat="server" CssClass="footnote_item" Text="(dd/mm/yyyy)" />
             </td>
            <td align="left" colspan="1">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" colspan="1">
                    
                      <asp:LinkButton ID="btnPrint" runat="server" Cssclass="btnLink" OnClientClick="javascript:openwindow('print/print_tenancy_termination.aspx?MoveOutDate=')">Print</asp:LinkButton>
                      &nbsp;
                         <asp:LinkButton ID="btnSendRequest" Cssclass="btnLink" runat="server">Send Request</asp:LinkButton>
            </td>
            <td align="left" colspan="1">
              
            </td>
        </tr>
        
                 <tr>
            <td colspan="2">
                <asp:Panel ID="pnlText" runat="server" Height="50px" Width="420px" CssClass="caption">
                    If you wish to end your tenancy you are required to provide Broadland Housing 
                    with 28 days&#8217; notice.
                    <br />
                    <br />
                    An inspection to your property will be undertaken while you are still residing 
                    in your property. We will confirm an inspection date in writing to you.
                    <br />
                    <br />
                    Any balances outstanding will need to be settled in full and again, we will 
                    advise you of this in writing. 
</asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td colspan="1">
                <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender_moveDate" runat="server" ClearTextOnInvalid="True"
                    Mask="99/99/9999" MaskType="Date" TargetControlID="txtTenancyMoveDate" UserDateFormat="DayMonthYear"/>
                
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_moveDate" runat="server"
                    TargetControlID="valMovOutDate"/>
                
                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_terminationDetails"
                    runat="server" TargetControlID="valRequireTerminationDetails"/>
                
            </td>
            <td colspan="1">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="1">
            </td>
        </tr>
    </table>
    
   
</asp:Content>
