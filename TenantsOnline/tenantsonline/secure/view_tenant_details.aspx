<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="view_tenant_details.aspx.vb" Inherits="tenantsonline.ViewTenantDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="page_header_area">
    <div id="head" class="rightheading px-3 py-6 border border-grey bg-white flex">
                 <div class="toggle-button burger" onclick="myFunction(this)">
                                    <div class="bar1"></div>
                                    <div class="bar2"></div>
                                    <div class="bar3"></div>
                                </div>
<h1 class="header uppercase text-grey text-base p-2 w-full">ACCOUNT DETAILS</h1>
    </div>
</asp:Content>

<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">
    <div id="accountDetailsPage" class="leading p-3 m-3 mb-8 border border-grey rounded bg-white flex flex-wrap">
        <style>
            table {
                width: 98%;
            }

            td {
                vertical-align: top;
            }

            .equal td {
                width: 25% !important;
            }

            .r1, .r3 {
                width: 20%;
            }

            .r2, .r4 {
                width: 30%
            }

            input[type=text], select {
            }

        </style>
        <h2 class="header uppercase text-grey text-base p-2">Your Details</h2>
        <asp:ValidationSummary ID="vsSummary" runat="server" Width="639px"/>
        <table cellpadding="3" class="table_content" id="TABLE1" border="0">
            <tr>
                <td colspan="4">

                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>

                </td>
            </tr>
            <tr>
                <td style=" height: 30px;" class="r1">
                    <strong class="text-right block px-3">Name
                    </strong>
                </td>
                <td style="padding-left: 2px;  height: 30px;" class="r2">
                    <asp:Label ID="lblName" runat="server" Font-Bold="False"></asp:Label>
                </td>

            </tr>
            <tr>
                <td align="left">

                    <strong class="text-right block px-3">Date of Birth
                    </strong>

                </td>
                <td style="padding-left: 2px; ">
                    <asp:Label ID="lblDob" runat="server" Font-Bold="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" class="r3">
                    <strong class="text-right block px-3">Address
                    </strong>
                </td>
                <td style="padding-left: 2px;" class="r4">
                    <asp:Label ID="lblAddress" runat="server" Font-Bold="False"></asp:Label>
                </td>
              <!--  <td align="left">
                    <strong class="text-right block px-3">Gender
                    </strong>
                </td>
                <td style="padding-left: 2px; ">
                    <asp:DropDownList ID="ddlGender" runat="server" CssClass="input_normal" AppendDataBoundItems="True"
                                      CausesValidation="True">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                    <span class="style6">
                <asp:RequiredFieldValidator ID="rfvGender" runat="server"
                                            ErrorMessage="Please enter your gender" Text="Please enter your gender"
                                            ControlToValidate="ddlGender"
                                            InitialValue="0" ToolTip="Please enter your gender"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                </span>
                </td>-->

            </tr>
            <tr>
                <td align="left" class="hidden">
                    <strong class="text-right block px-3">National Insurance Number
                    </strong>
                </td>
                <td style="padding-left: 2px; " class="hidden">
                    <asp:Label ID="lblNINumber" runat="server" Font-Bold="False"></asp:Label>
                </td>
                <td align="left">

                </td>
                <td style="padding-left: 2px; ">
                </td>
            </tr>

            <tr>
                <td align="left" colspan="4" class="hidden">
                    <strong class="text-right block px-3">Adults (over 18)
                    </strong>
                    <asp:Label ID="lblAdult" runat="server" Font-Bold="False"></asp:Label>
                    &nbsp;<strong class="text-right block px-3">Children (under 18)</strong>
                    <asp:Label ID="lblChildren" runat="server" Font-Bold="False"></asp:Label>
                </td>
            </tr>

            <tr>
                <td align="left">
                    <strong class="text-right block px-3">Tel Home</strong></td>
                <td style="padding-left: 2px; ">

                    <asp:TextBox ID="txtTelephone" runat="server" CssClass="input_small"
                                 MaxLength="20"></asp:TextBox>
                    <span class="style6">
                <asp:RegularExpressionValidator ID="revTelephone" runat="server"
                                                ControlToValidate="txtTelephone"
                                                Text="The home telephone number must begin with '0' and can only contain digits"
                                                ToolTip="The home telephone number must begin with '0' and can only contain digits"
                                                ErrorMessage="The home telephone number must begin with '0' and can only contain digits"
                                                ValidationExpression="^0[\d ]+$"
                                                Display="Dynamic"></asp:RegularExpressionValidator>
                </span></td>
             <!--   <td align="left">
                    <strong class="text-right block px-3">Tel. Work</strong></td>
                <td style="padding-left: 2px; ">

                    <asp:TextBox ID="txtTelWork" runat="server" CssClass="input_small"
                                 MaxLength="20"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revTelWork" runat="server"
                                                    ControlToValidate="txtTelWork"
                                                    Text="The work telephone number must begin with '0' and can only contain digits"
                                                    ToolTip="The work telephone number must begin with '0' and can only contain digits"
                                                    ErrorMessage="The work telephone number must begin with '0' and can only contain digits"
                                                    ValidationExpression="^0[\d ]+$"
                                                    Display="Dynamic"></asp:RegularExpressionValidator>
                </td>-->
            </tr>
            <tr>
                <td align="left">
                    <strong class="text-right block px-3">Tel. Mobile
                    </strong>
                </td>
                <td style="padding-left: 2px; ">
                    <asp:TextBox ID="txtTelMobile" runat="server" CssClass="input_small"
                                 MaxLength="20"></asp:TextBox>
                    <span class="style6">
                 <asp:RegularExpressionValidator ID="revTelMobile" runat="server"
                                                 ControlToValidate="txtTelMobile"
                                                 Text="The mobile telephone number must begin with '0' and can only contain digits"
                                                 ToolTip="The mobile telephone number must begin with '0' and can only contain digits"
                                                 ErrorMessage="The mobile telephone number must begin with '0' and can only contain digits"


                                                 ValidationExpression="^0[\d ]+$"
                                                 Display="Dynamic"></asp:RegularExpressionValidator></span></td>
             <!--   <td align="left">
                    <strong class="text-right block px-3">Tel. Other </strong>
                </td>
                <td style="padding-left: 2px; ">
                    <asp:TextBox ID="txtTelMobileOther" runat="server" CssClass="input_small"
                                 MaxLength="20"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revTelMobileOther" runat="server"
                                                    ControlToValidate="txtTelMobileOther"
                                                    Text="The telephone other number must begin with '0' and can only contain digits"
                                                    ToolTip="The telephone other number must begin with '0' and can only contain digits"
                                                    ErrorMessage="The telephone other number must begin with '0' and can only contain digits"


                                                    ValidationExpression="^0[\d ]+$"
                                                    Display="Dynamic"></asp:RegularExpressionValidator>
                </td>-->
            </tr>
            <!--      <tr>

                      <td align="left">
                          <strong class="text-right block px-3">Local Authority
                          </strong>
                      </td>
                      <td style="padding-left: 2px; ">
                          <asp:Label ID="lblLocalAuthority" runat="server" Font-Bold="False"></asp:Label>
                      </td>
                  </tr>-->
            <tr>
             <!--   <td align="left">
                    <strong class="text-right block px-3">Alt. Contact
                    </strong>
                </td>
                <td style="padding-left: 2px; ">
                    <asp:TextBox ID="txtAlternativeContact" runat="server" CssClass="input_small"
                                 MaxLength="1000"></asp:TextBox>
                </td>
                <td align="left">
                    <strong class="text-right block px-3">Tel. Alt. Contact
                    </strong>
                </td>
                <td style="padding-left: 2px; ">
                    <asp:TextBox ID="txtTelAlternativeContact" runat="server"
                                 CssClass="input_small" MaxLength="20"></asp:TextBox>
                    <span class="style6">
                   <asp:RegularExpressionValidator ID="revAltContact" runat="server"
                                                   ControlToValidate="txtTelAlternativeContact"
                                                   Text="The alternative telephone number must begin with '0' and can only contain digits"
                                                   ToolTip="The alternative telephone number must begin with '0' and can only contain digits"
                                                   ErrorMessage="The alternative telephone number must begin with '0' and can only contain digits"
                                                   ValidationExpression="^0[\d ]+$"
                                                   Display="Dynamic"></asp:RegularExpressionValidator>
                    </span></td>-->
                <td align="right">
                    <strong class="text-right block px-3">Email </strong></td>
                <td style="padding-left: 2px; ">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input_normal"
                                 MaxLength="150"></asp:TextBox>

                    <span class="style6">

                <asp:RegularExpressionValidator ID="rvEmail" runat="server" ControlToValidate="txtEmail"
                                                Display="Dynamic" ErrorMessage="Invalid email address format"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ToolTip="Invalid email address format">Invalid email address format</asp:RegularExpressionValidator>

                <asp:RequiredFieldValidator ID="rfvEmail" runat="server"
                                            ErrorMessage="Please enter your email" Text="Please enter your email"
                                            ControlToValidate="txtEmail"
                                            ToolTip="Please enter your email"
                                            Display="Dynamic"></asp:RequiredFieldValidator>
                   </span></td>
            </tr>
        </table>
        <div class="w-3/4"></div><a href="" class="w-1/4 text-center bg-orange text-white p-3 rounded uppercase mt-8 mb-3">Save</a>
    </div>
   <!-- <div id="aboutYou" class="hidden area1 p-3 m-3 mb-8 border border-grey rounded bg-white">
        <h2 class="header uppercase text-grey text-base p-2">About you</h2>
        <table cellpadding="3" id="TABLE2" border="0">
            <tr>
                <td align="left" style=" height: 33px;" class="r1">
                    <strong class="text-right block px-3">Marital status:
                    </strong>
                </td>
                <td style="padding-left: 2px;  height: 33px;" class="r2">
                    <asp:DropDownList ID="ddlMaritalStatus" runat="server" CssClass="input_normal"
                                      AppendDataBoundItems="True">
                        <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="left" class="r3">
                    <strong class="text-right block px-3">Nationality</strong></td>
                <td style="padding-left: 2px; " class="r4">
                    <div class="select">
                        <asp:DropDownList ID="ddlNationality" runat="server" CssClass="input_normal"
                                          AppendDataBoundItems="True">
                            <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                        </asp:DropDownList>
                        <br/>
                        Other
                        <asp:TextBox ID="txtNationalityOther" runat="server" CssClass="input_small"
                                     MaxLength="100"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left" style=" height: 33px;">
                    <strong class="text-right block px-3">Ethnicity:
                    </strong>
                </td>
                <td style="padding-left: 2px;  height: 33px;">
                    <div class="select">
                        <asp:DropDownList ID="ddlEthnicity" runat="server" CssClass="input_normal"
                                          AppendDataBoundItems="True">
                            <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                        </asp:DropDownList>
                        <br/>
                        Other:
                        <asp:TextBox ID="txtEthnicityOther" runat="server" CssClass="input_small"
                                     MaxLength="300"></asp:TextBox>
                    </div>
                </td>
                <td align="left">
                    <b>Religion:
                    </b>
                </td>
                <td style="padding-left: 2px; ">
                    <div class="select">
                        <asp:DropDownList ID="ddlReligion" runat="server" AppendDataBoundItems="True"
                                          CssClass="input_normal">
                            <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                        </asp:DropDownList>
                        <br/>
                        &nbsp;Other:
                        <asp:TextBox ID="txtReligionOther" runat="server" CssClass="input_small"
                                     MaxLength="50"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left" style=" height: 33px;">
                    <b>Sexual orientation:
                    </b>
                </td>
                <td style="padding-left: 2px;  height: 33px;">
                    <div class="select">
                        <asp:DropDownList ID="ddlSexualOrientation" runat="server" AppendDataBoundItems="True"
                                          CssClass="input_normal">
                            <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                        </asp:DropDownList>
                        <br/>
                        Other:
                        <asp:TextBox ID="txtSexualOrientationOther" runat="server"
                                     CssClass="input_small" MaxLength="10"></asp:TextBox>
                    </div>
                </td>
                <td align="left">
                    <b>Main language:
                    </b>
                </td>
                <td style="padding-left: 2px; ">

                    <asp:TextBox ID="txtMainLanguage" runat="server" CssClass="input_small"
                                 MaxLength="80"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="income" class="hidden area2 p-3 m-3 mb-8 border border-grey rounded bg-white">
        <h2 class="header uppercase text-grey text-base p-2">Income and Employment</h2>

        <table cellpadding="3" class="table_content" id="TABLE3" border="0">
            <tr>
                <td align="left" style=" height: 33px;" class="r1">
                    <strong class="text-right block px-3">Take home pay:
                    </strong>
                </td>
                <td style="padding-left: 2px;  height: 33px;" class="r2">
                    <asp:TextBox ID="txtTakeHomePay" runat="server" CssClass="input_small"></asp:TextBox>
                    <asp:RangeValidator ID="rvTakeHomePay" runat="server"
                                        Text="Take home pay should be a non negative number"
                                        ErrorMessage="Take home pay should be a non negative number"
                                        ToolTip="Take home pay should be a non negative number" MinimumValue="0"
                                        MaximumValue="2147483647" Type="Double" ControlToValidate="txtTakeHomePay"
                                        Display="Dynamic"></asp:RangeValidator>
                </td>
                <td align="left" class="r3">
                    <strong class="text-right block px-3">What is your job title </strong></td>
                <td style="padding-left: 2px; " class="r4">

                    <asp:TextBox ID="txtOccupation" runat="server" CssClass="input_small"
                                 MaxLength="100"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Household Income</b>
                </td>
                <td style="padding-left: 2px; " colspan="1">
                    <asp:DropDownList ID="ddlHouseholdIncome" runat="server" CssClass="input_normal"
                                      AppendDataBoundItems="True">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="left">
                    <b>Employment status
                    </b>
                </td>
                <td style="padding-left: 2px; ">

                    <asp:DropDownList ID="ddlEmploymentStatus" runat="server" CssClass="input_normal"
                                      AppendDataBoundItems="True">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" class="style3">
                    <b>Employer Name
                    </b>
                </td>
                <td style="padding-left: 2px; " colspan="3" class="style3">
                    <asp:TextBox ID="txtEmployerName" runat="server" CssClass="input_small"
                                 MaxLength="70"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Employer Address Line 1:
                    </b>
                </td>
                <td style="padding-left: 2px; " colspan="3">
                    <asp:TextBox ID="txtEmployerAddress1" runat="server" CssClass="input_small"
                                 MaxLength="70"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Employer Address Line 2:
                    </b>
                </td>
                <td style="padding-left: 2px; " colspan="3">
                    <asp:TextBox ID="txtEmployerAddress2" runat="server" CssClass="input_small"
                                 MaxLength="70"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Employer Town/City:
                    </b>
                </td>
                <td style="padding-left: 2px; " colspan="3">
                    <asp:TextBox ID="txtEmployerTownCity" runat="server" CssClass="input_small"
                                 MaxLength="30"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Employer Postcode:
                    </b>
                </td>
                <td style="padding-left: 2px; " colspan="3">
                    <asp:TextBox ID="txtEmployerPostcode" runat="server" CssClass="input_small"
                                 MaxLength="10" Width="200px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div id="comms" class="hidden area3 p-3 m-3 mb-8 border border-grey rounded bg-white">
        <h2 class="header uppercase text-grey text-base p-2">Communication Preferences</h2>

        <table cellpadding="3" class="table_content" id="TABLE4" border="0">


            <tr>
                <td align="left" style=" height: 25px" class="r1">
                    <b>Can we contact you by text:
                    </b>
                </td>
                <td style="padding-left: 2px; " colspan="1" class="r2">
                    <asp:DropDownList ID="ddlConsentText" runat="server" CssClass="input_normal"
                                      AppendDataBoundItems="True">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="r3">
                    <b>Can we contact you by email:
                    </b>
                </td>
                <td class="r4">
                    <asp:DropDownList ID="ddlConsentEmail" runat="server" CssClass="input_normal"
                                      AppendDataBoundItems="True">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" class="style2">
                    <b>Preferred style of communication:
                    </b>
                </td>
                <td style="padding-left: 2px; " colspan="1" class="style2">
                    <asp:DropDownList ID="ddlPreferedContact" runat="server" AppendDataBoundItems="True"
                                      CssClass="input_normal">
                        <asp:ListItem Value="-1">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style2"></td>
                <td class="style2"></td>
            </tr>
            <tr>
                <td align="left" style=" height: 25px;">
                    <b>Access to Internet:
                    </b>
                </td>
                <td style="padding-left: 2px; " colspan="3">
                    <div class="list">
                        <asp:CheckBoxList ID="chklInternetAccess" runat="server" RepeatColumns="3" Font-Bold="False">
                        </asp:CheckBoxList>
                        <br/>
                        Other:
                        <asp:TextBox ID="txtInternetAccessOther" runat="server" CssClass="input_small"
                                     MaxLength="100"></asp:TextBox>
                    </div>
                </td>
            </tr>

            <tr>

                <td style="padding-left: 2px; " colspan="4">
                    <div class="sectionSub">Do you have additional Communication Preferences?</div>
                    <br/>
                    <div class="list">
                        <asp:CheckBoxList ID="chklCommunication" runat="server" RepeatColumns="3"
                                          Font-Bold="False" CssClass="equal">
                        </asp:CheckBoxList>
                        <br/>
                        Other:
                        <asp:TextBox ID="txtCommunicationOther" runat="server" CssClass="input_small"
                                     MaxLength="100"></asp:TextBox>
                        <br/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div id="support" class="hidden area4 p-3 m-3 mb-8 border border-grey rounded bg-white">
        <h2 class="header uppercase text-grey text-base p-2">Support</h2>

        <table cellpadding="3" class="table_content" id="TABLE5" border="0">
            <tr>
                <td align="left" style=" height: 25px;" colspan="2">
                    <div class="sectionSub">Do you have any Disabilities</div>
                    <br/>
                    <div class="list">
                        <asp:CheckBoxList ID="chklDisability" runat="server" RepeatColumns="3">
                        </asp:CheckBoxList>
                        <br/>
                        Other:
                        <asp:TextBox ID="txtDisabilityOther" runat="server" CssClass="input_small"
                                     MaxLength="100"></asp:TextBox>
                    </div>
                </td>

            </tr>
            <tr>
                <td align="left" colspan="2" class="style2">
                    <div class="sectionSub">Do you receive support from any Agencies</div>
                    <br/>
                    <div class="list">
                        <asp:CheckBoxList ID="chklSupportAgency" runat="server" RepeatColumns="3" Font-Bold="False">
                        </asp:CheckBoxList>
                        <br/>
                        Other:
                        <asp:TextBox ID="txtSupportAgencyOther" runat="server" CssClass="input_small"
                                     MaxLength="100"></asp:TextBox>
                    </div>
                </td>
            </tr>

            <tr>
                <td align="left" style=" height: 25px;" colspan="2">
                    <div class="sectionSub">Do you receive any Benefits</div>
                    <br/>

                    <asp:CheckBoxList ID="chklBenefit" runat="server" RepeatColumns="2" Font-Bold="False">
                    </asp:CheckBoxList>
                </td>

            </tr>

            <tr>

                <td style="padding-left: 2px; " colspan="2">
                    <div class="sectionSub">Access to services</div>
                    <br/></td>
            </tr>

            <tr>
                <td align="left" style=" height: 25px;">
                    <strong class="text-right block px-3">Do you have access to Banking Facilities</strong><b>
                </b>
                </td>
                <td style="padding-left: 2px; ">
                    <div class="list">
                        <asp:CheckBoxList ID="chklBankingFacility" runat="server" RepeatColumns="3">
                        </asp:CheckBoxList>
                        <br/>
                        Other:
                        <asp:TextBox ID="txtBankingFacilityOther" runat="server" CssClass="input_small"
                                     MaxLength="100"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="left" style=" height: 25px">
                    <b>Do you have access to your own transport:
                    </b>
                </td>
                <td style="padding-left: 2px; ">
                    <asp:DropDownList ID="ddlOwnTransport" runat="server" CssClass="input_normal"
                                      AppendDataBoundItems="True">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" style=" height: 25px">
                    <b>Do you use a wheelchair:
                    </b>
                </td>
                <td style="padding-left: 2px; ">
                    <asp:DropDownList ID="ddlWheelchair" runat="server" CssClass="input_normal"
                                      AppendDataBoundItems="True">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" style=" height: 25px">
                    <b>Do you have a Live in Carer:
                    </b>
                </td>
                <td style="padding-left: 2px; ">
                    <asp:DropDownList ID="ddlCarer" runat="server" CssClass="input_normal" AppendDataBoundItems="True">
                        <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                    &nbsp;
                </td>
            </tr>

            <tr>

                <td style="padding-left: 2px;"></td>
                <td>

                    <asp:LinkButton ID="btnUpdate" runat="server"
                                    CssClass="btnLink">Update My Details
                    </asp:LinkButton>
                </td>
            </tr>

        </table>
    </div>-->
    <div id="password" class=" area3 p-3 m-3 mb-8 border border-grey rounded bg-white flex flex-wrap">
    <h2 class="w-full header uppercase text-grey text-base p-2">Change Password</h2>
        <div class="sectionHeadClick">
            <br>
        </div>
        <a href="/secure/change_password.aspx" Class="w-2/4 mt-6 text-center bg-orange text-white p-3 rounded uppercase mt-6 ">Change Password
            </a>

    </div>

  

    <script type="text/javascript" src="../js/view_tenant_details.js"></script>
    <script>
        jQuery('#ctl00_LeftMenuControl1_lnkMyDetails').addClass('active');
    </script>
</asp:Content>