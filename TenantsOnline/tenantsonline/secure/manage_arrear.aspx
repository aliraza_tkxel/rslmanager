<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="manage_arrear.aspx.vb" Inherits="tenantsonline.ManageArrear" %>

<%@ Register Src="~/user control/arrear_rent_menu.ascx" TagName="arrear_rent_menu"
    TagPrefix="uc1" %>
<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <div class="sectionHead" style="width: 98%">Help me manage my rent</div>
    <table cellpadding="3" class="table_content">
      
        <tr>
            <td align="left" colspan="3" style="height: 25px">
                <table class="table_box">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblName" runat="server" CssClass="body_caption_bold" Height="16px"
                                Text="Name: " Width="645px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTenancyRefNo" runat="server" CssClass="body_caption_bold" Height="16px"
                                Text="Tenancy Reference Number: " Width="648px"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="label3" runat="server" Text="* My rent balance is:" Width="187px"
                    CssClass="caption" Height="16px"></asp:Label></td>
            <td style="width: 164px">
                <asp:Label ID="lblRentBalance" runat="server" style="font-weight: 700"></asp:Label>
            </td>
            <td  align="right">
                &nbsp;</td>
        </tr>
    
        
        <tr>
            <td colspan="3">
                <asp:Panel ID="pnlText" runat="server" Height="50px" Width="420px" CssClass="caption">
                    &nbsp;* Please note this figure does not include any anticipated housing benefit.<br />
                    <br />
                    If you would like to discuss how we can support you to manage your arrears, 
                    please provide with some further information by <a href="contact.aspx">getting in touch</a>. We will 
                    acknowledge your query within 3 working days.
                    <br />
                    <br />
                    <strong>Problems Paying?
                    <br />
                    </strong>If you experience problems in paying your rent we are here to support 
                    you in getting back on track. A payment arrangement can be a good way of 
                    managing arrears, making regular, affordable payments to clear them. You will be 
                    able to discuss this with your Income Recovery Officer. To identify your Income 
                    Recovery Officer and arrange appointment, please follow <a href="http://broadlandgroup.org/your-home/your-neighbourhood-team.html" target="_blank">this link</a> and enter your 
                    post code to find their contact details.
                    <br />
                    <br />
                    You may also be entitled to support from our Tenancy Support Service. To check 
                    whether you meet the eligibility criteria please discuss this with your Income 
                    Recovery Officer.
                    <br />
                    <br />
                    You may be able to find further information on at your local <a href="http://www.adviceguide.org.uk/england/housing_e.htm" target="_blank">Citizens Advice
                    Bureau</a>.
                    <br />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;<uc1:arrear_rent_menu ID="Arrear_rent_menu1" runat="server" />
            </td>
            <td colspan="1">
            </td>
        </tr>
        <tr>
            <td style="width: 198px">
            </td>
            <td style="width: 164px">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                </td>
            <td colspan="1">
            </td>
        </tr>
    </table>

</asp:Content>
