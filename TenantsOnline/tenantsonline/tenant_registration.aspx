<%@ Page Language="vb" MasterPageFile="~/master pages/UnsecureMaster.Master" AutoEventWireup="false"
CodeBehind="tenant_registration.aspx.vb" Inherits="tenantsonline.TenantRegistration"
Title="Tenants Online :: Tenants Registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <h1 style="font-size: 38px;color: #000000;line-height: 44px;text-align: left"
        class="flex flex-wrap vc_custom_heading split-words red-word-highlight vc_custom_1530116459422"
        aria-label="Contact Broadland">
        <span class="word1 w-auto" aria-hidden="true">Register</span>&nbsp;<span class="word2 w-auto" aria-hidden="true">for</span>&nbsp;<span class="word3 w-auto" aria-hidden="true">the</span>&nbsp;
        <span
                class="word4 w-auto text-red" aria-hidden="true">Broadland</span>&nbsp;
        <span
            class=" w-auto" aria-hidden="true">Portal</span>
    </h1>

    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.</p>

    <div style="margin-left: 10px;">
        <div>
            <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red"></asp:Label>
        </div>
    
        <div style="margin-bottom: 15px;">
            <asp:Label ID="lblFirstName" runat="server" Text="First Name" Width="155px"/>
            <asp:TextBox
                    ID="txtFirstName" runat="server" MaxLength="50"/>
        </div>
        <asp:RequiredFieldValidator ID="valRequireFirstName" runat="server" ControlToValidate="txtFirstName"
                                    ErrorMessage="Please enter your first name" ToolTip="Please enter your first name"
                                    Display="Dynamic">Please enter your first name
        </asp:RequiredFieldValidator>
        <div style="margin-bottom: 15px;">
            <asp:Label ID="lblLastName" runat="server" Text="Last Name" Width="155px"/>
            <asp:TextBox
                    ID="txtLastName" runat="server" MaxLength="50"/>
        </div>
        <asp:RequiredFieldValidator ID="valRequireLastName" runat="server" ControlToValidate="txtLastName"
                                    ErrorMessage="Please enter your last name" ToolTip="Please enter your last name."
                                    Display="Dynamic">Please enter your last name
        </asp:RequiredFieldValidator>
        <div style="margin-bottom: 15px;">
            <asp:Label ID="lblTenancyId" runat="server" Text="Tenancy ID" Width="155px"/>
            <asp:TextBox
                    ID="txtTenancyID" runat="server" MaxLength="50"/>
            <br/>
            <em>Your 6 digit tenancy reference can be found at the top of any Broadland Housing
                letter or Rent Statement you will have received</em></div>
        <asp:RequiredFieldValidator ID="valRequireTenancyId" runat="server" ControlToValidate="txtTenancyID"
                                    ErrorMessage="Please enter your Tenancy ID" SetFocusOnError="True"
                                    ToolTip="Please enter your Tenancy ID"
                                    Display="Dynamic">Please enter your Tenancy ID
        </asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="valRegularTenancyId" runat="server" ControlToValidate="txtTenancyID"
                                        ErrorMessage="Please enter valid Tenancy ID" ValidationExpression="(\d)*"
                                        Display="Dynamic"
                                        ToolTip="Please enter valid Tenancy ID">Please enter valid Tenancy ID
        </asp:RegularExpressionValidator>
        <div style="margin-bottom: 15px;">
            <asp:Label ID="Label2" runat="server" Text="Date of Birth:" Width="155px"/>
            <div class="flex flex-wrap">
                <div class="w-5/6" >
                <asp:TextBox ID="txtDateOfBirth" runat="server" MaxLength="10"
                             EnableTheming="False"/>
                </div>
                <button ID="btnCalander" class="w-1/6 border-0"
                        CausesValidation="False"><img src="/images/buttons/Calendar_button.png"></button>
            </div>
        </div>
        <asp:RequiredFieldValidator ID="valRequireDateOfBirth" runat="server" ControlToValidate="txtDateOfBirth"
                                    Display="Dynamic" ErrorMessage="Please enter your date of birth"
                                    SetFocusOnError="True"
                                    ToolTip="Please enter your date of birth">Please enter your date of birth
        </asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="valRegularDateOfBirth" runat="server"
                                        ErrorMessage="Please enter a valid date of birth"
                                        Text="Please enter a valid date of birth" Display="Dynamic"
                                        ToolTip="Please enter a valid date of birth"
                                        ControlToValidate="txtDateOfBirth"
                                        ValidationExpression="\d\d/\d\d\/\d\d\d\d"></asp:RegularExpressionValidator>
        <asp:Label ID="lblDateFormat" runat="server" CssClass="footnote_item" Text="(dd/mm/yyyy)"/>
        <div>
            <asp:Label ID="lblPostCode" runat="server" Text="Postcode:" Width="155px"></asp:Label>
            <asp:TextBox ID="txtPostCode" runat="server" MaxLength="20"/>
        </div>
        <asp:RequiredFieldValidator ID="valRequirePostcode" runat="server" ControlToValidate="txtPostCode"
                                    ErrorMessage="Please enter your postcode" SetFocusOnError="True"
                                    ToolTip="Please enter your postcode"
                                    Display="Dynamic">Please enter your postcode
        </asp:RequiredFieldValidator>
        <div style="margin: 10px 0px">
            <asp:Label ID="lblPasswordInstruction1" runat="server"
                       Text="Protect your information with a password. This will be your only Broadland Housing Tenants Online Password"
                       Width="100%"/>
        </div>
        <div style="margin-bottom: 15px;">
            <asp:Label ID="lblEmail" runat="server" Text="email address" Width="155px"/>
            <asp:TextBox ID="txtEmail" runat="server" MaxLength="150"/>
        </div>
        <asp:RegularExpressionValidator ID="valFormatEmail" runat="server" ControlToValidate="txtEmail"
                                        ErrorMessage="Please enter a valid email address"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ToolTip="Please enter a valid email address" Display="Dynamic">Please enter a
            valid email address
        </asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="valRequireEmail" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="Please enter your email address" SetFocusOnError="True"
                                    ToolTip="Please enter your email address"
                                    Display="Dynamic"
                                    Text="Please enter your email address"></asp:RequiredFieldValidator>
        <div style="margin-bottom: 15px;">
            <asp:Label ID="Label1" runat="server" Text="Type it again:" Width="155px"></asp:Label>
            <asp:TextBox ID="txtConfirmEmail" runat="server" MaxLength="150"></asp:TextBox>
        </div>
        <asp:CompareValidator ID="valCompareEmail" runat="server" ControlToCompare="txtEmail"
                              ControlToValidate="txtConfirmEmail" ErrorMessage="Your email addresses do not match"
                              ToolTip="Your email addresses do not match" Display="Dynamic">Your email addresses do not
            match
        </asp:CompareValidator>
        <asp:RequiredFieldValidator ID="valRequireConfirmEmail" runat="server" ControlToValidate="txtConfirmEmail"
                                    ErrorMessage="Please enter your email address" SetFocusOnError="True"
                                    ToolTip="Please enter your email address"
                                    Display="Dynamic"
                                    Text="Please enter your email address"></asp:RequiredFieldValidator>
        <div style="margin-bottom: 15px;">
            <asp:Label ID="lblPassword" runat="server" Text="Enter a new password:" Width="155px"/>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="50"/>
        </div>
        <asp:RequiredFieldValidator ID="valRequirePassword" runat="server" ControlToValidate="txtPassword"
                                    ErrorMessage="Please enter your password" ToolTip="Please enter your password"
                                    Display="Dynamic">Please enter your password
        </asp:RequiredFieldValidator>&nbsp;
        <div style="margin-bottom: 15px;">
            <asp:Label ID="lblConfirmPassword" runat="server" Text="Type it again:" Width="155px"/>
            <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" MaxLength="50"/>
        </div>
        <asp:RequiredFieldValidator ID="valRequireConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                    ErrorMessage="Please enter your password" SetFocusOnError="True"
                                    ToolTip="Please enter your password"
                                    Display="Dynamic">Please enter your password
        </asp:RequiredFieldValidator>
        <asp:CompareValidator ID="valComparePassword" runat="server" ControlToCompare="txtPassword"
                              ControlToValidate="txtConfirmPassword" ErrorMessage="Your passwords do not match"
                              ToolTip="Your passwords do not match" Display="Dynamic">Your passwords do not match
        </asp:CompareValidator>
        <br/>
        <br/>
        <div class="flex flex-wrap">
            <div class="w-3/4"></div>
        <div  class="w-1/4">
            <asp:Button ID="ibtnRegister" runat="server" Text="Register" class="w-full btn bg-orange text-white p-3 rounded uppercase mt-3" OnClick="ibtnRegister_Click"/>
        </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"/>
        <ajax:CalendarExtender ID="CalendarExtender_dob" runat="server" Format="dd/MM/yyyy"
                               TargetControlID="txtDateOfBirth" PopupButtonID="btnCalander" EnableViewState="False"/>
    </div>
</asp:Content>
