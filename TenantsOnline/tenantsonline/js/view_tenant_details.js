﻿
$.fn.resetOther = function (hasOther, preserve) {

    var textBoxOther = $(this).closest('div').find(':text');

    textBoxOther.prop('disabled', (!hasOther));

    if (!hasOther) {
        textBoxOther.css("background-color", "lightgrey");
    }
    else {
        textBoxOther.css("background-color", "");
    }

    if ((!hasOther) && (!preserve)) {
        textBoxOther.val('');     
    }
}

$.fn.toggleOtherInList = function (preserve) {

    var selectedText = $(this).next().text().toLowerCase();
    var hasOther = ((selectedText.indexOf('other') != -1) && ($(this).prop('checked')));
    if (selectedText.indexOf('other') != -1) {
        $(this).resetOther(hasOther, preserve);
    }
}

$.fn.toggleOtherInSelect = function (preserve) {
    var selectedText = $('option:selected', $(this)).text().toLowerCase();
    var hasOther = (selectedText.indexOf('other') != -1);

    $(this).resetOther(hasOther, preserve);
}

$(document).ready(function () {

    $('.list').each(function (i) {
        $(this).find(':checkbox').each(function (i) {
            $(this).toggleOtherInList(true);
        });
    });

    $('.select').each(function (i) {
        $(this).first('input[type=select]').toggleOtherInSelect(true);
    });

    $('.list input[type=checkbox]').click(function () {
        $(this).toggleOtherInList(false);
    });

    $('.select select').change(function () {
        $(this).toggleOtherInSelect(false);
    });

});