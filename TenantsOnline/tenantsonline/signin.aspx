<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/UnsecureMaster.Master"
CodeBehind="SignIn.aspx.vb" Inherits="tenantsonline.SignIn" Title="Tenants Online :: Sign In" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="PageContent" runat="server" ContentPlaceHolderID="page_area">

    <h1 style="font-size: 38px;color: #000000;line-height: 44px;text-align: left"
        class="flex flex-wrap vc_custom_heading split-words red-word-highlight vc_custom_1530116459422"
        aria-label="Contact Broadland">
        <span class="word1 w-auto" aria-hidden="true">Your</span>&nbsp;<span class="word2 w-auto" aria-hidden="true">Broadland</span>&nbsp;<span
            class="word3 w-auto" aria-hidden="true">Portal</span>
    </h1>

    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
        magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat.</p>

    <div class="flex flex-wrap">
        <div class="w-full md:w-1/2 p-3">

            <h2>Login</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>
            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>

            <div style="margin-top: 20px; margin-bottom:5px;">
                <asp:Label ID="lblEmail" runat="server" Text="Enter your Tenancy Ref:" Width="140px"></asp:Label>
            </div>

            <div>
                <asp:TextBox ID="txtEmail" runat="server" class="wpcf7-form-control wpcf7-text" MaxLength="150"
                ></asp:TextBox>
            </div>

            <div>
                <asp:RequiredFieldValidator ID="valRequireEmail" runat="server" ControlToValidate="txtEmail"
                                            Display="Dynamic" ErrorMessage="Tenancy Ref missing" SetFocusOnError="True"
                                            ToolTip="Tenancy Ref missing">*
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                        ID="valFormatEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid Tenancy Ref"
                        ValidationExpression="^[0-9]{1,10}$" SetFocusOnError="True"
                        ToolTip="Invalid Tenancy Ref format"></asp:RegularExpressionValidator>
            </div>
            <div>
                <asp:Label ID="LblPassword" runat="server" Text="Enter your password:"
                           Width="146px"></asp:Label>
            </div>

            <div>
                <asp:TextBox ID="txtPassword" class="wpcf7-form-control wpcf7-text" runat="server" TextMode="Password"
                             MaxLength="50"></asp:TextBox>
                <div>
                    <asp:HyperLink ID="lnkForgotPassword" runat="server"
                                   NavigateUrl="~/password_assistance.aspx">Retrieve my password
                    </asp:HyperLink>
                </div>

                <asp:RequiredFieldValidator ID="valRequirePassword" runat="server" ControlToValidate="txtPassword"
                                            Display="Dynamic" ErrorMessage="Pasword missing" SetFocusOnError="True"
                                            ToolTip="Password Missing">*
                </asp:RequiredFieldValidator>
           

                <asp:Button ID="ibtnSignIn" class="w-full btn bg-orange text-white p-3 rounded uppercase mt-3"
                            runat="server" Text="Log in"/>
            </div>


        </div>
        <div class="w-full md:w-1/2 p-3"><h2>Register</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.</p>

            <a class="w-full btn bg-red text-white p-3 rounded uppercase mt-3"
               href="/tenant_registration.aspx">Register</a>


        </div>


    </div>
    <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valEmail" runat="server"
                                   TargetControlID="valRequireEmail"/>
    <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valPassword" runat="server"
                                   TargetControlID="valRequirePassword"/>
    <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valFormatEmail" runat="server"
                                   TargetControlID="valFormatEmail"/>
    <ajax:ToolkitScriptManager ID="Registration_ToolkitScriptManager" runat="server"/>
</asp:Content>
