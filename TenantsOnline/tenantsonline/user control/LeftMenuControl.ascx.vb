
#Region " Imports "

Imports Broadland.TenantsOnline.BusinessLogic

#End Region

Partial Public Class LeftMenuControl
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        lnkIncreaseSize.Attributes.Add("onClick", "FWresizeFont(1);return false")
        lnkReduceSize.Attributes.Add("onClick", "FWresizeFont(-1);return false")
        lnkReset.Attributes.Add("onClick", "resetFontSize();return false")
    End Sub

End Class