<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LeftMenuControl.ascx.vb" Inherits="tenantsonline.LeftMenuControl" %>

            <asp:HyperLink ID="lnkWelcome" runat="server" CssClass="menu_item" Width="183px" NavigateUrl="~/secure/Home.aspx">Dashboard</asp:HyperLink>
   
            <asp:HyperLink ID="lnkMyDetails" runat="server" CssClass="menu_item" NavigateUrl="~/secure/view_tenant_details.aspx">Account Details</asp:HyperLink>

            <asp:HyperLink ID="lnkRentManagement" runat="server" CssClass="menu_item" NavigateUrl="~/secure/rent_account.aspx">Statements</asp:HyperLink>
        
            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="menu_item" NavigateUrl="~/secure/customer_enquiries.aspx">My Enquiries</asp:HyperLink>

            <asp:HyperLink ID="lnkReportFault" runat="server" CssClass="menu_item" NavigateUrl="~/secure/faultlocator/report_a_fault.aspx">Repairs</asp:HyperLink>
       
           <asp:HyperLink ID="lnkOffices" runat="server" CssClass="menu_item" NavigateUrl="https://www.broadlandgroup.org/home/contact-us/">Contact Us</asp:HyperLink>


<%--
            <asp:HyperLink ID="lnkBroadlandNews"   runat="server" CssClass="menu_item" NavigateUrl="~/secure/broadland_news.aspx">Broadland Housing News</asp:HyperLink>
      
            <asp:HyperLink ID="lnkArrearsManagement" runat="server" CssClass="menu_item" NavigateUrl="~/secure/manage_arrear.aspx">Help me manage my rent</asp:HyperLink>
       
            <asp:HyperLink ID="lnkTenancyTermination" runat="server" Width="200px" CssClass="menu_item" NavigateUrl="~/secure/tenancy_termination.aspx">I would like to end my tenancy</asp:HyperLink>
--%>
<%--
            <asp:HyperLink ID="lnkMoveHouse" runat="server" CssClass="menu_item" NavigateUrl="~/secure/move_house.aspx">> I would like to move house</asp:HyperLink>
--%>
<%--
            <asp:HyperLink ID="lnkGarage" runat="server" CssClass="menu_item" NavigateUrl="~/secure/request_garage_parking.aspx">I would like a garage/parking space</asp:HyperLink>
 
            <asp:HyperLink ID="lnkBroadlandComplaint" runat="server" CssClass="menu_item" NavigateUrl="~/secure/broadland_complaint.aspx">I wish to make a complaint</asp:HyperLink>
       
            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="menu_item" NavigateUrl="~/secure/report_ASB.aspx">Report anti social behaviour</asp:HyperLink>

            <asp:HyperLink ID="lnkReportAbdProperty" runat="server" CssClass="menu_item" NavigateUrl="~/secure/report_abandoned.aspx">Report vandalism, property neglect or subletting</asp:HyperLink>

            <asp:HyperLink ID="lnkSignOut" runat="server" CssClass="menu_item" NavigateUrl="~/SignOut.aspx">Sign Out</asp:HyperLink>
--%>

          <div id="fontControl" style="padding-left: 10px">
            <asp:HyperLink ID="lnkIncreaseSize" runat="server" CssClass="link_resize" NavigateUrl="#"
             EnableTheming="True" ForeColor="Red">A+</asp:HyperLink>
             <asp:Label ID="lblSeperator1" runat="server" ForeColor="Silver" Text="|" Width="5px"></asp:Label>
             <asp:HyperLink ID="lnkReduceSize" runat="server" CssClass="link_resize" NavigateUrl="#" EnableTheming="True" ForeColor="Red">A-</asp:HyperLink></td>
            <asp:Label ID="lblSeperator2" runat="server" ForeColor="Gray" Text="|" Width="5px"></asp:Label></td>
             <asp:HyperLink ID="lnkReset" CssClass="link_resize" runat="server"  NavigateUrl="#" EnableTheming="True" ForeColor="Red">Reset</asp:HyperLink></td>
             </div>

