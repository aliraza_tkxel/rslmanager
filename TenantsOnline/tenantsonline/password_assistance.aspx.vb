Imports System
Imports System.Web.HttpContext

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class PasswordAssistance
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        ''Set the attribute so that when user presses enter key the specified event for the mentioned button will be fired
        txtConfirmEmail.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnContinue.UniqueID + "').click();return false;}} else {return true}; ")

        Me.txtEmail.Focus()

    End Sub

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Me.PasswordAssitance()
    End Sub


#End Region

#Region "Methods"

    Private Sub PasswordAssitance()

        Dim custHeadBO As CustomerHeaderBO = New CustomerHeaderBO()

        custHeadBO.UserName = txtEmail.Text.Trim()

        '' Call to Business Layer method  
        Dim custMngr As CustomerManager = New CustomerManager()
        custMngr.PasswordAssitance(custHeadBO)

        Dim flagStatus As Boolean = False
        flagStatus = custHeadBO.IsFlagStatus

        '' If customer email gets matched
        If (flagStatus) Then

            ''Read the email contents from the email contents text file
            Dim strEmailContents As String = EmailContentsSingleton.GetEmailTemplateContents
            Dim bytePassword As Byte() = Convert.FromBase64String(custHeadBO.Password)
            Dim decryptedPassword = System.Text.Encoding.UTF8.GetString(bytePassword)
            strEmailContents = Me.ConstructEmail(strEmailContents, decryptedPassword)

            '' NOTE:- Need to configure SMTP server
            '' Email wil be sent on cutomer's email address

            Dim objUtilityBL As New UtilityManager
            ''Note: Customer will be redirected to singin page. A message showed containing email is sent
            Dim emailSendFlag As Boolean = objUtilityBL.SendMail(txtEmail.Text, EmailConstants.PasswordAssistanceEmailSubject, strEmailContents)

            If (emailSendFlag) Then
                Me.lblInfoMsg.Text = ""
                Me.lblInfoMsg.Text = UserInfoMsgConstants.PasswordAssistanceEmailSent & Me.txtEmail.Text
            Else
                Me.lblInfoMsg.Text = ""
                Me.lblErrorMsg.Text = UserInfoMsgConstants.PasswordAssistanceEmailSendErrorMsg & Me.txtEmail.Text
            End If
        Else
            Me.lblInfoMsg.Text = ""
            Me.lblErrorMsg.Text = UserInfoMsgConstants.PasswordAssistanceIncorrectEmail
        End If

    End Sub
    Private Function ConstructEmail(ByVal strEmailContents As String, ByVal strPassword As String) As String

        strEmailContents = strEmailContents.Replace("#PASSWORD#", strPassword)

        Return strEmailContents
    End Function

#End Region

#Region "Functions"

    ''No function defined yet...

#End Region

   
End Class