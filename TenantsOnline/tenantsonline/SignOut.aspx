<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/UnsecureMaster.Master" CodeBehind="SignOut.aspx.vb" Inherits="tenantsonline.SignOut" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<asp:Content ID="PageContentSignOut" runat="server" ContentPlaceHolderID="page_area">

                    <div class="sectionHead" style="width: 98%">You have been successfully signed out from Tenants Online</div>

                    <div style="margin-left:10px;">
                    <br />
                    <br />
                    <div>
                    <asp:LinkButton ID="lnkSignIn" runat="server">Sign In to Tenants Online </asp:LinkButton>
                    </div>
                    <br />
                    <br />
                    <div>
                    <asp:LinkButton ID="lnkBrodland" runat="server">Broadland Housing Website</asp:LinkButton>
                    </div>
                    </div>
                    
</asp:Content>