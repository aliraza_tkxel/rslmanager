﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/UnsecureMaster.Master" CodeBehind="payment_terms_and_conditions.aspx.vb" Inherits="tenantsonline.payment_terms_and_conditions" %>
<asp:Content ID="Content2" ContentPlaceHolderID="page_area" runat="server">


 <div class="sectionHead" style="width: 98%">Online Payments Terms and Conditions</div>

 <div id="contentWrapper">

          <div id="leftcolumn"><div><asp:HyperLink ID="lnkHomePage" runat="server" NavigateUrl="~/signin.aspx" CssClass="menu_item">Login Page</asp:HyperLink></div></div>

       <div id="rightcolumn">

  <h2>Refund Policy</h2>

   
    <p>Where the Association agrees that a refund is due in respect of a payment made to them then:</p>
  <ul>
    <li>The credit/debit card used for the payment will be refunded </li>
    <li>This will be performed as soon as possible but within 14 days of   the refund application being received and agreed by the Association. </li>
  </ul>
  <p>Should you have any queries regarding an overpayment on your account   and think you may be eligible for a refund, please contact the relevant   department quoting your receipt or reference number.</p>

  <h2>Currencies</h2>
  
	<p>We  accept Pound Sterling (£ GBP)</p>
  
	<h2>Payments Accepted </h2>
	<p>
	  <script language="javascript" src="https://secure.worldpay.com/wcc/logo?instId=315455"></script>
	  <noscript>
      This information is pulled directly from Paythru by Javascript. Please enable javascript to view this information.
      </noscript>
	  
    </p>
	<h2>Company Registration</h2>
	
      <p><strong>Broadland Housing Association</strong> Part of the Broadland Housing Group, incorporating Broadland Housing, Broadland Meridian and Broadland 
      St Benedict&rsquo;s. Broadland Housing Association Limited Registered address: NCFC, Jarrold Stand, Carrow Road, Norwich, NR1 1HU. Registered under the Industrial 
      and Provident Societies Act 1965 in England and Wales as a non profit making housing association with charitable status. Company No. IPI6274R. TSA Reg. No. L0026. 
      VAT Registration No 927512717. Tel: 0303 303 0003. Email: <a href="mailto:enq@broadlandgroup.org">enq@broadlandgroup.org</a></p>

      </div>
      </div>
</asp:Content>
