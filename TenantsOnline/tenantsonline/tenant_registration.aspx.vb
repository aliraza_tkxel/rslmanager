Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class TenantRegistration
    Inherits System.Web.UI.Page

#Region "Events"

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        ''Set the attribute so that when user presses enter key the specified event for the mentioned button will be fired
        txtConfirmPassword.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + ibtnRegister.UniqueID + "').click();return false;}} else {return true}; ")
        Me.txtFirstName.Focus()

    End Sub

    'Protected Sub ibtnRegister_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnRegister.Click
    '    Me.CustomerRegistration()
    'End Sub

#End Region

#Region "Private Methods"

    Private Sub CustomerRegistration()

        ''Instantiate business object wich will hold the customer data
        Dim custBO As CustomerBO = New CustomerBO()

        ''Instantiate business object wich will hold the customer address data
        Dim custAddressBO As CustomerAddressBO = New CustomerAddressBO()

        ''Initialize business object attributes
        custBO.FirstName = Me.txtFirstName.Text.Trim()
        custBO.LastName = Me.txtLastName.Text.Trim()
        custBO.Tenancy = Me.txtTenancyID.Text.Trim()
        Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(txtPassword.Text)
        Dim encodedPassword As String = Convert.ToBase64String(byt)
        custBO.Password = encodedPassword

        Dim dob As DateTime

        Try
            dob = Utility.StringToDate(Me.txtDateOfBirth.Text)
        Catch ex As Exception
            ''Exception handled
        End Try

        If dob <> Nothing Then
            custBO.DateOfBirth = dob
        Else
            Me.lblErrorMessage.Text = UserInfoMsgConstants.CustomerRegistrationInvalidDateOfBirth
            Return
        End If

        ''Set the Busness Object attributes values

        custAddressBO.Email = Me.txtEmail.Text.Trim()
        custAddressBO.PostCode = Me.txtPostCode.Text.Trim()

        custBO.Address = custAddressBO

        ''Flag variable to varify the success scenario of the registration usecase
        Dim flagStatus As Boolean = False

        Dim objBL As New CustomerManager()
        flagStatus = objBL.CustomerRegistration(custBO)

        ''Redirect to home page if successfully registered, otherwise prompt error message
        If flagStatus Then
            Response.Redirect("~/secure/home.aspx")
        Else
            Me.lblErrorMessage.Text = custBO.UserMsg.Replace("\n", "</br>") & "</br>"
        End If

    End Sub


#End Region

    Protected Sub ibtnRegister_Click(sender As Object, e As EventArgs)
        Me.CustomerRegistration()
    End Sub
End Class