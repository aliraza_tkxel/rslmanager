Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class SignIn
    Inherits System.Web.UI.Page

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Request.Form("username") <> "" Then
            Me.txtEmail.Text = Request.Form("username")
            Me.txtPassword.Text = Request.Form("password")
            Me.AuthenticateCustomer()
        End If


    End Sub


#Region "Events"


#Region "SinIn Clicked"

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        ''Set the attribute so that when user presses enter key the specified event for the mentioned button will be fired
        txtPassword.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + ibtnSignIn.UniqueID + "').click();return false;}} else {return true}; ")
        Me.txtEmail.Focus()

    End Sub

   
#End Region



#Region "rdo changed"

    'Protected Sub rdoCustomerGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoCustomerGroup.SelectedIndexChanged

    '    '' If customer selects radio button option of "I am a New Customer"
    '    '' User will be redirected to Tenants Registration page        
    '    Response.Redirect("tenant_registration.aspx")

    'End Sub

    Protected Sub ibtnSignIn_Click(sender As Object, e As EventArgs) Handles ibtnSignIn.Click

        Me.AuthenticateCustomer()

    End Sub

#End Region


#End Region

#Region "Methods"

#Region "AuthenticateCustomer Function"

    Private Sub AuthenticateCustomer()

        '' If customer is returning customer
        '' Returning Customer option of radio button is selected by default

        Dim custHeadBO As CustomerHeaderBO = New CustomerHeaderBO()

        custHeadBO.TenancyID = txtEmail.Text.Trim()
        Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(txtPassword.Text)
        Dim encodedPassword As String = Convert.ToBase64String(byt)
        custHeadBO.Password = encodedPassword

        '' Call to Business Layer method  
        Dim custMngr As CustomerManager = New CustomerManager()
        custMngr.AuthenticateCustomer(custHeadBO)

        Dim flagStatus As Boolean = False
        flagStatus = custHeadBO.IsFlagStatus

        '' If customer gets authenticated, customer info stored in session and redirected to Home page
        If (flagStatus) Then

            custMngr.LogSessionStart(custHeadBO)
            Session("customerSession") = custHeadBO.sessionId
            Me.StoreInSession(custHeadBO)

            Response.Redirect("~/secure/home.aspx")
        Else
            Me.lblErrorMessage.Text = UserInfoMsgConstants.LoginIncorrectMsg
        End If

    End Sub

#End Region

#Region "StoreInSession Function"

    Private Sub StoreInSession(ByRef custHeadBO As CustomerHeaderBO)


        '' storing custHeadBO object in Session so that it can be used across pages
        Session("custHeadBO") = custHeadBO

        'Response.Redirect("~/secure/home.aspx")


    End Sub

#End Region

#End Region

End Class