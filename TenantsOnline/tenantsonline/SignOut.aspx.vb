
#Region " Imports "

Imports Broadland.TenantsOnline.BusinessLogic

#End Region

Partial Public Class SignOut
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim custMngr As CustomerManager = New CustomerManager()
        Dim custSession As Integer

        custSession = CType(Session("customerSession"), Integer)
        custMngr.LogSessionEnd(custSession)
        Session.Clear()

    End Sub

    Protected Sub lnkSignIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSignIn.Click
        Response.Redirect("SignIn.aspx")
    End Sub

    Protected Sub lnkBrodland_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkBrodland.Click
        Response.Redirect("http://www.broadlandhousing.org")
    End Sub
End Class