<%@ Page Language="vb" MasterPageFile="~/master pages/UnsecureMaster.Master" AutoEventWireup="false"
    CodeBehind="change_email.aspx.vb" Inherits="tenantsonline.ChangeEmail" Title="Tenants Online :: Change Email" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <div id="contentWrapper">
        <div class="sectionHead" style="width: 98%">
            Change your email address and sign in</div>
        <br />
        <br />
        <div id="leftcolumn">
            <div>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/signin.aspx" CssClass="menu_item">Login Page</asp:HyperLink></div>
        </div>
        <div id="rightcolumn">
            <table cellpadding="3" class="table_content">
                <tr>
                    <td colspan="3">
                        <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valEmail" runat="server"
                            TargetControlID="valRequireEmail" />
                        <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valRequireNewEmail" runat="server"
                            TargetControlID="valRequireNewEmail" />
                        <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valPassword" runat="server"
                            TargetControlID="valRequirePassword" />
                        <ajax:ToolkitScriptManager ID="Registration_ToolkitScriptManager" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblCaption1" runat="server" Text="Please enter your current email address and password"
                            Width="347px" />
                    </td>
                    <td style="width: 292px">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label ID="lblOldEmail" runat="server" Text="Old Email Address:" Width="141px" />
                    </td>
                    <td style="width: 109px">
                        <asp:TextBox ID="txtOldEmail" runat="server" MaxLength="150" />
                    </td>
                    <td style="width: 292px">
                        <asp:RequiredFieldValidator ID="valRequireEmail" runat="server" ControlToValidate="txtOldEmail"
                            Display="Dynamic" ErrorMessage="Enter email address" SetFocusOnError="True" ToolTip="Enter email address">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label ID="lblPassword" runat="server" Text="Password:" Width="64px" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="50" />
                    </td>
                    <td style="width: 292px">
                        <asp:RequiredFieldValidator ID="valRequirePassword" runat="server" ControlToValidate="txtPassword"
                            Display="Dynamic" ErrorMessage="Enter password" SetFocusOnError="True" ToolTip="Enter password">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="width: 292px">
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Label ID="lblNewEmail" runat="server" Text="New Email Address:" Width="146px" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewEmail" runat="server" CssClass="input_normal" MaxLength="150" />
                    </td>
                    <td style="width: 292px">
                        <asp:RequiredFieldValidator ID="valRequireNewEmail" runat="server" ControlToValidate="txtNewEmail"
                            Display="Dynamic" ErrorMessage="Enter new email address" SetFocusOnError="True"
                            ToolTip="Enter new email address">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <br />
                        <asp:Button ID="btnSignIn" runat="server" Text="Update" />
                    </td>
                    <td style="width: 292px">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
