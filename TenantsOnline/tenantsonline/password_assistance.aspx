<%@ Page Language="vb" MasterPageFile="~/master pages/UnsecureMaster.Master" AutoEventWireup="false"
Codebehind="password_assistance.aspx.vb" Inherits="tenantsonline.PasswordAssistance"
Title="Tenants Online :: Password Assistance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">

    <div id="contentWrapper">

        <h1 style="font-size: 38px;color: #000000;line-height: 44px;text-align: left"
            class="flex flex-wrap vc_custom_heading split-words red-word-highlight vc_custom_1530116459422"
            aria-label="Contact Broadland">
            <span class="word1 w-auto" aria-hidden="true">Password</span>&nbsp;<span class="word2 w-auto"
                                                                                     aria-hidden="true">Assistance</span>&nbsp;
        </h1>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo
            consequat.</p>


        <div>

            <table cellpadding="3">
                <tr>
                    <td colspan="3">
                        <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valEmail" runat="server"
                                                       TargetControlID="valRequireEmail"/>

                        <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valConfirmEmail" runat="server"
                                                       TargetControlID="valConfirmEmail"/>

                        <ajax:ValidatorCalloutExtender ID="ValidatorCalloutExtender_valCompareEmail" runat="server"
                                                       TargetControlID="valCompareEmail"/>

                        <ajax:ToolkitScriptManager ID="Registration_ToolkitScriptManager" runat="server"/>

                    </td>
                </tr>

                <tr>

                    <td colspan="3">
                        <asp:Label ID="lblErrorMsg" runat="server"/>
                        <asp:Label ID="lblInfoMsg" runat="server"/>
                    </td>

                </tr>

                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblPassword" runat="server"
                                   Text="Please enter your email address and your password will be forwarded to your email account."/>
                    </td>

                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 25px">
                        <asp:Label ID="lblPassword2" runat="server" Text=""/>
                    </td>

                </tr>
                <tr>
                    <td style="text-align: right;" align="right">
                        <asp:Label ID="lblEmail" runat="server" Text="Email address:" Width="89px"/>
                    </td>

                    <td style="width: 205px">
                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="150"/>
                    </td>

                    <td>
                        <asp:RequiredFieldValidator ID="valRequireEmail" runat="server" ControlToValidate="txtEmail"
                                                    Display="Dynamic" ErrorMessage="Email missing"
                                                    SetFocusOnError="True" ToolTip="Email missing">*
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>

                <tr>
                    <td style="text-align: right; width: 218px;" align="right">
                        <asp:Label ID="lblConfirmEmail" runat="server" Height="12px"
                                   Text="Please reconfirm your email address:"
                                   Width="226px"/>
                    </td>

                    <td style="width: 205px">
                        <asp:TextBox ID="txtConfirmEmail" runat="server" MaxLength="150"/>
                    </td>

                    <td>
                        <asp:RequiredFieldValidator ID="valConfirmEmail" runat="server"
                                                    ControlToValidate="txtConfirmEmail"
                                                    Display="Dynamic" ErrorMessage="Confirm email missing"
                                                    SetFocusOnError="True"
                                                    ToolTip="Email missing">*
                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="valCompareEmail" runat="server" ControlToCompare="txtEmail"
                                              ControlToValidate="txtConfirmEmail" ErrorMessage="Email does not match"
                                              ToolTip="Email does not match"></asp:CompareValidator>
                    </td>
                </tr>

                <tr>
                    <td style="width: 218px">

                    </td>

                    <td align="right" style="width: 205px">
                        <asp:Button ID="btnContinue" runat="server"
                                    class="w-full btn bg-orange text-white p-3 rounded uppercase mt-3" Text="Continue"/>
                    </td>

                    <td>
                    </td>

                </tr>

            </table>

        </div>

    </div>

</asp:Content>
