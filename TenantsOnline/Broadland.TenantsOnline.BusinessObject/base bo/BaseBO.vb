'' Class Name -- BaseBO
'' Base Class -- System
'' Summary -- All BOs will inherit from this class. Holds common attributes 
'' Methods -- None
'' Author  -- Muanwar Nadeem
Imports System
Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class BaseBO

#Region "Constructors"

        Public Sub New()

            Me.IsFlagStatus = False
            Me.UserMsg = String.Empty

        End Sub

#End Region

#Region "Properties"

        ' Get/Set property for _flagStatus
        Public Property IsFlagStatus() As Boolean

        ' Get/Set property for _userMsg
        Public Property UserMsg() As String

#End Region

    End Class

End Namespace


