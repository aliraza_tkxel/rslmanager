Imports System


Namespace Broadland.TenantsOnline.BusinessObject

    '' This class will hold News data

    Public Class NewsBO : Inherits BaseBO

#Region "Attributes"

        Private _newsid As Integer
        Private _newscategory As Integer
        Private _headline As String
        Private _imagepath As String
        Private _datecreated As DateTime
        Private _newscontent As String
        Private _rank As Integer
        Private _active As Boolean
        Private _ishomepage As Boolean
        Private _homepageimage As String

#End Region

#Region "Constructors"

        Public Sub New()
            _newsid = -1
            _newscategory = -1
            _headline = String.Empty
            _imagepath = String.Empty
            _datecreated = Nothing
            _newscontent = String.Empty
            _rank = -1
            _active = False
            _ishomepage = False
            _homepageimage = String.Empty

        End Sub

#End Region

#Region "Properties"

        Public Property NewsID() As Integer
            Get
                Return _newsid
            End Get
            Set(ByVal value As Integer)
                _newsid = value
            End Set
        End Property

        Public Property NewsCategory() As Integer
            Get
                Return _newscategory
            End Get
            Set(ByVal value As Integer)
                _newscategory = value
            End Set
        End Property

        Public Property HeadLine() As String
            Get
                Return _headline
            End Get
            Set(ByVal value As String)
                _headline = value
            End Set
        End Property

        Public Property ImagePath() As String
            Get
                Return _imagepath
            End Get
            Set(ByVal value As String)
                _imagepath = value
            End Set
        End Property

        Public Property DateCreated() As DateTime
            Get
                Return _datecreated
            End Get
            Set(ByVal value As DateTime)
                _datecreated = value
            End Set
        End Property

        Public Property NewsContent() As String
            Get
                Return _newscontent
            End Get
            Set(ByVal value As String)
                _newscontent = value
            End Set
        End Property

        Public Property Rand() As Integer
            Get
                Return _rank
            End Get
            Set(ByVal value As Integer)
                _rank = value
            End Set
        End Property

        Public Property IsActive() As Integer
            Get
                Return _active
            End Get
            Set(ByVal value As Integer)
                _active = value
            End Set
        End Property

        Public Property IsHomepage() As Integer
            Get
                Return _ishomepage
            End Get
            Set(ByVal value As Integer)
                _ishomepage = value
            End Set
        End Property

        Public Property HomepageImage() As String
            Get
                Return _homepageimage
            End Get
            Set(ByVal value As String)
                _homepageimage = value
            End Set
        End Property

#End Region

    End Class


End Namespace

