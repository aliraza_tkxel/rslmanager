Namespace Broadland.TenantsOnline.BusinessObject

    <Serializable()> _
    Public Class AsbBO : Inherits EnquiryLogBO


#Region "Attributes"

        Private _asbId As Integer
        Private _categoryId As Integer
        Private _categoryText As String


#End Region

#Region "Constructor"
        Public Sub New()

            _asbId = -1
            _categoryId = -1
            _categoryText = String.Empty

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _asbId
        Public Property AsbID() As Integer
            Get
                Return _asbId
            End Get
            Set(ByVal value As Integer)
                _asbId = value
            End Set
        End Property


        ' Get / Set property for _categoryId
        Public Property CategoryID() As Integer
            Get
                Return _categoryId
            End Get
            Set(ByVal value As Integer)
                _categoryId = value
            End Set
        End Property

        ' Get / Set property for _categoryText
        Public Property CategoryText() As String
            Get
                Return _categoryText
            End Get
            Set(ByVal value As String)
                _categoryText = value
            End Set
        End Property

#End Region

    End Class

End Namespace
