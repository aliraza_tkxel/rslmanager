Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class EnquiryLogBO : Inherits BaseBO


#Region "Attributes"

        Private _enquiryLogId As Integer
        Private _creationDate As DateTime
        Private _description As String
        ''  Private _removeFlag As Integer
        Private _itemStatusId As Integer
        Private _itemStatus As String
        Private _tenancyId As Integer
        Private _customerId As Integer
        Private _jouranlId As Integer
        Private _itemNatureId As Integer
        Private _enquiryCount As Integer
        Private _responseCount As Integer
        Private _terminationCount As Integer
        Private _terminationDate As Date

#End Region

#Region "Constructor"
        Public Sub New()
            _enquiryLogId = -1
            _creationDate = Nothing
            _description = String.Empty
            ''  _removeFlag = -1
            _itemStatusId = -1
            _itemStatus = String.Empty
            _tenancyId = -1
            _customerId = -1
            _jouranlId = -1
            _itemNatureId = -1
            _enquiryCount = -1
            _responseCount = -1
            _terminationCount = -1
            _terminationDate = Nothing
        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _enquiryLogId
        Public Property EnquiryLogId() As Integer
            Get
                Return _enquiryLogId
            End Get
            Set(ByVal value As Integer)
                _enquiryLogId = value
            End Set
        End Property

        ' Get / Set property for _creationDate
        Public Property CreationDate() As Date
            Get
                Return _creationDate
            End Get
            Set(ByVal value As Date)
                _creationDate = value
            End Set
        End Property

        ' Get / Set property for _description
        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property

        ' Get / Set property for _itemStatusId
        Public Property ItemStatusId() As Integer
            Get
                Return _itemStatusId
            End Get
            Set(ByVal value As Integer)
                _itemStatusId = value
            End Set
        End Property

        ' Get / Set property for _itemStatus
        Public Property ItemStatus() As String
            Get
                Return _itemStatus
            End Get
            Set(ByVal value As String)
                _itemStatus = value
            End Set
        End Property

        ' Get / Set property for _tenancyId
        Public Property TenancyId() As Integer
            Get
                Return _tenancyId
            End Get
            Set(ByVal value As Integer)
                _tenancyId = value
            End Set
        End Property

        ' Get / Set property for _customerId
        Public Property CustomerId() As Integer
            Get
                Return _customerId
            End Get
            Set(ByVal value As Integer)
                _customerId = value
            End Set
        End Property

        ' Get / Set property for _jouranlId
        Public Property JournalId() As Integer
            Get
                Return _jouranlId
            End Get
            Set(ByVal value As Integer)
                _jouranlId = value
            End Set
        End Property

        ' Get / Set property for  _itemNatureId
        Public Property ItemNatureId() As Integer
            Get
                Return _itemNatureId
            End Get
            Set(ByVal value As Integer)
                _itemNatureId = value
            End Set
        End Property

        ' Get / Set property for _terminationDate
        Public Property TerminationDate() As Date
            Get
                Return _terminationDate
            End Get
            Set(ByVal value As Date)
                _terminationDate = value
            End Set
        End Property

        ' Get / Set property for _enquiryCount
        Public Property CustomerEnquiryCount() As Integer
            Get
                Return _enquiryCount
            End Get
            Set(ByVal value As Integer)
                _enquiryCount = value
            End Set
        End Property

        ' Get / Set property for _terminationCount
        Public Property TerminationCount() As Integer
            Get
                Return _terminationCount
            End Get
            Set(ByVal value As Integer)
                _terminationCount = value
            End Set
        End Property

        ' Get / Set property for _responseCount
        Public Property EnquiryResponseCount() As Integer
            Get
                Return _responseCount
            End Get
            Set(ByVal value As Integer)
                _responseCount = value
            End Set
        End Property

#End Region

    End Class

End Namespace
