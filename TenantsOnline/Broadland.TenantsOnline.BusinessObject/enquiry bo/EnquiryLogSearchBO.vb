Imports System

Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class EnquiryLogSearchBO : Inherits BaseBO

#Region "Attributes"

        '' Holds column name by which we are sorting 
        '' rowcount is number of results we want before querying database again
        '' pageindex is page number on gridview we will use it to calculate along 
        '' with rowcount to get results in between
        Private _sortBy As String
        Private _sortOrder As String
        Private _rowCount As Integer
        Private _pageIndex As Integer
        Private _firstName As String
        Private _lastName As String
        Private _tenancyID As String
        Private _date As DateTime
        Private _nature As String
        Private _status As String
        Private _text As String
        Private _isSearch As Boolean



#End Region

#Region "Construtor"

        Public Sub New()
            _sortBy = String.Empty
            _sortOrder = String.Empty
            _rowCount = 10
            _pageIndex = 0
            _firstName = String.Empty
            _lastName = String.Empty
            _tenancyID = String.Empty
            _date = Nothing
            _nature = String.Empty
            _status = String.Empty
            _text = String.Empty
            _isSearch = False
        End Sub

#End Region



#Region "Get/Set Properties"
        ' Get / Set property for _sortBy
        Public Property SortBy() As String

            Get
                Return _sortBy
            End Get

            Set(ByVal value As String)
                _sortBy = value
            End Set

        End Property
        ' Get / Set property for _sortBy
        Public Property SortOrder() As String

            Get
                Return _sortOrder
            End Get

            Set(ByVal value As String)
                _sortOrder = value
            End Set

        End Property
        ' Get / Set property for _rowCount
        Public Property RowCount() As Integer

            Get
                Return _rowCount
            End Get

            Set(ByVal value As Integer)
                _rowCount = value
            End Set

        End Property

        ' Get / Set property for _pageIndex
        Public Property PageIndex() As Integer

            Get
                Return _pageIndex
            End Get

            Set(ByVal value As Integer)
                _pageIndex = value
            End Set

        End Property
        Public Property FirstName() As String

            Get
                Return _firstName
            End Get

            Set(ByVal value As String)
                _firstName = value
            End Set

        End Property

        ' Get / Set property for _lastName
        Public Property LastName() As String

            Get
                Return _lastName
            End Get

            Set(ByVal value As String)
                _lastName = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property TenancyID() As String

            Get
                Return _tenancyID
            End Get

            Set(ByVal value As String)
                _tenancyID = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property DateValue() As Date

            Get
                Return _date
            End Get

            Set(ByVal value As Date)
                _date = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property Nature() As String

            Get
                Return _nature
            End Get

            Set(ByVal value As String)
                _nature = value
            End Set

        End Property


        ' Get / Set property for _tenancyID
        Public Property Status() As String

            Get
                Return _status
            End Get

            Set(ByVal value As String)
                _status = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property TextValue() As String

            Get
                Return _text
            End Get

            Set(ByVal value As String)
                _text = value
            End Set

        End Property

        ' Get/Set property for _flagStatus
        Public Property IsSearch() As Boolean

            Get
                Return _isSearch
            End Get

            Set(ByVal value As Boolean)
                _isSearch = value
            End Set

        End Property
#End Region





    End Class

End Namespace