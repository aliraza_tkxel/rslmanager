Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class JournalBO : Inherits BaseBO


#Region "Attributes"

        Private _journalId As Integer
        Private _lastActionDate As DateTime
        Private _notes As String
        Private _historyId As Integer
        Private _enquiryId As Integer
        Private _lastActionUser As Integer

#End Region

#Region "Constructor"
        Public Sub New()
            _journalId = -1
            _lastActionDate = Nothing
            _notes = String.Empty
            _historyId = -1
            _enquiryId = -1
            _lastActionUser = -1

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _journalID
        Public Property JournalID() As Integer
            Get
                Return _journalId
            End Get
            Set(ByVal value As Integer)
                _journalId = value
            End Set
        End Property

        ' Get / Set property for _lastActionDate
        Public Property LastActionDate() As DateTime
            Get
                Return _lastActionDate
            End Get
            Set(ByVal value As Date)
                _lastActionDate = value
            End Set
        End Property

        ' Get / Set property for _notes
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property

        ' Get / Set property for _historyID
        Public Property HistoryID() As Integer
            Get
                Return _historyId
            End Get
            Set(ByVal value As Integer)
                _historyId = value
            End Set
        End Property

        ' Get / Set property for _enquiryId
        Public Property EnquiryId() As Integer
            Get
                Return _enquiryId
            End Get
            Set(ByVal value As Integer)
                _enquiryId = value
            End Set
        End Property

        ' Get / Set property for _lastActionUser
        Public Property LastActionUser() As Integer
            Get
                Return _lastActionUser
            End Get
            Set(ByVal value As Integer)
                _lastActionUser = value
            End Set
        End Property


#End Region

    End Class

End Namespace
