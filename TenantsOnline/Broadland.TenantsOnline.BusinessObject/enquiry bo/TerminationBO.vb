Namespace Broadland.TenantsOnline.BusinessObject

    Public Class TerminationBO : Inherits EnquiryLogBO


#Region "Attributes"

        Private _terminationId As Integer
        Private _movingOutDate As DateTime

#End Region

#Region "Constructor"
        Public Sub New()

            _terminationId = -1
            _movingOutDate = Nothing

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _terminationId
        Public Property TerminationId() As Integer
            Get
                Return _terminationId
            End Get
            Set(ByVal value As Integer)
                _terminationId = value
            End Set
        End Property

        ' Get / Set property for _movingOutDate
        Public Property MovingOutDate() As Date
            Get
                Return _movingOutDate
            End Get
            Set(ByVal value As Date)
                _movingOutDate = value
            End Set
        End Property

#End Region

    End Class

End Namespace
