﻿Namespace Broadland.TenantsOnline.BusinessObject

    Public Class PaythruBO : Inherits BaseBO

#Region " Constructor "

        Public Sub New()

            Me.ConfigSettingName = String.Empty
            Me.ConfigSettingValue = String.Empty

        End Sub

#End Region

#Region " Properties "

        Public Property ConfigSettingName As String
        Public Property ConfigSettingValue As String

#End Region

    End Class

End Namespace
