﻿Namespace Broadland.TenantsOnline.BusinessObject

    '' Class Name -- AreaBO
    '' Base Class -- BaseBO
    '' Created By  -- Noor Muhammad
    '' Create on -- 14th Oct,2008

    <Serializable()> _
    Public Class AreaBO : Inherits BaseBO


#Region "Attributes"

        Private _areaId As Integer
        Private _LocationId As Integer
        Private _areaName As String

#End Region

#Region "Constructor"
        Public Sub New()
            _areaId = -1
            _LocationId = -1
            _areaName = String.Empty

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _areaId
        Public Property AreaId() As Integer
            Get
                Return _areaId
            End Get
            Set(ByVal value As Integer)
                _areaId = value
            End Set
        End Property

        ' Get / Set property for _locationId
        Public Property LocationId() As Integer
            Get
                Return _locationId
            End Get
            Set(ByVal value As Integer)
                _locationId = value
            End Set
        End Property

        ' Get / Set property for _areaName
        Public Property AreaName() As String
            Get
                Return _areaName
            End Get
            Set(ByVal value As String)
                _areaName = value
            End Set
        End Property
#End Region

    End Class

End Namespace


