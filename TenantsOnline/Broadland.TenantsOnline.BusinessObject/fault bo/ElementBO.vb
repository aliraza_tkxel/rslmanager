﻿Namespace Broadland.TenantsOnline.BusinessObject

    '' Class Name -- ElementBO
    '' Base Class -- BaseBO
    '' Created By  -- Noor Muhammad
    '' Create on -- 14th Oct,2008

    <Serializable()> _
    Public Class ElementBO : Inherits BaseBO


#Region "Attributes"

        Private _elementId As Integer
        Private _areaId As Integer
        Private _elementName As String

#End Region

#Region "Constructor"
        Public Sub New()
            _elementId = -1
            _areaId = -1
            _elementName = String.Empty

        End Sub

        Public Sub New(ByVal elementId As Integer, ByVal elementName As String)

            Me.ElementId = elementId
            Me.ElementName = elementName

        End Sub
#End Region

#Region "Properties"
        ' Get / Set property for _elementId
        Public Property ElementId() As Integer
            Get
                Return _elementId
            End Get
            Set(ByVal value As Integer)
                _elementId = value
            End Set
        End Property

        ' Get / Set property for _areaId
        Public Property AreaId() As Integer
            Get
                Return _areaId
            End Get
            Set(ByVal value As Integer)
                _areaId = value
            End Set
        End Property

        ' Get / Set property for _elementName
        Public Property ElementName() As String
            Get
                Return _elementName
            End Get
            Set(ByVal value As String)
                _elementName = value
            End Set
        End Property
#End Region

    End Class

End Namespace