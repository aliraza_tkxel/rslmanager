﻿Namespace Broadland.TenantsOnline.BusinessObject

    '' Class Name -- FaultBO
    '' Base Class -- BaseBO
    '' Created By  -- Noor Muhammad
    '' Create on -- 15th Oct,2008

    <Serializable()> _
    Public Class BreadCrumbBO : Inherits BaseBO


#Region "Attributes"

        Private _locationId As Integer
        Private _locationName As String
        Private _areaId As Integer
        Private _areaName As String
        Private _elementId As Integer
        Private _elementName As String
        Private _faultId As Integer
        Private _faultName As String
        Private _curPageIdType As String
        Private _curPageId As Integer


#End Region

#Region "Constructor"
        Public Sub New()
            _locationId = -1
            _locationName = String.Empty
            _areaId = -1
            _areaName = String.Empty
            _elementId = -1
            _elementName = String.Empty
            _faultId = -1
            _faultName = String.Empty
            _curPageIdType = String.Empty
            _curPageId = -1


        End Sub
#End Region

#Region "Properties"


        ' Get / Set property for _locationId
        Public Property LocationId() As Integer
            Get
                Return _locationId
            End Get
            Set(ByVal value As Integer)
                _locationId = value
            End Set
        End Property

        ' Get / Set property for _locationName
        Public Property LocationName() As String
            Get
                Return _locationName
            End Get
            Set(ByVal value As String)
                _locationName = value
            End Set
        End Property

        ' Get / Set property for _areaId
        Public Property AreaId() As Integer
            Get
                Return _areaId
            End Get
            Set(ByVal value As Integer)
                _areaId = value
            End Set
        End Property

        ' Get / Set property for _areaName
        Public Property AreaName() As String
            Get
                Return _areaName
            End Get
            Set(ByVal value As String)
                _areaName = value
            End Set
        End Property

        ' Get / Set property for _elementId
        Public Property ElementId() As Integer
            Get
                Return _elementId
            End Get
            Set(ByVal value As Integer)
                _elementId = value
            End Set
        End Property

        ' Get / Set property for _elementName
        Public Property ElementName() As String
            Get
                Return _elementName
            End Get
            Set(ByVal value As String)
                _elementName = value
            End Set
        End Property

        ' Get / Set property for _faultId
        Public Property FaultId() As Integer
            Get
                Return _faultId
            End Get
            Set(ByVal value As Integer)
                _faultId = value
            End Set
        End Property

        ' Get / Set property for _faultName
        Public Property FaultName() As String
            Get
                Return _faultName
            End Get
            Set(ByVal value As String)
                _faultName = value
            End Set
        End Property

        ' Get / Set property for _curPageIdType
        Public Property CurPageIdType() As String
            Get
                Return _curPageIdType
            End Get
            Set(ByVal value As String)
                _curPageIdType = value
            End Set
        End Property

        ' Get / Set property for _curPageId
        Public Property CurPageId() As Integer
            Get
                Return _curPageId
            End Get
            Set(ByVal value As Integer)
                _curPageId = value
            End Set
        End Property
#End Region



    End Class

End Namespace

