﻿Namespace Broadland.TenantsOnline.BusinessObject

    '' Class Name -- FaultBO
    '' Base Class -- BaseBO
    '' Created By  -- Noor Muhammad
    '' Create on -- 13th Oct,2008

    <Serializable()> _
    Public Class FaultBO : Inherits BaseBO


#Region "Attributes"

        Private _faultId As Integer
        Private _submitDate As DateTime
        Private _description As String
        Private _elementId As Integer
        Private _priorityId As Integer
        Private _netCost As Double
        Private _vat As Double
        Private _gross As Double
        Private _vatRateId As Integer
        Private _effectFrom As DateTime
        Private _recharge As Integer
        Private _preInspection As Integer
        Private _postInspection As Integer
        Private _stockConditionItem As Integer
        Private _faultActive As Integer
        Private _faultAction As Integer





#End Region

#Region "Constructor"
        Public Sub New()
            _faultId = -1
            _submitDate = Nothing
            _description = String.Empty
            _elementId = -1
            _priorityId = -1
            _netCost = -1
            _vat = -1
            _gross = -1
            _vatRateId = -1
            _effectFrom = Nothing
            _recharge = -1
            _preInspection = -1
            _postInspection = -1
            _stockConditionItem = -1
            _faultActive = -1
            _faultAction = -1

        End Sub

        Public Sub New(ByVal faultId As Integer, ByVal description As String)

            Me.FaultId = faultId
            Me.Description = description

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _faultId
        Public Property FaultId() As Integer
            Get
                Return _faultId
            End Get
            Set(ByVal value As Integer)
                _faultId = value
            End Set
        End Property

        ' Get / Set property for SubmitDate
        Public Property SubmitDate() As Date
            Get
                Return _submitDate
            End Get
            Set(ByVal value As Date)
                _submitDate = value
            End Set
        End Property

        ' Get / Set property for _description
        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property

        ' Get / Set property for _elementId
        Public Property ElementId() As Integer
            Get
                Return _elementId
            End Get
            Set(ByVal value As Integer)
                _elementId = value
            End Set
        End Property

        ' Get / Set property for _prorityId
        Public Property PriorityId() As String
            Get
                Return _priorityId
            End Get
            Set(ByVal value As String)
                _priorityId = value
            End Set
        End Property

        ' Get / Set property for _netCost
        Public Property NetCost() As Double
            Get
                Return _netCost
            End Get
            Set(ByVal value As Double)
                _netCost = value
            End Set
        End Property

        ' Get / Set property for _vat
        Public Property Vat() As Double
            Get
                Return _vat
            End Get
            Set(ByVal value As Double)
                _vat = value
            End Set
        End Property

        ' Get / Set property for _gross
        Public Property Gross() As Double
            Get
                Return _gross
            End Get
            Set(ByVal value As Double)
                _gross = value
            End Set
        End Property

        ' Get / Set property for  _vatRateId
        Public Property VatRateId() As Integer
            Get
                Return _vatRateId
            End Get
            Set(ByVal value As Integer)
                _vatRateId = value
            End Set
        End Property

        ' Get / Set property for _effectFrom
        Public Property EffectFrom() As Date
            Get
                Return _effectFrom
            End Get
            Set(ByVal value As Date)
                _effectFrom = value
            End Set
        End Property

        ' Get / Set property for _recharge
        Public Property Recharge() As Integer
            Get
                Return _recharge
            End Get
            Set(ByVal value As Integer)
                _recharge = value
            End Set
        End Property

        ' Get / Set property for PreInspection
        Public Property PreInspection() As Integer
            Get
                Return _preInspection
            End Get
            Set(ByVal value As Integer)
                _preInspection = value
            End Set
        End Property
        ' Get / Set property for _postInspection
        Public Property PostInspection() As Integer
            Get
                Return _postInspection
            End Get
            Set(ByVal value As Integer)
                _postInspection = value
            End Set
        End Property

        ' Get / Set property for _stockConditionItem
        Public Property StockConditionItem() As Integer
            Get
                Return _stockConditionItem
            End Get
            Set(ByVal value As Integer)
                _stockConditionItem = value
            End Set
        End Property

        ' Get / Set property for _faultActive
        Public Property FaultActive() As Integer
            Get
                Return _faultActive
            End Get
            Set(ByVal value As Integer)
                _faultActive = value
            End Set
        End Property


        ' Get / Set property for _faultAction
        Public Property FaultAction() As Integer
            Get
                Return _faultAction
            End Get
            Set(ByVal value As Integer)
                _faultAction = value
            End Set
        End Property

#End Region

    End Class

End Namespace
