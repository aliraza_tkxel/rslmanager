Imports System
Imports System.Collections


Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class ComplaintList : Inherits CollectionBase

#Region "Attributes"

        Private _exceptionGenerated As Boolean

#End Region

#Region "Constructors"

        Public Sub New()
            _exceptionGenerated = False
        End Sub

#End Region

#Region "Properties"
        Default Public Property Item(ByVal Index As Integer) As ComplaintBO
            Get
                Return CType(List.Item(Index), ComplaintBO)
            End Get
            Set(ByVal Value As ComplaintBO)
                List.Item(Index) = Value
            End Set
        End Property

        Public Property IsExceptionGenerated() As Boolean
            Get
                Return _exceptionGenerated
            End Get
            Set(ByVal value As Boolean)
                _exceptionGenerated = value
            End Set
        End Property


#End Region

#Region "Functions"

        Public Function Add(ByVal Item As ComplaintBO) As Integer
            Return List.Add(Item)
        End Function

#End Region

    End Class


End Namespace
