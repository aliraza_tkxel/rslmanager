
#Region " Options "

Option Strict On
Option Explicit On

#End Region

#Region " Imports "

Imports System
Imports System.Collections

#End Region

Namespace Broadland.TenantsOnline.BusinessObject

    <Serializable()> _
    Public Class LookUpList : Inherits List(Of LookupBO)
    End Class

End Namespace
