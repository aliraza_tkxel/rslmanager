
#Region " Options "

Option Explicit On
Option Strict On

#End Region

#Region " Imports "

Imports System
Imports Broadland.TenantsOnline.BusinessObject

#End Region

Namespace Broadland.TenantsOnline.BusinessObject

    Public Class CustomerBO : Inherits BaseBO

#Region " Constructor "

        Public Sub New()

            Me.Id = -1
            Me.TitleId = New Nullable(Of Integer)
            Me.TitleOther = String.Empty
            Me.FirstName = String.Empty
            Me.MiddleName = String.Empty
            Me.LastName = String.Empty
            Me.Email = String.Empty
            Me.Password = String.Empty
            Me.Tenancy = String.Empty
            Me.Address = Nothing
            Me.DateOfBirth = Nothing

        End Sub

#End Region

#Region " Properties "

        Public Property Id As Integer
        Public Property TitleId As Nullable(Of Integer)
        Public Property TitleOther As String
        Public Property FirstName As String
        Public Property MiddleName As String
        Public Property LastName As String
        Public Property Email As String
        Public Property Tenancy As String
        Public Property Password As String
        Public Property Address As CustomerAddressBO
        Public Property DateOfBirth As DateTime
        Public ReadOnly Property DateOfBirthDDMMYY() As String
            Get
                Return String.Format("{0}/{1}/{2}", Me.DateOfBirth.Day.ToString, Me.DateOfBirth.Month.ToString, Me.DateOfBirth.Year.ToString)
            End Get
        End Property

        Public ReadOnly Property CustomerName() As String
            Get
                Return String.Format("{0} {1}", Me.LastName, Me.FirstName)
            End Get

        End Property

#End Region

    End Class

End Namespace
