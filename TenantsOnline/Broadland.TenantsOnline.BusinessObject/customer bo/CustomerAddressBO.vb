
#Region " Options "

Option Explicit On
Option Strict On

#End Region

#Region " Imports "

Imports Broadland.TenantsOnline.BusinessObject
Imports System.Xml.Serialization

#End Region

Namespace Broadland.TenantsOnline.BusinessObject

    Public Class CustomerAddressBO : Inherits BaseBO

#Region "Construtor"

        Public Sub New()

            Me.Id = -1
            Me.Address1 = String.Empty
            Me.Address2 = String.Empty
            Me.Address2 = String.Empty
            Me.Email = String.Empty
            Me.PostCode = String.Empty
            Me.Telephone = String.Empty
            Me.TelephoneMobile = String.Empty
            Me.TelephoneMobile2 = String.Empty
            Me.HouseNumber = String.Empty
            Me.TownCity = String.Empty
            Me.County = String.Empty
            Me.TelephoneWork = String.Empty
            Me.TelephoneAlternativeContact = String.Empty
            Me.AlternativeContact = String.Empty

        End Sub

#End Region

#Region "Properties"

        Public Property Id As Integer
        Public Property Address1 As String
        Public Property Address2 As String
        Public Property Address3 As String
        Public Property Email As String
        Public Property PostCode As String
        Public Property TelephoneMobile As String
        Public Property TelephoneMobile2 As String
        <XmlElement()>
        Public Property Telephone As String
        Public Property HouseNumber As String
        Public Property County As String
        Public Property TownCity As String
        Public Property TelephoneWork As String
        Public Property TelephoneAlternativeContact As String
        Public Property AlternativeContact As String

        Public ReadOnly Property CustomerAddress() As String
            Get
                Return String.Join(" ", New String() {Me.HouseNumber, Me.Address1, Me.County, Me.PostCode})
            End Get

        End Property

#End Region

    End Class

End Namespace