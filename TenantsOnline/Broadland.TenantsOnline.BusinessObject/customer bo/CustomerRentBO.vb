'' Class Name -- CustomerRentBO
'' Base Class -- BaseBO
'' Summary -- This BO will help in displaying Rent statement 
'' Methods -- None
'' Author  -- Muanwar Nadeem

Imports System

Namespace Broadland.TenantsOnline.BusinessObject

    Public Class CustomerRentBO : Inherits BaseBO

#Region "Attributes"


        Private _tenancyID As String
        Private _startDate As String
        Private _endDate As String

        '' These values returned by sproc (ARREARS_GET_SINGLE_CUSTOMER_HB)
        Private _antHB As Double
        Private _advHB As Double

        '' This is a calculated value
        Private _housingBalance As Double
        Private _accountBalance As Decimal
        Private _estimatedHB As Decimal
        Private _toPay As Decimal
  
#End Region

#Region "Construtor"

        Public Sub New()
            _startDate = String.Empty
            _endDate = String.Empty
            _tenancyID = String.Empty

            _antHB = 0
            _advHB = 0

            _housingBalance = 0
            _accountBalance = -1

            _estimatedHB = 0
            _toPay = 0

        End Sub

#End Region

#Region "Get/Set Properties"
        ' Get / Set property for _userName
        Public Property EndDate() As String

            Get
                Return _endDate
            End Get

            Set(ByVal value As String)
                _endDate = value
            End Set

        End Property

        ' Get / Set property for _password
        Public Property StartDate() As String

            Get
                Return _startDate
            End Get

            Set(ByVal value As String)
                _startDate = value
            End Set

        End Property

        ' Get / Set property for _ANTHB
        Public Property AntHB() As String

            Get
                Return _antHB
            End Get

            Set(ByVal value As String)
                _antHB = value
            End Set

        End Property

        ' Get / Set property for _ADVHB
        Public Property AdvHB() As String

            Get
                Return _advHB
            End Get

            Set(ByVal value As String)
                _advHB = value
            End Set

        End Property

        ' Get / Set property for _accountBalance
        Public Property HousingBalance() As String

            Get
                Return _housingBalance
            End Get

            Set(ByVal value As String)
                _housingBalance = value
            End Set

        End Property


        ' Get / Set property for _tenancyID
        Public Property TenancyID() As String

            Get
                Return _tenancyID
            End Get

            Set(ByVal value As String)
                _tenancyID = value
            End Set

        End Property

        ' Get / Set property for _accountBalance
        Public Property AccountBalance() As Decimal

            Get
                Return _accountBalance
            End Get

            Set(ByVal value As Decimal)
                _accountBalance = value
            End Set

        End Property

        Public Property EstimatedHB() As Decimal

            Get
                Return _estimatedHB
            End Get

            Set(ByVal value As Decimal)
                _estimatedHB = value
            End Set

        End Property

        
        Public Property ToPay() As Decimal

            Get
                Return _toPay
            End Get

            Set(ByVal value As Decimal)
                _toPay = value
            End Set

        End Property

#End Region



    End Class

End Namespace


