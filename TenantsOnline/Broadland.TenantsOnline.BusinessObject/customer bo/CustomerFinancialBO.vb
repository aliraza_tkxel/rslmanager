'' Class Name -- CustomerFinancialBO
'' Base Class -- BaseBO
'' Summary -- Stores Customer Basic Financial Info like AccountBalance, ArrearsAction and CommentsByCRM
'' Methods -- None
'' Author  -- Muanwar Nadeem

Imports System

Namespace Broadland.TenantsOnline.BusinessObject

    Public Class CustomerFinancialBO : Inherits BaseBO

#Region "Attributes"


        '' Holds customer account/rent balance
        '' Obtained by Executing StoredProcedure Named ""
        Private _balance As Decimal

        '' Holds ArrearsAction and, Comments entered by CRM
        '' Both Obtained by executing separate StoredProcedure Named ""
        Private _arrearsAction As String
        Private _crmComments As String


#End Region

#Region "Construtor"

        Public Sub New()

            _balance = 0

            _arrearsAction = String.Empty
            _crmComments = String.Empty

        End Sub

#End Region

#Region "Properties"

#Region "Get/Set Properties"
        ' Get / Set property for _balance
        Public Property Balance() As Decimal

            Get
                Return _balance
            End Get

            Set(ByVal value As Decimal)
                _balance = value
            End Set

        End Property

        ' Get / Set property for _arrearsAction
        Public Property ArrearsAction() As String

            Get
                Return _arrearsAction
            End Get

            Set(ByVal value As String)
                _arrearsAction = value
            End Set

        End Property

#End Region

#End Region

    End Class

End Namespace