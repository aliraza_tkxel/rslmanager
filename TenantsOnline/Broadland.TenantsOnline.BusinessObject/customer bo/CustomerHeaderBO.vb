'' Class Name -- CustomerHeaderBO
'' Base Class -- BaseBO
'' Summary -- Used for Session Management, Some attributes required/displayed across web pages
'' Methods -- None
'' Author  -- Muanwar Nadeem

Imports System

Namespace Broadland.TenantsOnline.BusinessObject

    Public Class CustomerHeaderBO : Inherits BaseBO

#Region "Attributes"

        '' Holds username (i.e. email) and password supplied at the time of signin
        Private _userName As String
        Private _password As String

        '' Used when customer/tenant tries to change his/her email (userName)
        Private _newUserName As String
        Private _newPassword As String

        '' Following attributes will be used/displayed across multiple pages
        Private _title As String
        Private _firstName As String
        Private _middleName As String
        Private _lastName As String
        Private _customerID As String
        Private _tenancyID As String
        Private _sessionID As Integer


#End Region

#Region "Construtor"

        Public Sub New()

            _userName = String.Empty
            _password = String.Empty

            _newUserName = String.Empty

            _title = String.Empty
            _firstName = String.Empty
            _middleName = String.Empty
            _lastName = String.Empty
            _customerID = String.Empty
            _tenancyID = String.Empty
            _sessionID = -1

        End Sub

#End Region

#Region "Properties"

#Region "Get/Set Properties"
        ' Get / Set property for _userName
        Public Property UserName() As String

            Get
                Return _userName
            End Get

            Set(ByVal value As String)
                _userName = value
            End Set

        End Property

        ' Get / Set property for _password
        Public Property Password() As String

            Get
                Return _password
            End Get

            Set(ByVal value As String)
                _password = value
            End Set

        End Property

        ' Get / Set property for _newPassword
        Public Property NewPassword() As String

            Get
                Return _newPassword
            End Get

            Set(ByVal value As String)
                _newPassword = value
            End Set

        End Property

        ' Get / Set property for _newUserName
        Public Property NewUserName() As String

            Get
                Return _newUserName
            End Get

            Set(ByVal value As String)
                _newUserName = value
            End Set

        End Property

        ' Get / Set property for _title
        Public Property Title() As String

            Get
                Return _title
            End Get

            Set(ByVal value As String)
                _title = value
            End Set

        End Property


        ' Get / Set property for _firstName
        Public Property FirstName() As String

            Get
                Return _firstName
            End Get

            Set(ByVal value As String)
                _firstName = value
            End Set

        End Property

        ' Get / Set property for _middleName
        Public Property MiddleName() As String

            Get
                Return _middleName
            End Get

            Set(ByVal value As String)
                _middleName = value
            End Set

        End Property

        ' Get / Set property for _lastName
        Public Property LastName() As String

            Get
                Return _lastName
            End Get

            Set(ByVal value As String)
                _lastName = value
            End Set

        End Property

        ' Get / Set property for _customerID
        Public Property CustomerID() As String

            Get
                Return _customerID
            End Get

            Set(ByVal value As String)
                _customerID = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property TenancyID() As String

            Get
                Return _tenancyID
            End Get

            Set(ByVal value As String)
                _tenancyID = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property sessionId() As Integer

            Get
                Return _sessionID
            End Get

            Set(ByVal value As Integer)
                _sessionID = value
            End Set

        End Property
#End Region

#Region "Read-Only Properties"

        '' RO-Property returns FullName after concatenating frist, middle & last name
        Public ReadOnly Property FullName() As String
            Get
                Return _firstName & " " & _lastName
            End Get
        End Property


#End Region

#End Region

    End Class

End Namespace


