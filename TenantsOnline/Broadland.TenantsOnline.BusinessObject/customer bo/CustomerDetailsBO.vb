
#Region " Options "

Option Explicit On
Option Strict On

#End Region

#Region " Imports "

Imports System
Imports System.Xml.Serialization

#End Region

Namespace Broadland.TenantsOnline.BusinessObject

    Public Class CustomerDetailsBO : Inherits CustomerBO

#Region " Constructor "

        Public Sub New()

            Me.AccountBalance = -1
            Me.GasServicingAppointmentDate = Nothing

            Me.Gender = String.Empty
            Me.NINumber = String.Empty
            Me.Occupation = String.Empty

            Me.TakeHomePay = New Nullable(Of Integer)
            Me.MaritalStatusId = New Nullable(Of Integer)
            Me.EthnicityId = New Nullable(Of Integer)
            Me.ReligionId = New Nullable(Of Integer)
            Me.SexualOrientationId = New Nullable(Of Integer)
            Me.PreferedContactId = New Nullable(Of Integer)

            Me.MainLanguage = String.Empty
            Me.DisabilityIdString = String.Empty
            Me.NoOfOccupantsAbove18 = New Nullable(Of Integer)
            Me.NoOfOccupantsBelow18 = New Nullable(Of Integer)
            Me.CommunicationIdString = String.Empty
            Me.EmploymentStatusId = New Nullable(Of Integer)
            Me.EthnicityOther = String.Empty
            Me.ReligionOther = String.Empty
            Me.SexualOrientationOther = String.Empty
            Me.CommunicationOther = String.Empty
            Me.InternetAccessIdString = String.Empty
            Me.NationalityOther = String.Empty
            Me.InternetAccessOther = String.Empty
            Me.NationalityId = New Nullable(Of Integer)
            Me.BankFacilityIdString = String.Empty
            Me.BankFacilityOther = String.Empty
            Me.HouseIncomeId = New Nullable(Of Short)
            Me.EmployerName = String.Empty
            Me.EmployerAddress1 = String.Empty
            Me.EmployerAddress2 = String.Empty
            Me.EmployerTown = String.Empty
            Me.EmployerPostcode = String.Empty
            Me.ConsentToText = New Nullable(Of Short)
            Me.ConsentToEmail = New Nullable(Of Short)
            Me.DisabilityOther = String.Empty
            Me.BenefitIdString = String.Empty
            Me.LiveInCarer = New Nullable(Of Short)
            Me.LocalAuthorityId = New Nullable(Of Integer)
            Me.SupportAgencyIdString = String.Empty
            Me.SupportAgencyOther = String.Empty
            Me.Wheelchair = New Nullable(Of Short)
            Me.OwnTransport = New Nullable(Of Short)

        End Sub

#End Region

#Region " Properties "

        Public Property AccountBalance As Decimal
        Public Property GasServicingAppointmentDate As DateTime
        Public Property Gender As String
        Public Property NINumber As String
        Public Property Occupation As String
        Public Property MainLanguage As String
        Public Property DisabilityIdString As String
        Public Property CommunicationIdString As String
        Public Property EthnicityOther As String
        Public Property ReligionOther As String
        Public Property SexualOrientationOther As String
        Public Property CommunicationOther As String
        Public Property InternetAccessIdString As String
        Public Property NationalityOther As String
        Public Property InternetAccessOther As String
        Public Property BankFacilityIdString As String
        Public Property BankFacilityOther As String
        Public Property EmployerName As String
        Public Property EmployerAddress1 As String
        Public Property EmployerAddress2 As String
        Public Property EmployerTown As String
        Public Property EmployerPostcode As String
        Public Property DisabilityOther As String
        Public Property BenefitIdString As String
        Public Property SupportAgencyIdString As String
        Public Property SupportAgencyOther As String

        <XmlIgnore()>
        Public Property VulnerabilityDataReader As IDataReader

        Public Property MaritalStatusId As Nullable(Of Integer)
        Public Property EthnicityId As Nullable(Of Integer)
        Public Property EmploymentStatusId As Nullable(Of Integer)
        Public Property ReligionId As Nullable(Of Integer)
        Public Property SexualOrientationId As Nullable(Of Integer)
        Public Property PreferedContactId As Nullable(Of Integer)
        Public Property NoOfOccupantsAbove18 As Nullable(Of Integer)
        Public Property NoOfOccupantsBelow18 As Nullable(Of Integer)

        Public Property NationalityId As Nullable(Of Integer)
        Public Property LocalAuthorityId As Nullable(Of Integer)
        Public Property TakeHomePay As Nullable(Of Integer)

        Public Property HouseIncomeId As Nullable(Of Short)
        Public Property ConsentToText As Nullable(Of Short)
        Public Property ConsentToEmail As Nullable(Of Short)
        Public Property LiveInCarer As Nullable(Of Short)
        Public Property Wheelchair As Nullable(Of Short)
        Public Property OwnTransport As Nullable(Of Short)

        Public Property Consent As Nullable(Of Boolean)

#End Region

    End Class

End Namespace