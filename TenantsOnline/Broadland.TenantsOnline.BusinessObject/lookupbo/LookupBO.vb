Imports System


Namespace Broadland.TenantsOnline.BusinessObject

    '' This class will hold Lookup Ids, names and values
    <Serializable()> _
    Public Class LookupBO : Inherits BaseBO

#Region "Attributes"

        Private _value As Integer
        Private _name As String

#End Region

#Region "Constructors"

        Public Sub New(ByVal val As Integer, ByVal name As String)

            Me.LookUpValue = val
            Me.LookUpName = name

        End Sub

#End Region

#Region "Properties"

        Public Property LookUpValue() As Integer
            Get
                Return _value
            End Get
            Set(ByVal value As Integer)
                _value = value
            End Set
        End Property

        Public Property LookUpName() As String
            Get
                Return _name
            End Get
            Set(ByVal value As String)
                _name = value
            End Set
        End Property

#End Region

    End Class


End Namespace

