Imports System

Imports System.Web
Imports System.net.Mail
Imports System.web.Configuration
Imports System.Configuration
Imports System.Net.Mime
Imports System.Web.HttpContext
Imports System.IO
Imports System.Globalization



Public Class UtilityFunctions

#Region "Functions"

#Region "Conversion functions"

    ''This utility method converts the given string value into DateTime
    Public Shared Function stringToDate(ByVal str As String) As DateTime

        Dim MyDateTime As DateTime = New DateTime()
        Return DateTime.ParseExact(str, "dd/MM/yyyy", Nothing)


        Return Nothing

    End Function

    ''this utility function appends ENQ0000 with equiryID
    Public Shared Function GetEnquiryString(ByVal enquiryID As String) As String
        Dim tempEnqry As String = enquiryID.ToString()
        Dim i As Integer
        Dim tmpStr As String = String.Empty
        For i = 0 To 5 - tempEnqry.Length
            tmpStr = tmpStr & "0"
        Next i
        Return "ENQ" & tmpStr & tempEnqry
    End Function

    'this utility function joins firstname and lastname
    Public Shared Function FormatName(ByVal firstName As String, ByVal lastName As String) As String
        If String.IsNullOrEmpty(firstName) = True Then
            Return lastName
        Else
            Return Trim(firstName)(0) & " " & lastName
        End If
    End Function

    'this utility function joins firstname and lastname
    Public Shared Function GetFullName(ByVal firstName As String, ByVal lastName As String) As String
        Return firstName & " " & lastName
    End Function

    'this utility functions converts date from mm/dd/yyyy formate to dd/mm/yyyy
    Public Shared Function FormatDate(ByVal str As String) As String        
        If Not str.Equals("") And Not (str Is Nothing) Then
            Dim strArr As String() = str.Split(" ")
            Return Convert.ToDateTime(strArr(0)).ToString("dd/MM/yyyy")
        Else
            Return ("")
        End If
    End Function

   


    'this utility function joins Location, Area and Element
    Public Shared Function FormatLocation(ByVal locationName As String, ByVal areaName As String, ByVal elementName As String) As String
        Return locationName & "_" & areaName & "_" & elementName
    End Function


    'this utility function Converts the values of true false into active, inactive
    Public Shared Function FormatPriorityReponse(ByVal responseTime As String, ByVal type As String) As String
        Return responseTime & " " & type

    End Function

    'this utility function joins Area and Element
    Public Shared Function FormatAreaElement(ByVal areaName As String, ByVal elementName As String) As String
        Return areaName & "/" & elementName
    End Function

    'this utility functions converts Duedate from mm/dd/yyyy formate to dd/mm/yyyy
    Public Shared Function FormatDueDate(ByVal str As String) As String
        If Not str.Equals("") And Not (str Is Nothing) Then
            Dim strArr As String() = str.Split(" ")
            Return Convert.ToDateTime(strArr(0)).ToString("dd/MM/yyyy")
        Else
            Return ("TBA")
        End If
    End Function
    'Function for date comparison : date format should be dd/MM/yyyy
    Public Shared Function IsDateTimeLessThanCurrentDateTime(ByVal dateToCompare As String, ByVal hour As Integer, ByVal min As Integer, ByVal sec As Integer)
        Try
            If dateToCompare <> "" Then

                Dim splitDate(6) As String
                splitDate = Split(dateToCompare, "/")

                Dim d As DateTime = New DateTime(splitDate(2), splitDate(1), splitDate(0))
                Dim B As DateTime = DateTime.Now()
                Dim comparison = DateTime.Today.Subtract(d).Days()

                If comparison > 0 Then
                    Return True
                Else
                    Return False
                End If

            End If
            Return True
        Catch ex As Exception
            Return True
        End Try
    End Function

#End Region

#End Region

End Class
