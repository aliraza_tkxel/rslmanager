Imports System


Namespace Broadland.TenantsOnline.Utilities

    Public Class SprocNameConstants

#Region "ViewDetails"

        ' '' Executed for Retrieving data for view screen
        Public Shared GetCustomerInfo As String = "TO_CUSTOMER_GETCUSTOMERINFO"
        Public Shared GetCustomerAccountBalance As String = "TO_F_RENTJOURNAL_CALCULATEACCOUNTBALANCE"
        Public Shared GetServicingAppointmentDate As String = "TO_CUSTOMER_GASSERVICINGAPPOINTMENTDATE"

#End Region   
        
#Region "EnquiryLogStoredProcedures"

        ''Customer ENQUIRYLOG 
        Public Shared GetInvoiceSearchRowCount = "FL_CO_INVOICING_RowCount"
        Public Shared GetRowCount As String = "TO_GETROWCOUNT"
        Public Shared EnquiryLogSelectAllList As String = "TO_ENQUIRY_LOG_SELECTLIST"
        Public Shared EnquiryLogSearch As String = "TO_ENQUIRY_LOG_SEARCHENQUIRY"

        Public Shared CustomerResponseSelectAllList As String = "TO_ENQUIRY_LOG_SELECTRESPONSELIST"
        Public Shared CustomerResponseSearch As String = "TO_ENQUIRY_LOG_SEARCHRESPONSE"

        Public Shared GetCustomerAddressTermination As String = "TO_TERMINATION_C_ADDRESS_VIEWTERMINATION"
        Public Shared GetCustomerAddressComplaint As String = "TO_COMPLAINT_ENQUIRYLOG_VIEWCOMPLAINT"
        Public Shared GetCustomerAddressASB As String = "TO_ASB_ENQUIRYLOG_VIEWASB"
        Public Shared GetCustomerAddressGarageParking As String = "TO_GARAGEPARKING_ENQUIRYLOG_REQUESTGARAGEPARKING"
        Public Shared GetCustomerAddressAbandon As String = "TO_ABANDON_ENQUIRYLOG_REPORTABANDON"
        Public Shared GetCustomerAddressTransfer As String = "TO_HOUSE_MOVE_ENQUIRYLOG_REQUESTTRANSFER"
        Public Shared DeleteCustomerEnquiry As String = "TO_ENQUIRY_LOG_DELETE"
        Public Shared DeleteCustomerEnquiryResponse As String = "TO_ENQUIRY_LOG_DELETECUSTOMERRESPONSE"
        Public Shared GetSearchRowCount As String = "TO_ENQUIRY_LOG_COUNTSEARCHRESULTS"
        Public Shared GetResponseSearchRowCount As String = "TO_ENQUIRY_LOG_RESPONSECOUNTSEARCHRESULTS"
        Public Shared GetCustomerIDByEnquiryLog As String = "TO_ENQUIRY_LOG_GET_CUSTOMERID"
        Public Shared GetCustomerNature As String = "TO_ENQUIRY_LOG_GET_C_REPORT_NATURE"
        Public Shared GetCustomerStatus As String = "TO_ENQUIRY_LOG_GET_C_REPORT_STATUS"

        Public Shared GetClearArrearInfo As String = "TO_COMPLAINT_C_ADDRESS_GET_CLEARARREARS"
        Public Shared GetDirectDebitRentInfo As String = "TO_COMPLAINT_C_ADDRESS_GET_DIRECTDEBITRENT"
        Public Shared GetCstRequestInfo As String = "TO_CST_REQUEST_ENQUIRYLOG_GETCSTREQUEST"

        Public Shared CreatCustomerResponseJournal As String = "TO_CUSTOMER_RESPONSE_CREATEJOURNAL"


#End Region

#Region "LookUpStoredProcedures"

        ' ''Broadland enquiry nature lookup stored procedure
        Public Shared getNatureLookup As String = "TO_C_NATURE_GetNatureLookup"

        ' ''Broadland enquiry status lookup stored procedure
        Public Shared getStatusLookup As String = "TO_C_STATUS_GetStatusLookup"

        ' ''Broadland fault status lookup stored procedure
        Public Shared getReportedFaultStatusLookup As String = "FL_GETFAULTSTATUS"

        ' ''Broadland fault status lookup for fault log stored procedure
        Public Shared getFaultStatusLookupForFaultLog As String = "FL_GET_FAULT_STATUS_FOR_FAULT_LOG"

        ' ''Broadland fault status lookup for contractor portal job sheet stored procedure
        Public Shared getJobSheetFaultStatusLookup As String = "FL_CO_GET_JOB_SHEET_FAULT_STATUS"

        ' ''Broadland contractor lookup stored procedure
        Public Shared getContractorLookup As String = "FL_CONTRACTOR_GETLOOKUP"
        'Public Shared getUserLookUp As String = "FL_USER_GETLOOKUP"
#End Region

#Region "Fault Location Procedure Constants"
        Public Shared CheckGasServicingAppointments As String = "FL_Customer_GasServicingAppointmentDate"
        Public Shared CheckPropertyReportedFaults As String = "FL_CHECK_PROPERTY_REPORTED_FAULTS"
        Public Shared GetLocationName As String = "FL_GET_LOCATION_NAME"
        Public Shared GetVatRate As String = "FL_VAT_GETLOOKUP"
        Public Shared GetElements As String = "FL_GET_ELEMENTS"
        Public Shared GetFaults As String = "FL_GET_FAULTS"
        Public Shared GetLocationLookup As String = "FL_LOCATION_GETLOOKUP"
        Public Shared GetPriorityLookup As String = "FL_FAULT_PRIORITY_GETLOOKUP"
        Public Shared GetAreaLookup As String = "FL_AREA_GETLOOKUP"
        Public Shared GetTblAllAreasLookup As String = "FL_TBL_ALL_AREAS_GETLOOKUP"
        Public Shared GetElementLookup As String = "FL_ELEMENT_GETLOOKUP"
        Public Shared GetTblItemGetLookup As String = "FL_TBL_ITEM_GETLOOKUP"
        Public Shared GetParameterLookup As String = "FL_TBL_PARAMETER_GETLOOKUP"
        Public Shared GetFaultSerachRecords As String = "FL_FAULT_SEARCHFAULT"
        Public Shared GetFaultSearchRowCount As String = "FL_FAULT_COUNTSEARCHRESULTS"
        Public Shared FaultSelectAllList As String = "FL_FAULT_SEARCHLIST"
        Public Shared FaultGetPriorityResponse As String = "FL_FAULT_PRIORITY_RESPONSE"
        Public Shared CreatNewFault As String = "FL_FAULT_CREATE"
        Public Shared GetFaultValues As String = "FL_FAULT_GETVALUES"
        Public Shared AmendFault As String = "FL_FAULT_AMEND"
        Public Shared AmendElementName As String = "FL_ELEMENT_AMENDNAME"
        Public Shared GetVatValues As String = "FL_VAT_GETVALUES"
        Public Shared GetFaultTransactionLogSearch As String = "FL_FAULT_TRANSACTION_SEARCHLOG"
        Public Shared GetFaultTransactionLogSearchRowCount As String = "FL_FAULT_TRANS_COUNTSEARCHRESULTS"
        Public Shared GetTeamLookup As String = "FL_TEAM_GETLOOKUP"
        Public Shared GetPreInspectionTeamLookup As String = "FL_PREINSPECTION_TEAM_GETLOOKUP"
        Public Shared GetPostInspectionTeamLookup As String = "FL_POSTINSPECTION_TEAM_GETLOOKUP"
        Public Shared GetUserLookup As String = "FL_USER_GETLOOKUP"
        Public Shared GetFaultPriorities As String = "FL_PRIORITY_GETVALUES"
        Public Shared AddAmendFaultPriorities As String = "FL_FAULT_PRIORITY_ADDAMEND"
        Public Shared GetFaultPriorityRowCount As String = "FL_PRIORITY_ROWCOUNT"
        Public Shared GetFaultPriorityRecord As String = "FL_PRIORITY_GETRECORD"
        Public Shared GetFaultDescriptionLookup As String = "FL_FAULTDESCRIPTION_GETLOOKUP"
        Public Shared GetPricingControlData As String = "FL_PRICINGCONTROL_GETDATA"
        Public Shared AddPricingControlData As String = "FL_ADD_PRICINGCONTROLDATA"

#Region "Sproc names from TO"
        ' Public Shared GetLocationName As String = "FL_GET_LOCATION_NAME"
        'Public Shared GetElements As String = "FL_GET_ELEMENTS"
        'Public Shared GetFaults As String = "FL_GET_FAULTS"
        Public Shared GetCustomerFaults As String = "FL_FAULT_LOG_GET_CUSTOMER_Faults"
        Public Shared GetFaultQuestion As String = "FL_GET_FAULT_QUESTION"
        Public Shared GetFaultRechargeCost As String = "FL_GET_FAULT_RECHARGE_COST"
        Public Shared SaveFaultTemporarily As String = "FL_SAVE_FAULT_TEMPORARILY"
        Public Shared TempFaultBasket As String = "FL_TEMP_FAULT_BASKET"
        Public Shared DeleteTempFault As String = "FL_DELETE_TEMP_FAULT"
        Public Shared saveFinalFaultBasket As String = "FL_SAVE_FINAL_FAULT_BASKET"
        Public Shared GetCustomerContactDetail As String = "FL_GET_CUSTOMER_CONTACT_DETAIL"
        Public Shared UpdateCustomerContactDetail As String = "FL_UPDATE_CUSTOMER_CONTACT_DETAIL"
        Public Shared UpdateFaultQuantity As String = "FL_UPDATE_FAULT_QUANTITY"
        Public Shared GetCustomerUnreadFaultsCount As String = "FL_FAULT_GetCustomerUnreadFaultsCount"
        Public Shared SaveSQuestions As String = "FL_FAULT_SAVESQUESTIONS"
        Public Shared LoadBreadCrumb As String = "FL_LOAD_BREADCRUMB"
        Public Shared getFaultStatusLookup As String = "FL_STATUS_GetFaultStatusLookup"

#End Region


#End Region

#Region "Fault Locator Procedure Constants"
        'Public Shared GetContractorLookup As String = "FL_CONTRACTOR_GETLOOKUP"
        Public Shared AddFaultJournal As String = "FL_FAULT_JOURNAL_ADD"
        Public Shared GetCustomerInfoForFault As String = "FL_GET_CUSTOMERINFO"
        Public Shared AddFaultRepairHistory As String = "FL_REPAIRHISTORY_ADD"
        Public Shared UpdateFaultContractor As String = "FL_UPDATE_FAULT_CONTRACTOR"
        Public Shared SaveFinalFaultBasketWithJournal As String = "FL_SAVE_FINAL_FAULT_BASKET_JOURNAL"
        Public Shared ReportedFaultReport As String = "FL_REPORTEDFAULT_REPORT"
        Public Shared ReportedFaultReportExport As String = "FL_REPORTEDFAULT_REPORT_EXPORT"
        Public Shared ReportedFaultSearch As String = "FL_REPORTEDFAULTSEARCH"
        Public Shared ReportedFaultSearchList As String = "FL_REPORTEDFAULTSEARCHLIST"
        Public Shared AddInspectionRecord As String = "FL_ADDINSPECTIONRECORD"
        Public Shared AddPreInspectionReportData As String = "FL_ADD_PREINSPECTION_REPORT_POPUP_DATA"
        Public Shared GetCustomerReportedFaults As String = "FL_GET_CUSTOMER_REPORTED_FAULTS"
        Public Shared GetFaultRepairList As String = "FL_GET_FAULT_REPAIR_LIST"
        Public Shared GetTempFaultValues As String = "FL_FAUTLOG_GETVALUES"
        Public Shared GetPreinspectionPopupData As String = "FL_FAUTLOG_PREINSPECTION_POPUP_DATA"
        Public Shared GetPostInspectionPopupData As String = "FL_CO_POSTINSPECTION_POPUP_DATA"
        Public Shared GetPreinspectionPopupDueDate As String = "FL_GET_DUEDATE"
        Public Shared GetPreinspectionReportedDueDate As String = "FL_GeT_PreInspection_DueDate"
        Public Shared AddInspectionRecordWithContractor As String = "FL_ADD_PREINSPECTION_TEMP"
        Public Shared AddNewRepair As String = "FL_ADD_NEW_REPAIR"
        Public Shared GetRepairValues As String = "FL_REPAIR_GETVALUES"
        Public Shared AmendRepair As String = "FL_AMEND_REPAIR"
        Public Shared GetFaultLocatorRowCount As String = "FL_REPORTEDFAULTSEARCH_RowCount"
        Public Shared GetReportedFaultReportRowCount As String = "FL_REPORTEDFAULT_Report_RowCount"
        Public Shared GetFaultBasketUpdateData As String = "FL_GET_FAULT_BASKET_UPDATE"
        Public Shared UpdateFaultLogContractor As String = "FL_UPDATE_CONTRACTOR"
        Public Shared GetJSSummarytoUpdateFaultLog As String = "FL_GET_JS_SUMMARY_UPDATE_DATA"
        Public Shared GetJSAsbestosSummarytoUpdateFaultLog As String = "FL_GET_JS_SUMMARY_UPDATE_ASBESTOS_DATA"
        Public Shared GetAsbestosSummarytoPropertyId As String = "FL_GET_JS_SUMMARY_ASBESTOS_DATA"
        Public Shared GetPostInspectionReport As String = "FL_CO_POSTINSPECTION_REPORT"
        Public Shared GetPostInspectionReportRowCount As String = "FL_CO_POSTINSPECTION_ROWCOUNT"
        Public Shared GetFaultforPostInspection As String = "FL_FAUTLOG_GETVALUES_UPDATE"
        Public Shared AddPostInspectionRecord As String = "FL_ADD_POSTINSPECTION_INFO"



#End Region

#Region "Contractor Portal constant"
        Public Shared GetPatchLookup As String = "FL_GETPATCHLOOKUP"
        Public Shared GetSchemeLookup As String = "FL_GETSCHEMELOOKUP"
        Public Shared GetInvoicingGridResult As String = "FL_CO_INVOICING_SEARCHRESULT"
        Public Shared GetJobSheetData As String = "FL_CO_GET_JOB_SHEET_DATA"
        Public Shared GetJobSheetFaultCount As String = "FL_CO_GET_COUNT_JOB_SHEET_DATA"
        Public Shared GetUserInvoicingLookUp As String = "FL_CO_USER_GETLOOKUP"
        Public Shared UpdateSelectedJobSheetFaults As String = "FL_CO_UPDATE_SELECTED_JOB_SHEET_FAULTS"
        Public Shared GetMonitoringSearchPanelData As String = "FL_CO_GET_MONITORING_SEARCH_PANEL_DATA"
        Public Shared GetMonitoringTimeBaseData As String = "FL_CO_GET_MONITORING_TIME_BASE_DATA"
        Public Shared GetMonitoringTimeBaseFaultCount As String = "FL_CO_GET_MONITORING_TIME_BASE_FAULT_COUNT"
        Public Shared GetInvoicingStageLookUp As String = "FL_GETSTAGELOOKUP"
        Public Shared GetUserInvoiceLookUp As String = "FL_CO_USER_GETLOOKUP"
        Public Shared GetInvoicePageResult = "FL_CO_INVOICE_SEARCH"
        Public Shared GetStageLookup As String = "FL_GETSTAGELOOKUP"
        Public Shared GetLetterLookup As String = "FL_GETLETTERLOOKUP"
        Public Shared GetOperativeLookup As String = "FL_GETOPERATIVELOOKUP"
        Public Shared GetManageAppointmentSearchRowCount As String = "FL_CO_MANAGEAPPOINTMENT_COUNTSEARCHRESULTS"
        Public Shared GetWorkCompletionSearchRowCount As String = "FL_CO_WORK_COMPLETION_COUNT_SEARCH_RESULTS"
        Public Shared GetManageAppointmentSearchRecords As String = "FL_CO_GET_MA_SEARCHRECORDS"
        Public Shared UpdateManageAppointmentRecord As String = "FL_CO_UPDATEMANAGEAPPOINTMENTRECORD"
        Public Shared GetWorkCompletionSearchRecords As String = "FL_CO_GETWCSEARCHRECORDS"
        Public Shared Get_User_Invoicing_LookUp As String = "FL_CO_USER_GETLOOKUP"
        Public Shared GetRepairData As String = "FL_CO_GETREPAIRDATA"
        Public Shared GetVatRateValue As String = "FL_CO_GETVATRATEVALUE"
        Public Shared SaveRepairRecord As String = "FL_CO_SAVE_REPAIR_RECORD"
        Public Shared GetRepairRecord As String = "FL_CO_Get_REPAIR_RECORD"
        Public Shared SaveRepairAgainstFault As String = "FL_CO_SAVE_REPAIR_AGAINST_FAULT"
        Public Shared SaveInvoicingRecord As String = "FL_CO_SAVE_INVOICING_RECORD"
        Public Shared SaveBatchInvoicing As String = "FL_CO_SAVE_BATCH_INVOICING"
        Public Shared GetTBASearch As String = "FL_CO_FAULTLOG_SEARCH"
        Public Shared GetTBASearchRowCount As String = "FL_CO_FAULTLOG_COUNTSEARCHRESULTS"
        Public Shared SaveAppointmentTBA As String = "FL_CO_SAVE_APPOINTMENT"
        Public Shared GetTBAUserLookup As String = "FL_USERTBA_GETLOOKUP"
        Public Shared GetTBAStatusLookup As String = "FL_GET_FAULT_STATUS_FOR_JOB_SHEET"
        Public Shared GetTBASearchNew As String = "FL_CO_TBA_SEARCH"
        Public Shared GetTBACustomerVulnerability As String = "TO_CUSTOMER_VULNERABILITY"
        Public Shared GetTBACustomerRisk As String = "FL_CUSTOMER_RISK"
        Public Shared GetTBAGasServicingData As String = "FL_Customer_GasServicingAppointmentDate"
        Public Shared GetTBACommunicationData As String = "FL_CUSTOMER_GETCOMMUNICATIONINFO"
        Public Shared GetTBASearchRowCountNew As String = "FL_CO_TBA_COUNTSEARCHRESULTS"
        Public Shared GetPreInspectionReport As String = "FL_PREINSPECTION_REPORT"
        Public Shared GetJSSummaryPopupData As String = "FL_GET_JS_SUMMARY_POPUP_DATA"
        Public Shared GetJSSummaryPopupAsbestosData As String = "FL_GET_JS_SUMMARY_POPUP_ASBESTOS_DATA"
        Public Shared GetPreInspectionReportRowCount As String = "FL_PREINSPECTION_REPORT_ROWCOUNT"
        Public Shared GetReactiveRepairDATA As String = "FL_CO_REACTIVEREPAIR_GRID"
        Public Shared GetPropertyReactiveRepairData As String = "FL_CO_PROPRTY_REACTIVEREPAIR_GRID_DATA"
        Public Shared GetReactiveRepairJournalDATA As String = "FL_CO_REACTIVEREPAIR_JOURNAL"        
        Public Shared SaveReactiveRepairJournalInspection As String = "FL_CO_SAVE_REACTIVE_REPAIR_JOURNAL_CONTRACTOR"
        Public Shared ReactiveRepairNatureGrid As String = "FL_CO_REACTIVE_REPAIR_NATURE_GRID"
        Public Shared GetCancelledJobReportData As String = "FL_CANCELLED_FAULTS_SEARCH"
        Public Shared GetCancelledJobReportRowCount As String = "FL_CANCELLED_FAULTS_ROWCOUNT"
        Public Shared UpdateCancelReportGrid As String = "FL_CANCEL_REPORT_GRID"
        Public Shared SaveAppointment As String = "FL_SAVE_PREINSPECTION_APPOINTMENT"
        Public Shared IsFaultDisApprovedByContractor As String = "FL_IS_FAULT_DISAPPROVED_BY_CONTRACTOR"
        Public Shared IsAllowedTOCancelORAssginContractor As String = "FL_CAN_FAULT_CANCELLED_OR_ASSIGNED"
        Public Shared UpdatePostInspectionPopUp As String = "FL_ADD_POSTINSPECTION_INFO_UPDATE"
#End Region

    End Class


End Namespace

