Public Class FaultConstants
    '' constant for customer faults

    ''Fault page URL 
    Public Shared DisplayFaultList As String = "../../display_fault_list.aspx"    
     '
    Public Shared SelectedFaultId As String = "UserSelectedFaultId"
    Public Shared SelectedElementId As String = "UserSelectedElementId"
    Public Shared SelectedAreaId As String = "UserSelectedAreaId"
    Public Shared TotalReportedFaults As String = "TotalReportedFaults"
    Public Shared BreadCrumbLoactionName As String = "BreadCrumbLoactionName"
    Public Shared BreadCrumbLoactionUrl As String = "BreadCrumbLoactionUrl"
    Public Shared BreadCrumbAreaName As String = "BreadCrumbAreaName"
    Public Shared BreadCrumbAreaUrl As String = "BreadCrumbAreaUrl"
    Public Shared Internal As String = "Internal"
    Public Shared External As String = "External"
    Public Shared LoactionUrlString As String = "~/secure/faultlocator/report_fault.aspx?customerid="
    Public Shared FaultListUrlString As String = "~/secure/faultlocator/display_fault_list.aspx?elementId="
    Public Shared WCRepairDataSet As String = "WCRepairDataSet"
    Public Shared WCFaultLogId As String = "WCFaultLogId"
    Public Shared WCTotalGrossValue As String = "WCTotalGrossValue"
    Public Shared ExceptionMessage As String = "ExceptionMessage"
    Public Shared ElementImagesBackButtonUrl As String = "~/secure/faultlocator/report_fault.aspx"






End Class
