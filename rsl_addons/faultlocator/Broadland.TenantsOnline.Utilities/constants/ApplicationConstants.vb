
Public Class ApplicationConstants

    '' constant for customer enquiry management
    '' number of rows to be fetched from db at one time before next qury
    Public Shared CustEnquiryManagRowCount As Integer = 10
    ''SortBy column 
    Public Shared CustEnquiryManagSortBy As String = "EnquiryLogID"
    Public Shared CustFaultManagSortBy As String = "FaultID"
    Public Shared CustInvoiceManagSortBy As String = "FL_FAULT_LOG.JobSheetNumber"
    Public Shared FaultTransactionLogSortBy As String = "FAULTTRANSLOGID"
    Public Shared FaultPrioritySortBy As String = "PRIORITYID"
    Public Shared ReportedFaultManagSortBy As String = "FL_FAULT_LOG.FaultLogID"
    Public Shared PreInspectionSortBy As String = "FL_FAULT_LOG.FaultLogID"
    Public Shared ContractorPortalJobSheetSortBy As String = "FL_FAULT_LOG.FaultLogID"
    Public Shared ManageAppointmentManagSortBy As String = "FL_FAULT_LOG.FaultLogID"
    Public Shared AppointmentTBASortBy As String = "FAULTLOGID"
    Public Shared MonitoringSortBy As String = "FL_FAULT_LOG.FaultLogID"
    Public Shared ContractorPortalFaultReportsSortBy As String = "FAULTLOGID"
    Public Shared PostInspectionReportSortBy As String = "FL_FAULT_LOG.FaultLogId"
    Public Shared JobSheetSortBy As String = "FL_FAULT_LOG.FaultLogID"
    Public Shared CancelledJSSortBy As String = "FL_FAULT_LOG.FaultLogId"
    'Public Shared ReactiveRepairSortBY As String = "FL_FAULT_LOG.FaultLogId"
    Public Shared ReactiveRepairSortBY As String = "FL_FAULT_STATUS.FaultStatusId"
    ''SortOrder
    Public Shared CustEnquiryManagDESCSortOrder As String = "DESC"
    Public Shared CustEnquiryManagASECSortOrder As String = "ASC"
    Public Shared CustFaultManagDESCSortOrder As String = "DESC"
    Public Shared InvoiceManageDESCSortOrder As String = "DESC"
    Public Shared CustFaultManagASECSortOrder As String = "ASC"
    Public Shared InvoiceManageASECSortOrder As String = "ASC"
    Public Shared FaultTransactionLogASECSortOrder As String = "ASC"
    Public Shared FaultTransactionLogDESCSortOrder As String = "DESC"
    Public Shared FaultPriorityASECSortOrder As String = "ASC"
    Public Shared FaultPriorityDESCSortOrder As String = "DESC"
    Public Shared ReportedFaultDESCSortOrder As String = "DESC"
    Public Shared ReportedFaultASCESortOrder As String = "ASC"

    Public Shared PreInspectionDESSortOrder As String = "DESC"
    Public Shared PreInspectionASCSortOrder As String = "ASC"

    Public Shared ContractorPortalJobSheetSortOrder As String = "ASC"
    Public Shared ManageAppointmentDESCSortOrder As String = "DESC"
    Public Shared ManageAppointmentASECSortOrder As String = "ASC"
    Public Shared AppointmentTBADESCSortOrder As String = "DESC"
    Public Shared AppointmentTBAASCESortOrder As String = "ASC"
    Public Shared ContractorProtalFaultReportsSortOrder As String = "ASC"

    Public Shared JobSheetDesSortOrder As String = "DESC"
    Public Shared JobSheetAscSortOrder As String = "ASC"

    Public Shared MonitoringDesSortOrder As String = "DESC"
    Public Shared MonitoringAscSortOrder As String = "ASC"

    Public Shared CancelledJSDESCSortOrder As String = "DESC"
    Public Shared CancelledJSASCSortOrder As String = "ASC"

    Public Shared ReactiveRepairJournalDESCSortOrder As String = "DESC"
    Public Shared ReactiveRepairJournalASCSortOrder As String = "ASC"

    Public Shared PostInspectionDESSortOrder As String = "DESC"
    Public Shared PostInspectionASCSortOrder As String = "ASC"


    '' mod number
    Public Shared CustEnquiryManagModNumber As Integer = 1
    ''constant for initial page index
    Public Shared CustEnquiryInitialPageIndex As Integer = 0
    Public Shared CustFaultInitialPageIndex As Integer = 0
    Public Shared CustInvoiceInitialPageIndex As Integer = 0
    Public Shared FaultTransactionLogInitialPageIndex As Integer = 0
    Public Shared FaultPriorityInitialPageIndex As Integer = 0
    Public Shared ReportedFaultInitialPageIndex As Integer = 0
    Public Shared PreInspectionInitialPageIndex As Integer = 0
    Public Shared ContractorPortalJobSheetIntialPageIndex As Integer = 0
    Public Shared ContractorPortalMonitoringInitialPageIndex As Integer = 0
    Public Shared ManageAppointmentInitialPageIndex As Integer = 0
    Public Shared AppointmentTBAInitialPageIndex As Integer = 0
    Public Shared ContractorPortalFaultReportsIntialPageIndex As Integer = 0
    Public Shared PostInspectionReportInitialPageIndex As Integer = 0
    Public Shared PostInspectionReportInitialPageIndexKey As String = "PostInspectionPageSize"
    Public Shared JobSheetInitialPageIndex As Integer = 0
    Public Shared CancelledJSInitialPageIndex As Integer = 0


    ''constant for initial page index
    Public Shared CustResponseInitialPageIndex As Integer = 0

    '' results per page
    Public Shared CustEnquiryManagResultPerPage As Integer = 10
    Public Shared CustFaultManagResultPerPage As Integer = 10
    Public Shared FaultTransactionLogResultsPerPage As Integer = 10
    Public Shared FaultPriorityResultsPerPage As Integer = 10
    Public Shared ReportedFaultManagResultPerPage As Integer = 10
    Public Shared ReportedFaultFinanceResultPerPage As Integer = 15
    Public Shared PreInspectionResultPerPage As Integer = 10
    Public Shared ContractorPortalJobSheetResultPerPage As Integer = 10
    Public Shared InvoiceSearchResultPerPage As Integer = 1000
    Public Shared ManageAppointmentManagResultPerPage As Integer = 10
    Public Shared AppointmentTBAManagResultPerPage As Integer = 10
    Public Shared ContractorPortalFaultReportsResultPerPage As Integer = 10
    Public Shared PostInspectionReportResultPerPage As Integer = 10
    Public Shared JobSheetResultPerPage As Integer = 10
    Public Shared CancelledJSResultsPerPage As Integer = 10
    Public Shared MonitoringResultPerPage As Integer = 10

    '''''''''''''''''
    Public Shared CustResponseResultPerPage As Integer = 10
    '''''''''''''''''
    ''the name for page index
    Public Shared ContractorProtalJobSheetPageIndexKey As String = "ContractorProtalJobSheetPageIndex"
    Public Shared ContractorProtalTBAPageIndexKey As String = "ContractorProtalTBAPageIndex"
    Public Shared ContractorProtalMAPageIndexKey As String = "ContractorProtalMAPageIndex"
    Public Shared ContractorProtalWCPageIndexKey As String = "ContractorProtalWCPageIndex"
    Public Shared ContractorProtalINVPageIndexKey As String = "ContractorProtalINVPageIndex"
    Public Shared ContractorProtalFaultReportsPageIndexKey As String = "ContractorProtalFaultReportsPageIndex"
    Public Shared PostInspectionReportsPageIndexKey As String = "PostInsepctionReportsPageIndex"
    Public Shared ReportedFaultInitialPageIndexKey As String = "ReportedFaultInitialPageIndex"
    Public Shared CancelledJSInitialPageIndexKey As String = "CancelledJSInitialPageIndex"

    ''the name for page size
    Public Shared ContractorProtalJobSheetPageSizeKey As String = "ContractorProtalJobSheetPageSize"
    Public Shared ContractorProtalTBAPageSizeKey As String = "ContractorProtalTBAPageSize"
    Public Shared ContractorProtalMAPageSizeKey As String = "ContractorProtalMAPageSize"
    Public Shared ContractorProtalWCPageSizeKey As String = "ContractorProtalWCPageSize"
    Public Shared ContractorProtalINVPageSizeKey As String = "ContractorProtalINVPageSize"
    Public Shared ReportedFaultPageSizeKey As String = "ReportedFaultInitialPagePageSize"
    Public Shared ContractorProtalFaultReportsPageSizeKey As String = "ContractorProtalFaultReportsPageSize"
    Public Shared PostInspectionReportsPageSizeKey As String = "PostInspectionReportsPageSize"
    Public Shared CancelledJSPageSizeKey As String = "CancelledJSPageSizeKey"

    ''style options for panel holding popover
    Public Shared CustEnquiryShowPanelStyleValue As String = "background-color:White;display:block"
    Public Shared CustEnquiryHidePanelStyleValue As String = "background-color:White;display:none"
    ''viewstate variable names in customer enquiry management
    ''ket name for dataset in viewstate
    Public Shared CustEnquiryDataSetViewState As String = "enquiryDS"
    Public Shared CustFaultDataSetViewState As String = "faultDS"
    Public Shared FaultTransactionDataSetViewState As String = "faultTransDS"
    Public Shared FaultPriorityDataSetViewState As String = "priorityDs"
    Public Shared CancelledJSDataSetViewState As String = "CancelledJobDS"
    Public Shared PostInspectionReportGS As String = "PostInspectionDS"
    ''key name for searchoptions in viewstate
    Public Shared CustEnquirySearchOptionsViewState As String = "SearchOptions"
    Public Shared CustFaultSearchOptionsViewState As String = "FaultSearchOptions"
    Public Shared CustInvoiceSearchOptionsViewState As String = "InvoiceSearchOptions"
    Public Shared FaultTransactionLogSearchOptionsViewState As String = "FaultTransactionSearchOptions"
    Public Shared FaultPrioritySearchOptionsViewState As String = "FaultPrioritySearchOptions"
    Public Shared ReportedFaultSearchOptionsViewState As String = "ReportedFaultSearchOptions"
    Public Shared PreInspectionSearchOptionsViewState As String = "PreInspectionSearchOptions"
    Public Shared ManageAppointmentSearchOptionsViewState As String = "ContractorSearchOptions"
    Public Shared AppointmentTBASearchOptionsViewState As String = "AppointmentTBASearchOptions"
    Public Shared PostInspectionReportSearchOptionsViewState As String = "PostInspecitonReportSearchOptions"
    Public Shared JobSheetSearchOptionViewState As String = "JobSheetSearchOption"
    Public Shared CancelledJSSearchOptionViewState As String = "CancelledJSSearchOptions"
    ''''''''''
    Public Shared CustResponseSearchOptionsViewState As String = "ResponseSearchOptions"
    ''''''''''
    ''key name for sortby in viewstate
    Public Shared CustEnquirySortByViewState As String = "SortBy"
    Public Shared CustFaultSortByViewState As String = "FaultSortBy"
    Public Shared FaultTransactionLogSortByViewState As String = "FaultTransactionSorBy"
    Public Shared FaultPrioritySortByViewState As String = "FaultPrioritySortBy"
    Public Shared ReportedFaultSortByViewState As String = "ReportedFaultSortBy"
    Public Shared PreInspectionSortByViewState As String = "PreInspectionSortBy"
    Public Shared ContractorProtalJobSheetSortByKey As String = "ContractorProtalJobSheetSortByKey"
    Public Shared ContractorPortalMonitoringSortByKey As String = "ContractorPortalMonitoringSortBYKey"
    Public Shared CustInvoiceSortByViewState As String = "InvoiceSortBy"
    Public Shared ManageAppointmentSortByViewState As String = "ManageAppointmentSortBy"
    Public Shared AppointmentTBASortByViewState As String = "TBAFaultSortBy"
    Public Shared MonitoringSortByViewState As String = "MonitoringSortBY"
    Public Shared ContractorProtalFaultReportsSortByKey As String = "ContractorProtalFaultReportsSortByKey"
    Public Shared PostInspectionReportSortByViewState As String = "PostInspectionReportSorBy"
    Public Shared JobSheetSortByViewState As String = "JobSheetSortBy"
    Public Shared CancelledJSSortByViewState As String = "CancelledJSSortBy"
    Public Shared ReactiveRepairSortByViewState As String = "ReactiveRepairSortBy"

    ''key name for sortorder in viewstate
    Public Shared CustEnquirySortOrderViewSate As String = "SortOrder"
    Public Shared CustFaultSortOrderViewState As String = "FaultSortOrder"
    Public Shared InvoiceSortOrderViewState As String = "InvoiceSortOrder"
    Public Shared MonitoringSortOrderViewState As String = "MonitoringSortOrder"
    Public Shared InvoicingSearchOptionsViewState As String = "InvoicingSearchOptions"
    Public Shared InvoicingResultSummary As String = "InvoicingResultSummary"
    Public Shared FaultTransactionLogSortOrderViewState As String = "FaultTransactionSortOrder"
    Public Shared FaultPrioritySortOrderViewState As String = "FaultPrioritySortOrder"
    Public Shared ReportedFaultSortOrderViewSate As String = "RFSortOrder"
    Public Shared PreInspectionSortOrderViewState As String = "PISortOrder"
    Public Shared ContractorProtalJobSheetSortOrderKey As String = "ContractorProtalJobSheetSortOrderKey"
    Public Shared ContractorPortalMonitoringSortOrderKey As String = "ContractorPortalMonitoringSortOrderKey"
    Public Shared ManageAppointmentSortOrderViewSate As String = "CPSortOrder"
    Public Shared AppointmentTBASortOrderViewSate As String = "TBASortOrder"
    Public Shared ContractorProtalFaultReportsSortOrderKey As String = "ContractorProtalFaultReportsSortOrderKey"
    Public Shared PostInspectionReportSortOrderViewSate As String = "PostInspectionSortOrder"
    Public Shared JobSheetSortOrderViewState As String = "JobSheetSortOrder"
    Public Shared CancelledJSSortOrderViewState As String = "CancelledJSSortOrder"
    Public Shared ReactiveRepairJournalSortOrderViewState As String = "ReactiveRepairJournalSortOrder"



    '''''''''''''''''''
    ''key name for sortorder in viewstate
    Public Shared CustResponseSortOrderViewSate As String = "ResSortOrder"
    '''''''''''''''''''''
    ''key holding gridview data state
    Public Shared CustEnquiryIsSearchViewState As String = "IsSearch"
    Public Shared CustFaultIsSearchViewState As String = "IsSearchFault"
    Public Shared CustInvoiceIsSearchViewState As String = "IsInvoiceSearch"
    Public Shared FaultTransactionLogIsSearchViewState As String = "IsSearchFaultTransaction"
    Public Shared FaultPriorityIsSearchViewState As String = "IsSearchFaultPriority"
    Public Shared ReportedFaultIsSearchViewState As String = "IsReportedFaultSearch"
    Public Shared PreInspectionIsSearchViewState As String = "IsPreInspectionSearch"
    Public Shared AppointmentTBAIsSearchViewState As String = "IsTBASearch"
    Public Shared PostInspectionIsSearchViewState As String = "PostInspectionReportIsSearch"
    Public Shared JobSheetIsSearchViewState As String = "JobSheetIsSearch"
    Public Shared CancelledJSIsSearchViewState As String = "CancelledJSIsSearch"

    Public Shared FaultJobSheetDataSet As String = "FaultJobSheetDataSet"
    Public Shared FaultContractorJobSheetDataSet As String = "FaultContractorJobSheetDataSet"
    Public Shared TBADataSet As String = "TBADataSet"
    Public Shared MADataSet As String = "MADataSet"
    Public Shared WCDataSet As String = "WCDataSet"
    Public Shared INVDataSet As String = "INVDataSet"
    Public Shared PreInspectionDS As String = "PreInsDS"
    Public Shared FaultLocatorDataSet As String = "FaultLocatorDataSet"
    Public Shared PostInspectionReportDataset As String = "PostInspectionDataSet"
    Public Shared FaultMonitoringDataSet As String = "FaultMonitoringDataSet"
    Public Shared JobSheetIsSearch As String = "JobSheetIsSearch"
    Public Shared ManageAppointmentIsSearchViewState As String = "IsContractorPortalSearch"
    Public Shared ReactiveRepairJournalDataSet As String = "ReactiveRepairJournalDS"
    Public Shared PostInspectionGridButtonStatus As String = "PostInspectionButtonUpdateStatus"



    '''''''''''''
    Public Shared CustResponseIsSearchViewState As String = "IsSearchRes"
    ''''''''''''

    ''key holding isedit vewstate
    Public Shared FaultPriorityIsEdit As String = "IsEditFaultPriority"
    ''key holding summary of results
    Public Shared CustEnquiryResultSummary As String = "ResultSummary"
    Public Shared CustFaultResultSummary As String = "FaultResultSummary"
    Public Shared FaultTransactionLogResultSummary As String = "FaultTransactionResultSummary"
    Public Shared FaultPriorityResultSummary As String = "FaultPriorityResultSummary"
    Public Shared ReportedFaultResultSummary As String = "ReportedFaultResultSummary"
    Public Shared PreInspectionResultSummary As String = "PreInspectionResultSummary"
    Public Shared ManageAppointmentResultSummary As String = "AppointmentResultSummary"
    Public Shared WorkCompletionResultSummary As String = "WorkCompletionResultSummary"
    Public Shared CustResponseResultSummary As String = "ResponseResultSummary"
    Public Shared AppointmentTBAResultSummary As String = "TBAResultSummary"
    Public Shared PostInspectionReportResultSummary As String = "PostInspectionResultSummary"
    Public Shared InvoiceCheckStatusArray As String = "InvoiceCheckStatusArray"
    Public Shared JobSheetResultSummary As String = "JobSheetResultSummary"
    Public Shared CancelledJSResultSummary As String = "CancelledJSResultSummary"

    ''strings for exporting dataset to excel
    ''constant for default file name
    Public Shared CustEnquiryAttachment As String = "attachment; filename=EnquirLog.xls"
    ''constant for ContentType which in this case is excel
    Public Shared CustEnquiryContentType As String = "application/vnd.ms-excel"
    ''constant for HTTP Request header
    Public Shared CustEnquiryHTTPHeader As String = "content-disposition"

    ''constant for Folder Path in Contractor Portal Monitoring
    'Public Shared ConMonitoringFileDirPath As String = "C:\test_inetpub\www\"


    ''constant for for server path
    Public Shared SeverPath As String = "http://localhost:2525/"


    ''constant email template file name
    Public Shared EmailTemplateFileName As String = "ForgotPasswordEmail.html"


    ''strings for different types of complaints/reports/request
    Public Shared CustEnquiryTerminationRequest As String = "Termination (TO)"
    Public Shared CustEnquiryServiceComplaint As String = "Service Complaints (TO)"
    Public Shared CustEnquiryGarageParkingRequest As String = "Garage/Car Parking (TO)"
    Public Shared CustEnquiryASBComplaint As String = "ASB - Complainant (TO)"
    Public Shared CustEnquiryAbandonedReport As String = "Abandoned Property/Vehicle (TO)"
    Public Shared CustEnquiryTransferRequest As String = "House Move (TO)"
    Public Shared CustEnquiryClearArrearsRequest As String = "Arrears"
    Public Shared CustEnquiryGeneralRequest As String = "General"
    Public Shared CustEnquiryDirectDebitRentRequest As String = "Rent"
    Public Shared CustEnquiryCstRequest As String = "CST Request (TO)"
    Public Shared CustomerEnquiryStatusNew As String = "26"
    Public Shared CustomerEnquiryStatusDeleted As String = "28"

    Public Shared MonitorigFileSessionKey As String = "MonitoringFileName"



End Class
