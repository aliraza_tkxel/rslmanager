Imports System
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters


Public Class LogHandler

    Public Shared Sub LogIt(ByVal message As String, ByVal logId As Integer)

        Dim logEntry As LogEntry = New LogEntry()
        logEntry.EventId = logId
        logEntry.Priority = 2
        logEntry.Message = message
        logEntry.Categories.Add("Trace")
        logEntry.Categories.Add("UI Events")

        Logger.Write(logEntry)

    End Sub

End Class
