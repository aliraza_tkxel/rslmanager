

Imports System
Imports System.Collections


Namespace Broadland.TenantsOnline.BusinessObject

    Public Class FaultElementList : Inherits CollectionBase

#Region "Attributes"

        Private _exceptionGenerated As Boolean

#End Region

#Region "Constructors"

        Public Sub New()
            _exceptionGenerated = False
        End Sub

#End Region

#Region "Properties"
        Default Public Property Item(ByVal Index As Integer) As ElementBO
            Get
                Return CType(List.Item(Index), ElementBO)
            End Get
            Set(ByVal Value As ElementBO)
                List.Item(Index) = Value
            End Set
        End Property

        Public Property IsExceptionGenerated() As Boolean
            Get
                Return _exceptionGenerated
            End Get
            Set(ByVal value As Boolean)
                _exceptionGenerated = value
            End Set
        End Property


#End Region

#Region "Functions"

        Public Function Add(ByVal Item As ElementBO) As Integer
            Return List.Add(Item)
        End Function

#End Region

    End Class


End Namespace

