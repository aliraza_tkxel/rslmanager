Namespace Broadland.TenantsOnline.BusinessObject
    Public Class CustomerAddressBO : Inherits BaseBO

#Region "Attributes"

        Private _id As Integer
        Private _addressLine1 As String
        Private _addressLine2 As String
        Private _addressLine3 As String
        Private _email As String
        Private _postCode As String
        Private _telephone As String
        Private _telephoneMobile As String
        Private _houseNumber As String
        Private _townCity As String
        Private _county As String

#End Region

#Region "Construtor"

        Public Sub New()

            _id = -1
            _addressLine1 = String.Empty
            _addressLine2 = String.Empty
            _addressLine3 = String.Empty
            _email = String.Empty
            _postCode = String.Empty
            _telephone = String.Empty
            _houseNumber = String.Empty
            _townCity = String.Empty
            _county = String.Empty

        End Sub

#End Region

#Region "Properties"

        ' Get / Set property for _id
        Public Property Id() As Integer

            Get
                Return _id
            End Get

            Set(ByVal value As Integer)
                _id = value
            End Set

        End Property


        ' Get / Set property for _addressLine1
        Public Property Address1() As String

            Get
                Return _addressLine1
            End Get

            Set(ByVal value As String)
                _addressLine1 = value
            End Set

        End Property


        ' Get / Set property for _addressLine2
        Public Property Address2() As String

            Get
                Return _addressLine2
            End Get

            Set(ByVal value As String)
                _addressLine2 = value
            End Set

        End Property

        ' Get / Set property for _addressLine3
        Public Property Address3() As String

            Get
                Return _addressLine3
            End Get

            Set(ByVal value As String)
                _addressLine3 = value
            End Set

        End Property

        ' Get / Set property for _email
        Public Property Email() As String

            Get
                Return _email
            End Get

            Set(ByVal value As String)
                _email = value
            End Set

        End Property

        ' Get / Set property for _postCode
        Public Property PostCode() As String

            Get
                Return _postCode
            End Get

            Set(ByVal value As String)
                _postCode = value
            End Set
        End Property

    

        ' Get / Set property for _telephoneMobile1
        Public Property TelephoneMobile() As String

            Get
                Return _telephoneMobile
            End Get

            Set(ByVal value As String)
                _telephoneMobile = value
            End Set
        End Property

        ' Get / Set property for _telephone
        Public Property Telephone() As String

            Get
                Return _telephone
            End Get

            Set(ByVal value As String)
                _telephone = value
            End Set
        End Property
        
        ' Get / Set property for _houseNumber
        Public Property HouseNumber() As String

            Get
                Return _houseNumber
            End Get

            Set(ByVal value As String)
                _houseNumber = value
            End Set
        End Property

        ' Get / Set property for _county
        Public Property Coutnty() As String

            Get
                Return _county
            End Get

            Set(ByVal value As String)
                _county = value
            End Set
        End Property

        ' Get / Set property for _townCity
        Public Property TownCity() As String

            Get
                Return _townCity
            End Get

            Set(ByVal value As String)
                _townCity = value
            End Set
        End Property

#Region "Properties return concatinated rusult of multiple attributes"
        ' Get property ,Customer Address
        Public ReadOnly Property CustomerAddress() As String
            Get
                Return Me.HouseNumber & " " & Me.Address1 & " " & Me.Coutnty & " " & Me.PostCode
            End Get

        End Property

#End Region


#End Region

#Region "Functions"


#End Region

    End Class
End Namespace

