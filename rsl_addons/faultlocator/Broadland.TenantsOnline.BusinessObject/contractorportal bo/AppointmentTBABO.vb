Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class AppointmentTBABO : Inherits BaseBO

#Region "Attributes"
      
        Private _appointmentId As Integer
        Private _jsNumber As String
        Private _appointmentDate As String
        Private _operativeId As Integer
        Private _letterId As Integer
        Private _appointmentStageId As Integer
        Private _lastActionDate As String
        Private _notes As String
        Private _appointmentTime As String
#End Region

#Region "Constructor"

        Public Sub New()
            _appointmentId = -1
            _jsNumber = String.Empty
            _appointmentDate = String.Empty
            _operativeId = -1
            _letterId = -1
            _appointmentStageId = -1
            _lastActionDate = String.Empty
            _notes = String.Empty
            _appointmentTime = String.Empty

        End Sub

#End Region

#Region "Get/Set Properties"

        'Get/Set Property for _appoitnemntId
        Public Property AppointmentId() As Integer
            Get
                Return _appointmentId
            End Get
            Set(ByVal value As Integer)
                _appointmentId = value
            End Set
        End Property

        'Get/Set Property for _jsNumber
        Public Property JSNumber() As String
            Get
                Return _jsNumber
            End Get
            Set(ByVal value As String)
                _jsNumber = value
            End Set
        End Property

        'Get/Set Property for _appointmentDate
        Public Property AppointmentDate() As String
            Get
                Return _appointmentDate
            End Get
            Set(ByVal value As String)
                _appointmentDate = value
            End Set
        End Property

        'Get/Set Property for _operativeId
        Public Property OperativeId() As Integer
            Get
                Return _operativeId
            End Get
            Set(ByVal value As Integer)
                _operativeId = value
            End Set
        End Property

        'Get/Set Property for _letterId
        Public Property LetterId() As Integer
            Get
                Return _letterId
            End Get
            Set(ByVal value As Integer)
                _letterId = value
            End Set
        End Property

        'Get/Set Property for _appointmentStageId
        Public Property AppointmentStageId() As Integer
            Get
                Return _appointmentStageId
            End Get
            Set(ByVal value As Integer)
                _appointmentStageId = value
            End Set
        End Property

        'Get/Set Property for _lastActionDate
        Public Property LastActionDate() As String
            Get
                Return _lastActionDate
            End Get
            Set(ByVal value As String)
                _lastActionDate = value
            End Set
        End Property

        'Get/Set Property for _notes
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property

        'Get/Set Property for _appointmentTime
        Public Property AppointmentTime() As String
            Get
                Return _appointmentTime
            End Get
            Set(ByVal value As String)
                _appointmentTime = value
            End Set
        End Property

#End Region

    End Class
End Namespace
