Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class InvoiceBO : Inherits BaseBO
#Region "Attributes"


        Private _sortBy As String
        Private _sortOrder As String
        Private _rowCount As Integer
        Private _pageIndex As Integer
        Private _faultLogId As Integer
        Private _locationId As Integer
        Private _areaId As Integer
        Private _elementId As Integer
        Private _userId As Integer
        Private _patchId As Integer
        Private _schemeId As Integer
        Private _priorityId As Integer
        Private _statusId As Integer
        Private _stageId As Integer
        Private _postCode As String
        Private _orgId As Integer
        Private _dueDate As String
        Private _propertyId As Integer
        Private _isSearch As Boolean
        Private _jsnumber As Integer



#End Region

#Region "Construtor"

        Public Sub New()

            _sortBy = String.Empty
            _sortOrder = String.Empty
            _rowCount = -1
            _pageIndex = -1
            _faultLogId = -1
            _locationId = -1
            _areaId = -1
            _elementId = -1
            _userId = -1
            _patchId = -1
            _schemeId = -1
            _priorityId = -1
            _statusId = -1
            _stageId = -1
            _postCode = String.Empty
            _orgId = -1
            _dueDate = String.Empty
            _propertyId = -1
            _isSearch = False
            _jsnumber = Nothing

        End Sub

#End Region
#Region "Properties"

        'Get/Set Property for IsSearch 

        Public Property IsSearch() As Boolean
            Get
                Return _isSearch

            End Get
            Set(ByVal value As Boolean)

                _isSearch = value


            End Set
        End Property

        Public Property SortBy() As String
            Get
                Return _sortBy
            End Get
            Set(ByVal value As String)
                _sortBy = value
            End Set
        End Property

        'Get/Set Property for _sortOrder
        Public Property SortOrder() As String
            Get
                Return _sortOrder
            End Get
            Set(ByVal value As String)
                _sortOrder = value
            End Set
        End Property

        'Get/Set Property for _rowCount
        Public Property RowCount() As Integer
            Get
                Return _rowCount
            End Get
            Set(ByVal value As Integer)
                _rowCount = value
            End Set
        End Property

        'Get/Set Property for _pageIndex
        Public Property PageIndex() As Integer
            Get
                Return _pageIndex
            End Get
            Set(ByVal value As Integer)
                _pageIndex = value
            End Set
        End Property

        ' Get / Set property for _propertyId

        Public Property PriorityId() As Integer
            Get
                Return _priorityId
            End Get
            Set(ByVal value As Integer)
                _priorityId = value
            End Set
        End Property

        ' Get / Set FaultLogId for _faultLogId
        Public Property FaultLogId() As Integer

            Get
                Return _faultLogId
            End Get

            Set(ByVal value As Integer)
                _faultLogId = value
            End Set

        End Property
        ' Get / Set OrgId for _orgId

        Public Property OrgId() As Integer
            Get
                Return _orgId
            End Get
            Set(ByVal value As Integer)
                _orgId = value
            End Set
        End Property
        ' Get / Set Location for _locationId

        Public Property LocationId() As Integer

            Get
                Return _locationId
            End Get

            Set(ByVal value As Integer)
                _locationId = value
            End Set

        End Property
        ' Get / Set Area for _areaId
        Public Property AreaId() As Integer

            Get
                Return _areaId
            End Get

            Set(ByVal value As Integer)
                _areaId = value
            End Set

        End Property
        ' Get / Set Element for _elementId
        Public Property ElementId() As Integer

            Get
                Return _elementId
            End Get

            Set(ByVal value As Integer)
                _elementId = value
            End Set

        End Property
        ' Get / Set User for _userId
        Public Property UserId() As Integer

            Get
                Return _userId
            End Get

            Set(ByVal value As Integer)
                _userId = value
            End Set

        End Property
        ' Get / Set Patch for _patchId
        Public Property PatchId() As Integer

            Get
                Return _patchId
            End Get

            Set(ByVal value As Integer)
                _patchId = value
            End Set

        End Property
        ' Get / Set Scheme for _schemeId
        Public Property SchemeId() As Integer

            Get
                Return _schemeId
            End Get

            Set(ByVal value As Integer)
                _schemeId = value
            End Set

        End Property
        ' Get / Set Status for _statusId
        Public Property StatusId() As Integer

            Get
                Return _statusId
            End Get

            Set(ByVal value As Integer)
                _statusId = value
            End Set

        End Property
        ' Get / Set Stage for _stageId
        Public Property StageId() As Integer

            Get
                Return _stageId
            End Get

            Set(ByVal value As Integer)
                _stageId = value
            End Set

        End Property
        ' Get / Set PostCode for _postCode
        Public Property PostCode() As String

            Get
                Return _postCode
            End Get

            Set(ByVal value As String)
                _postCode = value
            End Set

        End Property
        ' Get / Set Due for _due
        Public Property DueDate() As String

            Get
                Return _dueDate
            End Get

            Set(ByVal value As String)
                _dueDate = value
            End Set

        End Property

        Public Property JsNumber() As Integer

            Get
                Return _jsnumber
            End Get

            Set(ByVal value As Integer)
                _jsnumber = value
            End Set

        End Property
#End Region
    End Class
End Namespace

