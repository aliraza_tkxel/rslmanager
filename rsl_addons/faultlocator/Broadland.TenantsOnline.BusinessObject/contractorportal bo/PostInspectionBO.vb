Imports System
Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class PostInspectionBO : Inherits BaseBO
#Region "Attributes"

        '' Holds column name by which we are sorting 
        '' rowcount is number of results we want before querying database again
        '' pageindex is page number on gridview we will use it to calculate along 
        '' with rowcount to get results in between
        Private _userId As Integer
        Private _inspectionDate As DateTime
        Private _netCost As Double
        Private _dueDate As DateTime
        Private _notes As String
        Private _orgId As Integer
        Private _approved As Integer
        Private _tempFaultId As Integer
        Private _faultLogId As Integer
        Private _sortBy As String
        Private _sortOrder As String
        Private _rowCount As Integer
        Private _pageIndex As Integer
        Private _isSearch As Boolean



#End Region

#Region "Construtor"

        Public Sub New()
            _userId = -1
            _notes = String.Empty
            _inspectionDate = Nothing
            _dueDate = Nothing
            _netCost = 0.0
            _orgId = -1
            _approved = 0
            _tempFaultId = 0
            _faultLogId = 0
            _sortBy = String.Empty
            _sortOrder = String.Empty
            _rowCount = 10
            _pageIndex = 0
            _isSearch = False

        End Sub

#End Region

#Region "Get/Set Properties"
        ' Get / Set property for _notes
        Public Property Notes() As String

            Get
                Return _notes
            End Get

            Set(ByVal value As String)
                _notes = value
            End Set

        End Property
        ' Get / Set property for _dueDate
        Public Property DueDate() As Date

            Get
                Return _dueDate
            End Get

            Set(ByVal value As Date)
                _dueDate = value
            End Set

        End Property
        ' Get / Set property for inspectDate
        Public Property InspectionDate() As Date

            Get
                Return _inspectionDate
            End Get

            Set(ByVal value As Date)
                _inspectionDate = value
            End Set

        End Property

        ' Get / Set property for _netCost
        Public Property NetCost() As Double

            Get
                Return _netCost
            End Get

            Set(ByVal value As Double)
                _netCost = value
            End Set

        End Property

        ' Get/Set property for _faultLogId
        Public Property FaultLogId() As Integer

            Get
                Return _faultLogId
            End Get

            Set(ByVal value As Integer)
                _faultLogId = value
            End Set

        End Property
        ' Get/Set property for _userId
        Public Property UserId() As Integer

            Get
                Return _userId
            End Get

            Set(ByVal value As Integer)
                _userId = value
            End Set

        End Property
        ' Get/Set property for _approved
        Public Property Approved() As Integer

            Get
                Return _approved
            End Get

            Set(ByVal value As Integer)
                _approved = value
            End Set

        End Property
        ' Get/Set property for _orgId
        Public Property OrgId() As Integer

            Get
                Return _orgId
            End Get

            Set(ByVal value As Integer)
                _orgId = value
            End Set

        End Property

        'Get/Set property for _tempFaultId 
        Public Property TempFaultId() As Integer
            Get
                Return _tempFaultId
            End Get
            Set(ByVal value As Integer)
                _tempFaultId = value
            End Set
        End Property

        Public Property SortBy() As String

            Get
                Return _sortBy
            End Get

            Set(ByVal value As String)
                _sortBy = value
            End Set

        End Property
        ' Get / Set property for _sortBy
        Public Property SortOrder() As String

            Get
                Return _sortOrder
            End Get

            Set(ByVal value As String)
                _sortOrder = value
            End Set

        End Property
        ' Get / Set property for _rowCount
        Public Property RowCount() As Integer

            Get
                Return _rowCount
            End Get

            Set(ByVal value As Integer)
                _rowCount = value
            End Set

        End Property

        ' Get / Set property for _pageIndex
        Public Property PageIndex() As Integer

            Get
                Return _pageIndex
            End Get

            Set(ByVal value As Integer)
                _pageIndex = value
            End Set

        End Property

        'Get/Set Property for _isSearch
        Public Property IsSearch() As Boolean
            Get
                Return _isSearch
            End Get
            Set(ByVal value As Boolean)
                _isSearch = value
            End Set
        End Property
#End Region

    End Class
End Namespace
