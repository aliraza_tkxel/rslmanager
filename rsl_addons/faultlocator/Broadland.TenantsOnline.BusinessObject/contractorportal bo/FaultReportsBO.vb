Namespace Broadland.TenantsOnline.BusinessObject

    <Serializable()> _
    Public Class FaultReportsBO : Inherits BaseBO
#Region "Attributes"
        Private _customerId As Integer
        Private _submitdate As DateTime
        Private _faultDetail As String
        Private _faultStatus As Integer
        Private _faultResponse As String
        Private _unreadfaultCount As Integer
        Private _faultBO As FaultBO
        Private _orgId As Integer
        Private _pageSize As Integer
        Private _pageIndex As Integer
        Private _sortBy As String
        Private _sortOrder As String


#End Region

#Region "Constructor"
        Public Sub New()
            _customerId = -1
            _submitdate = Nothing
            _faultDetail = String.Empty
            _faultStatus = -1
            _faultResponse = String.Empty
            _orgId = -1
            _pageSize = -1
            _pageIndex = -1
            _sortBy = String.Empty
            _sortOrder = String.Empty

        End Sub
#End Region

#Region "Properties"
        ' Get / Set property for _creationDate
        Public Property CustomerId() As Integer
            Get
                Return _customerId
            End Get
            Set(ByVal value As Integer)
                _customerId = value
            End Set
        End Property
        ' Get / Set property for _creationDate
        Public Property Submitdate() As Date
            Get
                Return _submitdate
            End Get
            Set(ByVal value As Date)
                _submitdate = value
            End Set
        End Property

        ' Get / Set property for _faultDetail
        Public Property FaultDetail() As String
            Get
                Return _faultDetail
            End Get
            Set(ByVal value As String)
                _faultDetail = value
            End Set
        End Property

        ' Get / Set property for _faultStatus
        Public Property FaultStatus() As Integer
            Get
                Return _faultStatus
            End Get
            Set(ByVal value As Integer)
                _faultStatus = value
            End Set
        End Property

        ' Get / Set property for _faultResponse
        Public Property FaultResponse() As String
            Get
                Return _faultResponse
            End Get
            Set(ByVal value As String)
                _faultResponse = value
            End Set
        End Property

        ' Get / Set property for _orgId
        Public Property OrgId() As Integer
            Get
                Return _orgId
            End Get
            Set(ByVal value As Integer)
                _orgId = value
            End Set
        End Property
        ' Get / Set property for _pageSize
        Public Property PageSize() As Integer
            Get
                Return _pageSize
            End Get
            Set(ByVal value As Integer)
                _pageSize = value
            End Set
        End Property
        ' Get / Set property for _pageIndex
        Public Property PageIndex() As Integer
            Get
                Return _pageIndex
            End Get
            Set(ByVal value As Integer)
                _pageIndex = value
            End Set
        End Property

        ' Get / Set property for _sortBy
        Public Property SortBy() As String
            Get
                Return _sortBy
            End Get
            Set(ByVal value As String)
                _sortBy = value
            End Set
        End Property

        ' Get / Set property for _sortOrder
        Public Property SortOrder() As String
            Get
                Return _sortOrder
            End Get
            Set(ByVal value As String)
                _sortOrder = value
            End Set
        End Property
#End Region

    End Class
End Namespace
