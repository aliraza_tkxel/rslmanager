Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class ContractorPortalBO : Inherits BaseBO
#Region "Attributes"

        Private _faultLogId As Integer
        Private _locationId As Integer
        Private _areaId As Integer
        Private _elementId As Integer
        Private _teamId As Integer
        Private _rowCount As Integer
        Private _userId As Integer
        Private _patchId As Integer
        Private _schemeId As Integer
        Private _priorityId As Integer
        Private _statusId As Integer
        Private _stageId As Integer
        Private _postCode As String
        Private _due As String
        Private _orgId As Integer
        Private _pageIndex As Integer
        Private _pageSize As Integer
        Private _sortBy As String
        Private _sortOrder As String
        Private _isSearch As Integer
        Private _appointmentDate As String
        Private _lastActioned As String
        Private _operativeId As Integer
        Private _appointmentNotes As String
        Private _letterId As Integer
        Private _time As String
        Private _notes As String
        Private _appointmentId As Integer
        Private _vatId As Integer
        Private _vatRate As Double
        Private _vat As Double
        Private _net As Double
        Private _total As Double
        Private _repairDescription As String
        Private _repairId As Integer
        Private _completionDate As Date
        Private _completedBy As Integer
        Private _quantity As Integer
        Private _jsNumber As Integer
#End Region

#Region "Construtor"

        Public Sub New()
            _faultLogId = -1
            _locationId = -1
            _areaId = -1
            _elementId = -1
            _teamId = -1
            _userId = -1
            _patchId = -1
            _schemeId = -1
            _priorityId = -1
            _statusId = -1
            _stageId = -1
            _postCode = Nothing
            _due = String.Empty
            _orgId = -1
            _pageIndex = -1
            _pageSize = -1
            _sortBy = Nothing
            _sortOrder = Nothing
            _isSearch = -1
            _completionDate = Nothing
            _appointmentDate = Nothing
            _lastActioned = Nothing
            _operativeId = -1
            _appointmentNotes = Nothing
            _letterId = -1
            _time = Nothing
            _notes = Nothing
            _appointmentId = -1
            _vatId = -1
            _vatRate = -1
            _vat = -1
            _net = -1
            _total = -1
            _repairDescription = Nothing
            _repairId = -1
            _completedBy = -1
            _quantity = -1
            _jsNumber = Nothing

        End Sub

#End Region

#Region "Properties"

        ' Get / Set property for _faultLogId
        Public Property FaultLogId() As Integer

            Get
                Return _faultLogId
            End Get

            Set(ByVal value As Integer)
                _faultLogId = value
            End Set

        End Property

        ' Get / Set property for _locationId
        Public Property LocationId() As Integer

            Get
                Return _locationId
            End Get

            Set(ByVal value As Integer)
                _locationId = value
            End Set

        End Property
        ' Get / Set property for _areaId
        Public Property AreaId() As Integer

            Get
                Return _areaId
            End Get

            Set(ByVal value As Integer)
                _areaId = value
            End Set

        End Property
        ' Get / Set property for _elementId
        Public Property ElementId() As Integer

            Get
                Return _elementId
            End Get

            Set(ByVal value As Integer)
                _elementId = value
            End Set

        End Property
        ' Get / Set property for _teamId
        Public Property TeamId() As Integer

            Get
                Return _teamId
            End Get

            Set(ByVal value As Integer)
                _teamId = value
            End Set

        End Property
        ' Get / Set property for _userId
        Public Property UserId() As Integer

            Get
                Return _userId
            End Get

            Set(ByVal value As Integer)
                _userId = value
            End Set

        End Property
        ' Get / Set property for _patchId
        Public Property PatchId() As Integer

            Get
                Return _patchId
            End Get

            Set(ByVal value As Integer)
                _patchId = value
            End Set

        End Property
        ' Get / Set property for _schemeId
        Public Property SchemeId() As Integer

            Get
                Return _schemeId
            End Get

            Set(ByVal value As Integer)
                _schemeId = value
            End Set

        End Property
        ' Get / Set property for _priorityId
        Public Property PriorityId() As Integer

            Get
                Return _priorityId
            End Get

            Set(ByVal value As Integer)
                _priorityId = value
            End Set

        End Property
        ' Get / Set property for _statusId
        Public Property StatusId() As Integer

            Get
                Return _statusId
            End Get

            Set(ByVal value As Integer)
                _statusId = value
            End Set

        End Property
        ' Get / Set property for _stageId
        Public Property StageId() As Integer

            Get
                Return _stageId
            End Get

            Set(ByVal value As Integer)
                _stageId = value
            End Set

        End Property
        ' Get / Set property for _postCode
        Public Property PostCode() As String

            Get
                Return _postCode
            End Get

            Set(ByVal value As String)
                _postCode = value
            End Set

        End Property
        ' Get / Set property for _due
        Public Property Due() As String

            Get
                Return _due
            End Get

            Set(ByVal value As String)
                _due = value
            End Set

        End Property

        ' Get / Set property for _pageSize
        Public Property PageSize() As Integer

            Get
                Return _pageSize
            End Get

            Set(ByVal value As Integer)
                _pageSize = value
            End Set

        End Property

        ' Get / Set property for _completionDate
        Public Property CompletionDate() As Date

            Get
                Return _completionDate
            End Get

            Set(ByVal value As Date)
                _completionDate = value
            End Set

        End Property
        ' Get / Set property for _pageIndex
        Public Property PageIndex() As Integer

            Get
                Return _pageIndex
            End Get

            Set(ByVal value As Integer)
                _pageIndex = value
            End Set

        End Property
        ' Get / Set property for _rowCount
        Public Property RowCount() As Integer

            Get
                Return _rowCount
            End Get

            Set(ByVal value As Integer)
                _rowCount = value
            End Set

        End Property
        ' Get / Set property for _sortBy
        Public Property SortOrder() As String

            Get
                Return _sortOrder
            End Get

            Set(ByVal value As String)
                _sortOrder = value
            End Set

        End Property
        ' Get / Set property for _sortBy
        Public Property SortBy() As String

            Get
                Return _sortBy
            End Get

            Set(ByVal value As String)
                _sortBy = value
            End Set

        End Property
        ' Get / Set property for _isSearch
        Public Property IsSearch() As Integer

            Get
                Return _isSearch
            End Get

            Set(ByVal value As Integer)
                _isSearch = value
            End Set

        End Property
        ' Get / Set property for _orgId
        Public Property OrgId() As Integer

            Get
                Return _orgId
            End Get

            Set(ByVal value As Integer)
                _orgId = value
            End Set

        End Property
        ' Get / Set property for _appointmentDate
        Public Property AppointmentDate() As String

            Get
                Return _appointmentDate
            End Get

            Set(ByVal value As String)
                _appointmentDate = value
            End Set

        End Property
        ' Get / Set property for _lastActioned
        Public Property LastActioned() As String

            Get
                Return _lastActioned
            End Get

            Set(ByVal value As String)
                _lastActioned = value
            End Set

        End Property
        ' Get / Set property for _appointmentNotes
        Public Property AppointmentNotes() As String

            Get
                Return _appointmentNotes
            End Get

            Set(ByVal value As String)
                _appointmentNotes = value
            End Set

        End Property
        ' Get / Set property for _operativeId
        Public Property OperativeId() As Integer

            Get
                Return _operativeId
            End Get

            Set(ByVal value As Integer)
                _operativeId = value
            End Set

        End Property
        ' Get / Set property for _letterId
        Public Property LetterId() As Integer

            Get
                Return _letterId
            End Get

            Set(ByVal value As Integer)
                _letterId = value
            End Set

        End Property
        ' Get / Set property for _time
        Public Property Time() As String

            Get
                Return _time
            End Get

            Set(ByVal value As String)
                _time = value
            End Set

        End Property
        ' Get / Set property for _notes
        Public Property Notes() As String

            Get
                Return _notes
            End Get

            Set(ByVal value As String)
                _notes = value
            End Set

        End Property
        ' Get / Set property for _appointmentId
        Public Property AppointmentId() As Integer

            Get
                Return _appointmentId
            End Get

            Set(ByVal value As Integer)
                _appointmentId = value
            End Set

        End Property

        ' Get / Set property for _vatId
        Public Property VatId() As Integer

            Get
                Return _vatId
            End Get

            Set(ByVal value As Integer)
                _vatId = value
            End Set

        End Property
        ' Get / Set property for _vatRate
        Public Property VatRate() As Double

            Get
                Return _vatRate
            End Get

            Set(ByVal value As Double)
                _vatRate = value
            End Set

        End Property
        ' Get / Set property for _vat
        Public Property Vat() As Double

            Get
                Return _vat
            End Get

            Set(ByVal value As Double)
                _vat = value
            End Set

        End Property
        ' Get / Set property for _net
        Public Property Net() As Double

            Get
                Return _net
            End Get

            Set(ByVal value As Double)
                _net = value
            End Set

        End Property
        ' Get / Set property for _total
        Public Property Total() As Double

            Get
                Return _total
            End Get

            Set(ByVal value As Double)
                _total = value
            End Set

        End Property
        ' Get / Set property for _repairDescription
        Public Property RepairDescription() As String

            Get
                Return _repairDescription
            End Get

            Set(ByVal value As String)
                _repairDescription = value
            End Set

        End Property
        ' Get / Set property for _repairId
        Public Property RepairId() As Integer

            Get
                Return _repairId
            End Get

            Set(ByVal value As Integer)
                _repairId = value
            End Set

        End Property
        ' Get / Set property for _completedBy
        Public Property CompletedBy() As Integer

            Get
                Return _completedBy
            End Get

            Set(ByVal value As Integer)
                _completedBy = value
            End Set

        End Property

        'Get/Set Property for _quantity
        Public Property Quantity() As Integer
            Get
                Return _quantity
            End Get
            Set(ByVal value As Integer)
                _quantity = value
            End Set
        End Property

        Public Property JsNumber() As Integer

            Get
                Return _jsNumber
            End Get

            Set(ByVal value As Integer)
                _jsNumber = value
            End Set

        End Property
#End Region
    End Class
End Namespace

