Imports System

Namespace Broadland.TenantsOnline.BusinessObject

    Public Class PreInspectionBO : Inherits BaseBO

#Region "Attributes"

        Private _orgId As Integer
        Private _sortBy As String
        Private _sortOrder As String
        Private _rowCount As Integer
        Private _pageIndex As Integer
        Private _userId As Integer

#End Region

#Region "Constructor"

        Public Sub New()

            _orgId = -1
            _sortBy = String.Empty
            _sortOrder = String.Empty
            _rowCount = 10
            _pageIndex = 0
            _userId = Nothing

        End Sub

#End Region

#Region "Properties"

        Public Property SortBy() As String

            Get
                Return _sortBy
            End Get

            Set(ByVal value As String)
                _sortBy = value
            End Set

        End Property
        ' Get / Set property for _sortBy
        Public Property SortOrder() As String

            Get
                Return _sortOrder
            End Get

            Set(ByVal value As String)
                _sortOrder = value
            End Set

        End Property
        ' Get / Set property for _rowCount
        Public Property RowCount() As Integer

            Get
                Return _rowCount
            End Get

            Set(ByVal value As Integer)
                _rowCount = value
            End Set

        End Property

        ' Get / Set property for _pageIndex
        Public Property PageIndex() As Integer

            Get
                Return _pageIndex
            End Get

            Set(ByVal value As Integer)
                _pageIndex = value
            End Set

        End Property

        Public Property OrgId() As Integer
            Get
                Return _orgId
            End Get
            Set(ByVal value As Integer)

                _orgId = value

            End Set
        End Property

        ' Get/Set property for _userId
        Public Property UserId() As Integer

            Get
                Return _userId
            End Get

            Set(ByVal value As Integer)
                _userId = value
            End Set

        End Property


#End Region

    End Class

End Namespace

