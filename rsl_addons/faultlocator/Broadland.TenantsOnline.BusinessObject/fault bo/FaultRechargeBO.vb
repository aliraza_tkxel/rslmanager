Namespace Broadland.TenantsOnline.BusinessObject


    <Serializable()> _
    Public Class FaultRechargeBO : Inherits BaseBO


#Region "Attributes"

        Private _faultId As Integer
        Private _rechargeCost As String


#End Region

#Region "Constructor"
        Public Sub New()
            _faultId = -1
            _rechargeCost = ""
  
        End Sub

#End Region

#Region "Properties"

        ' Get / Set property for _faultId
        Public Property FaultId() As Integer
            Get
                Return _faultId
            End Get
            Set(ByVal value As Integer)
                _faultId = value
            End Set
        End Property

        Public Property RechargeCost() As String
            Get
                Return _rechargeCost
            End Get
            Set(ByVal value As String)
                _rechargeCost = value
            End Set
        End Property

        
#End Region

    End Class

End Namespace



