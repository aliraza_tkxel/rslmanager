Imports System

Namespace Broadland.TenantsOnline.BusinessObject

    <Serializable()> _
Public Class PreInspectBO : Inherits BaseBO
#Region "Attributes"

        '' Holds column name by which we are sorting 
        '' rowcount is number of results we want before querying database again
        '' pageindex is page number on gridview we will use it to calculate along 
        '' with rowcount to get results in between
        Private _userId As Integer
        Private _inspectionDate As DateTime
        Private _time As String
        Private _netCost As Double
        Private _dueDate As DateTime
        Private _notes As String
        Private _orgId As Integer
        Private _approved As Integer
        Private _tempFaultId As Integer
        Private _faultLogId As Integer
        Private _recharge As Boolean
        Private _actionUserId As Integer
        Private _reason As String
        Private _repairId As Integer



#End Region

#Region "Construtor"

        Public Sub New()
            _userId = -1
            _notes = String.Empty
            _inspectionDate = Nothing
            _time = String.Empty
            _dueDate = Nothing
            _netCost = 0.0
            _orgId = -1
            _approved = -1
            _tempFaultId = 0
            _faultLogId = 0
            _recharge = False
            _actionUserId = 1
            _reason = String.Empty
            _repairId = -1
        End Sub

#End Region

#Region "Get/Set Properties"

        Public Property Reason() As String
            Get
                Return _reason
            End Get
            Set(ByVal value As String)
                _reason = value

            End Set
        End Property
        Public Property Time() As String
            Get
                Return _time
            End Get
            Set(ByVal value As String)
                _time = value

            End Set
        End Property
        ' Get / Set property for _notes
        Public Property Notes() As String

            Get
                Return _notes
            End Get

            Set(ByVal value As String)
                _notes = value
            End Set

        End Property
        ' Get / Set property for _dueDate
        Public Property DueDate() As Date

            Get
                Return _dueDate
            End Get

            Set(ByVal value As Date)
                _dueDate = value
            End Set

        End Property
        ' Get / Set property for inspectDate
        Public Property InspectionDate() As Date

            Get
                Return _inspectionDate
            End Get

            Set(ByVal value As Date)
                _inspectionDate = value
            End Set

        End Property

        ' Get / Set property for _netCost
        Public Property NetCost() As Double

            Get
                Return _netCost
            End Get

            Set(ByVal value As Double)
                _netCost = value
            End Set

        End Property

        ' Get/Set property for _faultLogId
        Public Property FaultLogId() As Integer

            Get
                Return _faultLogId
            End Get

            Set(ByVal value As Integer)
                _faultLogId = value
            End Set

        End Property
        ' Get/Set property for _userId
        Public Property UserId() As Integer

            Get
                Return _userId
            End Get

            Set(ByVal value As Integer)
                _userId = value
            End Set

        End Property
        ' Get/Set property for _approved
        Public Property Approved() As Integer

            Get
                Return _approved
            End Get

            Set(ByVal value As Integer)
                _approved = value
            End Set

        End Property
        ' Get/Set property for _orgId
        Public Property OrgId() As Integer

            Get
                Return _orgId
            End Get

            Set(ByVal value As Integer)
                _orgId = value
            End Set

        End Property

        'Get/Set property for _tempFaultId 
        Public Property TempFaultId() As Integer
            Get
                Return _tempFaultId
            End Get
            Set(ByVal value As Integer)
                _tempFaultId = value
            End Set
        End Property

        'Get/Set property for _recharge 
        Public Property Recharge() As Boolean
            Get
                Return _recharge
            End Get
            Set(ByVal value As Boolean)
                _recharge = value
            End Set
        End Property

        'Get/Set property for ActionUserId 
        Public Property ActionUserId() As Integer
            Get
                Return _actionUserId
            End Get
            Set(ByVal value As Integer)
                _actionUserId = value
            End Set
        End Property

        ' Get/Set property for RepairId
        Public Property RepairId() As Integer

            Get
                Return _repairId
            End Get

            Set(ByVal value As Integer)
                _repairId = value
            End Set

        End Property
#End Region

    End Class
End Namespace
