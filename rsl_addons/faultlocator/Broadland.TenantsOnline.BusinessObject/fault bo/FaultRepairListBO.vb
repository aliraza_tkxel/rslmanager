Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class FaultRepairListBO : Inherits BaseBO

#Region "Attributes"

        Private _faultRepairListID As Integer        
        Private _description As String
        Private _netCost As Double       
        Private _vat As Double
        Private _gross As Double
        Private _vatRateID As Integer
        Private _vatRate As Double        
        Private _postInspection As Integer
        Private _stockConditionItem As Integer
        Private _repairActive As Integer
        Private _areaID As Integer
        Private _itemID As Integer
        Private _parameterID As Integer
        Private _sciVatRateIdID As Integer


#End Region

#Region "Constructor"

        Public Sub New()
            _faultRepairListID = -1
           
            _description = String.Empty
            _netCost = -1
            _vat = -1
            _gross = -1
            _vatRateID = -1
            _vatRate = -1        
            _postInspection = False
            _stockConditionItem = False
            _repairActive = False
            _areaID = -1
            _itemID = -1
            _parameterID = -1
            _sciVatRateIdID = -1


        End Sub

#End Region

#Region "Properties"

        'Get/Set Property for _faultRepairListID
        Public Property FaultRepairListID() As Integer
            Get
                Return _faultRepairListID
            End Get
            Set(ByVal value As Integer)
                _faultRepairListID = value
            End Set
        End Property

        'Get/Set Property for _description
        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property

        'Get/Set Property for _netCose
        Public Property NetCost() As Double
            Get
                Return _netCost
            End Get
            Set(ByVal value As Double)
                _netCost = value
            End Set
        End Property

        'Get/Set Property for _vat
        Public Property Vat() As Double
            Get
                Return _vat
            End Get
            Set(ByVal value As Double)
                _vat = value
            End Set
        End Property

        'Get/Set Property for _gross
        Public Property Gross() As Double
            Get
                Return _gross
            End Get
            Set(ByVal value As Double)
                _gross = value
            End Set
        End Property

        'Get/Set Property for _vatRateID
        Public Property VatRateID() As Integer
            Get
                Return _vatRateID
            End Get
            Set(ByVal value As Integer)
                _vatRateID = value
            End Set
        End Property

        'Get/Set Property for _vatRate
        Public Property VatRate() As Double
            Get
                Return _vatRate
            End Get
            Set(ByVal value As Double)
                _vatRate = value
            End Set
        End Property

        'Get/Set Property for _postInspection
        Public Property PostInspection() As Integer
            Get
                Return _postInspection
            End Get
            Set(ByVal value As Integer)
                _postInspection = value
            End Set
        End Property

        'Get/Set Property for _stockConditionItem
        Public Property StockConditionItem() As Integer
            Get
                Return _stockConditionItem
            End Get
            Set(ByVal value As Integer)
                _stockConditionItem = value
            End Set
        End Property

        'Get/Set Property for _repairActive
        Public Property RepairActive() As Integer
            Get
                Return _repairActive
            End Get
            Set(ByVal value As Integer)
                _repairActive = value
            End Set
        End Property

        'Get/Set Property for _areaID
        Public Property AreaID() As Integer
            Get
                Return _areaID
            End Get
            Set(ByVal value As Integer)
                _areaID = value
            End Set
        End Property

        'Get/Set Property for _itemID
        Public Property ItemID() As Integer
            Get
                Return _itemID
            End Get
            Set(ByVal value As Integer)
                _itemID = value
            End Set
        End Property

        'Get/Set Property for _parameterID
        Public Property ParameterID() As Integer
            Get
                Return _parameterID
            End Get
            Set(ByVal value As Integer)
                _parameterID = value
            End Set
        End Property

        'Get/Set Property for _sciVatRateIdID
        Public Property SciVatRateIdID() As Integer
            Get
                Return _sciVatRateIdID
            End Get
            Set(ByVal value As Integer)
                _sciVatRateIdID = value
            End Set
        End Property
#End Region

    End Class
End Namespace


