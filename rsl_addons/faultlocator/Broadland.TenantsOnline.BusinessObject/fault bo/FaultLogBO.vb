Namespace Broadland.TenantsOnline.BusinessObject

    <Serializable()> _
    Public Class FaultLogBO : Inherits BaseBO
#Region "Attributes"
        Private _customerId As Integer
        Private _submitdate As DateTime
        Private _faultDetail As String
        Private _faultStatus As String
        Private _faultResponse As String
        Private _unreadfaultCount As Integer
        Private _faultBO As FaultBO
        Private _faultLogId As Integer
        Private _propertyId As String
        Private _tenancyId As Integer

#End Region

#Region "Constructor"
        Public Sub New()
            _customerId = -1
            _submitdate = Nothing
            _faultDetail = String.Empty
            ''  _removeFlag = -1
            _faultStatus = String.Empty
            _faultResponse = String.Empty
            _unreadfaultCount = -1
            _faultLogId = -1
            _propertyId = String.Empty
            _tenancyId = -1

        End Sub
#End Region

#Region "Properties"
        ' Get / Set property for _creationDate
        Public Property CustomerId() As Integer
            Get
                Return _customerId
            End Get
            Set(ByVal value As Integer)
                _customerId = value
            End Set
        End Property
        Public Property TenancyId() As Integer
            Get
                Return _tenancyId
            End Get
            Set(ByVal value As Integer)
                _tenancyId = value
            End Set
        End Property
        ' Get / Set property for _creationDate
        Public Property Submitdate() As Date
            Get
                Return _submitdate
            End Get
            Set(ByVal value As Date)
                _submitdate = value
            End Set
        End Property

        ' Get / Set property for _faultDetail
        Public Property FaultDetail() As String
            Get
                Return _faultDetail
            End Get
            Set(ByVal value As String)
                _faultDetail = value
            End Set
        End Property

        ' Get / Set property for _faultStatus
        Public Property FaultStatus() As String
            Get
                Return _faultStatus
            End Get
            Set(ByVal value As String)
                _faultStatus = value
            End Set
        End Property

        ' Get / Set property for _faultResponse
        Public Property FaultResponse() As String
            Get
                Return _faultResponse
            End Get
            Set(ByVal value As String)
                _faultResponse = value
            End Set
        End Property
        ' Get / Set property for _creationDate
        Public Property UnreadfaultCount() As Integer
            Get
                Return _unreadfaultCount
            End Get
            Set(ByVal value As Integer)
                _unreadfaultCount = value
            End Set
        End Property

        ' Get / Set property for _faultBO
        Public Property FaultBO() As FaultBO
            Get
                Return _faultBO
            End Get
            Set(ByVal value As FaultBO)
                _faultBO = value
            End Set
        End Property

        ' Get / Set property for _faultLogId
        Public Property FaultLogId() As Integer
            Get
                Return _faultLogId
            End Get
            Set(ByVal value As Integer)
                _faultLogId = value
            End Set
        End Property

        ' Get / Set property for _propertyId
        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(ByVal value As String)
                _propertyId = value
            End Set
        End Property
#End Region

    End Class
End Namespace
