Namespace Broadland.TenantsOnline.BusinessObject
    '' Class Name -- SQuestionBO
    '' Base Class -- BaseBO
    '' Created By  -- Waseem Hassan
    '' Create on -- 31th Oct,2008

    <Serializable()> _
Public Class SQuestionBO : Inherits CollectionBase

#Region "Attributes"
        Private _exceptionGenerated As Boolean
#End Region
#Region "Properties"
        Default Public Property Item(ByVal Index As Integer) As Object
            Get
                Return CType(List.Item(Index), Object)
            End Get
            Set(ByVal Value As Object)
                List.Item(Index) = Value
            End Set
        End Property

        Public Property IsExceptionGenerated() As Boolean
            Get
                Return _exceptionGenerated
            End Get
            Set(ByVal value As Boolean)
                _exceptionGenerated = value
            End Set
        End Property


#End Region

#Region "Functions"

        Public Function Add(ByVal Item As Object) As Integer
            Return List.Add(Item)
        End Function

#End Region


    End Class
End Namespace
