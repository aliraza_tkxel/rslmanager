Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
Public Class PriorityBO : Inherits BaseBO

#Region "Attributes"

        Private _sortBy As String
        Private _sortOrder As String
        Private _rowCount As Integer
        Private _pageIndex As Integer
        Private _priorityID As Int32
        Private _priorityName As String
        Private _responseTime As Int32
        Private _status As Boolean
        Private _days As Boolean
        Private _hours As Boolean



#End Region

#Region "Construcotr"

        Public Sub New()

            _sortBy = String.Empty
            _sortOrder = String.Empty
            _rowCount = -1
            _pageIndex = -1
            _priorityID = -1
            _priorityName = String.Empty
            _responseTime = -1
            _status = False
            _days = False
            _hours = False
        End Sub

#End Region

#Region "Properties"

        'Get/Set Property for _sortBy
        Public Property SortBy() As String
            Get
                Return _sortBy
            End Get
            Set(ByVal value As String)
                _sortBy = value
            End Set
        End Property

        'Get/Set Property for _sortOrder
        Public Property SortOrder() As String
            Get
                Return _sortOrder
            End Get
            Set(ByVal value As String)
                _sortOrder = value
            End Set
        End Property

        'Get/Set Property for _rowCount
        Public Property RowCount() As Integer
            Get
                Return _rowCount
            End Get
            Set(ByVal value As Integer)
                _rowCount = value
            End Set
        End Property

        'Get/Set Property for _pageIndex
        Public Property PageIndex() As Integer
            Get
                Return _pageIndex
            End Get
            Set(ByVal value As Integer)
                _pageIndex = value
            End Set
        End Property

        'Get/Set Property for _priorityID
        Public Property PriorityID() As Int32
            Get
                Return _priorityID
            End Get
            Set(ByVal value As Int32)
                _priorityID = value
            End Set
        End Property


        'Get/Set Property for _priorityName
        Public Property PriorityName() As String
            Get
                Return _priorityName
            End Get
            Set(ByVal value As String)
                _priorityName = value
            End Set
        End Property

        'Get/Set Property for _responseTime
        Public Property ResponseTime() As Int32
            Get
                Return _responseTime
            End Get
            Set(ByVal value As Int32)
                _responseTime = value
            End Set
        End Property

        'Get/Set Property for _status
        Public Property Status() As Boolean
            Get
                Return _status
            End Get
            Set(ByVal value As Boolean)
                _status = value
            End Set

        End Property

        ''Get/Set Property for _days
        Public Property Days() As Boolean
            Get
                Return _days
            End Get
            Set(ByVal value As Boolean)
                _days = value
            End Set
        End Property

        ''Get/Set Property for _hours
        Public Property Hours() As Boolean
            Get
                Return (_hours)
            End Get
            Set(ByVal value As Boolean)
                _hours = value
            End Set
        End Property

#End Region

    End Class
End Namespace
