Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class FaultRepairHistoryBO : Inherits BaseBO

#Region "Attributes"

        Private _repairHistoryId As Int32
        Private _journalId As Int32
        Private _itemStatusId As Int32
        Private _itemActionId As Int32
        Private _lastActionDate As DateTime
        Private _lastActionUserId As Int32
        Private _faultLogId As Int32
        Private _orgId As Int32
        Private _scopeId As Int32
        Private _title As String
        Private _notes As String

#End Region

#Region "Construcor"

        Public Sub New()
            _repairHistoryId = -1
            _journalId = -1
            _itemStatusId = -1
            _itemActionId = -1
            _lastActionDate = Nothing
            _lastActionUserId = -1
            _faultLogId = -1
            _orgId = -1
            _scopeId = -1
            _title = String.Empty
            _notes = String.Empty
        End Sub

#End Region

#Region "Properties"

        'Get/Set Property for _repairHistoryId
        Public Property RepairHistoryId() As Int32
            Get
                Return _repairHistoryId
            End Get
            Set(ByVal value As Int32)
                _repairHistoryId = value
            End Set
        End Property

        'Get/Set Property for _journalId
        Public Property JournalId() As Int32
            Get
                Return _journalId
            End Get
            Set(ByVal value As Int32)
                _journalId = value
            End Set
        End Property
        'Get/Set Property for _itemStatusId
        Public Property ItemStatusId() As Int32
            Get
                Return _itemStatusId
            End Get
            Set(ByVal value As Int32)
                _itemStatusId = value
            End Set
        End Property

        'Get/Set Property for _itemActionId
        Public Property ItemActionId() As Int32
            Get
                Return _itemActionId
            End Get
            Set(ByVal value As Int32)
                _itemActionId = value
            End Set
        End Property

        'Get/Set Property for _lastActionDate
        Public Property LastActionDate() As DateTime
            Get
                Return _lastActionDate
            End Get
            Set(ByVal value As DateTime)
                _lastActionDate = value
            End Set
        End Property

        'Get/Set Property for _lastActionUserId
        Public Property LastActionUserId() As Int32
            Get
                Return _lastActionUserId
            End Get
            Set(ByVal value As Int32)
                _lastActionUserId = value
            End Set
        End Property

        'Get/Set Property for _faultLogId
        Public Property FaultLogId() As Int32
            Get
                Return _faultLogId
            End Get
            Set(ByVal value As Int32)
                _faultLogId = value
            End Set
        End Property

        'Get/Set Property for _orgId
        Public Property OrgId() As Int32
            Get
                Return _orgId
            End Get
            Set(ByVal value As Int32)
                _orgId = value
            End Set
        End Property

        'Get/Set Property for _scopeId
        Public Property ScopeId() As Int32
            Get
                Return _scopeId
            End Get
            Set(ByVal value As Int32)
                _scopeId = value
            End Set
        End Property

        'Get/Set Property for _title
        Public Property Title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property

        'Get/Set Property for _notes
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property
#End Region

    End Class
End Namespace
