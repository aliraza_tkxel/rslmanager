Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
Public Class ElementBO : Inherits BaseBO
#Region "Attributes"

        Private _elementID As Integer
        Private _areaID As Integer
        Private _elementName As String
      


#End Region

#Region "Constructor"

        Public Sub New()

            _elementID = -1
            _areaID = -1
            _elementName = String.Empty
        End Sub

        Public Sub New(ByVal elementId As Integer, ByVal elementName As String)

            Me.ElementId = elementId
            Me.ElementName = elementName

        End Sub

#End Region

#Region "Properties"

        'Get/Set Property for _elementID
        Public Property ElementID() As Integer
            Get
                Return _elementID
            End Get
            Set(ByVal value As Integer)
                _elementID = value
            End Set
        End Property

        'Get/Set Property for _areaID
        Public Property AreaID() As Integer
            Get
                Return _areaID
            End Get
            Set(ByVal value As Integer)
                _areaID = value
            End Set
        End Property

        'Get/Set Property for _elementName
        Public Property ElementName() As String
            Get
                Return _elementName
            End Get
            Set(ByVal value As String)
                _elementName = value
            End Set
        End Property

        




#End Region

    End Class
End Namespace
