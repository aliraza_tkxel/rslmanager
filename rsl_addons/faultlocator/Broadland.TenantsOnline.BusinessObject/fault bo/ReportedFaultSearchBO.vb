Imports System

Namespace Broadland.TenantsOnline.BusinessObject

    <Serializable()> _
Public Class ReportedFaultSearchBO : Inherits BaseBO

#Region "Attributes"

        '' Holds column name by which we are sorting 
        '' rowcount is number of results we want before querying database again
        '' pageindex is page number on gridview we will use it to calculate along 
        '' with rowcount to get results in between
        Private _sortBy As String
        Private _sortOrder As String
        Private _rowCount As Integer
        Private _pageIndex As Integer
        Private _firstName As String
        Private _lastName As String
        Private _JS As Integer
        Private _date As String
        Private _dateTo As String
        Private _nature As Integer
        Private _status As Integer
        Private _text As String
        Private _isSearch As Boolean
        Private _responseId As Integer
        Private _responseNotes As String
        Private _FaultStatus As String



#End Region

#Region "Construtor"

        Public Sub New()
            _sortBy = String.Empty
            _sortOrder = String.Empty
            _rowCount = -1
            _pageIndex = 0
            _JS = -1
            _lastName = String.Empty
            _date = String.Empty
            _dateTo = String.Empty
            _nature = -1
            _status = -1
            _FaultStatus = String.Empty
            _text = String.Empty
            _isSearch = False

            _responseId = -1
            _responseNotes = String.Empty

        End Sub

#End Region

#Region "Get/Set Properties"
        ' Get / Set property for _sortBy
        Public Property SortBy() As String

            Get
                Return _sortBy
            End Get

            Set(ByVal value As String)
                _sortBy = value
            End Set

        End Property
        ' Get / Set property for _sortBy
        Public Property SortOrder() As String

            Get
                Return _sortOrder
            End Get

            Set(ByVal value As String)
                _sortOrder = value
            End Set

        End Property
        ' Get / Set property for _rowCount
        Public Property RowCount() As Integer

            Get
                Return _rowCount
            End Get

            Set(ByVal value As Integer)
                _rowCount = value
            End Set

        End Property

        ' Get / Set property for _pageIndex
        Public Property PageIndex() As Integer

            Get
                Return _pageIndex
            End Get

            Set(ByVal value As Integer)
                _pageIndex = value
            End Set

        End Property
        ' Get / Set property for _tenancyID
        Public Property JS() As Integer

            Get
                Return _JS
            End Get

            Set(ByVal value As Integer)
                _JS = value
            End Set

        End Property

        ' Get / Set property for _lastName
        Public Property LastName() As String

            Get
                Return _lastName
            End Get

            Set(ByVal value As String)
                _lastName = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property DateValue() As String

            Get
                Return _date
            End Get

            Set(ByVal value As String)
                _date = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property DateToValue() As String

            Get
                Return _dateTo
            End Get

            Set(ByVal value As String)
                _dateTo = value
            End Set

        End Property

        ' Get / Set property for _tenancyID
        Public Property Nature() As Integer

            Get
                Return _nature
            End Get

            Set(ByVal value As Integer)
                _nature = value
            End Set

        End Property


        ' Get / Set property for _tenancyID
        Public Property Status() As Integer

            Get
                Return _status
            End Get

            Set(ByVal value As Integer)
                _status = value
            End Set

        End Property
        Public Property FaultStatus() As String

            Get
                Return _FaultStatus
            End Get

            Set(ByVal value As String)
                _FaultStatus = value
            End Set

        End Property
        ' Get / Set property for _tenancyID
        Public Property TextValue() As String

            Get
                Return _text
            End Get

            Set(ByVal value As String)
                _text = value
            End Set

        End Property

        ' Get/Set property for _flagStatus
        Public Property IsSearch() As Boolean

            Get
                Return _isSearch
            End Get

            Set(ByVal value As Boolean)
                _isSearch = value
            End Set

        End Property

        ' Get/Set property for _responseId
        Public Property CustResponseId() As Integer

            Get
                Return _responseId
            End Get

            Set(ByVal value As Integer)
                _responseId = value
            End Set

        End Property

        ' Get / Set property for _responseNotes
        Public Property CustResponseNotes() As String

            Get
                Return _responseNotes
            End Get

            Set(ByVal value As String)
                _responseNotes = value
            End Set

        End Property


#End Region

    End Class
End Namespace
