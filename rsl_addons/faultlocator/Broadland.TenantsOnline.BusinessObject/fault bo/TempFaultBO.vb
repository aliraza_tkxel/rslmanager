Namespace Broadland.TenantsOnline.BusinessObject

    '' Class Name -- TempFaultBO
    '' Base Class -- BaseBO
    '' Created By  -- Noor Muhammad
    '' Create on -- 20th Oct,2008

    <Serializable()> _
    Public Class TempFaultBO : Inherits BaseBO


#Region "Attributes"

        Private _tempFaultId As Integer       
        Private _faultId As Integer
        Private _customerId As Integer
        Private _quantity As Integer
        Private _problemDays As Double
        Private _recuringProblem As Integer
        Private _communalProblem As Integer
        Private _recharge As Integer
        Private _notes As String
        Private _countRecords As Integer
        Private _contractor As Integer

#End Region

#Region "Constructor"
        Public Sub New()
            _tempFaultId = -1
            _faultId = -1
            _customerId = -1
            _quantity = -1
            _problemDays = -1
            _recuringProblem = -1
            _recharge = -1
            _communalProblem = -1
            _notes = Nothing
            _countRecords = -1
            _contractor = -1
        End Sub

#End Region

#Region "Properties"

        ' Get / Set property for _tempFaultId
        Public Property TempFaultId() As Integer
            Get
                Return _tempFaultId
            End Get
            Set(ByVal value As Integer)
                _tempFaultId = value
            End Set
        End Property

        ' Get / Set property for _faultId
        Public Property FaultId() As Integer
            Get
                Return _faultId
            End Get
            Set(ByVal value As Integer)
                _faultId = value
            End Set
        End Property

        ' Get / Set property for _customerId
        Public Property CustomerId() As Integer
            Get
                Return _customerId
            End Get
            Set(ByVal value As Integer)
                _customerId = value
            End Set
        End Property

        ' Get / Set property for _quantity
        Public Property Quantity() As Integer
            Get
                Return _quantity
            End Get
            Set(ByVal value As Integer)
                _quantity = value
            End Set
        End Property

        ' Get / Set property for _problemDays
        Public Property ProblemDays() As Integer
            Get
                Return _problemDays
            End Get
            Set(ByVal value As Integer)
                _problemDays = value
            End Set
        End Property

        ' Get / Set property for  _recuringProblem
        Public Property RecuringProblem() As Integer
            Get
                Return _recuringProblem
            End Get
            Set(ByVal value As Integer)
                _recuringProblem = value
            End Set
        End Property

        ' Get / Set property for  _recuringProblem
        Public Property Recharge() As Integer
            Get
                Return _recharge
            End Get
            Set(ByVal value As Integer)
                _recharge = value
            End Set
        End Property
        ' Get / Set property for _communalProblem
        Public Property CommunalProblem() As Integer
            Get
                Return _communalProblem
            End Get
            Set(ByVal value As Integer)
                _communalProblem = value
            End Set
        End Property

        ' Get / Set property for Notes
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property

        ' Get / Set property for _countRecords
        Public Property CountRecords() As Integer
            Get
                Return _countRecords
            End Get
            Set(ByVal value As Integer)
                _countRecords = value
            End Set
        End Property

        'Get/Set Property for _contractor
        Public Property Contractor() As Integer
            Get
                Return _contractor
            End Get
            Set(ByVal value As Integer)
                _contractor = value
            End Set
        End Property
#End Region

    End Class

End Namespace



