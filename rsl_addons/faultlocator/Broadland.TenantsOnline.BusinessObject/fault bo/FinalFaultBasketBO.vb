Namespace Broadland.TenantsOnline.BusinessObject

    '' Class Name -- FinalFaultBasket
    '' Base Class -- BaseBO
    '' Created By  -- Noor Muhammad
    '' Create on -- 24th Oct,2008

    <Serializable()> _
    Public Class FinalFaultBasketBO : Inherits BaseBO


#Region "Attributes"

        Private _faultBasketId As Integer
        Private _customerId As Integer
        Private _prefContactTime As String
        Private _submitDate As DateTime
        Private _iamHappy As Integer
        Private _propertyId As String
        Private _tenancyId As Integer
        Private _UserId As Integer

#End Region

#Region "Constructor"
        Public Sub New()
            _faultBasketId = -1
            _customerId = -1
            _prefContactTime = String.Empty
            _submitDate = Nothing
            _iamHappy = -1
            _propertyId = String.Empty
            _tenancyId = -1
            _UserId = -1
        End Sub

#End Region

#Region "Properties"

        ' Get / Set property for _faultBasketId
        Public Property FaultBasketId() As Integer
            Get
                Return _faultBasketId
            End Get
            Set(ByVal value As Integer)
                _faultBasketId = value
            End Set
        End Property

        ' Get / Set property for _customerId
        Public Property CustomerId() As Integer
            Get
                Return _customerId
            End Get
            Set(ByVal value As Integer)
                _customerId = value
            End Set
        End Property

        ' Get / Set property for _prefContactTime
        Public Property PrefContactTime() As String
            Get
                Return _prefContactTime
            End Get
            Set(ByVal value As String)
                _prefContactTime = value
            End Set
        End Property

        ' Get / Set property for _submitDate
        Public Property SubmitDate() As DateTime
            Get
                Return _submitDate
            End Get
            Set(ByVal value As DateTime)
                _submitDate = value
            End Set
        End Property

        ' Get / Set property for  _iamHappy
        Public Property IamHappy() As Integer
            Get
                Return _iamHappy
            End Get
            Set(ByVal value As Integer)
                _iamHappy = value
            End Set
        End Property

        'Get/Set Property for _propertyId
        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(ByVal value As String)
                _propertyId = value
            End Set
        End Property

        'Get/Set Property for _tenancyId
        Public Property TenancyId() As Integer
            Get
                Return _tenancyId
            End Get
            Set(ByVal value As Integer)
                _tenancyId = value
            End Set
        End Property

        'Get/Set Property for _tenancyId
        Public Property UserId() As Integer
            Get
                Return _UserId
            End Get
            Set(ByVal value As Integer)
                _UserId = value
            End Set
        End Property

#End Region

    End Class

End Namespace


