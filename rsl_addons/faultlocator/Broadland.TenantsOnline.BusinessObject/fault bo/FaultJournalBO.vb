Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class FaultJournalBO : Inherits BaseBO

#Region "Attributes"
        Private _journalId As Int32
        Private _customerId As Int32
        Private _tenancyId As Int32
        Private _propertyId As String
        Private _itemId As Int32
        Private _itemNatureId As Int32
        Private _currentItemStatusId As Int32
        Private _letterAction As Int32
        Private _creationDate As DateTime
        Private _title As String
        Private _lastActionDate As DateTime
        Private _nextActionDate As DateTime


#End Region

#Region "Constructor"

        Public Sub New()
            _journalId = -1
            _customerId = -1
            _tenancyId = -1
            _propertyId = String.Empty
            _itemId = -1
            _itemNatureId = -1
            _currentItemStatusId = -1
            _letterAction = -1
            _creationDate = Nothing
            _title = String.Empty
            _lastActionDate = Nothing
            _nextActionDate = Nothing

        End Sub

#End Region

#Region "Properties"

        'Get/Set Property for _journalId
        Public Property JournalId() As Int32
            Get
                Return _journalId
            End Get
            Set(ByVal value As Int32)
                _journalId = value
            End Set
        End Property


        'Get/Set Property for _customerId
        Public Property CustomerId() As Int32
            Get
                Return _customerId
            End Get
            Set(ByVal value As Int32)
                _customerId = value
            End Set
        End Property


        'Get/Set Property for _tenancyId
        Public Property TenancyId() As Int32
            Get
                Return _tenancyId
            End Get
            Set(ByVal value As Int32)
                _tenancyId = value
            End Set
        End Property


        'Get/Set Property for _propertyId
        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(ByVal value As String)
                _propertyId = value
            End Set
        End Property

        'Get/Set Property for _itemId
        Public Property ItemId() As Int32
            Get
                Return _itemId
            End Get
            Set(ByVal value As Int32)
                _itemId = value
            End Set
        End Property

        'Get/Set Property for _itemNatureId
        Public Property ItemNatureId() As Int32
            Get
                Return _itemNatureId
            End Get
            Set(ByVal value As Int32)
                _itemNatureId = value
            End Set
        End Property

        'Get/Set Property for _currentItemStatusId
        Public Property CurrentItemStatusId() As Int32
            Get
                Return _currentItemStatusId
            End Get
            Set(ByVal value As Int32)
                _currentItemStatusId = value
            End Set
        End Property

        'Get/Set Property for _letterAction
        Public Property LetterAction() As Int32
            Get
                Return _letterAction
            End Get
            Set(ByVal value As Int32)
                _letterAction = value
            End Set
        End Property

        'Get/Set Property for _title
        Public Property Title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property

        'Get/Set Property for _creationDate
        Public Property CreationDate() As DateTime
            Get
                Return _creationDate
            End Get
            Set(ByVal value As DateTime)
                _creationDate = value
            End Set
        End Property

        'Get/Set Property for _lastActionDate
        Public Property LastActionDate() As DateTime
            Get
                Return _lastActionDate
            End Get
            Set(ByVal value As DateTime)
                _lastActionDate = value
            End Set
        End Property

        'Get/Set Property for _nextActionDate
        Public Property NextActionDate() As DateTime
            Get
                Return _nextActionDate
            End Get
            Set(ByVal value As DateTime)
                _nextActionDate = value
            End Set
        End Property

#End Region

    End Class
End Namespace
