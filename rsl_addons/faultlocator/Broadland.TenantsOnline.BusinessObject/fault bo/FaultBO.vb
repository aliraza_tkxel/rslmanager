Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class FaultBO : Inherits BaseBO

#Region "Attributes"

        Private _faultID As Integer
        Private _elementID As Integer
        Private _locationID As Integer
        Private _areaID As Integer
        Private _priorityID As Integer
        Private _description As String
        Private _netCost As Double
        Private _inflationValue As Double
        Private _vat As Double
        Private _gross As Double
        Private _vatRateID As Integer
        Private _vatRate As Double
        Private _effectFrom As String
        Private _recharge As Integer
        Private _preInspection As Integer
        Private _postInspection As Integer
        Private _stockConditionItem As Integer
        Private _faultActive As Integer
        Private _faultAction As Integer
        Private _submitDate As String
        Private _vatName As String
        Private _isRecordRetrieved As Boolean
        Private _userId As Integer
        Private _repairId As Integer



#End Region

#Region "Constructor"

        Public Sub New()
            _faultID = -1
            _elementID = -1
            _locationID = -1
            _areaID = -1
            _priorityID = -1
            _description = String.Empty
            _netCost = -1
            _vat = -1
            _gross = -1
            _inflationValue = -1
            _vatRateID = -1
            _vatRate = -1
            _effectFrom = String.Empty
            _recharge = False
            _preInspection = False
            _postInspection = False
            _stockConditionItem = False
            _faultActive = False
            _faultAction = False
            _submitDate = Nothing
            _vatName = Nothing
            _isRecordRetrieved = False
            _userId = -1
            _repairId = -1

        End Sub

        Public Sub New(ByVal faultId As Integer, ByVal description As String)

            Me.FaultId = faultId
            Me.Description = description

        End Sub

#End Region

#Region "Properties"

        'Get/Set Property for _faultID
        Public Property FaultID() As Integer
            Get
                Return _faultID
            End Get
            Set(ByVal value As Integer)
                _faultID = value
            End Set
        End Property

        'Get/Set Property for _locationID
        Public Property LocationID() As Integer
            Get
                Return _locationID
            End Get
            Set(ByVal value As Integer)
                _locationID = value
            End Set
        End Property

        'Get/Set Property for _areaID
        Public Property AreaID() As Integer
            Get
                Return _areaID
            End Get
            Set(ByVal value As Integer)
                _areaID = value
            End Set
        End Property

        'Get/Set Property for _elementID
        Public Property ElementID() As Integer
            Get
                Return _elementID
            End Get
            Set(ByVal value As Integer)
                _elementID = value
            End Set
        End Property

        'Get/Set Property for _priorityID
        Public Property PriorityID() As Integer
            Get
                Return _priorityID
            End Get
            Set(ByVal value As Integer)
                _priorityID = value
            End Set
        End Property

        'Get/Set Property for _description
        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property

        'Get/Set Property for _netCose
        Public Property NetCost() As Double
            Get
                Return _netCost
            End Get
            Set(ByVal value As Double)
                _netCost = value
            End Set
        End Property

        'Get/Set Property for _inflationValue
        Public Property InflationValue() As Double
            Get
                Return _inflationValue
            End Get
            Set(ByVal value As Double)
                _inflationValue = value
            End Set
        End Property

        'Get/Set Property for _vat
        Public Property Vat() As Double
            Get
                Return _vat
            End Get
            Set(ByVal value As Double)
                _vat = value
            End Set
        End Property

        'Get/Set Property for _gross
        Public Property Gross() As Double
            Get
                Return _gross
            End Get
            Set(ByVal value As Double)
                _gross = value
            End Set
        End Property

        'Get/Set Property for _vatRateID
        Public Property VatRateID() As Integer
            Get
                Return _vatRateID
            End Get
            Set(ByVal value As Integer)
                _vatRateID = value
            End Set
        End Property

        'Get/Set Property for _vatRate
        Public Property VatRate() As Double
            Get
                Return _vatRate
            End Get
            Set(ByVal value As Double)
                _vatRate = value
            End Set
        End Property

        'Get/Set Property for _effectFrom
        Public Property EffectFrom() As String
            Get
                Return _effectFrom
            End Get
            Set(ByVal value As String)
                _effectFrom = value
            End Set
        End Property

        'Get/Set Property for _recharge
        Public Property Recharge() As Integer
            Get
                Return _recharge
            End Get
            Set(ByVal value As Integer)
                _recharge = value
            End Set
        End Property

        'Get/Set Property for _preInspection
        Public Property PreInspection() As Integer
            Get
                Return _preInspection
            End Get
            Set(ByVal value As Integer)
                _preInspection = value
            End Set
        End Property

        'Get/Set Property for _postInspection
        Public Property PostInspection() As Integer
            Get
                Return _postInspection
            End Get
            Set(ByVal value As Integer)
                _postInspection = value
            End Set
        End Property

        'Get/Set Property for _stockConditionItem
        Public Property StockConditionItem() As Integer
            Get
                Return _stockConditionItem
            End Get
            Set(ByVal value As Integer)
                _stockConditionItem = value
            End Set
        End Property

        'Get/Set Property for _faultActive
        Public Property FaultActive() As Integer
            Get
                Return _faultActive
            End Get
            Set(ByVal value As Integer)
                _faultActive = value
            End Set
        End Property

        'Get/Set Property for _faultAction
        Public Property FaultAction() As Integer
            Get
                Return _faultAction
            End Get
            Set(ByVal value As Integer)
                _faultAction = value
            End Set
        End Property

        'Get/Set Property for _submitDate
        Public Property SubmitDate() As String
            Get
                Return _submitDate
            End Get
            Set(ByVal value As String)
                _submitDate = value
            End Set
        End Property

        'Get/Set Property for _VatName
        Public Property VatName() As String
            Get
                Return _vatName
            End Get
            Set(ByVal value As String)
                _vatName = value
            End Set
        End Property

        ' Get/Set property for _isRecordRetrieved
        Public Property IsRecordRetrieved() As Boolean

            Get
                Return _isRecordRetrieved
            End Get

            Set(ByVal value As Boolean)
                _isRecordRetrieved = value
            End Set

        End Property

        'Get/Set property for _userId 

        Public Property UserID() As Integer
            Get
                Return _userId
            End Get
            Set(ByVal value As Integer)
                _userId = value
            End Set
        End Property


        'Get/Set property for _repairId 

        Public Property RepairId() As Integer
            Get
                Return _repairId
            End Get
            Set(ByVal value As Integer)
                _repairId = value
            End Set
        End Property



#End Region

    End Class
End Namespace
