Namespace Broadland.TenantsOnline.BusinessObject

    '' Class Name -- FaultQuestionBO
    '' Base Class -- BaseBO
    '' Created By  -- Noor Muhammad
    '' Create on -- 14th Oct,2008

    <Serializable()> _
    Public Class FaultQuestionBO : Inherits BaseBO


#Region "Attributes"

        Private _questionId As Integer
        Private _question As String        

#End Region

#Region "Constructor"
        Public Sub New()
            _questionId = -1            
            _question = String.Empty

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _questionId
        Public Property QuestionId() As Integer
            Get
                Return _questionId
            End Get
            Set(ByVal value As Integer)
                _questionId = value
            End Set
        End Property

        ' Get / Set property for _question
        Public Property Question() As String
            Get
                Return _question
            End Get
            Set(ByVal value As String)
                _question = value
            End Set
        End Property
#End Region

    End Class

End Namespace

