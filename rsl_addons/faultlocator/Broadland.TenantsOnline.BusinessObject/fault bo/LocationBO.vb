Namespace Broadland.TenantsOnline.BusinessObject

    '' Class Name -- LocationBO
    '' Base Class -- BaseBO
    '' Created By  -- Noor Muhammad
    '' Create on -- 14th Oct,2008

    <Serializable()> _
    Public Class LocationBO : Inherits BaseBO


#Region "Attributes"

        Private _locationId As Integer
        Private _locationName As String

#End Region

#Region "Constructor"
        Public Sub New()
            _locationId = -1           
            _locationName = String.Empty           

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _locationId
        Public Property LocationId() As Integer
            Get
                Return _locationId
            End Get
            Set(ByVal value As Integer)
                _locationId = value
            End Set
        End Property

        ' Get / Set property for _locationName
        Public Property LocationName() As String
            Get
                Return _locationName
            End Get
            Set(ByVal value As String)
                _locationName = value
            End Set
        End Property
#End Region

    End Class

End Namespace
