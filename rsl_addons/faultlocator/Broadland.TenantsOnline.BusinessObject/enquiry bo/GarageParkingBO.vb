Namespace Broadland.TenantsOnline.BusinessObject

    Public Class GarageParkingBO : Inherits EnquiryLogBO


#Region "Attributes"

        Private _parkingSpaceId As Integer
        Private _lookUpValueId As Integer
        Private _lookUpValueText As String

#End Region

#Region "Constructor"
        Public Sub New()

            _parkingSpaceId = -1
            _lookUpValueId = -1
            _lookUpValueText = String.Empty

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _parkingSpaceId
        Public Property ParkingSpaceID() As Integer
            Get
                Return _parkingSpaceId
            End Get
            Set(ByVal value As Integer)
                _parkingSpaceId = value
            End Set
        End Property


        ' Get / Set property for _lookUpValueId
        Public Property LookUpValueID() As Integer
            Get
                Return _lookUpValueId
            End Get
            Set(ByVal value As Integer)
                _lookUpValueId = value
            End Set
        End Property

        ' Get / Set property for _lookUpValueText
        Public Property LookUpValueText() As String
            Get
                Return _lookUpValueText
            End Get
            Set(ByVal value As String)
                _lookUpValueText = value
            End Set
        End Property

#End Region



    End Class

End Namespace
