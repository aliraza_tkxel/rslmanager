Namespace Broadland.TenantsOnline.BusinessObject

    <Serializable()> _
    Public Class ComplaintBO : Inherits EnquiryLogBO


#Region "Attributes"

        Private _complaintId As Integer
        Private _categoryId As Integer
        Private _description As String


#End Region

#Region "Constructor"
        Public Sub New()

            _complaintId = -1
            _categoryId = -1
            _description = String.Empty

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _complaintId
        Public Property ComplaintId() As Integer
            Get
                Return _complaintId
            End Get
            Set(ByVal value As Integer)
                _complaintId = value
            End Set
        End Property

        ' Get / Set property for _categoryId
        Public Property CategoryId() As Integer
            Get
                Return _categoryId
            End Get
            Set(ByVal value As Integer)
                _categoryId = value
            End Set
        End Property

        ' Get / Set property for _description
        Public Property CategoryText() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property



#End Region

    End Class

End Namespace
