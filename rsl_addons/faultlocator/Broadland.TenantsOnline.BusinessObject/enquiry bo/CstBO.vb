Namespace Broadland.TenantsOnline.BusinessObject

    Public Class CstBO : Inherits EnquiryLogBO


#Region "Attributes"

        Private _cstRequestId As Integer
        Private _requestNatureId As Integer
        Private _requestNature As String

#End Region

#Region "Constructor"
        Public Sub New()

            _cstRequestId = -1
            _requestNatureId = -1
            _requestNature = String.Empty

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _cstRequestId
        Public Property CstID() As Integer
            Get
                Return _cstRequestId
            End Get
            Set(ByVal value As Integer)
                _cstRequestId = value
            End Set
        End Property

        ' Get / Set property for _requestNatureId
        Public Property RequestNatuerID() As Integer
            Get
                Return _requestNatureId
            End Get
            Set(ByVal value As Integer)
                _requestNatureId = value
            End Set
        End Property

        ' Get / Set property for _requestNature
        Public Property RequestNature() As String
            Get
                Return _requestNature
            End Get
            Set(ByVal value As String)
                _requestNature = value
            End Set
        End Property

#End Region

    End Class

End Namespace
