Namespace Broadland.TenantsOnline.BusinessObject
    <Serializable()> _
    Public Class CustomerResponseBO : Inherits BaseBO


#Region "Attributes"

        Private _custResponseId As Integer
        Private _enquiryID As Integer
        Private _notes As String
        Private _responseDate As DateTime
        Private _journalId As Integer
        Private _itemNatureId As Integer


#End Region

#Region "Constructor"
        Public Sub New()
            _custResponseId = -1
            _enquiryID = -1
            _notes = String.Empty
            _responseDate = Nothing
            _journalId = -1
            _itemNatureId = -1

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _custResponseId
        Public Property CustomerResponseId() As Integer
            Get
                Return _custResponseId
            End Get
            Set(ByVal value As Integer)
                _custResponseId = value
            End Set
        End Property


        ' Get / Set property for _enquiryId
        Public Property EnquiryId() As Integer
            Get
                Return _enquiryId
            End Get
            Set(ByVal value As Integer)
                _enquiryId = value
            End Set
        End Property

        ' Get / Set property for _notes
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property
        ' Get / Set property for _responseDate
        Public Property ResponseDate() As DateTime
            Get
                Return _responseDate
            End Get
            Set(ByVal value As Date)
                _responseDate = value
            End Set
        End Property

        ' Get / Set property for _journalId
        Public Property JournalId() As Integer
            Get
                Return _journalId
            End Get
            Set(ByVal value As Integer)
                _journalId = value
            End Set
        End Property

        ' Get / Set property for _itemNatureId
        Public Property ItemNatureId() As Integer
            Get
                Return _itemNatureId
            End Get
            Set(ByVal value As Integer)
                _itemNatureId = value
            End Set
        End Property

#End Region

    End Class

End Namespace
