Namespace Broadland.TenantsOnline.BusinessObject

    Public Class LetterBO : Inherits EnquiryLogBO


#Region "Attributes"

        Private _letterId As Integer
        Private _historyId As Integer
        Private _letterContents As String


#End Region

#Region "Constructor"
        Public Sub New()

            _letterId = -1
            _historyId = -1
            _letterContents = String.Empty

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _letterId
        Public Property LetterID() As Integer
            Get
                Return _letterId
            End Get
            Set(ByVal value As Integer)
                _letterId = value
            End Set
        End Property

        ' Get / Set property for _historyId
        Public Property HistoryID() As Integer
            Get
                Return _historyId
            End Get
            Set(ByVal value As Integer)
                _historyId = value
            End Set
        End Property

        ' Get / Set property for _letterContents
        Public Property LetterContents() As String
            Get
                Return _letterContents
            End Get
            Set(ByVal value As String)
                _letterContents = value
            End Set
        End Property


#End Region

    End Class

End Namespace
