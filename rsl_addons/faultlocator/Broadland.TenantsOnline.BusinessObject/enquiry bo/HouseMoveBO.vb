Namespace Broadland.TenantsOnline.BusinessObject

    Public Class HouseMoveBO : Inherits EnquiryLogBO


#Region "Attributes"

        Private _transferID As Integer
        Private _localAuthorityID As Integer
        Private _developmentID As Integer

        Private _localAuthority As String
        Private _development As String

        Private _noOfBedrooms As Integer
        Private _occupantsNoGreater18 As Integer
        Private _occupantsNoBelow18 As Integer
        Private _lookUpValueID As Integer

#End Region

#Region "Constructor"
        Public Sub New()

            _transferID = -1
            _localAuthorityID = -1
            _developmentID = -1
            _localAuthority = String.Empty
            _development = String.Empty
            _noOfBedrooms = -1
            _occupantsNoGreater18 = -1
            _occupantsNoBelow18 = -1
            _lookUpValueID = -1


        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _transferID
        Public Property TransferID() As Integer
            Get
                Return _transferID
            End Get
            Set(ByVal value As Integer)
                _transferID = value
            End Set
        End Property


        ' Get / Set property for _localAuthorityID
        Public Property LocalAuthorityID() As Integer
            Get
                Return _localAuthorityID
            End Get
            Set(ByVal value As Integer)
                _localAuthorityID = value
            End Set
        End Property

        ' Get / Set property for _developmentID
        Public Property DevelopmentID() As Integer
            Get
                Return _developmentID
            End Get
            Set(ByVal value As Integer)
                _developmentID = value
            End Set
        End Property


        ' Get / Set property for _localAuthorityID
        Public Property LocalAuthority() As String
            Get
                Return _localAuthority
            End Get
            Set(ByVal value As String)
                _localAuthority = value
            End Set
        End Property

        ' Get / Set property for _developmentID
        Public Property Development() As String
            Get
                Return _development
            End Get
            Set(ByVal value As String)
                _development = value
            End Set
        End Property

        ' Get / Set property for _noOfBedrooms
        Public Property NoOfBedrooms() As String
            Get
                Return _noOfBedrooms
            End Get
            Set(ByVal value As String)
                _noOfBedrooms = value
            End Set
        End Property

        ' Get / Set property for _occupantsNoGreater18
        Public Property OccupantsNoGreater18() As Integer
            Get
                Return _occupantsNoGreater18
            End Get
            Set(ByVal value As Integer)
                _occupantsNoGreater18 = value
            End Set

        End Property

        ' Get / Set property for _occupantsNoBelow18
        Public Property OccupantsNoBelow18() As Integer
            Get
                Return _occupantsNoBelow18
            End Get
            Set(ByVal value As Integer)
                _occupantsNoBelow18 = value
            End Set

        End Property


        ' Get / Set property for _lookUpValueID
        Public Property LookUpValueID() As Integer
            Get
                Return _lookUpValueID
            End Get
            Set(ByVal value As Integer)
                _lookUpValueID = value
            End Set

        End Property

#End Region

    End Class

End Namespace
