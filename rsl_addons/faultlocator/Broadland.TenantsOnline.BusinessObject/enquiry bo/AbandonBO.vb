

Namespace Broadland.TenantsOnline.BusinessObject

    Public Class AbandonBO : Inherits EnquiryLogBO


#Region "Attributes"

        Private _abandonedId As Integer
        Private _location As String
        Private _LookUpValueID As Integer
        Private _LookUpValueText As String

#End Region

#Region "Constructor"
        Public Sub New()

            _abandonedId = 1
            _location = String.Empty
            _LookUpValueID = 1
            _LookUpValueText = String.Empty

        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _abandonedId
        Public Property AbandonedID() As Integer
            Get
                Return _abandonedId
            End Get
            Set(ByVal value As Integer)
                _abandonedId = value
            End Set
        End Property

        ' Get / Set property for _location
        Public Property Location() As String
            Get
                Return _location
            End Get
            Set(ByVal value As String)
                _location = value
            End Set
        End Property


        ' Get / Set property for _LookUpValueID
        Public Property LookUpValueID() As Integer
            Get
                Return _LookUpValueID
            End Get
            Set(ByVal value As Integer)
                _LookUpValueID = value
            End Set
        End Property

        ' Get / Set property for _LookUpValueText
        Public Property LookUpValueText() As String
            Get
                Return _LookUpValueText
            End Get
            Set(ByVal value As String)
                _LookUpValueText = value
            End Set
        End Property
#End Region

    End Class

End Namespace
