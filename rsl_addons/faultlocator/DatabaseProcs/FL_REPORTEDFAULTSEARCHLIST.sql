USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[FL_REPORTEDFAULTSEARCHLIST]

/* ===========================================================================
 '   NAME:           FL_REPORTEDFAULTSEARCHLIST
 '   DATE CREATED:   19 Nov. 2008
 '   CREATED BY:     Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To display the reported fault by the customer
 '   IN:             @noOfRows int, @offSet int, @sortColumn varchar, @sortOrder
 '   OUT:            Nothing
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
	
	(	    
		-- These Parameters used to limit and sort records
		@noOfRows  int = 50,
		@offSet    int = 0,
		
		-- column name on which sorting is performed
		@sortColumn varchar(100) = 'FL_FAULT_LOG.FaultLogID',
		@sortOrder varchar (5) = 'DESC'
				
	)
	
		
AS
	-- variables used to build whole query
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),
	        @OrderClause  varchar(8000)
	

	        
	        
    -- Begin building SELECT clause
    SET @SelectClause = 'SELECT' + 
                        CHAR(10) + CHAR(9) + 'TOP ' + CONVERT (varchar, @noOfRows) +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_LOG.FaultLogID,CONVERT(varchar,  FL_FAULT_LOG.DueDate, 103) as DueDate, FL_FAULT_LOG.CustomerID,FL_FAULT_LOG.FaultID,FL_FAULT_LOG.FaultBasketID, CONVERT(varchar, FL_FAULT_LOG.SubmitDate, 103) as SubmitDate ,FL_FAULT_LOG.ORGID,FL_FAULT_LOG.StatusID,FL_FAULT_LOG.Quantity, ' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT.Description,FL_FAULT.Recharge,FL_FAULT.NetCost,FL_FAULT.PreInspection, ' +
                        CHAR(10) + CHAR(9) + 'C__CUSTOMER.CUSTOMERID,C__CUSTOMER.FIRSTNAME, C__CUSTOMER.LASTNAME, ' +                         
                        CHAR(10) + CHAR(9) + 'ISNULL(NULLIF(ISNULL(P__PROPERTY.HOUSENUMBER,'''') + ISNULL('' '' + P__PROPERTY.ADDRESS1, '''') + ISNULL('' '' + P__PROPERTY.ADDRESS2, '''') + ISNULL('' '' + P__PROPERTY.ADDRESS3, '''') + ISNULL('', '' + P__PROPERTY.TOWNCITY, '''') + ISNULL('', '' + P__PROPERTY.COUNTY, '''') + ISNULL('', '' + P__PROPERTY.POSTCODE, '''') ,''''),''N/A'') AS PropertyAddress, ' +
                        CHAR(10) + CHAR(9) + 'FL_ELEMENT.ElementName, '+ 
                        CHAR(10) + CHAR(9) + 'FL_AREA.AreaName, ' +  
                        CHAR(10) + CHAR(9) + '(CONVERT(VARCHAR, FL_FAULT_PRIORITY.ResponseTime)+' + ' ' + 'CASE FL_FAULT_PRIORITY.Days WHEN 1 THEN  '+'''Day(s)'''+ ' WHEN 0 THEN '+'''Hour(s)'''+ 'END) as TimeFrame , ' +
                        CHAR(10) + CHAR(9) + 'FL_FAULT_STATUS.DESCRIPTION as FaultStatus, '+ 
                        CHAR(10) + CHAR(9) + '(CASE WHEN S_ORGANISATION.NAME is NULL then '+'''TBA'''+'WHEN S_ORGANISATION.NAME IS NOT NULL then S_ORGANISATION.NAME END ) as NAME,'  +
	           CHAR(10) + CHAR(9) +  'FL_FAULT_LOG.JobSheetNumber as JS_Number , FL_CO_APPOINTMENT.APPOINTMENTDATE + FL_CO_APPOINTMENT.[TIME] as AppointmentDate ' 
                    
    -- End building SELECT clause
    
       
    
    -- Begin building FROM clause
    SET @FromClause = CHAR(10) + CHAR(10)+ 'FROM ' + 
                      CHAR(10) + CHAR(9) + 'FL_FAULT_LOG INNER JOIN C__CUSTOMER ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID ' +
	      CHAR(10) + CHAR(9) + 'LEFT JOIN FL_CO_APPOINTMENT ON FL_FAULT_LOG.JobSheetNumber = FL_CO_APPOINTMENT.JobSheetNumber' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_STATUS ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID ' +
                      CHAR(10) + CHAR(9) + 'LEFT  JOIN S_ORGANISATION ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_ELEMENT ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT.ElementID = FL_ELEMENT.ElementID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_FAULT_PRIORITY ' +
                      CHAR(10) + CHAR(9) + 'ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN FL_AREA ' +
                      CHAR(10) + CHAR(9) + 'ON FL_ELEMENT.AreaID = FL_AREA.AreaID ' +
                      CHAR(10) + CHAR(9) + 'INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID'
                     
    -- End building FROM clause
                            
    
    -- Begin building OrderBy clause
    SET @OrderClause =  CHAR(10) + ' Order By ' + @sortColumn + ' ' + @sortOrder
    -- End building OrderBy clause


    -- Begin building WHERE clause
    -- This Where clause contains subquery to exclude already displayed records 
    SET @WhereClause = CHAR(10)+  CHAR(10) + 'WHERE (' +
                        CHAR(10) + CHAR (9) + @sortColumn + ' NOT IN' + 
                        CHAR(10) + CHAR (9) + '( SELECT TOP ' + CONVERT(varchar, (@noOfRows * @offSet)) +
                        ' ' + @sortColumn +
                        + @FromClause + @OrderClause + ')'+
                        CHAR(10) + CHAR (9) + 'AND 1 = 1  )'                 
     
    -- End building WHERE clause


    PRINT (@SelectClause + @FromClause  + @WhereClause + @OrderClause)

    
    EXEC (@SelectClause + @FromClause + @WhereClause +  @OrderClause)
    
    

RETURN


