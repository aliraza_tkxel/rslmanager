Imports Broadland.TenantsOnline.BusinessObject

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters

Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.DataAccess

Public Class FaultManager

#Region "Functions"

    Public Function GetFaultSearchData(ByRef faultSearchBO As FaultSearchBO) As DataSet
        Dim faultDS As New DataSet
        Dim faultDAL As FaultDAL = New FaultDAL()
        Try
            faultDAL.GetFaultSearchData(faultDS, faultSearchBO)
            Return faultDS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function

    Public Function GetFaultTransactionlogSearchData(ByRef faultTransactionLogBO As FaultTransactionLogBO) As DataSet
        Dim faultTransDs As New DataSet
        Dim faultDAL As FaultDAL = New FaultDAL()
        Try
            faultDAL.GetFaultTransactionLogSearchData(faultTransDs, faultTransactionLogBO)
            Return faultTransDs
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function

    Public Function GetFaultRowCount(ByRef faultSearchBO As FaultSearchBO) As Integer

        Dim rowCount As Integer = 0

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            rowCount = faultDAL.GetFaultRowCount()

        Catch ex As Exception
            faultSearchBO.IsExceptionGenerated = True
            faultSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function

    Public Function GetFaultTransactionLogRowCount(ByRef faultTransactionBO As FaultTransactionLogBO) As Integer

        Dim rowCount As Integer = 0

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            rowCount = faultDAL.GetFaultTransactionLogRowCount()

        Catch ex As Exception
            faultTransactionBO.IsExceptionGenerated = True
            faultTransactionBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function

    Public Function GetFaultSearchRowCount(ByRef faultSearchBO As FaultSearchBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            rowCount = faultDAL.GetFaultSearchRowCount(faultSearchBO)

        Catch ex As Exception

            faultSearchBO.IsExceptionGenerated = True
            faultSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function

    Public Function GetFaultTransactionLogSearchRowCount(ByRef faultTransactionLogBO As FaultTransactionLogBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            rowCount = faultDAL.GetFaultTransactionLogSearchRowCount(faultTransactionLogBO)

        Catch ex As Exception

            faultTransactionLogBO.IsExceptionGenerated = True
            faultTransactionLogBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function

    Public Function GetFaultPrioritySearchRowCount(ByRef priorityBO As PriorityBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            rowCount = faultDAL.GetFaultPrioritySearchRowCount()

        Catch ex As Exception

            PriorityBO.IsExceptionGenerated = True
            PriorityBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function

    Public Function GetReportedFaultSearchRowCount(ByRef reportedFaultSearchBO As ReportedFaultSearchBO) As Integer
        'Public Function GetReportedFaultSearchRowCount() As Integer

        Dim rowCount As Integer = 0

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            'rowCount = faultDAL.GetReportedFaultSearchRowCount()
            rowCount = faultDAL.GetReportedFaultRowCount(reportedFaultSearchBO)

        Catch ex As Exception
            reportedFaultSearchBO.IsExceptionGenerated = True
            reportedFaultSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function

    Public Function GetReportedFaultReportSearchRowCount(ByRef reportedFaultSearchBO As ReportedFaultSearchBO) As Integer

        Dim rowCount As Integer = 0

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            'rowCount = faultDAL.GetReportedFaultSearchRowCount()
            rowCount = faultDAL.GetReportedFaultReportRowCount(reportedFaultSearchBO)

        Catch ex As Exception
            reportedFaultSearchBO.IsExceptionGenerated = True
            reportedFaultSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function


#End Region

#Region "Methods"

#Region "Add New Fault"

    Public Sub AddNewFault(ByRef faultBO As FaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.AddNewFault(faultBO)

        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub

#End Region

#Region "Amend A Fault"
    Public Sub AmendFault(ByRef faultBO As FaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.AmendFault(faultBO)

        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub



#End Region

#Region "Amend Element Name"
    Public Sub AmendElementName(ByRef elementBO As ElementBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.AmendElementName(elementBO)

        Catch ex As Exception
            elementBO.IsExceptionGenerated = True
            elementBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub
#End Region

#Region "Get Fault Values"
    Public Sub GetFaultValues(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetFaultValues(faultDS, faultBO)

        Catch ex As Exception

            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get Temp Fault Values"
    Public Sub GetTempFaultValues(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetTempFaultValues(faultDS, faultBO)

        Catch ex As Exception

            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get Fault Values for PostInspection"
    Public Sub GetPostInspecitonFault(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetPostInspecitonFault(faultDS, faultBO)

        Catch ex As Exception

            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get Post Inspeciton Fault Update"

    Public Sub GetPostInspecitonFaultUpdate(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetPostInspecitonFaultUpdate(faultDS, faultBO)

        Catch ex As Exception

            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get Preinspection Popup data"
    Public Sub GetPreinspectionPopupData(ByRef PIDS As DataSet, ByRef faultBO As FaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetPreinspectionPopupData(PIDS, faultBO)

        Catch ex As Exception

            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub

#End Region

#Region "Get Customer Info"
    Public Sub GetCustomerInfo(ByRef custDS As DataSet, ByRef customerBO As CustomerBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetCustomerInfo(custDS, customerBO)

        Catch ex As Exception

            customerBO.IsExceptionGenerated = True
            customerBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get Fault Priority Values"
    Public Sub GetPriorityValues(ByRef priorityDS As DataSet, ByRef priorityBO As PriorityBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetPriorityValues(priorityDS, priorityBO)

        Catch ex As Exception

            priorityBO.IsExceptionGenerated = True
            priorityBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get Vat Values"

    Public Sub GetVatValues(ByRef vatBO As VatBO)
        Try
            Dim faultDAL As New FaultDAL()
            faultDAL.getVatValues(vatBO)
        Catch ex As Exception
            vatBO.IsExceptionGenerated = True
            vatBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub

#End Region

#Region "Get Fault Priorities"
    Public Sub GetFaultPriorities(ByRef priorityDs As DataSet, ByRef priorityBO As PriorityBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetFaultPriorities(priorityDs, priorityBO)

        Catch ex As Exception
            priorityBO.IsExceptionGenerated = True
            priorityBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Add/Amend Priority"

    Public Sub AddAmendPriority(ByRef priorityBO As PriorityBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.AddAmendPriority(priorityBO)
        Catch ex As Exception
            priorityBO.IsExceptionGenerated = True
            priorityBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Sub

#End Region

#Region "GetPricingControlData"
    Public Sub getPricingControlData(ByVal spName As String, ByRef faultBO As FaultBO, ByVal inParam As String, ByVal inParamName As String)
        Try
            Dim objDAL As New FaultDAL
            objDAL.getPricingControlData(spName, faultBO, inParam, inParamName)
        Catch ex As Exception
            ''Execute the exception policy against this type of exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub
#End Region

#Region "AddPricingControlData"
    Public Sub AddPricingControlData(ByRef faultBO As FaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.AddPricingControlData(faultBO)

        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Sub
#End Region

#Region "Add New Fault Journal"

    Public Function AddNewJournal(ByRef faultJournalBO As FaultJournalBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            Return faultDAL.AddNewJournal(faultJournalBO)

        Catch ex As Exception
            faultJournalBO.IsExceptionGenerated = True
            faultJournalBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return -1

        End Try

    End Function

#End Region

#Region "Add New Fault RepairHistory"

    Public Sub AddNewRepairHistory(ByRef faultRepairHistoryBO As FaultRepairHistoryBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.AddNewRepairHistory(faultRepairHistoryBO)

        Catch ex As Exception
            faultRepairHistoryBO.IsExceptionGenerated = True
            faultRepairHistoryBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub

#End Region

#Region "Save Inspection record"
    Public Sub SaveInspectionRecord(ByRef piBO As PreInspectBO)
        Try
            Dim piDAL As FaultDAL = New FaultDAL()
            piDAL.SaveInspectionRecord(piBO)

        Catch ex As Exception

            piBO.IsExceptionGenerated = True
            piBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub

    Public Sub SaveAppointment(ByRef piBO As PreInspectBO)
        Try
            Dim piDAL As FaultDAL = New FaultDAL()
            piDAL.SaveInspectionAppointment(piBO)

        Catch ex As Exception

            piBO.IsExceptionGenerated = True
            piBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub

#End Region

#Region "Save PreInspection Report Data"
    Public Sub UpdatePreInspectionReportDataBL(ByRef piBO As PreInspectBO)
        Try
            Dim piDAL As FaultDAL = New FaultDAL()
            piDAL.UpdatePreInspectionReportData(piBO)

        Catch ex As Exception

            piBO.IsExceptionGenerated = True
            piBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Save ReactiveRepiarJournal Record record"
    Public Sub SavePreInspectionRecord(ByRef piBO As PreInspectBO)
        Try
            Dim piDAL As FaultDAL = New FaultDAL()
            piDAL.SaveReactiveRepairUpdate(piBO)

        Catch ex As Exception

            piBO.IsExceptionGenerated = True
            piBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Save Inspection record with contractor"
    Public Sub SaveInspectionRecordContractor(ByRef piBO As PreInspectBO)
        Try
            Dim piDAL As FaultDAL = New FaultDAL()
            piDAL.SaveInspectionRecordContractor(piBO)

        Catch ex As Exception

            piBO.IsExceptionGenerated = True
            piBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get ReporteFault record"
    Public Sub ReportedFaultData(ByRef rfDS As DataSet, ByRef rfSearchBO As ReportedFaultSearchBO)
        Try
            Dim rfDAL As FaultDAL = New FaultDAL()
            rfDAL.GetReportedFault(rfDS, rfSearchBO)

        Catch ex As Exception

            rfSearchBO.IsExceptionGenerated = True
            rfSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub

#End Region

#Region "Get ReportedFault Report"
    Public Sub ReportedFaultReport(ByRef rfDS As DataSet, ByRef rfSearchBO As ReportedFaultSearchBO)
        Try
            Dim rfDAL As FaultDAL = New FaultDAL()
            rfDAL.GetReportedFaultReport(rfDS, rfSearchBO)

        Catch ex As Exception

            rfSearchBO.IsExceptionGenerated = True
            rfSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub

#End Region

#Region "Get ReportedFault Report Export"
    Public Sub ReportedFaultReportExport(ByRef rfDSExport As DataSet, ByRef rfSearchBO As ReportedFaultSearchBO)
        Try
            Dim rfDAL As FaultDAL = New FaultDAL()
            rfDAL.GetReportedFaultReportExport(rfDSExport, rfSearchBO)

        Catch ex As Exception

            rfSearchBO.IsExceptionGenerated = True
            rfSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub

#End Region

#Region "Get Job Sheet update Data"
    Public Function GetJSSummaryUpdateData(ByRef JobSheetDS As DataSet, ByRef faultLogId As Integer)

        'Create the object of contractor dal
        Dim JSDAL As FaultDAL = New FaultDAL()
        Try
            JSDAL.GetJSSummaryUpdateData(JobSheetDS, faultLogId)
            Return Nothing
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#Region "Get Job Sheet Asbestos update Data"
    Public Function GetJSSummaryAsbestosUpdateData(ByRef JobSheetDS As DataSet, ByRef faultLogId As Integer)

        'Create the object of contractor dal
        Dim JSDAL As FaultDAL = New FaultDAL()
        Try
            JSDAL.GetJSSummaryAsbestosUpdateData(JobSheetDS, faultLogId)
            Return Nothing
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#Region "Get Property Summary Asbestos Data"
    Public Function GetPropertySummaryAsbestosData(ByRef JobSheetDS As DataSet, ByRef CustomerId As Integer)

        'Create the object of contractor dal
        Dim JSDAL As FaultDAL = New FaultDAL()
        Try
            JSDAL.GetPropertySummaryAsbestosData(JobSheetDS, CustomerId)
            Return Nothing
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#Region "Get Fault Basket for Update"
    Public Function GetFaultBasketUpdate(ByVal faultLogId As Integer, ByRef faultDS As DataSet)

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDS = faultDAL.GetFaultBasketUpdate(faultLogId)

        Catch ex As Exception

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function
#End Region

#Region "Update Contractor for fault log"

    Public Sub UpdateFaultLogContractor(ByVal faultLogId As Integer, ByVal orgId As Integer, ByVal notes As String, ByVal mail As Boolean)

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.UpdateFaultLogContractor(faultLogId, orgId, notes, mail)

        Catch ex As Exception

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try


    End Sub
#End Region

#Region "GetCustomerReportedFaults"
    'This function is for RSL Manager
    Public Function GetCustomerReportedFaults(ByRef faultLogBO As FaultLogBO, ByRef faultDS As DataSet)

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.GetCustomerReportedFaults(faultLogBO, faultDS)

        Catch ex As Exception

            faultLogBO.IsExceptionGenerated = True
            faultLogBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function
#End Region

#Region "SearchFaultRepairList"

    Public Function SearchFaultRepairList(ByRef faultRepListBO As FaultRepairListBO, ByVal faultRepairListDS As DataSet)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.SearchFaultRepairList(faultRepListBO, faultRepairListDS)

            Return Nothing

        Catch ex As Exception

            faultRepListBO.IsExceptionGenerated = True
            faultRepListBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            Return Nothing

        End Try
    End Function

#End Region

#Region "Add New Repair"

    Public Sub AddNewRepair(ByRef faultRepListBO As FaultRepairListBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.AddNewRepair(faultRepListBO)

        Catch ex As Exception
            faultRepListBO.IsExceptionGenerated = True
            faultRepListBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub

#End Region

#Region "Get Repair Values"
    Public Sub GetRepairValues(ByRef faultDS As DataSet, ByRef faultRepListBO As FaultRepairListBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetRepairValues(faultDS, faultRepListBO)

        Catch ex As Exception

            faultRepListBO.IsExceptionGenerated = True
            faultRepListBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Amend A Repair"
    Public Sub AmendRepair(ByRef faultRepListBO As FaultRepairListBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.AmendRepair(faultRepListBO)

        Catch ex As Exception
            faultRepListBO.IsExceptionGenerated = True
            faultRepListBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub

#End Region

#Region "Is Allowed TO Cancel OR Assgin Contractor "
    Public Function IsAllowedTOCancelORAssginContractor(ByVal faultLogId As String, ByVal fLogBO As FaultLogBO) As Integer
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            Return faultDAL.IsAllowedTOCancelORAssginContractor(faultLogId)


        Catch ex As Exception
            fLogBO.IsExceptionGenerated = True
            fLogBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Function

#End Region

#Region "Is Fault DisApproved By Contractor "
    Public Function IsFaultDisApprovedByContractor(ByVal faultLogId As String, ByVal fLogBO As FaultLogBO) As Integer
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            Return faultDAL.IsFaultDisApprovedByContractor(faultLogId)


        Catch ex As Exception
            fLogBO.IsExceptionGenerated = True
            fLogBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Function

#End Region

#Region " Check Reported Faults"
    Public Function CheckPropertyReportedFaults(ByRef faultDS As DataSet, ByVal objReportedFaults As FaultLogBO)
        Try
            'Create the obj of DAL
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.CheckPropertyReportedFaults(faultDS, objReportedFaults)            
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
        End Try
        Return Nothing
    End Function

#End Region

#Region "Check Gas Servicing Appointments "
    Public Sub CheckGasServicingAppointments(ByRef customerDS As DataSet, ByVal objFaultLog As FaultLogBO)
        Try
            'Create the obj of DAL
            Dim objDAL As FaultDAL = New FaultDAL()
            objDAL.CheckGasServicingAppointments(customerDS, objFaultLog)
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
        End Try

    End Sub
#End Region
#End Region

#Region "Functions from TO "
#Region "Functions"

#Region "GetLocationName"
    Function GetLocationName(ByRef locationBO As LocationBO)
        Try
            'Instanticale the DAL object
            Dim objFaultDAL As FaultDAL = New FaultDAL()

            'Call DAL method to get the location name against location id
            objFaultDAL.GetLocationName(locationBO)

            'Check the status
            If locationBO.IsFlagStatus Then
                Return True
            End If

        Catch ex As Exception
            locationBO.IsExceptionGenerated = True
            locationBO.ExceptionMsg = ex.Message

            'Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function

#End Region

#Region "GetCustomerFault"

    'Function gets called to retrieve the fault 
    Public Function GetCustomerFault(ByVal customerID As Integer, ByVal faultStatus As String) As DataSet

        Try

            ''Instantiate DAL object
            Dim faultDAL As FaultDAL = New FaultDAL()

            ''Call DAL method to retrieve information
            Dim dsFaults As DataSet = faultDAL.GetCustomerFaults(customerID, faultStatus)
            Return dsFaults
        Catch ex As Exception

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Throw
        End Try
        Return Nothing
    End Function


#End Region

#Region "GetUnreadFaultCount"
    '' Function gets called to retrieve the number of unread faults
    Public Function GetUnreadFaultCount(ByRef faultLogBO As FaultLogBO, ByVal faultStatus As String) As Boolean

        Try

            ''Instantiate DAL object
            Dim faultDAL As FaultDAL = New FaultDAL()

            ''Call DAL method to retrieve information
            faultDAL.GetUnreadFaultCount(faultLogBO, faultStatus)
            If (faultLogBO.IsFlagStatus) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            faultLogBO.IsExceptionGenerated = True
            faultLogBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Function
#End Region

#Region "SaveSQuestions"
    '' Function gets called to retrieve the number of unread faults
    Public Sub SaveSQuestions(ByRef sQuestionBO As SQuestionBO, ByRef sAnswerBO As SQuestionBO, ByVal faultLogID As Integer)

        Try

            ''Instantiate DAL object
            Dim faultDAL As FaultDAL = New FaultDAL()

            ''Call DAL method to store information
            faultDAL.SaveSQuestions(sQuestionBO, sAnswerBO, faultLogID)

        Catch ex As Exception
            sQuestionBO.IsExceptionGenerated = True

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Sub
#End Region

#Region "GetElements"
    Public Function GetElements(ByRef elementBO As ElementBO, ByRef elementList As FaultElementList)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetElements(elementBO, elementList)

        Catch ex As Exception
            elementBO.IsExceptionGenerated = True
            elementBO.ExceptionMsg = ex.Message

            'Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return Nothing
    End Function
#End Region

#Region "GetFaults"
    Public Function GetFaults(ByRef faultBO As FaultBO, ByRef faultList As FaultList)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()
            faultDAL.GetFaults(faultBO, faultList)

            'Note: we'll check whether exception is generated over here or not and log it
            'Check success scenario

            If faultBO.IsFlagStatus Then
                Return True
            End If

        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            'Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return False

    End Function
#End Region

#Region "GetFaultQuestion"
    Public Function GetFaultQuestion(ByRef faultQBO As FaultQuestionBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.GetFaultQuestion(faultQBO)

        Catch ex As Exception

            faultQBO.IsExceptionGenerated = True
            faultQBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return Nothing
    End Function
#End Region

#Region "SaveFaultTemporarily"

    Public Function SaveFaultTemporarily(ByRef tempFaultBO As TempFaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.SaveFaultTemporarily(tempFaultBO)

        Catch ex As Exception

            tempFaultBO.IsExceptionGenerated = True
            tempFaultBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return Nothing
    End Function
#End Region

#Region "GetRechargeCost"

    Public Function GetRechargeCost(ByRef faultRechargeBO As FaultRechargeBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.GetRechargeCost(faultRechargeBO)

        Catch ex As Exception

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return Nothing
    End Function
#End Region

#Region "GetTempFaultBasket"
    Public Function GetTempFaultBasket(ByRef tempFaultBO As TempFaultBO, ByRef tempFaultDS As DataSet)

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.GetTempFaultBasket(tempFaultBO, tempFaultDS)

        Catch ex As Exception

            tempFaultBO.IsExceptionGenerated = True
            tempFaultBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function
#End Region

#Region "DeleteTempFault"
    Public Function DeleteTempFault(ByRef tempFaultBO As TempFaultBO)
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.DeleteTempFault(tempFaultBO)

        Catch ex As Exception

            tempFaultBO.IsExceptionGenerated = True
            tempFaultBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function
#End Region

#Region "saveFinalFaultBasket"
    Public Function saveFinalFaultBasket(ByRef finalFaultBO As FinalFaultBasketBO) As Integer
        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            Return faultDAL.saveFinalFaultBasket(finalFaultBO)

        Catch ex As Exception

            finalFaultBO.IsExceptionGenerated = True
            finalFaultBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function

#End Region

#Region "UpdateFaultQuantity"

    Public Function UpdateFaultQuantity(ByRef tempFaultBO As TempFaultBO)

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.UpdateFaultQuantity(tempFaultBO)

        Catch ex As Exception

            tempFaultBO.IsExceptionGenerated = True
            tempFaultBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function
#End Region

#Region "UpdateFaultContractor"

    Public Function UpdateFaultContractor(ByRef tempFaultBO As TempFaultBO)

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.UpdateFaultContractor(tempFaultBO)

        Catch ex As Exception

            tempFaultBO.IsExceptionGenerated = True
            tempFaultBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function
#End Region

#Region "LoadBreadCrumb"
    Public Function LoadBreadCrumb(ByRef breadBO As BreadCrumbBO)

        Try
            Dim faultDAL As FaultDAL = New FaultDAL()

            faultDAL.LoadBreadCrumb(breadBO)

        Catch ex As Exception

            breadBO.IsExceptionGenerated = True
            breadBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return Nothing
    End Function

#End Region

#End Region
#End Region



End Class
