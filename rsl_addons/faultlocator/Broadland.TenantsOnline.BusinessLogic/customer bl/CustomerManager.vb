Imports System

Imports System.ComponentModel

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.DataAccess


Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters

Namespace Broadland.TenantsOnline.BusinessLogic

    Public Class CustomerManager

#Region "Methods"

        '#Region "AuthenticateCustomer Method"

        '        '' Function gets called when user tries to SignIn
        '        Public Sub AuthenticateCustomer(ByRef custBO As CustomerHeaderBO)
        '            Try
        '                Dim custDAL As CustomerDAL = New CustomerDAL()
        '                custDAL.AuthenticateCustomer(custBO)


        '                ''Note: we'll check whether exception is generated over here or not and log it
        '            Catch ex As Exception
        '                custBO.IsExceptionGenerated = True
        '                custBO.ExceptionMsg = ex.Message

        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        '            End Try


        '        End Sub

        '#End Region

        '#Region "PasswordAssitance Method"

        '        '' Function gets called when user forgot his/her password
        '        Public Sub PasswordAssitance(ByRef custBO As CustomerHeaderBO)
        '            Try
        '                Dim custDAL As CustomerDAL = New CustomerDAL()
        '                custDAL.CustomerPasswordAssitance(custBO)

        '                ''Note: we'll check whether exception is generated over here or not and log it
        '            Catch ex As Exception
        '                custBO.IsExceptionGenerated = True
        '                custBO.ExceptionMsg = ex.Message

        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        '            End Try



        '        End Sub

        '#End Region

        '#Region "ChangeEmail Method"


        '        '' Function gets called when user attempts to change his/her email(userName)
        '        Public Sub ChangeEmail(ByRef custHeadBO As CustomerHeaderBO)
        '            Try
        '                Dim custDAL As CustomerDAL = New CustomerDAL()
        '                custDAL.ChangeEmail(custHeadBO)

        '                ''Note: we'll check whether exception is generated over here or not and log it

        '            Catch ex As Exception
        '                custHeadBO.IsExceptionGenerated = True
        '                custHeadBO.ExceptionMsg = ex.Message

        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        '            End Try


        '        End Sub


        '#End Region

        '#Region "Password Method"


        '        '' Function gets called when user attempts to change his/her password
        '        Public Sub ChangePassword(ByRef custHeadBO As CustomerHeaderBO)
        '            Try
        '                Dim custDAL As CustomerDAL = New CustomerDAL()
        '                custDAL.ChangePassword(custHeadBO)

        '                ''Note: we'll check whether exception is generated over here or not and log it

        '            Catch ex As Exception
        '                custHeadBO.IsExceptionGenerated = True
        '                custHeadBO.ExceptionMsg = ex.Message

        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        '            End Try


        '        End Sub


        '#End Region

        '#Region "ManageFinancialInfo Function"

        '        '' Function gets called when user attempts to change his/her email(userName)
        '        Public Sub ManageFinancialInfo(ByRef custFinBO As CustomerFinancialBO, ByRef custHeadBO As CustomerHeaderBO, ByVal accBalOnly As Boolean)
        '            Try
        '                Dim custDAL As CustomerDAL = New CustomerDAL()
        '                custDAL.ManageFinancialInfo(custFinBO, custHeadBO, False)

        '                ''Note: we'll check whether exception is generated over here or not and log it

        '            Catch ex As Exception
        '                custHeadBO.IsExceptionGenerated = True
        '                custHeadBO.ExceptionMsg = ex.Message

        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        '            End Try

        '        End Sub


        '#End Region

        '#Region "RentStatement Methods"


        '        Public Sub RentStatement(ByRef rentDS As DataSet, ByVal custRentBO As CustomerRentBO)

        '            Try
        '                Dim custDAL As CustomerDAL = New CustomerDAL()
        '                custDAL.RentAccount(rentDS, custRentBO)

        '                '' If no rent/account statemnet found, not caluclating housing balance
        '                If custRentBO.IsFlagStatus Then

        '                    '' call to Me.CalculateHousingBalance
        '                    Me.CalcualteHousingBalance(custRentBO)
        '                    custDAL.GetCustomerPeriodAccountBalance(custRentBO)
        '                    Me.CalcualteAccountBalance(custRentBO)

        '                End If

        '                ''Note: we'll check whether exception is generated over here or not and log it


        '            Catch ex As Exception
        '                custRentBO.IsExceptionGenerated = True
        '                custRentBO.ExceptionMsg = ex.Message

        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        '            End Try

        '        End Sub

        '        '' This will calculate Housing balance
        '        Private Sub CalcualteHousingBalance(ByRef custRentBO As CustomerRentBO)

        '            '' If AntHB greater than 0, we set HousingBalance to it 
        '            '' Otherwise AdvHb willl be set to HousingBalance

        '            If custRentBO.AntHB > 0 Then

        '                custRentBO.HousingBalance = custRentBO.AntHB

        '            Else

        '                custRentBO.HousingBalance = custRentBO.AdvHB

        '            End If

        '        End Sub

        '        '' This will calculate Housing balance
        '        Private Sub CalcualteAccountBalance(ByRef custRentBO As CustomerRentBO)

        '            '' If AntHB greater than 0, we set AccountBalance to it 
        '            '' Otherwise AdvHb willl be set to AccountBalance

        '            If custRentBO.AntHB > 0 Then

        '                custRentBO.AccountBalance = custRentBO.AccountBalance - custRentBO.AntHB

        '            Else

        '                custRentBO.AccountBalance = custRentBO.AccountBalance + custRentBO.AdvHB

        '            End If

        '        End Sub


        '#End Region

#End Region

#Region "Functions"

        '#Region "CustomerRegistration"

        '        '' Function gets called when user needs to register
        '        Public Function CustomerRegistration(ByRef custBO As CustomerBO) As Boolean



        '            Try
        '                ''Instantiate DAL object
        '                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

        '                ''Call DAL method to varify provided information
        '                objCustomerDAL.CustomerRegistration(custBO)

        '                ''Check success scenario
        '                If custBO.IsFlagStatus Then
        '                    Return True
        '                End If

        '            Catch ex As Exception
        '                custBO.IsExceptionGenerated = True
        '                custBO.ExceptionMsg = ex.Message

        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        '            End Try

        '            Return False

        '        End Function

        '#End Region

        '#Region "HomePageRelated"

        '        '' Function gets called to retrieve information to be displayed on home page
        '        Public Function GetCustomerInfo(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailsInfo As Boolean) As Boolean

        '            Try

        '                ''Instantiate DAL object
        '                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

        '                ''Call DAL method to retrieve information
        '                objCustomerDAL.GetCustomerInfo(custDetailsBO, getDetailsInfo)

        '                If (custDetailsBO.IsFlagStatus) Then
        '                    Return True
        '                Else
        '                    Return False
        '                End If
        '            Catch ex As Exception
        '                custDetailsBO.IsExceptionGenerated = True
        '                custDetailsBO.ExceptionMsg = ex.Message

        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        '            End Try

        '        End Function

        '#End Region

#Region "GetCustomerDetails"

        '' Function gets called to retrieve information to be displayed on customer details page
        Public Function GetCustomerDetails(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailedInfo As Boolean) As Boolean

            Try

                ''Instantiate DAL object
                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

                ''Call DAL method to retrieve information
                objCustomerDAL.GetCustomerDetailsInfo(custDetailsBO, True)

                If (custDetailsBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                custDetailsBO.IsExceptionGenerated = True
                custDetailsBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function

#End Region
       
#Region "GetTerminationInfo"

        Public Function GetTerminationInfo(ByRef custAddressBO As CustomerAddressBO, ByRef terminBO As TerminationBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetTerminationInfo(custAddressBO, terminBO)

                If (terminBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                terminBO.IsExceptionGenerated = True
                terminBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function

#End Region

#Region "GetComplaintInfo"

        Public Function GetComplaintInfo(ByRef custAddressBO As CustomerAddressBO, ByRef cmpltBO As ComplaintBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetComplaintInfo(custAddressBO, cmpltBO)

                If (cmpltBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                cmpltBO.IsExceptionGenerated = True
                cmpltBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function

#End Region

#Region "GetASBInfo"

        Public Function GetASBInfo(ByRef custAddressBO As CustomerAddressBO, ByRef abBO As AsbBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetASBInfo(custAddressBO, abBO)

                If (abBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                abBO.IsExceptionGenerated = True
                abBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function

#End Region

#Region "GetGarageParkingInfo"

        Public Function GetGarageParkingInfo(ByRef custAddressBO As CustomerAddressBO, ByRef gpBO As GarageParkingBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetGarageParkingInfo(custAddressBO, gpBO)

                If (gpBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                gpBO.IsExceptionGenerated = True
                gpBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function

#End Region

#Region "GetAbandonInfo"
        Public Function GetAbandonInfo(ByRef custAddressBO As CustomerAddressBO, ByRef abndBO As AbandonBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetAbandonInfo(custAddressBO, abndBO)

                If (abndBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                abndBO.IsExceptionGenerated = True
                abndBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function
#End Region

#Region "GetTransferInfoInfo"
        Public Function GetTransferInfo(ByRef custAddressBO As CustomerAddressBO, ByRef houseTransBO As HouseMoveBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetTransferInfo(custAddressBO, houseTransBO)

                If (houseTransBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                houseTransBO.IsExceptionGenerated = True
                houseTransBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function
#End Region

#Region "GetClearDirectDebitRentInfo"
        Public Function GetClearDirectDebitRentInfo(ByRef custAddressBO As CustomerAddressBO, ByRef enqBO As EnquiryLogBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetClearDirectDebitRentInfo(custAddressBO, enqBO)

                If (enqBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                enqBO.IsExceptionGenerated = True
                enqBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function
#End Region

#Region "GetCstRequestInfo"
        Public Function GetCstRequestInfo(ByRef custAddressBO As CustomerAddressBO, ByRef cstReqBO As CstBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetCstRequestInfo(custAddressBO, cstReqBO)

                If (cstReqBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                cstReqBO.IsExceptionGenerated = True
                cstReqBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function
#End Region

#Region "GetClearArrearInfo"
        Public Function GetClearArrearInfo(ByRef custAddressBO As CustomerAddressBO, ByRef enqBO As EnquiryLogBO) As Boolean
            Try

                ''Instantiate DAL object
                Dim objCustDAL As CustomerDAL = New CustomerDAL

                ''Call DAL method to retrieve information
                objCustDAL.GetClearArrearInfo(custAddressBO, enqBO)

                If (enqBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                enqBO.IsExceptionGenerated = True
                enqBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function
#End Region

#Region "GetCustomerContactDetail For Fault Locator"

        '' Function gets called to retrieve information to be displayed on customer details page
        Public Function GetCustomerContactDetail(ByRef custDetailsBO As CustomerDetailsBO) As Boolean

            Try

                ''Instantiate DAL object
                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

                ''Call DAL method to retrieve information
                objCustomerDAL.GetCustomerContactDetail(custDetailsBO)

                If (custDetailsBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                custDetailsBO.IsExceptionGenerated = True
                custDetailsBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Function

#End Region

#Region "SaveCustomerContactDetail"
        Public Function UpdateCustomerContactDetail(ByRef cusBO As CustomerBO)

            Try

                ''Instantiate DAL object
                Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

                ''Call DAL method to retrieve information
                objCustomerDAL.UpdateCustomerContactDetail(cusBO)

                If (cusBO.IsFlagStatus) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                cusBO.IsExceptionGenerated = True
                cusBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

            Return Nothing
        End Function
#End Region

#End Region

    End Class

End Namespace
