Imports Broadland.TenantsOnline.BusinessObject

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters

Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.DataAccess


Public Class ContractorPortalManager


#Region "Job Sheet Functions - Coded By Noor Muhammad - January,2009 "

#Region "Get Job Sheet Data"
    Public Function GetJobSheetData(ByRef conBO As ContractorPortalBO, ByRef jobSheetDS As DataSet)

        'Create the object of contractor dal
        Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            conDAL.GetJobSheetData(jobSheetDS, conBO)
            Return Nothing
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#Region "Update Selected Job Sheet Faults"

    Public Function UpdateSelectedJobSheetFaults(ByRef selectedFaults As ArrayList, ByRef conBO As ContractorPortalBO)

        'Create the obj of data set
        Dim jobSheetDS As DataSet = New DataSet

        'Create the object of contractor dal
        Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            conDAL.UpdateSelectedJobSheetFaults(selectedFaults, conBO)
            Return jobSheetDS

        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#Region "GetFaultSearchRowCount"
    Public Function GetJobSheetFaultCount(ByRef conBO As ContractorPortalBO)
        'Create the object of contractor dal
        Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            Return conDAL.GetJobSheetFaultCount(conBO)
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#Region "Get Job Sheet Popup Data"
    Public Function GetJSSummaryPopupData(ByRef JobSheetDS As DataSet, ByRef jsNumber As String)

        'Create the object of contractor dal
        Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            conDAL.GetJSSummaryPopupData(JobSheetDS, jsNumber)
            Return Nothing
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#Region "Get Job Sheet Popup Asbestos Data"
    Public Function GetJSSummaryPopupAsbestosData(ByRef jobSheetAbsetosDS As DataSet, ByRef jsNumber As String)

        'Create the object of contractor dal
        Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            conDAL.GetJSSummaryPopupAsbestosData(jobSheetAbsetosDS, jsNumber)
            Return Nothing
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#End Region

    '-------------------------------------------------------------------------------------------------------------    

#Region "Appointment TBA Functions - Coded By Usman Sharif - January,2009"

#Region "Get TBA Search Data "

    Public Function GetTBASearchData(ByRef tbaSearchBO As TBASearchBO) As DataSet
        Dim TBADS As New DataSet
        Dim contractorDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            contractorDAL.GetTBASearchData(TBADS, tbaSearchBO)
            Return TBADS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function
#End Region

#Region "Get TBA Customer Vulnerability Data "

    Public Function GetTBAVulnerabilityData(ByRef CustomerId As Integer) As DataSet
        Dim TBADS As New DataSet
        Dim contractorDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            contractorDAL.GetTBAVulnerabilityData(TBADS, CustomerId)
            Return TBADS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function
#End Region

#Region "Get TBA Customer Risk Data "

    Public Function GetTBARiskData(ByRef CustomerId As Integer) As DataSet
        Dim TBADS As New DataSet
        Dim contractorDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            contractorDAL.GetTBARiskData(TBADS, CustomerId)
            Return TBADS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function
#End Region

#Region "Get TBA Customer Gas Servicing Data "

    Public Function GetTBAGasServicingData(ByRef CustomerId As Integer) As DataSet
        Dim TBADS As New DataSet
        Dim contractorDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            contractorDAL.GetTBAGasServicingData(TBADS, CustomerId)
            Return TBADS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function
#End Region

#Region "Get TBA Customer Communication Data "

    Public Function GetTBACommunicationData(ByRef CustomerId As Integer) As DataSet
        Dim TBADS As New DataSet
        Dim contractorDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            contractorDAL.GetTBACommunicationData(TBADS, CustomerId)
            Return TBADS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function
#End Region
#Region "Get Assignment TBA Search Row Count "
    Public Function GetAssignmentTBASearchRowCount(ByRef tbaSearchBO As TBASearchBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim tbaDAL As ContractorPortalDAL = New ContractorPortalDAL()
            rowCount = tbaDAL.GetAssignmentTBASearchRowCount(tbaSearchBO)

        Catch ex As Exception

            tbaSearchBO.IsExceptionGenerated = True
            tbaSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function
#End Region

#Region "Save Appointment To be Arranged "

    Public Sub SaveAppointment(ByRef appointmentBO As AppointmentTBABO)
        Try
            Dim contractorDAL As ContractorPortalDAL = New ContractorPortalDAL()
            contractorDAL.SaveAppointment(appointmentBO)

        Catch ex As Exception
            appointmentBO.IsExceptionGenerated = True
            appointmentBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub

#End Region

#End Region

    '-------------------------------------------------------------------------------------------------------------

#Region "Manage Appointment Functions - Coded By Waseem Hasan - January,2009 "

#Region "Update Manage Appointment Data "
    Public Sub UpdateManageAppointmentData(ByRef cpBO As ContractorPortalBO)
        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.UpdateManageAppointmentData(cpBO)
        Catch ex As Exception

            cpBO.IsExceptionGenerated = True
            cpBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub

#End Region

#Region "Get Manage Appointment Search Row Count "

    Public Function GetManageAppointmentSearchRowCount(ByRef cpBO As ContractorPortalBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim CPDAL As ContractorPortalDAL = New ContractorPortalDAL()
            rowCount = CPDAL.GetManageAppointmentSearchRowCount(cpBO)

        Catch ex As Exception

            cpBO.IsExceptionGenerated = True
            cpBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function
#End Region

#End Region

    '-------------------------------------------------------------------------------------------------------------

#Region "Common Functions - Manage Appointment & Work Completion - Coded By Waseem Hasan - January,2009"

#Region "Get Manage Appointment Search Data "

    Public Function GetManageAppointmentSearchData(ByRef cpBO As ContractorPortalBO) As DataSet
        Dim MADS As New DataSet
        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.GetManageAppointmentSearchData(MADS, cpBO)
            Return MADS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function
#End Region

#Region "Get Work Completion Search Data "
    Public Function GetWorkCompletionSearchData(ByRef cpBO As ContractorPortalBO) As DataSet
        Dim WCDS As New DataSet
        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.GetWorkCompletionSearchData(WCDS, cpBO)
            Return WCDS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function
#End Region

#End Region

    '-------------------------------------------------------------------------------------------------------------

#Region "Work Completion Functions - Coded By Waseem Hasan - January,2009 "

#Region "Get Work Completion Search Row Count "

    Public Function GetWorkCompletionSearchRowCount(ByRef cpBO As ContractorPortalBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim CPDAL As ContractorPortalDAL = New ContractorPortalDAL()
            rowCount = CPDAL.GetWorkCompletionSearchRowCount(cpBO)

        Catch ex As Exception

            cpBO.IsExceptionGenerated = True
            cpBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function
#End Region

#Region "Get Repair Data "
    Public Sub GetRepairData(ByRef cpBO As ContractorPortalBO, ByRef ds As DataSet)
        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.GetRepairData(cpBO, ds)
        Catch ex As Exception

            cpBO.IsExceptionGenerated = True
            cpBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get Vat Rate "
    Public Sub GetVatRate(ByRef cpBO As ContractorPortalBO)
        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.GetVatRate(cpBO)
        Catch ex As Exception

            cpBO.IsExceptionGenerated = True
            cpBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Save Repair Record "
    Public Sub SaveRepairRecord(ByRef cpBO As ContractorPortalBO)
        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.SaveRepairRecord(cpBO)
        Catch ex As Exception

            cpBO.IsExceptionGenerated = True
            cpBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Save Repairs Against Fault "
    Public Function SaveRepairsAgainstFault(ByVal conBO As ContractorPortalBO)

        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.SaveRepairsAgainstFault(conBO)
            Return Nothing
        Catch ex As Exception

            conBO.IsExceptionGenerated = True
            conBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#Region "Save Invoicing Record "
    Public Sub SaveInvoicingRecord(ByRef cpBO As ContractorPortalBO)
        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.SaveInvoicingRecord(cpBO)
        Catch ex As Exception

            cpBO.IsExceptionGenerated = True
            cpBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#Region "Get Repair Record "
    Public Sub GetRepairRecord(ByRef cpBO As ContractorPortalBO)
        Dim cpDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cpDAL.GetRepairRecord(cpBO)
        Catch ex As Exception

            cpBO.IsExceptionGenerated = True
            cpBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#End Region

    '-------------------------------------------------------------------------------------------------------------

#Region "Invoicing Functions - Coded By Tahir Gul - January,2009"

#Region "Get Invoice Search Row Count "

    ''' <summary>
    ''' Will return the number of rows if any
    ''' </summary>
    ''' <param name="invoiceSearchBO"> The invoiceBO that is retrieved from viewstate is passed to this function </param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetInvoiceSearchRowCount(ByRef invoiceSearchBO As InvoiceBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim invoiceDAL As ContractorPortalDAL = New ContractorPortalDAL()

            rowCount = invoiceDAL.GetInvoiceSearchRowCount(invoiceSearchBO)

        Catch ex As Exception

            invoiceSearchBO.IsExceptionGenerated = True
            invoiceSearchBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function
#End Region

#Region "Get Invoicin gContract Portal Data BL "

    ''' <summary>
    ''' This function is called to fetch records from the DAL which will then be shown on the gridveiw. 
    ''' </summary>
    ''' <param name="invBO">The invoice BO that is retrieved from viewstate is passed</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetInvoicingContractPortalDataBL(ByRef invBO As InvoiceBO) As DataSet
        Dim invDS As New DataSet
        Dim invDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            invDAL.GetInvoicingContractPortalDAL(invDS, invBO)
            Return invDS

        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing

        End Try

    End Function

#Region "Populate Inner Repair Grid"

    Public Sub PopulateRepairGrid(ByVal faultId As Integer, ByRef repairDS As DataSet)

        Try
            Dim invDAL As ContractorPortalDAL = New ContractorPortalDAL()
            invDAL.PopulateInnerRepairGrid(faultId, repairDS)
        Catch ex As Exception

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try


    End Sub
#End Region
#End Region
#Region "Create Batch for Process Invoice "
    Public Sub CreateInvoiceBatch(ByRef FaultList As String, ByRef conBO As ContractorPortalBO)
        Try
            Dim contractorDAL As ContractorPortalDAL = New ContractorPortalDAL()
            contractorDAL.CreateBatchforInvoicing(FaultList, conBO)

        Catch ex As Exception

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

    End Sub

#End Region

#End Region

    '-------------------------------------------------------------------------------------------------------------

#Region "Monitoring Functions - Coded By Noor Muhammad - January,2009 "

#Region "Get Monitoring Search Panel Data"
    Public Function GetMonitoringSearchPanelData(ByRef conBO As ContractorPortalBO, ByRef monitorDS As DataSet)
        Try
            'Create the object of contractor dal
            Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()

            conDAL.GetMonitoringSearchPanelData(conBO, monitorDS)
            Return Nothing

        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

    '#Region "Get Monitoring Time Base Data "

    '    Public Function GetMonitoringTimeBaseData(ByRef conBO As ContractorPortalBO, ByRef monitorDS As DataSet, ByVal selectedDateOption As String)
    '        Try
    '            'Create the object of contractor dal
    '            Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()

    '            conDAL.GetMonitoringTimeBaseData(conBO, monitorDS, selectedDateOption)
    '            Return Nothing

    '        Catch ex As Exception
    '            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
    '            Return Nothing
    '        End Try
    '    End Function
    '#End Region

#Region "Get Monitoring Time Base Fault Count"

    Public Function GetMonitoringTimeBaseFaultCount(ByRef conBO As ContractorPortalBO, ByRef conArrayList As ArrayList)
        Try
            'Create the object of contractor dal
            Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()

            conDAL.GetMonitoringTimeBaseFaultCount(conBO, conArrayList)
            Return Nothing

        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try
    End Function
#End Region

#End Region

    '-------------------------------------------------------------------------------------------------------------

#Region "Contractor Protal - Fault Reports - Coded By Noor Muhammad - Feburary,2009 "

#Region "Get Fault Reports "

    'Public Function GetFaultReports(ByRef faultRepBO, ByVal faultRepDS)
    '    Try
    '        'Create the object of contractor dal
    '        Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()

    '        conDAL.GetFaultReports(faultRepBO, faultRepDS)
    '        Return Nothing

    '    Catch ex As Exception
    '        Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        Return Nothing
    '    End Try
    'End Function
#End Region

#Region "Get Fault Reports Count"
    'Public Function GetFaultReportsCount(ByRef faultRepBO As FaultReportsBO)
    '    Try
    '        'Create the object of contractor dal
    '        Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()
    '        Return conDAL.GetFaultReportsCount(faultRepBO)

    '    Catch ex As Exception
    '        Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        Return Nothing
    '    End Try
    'End Function

#End Region

#Region "Contract Portal--PreInspection Report"

    Public Sub PreInspectionReportDate(ByRef PreInsDS As DataSet, ByRef preINBO As PreInspectionBO)
        Try
            Dim rfDAL As ContractorPortalDAL = New ContractorPortalDAL()
            rfDAL.GetPreInspectionDATA(PreInsDS, preINBO)

        Catch ex As Exception

            preINBO.IsExceptionGenerated = True
            preINBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region
#End Region

    '-------------------------------------------------------------------------------------------------------------

#Region "Contractor Protal - Pre Inspection Reports & Reactive Repair - Coded By Tahir Gul - Feb,2009 "

#Region "Contractor Portal--PreInspection Report "
    Public Function GetPreInspectRowCount(ByRef PreInBO As PreInspectionBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim PreInsp As ContractorPortalDAL = New ContractorPortalDAL()
            rowCount = PreInsp.GetPreInspectionRowCount(PreInBO)

        Catch ex As Exception

            PreInBO.IsExceptionGenerated = True
            PreInBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function
#End Region

#Region "Contractor Portal --Reactive Repair "

#Region "Populate Reactive Repair "

    Public Sub PopulateReactiveRepair(ByRef reactiveDS As DataSet, ByVal customerId As Integer, ByVal tenancyId As Integer)
        Try
            Dim reactiveRepairDAL As ContractorPortalDAL = New ContractorPortalDAL()
            reactiveRepairDAL.GetReactiveRepairDate(reactiveDS, customerId, tenancyId)


        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Sub
#End Region

#Region "Populate Reactive Repair Nature "

    Public Sub PopulateReactiveRepairNature(ByVal faultID As Integer, ByRef reactiveDS As DataSet)
        Try
            Dim reactiveRepairDAL As ContractorPortalDAL = New ContractorPortalDAL()
            'Dim fault As Integer = Convert.ToInt32(faultID)
            reactiveRepairDAL.GetReactiveRepairNature(faultID, reactiveDS)

        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Sub
#End Region

#Region "Populate Property Reactive Repair "

    Public Sub PopulatePropertyReactiveRepair(ByRef reactiveDS As DataSet, ByVal propertyId As String)
        Try
            Dim reactiveRepairDAL As ContractorPortalDAL = New ContractorPortalDAL()
            reactiveRepairDAL.GetPropertyReactiveRepairData(reactiveDS, propertyId)


        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Sub
#End Region


#End Region

#Region "Contractor Portal -- Reactrive Repair_Journal"

    Public Function PopulateReactiveRepairJournal(ByRef reactiveDS As DataSet, ByVal id As Integer, ByVal reactiveBO As PreInspectionBO) As Boolean
        Try
            Dim outputResult As Boolean
            Dim reactiveRepairDAL As ContractorPortalDAL = New ContractorPortalDAL()
            outputResult = reactiveRepairDAL.GetReactiveRepairJournalDate(reactiveDS, id, reactiveBO)

            Return outputResult

        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
    End Function


#End Region

#End Region

#Region "Contractor Portal - PostInspection Report - Coded By Usman Sharif - Feb,2009 "

#Region "Get post Inspection Report Count Result"

    Public Function GetPostInspectRowCount(ByRef postBO As PostInspectionBO) As Integer
        Dim rowCount As Integer = 0
        Try
            Dim PreInsp As ContractorPortalDAL = New ContractorPortalDAL()
            rowCount = PreInsp.GetPostInspectionRowCount(postBO)


        Catch ex As Exception

            postBO.IsExceptionGenerated = True
            postBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function

#End Region

#Region "Get PostInspection Report Results"

    Public Function GetPostInspectionReportData(ByRef postBO As PostInspectionBO) As DataSet
        Dim postDS As New DataSet
        Dim contractorDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            contractorDAL.GetPostInspectionDATA(postDS, postBO)
            Return postDS
        Catch ex As Exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
            Return Nothing
        End Try

    End Function

#End Region

#Region "Save Post Inspection record"
    Public Sub SavePostInspectionRecord(ByRef piBO As PreInspectBO)
        Try
            Dim piDAL As ContractorPortalDAL = New ContractorPortalDAL()
            piDAL.SavePostInspectionRecord(piBO)

        Catch ex As Exception

            piBO.IsExceptionGenerated = True
            piBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub

#End Region

#Region "Save Post Inspection Record Update"

    Public Sub SavePostInspectionRecordUpdate(ByRef piBO As PreInspectBO)
        Try
            Dim piDAL As ContractorPortalDAL = New ContractorPortalDAL()
            piDAL.SavePostInspectionRecordUpdate(piBO)

        Catch ex As Exception

            piBO.IsExceptionGenerated = True
            piBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")


        End Try
    End Sub
#End Region

#End Region

#Region "Cancelled Job Report - Coded By Usman Sharif - April,2009"

#Region "Get Cancelled Job Search Row Count"
    Public Function GetCancelledJobSearchRowCount(ByRef cancelledBO As CancelledJobBO) As Integer

        Dim rowCount As Integer = 0

        Try
            Dim conDal As ContractorPortalDAL = New ContractorPortalDAL()
            rowCount = conDal.GetCancelledJobSearchRowCount(cancelledBO)

        Catch ex As Exception
            cancelledBO.IsExceptionGenerated = True
            cancelledBO.ExceptionMsg = ex.Message

            ''Execute the exception policy against this type of exception
            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try
        Return rowCount
    End Function
#End Region

#Region "Update Cancel Report Grid "

    Public Sub UpdateCancelReportGrid(ByVal faultLogId As Integer, ByVal reason As String, ByVal userId As Integer)

        Dim cancelReportDAL As ContractorPortalDAL = New ContractorPortalDAL()
        Try
            cancelReportDAL.UpdateCancelReportGrid(faultLogId, reason, userId)

        Catch ex As Exception


        End Try



    End Sub
#End Region

#Region "Get Cancelled Job Search Data "

    Public Function GetCancelledJobSearchData(ByRef cancelledDS As DataSet, ByRef cancelledBO As CancelledJobBO)

        Try
            Dim conDAL As ContractorPortalDAL = New ContractorPortalDAL()

            conDAL.GetCancelledJobSearchData(cancelledDS, cancelledBO)

        Catch ex As Exception

            cancelledBO.IsExceptionGenerated = True
            cancelledBO.ExceptionMsg = ex.Message

            Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

        End Try

        Return Nothing
    End Function
#End Region

#End Region

End Class
