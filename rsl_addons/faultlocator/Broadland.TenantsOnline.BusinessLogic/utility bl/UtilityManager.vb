Imports Broadland.TenantsOnline.BusinessObject

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters

Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.DataAccess

Namespace Broadland.TenantsOnline.BusinessLogic

    Public Class UtilityManager

#Region "Methods"

#Region "getLookUpList"

        Public Sub getLookUpList(ByVal spName As String, ByRef lstLookup As LookUpList)
            Try
                Dim objDAL As New UtilityDAL
                objDAL.getLookUpList(spName, lstLookup)
            Catch ex As Exception

                ''Execute the exception policy against this type of exception
                lstLookup.IsExceptionGenerated = True
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Sub
        Public Sub getUserLookUpList(ByVal spName As String, ByRef lstLookup As LookUpList, ByVal inParam As String, ByVal inParamName As String, ByVal param As String, ByVal Paramname As String)
            Try
                Dim objDAL As New UtilityDAL
                objDAL.getUserLookUpList(spName, lstLookup, inParam, inParamName, param, Paramname)
            Catch ex As Exception

                ''Execute the exception policy against this type of exception
                lstLookup.IsExceptionGenerated = True
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Sub

        Public Sub getLookUpList(ByVal spName As String, ByRef lstLookup As LookUpList, ByVal inParam As String, ByVal inParamName As String)
            Try
                Dim objDAL As New UtilityDAL
                objDAL.getLookUpList(spName, lstLookup, inParam, inParamName)
            Catch ex As Exception

                ''Execute the exception policy against this type of exception
                lstLookup.IsExceptionGenerated = True
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Sub

        Public Sub getLookUpList(ByVal spName As String, ByRef lstLookUp As LookUpList, ByVal firstInParam As String, ByVal firstInParamName As String, ByVal secondInParam As String, ByVal secondInParamName As String)
            Try
                Dim objDAL As New UtilityDAL
                objDAL.getLookUpList(spName, lstLookUp, firstInParam, firstInParamName, secondInParam, secondInParamName)
            Catch ex As Exception

                ''Execute the exception policy against this type of exception
                lstLookUp.IsExceptionGenerated = True
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Sub

#End Region

#Region "GetPriorityResponseValue"

        Public Function GetPriorityResponse(ByVal inParam As String) As String
            Try
                Dim objDAL As New UtilityDAL
                Return objDAL.getPriorityResponse(inParam)
            Catch ex As Exception

                ''Execute the exception policy against this type of exception

                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
                Return Nothing

            End Try

        End Function

#End Region

#End Region
        
#Region "Functions"

        '#Region "SendEmail"

        '        Public Function SendMail(ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String) As Boolean
        '            Try

        '                Dim flagEmailSendSuccess As Boolean = False
        '                flagEmailSendSuccess = UtilityFunctions.SendMail(strTo, strSubject, strBody)
        '                If (flagEmailSendSuccess) Then
        '                    Return True
        '                End If
        '            Catch ex As Exception
        '                ''Execute the exception policy against this type of exception
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
        '            End Try
        '            Return False
        '        End Function

        '#End Region
        


        '#Region "getBroadlandNews"


        '        Public Function getNewsList() As NewsList
        '            Dim spName As String = SprocNameConstants.getBroadlandNews
        '            Dim lstNews As New NewsList

        '            Try

        '                Dim objDAL As New UtilityDAL
        '                objDAL.getNewsList(spName, lstNews)
        '            Catch ex As Exception

        '                ''Execute the exception policy against this type of exception
        '                lstNews.IsExceptionGenerated = True
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
        '                Throw
        '            End Try
        '            lstNews.RemoveAt(0)
        '            Return lstNews

        '        End Function

        '        Public Function getLatestNews() As NewsList

        '            Dim spName As String = SprocNameConstants.getBroadlandLatestNews
        '            Dim lstNews As New NewsList

        '            Try

        '                Dim objDAL As New UtilityDAL
        '                objDAL.getNewsList(spName, lstNews)
        '            Catch ex As Exception

        '                ''Execute the exception policy against this type of exception
        '                lstNews.IsExceptionGenerated = True
        '                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")
        '                Throw
        '            End Try

        '            Return lstNews

        '        End Function

        '#End Region

#End Region

    End Class

End Namespace
