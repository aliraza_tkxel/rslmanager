Imports Broadland.TenantsOnline.BusinessObject

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters

Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.DataAccess

Namespace Broadland.TenantsOnline.BusinessLogic

    Public Class EnquiryManager

#Region "Functions"

#End Region

#Region "Methods"

#Region "Enquiry Log"
        Public Sub EnquiryLogData(ByRef enquiryDS As DataSet, ByRef enqryLogSearchBO As EnquiryLogSearchBO)

            Try
                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
                enqryDAL.EnquiryLogData(enquiryDS, enqryLogSearchBO)

            Catch ex As Exception
                enqryLogSearchBO.IsExceptionGenerated = True
                enqryLogSearchBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Sub

        Public Sub CustomerResponseData(ByRef enquiryDS As DataSet, ByRef enqryLogSearchBO As EnquiryLogSearchBO)

            Try
                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
                enqryDAL.CustomerResponseData(enquiryDS, enqryLogSearchBO)

            Catch ex As Exception
                enqryLogSearchBO.IsExceptionGenerated = True
                enqryLogSearchBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Sub

        Public Function GetEnquiryLogRowCount(ByRef enqryLogSearchBO As EnquiryLogSearchBO) As Integer

            Dim rowCount As Integer = 0

            Try
                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
                rowCount = enqryDAL.GetEnquiryLogRowCount()

            Catch ex As Exception
                enqryLogSearchBO.IsExceptionGenerated = True
                enqryLogSearchBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try
            Return rowCount
        End Function

        Public Function GetEnquiryLogSearchRowCount(ByRef enqryLogSearchBO As EnquiryLogSearchBO, ByVal countType As String) As Integer
            Dim rowCount As Integer = 0
            Try
                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
                rowCount = enqryDAL.GetEnquiryLogSearchRowCount(enqryLogSearchBO, countType)

            Catch ex As Exception

                enqryLogSearchBO.IsExceptionGenerated = True
                enqryLogSearchBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try
            Return rowCount
        End Function

        Public Sub DeleteCustomerEnquiry(ByRef termBO As TerminationBO)
            Try
                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
                enqryDAL.DeleteCustomerEnquiry(termBO)

            Catch ex As Exception
                termBO.IsExceptionGenerated = True
                termBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try
        End Sub

        Public Sub DeleteCustomerEnquiryResponse(ByRef cstResponseBO As CustomerResponseBO)
            Try
                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()

                enqryDAL.DeleteCustomerEnquiryResponse(cstResponseBO)
            Catch ex As Exception
                cstResponseBO.IsExceptionGenerated = True
                cstResponseBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try
        End Sub

        Public Sub GetCustomerIDByEnquiryLog(ByRef enqryLog As EnquiryLogBO)
            Try
                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
                enqryDAL.GetCustomerIDByEnquiryLog(enqryLog)

            Catch ex As Exception
                enqryLog.IsExceptionGenerated = True
                enqryLog.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try
        End Sub
#End Region

#Region "Create Customer Response Journal"

        Public Sub CreateCustomerResponseJornal(ByRef custResBO As CustomerResponseBO)
            Try
                Dim enqryDAL As EnquiryDAL = New EnquiryDAL()
                enqryDAL.CreateCustomerResponseJornal(custResBO)

            Catch ex As Exception
                custResBO.IsExceptionGenerated = True
                custResBO.ExceptionMsg = ex.Message

                ''Execute the exception policy against this type of exception
                Dim rethrow As Boolean = ExceptionPolicy.HandleException(ex, "Exception Policy")

            End Try

        End Sub

#End Region

#End Region

    End Class

End Namespace
