<%@ Page Language="vb" ValidateRequest="false" EnableEventValidation="false"  MasterPageFile="~/master pages/EnquiryManagement.Master"  AutoEventWireup="false"  CodeBehind="customer_enquiry_management.aspx.vb" Inherits="tenantsonline.CustomerEnquiryManagement" Title="Tenants Online :: Customer Enquiry Management"%>

<script runat="server">
    Function MakeCommandString(ByVal enqryLog As String, ByVal creationDate As String, ByVal tenancyID As String, ByVal fullname As String, ByVal item As String, ByVal nature As String, ByVal custResId As String, ByVal custResNotes As String) As String
        Return enqryLog & "," & creationDate & "," & tenancyID & "," & fullname & "," & item & "," & nature & "," & custResId & "," & custResNotes
    End Function
    
    Function MakeCRMCommandString(ByVal enqryLog As String, ByVal tenancyID As String) As String
        Return enqryLog & "," & tenancyID
    End Function
    
    Function MakeUpdateJournalCommandString(ByVal EnquiryID As String, ByVal JournalID As String, ByVal NatureID As String, ByVal Notes As String, ByVal ResponseID As String, ByVal CustomerId As String) As String
        Return EnquiryID & "," & JournalID & "," & NatureID & "," & Notes & "," & ResponseID & "," & CustomerId
    End Function
     
    Function CRMButtonStatus(ByVal reportType As String) As Boolean
        If reportType.Contains("Parking") Or reportType.Contains("Abandoned") Then
            Return False
        Else
            Return True
        End If
    End Function
</script>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    




<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
 <script type="text/javascript" language="javascript">
     function onUpdating(){
        // get the update progress div
        var updateProgressDiv = $get('updateProgressDiv'); 

        //  get the gridview element        
        var gridView = $get('<%= GVEnquiryLog.ClientID %>');
        
        // make it visible
        updateProgressDiv.style.display = '';	    
        
        // get the bounds of both the gridview and the progress div
	    var gridViewBounds = Sys.UI.DomElement.getBounds(gridView);
	    var updateProgressDivBounds = Sys.UI.DomElement.getBounds(updateProgressDiv);
        
	    //  center of gridview
	    var x = gridViewBounds.x + Math.round(gridViewBounds.width / 2) - Math.round(updateProgressDivBounds.width / 2);
	    var y = gridViewBounds.y + Math.round(gridViewBounds.height / 2) - Math.round(updateProgressDivBounds.height / 2);	    

	    //	set the progress element to this position
	    Sys.UI.DomElement.setLocation (updateProgressDiv, x, y);           
    }

    function onUpdated() {
        // get the update progress div
        var updateProgressDiv = $get('updateProgressDiv'); 
        // make it invisible
        updateProgressDiv.style.display = 'none';
    }
    function ClearCtrl(){
        document.getElementById("<%= txtDate.ClientID %>").value = "";
        document.getElementById("<%= txtFirstName.ClientID %>").value = "";
        document.getElementById("<%= txtLastName.ClientID %>").value = "";
        document.getElementById("<%= txtTenancyID.ClientID %>").value = "";
        document.getElementById("<%= txtText.ClientID %>").value = "";
        document.getElementById("<%= ddlNature.ClientID %>").value = "";
        document.getElementById("<%= ddlStatus.ClientID %>").value = "";
        
    }
 </script>
   <div style="text-align: left">

        <table style="width: 100%; height: 100%">
            <tr>
                <td style="width: 81px">
                    <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" Width="224px"></asp:Label></td>
                <td style="width: 100px">
               <cc2:ToolkitScriptManager ID="Registration_ToolkitScriptManager" runat="server">
               </cc2:ToolkitScriptManager>                
            </td>
            </tr>
            <tr>
                <td rowspan="2" style="width: 81px" valign="top">
                    <table style="width: 5%; border-right: tan thin dotted; border-top: tan thin dotted; border-left: tan thin dotted; border-bottom: tan thin dotted;">
                        <tr>
                            <td colspan="2" style="border-bottom: thin dotted">
                                <asp:Label ID="lblEnqType" runat="server" Font-Names="Arial" Font-Size="Small" Text="Enquiry Type:"
                                    Width="84px"></asp:Label>
                                <asp:DropDownList ID="ddlEnqType" runat="server" Font-Names="Arial" Font-Size="Small" Width="135px" AppendDataBoundItems="True" AutoPostBack="True">
                                    <asp:ListItem Value="0">Enquiries</asp:ListItem>
                                    <asp:ListItem Value="1">Responses</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblSearch" runat="server" Font-Bold="True" Font-Names="Arial" Text="Search:"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblFirstName" runat="server" Text="First Name" Font-Names="Arial" Font-Size="Small" Width="64px"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtFirstName" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="50" Width="150px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblLastName" runat="server" Text="Last Name" Font-Names="Arial" Font-Size="Small"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtLastName" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="50" Width="150px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblDate" runat="server" Text="Date" Font-Names="Arial" Font-Size="Small"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtDate" runat="server" Font-Names="Arial" Font-Size="Small" Width="88px" MaxLength="10"></asp:TextBox>
                                
                                <asp:ImageButton ID="btnCalander" runat="server" CausesValidation="False" ImageUrl="~/images/buttons/Calendar_button.png" />
                            &nbsp;
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblTenancyID" runat="server" Text="TenancyID" Font-Names="Arial" Font-Size="Small"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtTenancyID" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="50" Width="150px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblNature" runat="server" Text="Nature" Font-Names="Arial" Font-Size="Small"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:DropDownList ID="ddlNature" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True">

                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblStatus" runat="server" Text="Status" Font-Names="Arial" Font-Size="Small"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:DropDownList ID="ddlStatus" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblText" runat="server" Text="Text" Font-Names="Arial" Font-Size="Small"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtText" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="255" Width="150px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                &nbsp;</td>
                            <td style="width: 145px">
                                &nbsp;<asp:Button ID="btn" runat="server" Text="Search" />
                                <asp:Button ID="btnClear" runat="server" Style="position: static" Text="Clear" OnClientClick="ClearCtrl();return false;" /></td>
                        </tr>
                    </table>
                </td>
                <td style="width: 100px">
                    <asp:Label ID="lblGVHeading" runat="server" BackColor="Black" Font-Bold="True" Font-Names="Arial"
                        ForeColor="White" Text="Customer Enquiry Management" Width="248px"></asp:Label></td>
            </tr>
            <tr>

                
                <td style="width: 100%; text-align: left;vertical-align:top; height: 350px;">
            <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
<cc1:PagingGridView id="GVEnquiryLog"  runat="server"    Width="100%" ForeColor="Black" Font-Names="Arial" BackColor="LightGoldenrodYellow" Font-Size="Small"  GridLines="None" CellPadding="2" BorderWidth="1px" BorderColor="Tan" AllowSorting="True" AllowPaging="True" VirtualItemCount="-1" OnSorting="GVEnquiryLog_Sorting" OnPageIndexChanging="GVEnquiryLog_PageIndexChanging" AutoGenerateColumns="False" OnRowCreated="GVEnquiryLog_RowCreated" EmptyDataText="No record exists" BorderStyle="Dotted" OnSelectedIndexChanged="GVEnquiryLog_SelectedIndexChanged"  >

<FooterStyle BackColor="#C00000" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:TemplateField SortExpression="CreationDate" HeaderText="Date" InsertVisible="False"><ItemTemplate>
<asp:Label id="GVLblCreationDate" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("CreationDate").ToString()) %>' __designer:wfdid="w1"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField SortExpression="EnquiryLogID" HeaderText="Enquiry" InsertVisible="False">
    <headerstyle horizontalalign="Left" />
    <ItemTemplate>
<asp:Label id="GVLblEnquiryLogID" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.GetEnquiryString(Eval("EnquiryLogID").ToString()) %>' __designer:wfdid="w34"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField SortExpression="TenancyID" HeaderText="Tenancy" InsertVisible="False"><ItemTemplate>
<asp:Label id="GVLblTenancyID" runat="server" CssClass="cellData" Text='<%# Bind("TenancyID") %>' __designer:wfdid="w37"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField SortExpression="FIRSTNAME" HeaderText="Name" InsertVisible="False"><ItemTemplate>
<asp:Label id="GVLblName" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatName( Eval("FIRSTNAME").ToString() , Eval("LASTNAME").ToString() ) %>' __designer:wfdid="w38"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Item" InsertVisible="False">
<HeaderStyle Font-Underline="True" horizontalalign="Left"></HeaderStyle>
<ItemTemplate>
<asp:Label id="GVLblItem" runat="server" CssClass="cellData" Text='<%# Bind("Item") %>' __designer:wfdid="w40"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField SortExpression="C_NATURE.DESCRIPTION" HeaderText="Nature"><ItemTemplate>
<asp:Label id="GVLblNature" runat="server" CssClass="cellData" Text='<%# Bind("Nature") %>' __designer:wfdid="w42"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField>
<ItemTemplate>
<asp:Button id="btnViewDetails" onclick="BtnViewDetails_Click" runat="server" Text="View" __designer:wfdid="w3" CommandArgument='<%#MakeCommandString(Eval("EnquiryLogID").ToString(), UtilityFunctions.FormatDate(Eval("CreationDate").ToString()), Eval("TenancyID"), UtilityFunctions.GetFullName( Eval("FIRSTNAME").ToString() , Eval("LASTNAME").ToString() ), Eval("Item"),Eval("Nature"),"",""      ) %>'></asp:Button> 
</ItemTemplate>
</asp:TemplateField>


<asp:TemplateField>
<ItemTemplate>
<asp:Button id="btnCreateCRM" onclick="BtnCreateCRM_Click" runat="server" Text="Create CRM" __designer:wfdid="w2" CommandArgument='<%# Eval("EnquiryLogID").ToString() %>' enabled="<%# GetStatusType() %>"></asp:Button> 
</ItemTemplate>
</asp:TemplateField>

<asp:TemplateField>
<ItemTemplate>
<asp:Button id="btnDeleteEnquiry" onclick="BtnDeleteEnquiry_Click" runat="server" Text="X" OnClientClick="return confirm('Are you sure to delete the selected record?');" __designer:wfdid="w2" CommandArgument='<%# Eval("EnquiryLogID").ToString() %>' enabled="<%# GetDeleteBtnStatus() %>"></asp:Button> 
</ItemTemplate>
</asp:TemplateField>
</Columns>

<RowStyle BackColor="#EFF3FB"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" ></PagerStyle>
        <pagersettings           
          pagebuttoncount="5" FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"/>
          
<HeaderStyle BackColor="#C00000" ForeColor="White" Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>

<AlternatingRowStyle BackColor="White" Wrap="True" Font-Size="Small" Font-Names="Arial"></AlternatingRowStyle>


</cc1:PagingGridView> 
                    <cc1:PagingGridView id="GVResponses"  runat="server"    Width="100%" ForeColor="Black" Font-Names="Arial" BackColor="LightGoldenrodYellow" Font-Size="Small" OrderBy GridLines="None" CellPadding="2" BorderWidth="1px" BorderColor="Tan" AllowSorting="True" AllowPaging="True" VirtualItemCount="-1" OnSorting="GVEnquiryLog_Sorting" OnPageIndexChanging="GVResponse_PageIndexChanging" AutoGenerateColumns="False" OnRowCreated="GVResponses_RowCreated" EmptyDataText="No record exists" BorderStyle="Dotted"  >
                        <pagersettings           
          pagebuttoncount="5" FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"/>
                        <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>                          
                            <asp:TemplateField SortExpression="CreationDate" HeaderText="Date" InsertVisible="False">
                                <itemtemplate>
<asp:Label id="GVLblCreationDate" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("CreationDate").ToString()) %>' __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="EnquiryLogID" HeaderText="Enquiry" InsertVisible="False">
                                <headerstyle horizontalalign="Left" />
                                <itemtemplate>
<asp:Label id="GVLblEnquiryLogID" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.GetEnquiryString(Eval("EnquiryLogID").ToString()) %>' __designer:wfdid="w34"></asp:Label> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="TenancyID" HeaderText="Tenancy" InsertVisible="False">
                                <itemtemplate>
<asp:Label id="GVLblTenancyID" runat="server" CssClass="cellData" Text='<%# Bind("TenancyID") %>' __designer:wfdid="w37"></asp:Label> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="FIRSTNAME" HeaderText="Name" InsertVisible="False">
                                <itemtemplate>
<asp:Label id="GVLblName" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatName( Eval("FIRSTNAME").ToString() , Eval("LASTNAME").ToString() ) %>' __designer:wfdid="w38"></asp:Label> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item" InsertVisible="False">
                                <headerstyle font-underline="True" horizontalalign="Left" />
                                <itemtemplate>
<asp:Label id="GVLblItem" runat="server" CssClass="cellData" Text='<%# Bind("Item") %>' __designer:wfdid="w40"></asp:Label> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="C_NATURE.DESCRIPTION" HeaderText="Nature">
                                <itemtemplate>
<asp:Label id="GVLblNature" runat="server" CssClass="cellData" Text='<%# Bind("Nature") %>' __designer:wfdid="w42"></asp:Label> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <itemtemplate>
<asp:Button id="btnViewDetails" runat="server" Text="View" __designer:wfdid="w5" CommandArgument='<%#MakeCommandString(Eval("EnquiryLogID").ToString(), UtilityFunctions.FormatDate(Eval("CreationDate").ToString()), Eval("TenancyID"), UtilityFunctions.GetFullName( Eval("FIRSTNAME").ToString() , Eval("LASTNAME").ToString() ), Eval("Item"),Eval("Nature"),Eval("RESPONSEID"),Eval("RESPONSENOTES") ) %>' OnClick="BtnViewDetails_Click"></asp:Button> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <itemtemplate>
<asp:Button id="btnUpdateJournal" runat="server" Text="Update Journal" __designer:wfdid="w8" enabled="True" CommandArgument='<%# MakeUpdateJournalCommandString(Eval("EnquiryLogID").ToString(),Eval("JOURNALID").ToString(),Eval("ItemNatureId").ToString(),Eval("RESPONSENOTES"),Eval("RESPONSEID").ToString(),Eval("CUSTOMERID").ToString()) %>' OnClick="btnUpdateJournal_Click"></asp:Button> 
</itemtemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <itemtemplate>
<asp:Button id="btnDeleteResponse" onclick="btnDeleteResponse_Click" runat="server" Text="X" OnClientClick="return confirm('Are you sure to delete the selected record?');" Visible="False" __designer:wfdid="w14" enabled="<%# GetDeleteBtnStatus() %>" CommandArgument='<%# Eval("RESPONSEID").ToString() %>'></asp:Button> 
</itemtemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" Font-Names="Arial" Font-Size="Small" Wrap="True" />
                    </cc1:PagingGridView>
</ContentTemplate>
            </asp:UpdatePanel> 
             <cc2:UpdatePanelAnimationExtender ID="upae" BehaviorID="animation" runat="server" TargetControlID="updatePanel">
                <Animations>
                    <OnUpdating>
                        <Parallel duration="0">
                            <%-- place the update progress div over the gridview control --%>
                            <ScriptAction Script="onUpdating();" />  
                         </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel duration="0">
                            <%--find the update progress div and place it over the gridview control--%>
                            <ScriptAction Script="onUpdated();" /> 
                        </Parallel> 
                    </OnUpdated>
                </Animations>
            </cc2:UpdatePanelAnimationExtender>
                                
                               <cc2:CalendarExtender ID="CalendarExtender_dob" runat="server" Format="dd/MM/yyyy"
                                   TargetControlID="txtDate" PopupButtonID="btnCalander">
                               </cc2:CalendarExtender>                                
            <div id="updateProgressDiv" class="updateProgress" style="display:none; z-index: 5; left: 50%; margin-left: 50px; position: absolute; top: 50%;">
                <div align="center" style="margin-top:13px;">
                    <img runat="server"  src="images/buttons/ajax-loader.gif" alt=".." />
                    <span class="updateProgressMessage">Loading ...</span>
                </div>
            </div>          
            
 <asp:LinkButton runat="server" CssClass="pageLinks" id="PreviousPage1" Text="[Previous Page]"
   OnCommand="NavigationLink_Click" CommandName="Prev" Visible="False" />
   <asp:LinkButton runat="server" CssClass="pageLinks" id="NextPage" Text="[Next Page]"
   OnCommand="NavigationLink_Click" CommandName="Next" Visible="False" />
                    </td>
            </tr>
            <tr>
                <td style="vertical-align: top; width: 10%; height: 8px">
                </td>
                <td style="vertical-align: top; width: 50%; height: 8px; text-align: right">
                    <asp:Button ID="BtnDataSet2Excel" runat="server" Text="Export" /></td>
            </tr>
            <tr>
                <td style="width: 81px; height: 30px;">
                </td>
                <td style="width: 100px; height: 30px;">

<asp:Panel ID="pnlPopup" runat="server" Width="500px"   style="background-color:White;display:none">
<asp:UpdatePanel  ID="updPnlCustomerDetail" runat="server" UpdateMode="Conditional" >
<ContentTemplate >
<TABLE style="WIDTH: 100%"><TBODY><TR><TD style="BACKGROUND-COLOR: #c00000" align=left colSpan=5><asp:Label id="lblViewEnquiry" runat="server" Width="97%" Text=" View Enquiry" ForeColor="White" Font-Names="Arial" Font-Bold="True" BackColor="#C00000" Font-Size="Small" Height="19px"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="lblPODate" runat="server" CssClass="caption" Text="Date:"></asp:Label></TD><TD style="WIDTH: 68px" align=left><asp:Label id="lblDateVal" runat="server" CssClass="caption"></asp:Label></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px" align=right><asp:Label id="lblPOName" runat="server" CssClass="caption" Text="Name:"></asp:Label></TD><TD style="WIDTH: 100px" align=left><asp:Label id="lblNameVal" runat="server" CssClass="caption"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="lblPOEnqryNo" runat="server" CssClass="caption" Text="Enquiry No:"></asp:Label></TD><TD style="WIDTH: 68px" align=left><asp:Label id="lblEnquiryNoVal" runat="server" CssClass="caption"></asp:Label></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px" align=right><asp:Label id="lblPOAddress" runat="server" Width="60px" CssClass="caption" Text="Address:"></asp:Label></TD><TD style="WIDTH: 100px" vAlign=top align=left rowSpan=1><asp:Label id="lblAddressVal" runat="server" CssClass="caption"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px; HEIGHT: 21px" align=right><asp:Label id="lblPOTenancy" runat="server" CssClass="caption" Text="Tenancy No:"></asp:Label></TD><TD style="WIDTH: 68px; HEIGHT: 21px"><asp:Label id="lblTenancyNoVal" runat="server" CssClass="caption"></asp:Label></TD><TD style="WIDTH: 100px; HEIGHT: 21px"></TD><TD style="WIDTH: 100px; HEIGHT: 21px" align=right></TD><TD style="WIDTH: 100px" vAlign=top align=left rowSpan=4><asp:Label id="lblAddress2" runat="server" CssClass="caption"></asp:Label><asp:Label id="lblAddress3" runat="server" CssClass="caption"></asp:Label><asp:Label id="lblCounty" runat="server" CssClass="caption"></asp:Label><asp:Label id="lblPostCode" runat="server" CssClass="caption" Height="20px"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px" align=right></TD><TD style="WIDTH: 68px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px" align=right></TD></TR><TR><TD style="WIDTH: 100px; HEIGHT: 21px" align=right></TD><TD style="WIDTH: 68px; HEIGHT: 21px"></TD><TD style="WIDTH: 100px; HEIGHT: 21px"></TD><TD style="WIDTH: 100px; HEIGHT: 21px" align=right></TD></TR><TR><TD style="WIDTH: 100px" align=right></TD><TD style="WIDTH: 68px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px" align=right></TD></TR><TR><TD style="WIDTH: 100px" align=right></TD><TD style="WIDTH: 68px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px" align=right></TD><TD style="WIDTH: 100px" align=left></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="lblPOEnqryItem" runat="server" Width="82px" CssClass="caption" Text="Enquiry Item:"></asp:Label></TD><TD style="WIDTH: 68px"><asp:Label id="lblEnqryItemVal" runat="server" Width="140px" CssClass="caption"></asp:Label></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px" align=right></TD><TD style="WIDTH: 100px" align=left></TD></TR><TR><TD style="WIDTH: 100px" align=right><asp:Label id="lblPOEnqryNature" runat="server" Width="96px" CssClass="caption" Text="Enquiry Nature:" Height="21px"></asp:Label></TD><TD style="WIDTH: 68px"><asp:Label id="lblEnqryNatureVal" runat="server" Width="139px" CssClass="caption"></asp:Label></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px" align=right><asp:Label id="lblPOTel" runat="server" Width="75px" CssClass="caption" Text="Telephone:"></asp:Label></TD><TD style="WIDTH: 100px" align=left><asp:Label id="lblTelVal" runat="server" CssClass="caption"></asp:Label></TD></TR><TR><TD style="HEIGHT: 21px" colSpan=5>
<HR />
<asp:Button style="DISPLAY: none" id="btnShowPopup" runat="server" OnClick="btnShowPopup_Click"></asp:Button><cc2:ModalPopupExtender id="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pnlPopup" CancelControlID="btnClose" BackgroundCssClass="modalBackground" Drag="True" PopupDragHandleControlID="lblViewEnquiry"></cc2:ModalPopupExtender> </TD></TR><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 68px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD style="WIDTH: 100px"><asp:Label id="lblPODetails" runat="server" CssClass="caption" Text="Details:"></asp:Label></TD><TD style="WIDTH: 68px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR><TR><TD colSpan=5><asp:TextBox id="txtPODetails" runat="server" Width="473px" Height="168px" TextMode="MultiLine" ReadOnly="True" MaxLength="4000"></asp:TextBox></TD></TR><TR><TD colSpan=2>&nbsp;&nbsp; <asp:Label id="lblliketoend" runat="server" Width="176px" Text="   I'd like to end my tenancy on" Font-Names="Arial" Font-Size="Small" Visible="False"></asp:Label></TD><TD colSpan=2><asp:TextBox id="txtTenancyEndDate" runat="server" Width="80px" Font-Size="Small" Visible="False" ReadOnly="True"></asp:TextBox></TD><TD style="WIDTH: 100px" align=right><asp:Button id="btnClose" runat="server" Width="56px" Text="Close" OnClick="btnClose_Click"></asp:Button></TD></TR></TBODY></TABLE>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>

                </td>
            </tr>
        </table>
    </div>

 <center>

 </center>

</asp:Content>
