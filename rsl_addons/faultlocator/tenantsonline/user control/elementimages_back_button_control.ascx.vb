Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class elementimages_back_button_control
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#Region "btn Back Click "

    Protected Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        'Create the obj of customer header bo
        Dim headerBO As CustomerHeaderBO = Nothing
        Dim customerId As String = ""

        Try
            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            customerId = headerBO.CustomerID.ToString()

        Catch ex As Exception
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End Try

        Response.Redirect(FaultConstants.LoactionUrlString + customerId)

    End Sub
#End Region

End Class