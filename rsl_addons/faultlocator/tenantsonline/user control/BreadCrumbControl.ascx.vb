Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject

Partial Public Class BreadCrumbControl
    Inherits System.Web.UI.UserControl

    Public locationName As String
    Public locationUrl As String

    Public areaName As String
    Public areaUrl As String
    Public areaId As Integer

    Public elementName As String    
    Public elementUrl As String
    Public elementId As Integer

    Public faultId As Integer
    Public faultName As String
    Public faultUrl As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        locationName = Me.GetLoactionName()
        locationUrl = Me.GetLoactionUrl()

        areaName = Me.GetAreaName()
        areaUrl = Me.GetAreaUrl()        

        'Load Bread Crumb from db
        Me.LoadBreadCrumb()
    End Sub

    Protected Function GetLoactionName() As String
        Return Session(FaultConstants.BreadCrumbLoactionName).ToString()
    End Function

    Protected Function GetLoactionUrl()
        
        Return Session(FaultConstants.BreadCrumbLoactionUrl)
    End Function

    Protected Function GetAreaName()

        Return Session(FaultConstants.BreadCrumbAreaName)
    End Function

    Protected Function GetAreaUrl()

        Return Session(FaultConstants.BreadCrumbAreaUrl)
    End Function

#Region "LoadBreadCrumb"

    Public Sub LoadBreadCrumb()

        Dim breadBO As BreadCrumbBO = New BreadCrumbBO()

        Dim elementId As Integer = New Integer()

        Dim faultId As Integer = New Integer()

        'Get the User submitted fault 
        faultId = CType(Session(FaultConstants.SelectedFaultId), Integer)

        If Not Request.QueryString("elementId") = String.Empty Then

            elementId = Request.QueryString("elementId")

            breadBO.CurPageId = elementId

            breadBO.CurPageIdType = "Element"

        Else

            If faultId > 0 Then

                breadBO.CurPageId = faultId

                breadBO.CurPageIdType = "Fault"

            End If
        End If


        'Create the fault BL object
        Dim faultBL = New FaultManager()

        faultBL.LoadBreadCrumb(breadBO)

        Me.areaId = breadBO.AreaId

        Session(FaultConstants.SelectedAreaId) = Me.areaId

        Me.areaUrl = CType(Me.areaUrl, String) + "?areaId=" + CType(Me.areaId, String)

        Me.elementName = breadBO.ElementName

        Me.elementId = breadBO.ElementId

        Session(FaultConstants.SelectedElementId) = Me.elementId

        Me.elementUrl = FaultConstants.FaultListUrlString + CType(Me.elementId, String)

        Me.faultId = breadBO.FaultId

        Me.faultName = IIf(breadBO.FaultName <> "", " > " + breadBO.FaultName, "")



    End Sub
#End Region


    Protected Sub lnkLoacation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLoacation.Click

        Response.Redirect(Me.locationUrl)

    End Sub

    Protected Sub lnkArea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkArea.Click

        Response.RedirectLocation = Me.areaUrl
        Response.Redirect(Response.RedirectLocation)       

    End Sub

    Protected Sub lnkElement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkElement.Click

        Response.RedirectLocation = Me.elementUrl
        Response.Redirect(Response.RedirectLocation)

    End Sub

End Class