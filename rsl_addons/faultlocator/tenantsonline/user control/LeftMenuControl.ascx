<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LeftMenuControl.ascx.vb" Inherits="tenantsonline.LeftMenuControl" %>

<table style="text-align: left; background-color: #CC0000; width: 197px;">
     
    <tr>
        <td style="width: 203px;">
            <asp:HyperLink ID="lnkTempFaultBasket" runat="server" CssClass="menu_item" NavigateUrl="~/secure/faultlocator/temp_fault_basket.aspx" EnableTheming="True">> Fault basket</asp:HyperLink>
        </td>
    </tr>    
    <tr>
        <td style="width: 203px;">
            <asp:HyperLink ID="lnkFaultLog" runat="server" CssClass="menu_item" NavigateUrl="~/secure/faultlocator/fault_log.aspx" EnableTheming="True">> Fault log</asp:HyperLink>
        </td>
    </tr>

    
</table>
<table border="0"  cellpadding="2">
    
    <tr>
        <td>
            <asp:HyperLink ID="lnkIncreaseSize" runat="server" CssClass="link_resize" NavigateUrl="#" 
             EnableTheming="True" ForeColor="Red">A+</asp:HyperLink></td>
        <td>
<asp:Label ID="lblSeperator1" runat="server" ForeColor="Silver" Text="|" Width="5px"></asp:Label></td>
        <td>
             <asp:HyperLink ID="lnkReduceSize" runat="server" CssClass="link_resize" NavigateUrl="#" 
             EnableTheming="True" ForeColor="Red">A-</asp:HyperLink></td>
        <td>
<asp:Label ID="lblSeperator2" runat="server" ForeColor="Gray" Text="|" Width="5px"></asp:Label></td>
        <td>
             <asp:HyperLink ID="lnkReset" CssClass="link_resize" runat="server"  NavigateUrl="#" 
             EnableTheming="True" ForeColor="Red">Reset</asp:HyperLink></td>
    </tr>
</table>
