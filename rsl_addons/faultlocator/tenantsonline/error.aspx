<%@ Page Language="vb" MasterPageFile="~/master pages/EnquiryManagement.Master" AutoEventWireup="false"
    Codebehind="error.aspx.vb" Inherits="tenantsonline.CommonErrorPage" Title="Tenants Online :: Error Msg" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">
    <span style="color: #000000">An internal error has occured. </span>
    <br>
    <span style="color: #000000">
        Please contact RSL Support by sending an E-Mail to rslsupport@broadlandhousing.org&nbsp;<br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="lblErrorTrace" runat="server"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </span>
</asp:Content>
