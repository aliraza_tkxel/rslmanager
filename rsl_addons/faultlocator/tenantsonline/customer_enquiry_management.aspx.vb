Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class CustomerEnquiryManagement
    Inherits System.Web.UI.Page
    'Inherits System.Web.UI.Page

#Region "Page and Control event handling"

#Region "Page Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'If user isn't authenticated, he/she will be redirected to ~/signin.aspx
        'If IsNothing(Session("custHeadBO")) Then
        'Response.Redirect("~/signin.aspx")
        'End If
        'If ASPSession("USERID") = "" Then
        ''Response.Redirect("~/BHAIntranet/login.aspx")
        'End If

        Response.Cache.SetNoStore()
    End Sub

#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        'If ASPSession("USERID") = "" Then
        ''Response.Redirect("~/BHAIntranet/login.aspx")
        'End If
        If Not Me.IsPostBack Then
            'populate  nature and status dropdown with the values for search
            Me.GetNatureLookUpValues()
            Me.GetStatusLookUpValues()
            Me.SetKeyPostback()


            'storing in view state boolean value indicating , results being displayed in gridview are 
            'from search or not
            ViewState(ApplicationConstants.CustEnquiryIsSearchViewState) = True
            'storing current sort order of any column asc or dsen
            ViewState(ApplicationConstants.CustEnquirySortOrderViewSate) = ApplicationConstants.CustEnquiryManagDESCSortOrder

            'function used for storing search parameters which will be used for further page requests if result is 
            'multipage
            SaveSearchOptions()
            SaveResponseSearchOptions()
            'setting initial page index of gridview by default its zero
            GVEnquiryLog.PageIndex = ApplicationConstants.CustEnquiryInitialPageIndex
            GVResponses.PageIndex = ApplicationConstants.CustResponseInitialPageIndex '''''''''

            'getting and setting row count of resultset
            GVEnquiryLog.VirtualItemCount = GetSearchRowCount()
            GVResponses.VirtualItemCount = GetResponseSearchRowCount() '''''

            ''''''''''''''''''''''''''

            Me.txtFirstName.Focus()

        End If
        toggleGridVisibility(Me.ddlEnqType.SelectedIndex)
    End Sub

#End Region

    ''''''''''''''''''''''''''''''''''''''
    Protected Sub ddlEnqType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlEnqType.SelectedIndexChanged
        Dim ddlType As DropDownList = CType(sender, DropDownList)
        toggleGridVisibility(ddlType.SelectedIndex)
    End Sub

    ''''''''''''''''''''''''''''''''''''''

#Region "Handler for delete enquiry button"
    Protected Sub BtnDeleteEnquiry_Click(ByVal sender As Object, ByVal e As EventArgs)
        'casting sender of this event into button object before getting command arguments from
        'button object
        Dim btn As Button = DirectCast(sender, Button)
        'creating array of command arguments send by delete enquiry button
        Dim strArr As String() = btn.CommandArgument.Split(",")
        'creating terminationBO object
        Dim termBO As TerminationBO = New TerminationBO()
        'setting enquirylogid property of terminationBO
        termBO.EnquiryLogId = CType(strArr(0), Integer)
        'creating EnquiryManager object
        Dim enqryManager As EnquiryManager = New EnquiryManager()
        'calling function DeleteCustomerEnquiry function of EnquiryManager
        'that will be used to mark the current record as deleted in db
        enqryManager.DeleteCustomerEnquiry(termBO)

        ' If delete operation is successful, decrease the grid count by 1 since the grid will be not fetched from
        ' the database again
        GVEnquiryLog.VirtualItemCount = GVEnquiryLog.VirtualItemCount - 1

        'storing the current page 
        Dim tmpPageIndex As Integer = GVEnquiryLog.PageIndex
        GVEnquiryLog.PageIndex = 0
        'after deletion populating the gridview again 
        If DirectCast(ViewState(ApplicationConstants.CustEnquiryIsSearchViewState), Boolean) = True Then
            'function used for populating gridview when record deleted belonged to particular serach record
            GetSearchResults()
        Else
            'function used for populating gridview when record deleted belonged to all serach record
            BindEnquiryLogGrid()
        End If
        'manually restoring the current page again
        Dim ee As GridViewPageEventArgs = New GridViewPageEventArgs(tmpPageIndex)
        GVEnquiryLog_PageIndexChanging(Nothing, ee)
    End Sub


#End Region

#Region "Handler for Create CRM"
    Protected Sub BtnCreateCRM_Click(ByVal sender As Object, ByVal e As EventArgs)
        'casting sender object as button 
        Dim btn As Button = DirectCast(sender, Button)
        'recovering command arguments from button
        Dim str As String = btn.CommandArgument
        Dim enqryManager As EnquiryManager = New EnquiryManager()
        Dim enqryLog As EnquiryLogBO = New EnquiryLogBO()
        'setting enquirylogid using command argument received
        enqryLog.EnquiryLogId = CType(str, Integer)
        'calling GetCustomerIDByEnquiryLog of EnquiryManager for getting parameters that need to be 
        'passed with the string
        enqryManager.GetCustomerIDByEnquiryLog(enqryLog)

        Dim crmPath = System.Configuration.ConfigurationManager.AppSettings("create_crm_path").ToString()
        Response.Redirect(crmPath & enqryLog.CustomerId & "&EnquiryID=" & str)
    End Sub
#End Region

#Region "GridView Page Index Handler"
    Protected Sub GVEnquiryLog_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVEnquiryLog.PageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreResultSummary()
        Dim enquiryDS As EnquiryLogDS = New EnquiryLogDS()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.CustEnquiryIsSearchViewState), Boolean) = True Then
            GetSearchResults()
        Else
            BindEnquiryLogGrid()
        End If

    End Sub
#End Region

#Region "View Details Handler"

    Protected Sub BtnViewDetails_Click(ByVal sender As Object, ByVal e As EventArgs)
        'getting style layout from application constants for pop over panel
        pnlPopup.Attributes("style") = ApplicationConstants.CustEnquiryShowPanelStyleValue

        'casting object as a button before further processing
        Dim btn As Button = DirectCast(sender, Button)
        'getting command arguments from button and storing them in string aray
        Dim strArr As String() = btn.CommandArgument.Split(",")
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()

        'if passed argument equals termination request thn GetTerminationInfo is called
        'that will show termination details of this particular enquiry in popover window
        If strArr(5).Equals(ApplicationConstants.CustEnquiryTerminationRequest) Then
            GetTerminationInfo(strArr)
            'if passed argument equals ServiceComplaint thn GetComplaintInfo is called
            'that will show ServiceComplaint details of this particular enquiry in popover window
        ElseIf strArr(5).Equals(ApplicationConstants.CustEnquiryServiceComplaint) Then
            GetComplaintInfo(strArr)
            'if passed argument equals GarageParkingRequest thn GetGarageParkingInfo is called
            'that will show GarageParkingRequest details of this particular enquiry in popover window
        ElseIf strArr(5).Equals(ApplicationConstants.CustEnquiryGarageParkingRequest) Then
            GetGarageParkingInfo(strArr)
            'if passed argument equals ASBComplaint thn GetASBInfo is called
            'that will show ASBComplaint details of this particular enquiry in popover window
        ElseIf strArr(5).Equals(ApplicationConstants.CustEnquiryASBComplaint) Then
            GetASBInfo(strArr)
            'if passed argument equals AbandonedReport thn GetAbandonInfo is called
            'that will show AbandonedReport details of this particular enquiry in popover window
        ElseIf strArr(5).Equals(ApplicationConstants.CustEnquiryAbandonedReport) Then
            GetAbandonInfo(strArr)
            'if passed argument equals TransferRequest thn GetTransferInfo is called
            'that will show TransferRequest details of this particular enquiry in popover window
        ElseIf strArr(5).Equals(ApplicationConstants.CustEnquiryTransferRequest) Then
            GetTransferInfo(strArr)
            'if passed argument equals ClearArrearsRequest thn GetClearArrearInfo is called
            'that will show ClearArrearsRequest details of this particular enquiry in popover window
        ElseIf strArr(5).Equals(ApplicationConstants.CustEnquiryClearArrearsRequest) Then
            GetClearArrearInfo(strArr)
            'if passed argument equals DirectDebitRentRequest thn GetClearDirectDebitRentInfo is called
            'that will show DirectDebitRentRequest details of this particular enquiry in popover window
        ElseIf strArr(5).Equals(ApplicationConstants.CustEnquiryDirectDebitRentRequest) Then
            GetClearDirectDebitRentInfo(strArr)
            'if passed argument equals CstRequest thn GetCstRequestInfo is called
            'that will show CstRequest details of this particular enquiry in popover window
        ElseIf strArr(5).Equals(ApplicationConstants.CustEnquiryCstRequest) Then
            GetCstRequestInfo(strArr)
        End If

        'update popup
        Me.updPnlCustomerDetail.Update()

        'show the modal popup
        Me.mdlPopup.Show()
        pnlPopup.Attributes("style") = ApplicationConstants.CustEnquiryHidePanelStyleValue
# End Sub
#End Region

#Region "GridView Sorting Handler"
    Protected Sub GVEnquiryLog_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.CustEnquirySortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVEnquiryLog.PageIndex = ApplicationConstants.CustEnquiryInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.CustEnquirySortOrderViewSate), String) = ApplicationConstants.CustEnquiryManagDESCSortOrder Then
            ViewState(ApplicationConstants.CustEnquirySortOrderViewSate) = ApplicationConstants.CustEnquiryManagASECSortOrder
        Else
            ViewState(ApplicationConstants.CustEnquirySortOrderViewSate) = ApplicationConstants.CustEnquiryManagDESCSortOrder
        End If

        'populating gridview with new results
        If DirectCast(ViewState(ApplicationConstants.CustEnquiryIsSearchViewState), Boolean) = True Then
            GetSearchResults()
        Else
            BindEnquiryLogGrid()
        End If
    End Sub
#End Region

#Region "Handler for Navigation Buttons Next Previous"
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)

        Select Case e.CommandName
            Case "Next"

                GVEnquiryLog.PageIndex = GVEnquiryLog.PageIndex + 1
                Exit Select
            Case "Prev"
                GVEnquiryLog.PageIndex = GVEnquiryLog.PageIndex - 1
                Exit Select
        End Select

        If Math.Floor((GVEnquiryLog.PageIndex - 1) / ApplicationConstants.CustEnquiryManagModNumber) <> Math.Floor((GVEnquiryLog.PageIndex) / ApplicationConstants.CustEnquiryManagModNumber) Then
            If DirectCast(ViewState(ApplicationConstants.CustEnquiryIsSearchViewState), Boolean) = True Then
                GetSearchResults()
            Else
                BindEnquiryLogGrid()
            End If
        Else
            ViewState("PageIndex") = GVEnquiryLog.PageIndex
            Dim enquiryDS As EnquiryLogDS = DirectCast(ViewState(ApplicationConstants.CustEnquiryDataSetViewState), EnquiryLogDS)
            GVEnquiryLog.DataSource = enquiryDS
            GVEnquiryLog.DataBind()

            GVResponses.DataSource = enquiryDS
            GVResponses.DataBind()

        End If
        'ShowHideNavButton()
    End Sub
#End Region

#Region "Handler for search button"

    Protected Sub btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn.Click
        'save these new search options entered by user unless search button is pressed again
        SaveSearchOptions()
        'setting new starting page index of for this new search
        GVEnquiryLog.PageIndex = ApplicationConstants.CustEnquiryInitialPageIndex
        'getting and setting row count of new search results
        GVEnquiryLog.VirtualItemCount = GetSearchRowCount()
        'getting search results

        If Me.ddlEnqType.SelectedIndex = 0 Then
            GetSearchResults()
        Else
            GetResponseSearchResults()
        End If
    End Sub

#End Region

#Region "handler for dataset2excel button"
    Protected Sub BtnDataSet2Excel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDataSet2Excel.Click
        'creating new object of EnquiryLogDS dataset
        Dim enquiryDS As EnquiryLogDS = New EnquiryLogDS()
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()

        If DirectCast(ViewState(ApplicationConstants.CustEnquiryIsSearchViewState), Boolean) = True Then
            'reteriving search options  and casting them into enqryLogSearchBO
            enqryLogSearchBO = DirectCast(ViewState(ApplicationConstants.CustEnquirySearchOptionsViewState), EnquiryLogSearchBO)
        End If
        If (Me.ddlEnqType.SelectedIndex = 0) Then
            'GetDataPage function called for populating dataset
            GetDataPage(0, GVEnquiryLog.VirtualItemCount, ApplicationConstants.CustEnquiryManagASECSortOrder, enquiryDS, enqryLogSearchBO)

        Else
            'GetDataPage function called for populating dataset
            GetDataPage(0, GVResponses.VirtualItemCount, ApplicationConstants.CustEnquiryManagASECSortOrder, enquiryDS, enqryLogSearchBO)

        End If
        'passing enquiryDS to ConvertDataSet2Excel function that will write it in xml file
        ConvertDataSet2Excel(enquiryDS)
        
    End Sub
#End Region

#Region "Handling rowcreated event to check for empty rows"
    Sub GVEnquiryLog_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'if a new row gets created get its row index
        Dim rowIndex As Integer = e.Row.RowIndex
        'if newly created row has index greater thn number of rows in search result thn hide this empty row
        Dim numofRows As Integer = CType(ViewState("NumberofRows"), Integer)
        If rowIndex > numofRows Then
            e.Row.Visible = False
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'if the row being created is of pager type thn create a lable and sets its text property
        'equal to result sumary and add this label to pager row
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim lblResultSummary As Label = New Label()
            lblResultSummary.Text = CType(ViewState(ApplicationConstants.CustEnquiryResultSummary), String)
            e.Row.Cells(0).Controls.Add(lblResultSummary)

        End If
    End Sub

    Protected Sub GVResponses_RowCreated(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        'if a new row gets created get its row index
        Dim rowIndex As Integer = e.Row.RowIndex
        'if newly created row has index greater thn number of rows in search result thn hide this empty row
        Dim numofRows As Integer = CType(ViewState("NumberofRows"), Integer)
        If rowIndex > numofRows Then
            e.Row.Visible = False
        End If

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'if the row being created is of pager type thn create a lable and sets its text property
        'equal to result sumary and add this label to pager row
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim lblResultSummary As Label = New Label()
            lblResultSummary.Text = CType(ViewState(ApplicationConstants.CustResponseResultSummary), String)
            e.Row.Cells(0).Controls.Add(lblResultSummary)

        End If
    End Sub
#End Region

#Region "txtDate Init"
    Protected Sub txtDate_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDate.Init
        'overriding the readonly property of readonly textbox so that we can read text from "readonly" textbox
        'txtDate.Attributes.Add("readonly", "readonly")
    End Sub
#End Region

#End Region

#Region "Dynamic data query"

#Region "Helper to bind the grid to the dynamic data"
    ''' Helper to bind the grid to the dynamic data
    Private Sub BindEnquiryLogGrid()
        Dim enquiryDS As EnquiryLogDS = New EnquiryLogDS()
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        GetDataPage(GVEnquiryLog.PageIndex, ApplicationConstants.CustEnquiryManagResultPerPage, GVEnquiryLog.OrderBy, enquiryDS, enqryLogSearchBO)
        GVEnquiryLog.DataSource = enquiryDS
        GVEnquiryLog.DataBind()
    End Sub
#End Region

    '''''''''''''''''''''''''''''''''''''''

#Region "Helper to bind the grid to the dynamic data"
    ''' Helper to bind the grid to the dynamic data
    Private Sub BindResponsesGrid()
        Dim enquiryDS As EnquiryLogDS = New EnquiryLogDS()
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        GetResponseDataPage(GVResponses.PageIndex, ApplicationConstants.CustResponseResultPerPage, GVResponses.OrderBy, enquiryDS, enqryLogSearchBO)

        GVResponses.DataSource = enquiryDS
        GVResponses.DataBind()

    End Sub
#End Region

    '''''''''''''''''''''''''''''''''''''''

#Region "Function to count number of rows in table"
    Private Function GetRowCount() As Integer
        Dim enqryMngr As EnquiryManager = New EnquiryManager()
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        Dim count As Integer = enqryMngr.GetEnquiryLogRowCount(enqryLogSearchBO)
        Return count
    End Function
#End Region

#Region "Function to count number of rows in search result"
    Private Function GetSearchRowCount() As Integer
        Dim enqryMngr As EnquiryManager = New EnquiryManager()
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        'reterive search options and get count of rows for these search options using GetEnquiryLogSearchRowCount
        'function of EnquiryManager
        enqryLogSearchBO = DirectCast(ViewState(ApplicationConstants.CustEnquirySearchOptionsViewState), EnquiryLogSearchBO)
        Dim count As Integer = enqryMngr.GetEnquiryLogSearchRowCount(enqryLogSearchBO, "Enquiry")
        Return count
    End Function


    ''' ''''''''''''''''''''''''''''''''''
    Private Function GetResponseSearchRowCount() As Integer
        Dim enqryMngr As EnquiryManager = New EnquiryManager()
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        'reterive search options and get count of rows for these search options using GetEnquiryLogSearchRowCount
        'function of EnquiryManager
        enqryLogSearchBO = DirectCast(ViewState(ApplicationConstants.CustEnquirySearchOptionsViewState), EnquiryLogSearchBO)
        Dim count As Integer = enqryMngr.GetEnquiryLogSearchRowCount(enqryLogSearchBO, "Response")
        Return count
    End Function
    ''' ''''''''''''''''''''''''''''''''''

#End Region

#Region "Function to get data "
    Public Sub GetDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef enquiryDS As EnquiryLogDS, ByVal enqryLogSearchBO As EnquiryLogSearchBO)

        'reteriving sortby column name from viewstate
        If ViewState(ApplicationConstants.CustEnquirySortByViewState) <> Nothing Then
            enqryLogSearchBO.SortBy = CType(ViewState(ApplicationConstants.CustEnquirySortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use equirylogid as a default
            enqryLogSearchBO.SortBy = ApplicationConstants.CustEnquiryManagSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        enqryLogSearchBO.PageIndex = pageIndex
        enqryLogSearchBO.RowCount = pageSize

        'reteriving sort order from viewstate
        enqryLogSearchBO.SortOrder = DirectCast(ViewState(ApplicationConstants.CustEnquirySortOrderViewSate), String)
        Dim enqryMngr As EnquiryManager = New EnquiryManager()
        'passing reference of dataset and passing enqryLogSearchBO to EnquiryLogData function
        'of EnquiryManager
        enqryMngr.EnquiryLogData(enquiryDS, enqryLogSearchBO)
        'storing number of rows in the result
        ViewState("NumberofRows") = enquiryDS.Tables(0).Rows.Count

        'setting BtnDataSet2Excel button visible true false according to result count
        If enquiryDS.Tables(0).Rows.Count < 1 Then
            BtnDataSet2Excel.Visible = False
        Else
            BtnDataSet2Excel.Visible = True
        End If

        ''Check for exception 
        Dim exceptionFlag As Boolean = enqryLogSearchBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("error.aspx")
        End If

    End Sub
#End Region

    ''' ''''''''''''''''''''''''''''''
    
#Region "Function to get response data"
    Public Sub GetResponseDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef enquiryDS As EnquiryLogDS, ByVal enqryLogSearchBO As EnquiryLogSearchBO)

        'reteriving sortby column name from viewstate
        If ViewState(ApplicationConstants.CustEnquirySortByViewState) <> Nothing Then
            enqryLogSearchBO.SortBy = CType(ViewState(ApplicationConstants.CustEnquirySortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use equirylogid as a default
            enqryLogSearchBO.SortBy = ApplicationConstants.CustEnquiryManagSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        enqryLogSearchBO.PageIndex = pageIndex
        enqryLogSearchBO.RowCount = pageSize

        'reteriving sort order from viewstate
        enqryLogSearchBO.SortOrder = DirectCast(ViewState(ApplicationConstants.CustEnquirySortOrderViewSate), String)
        enqryLogSearchBO.Status = 29
        Dim enqryMngr As EnquiryManager = New EnquiryManager()
        'passing reference of dataset and passing enqryLogSearchBO to EnquiryLogData function
        'of EnquiryManager
        enqryMngr.CustomerResponseData(enquiryDS, enqryLogSearchBO)
        'storing number of rows in the result
        ViewState("NumberofRows") = enquiryDS.Tables(0).Rows.Count

        'setting BtnDataSet2Excel button visible true false according to result count
        If enquiryDS.Tables(0).Rows.Count < 1 Then
            BtnDataSet2Excel.Visible = False
        Else
            BtnDataSet2Excel.Visible = True
        End If

        ''Check for exception 
        Dim exceptionFlag As Boolean = enqryLogSearchBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("error.aspx")
        End If

    End Sub
#End Region

    ''' ''''''''''''''''''''''''''''''

#Region "Sub for getting search results"

    Protected Sub GetSearchResults()
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        'reterive search options enqryLogSearchBO from viewstate before requesting results for next page of gridview
        enqryLogSearchBO = DirectCast(ViewState(ApplicationConstants.CustEnquirySearchOptionsViewState), EnquiryLogSearchBO)
        Dim enquiryDS As EnquiryLogDS = New EnquiryLogDS()
        'calling GetDataPage function that will further call function in buisness layer
        GetDataPage(GVEnquiryLog.PageIndex, ApplicationConstants.CustEnquiryManagResultPerPage, GVEnquiryLog.OrderBy, enquiryDS, enqryLogSearchBO)
        'binding gridview with the result returned from db
        Me.StoreResultSummary()
        GVEnquiryLog.DataSource = enquiryDS
        GVEnquiryLog.DataBind()
    End Sub

#End Region


    '''''''''''''''''''''''''''''''''''''''''''''
#Region "Sub for getting search results responses"

    Protected Sub GetResponseSearchResults()
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        'reterive search options enqryLogSearchBO from viewstate before requesting results for next page of gridview
        enqryLogSearchBO = DirectCast(ViewState(ApplicationConstants.CustResponseSearchOptionsViewState), EnquiryLogSearchBO)
        Dim enquiryDS As EnquiryLogDS = New EnquiryLogDS()
        'calling GetDataPage function that will further call function in buisness layer
        GetResponseDataPage(GVResponses.PageIndex, ApplicationConstants.CustResponseResultPerPage, GVResponses.OrderBy, enquiryDS, enqryLogSearchBO)
        'binding gridview with the result returned from db
        Me.StoreResultSummaryResponses()

        GVResponses.DataSource = enquiryDS
        GVResponses.DataBind()

    End Sub

#End Region

    '''''''''''''''''''''''''''''''''''''''''''''

#Region "Sub for hidding and showing next previous buttons"

    Private Sub ShowHideNavButton()
        NextPage.Visible = True
        PreviousPage1.Visible = True
        If GVEnquiryLog.PageIndex = Decimal.Ceiling((GVEnquiryLog.VirtualItemCount / ApplicationConstants.CustEnquiryManagResultPerPage) - 1) Then
            NextPage.Visible = False
        ElseIf GVEnquiryLog.PageIndex = 0 Then
            PreviousPage1.Visible = False
        End If
    End Sub

#End Region

#Region "Sub for storing search options"

    Private Sub SaveSearchOptions()
        'make EnquiryLogSearchBO object and store the search options entered by user untill he/she enters again
        'in this case the values will be overidden
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        'set firstname enetered
        enqryLogSearchBO.FirstName = IIf(txtFirstName.Text = "", Nothing, txtFirstName.Text)
        'set last name enetered
        enqryLogSearchBO.LastName = IIf(txtLastName.Text = "", Nothing, txtLastName.Text)
        'set tenanacyid entered
        enqryLogSearchBO.TenancyID = IIf(txtTenancyID.Text = "", Nothing, txtTenancyID.Text)

        'set text value enetered
        enqryLogSearchBO.TextValue = IIf(txtText.Text = "", Nothing, txtText.Text)
        If txtDate.Text <> "" Then
            Try
                'convert date string to date object and set its value
                enqryLogSearchBO.DateValue = UtilityFunctions.stringToDate(Me.txtDate.Text)
            Catch ex As Exception
                Me.lblErrorMessage.Text = "Please enter a valid date"
                Return
            End Try

        End If
        'set status value enetered
        enqryLogSearchBO.Status = IIf(ddlStatus.SelectedItem.Value = "", ApplicationConstants.CustomerEnquiryStatusNew, ddlStatus.SelectedItem.Value)
        'set nature value entered
        enqryLogSearchBO.Nature = IIf(ddlNature.SelectedItem.Value = "", Nothing, ddlNature.SelectedItem.Value)
        'tell EnquiryLogSearchBO it has values that will be used for searching
        enqryLogSearchBO.IsSearch = True
        'store EnquiryLogSearchBO object in viewstate
        ViewState(ApplicationConstants.CustEnquirySearchOptionsViewState) = enqryLogSearchBO
        'store true in viewstate to indicate that current data on gridview is from search
        ViewState(ApplicationConstants.CustEnquiryIsSearchViewState) = True
    End Sub


    Private Sub SaveResponseSearchOptions()
        'make EnquiryLogSearchBO object and store the search options entered by user untill he/she enters again
        'in this case the values will be overidden
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        'set firstname enetered
        enqryLogSearchBO.FirstName = IIf(txtFirstName.Text = "", Nothing, txtFirstName.Text)
        'set last name enetered
        enqryLogSearchBO.LastName = IIf(txtLastName.Text = "", Nothing, txtLastName.Text)
        'set tenanacyid entered
        enqryLogSearchBO.TenancyID = IIf(txtTenancyID.Text = "", Nothing, txtTenancyID.Text)

        'set text value enetered
        enqryLogSearchBO.TextValue = IIf(txtText.Text = "", Nothing, txtText.Text)
        If txtDate.Text <> "" Then
            Try
                'convert date string to date object and set its value
                enqryLogSearchBO.DateValue = UtilityFunctions.stringToDate(Me.txtDate.Text)
            Catch ex As Exception
                Me.lblErrorMessage.Text = "Please enter a valid date"
                Return
            End Try

        End If
        'set status value enetered
        enqryLogSearchBO.Status = IIf(ddlStatus.SelectedItem.Value = "", ApplicationConstants.CustomerEnquiryStatusNew, ddlStatus.SelectedItem.Value)
        'set nature value entered
        enqryLogSearchBO.Nature = IIf(ddlNature.SelectedItem.Value = "", Nothing, ddlNature.SelectedItem.Value)
        'tell EnquiryLogSearchBO it has values that will be used for searching
        enqryLogSearchBO.IsSearch = True
        'store EnquiryLogSearchBO object in viewstate
        ViewState(ApplicationConstants.CustResponseSearchOptionsViewState) = enqryLogSearchBO
        'store true in viewstate to indicate that current data on gridview is from search
        ViewState(ApplicationConstants.CustEnquiryIsSearchViewState) = True
    End Sub

#End Region

#Region "function for diabling createCRM button based on status search option"
    Public Function GetStatusType() As Boolean
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        'disable create crm button on gridview if gridview is showing any other enteries having status other
        'thn NEW
        enqryLogSearchBO = CType(ViewState(ApplicationConstants.CustEnquirySearchOptionsViewState), EnquiryLogSearchBO)
        If enqryLogSearchBO.Status.Equals(ApplicationConstants.CustomerEnquiryStatusNew) Then
            Return True
        Else
            Return False
        End If
        Return False
    End Function
#End Region

#Region "function for disbaling and enabling delete button"
    Public Function GetDeleteBtnStatus() As Boolean
        Dim enqryLogSearchBO As EnquiryLogSearchBO = New EnquiryLogSearchBO()
        'disbale delete button for enteries that are already deleted
        enqryLogSearchBO = CType(ViewState(ApplicationConstants.CustEnquirySearchOptionsViewState), EnquiryLogSearchBO)
        If enqryLogSearchBO.Status.Equals(ApplicationConstants.CustomerEnquiryStatusDeleted) Then
            Return False
        Else
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Write to excel file"
    Private Sub ConvertDataSet2Excel(ByRef ds As EnquiryLogDS)
        'make a datatable object
        Dim dtable As DataTable = New DataTable()
        'get datatable from dataset
        dtable = ds.Tables(0)
        'get default file name and file tppe from constants
        Dim attachment As String = ApplicationConstants.CustEnquiryAttachment
        Response.ClearContent()
        Response.AddHeader(ApplicationConstants.CustEnquiryHTTPHeader, attachment)
        'set content type which in this case is excel
        Response.ContentType = ApplicationConstants.CustEnquiryContentType
        Dim tab As String = ""
        'write data column names
        For Each dc As DataColumn In dtable.Columns
            Response.Write(tab + dc.ColumnName)
            tab = "" & Chr(9) & ""
        Next
        Response.Write("" & Chr(10) & "")

        Dim i As Integer
        'read datatable row by row and write 
        For Each dr As DataRow In dtable.Rows
            tab = ""
            For i = 0 To dtable.Columns.Count - 1
                Response.Write(tab + dr(i).ToString())
                tab = "" & Chr(9) & ""
            Next
            Response.Write("" & Chr(10) & "")
        Next
        Response.[End]()
    End Sub
#End Region

#Region "Format view details data"

    Public Sub FormatViewDetails(ByVal custAddBO As CustomerAddressBO, ByVal strArr() As String)
        lblDateVal.Text = IIf(strArr(1) <> "", strArr(1), "")
        lblEnquiryNoVal.Text = IIf(strArr(0) <> "", UtilityFunctions.GetEnquiryString(strArr(0)), "")
        lblTenancyNoVal.Text = IIf(strArr(2) <> "", strArr(2), "")
        lblNameVal.Text = IIf(strArr(3) <> "", strArr(3), "")
        lblEnqryItemVal.Text = IIf(strArr(4) <> "", strArr(4), "")
        lblEnqryNatureVal.Text = IIf(strArr(5) <> "", strArr(5), "")

        lblTelVal.Text = IIf(custAddBO.Telephone <> "", custAddBO.Telephone, "")
        lblAddressVal.Text = IIf(custAddBO.HouseNumber <> "", custAddBO.HouseNumber, "")
        lblAddressVal.Text &= IIf(custAddBO.Address1 <> "", custAddBO.Address1, "")
        lblAddress3.Text = IIf(custAddBO.Address3 <> "", custAddBO.Address3, "")
        lblCounty.Text = IIf(custAddBO.Coutnty <> "", custAddBO.Coutnty, "")
        lblPostCode.Text = IIf(custAddBO.PostCode <> "", custAddBO.PostCode, "")

    End Sub
#End Region

#Region "Get Termination Info"
    Private Sub GetTerminationInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim termBO As TerminationBO = New TerminationBO()
        'get EnquiryLogId and set the EnquiryLogId property of TerminationBO
        termBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and termBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetTerminationInfo(custAddBO, termBO)
        Dim isExceptionGen As Boolean = termBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If
        txtPODetails.Text = "I would like end my tenancy on: " & termBO.MovingOutDate & System.Environment.NewLine & termBO.Description()

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)
    End Sub
#End Region

#Region "Get Complaint Info"
    Private Sub GetComplaintInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim cmpltBO As ComplaintBO = New ComplaintBO()
        cmpltBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and cmpltBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetComplaintInfo(custAddBO, cmpltBO)
        Dim isExceptionGen As Boolean = cmpltBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If

        txtPODetails.Text = "I'm complaining about: " & cmpltBO.CategoryText & System.Environment.NewLine & "My complaint is: " & cmpltBO.Description()

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)

    End Sub
#End Region

#Region "GetASBInfo Anti Social Behaviour"
    Private Sub GetASBInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim abBO As AsbBO = New AsbBO()
        abBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and abBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetASBInfo(custAddBO, abBO)
        Dim isExceptionGen As Boolean = abBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If

        txtPODetails.Text = "Category: " & abBO.CategoryText & System.Environment.NewLine & "Explain your problem: " & abBO.Description()

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)

    End Sub
#End Region

#Region "GetGarageParkingInfo"
    Private Sub GetGarageParkingInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim gpBO As GarageParkingBO = New GarageParkingBO()
        gpBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and gpBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetGarageParkingInfo(custAddBO, gpBO)
        Dim isExceptionGen As Boolean = gpBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If

        txtPODetails.Text = "I'd like a: " & gpBO.LookUpValueText & System.Environment.NewLine & gpBO.Description()

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)

    End Sub
#End Region

#Region "GetAbandonInfo"

    Private Sub GetAbandonInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim abndBO As AbandonBO = New AbandonBO()
        abndBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and abndBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetAbandonInfo(custAddBO, abndBO)
        Dim isExceptionGen As Boolean = abndBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If

        txtPODetails.Text = "There is an abandoned: " & abndBO.LookUpValueText & " at " & abndBO.Location & System.Environment.NewLine & abndBO.Description()

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)

    End Sub

#End Region

#Region "GetTransferInfo"

    Private Sub GetTransferInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim houseTransBO As HouseMoveBO = New HouseMoveBO
        houseTransBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and houseTransBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetTransferInfo(custAddBO, houseTransBO)
        Dim isExceptionGen As Boolean = houseTransBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If
        txtPODetails.Text = "Reason to move house: " & houseTransBO.Description & System.Environment.NewLine
        txtPODetails.Text &= "Local Authority: " & houseTransBO.LocalAuthority & System.Environment.NewLine
        txtPODetails.Text &= "Development: " & houseTransBO.Development & System.Environment.NewLine
        txtPODetails.Text &= "Number of Bedrooms :" & houseTransBO.NoOfBedrooms & System.Environment.NewLine
        txtPODetails.Text &= "Occupants above 18 :" & houseTransBO.OccupantsNoBelow18 & System.Environment.NewLine
        txtPODetails.Text &= "Occupants below 18 :" & houseTransBO.OccupantsNoGreater18 & System.Environment.NewLine

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)
    End Sub

#End Region

#Region "GetClearArrearInfo"

    Private Sub GetClearArrearInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim enqBO As EnquiryLogBO = New EnquiryLogBO
        enqBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and enqBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetClearArrearInfo(custAddBO, enqBO)
        Dim isExceptionGen As Boolean = enqBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If

        txtPODetails.Text = "Details: " & enqBO.Description

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)
    End Sub

#End Region

#Region "GetClearDirectDebitRentInfo"

    Private Sub GetClearDirectDebitRentInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim enqBO As EnquiryLogBO = New EnquiryLogBO
        enqBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and enqBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetClearDirectDebitRentInfo(custAddBO, enqBO)
        Dim isExceptionGen As Boolean = enqBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If

        txtPODetails.Text = "Details: " & enqBO.Description

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)
    End Sub

#End Region

#Region "GetCstRequestInfo"

    Private Sub GetCstRequestInfo(ByVal str() As String)
        Dim custAddBO As CustomerAddressBO = New CustomerAddressBO()
        Dim cstReqBO As CstBO = New CstBO
        cstReqBO.EnquiryLogId = CType(str(0), Integer)
        Dim custBL As CustomerManager = New CustomerManager()
        'pass custAddBO and cstReqBO to GetTerminationInfo by reference to get values against EnquiryLogId
        custBL.GetCstRequestInfo(custAddBO, cstReqBO)
        Dim isExceptionGen As Boolean = cstReqBO.IsExceptionGenerated

        If isExceptionGen Then
            Response.Redirect("error.aspx")
        End If

        txtPODetails.Text = "Details: " & cstReqBO.Description & System.Environment.NewLine
        txtPODetails.Text &= "Request reason :" & cstReqBO.RequestNature

        If (Me.ddlEnqType.SelectedIndex = 1) Then
            txtPODetails.Text &= System.Environment.NewLine & System.Environment.NewLine & "Customer Response: " & str(7)
        End If

        FormatViewDetails(custAddBO, str)

    End Sub

#End Region

#End Region

#Region "UtilityMethods"

    ''Set the attribute so that when user presses enter key the specified event for the mentioned button will be fired
    Private Sub SetKeyPostback()

        Me.txtFirstName.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btn.UniqueID + "').click();return false;}} else {return true}; ")
        Me.txtLastName.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btn.UniqueID + "').click();return false;}} else {return true}; ")
        Me.txtTenancyID.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btn.UniqueID + "').click();return false;}} else {return true}; ")
        Me.txtText.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btn.UniqueID + "').click();return false;}} else {return true}; ")

    End Sub

    Private Sub toggleGridVisibility(ByVal index As Integer)

        If (index = 0) Then
            'manually setting page index of gridview on page load
            Dim ee As GridViewPageEventArgs = New GridViewPageEventArgs(GVEnquiryLog.PageIndex)
            GVEnquiryLog_PageIndexChanging(Nothing, ee)
            'when page load apply focus on first name text box in search pane

            Me.GVEnquiryLog.Visible = True
            Me.GVResponses.Visible = False
        Else
            ''''''''''''''''''''''''''
            Dim re As GridViewPageEventArgs = New GridViewPageEventArgs(GVResponses.PageIndex)
            GVResponse_PageIndexChanging(Nothing, re)
            'when page load apply focus on first name text box in search pane

            Me.GVResponses.Visible = True
            Me.GVEnquiryLog.Visible = False
        End If
    End Sub

#Region "GetLookUpValues"

    Private Sub GetNatureLookUpValues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getNatureLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlNature.DataSource = lstLookUp
            Me.ddlNature.DataValueField = "LookUpValue"
            Me.ddlNature.DataTextField = "LookUpName"
            Me.ddlNature.Items.Add(New ListItem("Please select", ""))
            Me.ddlNature.DataBind()
        End If

    End Sub


    Private Sub GetStatusLookUpValues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getStatusLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlStatus.DataSource = lstLookUp
            Me.ddlStatus.DataValueField = "LookUpValue"
            Me.ddlStatus.DataTextField = "LookUpName"
            Me.ddlStatus.Items.Add(New ListItem("Please select", ""))
            Me.ddlStatus.DataBind()

        End If

    End Sub


#End Region

#End Region

#Region "Store Result Summary"
    'when page index changes this functions gets called 
    Public Sub StoreResultSummary()
        Dim row As GridViewRow = GVEnquiryLog.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVEnquiryLog.PageIndex + 1 = GVEnquiryLog.PageCount Then
            strResultSummary = "Result " & ((GVEnquiryLog.PageIndex * GVEnquiryLog.PageSize) + 1) & " to " & GVEnquiryLog.VirtualItemCount & " of " & GVEnquiryLog.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVEnquiryLog.PageIndex * GVEnquiryLog.PageSize) + 1) & " to " & ((GVEnquiryLog.PageIndex + 1) * GVEnquiryLog.PageSize) & " of " & GVEnquiryLog.VirtualItemCount
        End If
        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.CustEnquiryResultSummary) = strResultSummary
    End Sub
#End Region

    ''''''''''''''''''''''''''''''''''''''''''
#Region "Store Result Summary Responses"
    'when page index changes this functions gets called 
    Public Sub StoreResultSummaryResponses()
        Dim row As GridViewRow = GVResponses.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVResponses.PageIndex + 1 = GVResponses.PageCount Then
            strResultSummary = "Result " & ((GVResponses.PageIndex * GVResponses.PageSize) + 1) & " to " & GVResponses.VirtualItemCount & " of " & GVResponses.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVResponses.PageIndex * GVResponses.PageSize) + 1) & " to " & ((GVResponses.PageIndex + 1) * GVResponses.PageSize) & " of " & GVResponses.VirtualItemCount
        End If
        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.CustResponseResultSummary) = strResultSummary
    End Sub
#End Region
    ''''''''''''''''''''''''''''''''''''''''''

    Protected Sub GVResponse_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVResponses.PageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreResultSummaryResponses()
        Dim enquiryDS As EnquiryLogDS = New EnquiryLogDS()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.CustEnquiryIsSearchViewState), Boolean) = True Then
            GetResponseSearchResults()
        Else
            BindResponsesGrid()
        End If

    End Sub

    Protected Sub btnUpdateJournal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim btnJournal As Button = CType(sender, Button)
        Dim strArr() As String = btnJournal.CommandArgument.Split(",")

        Dim objEnqBL As EnquiryManager = New EnquiryManager
        Dim objCustResp As CustomerResponseBO = New CustomerResponseBO

        objCustResp.EnquiryId = CType(strArr(0), Integer)
        objCustResp.JournalId = CType(strArr(1), Integer)
        objCustResp.ItemNatureId = CType(strArr(2), Integer)
        objCustResp.Notes = strArr(3)

        Dim str As String = "CustomerId=" & strArr(5) & "&EnquiryID=" & objCustResp.EnquiryId & "&JournalID=" & objCustResp.JournalId & "&NatureID=" & objCustResp.ItemNatureId
        Dim crmPath = System.Configuration.ConfigurationManager.AppSettings("create_journal_crm_path").ToString()
        objEnqBL.CreateCustomerResponseJornal(objCustResp)
        Dim responseId As Integer = CType(strArr(4), Integer)
        Me.deleteCustomerResponse(responseId)
        Response.Redirect(crmPath & str)
    End Sub

#Region "Handler for delete enquiry response button"
    Protected Sub btnDeleteResponse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'casting sender of this event into button object before getting command arguments from
        'button object
        Dim btn As Button = DirectCast(sender, Button)
        'creating array of command arguments send by delete enquiry button
        Dim strArr As String() = btn.CommandArgument.Split(",")
        
        Dim responseId As Integer = CType(strArr(0), Integer)
        Me.deleteCustomerResponse(responseId)
    End Sub

    Private Sub deleteCustomerResponse(ByVal custResId As Integer)
        'creating CustomerResponseBO object
        Dim cstResponseBO As CustomerResponseBO = New CustomerResponseBO()

        'setting CustomerResponseId property of CustomerResponseBO
        cstResponseBO.CustomerResponseId = custResId
        'creating EnquiryManager object
        Dim enqryManager As EnquiryManager = New EnquiryManager()
        'calling function DeleteCustomerEnquiryResponse function of EnquiryManager
        'that will be used to mark the current record as deleted in db
        enqryManager.DeleteCustomerEnquiryResponse(cstResponseBO)

        ' If delete operation is successful, decrease the grid count by 1 since the grid will be not fetched from
        ' the database again
        GVEnquiryLog.VirtualItemCount = GVEnquiryLog.VirtualItemCount - 1

        'storing the current page 
        Dim tmpPageIndex As Integer = GVEnquiryLog.PageIndex
        GVEnquiryLog.PageIndex = 0
        'after deletion populating the gridview again 
        If DirectCast(ViewState(ApplicationConstants.CustEnquiryIsSearchViewState), Boolean) = True Then
            'function used for populating gridview when record deleted belonged to particular serach record
            GetSearchResults()
        Else
            'function used for populating gridview when record deleted belonged to all serach record
            BindEnquiryLogGrid()
        End If
        'manually restoring the current page again
        Dim ee As GridViewPageEventArgs = New GridViewPageEventArgs(tmpPageIndex)
        GVResponse_PageIndexChanging(Nothing, ee)
    End Sub

#End Region

    Protected Sub GVEnquiryLog_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnShowPopup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnCalander_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub PreviousPage1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousPage1.Click

    End Sub
End Class