Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Web.UI.WebControls
Imports System.ComponentModel

Namespace Fadrian.Web.Control
    Public Class PagingGridView
        Inherits GridView
        Public Sub New()
            MyBase.New()
            Me.AllowPaging = True
            Me.AllowSorting = True
            Me.PagerSettings.Mode = PagerButtons.NumericFirstLast
        End Sub

#Region "Custom properties"
        <Browsable(True), Category("NewDynamic")> _
        <Description("Set the virtual item count for this grid")> _
        Public Property VirtualItemCount() As Integer
            Get
                If ViewState("pgv_vitemcount") Is Nothing Then
                    ViewState("pgv_vitemcount") = -1
                End If
                Return Convert.ToInt32(ViewState("pgv_vitemcount"))
            End Get
            Set(ByVal value As Integer)
                ViewState("pgv_vitemcount") = value
            End Set
        End Property

        <Browsable(True), Category("NewDynamic")> _
         <Description("Get the order by string to use for this grid when sorting event is triggered")> _
         Public Property OrderBy() As String
            Get
                If ViewState("pgv_orderby") Is Nothing Then
                    ViewState("pgv_orderby") = String.Empty
                End If
                Return ViewState("pgv_orderby").ToString()
            End Get
            Protected Set(ByVal value As String)
                ViewState("pgv_orderby") = value
            End Set
        End Property

        Private Property CurrentPageIndex() As Integer
            Get
                If ViewState("pgv_pageindex") Is Nothing Then
                    ViewState("pgv_pageindex") = 0
                End If
                Return Convert.ToInt32(ViewState("pgv_pageindex"))
            End Get
            Set(ByVal value As Integer)
                ViewState("pgv_pageindex") = value
            End Set
        End Property
        Private ReadOnly Property CustomPaging() As Boolean
            Get
                Return (VirtualItemCount <> -1)
            End Get
        End Property
#End Region

#Region "Overriding the parent methods"
        Public Overloads Overrides Property DataSource() As Object
            Get
                Return MyBase.DataSource
            End Get
            Set(ByVal value As Object)
                MyBase.DataSource = value
                ' we store the page index here so we dont lost it in databind
                CurrentPageIndex = PageIndex
            End Set
        End Property

        Protected Overloads Overrides Sub OnSorting(ByVal e As GridViewSortEventArgs)
            ' We store the direction for each field so that we can work out whether next sort should be asc or desc order
            Dim direction As SortDirection = SortDirection.Ascending
            If ViewState(e.SortExpression) IsNot Nothing AndAlso DirectCast(ViewState(e.SortExpression), SortDirection) = SortDirection.Ascending Then
                direction = SortDirection.Descending
            End If
            ViewState(e.SortExpression) = direction
            OrderBy = String.Format("{0} {1}", e.SortExpression, (IIf(direction = SortDirection.Descending, "DESC", "")))
            MyBase.OnSorting(e)
        End Sub

        Protected Overloads Overrides Sub InitializePager(ByVal row As GridViewRow, ByVal columnSpan As Integer, ByVal pagedDataSource As PagedDataSource)
            ' This method is called to initialise the pager on the grid. We intercepted this and override
            ' the values of pagedDataSource to achieve the custom paging using the default pager supplied
            If CustomPaging Then
                pagedDataSource.AllowCustomPaging = True
                pagedDataSource.VirtualCount = VirtualItemCount
                pagedDataSource.CurrentPageIndex = CurrentPageIndex
            End If
            MyBase.InitializePager(row, columnSpan, pagedDataSource)
        End Sub
#End Region
    End Class
End Namespace