<%@ Page Language="vb" AutoEventWireup="false"  MasterPageFile="~/master pages/FaultManagement.Master" CodeBehind="fault_manager.aspx.vb" Inherits="tenantsonline.fault_manager" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">

<script type="text/javascript" language="javascript">
    function ClearCtrl(){
        document.getElementById("<%= ddlLocation.ClientID %>").value = "";
        document.getElementById("<%= ddlArea.ClientID %>").value = "";
        document.getElementById("<%= ddlElement.ClientID %>").value = "";
        document.getElementById("<%= ddlStatus.ClientID %>").value = "-1";
        document.getElementById("<%= ddlPriority.ClientID %>").value = "";
              
    }
    
       function ClearAuditCtrl(){
        document.getElementById("<%= ddlAuditLocation.ClientID %>").value = "";
        document.getElementById("<%= ddlAuditArea.ClientID %>").value = "";
        document.getElementById("<%= ddlAuditElement.ClientID %>").value = "";
        document.getElementById("<%= ddlTeam.ClientID %>").value = "";
        document.getElementById("<%= ddlUser.ClientID %>").value = "";
              
    }
    
     function ClearAddPopupCtrl(){
        document.getElementById("<%= ddlLocationFault.ClientID %>").value = "";
        document.getElementById("<%= ddlAreaFault.ClientID %>").value = "";
        document.getElementById("<%= ddlElementFault.ClientID %>").value = "";
        document.getElementById("<%= ddlPriorityFault.ClientID %>").value = "";
        document.getElementById ("<%=txtDescription.ClientID %>").value="";
        document.getElementById("<%= txtNet.ClientID %>").value = "0.0";
        document.getElementById("<%= txtVat.ClientID %>").value = "0.0";
        document.getElementById("<%= txtGross.ClientID %>").value = "0.0";
        document.getElementById("<%= ddlVat.ClientID %>").value = "";
        document.getElementById("<%= txtDate.ClientID %>").value="";
        document.getElementById ("<%= rdbRechargeYes.ClientID %>").checked="true";
        document.getElementById ("<%= rdbPreYes.ClientID %>").checked="true";

        document.getElementById ("<%= rdbStockYes.ClientID %>").checked="true";
        document.getElementById ("<%= rdbFaultYes.ClientID %>").checked="true";
        
        
              
    }
    
    function isFloatingPointNumber(val){
        var rExp = /^-{0,1}\d*\.{0,1}\d*$/g;
        if (rExp.test(val)){return true;} else {return false;}
        }
    
    function validateInflationRate(){
    var val=document.getElementById("<%= txtPricingControlInflationValue.ClientID %>").value;
    if(isFloatingPointNumber(val)==false){
    document.getElementById("<%=lblPricingError.ClientID %>").value="Please Enter Valid Inflation Rate";
    return false;
    }else{
    document.getElementById("<%=lblPricingError.ClientID %>").value="";
    return true;
    }
    }
    
    function ClearAmendPopupCtrl(){
        
        document.getElementById("<%= ddlPriorityAmendFault.ClientID %>").value = "";
        document.getElementById("<%= txtAmendNet.ClientID %>").value = "0.0";
        document.getElementById("<%= txtAmendVat.ClientID %>").value = "0.0";
        document.getElementById("<%= txtAmendGross.ClientID %>").value = "0.0";
        document.getElementById("<%= ddlAmendVat.ClientID %>").value = "";
        document.getElementById("<%= txtAmendDate.ClientID %>").value="";
        
              
    }
    
     function getGross(){
        
        
        var a=document.getElementById("<%= txtAmendNet.ClientID %>").value;
        var b=document.getElementById("<%= txtAmendVat.ClientID %>").value;
        var c=parseFloat(a)+parseFloat(b);
        document.getElementById("<%= txtAmendGross.ClientID %>").value = c.toString();
        
        
              
    }
    

    
 </script>
        <table style="width: 100%; height: 200px">
            <tr>
                <td style="width: 172px; height: 34px">
                    &nbsp;<asp:Label ID="lblGVHeading" runat="server" BackColor="Black" Font-Bold="True"
                        Font-Names="Arial" ForeColor="White" Text="Fault Management Area" Width="204px"></asp:Label></td>
                <td style="height: 34px; width: 2139px;">
                    <cc2:toolkitscriptmanager id="Registration_ToolkitScriptManager" runat="server"></cc2:toolkitscriptmanager>
                    <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" Width="224px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 172px; height: 1px" valign="top">
                    <table style="border-right: tan thin dotted; border-top: tan thin dotted; border-left: tan thin dotted;
                        width: 5%; border-bottom: tan thin dotted">
                        <tr>
                            <td colspan="2" style="border-bottom: thin dotted">
                                <asp:Label ID="lblFaultType" runat="server" Font-Names="Arial" Font-Size="Small"
                                    Text="Select:" Width="61px"></asp:Label>
                                <asp:DropDownList ID="ddlFaultType" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                    Font-Names="Arial" Font-Size="Small" Width="135px">
                                    <asp:ListItem Value="0">Audit Trail</asp:ListItem>
                                    <asp:ListItem Value="1">Cancelled Faults</asp:ListItem>
                                    <asp:ListItem Value="2" Selected="True">Fault Management</asp:ListItem>
                                    <asp:ListItem Value="3">Priority Settings</asp:ListItem>
                                    <asp:ListItem Value="4">Pricing Control</asp:ListItem>
                                    <asp:ListItem Value="5">Repair Management </asp:ListItem>
                                    <asp:ListItem Value="6">Reported Faults</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 21px">
                                <asp:Label ID="lblSearch" runat="server" Font-Bold="True" Font-Names="Arial" Text="Search:"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:UpdatePanel ID="UpdatePanel_Search" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table style="border-right: tan thin dotted; border-top: tan thin dotted; border-left: tan thin dotted;
                        width: 5%; border-bottom: tan thin dotted">
                                            <tr>
                                                <td style="width: 100px">
                                                    <asp:Label ID="lblLocation1" runat="server" Font-Names="Arial" Font-Size="Small"
                                                        Text="Location:" Width="64px"></asp:Label></td>
                                                <td style="width: 145px">
                                                    <asp:UpdatePanel ID="UpdatePanel_Locationddl" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack ="true"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:UpdateProgress ID="UpdateProgress_Loadddl" runat="server" AssociatedUpdatePanelID="UpdatePanel_Locationddl" DisplayAfter="10">
                                                        <ProgressTemplate>
                                                            <asp:Image ID="imgProgressBar" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px; height: 24px">
                                                    <asp:Label ID="lblArea1" runat="server" Font-Names="Arial" Font-Size="Small" Text="Area:"></asp:Label></td>
                                                <td style="width: 145px; height: 24px">
                                                    <asp:UpdatePanel ID="UpdatePanel_LoadddlArea" runat="server" RenderMode="Inline"
                                    UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlArea" runat="server" AutoPostBack ="true"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged">
                                                                <asp:ListItem Value="">Please select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlLocation" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <asp:UpdateProgress ID="UpdateProgress_LoadddlElement" runat="server" AssociatedUpdatePanelID="UpdatePanel_LoadddlArea" DisplayAfter="10">
                                                        <ProgressTemplate>
                                                            <asp:Image ID="imgProgressBarElement" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px">
                                                    <asp:Label ID="lblElement1" runat="server" Font-Names="Arial" Font-Size="Small" Text="Element:"></asp:Label></td>
                                                <td style="width: 145px" valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel_LoadddlElement" runat="server" RenderMode="Inline"
                                    UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlElement" runat="server" AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px">
                                                                <asp:ListItem Value="">Please select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlArea" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px">
                                                    <asp:Label ID="lblStatus" runat="server" Font-Names="Arial" Font-Size="Small" Text="Status:"></asp:Label></td>
                                                <td style="width: 145px">
                                                    <asp:DropDownList ID="ddlStatus" runat="server" AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px">
                                                        <asp:ListItem Value="-1">Please select</asp:ListItem>
                                                        <asp:ListItem Value="0">Inactive</asp:ListItem>
                                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                            <td style="width: 100px; height: 24px;">
                                <asp:Label ID="lblPriority1" runat="server" Font-Names="Arial" Font-Size="Small" Text="Priority:"></asp:Label></td>
                            <td style="width: 145px; height: 24px;">
                                <asp:DropDownList ID="ddlPriority" runat="server" AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px">
                                </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px">
                                                    &nbsp;</td>
                                                <td style="width: 145px">
                                                    &nbsp;<asp:Button ID="btnSearch"  runat="server" Text="Search" />
                                                    <asp:Button ID="btnClear" runat="server"
                                    Style="position: static" Text="Clear" UseSubmitBehavior="False" OnClientClick="ClearCtrl();return false;" OnClick="btnClear_Click" /></td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel_AuditSearch" runat="server" RenderMode="Inline"
                        UpdateMode="Conditional" Visible="False">
                        <ContentTemplate>
                            <table style="border-right: tan thin dotted; border-top: tan thin dotted; border-left: tan thin dotted;
                        width: 5%; border-bottom: tan thin dotted">
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblAuditLocation" runat="server" Font-Names="Arial" Font-Size="Small"
                                            Text="Location:" Width="64px"></asp:Label></td>
                                    <td style="width: 145px">
                                        <asp:UpdatePanel ID="UpdatePanel_AuditLocationddl" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlAuditLocation" runat="server" AutoPostBack ="true"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlAuditLocation_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdateProgress ID="UpdateProgress_LoadAuditddl" runat="server" AssociatedUpdatePanelID="UpdatePanel_AuditLocationddl" DisplayAfter="10">
                                            <ProgressTemplate>
                                                <asp:Image ID="imgAuditProgressBar" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 24px">
                                        <asp:Label ID="lblAuditArea" runat="server" Font-Names="Arial" Font-Size="Small"
                                            Text="Area:"></asp:Label></td>
                                    <td style="width: 145px; height: 24px">
                                        <asp:UpdatePanel ID="UpdatePanel_LoadAuditddlArea" runat="server" RenderMode="Inline"
                                    UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlAuditArea" runat="server" AutoPostBack ="true"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlAuditArea_SelectedIndexChanged">
                                                    <asp:ListItem>Please select</asp:ListItem>
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlAuditLocation" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdateProgress ID="UpdateProgress_AuditLoadddlElement" runat="server" AssociatedUpdatePanelID="UpdatePanel_LoadAuditddlArea" DisplayAfter="10">
                                            <ProgressTemplate>
                                                <asp:Image ID="imgProgressBarAuditElement" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblAuditElement" runat="server" Font-Names="Arial" Font-Size="Small"
                                            Text="Element:"></asp:Label></td>
                                    <td style="width: 145px" valign="top">
                                        <asp:UpdatePanel ID="UpdatePanel_AuditLoadddlElement" runat="server" RenderMode="Inline"
                                    UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlAuditElement" runat="server" AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px">
                                                    <asp:ListItem>Please select</asp:ListItem>
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlAuditArea" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        <asp:Label ID="lblTeam" runat="server" Font-Names="Arial" Font-Size="Small" Text="Team:"></asp:Label></td>
                                    <td style="width: 145px">
                                        <asp:UpdatePanel ID="UpdatePanel_AuditTeam" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                            <ContentTemplate>
                                        <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" AutoPostBack="True" OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged">
                                            <asp:ListItem Value="-1">Please select</asp:ListItem>
                                        </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <asp:UpdateProgress ID="UpdateProgress_LoadUserddl" runat="server" AssociatedUpdatePanelID="UpdatePanel_AuditTeam" DisplayAfter="10">
                                            <ProgressTemplate>
                                                <asp:Image ID="imgProgressBarAuditUser" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 24px;">
                                    <asp:Label ID="lblUser" runat="server" Font-Names="Arial" Font-Size="Small" Text="User:"></asp:Label></td>
                                    </td>
                                    <td style="width: 145px; height: 24px;">
                                        <asp:UpdatePanel ID="UpdatePanel_LoadddlUser" runat="server" RenderMode="Inline"
                                            UpdateMode="Conditional">
                                            <ContentTemplate>
                                        <asp:DropDownList ID="ddlUser" runat="server" AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px">
                                            <asp:ListItem Value="-1">Please select</asp:ListItem>
                                        </asp:DropDownList>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlTeam" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px">
                                        &nbsp;</td>
                                    <td style="width: 145px">
                                        &nbsp;<asp:Button ID="btnAuditSearch"  runat="server" Text="Search" OnClick="btnAuditSearch_Click" />
                                        <asp:Button ID="btnAuditClear" runat="server"
                                    Style="position: static" Text="Clear" UseSubmitBehavior="False" OnClientClick="ClearAuditCtrl();return false;" /></td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="height: 1px; width: 2139px;" valign="top">
                    <asp:UpdatePanel ID="UpdatePanel_LoadGrid" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                            <cc1:paginggridview id="GVFault" runat="server" allowpaging="True" allowsorting="True"
                                autogeneratecolumns="False" backcolor="LightGoldenrodYellow" bordercolor="Tan"
                                borderstyle="Dotted" borderwidth="1px" cellpadding="2" emptydatatext="No record exists"
                                font-names="Arial" font-size="Small" forecolor="Black" gridlines="None" orderby=""
                                virtualitemcount="-1" width="100%" OnPageIndexChanging="GVFault_PageIndexChanging" OnPageIndexChanged="GVFault_PageIndexChanged" OnSelectedIndexChanged="GVFault_SelectedIndexChanged" OnSorting="GVFault_Sorting">

<FooterStyle BackColor="#C00000" ForeColor="White" Font-Bold="True"></FooterStyle>
<Columns>
<asp:TemplateField SortExpression="FL_Element.ElementName" HeaderText="Location/Area/Element" InsertVisible="False"><ItemTemplate>
<asp:Label id="GVLblLocation" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatLocation(Eval("LocationName").ToString(),Eval("AreaName").ToString(),Eval("ElementName").ToString()) %>' __designer:wfdid="w4"></asp:Label> 
</ItemTemplate>
    <itemstyle horizontalalign="Left" />
</asp:TemplateField>
<asp:TemplateField SortExpression="FL_Fault.Description" HeaderText="Description" InsertVisible="False">
    <ItemTemplate>
<asp:Label id="GVLblDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>' __designer:wfdid="w168"></asp:Label> 
</ItemTemplate>
    <headerstyle horizontalalign="Left" ></headerstyle>
</asp:TemplateField>
<asp:TemplateField SortExpression="FL_Fault_Priority.PriorityName" HeaderText="Priority" InsertVisible="False"><ItemTemplate>
<asp:Label id="GVLblPriority" runat="server" CssClass="cellData" Text='<%# Bind("Priority") %>' __designer:wfdid="w37"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField SortExpression="FL_Fault_Priority.ResponseTime" HeaderText="Response" InsertVisible="False"><ItemTemplate>
<asp:Label id="GVLblPriorityDays" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatPriorityReponse(Eval("ResponseTime").toString(),Eval("Type").toString()) %>' __designer:wfdid="w38"></asp:Label> 
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Net (&#163;):" InsertVisible="False" SortExpression="FL_Fault.NetCost">
<ItemTemplate>
<asp:Label id="GVLblNet" runat="server" CssClass="cellData" Text='<%# System.Math.Round(Convert.ToDouble(Eval("NetCost")),2).ToString() %>' __designer:wfdid="w40"></asp:Label> 
</ItemTemplate>
<HeaderStyle Font-Underline="True" horizontalalign="Left"></HeaderStyle>
</asp:TemplateField>
<asp:TemplateField SortExpression="FL_Fault.Vat" HeaderText="Vat"><ItemTemplate>
<asp:Label id="GVLblVat" runat="server" CssClass="cellData" Text='<%# System.Math.Round(Convert.ToDouble(Eval("Vat")),2).ToString() %>' __designer:wfdid="w42"></asp:Label>
</ItemTemplate>
</asp:TemplateField>
    <asp:TemplateField HeaderText="Gross  (&#163;):" SortExpression="FL_Fault.Gross">
    <ItemTemplate>
<asp:Label id="GVLblGross" runat="server" CssClass="cellData" Text='<%# System.Math.Round(Convert.ToDouble(Eval("Gross")),2).ToString() %>'></asp:Label> 
</ItemTemplate>
<HeaderStyle Font-Underline="True" horizontalalign="Left"></HeaderStyle>
</asp:TemplateField>
    <asp:TemplateField HeaderText="Status" SortExpression="FL_Fault.FaultActive">
        <ItemTemplate>
<asp:Label id="GVLblStatus" runat="server" CssClass="cellData" Text='<%# Bind("FaultActive") %>' __designer:wfdid="w640"></asp:Label> 
</ItemTemplate>
<HeaderStyle Font-Underline="True" horizontalalign="Left"></HeaderStyle>
    </asp:TemplateField>
    <asp:TemplateField>
        <itemtemplate>
<asp:Button id="btnAmend" onclick="btnAmend_Click" runat="server" Text="Amend" CommandArgument='<%# Bind("FaultID") %>' __designer:wfdid="w1"></asp:Button> 
</itemtemplate>
    </asp:TemplateField>
</Columns>

<RowStyle BackColor="#EFF3FB"></RowStyle>

<EditRowStyle BackColor="#2461BF"></EditRowStyle>

<SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" ></PagerStyle>
        <pagersettings           
          pagebuttoncount="5" FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"  />
          
<HeaderStyle BackColor="#C00000" ForeColor="White" Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>

<AlternatingRowStyle BackColor="White" Wrap="True" Font-Size="Small" Font-Names="Arial"></AlternatingRowStyle>


</cc1:paginggridview>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="height: 26px">
                    <asp:Button ID="btnAddFault" runat="server" Text="Add New Fault" /></td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    &nbsp;
                    <asp:UpdatePanel ID="UpdatePanel_PriceControl" RenderMode="Inline" runat="server" UpdateMode="Conditional" Visible="false">
                        <ContentTemplate >
                            <table border="1" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="2" cellspacing="3" width="90%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblPricingControlHeading" runat="server" Font-Bold="True" Font-Names="Arial"
                                                                Text="Pricing Control"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px">
                                                            <asp:Label ID="lblPricingControlLocation" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                Text="Location:" Width="64px"></asp:Label></td>
                                                        <td style="width: 145px">
                                                            <asp:UpdatePanel ID="UpdatePanel_LoadPricingControlLocation" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlPricingControlLocation" runat="server" AppendDataBoundItems="True"
                                                                        AutoPostBack="true" Font-Names="Arial" Font-Size="Small" OnSelectedIndexChanged="ddlPricingControlLocation_SelectedIndexChanged"
                                                                        Width="152px">
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            <asp:UpdateProgress ID="UpdateProgress_PricingControlLocation" runat="server" AssociatedUpdatePanelID="UpdatePanel_LoadPricingControlLocation"
                                                                DisplayAfter="10">
                                                                <ProgressTemplate>
                                                                    <asp:Image ID="imgProgressBarPricingControl" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px; height: 24px">
                                                            <asp:Label ID="lblPricingControlArea" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                Text="Area:"></asp:Label></td>
                                                        <td style="width: 145px; height: 24px">
                                                            <asp:UpdatePanel ID="UpdatePanel_LoadPricingControlArea" runat="server" RenderMode="Inline"
                                                                UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlPricingControlArea" runat="server" AppendDataBoundItems="True"
                                                                        AutoPostBack="true" Font-Names="Arial" Font-Size="Small" OnSelectedIndexChanged="ddlPricingControlArea_SelectedIndexChanged"
                                                                        Width="152px">
                                                                        <asp:ListItem>Select All</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlPricingControlLocation" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <asp:UpdateProgress ID="UpdateProgressPricingControlArea" runat="server" AssociatedUpdatePanelID="UpdatePanel_LoadPricingControlArea"
                                                                DisplayAfter="10">
                                                                <ProgressTemplate>
                                                                    <asp:Image ID="imgProgressBarPricingControlElement" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px">
                                                            <asp:Label ID="lblPricingControlElement" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                Text="Element:"></asp:Label></td>
                                                        <td style="width: 145px" valign="top">
                                                            <asp:UpdatePanel ID="UpdatePanel_LoadPricingControlElement" runat="server" RenderMode="Inline"
                                                                UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlPricingControlElement" runat="server" AppendDataBoundItems="True"
                                                                        AutoPostBack="True" Font-Names="Arial" Font-Size="Small" OnSelectedIndexChanged="ddlPricingControlElement_SelectedIndexChanged"
                                                                        Width="152px">
                                                                        <asp:ListItem>Select All</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlPricingControlArea" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <asp:UpdateProgress ID="UpdateProgressPricingControlElement" runat="server" AssociatedUpdatePanelID="UpdatePanel_LoadPricingControlElement"
                                                                DisplayAfter="10">
                                                                <ProgressTemplate>
                                                                    <asp:Image ID="imgProgressBarPricingControlDescription" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px">
                                                            <asp:Label ID="lblPricingControlDescription" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                Text="Description:"></asp:Label></td>
                                                        <td style="width: 145px" valign="top">
                                                            <asp:UpdatePanel ID="UpdatePanel_PricingControlDescription" runat="server" RenderMode="Inline"
                                                                UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlPricingControlDescription" runat="server" AppendDataBoundItems="True"
                                                                        AutoPostBack="True" Font-Names="Arial" Font-Size="Small" OnSelectedIndexChanged="ddlPricingControlDescription_SelectedIndexChanged"
                                                                        Width="152px">
                                                                        <asp:ListItem>Select All</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlPricingControlElement" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px">
                                                            <asp:Label ID="lblPricingControlNet" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                Text="Net: �" Width="68px"></asp:Label></td>
                                                        <td style="width: 145px">
                                                            <asp:TextBox ID="txtPricingControlNet" runat="server" Enabled="False" Width="148px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px">
                                                            <asp:Label ID="lblPricingControlVatRate" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                Text="VAT rate"></asp:Label></td>
                                                        <td style="width: 145px" valign="top">
                                                            <asp:TextBox ID="txtPricingControlVatRate" runat="server" Enabled="False" Width="148px"></asp:TextBox>
                                                            <asp:TextBox ID="txtvatRateHidden" runat="server" Visible="False" Width="20px"></asp:TextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px; height: 28px">
                                                            <asp:Label ID="lblPricingControlVat" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                Text="VAT: �" Width="68px"></asp:Label></td>
                                                        <td style="width: 145px; height: 28px">
                                                            <asp:TextBox ID="txtPricingControlVat" runat="server" Enabled="False" Width="148px"></asp:TextBox>
                                                            <asp:TextBox ID="txtVatHidden" runat="server" Visible="False" Width="21px"></asp:TextBox></td>
                                                        <td style="width: 115px; height: 28px">
                                                            <asp:Label ID="lblPricingControlInflationValue" runat="server" Font-Names="Arial"
                                                                Font-Size="Small" Text="Inflation Value: (%)" Width="115px"></asp:Label></td>
                                                        <td style="width: 185px; height: 28px">
                                                            <asp:TextBox ID="txtPricingControlInflationValue" runat="server" OnTextChanged="txtPricingControlInflationValue_TextChanged"
                                                                Width="148px"></asp:TextBox></td>
                                                        <td style="width: 185px; height: 28px">
                                                            &nbsp;<asp:Label ID="lblPricingError" runat="server" CssClass="caption" ForeColor="Red"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px">
                                                            <asp:Label ID="lblPricingControlGross" runat="server" Font-Names="Arial" Font-Size="Small"
                                                                Text="Gross: �" Width="68px"></asp:Label></td>
                                                        <td style="width: 145px">
                                                            <asp:TextBox ID="txtPricingControlGross" runat="server" Enabled="False" Width="148px"></asp:TextBox></td>
                                                        <td style="width: 115px">
                                                            <asp:Label ID="lblPricingControlEffectiveDate" runat="server" Font-Names="Arial"
                                                                Font-Size="Small" Text="Effective from Date:" Width="115px"></asp:Label></td>
                                                        <td style="width: 185px">
                                                            <asp:TextBox ID="txtPricingControlEffectiveDate" runat="server" Enabled="False" Width="128px"></asp:TextBox>
                                                            <asp:ImageButton ID="ibtnPricingControlEffectiveDate" runat="server" CausesValidation="False"
                                                                ImageUrl="~/images/buttons/Calendar_button.png" />
                                                        </td>
                                                        <td style="width: 24px">
                                                            <cc2:CalendarExtender ID="CalendarExtender_PricingControlDate" runat="server" Format="dd/MM/yyyy"
                                                                PopupButtonID="ibtnPricingControlEffectiveDate" PopupPosition="BottomRight" TargetControlID="txtPricingControlEffectiveDate">
                                                            </cc2:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>                                  
                                                    </tr>
                                                    <tr>
                                                        <td align="right" colspan="5">
                                                            <asp:Button ID="btnPricingControlReset" runat="server" OnClick="btnPricingControlReset_Click"
                                                                Text="Reset" /><asp:Button ID="btnPricingControlSave" runat="server"  OnClick="btnPricingControlSave_Click"  
                                                                 Style="position: static" Text="Save" UseSubmitBehavior="False" /></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel_AuditTrail" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <cc1:paginggridview id="GVAuditTrail" runat="server" allowpaging="True" allowsorting="True"
                                autogeneratecolumns="False" backcolor="LightGoldenrodYellow" bordercolor="Tan"
                                borderstyle="Dotted" borderwidth="1px" cellpadding="2" emptydatatext="No record exists"
                                font-names="Arial" font-size="Small" forecolor="Black" gridlines="None" orderby=""
                                virtualitemcount="-1" width="100%" OnPageIndexChanging="GVAuditTrail_PageIndexChanging" OnSorting="GVAuditTrail_Sorting">
                                        <pagersettings           
          pagebuttoncount="5" FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"  />
                                        <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                        <RowStyle BackColor="#EFF3FB" />
                                        <Columns>
                                            <asp:TemplateField SortExpression="TRANDATE" HeaderText="Date:" InsertVisible="False">
                                                <itemtemplate>
<asp:Label id="GVLblAuditLocation" runat="server" CssClass="cellData" Text='<%# Bind("TRANDATE") %>' __designer:wfdid="w5"></asp:Label> 
</itemtemplate>
                                                <itemstyle horizontalalign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField SortExpression="E__EMPLOYEE.ENAME" HeaderText="User:" InsertVisible="False">
                                                <itemtemplate>
<asp:Label id="GVLblAuditDescription" runat="server" CssClass="cellData" Text='<%# Bind("ENAME") %>' __designer:wfdid="w168"></asp:Label> 
</itemtemplate>
                                                <headerstyle horizontalalign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField SortExpression="E.FAULTACTION" HeaderText="Action:" InsertVisible="False">
                                                <itemtemplate>
<asp:Label id="GVLblAuditPriority" runat="server" CssClass="cellData" Text='<%# Bind("FAULTACTIONTYPE") %>' __designer:wfdid="w37"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField SortExpression="FL_ELEMENT.ElementName" HeaderText="Location/Area/Element:" InsertVisible="False">
                                                <itemtemplate>
<asp:Label id="GVLblAuditPriorityDays" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatLocation(Eval("LocationName").ToString(),Eval("AreaName").ToString(),Eval("ElementName").ToString()) %>' __designer:wfdid="w38"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description:" InsertVisible="False" SortExpression="FL_FAULT.Description">
                                                <itemtemplate>
<asp:Label id="GVLblAuditNet" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>' __designer:wfdid="w40"></asp:Label> 
</itemtemplate>
                                                <headerstyle font-underline="True" horizontalalign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <AlternatingRowStyle BackColor="White" Font-Names="Arial" Font-Size="Small" Wrap="True" />
                                    </cc1:PagingGridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel_PrioritySettings" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table style="width: 100%; height: 84px" cellpadding="2">
                                <tr>
                                    <td rowspan="3" style="height: 80px; width: 100%;" valign="top" align="left">
                                        &nbsp;<asp:Panel ID="panel_PriorityGrid" runat="server" Height="340px"
                                            Width="90%">
                                            <asp:UpdatePanel ID="UpdatePanel_PriorityGrid" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <cc1:paginggridview id="GVProiritySetting" runat="server" allowpaging="True" allowsorting="True"
                                autogeneratecolumns="False" backcolor="LightGoldenrodYellow" bordercolor="Tan"
                                borderstyle="Dotted" borderwidth="1px" cellpadding="2" emptydatatext="No record exists"
                                font-names="Arial" font-size="Small" forecolor="Black" gridlines="None" orderby=""
                                virtualitemcount="-1" width="100%" OnPageIndexChanging="GVProiritySetting_PageIndexChanging" OnSorting="GVProiritySetting_Sorting" OnSelectedIndexChanging="GVProiritySetting_SelectedIndexChanging">
                                                <pagersettings           
          pagebuttoncount="5" FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"  />
                                                <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                                <RowStyle BackColor="#EFF3FB" />
                                                <Columns>
                                                    <asp:TemplateField SortExpression="PRIORITYNAME" HeaderText="Priority:" InsertVisible="False">
                                                        <itemstyle horizontalalign="Left" />
                                                        <itemtemplate>
<asp:Label id="GVLblPriorityName" runat="server" CssClass="cellData" Text='<%# Bind("PRIORITYNAME") %>' __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="RESPONSETIME" HeaderText="Target Time:" InsertVisible="False">
                                                        <headerstyle horizontalalign="Left" />
                                                        <itemtemplate>
<asp:Label id="GVLblTargetTime" runat="server" CssClass="cellData" Text='<%#UtilityFunctions.FormatPriorityReponse(Eval("ResponseTime").toString(),Eval("Type").toString()) %>' __designer:wfdid="w168"></asp:Label> 
</itemtemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField SortExpression="STATUS" HeaderText="Status:" InsertVisible="False">
                                                        <itemtemplate>
<asp:Label id="GVLblPriorityStatus" runat="server" CssClass="cellData" Text='<%# Bind("STATUS") %>' __designer:wfdid="w37"></asp:Label> 
</itemtemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField InsertVisible="False">
                                                        <itemtemplate>
<asp:Button id="btnAmendPriority" onclick="btnAmendPriority_Click" runat="server" Text="Amend" __designer:wfdid="w2" CommandArgument='<%# Bind("PRIORITYID") %>'></asp:Button> 
</itemtemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" />
                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                                                <EditRowStyle BackColor="#2461BF" />
                                                <AlternatingRowStyle BackColor="White" Font-Names="Arial" Font-Size="Small" Wrap="True" />
                                            </cc1:PagingGridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                        </asp:Panel>
                                    </td>
                                    <td rowspan="3" style="width: 100%; height: 80px" valign="bottom" align="right">
                                        &nbsp;<asp:UpdatePanel ID="UpdatePanel_AddAmendPriority" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                            <ContentTemplate>
                                            
                                            <table border ="2"><tr><td>
                                                <table style="width: 139px">
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:Label ID="lblAddAmend" runat="server" Font-Bold="True" Font-Names="Arial" Text="Add/Amend"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <asp:UpdatePanel ID="updatePanel_lblPriorityErroMessage" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                            <asp:Label ID="lblPriorityErrorMessage" runat="server" CssClass="info_message_label"
                                                                Font-Size="Smaller"></asp:Label>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 65px; height: 26px">
                                                            <asp:Label ID="lblPrioritySettings" runat="server" CssClass="caption" Text="Priority:"></asp:Label></td>
                                                        <td style="width: 306px; height: 26px">
                                                            <asp:TextBox ID="txtPriorityDesc" runat="server" Width="175px"></asp:TextBox></td>
                                                        <td style="width: 29px; height: 26px">
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 65px">
                                                            <asp:Label ID="lblTargetTime" runat="server" CssClass="caption" Text="Target Time:" Width="79px"></asp:Label></td>
                                                        <td style="width: 306px">
                                                            <asp:TextBox ID="txtTargetTime" runat="server" Width="86px"></asp:TextBox>
                                                            <asp:DropDownList ID="ddlTargetTime" runat="server" Width="84px">
                                                                <asp:ListItem Value="1">Days</asp:ListItem>
                                                                <asp:ListItem Value="0">Hours</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                        <td style="width: 29px">
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 21px; width: 65px;">
                                                            <asp:Label ID="lblProrityStatus" runat="server" CssClass="caption" Text="Status:"></asp:Label></td>
                                                        <td style="height: 21px; width: 306px;">
                                                            <asp:RadioButton ID="rdbActive" runat="server" GroupName="grpPriority" Text="Active" Checked="True" CssClass="caption" />
                                                            &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                                            <asp:RadioButton ID="rdbInactive" runat="server" GroupName="grpPriority" Text="Inactive" CssClass="caption" /></td>
                                                        <td style="height: 21px; width: 29px;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 65px; height: 21px">
                                                            <asp:Label ID="lblPriorityID" runat="server" CssClass="caption" Visible="False"></asp:Label></td>
                                                        <td align="right" style="width: 306px; height: 21px">
                                                            <asp:Button ID="btnPriorityReset" runat="server" Text="Reset" OnClick="btnPriorityReset_Click" />
                                                            <asp:Button ID="btnPrioritySave" runat="server" Text="Save" OnClick="btnPrioritySave_Click" /></td>
                                                        <td style="width: 29px; height: 21px">
                                                        </td>
                                                    </tr>
                                                </table>
                                                </td></tr></table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                        <asp:UpdatePanel ID="updPnlAddFault" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
                        <ContentTemplate>
                        <table border ="1" style="background-color: #ffffff" ><tr><td>
                            <table style="width: 500px" border="0">
                                <tbody>
                                    <tr>
                                        <td align="left" colspan="5" style="background-color: #c00000">
                                            <asp:Label ID="lblViewEnquiry" runat="server" BackColor="#C00000" Font-Bold="True"
                                                Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Add a New Fault"
                                                Width="35%"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblLocation" runat="server" CssClass="caption" Text="Location:"></asp:Label></td>
                                        <td align="left" style="width: 444px">
                                            <asp:UpdatePanel ID="UpdatePanel_ddlLocationFault" runat="server">
                                                <ContentTemplate>
                                            <asp:DropDownList ID="ddlLocationFault" runat="server" AutoPostBack ="True"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlLocationFault_SelectedIndexChanged">
                                            </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="UpdateProgress_LoadddlAreaFault" runat="server" AssociatedUpdatePanelID="UpdatePanel_ddlLocationFault"
                                                DisplayAfter="10">
                                                <ProgressTemplate>
                                                    <asp:Image ID="imgProgressBarAreaFault" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td align="right" style="width: 225px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px; height: 21px">
                                            <asp:Label ID="lblArea" runat="server" CssClass="caption" Text="Area:"></asp:Label></td>
                                        <td align="left" style="width: 444px; height: 21px">
                                            <asp:UpdatePanel ID="UpdatePanel_LoadddlAreaFault" runat="server" RenderMode="Inline"
                                                UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:DropDownList ID="ddlAreaFault" runat="server" AutoPostBack ="true"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlAreaFault_SelectedIndexChanged">
                                            </asp:DropDownList>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlLocationFault" EventName="SelectedIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="UpdateProgress_LoadddlEleFault" runat="server" AssociatedUpdatePanelID="UpdatePanel_LoadddlAreaFault"
                                                DisplayAfter="10">
                                                <ProgressTemplate>
                                                    <asp:Image ID="imgProgressBarEleFault" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                        <td style="width: 100px; height: 21px">
                                        </td>
                                        <td align="right" style="width: 225px; height: 21px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px; height: 21px">
                                            <asp:Label ID="lblElement" runat="server" CssClass="caption" Text="Element:"></asp:Label></td>
                                        <td style="width: 444px; height: 21px">
                                            <asp:UpdatePanel ID="UpdatePanel_LoadddlElemetFault" runat="server" RenderMode="Inline"
                                                UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:DropDownList ID="ddlElementFault" runat="server" AutoPostBack="true"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlElementFault_SelectedIndexChanged">
                                            </asp:DropDownList><asp:Button ID="btnEdit" runat="server" Text="Edit Element Name" OnClick="btnEdit_Click" Width="141px" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlAreaFault" EventName="SelectedIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            &nbsp;
                                            </td>
                                        <td style="width: 100px; height: 21px">
                                        </td>
                                        <td align="right" style="width: 225px; height: 21px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblDescription" runat="server" CssClass="caption" Text="Description:"></asp:Label></td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtDescription" runat="server" Width="305px"></asp:TextBox><asp:RequiredFieldValidator ID="reqValidatorDesc" runat="server" ControlToValidate="txtDescription"
                                                ErrorMessage="Please Enter Description" ValidationGroup="grpValidation">*</asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px; height: 21px">
                                            <asp:Label ID="lblPriority" runat="server" CssClass="caption" Text="Priority:"></asp:Label></td>
                                        <td style="width: 444px; height: 21px">
                                            <asp:UpdatePanel ID="UpdatePanel_ddlPriority" runat="server">
                                                <ContentTemplate>
                                            <asp:DropDownList ID="ddlPriorityFault" runat="server" AutoPostBack =true  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlPriorityFault_SelectedIndexChanged">
                                            </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 100px; height: 21px">
                                        </td>
                                        <td align="right" style="width: 225px; height: 21px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblResponse" runat="server" CssClass="caption" Text="Response Time:" Width="110px"></asp:Label></td>
                                        <td style="width: 444px">
                                            <asp:UpdatePanel ID="UpdatePanel_lblResponse" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    &nbsp;
                                            <asp:Label ID="lblResponseTime" runat="server" CssClass="caption" Width="140px"></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlPriorityFault" EventName="SelectedIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td align="right" style="width: 225px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 2px; background-color: #c00000;" colspan="4" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblCost" runat="server" CssClass="caption" Font-Bold="True"
                                                Font-Italic="False" Text="Cost:" Width="82px"></asp:Label></td>
                                        <td style="width: 444px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td align="right" style="width: 225px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblNet" runat="server" CssClass="caption" Height="21px" Text="Net             � :"
                                                Width="96px"></asp:Label></td>
                                        <td style="width: 444px">
                                            <asp:UpdatePanel ID="updatePanel_NetCost" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:TextBox ID="txtNet" runat="server" OnTextChanged ="txtNet_TextChanged" AutoPostBack="True" CausesValidation="True"  ></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td align="right" style="width: 225px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblVateRate" runat="server" CssClass="caption" Text="Vat Rate:"></asp:Label></td>
                                        <td style="width: 444px">
                                            <asp:UpdatePanel ID="updatePanel_VatRate" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:DropDownList ID="ddlVat" runat="server" AutoPostBack ="True"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlVat_SelectedIndexChanged">
                                     <asp:ListItem Value="0">Please select</asp:ListItem>
                                     <asp:ListItem Value="1">20</asp:ListItem>
                                        </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="updateProgress_VatRate" runat="server" AssociatedUpdatePanelID="updatePanel_VatRate"
                                                DisplayAfter="10">
                                                <ProgressTemplate>
                                                    <asp:Image ID="imgProgressBarVatRate" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 225px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblVat" runat="server" CssClass="caption" Text="Vat          � :"></asp:Label></td>
                                        <td style="width: 444px">
                                            <asp:UpdatePanel ID="updatePanel_Vat" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:TextBox ID="txtVat" runat="server" MaxLength="8" AutoPostBack="True" OnTextChanged="txtVat_TextChanged" ReadOnly="True" >0.0</asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 225px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblGross" runat="server" CssClass="caption" Text="Gross      �  :"></asp:Label></td>
                                        <td style="width: 444px">
                                            <asp:UpdatePanel ID="updatePanel_Gross" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:TextBox ID="txtGross" runat="server" ReadOnly="True">0.0</asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 225px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblFrom" runat="server" CssClass="caption" Text="Efective from :"></asp:Label></td>
                                        <td style="width: 444px">
                                            <asp:TextBox ID="txtDate" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="10"
                                                Width="148px" Enabled="False"></asp:TextBox><asp:ImageButton ID="btnCalander" runat="server" CausesValidation="False"
                                                    ImageUrl="~/images/buttons/Calendar_button.png" /></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 225px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="5" style="height: 2px; background-color: #c00000" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblRecharge" runat="server" CssClass="caption" Text="Recharge :"></asp:Label></td>
                                        <td style="width: 444px">
                                            &nbsp;<asp:RadioButton ID="rdbRechargeYes" runat="server" Text="Yes" GroupName="grpRecharge" OnCheckedChanged="rdbRechargeYes_CheckedChanged2" Checked="True" CssClass="caption" />
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            <asp:RadioButton ID="rdbRechargeNo" runat="server" Text="No" GroupName="grpRecharge" OnCheckedChanged="rdbRechargeNo_CheckedChanged1" CssClass="caption" /></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 225px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px; height: 22px;">
                                            <asp:Label ID="lblPreInspection" runat="server" CssClass="caption" Text="Pre Inspection:"></asp:Label></td>
                                        <td style="width: 444px; height: 22px;">
                                            &nbsp;<asp:RadioButton ID="rdbPreYes" runat="server" Text="Yes" GroupName="grpPreInspection" Checked="True" CssClass="caption" />
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            <asp:RadioButton ID="rdnPreNo" runat="server" Text="No" GroupName="grpPreInspection" CssClass="caption" /></td>
                                        <td style="width: 100px; height: 22px;">
                                        </td>
                                        <td style="width: 225px; height: 22px;">
                                        </td>
                                        <td style="width: 100px; height: 22px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblStockCondition" runat="server" CssClass="caption" Text="Stock Condition Item:"
                                                Width="150px"></asp:Label></td>
                                        <td style="width: 444px">
                                            &nbsp;<asp:RadioButton ID="rdbStockYes" runat="server" Text="Yes" GroupName="grpStock" Checked="True" CssClass="caption" />
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            <asp:RadioButton ID="rdbStockNo" runat="server" Text="No" GroupName="grpStock" CssClass="caption" /></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 225px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px">
                                            <asp:Label ID="lblFaultActive" runat="server" CssClass="caption" Text="Fault Active:"></asp:Label></td>
                                        <td style="width: 444px">
                                            &nbsp;<asp:RadioButton ID="rdbFaultYes" runat="server" Text="Yes" GroupName="grpFaultActive" Checked="True" CssClass="caption" />
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            <asp:RadioButton ID="rdbFaultNo" runat="server" Text="No" GroupName="grpFaultActive" CssClass="caption" /></td>
                                        <td style="width: 100px">
                                        </td>
                                        <td style="width: 225px">
                                        </td>
                                        <td style="width: 100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="5">
                                            <cc2:ModalPopupExtender
                                                ID="MdlPopup_AddFault" runat="server" BackgroundCssClass="modalBackground" CancelControlID="btnClose"
                                                Drag="True" PopupControlID="updPnlAddFault" PopupDragHandleControlID="lblLoction"
                                                TargetControlID="lblArea">
                                            </cc2:ModalPopupExtender>
                                            <cc2:CalendarExtender ID="ceEfectiveFrom" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalander"
                                                PopupPosition="BottomRight" TargetControlID="txtDate">
                                            </cc2:CalendarExtender>
                                            <asp:UpdatePanel ID="UpdatePanel_errorMesg" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:Label ID="lblAddErrorMesg" runat="server" ForeColor="Red" CssClass="error_message_label"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="5" style="height: 2px; background-color: #c00000">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="5">
                                            <asp:Button ID="btnClose" runat="server" UseSubmitBehavior ="true" OnClick="btnClose_Click" Text="Close" Width="56px" />
                                            <asp:Button
                                                ID="btnReset" runat="server" OnClick="btnReset_Click" OnClientClick ="ClearAddPopupCtrl(); return false;" Text="Reset"  UseSubmitBehavior="False" />
                                            <asp:UpdatePanel ID="UpdatePanel_SaveButton" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            &nbsp; &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        </tr>
                        </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="updPnlAmendFault" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <table border ="1" style="background-color: #ffffff" >
                                <tr>
                                    <td>
                                        <table style="width: 500px" border="0">
                                            <tbody>
                                                <tr>
                                                    <td align="left" colspan="5" style="background-color: #c00000">
                                                        <asp:Label ID="lblAmendFault" runat="server" BackColor="#C00000" Font-Bold="True"
                                                            Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Amend a Fault"
                                                            Width="35%"></asp:Label>
                                                        <asp:Label ID="lblFaultID" runat="server" Visible="False"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendLocation" runat="server" CssClass="caption" Text="Location:"></asp:Label></td>
                                                    <td align="left" style="width: 444px">
                                                        <asp:Label ID="lblAmendFaultLocation" runat="server" CssClass="caption" Width="168px"></asp:Label></td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td align="right" style="width: 225px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px; height: 21px">
                                                        <asp:Label ID="lblAmendArea" runat="server" CssClass="caption" Text="Area:"></asp:Label></td>
                                                    <td align="left" style="width: 444px; height: 21px">
                                                        <asp:Label ID="lblAmendFaultArea" runat="server" CssClass="caption" Width="168px"></asp:Label></td>
                                                    <td style="width: 100px; height: 21px">
                                                    </td>
                                                    <td align="right" style="width: 225px; height: 21px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px; height: 21px">
                                                        <asp:Label ID="lblAmendElement" runat="server" CssClass="caption" Text="Element:"></asp:Label></td>
                                                    <td style="width: 444px; height: 21px">
                                                        <asp:Label ID="lblAmendFaultElement" runat="server" CssClass="caption" Width="168px"></asp:Label></td>
                                                    <td style="width: 100px; height: 21px">
                                                    </td>
                                                    <td align="right" style="width: 225px; height: 21px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendDescription" runat="server" CssClass="caption" Text="Description:"></asp:Label></td>
                                                    <td align="left" colspan="3">
                                                       <asp:TextBox ID="txtAmendFaultDesc" runat="server" CssClass="caption" Width="168px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px; height: 21px">
                                                        <asp:Label ID="lblAmendPriority" runat="server" CssClass="caption" Text="Priority:"></asp:Label></td>
                                                    <td style="width: 444px; height: 21px">
                                                        <asp:UpdatePanel ID="UpdatePanel_ddlAmPriority" runat="server">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlPriorityAmendFault" runat="server" AutoPostBack ="true"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlAmendPriorityFault_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td style="width: 100px; height: 21px">
                                                    </td>
                                                    <td align="right" style="width: 225px; height: 21px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendResponse" runat="server" CssClass="caption" Text="Response Time:"
                                                            Width="110px"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:UpdatePanel ID="UpdatePanel_lblAmResponse" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                               <asp:Label ID="lblAmendResponseTime" runat="server" CssClass="caption" Width="140px"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ddlPriorityAmendFault" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td align="right" style="width: 225px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="height: 2px; background-color: #c00000;" colspan="4" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendCost" runat="server" CssClass="caption" Font-Bold="True"
                                                            Font-Italic="False" Text="Cost:" Width="82px"></asp:Label></td>
                                                    <td style="width: 444px">
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td align="right" style="width: 225px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px; height: 26px;">
                                                        <asp:Label ID="lblAmendNet" runat="server" CssClass="caption" Height="21px" Text="Net             � :"
                                                            Width="96px"></asp:Label></td>
                                                    <td style="width: 444px; height: 26px;">
                                                        <asp:UpdatePanel ID="updatePanel_AmendNet" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                        <asp:TextBox ID="txtAmendNet" runat="server" AutoPostBack="True" OnTextChanged="txtAmendNet_TextChanged" ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td style="width: 100px; height: 26px;">
                                                    </td>
                                                    <td align="right" style="width: 225px; height: 26px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendVateRate" runat="server" CssClass="caption" Text="Vat Rate:"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:UpdatePanel ID="updatePanel_AmendVatRate" runat="server" RenderMode="Inline"
                                                            UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                        <asp:DropDownList ID="ddlAmendVat" runat="server" AutoPostBack ="True"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlAmendVat_SelectedIndexChanged">
                                                            <asp:ListItem Value="0">Please select</asp:ListItem>
                                                            <asp:ListItem Value="1">20</asp:ListItem>
                                                        </asp:DropDownList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:UpdateProgress ID="updateProgress_AmendVatRate" runat="server" AssociatedUpdatePanelID="updatePanel_AmendVatRate"
                                                DisplayAfter="10">
                                                            <ProgressTemplate>
                                                                <asp:Image ID="imgProgressBarAmendVatRate" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                            </ProgressTemplate>
                                                        </asp:UpdateProgress>
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td style="width: 225px">
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendVat" runat="server" CssClass="caption" Text="Vat          � :"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:UpdatePanel ID="updatePanel_AmendVat" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                        <asp:TextBox ID="txtAmendVat" runat="server" AutoPostBack="True" OnTextChanged="txtAmendVat_TextChanged" ReadOnly="True" ></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td style="width: 225px">
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendGross" runat="server" CssClass="caption" Text="Gross      �  :"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:UpdatePanel ID="updatePanel_AmendGross" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                        <asp:TextBox ID="txtAmendGross" runat="server" ReadOnly="True"></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td style="width: 225px">
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendFrom" runat="server" CssClass="caption" Text="Efective from :"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:TextBox ID="txtAmendDate" runat="server" Font-Names="Arial" Font-Size="Small"
                                                            MaxLength="10" Width="148px" Enabled="False"></asp:TextBox><asp:ImageButton ID="btnAmendCalander" runat="server" CausesValidation="False"
                                                    ImageUrl="~/images/buttons/Calendar_button.png" />&nbsp;
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td style="width: 225px">
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="5" style="height: 2px; background-color: #c00000" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendRecharge" runat="server" CssClass="caption" Text="Recharge :"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="width: 100px; height: 21px;">
                                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:RadioButton ID="rdbAmendRechargeYes" runat="server" Text="Yes" GroupName="grpRecharge" OnCheckedChanged="rdbRechargeYes_CheckedChanged2" CssClass="caption" AutoPostBack="True" CausesValidation="True" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                                <td style="width: 100px; height: 21px;">
                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                        <asp:RadioButton ID="rdbAmendRechargeNo" runat="server" Text="No" GroupName="grpRecharge" OnCheckedChanged="rdbRechargeNo_CheckedChanged1" CssClass="caption" />
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td style="width: 225px">
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px; height: 22px;">
                                                        <asp:Label ID="lblAmendPreInspection" runat="server" CssClass="caption" Text="Pre Inspection:"></asp:Label></td>
                                                    <td style="width: 444px; height: 22px;">
                                                        &nbsp;<asp:RadioButton ID="rdbAmendPreYes" runat="server" Text="Yes" GroupName="grpPreInspection" CssClass="caption" />
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <asp:RadioButton ID="rdbAmendPreNo" runat="server" Text="No" GroupName="grpPreInspection" CssClass="caption" /></td>
                                                    <td style="width: 100px; height: 22px;">
                                                    </td>
                                                    <td style="width: 225px; height: 22px;">
                                                    </td>
                                                    <td style="width: 100px; height: 22px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendStockCondition" runat="server" CssClass="caption" Text="Stock Condition Item:"
                                                            Width="150px"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        &nbsp;<asp:RadioButton ID="rdbAmendStockYes" runat="server" Text="Yes" GroupName="grpStock" CssClass="caption" />
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <asp:RadioButton ID="rdbAmendStockNo" runat="server" Text="No" GroupName="grpStock" CssClass="caption" /></td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td style="width: 225px">
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 191px">
                                                        <asp:Label ID="lblAmendFaultActive" runat="server" CssClass="caption" Text="Fault Active:"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        &nbsp;<asp:RadioButton ID="rdbAmendFaultYes" runat="server" Text="Yes" GroupName="grpFaultActive" CssClass="caption" />
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <asp:RadioButton ID="rdbAmendFaultNo" runat="server" Text="No" GroupName="grpFaultActive" CssClass="caption" /></td>
                                                    <td style="width: 100px">
                                                    </td>
                                                    <td style="width: 225px">
                                                    </td>
                                                    <td style="width: 100px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="5">
                                                        <cc2:ModalPopupExtender
                                                ID="MdlPopup_AmendFault" runat="server" BackgroundCssClass="modalBackground" CancelControlID="btnAmendClose"
                                                Drag="True" PopupControlID="updPnlAmendFault" PopupDragHandleControlID="lblAmendLoction"
                                                TargetControlID="lblAmendArea">
                                                        </cc2:ModalPopupExtender>
                                                        <cc2:CalendarExtender ID="ceAmendEfectiveFrom" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnAmendCalander"
                                                PopupPosition="BottomRight" TargetControlID="txtAmendDate">
                                                        </cc2:CalendarExtender><asp:UpdatePanel ID="UpdatePanel_AmenderrorMesg" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblAmendErrorMesg" runat="server" ForeColor="Red" CssClass="error_message_label"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="5" style="height: 2px; background-color: #c00000">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="5">
                                                        <asp:Button ID="btnAmendClose" runat="server" OnClick="btnAmendClose_Click" Text="Close" Width="56px" />
                                                        <asp:Button
                                                ID="btnAmendReset" runat="server" OnClick="btnAmendReset_Click" Text="Reset" OnClientClick="ClearAmendPopupCtrl();return false;" UseSubmitBehavior="False" />
                                                        <asp:UpdatePanel ID="UpdatePanel_AmendButton" runat="server" RenderMode="Inline"
                                                            UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                        <asp:Button ID="btnAmendSave" runat="server" OnClick="btnAmendSave_Click" Text="Save Amends" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        &nbsp; &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            </td> 
                            </tr> 
                            </table> 
                        </ContentTemplate>
                    </asp:UpdatePanel><asp:UpdatePanel ID="updatePnlEditElement" RenderMode="Inline" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
<TABLE bgColor="#f5f5f5"  border="2"><TBODY><TR><TD><TABLE style="WIDTH: 147px"><TBODY><TR><TD style="WIDTH: 132px; HEIGHT: 21px"><asp:Label id="lblEleName" runat="server" Text="Element Name:" Width="95px"></asp:Label></TD><TD style="WIDTH: 439px; HEIGHT: 21px"><asp:TextBox id="txtElementName" runat="server"></asp:TextBox></TD><TD style="WIDTH: 46px; HEIGHT: 21px"></TD></TR><TR><TD style="WIDTH: 132px"><asp:Label id="lblElementID" runat="server" Width="95px" Visible="False"></asp:Label></TD><TD style="WIDTH: 439px"><asp:Button id="btnUpdateName" onclick="btnUpdateName_Click" runat="server" Text="Update" UseSubmitBehavior="False"></asp:Button> <asp:Button id="btnCancel" runat="server" Text="Cancel"></asp:Button></TD><TD style="WIDTH: 46px"></TD></TR></TBODY></TABLE><cc2:ModalPopupExtender id="ModalPopup_EditElementName" runat="server" TargetControlID="lblEleName" PopupControlID="updatePnlEditElement" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
                            </cc2:ModalPopupExtender> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                    </asp:UpdatePanel>
                    &nbsp; &nbsp;
                   
                </td>
            </tr>
            <tr>
                <td style="width: 172px; height: 26px;">
                </td>
                <td align="right" style="width: 2139px; height: 26px;">
                    </td>
            </tr>
        </table>
    </asp:Content>

