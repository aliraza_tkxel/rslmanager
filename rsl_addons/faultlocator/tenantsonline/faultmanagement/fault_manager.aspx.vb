Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class fault_manager
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

#Region "Local Variables Declaration"
#End Region

#Region "Page Control Events"
#Region "Page Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/BHAIntranet/login.aspx")
        End If

        'If there's not customer info, he/she will be redirected to ~/error.aspx
        If IsNothing(Session("custHeadBO")) Then
            'Response.Redirect("~/error.aspx")
        End If


    End Sub

#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'rdbAmendRechargeYes.Attributes.Add("onLoad", Page.ClientScript.GetPostBackEventReference(rdbAmendRechargeYes, ""))

        If Not Me.IsPostBack Then
            If Request.QueryString("option") = "audit" Then
                Me.ddlFaultType.SelectedIndex = 0
                Me.ShowAuditTrail()
            ElseIf Request.QueryString("option") = "priority" Then
                Me.ddlFaultType.SelectedIndex = 3
                Me.ShowPrioritySettings()
            ElseIf Request.QueryString("option") = "pricing" Then
                Me.ddlFaultType.SelectedIndex = 4
                Me.ShowPricingControl()
            Else
                'Select the fault management option in drop down list
                ddlFaultType.SelectedIndex = 2
                ''Populate Location,Priority dropdown with values for search
                GetLocationLookUpValues()
                GetLocationPricingControlFaultLookUpValues()
                GetPriorityLookUpValues()
                GetVatRateLookUpValues()
                GetVatRateAmendFaultLookUpValues()
                UpdatePanel_PrioritySettings.Visible = False
                ViewState(ApplicationConstants.FaultPriorityIsEdit) = False

                'storing in view state boolean value indicating , results being displayed in gridview are 
                'from search or not
                ViewState(ApplicationConstants.CustFaultIsSearchViewState) = True
                'storing current sort order of any column asc or dsen
                ViewState(ApplicationConstants.CustFaultSortOrderViewState) = ApplicationConstants.CustFaultManagDESCSortOrder
                'function used for storing search parameters which will be used for further page requests if result is 
                'multipage
                SaveSearchOptions()
                'setting initial page index of gridview by default its zero
                GVFault.PageIndex = ApplicationConstants.CustFaultInitialPageIndex
                'getting and setting row count of resultset
                GVFault.VirtualItemCount = GetSearchRowCount()
                ''''''''''''''''''''''''''
                ddlLocation.Focus()
                ''Populate Grid for the First Time
                ' BindFaultGrid()
                PopulateSearchResults()

            End If
        End If




    End Sub
#End Region
#End Region

#Region "Helper to bind the grid to the dynamic data"

#Region "Bind Fault Grid "

    ''' Helper to bind the grid to the dynamic data
    Private Sub BindFaultGrid()
        Dim faultDS As DataSet = New DataSet()
        Dim faultSearchBO As FaultSearchBO = New FaultSearchBO()
        GetDataPage(GVFault.PageIndex, ApplicationConstants.CustFaultManagResultPerPage, GVFault.OrderBy, faultDS, faultSearchBO)
        GVFault.DataSource = faultDS
        GVFault.DataBind()
    End Sub
#End Region

#Region "Bind Fault Transaction Log Grid "

    Private Sub BindFaultTransactionLogGrid()
        Dim faultDS As DataSet = New DataSet()
        Dim faultTransactionLogBO As FaultTransactionLogBO = New FaultTransactionLogBO()
        GetDataPageFaultTransaction(GVAuditTrail.PageIndex, ApplicationConstants.FaultTransactionLogResultsPerPage, GVAuditTrail.OrderBy, faultDS, faultTransactionLogBO)
        GVAuditTrail.DataSource = faultDS
        GVAuditTrail.DataBind()
    End Sub
#End Region

#Region "Bind Fault Priority Grid "

    Private Sub BindFaultPriorityGrid()
        Dim faultDS As DataSet = New DataSet()
        Dim priorityBO As PriorityBO = New PriorityBO()
        GetDataPageFaultPriority(GVProiritySetting.PageIndex, ApplicationConstants.FaultPriorityResultsPerPage, GVProiritySetting.OrderBy, faultDS, priorityBO)
        GVAuditTrail.DataSource = faultDS
        GVAuditTrail.DataBind()
    End Sub
#End Region

#End Region

#Region "Utility Methods"

#Region "Get Lookup Values"

#Region "General Get Lookup Values Method"

#Region " Populate Lookup "

    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values by running the given stored procedure
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="spName">String: Stored Procedure's name to get data</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with two arguments</remarks>
    ''' 

    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
        End If

    End Sub
#End Region

#Region "Populate Lookup "

    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values by running the given stored procedure
    ''' against some given parameter
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="spName">String: Stored Procedure's name to get data</param>
    ''' <param name="inParam">String: parameter which has to be given to the stored procedure</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with three arguments</remarks>
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
        End If
    End Sub
#End Region

#End Region

#Region "Get Location Add Fault LookUp Values "

    ''Location Lookup Values to Add a new Fault
    Private Sub GetLocationAddFaultLookUpValues()
        PopulateLookup(ddlLocationFault, SprocNameConstants.GetLocationLookup)

    End Sub
#End Region

#Region "Get Element Add Fault LookUp Values "

    ''Element Lookup Values to Add a new Fault
    Private Sub GetElementAddFaultLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlElementFault, SprocNameConstants.GetElementLookup, inParam, inParamName)
    End Sub
#End Region

#Region "Get Area Add Fault Look Up Values "

    ''Area Lookup Values to Add a New Fault
    Private Sub GetAreaAddFaultLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlAreaFault, SprocNameConstants.GetAreaLookup, inParam, inParamName)

    End Sub
#End Region

#Region "Get Priority Add Fault LookUp Values "
    ''Priority Lookup Values to Add a new Fault
    Private Sub GetPriorityAddFaultLookUpValues()
        PopulateLookup(ddlPriorityFault, SprocNameConstants.GetPriorityLookup)

    End Sub
#End Region

#Region "Get Element Audit Look Up Values "
    ''Element Lookup Values for Audit Trail
    Private Sub GetElementAuditLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlAuditElement, SprocNameConstants.GetElementLookup, inParam, inParamName)
    End Sub
#End Region

#Region "Get Location Audit Fault LookUp Values "
    ''Location LookupValues for Audit Trail
    Private Sub GetLocationAuditFaultLookUpValues()
        PopulateLookup(ddlAuditLocation, SprocNameConstants.GetLocationLookup)

    End Sub
#End Region

#Region "Get Area Audit Look Up Values "

    ''Area Lookup Values for Audit Trail
    Private Sub GetAreaAuditLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlAuditArea, SprocNameConstants.GetAreaLookup, inParam, inParamName)
    End Sub
#End Region

#Region "Get Team Audit Look Up Values "
    ''Team Lookup Values for Audit Trail
    Private Sub GetTeamAuditLookUpValues()
        PopulateLookup(ddlTeam, SprocNameConstants.GetTeamLookup)
    End Sub
#End Region

#Region "Get User Audit Look Up Values "

    ''User Lookup Values for Audit Trail
    Private Sub GetUserAuditLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlUser, SprocNameConstants.GetUserLookup, inParam, inParamName)
    End Sub
#End Region

#Region "Get Location Pricing Control FaultLook Up Values "
    ''Location LookupValues for Audit Trail
    Private Sub GetLocationPricingControlFaultLookUpValues()
        'PopulateLookup(ddlPricingControlLocation, SprocNameConstants.GetLocationLookup)

        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlPricingControlLocation.Items.Clear()
        objBL.getLookUpList(SprocNameConstants.GetLocationLookup, lstLookup)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlPricingControlLocation.DataSource = lstLookup
            ddlPricingControlLocation.DataValueField = "LookUpValue"
            ddlPricingControlLocation.DataTextField = "LookUpName"
            ddlPricingControlLocation.Items.Add(New ListItem("Select All", ""))
            ddlPricingControlLocation.DataBind()
        End If

    End Sub
#End Region

#Region "Get Element Pricing Control LookUp Values "

    ''Element Lookup Values for Pricing Control
    Private Sub GetElementPricingControlLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        ' PopulateLookup(ddlPricingControlElement, SprocNameConstants.GetElementLookup, inParam, inParamName)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlPricingControlElement.Items.Clear()
        objBL.getLookUpList(SprocNameConstants.GetElementLookup, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlPricingControlElement.DataSource = lstLookup
            ddlPricingControlElement.DataValueField = "LookUpValue"
            ddlPricingControlElement.DataTextField = "LookUpName"
            ddlPricingControlElement.Items.Add(New ListItem("Select All", ""))
            ddlPricingControlElement.DataBind()
        End If
    End Sub
#End Region

#Region "Get Area Pricing Control LookUp Values "

    ''Area Lookup Values for Pricing Control
    Private Sub GetAreaPricingControlLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        'PopulateLookup(ddlPricingControlArea, SprocNameConstants.GetAreaLookup, inParam, inParamName)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlPricingControlArea.Items.Clear()
        objBL.getLookUpList(SprocNameConstants.GetAreaLookup, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlPricingControlArea.DataSource = lstLookup
            ddlPricingControlArea.DataValueField = "LookUpValue"
            ddlPricingControlArea.DataTextField = "LookUpName"
            ddlPricingControlArea.Items.Add(New ListItem("Select All", ""))
            ddlPricingControlArea.DataBind()
        End If
    End Sub
#End Region

#Region "Get Priority Amend Fault Look Up Values "

    ''Priority Lookup Values to Amend a Fault
    Private Sub GetPriorityAmendFaultLookUpValues()
        PopulateLookup(ddlPriorityAmendFault, SprocNameConstants.GetPriorityLookup)

    End Sub
#End Region

#Region "Get Vat Rate Amend Fault LookUp Values "
    ''Vat Rate Lookup Values for Amend Fault Panel
    Private Sub GetVatRateAmendFaultLookUpValues()
        PopulateLookup(ddlAmendVat, SprocNameConstants.GetVatRate)

    End Sub
#End Region

#Region "Get Location Look Up Values "

    ''Location Lookup Values
    Private Sub GetLocationLookUpValues()

        PopulateLookup(ddlLocation, SprocNameConstants.GetLocationLookup)

    End Sub
#End Region

#Region "Get Element Look Up Values "

    ''Element Lookup Values
    Private Sub GetElementLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlElement, SprocNameConstants.GetElementLookup, inParam, inParamName)

    End Sub
#End Region

#Region "Get Area Look Up Values "
    ''Area Lookup Values
    Private Sub GetAreaLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlArea, SprocNameConstants.GetAreaLookup, inParam, inParamName)

    End Sub
#End Region

#Region "Get Priority LookUp Values "

    ''Priority Lookup Values
    Private Sub GetPriorityLookUpValues()
        PopulateLookup(ddlPriority, SprocNameConstants.GetPriorityLookup)

    End Sub
#End Region

#Region "Get Vat Rate LookUp Values "

    ''Vat Rate Lookup Values
    Private Sub GetVatRateLookUpValues()
        PopulateLookup(ddlVat, SprocNameConstants.GetVatRate)

    End Sub
#End Region

#Region "Get Description Look Up Values "
    ''Area Lookup Values for fault description
    Private Sub GetDescriptionLookUpValues(ByVal inParam As String, ByVal inParamName As String)

        ' PopulateLookup(ddlPricingControlDescription, SprocNameConstants.GetFaultDescriptionLookup, inParam, inParamName)
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.GetFaultDescriptionLookup
        Dim lstLookUp As New LookUpList
        objBL.getLookUpList(spName, lstLookUp, inParam, inParamName)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlPricingControlDescription.Items.Clear()
            Me.ddlPricingControlDescription.DataSource = lstLookUp
            Me.ddlPricingControlDescription.DataValueField = "LookUpValue"
            Me.ddlPricingControlDescription.DataTextField = "LookUpName"
            Me.ddlPricingControlDescription.Items.Add(New ListItem("Select All", ""))
            Me.ddlPricingControlDescription.DataBind()

        End If

    End Sub
#End Region


#End Region

#Region "Get Priority Response"

    ''Priority Response Time
    Private Sub GetPriorityResponseTime(ByVal inParam As String, ByRef lblResponse As Label)
        Dim objBL As New UtilityManager
        Dim lstLookUp As New LookUpList
        Dim responseStr As String = ""
        responseStr = objBL.GetPriorityResponse(inParam)
        If responseStr <> Nothing Then
            lblResponse.Text = responseStr
        Else
            lblResponse.Text = ""
        End If



    End Sub

#End Region

#Region "GetSearchRecords"

    Protected Sub GetSearchResults()
        Dim faultSearchBO As FaultSearchBO = New FaultSearchBO()
        'reterive search options enqryLogSearchBO from viewstate before requesting results for next page of gridview
        faultSearchBO = DirectCast(ViewState(ApplicationConstants.CustFaultSearchOptionsViewState), FaultSearchBO)
        Dim faultDS As DataSet = New DataSet()
        'calling GetDataPage function that will further call function in buisness layer
        GetDataPage(GVFault.PageIndex, ApplicationConstants.CustEnquiryManagResultPerPage, GVFault.OrderBy, faultDS, faultSearchBO)
        'binding gridview with the result returned from db
        Me.StoreResultSummary()
        GVFault.DataSource = faultDS
        GVFault.DataBind()
    End Sub

    Protected Sub GetFaultTransactionLogSearchResults()
        Dim faultTransactionLogBO As FaultTransactionLogBO = New FaultTransactionLogBO()
        'reterive search options fualtTransactionLogSearchBO from viewstate before requesting results for next page of gridview
        faultTransactionLogBO = DirectCast(ViewState(ApplicationConstants.FaultTransactionLogSearchOptionsViewState), FaultTransactionLogBO)
        Dim faultDS As DataSet = New DataSet()
        'calling GetDataPage function that will further call function in buisness layer
        GetDataPageFaultTransaction(GVAuditTrail.PageIndex, ApplicationConstants.FaultTransactionLogResultsPerPage, GVAuditTrail.OrderBy, faultDS, faultTransactionLogBO)
        'binding gridview with the result returned from db
        Me.StoreFaultTransactionLogResultSummary()
        GVAuditTrail.DataSource = faultDS
        GVAuditTrail.DataBind()
    End Sub

    Protected Sub GetFaultPrioritySearchResults()
        Dim priorityBO As PriorityBO = New PriorityBO()
        'reterive search options priorityBO from viewstate before requesting results for next page of gridview
        priorityBO = DirectCast(ViewState(ApplicationConstants.FaultPrioritySearchOptionsViewState), PriorityBO)
        Dim priorityDS As DataSet = New DataSet()
        'calling GetDataPage function that will further call function in buisness layer
        GetDataPageFaultPriority(GVProiritySetting.PageIndex, ApplicationConstants.FaultPriorityResultsPerPage, GVProiritySetting.OrderBy, priorityDS, priorityBO)
        'binding gridview with the result returned from db
        Me.StoreFaultPrioritiesResultSummary()
        GVProiritySetting.DataSource = priorityDS
        GVProiritySetting.DataBind()
    End Sub

    Private Sub getSearchRecords()
        Dim objBAL As FaultManager = New FaultManager()
        Dim faultSearchBO As FaultSearchBO = New FaultSearchBO()
        Dim faultDS As DataSet = New DataSet()
        Try

            If Me.IsPostBack = True Then
                faultSearchBO.LocationID = ddlLocation.SelectedValue.ToString()
                faultSearchBO.AreaID = ddlArea.SelectedValue.ToString()
                faultSearchBO.ElementID = ddlElement.SelectedValue.ToString()
                faultSearchBO.PriorityID = ddlPriority.SelectedValue.ToString()
                If (ddlStatus.SelectedIndex = 0) Then
                    faultSearchBO.Status = 1
                Else
                    faultSearchBO.Status = 0
                End If

            End If
            'faultDS = objBAL.GetFaultSearchResults(faultSearchBO)
            'GVFault.DataSource = faultDS
            'GVFault.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub PopulateSearchResults()
        'save these new search options entered by user unless search button is pressed again
        SaveSearchOptions()
        'setting new starting page index of for this new search
        GVFault.PageIndex = ApplicationConstants.CustFaultInitialPageIndex
        'getting and setting row count of new search results
        GVFault.VirtualItemCount = GetSearchRowCount()
        'getting search results
        GetSearchResults()
        UpdatePanel_LoadGrid.Update()
    End Sub

    Private Sub PopulateFaultTransactionLogSearchResults()
        'save these new search options entered by user unless search button is pressed again
        SaveFaultTransactionLogSearchOptions()
        'setting new starting page index of for this new search
        GVAuditTrail.PageIndex = ApplicationConstants.FaultTransactionLogInitialPageIndex
        'getting and setting row count of new search results
        GVAuditTrail.VirtualItemCount = GetFaultTransactionLogSearchRowCount()
        'getting search results
        GetFaultTransactionLogSearchResults()
        UpdatePanel_AuditTrail.Update()
    End Sub

    Private Sub PopulateFaultPrioritySearchResults()
        'save these new search options entered by user unless search button is pressed again
        SaveFaultPrioritySearchOptions()
        'setting new starting page index of for this new search
        GVProiritySetting.PageIndex = ApplicationConstants.FaultPriorityInitialPageIndex
        'getting and setting row count of new search results
        GVProiritySetting.VirtualItemCount = GetFaultPrioritySearchRowCount()
        'getting search results
        GetFaultPrioritySearchResults()
        UpdatePanel_PriorityGrid.Update()
    End Sub

    Private Sub GetFaultTransactionLogSearchRecords()
        'storing in view state boolean value indicating , results being displayed in gridview are 
        'from search or not
        ViewState(ApplicationConstants.FaultTransactionLogIsSearchViewState) = True
        'storing current sort order of any column asc or dsen
        ViewState(ApplicationConstants.FaultTransactionLogSortOrderViewState) = ApplicationConstants.FaultTransactionLogDESCSortOrder
        'function used for storing search parameters which will be used for further page requests if result is 
        'multipage
        SaveFaultTransactionLogSearchOptions()
        'setting initial page index of gridview by default its zero
        GVAuditTrail.PageIndex = ApplicationConstants.FaultTransactionLogInitialPageIndex
        'getting and setting row count of resultset
        GVAuditTrail.VirtualItemCount = GetFaultTransactionLogSearchRowCount()
        ''''''''''''''''''''''''''
        ddlAuditLocation.Focus()
        ''Populate Grid for the First Time
        ' BindFaultGrid()
        PopulateFaultTransactionLogSearchResults()
    End Sub

    Private Sub GetFaultPrioritySearchRecords()
        'storing in view state boolean value indicating , results being displayed in gridview are 
        'from search or not
        ViewState(ApplicationConstants.FaultPriorityIsSearchViewState) = True
        'storing current sort order of any column asc or dsen
        ViewState(ApplicationConstants.FaultPrioritySortOrderViewState) = ApplicationConstants.FaultPriorityDESCSortOrder
        'function used for storing search parameters which will be used for further page requests if result is 
        'multipage
        SaveFaultPrioritySearchOptions()
        'setting initial page index of gridview by default its zero
        GVProiritySetting.PageIndex = ApplicationConstants.FaultPriorityInitialPageIndex
        'getting and setting row count of resultset
        GVProiritySetting.VirtualItemCount = GetFaultPrioritySearchRowCount()
        ''''''''''''''''''''''''''
        ''Populate Grid for the First Time
        ' BindFaultGrid()
        PopulateFaultPrioritySearchResults()
    End Sub

    Private Function GetSearchRowCount() As Integer
        Dim objBAL As FaultManager = New FaultManager()
        Dim faultSearchBO As FaultSearchBO = New FaultSearchBO()
        faultSearchBO = DirectCast(ViewState(ApplicationConstants.CustFaultSearchOptionsViewState), FaultSearchBO)
        Return (objBAL.GetFaultSearchRowCount(faultSearchBO))
    End Function

    Private Function GetFaultTransactionLogSearchRowCount() As Integer
        Dim objBAL As FaultManager = New FaultManager()
        Dim faultTransactionLogBO As FaultTransactionLogBO = New FaultTransactionLogBO()
        faultTransactionLogBO = DirectCast(ViewState(ApplicationConstants.FaultTransactionLogSearchOptionsViewState), FaultTransactionLogBO)
        Return (objBAL.GetFaultTransactionLogSearchRowCount(faultTransactionLogBO))
    End Function

    Private Function GetFaultPrioritySearchRowCount() As Integer
        Dim objBAL As FaultManager = New FaultManager()
        Dim priorityBO As PriorityBO = New PriorityBO()
        priorityBO = DirectCast(ViewState(ApplicationConstants.FaultPrioritySearchOptionsViewState), PriorityBO)
        Return (objBAL.GetFaultPrioritySearchRowCount(priorityBO))
    End Function

#End Region

#Region "Sub for storing search options"

    Private Sub SaveSearchOptions()
        'make FaultSearchBO object and store the search options entered by user untill he/she enters again
        'in this case the values will be overidden
        Dim faultSearchBO As FaultSearchBO = New FaultSearchBO()

        'set Location ID selected
        faultSearchBO.LocationID = IIf(ddlLocation.Text = "Please select" Or ddlLocation.Text = "", Nothing, ddlLocation.SelectedValue.ToString())

        'set Area Id selected
        faultSearchBO.AreaID = IIf(ddlArea.Text = "" Or ddlArea.Text = "Please select", Nothing, ddlArea.SelectedValue.ToString())

        'set Element ID selected
        faultSearchBO.ElementID = IIf(ddlElement.Text = "" Or ddlElement.Text = "Please select", Nothing, ddlElement.SelectedValue.ToString())

        'set Priority ID selected
        faultSearchBO.PriorityID = IIf(ddlPriority.Text = "" Or ddlPriority.Text = "Please select", Nothing, ddlPriority.SelectedValue.ToString())

        'set status value enetered
        faultSearchBO.Status = IIf(ddlStatus.Text = "" Or ddlStatus.Text = "Please select", Nothing, ddlStatus.SelectedValue.ToString)

        'tell EnquiryLogSearchBO it has values that will be used for searching
        faultSearchBO.IsSearch = True
        'store EnquiryLogSearchBO object in viewstate
        ViewState(ApplicationConstants.CustFaultSearchOptionsViewState) = faultSearchBO
        'store true in viewstate to indicate that current data on gridview is from search
        ViewState(ApplicationConstants.CustFaultIsSearchViewState) = True
    End Sub

    Private Sub SaveFaultTransactionLogSearchOptions()
        'make FaultTransactionLogBO object and store the search options entered by user untill he/she enters again
        'in this case the values will be overidden
        Dim faultTransactionLogBO As FaultTransactionLogBO = New FaultTransactionLogBO()

        'set Location ID selected
        faultTransactionLogBO.LocationID = IIf(ddlAuditLocation.Text = "Please select" Or ddlAuditLocation.Text = "", Nothing, ddlAuditLocation.SelectedValue.ToString())

        'set Area Id selected
        faultTransactionLogBO.AreaID = IIf(ddlAuditArea.Text = "" Or ddlAuditArea.Text = "Please select", Nothing, ddlAuditArea.SelectedValue.ToString())

        'set Element ID selected
        faultTransactionLogBO.ElementID = IIf(ddlAuditElement.Text = "" Or ddlAuditElement.Text = "Please select", Nothing, ddlAuditElement.SelectedValue.ToString())

        'set Priority ID selected
        faultTransactionLogBO.TeamID = IIf(ddlTeam.Text = "" Or ddlTeam.Text = "Please select", Nothing, ddlTeam.SelectedValue.ToString())

        'set status value enetered
        faultTransactionLogBO.UserID = IIf(ddlUser.Text = "" Or ddlUser.Text = "Please select", Nothing, ddlUser.SelectedValue.ToString)

        'tell EnquiryLogSearchBO it has values that will be used for searching
        faultTransactionLogBO.IsSearch = True
        'store EnquiryLogSearchBO object in viewstate
        ViewState(ApplicationConstants.FaultTransactionLogSearchOptionsViewState) = faultTransactionLogBO
        'store true in viewstate to indicate that current data on gridview is from search
        ViewState(ApplicationConstants.FaultTransactionLogIsSearchViewState) = True
    End Sub

    Private Sub SaveFaultPrioritySearchOptions()
        'make FaultTransactionLogBO object and store the search options entered by user untill he/she enters again
        'in this case the values will be overidden
        Dim priorityBO As PriorityBO = New PriorityBO()

        'tell EnquiryLogSearchBO it has values that will be used for searching
        'priorityBO.IsSearch = True
        'store EnquiryLogSearchBO object in viewstate
        ViewState(ApplicationConstants.FaultPrioritySearchOptionsViewState) = priorityBO
        'store true in viewstate to indicate that current data on gridview is from search
        ViewState(ApplicationConstants.FaultPriorityIsSearchViewState) = True
    End Sub
#End Region

#Region "Store Result Summary"
    'when page index changes this functions gets called 
    Public Sub StoreResultSummary()
        Dim row As GridViewRow = GVFault.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVFault.PageIndex + 1 = GVFault.PageCount Then
            strResultSummary = "Result " & ((GVFault.PageIndex * GVFault.PageSize) + 1) & " to " & GVFault.VirtualItemCount & " of " & GVFault.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVFault.PageIndex * GVFault.PageSize) + 1) & " to " & ((GVFault.PageIndex + 1) * GVFault.PageSize) & " of " & GVFault.VirtualItemCount
        End If
        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.CustFaultResultSummary) = strResultSummary
    End Sub

    'when page index changes this functions gets called 
    Public Sub StoreFaultTransactionLogResultSummary()
        Dim row As GridViewRow = GVAuditTrail.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVAuditTrail.PageIndex + 1 = GVAuditTrail.PageCount Then
            strResultSummary = "Result " & ((GVAuditTrail.PageIndex * GVAuditTrail.PageSize) + 1) & " to " & GVAuditTrail.VirtualItemCount & " of " & GVAuditTrail.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVAuditTrail.PageIndex * GVAuditTrail.PageSize) + 1) & " to " & ((GVAuditTrail.PageIndex + 1) * GVAuditTrail.PageSize) & " of " & GVAuditTrail.VirtualItemCount
        End If
        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.FaultTransactionLogResultSummary) = strResultSummary
    End Sub

    'when page index changes this functions gets called 
    Public Sub StoreFaultPrioritiesResultSummary()
        Dim row As GridViewRow = GVProiritySetting.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVProiritySetting.PageIndex + 1 = GVProiritySetting.PageCount Then
            strResultSummary = "Result " & ((GVProiritySetting.PageIndex * GVProiritySetting.PageSize) + 1) & " to " & GVProiritySetting.VirtualItemCount & " of " & GVProiritySetting.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVProiritySetting.PageIndex * GVProiritySetting.PageSize) + 1) & " to " & ((GVProiritySetting.PageIndex + 1) * GVProiritySetting.PageSize) & " of " & GVProiritySetting.VirtualItemCount
        End If
        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.FaultPriorityResultSummary) = strResultSummary
    End Sub

#End Region

#Region "Function to Reset Controls"

    Public Sub GetControlsReset()

        lblAddErrorMesg.Text = ""
        UpdatePanel_errorMesg.Update()

        ddlLocationFault.SelectedIndex = 0
        If (ddlAreaFault.Items.Count > 0) Then
            ddlAreaFault.SelectedIndex = 0
        End If
        If (ddlElementFault.Items.Count > 0) Then
            ddlElementFault.SelectedIndex = 0
        End If
        txtDescription.Text = ""
        ddlPriorityFault.SelectedIndex = 0
        lblResponseTime.Text = ""
        txtNet.Text = "0.0"
        If (ddlVat.Items.Count > 0) Then
            ddlVat.SelectedIndex = 0
        End If
        txtVat.Text = "0.0"
        txtGross.Text = "0.0"
        txtDate.Text = ""
        rdbRechargeYes.Checked = True
        rdbRechargeNo.Checked = False
        rdbPreYes.Checked = True
        rdnPreNo.Checked = False
        'rdbPostYes.Checked = True
        'rdbPostNo.Checked = False
        rdbStockYes.Checked = True
        rdbStockNo.Checked = False
        rdbFaultYes.Checked = True
        rdbFaultNo.Checked = False
        lblAddErrorMesg.Text = ""

    End Sub

#End Region

#Region "Method to Get Fault Values"

    Private Sub GetFaultValues(ByVal faultID As Int32)
        Dim faultBO As FaultBO = New FaultBO()
        Try
            Dim objFault As FaultManager = New FaultManager()

            Dim faultDS As New DataSet()
            faultBO.FaultID = faultID
            objFault.GetFaultValues(faultDS, faultBO)
            Me.PopulateAmendFaultFields(faultDS)

        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message


        End Try
    End Sub


#End Region

#Region "Method to Get Fault Priority Values"
    Private Sub GetPriorityValues(ByVal priorityID As Int32)
        Dim priorityBO As PriorityBO = New PriorityBO()
        Try
            Dim objFault As FaultManager = New FaultManager()

            Dim faultDS As New DataSet()
            priorityBO.PriorityID = priorityID
            objFault.GetPriorityValues(faultDS, priorityBO)
            Me.PopulateAmendPriorityFields(faultDS)

        Catch ex As Exception
            priorityBO.IsExceptionGenerated = True
            priorityBO.ExceptionMsg = ex.Message


        End Try
    End Sub
#End Region

#Region "Populate Amend Faults Fields"
    Private Sub PopulateAmendFaultFields(ByVal faultDS As DataSet)

        If faultDS.Tables(0).Rows.Count > 0 And Not (faultDS Is Nothing) Then
            Me.lblFaultID.Text = faultDS.Tables(0).Rows(0)("FAULTID").ToString()
            Me.lblAmendFaultLocation.Text = faultDS.Tables(0).Rows(0)("LocationName").ToString()
            Me.lblAmendFaultLocation.Visible = True
            Me.lblAmendFaultArea.Text = faultDS.Tables(0).Rows(0)("AreaName").ToString()
            Me.lblAmendFaultElement.Text = faultDS.Tables(0).Rows(0)("ElementName").ToString()
            Me.txtAmendFaultDesc.Text = faultDS.Tables(0).Rows(0)("Description").ToString()
            Me.ddlPriorityAmendFault.SelectedValue = CType(faultDS.Tables(0).Rows(0)("PriorityID"), Int32)
            GetPriorityResponseTime(ddlPriorityAmendFault.SelectedValue, lblAmendResponseTime)
            Me.txtAmendNet.Text = CType(faultDS.Tables(0).Rows(0)("NetCost"), Decimal).ToString("0.00")
            Me.txtAmendVat.Text = faultDS.Tables(0).Rows(0)("Vat").ToString()
            Me.txtAmendGross.Text = CType(faultDS.Tables(0).Rows(0)("Gross"), Decimal).ToString("0.00")
            If faultDS.Tables(0).Rows(0)("VatRateID").ToString() = "0" Or faultDS.Tables(0).Rows(0)("VatRateID").ToString() = Nothing Then
                Me.ddlAmendVat.SelectedValue = ""
            Else
                Me.ddlAmendVat.SelectedValue = CType(faultDS.Tables(0).Rows(0)("VatRateID"), Int32)
            End If

            Me.txtAmendDate.Text = UtilityFunctions.FormatDate(faultDS.Tables(0).Rows(0)("EffectFrom").ToString()).ToString()

            If CType(faultDS.Tables(0).Rows(0)("Recharge"), Boolean) = True Then
                rdbAmendRechargeYes.Checked = True
                rdbAmendRechargeNo.Checked = False

            Else
                rdbAmendRechargeNo.Checked = True
                rdbAmendRechargeYes.Checked = False


            End If
            If CType(faultDS.Tables(0).Rows(0)("PreInspection"), Boolean) = True Then
                rdbAmendPreYes.Checked = True
                rdbAmendPreNo.Checked = False
            Else
                rdbAmendPreNo.Checked = True
                rdbAmendPreYes.Checked = False

            End If
            'If CType(faultDS.Tables(0).Rows(0)("PostInspection"), Boolean) = True Then
            '    rdbAmendPostYes.Checked = True
            '    rdbAmendPostNo.Checked = False
            'Else
            '    rdbAmendPostNo.Checked = True
            '    rdbAmendPostYes.Checked = False

            'End If
            If CType(faultDS.Tables(0).Rows(0)("StockConditionItem"), Boolean) = True Then
                rdbAmendStockYes.Checked = True
                rdbAmendStockNo.Checked = False
            Else
                rdbAmendStockNo.Checked = True
                rdbAmendStockYes.Checked = False

            End If
            If CType(faultDS.Tables(0).Rows(0)("FaultActive"), Boolean) = True Then
                rdbAmendFaultYes.Checked = True
                rdbAmendFaultNo.Checked = False
            Else
                rdbAmendFaultNo.Checked = True
                rdbAmendFaultYes.Checked = False

            End If
        End If
    End Sub

#End Region

#Region "Populate Amend Priority Fields"
    Private Sub PopulateAmendPriorityFields(ByVal priorityDs As DataSet)
        If (priorityDs.Tables(0).Rows.Count > 0) Then
            lblPriorityID.Text = priorityDs.Tables(0).Rows(0)("PriorityID").ToString()
            txtPriorityDesc.Text = priorityDs.Tables(0).Rows(0)("PriorityName").ToString()
            txtTargetTime.Text = priorityDs.Tables(0).Rows(0)("ResponseTime").ToString()
            If CType(priorityDs.Tables(0).Rows(0)("Days"), Boolean) = True Then
                ddlTargetTime.SelectedValue = 1
            Else
                ddlTargetTime.SelectedValue = 0
            End If

            If (CType(priorityDs.Tables(0).Rows(0)("Status"), Boolean) = True) Then
                rdbActive.Checked = True
                rdbInactive.Checked = False
            Else
                rdbInactive.Checked = True
                rdbActive.Checked = False
            End If

        End If
    End Sub
#End Region

#Region "Method to populate Edit Element Fields "
    Private Sub populateElementField()

        txtElementName.Text = ddlElementFault.SelectedItem.Text
        lblElementID.Text = ddlElementFault.SelectedValue.ToString()
        updatePnlEditElement.Update()

    End Sub
#End Region

#Region "Method to get Vat Values"

    Private Sub PopulateVatValues(ByVal ddlName As String)
        Dim objFault As New FaultManager()
        Dim vatBO As New VatBO()
        Try
            If (ddlName = "Vat") Then
                vatBO.VatID = CType(ddlVat.SelectedValue, Int32)
                objFault.GetVatValues(vatBO)
            Else
                vatBO.VatID = CType(ddlAmendVat.SelectedValue, Int32)
                objFault.GetVatValues(vatBO)
            End If
            CalculateVat(vatBO, ddlName)
        Catch ex As Exception
            vatBO.IsExceptionGenerated = True
            vatBO.ExceptionMsg = ex.Message

        End Try

    End Sub

#End Region

#Region "Method to Calculate Vat"
    Private Sub CalculateVat(ByRef vatBO As VatBO, ByVal ddlName As String)

        Dim vatAmount As Double

        Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")
        If (ddlName = "Vat") Then
            If txtNet.Text <> "" And re.IsMatch(txtNet.Text) Then
                vatAmount = vatBO.VatRate
                txtVat.Text = Math.Round(((CType(txtNet.Text, Double) * vatAmount) / 100), 2).ToString()
                updatePanel_Vat.Update()
                txtGross.Text = Math.Round(Double.Parse(txtNet.Text) + ((Double.Parse(txtNet.Text) * vatAmount) / 100), 2)
                updatePanel_Gross.Update()
            Else
                lblAddErrorMesg.Text = "Please Enter Correct Net Cost"
                txtNet.Focus()
                UpdatePanel_errorMesg.Update()
            End If
        Else
            If txtAmendNet.Text <> "" And re.IsMatch(txtAmendNet.Text) Then
                vatAmount = vatBO.VatRate
                txtAmendVat.Text = Math.Round(((Double.Parse(txtAmendNet.Text) * vatAmount) / 100), 2).ToString()
                updatePanel_AmendVat.Update()
                txtAmendGross.Text = Math.Round(Double.Parse(txtAmendNet.Text) + ((Double.Parse(txtAmendNet.Text) * vatAmount) / 100), 2)
                updatePanel_AmendGross.Update()

            End If

        End If

    End Sub
#End Region

#Region "Method to Clear Priority Controls"
    Private Sub ClearPriorityControls()
        txtPriorityDesc.Text = ""
        lblPriorityID.Text = "0"
        txtTargetTime.Text = ""
        ddlTargetTime.SelectedValue = "1"
        rdbActive.Checked = True
        rdbInactive.Checked = False
        lblErrorMessage.Text = ""
        UpdatePanel_AddAmendPriority.Update()
    End Sub
#End Region

#Region "Get Pricing Control Data"
    ''Get pricing control data to populate net,vatrate,vat,gross 
    Private Sub GetPricingControlData(ByVal inParam As String, ByVal inParamName As String)
        Dim objBL As New FaultManager()
        Dim spName As String = SprocNameConstants.GetPricingControlData
        Dim faultBO As New FaultBO
        objBL.getPricingControlData(spName, faultBO, inParam, inParamName)

        ' If faultBO.IsExceptionGenerated Then
        'Response.Redirect("~/error.aspx")
        'End If

        If faultBO.IsRecordRetrieved = False Then
            txtPricingControlNet.Text = ""
            txtPricingControlVatRate.Text = ""
            txtPricingControlVat.Text = ""
            txtPricingControlGross.Text = ""
            txtvatRateHidden.Text = ""
            txtVatHidden.Text = ""
            UpdatePanel_PriceControl.Update()
        Else
            txtPricingControlNet.Text = CType(faultBO.NetCost, Decimal).ToString("0.00")
            txtPricingControlVatRate.Text = faultBO.VatName
            txtPricingControlVat.Text = faultBO.Vat
            txtPricingControlGross.Text = faultBO.Gross
            txtvatRateHidden.Text = faultBO.VatRateID
            txtVatHidden.Text = faultBO.VatRate
            UpdatePanel_PriceControl.Update()
        End If

    End Sub


#End Region

#Region "Reset net,vat and gross according to inflation"
    Private Sub resetByInflation()
        Try
            If txtPricingControlNet.Text <> String.Empty Then
                Dim net As Double = CType(txtPricingControlNet.Text, Double)
                Dim vat As Double = CType(txtPricingControlVat.Text, Double)
                Dim gross As Double
                If txtPricingControlInflationValue.Text <> String.Empty Then
                    Dim vatRate As Double = CType(txtVatHidden.Text, Double)
                    Dim inflation As Double = CType(txtPricingControlInflationValue.Text, Double)
                    net = net + (net * inflation / 100)
                    vat = net * vatRate / 100
                    gross = net + vat
                    Me.txtPricingControlNet.Text = net
                    Me.txtPricingControlVat.Text = vat
                    Me.txtPricingControlGross.Text = gross
                    Me.UpdatePanel_PriceControl.Update()
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)

        End Try
        
    End Sub
#End Region

#Region "Clear Pricing Controls"
    Private Sub ClearPricingControls()
        ddlPricingControlArea.Items.Clear()
        ddlPricingControlArea.Items.Add(New ListItem("Please select", ""))
        UpdatePanel_LoadPricingControlArea.Update()
        ddlPricingControlElement.Items.Clear()
        ddlPricingControlElement.Items.Add(New ListItem("Please select", ""))
        UpdatePanel_LoadPricingControlElement.Update()
        ddlPricingControlDescription.Items.Clear()
        ddlPricingControlDescription.Items.Add(New ListItem("Please select", ""))
        UpdatePanel_PricingControlDescription.Update()
        txtPricingControlNet.Text = ""
        txtPricingControlVatRate.Text = ""
        txtPricingControlVat.Text = ""
        txtPricingControlGross.Text = ""
        txtPricingControlInflationValue.Text = ""
        txtPricingControlEffectiveDate.Text = ""
        txtvatRateHidden.Text = ""
        txtVatHidden.Text = ""
        ddlPricingControlLocation.SelectedValue = ""
        lblPricingError.Text = ""
        UpdatePanel_PriceControl.Update()
    End Sub
#End Region


#End Region

#Region "Method to Save PriceControlData"
    Private Sub SavePriceControlData()
        Dim objFault As FaultManager = New FaultManager()
        Dim faultBO As FaultBO = New FaultBO()
        Try
            lblPricingError.Text = ""
            ''Get Values
            If Me.ddlPricingControlLocation.SelectedValue <> String.Empty And Me.ddlPricingControlLocation.SelectedValue <> "-1" Then
                faultBO.LocationID = CType(ddlPricingControlLocation.SelectedValue, Int32)
            End If
            If Me.ddlPricingControlArea.SelectedValue <> String.Empty And Me.ddlPricingControlArea.SelectedValue <> "-1" Then
                faultBO.AreaID = CType(ddlPricingControlArea.SelectedValue, Int32)
            End If
            If Me.ddlPricingControlElement.SelectedValue <> String.Empty And Me.ddlPricingControlElement.SelectedValue <> "-1" Then
                faultBO.ElementID = CType(ddlPricingControlElement.SelectedValue, Int32)
            End If
            If Me.ddlPricingControlDescription.SelectedValue <> String.Empty And Me.ddlPricingControlDescription.SelectedValue <> "-1" Then
                faultBO.FaultID = CType(ddlPricingControlDescription.SelectedValue, Int32)
            End If
            If Me.txtPricingControlNet.Text <> String.Empty Then
                faultBO.NetCost = CType(txtPricingControlNet.Text, Int32)
            End If
            If Me.txtvatRateHidden.Text <> String.Empty Then
                faultBO.VatRateID = CType(txtvatRateHidden.Text, Int32)
            End If

            If Me.txtPricingControlVat.Text <> String.Empty Then
                faultBO.Vat = CType(txtPricingControlVat.Text, Int32)
            End If

            If Me.txtPricingControlGross.Text <> String.Empty Then
                faultBO.Gross = CType(txtPricingControlGross.Text, Int32)
            End If

            If Me.txtPricingControlInflationValue.Text <> String.Empty Then
                faultBO.InflationValue = CType(txtPricingControlInflationValue.Text, Int32)
            End If

            If Me.txtPricingControlEffectiveDate.Text <> String.Empty Then
                faultBO.EffectFrom = txtPricingControlEffectiveDate.Text
            End If


            objFault.AddPricingControlData(faultBO)
            GetPricingControlData(ddlPricingControlDescription.SelectedValue.ToString, "@FaultID")
            If (faultBO.IsFlagStatus = True) Then
                lblPricingError.Text = "Pricing Values Changed Successfully"
                PopulateSearchResults()
                lblPricingError.Visible = True

            End If


        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message


        End Try
    End Sub
#End Region

#Region "Function to get data "
    Public Sub GetDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef faultDS As DataSet, ByVal faultSearchBO As FaultSearchBO)

        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.CustFaultSortByViewState) Is Nothing) Then
            faultSearchBO.SortBy = CType(ViewState(ApplicationConstants.CustFaultSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use equirylogid as a default
            faultSearchBO.SortBy = ApplicationConstants.CustFaultManagSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        faultSearchBO.PageIndex = pageIndex
        faultSearchBO.RowCount = pageSize

        'reteriving sort order from viewstate
        faultSearchBO.SortOrder = DirectCast(ViewState(ApplicationConstants.CustFaultSortOrderViewState), String)
        Dim faultMngr As FaultManager = New FaultManager()
        'passing reference of dataset and passing faultSearchBO to GetFaultSearchResults function
        'of EnquiryManager
        faultDS = faultMngr.GetFaultSearchData(faultSearchBO)
        'storing number of rows in the result
        ViewState("NumberofRows") = faultDS.Tables(0).Rows.Count

        ''Check for exception 
        Dim exceptionFlag As Boolean = faultSearchBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

    End Sub

    Public Sub GetDataPageFaultTransaction(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef faultTransDS As DataSet, ByVal faultTransactionLogBO As FaultTransactionLogBO)

        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.FaultTransactionLogSortByViewState) Is Nothing) Then
            faultTransactionLogBO.SortBy = CType(ViewState(ApplicationConstants.FaultTransactionLogSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use equirylogid as a default
            faultTransactionLogBO.SortBy = ApplicationConstants.FaultTransactionLogSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        faultTransactionLogBO.PageIndex = pageIndex
        faultTransactionLogBO.RowCount = pageSize

        'reteriving sort order from viewstate
        faultTransactionLogBO.SortOrder = DirectCast(ViewState(ApplicationConstants.FaultTransactionLogSortOrderViewState), String)
        Dim faultMngr As FaultManager = New FaultManager()
        'passing reference of dataset and passing faultSearchBO to GetFaultSearchResults function
        'of EnquiryManager
        faultTransDS = faultMngr.GetFaultTransactionlogSearchData(faultTransactionLogBO)
        'storing number of rows in the result
        ViewState("NumberofRows") = faultTransDS.Tables(0).Rows.Count

        ''Check for exception 
        Dim exceptionFlag As Boolean = faultTransactionLogBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

    End Sub

    Public Sub GetDataPageFaultPriority(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef priorityDs As DataSet, ByVal priorityBO As PriorityBO)

        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.FaultPrioritySortByViewState) Is Nothing) Then
            priorityBO.SortBy = CType(ViewState(ApplicationConstants.FaultPrioritySortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use equirylogid as a default
            priorityBO.SortBy = ApplicationConstants.FaultPrioritySortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        priorityBO.PageIndex = pageIndex
        priorityBO.RowCount = pageSize

        'reteriving sort order from viewstate
        priorityBO.SortOrder = DirectCast(ViewState(ApplicationConstants.FaultPrioritySortOrderViewState), String)
        Dim faultMngr As FaultManager = New FaultManager()
        'passing reference of dataset and passing faultSearchBO to GetFaultSearchResults function
        'of EnquiryManager
        faultMngr.GetFaultPriorities(priorityDs, priorityBO)
        'storing number of rows in the result
        ViewState("NumberofRows") = priorityDs.Tables(0).Rows.Count

        ''Check for exception 
        Dim exceptionFlag As Boolean = priorityBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

    End Sub
#End Region

#Region "Method to Add New Fault"

    Private Sub AddNewFault()
        Dim objFault As FaultManager = New FaultManager()
        Dim faultBO As FaultBO = New FaultBO()
        Try
            ''Get Values
            faultBO.ElementID = CType(ddlElementFault.SelectedValue, Int32)
            faultBO.PriorityID = CType(ddlPriorityFault.SelectedValue, Int32)
            faultBO.Description = txtDescription.Text
            faultBO.NetCost = CType(txtNet.Text, Double)
            faultBO.Vat = CType(txtVat.Text, Double)
            faultBO.Gross = CType(txtGross.Text, Double)
            If (ddlVat.Items.Count > 0) Then
                If ddlVat.SelectedValue = "" Then
                    faultBO.VatRateID = Nothing
                Else
                    faultBO.VatRateID = CType(ddlVat.SelectedValue, Int32)
                End If
            Else
                faultBO.VatRateID = 0
            End If
            faultBO.EffectFrom = txtDate.Text
            faultBO.Recharge = IIf(rdbRechargeYes.Checked = True, 1, 0)
            faultBO.Recharge = IIf(rdbRechargeNo.Checked = True, 0, 1)
            faultBO.PreInspection = IIf(rdbPreYes.Checked = True, 1, 0)
            faultBO.PreInspection = IIf(rdnPreNo.Checked = True, 0, 1)
            faultBO.StockConditionItem = IIf(rdbStockYes.Checked = True, 1, 0)
            faultBO.StockConditionItem = IIf(rdbStockNo.Checked = True, 0, 1)
            faultBO.FaultActive = IIf(rdbFaultYes.Checked = True, 1, 0)
            faultBO.FaultActive = IIf(rdbFaultNo.Checked = True, 0, 1)
            faultBO.FaultAction = 1
            faultBO.SubmitDate = UtilityFunctions.FormatDate(System.DateTime.Now.ToShortDateString())
            faultBO.UserID = CType(ASPSession("USERID"), Integer)
            'faultBO.UserID = 65
            objFault.AddNewFault(faultBO)
            If faultBO.IsFlagStatus = True Then
                PopulateSearchResults()
                MdlPopup_AddFault.Hide()
            End If


        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Method to Amend a Fault"

    Private Sub AmendFault()
        Dim objFault As FaultManager = New FaultManager()
        Dim faultBO As FaultBO = New FaultBO()
        Try
            ''Get Values
            faultBO.FaultID = CType(lblFaultID.Text, Int32)
            faultBO.PriorityID = CType(ddlPriorityAmendFault.SelectedValue, Int32)
            faultBO.Description = txtAmendFaultDesc.Text
            faultBO.NetCost = CType(txtAmendNet.Text, Double)
            faultBO.Vat = CType(txtAmendVat.Text, Double)
            faultBO.Gross = CType(txtAmendGross.Text, Double)
            If (ddlAmendVat.Items.Count > 0) Then
                faultBO.VatRateID = CType(ddlAmendVat.SelectedValue, Int32)
            Else
                faultBO.VatRateID = 0
            End If
            faultBO.EffectFrom = txtAmendDate.Text
            faultBO.Recharge = IIf(rdbAmendRechargeYes.Checked = True, 1, 0)
            faultBO.Recharge = IIf(rdbAmendRechargeNo.Checked = True, 0, 1)
            faultBO.PreInspection = IIf(rdbAmendPreYes.Checked = True, 1, 0)
            faultBO.PreInspection = IIf(rdbAmendPreNo.Checked = True, 0, 1)
            'faultBO.PostInspection = IIf(rdbAmendPostYes.Checked = True, 1, 0)
            'faultBO.PostInspection = IIf(rdbAmendPostNo.Checked = True, 0, 1)
            faultBO.StockConditionItem = IIf(rdbAmendStockYes.Checked = True, 1, 0)
            faultBO.StockConditionItem = IIf(rdbAmendStockNo.Checked = True, 0, 1)
            faultBO.FaultActive = IIf(rdbAmendFaultYes.Checked = True, 1, 0)
            faultBO.FaultActive = IIf(rdbAmendFaultNo.Checked = True, 0, 1)
            faultBO.UserID = CType(ASPSession("USERID"), Integer)
            'faultBO.UserID = 65
            faultBO.SubmitDate = UtilityFunctions.FormatDate(System.DateTime.Now.ToShortDateString())

            objFault.AmendFault(faultBO)
            If faultBO.IsFlagStatus = True Then
                PopulateSearchResults()
                MdlPopup_AmendFault.Hide()
            End If

        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message
        End Try
    End Sub

#End Region

#Region "Method to Amend an Element Name"

    Private Sub AmendElementName()
        Dim objFault As FaultManager = New FaultManager()
        Dim elementBO As ElementBO = New ElementBO()
        Try
            ''Get Values
            elementBO.ElementID = CType(lblElementID.Text, Int32)
            elementBO.ElementName = CType(txtElementName.Text, String)
            objFault.AmendElementName(elementBO)
            If elementBO.IsFlagStatus = True Then
                GetElementAddFaultLookUpValues(ddlAreaFault.SelectedValue.ToString(), "@AreaID")
                ddlElementFault.SelectedValue = lblElementID.Text
                updPnlAddFault.Update()
                'updatePnlEditElement.Visible = False
                ModalPopup_EditElementName.Hide()
                MdlPopup_AddFault.Show()
            Else
                ModalPopup_EditElementName.Hide()
                MdlPopup_AddFault.Show()
                'updatePnlEditElement.Visible = False

            End If

        Catch ex As Exception
            elementBO.IsExceptionGenerated = True
            elementBO.ExceptionMsg = ex.Message
        End Try
    End Sub

#End Region

#Region "Method to Add/Amend Priority"
    Private Sub AddAmendPriority()
        Dim objFault As FaultManager = New FaultManager()
        Dim priorityBO As PriorityBO = New PriorityBO()
        Try
            If (txtPriorityDesc.Text = "") Then
                lblPriorityErrorMessage.Text = "Please enter priority name"
            ElseIf txtTargetTime.Text = "" Then
                lblPriorityErrorMessage.Text = "Please enter Target Time"
            Else
                If (lblPriorityID.Text = "") Then
                    priorityBO.PriorityID = 0
                Else

                    priorityBO.PriorityID = CType(lblPriorityID.Text, Int32)

                End If

                priorityBO.PriorityName = txtPriorityDesc.Text
                priorityBO.ResponseTime = CType(txtTargetTime.Text, Int32)
                priorityBO.Days = IIf(ddlTargetTime.SelectedValue = "0", False, True)
                priorityBO.Hours = IIf(ddlTargetTime.SelectedValue = "0", True, False)
                priorityBO.Status = IIf(rdbActive.Checked = True, True, False)

                objFault.AddAmendPriority(priorityBO)

                If priorityBO.IsFlagStatus = True Then
                    Me.GetFaultPrioritySearchRecords()
                    lblPriorityID.Text = "0"
                    ClearPriorityControls()
                    lblPriorityErrorMessage.Text = "Priority has been saved successfully"
                    updatePanel_lblPriorityErroMessage.Update()
                End If
            End If

        Catch ex As Exception
            priorityBO.IsExceptionGenerated = True
            priorityBO.ExceptionMsg = ex.Message
            lblPriorityID.Text = "0"
            lblPriorityErrorMessage.Text = "Priority could not be saved"
            ClearPriorityControls()
        End Try

    End Sub
#End Region

#Region "Events"

#Region "ddl Index Changed"

    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex <> 0 Then
            GetAreaLookUpValues(ddlLocation.SelectedValue.ToString, "@LocationID")
        Else
            ddlArea.Items.Clear()
            ddlArea.Items.Add(New ListItem("Please select", ""))

        End If

    End Sub

    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlArea.SelectedIndex <> 0 Then
            GetElementLookUpValues(ddlArea.SelectedValue.ToString, "@AreaID")
        Else
            ddlElement.Items.Clear()
            ddlElement.Items.Add(New ListItem("Please select", ""))
        End If
    End Sub

    Protected Sub ddlLocationFault_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlLocationFault.SelectedIndex <> 0 Then
            GetAreaAddFaultLookUpValues(ddlLocationFault.SelectedValue.ToString, "@LocationID")
        Else
            ddlAreaFault.Items.Clear()
            ddlAreaFault.Items.Add(New ListItem("Please select", ""))

        End If

    End Sub

    Protected Sub ddlAreaFault_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlAreaFault.SelectedIndex <> 0 Then
            GetElementAddFaultLookUpValues(ddlAreaFault.SelectedValue.ToString, "@AreaID")
            'btnEdit.Enabled = True
        Else
            ddlElementFault.Items.Clear()
            ddlElementFault.Items.Add(New ListItem("Please select", ""))
            btnEdit.Enabled = False

        End If
    End Sub

    Protected Sub ddlPriorityFault_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GetPriorityResponseTime(ddlPriorityFault.SelectedValue.ToString(), lblResponseTime)
    End Sub

    Protected Sub ddlAmendPriorityFault_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GetPriorityResponseTime(ddlPriorityAmendFault.SelectedValue.ToString(), lblAmendResponseTime)
    End Sub

    Protected Sub ddlElementFault_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlElementFault.SelectedValue <> "0" And ddlElementFault.SelectedValue <> "" And ddlElementFault.SelectedValue <> "-1" Then
            btnEdit.Enabled = True
        Else
            btnEdit.Enabled = False
        End If
    End Sub

    Protected Sub ddlVat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateVatValues("Vat")
    End Sub

    Protected Sub ddlAmendVat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateVatValues("AmendVat")

    End Sub

    Protected Sub ddlFaultType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlFaultType.SelectedIndexChanged
        If ddlFaultType.SelectedIndex = 2 Then
            Me.ShowFaultManagement()
        Else
            If (ddlFaultType.SelectedIndex = 0) Then
                Me.ShowAuditTrail()
            ElseIf (ddlFaultType.SelectedIndex = 1) Then
                Response.Redirect("~/secure/reports/cancelled_faults.aspx")
            ElseIf (ddlFaultType.SelectedIndex = 3) Then
                Me.ShowPrioritySettings()
            ElseIf (ddlFaultType.SelectedIndex = 4) Then
                Me.ShowPricingControl()
            ElseIf (ddlFaultType.SelectedIndex = 5) Then
                Response.Redirect("~/secure/faultlocator/repair_list_management.aspx")
            ElseIf (ddlFaultType.SelectedIndex = 6) Then
                Response.Redirect("~/secure/faultlocator/fault_locator.aspx")

            End If
        End If
    End Sub

    Protected Sub ddlAuditLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlAuditLocation.SelectedIndex <> 0 Then
            GetAreaAuditLookUpValues(ddlAuditLocation.SelectedValue.ToString(), "@LocationID")
        Else
            ddlAuditArea.Items.Clear()
            ddlAuditArea.Items.Add(New ListItem("Please select", ""))
            ddlElement.Items.Clear()
            ddlElement.Items.Add(New ListItem("Please select", ""))
            UpdatePanel_AuditLoadddlElement.Update()
        End If

    End Sub

    Protected Sub ddlAuditArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlAuditArea.SelectedIndex <> 0 Then
            GetElementAuditLookUpValues(ddlAuditArea.SelectedValue.ToString(), "@AreaID")
        Else
            ddlElement.Items.Clear()
            ddlElement.Items.Add(New ListItem("Please select", ""))
        End If



    End Sub

    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (ddlTeam.SelectedIndex <> 0) Then
            GetUserAuditLookUpValues(ddlTeam.SelectedValue.ToString(), "@TeamId")
        Else
            ddlUser.Items.Clear()
            ddlUser.Items.Add(New ListItem("Please select", ""))
        End If

    End Sub

    Protected Sub ddlPricingControlLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlPricingControlLocation.SelectedIndex <> 0 Then
            GetAreaPricingControlLookUpValues(ddlPricingControlLocation.SelectedValue.ToString, "@LocationID")
            UpdatePanel_LoadPricingControlArea.Update()
        Else
            ddlPricingControlArea.Items.Clear()
            ddlPricingControlArea.Items.Add(New ListItem("Select All", ""))
            UpdatePanel_LoadPricingControlArea.Update()
            ddlPricingControlElement.Items.Clear()
            ddlPricingControlElement.Items.Add(New ListItem("Select All", ""))
            UpdatePanel_LoadPricingControlElement.Update()
            ddlPricingControlDescription.Items.Clear()
            ddlPricingControlDescription.Items.Add(New ListItem("Select All", ""))
            UpdatePanel_PricingControlDescription.Update()
            txtPricingControlNet.Text = ""
            txtPricingControlVatRate.Text = ""
            txtPricingControlVat.Text = ""
            txtPricingControlGross.Text = ""
            txtvatRateHidden.Text = ""
            txtVatHidden.Text = ""
            UpdatePanel_PriceControl.Update()

        End If
    End Sub

    Protected Sub ddlPricingControlArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ddlPricingControlArea.SelectedIndex <> 0 Then
            GetElementPricingControlLookUpValues(ddlPricingControlArea.SelectedValue.ToString, "@AreaID")
        Else
            ddlPricingControlElement.Items.Clear()
            ddlPricingControlElement.Items.Add(New ListItem("Select All", ""))
            ddlPricingControlDescription.Items.Clear()
            ddlPricingControlDescription.Items.Add(New ListItem("Select All", ""))
            UpdatePanel_PricingControlDescription.Update()
            txtPricingControlNet.Text = ""
            txtPricingControlVatRate.Text = ""
            txtPricingControlVat.Text = ""
            txtPricingControlGross.Text = ""
            txtvatRateHidden.Text = ""
            txtVatHidden.Text = ""
            UpdatePanel_PriceControl.Update()
        End If
    End Sub

    Protected Sub ddlPricingControlElement_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ddlPricingControlElement.SelectedIndex <> 0 Then
            GetDescriptionLookUpValues(ddlPricingControlElement.SelectedValue.ToString, "@ElementID")
        Else
            ddlPricingControlDescription.Items.Clear()
            ddlPricingControlDescription.Items.Add(New ListItem("Select All", ""))
            txtPricingControlNet.Text = ""
            txtPricingControlVatRate.Text = ""
            txtPricingControlVat.Text = ""
            txtPricingControlGross.Text = ""
            txtvatRateHidden.Text = ""
            txtVatHidden.Text = ""
            UpdatePanel_PriceControl.Update()
        End If
    End Sub

    Protected Sub ddlPricingControlDescription_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ddlPricingControlDescription.SelectedIndex <> 0 Then
            GetPricingControlData(ddlPricingControlDescription.SelectedValue.ToString, "@FaultID")
        Else
            txtPricingControlNet.Text = ""
            txtPricingControlVatRate.Text = ""
            txtPricingControlVat.Text = ""
            txtPricingControlGross.Text = ""
            txtvatRateHidden.Text = ""
            txtVatHidden.Text = ""
            UpdatePanel_PriceControl.Update()
        End If
    End Sub

#End Region

#Region "Grid Events"
    Protected Sub GVFault_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVFault.PageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreResultSummary()
        Dim faultDS As DataSet = New DataSet()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.CustFaultIsSearchViewState), Boolean) = True Then
            GetSearchResults()
        Else
            BindFaultGrid()
        End If

    End Sub

    Protected Sub GVFault_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub GVFault_PageIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub GVFault_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.CustFaultSortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVFault.PageIndex = ApplicationConstants.CustFaultInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.CustFaultSortOrderViewState), String) = ApplicationConstants.CustFaultManagDESCSortOrder Then
            ViewState(ApplicationConstants.CustFaultSortOrderViewState) = ApplicationConstants.CustFaultManagASECSortOrder
        Else
            ViewState(ApplicationConstants.CustFaultSortOrderViewState) = ApplicationConstants.CustFaultManagDESCSortOrder
        End If

        'populating gridview with new results
        If DirectCast(ViewState(ApplicationConstants.CustFaultIsSearchViewState), Boolean) = True Then
            GetSearchResults()
        Else
            BindFaultGrid()
        End If
    End Sub

    ''Audit Trail Grid
    Protected Sub GVAuditTrail_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVAuditTrail.PageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreFaultTransactionLogResultSummary()
        Dim faultDS As DataSet = New DataSet()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.FaultTransactionLogIsSearchViewState), Boolean) = True Then
            GetFaultTransactionLogSearchResults()
        Else
            BindFaultTransactionLogGrid()
        End If

    End Sub

    Protected Sub GVAuditTrail_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.FaultTransactionLogSortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVAuditTrail.PageIndex = ApplicationConstants.FaultTransactionLogInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.FaultTransactionLogSortOrderViewState), String) = ApplicationConstants.FaultTransactionLogDESCSortOrder Then
            ViewState(ApplicationConstants.FaultTransactionLogSortOrderViewState) = ApplicationConstants.FaultTransactionLogASECSortOrder
        Else
            ViewState(ApplicationConstants.FaultTransactionLogSortOrderViewState) = ApplicationConstants.FaultTransactionLogDESCSortOrder
        End If

        'populating gridview with new results
        If DirectCast(ViewState(ApplicationConstants.FaultTransactionLogIsSearchViewState), Boolean) = True Then
            GetFaultTransactionLogSearchResults()
        Else
            BindFaultTransactionLogGrid()
        End If
    End Sub

    ''Priority Settings
    Protected Sub GVProiritySetting_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVProiritySetting.PageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreFaultPrioritiesResultSummary()
        Dim faultDS As DataSet = New DataSet()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.FaultPriorityIsSearchViewState), Boolean) = True Then
            GetFaultPrioritySearchResults()
        Else
            BindFaultPriorityGrid()
        End If
    End Sub

    Protected Sub GVProiritySetting_SelectedIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs)

    End Sub

    Protected Sub GVProiritySetting_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.FaultPrioritySortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVProiritySetting.PageIndex = ApplicationConstants.FaultPriorityInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.FaultPrioritySortOrderViewState), String) = ApplicationConstants.FaultPriorityDESCSortOrder Then
            ViewState(ApplicationConstants.FaultPrioritySortOrderViewState) = ApplicationConstants.FaultPriorityASECSortOrder
        Else
            ViewState(ApplicationConstants.FaultPrioritySortOrderViewState) = ApplicationConstants.FaultPriorityDESCSortOrder
        End If

        'populating gridview with new results
        If DirectCast(ViewState(ApplicationConstants.FaultPriorityIsSearchViewState), Boolean) = True Then
            GetFaultPrioritySearchResults()
        Else
            BindFaultPriorityGrid()
        End If
    End Sub

#End Region

#Region "Button Click"

    Protected Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        PopulateSearchResults()

    End Sub

    Protected Sub btnAddFault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFault.Click
        GetLocationAddFaultLookUpValues()
        GetPriorityAddFaultLookUpValues()
        btnEdit.Enabled = False
        GetControlsReset()
        updPnlAddFault.Update()
        MdlPopup_AddFault.Show()

    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateSearchResults()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GetControlsReset()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblAddErrorMesg.Text = ""
        UpdatePanel_errorMesg.Update()

        Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")
        If rdbRechargeNo.Checked <> True And Not (re.IsMatch(txtNet.Text)) Then
            lblAddErrorMesg.Text = "Please Enter Correct Net Amount or atleast 0"
            UpdatePanel_errorMesg.Update()
            'txtNet.Text = "" Or 
        Else
            If ddlLocationFault.SelectedValue = "" Or ddlLocationFault.SelectedValue = "0" Or ddlLocationFault.SelectedValue = "-1" Then
                lblAddErrorMesg.Text = "Please Select A Location"
                UpdatePanel_errorMesg.Update()
            Else
                If ddlAreaFault.SelectedValue = "" Or ddlAreaFault.SelectedValue = "0" Or ddlAreaFault.SelectedValue = "-1" Then
                    lblAddErrorMesg.Text = "Please Select an Area"
                    UpdatePanel_errorMesg.Update()
                Else
                    If ddlElementFault.SelectedValue = "" Or ddlElementFault.SelectedValue = "0" Or ddlElementFault.SelectedValue = "-1" Then
                        lblAddErrorMesg.Text = "Please Select an Element"
                        UpdatePanel_errorMesg.Update()

                    Else
                        If txtDescription.Text = "" Then

                            lblAddErrorMesg.Text = "Please Enter Description For The Fault"
                            UpdatePanel_errorMesg.Update()
                        Else
                            If ddlPriorityFault.SelectedValue = "" Or ddlPriorityFault.SelectedValue = "0" Or ddlPriorityFault.SelectedValue = "-1" Then
                                lblAddErrorMesg.Text = "Please Select a Priority from the list"
                                UpdatePanel_errorMesg.Update()                              
                            Else
                                If rdbRechargeYes.Checked = True And (txtNet.Text = "0" Or txtNet.Text = "0.00" Or txtNet.Text = "" Or txtNet.Text = "0.0") Then
                                    lblAddErrorMesg.Text = "Please Enter Net Cost or Uncheck Recharge"
                                    UpdatePanel_errorMesg.Update()
                                Else
                                    lblAddErrorMesg.Text = ""
                                    UpdatePanel_errorMesg.Update()
                                    AddNewFault()
                                End If


                            End If
                        End If
                    End If
                End If


            End If
        End If

    End Sub

    Protected Sub btnAmendClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnAmendReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnAmendSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Me.updPnlAmendFault.Update()
        Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")
        If rdbAmendRechargeNo.Checked <> True And (txtAmendNet.Text = "" Or Not (re.IsMatch(txtAmendNet.Text))) Then
            lblAmendErrorMesg.Text = "Please Enter Correct Net Amount or atleast 0"
            UpdatePanel_AmenderrorMesg.Update()
        Else
            If ddlPriorityAmendFault.SelectedValue = "" Or ddlPriorityAmendFault.SelectedValue = "0" Or ddlPriorityAmendFault.SelectedValue = "-1" Then
                lblAmendErrorMesg.Text = "Please Select a Priority from the list"
                UpdatePanel_AmenderrorMesg.Update()
            Else
                If rdbAmendRechargeYes.Checked = True And (txtAmendNet.Text = "0" Or txtAmendNet.Text = "0.00" Or txtAmendNet.Text = "" Or txtAmendNet.Text = "0.0") Then

                    lblAmendErrorMesg.Text = "Please Enter Net Cost or Uncheck Recharge"
                    UpdatePanel_AmenderrorMesg.Update()                
                Else
                    lblAmendErrorMesg.Text = ""
                    UpdatePanel_AmenderrorMesg.Update()
                    AmendFault()
                    updPnlAmendFault.Update()
                End If
            End If
        End If


    End Sub

    Protected Sub btnAmend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'UpdatePanel2.Update()
        GetPriorityAmendFaultLookUpValues()
        Dim btn As Button = DirectCast(sender, Button)
        Dim faultID As String = btn.CommandArgument.ToString()
        Me.GetFaultValues(CType(faultID, Int32))
        Me.updPnlAmendFault.Update()
        MdlPopup_AmendFault.Show()

    End Sub

    Protected Sub btnAmendPriority_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        lblPriorityErrorMessage.Text = ""
        updatePanel_lblPriorityErroMessage.Update()
        Dim btn As Button = DirectCast(sender, Button)
        Dim priorityID As String = btn.CommandArgument.ToString()
        Me.GetPriorityValues(CType(priorityID, Int32))
        Me.UpdatePanel_AddAmendPriority.Update()
    End Sub

    Protected Sub btnPrioritySave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AddAmendPriority()
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        populateElementField()

        'updatePnlEditElement.Visible = True
        ModalPopup_EditElementName.Show()
    End Sub

    Protected Sub btnUpdateName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        AmendElementName()
    End Sub

    Protected Sub btnAuditSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateFaultTransactionLogSearchResults()
    End Sub

    Protected Sub btnPriorityReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ClearPriorityControls()
        lblPriorityErrorMessage.Text = ""
        updatePanel_lblPriorityErroMessage.Update()
    End Sub

    Protected Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnPricingControlReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ClearPricingControls()
    End Sub

    Protected Sub btnPricingControlSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        SavePriceControlData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MdlPopup_AddFault.Show()
    End Sub

#End Region

#Region "Radio Button Checked Change"
    Protected Sub rdbRechargeYes_CheckedChanged2(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub rdbRechargeNo_CheckedChanged1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
#End Region

#Region "Text Changed"

    Protected Sub txtNet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateVatValues("Vat")
        'txtGross.Text = Double.Parse(txtNet.Text) + ((Double.Parse(txtNet.Text) * Double.Parse(txtVat.Text)) / 100)
        'updatePanel_Gross.Update()

    End Sub

    Protected Sub txtVat_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtGross.Text = (Double.Parse(txtNet.Text).ToString() + Double.Parse(txtVat.Text)).ToString()
        updatePanel_Gross.Update()
    End Sub

    Protected Sub txtAmendNet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateVatValues("AmendVat")

    End Sub

    Protected Sub txtAmendVat_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtAmendGross.Text = (Double.Parse(txtAmendNet.Text) + Double.Parse(txtAmendVat.Text)).ToString()
        updatePanel_AmendGross.Update()
    End Sub

    Protected Sub txtPricingControlInflationValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        resetByInflation()
    End Sub

#End Region

#Region "Show Audit Trail"
    Private Sub ShowAuditTrail()

        GetLocationAuditFaultLookUpValues()
        GetTeamAuditLookUpValues()
        UpdatePanel_Search.Visible = False
        UpdatePanel_AuditSearch.Visible = True
        UpdatePanel_AuditSearch.Update()
        UpdatePanel_LoadGrid.Visible = False
        GetFaultTransactionLogSearchRecords()
        UpdatePanel_AuditTrail.Visible = True
        UpdatePanel_AuditTrail.Update()
        UpdatePanel_PrioritySettings.Visible = False
        UpdatePanel_AddAmendPriority.Visible = False
        ViewState(ApplicationConstants.FaultPriorityIsEdit) = False
        UpdatePanel_PriceControl.Visible = False
        PopulateSearchResults()
        lblSearch.Visible = True
        lblErrorMessage.Text = ""
        lblPricingError.Text = ""
        lblPriorityErrorMessage.Text = ""
        updatePanel_lblPriorityErroMessage.Update()

    End Sub
#End Region

#Region "Show Fault Management"
    Private Sub ShowFaultManagement()

        GetLocationLookUpValues()
        GetPriorityLookUpValues()
        UpdatePanel_Search.Visible = True
        UpdatePanel_Search.Update()
        UpdatePanel_AuditSearch.Visible = False
        UpdatePanel_PrioritySettings.Visible = False
        UpdatePanel_LoadGrid.Visible = True
        UpdatePanel_AuditTrail.Visible = False
        UpdatePanel_AddAmendPriority.Visible = False
        ViewState(ApplicationConstants.FaultPriorityIsEdit) = False
        UpdatePanel_PriceControl.Visible = False        
        lblSearch.Visible = True
        lblErrorMessage.Text = ""
        lblPricingError.Text = ""
        lblPriorityErrorMessage.Text = ""
        updatePanel_lblPriorityErroMessage.Update()

    End Sub
#End Region

#Region "Show Priority Settings"
    Private Sub ShowPrioritySettings()

        GetFaultPrioritySearchRecords()
        UpdatePanel_Search.Visible = False
        UpdatePanel_Search.Update()
        UpdatePanel_AuditSearch.Visible = False
        UpdatePanel_LoadGrid.Visible = False
        UpdatePanel_AuditTrail.Visible = False
        UpdatePanel_PrioritySettings.Visible = True
        UpdatePanel_AddAmendPriority.Visible = True
        ViewState(ApplicationConstants.FaultPriorityIsEdit) = False
        UpdatePanel_PriceControl.Visible = False        
        lblSearch.Visible = False
        lblPricingError.Text = ""
        lblPriorityErrorMessage.Text = ""
        updatePanel_lblPriorityErroMessage.Update()

    End Sub
#End Region

#Region "Show Pricing Control"
    Private Sub ShowPricingControl()

        UpdatePanel_Search.Visible = False
        UpdatePanel_Search.Update()
        UpdatePanel_AuditSearch.Visible = False
        UpdatePanel_LoadGrid.Visible = False
        UpdatePanel_AuditTrail.Visible = False
        UpdatePanel_PrioritySettings.Visible = False
        UpdatePanel_AddAmendPriority.Visible = False
        ViewState(ApplicationConstants.FaultPriorityIsEdit) = False
        UpdatePanel_PriceControl.Visible = True        
        lblSearch.Visible = False
        lblErrorMessage.Text = ""
        lblPriorityErrorMessage.Text = ""
        updatePanel_lblPriorityErroMessage.Update()
        lblPricingError.Text = ""
        ClearPricingControls()
    End Sub
#End Region
#End Region
   
    
End Class