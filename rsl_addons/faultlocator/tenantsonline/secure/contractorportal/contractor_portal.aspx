﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/ContractorPortal.Master" Title="Tenants Online :: Contractor Portal" CodeBehind="contractor_portal.aspx.vb" Inherits="tenantsonline.contractor_portal" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
      
<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
<script runat="server" >
    
    Function GetFullName(ByVal title As String, ByVal firstName As String, ByVal lastName As String) As String
        
        Return title & " " & firstName & " " & lastName
        
    End Function
    Function MakeCommandString(ByVal Name As String, ByVal Address As String, ByVal Telephone As String, ByVal CustomerNo As String, ByVal AppointmentDate As String, ByVal LastActioned As String, ByVal AppointmentNotes As String, ByVal AppointmentStageId As String, ByVal AppointmentOperativeId As String, ByVal AppointmentLetterId As String, ByVal TenancyRef As String, ByVal AppointmentID As String, ByVal AppointmentTime As String) As String
        Return Name & "," & Address & "," & Telephone & "," & CustomerNo & "," & AppointmentDate & "," & LastActioned & "," & AppointmentNotes & "," & AppointmentStageId & "," & AppointmentOperativeId & "," & AppointmentLetterId & "," & TenancyRef & "," & AppointmentID & "," & AppointmentTime
    End Function
    Function MakeCommandString(ByVal FaultLogId As String, ByVal Name As String, ByVal Address As String, ByVal LocationName As String, ByVal AreaName As String, ByVal ElementName As String, ByVal Description As String, ByVal AppointmentOperativeID As String, ByVal AppointmentID As String, ByVal title As String, ByVal lastName As String, ByVal CustomerHouseNo As String, ByVal CustomerPostCode As String, ByVal CustomerCounty As String, ByVal CustomerTownCity As String) As String
        Dim faultLocation As String = LocationName & " " & AreaName & " " & ElementName
        Return Name & "," & Address & "," & faultLocation & "," & Description & "," & AppointmentOperativeID & "," & AppointmentID & "," & FaultLogId & "," & title & "," & lastName & "," & CustomerHouseNo & "," & CustomerPostCode & "," & CustomerCounty & "," & CustomerTownCity
    End Function
    
    'MakeCommandString(Eval("FaultLogID").ToString(),Eval("Name").ToString(), Eval("CustomerAddress").ToString(),Eval("LocationName").ToString(),Eval("AreaName").ToString(),Eval("ElementName").ToString(),Eval("Description").ToString(),Eval("AppointmentOperativeID").ToString(),Eval("AppointmentID").ToString(), Eval("TitleDescription").ToString(),Eval("CustomerHouseNo").ToString(),Eval("CustomerPostCode").ToString(),
    'Eval("CustomerCounty").ToString(),Eval("CustomerTownCity").ToString())
 </script>
 <script language ="javascript" type="text/javascript" >
    function open_letter(){
     
     if(document.getElementById("<%= txtPopupAppointment.ClientID %>").value == "")
     {
        alert("Please enter appointment date");
        return false;
     }
     
     if(document.getElementById("<%= ddlPopupSelectLetter.ClientID %>").value == "")
     {
        alert("Please select letter");
        return false;
     }
     
     if(document.getElementById("<%= ddlPopupHr.ClientID %>").value == "00")
     {
        alert("Please select time ");
        return false;
     }   
    
     var tenancy_id = document.getElementById("<%= lblTenancyRefText.ClientID %>").innerHTML;
     var appointmentDate = document.getElementById("<%= txtPopupAppointment.ClientID %>").value;        
     var letterId = document.getElementById("<%= ddlPopupSelectLetter.ClientID %>").value;  
	 var AppointmentTime = document.getElementById("<%= ddlPopupHr.ClientID %>").value+":"+document.getElementById("<%= ddlPopupMin.ClientID %>").value+" "+document.getElementById("<%= ddlPopupFormat.ClientID %>").value;
	 window.open("pView_letter.asp?tenancyid="+tenancy_id+"&letterid="+letterId + "&DATE1=" +appointmentDate+" "+AppointmentTime+ "&DATE2="+appointmentDate , "_blank","width=570,height=600,left=100,top=50,scrollbars=yes");
	
	}


function printReport(someTableObject){
     var row = document.getElementById('JSHead');
     var rowPrint = document.getElementById('JSPrint');
     row.style.display = 'none';
     rowPrint.style.display = 'none';
     var printContent = document.getElementById(someTableObject);
     var windowUrl = 'about:blank';
     var uniqueName = new Date();
     var windowName = 'Print' + uniqueName.getTime();
     var printWindow = window.open(windowUrl, windowName, 'left=0,top=0,width=950,height=600');
     var thisTop ='<html><title>Print Job Sheet</title><style>Body{margin: 0px;}</style></head>';
     var aLabel='<br><center><table>'; 
     printWindow.document.write(thisTop + aLabel + printContent.innerHTML +'</table></center></body></html>');
     printWindow.document.close();
     printWindow.focus();
     printWindow.print();
     printWindow.close();
     
     row.style.display = 'block';
     rowPrint.style.display = 'block';
}

 </script>
    <div style="text-align: left">

        <table style="width: 100%; height: 100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 54px">
                    <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" Width="224px"></asp:Label></td>
                <td style="width: 100px">
               <cc2:ToolkitScriptManager ID="Registration_ToolkitScriptManager" runat="server">
               </cc2:ToolkitScriptManager>                
            </td>
            </tr>
            <tr>
                <td rowspan="2" style="width: 54px" valign="top">
                <asp:UpdatePanel ID="UpdatePanel_Search" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                <ContentTemplate>
                    <table style="width: 5%; border-right: tan thin dotted; border-top: tan thin dotted; border-left: tan thin dotted; border-bottom: tan thin dotted;" id="TABLE1" >
                        <tr>
                            <td colspan="2" style="border-bottom: thin dotted">
                                &nbsp;<table>
                                    <tr>
                                        <td style="width: 100px">
                                <asp:Label ID="lblContractorOption" runat="server" Font-Names="Arial" Font-Size="Small" Text="Select:"
                                    Width="64px" CssClass="caption"></asp:Label></td>
                                        <td style="width: 73px">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                                                <ContentTemplate>
                                <asp:DropDownList ID="ddlContractorType" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" AutoPostBack="True" CssClass="caption" OnSelectedIndexChanged="ddlContractorType_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Value="-1">Select</asp:ListItem>
                                    <asp:ListItem Value="JobSheet">Job Sheet</asp:ListItem>
                                    <asp:ListItem Value="Appointment">Appointment To Be Arranged</asp:ListItem>
                                    <asp:ListItem Value="Manage">Manage Appointment</asp:ListItem>
                                    <asp:ListItem Value="Work">Work Completion</asp:ListItem>
                                    <asp:ListItem>Invoicing</asp:ListItem>
                                    <asp:ListItem>Monitoring</asp:ListItem>
                                </asp:DropDownList>
                                              </ContentTemplate>
                                            </asp:UpdatePanel>
                                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="50" AssociatedUpdatePanelID="UpdatePanel1">
                                                <ProgressTemplate>
                                                    <img src="../../images/buttons/ajax-loader.gif" style="width: 152px; height: 24px" alt="" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblSearch" runat="server" Font-Bold="True" Font-Names="Arial" Text="Search:"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblJS" runat="server" Font-Names="Arial" Font-Size="Small" Text="JS Number" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtJS" runat="server" Width="147px" CssClass="caption"></asp:TextBox><br />
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtJS" FilterType = "Numbers">
                                </cc2:FilteredTextBoxExtender>
                                <cc2:TextBoxWatermarkExtender id="TextBoxWatermarkExtender3" runat="server" targetcontrolid="txtJS" watermarktext = "Enter JS Number">
                                </cc2:TextBoxWatermarkExtender>
                                
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblLocation" runat="server" Text="Location" Font-Names="Arial" Font-Size="Small" Width="64px" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px">
                            <asp:DropDownList ID="ddlLocation" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True">
                            </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblArea" runat="server" Text="Area" Font-Names="Arial" Font-Size="Small" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px">
                            <asp:DropDownList ID="ddlArea" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged">
                            </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblElement" runat="server" Text="Element" Font-Names="Arial" Font-Size="Small" Width="56px" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px">
                            <asp:DropDownList ID="ddlElement" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True">
                            </asp:DropDownList>&nbsp;
                        </tr>
                        <tr>
                            <td style="width: 100px" valign="top">
                                <asp:Label ID="lblUser" runat="server" Text="User" Font-Names="Arial" Font-Size="Small" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px" valign="top">
                                <asp:DropDownList ID="ddlTeam" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True" OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                </td>
                            <td style="width: 145px" valign="top">
                            <asp:DropDownList ID="ddlUser" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True">
                            </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="middle">
                                <hr style="border-right: #d2b48c thin dotted; border-top: #d2b48c thin dotted;
                                    border-left: #d2b48c thin dotted; border-bottom: #d2b48c thin dotted" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblPatch" runat="server" Font-Names="Arial" Font-Size="Small" Text="Patch" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px"><asp:DropDownList ID="ddlPatch" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True">
                            </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblScheme" runat="server" Font-Names="Arial" Font-Size="Small" Text="Scheme" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:DropDownList ID="ddlScheme" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblPostCode" runat="server" Font-Names="Arial" Font-Size="Small" Text="Postcode" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtPostCode" runat="server" Width="147px" CssClass="caption"></asp:TextBox><br />
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtPostCode" FilterType = "Custom,Numbers,UppercaseLetters,LowercaseLetters" ValidChars = " /.-">
                                </cc2:FilteredTextBoxExtender>
                                <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtPostCode" WatermarkText = "Enter Post Code">
                                </cc2:TextBoxWatermarkExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblDue" runat="server" Font-Names="Arial" Font-Size="Small" Text="Due Date" CssClass="caption" Width="64px"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtDueDate" runat="server" Width="112px" CssClass="caption" BackColor="Transparent" Font-Size="Smaller" ForeColor="#004040" Enabled="False"></asp:TextBox>
                                <asp:ImageButton
                                    ID="imgCalenderBtn" runat="server" ImageUrl="~/images/buttons/Calendar_button.png" />
                                <cc2:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID = "txtDueDate" PopupButtonID = "imgCalenderBtn" Format = "dd/MM/yyyy">
                                </cc2:CalendarExtender>
                                <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtDueDate" WatermarkText = "Pick Date">
                                </cc2:TextBoxWatermarkExtender>
                               
                             </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblPriority" runat="server" Font-Names="Arial" Font-Size="Small" Text="Priority" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:DropDownList ID="ddlPriority" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblStatus" runat="server" Font-Names="Arial" Font-Size="Small" Text="Status" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px">
                                <asp:DropDownList ID="ddlStatus" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="middle">
                                <hr style="border-right: #d2b48c thin dotted; border-top: #d2b48c thin dotted;
                                    border-left: #d2b48c thin dotted; border-bottom: #d2b48c thin dotted" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px" valign="top">
                                <asp:Label ID="lblStage" runat="server" Font-Names="Arial" Font-Size="Small" Text="Stage" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px" valign="top">
                                <asp:DropDownList ID="ddlStage" runat="server" Font-Names="Arial" Font-Size="Small" Width="152px" AppendDataBoundItems="True" CssClass="caption" AutoPostBack="True">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                &nbsp;</td>
                            <td style="width: 145px">
                                &nbsp;<asp:Button ID="btnInvoicingSearch" runat="server" OnClick="btnInvoicingSearch_Click"
                                    Text="Search" />
                                <asp:Button ID="btnClear" runat="server" Style="position: static" Text="Clear" OnClick="btnClear_Click" /></td>
                        </tr>
                    </table>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel_GVHeading" runat="server" ChildrenAsTriggers="False"
                        UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Label ID="lblGVHeading" runat="server" BackColor="Transparent" Font-Bold="True"
                                Font-Names="Arial" ForeColor="Black" Width="248px"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>

                
                <td style="width: 100%; text-align: left;vertical-align:top; height: 350px;">
                    <asp:UpdatePanel ID="UpdatePanel_AllPageGrids" runat="server" ChildrenAsTriggers="False"
                        UpdateMode="Conditional">
                        <ContentTemplate>
                    <asp:UpdatePanel ID="UpdatePanel_JobSheetGrid" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <cc1:PagingGridView ID="GVJobSheets" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                                BorderStyle="Dotted" BorderWidth="1px" CellPadding="2" EmptyDataText="No record exists"
                                Font-Names="Arial" Font-Size="Small" ForeColor="Black" GridLines="None" OnPageIndexChanging="GVJobSheets_PageIndexChanging" VirtualItemCount="-1" Width="86%" OnSorting="GVJobSheets_Sorting">
                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
                                    PageButtonCount="5" />
                                <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="JS No" SortExpression="FL_FAULT_LOG.FaultLogID">
                                        <headerstyle wrap="False" />
                                        <itemtemplate>
<asp:LinkButton id="GVLblJobSheetJSNumber" onclick="GVLblJobSheetJSNumber_Click1" runat="server" Text='<%# Bind("JSNumber") %>' CommandArgument='<%# Bind("JSNumber") %>' __designer:wfdid="w3"></asp:LinkButton> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Logged" InsertVisible="False" SortExpression="FL_FAULT_LOG.SubmitDate">
                                        <headerstyle horizontalalign="Left" />
                                        <itemtemplate>
<asp:Label id="GVLblJSPatch" runat="server" CssClass="cellData" Width="88px" Text='<%# Bind("SubmitDate") %>' __designer:wfdid="w4"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address" InsertVisible="False" SortExpression="P__PROPERTY.ADDRESS1">
                                        <itemtemplate>
<asp:Label id="GVlblHouseNumber" runat="server" CssClass="cellData" Width="104px" Text='<%# Bind("COMPLETEADDRESS") %>' __designer:wfdid="w5"></asp:Label>
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PostCode" InsertVisible="False" SortExpression="P__PROPERTY.POSTCODE">
                                        <itemtemplate>
<asp:Label id="GVLblJSPostCode" runat="server" CssClass="cellData" Text='<%# Bind("PostCode") %>' __designer:wfdid="w6"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Priority" InsertVisible="False" SortExpression="FL_FAULT_PRIORITY.ResponseTime">
                                        <itemtemplate>
<asp:Label id="GVLblJSPriority" runat="server" CssClass="cellData" Text='<%# Bind("PriorityTime") %>' __designer:wfdid="w6"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Due Date" InsertVisible="False" SortExpression="FL_FAULT_LOG.DueDate">
                                        <itemtemplate>
<asp:Label id="GVLblJSDueDate" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("Due").tostring()) %>' __designer:wfdid="w7"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fault Details" InsertVisible="False" SortExpression="FL_FAULT.Description">
                                        <headerstyle wrap="False" />
                                        <itemtemplate>
<asp:Label id="GVLblJSDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>' __designer:wfdid="w8"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" InsertVisible="False" SortExpression="FL_FAULT_STATUS.Description">
                                        <itemtemplate>
<asp:Label id="GVLblJSStatus" runat="server" CssClass="cellData" Text='<%# Bind("Status") %>' __designer:wfdid="w9"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Accepted">
                                        <itemtemplate>
&nbsp;<asp:CheckBox id="chkBoxAcceptSelected" runat="server" Visible='<%# Bind("FaultLogID") %>' __designer:wfdid="w10"></asp:CheckBox> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FaultLogID" ShowHeader="False" Visible="False">
                                        <itemtemplate>
<asp:Label id="lblJSFaultLogId" runat="server" Text='<%# Bind("FaultLogID") %>' __designer:wfdid="w11"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#EFF3FB" />
                                <EditRowStyle BackColor="#2461BF" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" />
                                <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                                <AlternatingRowStyle BackColor="White" Font-Names="Arial" Font-Size="Small" Wrap="True" />
                            </cc1:PagingGridView>
                            <asp:Button ID="btnJobSheetSelectAll" runat="server" Text="Select All" UseSubmitBehavior="False" />
                            <asp:Button ID="btnJobSheetAcceptSelected" runat="server" Text="Accept Selected"
                                UseSubmitBehavior="False" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="updatePanel_JbSSmry" runat="server" UpdateMode="Conditional" RenderMode="Inline" Visible="False">
                        <ContentTemplate>
                            <table id="tblJSPrint" style="width: 50%; border-right: black 2px solid; border-top: black 2px solid; border-left: black 2px solid; border-bottom: black 2px solid; background-color: #ffffff;">
                                <tr id="JSHead">
                                    <td>
                                        <asp:Image ID="BHALogo" runat="server" ImageUrl="~/images/buttons/BH-2006-RED_RGB.gif" /></td>
                                    <td align="right" style="float: right;" valign="top">
                                        <asp:ImageButton ID="btnJSPopupClose" runat="server" Height="22px" ImageUrl="~/images/closeimage.png"
                                            OnClick="btnJSPopupClose_Click" Width="22px" /></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #c00000;" colspan="2">
                                        &nbsp;<asp:Label ID="lblJobSheetSummary" runat="server" Font-Bold="True"
                                            Font-Names="Arial" Text="Job Sheet Summary" ForeColor="White"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top">
                                            <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="50%" valign="top">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td valign="top"><asp:Label ID="lblContractorLabel" runat="server" CssClass="caption" Text="Contractor"></asp:Label></td>
                                                                <td><asp:Label ID="lblContractor" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top"><asp:Label ID="lblPriorityLabel" runat="server" CssClass="caption" Text="Priority"></asp:Label></td>
                                                                <td><asp:Label ID="lblPopupPriority" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                             <tr>
                                                                <td><asp:Label ID="lblReponseTimeLabel" runat="server" CssClass="caption" Text="ResponseTime"></asp:Label></td>
                                                                <td><asp:Label ID="lblResponseTime" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                             <tr>
                                                                <td><asp:Label ID="lblJsNumberLabel" runat="server" CssClass="caption" Text="JS Number"></asp:Label></td>
                                                                <td><asp:Label ID="lblJSNumber" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblOrderDateLabel" runat="server" CssClass="caption" Text="Order Date"></asp:Label></td>
                                                                <td><asp:Label ID="lblOrderDate" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblDueDateLabel" runat="server" CssClass="caption" Text="Due Date"></asp:Label></td>
                                                                <td><asp:Label ID="lblDue1" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblAsbestorLabel" runat="server" CssClass="caption" Text="Asbestos"></asp:Label></td>
                                                                <td><asp:Label ID="lblAsbestos" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:GridView ID="gvAsbestosList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                        ForeColor="#333333" GridLines="None" CssClass="caption">
                                                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                        <Columns>
                                                                            <asp:BoundField DataField="RISK" HeaderText="Risk" />
                                                                            <asp:BoundField DataField="ELEMENTDISCRIPTION" HeaderText="Description" />
                                                                        </Columns>
                                                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="50%" valign="top">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td><asp:Label ID="lblJobSheetSummary1Name" runat="server" Text="Name" CssClass="caption"></asp:Label></td>
                                                                <td><asp:Label ID="lblTenantName" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblJobSheetSummary2Add" runat="server" Text="Address" CssClass="caption"></asp:Label></td>
                                                                <td><asp:Label ID="lblAddressPopup" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td nowrap="nowrap"><asp:Label ID="lblHouseNo" runat="server" CssClass="caption" Text="House Number"></asp:Label></td>
                                                                <td><asp:Label ID="lblHouseNum" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblPostCodee" runat="server" CssClass="caption" Text="Post Code"></asp:Label></td>
                                                                <td><asp:Label ID="lblPostCode1" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblJobSheetSummary3Town" runat="server" Text="Town" CssClass="caption"></asp:Label></td>
                                                                <td><asp:Label ID="lblTown" runat="server" CssClass="caption" Text=""></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblJobSheetSummary5Cntry" runat="server" Text="County" CssClass="caption"></asp:Label></td>
                                                                <td><asp:Label ID="lblCountry" runat="server" CssClass="caption" Text=""></asp:Label></td>
                                                            </tr>
                                                             <tr>
                                                                <td><asp:Label ID="lblTelephoneLabel" runat="server" CssClass="caption" EnableViewState="False" Text="Telephone"></asp:Label></td>
                                                                <td><asp:Label ID="lblTelephonePopup" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                             <tr>
                                                                <td><asp:Label ID="lblMobileLabel" runat="server" CssClass="caption" EnableViewState="False" Text="Mobile"></asp:Label></td>
                                                                <td><asp:Label ID="lblMobile" runat="server" CssClass="caption" Text=""></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td><asp:Label ID="lblEmailLabel" runat="server" CssClass="caption" Text="Email"></asp:Label></td>
                                                                <td><asp:Label ID="lblEmail" runat="server" CssClass="caption" Text=""></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                     </td>
                                                </tr>
                                            </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="grdCustomerReportedFaults" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CellPadding="4" EmptyDataText="No record found" Font-Names="Arial" Font-Size="Small"
                                            ForeColor="#333333" GridLines="None" PageSize="1" Width="600px">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Area/Item/Element" InsertVisible="False" SortExpression="CreationDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLocationName" runat="server" CssClass="caption" Text='<%# Bind("LocationName") %>'></asp:Label>_<asp:Label
                                                            ID="lblAreaName" runat="server" CssClass="caption" Text='<%# Bind("AreaName") %>'></asp:Label>_<asp:Label
                                                                ID="lblElementName" runat="server" CssClass="caption" Text='<%# Bind("ElementName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" InsertVisible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDescription" runat="server" CssClass="caption" Text='<%# Bind("Description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Qty" InsertVisible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQuantity" runat="server" CssClass="caption" Text='<%# Bind("Quantity") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Priority" InsertVisible="False">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExpectedTime" runat="server" CssClass="caption" Text='<%# Bind("FResponseTime") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Recharge Cost">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFRecharge" runat="server" CssClass="caption" Text='<%# Bind("FRecharge") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings Visible="False" />
                                            <HeaderStyle BackColor="#C00000" BorderStyle="None" ForeColor="White" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100px" valign="top">
                                                    <asp:Label ID="lblProblemDays" runat="server" CssClass="caption" Width="248px"></asp:Label></td>
                                                <td style="width: 265px; border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;" valign="top" rowspan="3">
                                                    <asp:Label ID="lblNotesLabel" runat="server" CssClass="caption"></asp:Label>
                                                    <asp:Label
                                                        ID="lblNotes" runat="server"></asp:Label>&nbsp; &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px">
                                                    <asp:Label ID="lblRecuringProblem" runat="server" CssClass="caption" Width="248px"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px; height: 21px;">
                                                    <asp:Label ID="lblCommunalArea" runat="server" CssClass="caption" Width="248px"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 5px">
                                        <asp:Label ID="lblJSPreferredContactTime" runat="server" CssClass="caption" Text="Customer has any preferred contact time or ways in which we could contact them"
                                            Width="577px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 5px">
                                        <asp:TextBox ID="txtPreferredContactTime" runat="server" Columns="90" Height="61px"
                                            ReadOnly="True" Rows="4" TextMode="MultiLine" Width="400px"></asp:TextBox></td>
                                </tr>
                                <tr id="JSPrint">
                                    <td colspan="2">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="btnPrint" runat="server" CssClass="noprint" OnClientClick="printReport('tblJSPrint');"
                                                    Text="Print" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 105px">
                                        <cc2:ModalPopupExtender ID="ModalPopup_JSsummary" runat="server" BackgroundCssClass="modalBackground"
                                            CancelControlID="btnJSPopupClose" PopupControlID="updatePanel_JbSSmry" TargetControlID="lblProblemDays">
                                        </cc2:ModalPopupExtender>
                                    </td>
                                    <td style="width: 100px">
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                            <br />
                            <asp:UpdatePanel ID="updatePanel_TBA" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <cc1:PagingGridView ID="GVTBA" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                                        BorderStyle="Dotted" BorderWidth="1px" CellPadding="2" EmptyDataText="No record exists"
                                        Font-Names="Arial" Font-Size="Small" ForeColor="Black" GridLines="None" OnPageIndexChanging="GVTBA_PageIndexChanging"
                                        OnSorting="GVTBA_Sorting" VirtualItemCount="-1" Width="100%">
                                        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
                                            PageButtonCount="5" />
                                        <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                        <RowStyle BackColor="#EFF3FB" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="JS No" SortExpression="FL_fault_Log.JOBSHEETNUMBER">
                                                <itemtemplate>
<asp:HiddenField id="hfCustomerId" runat="server" Value='<%# Bind("CUSTOMERID") %>' __designer:wfdid="w1"></asp:HiddenField> <asp:LinkButton id="lnkJobSheetNumber" onclick="lnkJobSheetNumber_Click" runat="server" Text='<%# Eval("JSNumber") %>' __designer:wfdid="w2" CommandArgument='<%# Eval("JSNumber") %>'></asp:LinkButton> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Logged" InsertVisible="False" SortExpression="FL_FAULT_LOG.SubmitDate">
                                                <itemtemplate>
&nbsp;<asp:Label id="lblTBAGVLogged" runat="server" Text='<%# UtilityFunctions.FormatDate(Eval("Due").toString()) %>' __designer:wfdid="w2"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name" InsertVisible="False" SortExpression="C__CUSTOMER.FIRSTNAME">
                                                <itemtemplate>
<asp:Label id="GVLblTBAName" runat="server" CssClass="cellData" Text='<%# GetFullName(Eval("TITLEDESCRIPTION").ToString(),Eval("Name").ToString(), Eval("LASTNAME").ToString()) %>' __designer:wfdid="w3"></asp:Label> 
</itemtemplate>
                                                <headerstyle horizontalalign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address" InsertVisible="False" SortExpression="P__PROPERTY.ADDRESS1">
                                                <itemtemplate>
<asp:Label id="GVLblTBAAddress" runat="server" CssClass="cellData" Text="<%# bind('COMPLETEADDRESS') %>" __designer:wfdid="w4"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Priority" InsertVisible="False" SortExpression="FL_FAULT_PRIORITY.ResponseTime">
                                                <itemtemplate>
<asp:Label id="GVLblTBAPriority" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatPriorityReponse(Eval("Priority").toString(),Eval("Type").toString()) %>' __designer:wfdid="w9"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Due Date" InsertVisible="False" SortExpression="FL_FAULT_LOG.DueDate ">
                                                <itemtemplate>
<asp:Label id="GVLblTBADue" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("DueDate").toString()) %>' __designer:wfdid="w14"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location" InsertVisible="False" SortExpression="FL_Area.AreaName">
                                                <itemtemplate>
<asp:Label id="GVLblTBALocation" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatAreaElement(Eval("Area").toString(),Eval("Element").toString()) %>' __designer:wfdid="w7"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fault Details" InsertVisible="False" SortExpression="FL_FAULT.Description">
                                                <itemtemplate>
<asp:Label id="GVLblTBADescription" runat="server" CssClass="cellData" Text="<%# bind('Description') %>" __designer:wfdid="w10"></asp:Label> 
</itemtemplate>
                                                <headerstyle wrap="False" />
                                            </asp:TemplateField>
                                           <asp:TemplateField>
                                                <itemtemplate>
                                                    <asp:ImageButton id="imgTBAPopup" runat="server" ImageUrl='<%# "~/images/exclamation_icon.gif" %>' VISIBLE="<%# bind('STATUS') %>" OnCommand="imgTBAPopup_Command" CommandArgument='<%# Bind("CUSTOMERID") %>'></asp:ImageButton>&nbsp;&nbsp;
                                                </itemtemplate>
                                           </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Select" InsertVisible="False" HeaderStyle-HorizontalAlign="center">
                                                <itemtemplate>
                                                    &nbsp; <%--<asp:ImageButton id="imgTBAPopup" runat="server" ImageUrl='<%# "~/images/exclamation_icon.gif" %>' __designer:wfdid="w3" VISIBLE="<%# bind('STATUS') %>" OnCommand="imgTBAPopup_Command" CommandArgument='<%# Bind("CUSTOMERID") %>'></asp:ImageButton>&nbsp;&nbsp; --%>
                                                    <asp:CheckBox id="chkTBASelect" runat="server" __designer:wfdid="w4"></asp:CheckBox> 
                                                </itemtemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <AlternatingRowStyle BackColor="White" Font-Names="Arial" Font-Size="Small" Wrap="True" />
                                    </cc1:PagingGridView>
                                    <table border="0" style="border-right: #d2b48c 2px dashed; border-top: #d2b48c 2px dashed; border-left: #d2b48c 2px dashed; border-bottom: #d2b48c 2px dashed">
                                        <tr>
                                            <td style="width: 100%; height: 204px; border-top-width: thin; border-left-width: thin; border-left-color: #d2b48c; border-bottom-width: thin; border-bottom-color: #d2b48c; border-top-color: #d2b48c; border-right-width: thin; border-right-color: #d2b48c;">
                                                <table border="0">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblTBAError" runat="server" ForeColor="Red" CssClass="caption"></asp:Label></td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 26px">
                                                        </td>
                                                        <td align="right" style="height: 26px">
                                                            <asp:Label ID="lblEnterDate" runat="server" Text="Enter Appointment Date and Time for Selected Jobs:" CssClass="caption"></asp:Label></td>
                                                        <td align="left" style="height: 26px">
                                                            <asp:TextBox ID="txtTBADate" runat="server" Enabled="False" Font-Names="Arial" Font-Size="Small"
                                                                MaxLength="10" Width="148px"></asp:TextBox><asp:ImageButton ID="btnTBACalander" runat="server"
                                                                    CausesValidation="False" ImageUrl="~/images/buttons/Calendar_button.png" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTBAHour" runat="server" AppendDataBoundItems="True" CssClass="caption"
                                                                Font-Names="Arial" Font-Size="Small" Width="48px">
                                                                <asp:ListItem>01</asp:ListItem>
                                                                <asp:ListItem>02</asp:ListItem>
                                                                <asp:ListItem>03</asp:ListItem>
                                                                <asp:ListItem>04</asp:ListItem>
                                                                <asp:ListItem>05</asp:ListItem>
                                                                <asp:ListItem>06</asp:ListItem>
                                                                <asp:ListItem>07</asp:ListItem>
                                                                <asp:ListItem>08</asp:ListItem>
                                                                <asp:ListItem>09</asp:ListItem>
                                                                <asp:ListItem>10</asp:ListItem>
                                                                <asp:ListItem>11</asp:ListItem>
                                                                <asp:ListItem>12</asp:ListItem>
                                                            </asp:DropDownList><asp:DropDownList ID="ddlTBAMin" runat="server" AppendDataBoundItems="True"
                                                                CssClass="caption" Font-Names="Arial" Font-Size="Small" Width="48px">
                                                                <asp:ListItem>00</asp:ListItem>
                                                                <asp:ListItem>15</asp:ListItem>
                                                                <asp:ListItem>30</asp:ListItem>
                                                                <asp:ListItem>45</asp:ListItem>
                                                            </asp:DropDownList><asp:DropDownList ID="ddlTBAAMPM" runat="server" AppendDataBoundItems="True"
                                                                CssClass="caption" Font-Names="Arial" Font-Size="Small" Width="48px">
                                                                <asp:ListItem Value="0">AM</asp:ListItem>
                                                                <asp:ListItem Value="1">PM</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td align="right">
                                                            <asp:Label ID="lblTBAOperative" runat="server" Text="Operative:" CssClass="caption"></asp:Label></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTBAOperative" runat="server" AppendDataBoundItems="True"
                                                                CssClass="caption" Font-Names="Arial" Font-Size="Small" Width="152px">
                                                                <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                            </asp:DropDownList></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            </td>
                                                        <td align="right">
                                                            <asp:Button ID="btnTBASave" runat="server" OnClick="btnTBASave_Click" Text="Save Appointment" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 44px">
                                                        </td>
                                                        <td style="height: 44px">
                                                            <cc2:CalendarExtender ID="ceTBAAppointment" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnTBACalander"
                                                                PopupPosition="BottomRight" TargetControlID="txtTBADate">
                                                            </cc2:CalendarExtender>
                                                        </td>
                                                        <td align="right" style="height: 44px">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="updatePanel_TBAPopup" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <cc2:ModalPopupExtender ID="modalPopupExtender_TBA" runat="server" BackgroundCssClass="modalBackground"
                                        CancelControlID="btnTBAClose" PopupControlID="updatePanel_TBAPopup" TargetControlID="lblTBAPopupTitle">
                                    </cc2:ModalPopupExtender>
                                   <%--  <asp:Panel ID="pnlTBAPopup" runat="server"> --%>
                                    <table bgcolor="white" border="2">
                                        <tr>
                                            <td>
                                                <table bgcolor="white" style="width: 243px; height: 250px">
                                                    <tr>
                                                        <td align="left" colspan="3" rowspan="1" style="width: 486px; background-color: #cc0000"
                                                            valign="middle">
                                                            <asp:Label ID="lblTBAPopupTitle" runat="server" Font-Bold="True" ForeColor="White"
                                                                Text="!Special Requirements!" CssClass="caption"></asp:Label></td>
                                                    </tr>
                                                    
                                                    <asp:Panel runat="server" ID="pnlVul" Visible="false">
                                                    <tr>
                                                        <td align="left" colspan="3" rowspan="1" style="width: 486px; height: 20px" valign="top">
                                                            <asp:Label ID="lblTBAPopupUp" runat="server" Font-Bold="False" Width="480px" CssClass="caption"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td style="height: 76px; width: 486px;" colspan="3">
                                                            <asp:GridView ID="gvCatList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                ForeColor="#333333" GridLines="None" CssClass="caption">
                                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="CATDESC" HeaderText="Category" />
                                                                    <asp:BoundField DataField="SUBCATDESC" HeaderText="Sub Category" />
                                                                </Columns>
                                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                            </asp:GridView>
                                                        </td>
   
                                                     </tr>
                                                     </asp:Panel>
                                                     <asp:Panel runat="server" ID="pnlRisk" Visible="false">
                                                     <tr>
                                                        <td align="left" colspan="3" rowspan="1" style="width: 486px; height: 20px" valign="top">
                                                            <asp:Label ID="lblRisk" runat="server" Width="480px" CssClass="caption"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td style="height: 76px; width: 486px;" colspan="3">
                                                            <asp:GridView ID="gvRiskList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                ForeColor="#333333" GridLines="None" CssClass="caption">
                                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                <Columns>
                                                                    <%--<asp:BoundField DataField="RISKDESC" HeaderText="Risk" />--%>
                                                                    <asp:BoundField DataField="CATDESC" HeaderText="Category" />
                                                                    <asp:BoundField DataField="SUBCATDESC" HeaderText="Sub Category" />
                                                                </Columns>
                                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                            </asp:GridView>
                                                        </td>
   
                                                     </tr>
                                                     </asp:Panel>
                                                     
                                                     <asp:Panel runat="server" ID="pnlComm" Visible="false">
                                                      <tr>
                                                        <td align="left" colspan="3" rowspan="1" style="width: 486px; height: 20px" valign="top">
                                                            <asp:Label ID="lblTBAPopUpComm" runat="server" Font-Bold="False" Width="480px" CssClass="caption"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     
                                                     <tr>
                                                        <td style="height: 50px; width: 486px;" colspan="3">
                                                            <asp:GridView ID="gvCommunication" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                ForeColor="#333333" GridLines="None" CssClass="caption">
                                                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="COMMUNICATION" HeaderText="Communication" />
                                                                 </Columns>
                                                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                            </asp:GridView>
                                                        </td>
                                                     </tr>  
                                                     </asp:Panel>
                                                    
                                                    <asp:Panel runat="server" ID="pnlGS" Visible="false"> 
                                                    <tr>
                                                        <td align="left" colspan="3" rowspan="1" style="width: 486px; height: 51px" valign="top">
                                                            <asp:Label ID="lblTBAPopupDown" runat="server" Font-Bold="False"
                                                                Width="480px" CssClass="caption"></asp:Label>
                                                            <asp:Label ID="lblGasServicing" runat="server" Font-Bold="False"
                                                               CssClass="caption"></asp:Label>
                                                           </td>
                                                    </tr>
                                                    </asp:Panel>
                                                     <tr>
                                                        <td align="right" colspan="3" rowspan="1"  style="width: 338px; height: 16px" valign="top">
                                                            <asp:Button ID="btnTBAClose" runat="server" Text="Close Window" OnClick="btnTBAClose_Click" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <%--</asp:Panel>--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="updatePanel_MAGV" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    &nbsp;<asp:Label ID="lblManageAppointmentMsg" runat="server"></asp:Label>
                                    <cc1:PagingGridView ID="GVManageAppointment" runat="server" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                                        BorderStyle="Dotted" BorderWidth="1px" CellPadding="2" EmptyDataText="No record exists"
                                        Font-Names="Arial" Font-Size="Small" ForeColor="Black" GridLines="None" OnPageIndexChanging="GVManageAppointment_PageIndexChanging"
                                        OnSorting="GVManageAppointment_Sorting" VirtualItemCount="-1" Width="100%">
                                        <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="JS No" SortExpression="FL_FAULT_LOG.JobSheetNumber">
                                                <itemtemplate>
<asp:LinkButton id="lnkJbSheetNumberMng" onclick="lnkJbSheetNumberMng_Click" runat="server" Text='<%# Bind("JSNumber") %>' __designer:wfdid="w1" CommandArgument='<%# Eval("JSNumber") %>'></asp:LinkButton> 
</itemtemplate>
                                                <headerstyle wrap="False" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Logged" InsertVisible="False" SortExpression="FL_FAULT_LOG.SubmitDate">
                                                <itemtemplate>
<asp:Label id="GVLbllogged" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("Logged").toString()) %>' __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name" InsertVisible="False" SortExpression="C__CUSTOMER.FIRSTNAME">
                                                <itemtemplate>
<asp:Label id="GVLblName" runat="server" CssClass="cellData" Text='<%# GetFullName(Eval("NameTitle").ToString(), Eval("Name").ToString(), Eval("LastName").ToString()) %>' __designer:wfdid="w3"></asp:Label> 
</itemtemplate>
                                                <headerstyle horizontalalign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address" InsertVisible="False" SortExpression="P__PROPERTY.ADDRESS1">
                                                <itemtemplate>
<asp:Label id="GVLblAddress" runat="server" CssClass="cellData" Text='<%# Eval("COMPLETEADDRESS") %>' __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Due Date" InsertVisible="False" SortExpression="FL_FAULT_PRIORITY.ResponseTime">
                                                <itemtemplate>
                    <asp:Label id="GVLblDue" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("DueDate").toString()) %>'></asp:Label> 
                    
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Appointment" InsertVisible="False" SortExpression="FL_CO_APPOINTMENT.AppointmentDate">
                                                <itemtemplate>
                    <asp:Label id="GVLblAppointment" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("Appointment").toString())%>'></asp:Label> 
                    
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fault Details" InsertVisible="False" SortExpression="FL_FAULT.Description">
                                                <itemtemplate>
                    <asp:Label id="GVLblDescription" runat="server" CssClass="cellData" Text='<%# Eval("Description")%>'></asp:Label> 
                    
</itemtemplate>
                                                <headerstyle wrap="False" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Operative" InsertVisible="False" SortExpression="E__EMPLOYEE.FIRSTNAME">
                                                <itemtemplate>
                    <asp:Label id="GVLblOperative" runat="server" CssClass="cellData" Text='<%# Eval("Operative")%>'></asp:Label> 
                    
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" InsertVisible="False" SortExpression="FL_CO_APPOINTMENT_STAGE.StageName">
                                                <itemtemplate>
                    <asp:Label id="GVLblStage" runat="server" CssClass="cellData" Text='<%# Eval("Stage")%>'></asp:Label> 
                    
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField InsertVisible="False">
                                                <itemtemplate>
<asp:Button id="btnManageAppointment" onclick="btnManageAppointment_Click" runat="server" Width="152px" Text="Manage Appointment" CommandArgument='<%# MakeCommandString(Eval("Name").ToString(), Eval("CustomerAddress").ToString(),Eval("CustomerTel").ToString(),Eval("CustomerNo").ToString(),UtilityFunctions.FormatDate(Eval("Appointment")).ToString(),UtilityFunctions.FormatDate(Eval("LastActioned")).ToString(),Eval("AppointmentNotes").ToString(),Eval("AppointmentStageId").ToString(),Eval("AppointmentOperativeID").ToString(),Eval("AppointmentLetterID").ToString(),Eval("TenancyRef").ToString(),Eval("AppointmentID").ToString(),Eval("AppointmentTime").ToString()) %>' __designer:wfdid="w99"></asp:Button> 
</itemtemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle BackColor="#EFF3FB" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" />
                                        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
                                            PageButtonCount="5" />
                                        <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                                        <AlternatingRowStyle BackColor="White" Font-Names="Arial" Font-Size="Small" Wrap="True" />
                                    </cc1:PagingGridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel_MAPopUp" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlManageAppointment" runat="server" BackColor="White" BorderColor="Transparent"
                                        Height="320px" HorizontalAlign="center" Width="520px">
                                        <table cellpadding="1" cellspacing="1">
                                            <tbody align="left">
                                                <tr>
                                                    <td align="left" colspan="13" style="width: 526px; height: 21px; background-color: #c00000">
                                                        <asp:Label ID="lblManageAppointmentHeading" runat="server" BackColor="#C00000" Font-Bold="True"
                                                            Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Manage Appointment"
                                                            Width="35%"></asp:Label>
                                                        <asp:LinkButton ID="lnkBtnPseudo" runat="server"></asp:LinkButton></td>
                                                </tr>
                                                <tr>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table cellpadding="5" style="width: 520px; height: 224px">
                                            <tr>
                                                <td rowspan="2" style="width: 212px; height: 208px" valign="top">
                                                    <table cellpadding="3" cellspacing="0" style="border-right: silver thin solid; border-top: silver thin solid;
                                                        border-left: silver thin solid; border-bottom: silver thin solid; height: 144px">
                                                        <tr>
                                                            <td align="left" style="width: 96px">
                                                                <asp:Label ID="lblCustomer" runat="server" CssClass="caption" Text="Customer"></asp:Label></td>
                                                            <td align="left" style="border-left: silver thin solid; width: 104px">
                                                                <asp:Label ID="lblCustomerText" runat="server" CssClass="caption" Width="88px">c</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 96px">
                                                                <asp:Label ID="lblCustomerAddress" runat="server" CssClass="caption" Text="Address"></asp:Label></td>
                                                            <td align="left" style="border-left: silver thin solid; width: 104px">
                                                                <asp:Label ID="lblAddressText" runat="server" CssClass="caption" Width="88px">a</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 96px">
                                                                <asp:Label ID="lblTelephone" runat="server" CssClass="caption" Text="Telephone"></asp:Label></td>
                                                            <td align="left" style="border-left: silver thin solid; width: 104px">
                                                                <asp:Label ID="lblTelephoneText" runat="server" CssClass="caption" Width="88px">t</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 96px">
                                                                <asp:Label ID="lblCustomerNo" runat="server" CssClass="caption" Text="Customer No."
                                                                    Width="96px"></asp:Label></td>
                                                            <td align="left" style="border-left: silver thin solid; width: 104px">
                                                                <asp:Label ID="lblCustomerNoText" runat="server" CssClass="caption" Width="88px">cn</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 96px">
                                                                <asp:Label ID="lblTenancyRef" runat="server" CssClass="caption" Text="Tenancy Ref."
                                                                    Width="96px"></asp:Label></td>
                                                            <td align="left" style="border-left: silver thin solid; width: 104px">
                                                                <asp:Label ID="lblTenancyRefText" runat="server" CssClass="caption" Width="88px">tr</asp:Label></td>
                                                        </tr>
                                                    </table>
                                                    <asp:Label ID="lblPopuphidden" runat="server" Visible="False"></asp:Label>
                                                </td>
                                                <td align="right" colspan="2" rowspan="2" style="width: 291px; height: 200px" valign="top">
                                                    <table cellpadding="3" cellspacing="0" style="width: 264px; height: 208px">
                                                        <tr>
                                                            <td align="left" style="width: 94px">
                                                                <asp:Label ID="lblPopupStage" runat="server" CssClass="caption" Text="Stage" Width="48px"></asp:Label></td>
                                                            <td align="left" style="width: 154px">
                                                                <asp:DropDownList ID="ddlPopupStage" runat="server" AppendDataBoundItems="True" CssClass="caption"
                                                                    Font-Names="Arial" Font-Size="Small" Width="152px">
                                                                    <asp:ListItem Value="">All</asp:ListItem>
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 94px">
                                                                <asp:Label ID="lblPopupAppointment" runat="server" CssClass="caption" Text="Appointment"
                                                                    Width="96px"></asp:Label></td>
                                                            <td align="left" style="width: 154px">
                                                                <asp:TextBox ID="txtPopupAppointment" runat="server" CssClass="caption" Enabled="False"
                                                                    Width="112px"></asp:TextBox>
                                                                <asp:ImageButton ID="ibtnCalender" runat="server" Height="16px" ImageUrl="~/images/buttons/Calendar_button.png"
                                                                    Width="24px" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 94px">
                                                                <asp:Label ID="lblPopupTime" runat="server" CssClass="caption" Text="Time" Width="88px"></asp:Label></td>
                                                            <td align="left" style="width: 154px">
                                                                <asp:DropDownList ID="ddlPopupHr" runat="server" AppendDataBoundItems="True" CssClass="caption"
                                                                    Font-Names="Arial" Font-Size="Small" Width="48px">
                                                                </asp:DropDownList>
                                                                <asp:DropDownList ID="ddlPopupMin" runat="server" AppendDataBoundItems="True" CssClass="caption"
                                                                    Font-Names="Arial" Font-Size="Small" Width="48px">
                                                                </asp:DropDownList>
                                                                <asp:DropDownList ID="ddlPopupFormat" runat="server" AppendDataBoundItems="True"
                                                                    CssClass="caption" Font-Names="Arial" Font-Size="Small" Width="48px">
                                                                    <asp:ListItem Value="AM">AM</asp:ListItem>
                                                                    <asp:ListItem Value="PM">PM</asp:ListItem>
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 94px">
                                                                <asp:Label ID="lblPopupLastActionBy" runat="server" CssClass="caption" Text="Last action by"
                                                                    Width="96px"></asp:Label></td>
                                                            <td align="left" style="width: 154px">
                                                                <asp:TextBox ID="txtPopupLastActionBy" runat="server" CssClass="caption" Enabled="False"
                                                                    Width="147px"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 94px">
                                                                <asp:Label ID="lblPopupOperative" runat="server" CssClass="caption" Text="Operative"
                                                                    Width="88px"></asp:Label></td>
                                                            <td align="left" style="width: 154px">
                                                                <asp:DropDownList ID="ddlPopupOperative" runat="server" AppendDataBoundItems="True"
                                                                    CssClass="caption" Font-Names="Arial" Font-Size="Small" Width="152px">
                                                                    <asp:ListItem Value="">Please Select</asp:ListItem>
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 94px">
                                                                <asp:Label ID="lblPopupNotes" runat="server" CssClass="caption" Text="Notes" Width="88px"></asp:Label></td>
                                                            <td align="left" style="width: 154px">
                                                                <asp:TextBox ID="txtPopupNotes" runat="server" CssClass="caption" MaxLength="500"
                                                                    Width="147px"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 94px">
                                                                <asp:Label ID="lblPopupSelectLetter" runat="server" CssClass="caption" Text="Select letter"
                                                                    Width="96px"></asp:Label></td>
                                                            <td align="left" style="width: 154px">
                                                                <asp:DropDownList ID="ddlPopupSelectLetter" runat="server" AppendDataBoundItems="True"
                                                                    CssClass="caption" Font-Names="Arial" Font-Size="Small" Width="152px"  >
                                                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                    </table>
                                                    <asp:UpdatePanel ID="UpdatePanel_ErrorMsg" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Label ID="lblAppointmentErrorMessage" runat="server" ForeColor="Red" CssClass="caption"></asp:Label>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender_notes" runat="server" FilterType="LowercaseLetters,Custom,Numbers,UppercaseLetters"
                                                        TargetControlID="txtPopupNotes" ValidChars=" #.%$@">
                                                    </cc2:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="3" style="height: 9px">
                                                    <table style="width: 496px">
                                                        <tr>
                                                            <td style="width: 149px">
                                                            </td>
                                                            <td style="width: 100px">
                                                                <br />
                                                                <asp:Button ID="btnCloseWindow" runat="server" CssClass="caption" Text="Close Window" /></td>
                                                            <td style="width: 100px">
                                                                <asp:UpdatePanel ID="UpdatePanel_ViewLetter" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        &nbsp;<asp:Button ID="btnViewLetter" runat="server" CssClass="caption" OnClientClick="return open_letter()"
                                                                            Text="View Letter" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td style="width: 100px">
                                                                <asp:UpdatePanel ID="UpdatePanel_WCSaveChanges" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <br />
                                                                        <asp:Button ID="btnSaveChanges" runat="server" CssClass="caption" OnClick="btnSaveChanges_Click"
                                                                            Text="Save Changes" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <cc2:ModalPopupExtender ID="MdlPopup_ManageAppointment" runat="server" BackgroundCssClass="modalBackground"
                                        CancelControlID="btnCloseWindow"  PopupControlID="UpdatePanel_MAPopUp"
                                        PopupDragHandleControlID="lblManageAppointmentHeading" TargetControlID="lnkBtnPseudo">
                                    </cc2:ModalPopupExtender>
                                    <cc2:CalendarExtender ID="CalendarExtender_Inspection" runat="server" BehaviorID="ctl56_CalendarExtender_Appointment"
                                        Format="dd/MM/yyyy" PopupButtonID="ibtnCalender" TargetControlID="txtPopupAppointment" PopupPosition="TopLeft">
                                    </cc2:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel_WCGV" runat="server" UpdateMode="Conditional"><ContentTemplate>
                                <asp:Label ID="lblWorkCompletionMsg" runat="server"></asp:Label>
<cc1:PagingGridView id="GVWorkCompletion" runat="server" Width="95%" Font-Size="Small" Font-Names="Arial" ForeColor="Black" BackColor="LightGoldenrodYellow" VirtualItemCount="-1" OnPageIndexChanging="GVWorkCompletion_PageIndexChanging" GridLines="None" EmptyDataText="No record exists" CellPadding="2" BorderWidth="1px" BorderStyle="Dotted" BorderColor="Tan" AutoGenerateColumns="False" AllowSorting="True" AllowPaging="True" OnSorting="GVWorkCompletion_Sorting">
                                <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White"  />
                                <Columns>
                                    <asp:TemplateField SortExpression="FL_CO_APPOINTMENT.JobSheetNumber" HeaderText="JS No" InsertVisible="False">
                                        <itemtemplate>
<asp:LinkButton id="lnkWCJSNumber" onclick="lnkWCJSNumber_Click" runat="server" Text='<%# Eval("JSNumber") %>' __designer:wfdid="w5" CommandArgument='<%# Eval("JSNumber") %>'></asp:LinkButton>
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="FL_FAULT_LOG.SubmitDate" HeaderText="Logged" InsertVisible="False">
                                        <itemtemplate>
<asp:Label id="GVWCLbllogged" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("Logged").toString()) %>' __designer:wfdid="w172"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="P__PROPERTY.ADDRESS1" HeaderText="Address" InsertVisible="False">
                                        <itemtemplate>
<asp:Label id="GVWCLblAddress" runat="server" CssClass="cellData" Text='<%# Eval("COMPLETEADDRESS") %>' __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="FL_FAULT_PRIORITY.ResponseTime" HeaderText="Due Date" InsertVisible="False">
                                        <itemtemplate>
                    <asp:Label id="GVWCLblDue" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatDate(Eval("DueDate").toString()) %>'></asp:Label> 
                    
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="FL_FAULT.Description" HeaderText="Fault Details" InsertVisible="False">
                                        <itemtemplate>
                    <asp:Label id="GVWCLblDescription" runat="server" CssClass="cellData" Text='<%# Eval("Description")%>'></asp:Label> 
                    
</itemtemplate>
                                        <headerstyle wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField SortExpression="E__EMPLOYEE.FIRSTNAME" HeaderText="Operative" InsertVisible="False">
                                        <itemtemplate>
                    <asp:Label id="GVWCLblOperative" runat="server" CssClass="cellData" Text='<%# Eval("Operative")%>'></asp:Label> 
                    
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField InsertVisible="False">
                                        <itemtemplate>
<asp:Button id="btnWorkCompletion" onclick="btnWorkCompletion_Click" runat="server" Text="Complete" __designer:wfdid="w1" CommandArgument='<%# MakeCommandString(Eval("FaultLogID").ToString(),Eval("Name").ToString(), Eval("CustomerAddress").ToString(),Eval("LocationName").ToString(),Eval("AreaName").ToString(),Eval("ElementName").ToString(),Eval("Description").ToString(),Eval("AppointmentOperativeID").ToString(),Eval("AppointmentID").ToString(), Eval("TitleDescription").ToString(),Eval("LASTNAME").ToString(), Eval("CustomerHouseNo").ToString(),Eval("CustomerPostCode").ToString(),Eval("CustomerCounty").ToString(),Eval("CustomerTownCity").ToString()) %>'></asp:Button> 
</itemtemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#EFF3FB"  />
                                <EditRowStyle BackColor="#2461BF"  />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"  />
                                <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left"  />
                                <pagersettings pagebuttoncount="5" FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                                <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left"  />
                                <AlternatingRowStyle BackColor="White" Font-Names="Arial" Font-Size="Small" Wrap="True"  />
                            </cc1:PagingGridView> 
</ContentTemplate>
</asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel_WCPopUp" runat="server" UpdateMode="Conditional" Visible="False">
                                <ContentTemplate>
                                    <asp:Panel BackColor="White" BorderColor="Transparent" HorizontalAlign="center" ID="pnlWorkCompletion" runat="server"  CssClass="wcpopup">
                                        <table cellpadding="0" cellspacing="0" style="width: 600px; margin: 5px;" >
                                            <tbody align="left">
                                                <tr>
                                                    <td align="left" colspan="13" style="width: 655px; height: 21px; background-color: #c00000; border-bottom: white thin solid;">
                                                        <asp:Image ID="imgRSLLogo" runat="server" ImageUrl="~/images/braodland_logo.gif" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="13" style="width: 655px; height: 21px; background-color: #c00000; padding-left: 10px;">
                                                        <asp:Label ID="lblWCPopUpHeading" runat="server" BackColor="#C00000" Font-Bold="True"
                                                            Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Work Completion Detail"
                                                            Width="35%"></asp:Label>
                                                        <asp:LinkButton ID="lnkPopupBtnPseudo" runat="server"></asp:LinkButton></td>
                                                </tr>
                                                <tr>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table style="width: 520px; height: 224px;" cellpadding="5">
                                            <tr>
                                                <td align="left" rowspan="1" style="width: 92px;" valign="top">
                                                    &nbsp;<table style="height: 88px; border-right: silver thin solid; border-top: silver thin solid; border-left: silver thin solid; border-bottom: silver thin solid;" cellpadding="3" cellspacing="0" width="600" >
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" colspan="" style="width: 100px">
                                                                    <asp:Label ID="lblWCCustomer" runat="server" CssClass="caption" Text="Customer:"></asp:Label></td>
                                                                <td align="left" style="width: 250px">
                                                                    <asp:Label ID="lblWCCustomerText" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="" style="width: 100px">
                                                                    <asp:Label ID="lblWCAddress" runat="server" CssClass="caption" Text="Address:"></asp:Label></td>
                                                                <td align="left" style="width: 250px">
                                                                    <asp:Label ID="lblWCAddressText" runat="server" CssClass="caption"></asp:Label>,
                                                                    <asp:Label ID="lblWCHouseNumberText" runat="server" CssClass="caption"></asp:Label>,
                                                                    <asp:Label ID="lblWCPostCodeText" runat="server" CssClass="caption"></asp:Label>,
                                                                    <asp:Label ID="lblWCTownCityText" runat="server" CssClass="caption"></asp:Label>,
                                                                    <asp:Label ID="lblWCCountyText" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="" style="width: 100px">
                                                                    <asp:Label ID="lblWCFaultLocation" runat="server" CssClass="caption" Text="Fault Location:"></asp:Label></td>
                                                                <td align="left" style="width: 250px">
                                                                    <asp:Label ID="lblWCFaultLocationText" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" colspan="" style="width: 100px">
                                                                    <asp:Label ID="lblWCDescription" runat="server" CssClass="caption" Text="Description:"
                                                                        Width="96px"></asp:Label></td>
                                                                <td align="left" style="width: 250px; height: 25px;">
                                                                    <asp:Label ID="lblWCDescriptionText" runat="server" CssClass="caption"></asp:Label></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <asp:Label ID="lblWCPopuphidden" runat="server" Visible="False"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 92px; height: 204px" rowspan="2" valign="top">
                                                    <table style="height: 1px; border-right: silver thin solid; border-top: silver thin solid; border-left: silver thin solid; border-bottom: silver thin solid;" cellpadding="0" cellspacing="0" id="TABLE2"  width="600">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" colspan="3" style="border-bottom: silver thin solid; height: 21px">
                                                                    <asp:Label ID="lblWCRepairWorkHeading" runat="server" CssClass="caption" Font-Bold="True"
                                                                        Text="Repair Work Details:" Width="152px"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" style="width: 100px">
                                                                    <table cellpadding="3" cellspacing="0" style="width: 264px; height: 208px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" style="width: 91px; height: 15px">
                                                                                    <asp:Label ID="lblWCPopupComplete" runat="server" CssClass="caption" Text="Complete:"
                                                                                        Width="96px"></asp:Label></td>
                                                                                <td align="left" style="width: 159px">
                                                                                    <asp:TextBox ID="txtWCPopupComplete" runat="server" CssClass="caption" Enabled="False"
                                                                                        Width="120px"></asp:TextBox>
                                                                                    <asp:ImageButton ID="ibtnWCCalender" runat="server" Height="16px" ImageUrl="~/images/buttons/Calendar_button.png"
                                                                                        Width="24px" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 91px">
                                                                                    <asp:Label ID="lblWCPopupTime" runat="server" CssClass="caption" Text="Time:" Width="88px"></asp:Label></td>
                                                                                <td align="left" style="width: 165px">
                                                                                    <asp:DropDownList ID="ddlWCPopupHr" runat="server" AppendDataBoundItems="True" CssClass="caption"
                                                                                        Font-Names="Arial" Font-Size="Small" Width="48px">
                                                                                    </asp:DropDownList>
                                                                                    <asp:DropDownList ID="ddlWCPopupMin" runat="server" AppendDataBoundItems="True" CssClass="caption"
                                                                                        Font-Names="Arial" Font-Size="Small" Width="46px">
                                                                                    </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlWCPopupFormat" runat="server" AppendDataBoundItems="True"
                                                                                        CssClass="caption" Font-Names="Arial" Font-Size="Small" Width="53px">
                                                                                        <asp:ListItem Value="AM">AM</asp:ListItem>
                                                                                        <asp:ListItem Value="PM">PM</asp:ListItem>
                                                                                    </asp:DropDownList>&nbsp;&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 91px">
                                                                                    <asp:Label ID="lblWCPopupBy" runat="server" CssClass="caption" Text="By:" Width="96px"></asp:Label></td>
                                                                                <td align="left" style="width: 149px">
                                                                                    <asp:DropDownList ID="ddlWCPopupBy" runat="server" AppendDataBoundItems="True" CssClass="caption"
                                                                                        Font-Names="Arial" Font-Size="Small" Width="155px">
                                                                                        <asp:ListItem>Please Select</asp:ListItem>
                                                                                    </asp:DropDownList></td>
                                                                            </tr>
                                     <%--                                       <tr>
                                                                                <td align="left" style="width: 91px; height: 30px">
                                                                                    <asp:Label ID="lblWCPopupSchedule" runat="server" CssClass="caption" Text="Schedule:"
                                                                                        Width="88px"></asp:Label></td>
                                                                                <td align="left" style="width: 149px">
                                                                                    <asp:DropDownList ID="ddlWCPopupSchedule" runat="server" AppendDataBoundItems="True"
                                                                                        CssClass="caption" Font-Names="Arial" Font-Size="Small" Width="155px">
                                                                                        <asp:ListItem>Please Select</asp:ListItem>
                                                                                    </asp:DropDownList></td>
                                                                            </tr>--%>
                                                                            <tr>
                                                                                <td align="left" style="width: 91px; height: 30px">
                                                                                    <asp:Label ID="lblRepairQty" runat="server" CssClass="caption"
                                                                                        Text="Quantity:" Width="104px" ></asp:Label></td>
                                                                                <td align="left" style="width: 149px">
                                                                                    <asp:DropDownList ID="ddlRepairQuantity" runat="server" AppendDataBoundItems="True" CssClass="WCDropDown"
                                                                                        Font-Names="Arial" Font-Size="Small" Width="46px">
                                                                                        <asp:ListItem>1</asp:ListItem>
                                                                                        <asp:ListItem>2</asp:ListItem>
                                                                                        <asp:ListItem>3</asp:ListItem>
                                                                                        <asp:ListItem>4</asp:ListItem>
                                                                                        <asp:ListItem>5</asp:ListItem>
                                                                                        <asp:ListItem>6</asp:ListItem>
                                                                                        <asp:ListItem>7</asp:ListItem>
                                                                                        <asp:ListItem>8</asp:ListItem>
                                                                                        <asp:ListItem>9</asp:ListItem>
                                                                                        <asp:ListItem>10</asp:ListItem>
                                                                                    </asp:DropDownList></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 91px">
                                                                                    <asp:Label ID="lblWCPopupRepairDetail" runat="server" CssClass="caption" Text="Repair Detail:"
                                                                                        Width="104px"></asp:Label></td>
                                                                                <td align="left" style="width: 149px">
                                                                                    <asp:UpdatePanel ID="UpdatePanel_WCRepairDescription" runat="server" UpdateMode="Conditional">
                                                                                        <ContentTemplate>
                                                                                            <asp:TextBox ID="txtWCPopupRepairDetail" runat="server" AutoPostBack="True" CssClass="caption"
                                                                                                OnTextChanged="txtWCPopupRepairDetail_TextChanged" Width="151px"></asp:TextBox>
                                                                                            <cc2:FilteredTextBoxExtender ID="flterExtRepDetails" runat="server"
                                                                                                FilterType="Custom,LowercaseLetters,Numbers,UppercaseLetters" TargetControlID="txtWCPopupRepairDetail"
                                                                                                ValidChars=" $.#*">
                                                                                            </cc2:FilteredTextBoxExtender>
                                                                                            <cc2:AutoCompleteExtender ID="AtoCmlteRepair" runat="server" EnableCaching="true"
                                                                                                MinimumPrefixLength="1" ServiceMethod="RepairAutoComplete" ServicePath="AutoComplete.asmx"
                                                                                                TargetControlID="txtWCPopupRepairDetail" >
                                                                                            </cc2:AutoCompleteExtender>
                                                                                           
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" style="width: 91px; height: 112px" valign="top">
                                                                                    <asp:Label ID="lblWCPopupCost" runat="server" CssClass="caption" Text="Cost:" Width="32px"></asp:Label></td>
                                                                                <td align="left" style="width: 149px; height: 112px" valign="top">
                                                                                    <asp:UpdatePanel ID="UpdatePanel_RepairDetail" runat="server" UpdateMode="Conditional">
                                                                                        <ContentTemplate>
                                                                                            <table style="height: 72px">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="width: 20px; height: 26px;" align="left">
                                                                                                            <asp:Label ID="lblWCPopupNet" runat="server" CssClass="caption" Text="Net  £" Width="56px"></asp:Label></td>
                                                                                                        <td style="width: 50px; height: 24px;" align="left">
                                                                                                            <asp:TextBox ID="txtWCPopupNet" runat="server" AutoPostBack="True" CssClass="caption"
                                                                                                                Enabled="False" MaxLength="8" OnTextChanged="txtWCPopupNet_TextChanged" Width="75px"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20px; height: 26px;" align="left">
                                                                                                            <asp:Label ID="lblWCPopupVatType" runat="server" CssClass="caption" Text="Vat Type"
                                                                                                                Width="64px"></asp:Label></td>
                                                                                                        <td style="width: 50px; height: 24px" align="left">
                                                                                                            <asp:DropDownList ID="ddlWCPopupVat" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                                                                                                CssClass="caption" Enabled="False" Font-Names="Arial" Font-Size="Small" OnSelectedIndexChanged="ddlWCPopupVat_SelectedIndexChanged"
                                                                                                                Width="75px">
                                                                                                                <asp:ListItem>Please Select</asp:ListItem>
                                                                                                            </asp:DropDownList></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20px; height: 26px;" align="left">
                                                                                                            <asp:Label ID="lblWCPopupVat" runat="server" CssClass="caption" Text="Vat  " Width="56px"></asp:Label></td>
                                                                                                        <td style="width: 50px; height: 26px;" align="left">
                                                                                                            <asp:TextBox ID="txtWCPopupVat" runat="server" CssClass="caption" Enabled="False"
                                                                                                                Width="75px"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20px; height: 26px;" align="left">
                                                                                                            <asp:Label ID="lblWCPopupTotal" runat="server" CssClass="caption" Text="Total  £" Width="80px"></asp:Label></td>
                                                                                                        <td style="width: 50px; height: 24px;" align="left">
                                                                                                            <asp:TextBox ID="txtWCPopUpTotal" runat="server" CssClass="caption" Enabled="False"
                                                                                                                Width="75px"></asp:TextBox></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" colspan="2" style="width: 15px">
                                                                                                            <asp:Label ID="lblWCRepairErrorMessage" runat="server" CssClass="caption" Width="224px"></asp:Label></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 20px; height: 26px;">
                                                                                                            <asp:Label ID="lblBooleanNewRepair" runat="server" Visible="False" CssClass="caption"></asp:Label></td>
                                                                                                        <td align="right" style="width: 50px; height: 26px;">
                                                                                                            &nbsp;<asp:Button ID="btnWCPopupRepairAdd" runat="server" CssClass="caption"
                                                                                                                OnClick="btnWCPopupRepairAdd_Click" Text="Add" /></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            &nbsp;<cc2:FilteredTextBoxExtender ID="FltrExte_Net" runat="server" FilterType="Custom,Numbers"
                                                                                                TargetControlID="txtWCPopupNet" ValidChars=".">
                                                                                            </cc2:FilteredTextBoxExtender>
                                                                                            <asp:Label ID="lblPoprepairIdHidden" runat="server" Visible="False" CssClass="caption"></asp:Label>&nbsp;
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td align="center" style="width: 1600px">
                                                                </td>
                                                                <td align="left" style="width: 137700px; background-color: silver;" valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel_WCRepairGrid" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                    <asp:GridView ID="GVWCRepairGrid" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                                                        BorderStyle="Dotted" CellPadding="2" Font-Names="Arial" Font-Size="Small" ForeColor="Black"
                                                        GridLines="None" AutoGenerateColumns="False" BorderWidth="1px" Width="100%">
                                                        <FooterStyle BackColor="#C00000" ForeColor="White" Font-Bold="True"></FooterStyle>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="RepairListID" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRepairListID" runat="server" Text='<%# Eval("RepairListID") %>' CssClass="caption"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Repair">
                                                                <ItemTemplate>
                                                                    &nbsp;
                                                                    <asp:Label ID="lblWCRepair" runat="server" Text='<%# Eval("Repair") %>' CssClass="caption"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Qty">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRepQty" runat="server" Text="<%# Bind('RepQty') %>" CssClass="caption"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Cost">
                                                                <ItemTemplate>
                                                                    &nbsp;<asp:Label ID="lblWCRepairCost" runat="server" Text='<%# Eval("Cost") %>' CssClass="caption"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    &nbsp;
                                                                    <asp:Button ID="btnWCRepairDelete" runat="server" Text="Delete" CommandArgument='<%# Eval("TempRepairID") %>' OnClick="btnWCRepairDelete_Click" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <RowStyle BackColor="#EFF3FB"></RowStyle>
                                                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                                                        <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                                                        <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" ></PagerStyle>
                                                        <pagersettings pagebuttoncount="5" FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"/>                              
                                                        <HeaderStyle BackColor="#C00000" ForeColor="White" Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
                                                        <AlternatingRowStyle BackColor="White" Wrap="True" Font-Size="Small" Font-Names="Arial"></AlternatingRowStyle>
                                                    </asp:GridView>
                                                            <asp:Label ID="lblTotalVat" runat="server" Text="Total (Inc Vat)" CssClass="caption"></asp:Label>
                                                            &nbsp;&nbsp;
                                                            <asp:Label ID="lblTotalGrossAmount" runat="server" Width="48px" CssClass="caption"></asp:Label>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <table style="background-color: silver">
                                                        <tr>
                                                            <td style="width: 50px; height: 25px" align="center" valign="middle">
                                                                &nbsp;<asp:Button ID="btnCloseWCWindow" runat="server" CssClass="caption" Text="Close Window" /></td>
                                                            <td align="left" style="width: 40px; height: 25px" valign="middle">
                                                                <asp:UpdatePanel ID="UpdatePanel_InvoiceBtn" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <br />
                                                                        <asp:Button ID="btnInvoiceThisAmount" runat="server" CssClass="caption" OnClick="btnInvoiceThisAmount_Click"
                                                                            Text="Invoice This Amount" UseSubmitBehavior="False" Width="150px" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:UpdatePanel ID="UpdatePanel_WCErrorMsg" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Label ID="lblWCErrorMessage" runat="server" CssClass="caption"></asp:Label>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <cc2:ModalPopupExtender ID="MdlPopup_WorkCompletion" runat="server" BackgroundCssClass="modalBackground"
                                        CancelControlID="btnCloseWCWindow"  PopupControlID="UpdatePanel_WCPopUp"
                                        PopupDragHandleControlID="lblWCPopUpHeading" TargetControlID="lnkPopupBtnPseudo">
                                    </cc2:ModalPopupExtender>
                                    <cc2:CalendarExtender ID="CalendarExtender_Complete" runat="server" BehaviorID="ctl56_CalendarExtender_Complete"
                                        Format="dd/MM/yyyy" PopupButtonID="ibtnWCCalender" TargetControlID="txtWCPopupComplete" PopupPosition="TopLeft">
                                    </cc2:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
            <asp:UpdatePanel ID="Invoicing_UpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <cc1:PagingGridView id="GVinvoicing"  runat="server"    Width="100%" ForeColor="Black" Font-Names="Arial" BackColor="LightGoldenrodYellow" Font-Size="Small"  GridLines="None" CellPadding="2" BorderWidth="1px" BorderColor="Tan" AllowSorting="True" AllowPaging="True" VirtualItemCount="-1"  AutoGenerateColumns="False"  EmptyDataText="No record exists" BorderStyle="Dotted" OnSorting="GVinvoicing_Sorting" OnPageIndexChanging="GVinvoicing_PageIndexChanging" PageSize="1000"   >

                    <FooterStyle BackColor="#C00000" ForeColor="White" Font-Bold="True"></FooterStyle>
                    <Columns>
                    <asp:TemplateField SortExpression="FL_FAULT_LOG.JobSheetNumber" HeaderText="JS No" InsertVisible="False">
                    <itemtemplate>
&nbsp;<asp:Label id="Label1" runat="server" Text='<%# Bind("[Job Sheet Ref:]") %>' __designer:wfdid="w8"></asp:Label> 
<asp:HiddenField id="hfFaultLogId" runat="server" Value='<%# Bind("FaultLogID") %>'></asp:HiddenField>
</ItemTemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="FL_FAULT_LOG.Submitdate" HeaderText="Logged:" InsertVisible="False">
                        <itemtemplate>
&nbsp;<asp:Label id="lblLogged" runat="server" Text='<%# UtilityFunctions.FormatDate(Eval("Logged").toString()) %>' __designer:wfdid="w9"></asp:Label> 
</ItemTemplate>
                        <headerstyle horizontalalign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="P__PROPERTY.Address1" HeaderText="Address:" InsertVisible="False">
                    <itemtemplate>
&nbsp;<asp:Label id="lblAddress" runat="server" Text='<%# Bind("COMPLETEADDRESS") %>' __designer:wfdid="w16"></asp:Label> 
</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="FL_FAULT.[Description]" HeaderText="Fault Details:" InsertVisible="False">
                    <itemtemplate>
&nbsp;<asp:Label id="lblDescription" runat="server" Text='<%# Bind("Description") %>' __designer:wfdid="w18"></asp:Label>
</ItemTemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Repair Details" SortExpression="FL_FAULT_REPAIR_LIST.Description">
                            <itemtemplate>
<asp:Label id="lblRepairDetails" runat="server" Text='<%# Bind("RepairDetails") %>' __designer:wfdid="w7"></asp:Label>
</itemtemplate>
                            <headerstyle wrap="False" />
                        </asp:TemplateField>
                    <asp:TemplateField SortExpression="FL_FAULT_LOG.DueDate" HeaderText="Completed:" InsertVisible="False">
                    <itemtemplate>
&nbsp;<asp:Label id="lblCompleted" runat="server" Text='<%# UtilityFunctions.FormatDate(Eval("Completed").toString()) %>' __designer:wfdid="w20"></asp:Label> 
</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression=" E__EMPLOYEE.FirstName" HeaderText="Operative:" InsertVisible="False">
                    <itemtemplate>
&nbsp;<asp:Label id="lblOperative" runat="server" Text='<%# Bind("Operative") %>' __designer:wfdid="w22"></asp:Label>
</ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="FL_FAULT.Gross" HeaderText="Value(Gross):" InsertVisible="False">
                    <itemtemplate>
&nbsp;<asp:Label id="lblGross" runat="server" Text='<%# Bind("Gross") %>' __designer:wfdid="w53"></asp:Label> 
</ItemTemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                        <asp:TemplateField>
                            <itemtemplate>
<asp:CheckBox id="chkInvoicing" runat="server" AutoPostBack="True" OnCheckedChanged="chkInvoicing_CheckedChanged" __designer:wfdid="w7"></asp:CheckBox>&nbsp;
</itemtemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <headerstyle wrap="True" />
                        </asp:TemplateField>
                    </Columns>

                    <RowStyle BackColor="#EFF3FB"></RowStyle>
                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                    <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" ></PagerStyle>
                    <pagersettings pagebuttoncount="5" FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"/>                              
                    <HeaderStyle BackColor="#C00000" ForeColor="White" Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
                    <AlternatingRowStyle BackColor="White" Wrap="True" Font-Size="Small" Font-Names="Arial"></AlternatingRowStyle>
                    </cc1:PagingGridView> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                    <div style="text-align: right">
                                    <asp:Label ID="lblInvoicingError" runat="server" ForeColor="Red" CssClass="caption"></asp:Label>
                                    <asp:Label ID="lblInvoicingValue" runat="server" Text="Invoice Value    ₤  " Width="112px" CssClass="caption"></asp:Label>
                                    <asp:TextBox ID="txtInvoicingValue" runat="server" Width="80px" BackColor="Transparent" Enabled="False" Font-Bold="True" ForeColor="Black"></asp:TextBox><br />
                    </div>
                    <div style="text-align: right">
                        <table>
                            <tr>
                                <td style="width: 100px">
                                    <asp:Button ID="btnInvoicingSelectAll" runat="server" OnClick="btnInvoicingSelectAll_Click"
                                        Text="Select All" /></td>
                                <td style="width: 100px">
                                    <asp:Button ID="btnInvoicingProcessInvoice" runat="server" Text="Process Invoice" UseSubmitBehavior="False" OnClick="btnInvoicingProcessInvoice_Click" /></td>
                            </tr>
                       
                        </table>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel> 
                            <asp:UpdatePanel ID="UpdatePanel_Monitoring" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <cc1:PagingGridView ID="GVMonitoring" runat="server"
                                        AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                                        BorderStyle="Dotted" BorderWidth="1px" CellPadding="2" EmptyDataText="No record exists"
                                        Font-Names="Arial" Font-Size="Small" ForeColor="Black" GridLines="None" VirtualItemCount="-1"
                                        Width="100%" PageSize="1" AllowSorting="True" OnSorting="GVMonitoring_Sorting" AllowPaging = "false">
                                        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
                                            PageButtonCount="5" Visible="False" />
                                        <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="JS No" SortExpression="FL_FAULT_LOG.JobSheetNumber">
                                                <itemtemplate>
<asp:Label id="GVLblMoniJSNumber" runat="server" CssClass="cellData" Text='<%# Bind("JSNumber") %>' __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Logged" InsertVisible="False" SortExpression="FL_FAULT_LOG.SubmitDate">
                                                <itemtemplate>
<asp:Label id="GVLblMoniLogged" runat="server" CssClass="cellData" Text='<%# Eval("Logged").toString() %>' __designer:wfdid="w2"></asp:Label> 
</itemtemplate>
                                                <headerstyle horizontalalign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Priority" InsertVisible="False" SortExpression="FL_FAULT_PRIORITY.ResponseTime">
                                                <itemtemplate>
<asp:Label id="GVLblMoniPriority" runat="server" CssClass="cellData" Text='<%# Bind("PriorityTime") %>' __designer:wfdid="w3"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Due Date" InsertVisible="False" SortExpression="FL_FAULT_LOG.DueDate">
                                                <itemtemplate>
<asp:Label id="GVLblMoniDueDate" runat="server" CssClass="cellData" Text='<%# Eval("Due").toString() %>' __designer:wfdid="w4"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address" InsertVisible="False" SortExpression="P__PROPERTY.ADDRESS1">
                                                <itemtemplate>
<asp:Label id="GVLblMoniAddress" runat="server" CssClass="cellData" Text='<%# Bind("COMPLETEADDRESS") %>' __designer:wfdid="w5"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" InsertVisible="False" SortExpression="FL_CO_APPOINTMENT_STAGE.StageName">
                                                <itemtemplate>
<asp:Label id="GVLblMoniStage" runat="server" CssClass="cellData" Text='<%# Bind("Stage") %>' __designer:wfdid="w6"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Fault Details" InsertVisible="False" SortExpression="FL_FAULT.Description">
                                                <itemtemplate>
<asp:Label id="GVLblMoniDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>' __designer:wfdid="w7"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Operative" InsertVisible="False" SortExpression="E__EMPLOYEE.FIRSTNAME">
                                                <itemtemplate>
<asp:Label id="GVLblMoniOperative" runat="server" CssClass="cellData" Text='<%# Bind("Operative") %>' __designer:wfdid="w8"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="FaultLogID" ShowHeader="False" Visible="False">
                                                <itemtemplate>
<asp:Label id="GVlblMoniFaultLogId" runat="server" Text='<%# Bind("FaultLogID") %>' __designer:wfdid="w14"></asp:Label> 
</itemtemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Over Due" SortExpression="FL_FAULT_LOG.DueDate">
                                                <itemtemplate>
<asp:Label id="lblOverDue" runat="server" Text='<%# Bind("OverDue") %>' __designer:wfdid="w9"></asp:Label> 
</itemtemplate>
                                                <headerstyle wrap="False" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle BackColor="#EFF3FB" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle BackColor="Transparent" ForeColor="Black" HorizontalAlign="Left" />
                                        <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                                        <AlternatingRowStyle BackColor="White" Font-Names="Arial" Font-Size="Small" Wrap="True" />
                                    </cc1:PagingGridView>
                                    <asp:Button ID="btnMoniExport" runat="server" OnClick="btnMoniExport_Click" Text="Export"
                                        UseSubmitBehavior="False" />                                       
                                    <asp:Label ID="lblDownloadMsg" runat="server" CssClass="caption" Text="If dowonload does not start automatically,"
                                        Visible="False" Width="296px"></asp:Label>
                                    <asp:LinkButton ID="lnkNewWindow" runat="server" Visible="False">Click Here</asp:LinkButton>
 
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="ExportProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel_Monitoring"
                                DisplayAfter="50">
                                <ProgressTemplate>
                                    <img src="../../images/buttons/ajax-loader.gif" style="width: 152px; height: 24px" alt="" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </ContentTemplate>
                    </asp:UpdatePanel>
             
                </td>
            </tr>
         </table>
     </div>
</asp:Content>