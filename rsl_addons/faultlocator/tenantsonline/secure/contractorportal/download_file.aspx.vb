Imports System
Imports System.Data
Imports System.Configuration
'Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.Net
Imports System.IO


Partial Public Class DownloadMonitoringFile

    Inherits System.Web.UI.Page
    'Inherits MSDN.SessionPage

    Public fileName As String
    Public fullFileName As String

#Region "Page Init "

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If GetOrgId() = String.Empty Then
        '    Response.Redirect("~/error.aspx")
        'End If
    End Sub
#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region

#Region "Link Button1 Click "

    Protected Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

        DownloadLateFile()
    End Sub
#End Region

#Region "Download Late File "

    Private Sub DownloadLateFile()

        'get the file name from query string
        fileName = Context.Request.QueryString("f")

        If fileName = String.Empty Then
            Response.Redirect("~/error.aspx")
        End If


        'Dim strFilePath As String = Server.MapPath(Request.app & "/projectdata/" & lblpcode.Text & "/IssueAttachments/" & lblPreFix.Text & lblatt.Text)
        Dim strFilePath As String = System.Configuration.ConfigurationManager.AppSettings("contractor_monitoring_file_dir_path") + fileName + ".csv"

        If strFilePath <> "" Then
            If System.IO.File.Exists(strFilePath) Then
                Dim name As String = IO.Path.GetFileName(strFilePath)
                Dim ext As String = IO.Path.GetExtension(strFilePath)
                Dim type As String = "" '" application/force-download"
                If Not IsDBNull(ext) Then
                    ext = LCase(ext)
                End If
                Select Case ext
                    Case ".htm", ".html"
                        type = "text/HTML"
                    Case ".txt"
                        type = "text/plain"
                    Case ".doc", ".rtf"
                        type = "Application/msword"
                    Case ".csv", ".xls"
                        type = "Application/x-msexcel"
                    Case ".pdf"
                        type = "application/pdf"
                    Case ".zip"
                        type = "application/zip"
                    Case ".rar"
                        type = "application/rar"
                    Case Else
                        type = "text/plain"
                End Select
                Response.AppendHeader("content-disposition", "attachment; filename=""" & name & """")
                Response.ContentType = type
                Response.WriteFile(strFilePath)
                Response.End()
            End If
        End If
    End Sub
#End Region

#Region "Get Org Id "
    Private Function GetOrgId()
        'Return ASPSession("ORGID")
        Return 2
    End Function
#End Region
End Class