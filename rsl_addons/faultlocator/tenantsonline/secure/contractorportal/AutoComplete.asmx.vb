Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel



<System.Web.Services.WebService(Namespace:="http://localhost:2525/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<ToolboxItem(False)> _
Public Class AutoComplete
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function RepairAutoComplete(ByVal prefixText As String, ByVal count As Integer) As String()

        'Dim list As StringCollection = DirectCast(HttpContext.Current.Profile("SearchTerms"), StringCollection)



        Dim ACDS As DataSet = New DataSet()
        Dim cpBO As ContractorPortalBO = New ContractorPortalBO()

        Dim cpMngr As ContractorPortalManager = New ContractorPortalManager()
        Dim sugession As List(Of String) = New List(Of String)

        cpMngr.GetRepairData(cpBO, ACDS)

        If count = 0 Then
            count = 10
        End If

        Dim i As Integer

        For i = 0 To ACDS.Tables(0).Rows.Count - 1
            Dim desc As String = ACDS.Tables(0).Rows(i)("val").ToString()
            If desc.ToUpper().Contains(prefixText.ToUpper()) Then
                sugession.Add(desc)
            End If
        Next


        ' ''Check for exception 
        'Dim exceptionFlag As Boolean = cpBO.IsExceptionGenerated
        'If exceptionFlag Then
        '    Return sugession.ToArray()
        'Else
        '    Return sugession.ToArray()
        'End If
        'Dim suggesionArr() As String = {"Ali", "Asim"}
        Return sugession.ToArray

    End Function
    Public Function test(ByVal prefixText As String, ByVal count As Integer) As String()

        Dim sugession() As String = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}

        'count = 10

        Return sugession
    End Function
End Class