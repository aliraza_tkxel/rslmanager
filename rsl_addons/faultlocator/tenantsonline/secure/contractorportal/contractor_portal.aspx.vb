Imports System
'Imports System.Reflection ' For Missing.Value and BindingFlags
'Imports System.Runtime.InteropServices ' For COMException
'Imports Excel

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Partial Public Class contractor_portal
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

#Region "LookUP Methods "
    ''' <summary>
    ''' To fill the Team drop dwon list 
    ''' </summary>
    ''' <remarks> it will take a input parameter </remarks>
    Private Sub GetTeamLookUpVallues()
        Try
            PopulateLookup(ddlTeam, SprocNameConstants.GetTeamLookup)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
    ''' <summary>
    ''' To fill the Location drop dwon list 
    ''' </summary>
    ''' <remarks> it will take a input parameter </remarks>

    Private Sub GetLocationLookUpValues()
        Try
            PopulateLookup(ddlLocation, SprocNameConstants.GetLocationLookup)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

    ''' <summary>
    ''' General function that will be called to populate the drop down lists
    ''' </summary>
    ''' <param name="ddlLookup"></param>
    ''' <param name="spName"></param>
    ''' <remarks></remarks>
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
        End If
    End Sub

    Private Sub PopulateUserLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String, ByVal param As String, ByVal paramName As String)

        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getUserLookUpList(spName, lstLookup, inParam, inParamName, param, paramName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
        End If
    End Sub

    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
        End If
    End Sub

    ''' <summary>
    ''' The area lookup function that will be called to fill the area down lookup 
    ''' </summary>
    ''' <param name="inParam">id </param>
    ''' <param name="inParamName"> stored procedure name</param>
    ''' <remarks></remarks>
    Private Sub GetAreaLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        Try
            PopulateLookup(ddlArea, SprocNameConstants.GetAreaLookup, inParam, inParamName)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

    ''' <summary>
    ''' Lookup functions taking two parameters 
    ''' </summary>
    ''' <param name="inParam"></param>
    ''' <param name="inParamName"></param>
    ''' <remarks></remarks>


    Private Sub GetElementLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        Try
            PopulateLookup(ddlElement, SprocNameConstants.GetElementLookup, inParam, inParamName)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

    ''User Lookup Values for User
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="inParam">The lookup will be populated on the basis of the parent dropdown list index changted event, which will be the first param</param>
    ''' <param name="inParamName"></param>
    ''' <param name="param"> specific users will be displayed in the user drop down list, whose id is passed as 2nd param</param>
    ''' <param name="paramName"></param>
    ''' <remarks></remarks>
    Private Sub GetUserLookUpValues(ByVal inParam As String, ByVal inParamName As String, ByVal param As String, ByVal paramName As String)
        Try
            PopulateUserLookup(ddlUser, SprocNameConstants.GetUserInvoiceLookUp, inParam, inParamName, param, paramName)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
    ''Lookup Values for Patch
    Private Sub GetPatchLookUpVallues()
        Try
            PopulateLookup(ddlPatch, SprocNameConstants.GetPatchLookup)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
    ''Lookup Values for Scheme
    Private Sub GetSchemeLookUpValues()
        Try
            PopulateLookup(ddlScheme, SprocNameConstants.GetSchemeLookup)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
    ''Status Lookup Values for Status
    Private Sub GetStatusLookUpValues()
        Try
            PopulateLookup(ddlStatus, SprocNameConstants.getReportedFaultStatusLookup)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
    ''Priority Lookup Values
    Private Sub GetPriorityLookUpValues()
        Try
            PopulateLookup(ddlPriority, SprocNameConstants.GetPriorityLookup)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

    ''Invoicing Stage Lookup Values
    Private Sub GetInvoicingStageLookupValues()
        Try
            PopulateLookup(ddlStage, SprocNameConstants.GetInvoicingStageLookUp)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Page Init "

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If GetOrgId() = String.Empty Then
        '    Response.Redirect("~/error.aspx")
        'End If
    End Sub

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Page Load Method "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then

            Me.GetLocationLookUpValues()
            Me.GetTeamLookUpVallues()
            Me.GetPatchLookUpVallues()
            Me.GetSchemeLookUpValues()
            Me.GetStatusLookUpValues()
            Me.GetPriorityLookUpValues()
            Me.GetInvoicingStageLookupValues()

            'Me.SaveSearchOptionsInViewState()
            ShowJobSheetPageComponents()
            ddlContractorType.SelectedValue = "JobSheet"
            Me.JobSheetPageLoad()
            UpdatePanel1.Update()



            'Call To Hide Methods
            'Me.HideJobSheetPageComponents()
            Me.HideAppointmentToBeArrangePageComponents()
            Me.HideManageAppointmentPageComponents()
            Me.HideWorkCompletionPageComponents()
            Me.HideInvoicingPageComponents()
            Me.HideMonitoringPageComponents()

            btnInvoiceThisAmount.Attributes.Add("onclick", "javascript:" + btnInvoiceThisAmount.ClientID + ".disabled=true;" + ClientScript.GetPostBackEventReference(btnInvoiceThisAmount, ""))

        End If

    End Sub

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Events"

#Region "Location Lookup Index Change Event"
    Protected Sub ddlLocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex <> 0 Then
            GetAreaLookUpValues(ddlLocation.SelectedValue.ToString(), "@LocationID")
        Else
            ddlArea.Items.Clear()
            ddlArea.Items.Add(New ListItem("Please select", ""))
            ddlElement.Items.Clear()
            ddlElement.Items.Add(New ListItem("Please select", ""))
            UpdatePanel_Search.Update()
        End If
    End Sub
#End Region

#Region "Area Lookup Index Change Event"
    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlArea.SelectedIndex <> 0 Then

            GetElementLookUpValues(ddlArea.SelectedValue.ToString, "@AreaID")

        Else
            ddlElement.Items.Clear()
            ddlElement.Items.Add(New ListItem("Please select", ""))

        End If

    End Sub
#End Region

#Region "Team Lookup Index Change Event"
    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If ddlTeam.SelectedIndex <> 0 Then

            Me.GetUserLookUpValues(ddlTeam.SelectedValue.ToString, "@TeamId", Me.GetOrgId(), "@orgId")

        Else
            ddlUser.Items.Clear()
            ddlUser.Items.Add(New ListItem("Please select", ""))
        End If

    End Sub

#End Region

#Region "Clear Button Click Event"
    ''' <summary>
    ''' The Clear Button Handler 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'ClearAll()
        GetControlReset()
        UpdatePanel_Search.Update()

    End Sub
#End Region

#Region "Search Button Click"

    ''' <summary>
    ''' handler for invoicing search button 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnInvoicingSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ddlContractorType.SelectedValue = "JobSheet" Then

            ''Save Job Sheet Paging Attributes
            Me.SaveJobSheetPagingAttrib(ApplicationConstants.ContractorPortalJobSheetIntialPageIndex, ApplicationConstants.ContractorPortalJobSheetResultPerPage, ApplicationConstants.ContractorPortalJobSheetSortBy, ApplicationConstants.ContractorPortalJobSheetSortOrder)

            'Hide all other page components/elements other than job sheet
            Me.HideAllOtherPageComponents("JobSheet")

            'Save Search Options In ViewState
            Me.SaveSearchOptionsInViewState()

            'Get Job Sheet Data
            Me.GetJobSheetData()

            'Update the Job Sheet Grid Panel
            Me.UpdateJobSheetGridPanel()
        ElseIf Me.ddlContractorType.SelectedValue = "Appointment" Then

            'Hide all other page components/elements other than appointment to be arranged
            Me.HideAllOtherPageComponents("Appointment")

            'Get Appointment to be Arrange Data
            Me.PopulateTBASearchResults()
        ElseIf Me.ddlContractorType.SelectedValue = "Manage" Then

            'Hide all other page components/elements other than manage appointment
            Me.HideAllOtherPageComponents("Manage")

            'Clear the message label
            Me.ClearMAMessage()

            'Get Manage Appointment Data
            Me.GetManageAppointmentData()

        ElseIf Me.ddlContractorType.SelectedValue = "Work" Then

            'Hide all other page components/elements other than work completion
            Me.HideAllOtherPageComponents("Work")

            'Get Work Completion Data
            Me.GetWorkCompletionData()

        ElseIf Me.ddlContractorType.SelectedValue = "Invoicing" Then

            'Hide all other page components/elements other than invoicing
            Me.HideAllOtherPageComponents("Invoicing")

            'Get Invoicing Data
            Me.GetInvoicingData()
            Me.txtInvoicingValue.Text = ""          ' To clear the already displayed value from the invoice value txtbox. 
            Invoicing_UpdatePanel.Update()

        ElseIf Me.ddlContractorType.SelectedValue = "Monitoring" Then

            'Hide all other page components/elements other than monitoring
            Me.HideAllOtherPageComponents("Monitoring")
            Me.SaveSearchOptionsInViewState()

            'Get Monitoring Data
            Me.GetMonitoringSearchPanelData()

            'Get monitoring time base fault count
            'Me.SetMonitoringTimeBaseLinkButtons()

        End If
    End Sub
#End Region

#Region "Contractor Type Index Change Event"
    Protected Sub ddlContractorType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ddlContractorType.SelectedValue = "JobSheet" Then

            'Hide all other page components/elements & Show Job Sheet Grid
            Me.HideAllOtherPageComponents("JobSheet")
            Me.ShowJobSheetPageComponents()

            'Load Job Sheet Data
            Me.JobSheetPageLoad()
        ElseIf Me.ddlContractorType.SelectedValue = "Appointment" Then

            'Hide all other page components/elements & Show Appointment To Be Arranged Grid
            Me.HideAllOtherPageComponents("Appointment")
            Me.ShowAppointmentToBeArrangePageComponents()

            'Load Appointment To Be Arranged Data            
            Me.AppointmentToBeArrangePageLoad()
        ElseIf Me.ddlContractorType.SelectedValue = "Manage" Then

            'Hide all other page components/elements & Show Manage Grid
            Me.HideAllOtherPageComponents("Manage")
            Me.ShowManageAppointmentPageComponents()

            'Load Manage Appointment Data
            Me.ResetApplicationConstant()
            Me.ManageAppointmentPageLoad()
        ElseIf Me.ddlContractorType.SelectedValue = "Work" Then

            'Hide all other page components/elements & Show Work Completion Grid
            Me.HideAllOtherPageComponents("Work")
            Me.ShowWorkCompletionPageComponents()

            'Load Work Completion Data
            Me.ResetApplicationConstant()
            Me.WorkCompletionPageLoad()
        ElseIf Me.ddlContractorType.SelectedValue = "Invoicing" Then

            'Hide all other page components/elements & Show Invoicing Grid
            Me.HideAllOtherPageComponents("Invoicing")
            Me.ShowInvoicingPageComponents()

            'Load Invoicing Data
            Me.InvoicingPageLoad()
        ElseIf Me.ddlContractorType.SelectedValue = "Monitoring" Then

            'Hide all other page components/elements & Show Monitoring Grid
            Me.HideAllOtherPageComponents("Monitoring")
            Me.ShowMonitoringPageComponents()

            'Load Monitoring Data
            Me.MonitoringPageLoad()
        End If
    End Sub
#End Region

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Generic Methods & Functions "

#Region "Hide All Other Page Components "

    Private Sub HideAllOtherPageComponents(ByVal donotHideThisPageComponent As String)
        If donotHideThisPageComponent = "JobSheet" Then

            Me.HideAppointmentToBeArrangePageComponents()
            Me.HideManageAppointmentPageComponents()
            Me.HideWorkCompletionPageComponents()
            Me.HideInvoicingPageComponents()
            Me.HideMonitoringPageComponents()

        ElseIf donotHideThisPageComponent = "Appointment" Then

            Me.HideJobSheetPageComponents()
            Me.HideManageAppointmentPageComponents()
            Me.HideWorkCompletionPageComponents()
            Me.HideInvoicingPageComponents()
            Me.HideMonitoringPageComponents()

        ElseIf donotHideThisPageComponent = "Manage" Then

            Me.HideJobSheetPageComponents()
            Me.HideAppointmentToBeArrangePageComponents()
            Me.HideWorkCompletionPageComponents()
            Me.HideInvoicingPageComponents()
            Me.HideMonitoringPageComponents()

        ElseIf donotHideThisPageComponent = "Work" Then

            Me.HideJobSheetPageComponents()
            Me.HideAppointmentToBeArrangePageComponents()
            Me.HideManageAppointmentPageComponents()
            Me.HideInvoicingPageComponents()
            Me.HideMonitoringPageComponents()

        ElseIf donotHideThisPageComponent = "Invoicing" Then

            Me.HideJobSheetPageComponents()
            Me.HideAppointmentToBeArrangePageComponents()
            Me.HideManageAppointmentPageComponents()
            Me.HideWorkCompletionPageComponents()
            Me.HideMonitoringPageComponents()

        ElseIf donotHideThisPageComponent = "Monitoring" Then

            Me.HideJobSheetPageComponents()
            Me.HideAppointmentToBeArrangePageComponents()
            Me.HideManageAppointmentPageComponents()
            Me.HideWorkCompletionPageComponents()
            Me.HideInvoicingPageComponents()
        End If
    End Sub

#End Region

#Region "Update - the Heading Update Panel"
    Private Sub UpdateGridHeadingPanel()
        Me.UpdatePanel_GVHeading.Update()
    End Sub
#End Region

#Region "Update - the All Grid Update Panel"
    Private Sub UpdateAllGridPanel()
        Me.UpdatePanel_AllPageGrids.Update()
    End Sub
#End Region

#Region "Get Org Id "
    Private Function GetOrgId()
        Return ASPSession("ORGID")
        'Return 1270
    End Function
#End Region
#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Job Sheet - Coded By Noor Muhammad - January,2009 "

#Region "Job Sheet Events "

#Region "Click Event For Select All In Job Sheet "

    Protected Sub btnJobSheetSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnJobSheetSelectAll.Click
        Me.SelectAllJobSheetFaultsInGrid()
    End Sub

#End Region

#Region "Click Event For Accept Selected  In Job Sheets "

    Protected Sub btnJobSheetAcceptSelected_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnJobSheetAcceptSelected.Click

        Dim pageIndex = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetPageIndexKey)
        Dim pageSize = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetPageSizeKey)
        Dim sortBy = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetSortByKey)
        Dim sortOrder = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetSortOrderKey)
        GVJobSheets.PageIndex = pageIndex

        Me.SaveJobSheetPagingAttrib(pageIndex, pageSize, sortBy, sortOrder)
        Me.UpdateSelectedJobSheetFaults()
    End Sub

#End Region

#Region "On Page Index Changing of Gob Sheet Grid "

    Protected Sub GVJobSheets_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            'Set the grid index
            Me.GVJobSheets.PageIndex = e.NewPageIndex

            'ApplicationConstants.jo()
            'Save Job Sheet Paging Attributes
            ''  Me.SaveJobSheetPagingAttrib(Me.GVJobSheets.PageIndex, ApplicationConstants.ContractorPortalJobSheetResultPerPage, ApplicationConstants.ContractorPortalJobSheetSortBy, ApplicationConstants.ContractorPortalJobSheetSortOrder)

            SaveJobSheetPagingAttrib(Me.GVJobSheets.PageIndex)
            
            'Get Job Sheet Data
            Me.GetJobSheetData()

        Catch ex As Exception
            'redirect to error page
            Response.Redirect("~/error.aspx")
        End Try
    End Sub

#End Region

#Region "JobSheet link Button Click"
    Protected Sub GVLblJobSheetJSNumber_Click1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HideJobSheetEmptyGridRows()
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        Dim jsnumber As String = btn.CommandArgument.ToString()
        GetJobSheetPopupData(jsnumber)
        GetJobSheetPopupAsbestosData(jsnumber)
        Me.updatePanel_JbSSmry.Visible = True
        Me.UpdateAllGridPanel()
        ModalPopup_JSsummary.Show()
    End Sub
#End Region

#Region "lnkJobSheetNumber Click Event "

    Protected Sub lnkJobSheetNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)        
        HideJobSheetEmptyGridRows()
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        Dim jsnumber As String = btn.CommandArgument.ToString()
        GetJobSheetPopupData(jsnumber)
        GetJobSheetPopupAsbestosData(jsnumber)
        Me.updatePanel_JbSSmry.Visible = True
        Me.UpdateAllGridPanel()
        ModalPopup_JSsummary.Show()

    End Sub
#End Region


#Region "GVJobSheets_Sorting"



    Protected Sub GVJobSheets_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)

        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.ContractorProtalJobSheetSortByKey) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVJobSheets.PageIndex = ApplicationConstants.ContractorPortalJobSheetIntialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.ContractorProtalJobSheetSortOrderKey), String) = ApplicationConstants.JobSheetDesSortOrder Then

            'ViewState(ApplicationConstants.JobSheetSortByViewState) = ApplicationConstants.JobSheetAscSortOrder
            ViewState(ApplicationConstants.ContractorProtalJobSheetSortOrderKey) = ApplicationConstants.JobSheetAscSortOrder
        Else
            ViewState(ApplicationConstants.ContractorProtalJobSheetSortOrderKey) = ApplicationConstants.JobSheetDesSortOrder
        End If

        'If DirectCast(ViewState(ApplicationConstants.JobSheetIsSearchViewState), Boolean) = True Then
        '    GetJobSheetData()
        'Else
        GetJobSheetData()

        'End If
    End Sub
#End Region

#End Region

#Region "JobSheet Methods & Functions"

#Region "Job Sheet Page Load "
    Private Sub JobSheetPageLoad()
        ''Save Job Sheet Paging Attributes
        Me.SaveJobSheetPagingAttrib(ApplicationConstants.ContractorPortalJobSheetIntialPageIndex, ApplicationConstants.ContractorPortalJobSheetResultPerPage, ApplicationConstants.ContractorPortalJobSheetSortBy, ApplicationConstants.ContractorPortalJobSheetSortOrder)

        'Save Search Options In ViewState
        Me.SaveSearchOptionsInViewState()

        'Get Job Sheet Data
        Me.GetJobSheetData()
    End Sub
#End Region

#Region "Get JobSheet Data"

    Private Sub GetJobSheetData()
        Try
            'Create the object of contractor portal bo
            Dim conBO As ContractorPortalBO = New ContractorPortalBO()

            'Create the object of contractor business layer
            Dim conBL As ContractorPortalManager = New ContractorPortalManager()

            'Create the obj of dataset
            Dim jobSheetDS As DataSet = New DataSet()

            'Set the attributes of contractor bo
            Me.SetContractorBOAttributes(conBO)

            'Set the paging attributes of contractor bo
            Me.SetContractorBOPagingAttributes(conBO)

            'Set the paging attributes of Grid
            Me.SetJobSheetGridPagingAttributes(conBO)

            'Call to Busineess Layer
            conBL.GetJobSheetData(conBO, jobSheetDS)

            'Hide Buttons if no record found else show buttons if already hidden
            Me.ShowHideJobSheetButtons(jobSheetDS)

            'Populating the data grid with data set
            ViewState.Add(ApplicationConstants.FaultContractorJobSheetDataSet, jobSheetDS)

            ''Set & bind dataset
            Me.SetandBindJobSheetDataSet(jobSheetDS)
            Me.HideShowGridCheckBox(jobSheetDS)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Select All Job Sheet Faults In Grid "
    Private Sub SelectAllJobSheetFaultsInGrid()
        Try
            'This function avoid the display of empty data grid rows where 
            'less records are being displayed then page size of grid
            Me.HideJobSheetEmptyGridRows()

            'Declare the checkbox object
            Dim chkBoxSelected As CheckBox

            'Declare the GridView row obj                       
            Dim gridRow As GridViewRow

            'Loop through grid to get select all check boxes
            For Each gridRow In GVJobSheets.Rows
                chkBoxSelected = gridRow.FindControl("chkBoxAcceptSelected")
                chkBoxSelected.Checked = True
            Next
        Catch ex As Exception
            Response.Redirect(".~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Update Selected JobSheet Faults"
    Private Sub UpdateSelectedJobSheetFaults()
        Try
            'Declare the checkbox object
            Dim chkBoxSelected As CheckBox

            'Declare the GridView row obj
            Dim row As GridViewRow

            'Declare the arraylist object
            Dim selectedFaults As ArrayList = New ArrayList()
            Dim lblFaultLogId

            'Declare the contractor portal bo object
            Dim conBO As ContractorPortalBO = New ContractorPortalBO()

            'Declare the business layer object
            Dim conBL As ContractorPortalManager = New ContractorPortalManager()

            'Loop through grid to get select all check boxes
            For Each row In GVJobSheets.Rows
                chkBoxSelected = row.Cells(8).FindControl("chkBoxAcceptSelected")

                If chkBoxSelected.Checked Then
                    lblFaultLogId = row.FindControl("lblJSFaultLogId")
                    selectedFaults.Add(lblFaultLogId.Text())
                End If

            Next

            If selectedFaults.Count > 0 Then

                conBO.OrgId = Me.GetOrgId()                

                'Call to BL function to save selected faults
                conBL.UpdateSelectedJobSheetFaults(selectedFaults, conBO)

                Me.GetJobSheetData()
            Else
                'This function avoid the display of empty data grid rows where 
                'less records are being displayed than page size of grid
                Me.HideJobSheetEmptyGridRows()
            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Hide Show Grid Check Box"

    Private Sub HideShowGridCheckBox(ByVal jobSheetDS As DataSet)

        'Declare the checkbox object
        Dim chkBoxSelected As CheckBox

        'Declare the GridView row obj
        Dim row As GridViewRow

        'Declare the counter variable
        Dim counter As Integer = New Integer()

        'Loop through grid to get select all check boxes
        For Each row In GVJobSheets.Rows

            Dim isSelected = jobSheetDS.Tables(0).Rows.Item(counter).ItemArray(2)

            If isSelected Then
                chkBoxSelected = row.FindControl("chkBoxAcceptSelected")
                chkBoxSelected.Visible = False
            End If

            counter = counter + 1
        Next
    End Sub
#End Region

#Region "Update - The Job Sheet Update Panel"
    Private Sub UpdateJobSheetGridPanel()
        Me.UpdatePanel_JobSheetGrid.Update()
    End Sub
#End Region

#Region "Set and Bind Data Set"
    Private Sub SetandBindJobSheetDataSet(ByRef jobSheetDS As DataSet)
        Try
            Me.GVJobSheets.DataSource = jobSheetDS
            Me.GVJobSheets.DataBind()

            'We will count the total number of records
            Dim pageCount As String
            pageCount = jobSheetDS.Tables(0).Rows.Count.ToString()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "Save Job Sheet Paging Attributes"

    Private Sub SaveJobSheetPagingAttrib(ByRef pageIndex As Integer, ByRef resultPerPage As Integer, ByVal sortBy As String, ByVal sortOrder As String)
        Try
            'Set the page index
            ViewState.Add(ApplicationConstants.ContractorProtalJobSheetPageIndexKey, pageIndex)

            'Set the page size
            ViewState.Add(ApplicationConstants.ContractorProtalJobSheetPageSizeKey, resultPerPage)

            'Set the Sort by
            ViewState.Add(ApplicationConstants.ContractorProtalJobSheetSortByKey, sortBy)

            'Set the sort order (Ascending /Descending)
            ViewState.Add(ApplicationConstants.ContractorProtalJobSheetSortOrderKey, sortOrder)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub

    Private Sub SaveJobSheetPagingAttrib(ByRef pageIndex As Integer)
        Try
            'Set the page index
            ViewState.Add(ApplicationConstants.ContractorProtalJobSheetPageIndexKey, pageIndex)

            'Set the page size
            ''ViewState.Add(ApplicationConstants.ContractorProtalJobSheetPageSizeKey, resultPerPage)

            'Set the Sort by
            ''ViewState.Add(ApplicationConstants.ContractorProtalJobSheetSortByKey, sortBy)

            'Set the sort order (Ascending /Descending)
            ''ViewState.Add(ApplicationConstants.ContractorProtalJobSheetSortOrderKey, sortOrder)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "Set Contractor BO Paging Attributes"
    Private Sub SetContractorBOPagingAttributes(ByRef conBO As ContractorPortalBO)
        Try
            'setting the range of results to be fetched from db (pageindex*pagesize),sort by ,sort order
            'conBO.PageIndex = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetPageIndexKey)
            'conBO.PageSize = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetPageSizeKey)
            conBO.PageIndex = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetPageIndexKey)
            conBO.PageSize = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetPageSizeKey)
            conBO.SortBy = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetSortByKey)
            conBO.SortOrder = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetSortOrderKey)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Set Job Sheet Grid Pagging Attributes"
    Private Sub SetJobSheetGridPagingAttributes(ByRef conBO As ContractorPortalBO)
        Try
            If conBO.PageIndex.Equals(0) Then
                'setting initial page index of gridview by default its zero
                GVJobSheets.PageIndex = ApplicationConstants.ContractorPortalJobSheetIntialPageIndex

                'getting and setting row count of resultset
                GVJobSheets.VirtualItemCount = GetJobSheetFaultCount(conBO)
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "Get Job Sheet Grid Paging Attributes From View State "
    Private Sub GetJobSheetGridPagingAttributesFromViewState()
        'Get the page index
        GVJobSheets.PageIndex = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetPageIndexKey)

        'Get the page size
        GVJobSheets.PageSize = ViewState.Item(ApplicationConstants.ContractorProtalJobSheetPageSizeKey)
    End Sub
#End Region

#Region "Get Job Sheet Fault Count"

    Private Function GetJobSheetFaultCount(ByRef conBO As ContractorPortalBO) As Integer
        Try
            'Create the object of contractor business layer
            Dim conBL As ContractorPortalManager = New ContractorPortalManager()
            Return (conBL.GetJobSheetFaultCount(conBO))
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Function
#End Region

#Region "Set Contractor BO Attributes"
    Private Sub SetContractorBOAttributes(ByRef conBO As ContractorPortalBO)
        Try
            conBO.OrgId = Me.GetOrgId()
            conBO.LocationId = ViewState.Item("LocationId")
            conBO.AreaId = ViewState.Item("AreaId")
            conBO.ElementId = ViewState.Item("ElementId")
            conBO.TeamId = ViewState.Item("TeamId")
            conBO.UserId = ViewState.Item("UserId")
            conBO.PatchId = ViewState.Item("PatchId")
            conBO.SchemeId = ViewState.Item("SchemeId")
            conBO.PostCode = ViewState.Item("PostCode")
            conBO.Due = ViewState.Item("Due")
            conBO.PriorityId = ViewState.Item("PriorityId")
            conBO.StatusId = ViewState.Item("StatusId")
            conBO.JsNumber = ViewState.Item("JsNumber")
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try



    End Sub
#End Region

#Region "Save Search Options In View State"
    Private Sub SaveSearchOptionsInViewState()
        Try
            ViewState.Add("OrgId", Me.GetOrgId())                    
            ViewState.Add("LocationId", IIf(Me.ddlLocation.SelectedValue <> String.Empty, Me.ddlLocation.SelectedValue, 0))
            ViewState.Add("AreaId", IIf(Me.ddlArea.SelectedValue <> String.Empty, Me.ddlArea.SelectedValue, 0))
            ViewState.Add("ElementId", IIf(Me.ddlElement.SelectedValue <> String.Empty, Me.ddlElement.SelectedValue, 0))
            ViewState.Add("TeamId", IIf(Me.ddlTeam.SelectedValue <> String.Empty, Me.ddlTeam.SelectedValue, 0))
            ViewState.Add("UserId", IIf(Me.ddlUser.SelectedValue <> String.Empty, Me.ddlUser.SelectedValue, 0))
            ViewState.Add("PatchId", IIf(Me.ddlPatch.SelectedValue <> String.Empty, Me.ddlPatch.SelectedValue, 0))
            ViewState.Add("SchemeId", IIf(Me.ddlScheme.SelectedValue <> String.Empty, Me.ddlScheme.SelectedValue, 0))
            ViewState.Add("PostCode", IIf(Me.txtPostCode.Text <> "", Me.txtPostCode.Text, ""))
            ViewState.Add("Due", IIf(Me.txtDueDate.Text.ToString <> String.Empty, Me.txtDueDate.Text, ""))
            ViewState.Add("PriorityId", IIf(Me.ddlPriority.SelectedValue <> String.Empty, Me.ddlPriority.SelectedValue, 0))
            ViewState.Add("StatusId", IIf(Me.ddlStatus.SelectedValue <> String.Empty, Me.ddlStatus.SelectedValue, 0))
            ViewState.Add("JsNumber", IIf(Me.txtJS.Text <> String.Empty, Me.txtJS.Text, 0))
            'ViewState.Add(ApplicationConstants.JobSheetIsSearchViewState, True)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "Show Job Sheet Page Components "
    Private Sub ShowJobSheetPageComponents()
        Try
            Me.lblGVHeading.Text = "Job Sheet"
            Me.GVJobSheets.Visible = True
            Me.btnJobSheetSelectAll.Visible = True
            Me.btnJobSheetAcceptSelected.Visible = True
            Me.UpdateJobSheetGridPanel()
            Me.UpdateGridHeadingPanel()
            Me.UpdatePanel_JobSheetGrid.Visible = True            
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Hide Job Sheet Page Components "
    Private Sub HideJobSheetPageComponents()
        Try
            Me.GVJobSheets.Visible = False
            Me.btnJobSheetSelectAll.Visible = False
            Me.btnJobSheetAcceptSelected.Visible = False
            Me.UpdateJobSheetGridPanel()
            Me.UpdateGridHeadingPanel()
            Me.UpdatePanel_JobSheetGrid.Visible = False
            Me.updatePanel_JbSSmry.Visible = False
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Show Hide Job Sheet Buttons "
    Private Sub ShowHideJobSheetButtons(ByVal jobSheetDS As DataSet)
        Try
            'Hide Buttons if no record found else show buttons if already hidden
            If jobSheetDS.Tables(0).Rows.Count = 0 Then
                Me.btnJobSheetSelectAll.Visible = False
                Me.btnJobSheetAcceptSelected.Visible = False
            Else
                If Me.btnJobSheetSelectAll.Visible = False Then
                    Me.btnJobSheetSelectAll.Visible = True
                End If

                If Me.btnJobSheetAcceptSelected.Visible = False Then
                    Me.btnJobSheetAcceptSelected.Visible = True
                End If
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "Hide Job Sheet Empty Grid Rows "
    'This function avoid the display of empty data grid rows where 
    'less records are being displayed than page size of grid
    Private Sub HideJobSheetEmptyGridRows()
        Try

            Dim jobSheetDS As DataSet

            'Set job sheet grid paging attributes from view state so that grid shouldn't show empty rows
            Me.GetJobSheetGridPagingAttributesFromViewState()

            'Get the job sheet data set from view state so that grid shouldn't show empty rows
            jobSheetDS = ViewState.Item(ApplicationConstants.FaultContractorJobSheetDataSet)

            ''Set & bind dataset so that grid shouldn't show empty rows
            Me.SetandBindJobSheetDataSet(jobSheetDS)

            Me.HideShowGridCheckBox(jobSheetDS)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Get Jobsheet Popup Data"

    Private Sub GetJobSheetPopupData(ByRef jsNumber As String)
        Try
            'Create the object of contractor portal bo
            Dim conBO As ContractorPortalBO = New ContractorPortalBO()

            'Create the object of contractor business layer
            Dim conBL As ContractorPortalManager = New ContractorPortalManager()

            'Create the obj of dataset
            Dim jobSheetDS As DataSet = New DataSet()

            'Call to Busineess Layer
            conBL.GetJSSummaryPopupData(jobSheetDS, jsNumber)

            grdCustomerReportedFaults.DataSource = Nothing
            grdCustomerReportedFaults.DataSource = jobSheetDS
            grdCustomerReportedFaults.DataBind()
            BindJobSheetPopupData(jobSheetDS)
            updatePanel_JbSSmry.Update()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub

#End Region

#Region "Bind Jobsheet Popup Data"
    Private Sub BindJobSheetPopupData(ByRef jobsheetDS As DataSet)
        Try
            If jobsheetDS.Tables(0).Rows.Count > 0 Then

                lblPostCode1.Text = jobsheetDS.Tables(0).Rows(0)("POSTCODE").ToString()
                lblHouseNum.Text = jobsheetDS.Tables(0).Rows(0)("HOUSENUMBER").ToString()
                lblDue1.Text = UtilityFunctions.FormatDate(jobsheetDS.Tables(0).Rows(0)("DueDate").ToString())
                'lblTitle1.Text = jobsheetDS.Tables(0).Rows(0)("TitleDescription").ToString()
                lblContractor.Text = jobsheetDS.Tables(0).Rows(0)("Contractor").ToString()
                lblPopupPriority.Text = jobsheetDS.Tables(0).Rows(0)("PriorityName").ToString()
                lblResponseTime.Text = jobsheetDS.Tables(0).Rows(0)("FResponseTime").ToString()
                lblJSNumber.Text = jobsheetDS.Tables(0).Rows(0)("JSNumber").ToString()
                lblOrderDate.Text = UtilityFunctions.FormatDate(jobsheetDS.Tables(0).Rows(0)("OrderDate").ToString())
                lblTenantName.Text = jobsheetDS.Tables(0).Rows(0)("TitleDescription").ToString() & " " & jobsheetDS.Tables(0).Rows(0)("FIRSTNAME").ToString() & " " & jobsheetDS.Tables(0).Rows(0)("MIDDLENAME").ToString() & " " & jobsheetDS.Tables(0).Rows(0)("LASTNAME").ToString()
                lblAddressPopup.Text = jobsheetDS.Tables(0).Rows(0)("ADDRESS1").ToString()
                lblTown.Text = jobsheetDS.Tables(0).Rows(0)("TOWNCITY").ToString()
                lblCountry.Text = jobsheetDS.Tables(0).Rows(0)("COUNTY").ToString()
                lblTelephonePopup.Text = jobsheetDS.Tables(0).Rows(0)("TEL").ToString()
                lblMobile.Text = jobsheetDS.Tables(0).Rows(0)("MOBILE").ToString()
                lblEmail.Text = jobsheetDS.Tables(0).Rows(0)("EMAIL").ToString()
                lblAsbestos.Text = jobsheetDS.Tables(0).Rows(0)("AsbestosRisk").ToString()
                lblProblemDays.Text = "This fault has existed for:" + jobsheetDS.Tables(0).Rows(0).Item("ProblemDays").ToString() & " day(s)"
                lblRecuringProblem.Text = jobsheetDS.Tables(0).Rows(0).Item("RecurringProblem").ToString()
                lblCommunalArea.Text = jobsheetDS.Tables(0).Rows(0).Item("CommunalProblem").ToString()
                lblNotes.Text = jobsheetDS.Tables(0).Rows(0).Item("Notes").ToString()
                txtPreferredContactTime.Text = IIf(jobsheetDS.Tables(0).Rows(0).Item("PreferredContactDetails").ToString() <> "", jobsheetDS.Tables(0).Rows(0).Item("PreferredContactDetails"), "")
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Get Jobsheet Popup Asbestos Data"

    Private Sub GetJobSheetPopupAsbestosData(ByRef jsNumber As String)
        Try
            'Create the object of contractor portal bo
            Dim conBO As ContractorPortalBO = New ContractorPortalBO()

            'Create the object of contractor business layer
            Dim conBL As ContractorPortalManager = New ContractorPortalManager()

            'Create the obj of dataset
            Dim jobSheetAbsetosDS As DataSet = New DataSet()

            'Call to Busineess Layer
            conBL.GetJSSummaryPopupAsbestosData(jobSheetAbsetosDS, jsNumber)

            gvAsbestosList.DataSource = Nothing
            gvAsbestosList.DataSource = jobSheetAbsetosDS
            gvAsbestosList.DataBind()

            updatePanel_JbSSmry.Update()

        Catch ex As Exception
            Response.Redirect("~/errorAbestos.aspx")
        End Try
    End Sub

#End Region

#End Region

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Appointment To Be Arranged - Coded By Usman Sharif - January,2009 "

#Region "Appointment To Be Aarranged - Lookup Methods"

#Region "Get Operative LookUp Values "

    ''' <summary>
    ''' GetOperativeLookUpValues
    ''' its a method which fill operative lookup placed on TBA Popup with values from database
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetTBAOperativeLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        Try
            PopulateLookup(ddlTBAOperative, SprocNameConstants.GetOperativeLookup, inParam, inParamName)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try



    End Sub
#End Region

#End Region

#Region "Appointment To Be Aarranged - Events"

#Region "Button Click "

#Region "btn TBA Save Click "

    Protected Sub btnTBASave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim errorOccur As Boolean = False

        Dim TBADate As String = txtTBADate.Text.ToString()
        Dim TBAHour = ddlTBAHour.SelectedValue
        Dim TBAMin = ddlTBAMin.SelectedValue
        Dim TBASec = "0"
        Dim TBATimeFormat = ddlTBAAMPM.SelectedValue.ToString()

        If TBATimeFormat = 1 And TBAHour <> 12 Then
            TBAHour = TBAHour + 12
        ElseIf TBATimeFormat = 0 And TBAHour.ToString() = 12 Then
            TBAHour = TBAHour + 12
        End If

        If (txtTBADate.Text = "") Then
            lblTBAError.Text = "Please Enter Date"
            errorOccur = True
        Else

            If (ddlTBAOperative.SelectedItem.ToString() = "Please select") Then

                lblTBAError.Text = "Please select an operative from operative dropdown"
                errorOccur = True
            Else
                SaveAppointment()
            End If
        End If

        'This function avoid the display of empty data grid rows where 
        'less records are being displayed than page size of grid
        Me.HideTBAEmptyGridRows()



    End Sub
#End Region

#Region "img TBA Popup Click "


    Protected Sub imgTBAPopup_Command(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)


        Dim CustomerId As String = ""

        pnlVul.Visible = False
        pnlComm.Visible = False
        pnlRisk.Visible = False
        pnlGS.Visible = False

        CustomerId = e.CommandArgument.ToString()
        GetTBAVulnerability(CType(CustomerId, Integer))
        GetGasServicingAppt(CType(CustomerId, Integer))
        GetTBACommunication(CType(CustomerId, Integer))
        GetTBARisk(CType(CustomerId, Integer))
        updatePanel_TBAPopup.Update()

        modalPopupExtender_TBA.Show()
    End Sub


#End Region

#End Region

#Region "Appointment To Be Aarranged - Grid Events"

#Region "GV TBA Page Index Changing "

    Protected Sub GVTBA_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVTBA.PageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreTBAResultSummary()
        Dim faultDS As DataSet = New DataSet()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.AppointmentTBAIsSearchViewState), Boolean) = True Then
            'PopulateTBASearchResults()
            GetTBASearchResults()
        Else
            ' BindFaultGrid()
        End If
    End Sub
#End Region

#Region "GV TBA Sorting "

    Protected Sub GVTBA_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.AppointmentTBASortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVTBA.PageIndex = ApplicationConstants.AppointmentTBAInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.AppointmentTBASortOrderViewSate), String) = ApplicationConstants.AppointmentTBADESCSortOrder Then
            ViewState(ApplicationConstants.AppointmentTBASortOrderViewSate) = ApplicationConstants.AppointmentTBAASCESortOrder
        Else
            ViewState(ApplicationConstants.AppointmentTBASortOrderViewSate) = ApplicationConstants.AppointmentTBADESCSortOrder
        End If

        'populating gridview with new results
        If DirectCast(ViewState(ApplicationConstants.AppointmentTBAIsSearchViewState), Boolean) = True Then
            GetTBASearchResults()
        Else
            'BindFaultGrid()
        End If
    End Sub
#End Region



#End Region

#End Region



#Region "Appointment To Be Aarranged - Methods & Functions "

#Region "Appointment ToBe Arrange - Page Load "

    Private Sub AppointmentToBeArrangePageLoad()
        Try
            Me.GetTBAOperativeLookUpValues(Me.GetOrgId().ToString(), "ORGID")           
            'storing in view state boolean value indicating , results being displayed in gridview are 
            'from search or not
            ViewState(ApplicationConstants.AppointmentTBAIsSearchViewState) = True
            'storing current sort order of any column asc or dsen
            ViewState(ApplicationConstants.AppointmentTBASortOrderViewSate) = ApplicationConstants.AppointmentTBADESCSortOrder
            'function used for storing search parameters which will be used for further page requests if result is 
            'multipage
            SaveTBASearchOptions()
            'setting initial page index of gridview by default its zero
            GVTBA.PageIndex = ApplicationConstants.CustFaultInitialPageIndex
            'getting and setting row count of resultset
            GVTBA.VirtualItemCount = GetTBASearchRowCount()
            ''''''''''''''''''''''''''
            ddlLocation.Focus()
            ''Populate Grid for the First Time
            ' BindFaultGrid()
            PopulateTBASearchResults()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub

#End Region

#Region "Method to Get AppointmentTBA Data"

    ''' <summary>
    ''' GetTBADataPage
    ''' its a method which is used to get a datapage for data grid. it takes in some parameters and fetch
    ''' records from database and fill a dataset to be displayed in grid
    ''' </summary>
    ''' <param name="pageIndex">current page index of datagrid</param>
    ''' <param name="pageSize">page size of datagrid for paging</param>
    ''' <param name="sortExpression">current sort expression on which records have to be sorted</param>
    ''' <param name="TBADS">dataset which will be filled with data</param>
    ''' <param name="tbaSearchBO">a search Business object which have all the search options in it</param>
    ''' <remarks></remarks>
    Public Sub GetTBADataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef TBADS As DataSet, ByVal tbaSearchBO As TBASearchBO)

        Me.ClearTBAMessage()
        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.AppointmentTBASortByViewState) Is Nothing) Then
            tbaSearchBO.SortBy = CType(ViewState(ApplicationConstants.AppointmentTBASortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use fualtLogid as a default
            tbaSearchBO.SortBy = ApplicationConstants.AppointmentTBASortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        tbaSearchBO.PageIndex = pageIndex
        tbaSearchBO.RowCount = pageSize

        'reteriving sort order from viewstate
        tbaSearchBO.SortOrder = DirectCast(ViewState(ApplicationConstants.AppointmentTBASortOrderViewSate), String)
        Dim tbaMngr As ContractorPortalManager = New ContractorPortalManager()
        'passing reference of dataset and passing tbaSerachBO to GetTBASearchResults function
        'of ContractorPortalManager
        TBADS = tbaMngr.GetTBASearchData(tbaSearchBO)
        'storing number of rows in the result
        ViewState("NumberofRows") = TBADS.Tables(0).Rows.Count

        ''Check for exception 
        Dim exceptionFlag As Boolean = tbaSearchBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

    End Sub

#End Region

#Region "Method to Save TBA Search Options"

    ''' <summary>
    ''' SaveTBASearchOptions
    ''' this method is used to save searchoptions for Appointments to be arranged and saved them in a business object
    ''' then save that business object in Page View state to make it available for further use.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SaveTBASearchOptions()
        Try
            'make FaultSearchBO object and store the search options entered by user untill he/she enters again
            'in this case the values will be overidden
            Dim tbaSearchBO As TBASearchBO = New TBASearchBO()

            'set Location ID selected
            tbaSearchBO.LocationID = IIf(ddlLocation.Text = "Please select" Or ddlLocation.Text = "", Nothing, ddlLocation.SelectedValue.ToString())

            'set Area Id selected
            tbaSearchBO.AreaID = IIf(ddlArea.Text = "" Or ddlArea.Text = "Please select", Nothing, ddlArea.SelectedValue.ToString())

            'set Element ID selected
            tbaSearchBO.ElementID = IIf(ddlElement.Text = "" Or ddlElement.Text = "Please select", Nothing, ddlElement.SelectedValue.ToString())

            'set Priority ID selected
            tbaSearchBO.PriorityID = IIf(ddlPriority.Text = "" Or ddlPriority.Text = "Please select", Nothing, ddlPriority.SelectedValue.ToString())

            'set status value enetered
            tbaSearchBO.Status = IIf(ddlStatus.Text = "" Or ddlStatus.Text = "Please select", Nothing, ddlStatus.SelectedValue.ToString)

            'set patch value enetered
            tbaSearchBO.PatchId = IIf(ddlPatch.Text = "" Or ddlPatch.Text = "Please select", Nothing, ddlPatch.SelectedValue.ToString)

            'set scheme value enetered
            tbaSearchBO.SchemeId = IIf(ddlScheme.Text = "" Or ddlScheme.Text = "Please select", Nothing, ddlScheme.SelectedValue.ToString)

            'set postCode value enetered
            tbaSearchBO.Postcode = IIf(txtPostCode.Text = "", Nothing, txtPostCode.Text)

            'set due value enetered
            tbaSearchBO.Due = IIf(txtDueDate.Text = "", Nothing, txtDueDate.Text)

            'set JS number enetered
            tbaSearchBO.JsNumber = IIf(txtJS.Text = "", Nothing, txtJS.Text)

            'set orgId value enetered           
            tbaSearchBO.OrgId = Me.GetOrgId().ToString()


            'set user value enetered
            tbaSearchBO.UserId = IIf(ddlUser.Text = "" Or ddlUser.Text = "Please select", Nothing, ddlUser.SelectedValue.ToString)


            'tell EnquiryLogSearchBO it has values that will be used for searching
            tbaSearchBO.IsSearch = True
            'store EnquiryLogSearchBO object in viewstate
            ViewState(ApplicationConstants.AppointmentTBASearchOptionsViewState) = tbaSearchBO
            'store true in viewstate to indicate that current data on gridview is from search
            ViewState(ApplicationConstants.AppointmentTBAIsSearchViewState) = True
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "Get TBA Serach Results"

    ''' <summary>
    ''' GetTBASearchResults
    ''' Get Search Results from Database and bind them with Datagrid to make them available on the screen
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub GetTBASearchResults()
        Try
            Dim tbaSearchBO As TBASearchBO = New TBASearchBO()
            'reterive search options tbaSearchBO from viewstate before requesting results for next page of gridview
            tbaSearchBO = DirectCast(ViewState(ApplicationConstants.AppointmentTBASearchOptionsViewState), TBASearchBO)
            Dim tbaDS As DataSet = New DataSet()
            'calling GetTBADataPage function that will further call function in buisness layer
            GetTBADataPage(GVTBA.PageIndex, ApplicationConstants.AppointmentTBAManagResultPerPage, GVTBA.OrderBy, tbaDS, tbaSearchBO)
            'binding gridview with the result returned from db
            Me.StoreTBAResultSummary()
            GVTBA.DataSource = tbaDS
            GVTBA.DataBind()

            'Save TBA Data Set in view State
            ViewState.Add(ApplicationConstants.TBADataSet, tbaDS)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Populate Vulnerability Records"
    Protected Sub GetTBAVulnerability(ByVal CustomerId As Integer)
        Try

            Dim tbaDS As DataSet = New DataSet()
            Dim tbaMngr As ContractorPortalManager = New ContractorPortalManager()
            'calling GetTBADataPage function that will further call function in buisness layer
            tbaDS = tbaMngr.GetTBAVulnerabilityData(CustomerId)

            If (tbaDS.Tables(0).Rows.Count) > 0 Then
                pnlVul.Visible = True
                lblTBAPopupUp.Text = "The selected Customer has the following Vulnerability information held within their records:"
            Else
                lblTBAPopupUp.Text = ""
            End If
            gvCatList.DataSource = tbaDS.Tables(0)
            gvCatList.DataBind()

            'Save TBA Data Set in view State
            'ViewState.Add(ApplicationConstants.TBADataSet, tbaDS)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Populate Risk Records"
    Protected Sub GetTBARisk(ByVal CustomerId As Integer)
        Try

            Dim tbaDS As DataSet = New DataSet()
            Dim tbaMngr As ContractorPortalManager = New ContractorPortalManager()
            'calling GetTBADataPage function that will further call function in buisness layer
            tbaDS = tbaMngr.GetTBARiskData(CustomerId)

            If (tbaDS.Tables(0).Rows.Count) > 0 Then
                pnlRisk.Visible = True
                lblRisk.Text = "The selected Customer has the following Risk information held within their records:"
            Else
                lblRisk.Text = ""
            End If
            gvRiskList.DataSource = tbaDS.Tables(0)
            gvRiskList.DataBind()

            'Save TBA Data Set in view State
            'ViewState.Add(ApplicationConstants.TBADataSet, tbaDS)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region



#Region " Populate Gas Servicing Appointment"
    Protected Sub GetGasServicingAppt(ByVal CustomerId As Integer)
        Try

            Dim tbaDS As DataSet = New DataSet()
            Dim tbaMngr As ContractorPortalManager = New ContractorPortalManager()
            'calling GetTBADataPage function that will further call function in buisness layer
            tbaDS = tbaMngr.GetTBAGasServicingData(CustomerId)

            If (tbaDS.Tables(0).Rows.Count) > 0 Then
                pnlGS.Visible = True
                lblTBAPopupDown.Text = " The selected Customer has an appointment booked on the following date(s):"
                lblGasServicing.Text = tbaDS.Tables(0).Rows(0).Item(0)
            Else
                lblTBAPopupDown.Text = ""
                lblGasServicing.Text = ""
            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region " Populate Customer Communication"
    Protected Sub GetTBACommunication(ByVal CustomerId As Integer)
        Try

            Dim tbaDS As DataSet = New DataSet()
            Dim tbaMngr As ContractorPortalManager = New ContractorPortalManager()
            'calling GetTBADataPage function that will further call function in buisness layer
            tbaDS = tbaMngr.GetTBACommunicationData(CustomerId)

            If (tbaDS.Tables(0).Rows.Count) > 0 Then
                pnlComm.Visible = True
                lblTBAPopUpComm.Text = "The selected Customer has the following Communication information held within their records:"
            Else
                lblTBAPopUpComm.Text = ""
            End If

            gvCommunication.DataSource = tbaDS.Tables(0)
            gvCommunication.DataBind()


        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region






#Region "Populate TBA Search results"

    ''' <summary>
    ''' PopulateTBASerachResults
    ''' this method is used to call all the methods which are to be used in searching results and showing them on screen
    ''' i.e SaveTBASearchOptions (to save search Option in View State)
    ''' GetTBASearchRowCount (to set the virtual Rowcount of the grid for paging)
    ''' GetTBASearchResults (to get data from database)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub PopulateTBASearchResults()
        'save these new search options entered by user unless search button is pressed again
        SaveTBASearchOptions()
        'setting new starting page index of for this new search
        GVTBA.PageIndex = ApplicationConstants.AppointmentTBAInitialPageIndex
        'getting and setting row count of new search results
        GVTBA.VirtualItemCount = GetTBASearchRowCount()
        'getting search results
        GetTBASearchResults()
        'updatePanel_TBA.Update()
    End Sub

#End Region

#Region "Get TBA Search Results Count"

    ''' <summary>
    ''' GetTBASearchRowCount
    ''' this method is used to get row count of the searched results so it can be set as virtual row count of 
    ''' the grid and later can be used for paging
    ''' </summary>
    ''' <returns>an integer which is actually the row count of serached resutls</returns>
    ''' <remarks></remarks>
    Private Function GetTBASearchRowCount() As Integer
        Try
            'Defining object of business layer
            Dim objBAL As ContractorPortalManager = New ContractorPortalManager()
            'defining and fetchin SearchBO from view state
            Dim tbaSearchBO As TBASearchBO = New TBASearchBO()
            tbaSearchBO = DirectCast(ViewState(ApplicationConstants.AppointmentTBASearchOptionsViewState), TBASearchBO)
            'Calling the function of business layer and returning the row count by passing the current Search Options to it
            Return (objBAL.GetAssignmentTBASearchRowCount(tbaSearchBO))
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try

    End Function

#End Region

#Region "Store TBA Search results Summary "

    ''' <summary>
    ''' StoreTBAResultSummary
    ''' this method is used to save the result summary in the page view state so it can be used between the pages of datagrid
    ''' it is called when a page index is changed
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StoreTBAResultSummary()
        Dim row As GridViewRow = GVTBA.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVTBA.PageIndex + 1 = GVTBA.PageCount Then
            strResultSummary = "Result " & ((GVTBA.PageIndex * GVTBA.PageSize) + 1) & " to " & GVTBA.VirtualItemCount & " of " & GVTBA.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVTBA.PageIndex * GVTBA.PageSize) + 1) & " to " & ((GVTBA.PageIndex + 1) * GVTBA.PageSize) & " of " & GVTBA.VirtualItemCount
        End If
        'save Page index & Page Size in View State
        ViewState.Add(ApplicationConstants.ContractorProtalTBAPageIndexKey, GVTBA.PageIndex)
        ViewState.Add(ApplicationConstants.ContractorProtalTBAPageSizeKey, GVTBA.PageSize)

        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.AppointmentTBAResultSummary) = strResultSummary
    End Sub

#End Region

#Region "Method to Save Appointment"

    ''' <summary>
    ''' SaveAppointment
    ''' this method is used to save the Appointments in the database according to the date and time selected from controls
    ''' it iterates the datagrid for every row and
    ''' gets the user input and push that into a business object of Appointment Type
    ''' then use that business object to save the record into the database
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SaveAppointment()

        'Create the instance of Fault AppointmentTBA BO
        Dim appointmentTBABO As AppointmentTBABO = New AppointmentTBABO()

        'create the instance of ContractorPortalManager
        Dim contractorBL As ContractorPortalManager = New ContractorPortalManager()
        Dim counter As Integer = 0
        Try
            'defining a row of DatagridRow type
            Dim row As GridViewRow
            Dim chkBox As New CheckBox
            'Iterating the Grid for each row
            For Each row In GVTBA.Rows
                'gettting the checkbox from row
                chkBox = DirectCast(row.FindControl("chkTBASelect"), CheckBox)
                'pushing the input values into AppointmentTBA Bo if checkbox is selected
                If chkBox.Checked = True Then
                    counter = counter + 1
                    appointmentTBABO.JSNumber = DirectCast(row.FindControl("lnkJobSheetNumber"), LinkButton).Text
                    'appointmentTBABO.AppointmentDate = UtilityFunctions.FormatDate(Date.ParseExact(txtTBADate.Text, "dd/mm/yyyy", Nothing).ToString())
                    appointmentTBABO.AppointmentDate = txtTBADate.Text.ToString()
                    appointmentTBABO.OperativeId = ddlTBAOperative.SelectedValue()
                    appointmentTBABO.AppointmentStageId = 2
                    appointmentTBABO.AppointmentTime = ddlTBAHour.SelectedValue.ToString() & ":" & ddlTBAMin.SelectedValue.ToString() & " " & ddlTBAAMPM.SelectedItem.ToString()
                    appointmentTBABO.LastActionDate = UtilityFunctions.FormatDate(System.DateTime.Now.ToShortDateString)
                    appointmentTBABO.LetterId = Nothing
                    'calling the Business layer function and passing an appointmentTBA BO to it
                    contractorBL.SaveAppointment(appointmentTBABO)
                End If
            Next
            If appointmentTBABO.IsFlagStatus = True Then
                'updating datagrid
                PopulateTBASearchResults()
                txtTBADate.Text = ""
                ddlTBAOperative.SelectedIndex = 0
            End If
            If counter = 0 Then
                lblTBAError.Text = "Please Select a job sheet from grid"
                lblTBAError.ForeColor = Drawing.Color.Red
            Else
                lblTBAError.Text = "Appointment(s) Saved Successfully"
                lblTBAError.ForeColor = Drawing.Color.Green
            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub

#End Region

#Region "Hide TBA Empty Grid Rows"

    ''' <summary>
    ''' HideTBAEmptyGridRows
    '''This function avoid the display of empty data grid rows where 
    '''ess records are being displayed than page size of grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub HideTBAEmptyGridRows()
        Try

            Dim tbaDS As DataSet

            'Get Page index & Page Size from View State
            GVTBA.PageIndex = ViewState.Item(ApplicationConstants.ContractorProtalTBAPageIndexKey)
            GVTBA.PageSize = ViewState.Item(ApplicationConstants.ContractorProtalTBAPageSizeKey)

            'Get TBA Data Set From view State
            tbaDS = ViewState.Item(ApplicationConstants.TBADataSet)
            GVTBA.DataSource = tbaDS
            GVTBA.DataBind()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Show Manage Appointment Page Components "
    Private Sub ShowAppointmentToBeArrangePageComponents()
        Try
            Me.lblGVHeading.Text = "Appointment To Be Arranged"
            Me.updatePanel_TBA.Visible = True
            Me.updatePanel_TBAPopup.Visible = True
            Me.updatePanel_TBA.Update()
            Me.updatePanel_TBAPopup.Update()
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Hide Manage Appointment Page Components "
    Private Sub HideAppointmentToBeArrangePageComponents()
        Try
            Me.updatePanel_TBA.Visible = False
            Me.updatePanel_TBAPopup.Visible = False
            Me.updatePanel_TBA.Update()
            Me.updatePanel_TBAPopup.Update()
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Clear TBA Message "

    Private Sub ClearTBAMessage()
        Try
            Me.lblTBAError.Text = ""
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#End Region

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Manage Appointment - Coded by Waseem Hasan - Januray, 2009 "

#Region "Manage Appointment - Events "

#Region "GV Manage Appointment Sorting"

    Protected Sub GVManageAppointment_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.ManageAppointmentSortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVManageAppointment.PageIndex = ApplicationConstants.ManageAppointmentInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate), String) = ApplicationConstants.ManageAppointmentDESCSortOrder Then
            ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate) = ApplicationConstants.ManageAppointmentASECSortOrder
        Else
            ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate) = ApplicationConstants.ManageAppointmentDESCSortOrder
        End If

        'populating gridview with new results
        If DirectCast(ViewState(ApplicationConstants.ManageAppointmentIsSearchViewState), Boolean) = True Then
            'GetJobSheetData()
            BindManageAppointmentGrid()
        Else
            BindManageAppointmentGrid()
        End If
    End Sub
#End Region

#Region "Click Event For Manage Appointment Button "

    Protected Sub btnManageAppointment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.ClearMAMessage()
        ClearMAControls()
        Me.UpdatePanel_MAPopUp.Visible = True
        Me.UpdatePanel_MAPopUp.Update()
        Me.UpdateAllGridPanel()
        Me.SetPopUpFields(sender)
        Me.MdlPopup_ManageAppointment.Show()

        'This function avoid the display of empty data grid rows where 
        'less records are being displayed than page size of grid
        Me.HideMAEmptyGridRows()
    End Sub

#End Region

#Region "Click Event For Save Changes Button "
    Protected Sub btnSaveChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.ValidatePopUp()
        'Get Manage Appointment Data after update
        Me.GetManageAppointmentData()
    End Sub
#End Region

#Region "Index Change Event For Manage Appointment Grid "
    Protected Sub GVManageAppointment_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVManageAppointment.PageIndex = e.NewPageIndex
        ApplicationConstants.ManageAppointmentInitialPageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreManageAppointmentResultSummary()
        Dim faultDS As DataSet = New DataSet()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.ManageAppointmentIsSearchViewState), Boolean) = True Then
            GetSearchResults()
        Else
            BindManageAppointmentGrid()
        End If
    End Sub
#End Region

#Region "Get Operative Look Up Values"
    ''Operative Lookup Values
    Private Sub GetOperativeLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        Try
            PopulateLookup(ddlPopupOperative, SprocNameConstants.GetOperativeLookup, inParam, inParamName)
            PopulateLookup(ddlWCPopupBy, SprocNameConstants.GetOperativeLookup, inParam, inParamName)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Get Stage Look Up Values "

    ''Lookup Values for Stage
    Private Sub GetStageLookUpValues()
        Try
            PopulateLookup(ddlStage, SprocNameConstants.GetStageLookup)
            PopulateLookup(ddlPopupStage, SprocNameConstants.GetStageLookup)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#End Region

#Region "Manage Appointment - Lookup Methods "

#Region "Get Letter LookUp Values "
    ''Lookup Values for Letter
    Private Sub GetLetterLookUpValues()
        Try
            PopulateLookup(ddlPopupSelectLetter, SprocNameConstants.GetLetterLookup)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region


#End Region

#Region "Manage Appointment Methods & Functions "

#Region "Manage Appointment Page Load "

    Private Sub ManageAppointmentPageLoad()
        Me.ClearMAMessage()
        Me.GetStageLookUpValues()
        Me.GetLetterLookUpValues()
        Me.GetOperativeLookUpValues(Me.GetOrgId().ToString(), "ORGID")       
        Me.GetVatLookUpValues()

        Try
            'storing in view state boolean value indicating , results being displayed in gridview are 
            'from search or not
            ViewState(ApplicationConstants.ManageAppointmentIsSearchViewState) = True
            'storing current sort order of any column asc or dsen
            ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate) = ApplicationConstants.ManageAppointmentDESCSortOrder
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
        'function used for storing search parameters which will be used for further page requests if result is 
        'multipage
        SaveSearchOptions()
        'setting initial page index of gridview by default its zero
        GVManageAppointment.PageIndex = ApplicationConstants.ManageAppointmentInitialPageIndex

        'getting and setting row count of resultset
        GVManageAppointment.VirtualItemCount = GetMAWCSearchRowCount()
        'Me.ddlContractorType.SelectedValue = 0
        Me.GetManageAppointmentData()

    End Sub

#End Region

#Region "Get ManageAppointment Data "
    Private Sub GetManageAppointmentData()
        Try            
            'save these new search options entered by user unless search button is pressed again
            SaveSearchOptions()
            'setting new starting page index of for this new search
            GVManageAppointment.PageIndex = ApplicationConstants.ManageAppointmentInitialPageIndex
            'getting and setting row count of new search results
            GVManageAppointment.VirtualItemCount = GetMAWCSearchRowCount()
            'getting search results
            GetSearchResults()
            UpdatePanel_Search.Update()
            updatePanel_MAGV.Update()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Get Search Results "
    Protected Sub GetSearchResults()
        Try
            Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
            'reterive search options ContractorPortalBO from viewstate before requesting results for next page of gridview
            cpBO = DirectCast(ViewState(ApplicationConstants.ManageAppointmentSearchOptionsViewState), ContractorPortalBO)
            Dim MADS As DataSet = New DataSet()
            'calling GetDataPage function that will further call function in buisness layer
            GetDataPage(GVManageAppointment.PageIndex, ApplicationConstants.ManageAppointmentManagResultPerPage, GVManageAppointment.OrderBy, MADS, cpBO)
            'binding gridview with the result returned from db
            Me.StoreManageAppointmentResultSummary()
            GVManageAppointment.DataSource = Nothing
            GVManageAppointment.DataSource = MADS
            GVManageAppointment.DataBind()

            'Save Manage Appointment Data Set in view State
            ViewState.Add(ApplicationConstants.MADataSet, MADS)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Store Manage Appointment Result Summary "
    'when page index changes this functions gets called 
    Public Sub StoreManageAppointmentResultSummary()
        Dim row As GridViewRow = GVManageAppointment.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVManageAppointment.PageIndex + 1 = GVManageAppointment.PageCount Then
            strResultSummary = "Result " & ((GVManageAppointment.PageIndex * GVManageAppointment.PageSize) + 1) & " to " & GVManageAppointment.VirtualItemCount & " of " & GVManageAppointment.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVManageAppointment.PageIndex * GVManageAppointment.PageSize) + 1) & " to " & ((GVManageAppointment.PageIndex + 1) * GVManageAppointment.PageSize) & " of " & GVManageAppointment.VirtualItemCount
        End If

        'save Page index & Page Size in View State
        ViewState.Add(ApplicationConstants.ContractorProtalMAPageIndexKey, GVManageAppointment.PageIndex)
        ViewState.Add(ApplicationConstants.ContractorProtalMAPageSizeKey, GVManageAppointment.PageSize)

        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.ManageAppointmentResultSummary) = strResultSummary
    End Sub
#End Region

#Region "Bind Manage Appointment Grid "
    ''' Helper to bind the grid to the dynamic data
    Private Sub BindManageAppointmentGrid()
        Dim MADS As DataSet = New DataSet()
        Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
        GetDataPage(GVManageAppointment.PageIndex, ApplicationConstants.ManageAppointmentManagResultPerPage, GVManageAppointment.OrderBy, MADS, cpBO)
        GVManageAppointment.DataSource = MADS
        GVManageAppointment.DataBind()
        updatePanel_MAGV.Update()
    End Sub
#End Region

#Region "Set Popup Fields "

    Public Sub SetPopUpFields(ByVal sender As System.Object)
        Try

            Me.SetTimeControl()
            'casting object as a button before further processing
            Dim btn As Button = DirectCast(sender, Button)
            'getting command arguments from button and storing them in string aray
            Dim strArr As String() = btn.CommandArgument.Split(",")
            Dim appointmentTimeFormat = strArr(12).Split(" ")
            Dim AppointmentTime = appointmentTimeFormat(0).ToString().Split(":")

            Me.lblCustomerText.Text = strArr(0)
            Me.lblAddressText.Text = strArr(1)
            Me.lblTelephoneText.Text = strArr(2)
            Me.lblCustomerNoText.Text = strArr(3)
            Dim strDate() As String = strArr(4).ToString().Split("/")
            Dim repStr As String = strDate(0) & "/" & strDate(1) & "/" & strDate(2)


            Me.txtPopupAppointment.Text = repStr

            Me.ddlPopupHr.SelectedValue = AppointmentTime(0)
            Me.ddlPopupMin.SelectedValue = AppointmentTime(1)
            Me.ddlPopupFormat.SelectedValue = appointmentTimeFormat(1)

            Me.txtPopupLastActionBy.Text = strArr(5)
            Me.txtPopupNotes.Text = strArr(6)
            If strArr(7) = "3" Then
            Me.ddlPopupStage.SelectedValue = strArr(7)
            End If
            Me.ddlPopupOperative.SelectedValue = Integer.Parse(strArr(8))

            'If strArr(9) <> "0" And strArr(9) <> 0 Then
            '    Me.ddlPopupSelectLetter.SelectedValue = Integer.Parse(strArr(9))
            'End If

            Me.lblTenancyRefText.Text = strArr(10)
            Me.lblPopuphidden.Text = strArr(11)
            Me.lblAppointmentErrorMessage.Text = ""
            Me.UpdatePanel_ErrorMsg.Update()
            Me.UpdatePanel_MAPopUp.Update()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Update Manage Appointment Data "
    Public Sub UpdateManageAppointmentData()
        Dim cpBO As ContractorPortalBO = New ContractorPortalBO()

        Dim appTime = Me.ddlPopupHr.SelectedValue & ":" & Me.ddlPopupMin.SelectedValue & ":00" & " " & Me.ddlPopupFormat.SelectedValue

        cpBO.StageId = Me.ddlPopupStage.SelectedValue
        cpBO.AppointmentDate = txtPopupAppointment.Text
        cpBO.Time = appTime
        cpBO.LastActioned = Me.txtPopupLastActionBy.Text
        cpBO.OperativeId = CType(Me.ddlPopupOperative.SelectedValue, Integer)
        cpBO.Notes = Me.txtPopupNotes.Text
        If (Me.ddlPopupSelectLetter.SelectedValue = "") Then
            cpBO.LetterId = Nothing
        Else
            cpBO.LetterId = CType(Me.ddlPopupSelectLetter.SelectedValue, Integer)
        End If

        cpBO.AppointmentId = CType(Me.lblPopuphidden.Text, Integer)

        Dim cpMngr As ContractorPortalManager = New ContractorPortalManager()

        cpMngr.UpdateManageAppointmentData(cpBO)

        Me.lblManageAppointmentMsg.Text = "Appointment has been saved successfully."
        Me.lblManageAppointmentMsg.CssClass = "info_message_label"

        ''Check for exception 
        Dim exceptionFlag As Boolean = cpBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If
    End Sub
#End Region

#Region "Validate Popup "
    Sub ValidatePopUp()
        If Me.ddlPopupStage.SelectedValue <> "" And Me.ddlPopupOperative.SelectedValue <> "" Then
            Me.UpdateManageAppointmentData()
            Me.UpdatePanel_MAPopUp.Update()
        ElseIf Me.ddlPopupStage.SelectedValue = "" Then
            Me.lblAppointmentErrorMessage.Text = "Please select the Stage"
            Me.UpdatePanel_ErrorMsg.Update()
        ElseIf Me.ddlPopupOperative.SelectedValue = "" Then
            Me.lblAppointmentErrorMessage.Text = "Please select the Operative"
            Me.UpdatePanel_ErrorMsg.Update()
        End If
    End Sub
#End Region

#Region "Hide MA Empty Grid Rows"
    'This function avoid the display of empty data grid rows where 
    'less records are being displayed than page size of grid

    Private Sub HideMAEmptyGridRows()
        Try

            Dim MADS As DataSet

            'Get Page index & Page Size from View State
            GVManageAppointment.PageIndex = ViewState.Item(ApplicationConstants.ContractorProtalMAPageIndexKey)
            GVManageAppointment.PageSize = ViewState.Item(ApplicationConstants.ContractorProtalMAPageSizeKey)

            'Get TBA Data Set From view State
            MADS = ViewState.Item(ApplicationConstants.MADataSet)
            GVManageAppointment.DataSource = MADS
            GVManageAppointment.DataBind()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Show Manage Appointment Page Components "
    Private Sub ShowManageAppointmentPageComponents()
        Try
            Me.lblGVHeading.Text = "Manage Appointment"
            Me.updatePanel_MAGV.Visible = True
            Me.updatePanel_MAGV.Update()
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Hide Manage Appointment Page Components "
    Private Sub HideManageAppointmentPageComponents()
        Try
            Me.updatePanel_MAGV.Visible = False
            Me.UpdatePanel_MAPopUp.Visible = False
            Me.updatePanel_MAGV.Update()
            Me.UpdatePanel_MAPopUp.Update()
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Clear MA Message "
    Private Sub ClearMAMessage()
        Me.lblManageAppointmentMsg.Text = ""


    End Sub

#End Region

#Region "Clear MA Controls"
    Private Sub ClearMAControls()
        ddlPopupStage.SelectedValue = ""
        txtPopupAppointment.Text = ""
        txtPopupLastActionBy.Text = ""
        txtPopupNotes.Text = ""
        ddlPopupOperative.SelectedValue = ""
        ddlPopupSelectLetter.SelectedValue = ""

    End Sub
#End Region

#End Region

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Common Methods For Manage Appointment & Work Completion - Coded by Waseem Hasan - Januray, 2009 "

#Region "Method For Storing Search Options "

    Private Sub SaveSearchOptions()
        Try
            'make ContractorPortalBO object and store the search options entered by user untill he/she enters again
            'in this case the values will be overidden
            Dim cpBO As ContractorPortalBO = New ContractorPortalBO()

            cpBO.OrgId = Me.GetOrgId()            

            'set Location ID selected
            If ddlLocation.SelectedValue.ToString() <> String.Empty Then
                cpBO.LocationId = CType(ddlLocation.SelectedValue, Integer)
            End If


            'set Area Id selected
            If ddlArea.SelectedValue.ToString() <> String.Empty Then
                cpBO.AreaId = CType(ddlArea.SelectedValue, Integer)
            End If

            'set Element ID selected
            If ddlElement.SelectedValue.ToString() <> String.Empty Then
                cpBO.ElementId = CType(ddlElement.SelectedValue, Integer)
            End If

            'set Team ID selected
            If ddlTeam.SelectedValue.ToString() <> String.Empty Then
                cpBO.TeamId = CType(ddlTeam.SelectedValue, Integer)
            End If

            'set User Id selected
            If ddlUser.SelectedValue.ToString() <> String.Empty Then
                cpBO.UserId = CType(ddlUser.SelectedValue, Integer)
            End If

            'set Patch ID selected
            If ddlPatch.SelectedValue.ToString() <> String.Empty Then
                cpBO.PatchId = CType(ddlPatch.SelectedValue, Integer)
            End If

            'set Scheme ID selected
            If ddlScheme.SelectedValue.ToString() <> String.Empty Then
                cpBO.SchemeId = CType(ddlScheme.SelectedValue, Integer)
            End If

            'set Priority Id selected
            If ddlPriority.SelectedValue.ToString() <> String.Empty Then
                cpBO.PriorityId = CType(ddlPriority.SelectedValue, Integer)
            End If

            'set Status ID selected
            If ddlStatus.SelectedValue.ToString() <> String.Empty Then
                cpBO.StatusId = CType(ddlStatus.SelectedValue, Integer)
            End If

            'set Stage ID selected
            If ddlStage.SelectedValue.ToString() <> String.Empty Then
                cpBO.StageId = CType(ddlStage.SelectedValue, Integer)
            End If

            'set Due text
            If txtDueDate.Text <> "" Then
                Try
                    'convert date string to date object and set its value
                    'Dim dueDate As Date = txtDueDate.Text
                    'cpBO.Due = dueDate.ToShortDateString()
                    cpBO.Due = txtDueDate.Text

                Catch ex As Exception
                    Response.Redirect("~/error.aspx")
                End Try
            End If

            'set JS Number text
            If txtJS.Text <> "" Then
                cpBO.JsNumber = txtJS.Text
            End If

            'set PostCode text
            If txtPostCode.Text <> "" Then
                cpBO.PostCode = txtPostCode.Text.ToString()
            End If

            'tell ContractorPortalBO it has values that will be used for searching
            cpBO.IsSearch = True
            'store ContractorPortalBO object in viewstate
            ViewState(ApplicationConstants.ManageAppointmentSearchOptionsViewState) = cpBO
            'store true in viewstate to indicate that current data on gridview is from search
            ViewState(ApplicationConstants.ManageAppointmentIsSearchViewState) = True
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "Get Manage Appointment & Work Completion Search Row Count"

    Private Function GetMAWCSearchRowCount() As Integer
        Try
            Dim objBL As ContractorPortalManager = New ContractorPortalManager()
            Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
            cpBO = DirectCast(ViewState(ApplicationConstants.ManageAppointmentSearchOptionsViewState), ContractorPortalBO)

            If Me.ddlContractorType.SelectedValue = "Manage" Then
                Return (objBL.GetManageAppointmentSearchRowCount(cpBO))
            ElseIf Me.ddlContractorType.SelectedValue = "Work" Then
                Return (objBL.GetWorkCompletionSearchRowCount(cpBO))
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try

    End Function
#End Region

#Region "Get Page data "
    Public Sub GetDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef ds As DataSet, ByVal cpBO As ContractorPortalBO)

        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.ManageAppointmentSortByViewState) Is Nothing) Then
            cpBO.SortBy = CType(ViewState(ApplicationConstants.ManageAppointmentSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use faultLogId as a default
            cpBO.SortBy = ApplicationConstants.ManageAppointmentManagSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        cpBO.PageIndex = pageIndex
        cpBO.RowCount = pageSize

        'reteriving sort order from viewstate
        cpBO.SortOrder = DirectCast(ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate), String)
        Dim cpMngr As ContractorPortalManager = New ContractorPortalManager()
        'passing reference of dataset and passing cpBO to GetSearchResults function
        'of ContractorPortalManager
        If Me.ddlContractorType.SelectedValue = "Manage" Then
            ds = cpMngr.GetManageAppointmentSearchData(cpBO)
        ElseIf Me.ddlContractorType.SelectedValue = "Work" Then
            ds = cpMngr.GetWorkCompletionSearchData(cpBO)
        End If
        Try
            ViewState("NumberofRows") = ds.Tables(0).Rows.Count
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Set Time Control"
    Private Sub SetTimeControl()
        Dim i As Integer
        Me.ddlPopupHr.Items.Clear()
        Me.ddlPopupMin.Items.Clear()
        Me.ddlWCPopupHr.Items.Clear()
        Me.ddlWCPopupMin.Items.Clear()
        For i = 0 To 12
            If i < 10 Then
                Dim hr As String = "0" & i
                ddlPopupHr.Items.Add(New ListItem(hr, hr))
                ddlWCPopupHr.Items.Add(New ListItem(hr, hr))
            Else
                ddlPopupHr.Items.Add(New ListItem(i, i))
                ddlWCPopupHr.Items.Add(New ListItem(i, i))
            End If
        Next i

        ddlPopupMin.Items.Add(New ListItem("00", "00"))
        ddlPopupMin.Items.Add(New ListItem("15", "15"))
        ddlPopupMin.Items.Add(New ListItem("30", "30"))
        ddlPopupMin.Items.Add(New ListItem("45", "45"))

        ddlWCPopupMin.Items.Add(New ListItem("00", "00"))
        ddlWCPopupMin.Items.Add(New ListItem("01", "01"))
        ddlWCPopupMin.Items.Add(New ListItem("02", "02"))
        ddlWCPopupMin.Items.Add(New ListItem("03", "03"))
        ddlWCPopupMin.Items.Add(New ListItem("04", "04"))
        ddlWCPopupMin.Items.Add(New ListItem("05", "05"))
        ddlWCPopupMin.Items.Add(New ListItem("06", "06"))
        ddlWCPopupMin.Items.Add(New ListItem("07", "07"))
        ddlWCPopupMin.Items.Add(New ListItem("08", "08"))
        ddlWCPopupMin.Items.Add(New ListItem("09", "09"))
        Dim j As Integer
        For j = 10 To 59
            ddlWCPopupMin.Items.Add(New ListItem(j.ToString(), j.ToString()))
        Next
       
    End Sub
#End Region

#Region "Reset Application Constant"

    Private Sub ResetApplicationConstant()
        Try
            ApplicationConstants.ManageAppointmentManagSortBy = "FL_FAULT_LOG.FaultLogID"
            ApplicationConstants.ManageAppointmentDESCSortOrder = "DESC"
            ApplicationConstants.ManageAppointmentASECSortOrder = "ASC"
            ApplicationConstants.ManageAppointmentInitialPageIndex = 0
            ApplicationConstants.ManageAppointmentManagResultPerPage = 10
            ApplicationConstants.ManageAppointmentSearchOptionsViewState = "ContractorSearchOptions"
            ApplicationConstants.ManageAppointmentSortByViewState = "ManageAppointmentSortBy"
            ApplicationConstants.ManageAppointmentSortOrderViewSate = "CPSortOrder"
            ApplicationConstants.ManageAppointmentIsSearchViewState = "IsContractorPortalSearch"
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "lnkJbSheetNumberMng  Click"

    Protected Sub lnkJbSheetNumberMng_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        Dim jsnumber As String = btn.CommandArgument.ToString()
        GetJobSheetPopupData(jsnumber)
        GetJobSheetPopupAsbestosData(jsnumber)
        Me.updatePanel_JbSSmry.Visible = True
        Me.UpdateAllGridPanel()
        ModalPopup_JSsummary.Show()
    End Sub
#End Region

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Work Completion - Coded by Waseem Hasan - Januray, 2009 "

#Region "Work Completion - Lookup Methods "

#Region "Get Vat Lookup Values "

    ''Lookup Values for Vat
    Private Sub GetVatLookUpValues()
        Try
            PopulateLookup(ddlWCPopupVat, SprocNameConstants.GetVatRate)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#End Region

#Region "Work Completion - Events"

#Region "GV Work Completion Sorting "
    Protected Sub GVWorkCompletion_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.ManageAppointmentSortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVManageAppointment.PageIndex = ApplicationConstants.ManageAppointmentInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate), String) = ApplicationConstants.ManageAppointmentDESCSortOrder Then
            ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate) = ApplicationConstants.ManageAppointmentASECSortOrder
        Else
            ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate) = ApplicationConstants.ManageAppointmentDESCSortOrder
        End If

        'populating gridview with new results
        If DirectCast(ViewState(ApplicationConstants.ManageAppointmentIsSearchViewState), Boolean) = True Then
            GetWorkCompletionSearchResults()
        Else
            BindWorkCompletionGrid()
        End If
    End Sub
#End Region

#Region "btn Work Completion Click "

    Protected Sub btnWorkCompletion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.SetWCPopUpFields(sender)
        Me.MdlPopup_WorkCompletion.Show()
        Me.ClearWCMessageLabel()
        Me.UpdatePanel_WCPopUp.Visible = True
        Me.UpdateAllGridPanel()
        'This function avoid the display of empty data grid rows where 
        'less records are being displayed than page size of grid
        Me.HideWCEmptyGridRows()
    End Sub

#End Region

#Region "txt WC Popup Net Text Changed "
    Protected Sub txtWCPopupNet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim str As String = Me.txtWCPopupNet.Text
        Dim strArr() As String = str.Split(".")
        If strArr.Length > 2 Then
            Me.lblWCRepairErrorMessage.Text = "Please enter valid float value for Net"
        Else
            Me.lblWCRepairErrorMessage.Text = ""
            If Me.ddlWCPopupVat.SelectedValue <> "" Then
                Me.txtWCPopupVat.Text = CType(Me.txtWCPopupNet.Text, Double) * (Me.GetVatRate() / 100)
                Me.txtWCPopUpTotal.Text = CType(Me.txtWCPopupNet.Text, Double) + CType(Me.txtWCPopupVat.Text, Double)
                Me.UpdatePanel_RepairDetail.Update()
            Else
                Me.txtWCPopupVat.Text = ""
                Me.txtWCPopUpTotal.Text = ""
                Me.UpdatePanel_RepairDetail.Update()
            End If
        End If
    End Sub
#End Region

#Region "ddl WC Popup Vat Selected Index Changed "

    Protected Sub ddlWCPopupVat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim str As String = Me.txtWCPopupNet.Text
        Dim strArr() As String = str.Split(".")
        If strArr.Length > 2 Then
            Me.lblWCRepairErrorMessage.Text = "Please enter valid float value for Net"
        Else
            Me.lblWCRepairErrorMessage.Text = ""
            If Me.ddlWCPopupVat.SelectedValue <> "" Then
                Me.txtWCPopupVat.Text = CType(Me.txtWCPopupNet.Text, Double) * (Me.GetVatRate() / 100)
                Me.txtWCPopUpTotal.Text = CType(Me.txtWCPopupNet.Text, Double) + CType(Me.txtWCPopupVat.Text, Double)
                Me.UpdatePanel_RepairDetail.Update()

            Else
                Me.txtWCPopupVat.Text = ""
                Me.txtWCPopUpTotal.Text = ""
                Me.UpdatePanel_RepairDetail.Update()
            End If
        End If
    End Sub
#End Region

#Region "btn WC Popup Save Click "

    Protected Sub btnWCPopupRepairAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Me.ClearWCMessageLabel()
        'btnInvoiceThisAmount.Text = "Invoice This Amount"

        Dim str As String = Me.txtWCPopupNet.Text
        Dim strArr() As String = str.Split(".")

        Me.lblWCRepairErrorMessage.CssClass = "error_message_label"

        If strArr.Length > 2 Then
            Me.lblWCRepairErrorMessage.Text = "Please enter valid float value for Net"
        Else
            If Me.txtWCPopupNet.Text <> "" And Me.ddlWCPopupVat.SelectedValue <> "" Then

                If lblBooleanNewRepair.Text.ToString() = "0" Then
                    Me.SaveRepairRecord()
                    
                End If

                Me.GetRepairRecord()
                Me.lblWCRepairErrorMessage.Text = ""

                'Add repair data to grid
                Me.AddRepairDataToRepairGrid()

                'Clear the repair fields
                Me.ClearRepairRelatedFields()
                Me.UpdatePanel_RepairDetail.Update()
                Me.UpdatePanel_WCRepairDescription.Update()
            Else
                If Me.txtWCPopupNet.Text = String.Empty Then
                    Me.lblWCRepairErrorMessage.Text = "Please select an active repair"
                ElseIf Me.ddlWCPopupVat.SelectedValue = "" Then
                    Me.lblWCRepairErrorMessage.Text = "Please select Vat Type"
                End If
                Me.UpdatePanel_RepairDetail.Update()
                
            End If
        End If
    End Sub
#End Region

#Region "txt WC Popup Repair Detail Text Changed "

    Protected Sub txtWCPopupRepairDetail_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.GetRepairRecord()
    End Sub
#End Region

#Region "btn Invoice This Amount Click "

    Protected Sub btnInvoiceThisAmount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.SaveAndValidateInvoicingRecord()
    End Sub

#End Region

#Region "GV Work Completion Page Index Changing "

    Protected Sub GVWorkCompletion_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVWorkCompletion.PageIndex = e.NewPageIndex
        ApplicationConstants.ManageAppointmentInitialPageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreResultSummary()
        Dim faultDS As DataSet = New DataSet()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.ManageAppointmentIsSearchViewState), Boolean) = True Then
            GetWorkCompletionSearchResults()
        Else
            BindWorkCompletionGrid()
        End If
    End Sub

#End Region

#Region "btn WC Repair Delete Click "
    Protected Sub btnWCRepairDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.ClearWCMessageLabel()
        Me.DeleteDataFromRepairGrid(sender)
    End Sub
#End Region

#Region "lnk WC JS Number Click "

    Protected Sub lnkWCJSNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        HideJobSheetEmptyGridRows()
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        Dim jsnumber As String = btn.CommandArgument.ToString()
        GetJobSheetPopupData(jsnumber)
        Me.updatePanel_JbSSmry.Visible = True
        Me.UpdateAllGridPanel()
        ModalPopup_JSsummary.Show()
    End Sub
#End Region

#End Region
#End Region

#Region "Work Completion - Methods & Functions "

#Region "Work Completion - Page Load "

    Private Sub WorkCompletionPageLoad()

        Try
            'storing in view state boolean value indicating , results being displayed in gridview are 
            'from search or not
            ViewState(ApplicationConstants.ManageAppointmentIsSearchViewState) = True

            'storing current sort order of any column asc or dsen
            ViewState(ApplicationConstants.ManageAppointmentSortOrderViewSate) = ApplicationConstants.ManageAppointmentDESCSortOrder

            'setting initial page index of gridview by default its zero
            GVWorkCompletion.PageIndex = ApplicationConstants.ManageAppointmentInitialPageIndex
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


        Me.PopulateLookup(ddlWCPopupBy, SprocNameConstants.GetOperativeLookup, Me.GetOrgId().ToString(), "ORGID")        

        Me.SaveSearchOptions()
        'getting and setting row count of resultset
        GVWorkCompletion.VirtualItemCount = GetMAWCSearchRowCount()
        Me.GetVatLookUpValues()
        Me.GetWorkCompletionData()
        Me.ClearWCMessageLabel()
    End Sub

#End Region

#Region "Get Work Completion Data "
    Private Sub GetWorkCompletionData()
        Try
            'save these new search options entered by user unless search button is pressed again
            SaveSearchOptions()

            'setting new starting page index of for this new search
            GVWorkCompletion.PageIndex = ApplicationConstants.ManageAppointmentInitialPageIndex

            'getting and setting row count of new search results
            GVWorkCompletion.VirtualItemCount = GetMAWCSearchRowCount()

            'getting search results
            GetWorkCompletionSearchResults()
            UpdatePanel_Search.Update()
            UpdatePanel_WCGV.Update()            
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Get Work Completion Search Results "
    Protected Sub GetWorkCompletionSearchResults()
        Try
            Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
            'reterive search options ContractorPortalBO from viewstate before requesting results for next page of gridview
            cpBO = DirectCast(ViewState(ApplicationConstants.ManageAppointmentSearchOptionsViewState), ContractorPortalBO)
            Dim WCDS As DataSet = New DataSet()
            'calling GetDataPage function that will further call function in buisness layer
            GetDataPage(GVWorkCompletion.PageIndex, ApplicationConstants.ManageAppointmentManagResultPerPage, GVWorkCompletion.OrderBy, WCDS, cpBO)
            'binding gridview with the result returned from db
            Me.StoreWorkCompletionResultSummary()
            GVWorkCompletion.DataSource = Nothing
            GVWorkCompletion.DataSource = WCDS
            GVWorkCompletion.DataBind()

            'Save Work Completion Data Set in view State
            ViewState.Add(ApplicationConstants.WCDataSet, WCDS)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Store Work Completion Result Summary "

    Public Sub StoreWorkCompletionResultSummary()
        Dim row As GridViewRow = GVWorkCompletion.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVWorkCompletion.PageIndex + 1 = GVWorkCompletion.PageCount Then
            strResultSummary = "Result " & ((GVWorkCompletion.PageIndex * GVWorkCompletion.PageSize) + 1) & " to " & GVWorkCompletion.VirtualItemCount & " of " & GVWorkCompletion.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVWorkCompletion.PageIndex * GVWorkCompletion.PageSize) + 1) & " to " & ((GVWorkCompletion.PageIndex + 1) * GVWorkCompletion.PageSize) & " of " & GVWorkCompletion.VirtualItemCount
        End If

        'Save Page index & Page Size in View State
        ViewState.Add(ApplicationConstants.ContractorProtalWCPageIndexKey, GVWorkCompletion.PageIndex)
        ViewState.Add(ApplicationConstants.ContractorProtalWCPageSizeKey, GVWorkCompletion.PageSize)

        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.WorkCompletionResultSummary) = strResultSummary
    End Sub

#End Region

#Region "Bind Work Completion Grid "
    ''' Helper to bind the grid to the dynamic data
    Private Sub BindWorkCompletionGrid()
        Dim WCDS As DataSet = New DataSet()
        Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
        GetDataPage(GVWorkCompletion.PageIndex, ApplicationConstants.ManageAppointmentManagResultPerPage, GVWorkCompletion.OrderBy, WCDS, cpBO)
        GVWorkCompletion.DataSource = WCDS
        GVWorkCompletion.DataBind()
        UpdatePanel_WCGV.Update()
    End Sub
#End Region

#Region "Set WC Pop Up Fields "
    Public Sub SetWCPopUpFields(ByVal sender As System.Object)
        Try
            Me.SetTimeControl()
            'casting object as a button before further processing
            Dim btn As Button = DirectCast(sender, Button)
            'getting command arguments from button and storing them in string aray
            Dim strArr As String() = btn.CommandArgument.Split(",")
            Dim areaStr As String = ""
            Me.lblWCCustomerText.Text = strArr(7) & " " & strArr(0) & " " & strArr(8)
            Me.lblWCAddressText.Text = strArr(1)
            Me.lblWCHouseNumberText.Text = strArr(8)
            Me.lblWCPostCodeText.Text = strArr(9)
            Me.lblWCPostCodeText.Text = strArr(9)
            Me.lblWCTownCityText.Text = strArr(11)
            Me.lblWCCountyText.Text = strArr(10)
            Dim addArr As String() = strArr(2).Split(" ")
            Dim i As Integer
            For i = 2 To addArr.Length - 1
                areaStr = areaStr & " " & addArr(i)
            Next
            Me.lblWCFaultLocationText.Text = addArr(0) & ">" & addArr(1) & ">" & areaStr
            Me.lblWCDescriptionText.Text = strArr(3)
            Me.ddlWCPopupBy.SelectedValue = strArr(4)
            Me.lblWCPopuphidden.Text = strArr(5)
            Me.txtWCPopupComplete.Text = ""
            Me.ddlWCPopupHr.SelectedValue = "00"
            Me.ddlWCPopupMin.SelectedValue = "00"
            Me.ddlWCPopupFormat.SelectedValue = "AM"
            Me.txtWCPopupRepairDetail.Text = ""
            Me.txtWCPopupNet.Enabled = False
            Me.ddlWCPopupVat.Enabled = False
            Me.txtWCPopupNet.Text = ""
            Me.ddlWCPopupVat.Text = ""
            Me.txtWCPopupVat.Text = ""
            Me.txtWCPopUpTotal.Text = ""
            Me.lblWCRepairErrorMessage.Text = ""
            Me.lblWCErrorMessage.Text = ""
            Me.lblPoprepairIdHidden.Text = ""
            Me.btnWCPopupRepairAdd.Enabled = True

            ViewState.Add(FaultConstants.WCFaultLogId, strArr(6))
            ViewState.Add(FaultConstants.WCRepairDataSet + ViewState.Item(FaultConstants.WCFaultLogId), "")
            Me.GVWCRepairGrid.Visible = False
            Me.UpdatePanel_WCRepairGrid.Update()

            Me.UpdatePanel_WCRepairDescription.Update()
            Me.UpdatePanel_RepairDetail.Update()
            Me.UpdatePanel_WCErrorMsg.Update()
            Me.UpdatePanel_WCPopUp.Update()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Set Repair Fields "
    Public Sub SetRepairFields(ByRef cpBO As ContractorPortalBO)
        Try
            Dim strNet As String = cpBO.Net
            'Me.txtWCPopupNet.Text = cpBO.Net
            Me.txtWCPopupNet.Text = CType(strNet, Decimal).ToString("0.00")
            Me.ddlWCPopupVat.SelectedValue = cpBO.VatRate
            Me.txtWCPopupVat.Text = cpBO.Vat
            Me.txtWCPopUpTotal.Text = cpBO.Total
            Me.lblPoprepairIdHidden.Text = cpBO.RepairId
            Me.txtWCPopupNet.Enabled = False
            Me.ddlWCPopupVat.Enabled = False

            Me.UpdatePanel_RepairDetail.Update()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Get Vat Rate "
    Public Function GetVatRate() As Double
        Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
        Dim cpMngr As ContractorPortalManager = New ContractorPortalManager()

        cpBO.VatId = CType(Me.ddlWCPopupVat.SelectedValue, Integer)
        cpMngr.GetVatRate(cpBO)

        ''Check for exception 
        Dim exceptionFlag As Boolean = cpBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

        Return CType(cpBO.VatRate, Double)
    End Function
#End Region

#Region "Save Repair Record "
    Public Sub SaveRepairRecord()
        Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
        Dim cpMngr As ContractorPortalManager = New ContractorPortalManager()

        cpBO.RepairDescription = Me.txtWCPopupRepairDetail.Text
        cpBO.Net = CType(Me.txtWCPopupNet.Text, Double)
        cpBO.VatId = CType(Me.ddlWCPopupVat.SelectedValue, Integer)
        cpBO.Vat = CType(Me.txtWCPopupVat.Text, Double)
        cpBO.Total = CType(Me.txtWCPopUpTotal.Text, Double)

        cpMngr.SaveRepairRecord(cpBO)
        Me.btnInvoiceThisAmount.Text = "Complete Works"
        UpdatePanel_InvoiceBtn.Update()

        ''Check for exception 
        Dim exceptionFlag As Boolean = cpBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If


    End Sub
#End Region

#Region "Get Repair Record "
    Public Sub GetRepairRecord()
        Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
        Dim cpMngr As ContractorPortalManager = New ContractorPortalManager()

        cpBO.RepairDescription = Me.txtWCPopupRepairDetail.Text

        cpMngr.GetRepairRecord(cpBO)

        ''Check for exception 
        Dim exceptionFlag As Boolean = cpBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

        If cpBO.Total <> -1 Then
            Me.SetRepairFields(cpBO)
            Me.lblBooleanNewRepair.Text = "1"
            'Me.btnInvoiceThisAmount.Text = "Complete Works"
            'UpdatePanel_InvoiceBtn.Update()
        Else
            Me.txtWCPopupNet.Text = ""
            Me.ddlWCPopupVat.SelectedValue = ""
            Me.txtWCPopupVat.Text = ""
            Me.txtWCPopUpTotal.Text = ""
            Me.lblPoprepairIdHidden.Text = ""
            ' # Start of comment
            ' This will stop Contractors to add repairs which do not exist in the database
            ' Me.txtWCPopupNet.Enabled = True
            ' Me.ddlWCPopupVat.Enabled = True
            ' End of comment #
            Me.lblBooleanNewRepair.Text = "0"
            Me.UpdatePanel_RepairDetail.Update()
        End If

    End Sub
#End Region

#Region "Add Repair Data To Repair Grid - Coded By Noor Muhammad - Feb,2009 "
    Private Sub AddRepairDataToRepairGrid()

        btnInvoiceThisAmount.Text = "Complete Works"
        UpdatePanel_InvoiceBtn.Update()

        Dim faultLogId = ViewState.Item(FaultConstants.WCFaultLogId)
        Dim repairTable As DataTable

        'creating a table named Repairs
        repairTable = New DataTable("Repairs")

        'declaring a row for the table
        Dim row As DataRow

        'Creating a dataset
        Dim repairDs As DataSet = New DataSet()
        'Declare dRowCounter as looping variable & 
        Dim dRowCounter As Integer = 0
        Dim isDuplicateRepair As Boolean = False
        Dim grossTotal As Double


        Me.lblWCRepairErrorMessage.Text = ""

        Try
            'Declaring a columns 
            Dim repairListIdCol As DataColumn = New DataColumn("RepairListID")
            Dim repairCol As DataColumn = New DataColumn("Repair")
            Dim qtyCol As DataColumn = New DataColumn("RepQty")
            Dim costCol As DataColumn = New DataColumn("Cost")
            Dim deleteCol As DataColumn = New DataColumn("TempRepairID")


            'Setting the datatype for the column
            repairListIdCol.DataType = System.Type.GetType("System.Int32")
            repairCol.DataType = System.Type.GetType("System.String")
            qtyCol.DataType = System.Type.GetType("System.Int32")
            costCol.DataType = System.Type.GetType("System.Double")
            deleteCol.DataType = System.Type.GetType("System.Int32")

            'Adding the columns to table
            repairTable.Columns.Add(repairListIdCol)
            repairTable.Columns.Add(repairCol)
            repairTable.Columns.Add(qtyCol)
            repairTable.Columns.Add(costCol)
            repairTable.Columns.Add(deleteCol)

            'Declaring a new row
            row = repairTable.NewRow()

            'Adding the completed row to the table
            repairTable.Rows.Add(row)

            'Adding the table to dataset 
            repairDs.Tables.Add(repairTable)

            'This if will execute on first entry in the grid
            If ViewState.Item(FaultConstants.WCRepairDataSet + faultLogId).ToString = "" Then

                repairDs.Tables(0).Rows(0).Item("RepairListID") = Me.lblPoprepairIdHidden.Text
                repairDs.Tables(0).Rows(0).Item("Repair") = Me.txtWCPopupRepairDetail.Text
                repairDs.Tables(0).Rows(0).Item("RepQty") = Me.ddlRepairQuantity.SelectedValue
                repairDs.Tables(0).Rows(0).Item("Cost") = (Double.Parse(Me.txtWCPopUpTotal.Text) * CType(ddlRepairQuantity.SelectedValue, Integer)).ToString()
                repairDs.Tables(0).Rows(0).Item("TempRepairID") = 0

                grossTotal = (Double.Parse(Me.txtWCPopUpTotal.Text) * CType(ddlRepairQuantity.SelectedValue, Integer))
                Me.lblTotalGrossAmount.Text = grossTotal

                ViewState.Add(FaultConstants.WCRepairDataSet + faultLogId, repairDs)
                ViewState.Add(FaultConstants.WCTotalGrossValue, grossTotal)


            Else
                'Temporary data set to check either repair is already in the grid or not
                Dim tempDs As DataSet = ViewState.Item(FaultConstants.WCRepairDataSet + faultLogId)
                grossTotal = ViewState.Item(FaultConstants.WCTotalGrossValue)

                'Loop the data set to check either this repair id is already added in Repair Grid or not
                For dRowCounter = 0 To tempDs.Tables(0).Rows.Count() - 1

                    'if repair id in the grid matches with value set in hidden label then it is already present in grid
                    If tempDs.Tables(0).Rows(dRowCounter).Item("RepairListID").ToString() = Me.lblPoprepairIdHidden.Text.ToString() Then
                        'set the duplicate flag true, show error message & update the panel
                        isDuplicateRepair = True

                        'Set color, visiblity, message of the error message
                        Me.lblWCErrorMessage.CssClass = "error_message_label"
                        Me.lblWCErrorMessage.Visible = True
                        Me.lblWCErrorMessage.Text = "This repair is already added in a grid."

                        'update the error message panel
                        Me.UpdatePanel_WCErrorMsg.Update()
                        Exit For
                    End If
                Next

                'If duplicate repair is false then add this repair in the grid 
                If isDuplicateRepair = False Then

                    repairDs.Merge(ViewState.Item(FaultConstants.WCRepairDataSet + faultLogId))
                    Dim repairCount = repairDs.Tables(0).Rows.Count

                    repairDs.Tables(0).Rows(0).Item("RepairListID") = Me.lblPoprepairIdHidden.Text
                    repairDs.Tables(0).Rows(0).Item("Repair") = Me.txtWCPopupRepairDetail.Text
                    repairDs.Tables(0).Rows(0).Item("RepQty") = Me.ddlRepairQuantity.SelectedValue
                    repairDs.Tables(0).Rows(0).Item("Cost") = (Double.Parse(Me.txtWCPopUpTotal.Text) * CType(ddlRepairQuantity.SelectedValue, Integer)).ToString()
                    repairDs.Tables(0).Rows(0).Item("TempRepairID") = repairCount - 1

                    grossTotal = grossTotal + (Double.Parse(Me.txtWCPopUpTotal.Text) * CType(ddlRepairQuantity.SelectedValue, Integer))
                    Me.lblTotalGrossAmount.Text = grossTotal

                    ViewState.Add(FaultConstants.WCRepairDataSet + faultLogId, repairDs)
                    ViewState.Add(FaultConstants.WCTotalGrossValue, grossTotal)


                    'Set the success message
                    Me.lblWCErrorMessage.CssClass = "info_message_label"
                    Me.lblWCErrorMessage.Text = "This repair has been added successfully in the grid"
                Else
                    repairDs = ViewState.Item(FaultConstants.WCRepairDataSet + faultLogId)
                End If
            End If


            'Binding the table to datagrid
            Me.GVWCRepairGrid.DataSource = repairDs
            Me.GVWCRepairGrid.DataBind()

            Me.GVWCRepairGrid.Visible = True
            Me.UpdatePanel_WCRepairGrid.Update()
            Me.UpdatePanel_WCErrorMsg.Update()
        Catch
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Delete Data From Repair Grid - Coded By Noor Muhammad - Feb,2009"
    Private Sub DeleteDataFromRepairGrid(ByVal sender)
        Try

            Me.btnInvoiceThisAmount.Text = "Invoice this Amount"
            UpdatePanel_InvoiceBtn.Update()
            Dim faultLogId = ViewState.Item(FaultConstants.WCFaultLogId)
            Dim grossTotal As Double

            'casting sender of this event into button object before getting command arguments from
            'button object
            Dim btn As Button = DirectCast(sender, Button)

            'creating array of command arguments send by delete enquiry button
            Dim strArr As String() = btn.CommandArgument.Split(",")
            Dim tempRepairId As Integer = CType(strArr(0), Integer)

            'Create data set & data table
            Dim repairDs As DataSet = New DataSet()
            Dim repairTable As DataTable = New DataTable()

            'define counter variable for loop & rowNumberToDelete to record the exact required row 
            Dim counter As Int32 = New Int32
            Dim rowNubmerToDelete As Integer = New Integer()

            repairDs.Merge(ViewState.Item(FaultConstants.WCRepairDataSet + faultLogId))
            grossTotal = ViewState.Item(FaultConstants.WCTotalGrossValue)


            For counter = 0 To repairDs.Tables(0).Rows.Count() - 1
                Dim b = repairDs.Tables(0).Rows(counter).Item(3)
                If repairDs.Tables(0).Rows(counter).Item(3) = tempRepairId Then
                    rowNubmerToDelete = counter
                End If
            Next

            'Set the tables of old repair dataset in new repair table & delete the row
            grossTotal = grossTotal - Double.Parse(repairDs.Tables(0).Rows(rowNubmerToDelete).Item("Cost").ToString())
            repairTable = repairDs.Tables(0)
            repairTable.Rows(rowNubmerToDelete).Delete()
            repairTable.AcceptChanges()

            'store new changed data set in a view state
            ViewState.Add(FaultConstants.WCRepairDataSet + faultLogId, repairDs)
            ViewState.Add(FaultConstants.WCTotalGrossValue, grossTotal)

            If grossTotal < 1 Then
                grossTotal = 0
            End If

            'Set & bind dataset with grid 
            Me.lblTotalGrossAmount.Text = grossTotal
            Me.GVWCRepairGrid.DataSource = repairDs
            Me.GVWCRepairGrid.DataBind()
            Me.lblWCErrorMessage.CssClass = "info_message_label"
            Me.lblWCErrorMessage.Text = "This repair has been deleted successfully from the grid."
            Me.UpdatePanel_WCRepairGrid.Update()
            Me.UpdatePanel_WCErrorMsg.Update()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Clear Repair Related Fields -  Coded By Noor Muhammad - Feb,2009"
    Private Sub ClearRepairRelatedFields()
        Me.txtWCPopupRepairDetail.Text = ""
        Me.txtWCPopupNet.Text = ""
        Me.txtWCPopupVat.Text = ""
        Me.txtWCPopupNet.Enabled = False
        Me.ddlWCPopupVat.Enabled = False
        Me.txtWCPopUpTotal.Text = ""
    End Sub
#End Region

#Region "Save And Validate Invoicing Record "

    Public Sub SaveAndValidateInvoicingRecord()

        Dim faultLogId = ViewState.Item(FaultConstants.WCFaultLogId)
        Dim isError As Boolean = False        

        If Me.txtWCPopupComplete.Text <> String.Empty And Me.ddlWCPopupHr.SelectedValue <> "00" And Me.ddlWCPopupBy.SelectedValue <> "" And Me.txtWCPopupRepairDetail.Text <> "" And Me.txtWCPopupNet.Enabled = False Then            
            Me.lblWCErrorMessage.Text = ""
        ElseIf Me.txtWCPopupComplete.Text = String.Empty Then
            isError = True
            Me.lblWCErrorMessage.Text = "Please enter complete date."
        ElseIf Me.ddlWCPopupHr.SelectedValue = "00" Then
            isError = True
            Me.lblWCErrorMessage.Text = "Please select time. "
        ElseIf Me.ddlWCPopupBy.SelectedValue = "" Then
            isError = True
            Me.lblWCErrorMessage.Text = "Please select work completion by."
        End If

        If isError = False Then
            If ViewState.Item(FaultConstants.WCRepairDataSet + faultLogId).ToString() = "" Then
                Me.lblWCErrorMessage.Text = "Please add repairs."
                Me.UpdatePanel_WCErrorMsg.Update()
            ElseIf ViewState.Item(FaultConstants.WCRepairDataSet + faultLogId).Tables(0).Rows.Count() = 0 Then
                Me.lblWCErrorMessage.Text = "Please add repairs."
                Me.UpdatePanel_WCErrorMsg.Update()
            Else
                SaveRepairsAgainstFault()
                SaveInvoicingRecord()
            End If
        End If

        Me.lblWCErrorMessage.CssClass = "error_message_label"
        Me.UpdatePanel_WCErrorMsg.Update()

    End Sub
#End Region

#Region "Save Repairs Against Fault"
    Private Sub SaveRepairsAgainstFault()
        Try
            Dim faultLogId = ViewState.Item(FaultConstants.WCFaultLogId)
            Dim conBO As ContractorPortalBO = New ContractorPortalBO()
            Dim conBL As ContractorPortalManager = New ContractorPortalManager()
            Dim repairDs As DataSet = New DataSet()
            Dim counter = 0

            repairDs = ViewState.Item(FaultConstants.WCRepairDataSet + faultLogId)

            For counter = 0 To repairDs.Tables(0).Rows.Count - 1
                conBO.RepairId = repairDs.Tables(0).Rows(counter).Item(0)
                conBO.Quantity = repairDs.Tables(0).Rows(counter).Item(2)
                conBO.FaultLogId = faultLogId
                conBL.SaveRepairsAgainstFault(conBO)
            Next counter

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Save Invoicing Record "

    Public Sub SaveInvoicingRecord()
        Dim cpBO As ContractorPortalBO = New ContractorPortalBO()
        Dim cpMngr As ContractorPortalManager = New ContractorPortalManager()

        cpBO.RepairId = CType(Me.lblPoprepairIdHidden.Text, Integer)
        cpBO.AppointmentId = CType(Me.lblWCPopuphidden.Text, Integer)
        ' cpBO.RepairDescription = "ASFD" 'CType(Me.txtWCPopupRepairDetail.Text, String)
 
        cpBO.OrgId = Me.GetOrgId()


        cpBO.Net = 1 'CType(Me.txtWCPopupNet.Text, Double)
        cpBO.VatId = 1 'CType(Me.ddlWCPopupVat.Text, Integer)
        cpBO.Vat = 1 'CType(Me.txtWCPopupVat.Text, Double)
        cpBO.Total = 1 'CType(Me.txtWCPopUpTotal.Text, Double)
        cpBO.FaultLogId = ViewState.Item(FaultConstants.WCFaultLogId)
        Try
            'convert date string to date object and set its value
            Dim completeDate As Date = UtilityFunctions.stringToDate(Me.txtWCPopupComplete.Text)
            cpBO.CompletionDate = completeDate

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
        cpBO.Time = Me.ddlWCPopupHr.SelectedValue & ":" & Me.ddlWCPopupMin.SelectedValue & ":00 " & Me.ddlWCPopupFormat.SelectedValue
        cpBO.CompletedBy = CType(Me.ddlWCPopupBy.SelectedValue, Integer)
        cpMngr.SaveInvoicingRecord(cpBO)

        ''Check for exception 
        Dim exceptionFlag As Boolean = cpBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

        If cpBO.IsFlagStatus = False Then
            Me.lblWorkCompletionMsg.CssClass = "error_message_label"
            Me.lblWorkCompletionMsg.Text = "Unable to process the invoice"
        Else
            Me.lblWorkCompletionMsg.CssClass = "info_message_label"
            Me.lblWorkCompletionMsg.Text = "Job sheet has been processed for invoice successfully."
        End If

        Me.GetWorkCompletionData()
        Me.UpdatePanel_WCPopUp.Update()
        Me.UpdatePanel_WCGV.Update()

    End Sub
#End Region

#Region "Show Work Completion Page Components "
    Private Sub ShowWorkCompletionPageComponents()
        Try
            Me.lblGVHeading.Text = "Work Completion"
            Me.UpdatePanel_WCGV.Visible = True            
            Me.UpdatePanel_WCPopUp.Update()
            Me.UpdateGridHeadingPanel()
            Me.UpdatePanel_WCGV.Update()
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Hide WorkCompletionPage Page Components "
    Private Sub HideWorkCompletionPageComponents()
        Try
            Me.UpdatePanel_WCGV.Visible = False
            Me.UpdatePanel_WCPopUp.Visible = False
            Me.UpdateGridHeadingPanel()
            Me.UpdatePanel_WCGV.Update()
            Me.UpdatePanel_WCPopUp.Update()
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Hide WC Empty Grid Rows"
    'This function avoid the display of empty data grid rows where 
    'less records are being displayed than page size of grid

    Private Sub HideWCEmptyGridRows()
        Try

            Dim WCDS As DataSet

            'Get Page index & Page Size from View State
            GVWorkCompletion.PageIndex = ViewState.Item(ApplicationConstants.ContractorProtalWCPageIndexKey)
            GVWorkCompletion.PageSize = ViewState.Item(ApplicationConstants.ContractorProtalWCPageSizeKey)

            'Get TBA Data Set From view State
            WCDS = ViewState.Item(ApplicationConstants.WCDataSet)
            GVWorkCompletion.DataSource = WCDS
            GVWorkCompletion.DataBind()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Clear WC Message Label - Coded By Noor Muhammad - Feb,2009"
    Private Sub ClearWCMessageLabel()
        Me.lblWorkCompletionMsg.Text = ""
        Me.lblWCRepairErrorMessage.Text = ""
        Me.lblWCRepairErrorMessage.Text = ""
        Me.lblWCErrorMessage.Text = ""
        'Me.btnInvoiceThisAmount.Text = "Invoice this amount"
        UpdatePanel_InvoiceBtn.Update()
        Me.UpdatePanel_RepairDetail.Update()
        Me.UpdatePanel_WCErrorMsg.Update()

    End Sub
#End Region

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Invoicing - Coded By Tahir Gul - January,2009  "

#Region "Invoicing Events "

#Region "Invoicing Grid CheckBox Check Changed"

    ''' <summary>
    ''' This function will be called when the invoicing check box is either checked or unchecked. 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub chkInvoicing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)


        CalcualateInvoiceValue()
        HideINVEmptyGridRows()
        Dim checkNewStatus() As Integer = CType(ViewState(ApplicationConstants.InvoiceCheckStatusArray), Integer())
        For i As Integer = 0 To checkNewStatus.Length - 1
            If checkNewStatus(i) = 1 Then
                DirectCast(GVinvoicing.Rows(i).Cells(8).FindControl("chkInvoicing"), CheckBox).Checked = True
            End If
        Next
        Invoicing_UpdatePanel.Update()


    End Sub
#End Region





#Region "Invoicing Grid Sorting Event"
    ''' <summary>
    ''' The GV invoicing handler, will be invoked when gridview header column is clicked. 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub GVinvoicing_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)

        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.CustInvoiceSortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        GVinvoicing.PageIndex = ApplicationConstants.CustInvoiceInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.InvoiceSortOrderViewState), String) = ApplicationConstants.InvoiceManageDESCSortOrder Then
            ViewState(ApplicationConstants.InvoiceSortOrderViewState) = ApplicationConstants.InvoiceManageASECSortOrder
        Else
            ViewState(ApplicationConstants.InvoiceSortOrderViewState) = ApplicationConstants.InvoiceManageDESCSortOrder
        End If

        'populating gridview with new results
        If DirectCast(ViewState(ApplicationConstants.CustInvoiceIsSearchViewState), Boolean) = True Then
            GetInvoicingSearchResults()
        Else
            BindInvoiceGrid()
        End If
        txtInvoicingValue.Text = ""
    End Sub

#End Region

#Region "Invoicing Select All Click Event"
    ''' <summary>
    ''' handler for invoicing select all check boxes. 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub btnInvoicingSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'This function avoid the display of empty data grid rows where 
        'less records are being displayed than page size of grid
        Me.HideINVEmptyGridRows()

        For Each row As GridViewRow In Me.GVinvoicing.Rows      ' value in the gross will be fetched from all rows. 

            Dim chkColumn As CheckBox = CType(row.Cells(8).FindControl("chkInvoicing"), CheckBox)
            chkColumn.Checked = True

        Next

        CalcualateInvoiceValue()        ' This fucntion will calculate all the gross values of the data grid. 
    End Sub
#End Region

#End Region

#Region "Invoicing Functions & Methods "

#Region "Invoicing Page Load"
    Private Sub InvoicingPageLoad()
        Me.ShowInvoicingPageComponents()
        Try
            ViewState(ApplicationConstants.CustInvoiceIsSearchViewState) = True
            'storing current sort order of any column asc or dsen
            ViewState(ApplicationConstants.InvoiceSortOrderViewState) = ApplicationConstants.InvoiceManageDESCSortOrder
            'function used for storing search parameters which will be used for further page requests if result is 
            'multipage
            SaveInvoiceSearchOptions()
            'setting initial page index of gridview by default its zero

            GVinvoicing.PageIndex = ApplicationConstants.CustInvoiceInitialPageIndex
            'getting and setting row count of resultset
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

        GVinvoicing.VirtualItemCount = GetSearchRowCount()

        ' the data grid will be populated 

        Me.GetInvoicingData()

    End Sub
#End Region

#Region "Saving Inovice search option"

    ''' <summary>
    ''' The function will be called to store the search values on the basis of which the user want make search. 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SaveInvoiceSearchOptions()
        Try
            'make InvoiceSearchBO object and store the search options entered by user untill he/she enters again
            'in this case the values will be overidden
            Dim invoiceSearchBO As InvoiceBO = New InvoiceBO

            ' this value will come from asp session later on. 
            invoiceSearchBO.OrgId = Me.GetOrgId()            

            'set Location ID selected

            If (ddlLocation.SelectedValue.ToString() <> String.Empty) Then
                invoiceSearchBO.LocationId = CType(ddlLocation.SelectedValue, Integer)
            End If

            If (ddlArea.SelectedValue.ToString() <> String.Empty) Then
                invoiceSearchBO.AreaId = CType(ddlArea.SelectedValue, Integer)
            End If

            'set Element ID selected

            If (ddlElement.SelectedValue.ToString() <> String.Empty) Then
                invoiceSearchBO.ElementId = CType(ddlElement.SelectedValue, Integer)
            End If

            'set Priority ID selected

            If (ddlPriority.SelectedValue.ToString() <> String.Empty) Then
                invoiceSearchBO.PriorityId = CType(ddlPriority.SelectedValue, Integer)
            End If

            'set  Status id 

            If (ddlStatus.SelectedValue <> String.Empty) Then
                invoiceSearchBO.StatusId = CType(ddlStatus.SelectedValue, Integer)
            End If

            If (ddlUser.SelectedValue <> String.Empty) Then
                invoiceSearchBO.UserId = CType(ddlUser.SelectedValue, Integer)
            End If

            If (ddlPatch.SelectedValue <> String.Empty) Then
                invoiceSearchBO.PatchId = CType(ddlPatch.SelectedValue, Integer)
            End If

            If (ddlScheme.SelectedValue <> String.Empty) Then
                invoiceSearchBO.SchemeId = CType(ddlScheme.SelectedValue, Integer)
            End If

            If (ddlStage.SelectedValue <> String.Empty) Then
                invoiceSearchBO.StageId = CType(ddlStage.SelectedValue, Integer)
            End If

            If (txtDueDate.Text <> String.Empty) Then
                invoiceSearchBO.DueDate = CType(txtDueDate.Text, String)
            End If

            If (txtPostCode.Text <> String.Empty) Then
                invoiceSearchBO.PostCode = CType(txtPostCode.Text, String)
            End If

            invoiceSearchBO.IsSearch = True


            'store EnquiryLogSearchBO object in viewstate
            ViewState(ApplicationConstants.CustInvoiceSearchOptionsViewState) = invoiceSearchBO
            'store true in viewstate to indicate that current data on gridview is from search
            ViewState(ApplicationConstants.CustInvoiceIsSearchViewState) = True
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub

#End Region

#Region "Get Invoicing Data"

    ''' <summary>
    ''' To fill the invoicing data grid on the basis of some search criteria 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetInvoicingData()
        Dim invBO As InvoiceBO = New InvoiceBO()

        Try
            SaveInvoiceSearchOptions()
            GVinvoicing.PageIndex = ApplicationConstants.CustInvoiceInitialPageIndex
            GVinvoicing.VirtualItemCount = GetSearchRowCount()
            GetInvoicingSearchResults()
            Invoicing_UpdatePanel.Update()
        Catch ex As Exception

            invBO.IsExceptionGenerated = True

        End Try

        If invBO.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        'getting and setting row count of new search results


    End Sub

#End Region

#Region "Get Invoicing Search Results"
    ''' <summary>
    ''' The results will be fetched from db and will be bind to the the invoicing data grid,the result summary will also be stored 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetInvoicingSearchResults()

        Dim invBO As InvoiceBO = New InvoiceBO()
        'reterive search options ContractorPortalBO from viewstate before requesting results for next page of gridview
        invBO = DirectCast(ViewState(ApplicationConstants.CustInvoiceSearchOptionsViewState), InvoiceBO)
        Dim invDS As DataSet = New DataSet()
        'calling GetDataPage function that will further call function in buisness layer
        GetDataPage(GVinvoicing.PageIndex, ApplicationConstants.InvoiceSearchResultPerPage, GVinvoicing.OrderBy, invDS, invBO)
        If invBO.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        'binding gridview with the result returned from db
        'the resultsummary is stored. 

        Me.StoreResultSummary()
        GVinvoicing.DataSource = Nothing
        GVinvoicing.DataSource = invDS
        GVinvoicing.DataBind()

        '' to populate the inner repair gird in the outer Invoicing Grid 

        Dim repairDS As DataSet = New DataSet()

        'Dim faultLogId As Integer = CType(invDS.Tables(0).Rows(0)("FaultLogID"), Integer)
        'PopulateInnerRepairGrid(faultLogId, repairDS)
        'GVRepairInvoicingGrid.DataSourceCacheDurationConverter()



        'Save Invoicing Data Set in view State
        ViewState.Add(ApplicationConstants.INVDataSet, invDS)

    End Sub
#End Region

#Region "Get Search Row Count"

    ''' <summary>
    ''' To get the number of row count 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetSearchRowCount() As Integer
        Try
            Dim objBL As ContractorPortalManager = New ContractorPortalManager()
            Dim invBO As InvoiceBO = New InvoiceBO()
            invBO = DirectCast(ViewState(ApplicationConstants.CustInvoiceSearchOptionsViewState), InvoiceBO)
            Return (objBL.GetInvoiceSearchRowCount(invBO))
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try


    End Function
#End Region

#Region "Calcualate Invoice Value"

    ''' <summary>
    ''' The Gross value against each row will be fetched and will be added so that to get invoice value. 
    ''' </summary>
    ''' <remarks></remarks>
    ''' 

    Private Sub CalcualateInvoiceValue()

        Dim invoBO As InvoiceBO = New InvoiceBO()
        Dim checkStatus(GVinvoicing.VirtualItemCount) As Integer ' = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

        Dim arrcount As Integer
        For arrcount = 0 To GVinvoicing.VirtualItemCount
            checkStatus(arrcount) = 0
        Next
        ViewState.Add(ApplicationConstants.InvoiceCheckStatusArray, checkStatus)
        Dim totalSum As Double = 0.0
        Dim i As Integer = 0
        Dim j As Integer = 0
        For Each row As GridViewRow In Me.GVinvoicing.Rows

            'For Each row As GridViewRow In Me.GVinvoicing.PageIndex
            Dim chkColumn As CheckBox = DirectCast(row.Cells(8).FindControl("chkInvoicing"), CheckBox)
            Dim sumColumn As Label = DirectCast(row.Cells(7).FindControl("lblGross"), Label)

            Try

                If (row.RowType = DataControlRowType.DataRow And sumColumn.Text <> "") Then ' to search in data row only 

                    Dim sum As Double = CType(sumColumn.Text, Double)

                    'For i = 0 To checkStatus.Length
                    If (chkColumn.Checked = True) Then  ' to see if the checkbox is selected 
                        totalSum += sum                 ' the Gross value against the checked control value will be added to the previous sum 

                        checkStatus(i) = 1

                    End If

                    'Next


                End If


                txtInvoicingValue.Text = totalSum





            Catch ex As Exception

                invoBO.IsExceptionGenerated = True

            End Try

            If invoBO.IsExceptionGenerated Then
                Response.Redirect("~/error.aspx")

            End If
            i = i + 1
            'Next
        Next
        ViewState.Add(ApplicationConstants.InvoiceCheckStatusArray, checkStatus)
    End Sub
#End Region

#Region "Process Invoice"
    Private Sub ProcessInvoice()

        Dim FaultLogId
        Dim FaultList As String
        Dim count As Integer
        Dim invoiceBL As ContractorPortalManager = New ContractorPortalManager()
        Dim conBO As ContractorPortalBO = New ContractorPortalBO()
        count = 0
        FaultList = ""
        Dim chkBoxSelected As CheckBox
        'Declare the GridView row obj
        Dim row As GridViewRow
        'Loop through grid to get select all check boxes
        For Each row In GVinvoicing.Rows
            chkBoxSelected = row.Cells(8).FindControl("chkInvoicing")

            If chkBoxSelected.Checked Then
                count = count + 1
                FaultLogId = CType(row.FindControl("hfFaultLogId"), HiddenField).Value
                If count > 1 Then
                    FaultList = FaultList & "," & FaultLogId
                Else
                    FaultList = FaultLogId
                End If
            End If

        Next

        conBO.OrgId = Me.GetOrgId()

        'Call to BL function to save selected faults

        If FaultList = "" Or count = 0 Then

            lblInvoicingError.Text = "Please Select record(s) to be Invocied from grid"
            lblInvoicingError.ForeColor = Drawing.Color.Red

        Else
            invoiceBL.CreateInvoiceBatch(FaultList, conBO)
            lblInvoicingError.Text = "Record(s) Invoiced Successfully"
            lblInvoicingError.ForeColor = Drawing.Color.Green
            Me.GetInvoicingData()
        End If
 

    End Sub
#End Region
#Region "Store Result Summary"
    ''' <summary>
    ''' To store result summary of the grid. 
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StoreResultSummary()
        Try
            Dim row As GridViewRow = GVinvoicing.BottomPagerRow()
            Dim strResultSummary As String = ""

            If GVinvoicing.PageIndex + 1 = GVinvoicing.PageCount Then
                strResultSummary = "Result " & ((GVinvoicing.PageIndex * GVinvoicing.PageSize) + 1) & " to " & GVinvoicing.VirtualItemCount & " of " & GVinvoicing.VirtualItemCount
            Else
                strResultSummary = "Result " & ((GVinvoicing.PageIndex * GVinvoicing.PageSize) + 1) & " to " & ((GVinvoicing.PageIndex + 1) * GVinvoicing.PageSize) & " of " & GVinvoicing.VirtualItemCount
            End If

            'Save Page index & Page Size in View State
            ViewState.Add(ApplicationConstants.ContractorProtalINVPageIndexKey, GVinvoicing.PageIndex)
            ViewState.Add(ApplicationConstants.ContractorProtalINVPageSizeKey, GVinvoicing.PageSize)


            'store result sumarry which will used later on when data bounds tp pager row
            ViewState(ApplicationConstants.InvoicingResultSummary) = strResultSummary
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Bind Invoice Grid"

    ''' <summary>
    '''  To bind the data to the gridview
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindInvoiceGrid()
        Try
            Dim invoiceDS As DataSet = New DataSet()
            Dim invoiceSearchBO As InvoiceBO = New InvoiceBO()
            GetDataPage(GVinvoicing.PageIndex, ApplicationConstants.InvoiceSearchResultPerPage, GVinvoicing.OrderBy, invoiceDS, invoiceSearchBO)
            GVinvoicing.DataSource = invoiceDS
            GVinvoicing.DataBind()
            'ViewState.Add(ApplicationConstants.INVDataSet, invoiceDS)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Get Data Page"

    ''' <summary>
    ''' Function to fetch data from db. 
    ''' </summary>
    ''' <param name="pageIndex">The index of the page of the data grid. </param>
    ''' <param name="pageSize">Number of rows per page</param>
    ''' <param name="sortExpression">Sort expression i.e Ascending or Descending </param>
    ''' <param name="invoiceDS"> The data set to be populated</param>
    ''' <param name="invoiceBO">The invoice BO </param>
    ''' <remarks></remarks>
    ''' 

    Public Sub GetDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef invoiceDS As DataSet, ByVal invoiceBO As InvoiceBO)

        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.CustInvoiceSortByViewState) Is Nothing) Then
            invoiceBO.SortBy = CType(ViewState(ApplicationConstants.CustInvoiceSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use jobsheetid as a default
            invoiceBO.SortBy = ApplicationConstants.CustInvoiceManagSortBy

        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        invoiceBO.PageIndex = pageIndex
        invoiceBO.RowCount = pageSize

        'reteriving sort order from viewstate
        invoiceBO.SortOrder = DirectCast(ViewState(ApplicationConstants.InvoiceSortOrderViewState), String)
        Dim invoiceManager As ContractorPortalManager = New ContractorPortalManager()
        'passing reference of dataset and passing invoiceSearchBO to InvoiceSearchResults function

        'invoiceDS = invoiceManager.GetInvoicingContractPortalDataBL(invoiceDS, invoiceBO)
        invoiceDS = invoiceManager.GetInvoicingContractPortalDataBL(invoiceBO)

        'storing number of rows in the result
        ViewState("NumberofRows") = invoiceDS.Tables(0).Rows.Count

        If invoiceDS.Tables(0).Rows.Count = 0 Then
            Me.lblInvoicingValue.Visible = False
            Me.btnInvoicingSelectAll.Visible = False
            Me.btnInvoicingProcessInvoice.Visible = False
            Me.txtInvoicingValue.Visible = False
        Else
            Me.lblInvoicingValue.Visible = True
            Me.btnInvoicingSelectAll.Visible = True
            Me.btnInvoicingProcessInvoice.Visible = True
            Me.txtInvoicingValue.Visible = True
        End If

        ''Check for exception, if exception occurs the user will be directed to the error page. 
        Dim exceptionFlag As Boolean = invoiceBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

    End Sub
#End Region

#Region "Get Control Reset"

    ''' <summary>
    ''' To Reset all the controls so that to make a new search.  
    ''' </summary>
    ''' <remarks></remarks>
    ''' 

    Public Sub GetControlReset()

        Try
            If (Me.ddlLocation.SelectedIndex > 0) Then

                Me.ddlLocation.SelectedIndex = 0
                Me.ddlArea.SelectedIndex = 0
                Me.ddlElement.SelectedIndex = 0

            End If

            If (Me.ddlTeam.SelectedIndex > 0) Then

                Me.ddlTeam.SelectedIndex = 0
                Me.ddlUser.SelectedIndex = 0

            End If

            If (txtJS.Text <> String.Empty) Then
                txtJS.Text = ""
            End If

            If (txtPostCode.Text <> String.Empty) Then
                txtPostCode.Text = ""
            End If

            If (txtDueDate.Text <> String.Empty) Then
                txtDueDate.Text = ""
            End If

            If (Me.ddlPatch.Items.Count > 0) Then
                Me.ddlPatch.SelectedIndex = 0
            End If

            If (Me.ddlScheme.Items.Count > 0) Then
                Me.ddlScheme.SelectedIndex = 0
            End If

            If (Me.ddlPriority.Items.Count > 0) Then
                Me.ddlPriority.SelectedIndex = 0
            End If

            If (Me.ddlStatus.Items.Count > 0) Then
                Me.ddlStatus.SelectedIndex = 0
            End If

            If (Me.ddlStage.Items.Count > 0) Then
                Me.ddlStage.SelectedIndex = 0
            End If

            txtTBADate.Text = ""
            If (Me.ddlTBAOperative.Items.Count > 0) Then
                Me.ddlTBAOperative.SelectedIndex = 0
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "Invoicing Page Index Change Event"
    ''' <summary>
    ''' Function to handle Grid Index changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub GVinvoicing_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Try
            'assiging new page index to the gridview
            GVinvoicing.PageIndex = e.NewPageIndex
            'calling function that will store and update current result summary being shown in the footer of
            'gridview
            Me.StoreResultSummary()
            'calling functions for populating gridview again after page indexing
            If DirectCast(ViewState(ApplicationConstants.CustInvoiceIsSearchViewState), Boolean) = True Then
                GetInvoicingSearchResults()
            Else
                BindInvoiceGrid()
            End If
            txtInvoicingValue.Text = ""

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "Show Invoicing Page Components"
    Private Sub ShowInvoicingPageComponents()
        Try
            Me.lblGVHeading.Text = "Invoicing"
            Me.Invoicing_UpdatePanel.Visible = True
            Me.UpdateInvoicingGridPanel()
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "Hide Invoicing Page Components"
    Private Sub HideInvoicingPageComponents()
        Try
            Me.Invoicing_UpdatePanel.Visible = False
            Me.UpdateInvoicingGridPanel()
            Me.UpdateAllGridPanel()
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "Update - The Invoicing Update Panel"
    Private Sub UpdateInvoicingGridPanel()
        Me.Invoicing_UpdatePanel.Update()
    End Sub
#End Region

#Region "Hide INV Empty Grid Rows"
    'This function avoid the display of empty data grid rows where 
    'less records are being displayed than page size of grid

    Private Sub HideINVEmptyGridRows()
        Try

            Dim INVDS As DataSet

            'Get Page index & Page Size from View State
            GVinvoicing.PageIndex = ViewState.Item(ApplicationConstants.ContractorProtalINVPageIndexKey)
            GVinvoicing.PageSize = ViewState.Item(ApplicationConstants.ContractorProtalINVPageSizeKey)

            'Get TBA Data Set From view State
            INVDS = ViewState.Item(ApplicationConstants.INVDataSet)
            GVinvoicing.DataSource = INVDS
            GVinvoicing.DataBind()
            'Me.Invoicing_UpdatePanel.Update()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub

    Private Sub HideINVEmptyGridRows1()
        CalcualateInvoiceValue()
        Invoicing_UpdatePanel.Update()
        Try

            Dim INVDS As DataSet

            'Get Page index & Page Size from View State
            GVinvoicing.PageIndex = ViewState.Item(ApplicationConstants.ContractorProtalINVPageIndexKey)
            GVinvoicing.PageSize = ViewState.Item(ApplicationConstants.ContractorProtalINVPageSizeKey)

            'Get TBA Data Set From view State
            INVDS = ViewState.Item(ApplicationConstants.INVDataSet)
            GVinvoicing.DataSource = INVDS
            GVinvoicing.DataBind()


            'Me.Invoicing_UpdatePanel.Update()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#End Region

#End Region

    ''------------------------------------------------------------------------------------------------------------

#Region "Monitoring - Coded By Noor Muhammad - January,2009  "

#Region "Monitoring Events"

    '#Region "Click Event For All Jobs Link Button In Monitoring"

    '    Protected Sub linkMoniAllJobs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles linkMoniAllJobs.Click
    '        Me.GetMonitoringTimeBaseData("AllJobs")
    '    End Sub
    '#End Region

    '#Region "Click Event For Over Due Jobs Link Button For Monitoring"

    '    Protected Sub linkMoniOverDue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles linkMoniOverDue.Click
    '        Me.GetMonitoringTimeBaseData("OverDue")
    '    End Sub

    '#End Region

    '#Region "Click Event For Jobs Due in Next Seven Days Link Button For Monitoring"

    '    Protected Sub linkMoniSeven_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles linkMoniSeven.Click
    '        Me.GetMonitoringTimeBaseData("SevenDays")
    '    End Sub

    '#End Region

    '#Region "Click Event For Jobs Due in Next Fourteen One Days Link Button For Monitoring"

    '    Protected Sub linkMoniOneFour_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles linkMoniOneFour.Click
    '        Me.GetMonitoringTimeBaseData("OneFourDays")
    '    End Sub

    '#End Region

    '#Region "Click Event For Jobs Due in Next Twenty One Days Link Button For Monitoring"

    '    Protected Sub linkMoniTwoOne_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles linkMoniTwoOne.Click
    '        Me.GetMonitoringTimeBaseData("TwoOneDays")
    '    End Sub

    '#End Region

    '#Region "Click Event For Jobs Due in Next Twenty Eight Days Link Button For Monitoring"

    '    Protected Sub linkMoniTwoEight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles linkMoniTwoEight.Click
    '        Me.GetMonitoringTimeBaseData("TwoEightDays")
    '    End Sub

    '#End Region

#Region "Click Event For Export List Button In Monitoring"

    Protected Sub btnMoniExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Me.GenerateMonitoringDataInExcelFile()
        Me.GenerateMonitoringDataInCSVFile()

    End Sub

#End Region

#Region "GV Monitoring Sorting "
    Protected Sub GVMonitoring_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.MonitoringSortByViewState) = e.SortExpression

        'setting pageindex of gridview equal to zero for new search
        GVMonitoring.PageIndex = ApplicationConstants.ContractorPortalMonitoringInitialPageIndex

        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.ContractorPortalMonitoringSortOrderKey), String) = ApplicationConstants.MonitoringDesSortOrder Then
            ViewState(ApplicationConstants.ContractorPortalMonitoringSortOrderKey) = ApplicationConstants.MonitoringAscSortOrder
        Else
            ViewState(ApplicationConstants.ContractorPortalMonitoringSortOrderKey) = ApplicationConstants.MonitoringDesSortOrder
        End If

        GetMonitoringSearchPanelData()

    End Sub
#End Region

#End Region

#Region "Monitoring Methods & Functions "

#Region "Monitoring Page Load "
    Private Sub MonitoringPageLoad()
        'Get Monitoring Data
        Me.GetMonitoringSearchPanelData()

        'Get monitoring time base fault count
        'Me.SetMonitoringTimeBaseLinkButtons()
    End Sub
#End Region

#Region "Get Monitoring Search Panel Data "
    Private Sub GetMonitoringSearchPanelData()
        Try
            'Create the object of contractor portal bo
            Dim conBO As ContractorPortalBO = New ContractorPortalBO()


            'Create the object of contractor business layer
            Dim conBL As ContractorPortalManager = New ContractorPortalManager()

            'Create the obj of dataset
            Dim monitorDS As DataSet = New DataSet()

            'Set the attributes of contractor bo
            Me.SetContractorBOAttributes(conBO)

            'Call to BL function 
            ''conBL.GetMonitoringSearchPanelData(conBO, monitorDS)

            GetMonitoringDataPage(GVMonitoring.PageIndex, ApplicationConstants.MonitoringResultPerPage, GVMonitoring.OrderBy, monitorDS, conBO)

            Dim count = monitorDS.Tables(0).Rows.Count

            If count = 0 Then
                Me.btnMoniExport.Visible = False
            Else
                Me.btnMoniExport.Visible = True
            End If

            'Save Dataset in view state
            ViewState(ApplicationConstants.FaultMonitoringDataSet) = monitorDS

            'Bind the data set with grid 
            Me.GVMonitoring.DataSource = monitorDS

            Me.GVMonitoring.DataBind()
            EditOverDueRecord()

            Me.UpdateMonitoringGridPanel()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub

#End Region

#Region "Edit Over Due Record"
    Public Sub EditOverDueRecord()

        'Dim i As Integer

        For Each row As GridViewRow In GVMonitoring.Rows

            Dim lblOverDue As Label = DirectCast(row.Cells(8).FindControl("lblOverDue"), Label)
            If (lblOverDue.Text <> "") Then
                'row.Style("Color") = "Red"
                'row.ForeColor = Drawing.Color.Red

                Dim jsNumber As Label = DirectCast(row.Cells(0).FindControl("GVLblMoniJSNumber"), Label)
                jsNumber.ForeColor = Drawing.Color.White

                Dim loggDate As Label = DirectCast(row.Cells(0).FindControl("GVLblMoniLogged"), Label)
                loggDate.ForeColor = Drawing.Color.White

                Dim priority As Label = DirectCast(row.Cells(0).FindControl("GVLblMoniPriority"), Label)
                priority.ForeColor = Drawing.Color.White

                Dim dueDateLabel As Label = DirectCast(row.Cells(0).FindControl("GVLblMoniDueDate"), Label)
                dueDateLabel.ForeColor = Drawing.Color.White

                Dim add As Label = DirectCast(row.Cells(0).FindControl("GVLblMoniAddress"), Label)
                add.ForeColor = Drawing.Color.White

                Dim status As Label = DirectCast(row.Cells(0).FindControl("GVLblMoniStage"), Label)
                status.ForeColor = Drawing.Color.White

                Dim faultDetails As Label = DirectCast(row.Cells(0).FindControl("GVLblMoniDescription"), Label)
                faultDetails.ForeColor = Drawing.Color.White

                Dim operative As Label = DirectCast(row.Cells(0).FindControl("GVLblMoniOperative"), Label)
                operative.ForeColor = Drawing.Color.White

                lblOverDue.ForeColor = Drawing.Color.White

                row.BackColor = Drawing.Color.Crimson
                'row.ForeColor = Drawing.Color.Red


            End If
        Next

    End Sub

#End Region

#Region "Get Monitoring Data Page"
    Public Sub GetMonitoringDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef monitoringDS As DataSet, ByVal conBO As ContractorPortalBO)

        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.MonitoringSortByViewState) Is Nothing) Then
            conBO.SortBy = CType(ViewState(ApplicationConstants.MonitoringSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use jobsheetid as a default
            conBO.SortBy = ApplicationConstants.MonitoringSortBy

        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        conBO.PageIndex = pageIndex
        conBO.RowCount = pageSize

        'reteriving sort order from viewstate
        ''conBO.SortOrder = DirectCast(ViewState(ApplicationConstants.MonitoringSortOrderViewState), String)
        conBO.SortOrder = DirectCast(ViewState(ApplicationConstants.ContractorPortalMonitoringSortOrderKey), String)


        'invoiceDS = invoiceManager.GetInvoicingContractPortalDataBL(invoiceDS, invoiceBO)
        ''monitoringDS = invoiceManager.GetInvoicingContractPortalDataBL(InvoiceBO)
        Dim conBL As ContractorPortalManager = New ContractorPortalManager()
        conBL.GetMonitoringSearchPanelData(conBO, monitoringDS)
        'storing number of rows in the result
        ViewState("NumberofRows") = monitoringDS.Tables(0).Rows.Count

        'If invoiceDS.Tables(0).Rows.Count = 0 Then
        '    Me.lblInvoicingValue.Visible = False
        '    Me.btnInvoicingSelectAll.Visible = False
        '    Me.btnInvoicingProcessInvoice.Visible = False
        '    Me.txtInvoicingValue.Visible = False
        'Else
        '    Me.lblInvoicingValue.Visible = True
        '    Me.btnInvoicingSelectAll.Visible = True
        '    Me.btnInvoicingProcessInvoice.Visible = True
        '    Me.txtInvoicingValue.Visible = True
        'End If

        ''Check for exception, if exception occurs the user will be directed to the error page. 
        Dim exceptionFlag As Boolean = conBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

    End Sub
#End Region

#End Region

    '#Region "Get Monitoring Time Base Data "
    '    Private Sub GetMonitoringTimeBaseData(ByVal selectedDateOption As String)
    '        Try
    '            'Create the object of contractor portal bo
    '            Dim conBO As ContractorPortalBO = New ContractorPortalBO()

    '            'Create the object of contractor business layer
    '            Dim conBL As ContractorPortalManager = New ContractorPortalManager()

    '            'Create the obj of dataset
    '            Dim monitorDS As DataSet = New DataSet()

    '            'Set the contractor id
    '            conBO.OrgId = Me.GetOrgId()

    '            'Call to BL function 
    '            conBL.GetMonitoringTimeBaseData(conBO, monitorDS, selectedDateOption)

    '            Dim count = monitorDS.Tables(0).Rows.Count

    '            'Save Dataset in view state
    '            ViewState(ApplicationConstants.FaultMonitoringDataSet) = monitorDS

    '            'Bind the data set with grid 
    '            Me.GVMonitoring.DataSource = monitorDS
    '            Me.GVMonitoring.DataBind()
    '            EditOverDueRecord()
    '            Me.UpdateMonitoringGridPanel()

    '        Catch ex As Exception
    '            Response.Redirect("~/error.aspx")
    '        End Try
    '    End Sub
    '#End Region

    '#Region "Set Monitoring TimeBase Link Buttons "

    '    Private Sub SetMonitoringTimeBaseLinkButtons()
    '        Try

    '            'Create the object of contractor portal bo
    '            Dim conBO As ContractorPortalBO = New ContractorPortalBO()

    '            'Create the object of contractor business layer
    '            Dim conBL As ContractorPortalManager = New ContractorPortalManager()

    '            'Create an array list to save values return from db
    '            Dim conArrayList As ArrayList = New ArrayList()

    '            'Set the contractor id in contractor bo attribute
    '            conBO.OrgId = Me.GetOrgId().ToString()

    '            'Call to BL function 
    '            conBL.GetMonitoringTimeBaseFaultCount(conBO, conArrayList)

    '            'set the text of links these are ordered in Stored Procedure
    '            Me.linkMoniAllJobs.Text = conArrayList.Item(0).ToString()
    '            Me.linkMoniOverDue.Text = conArrayList.Item(1).ToString()
    '            Me.linkMoniSeven.Text = conArrayList.Item(2).ToString()
    '            Me.linkMoniOneFour.Text = conArrayList.Item(3).ToString()
    '            Me.linkMoniTwoOne.Text = conArrayList.Item(4).ToString()
    '            Me.linkMoniTwoEight.Text = conArrayList.Item(5).ToString()

    '        Catch ex As Exception
    '            Response.Redirect("~/error.aspx")
    '        End Try
    '    End Sub
    '#End Region

#Region "Generate Monitoring Data In Excel File "

    Private Sub GenerateMonitoringDataInExcelFile()
        Try

            'Dim excel As New Microsoft.Office.Interop.Excel.ApplicationClass
            'Dim wBook As Microsoft.Office.Interop.Excel.Workbook
            'Dim wSheet As Microsoft.Office.Interop.Excel.Worksheet

            '' Start Excel and get Application object.
            'excel = CreateObject("Excel.Application")

            '' Get a new workbook.
            'wBook = excel.Workbooks.Add
            'wSheet = wBook.ActiveSheet

            ''Create the obj of dataset
            'Dim monitorDS As DataSet = New DataSet()

            ''Retreive Dataset from view satate
            'monitorDS = ViewState(ApplicationConstants.FaultMonitoringDataSet)

            'Dim dt As System.Data.DataTable = monitorDS.Tables(0)
            'Dim dc As System.Data.DataColumn
            'Dim dr As System.Data.DataRow

            'Dim colIndex As Integer = 0
            'Dim rowIndex As Integer = 0

            'For Each dc In dt.Columns
            '    colIndex = colIndex + 1
            '    excel.Cells(1, colIndex) = dc.ColumnName
            'Next

            'For Each dr In dt.Rows
            '    rowIndex = rowIndex + 1
            '    colIndex = 0
            '    For Each dc In dt.Columns
            '        colIndex = colIndex + 1
            '        excel.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName)

            '    Next
            'Next

            'wSheet.Columns.AutoFit()
            'Dim fileName = System.DateTime.Now().ToString()
            'fileName = Replace(Replace(Replace(fileName, ":", ""), "/", ""), " ", "")

            'Dim strFileName As String = ApplicationConstants.ConMonitoringFileDirPath + fileName + ".xls"


            'If System.IO.File.Exists(strFileName) Then
            '    System.IO.File.Delete(strFileName)
            'End If

            'wBook.SaveAs(strFileName)
            'excel.Workbooks.Close()

            'Process.Start("IExplore.exe", "http://localhost:2525/secure/contractorportal/download_file.aspx?f=" + fileName)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub


#End Region

#Region "Generate Monitoring Data In CSV File"
    Private Sub GenerateMonitoringDataInCSVFile()
        Try

            'Create the obj of dataset
            Dim monitorDS As DataSet = New DataSet()

            'Retreive Dataset from view satate
            monitorDS = ViewState(ApplicationConstants.FaultMonitoringDataSet)

            Dim count As Integer = 0

            'Coverting the dataset into data table
            Dim dt As System.Data.DataTable = monitorDS.Tables(0)
            Dim dc As System.Data.DataColumn
            Dim dr As System.Data.DataRow
            Dim arr(dt.Columns.Count) As String

            Dim fileName = System.DateTime.Now().ToString()
            fileName = Replace(Replace(Replace(fileName, ":", ""), "/", ""), " ", "")

            Dim strFileName As String = System.Configuration.ConfigurationManager.AppSettings("contractor_monitoring_file_dir_path") + fileName + ".csv"
            Dim objWriter As New System.IO.StreamWriter(strFileName)


            If System.IO.File.Exists(strFileName) = True Then

                For Each dc In dt.Columns
                    If dc.ColumnName <> "FaultLogID" Then
                        arr(count) = dc.ColumnName
                    End If
                    count = count + 1
                Next

                objWriter.WriteLine(String.Join(",", arr))

                For Each dr In dt.Rows

                    Dim dataArray(dt.Columns.Count) As String
                    Dim incount As Integer = 0
                    For Each dc In dt.Columns
                        If dc.ColumnName <> "FaultLogID" Then
                            dataArray(incount) = GetWriteableValue(dr(dc.ColumnName))
                        End If
                        incount = incount + 1
                    Next
                    objWriter.WriteLine(String.Join(",", dataArray))
                Next

                objWriter.Close()

                'Session.Add("exportFileName", fileName)
                Me.lblDownloadMsg.Visible = True
                Me.lnkNewWindow.Visible = True
                Me.lnkNewWindow.Attributes.Add("onClick", "window.open('download_file.aspx?f=" + fileName + "','_blank','directories=no,height=250,width=250,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no')")

                'System.Diagnostics.Process.Start("IExplore.exe", ApplicationConstants.SeverPath + "secure/contractorportal/download_file.aspx?f=" + fileName)
            Else
                Response.Redirect("~/error.aspx")
            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub

#End Region

#Region "Get Writeable Value"

    Public Function GetWriteableValue(ByVal o As Object)

        If o Is Nothing Or o.Equals(Convert.DBNull) Then
            Return ""
        ElseIf o.ToString().IndexOf(",") = -1 Then
            Return o.ToString()
        Else
            Return "\" + o.ToString() + " \ """
        End If

        Return Nothing
    End Function
#End Region

#Region "Update - The Monitoring Update Panel"
    Private Sub UpdateMonitoringGridPanel()
        Me.UpdatePanel_Monitoring.Update()
    End Sub
#End Region

#Region "Show Monitoring Page Components "
    Private Sub ShowMonitoringPageComponents()
        Try
            Me.lblGVHeading.Text = "Monitoring"
            Me.GVMonitoring.Visible = True
            Me.btnMoniExport.Visible = True
            Me.UpdateMonitoringGridPanel()
            Me.UpdateGridHeadingPanel()
            Me.UpdatePanel_Monitoring.Visible = True
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Hide Monitoring Page Components "
    Private Sub HideMonitoringPageComponents()
        Try
            Me.GVMonitoring.Visible = False
            Me.btnMoniExport.Visible = False
            Me.UpdateMonitoringGridPanel()
            Me.UpdateGridHeadingPanel()
            Me.UpdatePanel_Monitoring.Visible = False
            Me.UpdateAllGridPanel()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region


#End Region



    Protected Sub btnJSPopupClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub

    Protected Sub btnInvoicingProcessInvoice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.ProcessInvoice()
    End Sub

    Protected Sub btnTBAClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub

    Private Sub GVinvoicing_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVinvoicing.PageIndexChanged

    End Sub

    Private Sub GVinvoicing_PageIndexChanging1(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GVinvoicing.PageIndexChanging

    End Sub

    Private Sub GVinvoicing_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVinvoicing.SelectedIndexChanged

    End Sub

    Private Sub GVinvoicing_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GVinvoicing.SelectedIndexChanging

    End Sub


End Class