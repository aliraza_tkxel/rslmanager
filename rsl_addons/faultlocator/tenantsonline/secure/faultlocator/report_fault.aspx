<%@ Page Language="vb"  MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="report_fault.aspx.vb" Inherits="tenantsonline.report_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content  ID = "Content" runat = "server" ContentPlaceHolderID ="page_area" >
   <script type="text/javascript" >
function callme(){
    return false
}

</script>
    <table cellpadding="3" style="border-right: darkgray thin solid; border-top: darkgray thin solid;
        border-left: darkgray thin solid; width: 587px; border-bottom: darkgray thin solid;
        background-color: lightgrey">
        <tr>
            <td style="height: 21px; width: 429px;">
    <table cellpadding="2" cellspacing="3" style="width: 508px; background-color: lightgrey" >
        <tr>
            <td align="left" colspan="4" style="position: static; height: 45px">
                <table cellpadding="5" style="border-right: darkgray thin solid; border-top: darkgray thin solid;
                    border-left: darkgray thin solid; width: 630px; border-bottom: darkgray thin solid;
                     background-color: #eff1f2">
                    <tr>
                        <td align="left" colspan="4" style=" background-color: #eff1f2; width: 492px; height: 38px; background-image: url(../../images/propertyImages/External/external.bmp);">
                        </td>
                    </tr>
                </table>                
            </td>
        </tr>
        <tr>
            <td style="height: 19px;">
                <asp:ImageButton ID="btnPathFence" runat="server" ImageUrl="~/images/propertyImages/External/Path_Fence.bmp" /></td>
            <td style="width: 100px; height: 19px;">
                <asp:ImageButton ID="btnWindows" runat="server" ImageUrl="~/images/propertyImages/External/windows.bmp" />
            </td>
            <td style="width: 81px; height: 19px;">
                <asp:ImageButton ID="btnDoor" runat="server" ImageUrl="~/images/propertyImages/External/door.bmp" /></td>
            <td style="width: 93px; height: 19px;">
                <asp:ImageButton ID="btnGarden" runat="server" ImageUrl="~/images/propertyImages/External/garden.bmp" /></td>
        </tr>
        <tr>
            <td style="width: 101px; height: 6px">
                <asp:ImageButton ID="btnWalls" runat="server" ImageUrl="~/images/propertyImages/External/walls.bmp" /></td>
            <td style="width: 100px; height: 6px">
                <asp:ImageButton ID="btnRoofChimney" runat="server" ImageUrl="~/images/propertyImages/External/roof_chimney.bmp" /></td>
            <td style="width: 81px; height: 6px">
                <asp:ImageButton ID="btnDrive" runat="server" ImageUrl="~/images/propertyImages/External/drive.bmp" /></td>
            <td style="width: 93px; height: 6px">
                <asp:ImageButton ID="btnShed" runat="server" ImageUrl="~/images/propertyImages/External/shed.bmp" /></td>
        </tr>
    </table>
            </td>
           <%-- <td valign="top" style ="background-color:White; border:0px;"><table style="width: 216px">
                    <tr>
                        <td colspan="0" style="font-weight: bold; width: 216px; color: white; font-style: normal;
                            font-family: Arial; background-color: #be0000">
                            Important:</td>
                    </tr>
                    <tr>
                        <td colspan="0" style="width: 216px; color: white; font-family: Arial; background-color: #be0000">
                            Please note that emergency repairs must be reported by calling our emergency hotline
                            on
                        </td>
                    </tr>
                    <tr>
                        <td colspan="0" style="font-weight: bold; font-size: 30px; width: 216px; color: white;
                            font-family: Arial; background-color: #be0000; height: 37px;">
                            0800 123 4567
                         </td>
                    </tr>
                </table></td>--%>
        </tr>
    </table>
    <br />
    
    <table cellpadding="3" style="border-right: darkgray thin solid; border-top: darkgray thin solid;
        border-left: darkgray thin solid; width: 587px; border-bottom: darkgray thin solid;
        background-color: lightgrey">
        <tr>
            <td colspan="3" style="height: 21px">
    <table cellpadding="2" cellspacing="3" style="width: 508px; background-color: lightgrey" >
        <tr>
            <td align="left" colspan="4" style="position: static; height: 44px"><table cellpadding="5" style="border-right: darkgray thin solid; border-top: darkgray thin solid;
                    border-left: darkgray thin solid; width: 630px; border-bottom: darkgray thin solid;
                     background-color: #eff1f2">
                <tr>
                    <td align="left" colspan="4" style=" background-color: #eff1f2; width: 492px; height: 38px; background-image: url(../../images/propertyImages/Internal/internal.bmp);">
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px;">
                <asp:ImageButton ID="btnHallStairLanding" runat="server" ImageUrl="~/images/propertyImages/Internal/hall_stairs_landing.bmp" /></td>
            <td style="width: 100px; height: 19px;">
                <asp:ImageButton ID="btnLivingRoom" runat="server" ImageUrl="~/images/propertyImages/Internal/living_room.bmp" />
            </td>
            <td style="width: 81px; height: 19px;">
                <asp:ImageButton ID="btnDiningRoom" runat="server" ImageUrl="~/images/propertyImages/Internal/dining_room.bmp" /></td>
            <td style="width: 125px; height: 19px;">
                <asp:ImageButton ID="btnBasement" runat="server" ImageUrl="~/images/propertyImages/Internal/basement.bmp" /></td>
        </tr>
        <tr>
            <td style="width: 101px; height: 6px">
                <asp:ImageButton ID="btnKitchen" runat="server" ImageUrl="~/images/propertyImages/Internal/kitchen.bmp" /></td>
            <td style="width: 100px; height: 6px">
                <asp:ImageButton ID="btnBathroom_WC" runat="server" ImageUrl="~/images/propertyImages/Internal/bathroom_WC.bmp" /></td>
            <td style="width: 81px; height: 6px">
                <asp:ImageButton ID="btnBedroom" runat="server" ImageUrl="~/images/propertyImages/Internal/bedroom.bmp" /></td>
            <td style="width: 125px; height: 6px">
                <asp:ImageButton ID="btnLoft" runat="server" ImageUrl="~/images/propertyImages/Internal/loft.bmp" /></td>
        </tr>
    </table>
            </td>
        </tr>
    </table>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel_MostOuter" runat="server" RenderMode="Inline" UpdateMode="Conditional">
        <ContentTemplate>
            <cc2:ModalPopupExtender ID="mdlPropertyJobs" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="btnClose" Drag="true" PopupControlID="pnlPropertyJobs" TargetControlID="lblPopupMainHeading">
            </cc2:ModalPopupExtender>
            <asp:Panel ID="pnlPropertyJobs" runat="server" Height="500px" Width="680px" style="overflow:auto">
                <table style="background-color: white" border="0">
                    <tr>
                        <td style="width: 39px; background-color: #be0000">
                <asp:Label ID="lblPopupMainHeading" runat="server" Text="Jobs Currently Planned For This Property" CssClass="caption" Font-Bold="True" ForeColor="White" Height="1px" Width="308px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 39px">
                            <asp:Label ID="lblPopupMaintenanceAppointmentHeading" runat="server" CssClass="caption" Text="Planned Maintenance Contracts/ Appointments: (within the current year)" Font-Bold="True" Width="651px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 39px">
                            <asp:Label ID="lblPopupAppointments" runat="server" Text="-" CssClass="caption" Width="586px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 39px; height: 1px; background-color: #be0000;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 39px">
                            <asp:Label ID="lblPopupGasServicingHeading" runat="server" CssClass="caption" Font-Bold="True"
                                Text="Gas Servicing Appointments:" Width="315px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 39px">
                            <asp:Label ID="lblPopupGasServicingAppointment" runat="server" CssClass="caption"
                                Text="-" Width="592px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 39px; height: 1px; background-color: #be0000;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 39px">
                            <asp:Label ID="lblPopupFaultsHeading" runat="server" CssClass="caption" Font-Bold="True"
                                Text="Other Reported Faults:" Width="199px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 39px">
                            <asp:Label ID="lblPopupFaults" runat="server" CssClass="caption" Text="-" Width="598px"></asp:Label><asp:GridView
                                ID="grdReportedFaults" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                EmptyDataText="No Record Find" Font-Names="Arial" Font-Size="Small" ForeColor="#333333"
                                GridLines="None" ShowHeader="False" Width="648px">
                                <PagerSettings Visible="False" />
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Date" InsertVisible="False">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportedFaultDate" runat="server" CssClass="cellData" Text='<%# Eval("Date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reported Fault" InsertVisible="False">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportedFaultArea" runat="server" CssClass="cellData" Text='<%# Eval("AreaName") %>'></asp:Label><b>></b><asp:Label ID="lblReportedFaultElement" runat="server" CssClass="cellData" Text='<%# Eval("ElementName") %>'></asp:Label><b>></b><asp:Label ID="lblReportedFaultName" runat="server" CssClass="cellData" Text='<%# Eval("FaultName") %>'></asp:Label> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" InsertVisible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblReportedFaultStatus" runat="server" CssClass="cellData" Text='<%# Eval("StatusName") %>' Width="211px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                                <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 39px; height: 1px; background-color: #be0000;">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                        <asp:Button ID="btnClose" runat="server" Text="Close" />
                        <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Continue Reporting Fault >>" Width="220px"/>
                
                            </td>
                    </tr>
                </table>
                &nbsp;&nbsp;
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>