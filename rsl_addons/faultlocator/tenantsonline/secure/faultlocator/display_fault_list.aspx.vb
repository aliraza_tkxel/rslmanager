Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject


Partial Public Class DisplayFaultList
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage



#Region "Events"

#Region "Page_Init"


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

        'If there's not customer info, he/she will be redirected to ~/error.aspx
        If IsNothing(Session("custHeadBO")) Then
            'Response.Redirect("~/error.aspx")
        End If
        Try
            If IsNothing(Session(FaultConstants.BreadCrumbAreaUrl)) Then

                'Create the obj of customer header bo
                Dim headerBO As CustomerHeaderBO = Nothing

                'Get CustomerHeaderBO from session to get CustomerId
                headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

                Response.Redirect(FaultConstants.LoactionUrlString + headerBO.CustomerID.ToString())
            End If
        Catch ex As Exception
            Response.Redirect("~/secure/faultlocator/report_fault.aspx")
        End Try

    End Sub

#End Region

#Region "Page_Load"

    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            'Call the function to get Fault List 
            Me.GetFaults()

        End If
    End Sub

#End Region

#Region "btn Back  Click"

    Protected Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click

        Dim Url As String = CType(Session(FaultConstants.BreadCrumbAreaUrl), String)
        Url = Url + "?areaId=" + CType(Session(FaultConstants.SelectedAreaId), String)
        Response.RedirectLocation = Url
        Response.Redirect(Response.RedirectLocation)

    End Sub
#End Region

#Region "btnNext_Click"

    Protected Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click

        Me.SaveSelectedFault()

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "GetFaults"
    Private Sub GetFaults()
        Try

            If Request.QueryString("elementId") = String.Empty Then
                Response.Redirect("~/error.aspx")
            End If

            'Create the fault BO
            Dim faultBO As New FaultBO()

            'Get the area ID from Query String
            faultBO.ElementId = Request.QueryString("elementId")

            Dim faultBL As FaultManager = New FaultManager()
            Dim isExceptionGen As Boolean = faultBO.IsExceptionGenerated
            Dim faultList As FaultList = New FaultList()

            faultBL.GetFaults(faultBO, faultList)

            If isExceptionGen Then
                Response.Redirect("~/error.aspx")
            Else
                If faultList.Count > 0 Then

                    rdBtnFaultList.DataValueField = "FaultId"
                    rdBtnFaultList.DataTextField = "Description"
                    rdBtnFaultList.DataSource = faultList                    
                    rdBtnFaultList.DataBind()
                    rdBtnFaultList.Items(0).Selected = True

                Else
                    Response.Redirect("~/error.aspx")
                End If

            End If

        Catch ex As Exception

            Response.Redirect("~/error.aspx")

        End Try

    End Sub

#End Region

#Region "SaveSelectedFault"
    Private Sub SaveSelectedFault()
        Try
            Dim selectedFault = Me.rdBtnFaultList.SelectedItem.Value.ToString()
            Session(FaultConstants.SelectedElementId) = Request.QueryString("elementId")
            Session(FaultConstants.SelectedFaultId) = selectedFault
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
        Response.Redirect("~/secure/faultlocator/report_fault_detail.aspx")

    End Sub
#End Region

#End Region

End Class