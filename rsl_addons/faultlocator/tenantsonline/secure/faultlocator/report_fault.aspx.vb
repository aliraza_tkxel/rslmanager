Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject

Partial Public Class report_fault
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

    Private _propertyId As String
    Private _customerId As Integer
    Private _reportedFaultCount As Integer
    Private _gasAppointmentCount As Integer
    Private _plannedAppointmentCount As Integer
    Private _tenancyId As Integer

#Region "Constructor"
    Public Sub New()

        _propertyId = String.Empty
        _customerId = -1
        _reportedFaultCount = 1
        _gasAppointmentCount = -1
        _plannedAppointmentCount = -1
        _tenancyId = -1

    End Sub
#End Region

#Region "Properties"
    ' Get / Set property for _propertyId
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property

    ' Get / Set property for _customerId
    Public Property CustomerId() As Integer
        Get
            Return _customerId
        End Get
        Set(ByVal value As Integer)
            _customerId = value
        End Set
    End Property

    ' Get / Set property for _customerId
    Public Property TenancyId() As Integer
        Get
            Return _tenancyId
        End Get
        Set(ByVal value As Integer)
            _tenancyId = value
        End Set
    End Property

    ' Get / Set property for _reportedFaultCount
    Public Property ReportedFaultCount() As Integer
        Get
            Return _reportedFaultCount
        End Get
        Set(ByVal value As Integer)
            _reportedFaultCount = value
        End Set
    End Property

    ' Get / Set property for _gasAppointmentCount
    Public Property GasAppointmentCount() As Integer
        Get
            Return _gasAppointmentCount
        End Get
        Set(ByVal value As Integer)
            _gasAppointmentCount = value
        End Set
    End Property

    ' Get / Set property for _plannedAppointmentCount
    Public Property PlannedAppointmentCount() As Integer
        Get
            Return _plannedAppointmentCount
        End Get
        Set(ByVal value As Integer)
            _plannedAppointmentCount = value
        End Set
    End Property

#End Region

#Region "Page Init "

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

        If Request.QueryString("customerid") = String.Empty Then
            Response.Redirect("~/error.aspx")
        End If

        If Request.QueryString("propertyid") = String.Empty Then
            'Response.Redirect("~/error.aspx")
        End If

        Dim custHeadBO As CustomerHeaderBO = New CustomerHeaderBO
        Dim customerID = Request.QueryString("customerid")
        'Dim customerID = 10467

        Me.PropertyId = Request.QueryString("propertyid")
        Me.TenancyId = Request.QueryString("tenancyId")
        'Me.PropertyId = "A720020002"
        'Me.TenancyId = 973199
        Me.CustomerId = customerID

        custHeadBO.CustomerID = customerID
        Session("custHeadBO") = custHeadBO

    End Sub

#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region

#Region "Events "

#Region " btn Path Fence Click "

    Protected Sub btnPathFence_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPathFence.Click
        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/external/path_fence/path_fence.aspx?areaId=16")
        Else
            Response.Redirect("~/secure/faultlocator/external/path_fence/path_fence.aspx?areaId=16")
        End If
    End Sub
#End Region

#Region "btn Windows Click "
    Protected Sub btnWindows_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnWindows.Click
        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/external/windows/windows.aspx?areaId=17")
        Else
            Response.Redirect("~/secure/faultlocator/external/windows/windows.aspx?areaId=17")
        End If
    End Sub
    
#End Region

#Region "btn Door Click "

    Protected Sub btnDoor_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDoor.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/external/doors/door.aspx?areaId=18")
        Else
            Response.Redirect("~/secure/faultlocator/external/doors/door.aspx?areaId=18")
        End If

        Me.checkAppointments()
    End Sub
#End Region

#Region "btn Garden Click "


    Protected Sub btnGarden_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGarden.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/external/gardens/gardens.aspx?areaId=19")
        Else
            Response.Redirect("~/secure/faultlocator/external/gardens/gardens.aspx?areaId=19")
        End If

    End Sub
#End Region

#Region "btn Walls Click "


    Protected Sub btnWalls_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnWalls.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/external/walls/walls.aspx?areaId=20")
        Else
            Response.Redirect("~/secure/faultlocator/external/walls/walls.aspx?areaId=20")
        End If

    End Sub
#End Region

#Region "btn Roof Chimney Click "

    Protected Sub btnRoofChimney_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRoofChimney.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/external/roof_chimney/roof_chimney.aspx?areaId=21")
        Else
            Response.Redirect("~/secure/faultlocator/external/roof_chimney/roof_chimney.aspx?areaId=21")
        End If

    End Sub
#End Region

#Region "btn Drive Click "

    Protected Sub btnDrive_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDrive.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/external/drive/drive.aspx?areaId=22")
        Else
            Response.Redirect("~/secure/faultlocator/external/drive/drive.aspx?areaId=22")
        End If

    End Sub
#End Region

#Region "btn Shed Click "

    Protected Sub btnShed_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShed.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/external/shed/shed.aspx?areaId=23")
        Else
            Response.Redirect("~/secure/faultlocator/external/shed/shed.aspx?areaId=23")
        End If

    End Sub
#End Region

#Region "btn Hall Stair Landing Click "

    Protected Sub btnHallStairLanding_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHallStairLanding.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/internal/hallstairs/hallstairs_fault.aspx?areaId=24")
        Else
            Response.Redirect("~/secure/faultlocator/internal/hallstairs/hallstairs_fault.aspx?areaId=24")
        End If

    End Sub
#End Region

#Region "btn Living Room Click "

    Protected Sub btnLivingRoom_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLivingRoom.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/internal/livingroom/livingroom_fault.aspx?areaId=25")
        Else
            Response.Redirect("~/secure/faultlocator/internal/livingroom/livingroom_fault.aspx?areaId=25")
        End If

    End Sub
#End Region

#Region "btn Dining Room Click "

    Protected Sub btnDiningRoom_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDiningRoom.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/internal/diningroom/diningroom_fault.aspx?areaId=26")
        Else
            Response.Redirect("~/secure/faultlocator/internal/diningroom/diningroom_fault.aspx?areaId=26")
        End If

    End Sub
#End Region

#Region "btn Basement Click "

    Protected Sub btnBasement_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBasement.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/internal/basement/basement_fault.aspx?areaId=27")
        Else
            Response.Redirect("~/secure/faultlocator/internal/basement/basement_fault.aspx?areaId=27")
        End If
    End Sub
#End Region

#Region "btn Kitchen Click "

    Protected Sub btnKitchen_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnKitchen.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/internal/kitchen/kitchen_fault.aspx?areaId=28")
        Else
            Response.Redirect("~/secure/faultlocator/internal/kitchen/kitchen_fault.aspx?areaId=28")
        End If
    End Sub
#End Region

#Region "btn Bathroom WC Click "

    Protected Sub btnBathroom_WC_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBathroom_WC.Click

        If Me.checkAppointments() = True Then
            ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/internal/bathroom/bathroom_fault.aspx?areaId=29")
        Else
            Response.Redirect("~/secure/faultlocator/internal/bathroom/bathroom_fault.aspx?areaId=29")
        End If
    End Sub
#End Region

#Region "btn Bedroom Click "

    Protected Sub btnBedroom_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBedroom.Click

        If Me.checkAppointments() = True Then
            Me.ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/internal/bedroom/bedroom_fault.aspx?areaId=30")
        Else
            Response.Redirect("~/secure/faultlocator/internal/bedroom/bedroom_fault.aspx?areaId=30")
        End If
    End Sub
#End Region

#Region "btn Loft Click "

    Protected Sub btnLoft_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLoft.Click

        If Me.checkAppointments() = True Then
            Me.ShowPopup()
            ViewState.Add("SelectedArea", "~/secure/faultlocator/internal/loft/loft_fault.aspx?areaId=31")
        Else
            Response.Redirect("~/secure/faultlocator/internal/loft/loft_fault.aspx?areaId=31")
        End If
    End Sub
#End Region

#Region "btn Continue Click "

    Protected Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect(ViewState.Item("SelectedArea"))
    End Sub
#End Region

#End Region

#Region "Methods & Functions "

#Region "check Appointments "

    Private Function checkAppointments() As Boolean
        Me.lblPopupAppointments.Text = Me.CheckPlannedMaintenanceAppointments()
        Me.lblPopupGasServicingAppointment.Text = Me.CheckGasServicingAppointments()
        Me.lblPopupFaults.Text = Me.CheckPropertyReportedFaults()

        If Me.ReportedFaultCount <= 0 And Me.GasAppointmentCount <= 0 And Me.PlannedAppointmentCount <= 0 Then
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Check Planned Maintenance Appointments "

    Private Function CheckPlannedMaintenanceAppointments()
        Return UserInfoMsgConstants.ZeroPMCount
    End Function
#End Region

#Region "Check Gas Servicing Appointments "

    Private Function CheckGasServicingAppointments()
        Try

            'Create the obj of customer BO
            Dim objFaultLog As FaultLogBO = New FaultLogBO()

            'Create the obj of business layer
            Dim objBL As FaultManager = New FaultManager()

            'Creat dataset obj
            Dim customerDS As DataSet = New DataSet()

            objFaultLog.CustomerId = Me.CustomerId

            objBL.CheckGasServicingAppointments(customerDS, objFaultLog)

            '' If no record found means no account/rent statement exist and w'll display user msg
            If customerDS.Tables(0).Rows.Count = 0 Then
                Me.GasAppointmentCount = 0
                Return UserInfoMsgConstants.ZeroGasSercingCount
            Else
                'Set & bind dataset
                Me.GasAppointmentCount = customerDS.Tables(0).Rows.Count                 
                Return "- " + customerDS.Tables(0).Rows(0).Item(0).ToString()

            End If

        Catch ex As Exception
            Return UserInfoMsgConstants.ErrorGasServicingAppCountRetreival
        End Try        
    End Function
#End Region

#Region "Check Property Reported Faults "

    Private Function CheckPropertyReportedFaults() As String
        Try

            'Create the obj of fault log BO
            Dim objReportedFaults As FaultLogBO = New FaultLogBO()

            'Create the obj of business layer
            Dim objBL As FaultManager = New FaultManager()

            'Creat dataset obj
            Dim faultDS As DataSet = New DataSet()

            objReportedFaults.CustomerId = Me.CustomerId
            objReportedFaults.PropertyId = Me.PropertyId
            objReportedFaults.TenancyId = Me.TenancyId

            objBL.CheckPropertyReportedFaults(faultDS, objReportedFaults)

            '' If no record found means no account/rent statement exist and w'll display user msg
            If faultDS.Tables(0).Rows.Count = 0 Then
                Me.ReportedFaultCount = 0
                Return UserInfoMsgConstants.ZeroReportedFaultCount
            Else
                'Set & bind dataset
                Me.ReportedFaultCount = faultDS.Tables(0).Rows.Count
                Me.grdReportedFaults.DataSource = faultDS
                Me.grdReportedFaults.DataBind()
                Return ""

            End If

        Catch ex As Exception
            Return UserInfoMsgConstants.ErrorForReportedFaultCountRetreival
        End Try

    End Function

#End Region

#Region "Show Popup "


    Private Sub ShowPopup()
        Me.UpdatePanel_MostOuter.Update()
        Me.mdlPropertyJobs.Show()
    End Sub
#End Region

#End Region

End Class