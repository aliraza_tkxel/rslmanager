﻿<%@ Page Language="vb"  ValidateRequest = "false" EnableEventValidation = "false" MasterPageFile="~/master pages/EnquiryManagement.Master"  AutoEventWireup="false"  CodeBehind="repair_list_management.aspx.vb" Inherits="tenantsonline.RepairListManagement" Title="Tenants Online :: Repair List Management" EnableViewState="true"%>


<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">

<script language ="javascript" type="text/javascript"  >
    
    function ClearAddPopupCtrl()
    {
        document.getElementById ("<%=txtRepairDescription.ClientID %>").value="0";
        document.getElementById("<%= txtNet.ClientID %>").value = "0.0";
        document.getElementById("<%= txtVat.ClientID %>").value = "0.0";
        document.getElementById("<%= txtGross.ClientID %>").value = "0.0";
        document.getElementById("<%= ddlVat.ClientID %>").value = "";                       
        document.getElementById ("<%= rdbPostYes.ClientID %>").checked="true";
        document.getElementById ("<%= rdbStockYes.ClientID %>").checked="true";
        document.getElementById ("<%= rdbRepairYes.ClientID %>").checked="true";                              
    }
    
    function ClearAmendPopupCtrl()
    {                                                    
        document.getElementById("<%= txtAmendNet.ClientID %>").value = "0.0";
        document.getElementById("<%= txtAmendVat.ClientID %>").value = "0.0";
        document.getElementById("<%= txtAmendGross.ClientID %>").value = "0.0";
        document.getElementById("<%= ddlAmendVat.ClientID %>").value = ""; 
        document.getElementById ("<%= rdbAmendPostYes.ClientID %>").checked="true";
        //document.getElementById ("<%= rdbAmendStockYes.ClientID %>").checked="true";
        document.getElementById ("<%= rdbAmendRepairYes.ClientID %>").checked="true";                                  
    }
    
</script>
   <div style="text-align: left">

        <table style="width: 100%; height: 100%">
            <tr>
                <td rowspan="2" valign="top" style="width: 230px">
                    <table style="width: 5%; border-right: tan thin dotted; border-top: tan thin dotted; border-left: tan thin dotted; border-bottom: tan thin dotted;">
                        <tr>
                            <td colspan="2" style="border-bottom: thin dotted">
                                <asp:Label ID="lblSelect" runat="server" Font-Names="Arial" Font-Size="Small" Text="Select:"></asp:Label>
                                <asp:DropDownList ID="ddlSelect" runat="server" Font-Names="Arial" Font-Size="Small" AppendDataBoundItems="True" AutoPostBack="True">
                                    <asp:ListItem Value="0">Audit Trail</asp:ListItem>
                                    <asp:ListItem Value="1">Cancelled Faults</asp:ListItem>
                                    <asp:ListItem Value="2">Fault Management</asp:ListItem>
                                    <asp:ListItem Value="3">Priority Settings</asp:ListItem>
                                    <asp:ListItem Value="4">Pricing Control</asp:ListItem>
                                    <asp:ListItem Value="5" Selected="True">Repair Management</asp:ListItem>                                   
                                    <asp:ListItem Value="6">Reported Faults</asp:ListItem>
                                </asp:DropDownList>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                                </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <asp:Label ID="lblFind" runat="server" Text="Find" Font-Names="Arial" Font-Size="Small" Width="64px" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px;">
                                <asp:UpdatePanel ID="UpdatePanel_Search" runat="server" RenderMode="Inline" UpdateMode="Conditional" ChildrenAsTriggers="False">
                                    <ContentTemplate>
                                <asp:TextBox ID="txtFind" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="50" Width="150px" AutoPostBack="True" EnableViewState="true" OnTextChanged="txtFind_TextChanged"></asp:TextBox>
                                        <br />
                                        <cc2:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtFind" WatermarkText = "Enter Search Criteria">
                                        </cc2:TextBoxWatermarkExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px; height: 26px">
                                <asp:Label ID="lblSearchSCI" runat="server" Text="SCI" CssClass="caption"></asp:Label></td>
                            <td style="width: 145px; height: 26px">
                                <asp:DropDownList ID="ddlSCI" runat="server" AutoPostBack="True">
                                    <asp:ListItem Value="0">No</asp:ListItem>
                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                    <asp:ListItem Value="2">All</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                </td>
                <td style="width: 789px; height: 21px;">
                    <asp:Label ID="lblHeading" runat="server" BackColor="Transparent" Font-Bold="True" Font-Names="Arial"
                        ForeColor="Black" Text="Repair Management" Width="248px"></asp:Label>&nbsp;
                    </td>
            </tr>
            <tr>                
                <td style="width: 789px; text-align: left;vertical-align:top; height: 350px;">
                            <asp:UpdateProgress ID="UpdateProgress_AmendPopup" runat="server" AssociatedUpdatePanelID="UpdatePanel_Grid"
                                DisplayAfter="1">
                                <ProgressTemplate>
                                    <asp:ImageButton ID="btnGridPopupProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                    <asp:UpdatePanel ID="UpdatePanel_Grid" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:UpdatePanel ID="updPnlGeneralMsg" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="lblGeneralMsg" runat="server" CssClass="caption"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                    <asp:GridView ID="grdRepairListManagement" runat="server" AutoGenerateColumns="False"
                        CellPadding="4" EmptyDataText="No Record Find" Font-Names="Arial" Font-Size="Small"
                        ForeColor="#333333" GridLines="None" Width="600px" ShowFooter="True">
                        <PagerSettings Visible="False" />
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="Details" InsertVisible="False">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblDetails" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PostInspection" InsertVisible="False">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPostInspection" runat="server" CssClass="cellData" Text='<%# Bind("PostInspect") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SCI" InsertVisible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblQuantity" runat="server" CssClass="cellData" Text='<%# Bind("StockCondition") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Net" InsertVisible="False">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemTemplate>
                                    <asp:Label ID="lblNetCost" runat="server" CssClass="cellData" Text='<%# System.Math.Round(Eval("NetCost"),2).ToString() %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Active" InsertVisible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblRepairActive" runat="server" CssClass="cellData" Text="<%# Bind('IsActive') %>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amend" ShowHeader="False">
                                <ItemTemplate>
                                    &nbsp;<asp:Button ID="btnAmend" runat="server" CommandArgument='<%# Bind("FaultRepairListID") %>'
                                        Text="Amend" OnClick="btnAmend_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                        <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                        <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" />
                        <EmptyDataTemplate>
                            No Record Found
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <asp:Button ID="btnAddNewRepair" runat="server" Text="Add New Repair" OnClick="btnAddNewRepair_Click" />                           
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    &nbsp; &nbsp;
                    &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;
                    <br />
                    <asp:UpdatePanel ID="updPnlAddNewRepair" runat="server" UpdateMode="Conditional">
                        <ContentTemplate> 
                            <cc2:ModalPopupExtender ID="mdlAddNewRepairPopup" runat="server" TargetControlID="lblRepairDescription" 
                            Drag="true" PopupControlID="pnlAddNewRepair" BackgroundCssClass ="modalBackground" CancelControlID="btnClose" >
                            </cc2:ModalPopupExtender>
                            <asp:Panel ID="pnlAddNewRepair" runat="server" Height="270px" Width="125px" BorderColor="Transparent" BackColor="Transparent" ForeColor="Transparent" HorizontalAlign="Left">
                                <table style="WIDTH:300px; background-color: white;">
                                <tbody>
                                    <tr>
                                        <td style="BACKGROUND-COLOR: #c00000" colspan="2">
                                          &nbsp;&nbsp;&nbsp;
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="height: 21px">
                                            <strong>
                                                <asp:Label ID="Label1" runat="server" Text="Add New Repair" CssClass="caption"></asp:Label></strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="height: 21px">
                                            <asp:UpdatePanel ID="UpdPnlNewRepairMsg" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Label ID="lblNewRepairMsg" runat="server" CssClass="caption"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 349px">
                                            <asp:Label ID="lblRepairDescription" runat="server" Text="Description:" Width="79px" CssClass="caption"></asp:Label></td>
                                            <td><asp:TextBox ID="txtRepairDescription" runat="server" Width="234px"></asp:TextBox></td>                                       
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="height: 2px; background-color: #c00000">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 191px" colspan="2">
                                            <asp:Label ID="lblCost" runat="server" CssClass="caption" Font-Bold="True"
                                                Font-Italic="False" Text="Cost:" Width="39px"></asp:Label></td>
                                        
                                        
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 349px" >
                                            <asp:Label ID="lblNet" runat="server" CssClass="caption" Height="21px" Text="Net             £ :"
                                                Width="96px"></asp:Label>
                                            </td>
                                            <td>
                                            <asp:UpdatePanel ID="updPnlNetCost" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:TextBox ID="txtNet" runat="server" OnTextChanged ="txtNet_TextChanged" AutoPostBack="True" CausesValidation="True" MaxLength="8"  >0.0</asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 349px">
                                            <asp:Label ID="lblVateRate" runat="server" CssClass="caption" Text="Vat Rate:"></asp:Label>
                                            </td>
                                            <td>
                                            <asp:UpdatePanel ID="updPnlVatRate" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:DropDownList ID="ddlVat" runat="server" AutoPostBack ="True"  AppendDataBoundItems="True" Font-Names="Arial"
                                    Font-Size="Small" Width="152px" OnSelectedIndexChanged="ddlVat_SelectedIndexChanged">
                                     <asp:ListItem Value="0">Please select</asp:ListItem>
                                     <asp:ListItem Value="1">20</asp:ListItem>
                                        </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdateProgress ID="updateProgress_VatRate" runat="server" AssociatedUpdatePanelID="updPnlVatRate"
                                                DisplayAfter="10">
                                                <ProgressTemplate>
                                                    <asp:Image ID="imgProgressBarVatRate" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 349px">
                                            <asp:Label ID="lblVat" runat="server" CssClass="caption" Text="Vat          £ :"></asp:Label>
                                            </td>
                                            <td>
                                            <asp:UpdatePanel ID="updPnlVat" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:TextBox ID="txtVat" runat="server" MaxLength="8" AutoPostBack="True" OnTextChanged="txtVat_TextChanged" ReadOnly="True" >0.0</asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 349px">
                                            <asp:Label ID="lblGross" runat="server" CssClass="caption" Text="Gross      £  :"></asp:Label>
                                            </td>
                                            <td>
                                            <asp:UpdatePanel ID="updPnlGross" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <ContentTemplate>
                                            <asp:TextBox ID="txtGross" runat="server" ReadOnly="True">0.0</asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="5" style="height: 2px; background-color: #c00000" valign="top">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 349px">
                                            <asp:Label ID="Label6" runat="server" CssClass="caption" Text="Post Inspection:"></asp:Label>
                                            </td>
                                            <td>
                                            <asp:RadioButton ID="rdbPostYes" runat="server" Text="Yes" GroupName="grpPostInspection" Checked="True"  CssClass="caption"/>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;                                            
                                            <asp:RadioButton ID="rdbPostNo" runat="server" Text="No" GroupName="grpPostInspection" CssClass="caption" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 349px">
                                            <asp:Label ID="lblStockCondition" runat="server" CssClass="caption" Text="Stock Condition Item:"
                                                Width="150px"></asp:Label>
                                                </td>
                                            <td>
                                                <asp:UpdatePanel ID="UpdPnlSCI" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                            <asp:RadioButton ID="rdbStockYes" runat="server" Text="Yes" GroupName="grpStock" CssClass="caption" OnCheckedChanged="rdbStockYes_CheckedChanged" AutoPostBack="True" />
                                                  &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                       <asp:RadioButton ID="rdbStockNo" runat="server" Text="No" GroupName="grpStock"  CssClass="caption" OnCheckedChanged="rdbStockNo_CheckedChanged" AutoPostBack="True" Checked="True"/>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <asp:UpdateProgress ID="UpdateProgress_SCI" runat="server" AssociatedUpdatePanelID="UpdPnlSCI"
                                                    DisplayAfter="10">
                                                    <ProgressTemplate>
                                                        <asp:ImageButton ID="btnSCIProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" rowspan="4" style="height: 1px">
                                            <asp:UpdatePanel ID="UpdPnlSCIDependantOuter" runat="server">
                                                <ContentTemplate>
                                                    <asp:UpdatePanel ID="UpdPnlSCIDependantInner" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <table style="width: 306px" cellpadding="5">
                                                                <tr>
                                                                    <td align="right" style="width: 148px">
                                                                        <asp:Label ID="lblArea" runat="server" Text="Area :" CssClass="caption"></asp:Label></td>
                                                                    <td align="left" colspan="2">
                                                                        <asp:UpdatePanel ID="UpdPnlArea" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:DropDownList ID="ddlArea" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                        <asp:UpdateProgress ID="UpdateProgress_Area" runat="server" AssociatedUpdatePanelID="UpdPnlArea"
                                                                            DisplayAfter="10">
                                                                            <ProgressTemplate>
                                                                                <asp:ImageButton ID="btnAreaProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" style="width: 148px;">
                                                                        <asp:Label ID="lblItem" runat="server" Text="Item :" CssClass="caption"></asp:Label></td>
                                                                    <td align="left" colspan="2">
                                                                        <asp:UpdatePanel ID="UpdPnlItem" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:DropDownList ID="ddlItem" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlItem_SelectedIndexChanged">
                                                                                    <asp:ListItem Selected="True">Please Select</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                        <asp:UpdateProgress ID="UpdateProgress_Item" runat="server" AssociatedUpdatePanelID="UpdPnlItem"
                                                                            DisplayAfter="10">
                                                                            <ProgressTemplate>
                                                                                <asp:ImageButton ID="btnItemProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" style="width: 148px">
                                                                        <asp:Label ID="lblParameter" runat="server" Text="Parameter :" CssClass="caption"></asp:Label></td>
                                                                    <td align="left" colspan="2">
                                                                        <asp:UpdatePanel ID="UpdPnlParameter" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:DropDownList ID="ddlParameter" runat="server" AutoPostBack="True">
                                                                                    <asp:ListItem Selected="True">Please Select</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                        <asp:UpdateProgress ID="UpdateProgress_Parameter" runat="server" AssociatedUpdatePanelID="UpdPnlParameter"
                                                                            DisplayAfter="10">
                                                                            <ProgressTemplate>
                                                                                <asp:ImageButton ID="btnParameterProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                            </ProgressTemplate>
                                                                        </asp:UpdateProgress>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 349px">
                                            <asp:Label ID="lblRepairActive" runat="server" CssClass="caption" Text="Repair Active:"></asp:Label>
                                            </td>
                                            <td>
                                            <asp:RadioButton ID="rdbRepairYes" runat="server" Text="Yes" GroupName="grpRepairActive" Checked="True" CssClass="caption" />
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                            
                                            <asp:RadioButton ID="rdbRepairNo" runat="server" Text="No" GroupName="grpRepairActive"  CssClass="caption"/>
                                        </td>
                                    </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                                    <tr>
                                        <td colspan="2" style="background-color: #c00000">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 37px; width: 349px;" align="right" valign="top">
                                            <asp:Button ID="btnClose" runat="server" Text="Close" />
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" OnClientClick="ClearAddPopupCtrl(); return false;" UseSubmitBehavior="False" />
                                          </td>
                                          <td align="left" valign="top">
                                            <asp:UpdatePanel ID="UpdPnlSaveRepair" runat="server">
                                                <ContentTemplate>
                                            <asp:Button ID="btnSave" runat="server" Text="Save Repair" OnClick="btnSave_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>                                                                                          
                                        
                                    </tr>
                                     <tr>
                                        <td style="BACKGROUND-COLOR: #c00000; height: 21px;" colspan="2">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>                                        
                                    </tr>
                                  </tbody> 
                            </table>
                            </asp:Panel>
                    
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    &nbsp; &nbsp;
                    <br />
                    <asp:UpdatePanel ID="updPnlAmendFault" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table border="1" style="background-color: #ffffff">
                                <tr>
                                    <td>
                                        <table style="width: 500px">
                                            <tbody>
                                                <tr>
                                                    <td align="left" colspan="2" style="background-color: #c00000">
                                                        <asp:Label ID="lblAmendFault" runat="server" BackColor="#C00000" Font-Bold="True"
                                                            Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Amend a Repair"
                                                            Width="35%"></asp:Label>
                                                        <asp:Label ID="lblFaultRepairListID" runat="server" Visible="False"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:UpdatePanel ID="UpdPnlAmendMsg" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:Label ID="lblAmendMsg" runat="server" CssClass="caption"></asp:Label>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 300px">
                                                        <asp:Label ID="lblAmendDescription" runat="server" CssClass="caption" Text="Description:"
                                                            Width="79px"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:TextBox ID="txtAmendDescription" runat="server" Width="234px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="2" style="height: 2px; background-color: #c00000">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 300px">
                                                        <asp:Label ID="lblAmendCost" runat="server" CssClass="caption" Font-Bold="True" Font-Italic="False"
                                                            Text="Cost :" Width="35px"></asp:Label></td>
                                                    <td style="width: 444px">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 300px; height: 26px">
                                                        <asp:Label ID="lblAmendNet" runat="server" CssClass="caption" Height="21px" Text="Net             £ :"
                                                            Width="96px"></asp:Label></td>
                                                    <td style="width: 444px; height: 26px">
                                                        <asp:UpdatePanel ID="updPnlAmendNet" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtAmendNet" runat="server" AutoPostBack="True" OnTextChanged="txtAmendNet_TextChanged"></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 300px">
                                                        <asp:Label ID="lblAmendVateRate" runat="server" CssClass="caption" Text="Vat Rate:"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:UpdatePanel ID="updPnlAmendVatRate" runat="server" RenderMode="Inline"
                                                            UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlAmendVat" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                                                    Font-Names="Arial" Font-Size="Small" OnSelectedIndexChanged="ddlAmendVat_SelectedIndexChanged"
                                                                    Width="152px">
                                                                    <asp:ListItem Value="0">Please select</asp:ListItem>
                                                                    <asp:ListItem Value="1">20</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:UpdateProgress ID="updateProgress_AmendVatRate" runat="server" AssociatedUpdatePanelID="updPnlAmendVatRate"
                                                            DisplayAfter="10">
                                                            <ProgressTemplate>
                                                                <asp:Image ID="imgProgressBarAmendVatRate" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                            </ProgressTemplate>
                                                        </asp:UpdateProgress>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 300px">
                                                        <asp:Label ID="lblAmendVat" runat="server" CssClass="caption" Text="Vat          £ :"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:UpdatePanel ID="updPnlAmendVat" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtAmendVat" runat="server" AutoPostBack="True" OnTextChanged="txtAmendVat_TextChanged"
                                                                    ReadOnly="True"></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 300px">
                                                        <asp:Label ID="lblAmendGross" runat="server" CssClass="caption" Text="Gross      £  :"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:UpdatePanel ID="updPnlAmendGross" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtAmendGross" runat="server" ReadOnly="True"></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" style="height: 2px; background-color: #c00000" valign="top">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 300px; height: 22px;">
                                                        <asp:Label ID="lblAmendPostInspection" runat="server" CssClass="caption" Text="Post Inspection:"></asp:Label>&nbsp;</td>
                                                    <td style="width: 444px; height: 22px;">
                                                        &nbsp;<asp:RadioButton ID="rdbAmendPostYes" runat="server" GroupName="grpPostInspection"
                                                            Text="Yes" CssClass="caption" />
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <asp:RadioButton ID="rdbAmendPostNo" runat="server" GroupName="grpPostInspection"
                                                            Text="No" CssClass="caption" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 300px">
                                                        <asp:Label ID="lblAmendStockCondition" runat="server" CssClass="caption" Text="Stock Condition Item:"
                                                            Width="160px"></asp:Label></td>
                                                    <td style="width: 444px">
                                                        <asp:UpdatePanel ID="UpdPnlAmendSCI" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                &nbsp;<asp:RadioButton ID="rdbAmendStockYes" runat="server" GroupName="grpStock"
                                                            Text="Yes" CssClass="caption" OnCheckedChanged="rdbAmendStockYes_CheckedChanged" AutoPostBack="True" Checked="True" />
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                <asp:RadioButton ID="rdbAmendStockNo" runat="server" GroupName="grpStock" Text="No" CssClass="caption" OnCheckedChanged="rdbAmendStockNo_CheckedChanged" AutoPostBack="True" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:UpdateProgress ID="UpdateProgress_rdbAmendSCI" runat="server" AssociatedUpdatePanelID="UpdPnlAmendSCI"
                                                            DisplayAfter="10">
                                                            <ProgressTemplate>
                                                                <asp:ImageButton ID="btnAmendSCIProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                            </ProgressTemplate>
                                                        </asp:UpdateProgress>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" rowspan="3" style="float: left" valign="top">
                                                        <asp:UpdatePanel ID="UpdPnlAmendSCIDependantOuter" runat="server">
                                                            <ContentTemplate>
                                                        <asp:UpdatePanel ID="updPnlAmendSCIDependentInner" runat="server" RenderMode="Inline" UpdateMode="Conditional"
                                                            Visible="False">
                                                            <ContentTemplate>
                                                                <table border="0" cellpadding="5">
                                                                    <tr>
                                                                        <td style="width: 190px" align="right">
                                                                            <asp:Label ID="lblAmendArea" runat="server" CssClass="caption" Height="19px" Text="Area :  "
                                                                                Width="29px"></asp:Label></td>
                                                                        <td align="left">
                                                                            <asp:UpdatePanel ID="UpdPnlAmendArea" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:DropDownList ID="ddlAmendArea" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAmendArea_SelectedIndexChanged">
                                                                                        <asp:ListItem></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                            <asp:UpdateProgress ID="UpdateProgress_ddlAmendArea" runat="server" AssociatedUpdatePanelID="UpdPnlAmendArea"
                                                                                DisplayAfter="10">
                                                                                <ProgressTemplate>
                                                                                    <asp:ImageButton ID="btnAmendAreaProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 190px" align="right">
                                                                            <asp:Label ID="lblAmendItem" runat="server" CssClass="caption" Height="12px" Text="Item :"
                                                                                Width="5px"></asp:Label></td>
                                                                        <td align="left">
                                                                            <asp:UpdatePanel ID="UpdPnlAmendItem" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:DropDownList ID="ddlAmendItem" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAmendItem_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                            <asp:UpdateProgress ID="UpdateProgress_ddlAmendItem" runat="server" AssociatedUpdatePanelID="UpdPnlAmendItem"
                                                                                DisplayAfter="10">
                                                                                <ProgressTemplate>
                                                                                    <asp:ImageButton ID="btnAmendItemProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 190px" align="right">
                                                                            <asp:Label ID="lblAmendParameter" runat="server" CssClass="caption" Height="15px" Text=" Parameter :"
                                                                                Width="73px"></asp:Label></td>
                                                                        <td align="left">
                                                                            <asp:UpdatePanel ID="UpdPnlAmendParameter" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:DropDownList ID="ddlAmendParameter" runat="server" OnSelectedIndexChanged="ddlAmendParameter_SelectedIndexChanged">
                                                                                        <asp:ListItem Value="0">Please Select</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                            <asp:UpdateProgress ID="UpdateProgress_ddlAmendParameter" runat="server" AssociatedUpdatePanelID="UpdPnlAmendParameter"
                                                                                DisplayAfter="10">
                                                                                <ProgressTemplate>
                                                                                    <asp:ImageButton ID="btnAmendParameterProgress" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" />
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                </tr>
                                                <tr>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="width: 300px; height: 22px;">
                                                        <asp:Label ID="lblAmendRepairActive" runat="server" CssClass="caption" Text="Repair Active:"></asp:Label></td>
                                                    <td style="width: 444px; height: 22px;">
                                                        &nbsp;<asp:RadioButton ID="rdbAmendRepairYes" runat="server" GroupName="grpFaultActive"
                                                            Text="Yes" CssClass="caption" />
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <asp:RadioButton ID="rdbAmendRepairNo" runat="server" GroupName="grpFaultActive" Text="No" CssClass="caption" /></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="2">
                                                        <asp:Button ID="btnShowAmendPopup" runat="server" Style="display: none"
                                                            Visible="False" />
                                                        <cc2:ModalPopupExtender ID="mdlPopupAmendFault" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="btnAmendClose" Drag="True" PopupControlID="updPnlAmendFault" TargetControlID="lblAmendVateRate" PopupDragHandleControlID="lblViewEnquiry">
                                                        </cc2:ModalPopupExtender>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="2" style="height: 2px; background-color: #c00000">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="height: 26px; width: 300px;" valign="top">
                                                        <asp:Button ID="btnAmendClose" runat="server" Text="Close"
                                                            Width="56px" />
                                                        <asp:Button ID="btnAmendReset" runat="server" OnClientClick="ClearAmendPopupCtrl();return false;"
                                                            Text="Reset" UseSubmitBehavior="False" />&nbsp;
                                                        </td>    
                                                        <td align="left" valign="top">
                                                        <asp:UpdatePanel ID="UpdPnlSaveAmend" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                        <asp:Button ID="btnAmendSave" runat="server" Text="Save Amends" onClick="btnAmendSave_Click"/>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        </td>                                                        
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td> 
            </tr>
        </table>
    </div>
    <div id="divPopup">
        
    </div> 

 <center>

 </center>

</asp:Content>
