Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class fault_log
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

#Region "init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

        If Request.QueryString("customerid") = String.Empty Then
            'Response.Redirect("~/error.aspx")
        End If

        'Dim customerID = 1 'Request.QueryString("customerid")
        Dim customerID = Request.QueryString("customerid")

        Dim custHeadBO As CustomerHeaderBO = New CustomerHeaderBO

        custHeadBO.CustomerID = customerID

        Session("custHeadBO") = custHeadBO



    End Sub
#End Region

#Region "page_load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then            
            GetFaultStatusLookUp()
        End If
        GetUnreadFaultCount()
        GetCustomerFault()
        AddAttributeToRadio()

    End Sub
#End Region

#Region "ibtnReport_Click"

    Protected Sub ibtnReport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnReport.Click
        'Create the obj of customer header bo
        Dim headerBO As CustomerHeaderBO = Nothing

        'Get CustomerHeaderBO from session to get CustomerId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Response.Redirect(FaultConstants.LoactionUrlString + headerBO.CustomerID.ToString())
    End Sub
#End Region

#Region "Add attribute to radio buttons"
    Private Sub AddAttributeToRadio()
        Try
            Me.rdbQSixNo.Attributes.Add("onClick", "enableQSeven()")
            Me.rdbQEightNo.Attributes.Add("onClick", "enableQNine()")
            Me.rdbQSixYes.Attributes.Add("onClick", "disableQSeven()")
            Me.rdbQEightYes.Attributes.Add("onClick", "disableQNine()")
            Me.btnPrint.Attributes.Add("onClick", "printSQuestionaire()")

            Me.rdbQSevenOptionOne.Attributes.Add("value", Me.rdbQSevenOptionOne.Text)
            Me.rdbQSevenOptionTwo.Attributes.Add("value", Me.rdbQSevenOptionTwo.Text)
            Me.rdbQSevenOptionThree.Attributes.Add("value", Me.rdbQSevenOptionThree.Text)
            Me.rdbQSevenOptionFour.Attributes.Add("value", Me.rdbQSevenOptionFour.Text)
            Me.rdbQSevenOptionFive.Attributes.Add("value", Me.rdbQSevenOptionFive.Text)
            Me.rdbQSevenOptionSix.Attributes.Add("value", Me.rdbQSevenOptionSix.Text)

            Me.rdbQSevenOptionOne.Attributes.Add("onClick", "setRepairSatisfactoryReason(this)")
            Me.rdbQSevenOptionTwo.Attributes.Add("onClick", "setRepairSatisfactoryReason(this)")
            Me.rdbQSevenOptionThree.Attributes.Add("onClick", "setRepairSatisfactoryReason(this)")
            Me.rdbQSevenOptionFour.Attributes.Add("onClick", "setRepairSatisfactoryReason(this)")
            Me.rdbQSevenOptionFive.Attributes.Add("onClick", "setRepairSatisfactoryReason(this)")
            Me.rdbQSevenOptionSix.Attributes.Add("onClick", "setRepairSatisfactoryReason(this)")

            Me.rdbQNineOptionOne.Attributes.Add("value", Me.rdbQNineOptionOne.Text)
            Me.rdbQNineOptionTwo.Attributes.Add("value", Me.rdbQNineOptionTwo.Text)
            Me.rdbQNineOptionThree.Attributes.Add("value", Me.rdbQNineOptionThree.Text)
            Me.rdbQNineOptionFour.Attributes.Add("value", Me.rdbQNineOptionFour.Text)

            Me.rdbQNineOptionOne.Attributes.Add("onClick", "setAdministratorRepairSatisfactoryReason(this)")
            Me.rdbQNineOptionTwo.Attributes.Add("onClick", "setAdministratorRepairSatisfactoryReason(this)")
            Me.rdbQNineOptionThree.Attributes.Add("onClick", "setAdministratorRepairSatisfactoryReason(this)")
            Me.rdbQNineOptionFour.Attributes.Add("onClick", "setAdministratorRepairSatisfactoryReason(this)")

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "GetCustomerFault"

    ' Get the enquiries customer made
    Private Sub GetCustomerFault()

        Try
            Dim faultLogBO As New FaultLogBO
            ' Obtaining object from Session

            Dim custHeadBO As CustomerHeaderBO = Nothing
            custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            faultLogBO.CustomerId = CType(custHeadBO.CustomerID, Integer)

            Dim faultBL As FaultManager = New FaultManager()
            Dim countFault As Integer


            Try
                Dim ds As DataSet = faultBL.GetCustomerFault(custHeadBO.CustomerID, Me.ddlStatus.SelectedValue)
               
                Me.grdFaultLog.DataSource = ds
                ViewState.Add("myDataSet", ds)
                countFault = ds.Tables(0).Rows.Count
                Me.grdFaultLog.DataBind()
                Me.lblSentEnquiries.Text = "You have " & countFault & " reported fault"
            Catch ex As Exception
                Response.Redirect("~/error.aspx")
            End Try

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub

#End Region

#Region "GetFaultStatusLookUp"
    Private Sub GetFaultStatusLookUp()
        Try
            Dim objBL As New UtilityManager
            Dim spName As String = SprocNameConstants.getFaultStatusLookupForFaultLog
            'Dim spName As String = SprocNameConstants.

            Dim lstLookUp As New LookUpList

            objBL.getLookUpList(spName, lstLookUp)

            If lstLookUp.IsExceptionGenerated Then
                Response.Redirect("~/error.aspx")
            End If

            If Not lstLookUp Is Nothing Then
                Me.ddlStatus.DataSource = lstLookUp
                Me.ddlStatus.DataValueField = "LookUpValue"
                Me.ddlStatus.DataTextField = "LookUpName"
                Me.ddlStatus.Items.Add(New ListItem("All", "0"))
                Me.ddlStatus.DataBind()
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region


#Region "GetUnreadFaultCount"
    Private Sub GetUnreadFaultCount()
        Try

            Dim faultLogBO As New FaultLogBO
            ' Obtaining object from Session

            Dim custHeadBO As CustomerHeaderBO = Nothing
            custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            faultLogBO.CustomerId = CType(custHeadBO.CustomerID, Integer)

            Dim faultBL As FaultManager = New FaultManager()
            Dim resultStatus As Boolean = faultBL.GetUnreadFaultCount(faultLogBO, Me.ddlStatus.SelectedValue)

            ' Check for exception 
            Dim exceptionFlag As Boolean = faultLogBO.IsExceptionGenerated

            If exceptionFlag Then
                Response.Redirect("~/error.aspx")
            End If

            If faultLogBO.UnreadfaultCount > 0 Then
                Me.lblUnreadResponses.Text = "You have " & faultLogBO.UnreadfaultCount & " unread responses"
            Else
                Me.lblUnreadResponses.Text = "You have 0 unread response"
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "HandleQuestionairResponse"
    Private Sub HandleQuestionairResponse(ByRef sender As Object)
        Try
            Dim btn As LinkButton = DirectCast(sender, LinkButton)
            Dim faultLogId As Integer = Integer.Parse(btn.CommandArgument())
            ViewState.Add("faultLogId", faultLogId)

            Me.mdlQuestionairPopup.Show()
            SetDisablePropertyOfRadioButton()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "SetDisablePropertyOfRadioButton"
    Private Sub SetDisablePropertyOfRadioButton()
        Try
            Me.rdbQSevenOptionOne.Checked = False
            Me.rdbQSevenOptionOne.InputAttributes.Add("disabled", "disabled")
            Me.rdbQSevenOptionTwo.InputAttributes.Add("disabled", "disabled")
            Me.rdbQSevenOptionThree.InputAttributes.Add("disabled", "disabled")
            Me.rdbQSevenOptionFour.InputAttributes.Add("disabled", "disabled")
            Me.rdbQSevenOptionFive.InputAttributes.Add("disabled", "disabled")
            Me.rdbQSevenOptionSix.InputAttributes.Add("disabled", "disabled")

            Me.rdbQNineOptionOne.Checked = False
            Me.rdbQNineOptionOne.InputAttributes.Add("disabled", "disabled")
            Me.rdbQNineOptionTwo.InputAttributes.Add("disabled", "disabled")
            Me.rdbQNineOptionThree.InputAttributes.Add("disabled", "disabled")
            Me.rdbQNineOptionFour.InputAttributes.Add("disabled", "disabled")

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Events"

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        GetUnreadFaultCount()
        GetCustomerFault()
    End Sub

    Protected Sub grdFaultLog_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdFaultLog.PageIndexChanging
        Try
            Me.grdFaultLog.PageIndex = e.NewPageIndex
            Dim ds As DataSet = CType(ViewState.Item("myDataSet"), DataSet)
            grdFaultLog.DataSource = ds
            grdFaultLog.DataBind()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub

    Protected Sub lnkbtnQuestionair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.HandleQuestionairResponse(sender)
    End Sub
    Protected Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        SaveSQuestions()
    End Sub

#End Region

#Region "Save satisfactory questions"
    Private Sub SaveSQuestions()
        Try
            Dim faultLogID As Integer
            Dim sQuestionBO As New SQuestionBO
            Dim sAnswerBO As New SQuestionBO

            'Adding question id to sQuestionBO

            sQuestionBO.Add("1")
            sQuestionBO.Add("2")
            sQuestionBO.Add("3")
            sQuestionBO.Add("4")
            sQuestionBO.Add("5")
            sQuestionBO.Add("6")
            sQuestionBO.Add("7")
            sQuestionBO.Add("8")
            sQuestionBO.Add("9")


            sAnswerBO.Add(txtQOne.Text)

            If rdbQTwoYes.Checked <> True Then
                sAnswerBO.Add("No")
            Else
                sAnswerBO.Add("Yes")
            End If

            If rdbQThreeYes.Checked <> True Then
                sAnswerBO.Add("No")
            Else
                sAnswerBO.Add("Yes")
            End If

            If rdbQFourYes.Checked <> True Then
                sAnswerBO.Add("No")
            Else
                sAnswerBO.Add("Yes")
            End If

            If rdbQFiveYes.Checked <> True Then
                sAnswerBO.Add("No")
            Else
                sAnswerBO.Add("Yes")
            End If

            If rdbQSixYes.Checked <> True Then

                sAnswerBO.Add("No, " & Request.Form("hiddenRepairSatisfactoryReason"))
            Else
                sAnswerBO.Add("Yes")
            End If

            If rdbQEightYes.Checked <> True Then
                sAnswerBO.Add("No, " & Request.Form("hiddenAdministratorRepairSatisfactoryReason"))
            Else
                sAnswerBO.Add("Yes")
            End If

            sAnswerBO.Add(txtComment.Text)
            sAnswerBO.Add(txtAction.Text)
            faultLogID = ViewState("faultLogId")

            Dim faultBL As FaultManager = New FaultManager()
            faultBL.SaveSQuestions(sQuestionBO, sAnswerBO, faultLogID)

            ' Check for exception 
            Dim exceptionFlag As Boolean = sQuestionBO.IsExceptionGenerated

            If exceptionFlag Then
                'Me.lblErrorMsg.Visible = True
                'lblErrorMsg.Text = "System cannot submit questionaire because you have already submitted this questionaire against this fault."
                'Response.Redirect("~/error.aspx")
            End If
            ResetSForm()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "ResetSForm"
    Private Sub ResetSForm()
        Try
            txtQOne.Text = ""
            txtComment.Text = ""
            txtAction.Text = ""
            rdbQTwoYes.Checked = False
            rdbQTwoNo.Checked = True
            rdbQThreeYes.Checked = False
            rdbQThreeNo.Checked = True
            rdbQFourYes.Checked = False
            rdbQFourNo.Checked = True
            rdbQFiveYes.Checked = False
            rdbQFiveNo.Checked = True
            rdbQSixYes.Checked = True
            rdbQSixNo.Checked = False
            rdbQEightYes.Checked = True
            rdbQEightNo.Checked = False
            SetDisablePropertyOfRadioButton()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region



    Protected Sub grdFaultLog_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class