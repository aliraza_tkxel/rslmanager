<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/EnquiryManagement.Master"
    CodeBehind="jobsheet_summary_update.aspx.vb" Inherits="tenantsonline.jobsheet_summary_update"
    Title="Job Sheet Summary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
    <table style="width: 100%">
        <tr>
            <td style="width: 248px">
            </td>
            <td>
                <table class="table_box" style="width: 600px; right: -200px" id="TABLE1">
                    <tr>
                        <td colspan="3" style="width: 600px">
                            <asp:Label ID="lblHeading" runat="server" Font-Bold="True" Font-Names="Arial" Text="Report Fault"
                                Width="484px"></asp:Label>&nbsp;
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="height: 2px; width: 600px;">
                            <asp:Label ID="lblJobSheetSummary" runat="server" Font-Bold="True" Font-Names="Arial"
                                Text="Job Sheet Summary" Width="431px"></asp:Label>
                            &nbsp;
                            <asp:Button ID="btnPrint" runat="server" CssClass="noprint" OnClientClick="window.print()"
                                Text="Print" />
                            &nbsp;
                            <hr />
                            <asp:Label ID="lblMessage" runat="server" CssClass="info_message_label" ForeColor="Transparent"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width: 600px">
                            <div style="width: 592px">
                                <div>
                                    <table width="100%">
                                        <tr>
                                            <td valign="top" width="50%">                                                
                                                    <table width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                <asp:Label ID="lblContractorLabel" runat="server" CssClass="caption" Text="Contractor"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblContractor" runat="server" CssClass="caption"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblPriorityLabel" runat="server" CssClass="caption" Text="Priority"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblPriority" runat="server" CssClass="caption"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblReponseTimeLabel" runat="server" CssClass="caption" Text="ResponseTime"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblResponseTime" runat="server" CssClass="caption"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblCompletionLabel" runat="server" CssClass="caption" Text="Completion Due"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCompletion" runat="server" CssClass="caption"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblJsNumberLabel" runat="server" CssClass="caption" Text="JS Number"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblJSNumber" runat="server" CssClass="caption"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblOrderDateLabel" runat="server" CssClass="caption" Text="Order Date"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOrderDate" runat="server" CssClass="caption"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblAsbestorLabel" runat="server" CssClass="caption" Text="Asbestos"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblAsbestos" runat="server" CssClass="caption"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:GridView ID="grdAsbestosList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                    ForeColor="#333333" GridLines="None" CssClass="caption">
                                                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="RISK" HeaderText="Risk" />
                                                                        <asp:BoundField DataField="ELEMENTDISCRIPTION" HeaderText="Description" />
                                                                    </Columns>
                                                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </table>                                                
                                            </td>                                            
                                            <td valign="top" width="50%">
                                                <table width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <asp:Label ID="lblTenantNameLabel" runat="server" CssClass="caption" Text="Name"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblTenantName" runat="server" CssClass="caption"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblAddressLabel" runat="server" CssClass="caption" Text="Address"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblAddress" runat="server" CssClass="caption"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTownLabel" runat="server" CssClass="caption" Text="Town"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblTown" runat="server" CssClass="caption" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblCountryLabel" runat="server" CssClass="caption" Text="Country"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCountry" runat="server" CssClass="caption" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblTelephoneLabel" runat="server" CssClass="caption" EnableViewState="False"
                                                                Text="Telephone:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblTelephone" runat="server" CssClass="caption"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblMobileLabel" runat="server" CssClass="caption" EnableViewState="False"
                                                                Text="Mobile:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMobile" runat="server" CssClass="caption"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblEmailLabel" runat="server" CssClass="caption" Text="Email:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblEmail" runat="server" CssClass="caption"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 248px">
            </td>
            <td>
                <table id="GridTable" class="table_box" style="width: 590px;">
                    <tr>
                        <td colspan="3" style="width: 594px">
                            <asp:GridView ID="grdCustomerReportedFaults" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CellPadding="4" EmptyDataText="No record found" Font-Names="Arial" Font-Size="Small"
                                ForeColor="#333333" GridLines="None" PageSize="1" Width="600px">
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                                <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Area/Item/Element" InsertVisible="False" SortExpression="CreationDate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLocationName" runat="server" CssClass="cellData" Text='<%# Bind("LocationName") %>'></asp:Label>_<asp:Label
                                                ID="lblAreaName" runat="server" CssClass="cellData" Text='<%# Bind("AreaName") %>'></asp:Label>_<asp:Label
                                                    ID="lblElementName" runat="server" CssClass="cellData" Text='<%# Bind("ElementName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" InsertVisible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty" InsertVisible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQuantity" runat="server" CssClass="cellData" Text='<%# Bind("Quantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Priority" InsertVisible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblExpectedTime" runat="server" CssClass="cellData" Text="<%# Bind('FResponseTime') %>"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Recharge Cost">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFRecharge" runat="server" CssClass="cellData" Text="<%# Bind('FRecharge') %>"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Visible="False" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width: 594px">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width: 594px">
                            <table style="width: 550px">
                                <tr>
                                    <td>
                                        &nbsp;<asp:Label ID="lblProblemDays" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                    <td rowspan="3" style="vertical-align: top; width: 275px; text-align: left">
                                        <div style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                                            width: 275px; border-bottom: black thin solid; height: 65px">
                                            <asp:Label ID="lblNotesLabel" runat="server" CssClass="caption"></asp:Label>&nbsp;
                                            <asp:Label ID="lblNotes" runat="server" CssClass="caption"></asp:Label></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRecuringProblem" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCommunalArea" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width: 594px">
                            <hr />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width: 594px">
                            <asp:Label ID="lblPreferredContactTime" runat="server" CssClass="caption" Text="Customer has any preferred contact time or ways in which we could contact them"
                                Width="577px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="width: 594px">
                            <table style="width: 590px">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtPreferredContactTime" runat="server" Columns="90" Height="61px"
                                            ReadOnly="True" Rows="4" TextMode="MultiLine" Width="400px"></asp:TextBox>
                                    </td>
                                    <td style="vertical-align: bottom; width: 1500px">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <asp:Button ID="Button1" runat="server" PostBackUrl="~/secure/faultlocator/fault_locator.aspx"
                                Text="< Back To Reported Faults" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
