Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.Data

Partial Public Class SubmitFaultBasket
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page
    Dim faultToBePreInspected As Integer = 0

#Region "Local Variabls"

    Dim tempFaultIdLocal As Integer = -1

#End Region

#Region "Page_Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

        'rdbApprovedNo.Attributes.Add("onclick", "javascript:alert('The Repair will Not Be Progressed Any Further');")
        'If there's not customer info, he/she will be redirected to ~/error.aspx
        If IsNothing(Session("custHeadBO")) Then
            'Response.Redirect("~/error.aspx")
        End If

        Response.Cache.SetNoStore()

        'Get the Total Faults Submitted by user
        Dim totalReportedFaults As Integer = CType(Session(FaultConstants.TotalReportedFaults), Integer)

        'If session doesnot contain any record that means there's no fault in fault basket 
        'then redirect the user
        If totalReportedFaults <= 0 Then

            Dim customerId = ""

            Try
                'Create the obj of customer header bo
                Dim headerBO As CustomerHeaderBO = Nothing

                'Get CustomerHeaderBO from session to get CustomerId
                headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

                customerId = headerBO.CustomerID.ToString()

            Catch ex As Exception
                Response.Redirect("~/error.aspx")
            End Try

            Response.Redirect(FaultConstants.LoactionUrlString + customerId)


        End If



    End Sub
#End Region

#Region "Page_Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            Me.LoadTempFaultBasket()

            Me.getCustomerInfo()
            GetContractorLookup()
            'GetContractorLookupPreInspection()
        Else
            faultToBePreInspected = CType(ViewState("faultToBePreInspected"), Integer)
            EnableDisableSubmitButton()
        End If

    End Sub

#End Region

#Region "Utility Functions"


#Region "General Get Lookup Values Method"

    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values by running the given stored procedure
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="spName">String: Stored Procedure's name to get data</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with two arguments</remarks>
    ''' 

    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.DataBind()
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.SelectedValue = ""

            If String.Compare(SprocNameConstants.getContractorLookup, spName) = 0 Then
                ddlLookup.SelectedValue = 1270
            End If
        End If

    End Sub

    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values by running the given stored procedure
    ''' against some given parameter
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="spName">String: Stored Procedure's name to get data</param>
    ''' <param name="inParam">String: parameter which has to be given to the stored procedure</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with three arguments</remarks>
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", "0"))
            ddlLookup.DataBind()
        End If
    End Sub

#End Region

#Region "Get Contractor Lookup "

    ''Get Lookup values for Contractors
    Private Sub GetContractorLookup()
        For i As Integer = 0 To grdTempFaultBasket.Rows.Count - 1
            PopulateLookup(DirectCast(grdTempFaultBasket.Rows(i).Cells(6).FindControl("ddlContractor"), DropDownList), SprocNameConstants.getContractorLookup)
        Next

    End Sub
#End Region

#Region "Get Contractor Lookup PreInspection "

    'Get lookup Values for Contractors to be used in Preinspection popup
    'Private Sub GetContractorLookupPreInspection()
    '    PopulateLookup(ddlPreContractor, SprocNameConstants.getContractorLookup)
    'End Sub
#End Region

#Region "Get Team LookUp Vallues "

    'Get Team Look up values
    Private Sub GetTeamLookUpVallues()

        PopulateLookup(ddlTeam, SprocNameConstants.GetPreInspectionTeamLookup)
        'Dim objBL As New UtilityManager
        'Dim spName As String = SprocNameConstants.GetTeamLookup
        'Dim lstLookUp As New LookUpList

        'objBL.getLookUpList(spName, lstLookUp)

        'If lstLookUp.IsExceptionGenerated Then
        '    Response.Redirect("~/error.aspx")
        'End If

        'If Not lstLookUp Is Nothing Then
        '    Me.ddlTeam.Items.Clear()
        '    Me.ddlTeam.DataSource = lstLookUp
        '    Me.ddlTeam.DataValueField = "LookUpValue"
        '    Me.ddlTeam.DataTextField = "LookUpName"
        '    Me.ddlTeam.Items.Add(New ListItem("Please select", ""))
        '    Me.ddlTeam.DataBind()
        'End If

        Me.ddlOperative.Items.Clear()
        Me.ddlOperative.Items.Add(New ListItem("Please select", ""))

    End Sub
#End Region

#Region "Get Operative LookUp Values "

    ''User Lookup Values for Operative
    Private Sub GetOperativeLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlOperative, SprocNameConstants.GetUserLookup, inParam, inParamName)
        UpdatePanel_operative.Update()
    End Sub

#End Region

#End Region




#Region "Events"

#Region "btnSubmit_Click"

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Me.saveFinalFaultBasket()

    End Sub

#End Region

#Region "btn Pre Inspection Click "


    Protected Sub btnPreInspection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblMessage.Text = ""
        GetTeamLookUpVallues()
        Dim btn As Button = DirectCast(sender, Button)
        Dim tempFaultId As String = btn.CommandArgument.ToString()

        lblTempFaultId.Text = tempFaultId
        Me.ClearPreinspectionPopupFields()
        'rdbApprovedNo.Checked = False
        'rdbApprovedYes.Checked = True
        Me.GetTempFaultValues(tempFaultId)
        Me.PopulatePreinspectionPopup(tempFaultId)
        mdlPreInspectPopup.Show()
    End Sub
#End Region

#Region "ddl Team Selected Index Changed "


    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (ddlTeam.SelectedValue <> String.Empty) Then
            Me.GetOperativeLookUpValues(ddlTeam.SelectedValue.ToString(), "@TeamId")
        Else
            Me.ddlOperative.Items.Clear()
            Me.ddlOperative.Items.Add(New ListItem("Please select", ""))
        End If

        If lblValidate.Text <> "" Or ddlTeam.SelectedIndex <> 0 Then

            lblValidate.Text = ""
            updatePanel_ErrorMessage.Update()

        End If

    End Sub
#End Region

#Region "btn Save Changes Click "

    Protected Sub btnSaveChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblValidate.Text = ""
        ValidatePreInspection()

        'SaveInspectionRecord()

    End Sub
#End Region

#Region "rdb Approved No CheckedChanged "

    'Protected Sub rdbApprovedNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.ddlPreContractor.SelectedValue = ""
    '    ddlPreContractor.Enabled = False
    '    updatePanel_ContractorDropDown.Update()

    'End Sub
#End Region

#Region "rdb Approved Yes CheckedChanged "

    'Protected Sub rdbApprovedYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    ddlPreContractor.Enabled = True
    '    updatePanel_ContractorDropDown.Update()

    'End Sub
#End Region

#Region "ddl Operative Selected Index Changed "

    Protected Sub ddlOperative_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If lblValidate.Text <> "" Or ddlOperative.SelectedIndex <> 0 Then
            lblValidate.Text = ""
            updatePanel_ErrorMessage.Update()

        End If

    End Sub
#End Region

#Region "btn Back Click "

    Protected Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim customerId = ""

        Try
            'Create the obj of customer header bo
            Dim headerBO As CustomerHeaderBO = Nothing

            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

            customerId = headerBO.CustomerID.ToString()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
        Response.Redirect(FaultConstants.LoactionUrlString + customerId)
    End Sub
#End Region

#End Region

#Region "LoadTempFaultBasket"
    Protected Sub LoadTempFaultBasket()

        'Create the TempFaultBO
        Dim tempFaultBO As TempFaultBO = New TempFaultBO()

        'Creat the object of business layer
        Dim faultBL As FaultManager = New FaultManager()

        'Create the obj of customer header bo
        Dim headerBO As CustomerHeaderBO = Nothing

        Dim isExceptionGen As Boolean = tempFaultBO.IsExceptionGenerated

        Dim tempFaultDS As DataSet = New DataSet()

        'Get CustomerHeaderBO from session to get CustomerId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        Try
            tempFaultBO.CustomerId = headerBO.CustomerID
            faultBL.GetTempFaultBasket(tempFaultBO, tempFaultDS)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

        ''Check for exception
        If tempFaultBO.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        'Save the number of records in text field
        If tempFaultBO.IsFlagStatus = True And tempFaultDS.Tables(0).Rows.Count > 0 Then

            grdTempFaultBasket.DataSource = tempFaultDS
            grdTempFaultBasket.DataBind()
            EnableDisablePreInspectionButton(tempFaultDS)
        Else
            Response.Redirect(FaultConstants.LoactionUrlString + headerBO.CustomerID.ToString())
        End If


    End Sub

#End Region

#Region "GetCustomerInfo"

    ''Get the customer Info to be displayed on welcome page
    Private Sub getCustomerInfo()
        Try

            Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()

            '' Obtaining object from Session
            Dim custHeadBO As CustomerHeaderBO = Nothing
            custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)


            '' Passing info to custDetailsInfo, that will be used to populate customer's Home page
            custDetailsBO.Id = CType(custHeadBO.CustomerID, Integer)
            custDetailsBO.Tenancy = custHeadBO.TenancyID

            Dim custAddressBO As New CustomerAddressBO
            custDetailsBO.Address = custAddressBO

            Dim objBL As New CustomerManager()

            'Dim resultStatus As Boolean = objBL.GetCustomerInfo(custDetailsBO, False)
            Dim resultStatus As Boolean = objBL.GetCustomerContactDetail(custDetailsBO)

            ''Check for exception 
            Dim exceptionFlag As Boolean = custDetailsBO.IsExceptionGenerated

            If exceptionFlag Then
                Response.Redirect("~/error.aspx")
            End If

            If resultStatus Then

                setPresentationLables(custHeadBO, custDetailsBO)

            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "setPresentationLables"

    Private Sub setPresentationLables(ByRef custHeadBO As CustomerHeaderBO, ByRef custDetailsBO As CustomerDetailsBO)
        'Set Presentation lables from CustomerDetailsBO object
        Me.lblTenantName.Text = custHeadBO.Title & " " & custDetailsBO.FirstName & " " & custDetailsBO.MiddleName & " " & custDetailsBO.LastName
        Me.lblAddress.Text = IIf(custDetailsBO.Address.HouseNumber <> "", custDetailsBO.Address.HouseNumber, "")
        Me.lblAddress.Text &= " " & IIf(custDetailsBO.Address.Address1 <> "", custDetailsBO.Address.Address1, "")
        Me.lblTown.Text = IIf(custDetailsBO.Address.TownCity <> "", custDetailsBO.Address.TownCity, "")
        Me.lblCountry.Text = IIf(custDetailsBO.Address.Coutnty <> "", custDetailsBO.Address.Coutnty, "")
        Me.lblCountry.Text = Me.lblCountry.Text & " " & IIf(custDetailsBO.Address.PostCode <> "", custDetailsBO.Address.PostCode, "")
        Me.lblTelephone.Text = IIf(custDetailsBO.Address.Telephone <> "", custDetailsBO.Address.Telephone, "")
        Me.lblMobile.Text = IIf(custDetailsBO.Address.TelephoneMobile <> "", custDetailsBO.Address.TelephoneMobile, "")
        Me.lblEmail.Text = IIf(custDetailsBO.Address.Email <> "", custDetailsBO.Address.Email, "")

    End Sub

#End Region

#Region "Method to Get TempFault Values"

    Private Sub GetTempFaultValues(ByVal tempFaultID As Int32)

        Dim faultBO As FaultBO = New FaultBO()
        Try
            Dim objFault As FaultManager = New FaultManager()

            Dim faultDS As New DataSet()

            faultBO.FaultID = tempFaultID
            objFault.GetTempFaultValues(faultDS, faultBO)

            If (faultDS.Tables(0).Rows.Count > 0) Or Not (faultDS Is Nothing) Then
                Me.txtCost.Text = faultDS.Tables(0).Rows(0)("NetCost").ToString()
                Dim strResponseTime As Double = 0.0
                Dim dueDate As DateTime = Nothing
                Dim submitDate As DateTime = System.DateTime.Now.ToShortDateString()

                Dim dayHour As String = String.Empty

                strResponseTime = Convert.ToDouble(faultDS.Tables(0).Rows(0)("ResponseTime"))
                'submitDate = Convert.ToDateTime(dueDateDS.Tables(0).Rows(0)("SubmitDate"))
                dayHour = faultDS.Tables(0).Rows(0)("responseDays").ToString()

                If (dayHour = "Days") Then

                    'DateAdd(DateInterval.Day, strResponseTime, submitDate)
                    dueDate = submitDate.Add(New TimeSpan(strResponseTime, 0, 0, 0)).ToString()

                ElseIf (dayHour = "Hours") Then

                    'DateAdd(DateInterval.Hour, strResponseTime, submitDate)
                    dueDate = submitDate.Add(New TimeSpan(strResponseTime, 0, 0)).ToString()

                End If

                Me.txtDueDate.Text = UtilityFunctions.FormatDate(dueDate.ToString())

                If (DirectCast(faultDS.Tables(0).Rows(0)("Recharge"), Boolean) = False) Then
                    rdbRechargeYes.Checked = False
                    rdbRechargeNo.Checked = True
                Else
                    rdbRechargeYes.Checked = True
                    rdbRechargeNo.Checked = False
                End If

            End If


        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

        End Try
    End Sub

#End Region

#Region "saveFinalFaultBasket"

    Private Sub saveFinalFaultBasket()
        'Create the TempFaultBO
        Dim finalFaultBO As FinalFaultBasketBO = New FinalFaultBasketBO()

        'Creat the object of business layer
        Dim faultBL As FaultManager = New FaultManager()

        'Create the object of CustomerBO
        Dim customerBO As New CustomerBO

        'Create the Object of dataset for customer info
        Dim custDs As New DataSet

        'Create the obj of customer header bo
        Dim headerBO As CustomerHeaderBO = Nothing

        Dim isExceptionGen As Boolean = finalFaultBO.IsExceptionGenerated

        'Get CustomerHeaderBO from session to get CustomerId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        finalFaultBO.CustomerId = headerBO.CustomerID

        customerBO.Id = headerBO.CustomerID
        'Get Customer Tenancy id and property id
        Me.getCustomerInfo(custDs, customerBO)
        If Not (custDs Is Nothing) Or custDs.Tables(0).Rows.Count > 0 Then
            finalFaultBO.TenancyId = Convert.ToInt32(custDs.Tables(0).Rows(0)("TENANCY"))
            finalFaultBO.PropertyId = custDs.Tables(0).Rows(0)("PROPERTY").ToString()
        End If

        UpdateFaultContractor()

        finalFaultBO.PrefContactTime = Me.txtPreferredContactTime.Text

        finalFaultBO.SubmitDate = DateTime.Now

        finalFaultBO.IamHappy = IIf(Me.chkIamHappy.Checked = True, 1, 0)
        finalFaultBO.UserId = ASPSession("USERID")

        Session("FAULTBASKETID") = faultBL.saveFinalFaultBasket(finalFaultBO).ToString()



        ''Check for exception
        If finalFaultBO.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not finalFaultBO.IsFlagStatus Then
            Response.Redirect("~/error.aspx")
        Else
            'Now Faults has been submitted successufully. Clear the total count of 
            'fualt from session and redirec to message page
            Session.Remove(FaultConstants.TotalReportedFaults)
            Response.Redirect("~/secure/faultlocator/job_sheet_summary.aspx")
        End If

    End Sub

#End Region

#Region "Get Customer Info"

    Private Sub GetCustomerInfo(ByRef ds As DataSet, ByVal customerBO As CustomerBO)
        Try

            Dim faultDal As New FaultManager()

            faultDal.GetCustomerInfo(ds, CustomerBO)

        Catch ex As Exception

            customerBO.IsExceptionGenerated = True
            customerBO.ExceptionMsg = ex.Message

        End Try
    End Sub

#End Region

#Region "saveFaultJournal"

    Private Sub AddNewJournal()
        'Create an object for Journal Id
        Dim journalId As Int32

        'Create the TempFaultJournalBO
        Dim faultJournalBO As FaultJournalBO = New FaultJournalBO()

        'Creat the object of business layer
        Dim faultBL As FaultManager = New FaultManager()

        'Create the object of CustomerBO
        Dim customerBO As New CustomerBO

        'Create the obj of customer header bo
        Dim headerBO As CustomerHeaderBO = Nothing

        'Create a dataset to get customer tenancy and property id
        Dim custDs As New DataSet

        Dim isExceptionGen As Boolean = faultJournalBO.IsExceptionGenerated

        'Get CustomerHeaderBO from session to get CustomerId
        headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)

        customerBO.Id = headerBO.CustomerID
        'Get Customer Tenancy id and property id
        Me.getCustomerInfo(custDs, customerBO)
        If Not (custDs Is Nothing) Or custDs.Tables(0).Rows.Count > 0 Then
            faultJournalBO.TenancyId = Convert.ToInt32(custDs.Tables(0).Rows(0)("TENANCY"))
            faultJournalBO.PropertyId = custDs.Tables(0).Rows(0)("PROPERTY").ToString()
        End If

        faultJournalBO.CustomerId = headerBO.CustomerID
        faultJournalBO.ItemId = 1
        faultJournalBO.ItemNatureId = 60
        faultJournalBO.CurrentItemStatusId = 1
        faultJournalBO.CreationDate = System.DateTime.Now
        faultJournalBO.NextActionDate = Nothing
        faultJournalBO.LastActionDate = Nothing

        'Save Journal
        journalId = faultBL.AddNewJournal(faultJournalBO)

        ''Check for exception
        If faultJournalBO.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not faultJournalBO.IsFlagStatus Then
            Response.Redirect("~/error.aspx")
        Else
            'Now Faults has been submitted successufully. Clear the total count of 
            'fualt from session and redirec to message page
            Session.Remove(FaultConstants.TotalReportedFaults)
            Response.Redirect("~/secure/faultlocator/fault_submit_success_msg.aspx")
        End If

    End Sub

#End Region

#Region "Save Inspection Record"
    Private Sub SaveInspectionRecord()
        Dim piBO As PreInspectBO = New PreInspectBO()
        'fill piBO attribute from inspection data
        If Me.ddlOperative.SelectedValue <> String.Empty Then
            piBO.UserId = CType(ddlOperative.SelectedValue, Integer)
        End If

        piBO.NetCost = CType(txtCost.Text, Double)
        piBO.Notes = txtNotes.Text

        If Me.txtInspect.Text <> String.Empty Then
            piBO.InspectionDate = UtilityFunctions.stringToDate(txtInspect.Text)
        End If
        If Me.txtDueDate.Text <> String.Empty Then
            piBO.DueDate = UtilityFunctions.stringToDate(txtDueDate.Text)
        End If

        'If Me.ddlPreContractor.SelectedValue <> String.Empty Then
        '    piBO.OrgId = CType(ddlPreContractor.SelectedValue, Integer)

        '    'Else
        '    '    piBO.OrgId = Nothing

        'End If

        'If Me.rdbApprovedYes.Checked = True Then
        '    piBO.Approved = 1
        'Else
        '    piBO.Approved = 0
        'End If
        piBO.TempFaultId = CType(lblTempFaultId.Text, Integer)
        Dim piBL As FaultManager = New FaultManager()
        piBL.SaveInspectionRecordContractor(piBO)

        faultToBePreInspected = faultToBePreInspected - 1
        ViewState("faultToBePreInspected") = faultToBePreInspected

  
        'LoadTempFaultBasket()



    End Sub
#End Region

#Region "Add New Fault RepairHistory"

    Public Sub AddNewRepairHistory(ByRef faultRepairHistoryBO As FaultRepairHistoryBO)
        Try
            Dim faultBL As FaultManager = New FaultManager()
            faultBL.AddNewRepairHistory(faultRepairHistoryBO)

        Catch ex As Exception
            faultRepairHistoryBO.IsExceptionGenerated = True
            faultRepairHistoryBO.ExceptionMsg = ex.Message

        End Try

    End Sub

#End Region

#Region "Enable/Disable Submit Button"
    Public Sub EnableDisableSubmitButton()
        If faultToBePreInspected = 0 Then
            btnSubmit.Enabled = True
            'btnSubmit.Attributes.Remove("onclick")
        Else
            btnSubmit.Enabled = False
            'btnSubmit.Attributes.Add("onclick", "javascript:alert('Please complete the Pre Inspection Details');return false;")
        End If
    End Sub
#End Region
#Region "Enable/Disable PreInspection Button"

    Private Sub EnableDisablePreInspectionButton(ByVal ds As DataSet)
        Try
            For i As Integer = 0 To grdTempFaultBasket.Rows.Count - 1
                If (DirectCast(ds.Tables(0).Rows(i)(7), Boolean) <> True) Then
                    grdTempFaultBasket.Rows(i).Cells(5).Enabled = False
                Else
                    grdTempFaultBasket.Rows(i).Cells(6).Enabled = False
                    grdTempFaultBasket.Rows(i).Cells(5).Enabled = True
                    If Not IsPostBack Then
                        faultToBePreInspected = faultToBePreInspected + 1
                    End If
                    EnableDisableSubmitButton()
                End If


            Next
            ViewState("faultToBePreInspected") = faultToBePreInspected
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "UpdateFaultContractor"

    Private Sub UpdateFaultContractor()
        Try
            'Create the TempFaultBO
            Dim tempFaultBO As TempFaultBO = New TempFaultBO()

            'Creat the object of business layer
            Dim faultBL As FaultManager = New FaultManager()

            Dim isExceptionGen As Boolean = tempFaultBO.IsExceptionGenerated

            Dim ddlContractor As DropDownList = New DropDownList()

            Dim btnPreInspect As Button = New Button
            Dim lblTempFaultId

            Dim tempFaultId As Integer = New Integer()

            Dim row As GridViewRow

            Dim i As Integer = 0
            For Each row In grdTempFaultBasket.Rows

                ddlContractor = row.FindControl("ddlContractor")

                lblTempFaultId = row.FindControl("lblTempFaultId")
                btnPreInspect = row.FindControl("btnPreInspection")

                tempFaultBO.TempFaultId = lblTempFaultId.Text()


                If ddlContractor.SelectedValue = "" Or (grdTempFaultBasket.Rows(i).Cells(5).Enabled = True) Then
                    tempFaultBO.Contractor = 0
                Else
                    tempFaultBO.Contractor = ddlContractor.SelectedItem.Value
                    faultBL.UpdateFaultContractor(tempFaultBO)
                End If
                i = i + 1
                'If Not tempFaultBO.IsFlagStatus Then

                'Me.lblMessage.CssClass = "error_message_label"
                'Me.lblMessage.Text = tempFaultBO.UserMsg
                ' Me.LoadTempFaultBasket()
                'Exit For
                'End If

                'Check for exception
                If tempFaultBO.IsExceptionGenerated Then
                    Response.Redirect("~/error.aspx")
                End If
            Next

            If tempFaultBO.IsFlagStatus Then

                'Me.lblMessage.CssClass = "info_message_label"
                'Me.lblMessage.Text = UserInfoMsgConstants.FaultUpdationSuccess
                'Me.LoadTempFaultBasket()

            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "Validate Pre Inspection "

    Public Sub ValidatePreInspection()

        If ddlTeam.SelectedValue = String.Empty Or ddlTeam.SelectedValue = "Please select" Then

            lblValidate.Text = "Please Choose Team"
            updatePanel_ErrorMessage.Update()

        ElseIf ddlOperative.SelectedIndex = 0 Then

            lblValidate.Text = "Please Choose Operative"
            updatePanel_ErrorMessage.Update()

        ElseIf txtInspect.Text = String.Empty Then

            lblValidate.Text = "Please Choose Inspection Date"
            updatePanel_ErrorMessage.Update()

        ElseIf txtDueDate.Text = "" Then

            lblValidate.Text = "Please Select Due Date"
            updatePanel_ErrorMessage.Update()

            'ElseIf ddlPreContractor.SelectedValue = String.Empty Or ddlPreContractor.SelectedValue = "Please select" Then

            '    lblValidate.Text = "Please Choose Contractor"
            '    updatePanel_ErrorMessage.Update()

            'ElseIf (rdbApprovedYes.Checked = True And ddlPreContractor.SelectedValue = "") Then
            '    lblValidate.Text = "Please Choose a Contractor"
            '    updatePanel_ErrorMessage.Update()

        Else
            SaveInspectionRecord()
            lblMessage.Text = "Successfully Saved"
            updatePanel_lblSaveSuccessfully.Update()
            EnableDisableSubmitButton()
            UpdatePanelSubmit.Update()
            pnlPreInspectionPopup.Update()

        End If
    End Sub
#End Region

#Region "Clear Preinspection Popup Fields "

    Public Sub ClearPreinspectionPopupFields()
        Me.lblValidate.Text = String.Empty
        Me.ddlTeam.SelectedValue = String.Empty
        Me.ddlOperative.SelectedValue = String.Empty
        Me.txtInspect.Text = String.Empty
        Me.txtDueDate.Text = String.Empty
        Me.txtNotes.Text = String.Empty
        'Me.ddlPreContractor.SelectedValue = String.Empty
        'Me.rdbApprovedYes.Checked = True
        'Me.rdbApprovedNo.Checked = False

        Me.lblValidate.Enabled = True
        Me.ddlTeam.Enabled = True
        Me.ddlOperative.Enabled = True
        Me.txtInspect.Enabled = True
        Me.txtDueDate.Enabled = True
        Me.txtNotes.Enabled = True
        'Me.ddlPreContractor.Enabled = True
        'Me.rdbApprovedYes.Enabled = True
        'Me.rdbApprovedNo.Enabled = True
        Me.btnSaveChanges.Enabled = True

    End Sub
#End Region

#Region "Populate Preinspection Popup "

    Public Sub PopulatePreinspectionPopup(ByVal tempFaultID As Int32)
        Dim PIDS As DataSet = New DataSet()
        Dim faultBO As FaultBO = New FaultBO()
        Dim objFaultBL As FaultManager = New FaultManager()
        Try
            faultBO.FaultID = tempFaultID
            objFaultBL.GetPreinspectionPopupData(PIDS, faultBO)

            If (PIDS.Tables(0).Rows.Count > 0) Then
                Me.ddlTeam.SelectedValue = PIDS.Tables(0).Rows(0)("Team").ToString()
                Me.ddlOperative.Items.Clear()
                Me.ddlOperative.Items.Add(New ListItem(PIDS.Tables(0).Rows(0)("UserName").ToString(), ""))
                Me.txtInspect.Text = UtilityFunctions.FormatDate(PIDS.Tables(0).Rows(0)("INSPECTIONDATE").ToString())
                Me.txtDueDate.Text = UtilityFunctions.FormatDate(PIDS.Tables(0).Rows(0)("DUEDATE").ToString())
                Me.txtNotes.Text = PIDS.Tables(0).Rows(0)("NOTES").ToString()
                'If PIDS.Tables(0).Rows(0)("ORGID").ToString() <> "0" Then
                '    Me.ddlPreContractor.Text = PIDS.Tables(0).Rows(0)("ORGID").ToString()
                'End If

                'If PIDS.Tables(0).Rows(0)("ORGID").ToString() <> "0" Then
                '    Me.rdbApprovedYes.Checked = True
                '    Me.rdbApprovedNo.Checked = False
                'Else
                '    Me.rdbApprovedNo.Checked = True
                '    Me.rdbApprovedYes.Checked = False
                'End If
                Me.DisablePreinspectionPopupFields()

            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub

#End Region

#Region "Disable Preinspection Popup Fields "

    Public Sub DisablePreinspectionPopupFields()
        Me.lblValidate.Enabled = False
        Me.ddlTeam.Enabled = False
        Me.ddlOperative.Enabled = False
        Me.txtInspect.Enabled = False
        Me.txtDueDate.Enabled = False
        Me.txtNotes.Enabled = False
        'Me.ddlPreContractor.Enabled = False
        'Me.rdbApprovedYes.Enabled = False
        'Me.rdbApprovedNo.Enabled = False
        Me.btnSaveChanges.Enabled = False

    End Sub
#End Region

End Class