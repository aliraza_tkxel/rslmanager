Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Imports System.Data

Partial Public Class RepairListManagement
    'Inherits System.Web.UI.Page
    'Implements System.Web.UI.ICallbackEventHandler
    Inherits MSDN.SessionPage

#Region "Local Vairables"
    Private _callbackResult As String = Nothing
#End Region

#Region "Events "

#Region "General Events"


#Region "Page Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()
        '  txtFind.Attributes.Add("onkeypress", Page.ClientScript.GetPostBackEventReference(txtFind, "TextChanged"))

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

    End Sub

#End Region

#Region "Page_Load"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        txtFind.Attributes.Add("OnKeyPress", Page.ClientScript.GetPostBackEventReference(txtFind, ""))
        txtFind.Attributes.Add("OnKeyUp", Page.ClientScript.GetPostBackEventReference(txtFind, ""))

        If Not Me.IsPostBack Then
            ddlSCI.SelectedIndex = 2
            Me.SearchFaultRepairList(txtFind.Text, ddlSCI.SelectedIndex)
        End If
        Me.ClearGeneralMsgLabel()
        txtFind.Focus()

    End Sub

#End Region

#Region "ddl Select Selected Index Changed "
    Protected Sub ddlSelect_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSelect.SelectedIndexChanged

        If ddlSelect.SelectedIndex = 0 Then
            Response.Redirect("~/faultmanagement/fault_manager.aspx?option=audit")
        ElseIf ddlSelect.SelectedIndex = 1 Then
            Response.Redirect("~/secure/reports/cancelled_faults.aspx")
        ElseIf ddlSelect.SelectedIndex = 2 Then
            Response.Redirect("~/faultmanagement/fault_manager.aspx?option=manager")
        ElseIf ddlSelect.SelectedIndex = 3 Then
            Response.Redirect("~/faultmanagement/fault_manager.aspx?option=priority")
        ElseIf ddlSelect.SelectedIndex = 4 Then
            Response.Redirect("~/faultmanagement/fault_manager.aspx?option=pricing")
        ElseIf ddlSelect.SelectedIndex = 6 Then
            Response.Redirect("~/secure/faultlocator/fault_locator.aspx")
        End If
    End Sub
#End Region

#Region "txtFind_TextChanged"

    Protected Sub txtFind_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFind.TextChanged

        Me.SearchFaultRepairList(txtFind.Text, ddlSCI.SelectedIndex)
        UpdatePanel_Grid.Update()

    End Sub

#End Region

#End Region

#Region "Add New Repair Events"

#Region "btn Add New Repair Click "

    Protected Sub btnAddNewRepair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        clearPopupControls("NewRepairPopup")

        'Populate the lookup
        Me.GetVatRateLookUpValues("NewRepairVat")

        Me.lblNewRepairMsg.Text = ""
        Me.UpdPnlNewRepairMsg.Update()

        Me.rdbStockNo.Checked = True
        Me.rdbStockYes.Checked = False
        Me.UpdPnlSCI.Update()

        Me.UpdPnlSCIDependantInner.Visible = False
        Me.UpdPnlSCIDependantInner.Update()

        updPnlAddNewRepair.Update()
        mdlAddNewRepairPopup.Show()
    End Sub

#End Region

#Region "txt Net Text Changed "

    Protected Sub txtNet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.PopulateVatValues("Vat")

    End Sub
#End Region

#Region "ddl Vat Selected Index Changed "

    Protected Sub ddlVat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.PopulateVatValues("Vat")
    End Sub
#End Region

#Region "txt Vat Text Changed "
    Protected Sub txtVat_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtGross.Text = (Double.Parse(txtNet.Text) + Double.Parse(txtVat.Text)).ToString()
        updPnlGross.Update()
    End Sub
#End Region

#Region "btn Save Click "

    Protected Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If Me.ValidateNewRepairFields() = True Then
            Me.AddNewRepair()
            Me.updPnlAddNewRepair.Update()
            UpdatePanel_Grid.Update()
        Else
            Me.lblNewRepairMsg.Style.Item("color") = "red"
            Me.UpdPnlNewRepairMsg.Update()
        End If

    End Sub
#End Region

#Region "ddl SCI Selected Index Changed "

    Protected Sub ddlSCI_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSCI.SelectedIndexChanged
        Me.SearchFaultRepairList(txtFind.Text, ddlSCI.SelectedIndex)
        txtFind.Focus()
    End Sub
#End Region

#Region "ddl Area Selected Index Changed "

    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ddlArea.SelectedIndex <> 0 Then
            GetItemLookUpValues(ddlArea.SelectedValue.ToString, "@AreaID", "NewRepair")
            Me.UpdPnlItem.Update()
        Else
            ddlItem.Items.Clear()
            ddlParameter.Items.Clear()

            ddlItem.Items.Add(New ListItem("Please select", ""))
            ddlParameter.Items.Add(New ListItem("Please select", ""))

            Me.UpdPnlItem.Update()
            Me.UpdPnlParameter.Update()


        End If
    End Sub
#End Region

#Region "ddl Item Selected Index Changed "

    Protected Sub ddlItem_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlItem.SelectedIndex <> 0 Then
            GetParameterLookUpValues(ddlItem.SelectedValue.ToString, "@ItemID", "NewRepair")
            Me.UpdPnlParameter.Update()
        Else
            ddlParameter.Items.Clear()
            ddlParameter.Items.Add(New ListItem("Please select", ""))
            Me.UpdPnlParameter.Update()
        End If
    End Sub
#End Region

#Region "rdb StockYes Checked Changed "

    Protected Sub rdbStockYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Me.GetAreaLookUpValues("NewRepair")        
        Me.UpdPnlArea.Update()

        Me.UpdPnlSCIDependantInner.Visible = True
        Me.UpdPnlSCIDependantInner.Update()
    End Sub
#End Region

#Region "rdb Stock No Checked Changed "
    Protected Sub rdbStockNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.UpdPnlSCIDependantInner.Visible = False
        Me.UpdPnlSCIDependantInner.Update()
    End Sub
#End Region

#End Region

#Region "Amend Repair Events"

#Region "btn Amend Click "

    Protected Sub btnAmend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Me.clearPopupControls("AmendRepairPopup")
        Me.GetAreaLookUpValues("AmendRepair")
        Me.GetVatRateLookUpValues("AmendRepairVat")        

        Me.UpdPnlAmendArea.Update()        

        Dim btn As Button = DirectCast(sender, Button)
        Dim faultRepListID As String = btn.CommandArgument.ToString()

        Me.GetRepairValues(CType(faultRepListID, Int32))
        Me.updPnlAmendFault.Update()
        mdlPopupAmendFault.Show()

    End Sub
#End Region

#Region "txt Amend Net Text Changed "

    Protected Sub txtAmendNet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateVatValues("AmendVat")

    End Sub
#End Region

#Region "ddl Amend Vat Selected Index Changed "

    Protected Sub ddlAmendVat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateVatValues("AmendVat")

    End Sub
#End Region

#Region "txt Amend Vat Text Changed "

    Protected Sub txtAmendVat_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtAmendGross.Text = (Double.Parse(txtAmendNet.Text) + Double.Parse(txtAmendVat.Text)).ToString()
        updPnlAmendGross.Update()
    End Sub
#End Region

#Region "btn Amend Save Click "

    Protected Sub btnAmendSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ValidateAmendRepairFields() = True Then
            Me.AmendRepair()
            Me.updPnlAmendFault.Update()
            UpdatePanel_Grid.Update()
        Else
            Me.lblAmendMsg.Style.Item("color") = "red"
            Me.UpdPnlAmendMsg.Update()
        End If
    End Sub
#End Region

#Region "ddl Amend Area Selected Index Changed "

    Protected Sub ddlAmendArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ddlAmendArea.SelectedIndex <> 0 Then
            GetItemLookUpValues(ddlAmendArea.SelectedValue.ToString, "@AreaID", "AmendRepair")
            Me.UpdPnlAmendItem.Update()
        Else
            ddlAmendItem.Items.Clear()
            ddlAmendParameter.Items.Clear()

            ddlAmendItem.Items.Add(New ListItem("Please select", ""))
            ddlAmendParameter.Items.Add(New ListItem("Please select", ""))

            Me.UpdPnlAmendItem.Update()
            Me.UpdPnlAmendParameter.Update()


        End If
    End Sub

#End Region

#Region "ddl Amend Item Selected Index Changed "

    Protected Sub ddlAmendItem_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If ddlAmendItem.SelectedValue <> 0 Then
            GetParameterLookUpValues(ddlAmendItem.SelectedValue.ToString, "@ItemID", "AmendRepair")
            Me.UpdPnlAmendParameter.Update()
        Else
            ddlAmendParameter.Items.Clear()
            ddlAmendParameter.Items.Add(New ListItem("Please select", ""))
            Me.UpdPnlAmendParameter.Update()
        End If
    End Sub
#End Region

#Region "ddl Amend Parameter Selected Index Changed "
    Protected Sub ddlAmendParameter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ddlAmendParameter.SelectedValue > 0 Then
            Me.rdbAmendStockYes.Checked = True
        End If
    End Sub
#End Region

#Region "rdb Amend Stock Yes Checked Changed "
    Protected Sub rdbAmendStockYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        ddlAmendItem.SelectedValue = 0
        ddlAmendParameter.SelectedValue = 0

        Me.updPnlAmendSCIDependentInner.Visible = True
        Me.updPnlAmendSCIDependentInner.Update()
    End Sub
#End Region

#Region "rdb Amend Stock No Checked Changed "
    Protected Sub rdbAmendStockNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.updPnlAmendSCIDependentInner.Visible = False
        Me.updPnlAmendSCIDependentInner.Update()
    End Sub
#End Region

#End Region

#End Region

#Region "Search Faul tRepair List "

    Public Sub SearchFaultRepairList(ByVal repairName As String, ByVal sci As Integer)
        Try

            'Create the instance of Fault Repair List BO
            Dim faultRepListBO As FaultRepairListBO = New FaultRepairListBO()

            'Create the instance of Fault Manager
            Dim faultBL As FaultManager = New FaultManager()

            'Create Fault Repair List Dataset
            Dim faultRepairListDS As DataSet = New DataSet()

            'Set the description of Fault Repair list BO
            'faultRepListBO.Description = Me.txtFind.Text
            faultRepListBO.Description = repairName
            faultRepListBO.StockConditionItem = sci


            'Call the Business Logic Layer (Fault Manager) function 
            faultBL.SearchFaultRepairList(faultRepListBO, faultRepairListDS)

            ''Check for exception
            If faultRepListBO.IsExceptionGenerated Then
                Response.Redirect("~/error.aspx")
            End If

            'If flag is true then set the data set 
            If faultRepListBO.IsFlagStatus = True And faultRepairListDS.Tables(0).Rows.Count > 0 Then

                'Populating the data grid with data set
                ViewState.Add(ApplicationConstants.FaultJobSheetDataSet, faultRepairListDS)

                'Set & bind dataset
                Me.grdRepairListManagement.DataSource = faultRepairListDS
                Me.grdRepairListManagement.DataBind()
                ' UpdatePanel_Grid.Update()
            Else
                'Populating the data grid with data set
                ViewState.Add(ApplicationConstants.FaultJobSheetDataSet, faultRepairListDS)

                'Set & bind dataset
                Me.grdRepairListManagement.DataSource = faultRepairListDS
                Me.grdRepairListManagement.DataBind()
            End If

            '_callbackResult = "faultRepairListDS"

        Catch ex As Exception
            'redirect to error page
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "Get Vat Rate Look Up Values "

    ''Vat Rate Lookup Values
    Private Sub GetVatRateLookUpValues(ByVal ddlVatName As String)

        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.GetVatRate
        Dim lstLookUp As New LookUpList
        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then

            If ddlVatName = "NewRepairVat" Then

                Me.ddlVat.Items.Clear()
                Me.ddlVat.DataSource = lstLookUp
                Me.ddlVat.DataValueField = "LookUpValue"
                Me.ddlVat.DataTextField = "LookUpName"
                Me.ddlVat.Items.Add(New ListItem("Please select", "-1"))
                Me.ddlVat.DataBind()

            ElseIf ddlVatName = "AmendRepairVat" Then

                Me.ddlAmendVat.Items.Clear()
                Me.ddlAmendVat.DataSource = lstLookUp
                Me.ddlAmendVat.DataValueField = "LookUpValue"
                Me.ddlAmendVat.DataTextField = "LookUpName"
                Me.ddlAmendVat.Items.Add(New ListItem("Please select", "-1"))
                Me.ddlAmendVat.DataBind()

            End If


        End If

    End Sub
#End Region

#Region "Method to get Vat Values"

    Private Sub PopulateVatValues(ByVal ddlName As String)
        Dim objFault As New FaultManager()
        Dim vatBO As New VatBO()
        Try
            If (ddlName = "Vat") Then
                vatBO.VatID = CType(ddlVat.SelectedValue, Int32)
                objFault.GetVatValues(vatBO)
            ElseIf (ddlName = "AmendVat") Then
                vatBO.VatID = CType(ddlAmendVat.SelectedValue, Int32)
                objFault.GetVatValues(vatBO)
            End If

            CalculateVat(vatBO, ddlName)
        Catch ex As Exception
            vatBO.IsExceptionGenerated = True
            vatBO.ExceptionMsg = ex.Message

        End Try

    End Sub

#End Region

#Region "Method to Calculate Vat"
    Private Sub CalculateVat(ByRef vatBO As VatBO, ByVal ddlName As String)
        Dim vatAmount As Double
        If (ddlName = "Vat") Then
            If txtNet.Text <> "" Then
                vatAmount = vatBO.VatRate
                txtVat.Text = Math.Round(((Double.Parse(txtNet.Text) * vatAmount) / 100), 2).ToString()
                updPnlVat.Update()
                txtGross.Text = Math.Round(Double.Parse(txtNet.Text) + ((Double.Parse(txtNet.Text) * vatAmount) / 100), 2)
                updPnlGross.Update()
            End If
        Else
            If txtAmendNet.Text <> "" Then
                vatAmount = vatBO.VatRate
                txtAmendVat.Text = Math.Round(((Double.Parse(txtAmendNet.Text) * vatAmount) / 100), 2).ToString()
                updPnlAmendVat.Update()
                txtAmendGross.Text = Math.Round(Double.Parse(txtAmendNet.Text) + ((Double.Parse(txtAmendNet.Text) * vatAmount) / 100), 2)
                updPnlAmendGross.Update()
            End If

        End If

    End Sub
#End Region

#Region "Add New Repair "

    Private Sub AddNewRepair()

        'Create the instance of Fault Repair List BO
        Dim faultRepListBO As FaultRepairListBO = New FaultRepairListBO()

        Dim faultBL As FaultManager = New FaultManager()

        Try
            ''Get Values
            faultRepListBO.Description = txtRepairDescription.Text
            faultRepListBO.NetCost = CType(txtNet.Text, Double)
            faultRepListBO.Vat = CType(txtVat.Text, Double)
            faultRepListBO.Gross = CType(txtGross.Text, Double)
            If (ddlVat.Items.Count > 0) Then
                faultRepListBO.VatRateID = CType(ddlVat.SelectedValue, Int32)
            Else
                faultRepListBO.VatRateID = 0
            End If
            faultRepListBO.PostInspection = IIf(rdbPostYes.Checked = True, 1, 0)
            faultRepListBO.StockConditionItem = IIf(rdbStockYes.Checked = True, 1, 0)
            faultRepListBO.RepairActive = IIf(rdbRepairYes.Checked = True, 1, 0)

            If faultRepListBO.StockConditionItem = 1 Then
                faultRepListBO.ParameterID = Me.ddlParameter.SelectedValue
            End If

            faultBL.AddNewRepair(faultRepListBO)
            If faultRepListBO.IsFlagStatus = True Then
                Me.SetSuccessMsgInGeneralLabel("Add")
                Me.SearchFaultRepairList(txtFind.Text, ddlSCI.SelectedIndex)
            End If

        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Get Repair Values "



    Private Sub GetRepairValues(ByVal faultRepListID As Int32)

        Dim faultRepListBO As FaultRepairListBO = New FaultRepairListBO()
        Try
            Dim faultBL As FaultManager = New FaultManager()

            Dim faultDS As New DataSet()

            faultRepListBO.FaultRepairListID = faultRepListID
            faultBL.GetRepairValues(faultDS, faultRepListBO)
            Me.PopulateAmendRepairFields(faultDS)

        Catch ex As Exception
            faultRepListBO.IsExceptionGenerated = True
            faultRepListBO.ExceptionMsg = ex.Message
        End Try
    End Sub

#End Region

#Region "Clear Popup Controls "
    Private Sub clearPopupControls(ByVal popupType As String)
        If (popupType = "NewRepairPopup") Then

            txtRepairDescription.Text = ""
            txtNet.Text = "0.0"
            txtVat.Text = "0.0"
            txtGross.Text = "0.0"
            ddlVat.SelectedIndex = -1
            ddlArea.SelectedIndex = 0
            ddlItem.SelectedIndex = 0
            ddlParameter.SelectedIndex = 0
            rdbPostYes.Checked = True
            rdbPostNo.Checked = False
            rdbRepairYes.Checked = True
            rdbRepairNo.Checked = False
            rdbStockYes.Checked = True
            rdbStockNo.Checked = False

        ElseIf (popupType = "AmendRepairPopup") Then

            txtAmendDescription.Text = ""
            txtAmendNet.Text = "0.0"
            txtAmendVat.Text = "0.0"
            txtAmendGross.Text = "0.0"

            ddlAmendVat.SelectedIndex = -1

            rdbAmendPostYes.Checked = True
            rdbAmendPostNo.Checked = False
            rdbAmendRepairYes.Checked = True
            rdbAmendRepairNo.Checked = False
            rdbAmendStockYes.Checked = True
            rdbAmendStockNo.Checked = False

            lblAmendMsg.Text = ""
            UpdPnlAmendMsg.Update()

        End If
        
    End Sub
#End Region

#Region "Populate Amend Repair Fields"
    Private Sub PopulateAmendRepairFields(ByVal faultDS As DataSet)

        If faultDS.Tables(0).Rows.Count > 0 And Not (faultDS Is Nothing) Then

            Me.lblFaultRepairListID.Text = faultDS.Tables(0).Rows(0)("FaultRepairListID").ToString()
            Me.txtAmendDescription.Text = faultDS.Tables(0).Rows(0)("Description").ToString()
            Me.txtAmendNet.Text = faultDS.Tables(0).Rows(0)("NetCost").ToString()
            Me.txtAmendVat.Text = faultDS.Tables(0).Rows(0)("Vat").ToString()
            Me.txtAmendGross.Text = faultDS.Tables(0).Rows(0)("Gross").ToString()
            Me.ddlAmendVat.SelectedValue = CType(faultDS.Tables(0).Rows(0)("VatRateID"), Int32)

            If CType(faultDS.Tables(0).Rows(0)("PostInspection"), Boolean) = True Then
                rdbAmendPostYes.Checked = True
                rdbAmendPostNo.Checked = False
            Else
                rdbAmendPostNo.Checked = True
                rdbAmendPostYes.Checked = False

            End If

            If CType(faultDS.Tables(0).Rows(0)("StockConditionItem"), Boolean) = True Then
                rdbAmendStockYes.Checked = True
                rdbAmendStockNo.Checked = False

                Me.ddlAmendArea.SelectedValue = CType(IIf(IsDBNull(faultDS.Tables(0).Rows(0)("AreaID")) = True, 0, faultDS.Tables(0).Rows(0)("AreaID")), Integer)

                Me.GetItemLookUpValues(ddlAmendArea.SelectedValue.ToString, "@AreaID", "AmendRepair")
                Me.ddlAmendItem.SelectedValue = CType(IIf(IsDBNull(faultDS.Tables(0).Rows(0)("ItemID")) = True, 0, faultDS.Tables(0).Rows(0)("ItemID")), Integer)

                Me.GetParameterLookUpValues(ddlAmendItem.SelectedValue.ToString, "@ItemID", "AmendRepair")
                Me.ddlAmendParameter.SelectedValue = CType(IIf(IsDBNull(faultDS.Tables(0).Rows(0)("ItemID")) = True, 0, faultDS.Tables(0).Rows(0)("ParameterID")), Integer)


                Me.UpdPnlAmendItem.Update()
                Me.UpdPnlAmendParameter.Update()

                Me.updPnlAmendSCIDependentInner.Visible = True
                Me.updPnlAmendSCIDependentInner.Update()
            Else
                rdbAmendStockNo.Checked = True
                rdbAmendStockYes.Checked = False

                Me.updPnlAmendSCIDependentInner.Visible = False
                Me.updPnlAmendSCIDependentInner.Update()
            End If

            If CType(faultDS.Tables(0).Rows(0)("RepairActive"), Boolean) = True Then
                rdbAmendRepairYes.Checked = True
                rdbAmendRepairNo.Checked = False
            Else
                rdbAmendRepairNo.Checked = True
                rdbAmendRepairYes.Checked = False

            End If
        End If
    End Sub

#End Region

#Region "Amend Repair "

    Private Sub AmendRepair()
        Dim objFault As FaultManager = New FaultManager()
        Dim faultRepListBO As FaultRepairListBO = New FaultRepairListBO()
        Try
            ''Get Values from text boxes
            faultRepListBO.FaultRepairListID = CType(lblFaultRepairListID.Text, Int32)
            faultRepListBO.Description = CType(txtAmendDescription.Text, String)
            faultRepListBO.NetCost = CType(txtAmendNet.Text, Double)
            faultRepListBO.Vat = CType(txtAmendVat.Text, Double)
            faultRepListBO.Gross = CType(txtAmendGross.Text, Double)

            ''Get vat drop down value
            If (ddlAmendVat.Items.Count > 0) Then
                faultRepListBO.VatRateID = CType(ddlAmendVat.SelectedValue, Int32)
            Else
                faultRepListBO.VatRateID = 0
            End If

            ''Get the value from the check boxes
            faultRepListBO.PostInspection = IIf(rdbAmendPostYes.Checked = True, 1, 0)
            faultRepListBO.StockConditionItem = IIf(rdbAmendStockYes.Checked = True, 1, 0)
            faultRepListBO.RepairActive = IIf(Me.rdbAmendRepairYes.Checked = True, 1, 0)

            ''Get value from parameter drop down
            If faultRepListBO.StockConditionItem = 1 Then
                faultRepListBO.ParameterID = Me.ddlAmendParameter.SelectedValue
            End If

            ''Call to business layer to Amend repair
            objFault.AmendRepair(faultRepListBO)

            ''Check if Save successfully
            If faultRepListBO.IsFlagStatus = True Then
                Me.SetSuccessMsgInGeneralLabel("Amend")
                Me.SearchFaultRepairList(txtFind.Text, ddlSCI.SelectedIndex)
            Else

            End If

        Catch ex As Exception
            faultRepListBO.IsExceptionGenerated = True
            faultRepListBO.ExceptionMsg = ex.Message
        End Try
    End Sub

#End Region

#Region "Get Area Look Up Values "
    ''Area Lookup Values
    Private Sub GetAreaLookUpValues(ByVal popupType As String)

        If popupType = "NewRepair" Then
            PopulateLookup(ddlArea, SprocNameConstants.GetTblAllAreasLookup)
        ElseIf popupType = "AmendRepair" Then
            PopulateLookup(ddlAmendArea, SprocNameConstants.GetTblAllAreasLookup)
        End If

    End Sub
#End Region

#Region "Get Item Look Up Values "
    Private Sub GetItemLookUpValues(ByVal inParam As String, ByVal inParamName As String, ByVal popupType As String)

        If popupType = "NewRepair" Then
            PopulateLookup(ddlItem, SprocNameConstants.GetTblItemGetLookup, inParam, inParamName)
        ElseIf popupType = "AmendRepair" Then
            PopulateLookup(ddlAmendItem, SprocNameConstants.GetTblItemGetLookup, inParam, inParamName)
        End If

    End Sub
#End Region

#Region "Get Parameter Look Up Values "
    Private Sub GetParameterLookUpValues(ByVal inParam As String, ByVal inParamName As String, ByVal popupType As String)

        If popupType = "NewRepair" Then
            PopulateLookup(ddlParameter, SprocNameConstants.GetParameterLookup, inParam, inParamName)
        ElseIf popupType = "AmendRepair" Then
            PopulateLookup(ddlAmendParameter, SprocNameConstants.GetParameterLookup, inParam, inParamName)
        End If

    End Sub
#End Region

#Region "Populate Lookup "
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String)

        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList

        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup)

        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.DataBind()
            ddlLookup.Items.Add(New ListItem("Please select", "0", True))
            ddlLookup.SelectedValue = 0
        End If
    End Sub
#End Region

#Region "Populate Lookup "
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String)

        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList

        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup, inParam, inParamName)

        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.DataBind()
            ddlLookup.Items.Add(New ListItem("Please select", "0", True))
            ddlLookup.SelectedValue = 0
        End If
    End Sub
#End Region

#Region "Validate Amend Repair Fields "
    Private Function ValidateAmendRepairFields() As Boolean
        Dim errorFree As Boolean = True

        If Me.txtAmendDescription.Text = String.Empty Then
            errorFree = False
            Me.lblAmendMsg.Text = "Please Enter the Description of Repair"
        End If

        If Me.rdbAmendStockYes.Checked = True Then

            If Me.ddlAmendArea.SelectedValue = "0" Or Me.ddlAmendArea.SelectedValue = String.Empty Then

                errorFree = False
                Me.lblAmendMsg.Text = "Please Select Area OR Choose 'No' for stock condition item"

            ElseIf Me.ddlAmendItem.SelectedValue = "0" Or Me.ddlAmendItem.SelectedValue = String.Empty Then

                errorFree = False
                Me.lblAmendMsg.Text = "Please Select Item OR Choose 'No' for stock condition item"


            ElseIf Me.ddlAmendParameter.SelectedValue = "0" Or Me.ddlAmendParameter.SelectedValue = String.Empty Then

                errorFree = False
                Me.lblAmendMsg.Text = "Please Select Parameter OR Choose 'No' for stock condition item"
            End If

        End If

        Return errorFree
    End Function
#End Region

#Region "Validate New Repair Fields "
    Private Function ValidateNewRepairFields() As Boolean
        Dim errorFree As Boolean = True

        If Me.txtRepairDescription.Text = String.Empty Then
            errorFree = False
            Me.lblNewRepairMsg.Text = "Please Enter the Description of Repair"
        End If

        If Me.rdbStockYes.Checked = True Then

            If Me.ddlArea.SelectedValue = "0" Or Me.ddlArea.SelectedValue = String.Empty Then

                errorFree = False
                Me.lblNewRepairMsg.Text = "Please Select Area OR Choose 'No' for stock condition item"

            ElseIf Me.ddlItem.SelectedValue = "0" Or Me.ddlItem.SelectedValue = String.Empty Then

                errorFree = False
                Me.lblNewRepairMsg.Text = "Please Select Item OR Choose 'No' for stock condition item"


            ElseIf Me.ddlParameter.SelectedValue = "0" Or Me.ddlParameter.SelectedValue = String.Empty Then

                errorFree = False
                Me.lblNewRepairMsg.Text = "Please Select Parameter OR Choose 'No' for stock condition item"

            End If

        End If

        Return errorFree
    End Function
#End Region

#Region "Clear General Msg Label "
    Private Sub ClearGeneralMsgLabel()
        Me.lblGeneralMsg.Text = ""
        Me.updPnlGeneralMsg.Update()
    End Sub
#End Region

#Region "Set Success Msg In General Label "

    Private Sub SetSuccessMsgInGeneralLabel(ByVal action As String)

        If action = "Add" Then
            Me.lblGeneralMsg.Text = "Repair has been added successfully."
        ElseIf action = "Amend" Then
            Me.lblGeneralMsg.Text = "Repair has been amended successfully."
        End If

        Me.lblGeneralMsg.Style.Item("color") = "green"
        Me.updPnlGeneralMsg.Update()

    End Sub
#End Region


    
End Class