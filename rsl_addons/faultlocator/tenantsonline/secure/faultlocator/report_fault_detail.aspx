<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="report_fault_detail.aspx.vb" Inherits="tenantsonline.ReportFaultDetail" Title="Tenants Online :: Fault Locator" EnableViewState="true" ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<%@ Register Src="../../user control/BreadCrumbControl.ascx" TagName="BreadCrumbControl"
    TagPrefix="uc2" %>      
   

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">
    <script type="text/javascript" language="javascript">
        function fillPopup(){
            
            
            document.getElementById('popupQuantity').innerHTML = document.getElementById('<%=ddlQuantity.ClientID%>').value 
            
            // for 7 + days
            if (document.getElementById('<%=ddlDays.ClientID%>').value == "8")
                {
                document.getElementById('popupDays').innerHTML = '7 + day(s)'     
                }
            else
                {
                document.getElementById('popupDays').innerHTML = document.getElementById('<%=ddlDays.ClientID%>').value + ' day(s)' 
                }    
            if(document.getElementById('<%=ddlRecProb.ClientID%>').value == 0){
                
                document.getElementById('popupRecProb').innerHTML = 'This is not a recurring problem' 
            } 
            else{ 
                
                document.getElementById('popupRecProb').innerHTML = 'This is a recurring problem' 
            } 
            
            if(document.getElementById('<%=ddlComProb.ClientID%>').value == 0){
            
                document.getElementById('popupComProb').innerHTML = 'It is not in a communal area' 
            } 
            else{ 
                
                document.getElementById('popupComProb').innerHTML = 'It is in a communal area' 
            } 
            
            if(document.getElementById('<%=ddlRecharge.ClientID%>').value == 0){
                
                document.getElementById('popupRecharge').innerHTML = 'This is not Rechargable' 
            } 
            else{ 
                  document.getElementById('popupRecharge').innerHTML = 'Recharge Cost is ' + '<%=lblRechargeCost.Text %>'
                } 
            
            document.getElementById('popupNotes').innerHTML = '<p>'+document.getElementById('<%=txtNotes.ClientID%>').value+'</p>' 

        }
        
        
        
    </script>
     
    <table cellpadding="3" class="table_content" width="100%">
        <tr>
            <td align="left">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Text="Report a fault"></asp:Label>               
           </td>
           <td align="right">
            <asp:Button ID="btnTopBack" runat="server" Text=" < Back"/>
           </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <hr /><br />
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" BackColor="Blue" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2" rowspan="1" style="height: 25px" >
                <asp:Label ID="Label1" runat="server" CssClass="cellData" Text="You have selected:"
                    Width="147px"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" style="color: #ffffff; background-color: black; height: 25px;" colspan="2">
                <uc2:BreadCrumbControl ID="BreadCrumbControl" runat="server" /></td>
        </tr>
        <tr>
            <td align="left" colspan="2" style="height: 44px">
                <asp:Label ID="Label2" runat="server" CssClass="cellData" Text="If you have chosen the fault in error you can go back to the previous page by clicking the 'Back' button above or the name of the area you have selected"
                    Width="480px"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="2">
            </td>
        </tr>
        <tr>
            <td style="width: 160px" valign="top" class="body_caption_bold">
                <%=GetFaultQuestion(1).ToString()%></td>
            <td align="left" style="width: 7px">
                <asp:DropDownList ID="ddlQuantity" runat="server" CssClass="select_normal" Width="72px">
                    <asp:ListItem Selected="True" Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                    <asp:ListItem Value="6">6</asp:ListItem>
                    <asp:ListItem Value="7">7</asp:ListItem>
                    <asp:ListItem Value="8">8</asp:ListItem>
                    <asp:ListItem Value="9">9</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="body_caption_bold"><%=GetFaultQuestion(2).ToString()%>
            </td>
            <td>
                <asp:DropDownList ID="ddlDays" runat="server" CssClass="select_normal" Width="72px">
                    <asp:ListItem Selected="True" Value="1">1 day</asp:ListItem>
                    <asp:ListItem Value="2">2 days</asp:ListItem>
                    <asp:ListItem Value="3">3 days</asp:ListItem>
                    <asp:ListItem Value="4">4 days</asp:ListItem>
                    <asp:ListItem Value="5">5 days</asp:ListItem>
                    <asp:ListItem Value="6">6 days</asp:ListItem>
                    <asp:ListItem Value="7">7 days</asp:ListItem>
                    <asp:ListItem Value="8">7 + days</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="body_caption_bold"><%=GetFaultQuestion(3).ToString()%>
            </td>
            <td align="left" style="width: 7px">
                <asp:DropDownList ID="ddlRecProb" runat="server" CssClass="select_normal" Width="72px">
                    <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td class="body_caption_bold"><%=GetFaultQuestion(4).ToString()%>
            </td>
            <td align="left" style="width: 7px">
                <asp:DropDownList ID="ddlComProb" runat="server" CssClass="select_normal" Width="72px">
                    <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        

         <tr>
            <td class="body_caption_bold"><%=GetFaultQuestion(6).ToString()%>
            </td>
            <td align="left" style="width: 7px">
                <asp:DropDownList ID="ddlRecharge" runat="server" CssClass="select_normal" Width="72px" AutoPostBack="true">
                    <asp:ListItem Selected="True" Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        
        <tr>
            <td class="body_caption_bold">
                <asp:Label runat="server" ID="lblRecharge" Visible="False" Text = " Recharge (Net) Cost is �"></asp:Label>
            </td>
           <td align="left" style="width: 7px">
                <asp:Label runat="server" ID="lblRechargeCost" Visible="False" CssClass="select_normal" Width="72px"></asp:Label>
            </td>
            
        </tr>
        
        <tr>
            <td class="body_caption_bold"><%=GetFaultQuestion(5).ToString()%>
            </td>
            <td align="left" style="width: 7px">
            </td>
        </tr>
       
        <tr>
            <td>
            
                <asp:TextBox ID="txtNotes" runat="server" Columns="30" Rows="6" TextMode="MultiLine" CssClass="input_normal"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="right" colspan="2">                &nbsp;</td>
       </tr>                                        
       
    </table> 
    <div id="divPopup">
        &nbsp;<asp:UpdatePanel ID="pnlUpdate" runat="server">
                    
                    <ContentTemplate> <div style="float:right"><asp:Button ID="btnNext" runat="server" Text="Next > "/></div>
                        &nbsp;<cc2:ModalPopupExtender ID="mdlPopup" runat="server"  BackgroundCssClass="modalBackground" TargetControlID="btnNext" CancelControlID="btnBackPopup" PopupControlID="pnlResponsePopup" Drag="false">
                        </cc2:ModalPopupExtender>
                        <asp:Panel  ID="pnlResponsePopup" runat="server" BackColor="White" BorderColor="Transparent" CssClass="caption"> 
                            <table style="WIDTH:350px">
                                <tbody>
                                    <tr>
                                        <td style="BACKGROUND-COLOR: #c00000" colspan="2">
                                          &nbsp;&nbsp;&nbsp;
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <strong>Please Confirm Fault Details</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 182px">
                                            Quantity
                                        </td>
                                        <td>
                                            <div id="popupQuantity"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 190px; height: 21px;">
                                            You have had this problem for
                                        </td>
                                        <td style="height: 21px">
                                            <div id="popupDays" ></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             <div id="popupRecProb" ></div>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                             <div id="popupComProb" ></div>
                                        </td>                                        
                                    </tr>   
                                    <tr>
                                        <td colspan="2">
                                             <div id="popupRecharge" ></div>
                                        </td>                                        
                                    </tr>   
                                    <tr>
                                        <td style="width: 182px" >
                                             Notes
                                        </td>
                                        <td >
                                             <div id="popupNotes"></div><br />
                                        </td>                                        
                                    </tr>                                    
                                     <tr>
                                        <td colspan="2">
                                             &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <asp:Button ID="btnBackPopup" runat="server" Text="< Back" /> <asp:Button ID="btnContinuePopup" runat="server" Text="Continue >" OnClick="btnContinuePopup_Click"  />  
                                        </td>                                                                                                                  
                                    </tr>
                                     <tr>
                                        <td style="BACKGROUND-COLOR: #c00000" colspan="2">
                                            &nbsp;&nbsp;&nbsp;
                                        </td>                                        
                                    </tr>
                                  </tbody> 
                            </table>                                                            
                       </asp:Panel>
                    </ContentTemplate>
                                
                </asp:UpdatePanel>
                </div>                         
</asp:Content>