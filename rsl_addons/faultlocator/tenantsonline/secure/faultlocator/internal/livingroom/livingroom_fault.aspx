<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="livingroom_fault.aspx.vb" Inherits="tenantsonline.livingroom_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
    
<%@ Register Src="../../../../user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 
    
<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">

<script type="text/javascript" language ="javascript">
var imageList = new Array(12);
//Load all images on page load
var livingroomMainImage= new Image();
livingroomMainImage.src="../../../../images/propertyImages/internal/livingroom/fl_livingroom_main.gif";
var ceilingImage= new Image();
ceilingImage.src="../../../../images/propertyImages/internal/livingroom/ceiling_01.gif";
imageList[0]=ceilingImage;
var smokedetectorImage= new Image();
smokedetectorImage.src="../../../../images/propertyImages/internal/livingroom/smokedetector_01.gif";
imageList[1]=smokedetectorImage;
var doorImage= new Image();
doorImage.src="../../../../images/propertyImages/internal/livingroom/door_01.gif";
imageList[2]=doorImage;
var gasfireImage= new Image();
gasfireImage.src="../../../../images/propertyImages/internal/livingroom/gasfire_01.gif";
imageList[3]=gasfireImage;
var electricsImage= new Image();
electricsImage.src="../../../../images/propertyImages/internal/livingroom/electrics_01.gif";
imageList[4]=electricsImage;
var wallImage= new Image();
wallImage.src="../../../../images/propertyImages/internal/livingroom/walls_01.gif";
imageList[5]=wallImage;
var tvImage= new Image();
tvImage.src="../../../../images/propertyImages/internal/livingroom/tv_01.gif";
imageList[6]=tvImage;
var alarmsensorImage= new Image();
alarmsensorImage.src="../../../../images/propertyImages/internal/livingroom/alarmsensor_01.gif";
imageList[7]=alarmsensorImage;
var windowImage= new Image();
windowImage.src="../../../../images/propertyImages/internal/livingroom/window_01.gif";
imageList[8]=windowImage;
var lightingImage= new Image();
lightingImage.src="../../../../images/propertyImages/internal/livingroom/lighting_01.gif";
imageList[9]=lightingImage;
var radiatorImage= new Image();
radiatorImage.src="../../../../images/propertyImages/internal/livingroom/radiator_01.gif";
imageList[10]=radiatorImage;
var floorImage= new Image();
floorImage.src="../../../../images/propertyImages/internal/livingroom/floor_01.gif";
imageList[11]=floorImage;


//function called on click event of listbox
    function changeImageSourceUsingList (sender) {
	    changeImageSource(sender.selectedIndex);	    
	}

//function called onmouseover of image map and change the image source 
//according to input paramater		
	function changeImageSource(index)
	{
		document.getElementById("livingroom").src = imageList[index].src;			
	}
	
	//function called onmouseout of image map and set the main diningroom image 
	function changeToMain()
	{
		document.getElementById("livingroom").src = livingroomMainImage.src;
	}
</script>


<table cellpadding="3" cellspacing="0" border="0">
 <tr><td class="elementimages_btnback">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
 <tr>
    <td style="height: 435px">
        &nbsp;<img src="../../../../images/propertyImages/internal/livingroom/fl_livingroom_main.gif"  width="546" height="431" border="0" usemap="#Map" id="livingroom" alt=""  /><map name="Map" id="livingroomMap" ><area shape="poly" coords="363,27,370,25,379,27,380,63,371,64,363,63" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>"/><area shape="poly" coords="545,4,544,15,536,16,380,27,371,25,361,28,278,30,267,24,255,22,243,23,235,27,232,32,212,34,167,24,1,29,0,0" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" /><area shape="poly" coords="1,38,10,37,75,48,74,429,0,430" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(2).ToString() %>" onmouseover="changeImageSource('2')" onmouseout="changeToMain()" alt="<%= GetElementName(2).ToString() %>" title="<%= GetElementName(2).ToString()  %>" /><area shape="poly" coords="222,281,242,282,242,301,221,301" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>" /><area shape="poly" coords="449,295,464,301,464,363,544,379,542,420,279,423,266,421,266,429,262,429,263,420,264,411,262,399,257,387,251,383,243,373,268,380,270,393,282,392,282,384,326,375,329,384,338,385,337,338,268,316,208,325,209,357,211,363,208,365,189,365,158,351,150,348,130,322,122,321,107,323,96,324,75,314,75,307,176,297,212,304,308,301,308,313,311,316,315,313,316,305,442,304,443,313,447,314,450,311" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(11).ToString() %>" onmouseover="changeImageSource('11')" onmouseout="changeToMain()" alt="<%= GetElementName(11).ToString() %>" title="<%= GetElementName(11).ToString()  %>" /><area shape="poly" coords="76,183,150,180,171,184,161,184,162,298,74,306" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(3).ToString() %>" onmouseover="changeImageSource('3')" onmouseout="changeToMain()" alt="<%= GetElementName(3).ToString() %>" title="<%= GetElementName(3).ToString()  %>" /><area shape="poly" coords="488,139,506,139,505,162,489,159" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(9).ToString() %>" onmouseover="changeImageSource('9')" onmouseout="changeToMain()" alt="<%= GetElementName(9).ToString() %>" title="<%= GetElementName(9).ToString()  %>" /><area shape="poly" coords="477,296,465,299,464,363,545,379,544,323,544,307" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(10).ToString() %>" onmouseover="changeImageSource('10')" onmouseout="changeToMain()" alt="<%= GetElementName(10).ToString() %>" title="<%= GetElementName(10).ToString()  %>"/><area shape="poly" coords="233,32,238,25,254,22,268,23,275,27,277,33,278,39,274,44,260,47,247,46,236,44,232,40" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" /><area shape="poly" coords="326,168,421,167,422,239,387,238,391,245,440,243,448,247,450,313,444,313,442,304,316,304,315,313,309,313,309,249,317,243,362,245,365,238,327,238" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>" /><area shape="poly" coords="544,16,544,307,478,295,479,160,488,159,507,162,507,140,488,139,488,159,478,159,477,40,422,42,422,56,427,58,427,243,392,244,387,238,423,239,422,167,326,169,326,239,365,239,361,245,317,243,308,249,308,300,242,302,242,282,220,281,221,301,239,300,238,305,213,305,177,298,162,300,161,185,171,185,151,179,75,183,77,48,13,38,3,38,3,31,170,26,208,33,232,33,232,41,241,45,250,47,265,48,276,42,278,38,279,34,278,31,362,29,363,61,368,65,374,65,379,62,380,27" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>" /><area shape="poly" coords="478,39,422,41,422,57,427,56,427,243,441,243,448,248,450,296,464,301,464,299,477,295" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(8).ToString() %>" onmouseover="changeImageSource('8')" onmouseout="changeToMain()" alt="<%= GetElementName(8).ToString() %>" title="<%= GetElementName(8).ToString()  %>"/></map></td>
     <td class="listbox_td">
     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox></td>
  </tr>
     </table>
</asp:Content>