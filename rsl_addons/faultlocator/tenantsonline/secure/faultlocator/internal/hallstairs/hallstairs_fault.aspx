<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="hallstairs_fault.aspx.vb" Inherits="tenantsonline.hallstairs_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
    
<%@ Register Src="../../../../user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 
    
<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">
<script type="text/javascript" language ="javascript">
var imageList = new Array(15);
//Load all images on page load
var hallstairMainImage= new Image();
hallstairMainImage.src="../../../../images/propertyImages/internal/hallstairs/fl_hallstairs_main.gif";
var smokedetectorImage= new Image();
smokedetectorImage.src="../../../../images/propertyImages/internal/hallstairs/smokedetector_01.gif";
imageList[0]=smokedetectorImage;
var ceilingImage= new Image();
ceilingImage.src="../../../../images/propertyImages/internal/hallstairs/ceiling_01.gif";
imageList[1]=ceilingImage;
var spyholeImage= new Image();
spyholeImage.src="../../../../images/propertyImages/internal/hallstairs/spyhole_01.gif";
imageList[2]=spyholeImage;
var doorbellImage= new Image();
doorbellImage.src="../../../../images/propertyImages/internal/hallstairs/doorbell_01.gif";
imageList[3]=doorbellImage;
var letterboxImage= new Image();
letterboxImage.src="../../../../images/propertyImages/internal/hallstairs/letterbox_01.gif";
imageList[4]=letterboxImage;
var doorImage= new Image();
doorImage.src="../../../../images/propertyImages/internal/hallstairs/door_01.gif";
imageList[5]=doorImage;
var floorImage= new Image();
floorImage.src="../../../../images/propertyImages/internal/hallstairs/floor_01.gif";
imageList[6]=floorImage;
var electricsImage= new Image();
electricsImage.src="../../../../images/propertyImages/internal/hallstairs/electrics_01.gif";
imageList[7]=electricsImage;
var stairsImage= new Image();
stairsImage.src="../../../../images/propertyImages/internal/hallstairs/stairs_01.gif";
imageList[8]=stairsImage;
var stairliftImage= new Image();
stairliftImage.src="../../../../images/propertyImages/internal/hallstairs/stairlift_01.gif";
imageList[9]=stairliftImage;
var alarmpanelImage= new Image();
alarmpanelImage.src="../../../../images/propertyImages/internal/hallstairs/alarmpanel_01.gif";
imageList[10]=alarmpanelImage;
var lightingImage= new Image();
lightingImage.src="../../../../images/propertyImages/internal/hallstairs/lighting_01.gif";
imageList[11]=lightingImage;
var wallImage= new Image();
wallImage.src="../../../../images/propertyImages/internal/hallstairs/wall_01.gif";
imageList[12]=wallImage;
var alarmsensorImage= new Image();
alarmsensorImage.src="../../../../images/propertyImages/internal/hallstairs/alarmsensor_01.gif";
imageList[13]=alarmsensorImage;
var landingImage= new Image();
landingImage.src="../../../../images/propertyImages/internal/hallstairs/landing_01.gif";
imageList[14]=landingImage;

//function called on click event of listbox
    function changeImageSourceUsingList (sender) {
	    changeImageSource(sender.selectedIndex);	    
	}

//function called onmouseover of image map and change the image source 
//according to input paramater		
	function changeImageSource(index)
	{
		document.getElementById("hallstair").src = imageList[index].src;		
	}
	
	//function called onmouseout of image map and set the main hallstair image 
	function changeToMain()
	{
		document.getElementById("hallstair").src = hallstairMainImage.src;
	}
</script>
 <table cellpadding="3" cellspacing="0" border="0">
 <tr><td class="elementimages_btnback">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
 <tr>
    <td style="height: 435px">
        &nbsp;<img src="../../../../images/propertyImages/internal/hallstairs/fl_hallstairs_main.gif"  width="546" height="431" border="0" usemap="#Map" id="hallstair" alt=""  /><map name="Map" id="hallstairMap" ><area shape="poly" coords="434,221,451,223,451,246,434,244" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(10).ToString() %>" onmouseover="changeImageSource('10')" onmouseout="changeToMain()" alt="<%= GetElementName(10).ToString() %>" title="<%= GetElementName(10).ToString()  %>"  /><area shape="poly" coords="437,10,445,8,455,12,456,47,437,43" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(13).ToString() %>" onmouseover="changeImageSource('13')" onmouseout="changeToMain()" alt="<%= GetElementName(13).ToString() %>" title="<%= GetElementName(13).ToString()  %>" /><area shape="poly" coords="0,0,0,40,114,14,137,13,136,31,147,30,149,20,165,13,180,13,190,18,193,26,191,29,200,29,197,1" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" /><area shape="poly" coords="200,30,192,29,181,36,167,36,153,33,147,30,137,31,137,81,204,82" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" /><area shape="poly" coords="1,40,1,430,33,429,44,424,136,382,136,237,91,239,39,259,37,244,92,223,61,135,58,129,60,124,66,128,62,135,92,224,91,240,136,237,136,196,136,169,135,145,128,141,124,142,119,138,120,128,128,128,129,141,135,142,136,110,136,91,135,61,135,31,135,18,135,14,124,14,114,14" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>" /><area shape="poly" coords="121,128,128,128,130,142,124,143,120,140" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(3).ToString() %>" onmouseover="changeImageSource('3')" onmouseout="changeToMain()" alt="<%= GetElementName(3).ToString() %>" title="<%= GetElementName(3).ToString()  %>"/><area shape="poly" coords="211,372,276,372,276,399,221,400,216,398,216,377" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>"/><area shape="poly" coords="34,429,42,425,137,381,137,321,154,316,154,298,195,298,195,323,213,343,214,368,211,372,216,379,215,394,222,401,368,396,373,400,388,401,391,406,396,404,399,401,413,401,414,404,423,397,441,411,457,414,487,429" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>" /><area shape="poly" coords="218,1,225,37,225,29,302,28,294,4" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(14).ToString() %>" onmouseover="changeImageSource('14')" onmouseout="changeToMain()" alt="<%= GetElementName(14).ToString() %>" title="<%= GetElementName(14).ToString()  %>"/><area shape="poly" coords="37,245,92,224,93,239,39,259" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>"/><area shape="poly" coords="424,181,442,181,442,202,424,201" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(11).ToString() %>" onmouseover="changeImageSource('11')" onmouseout="changeToMain()" alt="<%= GetElementName(11).ToString() %>" title="<%= GetElementName(11).ToString()  %>"/><area shape="poly" coords="147,23,151,17,164,14,181,13,191,21,192,29,184,34,173,36,158,35,151,33,147,30" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>"/><area shape="poly" coords="62,123,66,130,63,136,59,132,59,127" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(2).ToString() %>" onmouseover="changeImageSource('2')" onmouseout="changeToMain()" alt="<%= GetElementName(2).ToString() %>" title="<%= GetElementName(2).ToString()  %>"/><area shape="poly" coords="295,4,359,251,370,285,350,273,339,272,350,282,371,295,375,312,367,314,365,318,366,325,374,333,375,353,375,364,379,380,369,389,366,385,334,368,328,368,327,374,362,393,368,393,370,399,374,400,389,400,389,405,396,405,399,400,413,402,412,405,419,401,423,395,422,352,415,337,391,336,392,330,420,330,420,325,423,321,422,267,423,260,418,257,418,245,421,235,422,232,419,230,409,233,403,238,391,246,382,214,324,4,309,8,375,252,365,247,301,4" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(9).ToString() %>" onmouseover="changeImageSource('9')" onmouseout="changeToMain()" alt="<%= GetElementName(9).ToString() %>" title="<%= GetElementName(9).ToString()  %>"/><area shape="poly" coords="303,28,360,250,358,254,362,256,369,286,349,272,340,272,340,277,348,280,371,295,375,313,369,313,365,316,367,319,366,327,374,333,374,352,374,359,378,381,368,392,366,385,336,368,328,367,326,372,364,392,369,393,276,397,275,372,216,374,210,372,213,369,212,342,199,323,209,324,207,174,207,144,207,120,207,111,206,101,204,88,204,80,203,71,201,53,201,39,200,28,198,10,198,5,207,3,212,3,218,3,220,10,222,20,222,27,223,28" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(8).ToString() %>" onmouseover="changeImageSource('8')" onmouseout="changeToMain()" alt="<%= GetElementName(8).ToString() %>" title="<%= GetElementName(8).ToString()  %>"/><area shape="poly" coords="445,6,455,14,455,47,438,44,436,9,454,4,325,4,392,248,401,236,415,231,421,231,419,241,419,258,422,260,423,308,423,323,421,326,421,331,391,331,392,336,417,336,422,347,423,394,441,412,459,414,535,428,543,417,544,261,448,246,434,243,434,222,425,201,424,182,441,182,441,203,426,202,434,222,450,222,450,247,544,259,544,183,545,118,545,42,544,6" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(12).ToString() %>" onmouseover="changeImageSource('12')" onmouseout="changeToMain()" alt="<%= GetElementName(12).ToString() %>" title="<%= GetElementName(12).ToString()  %>"/></map></td>
     <td class="listbox_td">
     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox></td>
  </tr>
     </table>
</asp:Content>