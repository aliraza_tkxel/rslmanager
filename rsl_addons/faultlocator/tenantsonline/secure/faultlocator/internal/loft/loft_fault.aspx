<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="loft_fault.aspx.vb" Inherits="tenantsonline.loft_fault" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
    
<%@ Register Src="../../../../user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 
    
<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">

<script type="text/javascript" language ="javascript">
var imageList = new Array(8);
//Load all images on page load
var loftMainImage= new Image();
loftMainImage.src="../../../../images/propertyImages/internal/loft/fl_loft_main.gif";
var ceilingImage= new Image();
ceilingImage.src="../../../../images/propertyImages/internal/loft/ceiling_01.gif";
imageList[0]=ceilingImage;
var watertankImage= new Image();
watertankImage.src="../../../../images/propertyImages/internal/loft/watertank_01.gif";
imageList[1]=watertankImage;
var windowImage= new Image();
windowImage.src="../../../../images/propertyImages/internal/loft/window_01.gif";
imageList[2]=windowImage;
var electricsImage= new Image();
electricsImage.src="../../../../images/propertyImages/internal/loft/electrics_01.gif";
imageList[3]=electricsImage;
var floorImage= new Image();
floorImage.src="../../../../images/propertyImages/internal/loft/floor_01.gif";
imageList[4]=floorImage;
var lightingImage= new Image();
lightingImage.src="../../../../images/propertyImages/internal/loft/lighting_01.gif";
imageList[5]=lightingImage;
var insulationImage= new Image();
insulationImage.src="../../../../images/propertyImages/internal/loft/insulation_01.gif";
imageList[6]=insulationImage;
var wallImage= new Image();
wallImage.src="../../../../images/propertyImages/internal/loft/wall_01.gif";
imageList[7]=wallImage;


//function called on click event of listbox
    function changeImageSourceUsingList (sender) {
	    changeImageSource(sender.selectedIndex);	    
	}


//function called onmouseover of image map and change the image source 
//according to input paramater		
	function changeImageSource(index)
	{
		document.getElementById("loft").src = imageList[index].src;					
	}
	
	//function called onmouseout of image map and set the main diningroom image 
	function changeToMain()
	{
		document.getElementById("loft").src = loftMainImage.src;
	}
</script>

<table cellpadding="3" cellspacing="0" border="0">
 <tr><td class="elementimages_btnback">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td></tr>
 <tr>
    <td style="height: 435px">
        &nbsp;<img src="../../../../images/propertyImages/internal/loft/fl_loft_main.gif"  width="546" height="431" border="0" usemap="#Map" id="loft" alt=""  /><map name="Map" id="loftMap" ><area shape="poly" coords="544,166,544,4,355,5,278,4,278,15,282,27,281,29,283,36,281,43,274,43,270,37,272,29,272,28,270,28,272,21,275,15,275,9,273,4,0,0,1,176,74,154,74,136,84,133,102,132,124,133,137,134,173,125,241,62,294,61,362,126" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" /><area shape="poly" coords="474,206,491,209,491,229,473,223" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(3).ToString() %>" onmouseover="changeImageSource('3')" onmouseout="changeToMain()" alt="<%= GetElementName(3).ToString() %>" title="<%= GetElementName(3).ToString()  %>"/><area shape="poly" coords="362,188,543,326,538,325,482,323,464,319,450,319,439,324,348,323,328,298,296,298,292,264,287,264,288,291,265,292,266,265,262,265,259,299,232,298,204,331,10,330,65,282,65,293,65,298,69,297,75,301,73,307,86,314,106,317,128,315,140,311,146,304,143,301,146,295,148,289,154,288,154,294,159,294,159,290,160,287,152,285,146,283,145,277,146,268,145,259,147,253,147,244,146,235,147,226,142,221,151,210,154,205,163,198,176,229,219,219,231,239,276,226,280,188" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>"/><area shape="poly" coords="11,348,1,356,1,399,260,399,265,374,179,375,178,361,189,348" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>" /><area shape="poly" coords="255,417,1,417,0,430,251,430" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>"/><area shape="poly" coords="544,418,535,427,530,429,304,430,301,418" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>"/><area shape="poly" coords="298,397,294,374,378,374,379,361,362,341,422,340,431,328,446,321,466,319,486,325,494,330,512,343,517,351,522,364,521,373,517,383,512,389,503,396,501,399" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>"  onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>"/><area shape="poly" coords="278,4,278,15,282,26,278,27,283,32,282,42,276,43,271,40,271,32,274,28,270,28,275,15,275,6" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>"/><area shape="poly" coords="301,307,321,307,321,324,301,325" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>"/><area shape="poly" coords="0,175,74,154,74,189,73,199,79,204,68,214,65,225,65,281,1,337" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>" /><area shape="poly" coords="85,207,90,212,72,225,74,216" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>"  onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>"/><area shape="poly" coords="147,200,142,210,132,221,130,213,131,207,140,204" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>"/><area shape="poly" coords="148,165,148,187,146,201,144,181,145,167" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>"/><area shape="poly" coords="544,168,544,324,482,278,490,229,491,208,473,205,473,224,489,232,480,278,362,189,281,188,281,178,254,171,232,172,233,163,203,153,173,155,159,157,162,197,154,203,155,183,153,169,153,155,152,144,144,142,145,138,142,136,173,126,241,61,231,95,230,142,307,143,307,93,231,95,242,63,297,62,361,127" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>"/><area shape="poly" coords="77,135,107,131,134,134,144,135,144,141,154,144,155,185,153,202,146,214,140,221,132,219,146,203,149,187,147,170,145,150,142,197,138,204,131,207,130,217,145,224,144,234,147,250,143,261,145,285,148,293,144,301,146,306,135,313,117,317,104,317,91,314,80,311,74,307,75,302,71,297,66,297,64,228,66,220,81,203,86,207,71,225,72,287,74,293,75,229,81,220,90,211,87,206,80,201,74,197,74,192,76,141" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>"/><area shape="poly" coords="231,93,306,93,309,143,230,142" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(2).ToString() %>" onmouseover="changeImageSource('2')" onmouseout="changeToMain()" alt="<%= GetElementName(2).ToString() %>" title="<%= GetElementName(2).ToString()  %>"/></map></td>
     <td class="listbox_td">
     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox></td>
  </tr>
     </table>
</asp:Content>