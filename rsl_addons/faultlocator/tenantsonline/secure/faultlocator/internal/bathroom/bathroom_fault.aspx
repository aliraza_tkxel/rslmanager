<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="bathroom_fault.aspx.vb" Inherits="tenantsonline.bathroom" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
    
<%@ Register Src="../../../../user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 

<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">

<script type="text/javascript" language ="javascript">
var imageList = new Array(15);
//Load all images on page load
var bathroomMainImage= new Image();
bathroomMainImage.src="../../../../images/propertyImages/internal/bathroom/fl_bathroom_main.gif";
var basinImage= new Image();
basinImage.src="../../../../images/propertyImages/internal/bathroom/basin_01.gif";
imageList[0]=basinImage;
var bathImage= new Image();
bathImage.src="../../../../images/propertyImages/internal/bathroom/bath_01.gif";
imageList[1]=bathImage;
var extractorfanImage= new Image();
extractorfanImage.src="../../../../images/propertyImages/internal/bathroom/extractorfan_01.gif";
imageList[2]=extractorfanImage;
var grabrailsImage= new Image();
grabrailsImage.src="../../../../images/propertyImages/internal/bathroom/grabrails_01.gif";
imageList[3]=grabrailsImage;
var showerImage= new Image();
showerImage.src="../../../../images/propertyImages/internal/bathroom/shower_01.gif";
imageList[4]=showerImage;
var toiletImage= new Image();
toiletImage.src="../../../../images/propertyImages/internal/bathroom/toilet_01.gif";
imageList[5]=toiletImage;
var doorImage= new Image();
doorImage.src="../../../../images/propertyImages/internal/bathroom/door_01.gif";
imageList[6]=doorImage;
var windowsImage= new Image();
windowsImage.src="../../../../images/propertyImages/internal/bathroom/windows_01.gif";
imageList[7]=windowsImage;
var lightingImage= new Image();
lightingImage.src="../../../../images/propertyImages/internal/bathroom/lighting_01.gif";
imageList[8]=lightingImage;
var electricsImage= new Image();
electricsImage.src="../../../../images/propertyImages/internal/bathroom/electrics_01.gif";
imageList[9]=electricsImage;
var ceilingImage= new Image();
ceilingImage.src="../../../../images/propertyImages/internal/bathroom/ceiling_01.gif";
imageList[10]=ceilingImage;
var airingcupboardImage= new Image();
airingcupboardImage.src="../../../../images/propertyImages/internal/bathroom/airingcupboard_01.gif";
imageList[11]=airingcupboardImage;
var floorImage= new Image();
floorImage.src="../../../../images/propertyImages/internal/bathroom/floor_01.gif";
imageList[12]=floorImage;
var cisternImage= new Image();
cisternImage.src="../../../../images/propertyImages/internal/bathroom/cistern_01.gif";
imageList[13]=cisternImage;
var wallImage= new Image();
wallImage.src="../../../../images/propertyImages/internal/bathroom/wall_01.gif";
imageList[14]=wallImage;


//function called on click event of listbox
    function changeImageSourceUsingList (sender) {
	    changeImageSource(sender.selectedIndex);	    
	}

//function called onmouseover of image map and change the image source 
//according to input paramater	
	function changeImageSource(index)
	{
		document.getElementById("bathroom").src = imageList[index].src;								
	}
	//function called onmouseout of image map and set the main bathroom image 
	function changeToMain()
	{
		document.getElementById("bathroom").src = bathroomMainImage.src;
	}
</script>

<table cellpadding="3" cellspacing="0" border="0">
 <tr><td class="elementimages_btnback">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td>
     <td align="right" style="width: 7px">
     </td>
 </tr>
 <tr>
    <td style="height: 435px">
        &nbsp;<img src="../../../../images/propertyImages/internal/bathroom/fl_bathroom_main.gif"  width="546" height="431" border="0" usemap="#Map" id="bathroom"  alt="" /><map name="Map" id="bathroomMap" ><area shape="poly" coords="544,40,482,42,483,288,544,302" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(11).ToString() %>" onmouseover="changeImageSource('11')" onmouseout="changeToMain()" alt="<%= GetElementName(11).ToString() %>" title="<%= GetElementName(11).ToString()  %>"   /><area shape="poly" coords="267,174,271,177,275,178,276,173,278,179,332,186,332,203,331,211,325,219,316,224,303,227,293,229,291,323,281,326,271,324,267,323,264,229,252,222,242,215,237,205,249,198,263,196,269,196,271,194,270,187,269,182" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" onmouseover="changeImageSource('0')" onmouseout="changeToMain()" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" /><area shape="poly" coords="45,219,52,216,58,221,52,227,91,226,159,292,157,391,1,421,1,319,1,228,41,228,44,224,36,222,39,218,47,217" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" onmouseover="changeImageSource('1')" onmouseout="changeToMain()" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" /><area shape="poly" coords="1,0,542,4,544,21,209,31,0,26" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(10).ToString() %>" onmouseover="changeImageSource('10')" onmouseout="changeToMain()" alt="<%= GetElementName(10).ToString() %>" title="<%= GetElementName(10).ToString()  %>" /><area shape="poly" coords="135,193,174,192,181,199,178,229,176,235,165,236,156,237,149,240,143,242,135,238,132,231,131,200,133,195" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(13).ToString() %>" onmouseover="changeImageSource('13')" onmouseout="changeToMain()" alt="<%= GetElementName(13).ToString() %>" title="<%= GetElementName(13).ToString()  %>" /><area shape="poly" coords="372,45,449,44,448,380,371,349" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(6).ToString() %>" onmouseover="changeImageSource('6')" onmouseout="changeToMain()" alt="<%= GetElementName(6).ToString() %>" title="<%= GetElementName(6).ToString()  %>" /><area shape="poly" coords="336,308,355,315,354,337,336,329" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(9).ToString() %>" onmouseover="changeImageSource('9')" onmouseout="changeToMain()" alt="<%= GetElementName(9).ToString() %>" title="<%= GetElementName(9).ToString()  %>"/><area shape="poly" coords="16,39,37,40,37,62,16,62,12,61,12,40,15,39" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(2).ToString() %>" onmouseover="changeImageSource('2')" onmouseout="changeToMain()" alt="<%= GetElementName(2).ToString() %>" title="<%= GetElementName(2).ToString()  %>" /><area shape="poly" coords="534,421,448,384,446,380,372,349,368,349,293,315,292,324,285,326,279,327,271,325,266,322,266,305,208,280,183,282,184,299,184,304,176,307,166,307,159,304,158,391,1,422,2,428" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(12).ToString() %>" onmouseover="changeImageSource('12')" onmouseout="changeToMain()" alt="<%= GetElementName(12).ToString() %>" title="<%= GetElementName(12).ToString()  %>" /><area shape="poly" coords="106,223,112,223,112,227,153,256,157,262,155,269,152,275,146,274,109,241,111,240,148,272,150,270,152,266,151,263,152,261,113,233,113,243,106,242,106,228" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(3).ToString() %>" onmouseover="changeImageSource('3')" onmouseout="changeToMain()" alt="<%= GetElementName(3).ToString() %>" title="<%= GetElementName(3).ToString()  %>" /><area shape="poly" coords="338,151,355,152,358,152,358,174,338,173,337,151" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(8).ToString() %>" onmouseover="changeImageSource('8')" onmouseout="changeToMain()" alt="<%= GetElementName(8).ToString() %>" title="<%= GetElementName(8).ToString()  %>" /><area shape="poly" coords="47,70,53,76,52,80,47,82,46,90,44,98,44,160,55,160,55,165,47,169,43,169,39,182,38,188,40,197,46,203,51,204,56,204,62,200,67,196,69,189,67,184,64,182,67,178,72,177,73,183,72,189,70,195,67,200,63,205,57,208,50,207,43,205,39,201,35,196,34,190,36,179,38,176,39,171,33,168,26,167,24,165,25,162,39,160,39,94,37,89,38,86,37,82,39,77,40,75,42,73" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>" /><area shape="poly" coords="71,146,78,152,77,159,74,164,72,167,69,164,65,161,62,157,62,150,65,147,68,146,70,146" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(4).ToString() %>" onmouseover="changeImageSource('4')" onmouseout="changeToMain()" alt="<%= GetElementName(4).ToString() %>" title="<%= GetElementName(4).ToString()  %>"  /><area shape="poly" coords="175,235,191,241,198,248,198,253,197,261,192,266,186,271,183,273,184,299,186,302,180,306,173,307,166,307,160,304,159,292,150,283,150,275,155,272,157,269,157,263,156,259,151,253,146,251,147,241,158,237,171,235" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>"  onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>"/><area shape="poly" coords="147,258,152,264,150,270,148,264,146,260" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(5).ToString() %>" onmouseover="changeImageSource('5')" onmouseout="changeToMain()" alt="<%= GetElementName(5).ToString() %>" title="<%= GetElementName(5).ToString()  %>" /><area shape="poly" coords="544,21,544,39,482,43,483,288,544,303,544,360,467,334,466,36,362,37,363,151,356,153,337,152,337,173,357,174,356,153,363,153,362,174,362,299,293,274,293,230,316,224,321,224,330,211,332,203,331,185,278,178,277,174,273,179,267,174,268,179,270,187,270,194,256,197,242,199,237,205,242,215,249,221,262,227,265,228,265,264,208,247,198,247,189,239,175,235,178,228,180,213,180,202,180,197,176,192,135,193,130,199,132,227,134,237,141,241,147,242,145,249,140,247,131,240,112,227,112,223,106,223,105,239,93,228,98,227,100,29,211,31,212,38,203,38,110,36,109,169,202,166,209,165,208,202,203,203,186,203,185,215,188,218,193,222,203,222,208,221,208,218,204,218,204,212,204,209,205,207,206,214,209,214,209,210,208,207,209,203,209,166,200,165,202,40,211,40,211,31,330,26,328,39,262,43,262,149,327,154,328,40,329,25,543,20" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(14).ToString() %>" onmouseover="changeImageSource('14')" onmouseout="changeToMain()" alt="<%= GetElementName(14).ToString() %>" title="<%= GetElementName(14).ToString()  %>" /><area shape="poly" coords="110,36,191,38,202,38,201,165,108,169,109,36" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(7).ToString() %>" onmouseover="changeImageSource('7')" onmouseout="changeToMain()" alt="<%= GetElementName(7).ToString() %>" title="<%= GetElementName(7).ToString()  %>"/></map></td>
     <td class="listbox_td">
     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox></td>
  </tr>
     </table>
</asp:Content>