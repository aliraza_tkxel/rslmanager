
Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class ceiling_fault
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

#Region "Event "

#Region "Page Init"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

        'If there's not customer info, he/she will be redirected to ~/error.aspx
        If IsNothing(Session("custHeadBO")) Then
            'Response.Redirect("~/error.aspx")
        End If

    End Sub
#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#End Region
End Class