Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters


Partial Public Class kitchen_fault
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

    Dim elementList As FaultElementList
#Region "Event "

#Region "Page Init"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

        'If there's not customer info, he/she will be redirected to ~/error.aspx
        If IsNothing(Session("custHeadBO")) Then
            'Response.Redirect("~/error.aspx")
        End If

    End Sub
#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetElements()
            Me.lstElementName.Attributes.Add("onclick", "changeImageSourceUsingList (this);")
            Me.LoadPathInSession()
        End If
    End Sub

#End Region

#End Region

#Region "Functions & Methods "

#Region "GetElements"
    Public Sub GetElements()

        Try
            'Create the Element BO
            Dim elementBO As New ElementBO()

            'Get the area ID from Query String
            If Not Request.QueryString("areaId") Is Nothing Or Request.QueryString("areaId") > 0 Then

                elementBO.AreaID = Request.QueryString("areaId")

                Dim faultBL As FaultManager = New FaultManager()

                Dim isExceptionGen As Boolean = elementBO.IsExceptionGenerated

                elementList = New FaultElementList()

                faultBL.GetElements(elementBO, elementList)
                Me.lstElementName.DataSource = elementList
                Me.lstElementName.DataTextField = "ElementName"
                Me.lstElementName.DataValueField = "ElementId"
                Me.lstElementName.DataBind()

                'Dim totalElements As String = GetElementName(0)
                If isExceptionGen Then
                    'Session.Add(FaultConstants.ExceptionMessage, elementBO.ExceptionMsg + "Exception in Get Elements Method")
                    Response.Redirect("error.aspx")
                End If
            Else
                'Session.Add(FaultConstants.ExceptionMessage, "Area ID not specified")
                Response.Redirect("~/error.aspx")
            End If
        Catch ex As Exception
            'Session.Add(FaultConstants.ExceptionMessage, ex.Message + "Get Elements")
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "GetElementName"
    Function GetElementName(ByVal index As Integer) As String
        Try
            Dim countItems As Integer = elementList.Count()

            If index > countItems Then
                'Session.Add(FaultConstants.ExceptionMessage, "Index out of bound")
                Response.Redirect("~/error.aspx")
            End If
            Return elementList.Item(index).ElementName
        Catch ex As Exception
            'Session.Add(FaultConstants.ExceptionMessage, ex.Message + "Get Element Name")
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try

    End Function
#End Region

#Region "GetElementId"
    Function GetElementId(ByVal index As Integer) As Integer

        Try
            Dim countItems As Integer = elementList.Count()

            If index > countItems Then
                'Session.Add(FaultConstants.ExceptionMessage, "Index out of bound")
                Response.Redirect("~/error.aspx")
            End If
            Return elementList.Item(index).ElementID
        Catch ex As Exception
            'Session.Add(FaultConstants.ExceptionMessage, ex.Message + "Get Element ID")
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try


    End Function
#End Region

#Region "LoadPathInSession"
    Private Sub LoadPathInSession()
        Try
            Session(FaultConstants.BreadCrumbLoactionName) = FaultConstants.Internal
            Session(FaultConstants.BreadCrumbLoactionUrl) = FaultConstants.LoactionUrlString
            Session(FaultConstants.BreadCrumbAreaName) = "Kitchen"
            Session(FaultConstants.BreadCrumbAreaUrl) = "~/secure/faultlocator/internal/kitchen/kitchen_fault.aspx"
        Catch ex As Exception
            'Session.Add(FaultConstants.ExceptionMessage, ex.Message + "bread crum issue")
            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#End Region
End Class