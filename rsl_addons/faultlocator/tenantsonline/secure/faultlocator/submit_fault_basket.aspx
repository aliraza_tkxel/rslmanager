<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="submit_fault_basket.aspx.vb" Inherits="tenantsonline.SubmitFaultBasket" ValidateRequest="false" EnableEventValidation="false" %> 

<script runat="server" >
    Function MakeCommandString(ByVal Recharge As String, ByVal NetCost As String) As String
        Return Recharge & "," & NetCost
    End Function
 </script>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <table style="width: 100%">
        <tr>
            <td colspan="3">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 40px">
                <asp:Label ID="lblRequestConfirmation" runat="server" CssClass="header_black" Font-Bold="True" Font-Names="Arial" Text="Request Summary and Confirmation"></asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblCheckDetails" runat="server" Font-Names="Arial" Text="Please check the details are correct and update the contact details before confirming the request."></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right">
                </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 40px">
                <asp:UpdatePanel ID="updatePanel_lblSaveSuccessfully" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="lblMessage" runat="server" CssClass="info_message_label"></asp:Label>
                        
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Label ID="lblFaultBasket" runat="server" Font-Bold="True" Font-Names="Arial"
                    Text="Faults(s):"></asp:Label></td>
        </tr>
    </table>
    <table class="table_box">
        <tr>
            <td colspan="3">
                <asp:GridView ID="grdTempFaultBasket" runat="server" CellPadding="4" AutoGenerateColumns="False"
                    EmptyDataText="No record found" ForeColor="#333333" GridLines="None" Font-Names="Arial" Font-Size="Small" PageSize="1">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left"  />
                    <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White"  />
                    <Columns>
                    <asp:TemplateField InsertVisible="False" HeaderText="Area/Item/Element" SortExpression="CreationDate" >
                        <ItemTemplate>
                         <asp:Label ID="lblLocationName" runat="server" CssClass="cellData" Text='<%# Bind("LocationName") %>' />_<asp:Label ID="lblAreaName" runat="server" CssClass="cellData" Text='<%# Bind("AreaName") %>' />_<asp:Label ID="lblElementName" runat="server" CssClass="cellData" Text='<%# Bind("ElementName") %>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                     <asp:TemplateField InsertVisible="False" HeaderText="Description" >
                        <ItemTemplate>
                         <asp:Label ID="lblDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>' />
                        </ItemTemplate>
                         <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField InsertVisible="False" HeaderText="Qty">
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server" CssClass="cellData" Text='<%# Bind("Quantity") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField InsertVisible="False" HeaderText="Recharge">
                        <ItemTemplate>
                            <asp:Label ID="lblRecharge" runat="server" CssClass="cellData" Text='<%# Bind("Recharge") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                     <asp:TemplateField InsertVisible="False" HeaderText="Priority" >
                        <ItemTemplate>
                         <asp:Label ID="lblExpectedTime" runat="server" CssClass="cellData" Text="<%# Bind('FResponseTime') %>" />
                        </ItemTemplate>
                         <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnPreInspection" runat="server" Text="Pre Inspection" OnClick="btnPreInspection_Click" CommandArgument ='<%# Bind("TempFaultID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contractor">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlContractor" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TempFaultId" InsertVisible="False" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblTempFaultId" runat="server" Text='<%# Eval("TempFaultID").ToString() %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    
                    </Columns>
                </asp:GridView>                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3"><hr /></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 21px"><asp:Label ID="lblMyDetails" runat="server" CssClass="header_black" Font-Bold="True" Font-Names="Arial" Text="Details:"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 122px">
                <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid; width: 688px;">
                    <tr>
                         <td style="float: right; width: 60px;" class="caption">
                            <asp:Label ID="lblTenantNameLabel" runat="server" EnableViewState="False" Text="Name:"></asp:Label></td>
                        <td style="width: 150px;">
            <asp:Label ID="lblTenantName" runat="server" Text="" CssClass="caption" ></asp:Label></td>
                        <td style="float: right; width: 90px;" class="caption">
                            <asp:Label ID="lblTelephoneLabel" runat="server" EnableViewState="False" Text="Telephone:"></asp:Label></td>
                        <td colspan="2" style="width: 322px">
            <asp:Label ID="lblTelephone" runat="server" Text="" CssClass="caption"></asp:Label></td>
                    </tr>
                    <tr>
                    <td style="float: right; width: 60px;" class="caption">
                            <asp:Label ID="lblAddressLabel" runat="server" EnableViewState="False" Text="Address:"></asp:Label></td>
                        <td style="width: 150px; height: 18px;">
            <asp:Label ID="lblAddress" runat="server" CssClass="caption" Width="162px" ></asp:Label></td>
                        <td style="float: right; width: 92px; height: 18px;" class="caption">
                            <asp:Label ID="lblMobileLabel" runat="server" EnableViewState="False" Text="Mobile:"></asp:Label></td>
                        <td colspan="2" style="height: 18px; width: 322px;">
                            <asp:Label ID="lblMobile" runat="server" CssClass="caption" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 60px"></td>
                        <td style="width: 150px">
            <asp:Label ID="lblTown" runat="server" Text="" CssClass="caption" ></asp:Label></td>
                        <td style="float: right; width: 90px;" class="caption">
                            <asp:Label ID="lblEmailLabel" runat="server" Text="Email:"></asp:Label></td>
                        <td colspan="2" style="width: 322px">
             <asp:Label ID="lblEmail" runat="server" Text="" CssClass="caption"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 60px"></td>
                        <td style="width: 150px">
            <asp:Label ID="lblCountry" runat="server" Text="" CssClass="caption" ></asp:Label></td>
                        <td style="float: right; width: 90px;" class="caption">
                        </td>
                        <td colspan="2" style="align: right; width: 322px;">
               <asp:Button ID="btnUpdateMyDetail" runat="server" CausesValidation="False" EnableViewState="False"
                   Text="Update Customer Details" PostBackUrl="~/secure/faultlocator/update_customer_detail.aspx" Width="175px" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblPreferredContactTime" runat="server" CssClass="caption" Text="If the customer has any preferred contact time, or ways in which we could contact them, please enter them in the box below:"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 72px">
                <asp:TextBox ID="txtPreferredContactTime" runat="server" Columns="90" Rows="4" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 46px; float: right;">
                <asp:CheckBox ID="chkIamHappy" runat="server" CssClass="caption" Text="Customer would like to be emailed about the level of satisfaction with their reported faults." />&nbsp;&nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <%--<asp:Button ID="btnSubmit" runat="server" Text="Submit" />--%>
                </td>
        </tr>
        <tr>
            <td colspan="1" style="float: right">
                    <asp:UpdatePanel ID="UpdatePanelSubmit" runat="server"  UpdateMode="Conditional">
                     <ContentTemplate>
                         <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                     </ContentTemplate>
                    </asp:UpdatePanel>
            </td>
            <td colspan="1" style="float: right">
                <asp:Button ID="btnBack" runat="server" Text="Return To Fault Locator" /></td>
        </tr>
    </table>
    &nbsp;
    &nbsp;&nbsp;<asp:UpdatePanel ID="pnlPreInspectionPopup" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="1" cellspacing="1" style="width: 415px; background-color: white">
                <tbody align="left">
                    <tr>
                        <td align="left" colspan="11" style="height: 21px; background-color: #c00000">
                            &nbsp;
                            <asp:Label ID="lblPreInspectionHeading" runat="server" BackColor="#C00000" Font-Bold="True"
                                Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Pre Inspection"
                                Width="66%"></asp:Label>
                            <asp:LinkButton ID="lnkBtnPseudo" runat="server"></asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                            </td>
                        <td align="left" colspan="2">
                            &nbsp;<asp:UpdatePanel ID="updatePanel_ErrorMessage" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                            <asp:Label ID="lblValidate" runat="server" Font-Bold="True" ForeColor="Red" CssClass="error_message_label"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="Label1" runat="server" CssClass="caption" Text="Team: *"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel_team" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged" Width="229px">
                                    </asp:DropDownList>
                                    <asp:UpdateProgress ID="UpdateProgress_popupteam" runat="server" DisplayAfter="10">
                                        <ProgressTemplate>
                                            <asp:Panel ID="poppnlProgressTeam" runat="server" BackColor="Silver" Font-Names="Arial"
                                                Font-Size="Small" Height="40px" HorizontalAlign="Center" Width="221px">
                                                <asp:Image ID="imgPopProgressBarTeam" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" /><br />
                                                Loading ...
                                            </asp:Panel>
                                            &nbsp; &nbsp;
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblOperative" runat="server" Text="Surveyor: *"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel_operative" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlOperative" runat="server" AppendDataBoundItems="True"
                                        Width="229px" AutoPostBack="True" OnSelectedIndexChanged="ddlOperative_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlTeam" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblInspectionDate" runat="server" Text="Inspection Date: *" Width="120px"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtInspect" runat="server" Enabled="False" Width="203px"></asp:TextBox>
                            <asp:ImageButton ID="btnInspectionCalander" runat="server" CausesValidation="False"
                                ImageUrl="~/images/buttons/Calendar_button.png" />&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblNotes" runat="server" CssClass="caption" Text="Notes:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtNotes" runat="server" Height="122px" TextMode="MultiLine" Width="223px"></asp:TextBox><cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom,Numbers,UppercaseLetters,LowercaseLetters"
                                TargetControlID="txtNotes" ValidChars=" /.-">
                            </cc2:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px; height: 22px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px; height: 22px">
                            <asp:Label ID="lblRecharge" runat="server" CssClass="caption" Text="Recharge"></asp:Label></td>
                        <td align="center" colspan="1" style="width: 152px; height: 22px">
                            <asp:RadioButton ID="rdbRechargeYes" runat="server" CssClass="caption" Enabled="False"
                                GroupName="rdbRecharge" Text="Y" />&nbsp;</td>
                        <td align="left" colspan="1" style="width: 124px; height: 22px">
                            <asp:RadioButton ID="rdbRechargeNo" runat="server" CssClass="caption" Enabled="False"
                                GroupName="rdbRecharge" Text="N" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblCost" runat="server" CssClass="caption" Text="Cost(Net)�:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtCost" runat="server" Enabled="False" Width="223px">0.0</asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblDueDate" runat="server" CssClass="caption" Text="Due Date *:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtDueDate" runat="server" Enabled="False" Width="224px" ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                        </td>
                        <td align="left" colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 61px">
                            <asp:Button ID="btnCancel" runat="server" CssClass="caption" Text="Cancel" /></td>
                                    <td style="width: 100px">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                            <asp:Button ID="btnSaveChanges" runat="server" CssClass="caption" OnClick="btnSaveChanges_Click"
                                Text="Save Changes" Width="160px" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px">
                        </td>
                        <td align="left" colspan="8" style="width: 152px">
                            </td>
                        <td align="center" colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="1" style="width: 11px; height: 21px">
                        </td>
                        <td align="left" colspan="10" style="height: 21px" valign="bottom">
                            <asp:Label ID="Label2" runat="server" BackColor="Transparent" ForeColor="Red" Text="(*) Fields with asterik are necessary to fill for proper assignment of contractor. "></asp:Label><br />
                            </td>
                    </tr>
                </tbody>
            </table>
            <cc2:ModalPopupExtender ID="mdlPreInspectPopup" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="btnCancel" Drag="True" PopupControlID="pnlPreInspectionPopup"
                TargetControlID="lnkBtnPseudo"
                >
            </cc2:ModalPopupExtender>
            <cc2:CalendarExtender ID="CalendarExtender_Inspection" runat="server" Format="dd/MM/yyyy"
                PopupButtonID="btnInspectionCalander" TargetControlID="txtInspect">
            </cc2:CalendarExtender>
            &nbsp;
            <asp:Label ID="lblTempFaultId" runat="server" Visible="False"></asp:Label>
            &nbsp;&nbsp;
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>

