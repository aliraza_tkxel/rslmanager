<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="drive.aspx.vb" Inherits="tenantsonline.drive" %>


<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
    
<%@ Register Src="../../../../user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 
    
<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">
    &nbsp;<table style="width: 100%">
        <tr>
            <td class="elementimages_btnback">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td>
        </tr>
        <tr>
            <td style="width: 100px">
    <img src="../../../../images/propertyImages/external/externalFaultImages/drive_01.gif"  width="546" height="431" border="0" usemap="#Map" id="drive" alt=""  /></td>
        </tr>
    </table>
    <map name="Map" id="driveMap" >        
       <area shape="POLY" coords="544,207,340,291,464,386,468,378,474,383,479,375,484,380,489,371,494,376,498,361,506,367,511,356,516,362,518,367,520,361,525,356,528,358,530,362,533,362,534,357,537,351,542,357,544,354" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
     </map>
</asp:Content>