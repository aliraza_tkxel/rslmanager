<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="path_fence.aspx.vb" Inherits="tenantsonline.path_fence" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
    
<%@ Register Src="../../../../user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 
    
<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">

<script type="text/javascript" language ="javascript">
//Load all images on page load
var pathHighlightedImage= new Image();
pathHighlightedImage.src="../../../../images/propertyImages/external/externalFaultImages/paths_01.gif";
var fenceHightlightedImage= new Image();
fenceHightlightedImage.src="../../../../images/propertyImages/external/externalFaultImages/fences_01.gif";

//function called on click event of listbox
    function changeImageSourceUsingList (sender) {
	    if(sender.selectedIndex==1)
	        changeImageSource();
	    else
	        changeToMain();	    
	}

//function called onmouseover of image map and change the image source 
//according to input paramater		
	function changeImageSource()
	{
		document.getElementById("path_fence").src = fenceHightlightedImage.src;					
	}
	
	//function called onmouseout of image map and set the path highlighted image 
	function changeToMain()
	{
		document.getElementById("path_fence").src = pathHighlightedImage.src;
	}
</script>

<table cellpadding="3" cellspacing="0" border="0">
 <tr><td class="elementimages_btnback">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td>
     <td align="right">
     </td>
 </tr>
 <tr>
    <td style="height: 435px">
        &nbsp;<img src="../../../../images/propertyImages/external/externalFaultImages/paths_01.gif"  width="546" height="431" border="0" usemap="#Map" id="path_fence" alt=""  /><map name="Map" id="pathFence" ><area shape="poly" coords="395,196,396,192,408,189,464,240,440,249" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" /><area shape="poly" coords="1,272,99,347,105,347,118,357,136,348,120,340,127,336,138,343,177,328,203,347,226,339,230,339,235,343,239,342,244,343,248,343,252,342,257,344,262,344,266,343,271,339,280,334,280,330,288,324,292,320,295,317,299,320,301,316,299,314,300,311,301,307,310,305,340,292,456,381,453,390,446,386,441,391,435,381,429,388,429,398,424,399,424,394,418,386,414,395,412,403,409,403,408,396,402,391,399,396,342,352,342,324,338,319,202,376,171,352,99,383,1,307" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" /><area shape="poly" coords="390,5,394,4,396,11,400,14,405,5,407,13,408,18,412,21,412,17,417,15,420,22,421,28,415,30,409,38,406,23,403,18,400,21,395,17,388,13,387,9" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" onmouseover="changeImageSource()" onmouseout="changeToMain()" /><area shape="poly" coords="425,32,429,35,429,29,432,27,436,34,436,39,443,44,446,44,448,37,451,45,452,50,456,54,460,57,465,48,470,62,474,67,478,67,479,62,483,61,485,69,485,73,490,77,493,77,497,71,501,79,500,82,509,91,509,85,515,80,518,91,518,96,526,101,526,95,530,92,533,101,533,109,541,113,544,106,543,122,543,146,533,143,533,152,525,149,525,133,517,127,517,138,508,135,509,121,500,116,500,126,493,122,493,111,485,103,485,115,477,112,477,98,469,90,469,101,461,98,461,84,454,80,452,90,443,86,446,74,436,67,437,77,435,78,429,74,429,60,421,56,420,47" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" onmouseover="changeImageSource()" onmouseout="changeToMain()" /><area shape="poly" coords="494,375,497,370,499,361,504,368,508,368,511,357,516,363,517,368,521,368,521,361,524,356,529,358,529,363,532,362,537,351,541,354,542,357,542,366,543,378,543,386,541,392,542,395,535,398,534,387,529,389,529,401,522,401,521,391,517,393,517,405,509,408,508,396,504,396,504,411,498,411,495,408" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" onmouseover="changeImageSource()" onmouseout="changeToMain()" /><area shape="poly" coords="333,429,333,424,336,413,344,421,343,428,349,427,349,417,351,408,360,416,360,423,366,421,365,414,368,402,375,411,375,419,381,415,380,409,385,397,392,407,392,413,397,411,397,404,401,393,408,401,408,408,413,403,413,397,417,387,423,394,424,402,430,398,429,389,434,381,441,389,440,428,431,426,429,406,422,408,423,417,428,421,424,423,424,430,417,428,417,410,416,426,407,428,349,428,342,429" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(1).ToString() %>" alt="<%= GetElementName(1).ToString() %>" title="<%= GetElementName(1).ToString()  %>" onmouseover="changeImageSource()" onmouseout="changeToMain()" /></map></td>
    <td class="listbox_td">
     <asp:ListBox  ID="lstElementName" runat="server" CssClass="listbox_elements" ></asp:ListBox></td>
  </tr>
     </table>
</asp:Content>
