<%@ Page Language="vb" MasterPageFile = "~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="door.aspx.vb" Inherits="tenantsonline.door" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
    
<%@ Register Src="../../../../user control/elementimages_back_button_control.ascx" TagName="elementimages_back_button_control"
    TagPrefix="uc2" %> 
    
<asp:Content ID = "Content" runat = "server" ContentPlaceHolderID ="page_area">
    &nbsp;<table style="width: 100%">
        <tr>
            <td class="elementimages_btnback">      
     <uc2:elementimages_back_button_control ID="elementimages_back_button_control" runat="server" />
     </td>
            
        </tr>
        <tr>
            <td style="width: 100px">
    <img src="../../../../images/propertyImages/external/externalFaultImages/door_01.gif"  width="546" height="431" border="0" usemap="#Map" id="door" alt=""  /></td>
            
        </tr>
    </table>
    <map name="Map" id="doorMap" >        
       <area shape="POLY" coords="132,256,171,242,169,316,175,320,175,326,139,340,138,335,133,331" href="<%=FaultConstants.DisplayFaultList %>?elementId=<%= GetElementId(0).ToString() %>" alt="<%= GetElementName(0).ToString() %>" title="<%= GetElementName(0).ToString()  %>" />
     </map>
</asp:Content>