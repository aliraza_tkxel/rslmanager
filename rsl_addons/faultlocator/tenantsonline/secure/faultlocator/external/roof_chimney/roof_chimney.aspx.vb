Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class roof_chimney
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

    Dim elementList As FaultElementList

#Region "Event "

#Region "Page Init"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

        'If there's not customer info, he/she will be redirected to ~/error.aspx
        If IsNothing(Session("custHeadBO")) Then
            'Response.Redirect("~/error.aspx")
        End If

    End Sub
#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetElements()
            Me.lstElementName.Attributes.Add("onclick", "changeImageSourceUsingList (this);")
            Me.LoadPathInSession()
        End If
    End Sub

#End Region

#End Region

#Region "Functions & Methods "

#Region "GetElements"
    Public Sub GetElements()

        Try
            'Create the Element BO
            Dim elementBO As New ElementBO()

            'Get the area ID from Query String
            If Not Request.QueryString("areaId") Is Nothing Then

                elementBO.AreaID = Request.QueryString("areaId")

                Dim faultBL As FaultManager = New FaultManager()

                Dim isExceptionGen As Boolean = elementBO.IsExceptionGenerated

                elementList = New FaultElementList()

                faultBL.GetElements(elementBO, elementList)
                Me.lstElementName.DataSource = elementList
                Me.lstElementName.DataTextField = "ElementName"
                Me.lstElementName.DataValueField = "ElementId"
                Me.lstElementName.DataBind()

                'Dim totalElements As String = GetElementName(0)
                If isExceptionGen Then

                    Response.Redirect("error.aspx")

                End If
            Else

                Response.Redirect("~/error.aspx")

            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "GetElementName"
    Function GetElementName(ByVal index As Integer) As String
        Try
            Return elementList.Item(index).ElementName
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try

    End Function
#End Region

#Region "GetElementId"
    Function GetElementId(ByVal index As Integer) As Integer
        Try
            Return elementList.Item(index).ElementID
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try

    End Function
#End Region

#Region "LoadPathInSession"
    Private Sub LoadPathInSession()
        Try

            Session(FaultConstants.BreadCrumbLoactionName) = FaultConstants.External
            Session(FaultConstants.BreadCrumbLoactionUrl) = FaultConstants.LoactionUrlString
            Session(FaultConstants.BreadCrumbAreaName) = "Roof/Chimney"
            Session(FaultConstants.BreadCrumbAreaUrl) = "~/secure/faultlocator/external/roof_chimney/roof_chimney.aspx"
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#End Region


End Class