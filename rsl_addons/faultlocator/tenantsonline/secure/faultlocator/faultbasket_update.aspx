<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/EnquiryManagement.Master" CodeBehind="faultbasket_update.aspx.vb" Inherits="tenantsonline.faultbasket_update" 
    title="Contractor Portal: Update Job Sheet Contractor" %>
    
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
    <table>
        <tr>
            <td style="width: 234px">
            </td>
            <td style="width: 600px">
    <table style="width: 600px">
        <tr>
            <td colspan="3" style="width: 600px">
                <asp:Label ID="lblHeading" runat="server" Font-Bold="True"
                    Font-Names="Arial" Text="I would like to update job sheet " Width="520px"></asp:Label>&nbsp;
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 600px; height: 56px">
                <asp:Label ID="lblRequestConfirmation" runat="server" Font-Bold="True"
                    Font-Names="Arial" Text="Request Summary and Confirmation" Width="570px"></asp:Label>
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 600px">
                <asp:Label runat="server" ID="lblJS" Font-Names="Arial" Font-Bold="True"></asp:Label> <hr />
                <cc2:toolkitscriptmanager id="Registration_ToolkitScriptManager" runat="server"></cc2:toolkitscriptmanager>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 600px">
                <asp:Label ID="lblFaultBasket" runat="server" Font-Bold="True" Font-Names="Arial"
                    Text="Fault(s):"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="width: 600px; height: 97px; vertical-align: top;">
                <asp:UpdatePanel ID="updatePanel_lblSaveSuccessfully" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        &nbsp;<asp:Label ID="lblMessage" runat="server" CssClass="info_message_label"></asp:Label><br />
                    </ContentTemplate>
                </asp:UpdatePanel>
    <table class="table_box" style="width: 600px">
        <tr>
            <td colspan="3" style="width: 603px;">
                <asp:GridView ID="grdTempFaultBasket" runat="server"
                    AutoGenerateColumns="False" CellPadding="4" EmptyDataText="No record found" Font-Names="Arial"
                    Font-Size="Small" ForeColor="#333333" GridLines="None" PageSize="1" Width="600px">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Area/Item/Element" InsertVisible="False" SortExpression="CreationDate">
                            <ItemTemplate>
                                <asp:Label ID="lblLocationName" runat="server" CssClass="cellData" Text='<%# Bind("LocationName") %>'></asp:Label>_<asp:Label
                                    ID="lblAreaName" runat="server" CssClass="cellData" Text='<%# Bind("AreaName") %>'></asp:Label>_<asp:Label
                                        ID="lblElementName" runat="server" CssClass="cellData" Text='<%# Bind("ElementName") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Qty" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" CssClass="cellData" Text='<%# Bind("Quantity") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Recharge" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblRecharge" runat="server" CssClass="cellData" Text='<%# Bind("Recharge") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Priority" InsertVisible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblExpectedTime" runat="server" CssClass="cellData" Text="<%# Bind('FResponseTime') %>"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contractor">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlContractor" runat="server" OnSelectedIndexChanged="ddlContractor_SelectedIndexChanged">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TempFaultId" InsertVisible="False" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblTempFaultId" runat="server" Text='<%# Eval("FaultLogId").ToString() %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 603px">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 21px; width: 603px;">
                <asp:Label ID="lblMyDetails" runat="server" Font-Bold="True"
                    Font-Names="Arial" Text="Customer Details:" Width="575px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 122px; width: 603px;">
                <table style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid;
                    width: 575px; border-bottom: black thin solid">
                    <tr>
                        <td style="width: 66px">
                            <asp:Label ID="lblNameLabel" runat="server" CssClass="caption" Text="Name:"></asp:Label></td>
                        <td class="caption" style="float: right; width: 152px;">
                            <asp:Label ID="lblTenantName" runat="server" CssClass="caption" Text=""></asp:Label></td>
                        <td style="width: 99px">
                            <asp:Label ID="lblTelephoneLabel" runat="server" EnableViewState="False" Text="Telephone:" CssClass="caption"></asp:Label></td>
                        <td style="width: 153px">
                            <asp:Label ID="lblTelephone" runat="server" CssClass="caption" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 66px; height: 21px">
                            <asp:Label ID="lblAddressLabel" runat="server" CssClass="caption" Text="Address:"></asp:Label></td>
                        <td class="caption" style="float: right; width: 152px; height: 21px;">
                            <asp:Label ID="lblAddress" runat="server" CssClass="caption" Text=""></asp:Label></td>
                        <td style="width: 99px; height: 21px">
                            <asp:Label ID="lblMobileLabel" runat="server" EnableViewState="False" Text="Mobile:" CssClass="caption"></asp:Label></td>
                        <td style="height: 21px">
                            <asp:Label ID="lblMobile" runat="server" CssClass="caption" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 66px">
                            <asp:Label ID="lblTownLabel" runat="server" CssClass="caption" Text="Town:"></asp:Label></td>
                        <td class="caption" style="float: right; width: 152px;">
                            <asp:Label ID="lblTown" runat="server" CssClass="caption" Text=""></asp:Label></td>
                        <td style="width: 99px">
                            <asp:Label ID="lblEmailLabel" runat="server" Text="Email:" CssClass="caption"></asp:Label></td>
                        <td>
                            <asp:Label ID="lblEmail" runat="server" CssClass="caption" Text=""></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 66px">
                            <asp:Label ID="lblCountyLabel" runat="server" CssClass="caption" Text="County:"></asp:Label></td>
                        <td class="caption" style="float: right; width: 152px;">
                            <asp:Label ID="lblCountry" runat="server" CssClass="caption" Text=""></asp:Label></td>
                        <td style="width: 99px">
                        </td>
                        <td style="float: right; width: 153px">
                            <asp:Button ID="btnUpdateMyDetail" runat="server" CausesValidation="False" EnableViewState="False"
                                PostBackUrl="~/secure/faultlocator/update_customer_detail.aspx" Text="Update Customer Details" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width: 603px; height: 41px">
                <asp:Label ID="lblPreferredContactTime" runat="server" CssClass="caption" Text="If customer has any preferred contact time or ways in which we could contact them, please enter them in the box below:"
                    Width="577px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 72px; width: 603px;">
                <asp:TextBox ID="txtPreferredContactTime" runat="server" Columns="90" Rows="4" TextMode="MultiLine"
                    Width="570px" CssClass="caption"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="3" style="float: right; height: 29px; width: 603px;">
                <asp:CheckBox ID="CheckBox1" runat="server" CssClass="caption" Text="Customer would like to be emailed about the level of satisfaction with their reported faults."
                    Width="640px" /></td>
        </tr>
        <tr>
            <td colspan="3" style="float: right; width: 603px; height: 18px">
                <asp:CheckBox ID="chkCancelFaults" runat="server" Text="Cancel the reported fault (Enter the reason for cancellation below)" CssClass="caption" /></td>
        </tr>
        <tr>
            <td colspan="3" style="float: right; width: 603px; height: 18px">
                <asp:TextBox ID="txtReason" runat="server" Height="72px" TextMode="MultiLine" Width="568px" CssClass="caption" MaxLength="500"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="1" style="float: right; width: 400px; height: 46px">
                <asp:Button ID="btnSubmit" runat="server" Text="Update Job Sheet" /></td>
             <td align="center" style="width:10px"><asp:Button ID="btnBack" runat="server" CausesValidation="False" EnableViewState="False"
                                PostBackUrl="~/secure/faultlocator/fault_locator.aspx" Text="Back to Reported Faults" /></td>

        </tr>
    </table>
            </td>
        </tr>
    </table>
            </td>
            <td style="width: 124px">
            </td>
        </tr>
    </table>
    &nbsp; &nbsp; &nbsp;&nbsp;
</asp:Content>
