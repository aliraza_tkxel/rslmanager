<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    CodeBehind="job_sheet_summary.aspx.vb" Inherits="tenantsonline.JobSheetSummary"
    ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">
    <table style="width: 100%" class="table_box">
        <tr>
            <td colspan="3">
                <asp:Label ID="lblHeading" runat="server" CssClass="header_black" Font-Bold="True"
                    Font-Names="Arial" Text="Report Fault" Width="484px"></asp:Label>&nbsp;
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 2px">
                <asp:Label ID="lblJobSheetSummary" runat="server" CssClass="header_black" Font-Bold="True"
                    Font-Names="Arial" Text="Job Sheet Summary" Width="431px"></asp:Label>
                &nbsp;
                <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="noprint" OnClientClick="window.print()" />
                <asp:Label ID="lblTopPaging" runat="server" CssClass="caption" Width="57px"></asp:Label>
                <hr />
                <asp:Label ID="lblMessage" runat="server" CssClass="info_message_label" ForeColor="Transparent"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%">
                    <tr>
                        <td valign="top" width="50%">
                            <table width="100%">
                                <tr>
                                    <td valign="top">
                                        <asp:Label ID="lblJsNumberLabel" runat="server" Text="JS Number:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblJSNumber" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblContractorLabel" runat="server" Text="Contractor:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblContractor" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPriorityLabel" runat="server" Text="Priority:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPriority" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblReponseTimeLabel" runat="server" Text="ResponseTime:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        &nbsp;<asp:Label ID="lblResponseTime" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCompletionLabel" runat="server" Text="Completion Due:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCompletion" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblOrderDateLabel" runat="server" Text="Order Date:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOrderDate" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAsbestorLabel" runat="server" Text="Asbestos:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAsbestos" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" width="50%">
                            <table width="100%">
                                <tr>
                                    <td valign="top">
                                        <asp:Label ID="lblTenantNameLabel" runat="server" Text="Name:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTenantName" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAddressLabel" runat="server" Text="Address:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAddress" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTownLabel" runat="server" Text="Town:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTown" runat="server" Text="" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblPostCodeLabel" runat="server" Text="PostCode:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPostCode" runat="server" Text="" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTelephoneLabel" runat="server" EnableViewState="False" Text="Telephone:"
                                            CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTelephone" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMobileLabel" runat="server" EnableViewState="False" Text="Mobile:"
                                            CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblMobile" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblEmailLabel" runat="server" Text="Email:" CssClass="caption"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEmail" runat="server" CssClass="caption"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="grdAsbestosList" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                ForeColor="#333333" GridLines="None" CssClass="caption">
                                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="RISK" HeaderText="Risk" />
                                    <asp:BoundField DataField="ELEMENTDISCRIPTION" HeaderText="Description" />
                                </Columns>
                                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        </tr>
    </table>
    <table class="table_box" style="width: 100%" id="GridTable">
        <tr>
            <td colspan="3">
                <asp:GridView ID="grdCustomerReportedFaults" runat="server" CellPadding="4" AutoGenerateColumns="False"
                    EmptyDataText="No record found" ForeColor="#333333" GridLines="None" Font-Names="Arial"
                    Font-Size="Small" Width="600px" AllowPaging="True" PageSize="1">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                    <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField InsertVisible="False" HeaderText="Area/Item/Element" SortExpression="CreationDate">
                            <ItemTemplate>
                                <asp:Label ID="lblLocationName" runat="server" CssClass="cellData" Text='<%# Bind("LocationName") %>' />_<asp:Label
                                    ID="lblAreaName" runat="server" CssClass="cellData" Text='<%# Bind("AreaName") %>' />_<asp:Label
                                        ID="lblElementName" runat="server" CssClass="cellData" Text='<%# Bind("ElementName") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField InsertVisible="False" HeaderText="Description">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField InsertVisible="False" HeaderText="Qty">
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" CssClass="cellData" Text='<%# Bind("Quantity") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField InsertVisible="False" HeaderText="Priority">
                            <ItemTemplate>
                                <asp:Label ID="lblExpectedTime" runat="server" CssClass="cellData" Text="<%# Bind('FResponseTime') %>" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Recharge Cost">
                         <ItemTemplate>
                         <asp:Label ID="lblFRecharge" runat="server" CssClass="cellData" Text='<%# System.Math.Round(Convert.ToDouble(Eval("FRecharge")),2).ToString() %>' />
                        </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Recharge?">
                            <ItemTemplate>
                                <asp:Label ID="lblFRecharge" runat="server" CssClass="cellData" Text='<%# Bind("Recharge") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Visible="False" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="width: 550px">
                    <tr>
                        <td style="width: 262px; vertical-align: top;">
                            <asp:Label ID="lblProblemDays" runat="server" CssClass="caption" Width="256px"></asp:Label>
                        </td>
                        <td rowspan="3" style="width: 275px; vertical-align: top; text-align: left;">
                            <div style="width: 275px; border-right: black thin solid; border-top: black thin solid;
                                border-left: black thin solid; border-bottom: black thin solid; height: 65px;">
                                <asp:Label ID="lblNotesLabel" runat="server" CssClass="caption"></asp:Label>&nbsp;
                                <asp:Label ID="lblNotes" runat="server" CssClass="caption"></asp:Label></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 262px">
                            <asp:Label ID="lblRecuringProblem" runat="server" CssClass="caption" Width="256px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 262px; height: 30px;">
                            <asp:Label ID="lblCommunalArea" runat="server" CssClass="caption" Width="256px"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblPreferredContactTime" runat="server" CssClass="caption" Text="Customer has any preferred contact time or ways in which we could contact them"
                    Width="577px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="width: 590px">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtPreferredContactTime" runat="server" Columns="90" Rows="4" TextMode="MultiLine"
                                Width="400px" ReadOnly="True" Height="61px"></asp:TextBox>
                        </td>
                        <td style="width: 1500px; vertical-align: bottom;">
                            <asp:Button ID="btnBackBottom" runat="server" Text="< Back" CommandName="Prev" OnCommand="NavigationLink_Click"
                                CssClass="noprint" />
                            <asp:Button ID="btnNext" runat="server" Text="Next >" CommandName="Next" OnCommand="NavigationLink_Click"
                                CssClass="noprint" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
