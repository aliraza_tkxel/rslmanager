<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false" CodeBehind="temp_fault_basket.aspx.vb" Inherits="tenantsonline.TempFaultBasket" ValidateRequest="false" EnableEventValidation="false" %> 

<asp:Content ID="PageContent" ContentPlaceHolderID="page_area" runat="server">


    <table style="width: 100%">
        <tr>
            <td colspan="3">
                &nbsp;<asp:Label ID="lblFaultBasketHeading" runat="server" CssClass="sub_header_black"
                    Text="Fault Basket" Width="208px"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3" style="height: 21px; color: #ffffff; background-color: #ffffff;">
                <asp:Label ID="lblMessage" runat="server" CssClass="info_message_label" ForeColor="Transparent"></asp:Label></td>
        </tr>
    </table>
    <table class="table_box" style="width: 100%" id="TABLE1">
        <tr>
            <td colspan="3">
                <asp:GridView ID="grdTempFaultBasket" runat="server" CellPadding="4" AutoGenerateColumns="False"
                    EmptyDataText="No record found" ForeColor="#333333" GridLines="None" Font-Names="Arial" Font-Size="Small" Width="600px" PageSize="1">
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                    <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left"  />
                    <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White"  />
                    <Columns>
                        <asp:TemplateField HeaderText="TempFaultId" InsertVisible="False" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblTempFaultId" runat="server" Text='<%# Eval("TempFaultID").ToString() %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    <asp:TemplateField InsertVisible="False" HeaderText="Area/Item/Element" SortExpression="CreationDate" >
                        <ItemTemplate>
                         <asp:Label ID="lblLocationName" runat="server" CssClass="cellData" Text='<%# Bind("LocationName") %>' />_<asp:Label ID="lblAreaName" runat="server" CssClass="cellData" Text='<%# Bind("AreaName") %>' />_<asp:Label ID="lblElementName" runat="server" CssClass="cellData" Text='<%# Bind("ElementName") %>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                     <asp:TemplateField InsertVisible="False" HeaderText="Description" >
                        <ItemTemplate>
                         <asp:Label ID="lblDescription" runat="server" CssClass="cellData" Text='<%# Bind("Description") %>' />
                        </ItemTemplate>
                         <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                    <asp:TemplateField InsertVisible="False" HeaderText="Qty">
                        <ItemTemplate>
                            &nbsp;
                            <asp:DropDownList ID="ddlQuantity" runat="server" CssClass="select_normal" style="width:50px" SelectedValue='<%# Bind("Quantity") %>' >
                            <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                            <asp:ListItem Value="6">6</asp:ListItem>
                            <asp:ListItem Value="7">7</asp:ListItem>
                            <asp:ListItem Value="8">8</asp:ListItem>
                            <asp:ListItem Value="9">9</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                        </asp:DropDownList>
                        </ItemTemplate>
                        <EditItemTemplate>
                            &nbsp;
                        </EditItemTemplate>
                    </asp:TemplateField>
                    
                     <asp:TemplateField InsertVisible="False" HeaderText="Recharge?" >
                        <ItemTemplate>
                         <asp:Label ID="lblRecharge" runat="server" CssClass="cellData" Text="<%# Bind('Recharge') %>" />
                        </ItemTemplate>
                         <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    
                     <asp:TemplateField InsertVisible="False" HeaderText="Priority" >
                        <ItemTemplate>
                         <asp:Label ID="lblExpectedTime" runat="server" CssClass="cellData" Text="<%# Bind('FResponseTime') %>" />
                        </ItemTemplate>
                         <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField InsertVisible="False" HeaderText="Delete" >
                        <ItemTemplate>
                         <asp:Button ID="btnDelete" onclick="btnDelete_Click" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure to delete the selected record?');" CommandArgument='<%# Eval("TempFaultID").ToString() %>'></asp:Button> 
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    </Columns>
                </asp:GridView>                
            </td>
        </tr>
        <tr>
            <td colspan="3"><hr /></td>
        </tr>
        <tr>
            <td style="width: 147460px; text-align:right">
    <asp:Button ID="btnAddMore" runat="server" Text="Add More Faults" /></td>
            <td style="width: 381px;">
                <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandArgument='<%# Eval("TempFaultID").ToString() %>' /></td>
            <td style="width: 64px">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" /></td>
        </tr>
    </table>
    <asp:TextBox ID="txtCountRecords" runat="server" Visible="False" Text="0"></asp:TextBox>


</asp:Content>

