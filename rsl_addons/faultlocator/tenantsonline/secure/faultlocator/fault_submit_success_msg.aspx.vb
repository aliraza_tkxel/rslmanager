Partial Public Class FaultSubmitSuccessMsg
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If
    End Sub

End Class