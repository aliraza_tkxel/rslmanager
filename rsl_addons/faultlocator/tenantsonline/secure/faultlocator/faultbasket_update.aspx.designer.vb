

'''<summary>
'''faultbasket_update class.
'''</summary>
'''<remarks>
'''Auto-generated class.
'''</remarks>
Partial Public Class faultbasket_update

    '''<summary>
    '''lblHeading control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeading As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRequestConfirmation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRequestConfirmation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblJS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblJS As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Registration_ToolkitScriptManager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Registration_ToolkitScriptManager As Global.AjaxControlToolkit.ToolkitScriptManager

    '''<summary>
    '''lblFaultBasket control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaultBasket As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''updatePanel_lblSaveSuccessfully control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updatePanel_lblSaveSuccessfully As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''grdTempFaultBasket control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdTempFaultBasket As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''lblMyDetails control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMyDetails As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNameLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNameLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTenantName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTenantName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTelephoneLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTelephoneLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTelephone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTelephone As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAddressLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddressLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAddress As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMobileLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMobileLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblMobile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMobile As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTownLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTownLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTown As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEmailLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmailLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEmail As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCountyLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCountyLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCountry As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnUpdateMyDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdateMyDetail As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblPreferredContactTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPreferredContactTime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtPreferredContactTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPreferredContactTime As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CheckBox1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CheckBox1 As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkCancelFaults control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkCancelFaults As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''txtReason control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReason As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSubmit As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBack As Global.System.Web.UI.WebControls.Button
End Class
