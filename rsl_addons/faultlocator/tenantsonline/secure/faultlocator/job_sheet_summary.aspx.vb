Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.Data

Partial Public Class JobSheetSummary
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage


#Region "Page_Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        'If IsNothing(ASPSession("USERID")) Then
        'Response.Redirect("~/../BHAIntranet/login.aspx")
        'End If

        'If there's not customer info, he/she will be redirected to ~/error.aspx
        If IsNothing(Session("custHeadBO")) Then
            'Response.Redirect("~/error.aspx")
        End If

        Response.Cache.SetNoStore()

    End Sub
#End Region

#Region "Page_Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            'Load Customer Reported Faults
            Me.LoadFaults()

            'Get Customer Information
            Me.getCustomerInfo()

        End If

    End Sub

#End Region

#Region "Events"

    Protected Sub grdCustomerReportedFaults_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdCustomerReportedFaults.PageIndexChanging
        Try
            'Set the grid index
            Me.grdCustomerReportedFaults.PageIndex = e.NewPageIndex

            'Load fault data 
            Me.LoadFaultOnIndexChange()

        Catch ex As Exception
            'redirect to error page
            'Response.Redirect("~/error.aspx")
        End Try

    End Sub

#Region "Handler for Navigation Buttons Next Previous"
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Try
            'Select case to identify the user action
            Select Case e.CommandName
                Case "Next"

                    'Increase the index of grid to load next fault
                    grdCustomerReportedFaults.PageIndex = grdCustomerReportedFaults.PageIndex + 1

                    'If back button is already disabled & user want to move ahead then enable the back button
                    If Me.btnBackBottom.Enabled = False Then
                        Me.btnBackBottom.Enabled = True
                    End If

                Case "Prev"

                    'If previous index is greater or equal to zero then move to previous index
                    If grdCustomerReportedFaults.PageIndex - 1 >= 0 Then
                        'Set the grid index
                        grdCustomerReportedFaults.PageIndex = grdCustomerReportedFaults.PageIndex - 1
                    End If


                    'If next button is already disabled & user want to move back then enable the next button
                    If Me.btnNext.Enabled = False Then
                        Me.btnNext.Enabled = True
                    End If

            End Select

            'Load fault data Set
            Me.LoadFaultOnIndexChange()
            
        Catch ex As Exception
            'redirect to error page
            'Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#End Region

#Region "LoadFaults"
    'This function loads the faults in Grid
    Protected Sub LoadFaults()
        Try

            'Create the FaultBO
            Dim faultBO As FaultBO = New FaultBO()

            'Create the FaultLogBO
            Dim faultLogBO As FaultLogBO = New FaultLogBO()

            'Creat the object of business layer
            Dim faultBL As FaultManager = New FaultManager()

            'Create the obj of customer header bo
            Dim headerBO As CustomerHeaderBO = Nothing

            'Create the obj of exception
            Dim isExceptionGen As Boolean = faultBO.IsExceptionGenerated

            'Create the obj of dataset
            Dim faultDS As DataSet = New DataSet()

            'Get CustomerHeaderBO from session to get CustomerId
            headerBO = CType(Session("custHeadBO"), CustomerHeaderBO)
            faultLogBO.CustomerId = CType(Session("FAULTBASKETID"), Integer)

            'Assign the attributes of FaultBO to FaultLogBO
            faultLogBO.FaultBO = faultBO
            faultBL.GetCustomerReportedFaults(faultLogBO, faultDS)
            'faultBL.GetPropertySummaryAsbestosData(faultDS, faultLogBO.CustomerId)
            GetPropertySummaryAsbestosData(faultLogBO.CustomerId)

            ''Check for exception
            If faultLogBO.IsExceptionGenerated Then
                'Response.Redirect("~/error.aspx")
            End If

            'If flag is true then set the data set and fault presentation labels
            If faultLogBO.IsFlagStatus = True And faultDS.Tables(0).Rows.Count > 0 Then

                'Populating the data grid with data set
                ViewState.Add(ApplicationConstants.FaultJobSheetDataSet, faultDS)

                'Set & bind dataset
                Me.SetandBindDataSet(faultDS)

                'Populate the fault presentation labels
                Me.setFaultPresentationLables(faultDS, 0)

            Else
                Me.lblMessage.Text = faultLogBO.UserMsg
            End If

        Catch ex As Exception
            'redirect to error page
            'Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "Get Job Sheet Summary Asbestos Data "

    Private Sub GetPropertySummaryAsbestosData(ByVal CustomerId As Integer)
        Try
            Dim faultBL As FaultManager = New FaultManager()
            Dim jsDS As New DataSet()
            faultBL.GetPropertySummaryAsbestosData(jsDS, CustomerId)
            If jsDS.Tables(0).Rows.Count > 0 Then
                grdAsbestosList.DataSource = Nothing
                grdAsbestosList.DataSource = jsDS
                grdAsbestosList.DataBind()
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "setFaultPresentationLables"

    Private Sub setFaultPresentationLables(ByRef faultDS As DataSet, ByRef recordIndex As Integer)

        Try
            'Declare some string variable to set order date and completion date
            Dim orderDate As String
            Dim completionDate As String
            Dim onlyResponseTime As String
            Dim onlyDayHour As String

            'Set Presentation Lables of Fault from Dataset
            Me.lblContractor.Text = IIf(faultDS.Tables(0).Rows(recordIndex).Item("Contractor").ToString() = "", "N/A", faultDS.Tables(0).Rows(recordIndex).Item("Contractor"))
            Me.lblPriority.Text = IIf(faultDS.Tables(0).Rows(recordIndex).Item("PriorityName").ToString() <> "", faultDS.Tables(0).Rows(recordIndex).Item("PriorityName"), "")
            Me.lblResponseTime.Text = IIf(faultDS.Tables(0).Rows(recordIndex).Item("FResponseTime").ToString() <> "", faultDS.Tables(0).Rows(recordIndex).Item("FResponseTime"), "")
            Me.lblCompletion.Text = UtilityFunctions.FormatDate(faultDS.Tables(0).Rows(recordIndex).Item("DueDate").ToString())
            Me.lblJSNumber.Text = IIf(faultDS.Tables(0).Rows(recordIndex).Item("JSNumber").ToString() <> "", faultDS.Tables(0).Rows(recordIndex).Item("JSNumber"), "")
            Me.lblOrderDate.Text = UtilityFunctions.FormatDate(faultDS.Tables(0).Rows(0)("OrderDate").ToString())
            Me.lblAsbestos.Text = faultDS.Tables(0).Rows(0)("AsbestosRisk").ToString()
            If faultDS.Tables(0).Rows(0).Item("ProblemDays").ToString() = "8" Then
                Me.lblProblemDays.Text = "This fault has existed for 7 + day(s)"
            Else
                Me.lblProblemDays.Text = "This fault has existed for:" + faultDS.Tables(0).Rows(0).Item("ProblemDays").ToString() & " day(s)"
            End If
            Me.lblRecuringProblem.Text = IIf(faultDS.Tables(0).Rows(recordIndex).Item("RecuringProblem") <> 0, "This is a recurring problem", "This is not a recurring problem")
            Me.lblCommunalArea.Text = IIf(faultDS.Tables(0).Rows(recordIndex).Item("CommunalProblem") <> 0, "This is a communal problem", "This is not a communal problem")
            Me.lblNotes.Text = IIf(faultDS.Tables(0).Rows(recordIndex).Item("Notes").ToString() <> "", faultDS.Tables(0).Rows(recordIndex).Item("Notes"), "")
            Me.txtPreferredContactTime.Text = IIf(faultDS.Tables(0).Rows(recordIndex).Item("PreferredContactDetails").ToString() <> "", faultDS.Tables(0).Rows(recordIndex).Item("PreferredContactDetails"), "")

            'Get the order date
            orderDate = IIf(CType(faultDS.Tables(0).Rows(recordIndex).Item("OrderDate"), String) <> "", faultDS.Tables(0).Rows(recordIndex).Item("OrderDate"), "")
            Me.lblOrderDate.Text = orderDate.ToString()

            'Get the response time
            onlyResponseTime = IIf(faultDS.Tables(0).Rows(recordIndex).Item("OnlyResponseTime") <> 0, faultDS.Tables(0).Rows(recordIndex).Item("OnlyResponseTime"), "0")

            'Get the string hour or day :see stored procedure
            onlyDayHour = faultDS.Tables(0).Rows(recordIndex).Item("OnlyDayHour")

            'Create an instance of datetime
            Dim Dt As DateTime = New DateTime()
            Dt = orderDate

            'If response time is in day then add days in order date else add hours in order date
            If onlyDayHour = "Day" Then
                completionDate = Dt.Add(New TimeSpan(onlyResponseTime, 0, 0, 0)).ToString()
            Else
                completionDate = Dt.Add(New TimeSpan(onlyResponseTime, 0, 0)).ToString()
            End If

            'Set the completion label
            Me.lblCompletion.Text = completionDate

        Catch ex As Exception
            'redirect to error page
            'Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "GetCustomerInfo"

    ''Get the customer Info to be displayed on welcome page
    Private Sub getCustomerInfo()
        Try

            Dim custDetailsBO As CustomerDetailsBO = New CustomerDetailsBO()

            '' Obtaining object from Session
            Dim custHeadBO As CustomerHeaderBO = Nothing
            custHeadBO = CType(Session("custHeadBO"), CustomerHeaderBO)


            '' Passing info to custDetailsInfo, that will be used to populate customer's Home page
            custDetailsBO.Id = CType(custHeadBO.CustomerID, Integer)
            custDetailsBO.Tenancy = custHeadBO.TenancyID

            Dim custAddressBO As New CustomerAddressBO
            custDetailsBO.Address = custAddressBO

            Dim objBL As New CustomerManager()

            'Dim resultStatus As Boolean = objBL.GetCustomerInfo(custDetailsBO, False)
            Dim resultStatus As Boolean = objBL.GetCustomerContactDetail(custDetailsBO)

            ''Check for exception 
            Dim exceptionFlag As Boolean = custDetailsBO.IsExceptionGenerated

            If exceptionFlag Then
                'Response.Redirect("~/error.aspx")
            End If

            If resultStatus Then

                setCustomerPresentationLables(custHeadBO, custDetailsBO)

            End If

        Catch ex As Exception
            'redirect to error page
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "setCustomerPresentationLables"

    Private Sub setCustomerPresentationLables(ByRef custHeadBO As CustomerHeaderBO, ByRef custDetailsBO As CustomerDetailsBO)

        Try

            'Set Presentation lables from CustomerDetailsBO object
            Me.lblTenantName.Text = custHeadBO.Title & " " & custDetailsBO.FirstName & " " & custDetailsBO.MiddleName & " " & custDetailsBO.LastName
            Me.lblAddress.Text = IIf(custDetailsBO.Address.HouseNumber <> "", custDetailsBO.Address.HouseNumber, "")
            Me.lblAddress.Text &= " " & IIf(custDetailsBO.Address.Address1 <> "", custDetailsBO.Address.Address1, "")
            Me.lblTown.Text = IIf(custDetailsBO.Address.TownCity <> "", custDetailsBO.Address.TownCity, "")
            'Me.lblCountry.Text = IIf(custDetailsBO.Address.Coutnty <> "", custDetailsBO.Address.Coutnty, "")
            Me.lblPostCode.Text = IIf(custDetailsBO.Address.PostCode <> "", custDetailsBO.Address.PostCode, "")
            Me.lblTelephone.Text = IIf(custDetailsBO.Address.Telephone <> "", custDetailsBO.Address.Telephone, "")
            Me.lblMobile.Text = IIf(custDetailsBO.Address.TelephoneMobile <> "", custDetailsBO.Address.TelephoneMobile, "")
            Me.lblEmail.Text = IIf(custDetailsBO.Address.Email <> "", custDetailsBO.Address.Email, "")

        Catch ex As Exception
            'redirect to error page
            'Response.Redirect("~/error.aspx")
        End Try
    End Sub

#End Region

#Region "LoadFaultOnIndexChange"
    Private Sub LoadFaultOnIndexChange()

        Try
            Dim pageIndex = Me.grdCustomerReportedFaults.PageIndex
            Dim faultDs As DataSet = CType(ViewState.Item(ApplicationConstants.FaultJobSheetDataSet), DataSet)

            'Set & bind dataset
            Me.SetandBindDataSet(faultDs)

            'Populate the fault presentation labels
            Me.setFaultPresentationLables(faultDs, pageIndex)

        Catch ex As Exception
            'Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "SetandBindDataSet"
    Private Sub SetandBindDataSet(ByRef faultDS As DataSet)
        Try

            Me.grdCustomerReportedFaults.DataSource = faultDS
            Me.grdCustomerReportedFaults.DataBind()

            'We will count the total number of records
            Dim pageCount As String
            pageCount = faultDS.Tables(0).Rows.Count.ToString()

            'Set the label
            Me.lblTopPaging.Text = (grdCustomerReportedFaults.PageIndex + 1).ToString + " of " + pageCount

            'Disable the next prev button 
            Me.DisableNextPrevButton(pageCount)

        Catch ex As Exception
            'Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "DisableNextPrevButton"
    Private Sub DisableNextPrevButton(ByVal pageCount As Integer)

        'If previous of previous index is greater or equal to total records then disabled the previous button
        If grdCustomerReportedFaults.PageIndex - 1 < 0 Then
            Me.btnBackBottom.Enabled = False
        End If

        'If index reaches to total records then disabled the next button
        If grdCustomerReportedFaults.PageIndex + 1 = pageCount Then
            Me.btnNext.Enabled = False
        End If

    End Sub
#End Region

    
End Class