<%@ Page Language="vb" MasterPageFile="~/master pages/MenuMaster.Master" AutoEventWireup="false"
    Codebehind="fault_submit_success_msg.aspx.vb" Inherits="tenantsonline.FaultSubmitSuccessMsg" Title="Tenants Online :: Success Msg" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">

  <span style="font-size:12pt; font-family: Arial">
    Thank you your fault has been received and you will be contacted by our contractor to arrange a time to visit you.
    <br />
  </span>
    
</asp:Content>
