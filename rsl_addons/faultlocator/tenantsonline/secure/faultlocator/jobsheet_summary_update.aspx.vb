Imports System

Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Imports System.Data

Partial Public Class jobsheet_summary_update
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

        If Not Request.QueryString("faultlogId") = String.Empty Then
            Dim faultLogId As Integer = CType(Request.QueryString("faultlogid"), Integer)
            'Dim faultLogId As Integer = 50262
            GetJobSheetSummaryData(faultLogId)
            GetJobSheetSummaryAsbestosData(faultLogId)
        Else
            Response.Redirect("~/error.aspx")
        End If
    End Sub

#Region "Methods"

#Region "Get Job Sheet Summary Data "

    Private Sub GetJobSheetSummaryData(ByVal faultLogId As Integer)
        Try
            Dim faultBL As FaultManager = New FaultManager()
            Dim jsDS As New DataSet()
            faultBL.GetJSSummaryUpdateData(jsDS, faultLogId)
            If jsDS.Tables(0).Rows.Count > 0 Then
                grdCustomerReportedFaults.DataSource = Nothing
                grdCustomerReportedFaults.DataSource = jsDS
                grdCustomerReportedFaults.DataBind()
                populateControls(jsDS)
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Get Job Sheet Summary Asbestos Data "

    Private Sub GetJobSheetSummaryAsbestosData(ByVal faultLogId As Integer)
        Try
            Dim faultBL As FaultManager = New FaultManager()
            Dim jsDS As New DataSet()
            faultBL.GetJSSummaryAsbestosUpdateData(jsDS, faultLogId)
            If jsDS.Tables(0).Rows.Count > 0 Then
                grdAsbestosList.DataSource = Nothing
                grdAsbestosList.DataSource = jsDS
                grdAsbestosList.DataBind()
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Populate Controls "
    Private Sub populateControls(ByRef jsDS As DataSet)
        Try
            If jsDS.Tables(0).Rows.Count > 0 Then
                Me.lblContractor.Text = jsDS.Tables(0).Rows(0)("Contractor").ToString()
                Me.lblPriority.Text = jsDS.Tables(0).Rows(0)("PriorityName").ToString()
                Me.lblResponseTime.Text = jsDS.Tables(0).Rows(0)("FResponseTime").ToString()
                Me.lblCompletion.Text = UtilityFunctions.FormatDate(jsDS.Tables(0).Rows(0)("DueDate").ToString())
                Me.lblJSNumber.Text = jsDS.Tables(0).Rows(0)("JSNumber").ToString()
                Me.lblOrderDate.Text = UtilityFunctions.FormatDate(jsDS.Tables(0).Rows(0)("OrderDate").ToString())
                Me.lblAsbestos.Text = jsDS.Tables(0).Rows(0)("AsbestosRisk").ToString()
                Me.txtPreferredContactTime.Text = jsDS.Tables(0).Rows(0)("PreferredContactDetails").ToString()
                Me.lblTenantName.Text = jsDS.Tables(0).Rows(0)("FIRSTNAME").ToString()
                Me.lblAddress.Text = jsDS.Tables(0).Rows(0)("ADDRESS1").ToString()
                Me.lblTown.Text = jsDS.Tables(0).Rows(0)("TOWNCITY").ToString()
                Me.lblCountry.Text = jsDS.Tables(0).Rows(0)("COUNTY").ToString()
                Me.lblTelephone.Text = jsDS.Tables(0).Rows(0)("TEL").ToString()
                Me.lblMobile.Text = jsDS.Tables(0).Rows(0)("MOBILE").ToString()
                Me.lblEmail.Text = jsDS.Tables(0).Rows(0)("EMAIL").ToString()
                Me.lblProblemDays.Text = "This fault has existed for:" + jsDS.Tables(0).Rows(0).Item("ProblemDays").ToString() & " day(s)"
                Me.lblRecuringProblem.Text = IIf(jsDS.Tables(0).Rows(0).Item("RecuringProblem") <> 0, "This is a recurring problem", "This is not recurring problem")
                Me.lblCommunalArea.Text = IIf(jsDS.Tables(0).Rows(0).Item("CommunalProblem") <> 0, "This is a communal problem", "This is not a communal problem")
                lblNotes.Text = jsDS.Tables(0).Rows(0)("Notes").ToString()
            End If
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region
#End Region

End Class