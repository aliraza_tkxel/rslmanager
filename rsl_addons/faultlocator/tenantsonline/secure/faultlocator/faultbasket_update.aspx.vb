Imports System
Imports Broadland.TenantsOnline.Utilities
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject

Partial Public Class faultbasket_update    
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

#Region "Page Load "

    ' User Id will come from Login 
    Dim userId As Integer = 143
    'Dim userId As Integer = ASPSession("USERID")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (Me.IsPostBack) Then
            'If user isn't authenticated, he/she will be redirected to ~/login.aspx
            If IsNothing(ASPSession("USERID")) Then
                Response.Redirect("~/../BHAIntranet/login.aspx")
            End If


            Dim row As GridViewRow

            'If Not Request.QueryString("faultlogId") = String.Empty Then
            If Not Session("faultLogId") = String.Empty Then

                'Dim faultLogId As Integer = CType(Request.QueryString("faultlogid"), Integer)
                lblJS.Text = "Job Sheet Number:" & Session("faultLogId").ToString()
                Dim faultLogId As Integer = CType(Session("faultLogId").ToString(), Integer)
                Dim contractorName As String = CType(Session("contractorName"), String)



                PopulateFaultBasketUpdateDate(faultLogId)
                'GetContractorLookup()

                'If (Request.QueryString("contractorName") <> String.Empty) Then

                'Dim contractorName As String = CType(Request.QueryString("contractorName"), String)

                For Each row In grdTempFaultBasket.Rows

                    Dim contractorDropDown As DropDownList = DirectCast(row.Cells(4).FindControl("ddlContractor"), DropDownList)
                    If (contractorName <> "TBA") Then
                        GetContractorLookup(contractorName)

                    Else
                        GetContractorLookup()
                    End If
                    'contractorDropDown.Items.Clear()
                    ''Dim contractorDropDown As DropDownList = DirectCast(grdTempFaultBasket.FindControl("ddlContractor"), DropDownList)
                    ''contractorDropDown.SelectedValue = contractorName
                    'contractorDropDown.Items.Add(New ListItem(contractorName, ""))
                    'contractorDropDown.DataBind()

                Next





                'PopulateFaultBasketUpdateDate(faultLogId)
                'GetContractorLookup()

            Else
                Response.Redirect("~/error.aspx")
            End If
        End If

    End Sub
#End Region

#Region "Events "

#Region "btn Submit Click "

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        UpdateFaultLogContractor(GetUserId())

    End Sub

#End Region

#Region "Update Cancel Grid "

    Public Sub UpdateCancelGrid(ByVal userId As Integer)
        Try
            Dim faultLogId As Integer
            Dim reasonOfCancellation As String = txtReason.Text.ToString().Trim()
            faultLogId = CType((DirectCast(grdTempFaultBasket.Rows(0).Cells(5).FindControl("lblTempFaultId"), Label).Text), Integer)
            Dim contractPoManager As ContractorPortalManager = New ContractorPortalManager()
            contractPoManager.UpdateCancelReportGrid(faultLogId, reasonOfCancellation, userId)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")

        End Try

    End Sub
#End Region

#Region "ddl Contractor Selected Index Changed "

    Protected Sub ddlContractor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim contractorddl As DropDownList = DirectCast(sender, DropDownList)
        Dim str2 As String = contractorddl.SelectedItem.Value
    End Sub
#End Region

#Region "btn Update My Detail Click "
    Protected Sub btnUpdateMyDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateMyDetail.Click
        Response.Redirect("update_customer_detail.aspx?customer")
    End Sub
#End Region

#End Region

#Region "Methods"

#Region "Lookup Methods"
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.DataBind()
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.SelectedValue = ""

        End If

    End Sub

    Private Sub PopulateContractorLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal contName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        'ddlLookup.Items.Add(New ListItem(contName, ddlLookup.DataValueField))
        objBL.getLookUpList(spName, lstLookup)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem(contName, ddlLookup.DataValueField))
            ddlLookup.DataBind()
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            'ddlLookup.SelectedValue = ""

        End If

    End Sub

    ''Get Lookup values for Contractors
    Private Sub GetContractorLookup()
        For i As Integer = 0 To grdTempFaultBasket.Rows.Count - 1
            PopulateLookup(DirectCast(grdTempFaultBasket.Rows(i).Cells(4).FindControl("ddlContractor"), DropDownList), SprocNameConstants.getContractorLookup)
        Next

    End Sub

    Private Sub GetContractorLookup(ByVal contractorName As String)
        For i As Integer = 0 To grdTempFaultBasket.Rows.Count - 1
            PopulateContractorLookup(DirectCast(grdTempFaultBasket.Rows(i).Cells(4).FindControl("ddlContractor"), DropDownList), SprocNameConstants.getContractorLookup, contractorName)
        Next

    End Sub
#End Region

#Region "Get Fault Basket Data for Update"

    Private Sub PopulateFaultBasketUpdateDate(ByVal faultLogId As Integer)
        Try
            Dim faultMngr As FaultManager = New FaultManager()
            Dim faultDS As New DataSet()

            faultMngr.GetFaultBasketUpdate(faultLogId, faultDS)

            If faultDS.Tables(0).Rows.Count > 0 Then
                populateControls(faultDS)
            Else
                Response.Redirect("~/error.aspx")
            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Populate Controls"
    Private Sub populateControls(ByRef faultDS As DataSet)
        Try
            grdTempFaultBasket.DataSource = faultDS
            grdTempFaultBasket.DataBind()
            lblTenantName.Text = faultDS.Tables(0).Rows(0)("FirstName").ToString() & " " & faultDS.Tables(0).Rows(0)("LastName").ToString()
            lblAddress.Text = faultDS.Tables(0).Rows(0)("HouseNumber").ToString() & " " & faultDS.Tables(0).Rows(0)("Address1").ToString()
            lblTelephone.Text = faultDS.Tables(0).Rows(0)("TEL").ToString()
            lblMobile.Text = faultDS.Tables(0).Rows(0)("Mobile").ToString()
            lblEmail.Text = faultDS.Tables(0).Rows(0)("EMAIL").ToString()
            lblTown.Text = faultDS.Tables(0).Rows(0)("TownCity").ToString()
            lblCountry.Text = faultDS.Tables(0).Rows(0)("County").ToString() & " " & faultDS.Tables(0).Rows(0)("PostCode").ToString()

            txtPreferredContactTime.Text = faultDS.Tables(0).Rows(0)("PreferredContactDetails").ToString()
            txtReason.Text = faultDS.Tables(0).Rows(0)("CancelReason").ToString()

            If (CType(faultDS.Tables(0).Rows(0)("IamHappy").ToString(), Boolean)) Then
                Me.CheckBox1.Checked = True
            Else
                Me.CheckBox1.Checked = False
            End If


            If faultDS.Tables(0).Rows(0)("Status") = "Works Completed" Or faultDS.Tables(0).Rows(0)("Status") = "Post Inspected" Or faultDS.Tables(0).Rows(0)("Status") = "Invoiced" Or faultDS.Tables(0).Rows(0)("Status") = "Cancelled Fault" Then
                Me.chkCancelFaults.Enabled = False
                Me.txtReason.Enabled = False
            End If

            Dim custHeadBO As CustomerHeaderBO = New CustomerHeaderBO

            custHeadBO.CustomerID = faultDS.Tables(0).Rows(0)("CustomerID").ToString()
            custHeadBO.UserMsg = "Update Mode"

            Session("custHeadBO") = custHeadBO
            Session("faultLogId") = faultDS.Tables(0).Rows(0)("FaultLogId").ToString()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Update Contractor For fault log"

    Private Sub UpdateFaultLogContractor(ByVal userId As Integer)

        Dim faultMngr As FaultManager = New FaultManager()
        Dim conDDL As DropDownList = New DropDownList()
        Dim notes As String = Me.txtPreferredContactTime.Text.ToString()
        Dim mail As Boolean
        If (Me.CheckBox1.Checked = True) Then
            mail = 1

        Else
            mail = 0

        End If

        'Dim row As GridViewRow
        Dim url As String = ""
        Dim faultLogId As Integer

        Try
            ' For Each row In grdTempFaultBasket.Rows
            ' conDDL = row.FindControl("ddlContractor")
            ' Next
            conDDL = CType(grdTempFaultBasket.Rows(0).Cells(4).FindControl("ddlContractor"), DropDownList)

            faultLogId = CType((DirectCast(grdTempFaultBasket.Rows(0).Cells(5).FindControl("lblTempFaultId"), Label).Text), Integer)
            'faultMngr.UpdateFaultLogContractor(faultLogId, CType(conDDL.SelectedItem.Value, Integer), notes, mail)
            If (chkCancelFaults.Checked = True) Then
                UpdateCancelGrid(userId)
            End If
            url = "jobsheet_summary_update.aspx?faultlogid=" & faultLogId.ToString()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

        If conDDL.SelectedItem.Value = "" Then
            Response.Redirect(url)
        Else
            Try
                faultMngr.UpdateFaultLogContractor(faultLogId, CType(conDDL.SelectedItem.Value, Integer), notes, mail)
            Catch ex As Exception
                Response.Redirect("~/error.aspx")
            End Try
            Response.Redirect(url)
        End If


    End Sub

#End Region

#Region "Get User Id "
    Private Function GetUserId()
        ' User Id will come from Login 
        'Dim userId As Integer = 143
        Dim userId As Integer = ASPSession("USERID")

        Return userId
    End Function
#End Region

#End Region



End Class