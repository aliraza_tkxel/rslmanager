Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class PreInspection_Report
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage
#Region "Events"

#Region "Page Init "

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If
    End Sub

#End Region

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            rdbApprovedNo.Attributes.Add("onclick", "javascript:alert('The Repair Will Not Be Progressed Any Further');")
            If Not IsPostBack Then

                ViewState(ApplicationConstants.PreInspectionSortOrderViewState) = ApplicationConstants.PreInspectionDESSortOrder

                GVPreInspectionReport.PageIndex = ApplicationConstants.PreInspectionInitialPageIndex

                GVPreInspectionReport.VirtualItemCount = GetPreInspectionRowCount()
                'GetPreInspectionTeamLookUp()
                'Me.SavePagingAttrib(ApplicationConstants.ReportedFaultInitialPageIndex, ApplicationConstants.PreInspectionResultPerPage, ApplicationConstants.PreInspectionSortByViewState, ApplicationConstants.PreInspectionSortOrderViewState)
                GetPreInspectionReport()
            End If
        Catch ex As Exception

            Response.Redirect("~/error.aspx")
        End Try



    End Sub

#End Region

#Region "btn Pre Inspect Click "

    Protected Sub btnPreInspect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            HideEmptyRows()
            lblSuccessMessage.Text = ""
            updatePanel_successfullySaved.Update()
            lblErrorMsg.Text = ""
            'Reset()
            ClearPreinspectionPopupFields()
            GetPreInspectionTeamLookUp()
            GetContractorLookUpVallues()

            Dim btn As Button = DirectCast(sender, Button)
            Dim strArray As String() = btn.CommandArgument.Split(",")            

            PopulatePreinspectionPopup(strArray)
            pnlPreInspectionPopup.Update()

            mdlPreInspectPopup.Show()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "To Clear the Pre Inspection Contorls"
    Public Sub ClearPreinspectionPopupFields()

        Me.ddlTeam.SelectedValue = String.Empty
        Me.ddlOperative.Items.Clear()
        Me.ddlOperative.SelectedValue = String.Empty
        Me.txtInspect.Text = String.Empty
        Me.txtDueDate.Text = String.Empty
        Me.txtNotes.Text = String.Empty
        'Me.ddlContractor.SelectedValue = String.Empty
        'Me.rdbApprovedYes.Checked = True
        'Me.rdbApprovedNo.Checked = False
        'Me.rdbRechargeNo.Checked = False
        'Me.rdbRechargeYes.Checked = False


        Me.ddlTeam.Enabled = True
        Me.ddlOperative.Enabled = True
        Me.txtInspect.Enabled = True
        Me.txtDueDate.Enabled = True
        Me.txtNotes.Enabled = True
        Me.ddlContractor.Enabled = True
        Me.rdbApprovedYes.Enabled = True
        Me.rdbApprovedNo.Enabled = True
        Me.btnSaveChanges.Enabled = True
        Me.txtCost.Enabled = True
        Me.rdbRechargeYes.Enabled = True
        Me.rdbRechargeNo.Enabled = True

    End Sub

#End Region

#Region "Populate Preinspection Popup"
    Public Sub PopulatePreinspectionPopup(ByVal commandArgmentArray As Array)

        Dim recharge As Boolean = CType(commandArgmentArray(0), Boolean)
        Dim tempFaultID As Integer = commandArgmentArray(2).ToString()
        Dim newRecharge As String = commandArgmentArray(4)
        Dim approved As String = commandArgmentArray(5)
        Dim orgId As String

        Me.lblFaultLogID.Text = tempFaultID

        'If no new recharge value has been set the default recharge value will be used
        If newRecharge = "" Then
            If (recharge) Then
                rdbRechargeYes.Checked = True
                rdbRechargeNo.Checked = False
            Else
                rdbRechargeNo.Checked = True
                rdbRechargeYes.Checked = False
            End If
        Else
            Dim newRechargeValue As Boolean = CType(commandArgmentArray(4), Boolean)

            If (newRechargeValue) Then
                rdbRechargeYes.Checked = True
                rdbRechargeNo.Checked = False
            Else
                rdbRechargeNo.Checked = True
                rdbRechargeYes.Checked = False
            End If
        End If


        Dim PIDS As DataSet = New DataSet()
        Dim faultBO As FaultBO = New FaultBO()
        Dim objFaultBL As FaultManager = New FaultManager()


        Try
            faultBO.FaultID = tempFaultID
            objFaultBL.GetPreinspectionPopupData(PIDS, faultBO)

            If (PIDS.Tables(0).Rows.Count > 0) Then

                Me.ddlTeam.SelectedValue = PIDS.Tables(0).Rows(0)("Team").ToString()
                Me.ddlOperative.Items.Clear()
                Me.ddlOperative.Items.Add(New ListItem(PIDS.Tables(0).Rows(0)("UserName").ToString(), PIDS.Tables(0).Rows(0)("Operative").ToString()))
                Me.txtCost.Text = CType(PIDS.Tables(0).Rows(0)("NETCOST"), Decimal).ToString("0.00")
                Me.txtInspect.Text = UtilityFunctions.FormatDate(PIDS.Tables(0).Rows(0)("INSPECTIONDATE").ToString())
                Me.txtDueDate.Text = UtilityFunctions.FormatDate(PIDS.Tables(0).Rows(0)("DUEDATE").ToString())
                Me.txtNotes.Text = PIDS.Tables(0).Rows(0)("NOTES").ToString()
                Me.ddlContractor.SelectedValue = PIDS.Tables(0).Rows(0)("ORGID").ToString()


                If approved = "" Then
                    rdbApprovedYes.Checked = True
                    rdbApprovedNo.Checked = False
                Else
                    Dim approvedValue As Boolean = CType(commandArgmentArray(5), Boolean)
                    orgId = PIDS.Tables(0).Rows(0)("ORGID").ToString()

                    If orgId <> "" And approvedValue = True Then
                        Me.rdbApprovedYes.Checked = True
                        Me.rdbApprovedNo.Checked = False
                        Me.DisablePreinspectionPopupFields()
                    ElseIf orgId = "" And approvedValue = False Then
                        Me.rdbApprovedNo.Checked = True
                        Me.rdbApprovedYes.Checked = False
                        Me.DisablePreinspectionPopupFields()
                    ElseIf orgId = Nothing And approvedValue = Nothing Then
                        Me.rdbApprovedYes.Checked = True
                        Me.rdbApprovedNo.Checked = False
                    End If
                    
                End If

            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub

#End Region

#Region "Disable Preinspection Popup Fields"


    Public Sub DisablePreinspectionPopupFields()
        'Me.lblValidate.Enabled = False
        Me.ddlTeam.Enabled = False
        Me.ddlOperative.Enabled = False
        Me.txtInspect.Enabled = False
        Me.txtDueDate.Enabled = False
        Me.txtNotes.Enabled = False
        Me.ddlContractor.Enabled = False
        Me.rdbApprovedYes.Enabled = False
        Me.rdbApprovedNo.Enabled = False
        Me.btnSaveChanges.Enabled = False
        Me.rdbRechargeNo.Enabled = False
        Me.rdbRechargeYes.Enabled = False
        Me.txtCost.Enabled = False
    End Sub

#End Region

#Region "Hide Empty Rows "

    Public Sub HideEmptyRows()
        Dim ds As DataSet = New DataSet()
        ds = ViewState.Item(ApplicationConstants.PreInspectionDS)
        GVPreInspectionReport.DataSource = ds
        GVPreInspectionReport.DataBind()
    End Sub
#End Region

#Region "ddl Team Selected Index Changed"

    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If (ddlTeam.SelectedValue <> "") Then

            lblErrorMsg.Text = ""
            updatePanel_ErrorMessage.Update()

        End If

        Try
            If (ddlTeam.SelectedIndex <> 0) Then
                Me.GetOperativeLookUpValues(ddlTeam.SelectedValue.ToString(), "@TeamId")
            Else
                Me.ddlOperative.Items.Clear()
                Me.ddlOperative.Items.Add(New ListItem("Please select", ""))

            End If
        Catch ex As Exception

            Response.Redirect("~/error.aspx")
        End Try


    End Sub
#End Region

#Region "ddl Operative Selected Index Changed "

    Protected Sub ddlOperative_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If (ddlOperative.SelectedValue <> "") Then

            lblErrorMsg.Text = ""
            updatePanel_ErrorMessage.Update()

        End If

        If ddlOperative.SelectedValue = "" Then
            ddlTeam.SelectedValue = ""
            UpdatePanel_team.Update()
        End If
    End Sub

#End Region

#Region "ddl Contractor Selected Index Changed "

    Protected Sub ddlContractor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If (ddlContractor.SelectedValue <> "") Then
            lblErrorMsg.Text = ""
            updatePanel_ErrorMessage.Update()

        End If

    End Sub
#End Region

#Region "txt Inspect Text Changed "
    Protected Sub txtInspect_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        lblErrorMsg.Text = ""
        updatePanel_ErrorMessage.Update()

    End Sub
#End Region

#Region "txt Due Date Text Changed "

    Protected Sub txtDueDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        lblErrorMsg.Text = ""
        updatePanel_ErrorMessage.Update()

    End Sub

#End Region

#Region "btn Save Changes Click "

    Protected Sub btnSaveChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        ValidatePreInspectionPopup()
        'pnlPreInspectionPopup.Update()
        GetPreInspectionReport()
        updatePanel_PreInspection_Report.Update()

    End Sub

#End Region

#Region "GV Pre Inspection Report Page Index Changing "
    Protected Sub GVPreInspectionReport_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

        Try
            lblSuccessMessage.Text = ""
            updatePanel_successfullySaved.Update()
            'assiging new page index to the gridview
            GVPreInspectionReport.PageIndex = e.NewPageIndex
            'calling function that will store and update current result summary being shown in the footer of
            'gridview
            Me.StoreResultSummary()
            GetPreInspectionReport()
            updatePanel_PreInspection_Report.Update()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")

        End Try



    End Sub
#End Region

#Region "GV Pre Inspection Report Sorting "

    'Protected Sub GVPreInspectionReport_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
    '    'storing the column name by which new sort is requested
    '    ViewState(ApplicationConstants.PreInspectionSortByViewState) = e.SortExpression
    '    'setting pageindex of gridview equal to zero for new search
    '    GVPreInspectionReport.PageIndex = ApplicationConstants.PreInspectionInitialPageIndex

    '    'setting new sorting order
    '    If DirectCast(ViewState(ApplicationConstants.PreInspectionSortOrderViewState), String) = ApplicationConstants.PreInspectionDESSortOrder Then
    '        ViewState(ApplicationConstants.PreInspectionSortOrderViewState) = ApplicationConstants.PreInspectionASCSortOrder
    '    Else
    '        ViewState(ApplicationConstants.PreInspectionSortOrderViewState) = ApplicationConstants.PreInspectionDESSortOrder
    '    End If

    '    GetPreInspectionReport()
    '    updatePanel_PreInspection_Report.Update()

    'End Sub

#End Region

#Region "rdb Approved No Checked Changed "


    Protected Sub rdbApprovedNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.ddlContractor.SelectedValue = ""
        ddlContractor.Enabled = False
        updatePanel_ContractorDropDown.Update()

    End Sub

    Protected Sub rdbApprovedYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        ddlContractor.Enabled = True
        updatePanel_ContractorDropDown.Update()

    End Sub

#End Region


#End Region

#Region "Methods "

#Region "Pre Inspection RowCount"

    Public Function GetPreInspectionRowCount() As Integer

        Dim PreInsBO As PreInspectionBO = New PreInspectionBO()

        Try
            If ViewState(ApplicationConstants.PreInspectionSortByViewState) <> Nothing Then
                PreInsBO.SortBy = CType(ViewState(ApplicationConstants.PreInspectionSortByViewState), String)
            Else
                'if thr is no sortby column in viewstate thn use FaultLogID as a default
                PreInsBO.SortBy = ApplicationConstants.PreInspectionSortBy
            End If

            'setting the range of results to be fetched from db (pageindex*pagesize)
            PreInsBO.PageIndex = ApplicationConstants.PreInspectionInitialPageIndex
            PreInsBO.RowCount = ApplicationConstants.PreInspectionResultPerPage

            If Request.QueryString("EmployeeFilter") = 1 Then
                PreInsBO.UserId = ASPSession("USERID")
            End If
            'reteriving sort order from viewstate
            PreInsBO.SortOrder = DirectCast(ViewState(ApplicationConstants.PreInspectionSortOrderViewState), String)
            Dim preInsMngr As ContractorPortalManager = New ContractorPortalManager()
            Return preInsMngr.GetPreInspectRowCount(PreInsBO)

        Catch ex As Exception
            Response.Redirect("error.aspx")
        End Try

    End Function
#End Region

#Region "Get Pre Inspection Report "
    Public Sub GetPreInspectionReport()

        Try
            Dim PreInspBO As PreInspectionBO = New PreInspectionBO()

            Dim PreInspecDS As DataSet = New DataSet()
            GetPreInspectionDataPage(GVPreInspectionReport.PageIndex, ApplicationConstants.PreInspectionResultPerPage, GVPreInspectionReport.OrderBy, PreInspecDS, PreInspBO)
            Me.StoreResultSummary()
            GVPreInspectionReport.DataSource = PreInspecDS
            GVPreInspectionReport.DataBind()

            ViewState.Add(ApplicationConstants.PreInspectionDS, PreInspecDS)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")

        End Try


    End Sub
#End Region

#Region "Store Result Summary"


    Public Sub StoreResultSummary()
        Dim row As GridViewRow = GVPreInspectionReport.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVPreInspectionReport.PageIndex + 1 = GVPreInspectionReport.PageCount Then
            strResultSummary = "Result " & ((GVPreInspectionReport.PageIndex * GVPreInspectionReport.PageSize) + 1) & " to " & GVPreInspectionReport.VirtualItemCount & " of " & GVPreInspectionReport.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVPreInspectionReport.PageIndex * GVPreInspectionReport.PageSize) + 1) & " to " & ((GVPreInspectionReport.PageIndex + 1) * GVPreInspectionReport.PageSize) & " of " & GVPreInspectionReport.VirtualItemCount
        End If
        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.PreInspectionResultSummary) = strResultSummary
    End Sub

#End Region

#Region "Pre Inspection DataPage"

    Public Sub GetPreInspectionDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef PrIDS As DataSet, ByVal PreInsBO As PreInspectionBO)

        'reteriving sortby column name from viewstate
        If ViewState(ApplicationConstants.PreInspectionSortByViewState) <> Nothing Then
            PreInsBO.SortBy = CType(ViewState(ApplicationConstants.PreInspectionSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use FaultLogID as a default
            PreInsBO.SortBy = ApplicationConstants.PreInspectionSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        PreInsBO.PageIndex = pageIndex
        PreInsBO.RowCount = pageSize

        If Request.QueryString("EmployeeFilter") = 1 Then
            PreInsBO.UserId = ASPSession("USERID")
        End If


        'reteriving sort order from viewstate
        PreInsBO.SortOrder = DirectCast(ViewState(ApplicationConstants.PreInspectionSortOrderViewState), String)
        Dim preInsMngr As ContractorPortalManager = New ContractorPortalManager()
        'passing reference of dataset and passing enqryLogSearchBO to EnquiryLogData function
        'of EnquiryManager
        preInsMngr.PreInspectionReportDate(PrIDS, PreInsBO)
        'storing number of rows in the result
        ViewState("NumberofRows") = PrIDS.Tables(0).Rows.Count

        ''Check for exception 
        Dim exceptionFlag As Boolean = PreInsBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("error.aspx")
        End If

    End Sub

#End Region

#Region "Save Inspection Record"


    Private Sub SaveInspectionRecord()

        Try

            Dim piBO As PreInspectBO = New PreInspectBO()
            'fill piBO attribute from inspection data
            If Me.ddlOperative.SelectedValue <> String.Empty Then
                piBO.UserId = CType(ddlOperative.SelectedValue, Integer)
            End If

            If rdbRechargeYes.Checked Then
                piBO.Recharge = 1
            Else
                piBO.Recharge = 0
            End If

            piBO.NetCost = CType(txtCost.Text, Double)
            piBO.Notes = txtNotes.Text

            If Me.txtInspect.Text <> String.Empty Then
                piBO.InspectionDate = UtilityFunctions.stringToDate(txtInspect.Text)
            End If
            If Me.txtDueDate.Text <> String.Empty Then
                piBO.DueDate = UtilityFunctions.stringToDate(txtDueDate.Text)
            End If

            If Me.ddlContractor.SelectedValue <> String.Empty And Me.ddlContractor.Enabled = True Then
                piBO.OrgId = CType(ddlContractor.SelectedValue, Integer)
            End If

            If Me.rdbApprovedYes.Checked = True Then
                piBO.Approved = 1
            Else
                piBO.Approved = 0

            End If

            piBO.FaultLogId = CType(lblFaultLogID.Text, Integer)
            piBO.ActionUserId = GetUserId()
            Dim piBL As FaultManager = New FaultManager()
            piBL.UpdatePreInspectionReportDataBL(piBO)
            GVPreInspectionReport.VirtualItemCount = GetPreInspectionRowCount() ' To Get the number of row count at each page load. 
            updatePanel_PreInspection_Report.Update()

        Catch ex As Exception

            Response.Redirect("~/error.aspx")
        End Try
       
    End Sub
#End Region

#Region "Contractor LookUpVallues"


    Private Sub GetContractorLookUpVallues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getContractorLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlContractor.Items.Clear()
            Me.ddlContractor.DataSource = lstLookUp
            Me.ddlContractor.DataValueField = "LookUpValue"
            Me.ddlContractor.DataTextField = "LookUpName"
            Me.ddlContractor.Items.Add(New ListItem("Please select", ""))
            Me.ddlContractor.DataBind()
        End If

    End Sub

#End Region

#Region "Pre Inspection Team LookUp"


    Public Sub GetPreInspectionTeamLookUp()

        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.GetPreInspectionTeamLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlTeam.Items.Clear()
            Me.ddlTeam.DataSource = lstLookUp
            Me.ddlTeam.DataValueField = "LookUpValue"
            Me.ddlTeam.DataTextField = "LookUpName"
            Me.ddlTeam.Items.Add(New ListItem("Please select", ""))
            Me.ddlTeam.DataBind()
        End If

        Me.ddlOperative.Items.Clear()
        Me.ddlOperative.Items.Add(New ListItem("Please select", ""))

    End Sub

#End Region

#Region "Operative LookUp Values"

    Private Sub GetOperativeLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlOperative, SprocNameConstants.GetUserLookup, inParam, inParamName)
    End Sub

#End Region

#Region "Populate Lookup"

    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
            UpdatePanel_operative.Update()
        End If
    End Sub

#End Region

#Region "Validate Pre InspectionPopup"

    Public Sub ValidatePreInspectionPopup()

        Try

            If (ddlTeam.SelectedValue = String.Empty Or ddlTeam.SelectedValue = "Please select") Then
                lblErrorMsg.Text = "Please Select Team"
                updatePanel_ErrorMessage.Update()

            ElseIf (ddlOperative.SelectedValue = "") Then

                lblErrorMsg.Text = "Please Select Surveyor"
                updatePanel_ErrorMessage.Update()

            ElseIf (txtInspect.Text = "") Then

                lblErrorMsg.Text = "Please Select Inspection Date"
                updatePanel_ErrorMessage.Update()

            ElseIf (rdbApprovedYes.Checked = True And ddlContractor.SelectedValue = "") Then
                lblErrorMsg.Text = "Please Choose Contractor or Uncheck Approved"
                updatePanel_ErrorMessage.Update()
            Else
                SaveInspectionRecord()
                GetPreInspectionReport()
                lblSuccessMessage.Text = "PreInspection Approved Successfully"
                updatePanel_successfullySaved.Update()
                updatePanel_PreInspection_Report.Update()
                pnlPreInspectionPopup.Update()

            End If
        Catch ex As Exception

            Response.Redirect("~/error.aspx")
        End Try
        
    End Sub

#End Region

#Region "Reset PreInspection Popup"

    Public Sub Reset()

        If (ddlTeam.SelectedIndex <> 0) Then
            ddlTeam.SelectedValue = ""
            ddlContractor.SelectedValue = ""
            ddlOperative.SelectedValue = ""
        End If

        txtInspect.Text = ""
        txtNotes.Text = ""
        rdbRechargeNo.Checked = False
        rdbRechargeYes.Checked = False
        rdbApprovedNo.Checked = False
        rdbApprovedYes.Checked = True
        ddlContractor.Enabled = True
        updatePanel_YesNO.Update()

    End Sub
#End Region

#Region "Bind PreInspection Grid"

    Private Sub BindPreInspectionGrid()
        Try
            Dim PrINDS As DataSet = New DataSet()
            Dim PreIBO As PreInspectionBO = New PreInspectionBO()
            GetPreInspectionDataPage(GVPreInspectionReport.PageIndex, ApplicationConstants.PreInspectionResultPerPage, GVPreInspectionReport.OrderBy, PrINDS, PreIBO)
            GVPreInspectionReport.DataSource = PrINDS
            GVPreInspectionReport.DataBind()
            updatePanel_PreInspection_Report.Update()

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "Save PagingAttrib"

    Private Sub SavePagingAttrib(ByRef pageIndex As Integer, ByRef resultPerPage As Integer, ByVal sortBy As String, ByVal sortOrder As String)
        Try
            'Set the page index
            ViewState.Add(ApplicationConstants.PreInspectionInitialPageIndex, pageIndex)

            'Set the page size
            ViewState.Add(ApplicationConstants.PreInspectionResultPerPage, resultPerPage)

            'Set the Sort by
            ViewState.Add(ApplicationConstants.PreInspectionSortBy, sortBy)

            'Set the sort order (Ascending /Descending)
            ViewState.Add(ApplicationConstants.PreInspectionSortOrderViewState, sortOrder)
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try
    End Sub
#End Region

#Region "Get Org Id "
    Private Function GetOrgId()
        Return ASPSession("ORGID")
        'Return 2
    End Function
#End Region
#Region "Get User Id "
    Private Function GetUserId()
        ' User Id will come from Login 
        'Dim userId As Integer = 143
        Dim userId As Integer = ASPSession("USERID")

        Return userId
    End Function
#End Region

#End Region


    
End Class



