<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" MasterPageFile="~/master pages/EnquiryManagement.Master"
    EnableEventValidation="false" CodeBehind="reported_faults.aspx.vb" Inherits="tenantsonline.reported_faults"
    Title="Reported Faults" MaintainScrollPositionOnPostback="true" EnableViewState="true" %>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">
    <%--<script runat="server" > ddlStatus.ClientID </script>
    --%>
    <script runat="server">
        Function MakeCommandString(ByVal recharge As String, ByVal netCost As String, ByVal faultLogId As String, ByVal dueDate As String)
        
            Return recharge & "," & netCost & "," & faultLogId & "," & dueDate
        
        End Function
    
    
    </script>
    <script type="text/javascript" language="javascript">

        function ClearCtrl() {
            document.getElementById("<%= txtJS.ClientID %>").value = "";
            document.getElementById("<%= txtDateFrom.ClientID %>").value = "";
            document.getElementById("<%= txtDateTo.ClientID %>").value = "";
            document.getElementById("<%= txtLastName.ClientID %>").value = "";
            document.getElementById("<%= txtText.ClientID %>").value = "";

            var parent = document.getElementById('<%= chkListFaultStatus.ClientID%>');
            var checkBoxes = parent.getElementsByTagName("input");
            for (i = 0; i < checkBoxes.length; i++) {
                checkBoxes[i].checked = false;
            }

        }
    
    </script>
    <div style="text-align: left; vertical-align: top;">
        <table style="width: 100%; height: 100%" cellpadding="0">
            <tr>
                <td style="width: 77px;">
                    <asp:Label ID="lblErrorMessage" runat="server" CssClass="error_message_label" Width="224px"></asp:Label>
                </td>
                <td style="width: 100px;">
                    &nbsp;<cc2:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                    </cc2:ToolkitScriptManager>
                    <cc2:CalendarExtender ID="CalendarExtender_dob" runat="server" Format="dd/MM/yyyy"
                        PopupButtonID="btnCalander" TargetControlID="txtDateFrom">
                    </cc2:CalendarExtender>
                    <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnCalander1"
                        TargetControlID="txtDateTo">
                    </cc2:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td rowspan="2" style="width: 100px" valign="top">
                    <table style="width: 15%; border-right: tan thin dotted; border-top: tan thin dotted;
                        border-left: tan thin dotted; border-bottom: tan thin dotted;">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblSearch" runat="server" Font-Bold="True" Font-Names="Arial" Text="Search:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblJS" runat="server" Text="JS" Font-Names="Arial" Font-Size="Small"></asp:Label>
                            </td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtJS" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="10"
                                    Width="150px" AutoPostBack="false"></asp:TextBox><br />
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                    TargetControlID="txtJS">
                                </cc2:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblLastName" runat="server" Text="Last Name" Font-Names="Arial" Font-Size="Small"
                                    Width="64px"></asp:Label>
                            </td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtLastName" runat="server" Font-Names="Arial" Font-Size="Small"
                                    MaxLength="50" Width="150px" AutoPostBack="false"></asp:TextBox><br />
                                <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtLastName"
                                    FilterType="Custom" FilterMode="invalidChars" InvalidChars="~!@#$%^&*0123456789">
                                </cc2:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">
                                <asp:Label ID="lblDateFrom" runat="server" Text="Date From" Font-Names="Arial" Font-Size="Small"></asp:Label>
                            </td>
                            <td style="width: 145px">
                                <asp:TextBox ID="txtDateFrom" runat="server" Font-Names="Arial" Font-Size="Small"
                                    Width="112px" MaxLength="10" AutoPostBack="false"></asp:TextBox>
                                <asp:ImageButton ID="btnCalander" runat="server" CausesValidation="False" ImageUrl="~/images/buttons/Calendar_button.png" />&nbsp;
                            </td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="lblDateTo" runat="server" Text="Date To" Font-Names="Arial" Font-Size="Small"></asp:Label>
                </td>
                <td style="width: 145px">
                    <asp:TextBox ID="txtDateTo" runat="server" Font-Names="Arial" Font-Size="Small" Width="112px"
                        MaxLength="10" AutoPostBack="false"></asp:TextBox>
                    <asp:ImageButton ID="btnCalander1" runat="server" CausesValidation="False" ImageUrl="~/images/buttons/Calendar_button.png" />&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="lblStatus" runat="server" Text="Status" Font-Names="Arial" Font-Size="Small"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 345px">
                    <asp:CheckBoxList ID="chkListFaultStatus" runat="server" Font-Names="Arial" Font-Size="Small"
                        RepeatColumns="1" RepeatDirection="Horizontal">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="lblText" runat="server" Text="Detail" Font-Names="Arial" Font-Size="Small"></asp:Label>
                </td>
                <td style="width: 145px">
                    <asp:TextBox ID="txtText" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="255"
                        Width="150px" AutoPostBack="false"></asp:TextBox><br />
                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtText"
                        FilterType="Custom, LowercaseLetters, UppercaseLetters" FilterMode="invalidChars"
                        InvalidChars="~!@#$%^&*0123456789">
                    </cc2:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    &nbsp;
                </td>
                <td style="width: 145px">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btn" runat="server" Text="Search" /><asp:Button ID="btnClear" runat="server"
                                Style="position: static" Text="Clear" OnClientClick="ClearCtrl();return false;"
                                OnClick="btnClear_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    &nbsp;
                </td>
            </tr>
        </table>
        </td>
        <td valign="top">
            <table style="width: 100%; height: 100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 100px; height: 21px;">
                        <asp:Label ID="lblRFHeading" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Black"
                            Text="Reported Faults" Width="128px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; height: 21px">
                        <asp:UpdatePanel ID="updatePanel_SavedSuccessfully" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="lblSavedSuccessfully" runat="server" CssClass="info_message_label"
                                    Width="584px"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td style="height: 376px" valign="top">
                        <asp:UpdatePanel ID="updatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <cc1:PagingGridView ID="RFLog" runat="server" AllowPaging="True" AllowSorting="True"
                                    AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                                    BorderStyle="Dotted" BorderWidth="1px" CellPadding="2" EmptyDataText="No record exists"
                                    Font-Names="Arial" Font-Size="Small" ForeColor="Black" GridLines="None" VirtualItemCount="-1"
                                    Width="100%" OnSorting="RFLog_Sorting" OnPageIndexChanging="RFLog_PageIndexChanging"
                                    PageSize="15">
                                    <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                    <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                                    <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="JS Num" SortExpression="FL_FAULT_LOG.FAULTLOGID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblJSNumber" runat="server" Text='<%# Bind("JS_Number") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" />
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="FL_FAULT_LOG.SubmitDate" HeaderText="Submit Date"
                                            InsertVisible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblCreationDate" runat="server" CssClass="cellData" Text='<%# Eval("SubmitDate").ToString() %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Appointment Date" SortExpression="FL_FAULT_PREINSPECTIONINFO.InspectionDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblApptDate" runat="server" Text='<%# Bind("AppointmentDate") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" />
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="C__CUSTOMER.LASTNAME" HeaderText="Customer" InsertVisible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblCustomer" runat="server" CssClass="cellData" Text='<%#  Eval("LASTNAME").ToString() %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="P__PROPERTY.HOUSENUMBER" HeaderText="Property Address" InsertVisible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblPropertyAddress" runat="server" CssClass="cellData" Text='<%#  Eval("PropertyAddress").ToString() %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Wrap="false" />
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="FL_AREA.AreaName" HeaderText="Area/Item" InsertVisible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblAreaItem" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatAreaElement( Eval("AreaName").ToString() , Eval("ElementName").ToString() ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Detail" SortExpression="FL_FAULT.Description" InsertVisible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblDetails" runat="server" CssClass="cellData" Text='<%# Eval("Description") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Recharge?" SortExpression="FL_FAULT_LOG.Recharge"
                                            InsertVisible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblRecharge" runat="server" CssClass="cellData" Text='<%# Eval("Recharge").ToString()  %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty" SortExpression="FL_FAULT_LOG.Quantity" InsertVisible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblQty" runat="server" CssClass="cellData" Text='<%# Eval("Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Underline="True" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField SortExpression="FL_FAULT_PRIORITY.ResponseTime" HeaderText="Priority">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblPriority" runat="server" CssClass="cellData" Text='<%# Eval("TimeFrame") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" SortExpression="FL_FAULT_STATUS.DESCRIPTION">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblStatus" runat="server" CssClass="cellData" Text='<%# Eval("FaultStatus") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--                  <asp:TemplateField>
                                        <itemtemplate>
<asp:Button id="btnPreInspect" onclick="btnPreInspect_Click" runat="server" Text="Pre Inspect" UseSubmitBehavior="False" CommandArgument='<%# MakeCommandString(Eval("Recharge").ToString(), Eval("NetCost").ToString(),Eval("faultLogID").ToString(),Eval("DueDate").ToString()) %>'></asp:Button>&nbsp; 
</itemtemplate>
                                    </asp:TemplateField>--%>
                                        <asp:TemplateField SortExpression="S_ORGANISATION.NAME" HeaderText="Contractor">
                                            <ItemTemplate>
                                                <asp:Label ID="RFLblContractor" runat="server" CssClass="cellData" Text='<%# Eval("NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <EmptyDataRowStyle Wrap="False" />
                                </cc1:PagingGridView>
                                <asp:UpdateProgress ID="UpdateProgress_enquiries" runat="server" DisplayAfter="10">
                                    <ProgressTemplate>
                                        <asp:Panel ID="pnlProgress" runat="server" BackColor="Silver" Font-Names="Arial"
                                            Font-Size="Small" Height="40px" HorizontalAlign="Center" Width="221px">
                                            <asp:Image ID="imgProgressBar" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" /><br />
                                            Loading ...
                                        </asp:Panel>
                                        &nbsp;
                                        <cc2:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender_progress" runat="server"
                                            HorizontalSide="Center" TargetControlID="pnlProgress" VerticalSide="Middle">
                                        </cc2:AlwaysVisibleControlExtender>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%--Update Panel, GridView for Export--%>
                        <asp:UpdatePanel ID="updatePanelExport" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="RFLogExport" runat="server" Visible="false">
                                </asp:GridView>
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="10">
                                    <ProgressTemplate>
                                        <asp:Panel ID="pnlProgressExport" runat="server" BackColor="Silver" Font-Names="Arial"
                                            Font-Size="Small" Height="40px" HorizontalAlign="Center" Width="221px">
                                            <asp:Image ID="imgProgressBarExport" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" /><br />
                                            Loading ...
                                        </asp:Panel>
                                        &nbsp;
                                        <cc2:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender_progressExp" runat="server"
                                            HorizontalSide="Center" TargetControlID="pnlProgressExport" VerticalSide="Middle">
                                        </cc2:AlwaysVisibleControlExtender>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%--End of comment: Update Panel, GridView for Export--%>
                        <asp:Panel ID="panel_PreInspectionPopup" runat="server" Height="0px" Width="15px"
                            Style="display: none">
                            <asp:UpdatePanel ID="pnlPreInspectionPopup" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table cellpadding="1" cellspacing="1" style="width: 50%; background-color: White;">
                                        <tbody align="left">
                                            <tr>
                                                <td align="left" colspan="11" style="height: 21px; background-color: #c00000">
                                                    &nbsp;
                                                    <asp:Label ID="lblPreInspectionHeading" runat="server" BackColor="#C00000" Font-Bold="True"
                                                        Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Arrange Pre Inspection Appointment"
                                                        Width="66%"></asp:Label>
                                                    <asp:LinkButton ID="lnkBtnPseudo" runat="server"></asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="11" style="height: 21px; background-color: #ffffff">
                                                    <asp:UpdatePanel ID="updatePanel_ErrorMessage" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            &nbsp;<asp:Label ID="lblErrorMsg" runat="server" CssClass="error_message_label" Width="224px"></asp:Label>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px">
                                                </td>
                                                <td align="left" colspan="8" style="width: 152px">
                                                    <asp:Label ID="Label1" runat="server" CssClass="caption" Text="Team: *"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <asp:UpdatePanel ID="UpdatePanel_team" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlTeam" runat="server" Width="229px" AppendDataBoundItems="True"
                                                                AutoPostBack="True" OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            &nbsp;
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px">
                                                </td>
                                                <td align="left" colspan="8" style="width: 152px">
                                                    <asp:Label ID="lblOperative" runat="server" Text="Surveyor: *" CssClass="caption"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <asp:UpdatePanel ID="UpdatePanel_operative" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlOperative" runat="server" Width="229px" AppendDataBoundItems="True"
                                                                AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px">
                                                </td>
                                                <td align="left" colspan="8" style="width: 152px">
                                                    <asp:Label ID="lblInspectionDate" runat="server" Text="Inspection Date: *" Width="136px"
                                                        CssClass="caption"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <asp:TextBox ID="txtInspect" runat="server" Width="203px" Enabled="False"></asp:TextBox>
                                                    <asp:ImageButton ID="btnInspectionCalander" runat="server" CausesValidation="False"
                                                        ImageUrl="~/images/buttons/Calendar_button.png" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px">
                                                </td>
                                                <td align="left" colspan="8" style="width: 152px">
                                                    <asp:Label ID="lblNotes" runat="server" CssClass="caption" Text="Notes:"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <asp:TextBox ID="txtNotes" runat="server" Height="122px" TextMode="MultiLine" Width="223px"></asp:TextBox><br />
                                                    <cc2:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtNotes"
                                                        FilterType="Custom" FilterMode="invalidChars" InvalidChars="~!@#$%^&*">
                                                    </cc2:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px; height: 22px;">
                                                </td>
                                                <td align="left" colspan="8" style="width: 152px; height: 22px;">
                                                    <asp:Label ID="lblRecharge" runat="server" CssClass="caption" Text="Recharge"></asp:Label>
                                                </td>
                                                <td align="center" colspan="1" style="width: 54px; height: 22px;">
                                                    <asp:RadioButton ID="rdbRechargeYes" runat="server" CssClass="caption" GroupName="rdbRecharge"
                                                        Text="Y" Enabled="False" />&nbsp;
                                                </td>
                                                <td align="left" colspan="1" style="width: 124px; height: 22px;">
                                                    <asp:RadioButton ID="rdbRechargeNo" runat="server" CssClass="caption" GroupName="rdbRecharge"
                                                        Text="N" Enabled="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px">
                                                </td>
                                                <td align="left" colspan="8" style="width: 152px">
                                                    <asp:Label ID="lblCost" runat="server" CssClass="caption" Text="Cost(Net)�:" Width="88px"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <asp:TextBox ID="txtCost" runat="server" Width="223px" Enabled="False">0.0</asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px">
                                                </td>
                                                <td align="left" colspan="8" style="width: 152px">
                                                    <asp:Label ID="lblDueDate" runat="server" CssClass="caption" Text="Due Date:"></asp:Label>
                                                </td>
                                                <td align="left" colspan="2">
                                                    <asp:TextBox ID="txtDueDate" runat="server" Width="224px" Enabled="False" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px">
                                                </td>
                                                <td align="left" colspan="8" style="width: 152px">
                                                    <asp:Label ID="lblFaultLogID" runat="server" Visible="False"></asp:Label>
                                                </td>
                                                <td align="right" colspan="1" valign="top" style="width: 54px">
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="caption" Text="Cancel" OnClick="btnCancel_Click" />
                                                </td>
                                                <td align="left" colspan="2" valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:Button ID="btnSaveChanges" runat="server" CssClass="caption" Text="Save Appointment"
                                                                OnClick="btnSaveChanges_Click" Width="144px" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="1" style="width: 11px">
                                                </td>
                                                <td align="left" colspan="10">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <cc2:ModalPopupExtender BackgroundCssClass="modalBackground" CancelControlID="btnCancel"
                                        Drag="True" ID="mdlPreInspectPopup" PopupControlID="panel_PreInspectionPopup"
                                        runat="server" TargetControlID="lblDueDate">
                                    </cc2:ModalPopupExtender>
                                    <cc2:CalendarExtender ID="CalendarExtender_Inspection" runat="server" Format="dd/MM/yyyy"
                                        PopupButtonID="btnInspectionCalander" TargetControlID="txtInspect">
                                    </cc2:CalendarExtender>
                                    &nbsp; &nbsp;
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnBack" runat="server" Text="Back to Finance" />
                        <asp:Button ID="btnExport" runat="server" Text="Export" />
                    </td>
                </tr>
            </table>
        </td>
        </tr> </table>
    </div>
</asp:Content>
