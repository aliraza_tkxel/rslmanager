<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/ContractorPortal.Master" CodeBehind="PreInspection_Report.aspx.vb" Inherits="tenantsonline.PreInspection_Report"%>

<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="page_area">
<script runat="server" >
    Function MakeCommandString(ByVal Recharge As String, ByVal NetCost As String, ByVal faultLogID As String, ByVal DueDate As String, ByVal NewRecharge As String, ByVal Approved As String) As String
        Return Recharge & "," & NetCost & "," & faultLogID & "," & DueDate & "," & NewRecharge & "," & Approved
    End Function
    Function FullName(ByVal fName As String, ByVal lastName As String)
        Return fName & " " & lastName
    End Function
 </script>

    <div style="text-align: left; vertical-align:top;">
         <table style="width: 100%; height: 100%" cellpadding="0">
             <tr>
                 <td style="width: 77px">
                     <cc2:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                     </cc2:ToolkitScriptManager>
                 </td>
                 <td style="width: 100px">
                 </td>
             </tr>
            <tr>
                <td style="width: 77px;">
                    &nbsp;<asp:Label ID="lblPreInspecHeading" runat="server" BackColor="White" Font-Bold="True" Font-Names="Arial"
                                ForeColor="Black" Text="PreInspection Report" Width="184px"></asp:Label></td>
                <td style="width: 100px;">
                    &nbsp; &nbsp;
                </td>
            </tr>
            <tr>
                <td rowspan="1" valign="top" colspan="2">
                <table style="width: 100%; height: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 100px; height: 21px;">
                            &nbsp;<asp:UpdatePanel ID="updatePanel_successfullySaved" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                    <asp:Label ID="lblSuccessMessage" runat="server" CssClass="info_message_label" Width="504px"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                    </tr>
                    <tr>
                    <td style="height: 376px" valign="top">
                    <asp:UpdatePanel ID="updatePanel_PreInspection_Report" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <cc1:PagingGridView ID="GVPreInspectionReport" runat="server"
                                AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                                BorderStyle="Dotted" BorderWidth="1px" CellPadding="2" EmptyDataText="No record exists"
                                Font-Names="Arial" Font-Size="Small" ForeColor="Black" GridLines="None" VirtualItemCount="-1" Width="100%" OnPageIndexChanging="GVPreInspectionReport_PageIndexChanging" AllowPaging="True" AllowSorting="True">
                                <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                                <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White"  />
                                <Columns>
                                    <asp:TemplateField HeaderText="Date" InsertVisible="False" >
                                        <itemtemplate>
<asp:Label id="RFLblCreationDate" runat="server" CssClass="cellData" Text='<%# Format(Eval("SubmitDate"),"dd/MM/yy") %>'></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Customer" InsertVisible="False" >
                                        <itemtemplate>
<asp:Label id="RFLblCustomer" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatName( Eval("FIRSTNAME").ToString() , Eval("LASTNAME").ToString() ) %>'></asp:Label> 
</itemtemplate>
                                        <headerstyle horizontalalign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField  HeaderText="Address" InsertVisible="False" >
                                        <itemtemplate>
<asp:Label id="RFLblAreaItem" runat="server" CssClass="cellData" Text='<%# BIND("COMPLETEADDRESS") %>'></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Detail" InsertVisible="False" >
                                        <itemtemplate>
<asp:Label id="RFLblDetails" runat="server" CssClass="cellData" Text='<%# Eval("Description") %>'></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty" InsertVisible="False">
                                        <itemtemplate>
<asp:Label id="RFLblQty" runat="server" CssClass="cellData" Text='<%# Eval("Quantity") %>'></asp:Label> 
</itemtemplate>
                                        <headerstyle font-underline="True" horizontalalign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Priority" >
                                        <itemtemplate>
<asp:Label id="RFLblPriority" runat="server" Text='<%# Eval("TimeFrame") %>' CssClass="cellData"></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Appointment">
                                        <itemtemplate>
<asp:Label id="lblAppointment" runat="server" Text='<%# Bind("InspectionDate") %>'></asp:Label>
</itemtemplate>
                                        <headerstyle wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Surveyor">
                                        <itemtemplate>
<asp:Label id="lblSurveyor" runat="server" Text='<%# FullName(Eval("EmpFirstName").ToString(), Eval("EmpLastName").ToString()) %>'></asp:Label>
</itemtemplate>
                                        <headerstyle wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <itemtemplate>
<asp:Button id="btnPreInspect" onclick="btnPreInspect_Click" runat="server" Text="Pre Inspect" UseSubmitBehavior="False" CommandArgument='<%# MakeCommandString(Eval("Recharge").ToString(), Eval("NetCost").ToString(),Eval("faultLogID").ToString(),Eval("DueDate").ToString(), Eval("NewRecharge").ToString(),  Eval("Approved").ToString()) %>'></asp:Button> 
</itemtemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Contractor">
                                        <itemtemplate>
<asp:Label id="RFLblContractor" runat="server" CssClass="cellData" Text='<%# Eval("NAME") %>'></asp:Label> 
</itemtemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerSettings Mode="NumericFirstLast" />
                                
                            </cc1:PagingGridView>
                            &nbsp; &nbsp;
                                        
                    </ContentTemplate>
                    </asp:UpdatePanel>
                        <br />
                        
                    
                       
                        <asp:UpdateProgress ID="UpdateProgress_PreInspection" runat="server" DisplayAfter="10" AssociatedUpdatePanelID="updatePanel_PreInspection_Report">
                            <ProgressTemplate>
                                <asp:Panel ID="pnlProgress" runat="server" BackColor="Silver" Font-Names="Arial"
                                    Font-Size="Small" Height="40px" HorizontalAlign="Center" Width="221px">
                                    <asp:Image ID="imgProgressBar" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" /><br />
                                    Loading ...
                                </asp:Panel>
                                &nbsp;
                                <cc2:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender_progress" runat="server"
                                    HorizontalSide="Center" TargetControlID="pnlProgress" VerticalSide="Middle">
                                </cc2:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <br />
                        <asp:Panel ID="panel_PreInspectionPopUp" runat="server" Height="0px" Width="125px" style="display:none">
                    <asp:UpdatePanel ID="pnlPreInspectionPopup" runat="server" UpdateMode="Conditional">
                       <ContentTemplate>
                        <table cellpadding="1" cellspacing="1" style="width: 50%; background-color:White" >
                            <tbody align="left">
                                <tr >
                                    <td align="left" colspan="11" style="height: 21px; background-color: #c00000">
                                        &nbsp;
                                        <asp:Label ID="lblPreInspectionHeading" runat="server" BackColor="#C00000" Font-Bold="True"
                                            Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Record Pre Inspection"
                                            Width="66%"></asp:Label>
                                        <asp:LinkButton ID="lnkBtnPseudo" runat="server"></asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="11" style="height: 21px">
                                        <asp:UpdatePanel ID="updatePanel_ErrorMessage" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                &nbsp;<asp:Label ID="lblErrorMsg" runat="server" CssClass="error_message_label" Width="320px"></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px">
                                        <asp:Label ID="Label1" runat="server" CssClass="caption" Text="Team: *" EnableViewState="False"></asp:Label></td>
                                    <td align="left" colspan="2">
                                        <asp:UpdatePanel ID="UpdatePanel_team" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                        <asp:DropDownList ID="ddlTeam" runat="server" Width="232px" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged">
                                        </asp:DropDownList>&nbsp;
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px">
                                        <asp:Label ID="lblOperative" runat="server" Text="Surveyor: *" CssClass="caption" EnableViewState="False"></asp:Label></td>
                                    <td align="left" colspan="2">
                                        <asp:UpdatePanel ID="UpdatePanel_operative" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                        <asp:DropDownList ID="ddlOperative" runat="server" Width="232px" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlOperative_SelectedIndexChanged">
                                        </asp:DropDownList>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlTeam" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px">
                                        <asp:Label ID="lblInspectionDate" runat="server" Text="Inspection Date: *" Width="136px" CssClass="caption" EnableViewState="False"></asp:Label></td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtInspect" runat="server" Width="208px" Enabled="False" OnTextChanged="txtInspect_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="btnInspectionCalander" runat="server" CausesValidation="False" ImageUrl="~/images/buttons/Calendar_button.png" /><br />
                                        <cc2:CalendarExtender ID="CalendarExtender_Inspection" runat="server" Format="dd/MM/yyyy"
                        PopupButtonID="btnInspectionCalander" TargetControlID="txtInspect">
                        </cc2:CalendarExtender>
                          </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px; height: 22px;">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px; height: 22px;">
                                        <asp:Label ID="lblRecharge" runat="server" CssClass="caption" Text="Recharge" EnableViewState="False"></asp:Label></td>
                                    <td align="center" colspan="1" style="width: 152px; height: 22px;">
                                        <asp:RadioButton ID="rdbRechargeYes" runat="server" CssClass="caption" GroupName="rdbRecharge"
                                            Text="Y" />&nbsp;</td>
                                    <td align="left" colspan="1" style="width: 175px; height: 22px;">
                                        <asp:RadioButton ID="rdbRechargeNo" runat="server" CssClass="caption" GroupName="rdbRecharge"
                                            Text="N" /></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px">
                                        <asp:Label ID="lblCost" runat="server" CssClass="caption" Text="Cost(Net)�:" Width="88px" EnableViewState="False"></asp:Label></td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtCost" runat="server" Width="229px">0.0</asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px">
                                        <asp:Label ID="lblDueDate" runat="server" CssClass="caption" Text="Due Date:" EnableViewState="False"></asp:Label></td>
                                    <td align="left" colspan="2">
                                        <asp:TextBox ID="txtDueDate" runat="server" Width="229px" Enabled="False" OnTextChanged="txtDueDate_TextChanged" ReadOnly="True"></asp:TextBox>
                                        &nbsp;
                                        </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px; height: 36px">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px; height: 36px">
                                        <asp:Label ID="lblNotes" runat="server" CssClass="caption" Text="Notes:" EnableViewState="False"></asp:Label></td>
                                    <td align="left" colspan="2" style="height: 36px">
                                        <asp:TextBox ID="txtNotes" runat="server" Height="122px" TextMode="MultiLine" Width="229px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px; height: 25px">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px; height: 25px">
                                        <asp:Label ID="lblApproved" runat="server" CssClass="caption" Text="Approved" EnableViewState="False"></asp:Label></td>
                                    <td align="left" colspan="2" style="height: 25px">
                                        <asp:UpdatePanel ID="updatePanel_YesNO" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="center" style="width: 137px">
                                                    <asp:RadioButton ID="rdbApprovedYes" runat="server" CssClass="caption" GroupName="rdbApproved"
                                            Text="Y" AutoPostBack="True" OnCheckedChanged="rdbApprovedYes_CheckedChanged" Checked="True" /></td>
                                                <td style="width: 104px">
                                                    <asp:RadioButton ID="rdbApprovedNo" runat="server" CssClass="caption" GroupName="rdbApproved"
                                            Text="N" AutoPostBack="True" OnCheckedChanged="rdbApprovedNo_CheckedChanged" /></td>
                                            </tr>
                                        </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px">
                                        <asp:Label ID="lblContractor" runat="server" CssClass="caption" Text="Contractor:" EnableViewState="False"></asp:Label></td>
                                    <td align="left" colspan="2">
                                        <asp:UpdatePanel ID="updatePanel_ContractorDropDown" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlContractor" runat="server" Width="232px" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlContractor_SelectedIndexChanged">
                                    </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                        <cc2:ModalPopupExtender BackgroundCssClass="modalBackground" CancelControlID="btnCancel" Drag="True" ID="mdlPreInspectPopup" 
                        PopupControlID="panel_PreInspectionPopUp" runat="server" TargetControlID="txtDueDate" >
                        </cc2:ModalPopupExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="1" style="width: 11px; height: 64px;">
                                    </td>
                                    <td align="left" colspan="8" style="width: 152px; height: 64px;" valign="top">
                                        <asp:Label ID="lblFaultLogID" runat="server" Visible="False"></asp:Label><br />
                                    </td>
                                    <td align="left" colspan="2" style="height: 64px" valign="top">
                                        <table>
                                            <tr>
                                                <td style="width: 68px; height: 28px;" valign="top">
                                        <asp:Button ID="btnCancel" runat="server" CssClass="caption" Text="Cancel" /></td>
                                                <td style="width: 100px; height: 28px;" valign="top">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                        <ContentTemplate>
                                        <asp:Button ID="btnSaveChanges" runat="server" CssClass="caption" Text="Approve Inspection" OnClick="btnSaveChanges_Click" Width="160px" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                          </ContentTemplate>
                    </asp:UpdatePanel>
                        </asp:Panel>
                  </table>
                </td>
            </tr>
            </table>
        
    </div>
    
</asp:Content>    


