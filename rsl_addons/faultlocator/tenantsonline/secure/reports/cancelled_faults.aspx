<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/master pages/EnquiryManagement.Master" CodeBehind="cancelled_faults.aspx.vb" Inherits="tenantsonline.cancelled_faults" 
    title="Tenants Online::Cancelled Jobs Report" %>
    
    <%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
<script language ="javascript" type ="text/javascript">

function ClearCtrl(){
        document.getElementById("<%= txtDate.ClientID %>").value = "";
        document.getElementById("<%= txtFirstName.ClientID %>").value = "";
        document.getElementById("<%= txtLastName.ClientID %>").value = "";
        document.getElementById("<%= txtTenancyID.ClientID %>").value = "";
        document.getElementById("<%= txtText.ClientID %>").value = "";
        document.getElementById("<%= ddlStatus.ClientID %>").value = "";
        
    }
    
    
</script>
 
<script runat = "server">
    Function MakeCommandString(ByVal recharge As String, ByVal netCost As String, ByVal faultLogId As String, ByVal dueDate As String)
        
        Return recharge & "," & netCost & "," & faultLogId & "," & dueDate
        
    End Function
    
    Function MakeCommandString(ByVal id As Integer, ByVal contractorName As String) As String
        
        Return id & "," & contractorName
        
    End Function
    
    Function GetFullName(ByVal firstName As String, ByVal lastName As String)
        
        Return firstName & " " & lastName
        
    End Function
    
</script>
    <table style="width: 100%; height: 100%">
        <tr>
            <td style="width: 234px">
                <cc2:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                </cc2:ToolkitScriptManager>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 234px">
                <table style="border-right: tan thin dotted; border-top: tan thin dotted; border-left: tan thin dotted;
                    width: 5%; border-bottom: tan thin dotted">
                    <tr>
                        <td colspan="2" style="border-bottom: thin dotted">
                            <asp:Label ID="lblArea" runat="server" Font-Names="Arial" Font-Size="Small" Text="Area:"
                                Width="84px"></asp:Label>
                            <asp:DropDownList ID="ddlArea" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                Font-Names="Arial" Font-Size="Small" Width="135px">
                                <asp:ListItem Value="0">Audit Trail</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">Cancelled Faults</asp:ListItem>
                                <asp:ListItem Value="2">Fault Management</asp:ListItem>
                                <asp:ListItem Value="3">Priority Settings</asp:ListItem>
                                <asp:ListItem Value="4">Pricing Control</asp:ListItem>
                                <asp:ListItem Value="5">Repair List</asp:ListItem>
                                <asp:ListItem Value="6">Reported Faults</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblSearch" runat="server" Font-Bold="True" Font-Names="Arial" Text="Search:"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblFirstName" runat="server" Font-Names="Arial" Font-Size="Small"
                                Text="First Name" Width="64px"></asp:Label></td>
                        <td style="width: 145px">
                            <asp:TextBox ID="txtFirstName" runat="server" Font-Names="Arial" Font-Size="Small"
                                MaxLength="50" Width="150px"></asp:TextBox><br />
                            <cc2:filteredtextboxextender id="filtbox" runat="server" filtermode="invalidChars"
                                filtertype="Custom" invalidchars="~!@#$%^&*0123456789" targetcontrolid="txtFirstName"></cc2:filteredtextboxextender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblLastName" runat="server" Font-Names="Arial" Font-Size="Small" Text="Last Name"></asp:Label></td>
                        <td style="width: 145px">
                            <asp:TextBox ID="txtLastName" runat="server" Font-Names="Arial" Font-Size="Small"
                                MaxLength="50" Width="150px"></asp:TextBox><br />
                            <cc2:filteredtextboxextender id="fltr2" runat="server" filtermode="invalidChars"
                                filtertype="Custom" invalidchars="~!@#$%^&*0123456789" targetcontrolid="txtLastName"></cc2:filteredtextboxextender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDate" runat="server" Font-Names="Arial" Font-Size="Small" Text="Date"></asp:Label></td>
                        <td style="width: 145px">
                            <asp:TextBox ID="txtDate" runat="server" Enabled="False" Font-Names="Arial" Font-Size="Small"
                                MaxLength="10" Width="88px"></asp:TextBox>
                            <asp:ImageButton ID="btnCalander" runat="server" CausesValidation="False" ImageUrl="~/images/buttons/Calendar_button.png" />
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblTenancyID" runat="server" Font-Names="Arial" Font-Size="Small"
                                Text="TenancyID"></asp:Label></td>
                        <td style="width: 145px">
                            <asp:TextBox ID="txtTenancyID" runat="server" Font-Names="Arial" Font-Size="Small"
                                MaxLength="10" Width="150px"></asp:TextBox><br />
                            <cc2:filteredtextboxextender id="fltr3" runat="server" filtertype="numbers" targetcontrolid="txtTenancyID"></cc2:filteredtextboxextender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblStatus" runat="server" Font-Names="Arial" Font-Size="Small" Text="Status"></asp:Label></td>
                        <td style="width: 145px">
                            <asp:DropDownList ID="ddlStatus" runat="server" AppendDataBoundItems="True" Font-Names="Arial"
                                Font-Size="Small" Width="152px">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblText" runat="server" Font-Names="Arial" Font-Size="Small" Text="Text"></asp:Label></td>
                        <td style="width: 145px">
                            <asp:TextBox ID="txtText" runat="server" Font-Names="Arial" Font-Size="Small" MaxLength="255"
                                Width="150px"></asp:TextBox><br />
                            <cc2:filteredtextboxextender id="fltr4" runat="server" filtermode="invalidChars"
                                filtertype="Custom, LowercaseLetters, UppercaseLetters" invalidchars="~!@#$%^&*0123456789"
                                targetcontrolid="txtText"></cc2:filteredtextboxextender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            &nbsp;</td>
                        <td style="width: 145px">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btn" runat="server" Text="Search" OnClick="btn_Click" /><asp:Button ID="btnClear" runat="server"
                                        OnClientClick="ClearCtrl(); return false;" Style="position: static" Text="Clear" OnClick="btnClear_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <cc2:CalendarExtender ID="calendarExtender_Date" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="btnCalander" TargetControlID="txtDate">
                </cc2:CalendarExtender>
            </td>
            <td align="left" valign="top">
                <asp:Label ID="lblRFHeading" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Black"
                    Text="Cancelled Faults" Width="128px"></asp:Label>&nbsp;<br />
                <asp:UpdatePanel ID="updatePanel_LoadGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <cc1:PagingGridView ID="grvCancelledFaults" runat="server" AllowPaging="True"
                            AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan"
                            BorderStyle="Dotted" BorderWidth="1px" CellPadding="2" EmptyDataText="No record exists"
                            Font-Names="Arial" Font-Size="Small" ForeColor="Black" GridLines="None" VirtualItemCount="-1" Width="95%" OnPageIndexChanging="grvCancelledFaults_PageIndexChanging" AllowSorting="True">
                            <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                            <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Submit Date" InsertVisible="False">
                                    <itemtemplate>
<asp:Label id="RFLblCreationDate" runat="server" CssClass="cellData" Text='<%# Eval("SubmitDate").ToString() %>' Width="120px" __designer:wfdid="w12"></asp:Label> 
</itemtemplate>
                                    <headerstyle wrap="False" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Customer" InsertVisible="False">
                                    <itemtemplate>
<asp:Label id="RFLblCustomer" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatName( Eval("FIRSTNAME").ToString() , Eval("LASTNAME").ToString() ) %>' Width="104px" __designer:wfdid="w34"></asp:Label> 
</itemtemplate>
                                    <headerstyle horizontalalign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="JSNumber" Visible="False">
                                    <itemtemplate>
<asp:Label id="lblJSNumber" runat="server" Text='<%# Bind("JS_Number") %>' Width="88px" __designer:wfdid="w3"></asp:Label> 
</itemtemplate>
                                    <headerstyle wrap="False" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Area/Item" InsertVisible="False">
                                    <itemtemplate>
<asp:Label id="RFLblAreaItem" runat="server" CssClass="cellData" Text='<%# UtilityFunctions.FormatAreaElement( Eval("AreaName").ToString() , Eval("ElementName").ToString() ) %>' Width="184px" __designer:wfdid="w7"></asp:Label> 
</itemtemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Detail" InsertVisible="False">
                                    <itemtemplate>
<asp:Label id="lblDetails" runat="server" Text='<%# Eval("Description") %>' Width="120px" __designer:wfdid="w2"></asp:Label> 
</itemtemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty" InsertVisible="False">
                                    <itemtemplate>
<asp:Label id="RFLblQty" runat="server" CssClass="cellData" Text='<%# Eval("Quantity") %>' Width="64px" __designer:wfdid="w3"></asp:Label> 
</itemtemplate>
                                    <headerstyle font-underline="True" horizontalalign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Priority">
                                    <itemtemplate>
<asp:Label id="RFLblPriority" runat="server" CssClass="cellData" Text='<%# Eval("TimeFrame") %>' Width="88px" __designer:wfdid="w15"></asp:Label> 
</itemtemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="By">
                                    <itemtemplate>
<asp:Label id="lblEmployee" runat="server" Text='<%# GetFullName(Eval("UserFirstName").ToString(), Eval("UserLastName").ToString()) %>' __designer:wfdid="w2"></asp:Label> <asp:Label id="RFLblBy" runat="server" CssClass="cellData" Text='<% GetFullName(Eval("UserFirstName").ToString(), Eval("UserLastName").ToString()) %>' Width="96px" __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Notes">
                                    <itemtemplate>
<asp:Label id="RFLblNotes" runat="server" CssClass="cellData" Text='<%# Eval("NOTES") %>' Width="200px" __designer:wfdid="w4"></asp:Label> 
</itemtemplate>
                                    <itemstyle width="300px" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" />
                            <EmptyDataRowStyle Wrap="False" />
                        </cc1:PagingGridView>
                        &nbsp;
                        <asp:UpdateProgress ID="UpdateProgress_enquiries" runat="server" DisplayAfter="10">
                            <ProgressTemplate>
                                <asp:Panel ID="pnlProgress" runat="server" BackColor="Silver" Font-Names="Arial"
                                    Font-Size="Small" Height="40px" HorizontalAlign="Center" Width="221px">
                                    <asp:Image ID="imgProgressBar" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" /><br />
                                    Loading ...
                                </asp:Panel>
                                &nbsp;
                                <cc2:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender_progress" runat="server"
                                    HorizontalSide="Center" TargetControlID="pnlProgress" VerticalSide="Middle">
                                </cc2:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 234px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
