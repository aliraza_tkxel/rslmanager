Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Partial Public Class reported_faults
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page


#Region "Page Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
        Response.Redirect("~/../BHAIntranet/login.aspx")
        End If

    End Sub

#End Region

#Region "Page Load Event"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
        Response.Redirect("~/../BHAIntranet/login.aspx")
        End If
        'rdbApprovedNo.Attributes.Add("onclick", "javascript:alert('The Repair will Not Be Progressed Any Further');")

        ' The userId will come from

        If Not Me.IsPostBack Then
            'populate  nature and status dropdown with the values for search
            'Me.GetNatureLookUpValues()
            Me.GetStatusLookUpValues()
            Me.SetKeyPostback()

            'storing in view state boolean value indicating , results being displayed in gridview are 
            'from search or not
            ViewState(ApplicationConstants.ReportedFaultIsSearchViewState) = True
            'storing current sort order of any column asc or dsen
            ViewState(ApplicationConstants.ReportedFaultSortOrderViewSate) = ApplicationConstants.ReportedFaultDESCSortOrder

            'function used for storing search parameters which will be used for further page requests if result is 
            'multipage
            SaveSearchOptions("")
            'setting initial page index of gridview by default its zero
            RFLog.PageIndex = ApplicationConstants.ReportedFaultInitialPageIndex


            'getting and setting row count of resultset
            RFLog.VirtualItemCount = GetRowCount()

            ''''''''''''''''''''''''''


            Me.txtLastName.Focus()
            GetReportedFault()
            updatePanel.Update()

        End If


    End Sub

#End Region

#Region "GetLookUpValues"

    Private Sub GetStatusLookUpValues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getReportedFaultStatusLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then

            Me.chkListFaultStatus.DataSource = lstLookUp
            Me.chkListFaultStatus.DataValueField = "LookUpValue"
            Me.chkListFaultStatus.DataTextField = "LookUpName"
            Me.chkListFaultStatus.DataBind()


        End If

    End Sub
    Private Sub GetTeamLookUpVallues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.GetPreInspectionTeamLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlTeam.Items.Clear()
            Me.ddlTeam.DataSource = lstLookUp
            Me.ddlTeam.DataValueField = "LookUpValue"
            Me.ddlTeam.DataTextField = "LookUpName"
            Me.ddlTeam.Items.Add(New ListItem("Please select", ""))
            Me.ddlTeam.DataBind()
        End If

        Me.ddlOperative.Items.Clear()
        Me.ddlOperative.Items.Add(New ListItem("Please select", ""))

    End Sub

    ''User Lookup Values for Operative
    Private Sub GetOperativeLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlOperative, SprocNameConstants.GetUserLookup, inParam, inParamName)
    End Sub
#End Region

#Region "UtilityMethods"

    'Set the attribute so that when user presses enter key the specified event for the mentioned button will be fired
    Private Sub SetKeyPostback()

        Me.txtJS.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btn.UniqueID + "').click();return false;}} else {return true}; ")
        Me.txtLastName.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btn.UniqueID + "').click();return false;}} else {return true}; ")
        Me.txtText.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btn.UniqueID + "').click();return false;}} else {return true}; ")

    End Sub
#End Region

#Region "GetReportedFaults"
    Private Sub GetReportedFault()

        Try
            Dim RFSearchBO As ReportedFaultSearchBO = New ReportedFaultSearchBO()
            'reterive search options ReportedFaultSearchBO from viewstate before requesting results for next page of gridview
            RFSearchBO = DirectCast(ViewState(ApplicationConstants.ReportedFaultSearchOptionsViewState), ReportedFaultSearchBO)
            Dim rfDS As DataSet = New DataSet()
            Dim rfDSExport As DataSet = New DataSet()
            'calling GetDataPage function that will further call function in buisness layer
            GetDataPage(RFLog.PageIndex, ApplicationConstants.ReportedFaultFinanceResultPerPage, RFLog.OrderBy, rfDS, rfDSExport, RFSearchBO)
            'binding gridview with the result returned from db
            Me.StoreResultSummary()
            RFLog.DataSource = rfDS

            RFLog.VirtualItemCount = GetRowCount()
            RFLog.DataBind()
            updatePanel.Update()

            ' Filling the Export Grid
            RFLogExport.DataSource = rfDSExport
            RFLogExport.DataBind()

            updatePanelExport.Update()

            ViewState.Add(ApplicationConstants.FaultLocatorDataSet, rfDS)
            'ViewState.Add(ApplicationConstants.ReportedFaultInitialPageIndex, RFLog.PageIndex)
            ViewState.Add(ApplicationConstants.ReportedFaultPageSizeKey, RFLog.PageSize)

            DisableEnalbePreInspectionButton(rfDS)

        Catch ex As Exception

            Response.Redirect("~/error.aspx")

        End Try


    End Sub
#End Region

#Region "Method to EnableDisable PreInspection"

    Public Sub DisableEnalbePreInspectionButton(ByRef ds As DataSet)

        Try
            For i As Integer = 0 To RFLog.Rows.Count - 1
                If (DirectCast(ds.Tables(0).Rows(i)(12), Boolean) <> True) Then
                    RFLog.Rows(i).Cells(9).FindControl("btnPreInspect").Visible = False
                Else
                    RFLog.Rows(i).Cells(9).FindControl("btnPreInspect").Visible = True
                    ' If job has been Cancelled, then disable the PreInspection Button
                    If CType(RFLog.Rows(i).Cells(8).FindControl("RFLblStatus"), Label).Text = "Cancelled Fault" Then
                        RFLog.Rows(i).Cells(9).Enabled = False
                    End If
                End If
            Next
        Catch ex As Exception

        End Try

    End Sub

#End Region

#Region "SaveInspectionRecord"
    Private Sub SaveInspectionRecord()
        Dim piBO As PreInspectBO = New PreInspectBO()
        'fill piBO attribute from inspection data
        If Me.ddlOperative.SelectedValue <> String.Empty Then
            piBO.UserId = CType(ddlOperative.SelectedValue, Integer)
        End If

        piBO.NetCost = CType(txtCost.Text, Double)
        piBO.Notes = txtNotes.Text

        If Me.txtInspect.Text <> String.Empty Then
            piBO.InspectionDate = UtilityFunctions.stringToDate(txtInspect.Text)
        End If
        If Me.txtDueDate.Text <> String.Empty Then
            piBO.DueDate = UtilityFunctions.stringToDate(txtDueDate.Text)
        End If

        'If Me.ddlContractor.SelectedValue <> String.Empty Then
        '    piBO.OrgId = CType(ddlContractor.SelectedValue, Integer)
        'End If

        'If Me.rdbApprovedYes.Checked = True Then
        '    piBO.Approved = 1
        'End If

        piBO.FaultLogId = CType(lblFaultLogID.Text, Integer)
        Dim piBL As FaultManager = New FaultManager()
        'piBL.SaveInspectionRecord(piBO)
        piBL.SaveAppointment(piBO)



    End Sub
#End Region

#Region "Sub for storing search options"

    Private Sub SaveSearchOptions(ByVal isSearch As String)

        'make ReportedFaultSearchBO object and store the search options entered by user untill he/she enters again
        'in this case the values will be overidden
        Dim RFSearchBO As ReportedFaultSearchBO = New ReportedFaultSearchBO()
        Dim i As Integer
        'set JS entered
        RFSearchBO.JS = IIf(txtJS.Text = "", -1, txtJS.Text)
        'set last name enetered
        RFSearchBO.LastName = IIf(txtLastName.Text = "", String.Empty, txtLastName.Text)
        'set text value enetered
        RFSearchBO.TextValue = IIf(txtText.Text = "", String.Empty, txtText.Text)

        If txtDateFrom.Text <> "" Then
            Try
                'convert date string to date object and set its value
                'RFSearchBO.DateValue = UtilityFunctions.stringToDate(Me.txtDate.Text)
                RFSearchBO.DateValue = txtDateFrom.Text.ToString().Trim()
            Catch ex As Exception
                Me.lblErrorMessage.Text = "Please enter a valid From date"
                Return
            End Try
        End If

        If txtDateTo.Text <> "" Then
            Try
                'convert date string to date object and set its value
                'RFSearchBO.DateValue = UtilityFunctions.stringToDate(Me.txtDate.Text)
                RFSearchBO.DateToValue = txtDateTo.Text.ToString().Trim()
            Catch ex As Exception
                Me.lblErrorMessage.Text = "Please enter a valid To date"
                Return
            End Try
        End If

        Dim checkedStatus As String = ""
        For i = 0 To chkListFaultStatus.Items.Count - 1
            If chkListFaultStatus.Items(i).Selected = True Then
                If checkedStatus = "" Then
                    checkedStatus = chkListFaultStatus.Items(i).Value
                Else
                    checkedStatus = checkedStatus & "," & chkListFaultStatus.Items(i).Value
                End If

            End If
        Next
        RFSearchBO.FaultStatus = checkedStatus

        'tell RFSearchBO it has values that will be used for searching
        If isSearch <> String.Empty Then
            RFSearchBO.IsSearch = True
        Else
            RFSearchBO.IsSearch = False
        End If


        'store RFSearchBO object in viewstate
        ViewState(ApplicationConstants.ReportedFaultSearchOptionsViewState) = RFSearchBO
        'store true in viewstate to indicate that current data on gridview is from search
        ViewState(ApplicationConstants.ReportedFaultIsSearchViewState) = True
    End Sub
#End Region

#Region "Function to get data "
    Public Sub GetDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef rfDS As DataSet, ByRef rfDSExport As DataSet, ByVal rfSearchBO As ReportedFaultSearchBO)


        'reteriving sortby column name from viewstate
        If ViewState(ApplicationConstants.ReportedFaultSortByViewState) <> Nothing Then
            rfSearchBO.SortBy = CType(ViewState(ApplicationConstants.ReportedFaultSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use FaultLogID as a default
            rfSearchBO.SortBy = ApplicationConstants.ReportedFaultManagSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        rfSearchBO.PageIndex = pageIndex
        rfSearchBO.RowCount = pageSize

        'reteriving sort order from viewstate
        rfSearchBO.SortOrder = DirectCast(ViewState(ApplicationConstants.ReportedFaultSortOrderViewSate), String)
        Dim rfMngr As FaultManager = New FaultManager()
        'passing reference of dataset and passing enqryLogSearchBO to EnquiryLogData function
        'of EnquiryManager
        Try

            rfMngr.ReportedFaultReport(rfDS, rfSearchBO)
            'storing number of rows in the result
            ViewState("NumberofRows") = rfDS.Tables(0).Rows.Count

            rfMngr.ReportedFaultReportExport(rfDSExport, rfSearchBO)

        Catch ex As Exception

            Response.Redirect("error.aspx")

        End Try


        ''Check for exception 
        'Dim exceptionFlag As Boolean = rfSearchBO.IsExceptionGenerated
        'If exceptionFlag Then
        '    Response.Redirect("error.aspx")
        'End If

    End Sub
#End Region

#Region "Events"

#Region "btn_Click"

    Protected Sub btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn.Click

        GetFaultUsingSearchCriteria()
        updatePanel.Update()
        RFLog.PageIndex = ViewState.Item(ApplicationConstants.ReportedFaultInitialPageIndexKey)

    End Sub

#End Region

#Region "btnPreInspect_Click"

    Protected Sub btnPreInspect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        RFLog.PageIndex = ViewState(ApplicationConstants.ReportedFaultInitialPageIndexKey)
        lblSavedSuccessfully.Text = String.Empty
        updatePanel_SavedSuccessfully.Update()

        lblErrorMsg.Text = ""
        updatePanel_ErrorMessage.Update()

        Try
            HideEmptyRows()

            Me.GetTeamLookUpVallues()
            'Me.GetContractorLookUpVallues()

            Dim btn As Button = DirectCast(sender, Button)
            Dim strArray As String() = btn.CommandArgument.Split(",")

            Dim faultLogId As Integer = CType(strArray(2).ToString(), Integer)

            ClearPreinspectionPopupFields()


            Dim recharge As Boolean = CType(strArray(0).ToString(), Boolean)

            If (recharge) Then
                rdbRechargeYes.Checked = True
            Else
                rdbRechargeNo.Checked = True
            End If

            Dim dueDate As String = strArray(3).ToString()
            'Me.txtDueDate.Text = UtilityFunctions.FormatDate(dueDate.ToString())
            Me.txtDueDate.Text = dueDate.ToString()
            Me.txtCost.Text = strArray(1).ToString()


            lblFaultLogID.Text = faultLogId.ToString()

            Me.PopulatePreinspectionPopup(faultLogId)
            pnlPreInspectionPopup.Update()

            mdlPreInspectPopup.Show()

        Catch ex As Exception

            Response.Redirect("~/error.aspx")

        End Try

    End Sub

    Public Sub HideEmptyRows()

        RFLog.PageIndex = CType(ViewState(ApplicationConstants.ReportedFaultInitialPageIndexKey), Int32)
        RFLog.PageSize = ViewState(ApplicationConstants.ReportedFaultPageSizeKey)
        Dim faultDS As DataSet = New DataSet()
        faultDS = ViewState.Item(ApplicationConstants.FaultLocatorDataSet)
        RFLog.DataSource = faultDS

        RFLog.DataBind()
        DisableEnalbePreInspectionButton(faultDS)

    End Sub

#End Region

#Region "Clear Preinspection Popup Fields"


    Public Sub ClearPreinspectionPopupFields()

        Me.ddlTeam.SelectedValue = String.Empty
        Me.ddlOperative.SelectedValue = String.Empty
        Me.txtInspect.Text = String.Empty
        Me.txtDueDate.Text = String.Empty
        Me.txtNotes.Text = String.Empty
        'Me.ddlContractor.SelectedValue = String.Empty
        'Me.rdbApprovedYes.Checked = True
        'Me.rdbApprovedNo.Checked = False
        Me.rdbRechargeNo.Checked = False
        Me.rdbRechargeYes.Checked = False


        Me.ddlTeam.Enabled = True
        Me.ddlOperative.Enabled = True
        Me.txtInspect.Enabled = True
        Me.txtDueDate.Enabled = True
        Me.txtNotes.Enabled = True
        'Me.ddlContractor.Enabled = True
        'Me.rdbApprovedYes.Enabled = True
        'Me.rdbApprovedNo.Enabled = True
        Me.btnSaveChanges.Enabled = True

    End Sub

#End Region

#Region "ddlTeam_SelectedIndexChanged"


    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (ddlTeam.SelectedIndex <> 0) Then
            Me.GetOperativeLookUpValues(ddlTeam.SelectedValue.ToString(), "@TeamId")
        Else
            Me.ddlOperative.Items.Clear()
            Me.ddlOperative.Items.Add(New ListItem("Please select", ""))
        End If
    End Sub

#End Region

#Region "btnSaveChanges_Click"


    Protected Sub btnSaveChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        RFLog.PageIndex = ViewState(ApplicationConstants.ReportedFaultInitialPageIndexKey)
        ValidatePreInspectionPopup()

    End Sub

#End Region

#Region "Approved Radio Button Checked Events"

    Protected Sub rdbApprovedNo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Me.ddlContractor.SelectedValue = ""
        'ddlContractor.Enabled = False
        'updatePanel_Contractor.Update()

    End Sub

    Protected Sub rdbApprovedYes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'ddlContractor.Enabled = True
        'updatePanel_Contractor.Update()

    End Sub

#End Region

#Region "Sorting Event Handler"
    Protected Sub RFLog_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        'storing the column name by which new sort is requested
        ViewState(ApplicationConstants.ReportedFaultSortByViewState) = e.SortExpression
        'setting pageindex of gridview equal to zero for new search
        RFLog.PageIndex = ApplicationConstants.ReportedFaultInitialPageIndex
        'setting new sorting order
        If DirectCast(ViewState(ApplicationConstants.ReportedFaultSortOrderViewSate), String) = ApplicationConstants.ReportedFaultDESCSortOrder Then
            ViewState(ApplicationConstants.ReportedFaultSortOrderViewSate) = ApplicationConstants.ReportedFaultASCESortOrder
        Else
            ViewState(ApplicationConstants.ReportedFaultSortOrderViewSate) = ApplicationConstants.ReportedFaultDESCSortOrder
        End If

        GetFaultUsingSearchCriteria()
        lblSavedSuccessfully.Text = String.Empty
        updatePanel_SavedSuccessfully.Update()
    End Sub
#End Region

#Region "PageIndexChange Handler"
    Protected Sub RFLog_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)

        lblSavedSuccessfully.Text = String.Empty
        updatePanel_SavedSuccessfully.Update()
        'assiging new page index to the gridview
        RFLog.PageIndex = e.NewPageIndex
        ViewState.Add(ApplicationConstants.ReportedFaultInitialPageIndexKey, e.NewPageIndex)
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreResultSummary()
        'GetFaultUsingSearchCriteria()
        GetReportedFault()



    End Sub
#End Region

#End Region

#Region "GetFaultUsingSearchCriteria"
    Public Sub GetFaultUsingSearchCriteria()
        'save these new search options entered by user unless search button is pressed again
        SaveSearchOptions("search")

        'getting search results
        GetReportedFault()
    End Sub
#End Region

#Region "Store Result Summary"
    'when page index changes this functions gets called 
    Public Sub StoreResultSummary()
        Dim row As GridViewRow = RFLog.BottomPagerRow()
        Dim strResultSummary As String = ""

        If RFLog.PageIndex + 1 = RFLog.PageCount Then
            strResultSummary = "Result " & ((RFLog.PageIndex * RFLog.PageSize) + 1) & " to " & RFLog.VirtualItemCount & " of " & RFLog.VirtualItemCount
        Else
            strResultSummary = "Result " & ((RFLog.PageIndex * RFLog.PageSize) + 1) & " to " & ((RFLog.PageIndex + 1) * RFLog.PageSize) & " of " & RFLog.VirtualItemCount
        End If
        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.ReportedFaultResultSummary) = strResultSummary
    End Sub
#End Region

#Region "Function to count total number of rows"
    Private Function GetRowCount() As Integer
        Dim rfMngr As FaultManager = New FaultManager()
        Dim RFSearchBO As ReportedFaultSearchBO = New ReportedFaultSearchBO()
        'reterive search options and get count of rows for these search options using GetReportedFaultSearchRowCount
        'function of FaultManager
        RFSearchBO = DirectCast(ViewState(ApplicationConstants.ReportedFaultSearchOptionsViewState), ReportedFaultSearchBO)
        Dim count As Integer = rfMngr.GetReportedFaultReportSearchRowCount(RFSearchBO)
        Return count
    End Function
#End Region

#Region "Populate Preinspection Popup"

    Public Sub PopulatePreinspectionPopup(ByVal tempFaultID As Int32)
        Dim PIDS As DataSet = New DataSet()
        Dim faultBO As FaultBO = New FaultBO()
        Dim objFaultBL As FaultManager = New FaultManager()
        Try
            faultBO.FaultID = tempFaultID
            objFaultBL.GetPreinspectionPopupData(PIDS, faultBO)

            If (PIDS.Tables(0).Rows.Count > 0) Then
                Me.ddlTeam.SelectedValue = PIDS.Tables(0).Rows(0)("Team").ToString()
                Me.ddlOperative.Items.Clear()
                Me.ddlOperative.Items.Add(New ListItem(PIDS.Tables(0).Rows(0)("UserName").ToString(), ""))
                Me.txtCost.Text = CType(PIDS.Tables(0).Rows(0)("NETCOST"), Decimal).ToString("0.00")
                Me.txtInspect.Text = UtilityFunctions.FormatDate(PIDS.Tables(0).Rows(0)("INSPECTIONDATE").ToString())
                Me.txtDueDate.Text = UtilityFunctions.FormatDate(PIDS.Tables(0).Rows(0)("DUEDATE").ToString())
                Me.txtNotes.Text = PIDS.Tables(0).Rows(0)("NOTES").ToString()
                'Me.ddlContractor.Text = PIDS.Tables(0).Rows(0)("ORGID").ToString()
                'If PIDS.Tables(0).Rows(0)("ORGID").ToString() <> "0" Then
                '    Me.rdbApprovedYes.Checked = True
                '    Me.rdbApprovedNo.Checked = False
                'Else
                '    Me.rdbApprovedNo.Checked = True
                '    Me.rdbApprovedYes.Checked = False
                'End If
                Me.DisablePreinspectionPopupFields()

            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


    End Sub

#End Region

#Region "Disable Preinspection Popup Fields"


    Public Sub DisablePreinspectionPopupFields()
        'Me.lblValidate.Enabled = False
        Me.ddlTeam.Enabled = False
        Me.ddlOperative.Enabled = False
        Me.txtInspect.Enabled = False
        Me.txtDueDate.Enabled = False
        Me.txtNotes.Enabled = False
        'Me.ddlContractor.Enabled = False
        'Me.rdbApprovedYes.Enabled = False
        'Me.rdbApprovedNo.Enabled = False
        Me.btnSaveChanges.Enabled = False
    End Sub

#End Region

#Region "Methods "
    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values by running the given stored procedure
    ''' against some given parameter
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="spName">String: Stored Procedure's name to get data</param>
    ''' <param name="inParam">String: parameter which has to be given to the stored procedure</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with three arguments</remarks>
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
            UpdatePanel_operative.Update()
        End If
    End Sub
#End Region

#Region "Method to Validate PreInspectionPopup"

    Public Sub ValidatePreInspectionPopup()

        If (ddlTeam.SelectedValue = String.Empty Or ddlTeam.SelectedValue = "Please select") Then
            lblErrorMsg.Text = "Please Select Team"
            updatePanel_ErrorMessage.Update()

        ElseIf (ddlOperative.SelectedIndex = 0) Then

            lblErrorMsg.Text = "Please Select Operative"
            updatePanel_ErrorMessage.Update()

        ElseIf (txtInspect.Text = "") Then

            lblErrorMsg.Text = "Please Select Inspection Date"
            updatePanel_ErrorMessage.Update()

            'ElseIf (rdbApprovedYes.Checked = True And ddlContractor.SelectedValue = "") Then
            '    lblErrorMsg.Text = "Please Choose a Contractor"
            '    updatePanel_ErrorMessage.Update()

        Else
            SaveInspectionRecord()
            GetReportedFault()
            'RFLog.PageIndex = ViewState.Item(ApplicationConstants.ReportedFaultInitialPageIndexKey)
            updatePanel.Update()
            lblSavedSuccessfully.Text = "PreInspection Saved Successfully"
            updatePanel_SavedSuccessfully.Update()
            pnlPreInspectionPopup.Update()

        End If
    End Sub
#End Region


#Region "Can Cancel OR Assgin Contractor "
    Private Function IsAllowedTOCancelORAssginContractor(ByVal faultLogId)
        Try
            Dim fLogBO As FaultLogBO = New FaultLogBO()
            Dim faultBL As FaultManager = New FaultManager()
            Dim disApprove As Boolean


            disApprove = faultBL.IsAllowedTOCancelORAssginContractor(faultLogId, fLogBO)
            Return disApprove

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try
    End Function
#End Region

#Region "btnCancel_Click"
    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'RFLog.PageIndex = ViewState(ApplicationConstants.ReportedFaultInitialPageIndexKey)
        'Dim ds As DataSet = New DataSet()
        'ds = ViewState(ApplicationConstants.FaultLocatorDataSet)
        'RFLog.DataSource = ds
        'RFLog.DataBind()
        'updatePanel.Update()

    End Sub
#End Region

    Protected Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Response.Clear()
        Response.ContentType = "application/x-msexcel"
        'Response.AddHeader("content-disposition", "attachment;filename=123.xls")
        'Response.AddHeader("content-disposition", "inline;filename=123.xls")
        Response.Charset = ""
        Me.EnableViewState = False
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter
        Dim htmlWrite As HtmlTextWriter = New HtmlTextWriter(stringWrite)
        'ClearControls(gvExport)

        RFLogExport.Visible = True
        RFLogExport.RenderControl(htmlWrite)
        RFLogExport.Visible = False
        Response.Write(stringWrite.ToString())
        Response.End()


    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)
        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
        ' No code required here. 
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("/Finance/Finance.asp")
    End Sub
End Class