Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Partial Public Class postinspection_report
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

#Region "Events"

#Region "Page Init "

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
            Response.Redirect("~/../BHAIntranet/login.aspx")
        End If
    End Sub

#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            GVPostInspectionReport.PageIndex = ApplicationConstants.PostInspectionReportInitialPageIndex
            ViewState.Add(ApplicationConstants.PostInspectionReportSortOrderViewSate, ApplicationConstants.PostInspectionDESSortOrder)

            PopulateSearchResults()
            'updatePanel_Popup.Visible = False            
            'updatePanel_PostInspectionUpdate.Visible = False


        End If
    End Sub
#End Region

#Region "ddl Team Selected Index Changed "

    Protected Sub ddlTeam_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (ddlTeam.SelectedIndex <> 0 And ddlTeam.SelectedValue <> String.Empty) Then
            Me.GetOperativeLookUpValues(ddlTeam.SelectedValue.ToString(), "@TeamId")
        Else
            Me.ddlOperative.Items.Clear()
            Me.ddlOperative.Items.Add(New ListItem("Please select", ""))
        End If
    End Sub

    Protected Sub ddlTeamUpdate_SelectedIndexChanged1(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If (ddlTeamUpdate.SelectedIndex <> 0 And ddlTeamUpdate.SelectedValue <> String.Empty) Then
            Me.GetOperativeLookUpValuesUpdate(ddlTeamUpdate.SelectedValue.ToString(), "@TeamId")
        Else
            Me.ddlOperativeUpdate.Items.Clear()
            Me.ddlOperativeUpdate.Items.Add(New ListItem("Please select", ""))
        End If
    End Sub

#End Region

#Region "btn Save Changes Click "

    Protected Sub btnSaveChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        ValidatePreInspectionPopup()

    End Sub
#End Region

#Region "Validate Pre Inspection Popup "

    Public Sub ValidatePreInspectionPopup()

        If (ddlTeam.SelectedValue = String.Empty Or ddlTeam.SelectedValue = "Please select") Then
            lblErrorMsg.Text = "Please Select Team"
            updatePanel_ErrorMessage.Update()

        ElseIf (ddlOperative.SelectedIndex = 0) Then

            lblErrorMsg.Text = "Please Select Operative"
            updatePanel_ErrorMessage.Update()

        ElseIf txtInspect.Text = "" Then

            lblErrorMsg.Text = "Please Select Inspection Date"
            updatePanel_ErrorMessage.Update()

        ElseIf ddlPostInspectionHr.SelectedValue = "00" Then

            lblErrorMsg.Text = "Please Select Hours"
            updatePanel_ErrorMessage.Update()

        ElseIf (txtInspect.Text = "") Then

            lblErrorMsg.Text = "Please Select Inspection Date"
            updatePanel_ErrorMessage.Update()

        Else
            SavePostInspectionRecord()
            'GetPreInspectionReport()
            'lblSuccessMessage.Text = "Record Saved Successfully"
            'updatePanel_successfullySaved.Update()

            PopulateSearchResults()
            updatePanel_Popup.Update()
            Me.updatePanel_ReportGrid.Update()

        End If
    End Sub
#End Region

#Region "Validate Pre Inspection Popup Update "

    Public Sub ValidatePreInspectionPopupUpdate()

        If (ddlTeamUpdate.SelectedValue = String.Empty Or ddlTeam.SelectedValue = "Please select") Then
            lblErrorMsgUpdate.Text = "Please Select Team"
            updatePanel_ErrorMessageUpdate.Update()

        ElseIf (ddlOperativeUpdate.SelectedValue = String.Empty) Then

            lblErrorMsgUpdate.Text = "Please Select Operative"
            updatePanel_ErrorMessageUpdate.Update()

        ElseIf txtInspectUpdate.Text = "" Then

            lblErrorMsgUpdate.Text = "Please Select Inspection Date"
            updatePanel_ErrorMessageUpdate.Update()

        ElseIf ddlPostInspectionHrUpdate.SelectedValue = "00" Then

            lblErrorMsgUpdate.Text = "Please Select Hours"
            updatePanel_ErrorMessageUpdate.Update()

        ElseIf Me.rdbApprovedYesUpdate.Checked = False Then


            lblErrorMsgUpdate.Text = "Please Approve the Appointment"
            updatePanel_ErrorMessageUpdate.Update()

        Else
            SavePostInspectionRecordUpdate()
            'GetPreInspectionReport()
            'lblSuccessMessage.Text = "Record Saved Successfully"
            'updatePanel_successfullySaved.Update()
            updatePanel_ReportGrid.Update()
            updatePanel_PostInspectionUpdate.Update()

        End If
    End Sub

#End Region

#Region "btn Post Inspect Click "

    Protected Sub btnPostInspect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        HidePostInspectionRows()
        Dim btn As Button = DirectCast(sender, Button)

        Dim commandArgumentArray() As String = btn.CommandArgument.Split(",")

        Dim faultLogId As String = commandArgumentArray(0).ToString()
        Dim isRecharge As Boolean = commandArgumentArray(2).ToString()
        Dim repairId As String = commandArgumentArray(3).ToString()
        Dim netCost As String = commandArgumentArray(4).ToString()
        Dim newRecharge As String = commandArgumentArray(5).ToString()

        txtCost.Text = CType(netCost, Double).ToString("0.00")

        'If no new recharge value has been set the default recharge value will be used
        If newRecharge = "" Then
            If (isRecharge) Then
                rdbRechargeYes.Checked = True
                rdbRechargeYesUpdate.Checked = True
                rdbRechargeNo.Checked = False
                rdbRechargeNoUpdate.Checked = False
            Else
                rdbRechargeNo.Checked = True
                rdbRechargeNoUpdate.Checked = True
                rdbRechargeYes.Checked = False
                rdbRechargeYesUpdate.Checked = False
            End If
        Else
            Dim newRechargeValue As Boolean = CType(commandArgumentArray(5), Boolean)

            If (newRechargeValue) Then
                rdbRechargeYes.Checked = True
                rdbRechargeYesUpdate.Checked = True
                rdbRechargeNo.Checked = False
                rdbRechargeNoUpdate.Checked = False
            Else
                rdbRechargeNo.Checked = True
                rdbRechargeNoUpdate.Checked = True
                rdbRechargeYes.Checked = False
                rdbRechargeYesUpdate.Checked = False
            End If
        End If
        
        Dim isAppointmentSaved As Boolean

        If (commandArgumentArray(1).ToString() <> "") Then

            isAppointmentSaved = CType(commandArgumentArray(1).ToString(), Boolean)

        End If


        If (isAppointmentSaved) Then

            lblFaultLogIDUpdate.Text = FaultLogId
            lblFaultRepairIdUpdate.Text = repairId
            Me.ClearPostInspectionPopupFieldsUpdate()

            GetTeamLookUpValluesUpdate()
            rdbApprovedNoUpdate.Checked = False
            rdbApprovedYesUpdate.Checked = False

            Me.GetFaultValuesUpdate(faultLogId, repairId)
            Me.updatePanel_PostInspectionUpdate.Update()
            Me.mdlPostInspectPopup_Update.Show()

        Else
            lblFaultLogID.Text = FaultLogId
            lblFaultRepairID.Text = repairId
            Me.ClearPostInspectionPopupFields()
            GetTeamLookUpVallues()

            Me.GetFaultValues(FaultLogId)
            Me.updatePanel_Popup.Update()
            Me.mdlPostInspectPopup.Show()

        End If
        Me.updatePanel_ReportGrid.Update()


    End Sub

#End Region

#Region "GV Post Inspection Report Page Index Changing "

    Protected Sub GVPostInspectionReport_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        GVPostInspectionReport.PageIndex = e.NewPageIndex
        ViewState.Add(ApplicationConstants.PostInspectionReportInitialPageIndexKey, e.NewPageIndex)
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreResultSummary()
        Dim faultDS As DataSet = New DataSet()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.PostInspectionIsSearchViewState), Boolean) = True Then
            PopulateSearchResults()
            updatePanel_ReportGrid.Update()
        Else
            PopulateSearchResults()
            updatePanel_ReportGrid.Update()
        End If
    End Sub
#End Region
#End Region


#Region "GV Post Inspection Report Sorting "

    Protected Sub GVPostInspectionReport_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)

        ViewState.Add(ApplicationConstants.PostInspectionReportSortByViewState, e.SortExpression)

        If DirectCast(ViewState(ApplicationConstants.PostInspectionReportSortOrderViewSate), String) = ApplicationConstants.PostInspectionDESSortOrder Then

            ViewState(ApplicationConstants.PostInspectionReportSortOrderViewSate) = ApplicationConstants.PostInspectionASCSortOrder
        Else
            ViewState(ApplicationConstants.PostInspectionReportSortOrderViewSate) = ApplicationConstants.PostInspectionDESSortOrder

        End If
        PopulateSearchResults()
        updatePanel_ReportGrid.Update()

    End Sub
#End Region


#Region "Methods"

#Region "Populate lookup values"


    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", "0"))
            ddlLookup.DataBind()
        End If
    End Sub
#End Region

#Region "Get Operative Look Up Values "


    Private Sub GetOperativeLookUpValues(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlOperative, SprocNameConstants.GetUserLookup, inParam, inParamName)
        UpdatePanel_operative.Update()
    End Sub

    Private Sub GetOperativeLookUpValuesUpdate(ByVal inParam As String, ByVal inParamName As String)
        PopulateLookup(ddlOperativeUpdate, SprocNameConstants.GetUserLookup, inParam, inParamName)
        UpdatePanel_operativeUpdate.Update()
    End Sub


#End Region

#Region "Get Team Look Up Vallues "

    Private Sub GetTeamLookUpVallues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.GetPostInspectionTeamLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlTeam.Items.Clear()
            Me.ddlTeam.DataSource = lstLookUp
            Me.ddlTeam.DataValueField = "LookUpValue"
            Me.ddlTeam.DataTextField = "LookUpName"
            Me.ddlTeam.Items.Add(New ListItem("Please select", ""))
            Me.ddlTeam.DataBind()
        End If

        Me.ddlOperative.Items.Clear()
        Me.ddlOperative.Items.Add(New ListItem("Please select", ""))

    End Sub

    Private Sub GetTeamLookUpValluesUpdate()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.GetPostInspectionTeamLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlTeamUpdate.Items.Clear()
            Me.ddlTeamUpdate.DataSource = lstLookUp
            Me.ddlTeamUpdate.DataValueField = "LookUpValue"
            Me.ddlTeamUpdate.DataTextField = "LookUpName"
            Me.ddlTeamUpdate.Items.Add(New ListItem("Please select", ""))
            Me.ddlTeamUpdate.DataBind()
        End If

        Me.ddlOperativeUpdate.Items.Clear()
        Me.ddlOperativeUpdate.Items.Add(New ListItem("Please select", ""))

    End Sub
#End Region

#Region "Get Search Results "

    Protected Sub GetSearchResults()
        Try
            Dim postBO As PostInspectionBO = New PostInspectionBO()
            'reterive search options tbaSearchBO from viewstate before requesting results for next page of gridview
            postBO = DirectCast(ViewState(ApplicationConstants.PostInspectionReportSearchOptionsViewState), PostInspectionBO)
            Dim postDS As DataSet = New DataSet()
            'calling GetTBADataPage function that will further call function in buisness layer
            GetDataPage(GVPostInspectionReport.PageIndex, ApplicationConstants.PostInspectionReportResultPerPage, GVPostInspectionReport.OrderBy, postDS, postBO)
            'binding gridview with the result returned from db
            Me.StoreResultSummary()

            GVPostInspectionReport.DataSource = postDS
            GVPostInspectionReport.DataBind()
            EnableDisablePostInspectionButton(postDS)

            Dim i As Integer
            Dim btn As Button
            Dim updateStatus() As Integer = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

            For i = 0 To GVPostInspectionReport.Rows.Count - 1

                btn = CType(GVPostInspectionReport.Rows(i).Cells(12).FindControl("btnPostInspect"), Button)
                If (btn.Text = "Update") Then
                    updateStatus(i) = 1
                End If

            Next
            ViewState.Add(ApplicationConstants.PostInspectionGridButtonStatus, updateStatus)

            'Save Post Inspection GRid in in view State, so that to be retrieved later on. 
            ViewState.Add(ApplicationConstants.PostInspectionReportDataset, postDS)

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "Hide Post Inspection Empty Rows"

    Public Sub HidePostInspectionRows()

        Dim postInspectionStatus() As Integer
        Dim j As Integer
        Dim btn As Button

        Dim postDS As DataSet = New DataSet()
        GVPostInspectionReport.PageIndex = ViewState(ApplicationConstants.PostInspectionReportInitialPageIndexKey)
        postDS = CType(ViewState(ApplicationConstants.PostInspectionReportDataset), DataSet)
        GVPostInspectionReport.DataSource = postDS
        GVPostInspectionReport.DataBind()

        postInspectionStatus = CType(ViewState(ApplicationConstants.PostInspectionGridButtonStatus), Integer())
        For j = 0 To GVPostInspectionReport.Rows.Count - 1
            If (postInspectionStatus(j) = 1) Then
                btn = DirectCast(GVPostInspectionReport.Rows(j).Cells(12).FindControl("btnPostInspect"), Button)
                btn.Text = "Update"
            End If
        Next


    End Sub
#End Region

#Region "Enable Disable PostInspection Button"

    Public Sub EnableDisablePostInspectionButton(ByVal ds As DataSet)

        Dim btn As Button
        Dim i As Integer

        For i = 0 To ds.Tables(0).Rows.Count - 1

            If Not ds.Tables(0).Rows(i)("IsAppointmentSaved") Is Nothing Then

                If ds.Tables(0).Rows(i)("IsAppointmentSaved") <> False Then

                    btn = DirectCast(GVPostInspectionReport.Rows(i).Cells(12).FindControl("btnPostInspect"), Button)
                    btn.Text = "Update"
                    'updatePanel_ReportGrid.Update()


                End If
            End If
        Next
    End Sub

#End Region

#Region "Get Data Page "

    Public Sub GetDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef postDS As DataSet, ByVal postBO As PostInspectionBO)

        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.PostInspectionReportSortByViewState) Is Nothing) Then
            postBO.SortBy = CType(ViewState(ApplicationConstants.PostInspectionReportSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use fualtLogid as a default
            postBO.SortBy = ApplicationConstants.PostInspectionReportSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        postBO.PageIndex = pageIndex
        postBO.RowCount = pageSize

        'reteriving sort order from viewstate
        postBO.SortOrder = DirectCast(ViewState(ApplicationConstants.PostInspectionReportSortOrderViewSate), String)
        Dim conMngr As ContractorPortalManager = New ContractorPortalManager()
        'passing reference of dataset and passing tbaSerachBO to GetTBASearchResults function
        'of ContractorPortalManager
        postDS = conMngr.GetPostInspectionReportData(postBO)
        'storing number of rows in the result
        ViewState("NumberofRows") = postDS.Tables(0).Rows.Count

        ''Check for exception 
        Dim exceptionFlag As Boolean = postBO.IsExceptionGenerated
        If exceptionFlag Then
            Response.Redirect("~/error.aspx")
        End If

    End Sub
#End Region

#Region "Store Result Summary "

    Public Sub StoreResultSummary()
        Dim row As GridViewRow = GVPostInspectionReport.BottomPagerRow()
        Dim strResultSummary As String = ""

        If GVPostInspectionReport.PageIndex + 1 = GVPostInspectionReport.PageCount Then
            strResultSummary = "Result " & ((GVPostInspectionReport.PageIndex * GVPostInspectionReport.PageSize) + 1) & " to " & GVPostInspectionReport.VirtualItemCount & " of " & GVPostInspectionReport.VirtualItemCount
        Else
            strResultSummary = "Result " & ((GVPostInspectionReport.PageIndex * GVPostInspectionReport.PageSize) + 1) & " to " & ((GVPostInspectionReport.PageIndex + 1) * GVPostInspectionReport.PageSize) & " of " & GVPostInspectionReport.VirtualItemCount
        End If
        'save Page index & Page Size in View State
        ViewState.Add(ApplicationConstants.PostInspectionReportsPageIndexKey, GVPostInspectionReport.PageIndex)
        ViewState.Add(ApplicationConstants.PostInspectionReportsPageSizeKey, GVPostInspectionReport.PageSize)

        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.PostInspectionReportResultSummary) = strResultSummary
    End Sub
#End Region

#Region "Populate Search Results "


    Private Sub PopulateSearchResults()
        'save these new search options entered by user unless search button is pressed again
        SaveSearchOptions()
        'setting new starting page index of for this new search
        'GVPostInspectionReport.PageIndex = ApplicationConstants.PostInspectionReportInitialPageIndex
        'getting and setting row count of new search results

        GVPostInspectionReport.PageIndex = ViewState(ApplicationConstants.PostInspectionReportInitialPageIndexKey)
        GVPostInspectionReport.VirtualItemCount = GetSearchRowCount()


        'getting search results
        GetSearchResults()

    End Sub

#End Region

#Region "Get Search Row Count "

    Private Function GetSearchRowCount() As Integer
        Try
            'Defining object of business layer
            Dim objBAL As ContractorPortalManager = New ContractorPortalManager()
            'defining and fetchin SearchBO from view state
            Dim postBO As PostInspectionBO = New PostInspectionBO()
            postBO = DirectCast(ViewState(ApplicationConstants.PostInspectionReportSearchOptionsViewState), PostInspectionBO)
            'Calling the function of business layer and returning the row count by passing the current Search Options to it
            Return (objBAL.GetPostInspectRowCount(postBO))
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
            Return Nothing
        End Try

    End Function

#End Region

#Region "Save Search Options "

    Private Sub SaveSearchOptions()

        'GVPostInspectionReport.PageIndex = ViewState(ApplicationConstants.PostInspectionReportInitialPageIndexKey)
        Try
            'make FaultSearchBO object and store the search options entered by user untill he/she enters again
            'in this case the values will be overidden
            Dim postBO As PostInspectionBO = New PostInspectionBO()

            'tell EnquiryLogSearchBO it has values that will be used for searching
            postBO.IsSearch = True
            'store EnquiryLogSearchBO object in viewstate
            ViewState(ApplicationConstants.PostInspectionReportSearchOptionsViewState) = postBO
            'store true in viewstate to indicate that current data on gridview is from search
            ViewState(ApplicationConstants.PostInspectionIsSearchViewState) = True
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#Region "Clear Post Inspection Popup Fields "

    Public Sub ClearPostInspectionPopupFields()
        lblErrorMsg.Text = String.Empty
        Me.ddlTeam.SelectedValue = String.Empty
        'Me.ddlOperative.SelectedIndex = 0
        Try
            Me.ddlOperative.SelectedValue = String.Empty
        Catch ex As Exception

        End Try
        Me.txtInspect.Text = String.Empty
        'Me.txtDueDate.Text = String.Empty
        Me.txtNotes.Text = String.Empty
        'Me.rdbApprovedYes.Checked = True
        'Me.rdbApprovedNo.Checked = False
        Me.ddlPostInspectionHr.SelectedValue = "00"
        Me.ddlPostInspectionMnts.SelectedValue = "00"
        Me.ddlPostInspectionAMPM.SelectedValue = "0"

        ' Me.lblValidate.Enabled = True
        Me.ddlTeam.Enabled = True
        Me.ddlOperative.Enabled = True
        Me.txtInspect.Enabled = False
        Me.txtDueDate.Enabled = False
        Me.txtNotes.Enabled = True
        Me.txtCost.Enabled = True
        Me.ddlPostInspectionHr.Enabled = True
        Me.ddlPostInspectionMnts.Enabled = True
        Me.ddlPostInspectionAMPM.Enabled = True

        'Me.rdbApprovedYes.Enabled = True
        'Me.rdbApprovedNo.Enabled = True
        Me.btnSaveChanges.Enabled = True

    End Sub
#End Region

#Region "Clear Post Inspection Popup Fields Update"

    Public Sub ClearPostInspectionPopupFieldsUpdate()
        lblErrorMsgUpdate.Text = String.Empty
        Me.ddlTeamUpdate.SelectedValue = String.Empty
        Me.ddlOperativeUpdate.SelectedIndex = 0
        'Me.ddlOperativeUpdate.SelectedValue = String.Empty
        Me.txtInspectUpdate.Text = String.Empty
        Me.txtDueDateUpdate.Text = String.Empty
        Me.txtNotesUpdate.Text = String.Empty
        Me.ddlPostInspectionHrUpdate.SelectedValue = "00"
        Me.ddlPostInspectionMntsUpdate.SelectedValue = "00"
        Me.ddlPostInspectionAMPMUpdate.SelectedValue = "AM"
        Me.txtCostUpdate.Enabled = True
        Me.rdbRechargeYesUpdate.Enabled = True
        Me.rdbRechargeNoUpdate.Enabled = True
        Me.txtReason.Text = String.Empty


        Me.rdbApprovedYesUpdate.Checked = True
        'Me.rdbApprovedNoUpdate.Checked = False
        Me.ddlTeamUpdate.Enabled = True
        Me.ddlOperativeUpdate.Enabled = True
        Me.txtInspectUpdate.Enabled = False
        Me.txtDueDateUpdate.Enabled = False
        Me.txtNotesUpdate.Enabled = True
        Me.ddlPostInspectionHrUpdate.Enabled = True
        Me.ddlPostInspectionMntsUpdate.Enabled = True
        Me.ddlPostInspectionAMPMUpdate.Enabled = True
        Me.btnInspectionCalanderUpdate.Enabled = True
        Me.btnImgDueDate.Enabled = True
        Me.txtReason.Enabled = True

        Me.rdbApprovedYesUpdate.Enabled = True
        'Me.rdbApprovedNoUpdate.Enabled = True
        Me.lblSaveChangesUpdate.Enabled = True

    End Sub
#End Region

#Region "Get Fault Values "

    Private Sub GetFaultValues(ByVal FaultID As Int32)

        Dim faultBO As FaultBO = New FaultBO()
        Try
            Dim objFault As FaultManager = New FaultManager()

            Dim faultDS As New DataSet()

            faultBO.FaultID = FaultID
            objFault.GetPostInspecitonFault(faultDS, faultBO)

            If (faultDS.Tables(0).Rows.Count > 0) Or Not (faultDS Is Nothing) Then

                Dim isRecharge As Boolean = CType(faultDS.Tables(0).Rows(0)("Recharge"), Boolean)                
                Dim strResponseTime As Double = 0.0
                Dim dueDate As DateTime = Nothing
                Dim submitDate As DateTime = System.DateTime.Now.ToShortDateString()

                Dim dayHour As String = String.Empty

                strResponseTime = Convert.ToDouble(faultDS.Tables(0).Rows(0)("ResponseTime"))
                dayHour = faultDS.Tables(0).Rows(0)("responseDays").ToString()

                If (dayHour = "Days") Then
                    dueDate = submitDate.Add(New TimeSpan(strResponseTime, 0, 0, 0)).ToString()
                ElseIf (dayHour = "Hours") Then
                    dueDate = submitDate.Add(New TimeSpan(strResponseTime, 0, 0)).ToString()
                End If

                Me.txtDueDate.Text = UtilityFunctions.FormatDate(dueDate.ToString())


            End If


        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

        End Try
    End Sub


    'To Populate the PostInspectio Popu Update data 

    Private Sub GetFaultValuesUpdate(ByVal faultId As Integer, ByVal repairId As Integer)

        Dim faultBO As FaultBO = New FaultBO()
        Try
            Dim objFault As FaultManager = New FaultManager()

            Dim faultDS As New DataSet()
            Dim postInspectionPopupDS As New DataSet()

            faultBO.FaultID = faultId
            faultBO.RepairId = repairId

            objFault.GetPostInspecitonFault(faultDS, faultBO)
            objFault.GetPostInspecitonFaultUpdate(postInspectionPopupDS, faultBO)

            If (faultDS.Tables(0).Rows.Count > 0) Or Not (faultDS Is Nothing) Then

                Dim strNetCost As String = CType(faultDS.Tables(0).Rows(0)("NetCost"), Decimal).ToString("0.00")
                Me.txtCostUpdate.Text = strNetCost.ToString()
                Dim strResponseTime As Double = 0.0
                Dim dueDate As DateTime = Nothing
                Dim submitDate As DateTime = System.DateTime.Now.ToShortDateString()

                Dim dayHour As String = String.Empty

                strResponseTime = Convert.ToDouble(faultDS.Tables(0).Rows(0)("ResponseTime"))
                dayHour = faultDS.Tables(0).Rows(0)("responseDays").ToString()

                If (dayHour = "Days") Then
                    dueDate = submitDate.Add(New TimeSpan(strResponseTime, 0, 0, 0)).ToString()
                ElseIf (dayHour = "Hours") Then
                    dueDate = submitDate.Add(New TimeSpan(strResponseTime, 0, 0)).ToString()
                End If

                Me.txtDueDateUpdate.Text = UtilityFunctions.FormatDate(dueDate.ToString())

            End If

            If postInspectionPopupDS.Tables(0).Rows.Count > 0 Then

                Dim isApproved As Boolean
                Dim isRecharge As Boolean
                Dim inspectionTime As String
                Dim inspectionTimeArray As Array
                Dim amPmTimeSlot As String
                Dim hourSlot As String
                Dim minuteSlot As String

                inspectionTime = postInspectionPopupDS.Tables(0).Rows(0)("InspectionTime").ToString()
                inspectionTimeArray = inspectionTime.Split(" ")
                amPmTimeSlot = inspectionTimeArray(1)
                inspectionTimeArray = inspectionTimeArray(0).ToString().Split(":")
                hourSlot = inspectionTimeArray(0)
                minuteSlot = inspectionTimeArray(1)

                Me.ddlPostInspectionHrUpdate.SelectedValue = hourSlot
                Me.ddlPostInspectionMntsUpdate.SelectedValue = minuteSlot
                Me.ddlPostInspectionAMPMUpdate.SelectedValue = amPmTimeSlot

                Me.ddlTeamUpdate.SelectedValue = postInspectionPopupDS.Tables(0).Rows(0)("Team").ToString()
                Me.ddlOperativeUpdate.Items.Clear()
                
                Me.ddlOperativeUpdate.Items.Add(New ListItem(postInspectionPopupDS.Tables(0).Rows(0)("UserName").ToString(), postInspectionPopupDS.Tables(0).Rows(0)("Operative").ToString()))
                Me.txtCostUpdate.Text = CType(postInspectionPopupDS.Tables(0).Rows(0)("NETCOST"), Decimal).ToString("0.00")
                Me.txtInspectUpdate.Text = UtilityFunctions.FormatDate(postInspectionPopupDS.Tables(0).Rows(0)("INSPECTIONDATE").ToString())
                Me.txtDueDateUpdate.Text = UtilityFunctions.FormatDate(postInspectionPopupDS.Tables(0).Rows(0)("DUEDATE").ToString())
                Me.txtNotesUpdate.Text = postInspectionPopupDS.Tables(0).Rows(0)("NOTES").ToString()
                Me.txtReason.Text = postInspectionPopupDS.Tables(0).Rows(0)("Reason").ToString()


                isApproved = postInspectionPopupDS.Tables(0).Rows(0)("Approved")
                isRecharge = postInspectionPopupDS.Tables(0).Rows(0)("Recharge")

                If (isRecharge) Then
                    rdbRechargeYesUpdate.Checked = True
                    rdbRechargeNoUpdate.Checked = False
                Else
                    rdbRechargeNoUpdate.Checked = True
                    rdbRechargeYesUpdate.Checked = False
                End If


                If (isApproved) Then
                    rdbApprovedYesUpdate.Checked = True
                    DisablePostInspectionPopupFields()
                End If


            End If

        Catch ex As Exception
            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

        End Try
    End Sub
#End Region

#Region "Disable Post Inspection Popup Fields"
    Public Sub DisablePostInspectionPopupFields()
        Me.ddlTeamUpdate.Enabled = False
        Me.ddlOperativeUpdate.Enabled = False
        Me.txtInspectUpdate.Enabled = False
        Me.txtDueDateUpdate.Enabled = False
        Me.txtNotesUpdate.Enabled = False
        Me.lblSaveChangesUpdate.Enabled = False
        Me.rdbRechargeNoUpdate.Enabled = False
        Me.rdbRechargeYesUpdate.Enabled = False
        Me.rdbApprovedYesUpdate.Enabled = False
        Me.rdbApprovedNoUpdate.Enabled = False
        Me.ddlPostInspectionHrUpdate.Enabled = False
        Me.ddlPostInspectionMntsUpdate.Enabled = False
        Me.ddlPostInspectionAMPMUpdate.Enabled = False
        Me.txtNotesUpdate.Enabled = False
        Me.txtReason.Enabled = False

        Me.btnInspectionCalanderUpdate.Enabled = False
        Me.btnImgDueDate.Enabled = False

        Me.txtCostUpdate.Enabled = False
        Me.txtDueDateUpdate.Enabled = False
    End Sub

#End Region


#Region "Save PostInspection Record"
    Private Sub SavePostInspectionRecord()

        GVPostInspectionReport.PageIndex = ViewState(ApplicationConstants.PostInspectionReportInitialPageIndexKey)
        Try
            Dim piBO As PreInspectBO = New PreInspectBO()
            'fill piBO attribute from inspection data
            If Me.ddlOperative.SelectedValue <> String.Empty Then
                piBO.UserId = CType(ddlOperative.SelectedValue, Integer)
            End If

            piBO.NetCost = CType(txtCost.Text, Double)
            piBO.Notes = txtNotes.Text

            If Me.txtInspect.Text <> String.Empty Then
                piBO.InspectionDate = UtilityFunctions.stringToDate(txtInspect.Text)
            End If
            If Me.txtDueDate.Text <> String.Empty Then
                piBO.DueDate = UtilityFunctions.stringToDate(txtDueDate.Text)
            End If

            If ddlPostInspectionHr.SelectedValue <> "00" Or ddlPostInspectionMnts.SelectedValue <> "00" Then

                Dim time As String = ddlPostInspectionHr.SelectedValue.ToString() & ":" & ddlPostInspectionMnts.SelectedValue.ToString() & " " & ddlPostInspectionAMPM.SelectedValue.ToString()
                piBO.Time = time.ToString.Trim()
            Else
                piBO.Time = Nothing

            End If

            If rdbRechargeYes.Checked = True Then
                piBO.Recharge = 1

            Else
                piBO.Recharge = 0
            End If


            piBO.TempFaultId = CType(lblFaultLogID.Text, Integer)
            piBO.RepairId = CType(lblFaultRepairID.Text, Integer)

            Dim piBL As ContractorPortalManager = New ContractorPortalManager()
            piBL.SavePostInspectionRecord(piBO)

            ''
            updatePanel_Popup.Update()
            PopulateSearchResults()
            updatePanel_ReportGrid.Update()


            If (piBO.IsExceptionGenerated = True) Then
                Response.Redirect("~/error.aspx")
            Else

                lblErrorMsg.Text = "PostInspection Saved"

            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

   
#End Region

#Region "Save Post Inspection Record Update"


    Private Sub SavePostInspectionRecordUpdate()

        GVPostInspectionReport.PageIndex = ViewState(ApplicationConstants.PostInspectionReportInitialPageIndexKey)
        Try
            Dim piBO As PreInspectBO = New PreInspectBO()
            'fill piBO attribute from inspection data
            If Me.ddlOperativeUpdate.SelectedValue <> String.Empty Then
                piBO.UserId = CType(ddlOperativeUpdate.SelectedValue, Integer)
            End If

            piBO.NetCost = CType(txtCostUpdate.Text, Double)
            piBO.Notes = txtNotesUpdate.Text

            If Me.txtInspectUpdate.Text <> String.Empty Then
                piBO.InspectionDate = UtilityFunctions.stringToDate(txtInspectUpdate.Text)
            End If
            If Me.txtDueDateUpdate.Text <> String.Empty Then
                piBO.DueDate = UtilityFunctions.stringToDate(txtDueDateUpdate.Text)
            End If

            If ddlPostInspectionHrUpdate.SelectedValue <> "00" Or ddlPostInspectionMntsUpdate.SelectedValue <> "00" Then

                Dim time As String = ddlPostInspectionHrUpdate.SelectedValue.ToString() & ":" & ddlPostInspectionMntsUpdate.SelectedValue.ToString() & " " & ddlPostInspectionAMPMUpdate.SelectedValue.ToString()
                piBO.Time = time.ToString.Trim()
            Else
                piBO.Time = Nothing

            End If

            ''
            If Me.rdbApprovedYesUpdate.Checked = True Then
                piBO.Approved = 1
            Else
                piBO.Approved = 0
            End If
            ''

            If Me.rdbRechargeYesUpdate.Checked = True Then
                piBO.Recharge = 1
            Else
                piBO.Recharge = 0
            End If

            piBO.Reason = txtReason.Text

            piBO.TempFaultId = CType(lblFaultLogIDUpdate.Text, Integer)
            piBO.RepairId = CType(lblFaultRepairIdUpdate.Text, Integer)

            Dim piBL As ContractorPortalManager = New ContractorPortalManager()

            piBL.SavePostInspectionRecordUpdate(piBO)
            PopulateSearchResults()

            If (piBO.IsExceptionGenerated = True) Then
                Response.Redirect("~/error.aspx")
            Else

                lblErrorMsg.Text = "PostInspection Saved"

            End If

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub

#End Region

#End Region

#Region "btn Save Changes Update Click"
    Protected Sub lblSaveChangesUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        ValidatePreInspectionPopupUpdate()

    End Sub
#End Region

End Class