<%@ Page Language="vb" AutoEventWireup="false"    ValidateRequest = "false"   EnableEventValidation = "false"     MasterPageFile="~/master pages/FaultManagement.Master" CodeBehind="postinspection_report.aspx.vb" Inherits="tenantsonline.postinspection_report" %>
    
    <%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
    <script runat = "server">
    
        Public Function CommandArgument(ByVal faultId As Integer, ByVal isAppointmentSave As String, ByVal recharge As String, ByVal repairId As String, ByVal netCost As String, ByVal newRecharge As String)
            
            Return faultId & "," & isAppointmentSave & "," & recharge & "," & repairId & "," & netCost & "," & newRecharge
            
        End Function
    
    </script>
         
    
<asp:Content ID="Content1" ContentPlaceHolderID="page_area" runat="server">
    <cc2:ToolkitScriptManager ID="Registration_ToolkitScriptManager" runat="server">
    </cc2:ToolkitScriptManager>
    <table style="width: 100%">
        <tr>
            <td style="width: 100px; height: 21px;">
    <asp:Label ID="lblPreInspecHeading" runat="server" BackColor="White" Font-Bold="True"
        Font-Names="Arial" ForeColor="Black" Text="Post Inspection Management" Width="296px"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 100px">
    <asp:UpdatePanel ID="updatePanel_ReportGrid" runat="server" UpdateMode="Conditional" RenderMode="Inline" ChildrenAsTriggers="False">
        <ContentTemplate>
            <cc1:PagingGridView ID="GVPostInspectionReport" runat="server" AutoGenerateColumns="False"
                BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderStyle="Dotted" BorderWidth="1px"
                CellPadding="2" EmptyDataText="No record exists" Font-Names="Arial" Font-Size="Small"
                ForeColor="Black" GridLines="None" VirtualItemCount="-1"
                Width="99%" OnPageIndexChanging="GVPostInspectionReport_PageIndexChanging" AllowPaging="True" AllowSorting="True" OnSorting="GVPostInspectionReport_Sorting">
                <PagerSettings Mode="NumericFirstLast" />
                <FooterStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Date" InsertVisible="False" SortExpression="FL_FAULT_LOG.DueDate">
                        <itemtemplate>
<asp:Label id="RFLblCreationDate" runat="server" Text='<%# Format(Eval("DueDate"),"dd/MM/yy") %>' CssClass="cellData" __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="JS No" SortExpression="FL_FAULT_LOG.JobSheetNumber">
                        <itemtemplate>
<asp:Label id="lblJsNumber" runat="server" Text='<%# Bind("JobSheetNumber") %>' __designer:wfdid="w2"></asp:Label>
</itemtemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer" InsertVisible="False" SortExpression="C__CUSTOMER.FIRSTNAME">
                        <itemtemplate>
<asp:Label id="RFLblCustomer" runat="server" Text='<%# UtilityFunctions.FormatName( Eval("FIRSTNAME").ToString() , Eval("LASTNAME").ToString() ) %>' CssClass="cellData" __designer:wfdid="w48"></asp:Label> 
</itemtemplate>
                        <headerstyle horizontalalign="Left" wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address" SortExpression="P__PROPERTY.ADDRESS1">
                        <itemtemplate>
<asp:Label id="Label2" runat="server" Text='<%# Eval("COMPLETEADDRESS") %>' __designer:wfdid="w2"></asp:Label> 
</itemtemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Area/Item" InsertVisible="False" SortExpression="FL_AREA.AreaName">
                        <itemtemplate>
<asp:Label id="RFLblAreaItem" runat="server" Text='<%# UtilityFunctions.FormatAreaElement( Eval("AreaName").ToString() , Eval("ElementName").ToString() ) %>' CssClass="cellData" __designer:wfdid="w1"></asp:Label> 
</itemtemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fault" InsertVisible="False" SortExpression="FL_FAULT.Description">
                        <itemtemplate>
<asp:Label id="RFLblDetails" runat="server" Text='<%# Eval("Description") %>' CssClass="cellData" __designer:wfdid="w50"></asp:Label> 
</itemtemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Qty" InsertVisible="False" SortExpression="FL_FAULT_LOG.Quantity">
                        <itemtemplate>
<asp:Label id="RFLblQty" runat="server" Text='<%# Eval("Quantity") %>' CssClass="cellData" __designer:wfdid="w51"></asp:Label> 
</itemtemplate>
                        <headerstyle font-underline="True" horizontalalign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Repairs" SortExpression="FL_FAULT_REPAIR_LIST.Description">
                        <itemtemplate>
<asp:Label id="lblRepairDetails" runat="server" Text='<%# Bind("RepairDetails") %>' __designer:wfdid="w1"></asp:Label>
</itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Priority" SortExpression="FL_FAULT_PRIORITY.PriorityName">
                        <itemtemplate>
<asp:Label id="RFLblPriority" runat="server" CssClass="cellData" Text='<%# Eval("PriorityTime") %>' __designer:wfdid="w52"></asp:Label> 
</itemtemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appointment" SortExpression="FL_CO_FAULTLOG_TO_REPAIR.InspectionDate">
                        <itemtemplate>
<asp:Label id="lblAppointmentDate" runat="server" Text='<%# Bind("AppointmentDate") %>' __designer:wfdid="w1"></asp:Label>
</itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surveyor" SortExpression="E__EMPLOYEE.FIRSTNAME">
                        <itemtemplate>
<asp:Label id="lblSurveyor" runat="server" Text='<%# Bind("Surveyor") %>' __designer:wfdid="w2"></asp:Label> 
</itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" SortExpression="FL_FAULT_STATUS.Description">
                        <itemtemplate>
<asp:Label id="RFLblStatus" runat="server" Text='<%# Eval("Status") %>' CssClass="cellData" __designer:wfdid="w53"></asp:Label> 
</itemtemplate>
                        <headerstyle wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <itemtemplate>
<asp:Button id="btnPostInspect" onclick="btnPostInspect_Click" runat="server" Text="Arrange Inspection" UseSubmitBehavior="False" CommandArgument='<%# CommandArgument(Eval("FaultLogID").ToString(), Eval("ISAppointmentSaved").ToString(), Eval("Recharge").ToString(), Eval("FaultRepairListID").ToString(), Eval("NetCost").ToString(), Eval("NewRecharge").ToString()) %>' __designer:wfdid="w1"></asp:Button> 
</itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contractor" SortExpression="S_ORGANISATION.NAME">
                        <itemtemplate>
<asp:Label id="RFLblContractor" runat="server" Text='<%# Eval("Contractor") %>' CssClass="cellData" __designer:wfdid="w55"></asp:Label> 
</itemtemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle BackColor="#EFF3FB" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <PagerStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Left" />
                <HeaderStyle BackColor="#C00000" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </cc1:PagingGridView>
            <br />
            <br />
                        <asp:UpdatePanel ID="updatePanel_Popup" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="1" cellspacing="1" style="width: 400px; background-color: white">
                <tbody align="left">
                    <tr>
                        <td align="left" colspan="10" style="height: 21px; background-color: #c00000">
                            &nbsp;
                            <asp:Label ID="lblPostInspectionHeading" runat="server" BackColor="#C00000" Font-Bold="True"
                                Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Post Inspection"
                                Width="66%"></asp:Label>
                            <asp:LinkButton ID="lnkBtnPseudo" runat="server"></asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="10" style="height: 21px">
                            <asp:UpdatePanel ID="updatePanel_ErrorMessage" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    &nbsp;<asp:Label ID="lblErrorMsg" runat="server" CssClass="error_message_label" Width="224px"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="Label1" runat="server" CssClass="caption" Text="Team: *"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel_team" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged" Width="203px">
                                    </asp:DropDownList>
                                    <asp:UpdateProgress ID="UpdateProgress_popupteam" runat="server" DisplayAfter="10">
                                        <ProgressTemplate>
                                            <asp:Panel ID="poppnlProgressTeam" runat="server" BackColor="Silver" Font-Names="Arial"
                                                Font-Size="Small" Height="40px" HorizontalAlign="Center" Width="221px">
                                                <asp:Image ID="imgPopProgressBarTeam" runat="server" ImageUrl="~/images/buttons/ajax-loader.gif" /><br />
                                                Loading ...
                                            </asp:Panel>
                                            &nbsp; &nbsp;
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblOperative" runat="server" Text="Operative: *" CssClass="caption"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel_operative" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlOperative" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                        Width="203px">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlTeam" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblTimePostInspection" runat="server" Text="Time: *" CssClass="caption"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:DropDownList ID="ddlPostInspectionHr" runat="server" Width="56px">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem>09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>&nbsp; <asp:DropDownList ID="ddlPostInspectionMnts" runat="server"
                                Width="56px">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem>09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                                <asp:ListItem>24</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>26</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>28</asp:ListItem>
                                <asp:ListItem>29</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                                <asp:ListItem>31</asp:ListItem>
                                <asp:ListItem>32</asp:ListItem>
                                <asp:ListItem>33</asp:ListItem>
                                <asp:ListItem>34</asp:ListItem>
                                <asp:ListItem>35</asp:ListItem>
                                <asp:ListItem>36</asp:ListItem>
                                <asp:ListItem>37</asp:ListItem>
                                <asp:ListItem>38</asp:ListItem>
                                <asp:ListItem>39</asp:ListItem>
                                <asp:ListItem>40</asp:ListItem>
                                <asp:ListItem>41</asp:ListItem>
                                <asp:ListItem>42</asp:ListItem>
                                <asp:ListItem>43</asp:ListItem>
                                <asp:ListItem>44</asp:ListItem>
                                <asp:ListItem>45</asp:ListItem>
                                <asp:ListItem>46</asp:ListItem>
                                <asp:ListItem>47</asp:ListItem>
                                <asp:ListItem>48</asp:ListItem>
                                <asp:ListItem>49</asp:ListItem>
                                <asp:ListItem>50</asp:ListItem>
                                <asp:ListItem>51</asp:ListItem>
                                <asp:ListItem>52</asp:ListItem>
                                <asp:ListItem>53</asp:ListItem>
                                <asp:ListItem>54</asp:ListItem>
                                <asp:ListItem>55</asp:ListItem>
                                <asp:ListItem>56</asp:ListItem>
                                <asp:ListItem>57</asp:ListItem>
                                <asp:ListItem>58</asp:ListItem>
                                <asp:ListItem>59</asp:ListItem>
                            </asp:DropDownList>&nbsp; <asp:DropDownList ID="ddlPostInspectionAMPM" runat="server" Width="56px">
                                <asp:ListItem Selected="True" Value="AM">AM</asp:ListItem>
                                <asp:ListItem>PM</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblInspectionDate" runat="server" Text="Inspection Date: *" Width="128px" CssClass="caption"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtInspect" runat="server" Enabled="False" Width="203px"></asp:TextBox>
                            <asp:ImageButton ID="btnInspectionCalander" runat="server" CausesValidation="False"
                                ImageUrl="~/images/buttons/Calendar_button.png" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblDueDate" runat="server" CssClass="caption" Text="Repair Completion:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtDueDate" runat="server" Enabled="False" Width="203px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblNotes" runat="server" CssClass="caption" Text="Notes:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtNotes" runat="server" Height="72px" TextMode="MultiLine" Width="203px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px; height: 22px">
                            <asp:Label ID="lblRecharge" runat="server" CssClass="caption" Text="Recharge"></asp:Label></td>
                        <td align="center" colspan="1" style="width: 152px; height: 22px">
                            <asp:RadioButton ID="rdbRechargeYes" runat="server" CssClass="caption"
                                GroupName="rdbRecharge" Text="Y" />&nbsp;</td>
                        <td align="left" colspan="1" style="width: 175px; height: 22px">
                            <asp:RadioButton ID="rdbRechargeNo" runat="server" CssClass="caption"
                                GroupName="rdbRecharge" Text="N" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblCost" runat="server" CssClass="caption" Text="Cost(Net)�:" Width="88px"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtCost" runat="server" Width="203px" Enabled="False">0.0</asp:TextBox><br />
                            <cc2:FilteredTextBoxExtender ID="fltrNetCost" runat="server" TargetControlID="txtCost" FilterType="Numbers, Custom" ValidChars = ".">
                            </cc2:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblFaultLogID" runat="server" Visible="False"></asp:Label><br />
                            <asp:Label ID="lblFaultRepairID" runat="server" Visible="False"></asp:Label></td>
                        <td align="left" colspan="2" valign="top">
                            &nbsp;<table>
                                <tr>
                                    <td style="width: 100px" valign="top">
                                        <asp:Button ID="btnCancel" runat="server" CssClass="caption" Text="Cancel" /></td>
                                    <td style="width: 100px">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="btnSaveChanges" runat="server" CssClass="caption" OnClick="btnSaveChanges_Click"
                                                    Text="Save Appointment" Width="128px" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <cc2:ModalPopupExtender ID="mdlPostInspectPopup" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="btnCancel" Drag="false" PopupControlID="updatePanel_Popup"
                TargetControlID="txtDueDate">
            </cc2:ModalPopupExtender>
            <cc2:CalendarExtender ID="CalendarExtender_Inspection" runat="server" Format="dd/MM/yyyy"
                PopupButtonID="btnInspectionCalander" TargetControlID="txtInspect">
            </cc2:CalendarExtender>
            &nbsp; &nbsp;&nbsp;
        </ContentTemplate>
    </asp:UpdatePanel>
            <br />
                <asp:UpdatePanel ID="updatePanel_PostInspectionUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="1" cellspacing="1" style="width: 400px; background-color: white">
                <tbody align="left">
                    <tr>
                        <td align="left" colspan="10" style="height: 21px; background-color: #c00000">
                            &nbsp;
                            <asp:Label ID="lblPostInspectionHeadingUpdate" runat="server" BackColor="#C00000" Font-Bold="True"
                                Font-Names="Arial" Font-Size="Small" ForeColor="White" Height="19px" Text="Update Post Inspection"
                                Width="66%"></asp:Label>
                            <asp:LinkButton ID="lnkBtnPseudoUpdate" runat="server"></asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="10">
                            <asp:UpdatePanel ID="updatePanel_ErrorMessageUpdate" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    &nbsp;<asp:Label ID="lblErrorMsgUpdate" runat="server" CssClass="error_message_label"
                                        Width="224px"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="LblTeamUpdate" runat="server" CssClass="caption" Text="Team: *"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel_teamUpdate" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlTeamUpdate" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlTeamUpdate_SelectedIndexChanged1" Width="203px">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblOperativeUpdate" runat="server" CssClass="caption" Text="Operative: *"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel_operativeUpdate" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlOperativeUpdate" runat="server" AppendDataBoundItems="True"
                                        AutoPostBack="True" Width="203px">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlTeamUpdate" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblTimePostInspectionUpdate" runat="server" Text="Time: *" CssClass="caption"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:DropDownList ID="ddlPostInspectionHrUpdate" runat="server" Width="56px">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem>09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlPostInspectionMntsUpdate" runat="server"
                                Width="56px">
                                <asp:ListItem>00</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem>09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                                <asp:ListItem>24</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>26</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>28</asp:ListItem>
                                <asp:ListItem>29</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                                <asp:ListItem>31</asp:ListItem>
                                <asp:ListItem>32</asp:ListItem>
                                <asp:ListItem>33</asp:ListItem>
                                <asp:ListItem>34</asp:ListItem>
                                <asp:ListItem>35</asp:ListItem>
                                <asp:ListItem>36</asp:ListItem>
                                <asp:ListItem>37</asp:ListItem>
                                <asp:ListItem>38</asp:ListItem>
                                <asp:ListItem>39</asp:ListItem>
                                <asp:ListItem>40</asp:ListItem>
                                <asp:ListItem>41</asp:ListItem>
                                <asp:ListItem>42</asp:ListItem>
                                <asp:ListItem>43</asp:ListItem>
                                <asp:ListItem>44</asp:ListItem>
                                <asp:ListItem>45</asp:ListItem>
                                <asp:ListItem>46</asp:ListItem>
                                <asp:ListItem>47</asp:ListItem>
                                <asp:ListItem>48</asp:ListItem>
                                <asp:ListItem>49</asp:ListItem>
                                <asp:ListItem>50</asp:ListItem>
                                <asp:ListItem>51</asp:ListItem>
                                <asp:ListItem>52</asp:ListItem>
                                <asp:ListItem>53</asp:ListItem>
                                <asp:ListItem>54</asp:ListItem>
                                <asp:ListItem>55</asp:ListItem>
                                <asp:ListItem>56</asp:ListItem>
                                <asp:ListItem>57</asp:ListItem>
                                <asp:ListItem>58</asp:ListItem>
                                <asp:ListItem>59</asp:ListItem>
                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlPostInspectionAMPMUpdate" runat="server" Width="56px">
                                <asp:ListItem Selected="True" Value="AM">AM</asp:ListItem>
                                <asp:ListItem>PM</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblInspectionDateUpdate" runat="server" CssClass="caption" Text="Inspection Date: *"
                                Width="128px"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtInspectUpdate" runat="server" Enabled="False" Width="203px"></asp:TextBox>
                            <asp:ImageButton ID="btnInspectionCalanderUpdate" runat="server" CausesValidation="False"
                                ImageUrl="~/images/buttons/Calendar_button.png" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblDueDateUpdate" runat="server" CssClass="caption" Text="Repair Completion:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtDueDateUpdate" runat="server" Enabled="False" Width="203px"></asp:TextBox>
                            <asp:ImageButton ID="btnImgDueDate" runat="server" CausesValidation="False"
                                ImageUrl="~/images/buttons/Calendar_button.png" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblNotesUpdate" runat="server" CssClass="caption" Text="Notes:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtNotesUpdate" runat="server" Height="72px" TextMode="MultiLine"
                                Width="203px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px;">
                            <asp:Label ID="lblRechargeUpdate" runat="server" CssClass="caption" Text="Recharge"></asp:Label></td>
                        <td align="center" colspan="1" style="width: 152px;">
                            <asp:RadioButton ID="rdbRechargeYesUpdate" runat="server" CssClass="caption"
                                GroupName="rdbRecharge" Text="Y" />&nbsp;</td>
                        <td align="left" colspan="1" style="width: 164px;">
                            <asp:RadioButton ID="rdbRechargeNoUpdate" runat="server" CssClass="caption"
                                GroupName="rdbRecharge" Text="N" /></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblCostUpdate" runat="server" CssClass="caption" Text="Cost(Net)�:"
                                Width="88px"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtCostUpdate" runat="server" Width="203px">0.0</asp:TextBox><br />
                            <cc2:FilteredTextBoxExtender ID="fltrDueDate" runat="server" TargetControlID="txtCostUpdate" FilterType = "Numbers, Custom" ValidChars = ".">
                            </cc2:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px;">
                            <asp:Label ID="lblApprovedUpdate" runat="server" CssClass="caption" Text="Approved:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" style="width: 137px; height: 20px;">
                                        <asp:RadioButton ID="rdbApprovedYesUpdate" runat="server"
                                                    CssClass="caption" GroupName="rdbApproved" Text="Y" /></td>
                                    <td style="width: 104px; height: 20px;">
                                        <asp:RadioButton ID="rdbApprovedNoUpdate" runat="server" CssClass="caption"
                                                    GroupName="rdbApproved" Text="N" Enabled="False" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                            <asp:Label ID="lblReason" runat="server" CssClass="caption" Text="Reason:"></asp:Label></td>
                        <td align="left" colspan="2">
                            <asp:TextBox ID="txtReason" runat="server" Height="72px" Rows="5" TextMode="MultiLine"
                                Width="203px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="8" style="width: 152px">
                        </td>
                        <td align="left" colspan="2">
                            <table>
                                <tr>
                                    <td style="width: 100px; height: 97px;" valign="top">
                                        <asp:Button ID="btnCancelUpdate" runat="server" CssClass="caption" Text="Cancel" /></td>
                                    <td style="width: 100px; height: 97px;" valign="top">
                                        <asp:UpdatePanel ID="updatePanel_UpdatePostInspection" runat="server">
                                            <ContentTemplate>
                                                <asp:Button ID="lblSaveChangesUpdate" runat="server" OnClick="lblSaveChangesUpdate_Click"
                                                    Text="Save Updates" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                            <asp:Label ID="lblFaultLogIDUpdate" runat="server" Visible="False"></asp:Label>
                                        <asp:Label ID="lblFaultRepairIdUpdate" runat="server" Visible="False"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <cc2:ModalPopupExtender ID="mdlPostInspectPopup_Update" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="btnCancel" Drag="false" PopupControlID="updatePanel_PostInspectionUpdate"
                TargetControlID="lblApprovedUpdate" >
            </cc2:ModalPopupExtender>
            <cc2:CalendarExtender ID="CalendarExtender_Inspection_Update" runat="server" Format="dd/MM/yyyy"
                PopupButtonID="btnInspectionCalanderUpdate" TargetControlID="txtInspectUpdate">
            </cc2:CalendarExtender>
            <cc2:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="btnImgDueDate"
                TargetControlID="txtDueDateUpdate">
            </cc2:CalendarExtender>
            <br />
            &nbsp; &nbsp;&nbsp;
        </ContentTemplate>
    </asp:UpdatePanel>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td style="width: 100px" rowspan="2">
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            </td>
        </tr>
        <tr>
        </tr>
    </table>
    <br />
    <br />
    <br />
    &nbsp; &nbsp;<br />
    &nbsp; &nbsp;<br />
    <br />
    <br />
    <br />
    <br />
    <br />
    

</asp:Content>
