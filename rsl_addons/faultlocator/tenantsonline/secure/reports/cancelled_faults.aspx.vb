Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Partial Public Class cancelled_faults
    Inherits System.Web.UI.Page
    'Inherits MSDN.SessionPage

#Region "Page Control Events"
#Region "Page Init"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        'If IsNothing(ASPSession("USERID")) Then
        'Response.Redirect("~/../BHAIntranet/login.aspx")
        'End If

    End Sub

#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not Me.IsPostBack Then

            ''Populate status dropdown with values for search
            GetStatusLookUpValues()
            'storing in view state boolean value indicating , results being displayed in gridview are 
            'from search or not
            ViewState(ApplicationConstants.CancelledJSIsSearchViewState) = True
            'storing current sort order of any column asc or dsen
            ViewState(ApplicationConstants.CancelledJSSortOrderViewState) = ApplicationConstants.CancelledJSDESCSortOrder
            'function used for storing search parameters which will be used for further page requests if result is 
            'multipage
            SaveSearchOptions()
            'setting initial page index of gridview by default its zero
            grvCancelledFaults.PageIndex = ApplicationConstants.CancelledJSInitialPageIndex
            'getting and setting row count of resultset
            grvCancelledFaults.VirtualItemCount = GetSearchRowCount()
            ''''''''''''''''''''''''''
            txtFirstName.Focus()
            ''Populate Grid for the First Time
            ' BindFaultGrid()
            PopulateSearchResults()

        End If





    End Sub
#End Region
#End Region

#Region "Events "

#Region "Button Clicks "
    Protected Sub btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PopulateSearchResults()
    End Sub
#End Region

#Region "Grid Events "

    Protected Sub grvCancelledFaults_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        grvCancelledFaults.PageIndex = e.NewPageIndex
        'calling function that will store and update current result summary being shown in the footer of
        'gridview
        Me.StoreResultSummary()
        Dim faultDS As DataSet = New DataSet()

        'calling functions for populating gridview again after page indexing
        If DirectCast(ViewState(ApplicationConstants.CancelledJSIsSearchViewState), Boolean) = True Then
            GetSearchResults()
        Else
            'BindFaultGrid()
        End If
    End Sub

    Protected Sub grvCancelledFaults_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)

    End Sub
#End Region

#Region "ddl Area Selected Index Changed "

    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlArea.SelectedIndexChanged

        If ddlArea.SelectedIndex = 0 Then
            Response.Redirect("~/faultmanagement/fault_manager.aspx?option=audit")
        ElseIf ddlArea.SelectedIndex = 2 Then
            Response.Redirect("~/faultmanagement/fault_manager.aspx?option=manager")
        ElseIf ddlArea.SelectedIndex = 3 Then
            Response.Redirect("~/faultmanagement/fault_manager.aspx?option=priority")
        ElseIf ddlArea.SelectedIndex = 4 Then
            Response.Redirect("~/faultmanagement/fault_manager.aspx?option=pricing")
        ElseIf ddlArea.SelectedIndex = 5 Then
            Response.Redirect("~/secure/faultlocator/repair_list_management.aspx")
        ElseIf ddlArea.SelectedIndex = 6 Then
            Response.Redirect("~/secure/faultlocator/fault_locator.aspx")

        End If
    End Sub
#End Region

#Region "btn Clear Click "

    Protected Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ResetControls()
    End Sub
#End Region

#Region " Reset Controls "

    Public Sub ResetControls()

        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtDate.Text = ""
        txtTenancyID.Text = ""
        ddlStatus.SelectedValue = ""
        txtText.Text = ""

    End Sub
#End Region

#End Region

#Region "Methods "

#Region "Utility Methods "

#Region "Load Lookup values "
    Private Sub GetStatusLookUpValues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getReportedFaultStatusLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlStatus.DataSource = lstLookUp
            Me.ddlStatus.DataValueField = "LookUpValue"
            Me.ddlStatus.DataTextField = "LookUpName"
            Me.ddlStatus.Items.Add(New ListItem("Please select", ""))
            Me.ddlStatus.DataBind()

        End If

    End Sub
#End Region

#Region "Save Search Options "

    Private Sub SaveSearchOptions()
        'make FaultSearchBO object and store the search options entered by user untill he/she enters again
        'in this case the values will be overidden
        Dim cancelledBO As CancelledJobBO = New CancelledJobBO()

        'set First Name enetered
        cancelledBO.FirstName = IIf(txtFirstName.Text = "Please select" Or txtFirstName.Text = "", Nothing, txtFirstName.Text)

        'set Last Name entered
        cancelledBO.LastName = IIf(txtLastName.Text = "" Or txtLastName.Text = "Please select", Nothing, txtLastName.Text)

        'set date selected
        cancelledBO.DueDate = IIf(txtDate.Text = "" Or txtDate.Text = "Please select", Nothing, txtDate.Text)

        'set tenancy Id entered
        cancelledBO.TenancyId = IIf(txtTenancyID.Text = "" Or txtTenancyID.Text = "Please select", Nothing, txtTenancyID.Text)

        'set status value enetered
        cancelledBO.Status = IIf(ddlStatus.Text = "" Or ddlStatus.Text = "Please select", Nothing, ddlStatus.SelectedValue.ToString)

        cancelledBO.Text = IIf(txtText.Text = "", Nothing, txtText.Text)


        'tell CancelledJobBO it has values that will be used for searching
        cancelledBO.IsSearch = True
        'store EnquiryLogSearchBO object in viewstate
        ViewState(ApplicationConstants.CancelledJSSearchOptionViewState) = cancelledBO
        'store true in viewstate to indicate that current data on gridview is from search
        ViewState(ApplicationConstants.CancelledJSIsSearchViewState) = True
    End Sub

#End Region
#End Region

#Region "Get Search Row Count "
    Private Function GetSearchRowCount() As Integer
        Dim objBAL As ContractorPortalManager = New ContractorPortalManager()
        Dim cancelledBO As CancelledJobBO = New CancelledJobBO()
        cancelledBO = DirectCast(ViewState(ApplicationConstants.CancelledJSSearchOptionViewState), CancelledJobBO)
        Return (objBAL.GetCancelledJobSearchRowCount(cancelledBO))
    End Function
#End Region

#Region "Populate Search Results "
    Private Sub PopulateSearchResults()
        'save these new search options entered by user unless search button is pressed again
        SaveSearchOptions()
        'setting new starting page index of for this new search
        grvCancelledFaults.PageIndex = ApplicationConstants.CancelledJSInitialPageIndex
        'getting and setting row count of new search results
        grvCancelledFaults.VirtualItemCount = GetSearchRowCount()
        'getting search results
        GetSearchResults()
        updatePanel_LoadGrid.Update()
    End Sub
#End Region

#Region "Get Search Records "
    Protected Sub GetSearchResults()
        Dim cancelledBO As CancelledJobBO = New CancelledJobBO()
        'reterive search options enqryLogSearchBO from viewstate before requesting results for next page of gridview
        cancelledBO = DirectCast(ViewState(ApplicationConstants.CancelledJSSearchOptionViewState), CancelledJobBO)
        Dim cancelledJobDS As DataSet = New DataSet()
        'calling GetDataPage function that will further call function in buisness layer
        GetDataPage(grvCancelledFaults.PageIndex, ApplicationConstants.CustEnquiryManagResultPerPage, grvCancelledFaults.OrderBy, cancelledJobDS, cancelledBO)
        'binding gridview with the result returned from db
        Me.StoreResultSummary()
        grvCancelledFaults.DataSource = cancelledJobDS
        grvCancelledFaults.DataBind()
    End Sub
#End Region

#Region "Function to get data "
    Public Sub GetDataPage(ByVal pageIndex As Integer, ByVal pageSize As Integer, ByVal sortExpression As String, ByRef cancelledJobDs As DataSet, ByVal cancelledBO As CancelledJobBO)

        'reteriving sortby column name from viewstate
        If Not (ViewState(ApplicationConstants.CancelledJSSortByViewState) Is Nothing) Then
            cancelledBO.SortBy = CType(ViewState(ApplicationConstants.CancelledJSSortByViewState), String)
        Else
            'if thr is no sortby column in viewstate thn use equirylogid as a default
            cancelledBO.SortBy = ApplicationConstants.CancelledJSSortBy
        End If

        'setting the range of results to be fetched from db (pageindex*pagesize)
        cancelledBO.PageIndex = pageIndex
        cancelledBO.RowCount = pageSize

        'reteriving sort order from viewstate
        cancelledBO.SortOrder = DirectCast(ViewState(ApplicationConstants.CancelledJSSortOrderViewState), String)
        Dim contMngr As ContractorPortalManager = New ContractorPortalManager()
        'passing reference of dataset and passing faultSearchBO to GetFaultSearchResults function
        'of EnquiryManager
        Try
            contMngr.GetCancelledJobSearchData(cancelledJobDs, cancelledBO)
            'storing number of rows in the result
            ViewState("NumberofRows") = cancelledJobDs.Tables(0).Rows.Count()
        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try


        ''Check for exception 
        'Dim exceptionFlag As Boolean = cancelledBO.IsExceptionGenerated
        'If exceptionFlag Then

        'End If

    End Sub
#End Region

#Region "Store Result Summary "
    'when page index changes this functions gets called 
    Public Sub StoreResultSummary()
        Dim row As GridViewRow = grvCancelledFaults.BottomPagerRow()
        Dim strResultSummary As String = ""

        If grvCancelledFaults.PageIndex + 1 = grvCancelledFaults.PageCount Then
            strResultSummary = "Result " & ((grvCancelledFaults.PageIndex * grvCancelledFaults.PageSize) + 1) & " to " & grvCancelledFaults.VirtualItemCount & " of " & grvCancelledFaults.VirtualItemCount
        Else
            strResultSummary = "Result " & ((grvCancelledFaults.PageIndex * grvCancelledFaults.PageSize) + 1) & " to " & ((grvCancelledFaults.PageIndex + 1) * grvCancelledFaults.PageSize) & " of " & grvCancelledFaults.VirtualItemCount
        End If
        'store result sumarry which will used later on when data bounds tp pager row
        ViewState(ApplicationConstants.CancelledJSResultSummary) = strResultSummary
    End Sub
#End Region

#End Region

End Class