<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="property_reactive_repair_popup.aspx.vb" Inherits="tenantsonline.property_reactive_repair_popup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
        <table style="background-color: white">
            <tr>
                <td colspan="2">
                    <asp:UpdatePanel ID="updPnl_Error" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblErrorMsg" runat="server" CssClass="error_message_label" Width="224px"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="lblContractor" runat="server" CssClass="caption" Text="Contractor:"></asp:Label></td>
                <td style="width: 100px">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlContractor" runat="server" AppendDataBoundItems="True" Width="229px">
                                <asp:ListItem Selected="True" Value="0">Please Select</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="lblReason" runat="server" CssClass="caption" Text="Reason:"></asp:Label></td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtNotes" runat="server" Height="122px" TextMode="MultiLine" Width="224px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <asp:Label ID="lblFaultLogID" runat="server" Visible="False"></asp:Label></td>
                <td style="width: 100px">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSaveChanges" runat="server" OnClick="btnSaveChanges_Click1" Text="Save Changes" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
