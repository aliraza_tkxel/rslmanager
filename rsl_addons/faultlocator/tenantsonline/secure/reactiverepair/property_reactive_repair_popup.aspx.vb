Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities


Partial Public Class property_reactive_repair_popup
    Inherits System.Web.UI.Page

#Region "Page Init "

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        'If IsNothing(ASPSession("USERID")) Then
        'Response.Redirect("~/error.aspx")
        'End If

    End Sub
#End Region

#Region "Page Load "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Request.QueryString("faultlogid") = String.Empty Then

            Dim faultLogId As Integer = Request.QueryString("faultlogid")

            lblErrorMsg.Text = ""

            If Not Me.IsPostBack Then
                GetContractorLookUpVallues()
                SetFaultLogId()
            End If

        Else
            Me.lblErrorMsg.Text = "Fault Log ID is empty. Fault Log ID is required."
        End If

    End Sub
#End Region

#Region "btn Save Changes Click "

    Protected Sub btnSaveChanges_Click1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ValidatePreInspectionPopup()
        SetMsgLabel("Contractor Saved Successfully", "green")
    End Sub
#End Region

#Region "ddlContractor Index Changed Event"

    Protected Sub ddlContractor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (ddlContractor.SelectedValue <> "") Then
            lblErrorMsg.Text = ""
            updPnl_Error.Update()

        End If
    End Sub
#End Region

#Region "Get Contractor Look Up Vallues "


    Private Sub GetContractorLookUpVallues()
        Dim objBL As New UtilityManager
        Dim spName As String = SprocNameConstants.getContractorLookup
        Dim lstLookUp As New LookUpList

        objBL.getLookUpList(spName, lstLookUp)

        If lstLookUp.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If

        If Not lstLookUp Is Nothing Then
            Me.ddlContractor.Items.Clear()
            Me.ddlContractor.DataSource = lstLookUp
            Me.ddlContractor.DataValueField = "LookUpValue"
            Me.ddlContractor.DataTextField = "LookUpName"
            Me.ddlContractor.Items.Add(New ListItem("Please select", ""))
            Me.ddlContractor.DataBind()
        End If

    End Sub

#End Region

#Region "Validate Pre Inspection Popup "

    Public Sub ValidatePreInspectionPopup()

        If (ddlContractor.SelectedValue = String.Empty Or ddlContractor.SelectedValue = "Please select") Then
            SetMsgLabel("Please Select Contractor", "red")
        Else
            SaveInspectionRecord()

        End If
    End Sub

#End Region

#Region "Save Inspection Record "

    Private Sub SaveInspectionRecord()
        Try

            Dim piBO As PreInspectBO = New PreInspectBO()

            If Me.ddlContractor.SelectedValue <> String.Empty Then
                piBO.OrgId = CType(ddlContractor.SelectedValue, Integer)
            End If

            piBO.FaultLogId = Convert.ToInt32(GetFaultLogId())
            Dim piBL As FaultManager = New FaultManager()
            piBL.SavePreInspectionRecord(piBO)
            Reset()
        Catch ex As Exception

            SetMsgLabel("Error Occured While Saving Contractor", "red")

        End Try
    End Sub
#End Region

#Region "Reset "
    Public Sub Reset()
        lblErrorMsg.Text = ""
        ddlContractor.SelectedValue = ""
        txtNotes.Text = ""
        ddlContractor.Enabled = True
    End Sub
#End Region

#Region "Set Fault Log Id "
    Private Sub SetFaultLogId()
        Me.lblFaultLogID.Text = Request.QueryString("faultlogid")
    End Sub
#End Region

#Region "Get Fault Log Id "
    Private Function GetFaultLogId() As String
        Return Me.lblFaultLogID.Text.ToString()
    End Function
#End Region

#Region "Set Msg Label Color "
    Private Sub SetMsgLabel(ByVal msg As String, ByVal color As String)

        lblErrorMsg.Text = msg
        lblErrorMsg.Style("color") = color
        updPnl_Error.Update()

    End Sub
#End Region
End Class