<%@ Page Language="vb" AutoEventWireup="false"     CodeBehind="fl_Reactive_Repair.aspx.vb" Inherits="tenantsonline.fl_Reactive_Repair" %>
<%@ Register Assembly="Broadland.Tenants.Web" Namespace="tenantsonline.Fadrian.Web.Control"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
 
<script runat = "server">
    Function ReactiveRepairCommandString(ByVal id As String, ByVal preInspection As String, ByVal dueDate As String, ByVal netCost As String)
        
        Return id & "," & preInspection & "," & dueDate & "," & netCost
        
    End Function
        
</script>

    
      <script language = "javascript" type ="text/javascript">
   
    function AlertMessage()
    {
       alert("The repair will not be progressed any further");    
    }

</script>
<head id="Head1" runat="server">
<title> Reactive Repair </title>
<link href="../../css/IE_Style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="form1" runat="server">
    
<table style="width: 100%">
    <tr>
        <td style="width: 100px; background-color: #c00000;">
            <br />
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/braodland_logo.gif" /><br />
        </td>
    </tr>
    <tr>
        <td style="width: 100px">
        </td>
    </tr>
    <tr>
        <td style="width: 100px; background-color: #c00000">
            <asp:Label ID="Label2" runat="server" CssClass="header_white_medium" Font-Bold="False"
                Text="Reactive Repair" Width="352px"></asp:Label></td>
    </tr>
    <tr>
        <td style="width: 100px">
            <cc2:toolkitscriptmanager id="Registration_ToolkitScriptManager" runat="server"></cc2:toolkitscriptmanager></td>
    </tr>
</table>
    <table style="width: 100%">
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:updatepanel id="updatePanel_Reactive_Repair" runat="server" updatemode="Conditional"><ContentTemplate>
<asp:GridView id="GVReactiveRepair" runat="server" ForeColor="White" Width="816px" Font-Overline="False" BorderWidth="1px" BorderStyle="Dashed" EmptyDataText="No Record Exists" AutoGenerateColumns="False" CssClass="caption" BackColor="#C00000" PageSize="1" AllowPaging="false" AllowSorting="false">
                    <Columns>
                        <asp:TemplateField HeaderText="JS:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkJS" runat="server" CommandArgument='<%# ReactiveRepairCommandString(Eval("FaultLogID").ToString(), Eval("PreInspection").ToString(), Eval("DueDate").ToString(), Eval("NetCost").ToString()) %>'
                                    Text='<%# "JS" & Eval("FaultLogID") %>' ForeColor="#404040" OnClick="lnkTitle_Click" CssClass="link_item"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"  />
                            <ItemStyle Wrap="False"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Creation Date/Time:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkLastActionDate"  runat="server" CommandArgument='<%# ReactiveRepairCommandString(Eval("FaultLogID").ToString(), Eval("PreInspection").ToString(), Eval("DueDate").ToString(), Eval("NetCost").ToString()) %>'
                                    Text='<%# Eval("LastActionDate") %>' ForeColor="#404040" OnClick="lnkLastActionDate_Click" CssClass="link_item"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"  />
                            <ItemStyle Wrap="False"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fault:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkTitle" runat="server" CommandArgument='<%# ReactiveRepairCommandString(Eval("FaultLogID").ToString(), Eval("PreInspection").ToString(), Eval("DueDate").ToString(), Eval("NetCost").ToString()) %>'
                                    Text='<%# Eval("FaultDescription") %>' ForeColor="#404040" OnClick="lnkTitle_Click" CssClass="link_item"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"  />
                            <ItemStyle Wrap="False"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Repair:">
                            <ItemTemplate>
                                &nbsp;<asp:GridView ID="GVNature" runat="server" ForeColor="Black" GridLines="None"
                                    ShowHeader="False" CssClass="caption" Font-Bold="False" Font-Strikeout="False">
                                </asp:GridView>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"  />
                            <ItemStyle Wrap="False"  />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkStatus" runat="server" CommandArgument='<%# ReactiveRepairCommandString(Eval("FaultLogID").ToString(), Eval("PreInspection").ToString(), Eval("DueDate").ToString(), Eval("NetCost").ToString()) %>'
                                    Text='<%# Eval("FaultStatus") %>' ForeColor="#404040" OnClick="lnkStatus_Click" CssClass="link_item"></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"  />
                            <ItemStyle Wrap="False"  />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle ForeColor="White"  />
                    <RowStyle BackColor="#EFF3FB"  />
                    <AlternatingRowStyle BackColor="White"  />
                </asp:GridView> &nbsp;
                    <BR /><TABLE style="WIDTH: 816px; HEIGHT: 7px"><TBODY><TR><TD style="WIDTH: 100px; BACKGROUND-COLOR: #c00000"></TD></TR></TBODY></TABLE><BR />
                    &nbsp;<asp:UpdatePanel id="UpdatePanel2" runat="server"><ContentTemplate>
<BR /><TABLE><TBODY>
    <tr>
        <td style="border-right: gray 1px solid; border-top: gray 1px solid; border-left: gray 1px solid;
            width: 100px; border-bottom: gray 1px solid; height: 30px; border-collapse: collapse;" valign="top">
            <asp:Label id="Label1" runat="server" ForeColor="Black" Text="Fault Journal:" Width="280px" Font-Names="Arial" Font-Bold="True" BackColor="White" CssClass="sub_header_black"></asp:Label><br />
            <asp:UpdatePanel id="updatePanel_SavedSuccessfully" runat="server" UpdateMode="Conditional"><ContentTemplate>
<asp:Label id="lblSavedSuccessfully" runat="server" CssClass="info_message_label" Width="368px"></asp:Label> 
</ContentTemplate>
</asp:UpdatePanel> 
        </td>
        <td style="border-left-color: gray; border-bottom-color: gray; width: 100px;
            border-top-color: gray;
            height: 30px; border-right-color: gray; border-collapse: collapse;" valign="top">
            <%--<asp:UpdatePanel id="updpnal_btnUpd" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button ID="btnUpdateContractor" runat="server" CssClass="caption" OnClick="btnUpdateContractor_Click"
                    Text="Update " />
                </ContentTemplate>
            </asp:UpdatePanel> --%>
        </td>
    </tr>
    <TR><TD style="WIDTH: 100px; HEIGHT: 30px; border-right: gray 1px solid; border-top: gray 1px solid; border-left: gray 1px solid; border-bottom: gray 1px solid;" colspan="" valign="top"> 
    <TABLE style="WIDTH: 772px"><TBODY><TR><TD style="WIDTH: 95px"><TABLE><TBODY><TR><TD style="WIDTH: 77px; height: 17px;"><asp:Label id="lblJobSheet" runat="server" CssClass="caption" Text="JS Number:" Width="212px"></asp:Label></TD><TD style="WIDTH: 95px; height: 17px;"><asp:Label id="lblJS" runat="server" CssClass="caption" Width="210px"></asp:Label></TD></TR><TR><TD style="WIDTH: 77px; height: 17px;"><asp:Label id="lblRaised" runat="server" CssClass="caption" Text="Raised By:" Width="212px"></asp:Label></TD><TD style="WIDTH: 95px; height: 17px;"><asp:Label id="lblRaisedBy" runat="server" CssClass="caption" Width="210px"></asp:Label></TD></TR><TR><TD style="WIDTH: 77px; height: 17px;"><asp:Label id="lblRepairInformation" runat="server" CssClass="caption" Text="Fault Information:" Width="212px"></asp:Label></TD><TD style="WIDTH: 95px; height: 17px;"><asp:Label id="lblRepairInfo" runat="server" CssClass="caption" Width="210px"></asp:Label></TD></TR><TR><TD style="WIDTH: 77px"><asp:Label id="lbljobstartdate" runat="server" CssClass="caption" Text="Job Start Date/Time:" Width="149px"></asp:Label></TD><TD style="WIDTH: 95px"><asp:Label id="lblJobStartDt" runat="server" CssClass="caption" Width="226px"></asp:Label></TD></TR><TR><TD style="WIDTH: 77px"><asp:Label id="lblJobCompletionDate" runat="server" CssClass="caption" Text="Est. Job Completion Date/Time:" Width="212px"></asp:Label></TD><TD style="WIDTH: 95px"><asp:Label id="lblJbCmpDte" runat="server" CssClass="caption" Width="227px"></asp:Label></TD></TR><TR><TD style="WIDTH: 77px"><asp:Label id="lblActualCDate" runat="server" CssClass="caption" Text="Actual Completion Date/Time:" Width="212px"></asp:Label></TD><TD style="WIDTH: 95px"><asp:Label id="lblActualCompletion" runat="server" CssClass="caption" Width="227px"></asp:Label></TD></TR><TR><TD style="WIDTH: 77px"><b><asp:Label id="lblNotesCaption" runat="server" CssClass="caption" Text="Notes:" Width="212px"></asp:Label></b></TD><TD style="WIDTH: 95px"><div style="width: 275px; border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid; height: 65px; overflow:auto"><asp:Label id="lblNotes" runat="server" CssClass="caption" Width="227px"></asp:Label></div></TD></TR></TBODY></TABLE></TD><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"><TABLE><TBODY><TR><TD style="WIDTH: 100px"><asp:Label id="lblCurrentStatus" runat="server" CssClass="caption" Text="Current Status:" Width="120px"></asp:Label></TD><TD style="WIDTH: 100px"><asp:Label id="lblCrntStatus" runat="server" CssClass="caption" Width="168px"></asp:Label></TD></TR><TR><TD style="WIDTH: 100px"></TD><TD style="WIDTH: 100px"></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD><TD style="WIDTH: 100px; HEIGHT: 30px">
    &nbsp;</TD></TR><TR><TD style="WIDTH: 100px; border-right: gray 1px solid; border-top: gray 1px solid; border-left: gray 1px solid; border-bottom: gray 1px solid;"><asp:UpdatePanel id="Upd_pnl_Journal" runat="server" UpdateMode="Conditional"><ContentTemplate>
<asp:GridView id="GVReactiveRepairJournal" runat="server" Width="772px" BorderWidth="1px" BorderStyle="Dashed" AutoGenerateColumns="False" GridLines="None" CssClass="caption" BackColor="#C00000" PageSize="1" AllowSorting="True" OnSorting="GVReactiveRepairJournal_Sorting">
                                                    <Columns>
                                                    <asp:TemplateField HeaderText="Status Change Date:" SortExpression="FL_FAULT_STATUS.FaultStatusId">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDate" runat="server" Text='<%# Eval("HistoryDateTime") %>' Width="160px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Item:" SortExpression="C_ITEM.[Description]">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("ItemDescription") %>' CssClass="caption" Width="72px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Title:" SortExpression="FL_FAULT.[Description]">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("FaultDescription") %>' CssClass="caption" Width="96px"></asp:Label>&nbsp;
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status:" SortExpression="FL_FAULT_STATUS.DESCRIPTION">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFaultStatus" runat="server" Text='<%# Eval("FaultStatus") %>' CssClass="caption" Width="88px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Appointment Date:" SortExpression="FL_CO_APPOINTMENT.APPOINTMENTDATE">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAppointmentDate" runat="server" Text='<%# Eval("AppointmentDateTime") %>' CssClass="caption"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <%--<asp:TemplateField>
                                                            <itemtemplate>
                                                                <asp:ImageButton id="imgNotesPopup" runat="server" ImageUrl='<%# "~/images/exclamation_icon.gif" %>' VISIBLE="true" OnCommand="imgNotesPopup_Command"></asp:ImageButton>&nbsp;&nbsp;
                                                            </itemtemplate>
                                                        </asp:TemplateField>--%>
                                                         <asp:TemplateField>
                                                            <itemtemplate>
                                                                <asp:HiddenField ID="hdNotes" Value='<%# Bind("CancelReason") %>' runat="server"   />
                                                            </itemtemplate>
                                                        </asp:TemplateField>
                                                        
                                                       </Columns>
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="sub_header_black1" BorderColor="DarkGray" BorderStyle="Solid" BorderWidth="2px" ForeColor="White"  />
                                                    <RowStyle BackColor="#EFF3FB"  />
                                                    <AlternatingRowStyle BackColor="White"  />
                                                </asp:GridView> 
</ContentTemplate>
</asp:UpdatePanel> </TD><TD style="WIDTH: 100px"></TD></TR></TBODY></TABLE><DIV style="TEXT-ALIGN: left">
    &nbsp;<asp:Panel ID="PanelPreInspectionPopUp" runat="server" Height="50px" Width="125px" BorderStyle="Solid" BorderWidth="1px">
        <asp:UpdatePanel ID="PnlPreIns_Constractor" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="background-color: white">
                    <tr>
                        <td colspan="3" style="height: 3px; background-color: #c00000">
                        </td>
                        <td colspan="1" style="height: 3px; background-color: #c00000">
                            <asp:ImageButton ID="imgClose" runat="server" ImageUrl="~/images/closeimage.png" Height="16px" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:UpdatePanel ID="updPnl_Error" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label id="lblErrorMsg" runat="server" CssClass="error_message_label" Width="224px"></asp:Label> 
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td align="right" style="width: 100px">
                            </td>
                        <td align="right" style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label id="lblContractor" runat="server" CssClass="caption" Text="Contractor:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
<asp:DropDownList id="ddlContractor" runat="server" Width="229px" OnSelectedIndexChanged="ddlContractor_SelectedIndexChanged" AppendDataBoundItems="True">
                                                </asp:DropDownList> 
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label id="lblReason" runat="server" CssClass="caption" Text="Reason:"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:TextBox id="txtNotes" runat="server" Width="224px" Height="122px" TextMode="MultiLine"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label id="lblFaultLogID" runat="server" Visible="False"></asp:Label></td>
                        <td style="width: 100px">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
        <asp:Button ID="btnSaveChanges" runat="server" OnClick="btnSaveChanges_Click1" Text="Save Changes" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <cc2:ModalPopupExtender id="mdl_ReactRer_PreInsPopup" runat="server" TargetControlID="lblReason" PopupControlID="PanelPreInspectionPopUp" Drag="True" CancelControlID="imgClose"  BackgroundCssClass="modalBackground">
                                        </cc2:ModalPopupExtender> 
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="background-color: #c00000; height: 16px;">
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    &nbsp;<br />
<%--    <asp:UpdatePanel ID="updatePanel_NotesPopup" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cc2:ModalPopupExtender ID="modalPopupExtender_Notes" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="btnNotesClose" PopupControlID="updatePanel_NotesPopup" TargetControlID="lblNotesPopupTitle">
            </cc2:ModalPopupExtender>
           
            <table bgcolor="white" border="2">
                <tr>
                    <td>
                        <table bgcolor="white" style="width: 243px; height: 250px">
                            <tr>
                                <td align="left" colspan="3" rowspan="1" style="width: 486px; background-color: #cc0000"
                                    valign="middle">
                                    <asp:Label ID="lblNotesPopupTitle" runat="server" Font-Bold="True" ForeColor="White"
                                        Text="!Special Requirements!" CssClass="caption"></asp:Label></td>
                            </tr>
                            
          
                             <tr>
                                <td align="right" colspan="3" rowspan="1"  style="width: 338px; height: 16px" valign="top">
                                    <asp:Button ID="btnNotesClose" runat="server" Text="Close Window" OnClick="btnNotesClose_Click" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
                                  
        </ContentTemplate>
    </asp:UpdatePanel>--%>
</DIV>
                    &nbsp; &nbsp; &nbsp;&nbsp;<BR />
</ContentTemplate>
</asp:UpdatePanel> <BR />
</ContentTemplate>
</asp:updatepanel>
            </td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</form>
</body>
</html>
