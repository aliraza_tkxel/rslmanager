Imports System
Imports Broadland.TenantsOnline.BusinessLogic
Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Partial Public Class property_reactive_Repair
    'Inherits System.Web.UI.Page
    Inherits MSDN.SessionPage

#Region "Events "

#Region "Page Init "

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Response.Cache.SetNoStore()

        'If user isn't authenticated, he/she will be redirected to ~/login.aspx
        If IsNothing(ASPSession("USERID")) Then
        Response.Redirect("~/../BHAIntranet/login.aspx")
        End If



    End Sub

#End Region

#Region "Page Load Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            'If Not Request.QueryString("PropertyID") = String.Empty Then

            Dim PropertyID As String = Request.QueryString("PropertyID")
            'Dim PropertyID As String = "P040340007"
            PopulateReactiveRepairGrid(PropertyID)
            'Else
            '    Me.lblMsg.Text = "Property Id is empty. Property ID is required."
            'End If

            ViewState.Add(ApplicationConstants.ReactiveRepairJournalSortOrderViewState, ApplicationConstants.ReactiveRepairJournalDESCSortOrder)

            UpdatePanel2.Visible = False
            'updatePanel_PreInspection.Visible = False
            'PanelPreInspectionPopUp.Visible = False

        End If
    End Sub
#End Region

#Region "Populate Grid Method "

    Public Sub PopulateReactiveRepairGrid(ByVal propertyId As String)

        Try
            ' to porperly hanlde excpeton if any. 

            Dim reactiveRepairDS As New DataSet()
            Dim natureDS As New DataSet()
            Dim faultLogID As Integer
            Dim i As Integer

            Dim ContractorPortalManager As ContractorPortalManager = New ContractorPortalManager()

            ContractorPortalManager.PopulatePropertyReactiveRepair(reactiveRepairDS, propertyId)
            GVReactiveRepair.DataSource = reactiveRepairDS
            GVReactiveRepair.DataBind()

            For i = 0 To reactiveRepairDS.Tables(0).Rows.Count - 1

                'faultLogID = CType(reactiveRepairDS.Tables(0).Rows(i)("FaultLogID"), Int32)
                faultLogID = CType(reactiveRepairDS.Tables(0).Rows(i)(3), Int32)
                BindNatureGrid(faultLogID, natureDS)
                Dim GVnature As GridView = DirectCast(GVReactiveRepair.Rows(i).Cells(2).FindControl("GVNature"), GridView)

                GVnature.DataSource = natureDS
                GVnature.DataBind()

            Next

        Catch ex As Exception

            Response.Redirect("~/error.aspx")

        End Try

    End Sub

#End Region

#Region "btn Update Contractor Click "

    Protected Sub btnUpdateContractor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        '    lblErrorMsg.Text = ""
        '    lblSavedSuccessfully.Text = String.Empty
        '    updatePanel_SavedSuccessfully.Update()
        '    Reset()

        '    'GetPreInspectionTeamLookUp()
        '    GetContractorLookUpVallues()


        '    'updatePanel_PreInspection.Update()

        '    '
        '    mdl_ReactRer_PreInsPopup.Show()
        '    PanelPreInspectionPopUp.Visible = True

        'Catch ex As Exception
        '    Response.Redirect("~/error.aspx")
        'End Try
    End Sub
#End Region

#Region "Bind Grid"
    Public Sub BindNatureGrid(ByVal faultID As Integer, ByRef natureDS As DataSet)

        Try
            Dim contractManager As ContractorPortalManager = New ContractorPortalManager()
            contractManager.PopulateReactiveRepairNature(faultID, natureDS)

        Catch ex As Exception

            Response.Redirect("~/error.aspx")

        End Try




    End Sub

#End Region

#Region "lnk Last Action Date Click "
    Protected Sub lnkLastActionDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim targetUrl As String = "~/secure/reactiverepair/fl_Reactive_Repair_Journal.aspx"

        Dim lnkButton As LinkButton = CType(sender, LinkButton)
        Dim strArray() As String = lnkButton.CommandArgument.Split(",")
        Dim id As Integer = Convert.ToInt32(strArray(0).ToString())
        Dim preInspection As Boolean = Convert.ToBoolean(strArray(1).ToString())
        Dim dueDate As String = strArray(2).ToString()
        Dim netCost As String = strArray(3).ToString()
        ViewState.Add("FaultLogID", id)
        PopulateJournalGrid(id)
        UpdatePanel2.Visible = True

    End Sub
#End Region

#Region "lnk Action Click "
    Protected Sub lnkAction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Dim targetUrl As String = "~/secure/reactiverepair/fl_Reactive_Repair_Journal.aspx"
        Dim lnkButton As LinkButton = CType(sender, LinkButton)
        Dim strArray() As String = lnkButton.CommandArgument.Split(",")
        Dim id As Integer = Convert.ToInt32(strArray(0).ToString())
        Dim preInspection As Boolean = Convert.ToBoolean(strArray(1).ToString())
        Dim dueDate As String = strArray(2).ToString()
        Dim netCost As String = strArray(3).ToString()
        ViewState.Add("FaultLogID", id)
        PopulateJournalGrid(id)
        UpdatePanel2.Visible = True

    End Sub
#End Region

#Region "lnk Item Click "

    Protected Sub lnkItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim targetUrl As String = "~/secure/reactiverepair/fl_Reactive_Repair_Journal.aspx"
        Dim lnkButton As LinkButton = CType(sender, LinkButton)
        Dim strArray() As String = lnkButton.CommandArgument.Split(",")
        Dim id As Integer = Convert.ToInt32(strArray(0).ToString())
        Dim preInspection As Boolean = Convert.ToBoolean(strArray(1).ToString())
        Dim dueDate As String = strArray(2).ToString()
        Dim netCost As String = strArray(3).ToString()
        ViewState.Add("FaultLogID", id)
        PopulateJournalGrid(id)
        UpdatePanel2.Visible = True
    End Sub
#End Region

#Region "lnk Nature Description Click "

    Protected Sub lnkNatureDescription_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Dim targetUrl As String = "~/secure/reactiverepair/fl_Reactive_Repair_Journal.aspx"
        Dim lnkButton As LinkButton = CType(sender, LinkButton)
        Dim strArray() As String = lnkButton.CommandArgument.Split(",")
        Dim id As Integer = Convert.ToInt32(strArray(0).ToString())
        Dim preInspection As Boolean = Convert.ToBoolean(strArray(1).ToString())
        Dim dueDate As String = strArray(2).ToString()
        Dim netCost As String = strArray(3).ToString()
        ViewState.Add("FaultLogID", id)
        PopulateJournalGrid(id)
        UpdatePanel2.Visible = True
    End Sub
#End Region

#Region "lnk Title Click "

    Protected Sub lnkTitle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim targetUrl As String = "~/secure/reactiverepair/fl_Reactive_Repair_Journal.aspx"
        Dim lnkButton As LinkButton = CType(sender, LinkButton)
        Dim strArray() As String = lnkButton.CommandArgument.Split(",")
        Dim id As Integer = Convert.ToInt32(strArray(0).ToString())
        Dim preInspection As Boolean = Convert.ToBoolean(strArray(1).ToString())
        Dim dueDate As String = strArray(2).ToString()
        Dim netCost As String = strArray(3).ToString()
        ViewState.Add("FaultLogID", id)
        PopulateJournalGrid(id)
        UpdatePanel2.Visible = True        
    End Sub
#End Region

#Region "lnk Status Click "

    Protected Sub lnkStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim targetUrl As String = "~/secure/reactiverepair/fl_Reactive_Repair_Journal.aspx"
        Dim lnkButton As LinkButton = CType(sender, LinkButton)
        Dim strArray() As String = lnkButton.CommandArgument.Split(",")
        Dim id As Integer = Convert.ToInt32(strArray(0).ToString())
        Dim preInspection As Boolean = Convert.ToBoolean(strArray(1).ToString())
        Dim dueDate As String = strArray(2).ToString()
        Dim netCost As String = strArray(3).ToString()
        ViewState.Add("FaultLogID", id)
        PopulateJournalGrid(id)
        UpdatePanel2.Visible = True

    End Sub
#End Region

#Region "ddlContractor Index Changed Event"

    'Protected Sub ddlContractor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If (ddlContractor.SelectedValue <> "") Then
    '        lblErrorMsg.Text = ""
    '        updPnl_Error.Update()

    '    End If
    'End Sub
#End Region

#Region "Get Contractor Look Up Vallues "


    'Private Sub GetContractorLookUpVallues()
    '    Dim objBL As New UtilityManager
    '    Dim spName As String = SprocNameConstants.getContractorLookup
    '    Dim lstLookUp As New LookUpList

    '    objBL.getLookUpList(spName, lstLookUp)

    '    If lstLookUp.IsExceptionGenerated Then
    '        Response.Redirect("~/error.aspx")
    '    End If

    '    If Not lstLookUp Is Nothing Then
    '        Me.ddlContractor.Items.Clear()
    '        Me.ddlContractor.DataSource = lstLookUp
    '        Me.ddlContractor.DataValueField = "LookUpValue"
    '        Me.ddlContractor.DataTextField = "LookUpName"
    '        Me.ddlContractor.Items.Add(New ListItem("Please select", ""))
    '        Me.ddlContractor.DataBind()
    '    End If

    'End Sub

#End Region

#Region "Generic Method to Populate a Drop Down"

    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByVal spName As String, ByVal inParam As String, ByVal inParamName As String)
        Dim objBL As New UtilityManager
        Dim lstLookup As New LookUpList
        ddlLookup.Items.Clear()
        objBL.getLookUpList(spName, lstLookup, inParam, inParamName)
        If lstLookup.IsExceptionGenerated Then
            Response.Redirect("~/error.aspx")
        End If
        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
            'UpdatePanel_operative.Update()
        End If
    End Sub

#End Region

#Region "btn Save Changes Click "

    'Protected Sub btnSaveChanges_Click1(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    ValidatePreInspectionPopup()
    'End Sub
#End Region

#Region "ddlOperative Index Changed Event"

    'Protected Sub ddlOperative_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If (ddlOperative.SelectedValue <> "") Then

    '        lblErrorMsg.Text = ""
    '        updatePanel_ErrorMessage.Update()

    '    End If

    '    If ddlOperative.SelectedValue = "" Then
    '        ddlTeam.SelectedValue = ""
    '        UpdatePanel_team.Update()
    '    End If
    'End Sub
#End Region

#End Region

#Region "Methods & Functions "

#Region "Reset "


    'Public Sub Reset()
    '    lblErrorMsg.Text = ""        
    '    ddlContractor.SelectedValue = ""

    '    ddlContractor.Enabled = True


    'End Sub
#End Region

#Region "Populate Journal Grid Values "

    Public Sub PopulateJournalGridValues(ByVal rectJournal As DataSet)

        lblJS.Text = "JS " & rectJournal.Tables(0).Rows(0)("FaultLogId").ToString()
        lblRepairInfo.Text = rectJournal.Tables(0).Rows(0)("FaultDescription").ToString()
        lblJobStartDt.Text = rectJournal.Tables(0).Rows(0)("SubmitDate").ToString()
        lblJbCmpDte.Text = rectJournal.Tables(0).Rows(0)("DueDate").ToString()
        lblCrntStatus.Text = rectJournal.Tables(0).Rows(0)("CURRENTSTATUS").ToString()
        lblActualCompletion.Text = rectJournal.Tables(0).Rows(0)("CompletionDate").ToString()


    End Sub
#End Region

#Region "Validate Pre Inspection Popup "

    Public Sub ValidatePreInspectionPopup()

        'If (ddlContractor.SelectedValue = String.Empty Or ddlContractor.SelectedValue = "Please select") Then
        '    lblErrorMsg.Text = "Please Select Contractor"
        '    updPnl_Error.Update()
        'Else
        '    SaveInspectionRecord()
        '    lblSavedSuccessfully.Text = "Contractor Saved Successfully"
        '    lblSavedSuccessfully.Style("color") = "green"
        '    updatePanel_SavedSuccessfully.Update()
        '    PnlPreIns_Constractor.Update()

        'End If
    End Sub

#End Region

#Region "Save Inspection Record "

    'Private Sub SaveInspectionRecord()
    '    Try

    '        Dim piBO As PreInspectBO = New PreInspectBO()

    '        If Me.ddlContractor.SelectedValue <> String.Empty Then
    '            piBO.OrgId = CType(ddlContractor.SelectedValue, Integer)
    '        End If

    '        piBO.FaultLogId = Convert.ToInt32(Session("FaultLogId"))
    '        Dim piBL As FaultManager = New FaultManager()
    '        'piBL.SaveInspectionRecord(piBO)
    '        piBL.SavePreInspectionRecord(piBO)

    '    Catch ex As Exception

    '        Response.Redirect("/error.aspx")

    '    End Try
    'End Sub
#End Region

#Region "Populate Journal Grid "

    Public Sub PopulateJournalGrid(ByVal id As Integer)

        Try

            Dim preInsBO As PreInspectionBO = New PreInspectionBO()
            Dim reactiveRepairJournalDS As DataSet = New DataSet()

            'preInsBO.SortBy = ApplicationConstants.PreInspectionSortBy
            'preInsBO.SortOrder = ApplicationConstants.PreInspectionSortOrderViewState
            GetReactiveRepairDataPage(preInsBO, reactiveRepairJournalDS, id)

            If reactiveRepairJournalDS.Tables(0).Rows.Count > 0 Then
                PopulateJournalGridValues(reactiveRepairJournalDS)
                GVReactiveRepairJournal.DataSource = reactiveRepairJournalDS
                ViewState.Add(ApplicationConstants.ReactiveRepairJournalDataSet, reactiveRepairJournalDS)
                GVReactiveRepairJournal.DataBind()
                Upd_pnl_Journal.Update()
            End If

            Me.SetPopupSpecs(id)            

        Catch ex As Exception
            Response.Redirect("~/error.aspx")
        End Try

    End Sub



#End Region

#Region "Get Reactive Repair Data Page"

    Public Sub GetReactiveRepairDataPage(ByRef prInsBO As PreInspectionBO, ByRef preInsDS As DataSet, ByVal id As Integer)

        If CType(ViewState(ApplicationConstants.ReactiveRepairSortByViewState), String) <> "" Then
            prInsBO.SortBy = CType(ViewState(ApplicationConstants.ReactiveRepairSortByViewState), String)
        Else
            prInsBO.SortBy = ApplicationConstants.ReactiveRepairSortBY
        End If

        prInsBO.SortOrder = DirectCast(ViewState(ApplicationConstants.ReactiveRepairJournalSortOrderViewState), String)

        Dim preInspection As Boolean
        Dim contraPortalManager As ContractorPortalManager = New ContractorPortalManager()

        Try
            preInspection = contraPortalManager.PopulateReactiveRepairJournal(preInsDS, id, prInsBO)

        Catch ex As Exception

            Response.Redirect("~/error.aspx")
        End Try

    End Sub
#End Region

#Region "GVReactive Repair Journal Sorting"


    Protected Sub GVReactiveRepairJournal_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)

        ViewState.Add(ApplicationConstants.ReactiveRepairSortByViewState, e.SortExpression)

        If DirectCast(ViewState(ApplicationConstants.ReactiveRepairJournalSortOrderViewState), String) = ApplicationConstants.ReactiveRepairJournalDESCSortOrder Then

            ViewState(ApplicationConstants.ReactiveRepairJournalSortOrderViewState) = ApplicationConstants.ReactiveRepairJournalASCSortOrder
        Else
            ViewState(ApplicationConstants.ReactiveRepairJournalSortOrderViewState) = ApplicationConstants.ReactiveRepairJournalDESCSortOrder

        End If
        Dim faultLogId As Integer = CType(ViewState("FaultLogID"), Integer)
        PopulateJournalGrid(faultLogId)

    End Sub
#End Region

#Region "Set Popup Specs "
    Private Sub SetPopupSpecs(ByVal faultLogId As Integer)
        'Me.btnUpdateContractor.Attributes.Add("onClick", "window.open('property_reactive_repair_popup.aspx?faultlogid=" + faultLogId.ToString() + "','_blank','directories=no,height=250,width=350,resizable=yea,scrollbars=no,status=no,titlebar=no,toolbar=no')")
    End Sub

#End Region


#End Region

End Class