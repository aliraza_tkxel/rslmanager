Partial Public Class CommonErrorPage
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lblErrorTrace.Text = Session.Item(FaultConstants.ExceptionMessage)
        Me.UpdatePanel1.Update()
    End Sub
End Class