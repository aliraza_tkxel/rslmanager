Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess

    Public Class UtilityDAL : Inherits BaseDAL

#Region "Functions"



#Region "getLookUpList"

        Public Function getLookUpList(ByVal spName As String, ByRef lstLookUp As LookUpList) As LookUpList

            Dim sprocName As String = spName
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, sprocName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookupBO(id, name)
                lstLookUp.Add(objLookUp)
            End While

            Return lstLookUp

        End Function

        Public Function getLookUpList(ByVal spName As String, ByRef lstLookUp As LookUpList, ByVal inParam As String, ByVal inParamName As String) As LookUpList

            Dim sprocName As String = spName
            Dim inParamList As New ParameterList()
            Dim inParamBO As New ParameterBO(inParamName, Integer.Parse(inParam), DbType.Int32)
            inParamList.Add(inParamBO)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookupBO(id, name)
                lstLookUp.Add(objLookUp)
            End While

            Return lstLookUp

        End Function

        Public Function getLookUpList(ByVal spName As String, ByRef lstLookUp As LookUpList, ByVal firstInParam As String, ByVal firstInParamName As String, ByVal secondInParam As String, ByVal secondInParamName As String) As LookUpList

            Dim sprocName As String = spName
            Dim inParamList As New ParameterList()
            Dim inParamBO As New ParameterBO
            inParamBO = New ParameterBO(firstInParamName, Integer.Parse(firstInParam), DbType.Int32)
            inParamList.Add(inParamBO)
            inParamBO = New ParameterBO(secondInParamName, Integer.Parse(secondInParam), DbType.Int32)
            inParamList.Add(inParamBO)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookupBO(id, name)
                lstLookUp.Add(objLookUp)
            End While

            Return lstLookUp

        End Function

        Public Function getUserLookUpList(ByVal spName As String, ByRef lstLookUp As LookUpList, ByVal inParam As String, ByVal inParamName As String, ByVal param As String, ByVal paramName As String) As LookUpList

            Dim inParamList As New ParameterList()
            Dim inParamTeam As New ParameterBO(inParamName, Integer.Parse(inParam), DbType.Int32)
            Dim inParamOrg As New ParameterBO(paramName, Integer.Parse(param), DbType.Int32)


            inParamList.Add(inParamOrg)
            inParamList.Add(inParamTeam)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, spName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookupBO(id, name)
                lstLookUp.Add(objLookUp)
            End While

            Return lstLookUp

        End Function


#End Region

#Region "GetPriorityResponse"

        Public Function getPriorityResponse(ByVal inParam As String) As String

            Dim responstStr As String = ""
            Dim sprocName As String = SprocNameConstants.FaultGetPriorityResponse
            Dim inParamList As New ParameterList()
            Dim inParamBO As New ParameterBO("@priorityID", Integer.Parse(inParam), DbType.Int32)
            inParamList.Add(inParamBO)
            Dim myDataSet As DataSet = New DataSet()
            MyBase.LoadDataSet(myDataSet, inParamList, sprocName)
            If Not (myDataSet Is Nothing) And myDataSet.Tables(0).Rows.Count > 0 Then
                responstStr = myDataSet.Tables(0).Rows(0)(0).ToString() & " " & myDataSet.Tables(0).Rows(0)(1)
            End If
            Return responstStr

        End Function

#End Region

#End Region

#Region "Methods"

#End Region
    End Class

End Namespace
