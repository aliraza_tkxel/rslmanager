'' Class Name -- BaseDAL
'' Base Class -- System
'' Summary -- Abstract class, all DAL classes will inherit from it
'' Methods -- SaveRecord, SelectRecord
'' Author  -- Muanwar Nadeem

Imports System
Imports System.Data.Common
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Data

Imports Broadland.TenantsOnline.BusinessObject


Namespace Broadland.TenantsOnline.DataAccess

    '' BaseDAL is an abstract class. All DAL classes will extend their functionality by inheriting from it.
    '' This class utilizes the Microsoft Enterprise Data Access Application Block to interact with database

    Public MustInherit Class BaseDAL

#Region "Atrributes"

        '' Used to establish connection with Database by reading configuration from web.config file
        Private myCustomerDB As Database

        '' Used to Execute commands i.e. StoredProcedures
        Private myDbCommand As DbCommand

#End Region

#Region "Properties"

        '' No Properties are defined as Attributes are supposed to be accessed within this class only

#End Region

#Region "Constructor"

        Public Sub New()

            myCustomerDB = Nothing
            myDbCommand = Nothing

        End Sub



#End Region

#Region "Functions"

#Region "SaveRecord Function"

        ' This generic Function will be used to save(new record), delete and update records

        Protected Function SaveRecord(ByRef inParamList As ParameterList, ByRef outParamList As ParameterList, ByRef sprocName As String) As Integer

            '' Used to return No of rows affected
            Dim rowCount As Integer = -1

         
            myCustomerDB = DatabaseFactory.CreateDatabase()
            myDbCommand = myCustomerDB.GetStoredProcCommand(sprocName)


            '' For Each Loop to iterate over inParamList
            '' IN-Parameters will be extracted and added to StoredProcedure
            For Each paramBO As ParameterBO In inParamList

                '' Adding parameter Name and Type
                myCustomerDB.AddInParameter(myDbCommand, paramBO.Name, paramBO.Type)

                '' Setting value of parameter
                myCustomerDB.SetParameterValue(myDbCommand, paramBO.Name, paramBO.Value)

            Next


            '' For Each Loop to iterate over outParamList
            '' OUT-Parameters will be extracted and added to StoredProcedure
            For Each outParamBO As ParameterBO In outParamList

                '' Adding output parameters Name and Type
                myCustomerDB.AddOutParameter(myDbCommand, outParamBO.Name, outParamBO.Type, 100)
            Next


            '' Executing Non query and recieving results (No. of rows affected)
            rowCount = myCustomerDB.ExecuteNonQuery(myDbCommand)



            '' Getting out parameter values after executing query
            For Each paramBO As ParameterBO In outParamList
                paramBO.Value = myCustomerDB.GetParameterValue(myDbCommand, paramBO.Name)

            Next
            Return rowCount

        End Function

#End Region

#Region "Select Function"

        ' This Function will be used to Select Single/Multiple records

        Protected Function SelectRecord(ByRef paramList As ParameterList, ByRef sprocName As String) As IDataReader

            '' Holds the results/records found after executing stored procedure
            Dim myReader As IDataReader = Nothing



            myCustomerDB = DatabaseFactory.CreateDatabase()
            myDbCommand = myCustomerDB.GetStoredProcCommand(sprocName)

            '' For Each Loop to iterate over paramList
            '' ParameterBO's will be extracted and added to StoredProcedure
            If Not paramList Is Nothing Then
                For Each paramBO As ParameterBO In paramList

                    '' Adding parameter Name and Type
                    myCustomerDB.AddInParameter(myDbCommand, paramBO.Name, paramBO.Type)

                    '' Setting value of parameter
                    myCustomerDB.SetParameterValue(myDbCommand, paramBO.Name, paramBO.Value)

                Next
            End If
          

            '' Executing ExecuteReader() and returning records found
            myReader = myCustomerDB.ExecuteReader(myDbCommand)


            Return myReader

        End Function



        ' This Function will be used to Select Single/Multiple records

        Protected Function SelectRecord(ByRef inParamList As ParameterList, ByRef outParamList As ParameterList, ByRef sprocName As String) As ParameterList



            myCustomerDB = DatabaseFactory.CreateDatabase()
            myDbCommand = myCustomerDB.GetStoredProcCommand(sprocName)

            '' For Each Loop to iterate over paramList
            '' ParameterBO's will be extracted and added to StoredProcedure

            For Each paramBO As ParameterBO In inParamList

                '' Adding parameter Name and Type
                myCustomerDB.AddInParameter(myDbCommand, paramBO.Name, paramBO.Type)

                '' Setting value of parameter
                myCustomerDB.SetParameterValue(myDbCommand, paramBO.Name, paramBO.Value)
            Next

            For Each outParamBO As ParameterBO In outParamList
                '' Adding parameter Name and Type
                myCustomerDB.AddOutParameter(myDbCommand, outParamBO.Name, outParamBO.Type, 8000)
            Next

            myCustomerDB.ExecuteScalar(myDbCommand)

            For Each paramBO As ParameterBO In outParamList
                paramBO.Value = myCustomerDB.GetParameterValue(myDbCommand, paramBO.Name)

            Next


            Return outParamList

        End Function

#End Region

#End Region

#Region "Load DataSet"

        ' This Function will be return typed dataset
        Protected Sub LoadDataSet(ByRef myDataSet As DataSet, ByRef paramList As ParameterList, ByRef sprocName As String)


            myCustomerDB = DatabaseFactory.CreateDatabase()
            myDbCommand = myCustomerDB.GetStoredProcCommand(sprocName)

            '' For Each Loop to iterate over paramList
            '' ParameterBO's will be extracted and added to StoredProcedure

            For Each paramBO As ParameterBO In paramList

                '' Adding parameter Name and Type
                myCustomerDB.AddInParameter(myDbCommand, paramBO.Name, paramBO.Type)

                '' Setting value of parameter
                myCustomerDB.SetParameterValue(myDbCommand, paramBO.Name, paramBO.Value)


            Next

            Dim reader As IDataReader = myCustomerDB.ExecuteReader(myDbCommand)

            Me.ConvertDataReaderToDataSet(myDataSet, reader)

        End Sub

        Protected Sub LoadDataSetNature(ByRef myDataSet As DataSet, ByRef paramList As ParameterList, ByRef sprocName As String)


            myCustomerDB = DatabaseFactory.CreateDatabase()
            myDbCommand = myCustomerDB.GetStoredProcCommand(sprocName)

            '' For Each Loop to iterate over paramList
            '' ParameterBO's will be extracted and added to StoredProcedure

            For Each paramBO As ParameterBO In paramList

                '' Adding parameter Name and Type
                myCustomerDB.AddInParameter(myDbCommand, paramBO.Name, paramBO.Type)

                '' Setting value of parameter
                myCustomerDB.SetParameterValue(myDbCommand, paramBO.Name, paramBO.Value)


            Next

            myDataSet = myCustomerDB.ExecuteDataSet(myDbCommand)




            'Dim reader As IDataReader = myCustomerDB.ExecuteReader(myDbCommand)

            'Me.ConvertDataReaderToDataSet(myDataSet, reader)

        End Sub

        ''***********
#End Region

#Region "Load DataSet with Output Parameters"

        Protected Function LoadDataSet(ByRef myDataSet As DataSet, ByRef paramList As ParameterList, ByRef outParamList As ParameterList, ByRef sprocName As String) As ParameterList

            myCustomerDB = DatabaseFactory.CreateDatabase()
            myDbCommand = myCustomerDB.GetStoredProcCommand(sprocName)

            '' For Each Loop to iterate over paramList
            '' ParameterBO's will be extracted and added to StoredProcedure

            For Each paramBO As ParameterBO In paramList

                '' Adding parameter Name and Type
                myCustomerDB.AddInParameter(myDbCommand, paramBO.Name, paramBO.Type)

                '' Setting value of parameter
                myCustomerDB.SetParameterValue(myDbCommand, paramBO.Name, paramBO.Value)

            Next

            For Each outParamBO As ParameterBO In outParamList
                '' Adding parameter Name and Type
                myCustomerDB.AddOutParameter(myDbCommand, outParamBO.Name, outParamBO.Type, 8000)
            Next

            'rowCount = myCustomerDB.ExecuteNonQuery(myDbCommand)
            ' the ExecuteDataSet method is caled instead of ExecuteReader, since Executereader return nothing and we need output parameter
            myDataSet = myCustomerDB.ExecuteDataSet(myDbCommand)


            'Dim reader As IDataReader = myCustomerDB.ExecuteReader(myDbCommand)


            For Each paramBO As ParameterBO In outParamList

                paramBO.Value = myCustomerDB.GetParameterValue(myDbCommand, paramBO.Name)

            Next

            Return outParamList

        End Function
#End Region

#Region "Load DataSet which takes 0 parameters"

        Protected Sub LoadDataSetReactiveRepair(ByRef myDataSet As DataSet, ByRef paramList As ParameterList, ByRef sprocName As String)


            myCustomerDB = DatabaseFactory.CreateDatabase()
            myDbCommand = myCustomerDB.GetStoredProcCommand(sprocName)

            '' For Each Loop to iterate over paramList
            '' ParameterBO's will be extracted and added to StoredProcedure

            If Not paramList Is Nothing Then

                For Each paramBO As ParameterBO In paramList

                    '' Adding parameter Name and Type
                    myCustomerDB.AddInParameter(myDbCommand, paramBO.Name, paramBO.Type)

                    '' Setting value of parameter
                    myCustomerDB.SetParameterValue(myDbCommand, paramBO.Name, paramBO.Value)

                Next
            End If

            Dim reader As IDataReader = myCustomerDB.ExecuteReader(myDbCommand)

            Me.ConvertDataReaderToDataSet(myDataSet, reader)

        End Sub
#End Region

#Region "Conversion from dataReader to dataSet"

        Private Sub ConvertDataReaderToDataSet(ByRef dataSet As DataSet, ByVal reader As IDataReader)

            Dim schemaTable As DataTable = reader.GetSchemaTable()

            Dim dataTable As DataTable = New DataTable()

            Dim intCounter As Integer

            For intCounter = 0 To schemaTable.Rows.Count - 1

                Dim dataRow As DataRow = schemaTable.Rows(intCounter)

                Dim columnName As String = CType(dataRow("ColumnName"), String)

                Dim column As DataColumn = New DataColumn(columnName, CType(dataRow("DataType"), Type))

                dataTable.Columns.Add(column)

            Next

            dataSet.Tables.Add(dataTable)

            While reader.Read()

                Dim dataRow As DataRow = dataTable.NewRow()

                For intCounter = 0 To reader.FieldCount - 1

                    dataRow(intCounter) = reader.GetValue(intCounter)

                Next

                dataTable.Rows.Add(dataRow)

            End While

        End Sub

#End Region

    End Class
End Namespace


