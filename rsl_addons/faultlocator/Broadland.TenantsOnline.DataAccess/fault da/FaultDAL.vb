'' Class Name -- CustomerDAL
'' Base Class -- BaseDAL
'' Summary --    Contains Functionality related to Customer/Tenant
'' Methods --    AuthenticateCustomer
'' Created By  -- Muanwar Nadeem

''---------------------------------
Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities
Namespace Broadland.TenantsOnline.DataAccess
    Public Class FaultDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get Fault Row Count "

        Public Function GetFaultRowCount() As Integer

            Dim sprocName As String = SprocNameConstants.GetRowCount

            Dim paramList As ParameterList = New ParameterList()

            '' used to return no. of results found
            Dim rowCount As Integer = 0

            Dim faultParam As ParameterBO

            faultParam = New ParameterBO("tableName", "FL_FAULT", DbType.String)
            paramList.Add(faultParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
                ''  custHeadBO.isFlagStatus = True

            End If

            Return rowCount

        End Function
#End Region

#Region "Get Fault Priority Search Row Count "

        Public Function GetFaultPrioritySearchRowCount() As Integer

            Dim sprocName As String = SprocNameConstants.GetRowCount

            Dim paramList As ParameterList = New ParameterList()

            '' used to return no. of results found
            Dim rowCount As Integer = 0

            Dim faultParam As ParameterBO

            faultParam = New ParameterBO("tableName", "FL_FAULT_PRIORITY", DbType.String)
            paramList.Add(faultParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
                ''  custHeadBO.isFlagStatus = True

            End If

            Return rowCount

        End Function
#End Region

#Region "Get Fault Transaction Log Row Count "

        Public Function GetFaultTransactionLogRowCount() As Integer

            Dim sprocName As String = SprocNameConstants.GetRowCount

            Dim paramList As ParameterList = New ParameterList()

            '' used to return no. of results found
            Dim rowCount As Integer = 0

            Dim faultTranLogParam As ParameterBO

            faultTranLogParam = New ParameterBO("tableName", "FL_FAULT_TRANSACTION_LOG", DbType.String)
            paramList.Add(faultTranLogParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
                ''  custHeadBO.isFlagStatus = True

            End If

            Return rowCount
        End Function
#End Region

#Region "Get Reported Fault Row Count "

        Public Function GetReportedFaultRowCount(ByRef repFaultBO As ReportedFaultSearchBO) As Integer
            Dim sprocName As String = SprocNameConstants.GetFaultLocatorRowCount

            Dim paramList As ParameterList = New ParameterList()
            Dim rowCount As Integer = 0
            Dim faultParamBO As ParameterBO

            If (repFaultBO.JS <> -1) Then
                faultParamBO = New ParameterBO("@JS", repFaultBO.JS, DbType.Int32)
                paramList.Add(faultParamBO)
            End If
            If (repFaultBO.LastName <> String.Empty) Then
                faultParamBO = New ParameterBO("@lastName", repFaultBO.LastName, DbType.String)
                paramList.Add(faultParamBO)
            End If
            If (repFaultBO.DateValue <> Nothing) Then
                faultParamBO = New ParameterBO("@date", repFaultBO.DateValue, DbType.String)
                paramList.Add(faultParamBO)
            End If

            If (repFaultBO.Nature <> -1) Then
                faultParamBO = New ParameterBO("@nature", repFaultBO.Nature, DbType.Int32)
                paramList.Add(faultParamBO)
            End If

            If (repFaultBO.Status <> -1) Then
                faultParamBO = New ParameterBO("@status", repFaultBO.Status, DbType.Int32)
                paramList.Add(faultParamBO)
            End If

            If (repFaultBO.TextValue <> String.Empty) Then
                faultParamBO = New ParameterBO("@text", repFaultBO.TextValue, DbType.String)
                paramList.Add(faultParamBO)
            End If

            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
            End If

            Return rowCount

        End Function
#End Region

#Region "Get Reported Fault Finance Report Row Count "

        Public Function GetReportedFaultReportRowCount(ByRef repFaultBO As ReportedFaultSearchBO) As Integer
            Dim sprocName As String = SprocNameConstants.GetReportedFaultReportRowCount

            Dim paramList As ParameterList = New ParameterList()
            Dim rowCount As Integer = 0
            Dim faultParamBO As ParameterBO

            If (repFaultBO.JS <> -1) Then
                faultParamBO = New ParameterBO("@JS", repFaultBO.JS, DbType.Int32)
                paramList.Add(faultParamBO)
            End If
            If (repFaultBO.LastName <> String.Empty) Then
                faultParamBO = New ParameterBO("@lastName", repFaultBO.LastName, DbType.String)
                paramList.Add(faultParamBO)
            End If
            If (repFaultBO.DateValue <> Nothing) Then
                faultParamBO = New ParameterBO("@date", repFaultBO.DateValue, DbType.String)
                paramList.Add(faultParamBO)
            End If

            If (repFaultBO.DateToValue <> Nothing) Then
                faultParamBO = New ParameterBO("@dateTo", repFaultBO.DateToValue, DbType.String)
                paramList.Add(faultParamBO)
            End If

            If (repFaultBO.Nature <> -1) Then
                faultParamBO = New ParameterBO("@nature", repFaultBO.Nature, DbType.Int32)
                paramList.Add(faultParamBO)
            End If

            If (repFaultBO.FaultStatus <> String.Empty) Then
                faultParamBO = New ParameterBO("@FaultStatus", repFaultBO.FaultStatus, DbType.String)
                paramList.Add(faultParamBO)
            End If

            If (repFaultBO.TextValue <> String.Empty) Then
                faultParamBO = New ParameterBO("@text", repFaultBO.TextValue, DbType.String)
                paramList.Add(faultParamBO)
            End If

            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
            End If

            Return rowCount

        End Function
#End Region
#Region "Get Reported Fault Search Row Count "

        Public Function GetReportedFaultSearchRowCount() As Integer

            Dim sprocName As String = SprocNameConstants.GetRowCount

            Dim paramList As ParameterList = New ParameterList()

            '' used to return no. of results found
            Dim rowCount As Integer = 0

            Dim reportedFaultLogParam As ParameterBO

            reportedFaultLogParam = New ParameterBO("tableName", "FL_FAULT_LOG", DbType.String)
            paramList.Add(reportedFaultLogParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
                ''  custHeadBO.isFlagStatus = True

            End If

            Return rowCount
        End Function
#End Region

#Region "GetRowCount Function"

        '' Private method used to return no. of results
        Private Function GetRowCount(ByRef myDataRecord As IDataRecord) As Integer

            Return myDataRecord.GetInt32(myDataRecord.GetOrdinal("numOfRows"))

        End Function

        Public Function GetFaultSearchRowCount(ByRef faultSearchBO As FaultSearchBO) As Integer
            Dim sprocName As String
            Dim rowCount As Integer = 0

            sprocName = SprocNameConstants.GetFaultSearchRowCount
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO

            If faultSearchBO.LocationID <> "" Then
                faultParams = New ParameterBO("locationId", CType(faultSearchBO.LocationID, Int32), DbType.Int32)
                paramList.Add(faultParams)
            End If
            If faultSearchBO.AreaID <> "" Then
                faultParams = New ParameterBO("areaId", CType(faultSearchBO.AreaID, Int32), DbType.String)
                paramList.Add(faultParams)
            End If

            If faultSearchBO.ElementID <> "" Then
                faultParams = New ParameterBO("elementId", CType(faultSearchBO.ElementID, Int32), DbType.Int32)
                paramList.Add(faultParams)
            End If

            If faultSearchBO.PriorityID <> "" Then
                faultParams = New ParameterBO("priorityId", CType(faultSearchBO.PriorityID, Int32), DbType.Int32)
                paramList.Add(faultParams)
            End If

            If faultSearchBO.Status <> -1 Then
                faultParams = New ParameterBO("status", CType(faultSearchBO.Status, Integer), DbType.Int32)
                paramList.Add(faultParams)
            End If

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If

            Return rowCount

        End Function

        Public Function GetFaultTransactionLogSearchRowCount(ByRef faultTransactionLogBO As FaultTransactionLogBO) As Integer
            Dim sprocName As String
            Dim rowCount As Integer = 0

            sprocName = SprocNameConstants.GetFaultTransactionLogSearchRowCount
            Dim paramList As ParameterList = New ParameterList()

            Dim faultTransLogParams As ParameterBO

            If faultTransactionLogBO.LocationID <> "" Then
                faultTransLogParams = New ParameterBO("locationId", CType(faultTransactionLogBO.LocationID, Int32), DbType.Int32)
                paramList.Add(faultTransLogParams)
            End If
            If faultTransactionLogBO.AreaID <> "" Then
                faultTransLogParams = New ParameterBO("areaId", CType(faultTransactionLogBO.AreaID, Int32), DbType.String)
                paramList.Add(faultTransLogParams)
            End If

            If faultTransactionLogBO.ElementID <> "" Then
                faultTransLogParams = New ParameterBO("elementId", CType(faultTransactionLogBO.ElementID, Int32), DbType.Int32)
                paramList.Add(faultTransLogParams)
            End If

            If faultTransactionLogBO.TeamID <> "" Then
                faultTransLogParams = New ParameterBO("teamId", CType(faultTransactionLogBO.TeamID, Int32), DbType.Int32)
                paramList.Add(faultTransLogParams)
            End If

            If faultTransactionLogBO.UserID <> -1 Then
                faultTransLogParams = New ParameterBO("userId", CType(faultTransactionLogBO.UserID, Integer), DbType.Int32)
                paramList.Add(faultTransLogParams)
            End If

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If

            Return rowCount

        End Function


#End Region

#End Region

#Region "Methods"

#Region "Fault Search"

        Public Sub GetFaultSearchData(ByRef faultDS As DataSet, ByRef faultSearchBO As FaultSearchBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim faultParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            faultParam = New ParameterBO("noOfRows", faultSearchBO.RowCount, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("offSet", faultSearchBO.PageIndex, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortColumn", faultSearchBO.SortBy, DbType.String)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortOrder", faultSearchBO.SortOrder, DbType.String)
            paramList.Add(faultParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object
            If faultSearchBO.IsSearch = True Then

                If faultSearchBO.LocationID <> "" Then
                    faultParam = New ParameterBO("locationId", CType(faultSearchBO.LocationID, Int32), DbType.Int32)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("locationId", Nothing, DbType.Int32)
                    paramList.Add(faultParam)
                End If

                If faultSearchBO.AreaID <> "" And faultSearchBO.AreaID <> "0" And faultSearchBO.AreaID <> "-1" Then
                    faultParam = New ParameterBO("areaId", CType(faultSearchBO.AreaID, Int32), DbType.String)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("areaId", Nothing, DbType.String)
                    paramList.Add(faultParam)
                End If

                If faultSearchBO.ElementID <> "" And faultSearchBO.ElementID <> "-1" And faultSearchBO.ElementID <> "0" Then
                    faultParam = New ParameterBO("elementId", CType(faultSearchBO.ElementID, Int32), DbType.Int32)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("elementId", Nothing, DbType.Int32)
                    paramList.Add(faultParam)
                End If

                If faultSearchBO.PriorityID <> "" And faultSearchBO.PriorityID <> "-1" Then
                    faultParam = New ParameterBO("priorityId", CType(faultSearchBO.PriorityID, Int32), DbType.Int32)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("priorityId", Nothing, DbType.Int32)
                    paramList.Add(faultParam)
                End If

                If faultSearchBO.Status <> -1 Then
                    faultParam = New ParameterBO("status", CType(faultSearchBO.Status, Integer), DbType.Int32)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("status", Nothing, DbType.Int32)
                    paramList.Add(faultParam)
                End If

                sprocName = SprocNameConstants.GetFaultSerachRecords
            Else
                sprocName = SprocNameConstants.FaultSelectAllList
            End If

            MyBase.LoadDataSet(faultDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If faultDS.Tables(0).Rows.Count = 0 Then

                faultSearchBO.IsFlagStatus = False
                faultSearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                faultSearchBO.IsFlagStatus = True
            End If
        End Sub

        Public Function GetFaultSearch(ByRef faultSearchBO As FaultSearchBO) As DataSet
            Dim sprocName As String
            Dim faultDS As New DataSet

            sprocName = SprocNameConstants.GetFaultSerachRecords
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO

            If faultSearchBO.LocationID <> "" Then
                faultParams = New ParameterBO("locationId", CType(faultSearchBO.LocationID, Int32), DbType.Int32)
                paramList.Add(faultParams)
            End If
            If faultSearchBO.AreaID <> "" Then
                faultParams = New ParameterBO("areaId", CType(faultSearchBO.AreaID, Int32), DbType.String)
                paramList.Add(faultParams)
            End If

            If faultSearchBO.ElementID <> "" Then
                faultParams = New ParameterBO("elementId", CType(faultSearchBO.ElementID, Int32), DbType.Int32)
                paramList.Add(faultParams)
            End If

            If faultSearchBO.PriorityID <> "" Then
                faultParams = New ParameterBO("priorityId", CType(faultSearchBO.PriorityID, Int32), DbType.Int32)
                paramList.Add(faultParams)
            End If

            If faultSearchBO.Status <> -1 Then
                faultParams = New ParameterBO("status", CType(faultSearchBO.Status, Integer), DbType.Int32)
                paramList.Add(faultParams)
            End If

            '' Passing Array of ParameterBO, sproc-name and Dataset to base class method: LoadDataSet
            MyBase.LoadDataSet(faultDS, paramList, sprocName)


            Return faultDS

        End Function

#End Region

#Region "Fault Transaction Log Search"

        Public Sub GetFaultTransactionLogSearchData(ByRef faultTranLogDS As DataSet, ByRef faultTransactionLogBO As FaultTransactionLogBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim faultParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            faultParam = New ParameterBO("noOfRows", faultTransactionLogBO.RowCount, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("offSet", faultTransactionLogBO.PageIndex, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortColumn", faultTransactionLogBO.SortBy, DbType.String)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortOrder", faultTransactionLogBO.SortOrder, DbType.String)
            paramList.Add(faultParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object
            If faultTransactionLogBO.IsSearch = True Then

                If faultTransactionLogBO.LocationID <> "" Then
                    faultParam = New ParameterBO("locationId", CType(faultTransactionLogBO.LocationID, Int32), DbType.Int32)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("locationId", Nothing, DbType.Int32)
                    paramList.Add(faultParam)
                End If

                If faultTransactionLogBO.AreaID <> "" And faultTransactionLogBO.AreaID <> "0" And faultTransactionLogBO.AreaID <> "-1" Then
                    faultParam = New ParameterBO("areaId", CType(faultTransactionLogBO.AreaID, Int32), DbType.String)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("areaId", Nothing, DbType.String)
                    paramList.Add(faultParam)
                End If

                If faultTransactionLogBO.ElementID <> "" And faultTransactionLogBO.ElementID <> "-1" And faultTransactionLogBO.ElementID <> "0" Then
                    faultParam = New ParameterBO("elementId", CType(faultTransactionLogBO.ElementID, Int32), DbType.Int32)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("elementId", Nothing, DbType.Int32)
                    paramList.Add(faultParam)
                End If

                If faultTransactionLogBO.TeamID <> "" And faultTransactionLogBO.TeamID <> "-1" Then
                    faultParam = New ParameterBO("teamId", CType(faultTransactionLogBO.TeamID, Int32), DbType.Int32)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("teamId", Nothing, DbType.Int32)
                    paramList.Add(faultParam)
                End If

                If faultTransactionLogBO.UserID <> -1 And faultTransactionLogBO.UserID <> "" And faultTransactionLogBO.UserID <> "0" Then
                    faultParam = New ParameterBO("userId", CType(faultTransactionLogBO.UserID, Integer), DbType.Int32)
                    paramList.Add(faultParam)
                Else
                    faultParam = New ParameterBO("userId", Nothing, DbType.Int32)
                    paramList.Add(faultParam)
                End If

                sprocName = SprocNameConstants.GetFaultTransactionLogSearch
            Else
                sprocName = SprocNameConstants.FaultSelectAllList
            End If

            MyBase.LoadDataSet(faultTranLogDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If faultTranLogDS.Tables(0).Rows.Count = 0 Then

                faultTransactionLogBO.IsFlagStatus = False
                faultTransactionLogBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                faultTransactionLogBO.IsFlagStatus = True
            End If
        End Sub

#End Region

#Region "Get Fault Values"

        Public Sub GetFaultValues(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
            Dim sprocName As String

            sprocName = SprocNameConstants.GetFaultValues
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO
            faultParams = New ParameterBO("faultID", faultBO.FaultID, DbType.Int32)
            paramList.Add(faultParams)
            MyBase.LoadDataSet(faultDS, paramList, sprocName)
        End Sub

#End Region

#Region "Get TempFault Values"

        Public Sub GetTempFaultValues(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
            Dim sprocName As String

            sprocName = SprocNameConstants.GetTempFaultValues
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO
            faultParams = New ParameterBO("TempFaultId", faultBO.FaultID, DbType.Int32)
            paramList.Add(faultParams)
            MyBase.LoadDataSet(faultDS, paramList, sprocName)
        End Sub

#End Region

#Region "Get Fault Values for PostInspectionReport"

        Public Sub GetPostInspecitonFault(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
            Dim sprocName As String

            sprocName = SprocNameConstants.GetFaultforPostInspection
            'sprocName = SprocNameConstants.GetPreinspectionPopupData
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO
            faultParams = New ParameterBO("FaultLogId", faultBO.FaultID, DbType.Int32)
            paramList.Add(faultParams)
            MyBase.LoadDataSet(faultDS, paramList, sprocName)
        End Sub

#End Region

#Region "Get Post Inspeciton Fault Update"

        Public Sub GetPostInspecitonFaultUpdate(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
            Dim sprocName As String

            'sprocName = SprocNameConstants.GetFaultforPostInspection
            sprocName = SprocNameConstants.GetPostInspectionPopupData
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO
            faultParams = New ParameterBO("FaultLogId", faultBO.FaultID, DbType.Int32)
            paramList.Add(faultParams)

            faultParams = New ParameterBO("RepairId", faultBO.RepairId, DbType.Int32)
            paramList.Add(faultParams)

            MyBase.LoadDataSet(faultDS, paramList, sprocName)
        End Sub
#End Region

#Region "Get Preinspection Popup data"

        Public Sub GetPreinspectionPopupData(ByRef faultDS As DataSet, ByRef faultBO As FaultBO)
            Dim sprocName As String

            sprocName = SprocNameConstants.GetPreinspectionPopupData
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO
            faultParams = New ParameterBO("TempFaultId", faultBO.FaultID, DbType.Int32)
            paramList.Add(faultParams)
            MyBase.LoadDataSet(faultDS, paramList, sprocName)
        End Sub

        ''' <summary>
        ''' To load Due date of the preinspection Popup (gul)
        ''' </summary>
        ''' <param name="DueDateDS"></param>
        ''' <param name="faultBO"></param>
        ''' <remarks></remarks>
        Public Sub GetPreInspectionDueDate(ByRef DueDateDS As DataSet, ByRef faultBO As FaultBO)

            Dim sprocName As String = SprocNameConstants.GetPreinspectionPopupDueDate
            Dim paramList As ParameterList = New ParameterList()
            Dim faultParam As ParameterBO
            faultParam = New ParameterBO("faultId", faultBO.FaultID, DbType.Int32)
            paramList.Add(faultParam)
            MyBase.LoadDataSet(DueDateDS, paramList, sprocName)

        End Sub

#End Region

#Region "Get Customer Info"

        Public Sub GetCustomerInfo(ByRef custDS As DataSet, ByRef customerBO As CustomerBO)
            Dim sprocName As String

            sprocName = SprocNameConstants.GetCustomerInfoForFault
            Dim paramList As ParameterList = New ParameterList()

            Dim custParams As ParameterBO
            custParams = New ParameterBO("CUSTOMERID", customerBO.Id, DbType.Int32)
            paramList.Add(custParams)
            MyBase.LoadDataSet(custDS, paramList, sprocName)
        End Sub

#End Region

#Region "Get Priority Values"
        Public Sub GetPriorityValues(ByRef priorityDS As DataSet, ByRef priorityBO As PriorityBO)
            Dim sprocName As String

            sprocName = SprocNameConstants.GetFaultPriorityRecord
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO
            faultParams = New ParameterBO("PRIORITYID", priorityBO.PriorityID, DbType.Int32)
            paramList.Add(faultParams)
            MyBase.LoadDataSet(priorityDS, paramList, sprocName)
        End Sub

#End Region

#Region "Add New Fault"

        Public Sub AddNewFault(ByRef faultBO As FaultBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.CreatNewFault

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters elementID
            Dim elementID As ParameterBO = New ParameterBO("ELEMENTID", faultBO.ElementID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters priorityID
            Dim priorityID As ParameterBO = New ParameterBO("PRIORITYID", faultBO.PriorityID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters description
            Dim description As ParameterBO = New ParameterBO("DESCRIPTION", faultBO.Description, DbType.AnsiString)

            '' Creating input ParameterBO objects and passing prameters netCost
            Dim netCost As ParameterBO = New ParameterBO("NETCOST", faultBO.NetCost, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vat
            Dim vat As ParameterBO = New ParameterBO("VAT", faultBO.Vat, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters gross
            Dim gross As ParameterBO = New ParameterBO("GROSS", faultBO.Gross, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vatRateID
            Dim vatRateID As ParameterBO = New ParameterBO("VATRATEID", faultBO.VatRateID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters effectFrom
            Dim effectFrom As ParameterBO = New ParameterBO("EFFECTFROM", faultBO.EffectFrom, DbType.String)

            '' Creating input ParameterBO objects and passing prameters recharge
            Dim recharge As ParameterBO = New ParameterBO("RECHARGE", faultBO.Recharge, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters preInspection
            Dim preInspection As ParameterBO = New ParameterBO("PREINSPECTION", faultBO.PreInspection, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters postInspection
            Dim postInspection As ParameterBO = New ParameterBO("POSTINSPECTION", faultBO.PostInspection, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters elementID
            Dim stockConditionItem As ParameterBO = New ParameterBO("STOCKCONDITIONITEM", faultBO.StockConditionItem, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters faultActive
            Dim faultActive As ParameterBO = New ParameterBO("FAULTACTIVE", faultBO.FaultActive, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters faultAction
            Dim faultAction As ParameterBO = New ParameterBO("FAULTACTION", faultBO.FaultAction, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters submitDate
            Dim submitDate As ParameterBO = New ParameterBO("SUBMITDATE", faultBO.SubmitDate, DbType.String)

            '' Creating input ParameterBO objects and passing parameters userId
            Dim userID As ParameterBO = New ParameterBO("USERID", faultBO.UserID, DbType.Int32)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(elementID)
            inParamList.Add(priorityID)
            inParamList.Add(description)
            inParamList.Add(netCost)
            inParamList.Add(vat)
            inParamList.Add(gross)
            inParamList.Add(vatRateID)
            inParamList.Add(effectFrom)
            inParamList.Add(recharge)
            inParamList.Add(preInspection)
            inParamList.Add(postInspection)
            inParamList.Add(stockConditionItem)
            inParamList.Add(faultActive)
            inParamList.Add(faultAction)
            inParamList.Add(submitDate)
            inParamList.Add(userID)

            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                faultBO.IsFlagStatus = False
            Else
                faultBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "Amend a Fault"

        Public Sub AmendFault(ByRef faultBO As FaultBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AmendFault

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters faultID
            Dim faultID As ParameterBO = New ParameterBO("FAULTID", faultBO.FaultID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters priorityID
            Dim priorityID As ParameterBO = New ParameterBO("PRIORITYID", faultBO.PriorityID, DbType.Int32)

            Dim description As ParameterBO = New ParameterBO("DESCRIPTION", faultBO.Description, DbType.String)
            '' Creating input ParameterBO objects and passing prameters netCost
            Dim netCost As ParameterBO = New ParameterBO("NETCOST", faultBO.NetCost, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vat
            Dim vat As ParameterBO = New ParameterBO("VAT", faultBO.Vat, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters gross
            Dim gross As ParameterBO = New ParameterBO("GROSS", faultBO.Gross, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vatRateID
            Dim vatRateID As ParameterBO = New ParameterBO("VATRATEID", faultBO.VatRateID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters effectFrom
            Dim effectFrom As ParameterBO = New ParameterBO("EFFECTFROM", faultBO.EffectFrom, DbType.String)

            '' Creating input ParameterBO objects and passing prameters recharge
            Dim recharge As ParameterBO = New ParameterBO("RECHARGE", faultBO.Recharge, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters preInspection
            Dim preInspection As ParameterBO = New ParameterBO("PREINSPECTION", faultBO.PreInspection, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters postInspection
            Dim postInspection As ParameterBO = New ParameterBO("POSTINSPECTION", faultBO.PostInspection, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters elementID
            Dim stockConditionItem As ParameterBO = New ParameterBO("STOCKCONDITIONITEM", faultBO.StockConditionItem, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters faultActive
            Dim faultActive As ParameterBO = New ParameterBO("FAULTACTIVE", faultBO.FaultActive, DbType.Int32)

            ''Creating input ParameterBO objects and passing parameters userID
            Dim userID As ParameterBO = New ParameterBO("USERID", faultBO.UserID, DbType.Int32)

            ''Creating input ParameterBO objects and passing parameters submitdate
            Dim submitDate As ParameterBO = New ParameterBO("SUBMITDATE", faultBO.SubmitDate, DbType.String)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(faultID)
            inParamList.Add(priorityID)
            inParamList.Add(description)
            inParamList.Add(netCost)
            inParamList.Add(vat)
            inParamList.Add(gross)
            inParamList.Add(vatRateID)
            inParamList.Add(effectFrom)
            inParamList.Add(recharge)
            inParamList.Add(preInspection)
            inParamList.Add(postInspection)
            inParamList.Add(stockConditionItem)
            inParamList.Add(userID)
            inParamList.Add(submitDate)
            inParamList.Add(faultActive)
            

            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                faultBO.IsFlagStatus = False
            Else
                faultBO.IsFlagStatus = True
            End If



        End Sub
#End Region

#Region "Amend Element Name"

        Public Sub AmendElementName(ByRef elementBO As ElementBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AmendElementName

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters elementID
            Dim elementID As ParameterBO = New ParameterBO("ELEMENTID", elementBO.ElementID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters elementName
            Dim elementName As ParameterBO = New ParameterBO("ELEMENTNAME", elementBO.ElementName, DbType.String)




            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(elementID)
            inParamList.Add(elementName)

            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                elementBO.IsFlagStatus = False
            Else
                elementBO.IsFlagStatus = True
            End If



        End Sub

#End Region

#Region "Get Vat Rate"
        Public Sub getVatValues(ByRef vatBO As VatBO)
            Dim sprocName As String
            Dim myDS As New DataSet()
            sprocName = SprocNameConstants.GetVatValues
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO
            faultParams = New ParameterBO("VATID", vatBO.VatID, DbType.Int32)
            paramList.Add(faultParams)
            MyBase.LoadDataSet(myDS, paramList, sprocName)
            If (myDS.Tables(0).Rows.Count > 0 And Not (myDS Is Nothing)) Then
                vatBO.VatID = CType(myDS.Tables(0).Rows(0)("VATID"), Int32)
                vatBO.VatName = myDS.Tables(0).Rows(0)("VATNAME").ToString()
                vatBO.VatRate = CType(myDS.Tables(0).Rows(0)("VATRATE"), Double)
                vatBO.VatCode = CType(myDS.Tables(0).Rows(0)("VATCODE"), Char)
            End If

        End Sub
#End Region

#Region "Get Fault Priorities"
        Public Sub GetFaultPriorities(ByRef priorityDs As DataSet, ByRef priorityBO As PriorityBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim priorityParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            priorityParam = New ParameterBO("noOfRows", priorityBO.RowCount, DbType.Int32)
            paramList.Add(priorityParam)
            priorityParam = New ParameterBO("offSet", priorityBO.PageIndex, DbType.Int32)
            paramList.Add(priorityParam)
            priorityParam = New ParameterBO("sortColumn", priorityBO.SortBy, DbType.String)
            paramList.Add(priorityParam)
            priorityParam = New ParameterBO("sortOrder", priorityBO.SortOrder, DbType.String)
            paramList.Add(priorityParam)
            sprocName = SprocNameConstants.GetFaultPriorities

            MyBase.LoadDataSet(priorityDs, paramList, sprocName)

            If (priorityDs.Tables(0).Rows.Count > 0) Then
                priorityBO.IsFlagStatus = True
            Else
                priorityBO.IsFlagStatus = False
                priorityBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            End If
        End Sub
#End Region

#Region "Add a Priority"
        Public Sub AddAmendPriority(ByRef priorityBO As PriorityBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AddAmendFaultPriorities

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters priorityID
            Dim priorityID As ParameterBO = New ParameterBO("PRIORITYID", priorityBO.PriorityID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters priorityName
            Dim priorityName As ParameterBO = New ParameterBO("PRIORITYNAME", priorityBO.PriorityName, DbType.String)

            '' Creating input ParameterBO objects and passing prameters responseTime
            Dim responseTime As ParameterBO = New ParameterBO("RESPONSETIME", priorityBO.ResponseTime, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters status
            Dim status As ParameterBO = New ParameterBO("STATUS", priorityBO.Status, DbType.Boolean)

            '' Creating input ParameterBO objects and passing prameters days
            Dim days As ParameterBO = New ParameterBO("DAYS", priorityBO.Days, DbType.Boolean)

            '' Creating input ParameterBO objects and passing prameters hours
            Dim hours As ParameterBO = New ParameterBO("HRS", priorityBO.Hours, DbType.Boolean)


            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(priorityID)
            inParamList.Add(priorityName)
            inParamList.Add(responseTime)
            inParamList.Add(status)
            inParamList.Add(days)
            inParamList.Add(hours)
            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                priorityBO.IsFlagStatus = False
            Else
                priorityBO.IsFlagStatus = True
            End If



        End Sub

#End Region

#Region "GetPricingControlData"
        Public Sub getPricingControlData(ByVal spName As String, ByRef faultBO As FaultBO, ByVal inParam As String, ByVal inParamName As String)

            Dim sprocName As String = spName
            Dim inParamList As New ParameterList()
            Dim inParamBO As New ParameterBO(inParamName, Integer.Parse(inParam), DbType.Int32)
            inParamList.Add(inParamBO)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ''Iterate the dataReader
            If myDataReader.Read Then

                Dim faultId As Integer
                Dim vatRateId As Integer
                Dim NetCost As Double
                Dim Vat As Double
                Dim Gross As Double
                Dim VatName As String = String.Empty
                Dim VatRate As Double


                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("FaultID")) Then
                    faultId = myDataReader.GetInt32(myDataReader.GetOrdinal("FaultID"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("VatRateID")) Then
                    vatRateId = myDataReader.GetInt32(myDataReader.GetOrdinal("VatRateID"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NetCost")) Then
                    NetCost = myDataReader.GetDouble(myDataReader.GetOrdinal("NetCost"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Vat")) Then
                    Vat = myDataReader.GetDouble(myDataReader.GetOrdinal("Vat"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Gross")) Then
                    Gross = myDataReader.GetDouble(myDataReader.GetOrdinal("Gross"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("VATNAME")) Then
                    VatName = myDataReader.GetString(myDataReader.GetOrdinal("VATNAME"))
                End If
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("VATRATE")) Then
                    VatRate = myDataReader.GetDouble(myDataReader.GetOrdinal("VATRATE"))
                End If

                faultBO.FaultID = faultId
                faultBO.NetCost = NetCost
                faultBO.Vat = Vat
                faultBO.Gross = Gross
                faultBO.VatName = VatName
                faultBO.VatRateID = vatRateId
                faultBO.VatRate = VatRate
                faultBO.IsRecordRetrieved = True
            End If



        End Sub
#End Region

#Region "Add Pricing Control Data"

        Public Sub AddPricingControlData(ByRef faultBO As FaultBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AddPricingControlData

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters locationID
            Dim locationID As ParameterBO = New ParameterBO("LOCATIONID", faultBO.LocationID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters areaID
            Dim areaID As ParameterBO = New ParameterBO("AREAID", faultBO.AreaID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters elementID
            Dim elementID As ParameterBO = New ParameterBO("ELEMENTID", faultBO.ElementID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters descriptionID
            Dim faultID As ParameterBO = New ParameterBO("DESCRIPTIONID", faultBO.FaultID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters netCost
            Dim netCost As ParameterBO = New ParameterBO("NETCOST", faultBO.NetCost, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vat
            Dim vat As ParameterBO = New ParameterBO("VAT", faultBO.Vat, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters gross
            Dim gross As ParameterBO = New ParameterBO("GROSS", faultBO.Gross, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vatRateID
            Dim vatRateID As ParameterBO = New ParameterBO("VATRATEID", faultBO.VatRateID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters effectFrom
            Dim effectFrom As ParameterBO = New ParameterBO("EFFECTFROM", faultBO.EffectFrom, DbType.String)

            '' Creating input ParameterBO objects and passing prameters inflation rate
            Dim inflationValue As ParameterBO = New ParameterBO("INFLATIONVALUE", faultBO.InflationValue, DbType.Double)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(locationID)
            inParamList.Add(areaID)
            inParamList.Add(elementID)
            inParamList.Add(faultID)
            inParamList.Add(netCost)
            inParamList.Add(vat)
            inParamList.Add(gross)
            inParamList.Add(vatRateID)
            inParamList.Add(effectFrom)
            inParamList.Add(inflationValue)


            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                faultBO.IsFlagStatus = False
            Else
                faultBO.IsFlagStatus = True
            End If



        End Sub
#End Region

#Region "Add New Fault Journal"

        Public Function AddNewJournal(ByRef faultJournalBO As FaultJournalBO) As Int32

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AddFaultJournal

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters customerId
            Dim customerId As ParameterBO = New ParameterBO("CUSTOMERID", faultJournalBO.CustomerId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters tenancyId
            Dim tenancyID As ParameterBO = New ParameterBO("TENANCYID", faultJournalBO.TenancyId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters propertyId
            Dim propertyID As ParameterBO = New ParameterBO("PROPERTYID", faultJournalBO.PropertyId, DbType.AnsiString)

            '' Creating input ParameterBO objects and passing prameters ItemId
            Dim itemID As ParameterBO = New ParameterBO("ITEMID", faultJournalBO.ItemId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters itemNatureId
            Dim itemNatureID As ParameterBO = New ParameterBO("ITEMNATUREID", faultJournalBO.ItemNatureId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters currentItemStatusId
            Dim currentItemStatusId As ParameterBO = New ParameterBO("CURRENTITEMSTATUSID", faultJournalBO.CurrentItemStatusId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters letterAction
            Dim letterAction As ParameterBO
            If faultJournalBO.LetterAction <> -1 Then
                letterAction = New ParameterBO("LETTERACTION", faultJournalBO.LetterAction, DbType.Int32)
            Else
                letterAction = New ParameterBO("LETTERACTION", Nothing, DbType.Int32)
            End If


            '' Creating input ParameterBO objects and passing prameters creationDate
            Dim creationDate As ParameterBO = New ParameterBO("CREATIONDATE", faultJournalBO.CreationDate, DbType.DateTime)

            '' Creating input ParameterBO objects and passing prameters title
            Dim title As ParameterBO = New ParameterBO("TITLE", faultJournalBO.Title, DbType.AnsiString)

            '' Creating input ParameterBO objects and passing prameters lastActionDate
            Dim lastActionDate As ParameterBO
            If faultJournalBO.LastActionDate <> Nothing Then
                lastActionDate = New ParameterBO("LASTACTIONDATE", faultJournalBO.LastActionDate, DbType.DateTime)
            Else
                lastActionDate = New ParameterBO("LASTACTIONDATE", Nothing, DbType.String)
            End If

            Dim nextActionDate As ParameterBO
            '' Creating input ParameterBO objects and passing prameters nextActionDate
            If (faultJournalBO.NextActionDate <> Nothing) Then
                nextActionDate = New ParameterBO("NEXTACTIONDATE", faultJournalBO.NextActionDate, DbType.DateTime)
            Else
                nextActionDate = New ParameterBO("NEXTACTIONDATE", Nothing, DbType.String)
            End If


            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)

            '' Creating output ParameterBO objects and passing prameters journalId
            Dim journalId As ParameterBO = New ParameterBO("JOURNALID", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(customerId)
            inParamList.Add(tenancyID)
            inParamList.Add(propertyID)
            inParamList.Add(itemID)
            inParamList.Add(itemNatureID)
            inParamList.Add(currentItemStatusId)
            inParamList.Add(letterAction)
            inParamList.Add(creationDate)
            inParamList.Add(title)
            inParamList.Add(lastActionDate)
            inParamList.Add(nextActionDate)


            ''adding object to output paramList
            outParamList.Add(result)
            outParamList.Add(journalId)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                faultJournalBO.IsFlagStatus = False
            Else
                faultJournalBO.IsFlagStatus = True
            End If

            Return outParamList.Item(1).Value

        End Function
#End Region

#Region "Add New Fault RepairHistory"

        Public Sub AddNewRepairHistory(ByRef faultRepairHistoryBO As FaultRepairHistoryBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AddFaultRepairHistory

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters journalId
            Dim journalId As ParameterBO = New ParameterBO("JOURNALID", faultRepairHistoryBO.JournalId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters itemStatusId
            Dim itemStatusId As ParameterBO = New ParameterBO("ITEMSTATUSID", faultRepairHistoryBO.ItemStatusId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters itemActionId
            Dim itemActionId As ParameterBO = New ParameterBO("ITEMACTIONID", faultRepairHistoryBO.ItemActionId, DbType.AnsiString)

            '' Creating input ParameterBO objects and passing prameters lastActionDate
            Dim lastActionDate As ParameterBO = New ParameterBO("LASTACTIONDATE", faultRepairHistoryBO.LastActionDate, DbType.DateTime)

            '' Creating input ParameterBO objects and passing prameters lastActionUserId
            Dim lastActionUserId As ParameterBO = New ParameterBO("LASTACTIONUSERID", faultRepairHistoryBO.LastActionUserId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters faultLogId
            Dim faultLogId As ParameterBO = New ParameterBO("FAULTLOGID", faultRepairHistoryBO.FaultLogId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters orgId
            Dim orgId As ParameterBO = New ParameterBO("ORGID", faultRepairHistoryBO.OrgId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters scopeId
            Dim scopeId As ParameterBO = New ParameterBO("SCOPEID", faultRepairHistoryBO.ScopeId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters title
            Dim title As ParameterBO = New ParameterBO("TITLE", faultRepairHistoryBO.Title, DbType.AnsiString)

            '' Creating input ParameterBO objects and passing prameters notes
            Dim notes As ParameterBO = New ParameterBO("NOTES", faultRepairHistoryBO.Notes, DbType.AnsiString)


            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(journalId)
            inParamList.Add(itemStatusId)
            inParamList.Add(itemActionId)
            inParamList.Add(lastActionDate)
            inParamList.Add(lastActionUserId)
            inParamList.Add(faultLogId)
            inParamList.Add(orgId)
            inParamList.Add(scopeId)
            inParamList.Add(title)
            inParamList.Add(notes)

            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                faultRepairHistoryBO.IsFlagStatus = False
            Else
                faultRepairHistoryBO.IsFlagStatus = True
            End If



        End Sub
#End Region

#Region "Save Inspection Record"

        Public Sub SaveReactiveRepairUpdate(ByRef piBO As PreInspectBO)

            Dim sprocName As String = SprocNameConstants.SaveReactiveRepairJournalInspection
            Dim inParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList
            Dim paramBO As ParameterBO
            paramBO = New ParameterBO("FaultLogId", piBO.FaultLogId, DbType.Int32)
            inParameterList.Add(paramBO)
            paramBO = New ParameterBO("OrgId", piBO.OrgId, DbType.Int32)
            inParameterList.Add(paramBO)

            paramBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParameterList.Add(paramBO)

            MyBase.SaveRecord(inParameterList, outParameterList, sprocName)

            Dim result As Integer = outParameterList.Item(0).Value

            If result = -1 Then
                piBO.IsFlagStatus = False

            Else
                piBO.IsFlagStatus = True
            End If

        End Sub

        Public Sub SaveInspectionRecord(ByRef piBO As PreInspectBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AddInspectionRecord
            'Dim sprocName As String = SprocNameConstants.AddPreInspectionReportData
            '' This List will hold instances of type PreInspectBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not

            Dim userId As ParameterBO = New ParameterBO("userId", piBO.UserId, DbType.Int32)
            inParamList.Add(userId)

            If piBO.OrgId <> -1 Then
                Dim orgId As ParameterBO = New ParameterBO("orgId", piBO.OrgId, DbType.Int32)
                inParamList.Add(orgId)
            Else
                Dim orgId As ParameterBO = New ParameterBO("orgId", Nothing, DbType.Int32)
                inParamList.Add(orgId)
            End If

            Dim notes As ParameterBO = New ParameterBO("notes", piBO.Notes, DbType.String)
            inParamList.Add(notes)

            Dim inspectionDate As ParameterBO = New ParameterBO("inspectionDate", piBO.InspectionDate, DbType.DateTime)
            inParamList.Add(inspectionDate)

            If piBO.DueDate <> Nothing Then
                Dim dueDate As ParameterBO = New ParameterBO("dueDate", piBO.DueDate, DbType.DateTime)
                inParamList.Add(dueDate)
            End If


            Dim netCost As ParameterBO = New ParameterBO("netCost", piBO.NetCost, DbType.Double)
            inParamList.Add(netCost)

            Dim faultLogId As ParameterBO = New ParameterBO("faultLogId", piBO.FaultLogId, DbType.Int32)
            inParamList.Add(faultLogId)

            If piBO.Approved <> -1 Then
                Dim approved As ParameterBO = New ParameterBO("approved", piBO.Approved, DbType.Int32)
                inParamList.Add(approved)
            Else

                Dim approved As ParameterBO = New ParameterBO("approved", Nothing, DbType.Int32)
                inParamList.Add(approved)

            End If

            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            '' Passing Array of piBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the PREINSPECTONID in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                piBO.IsFlagStatus = False
            Else
                piBO.IsFlagStatus = True
            End If

        End Sub

        Public Sub SaveInspectionAppointment(ByRef piBO As PreInspectBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            'Dim sprocName As String = SprocNameConstants.AddInspectionRecord
            Dim sprocName As String = SprocNameConstants.SaveAppointment
            'Dim sprocName As String = SprocNameConstants.AddPreInspectionReportData
            '' This List will hold instances of type PreInspectBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not

            Dim userId As ParameterBO = New ParameterBO("userId", piBO.UserId, DbType.Int32)
            inParamList.Add(userId)

            If piBO.OrgId <> -1 Then
                Dim orgId As ParameterBO = New ParameterBO("orgId", piBO.OrgId, DbType.Int32)
                inParamList.Add(orgId)
            Else
                Dim orgId As ParameterBO = New ParameterBO("orgId", Nothing, DbType.Int32)
                inParamList.Add(orgId)
            End If

            Dim notes As ParameterBO = New ParameterBO("notes", piBO.Notes, DbType.String)
            inParamList.Add(notes)

            Dim inspectionDate As ParameterBO = New ParameterBO("inspectionDate", piBO.InspectionDate, DbType.DateTime)
            inParamList.Add(inspectionDate)

            If piBO.DueDate <> Nothing Then
                Dim dueDate As ParameterBO = New ParameterBO("dueDate", piBO.DueDate, DbType.DateTime)
                inParamList.Add(dueDate)
            End If


            Dim netCost As ParameterBO = New ParameterBO("netCost", piBO.NetCost, DbType.Double)
            inParamList.Add(netCost)

            Dim faultLogId As ParameterBO = New ParameterBO("faultLogId", piBO.FaultLogId, DbType.Int32)
            inParamList.Add(faultLogId)

            If piBO.Approved <> -1 Then
                Dim approved As ParameterBO = New ParameterBO("approved", piBO.Approved, DbType.Int32)
                inParamList.Add(approved)
            Else

                Dim approved As ParameterBO = New ParameterBO("approved", Nothing, DbType.Int32)
                inParamList.Add(approved)

            End If

            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            '' Passing Array of piBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the PREINSPECTONID in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                piBO.IsFlagStatus = False
            Else
                piBO.IsFlagStatus = True
            End If

        End Sub


#End Region

#Region "Update Pre Inspection Report Data "

        Public Sub UpdatePreInspectionReportData(ByRef piBO As PreInspectBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            'Dim sprocName As String = SprocNameConstants.AddInspectionRecord
            Dim sprocName As String = SprocNameConstants.AddPreInspectionReportData
            '' This List will hold instances of type PreInspectBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not

            Dim userId As ParameterBO = New ParameterBO("userId", piBO.UserId, DbType.Int32)
            inParamList.Add(userId)

            Dim actionUserId As ParameterBO = New ParameterBO("actionUserId", piBO.ActionUserId, DbType.Int32)
            inParamList.Add(actionUserId)

            Dim recharge As ParameterBO = New ParameterBO("recharge", piBO.Recharge, DbType.Boolean)
            inParamList.Add(recharge)

            If piBO.OrgId <> -1 Then
                Dim orgId As ParameterBO = New ParameterBO("orgId", piBO.OrgId, DbType.Int32)
                inParamList.Add(orgId)
            End If

            Dim notes As ParameterBO = New ParameterBO("notes", piBO.Notes, DbType.String)
            inParamList.Add(notes)

            Dim inspectionDate As ParameterBO = New ParameterBO("inspectionDate", piBO.InspectionDate, DbType.DateTime)
            inParamList.Add(inspectionDate)

            If piBO.DueDate <> Nothing Then
                Dim dueDate As ParameterBO = New ParameterBO("dueDate", piBO.DueDate, DbType.DateTime)
                inParamList.Add(dueDate)
            End If


            Dim netCost As ParameterBO = New ParameterBO("netCost", piBO.NetCost, DbType.Double)
            inParamList.Add(netCost)

            Dim faultLogId As ParameterBO = New ParameterBO("faultLogId", piBO.FaultLogId, DbType.Int32)
            inParamList.Add(faultLogId)

            If (piBO.Approved <> -1) Then
                Dim approved As ParameterBO = New ParameterBO("approved", piBO.Approved, DbType.Int32)
                inParamList.Add(approved)

            Else

                Dim approved As ParameterBO = New ParameterBO("approved", Nothing, DbType.Int32)
                inParamList.Add(approved)

            End If


            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            '' Passing Array of piBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)


            ''Out parameter will return the PREINSPECTONID in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                piBO.IsFlagStatus = False
            Else
                piBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "Save Inspection Record with contractor"

        Public Sub SaveInspectionRecordContractor(ByRef piBO As PreInspectBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            ''Dim sprocName As String = SprocNameConstants.AddInspectionRecordWithContractor
            Dim sprocName As String = SprocNameConstants.SaveAppointment
            '' This List will hold instances of type PreInspectBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not

            Dim userId As ParameterBO = New ParameterBO("USERID", piBO.UserId, DbType.Int32)
            inParamList.Add(userId)

            Dim inspectionDate As ParameterBO = New ParameterBO("INSPECTIONDATE", piBO.InspectionDate, DbType.DateTime)
            inParamList.Add(inspectionDate)

            Dim netCost As ParameterBO = New ParameterBO("NETCOST", piBO.NetCost, DbType.Double)
            inParamList.Add(netCost)

            If piBO.DueDate <> Nothing Then
                Dim dueDate As ParameterBO = New ParameterBO("DUEDATE", piBO.DueDate, DbType.DateTime)
                inParamList.Add(dueDate)
            End If

            Dim notes As ParameterBO = New ParameterBO("NOTES", piBO.Notes, DbType.String)
            inParamList.Add(notes)

            If piBO.OrgId <> -1 Then
                Dim orgId As ParameterBO = New ParameterBO("ORGID", piBO.OrgId, DbType.Int32)
                inParamList.Add(orgId)
            End If

            If piBO.Approved <> -1 Then
                Dim approved As ParameterBO = New ParameterBO("APPROVED", piBO.Approved, DbType.Int32)
                inParamList.Add(approved)

            Else
                Dim approved As ParameterBO = New ParameterBO("APPROVED", Nothing, DbType.Int32)
                inParamList.Add(approved)
            End If

            Dim tempFaultId As ParameterBO = New ParameterBO("faultLogId", piBO.TempFaultId, DbType.Int32)
            inParamList.Add(tempFaultId)

            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            '' Passing Array of piBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ' ''Out parameter will return the PREINSPECTONID in case of success or -1 otherwise
            'Dim qryResult As Integer = outParamList.Item(0).Value

            ' ''In the case of invalid information provided
            'If qryResult = -1 Then
            '    piBO.IsFlagStatus = False
            'Else
            '    piBO.IsFlagStatus = True
            'End If

        End Sub

#End Region

#Region "Get FaultBasket Data for update"
        Public Function GetFaultBasketUpdate(ByVal faultLogId As Integer) As DataSet

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetFaultBasketUpdateData

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create faultLog parameters
            Dim faultLogParam As ParameterBO = New ParameterBO("FaultLogId", faultLogId, DbType.Int32)


            '' Adding object to paramList
            inParamList.Add(faultLogParam)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            Dim dsFault As DataSet = New DataSet
            MyBase.LoadDataSet(dsFault, inParamList, sprocName)

            Return dsFault

        End Function

#End Region

#Region "Update Fault Contractor in Fault Log"
        Public Sub UpdateFaultLogContractor(ByVal faultLogId As Integer, ByVal orgId As Integer, ByVal notes As String, ByVal email As Boolean)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.UpdateFaultLogContractor

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create parameters           
            Dim faultLogParam As ParameterBO = New ParameterBO("FaultLogId", faultLogId, DbType.Int32)
            Dim orgParam As ParameterBO = New ParameterBO("ORGID", orgId, DbType.Int32)
            Dim notesParam As ParameterBO = New ParameterBO("notes", notes, DbType.String)
            Dim mail As ParameterBO = New ParameterBO("email", email, DbType.Boolean)

            '' Adding object to paramList
            inParamList.Add(faultLogParam)
            inParamList.Add(orgParam)
            inParamList.Add(notesParam)
            inParamList.Add(mail)

            Dim outParamList As ParameterList = New ParameterList()

            '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)


        End Sub
#End Region

#Region "Get JSSummary Data for updated record"


        Public Sub GetJSSummaryUpdateData(ByRef JobSheetDS As DataSet, ByRef faultLogId As Integer)


            Dim sProc As String

            Dim JSsummaryParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim JSSummaryParamBO As ParameterBO

            JSSummaryParamBO = New ParameterBO("FaultLogId", faultLogId, DbType.String)
            JSsummaryParameterList.Add(JSSummaryParamBO)

            sProc = SprocNameConstants.GetJSSummarytoUpdateFaultLog

            MyBase.LoadDataSet(JobSheetDS, JSsummaryParameterList, sProc)


        End Sub
#End Region

#Region "Get JSSummary Asbestos Data for updated record"

        Public Sub GetJSSummaryAsbestosUpdateData(ByRef JobSheetDS As DataSet, ByRef faultLogId As Integer)
            Dim sProc As String
            Dim JSsummaryParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()
            Dim JSSummaryParamBO As ParameterBO
            JSSummaryParamBO = New ParameterBO("FaultLogId", faultLogId, DbType.String)
            JSsummaryParameterList.Add(JSSummaryParamBO)
            sProc = SprocNameConstants.GetJSAsbestosSummarytoUpdateFaultLog
            MyBase.LoadDataSet(JobSheetDS, JSsummaryParameterList, sProc)
        End Sub

#End Region


#Region "Get Asbestos Data for Summary record"

        Public Sub GetPropertySummaryAsbestosData(ByRef JobSheetDS As DataSet, ByRef CustomerId As Integer)
            Dim sProc As String
            Dim JSsummaryParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()
            Dim JSSummaryParamBO As ParameterBO
            JSSummaryParamBO = New ParameterBO("CustomerId", CustomerId, DbType.String)
            JSsummaryParameterList.Add(JSSummaryParamBO)
            sProc = SprocNameConstants.GetAsbestosSummarytoPropertyId
            MyBase.LoadDataSet(JobSheetDS, JSsummaryParameterList, sProc)
        End Sub

#End Region

#Region "Reported Fault Search"

        Public Sub GetReportedFault(ByRef rfDS As DataSet, ByRef rfSearchBO As ReportedFaultSearchBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type ReportedFaultSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim tenancyParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            tenancyParam = New ParameterBO("noOfRows", rfSearchBO.RowCount, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("offSet", rfSearchBO.PageIndex, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortColumn", rfSearchBO.SortBy, DbType.String)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortOrder", rfSearchBO.SortOrder, DbType.String)
            paramList.Add(tenancyParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object

            If rfSearchBO.IsSearch Then

                If rfSearchBO.JS <> -1 Then
                    tenancyParam = New ParameterBO("JS", CType(rfSearchBO.JS, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.LastName <> String.Empty Then

                    tenancyParam = New ParameterBO("lastName", rfSearchBO.LastName, DbType.String)
                    paramList.Add(tenancyParam)

                End If


                'If rfSearchBO.DateValue <> "12:00:00 AM" Then
                '    tenancyParam = New ParameterBO("date", rfSearchBO.DateValue, DbType.String)
                '    paramList.Add(tenancyParam)
                'End If

                If rfSearchBO.DateValue <> String.Empty Then

                    tenancyParam = New ParameterBO("date", rfSearchBO.DateValue, DbType.String)
                    paramList.Add(tenancyParam)

                End If

                If rfSearchBO.Nature <> -1 Then
                    tenancyParam = New ParameterBO("nature", CType(rfSearchBO.Nature, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.Status <> -1 Then
                    tenancyParam = New ParameterBO("status", CType(rfSearchBO.Status, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.TextValue <> String.Empty Then

                    tenancyParam = New ParameterBO("text", rfSearchBO.TextValue, DbType.String)
                    paramList.Add(tenancyParam)
                End If

                sprocName = SprocNameConstants.ReportedFaultSearch
            Else
                sprocName = SprocNameConstants.ReportedFaultSearchList

            End If

            MyBase.LoadDataSet(rfDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If rfDS.Tables(0).Rows.Count = 0 Then

                rfSearchBO.IsFlagStatus = False
                rfSearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                rfSearchBO.IsFlagStatus = True
            End If

        End Sub

#End Region
#Region "Reported Fault Report for Finance"

        Public Sub GetReportedFaultReport(ByRef rfDS As DataSet, ByRef rfSearchBO As ReportedFaultSearchBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type ReportedFaultSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim tenancyParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            tenancyParam = New ParameterBO("noOfRows", rfSearchBO.RowCount, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("offSet", rfSearchBO.PageIndex, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortColumn", rfSearchBO.SortBy, DbType.String)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortOrder", rfSearchBO.SortOrder, DbType.String)
            paramList.Add(tenancyParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object

            If rfSearchBO.IsSearch Then

                If rfSearchBO.JS <> -1 Then
                    tenancyParam = New ParameterBO("JS", CType(rfSearchBO.JS, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.LastName <> String.Empty Then

                    tenancyParam = New ParameterBO("lastName", rfSearchBO.LastName, DbType.String)
                    paramList.Add(tenancyParam)

                End If

                ' From Date Paramater
                If rfSearchBO.DateValue <> String.Empty Then

                    tenancyParam = New ParameterBO("date", rfSearchBO.DateValue, DbType.String)
                    paramList.Add(tenancyParam)

                End If
                ' To Date Paramater
                If rfSearchBO.DateToValue <> String.Empty Then

                    tenancyParam = New ParameterBO("dateTo", rfSearchBO.DateToValue, DbType.String)
                    paramList.Add(tenancyParam)

                End If

                If rfSearchBO.Nature <> -1 Then
                    tenancyParam = New ParameterBO("nature", CType(rfSearchBO.Nature, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.FaultStatus <> String.Empty Then
                    tenancyParam = New ParameterBO("FaultStatus", rfSearchBO.FaultStatus, DbType.String)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.TextValue <> String.Empty Then

                    tenancyParam = New ParameterBO("text", rfSearchBO.TextValue, DbType.String)
                    paramList.Add(tenancyParam)
                End If

                sprocName = SprocNameConstants.ReportedFaultReport
            Else
                sprocName = SprocNameConstants.ReportedFaultSearchList

            End If

            MyBase.LoadDataSet(rfDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If rfDS.Tables(0).Rows.Count = 0 Then

                rfSearchBO.IsFlagStatus = False
                rfSearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                rfSearchBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "Reported Fault Report Export for Finance"

        Public Sub GetReportedFaultReportExport(ByRef rfDSExport As DataSet, ByRef rfSearchBO As ReportedFaultSearchBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type ReportedFaultSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim tenancyParam As ParameterBO

            tenancyParam = New ParameterBO("sortColumn", rfSearchBO.SortBy, DbType.String)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortOrder", rfSearchBO.SortOrder, DbType.String)
            paramList.Add(tenancyParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object

            If rfSearchBO.IsSearch Then

                If rfSearchBO.JS <> -1 Then
                    tenancyParam = New ParameterBO("JS", CType(rfSearchBO.JS, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.LastName <> String.Empty Then

                    tenancyParam = New ParameterBO("lastName", rfSearchBO.LastName, DbType.String)
                    paramList.Add(tenancyParam)

                End If

                ' From Date Paramater
                If rfSearchBO.DateValue <> String.Empty Then

                    tenancyParam = New ParameterBO("date", rfSearchBO.DateValue, DbType.String)
                    paramList.Add(tenancyParam)

                End If
                ' To Date Paramater
                If rfSearchBO.DateToValue <> String.Empty Then

                    tenancyParam = New ParameterBO("dateTo", rfSearchBO.DateToValue, DbType.String)
                    paramList.Add(tenancyParam)

                End If

                If rfSearchBO.Nature <> -1 Then
                    tenancyParam = New ParameterBO("nature", CType(rfSearchBO.Nature, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.FaultStatus <> String.Empty Then
                    tenancyParam = New ParameterBO("FaultStatus", rfSearchBO.FaultStatus, DbType.String)
                    paramList.Add(tenancyParam)
                End If

                If rfSearchBO.TextValue <> String.Empty Then

                    tenancyParam = New ParameterBO("text", rfSearchBO.TextValue, DbType.String)
                    paramList.Add(tenancyParam)
                End If

                sprocName = SprocNameConstants.ReportedFaultReportExport
            Else
                sprocName = SprocNameConstants.ReportedFaultReportExport

            End If

            MyBase.LoadDataSet(rfDSExport, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If rfDSExport.Tables(0).Rows.Count = 0 Then

                rfSearchBO.IsFlagStatus = False
                rfSearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                rfSearchBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "GetCustomerReportedFaults"

        Public Sub GetCustomerReportedFaults(ByRef faultLogBO As FaultLogBO, ByRef faultDS As DataSet)
            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerReportedFaults

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            'Creating ParameterBO objects and passing prameters Name, Value and Type
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", faultLogBO.CustomerId, DbType.Int32)


            'Adding object to paramList           
            inParamList.Add(customerId)

            MyBase.LoadDataSet(faultDS, inParamList, sprocName)

            'If no record found means no account/rent statement exist and w'll display user msg
            If faultDS.Tables(0).Rows.Count = 0 Then

                'Set the status flag and user message
                faultLogBO.IsFlagStatus = False

                faultLogBO.UserMsg = UserInfoMsgConstants.RecordNotExist

            Else

                faultLogBO.IsFlagStatus = True

            End If
        End Sub

#End Region

#Region "SearchFaultRepairList"

        Public Sub SearchFaultRepairList(ByRef faultRepListBO As FaultRepairListBO, ByVal faultRepairListDS As DataSet)
            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetFaultRepairList

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type
            Dim sci As ParameterBO = New ParameterBO("SCI", faultRepListBO.StockConditionItem, DbType.Int32)

            'Creating ParameterBO objects and passing prameters Name, Value and Type
            Dim description As ParameterBO = New ParameterBO("Description", faultRepListBO.Description, DbType.String)


            'Adding object to paramList           
            inParamList.Add(sci)
            inParamList.Add(description)

            MyBase.LoadDataSet(faultRepairListDS, inParamList, sprocName)

            'If no record found means no account/rent statement exist and w'll display user msg
            If faultRepairListDS.Tables(0).Rows.Count = 0 Then

                'Set the status flag 
                faultRepListBO.IsFlagStatus = False

                'Set the user message
                faultRepListBO.UserMsg = UserInfoMsgConstants.RecordNotExist

            Else
                'Set the status flag 
                faultRepListBO.IsFlagStatus = True

            End If
        End Sub
#End Region

#Region "Add New Repair"

        Public Sub AddNewRepair(ByRef faultRepListBO As FaultRepairListBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AddNewRepair

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters description
            Dim description As ParameterBO = New ParameterBO("DESCRIPTION", faultRepListBO.Description, DbType.AnsiString)

            '' Creating input ParameterBO objects and passing prameters netCost
            Dim netCost As ParameterBO = New ParameterBO("NETCOST", faultRepListBO.NetCost, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vat
            Dim vat As ParameterBO = New ParameterBO("VAT", faultRepListBO.Vat, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters gross
            Dim gross As ParameterBO = New ParameterBO("GROSS", faultRepListBO.Gross, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vatRateID
            Dim vatRateID As ParameterBO = New ParameterBO("VATRATEID", faultRepListBO.VatRateID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters postInspection
            Dim postInspection As ParameterBO = New ParameterBO("POSTINSPECTION", faultRepListBO.PostInspection, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters elementID
            Dim stockConditionItem As ParameterBO = New ParameterBO("STOCKCONDITIONITEM", faultRepListBO.StockConditionItem, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters RepairActive
            Dim repairActive As ParameterBO = New ParameterBO("REPAIRACTIVE", faultRepListBO.RepairActive, DbType.Int32)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters ParameterID
            Dim parameterID As ParameterBO = New ParameterBO("PARAMETERID", faultRepListBO.ParameterID, DbType.Int32)



            ''Adding objects to input paramList

            inParamList.Add(description)
            inParamList.Add(netCost)
            inParamList.Add(vat)
            inParamList.Add(gross)
            inParamList.Add(vatRateID)
            inParamList.Add(postInspection)
            inParamList.Add(stockConditionItem)
            inParamList.Add(repairActive)
            inParamList.Add(parameterID)


            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                faultRepListBO.IsFlagStatus = False
            Else
                faultRepListBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "Get Repair Values"

        Public Sub GetRepairValues(ByRef faultDS As DataSet, ByRef faultRepListBO As FaultRepairListBO)
            Dim sprocName As String

            sprocName = SprocNameConstants.GetRepairValues
            Dim paramList As ParameterList = New ParameterList()

            Dim faultRepListID As ParameterBO
            faultRepListID = New ParameterBO("FaultRepairListID", faultRepListBO.FaultRepairListID, DbType.Int32)
            paramList.Add(faultRepListID)
            MyBase.LoadDataSet(faultDS, paramList, sprocName)
        End Sub

#End Region

#Region "Amend a Fault"

        Public Sub AmendRepair(ByRef faultRepListBO As FaultRepairListBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AmendRepair

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters faultRepairListID
            Dim faultRepairListID As ParameterBO = New ParameterBO("FAULTREPAIRLISTID", faultRepListBO.FaultRepairListID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters descriotion
            Dim description As ParameterBO = New ParameterBO("DESCRIPTION", faultRepListBO.Description, DbType.String)

            '' Creating input ParameterBO objects and passing prameters netCost
            Dim netCost As ParameterBO = New ParameterBO("NETCOST", faultRepListBO.NetCost, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vat
            Dim vat As ParameterBO = New ParameterBO("VAT", faultRepListBO.Vat, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters gross
            Dim gross As ParameterBO = New ParameterBO("GROSS", faultRepListBO.Gross, DbType.Double)

            '' Creating input ParameterBO objects and passing prameters vatRateID
            Dim vatRateID As ParameterBO = New ParameterBO("VATRATEID", faultRepListBO.VatRateID, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters postInspection
            Dim postInspection As ParameterBO = New ParameterBO("POSTINSPECTION", faultRepListBO.PostInspection, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters elementID
            Dim stockConditionItem As ParameterBO = New ParameterBO("STOCKCONDITIONITEM", faultRepListBO.StockConditionItem, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters faultActive
            Dim repairActive As ParameterBO = New ParameterBO("REPAIRACTIVE", faultRepListBO.RepairActive, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters ParameterID
            Dim parameterID As ParameterBO = New ParameterBO("PARAMETERID", faultRepListBO.ParameterID, DbType.Int32)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(faultRepairListID)
            inParamList.Add(description)
            inParamList.Add(netCost)
            inParamList.Add(vat)
            inParamList.Add(gross)
            inParamList.Add(vatRateID)
            inParamList.Add(postInspection)
            inParamList.Add(stockConditionItem)
            inParamList.Add(repairActive)
            inParamList.Add(parameterID)            


            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                faultRepListBO.IsFlagStatus = False
            Else
                faultRepListBO.IsFlagStatus = True
            End If



        End Sub
#End Region

#Region "Is Allowed TO Cancel OR Assgin Contractor "

        Public Function IsAllowedTOCancelORAssginContractor(ByVal faultLogId As String) As Integer
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.IsAllowedTOCancelORAssginContractor

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters faultRepairListID
            Dim fLogId As ParameterBO = New ParameterBO("FaultLogId", faultLogId, DbType.Int32)

            ''Adding objects to input paramList
            inParamList.Add(fLogId)

            '' Creating output ParameterBO objects and passing prameters result
            Dim isApproved As ParameterBO = New ParameterBO("IsAllowed", Nothing, DbType.Int32)
            outParamList.Add(isApproved)

            Dim myReader = SelectRecord(inParamList, outParamList, sprocName)

            Return outParamList.Item(0).Value
        End Function
#End Region

#Region "Is Fault DisApproved By Contractor "

        Public Function IsFaultDisApprovedByContractor(ByVal faultLogId As String) As Integer
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.IsFaultDisApprovedByContractor

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters faultRepairListID
            Dim fLogId As ParameterBO = New ParameterBO("FaultLogId", faultLogId, DbType.Int32)

            ''Adding objects to input paramList
            inParamList.Add(fLogId)

            '' Creating output ParameterBO objects and passing prameters result
            Dim isApproved As ParameterBO = New ParameterBO("IsDisApproved", Nothing, DbType.Int32)
            outParamList.Add(isApproved)

            Dim myReader = SelectRecord(inParamList, outParamList, sprocName)

            Return outParamList.Item(0).Value
        End Function
#End Region

#Region " Check Reported Faults"
        Public Sub CheckPropertyReportedFaults(ByRef faultDS As DataSet, ByVal objReportedFaults As FaultLogBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.CheckPropertyReportedFaults

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters faultRepairListID
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", objReportedFaults.CustomerId, DbType.Int32)
            Dim propertyId As ParameterBO = New ParameterBO("PropertyId", objReportedFaults.PropertyId, DbType.String)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyId", objReportedFaults.TenancyId, DbType.Int32)

            ''Adding objects to input paramList
            inParamList.Add(customerId)
            inParamList.Add(propertyId)
            inParamList.Add(tenancyId)

            MyBase.LoadDataSet(faultDS, inParamList, sprocName)

        End Sub

#End Region

#Region "Check Gas Servicing Appointments "
        Public Sub CheckGasServicingAppointments(ByRef customerDS As DataSet, ByVal objFaultLog As FaultLogBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.CheckGasServicingAppointments

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters faultRepairListID
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", objFaultLog.CustomerId, DbType.Int32)

            ''Adding objects to input paramList
            inParamList.Add(customerId)

            MyBase.LoadDataSet(customerDS, inParamList, sprocName)

        End Sub
#End Region

#End Region

#Region "Functions and methods from TO"

#Region "Methods"
#Region "GetLocationName"
        Sub GetLocationName(ByRef locationBO As LocationBO)

            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetLocationName

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim locationId As ParameterBO = New ParameterBO("LocationID", locationBO.LocationId, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(locationId)


            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ' If record found 
            If myReader.Read Then

                If Not myReader.IsDBNull(myReader.GetOrdinal("LocationName")) Then
                    locationBO.LocationName = myReader.GetOrdinal("LocationName")
                End If
                locationBO.IsFlagStatus = True

            Else
                ' If No record found
                locationBO.IsFlagStatus = False

            End If

        End Sub
#End Region

#Region "GetCustomerFaults"

        Public Function GetCustomerFaults(ByVal customerID As Integer, ByVal faultStatus As String) As DataSet

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerFaults

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", customerID, DbType.Int32)

            '' Create EnquiryLogBO parameters
            Dim faultStatuseParam As ParameterBO
            faultStatuseParam = New ParameterBO("FaultStatus", faultStatus, DbType.String)

            '' Adding object to paramList
            inParamList.Add(customerIdParam)
            inParamList.Add(faultStatuseParam)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            Dim dsEnq As DataSet = New DataSet
            MyBase.LoadDataSet(dsEnq, inParamList, sprocName)

            Return dsEnq

        End Function

#End Region

#Region "GetUnreadFaultCount"

        Public Sub GetUnreadFaultCount(ByRef faultLogBO As FaultLogBO, ByVal faultStatus As String)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerUnreadFaultsCount

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create EnquiryLogBO parameters
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", faultLogBO.CustomerId, DbType.Int32)

            '' Create EnquiryLogBO parameters
            Dim faultStatuseParam As ParameterBO
            faultStatuseParam = New ParameterBO("FaultStatus", faultStatus, DbType.String)

            '' Adding object to paramList
            inParamList.Add(customerIdParam)
            inParamList.Add(faultStatuseParam)

            ''Out parameter will return the enquiry log count
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("UnreadFaultCount")) Then
                    faultLogBO.UnreadfaultCount = myDataReader.GetInt32(myDataReader.GetOrdinal("UnreadFaultCount"))
                End If
                faultLogBO.IsFlagStatus = True
            Else
                faultLogBO.IsFlagStatus = False
            End If




        End Sub

#End Region

#Region "SaveSQuestions"

        Public Sub SaveSQuestions(ByRef sQuestionBO As SQuestionBO, ByRef sAnswerBO As SQuestionBO, ByVal faultLogID As Integer)

            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.SaveSQuestions

            Dim count As Integer

            For count = 0 To sQuestionBO.Count - 1
                Dim questionId As ParameterBO = New ParameterBO("QUESTIONID", sQuestionBO.Item(count), DbType.Int32)
                Dim answer As ParameterBO = New ParameterBO("ANSWER", sAnswerBO.Item(count), DbType.String)
                Dim fLogID As ParameterBO = New ParameterBO("FAULTLOGID", faultLogID, DbType.Int32)
                Dim tempSQID As ParameterBO = New ParameterBO("SAID", Nothing, DbType.Int32)

                ''
                '' This List will hold instances of type ParameterBO
                Dim inParamList As ParameterList = New ParameterList()
                Dim outParamList As ParameterList = New ParameterList()

                inParamList.Add(questionId)
                inParamList.Add(answer)
                inParamList.Add(fLogID)
                outParamList.Add(tempSQID)

                '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
                MyBase.SaveRecord(inParamList, outParamList, sprocName)

                ''Out parameter will return the TempFaultId in case of success or -1 otherwise
                Dim qryResult As Integer = outParamList.Item(0).Value

                ''In the case of invalid information provided
                If qryResult = -1 Then
                    sQuestionBO.IsExceptionGenerated = True
                    count = sQuestionBO.Count
                Else
                    sQuestionBO.IsExceptionGenerated = False
                End If
            Next
        End Sub

#End Region

#Region "GetElements"
        Public Sub GetElements(ByRef elementBO As ElementBO, ByRef elementList As FaultElementList)

            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetElements

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim areaId As ParameterBO = New ParameterBO("AreaId", elementBO.AreaID, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(areaId)

            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)


            ''Iterate the dataReader
            While (myReader.Read)

                Dim elementId As Integer
                Dim elementName As String = String.Empty

                If Not myReader.IsDBNull(myReader.GetOrdinal("ElementId")) Then
                    elementId = myReader.GetInt32(myReader.GetOrdinal("ElementId"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("ElementName")) Then
                    elementName = myReader.GetString(myReader.GetOrdinal("ElementName"))
                End If

                Dim objElementBO As New ElementBO(elementId, elementName)

                elementList.Add(objElementBO)

            End While

            If elementList.Count <= 0 Then
                ' If No record found
                elementBO.IsFlagStatus = False

            End If

        End Sub
#End Region

#Region "GetFaults"
        Public Sub GetFaults(ByRef faultBO As FaultBO, ByRef faultList As FaultList)


            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetFaults

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim elementId As ParameterBO = New ParameterBO("ElementId", faultBO.ElementID, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(elementId)

            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            Dim counter As Integer = New Integer()

            counter = 0
            ''Iterate the dataReader
            While (myReader.Read)

                Dim faultId As Integer
                Dim description As String = String.Empty

                If Not myReader.IsDBNull(myReader.GetOrdinal("FaultId")) Then
                    faultId = myReader.GetInt32(myReader.GetOrdinal("FaultId"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("description")) Then
                    description = myReader.GetString(myReader.GetOrdinal("description"))
                End If

                Dim objFaultBO As New FaultBO(faultId, description)

                faultList.Add(objFaultBO)

                counter = counter + 1
            End While

            If Not counter Then
                faultBO.IsFlagStatus = False
            End If

        End Sub
#End Region

#Region "SaveFaultTemporarily"
        Public Sub SaveFaultTemporarily(ByRef tempFaultBO As TempFaultBO)
            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.SaveFaultTemporarily

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TempFaultBO parameters
            Dim faultId As ParameterBO = New ParameterBO("FaultId", tempFaultBO.FaultId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", tempFaultBO.CustomerId, DbType.String)
            Dim quantity As ParameterBO = New ParameterBO("Quantity", tempFaultBO.Quantity, DbType.Int32)
            Dim problemDays As ParameterBO = New ParameterBO("ProblemDays", tempFaultBO.ProblemDays, DbType.Int32)
            Dim recuringProblem As ParameterBO = New ParameterBO("RecuringProblem", tempFaultBO.RecuringProblem, DbType.Int32)
            Dim communalProblem As ParameterBO = New ParameterBO("CommunalProblem", tempFaultBO.CommunalProblem, DbType.Int32)
            Dim recharge As ParameterBO = New ParameterBO("Recharge", tempFaultBO.Recharge, DbType.Int32)
            Dim notes As ParameterBO = New ParameterBO("Notes", tempFaultBO.Notes, DbType.String)

            Dim tempFaultId As ParameterBO = New ParameterBO("TempFaultId", Nothing, DbType.Int32)


            '' Adding object to paramList
            inParamList.Add(faultId)
            inParamList.Add(customerId)
            inParamList.Add(quantity)
            inParamList.Add(problemDays)
            inParamList.Add(recuringProblem)
            inParamList.Add(communalProblem)
            inParamList.Add(recharge)
            inParamList.Add(notes)
            outParamList.Add(tempFaultId)

            '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                tempFaultBO.IsFlagStatus = False

                tempFaultBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else

                tempFaultBO.IsFlagStatus = True

            End If
        End Sub

#End Region

#Region "GetFaultQuestion"
        Public Sub GetFaultQuestion(ByRef faultQBO As FaultQuestionBO)

            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetFaultQuestion

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim questionId As ParameterBO = New ParameterBO("QuestionId", faultQBO.QuestionId, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(questionId)

            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ' If record found 
            If myReader.Read Then


                If Not myReader.IsDBNull(myReader.GetOrdinal("FQuestion")) Then
                    faultQBO.Question = myReader.GetString(myReader.GetOrdinal("FQuestion"))
                End If

                faultQBO.IsFlagStatus = True

            Else
                ' If No record found
                faultQBO.IsFlagStatus = False


            End If
        End Sub
#End Region

#Region "GetRechargeCost"
        Public Sub GetRechargeCost(ByRef faultRechargeBO As FaultRechargeBO)

            ' NOTE:-
            ' We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.GetFaultRechargeCost
            Dim myDS As New DataSet()

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            ' Creating ParameterBO objects and passing prameters Name, Value and Type            
            Dim faultId As ParameterBO = New ParameterBO("FaultId", faultRechargeBO.FaultId, DbType.Int32)

            ' Adding object to paramList
            inParamList.Add(faultId)

            MyBase.LoadDataSet(myDS, inParamList, sprocName)
            If (myDS.Tables(0).Rows.Count > 0 And Not (myDS Is Nothing)) Then
                faultRechargeBO.RechargeCost = CType(myDS.Tables(0).Rows(0)("NetCost"), String)
                
            End If

        End Sub
#End Region


#Region "GetTempFaultBasket"

        Public Sub GetTempFaultBasket(ByRef tempFaultBO As TempFaultBO, ByRef tempFaultDS As DataSet)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.TempFaultBasket

            ' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TerminationBO parameters
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", tempFaultBO.CustomerId, DbType.Int32)


            '' Adding object to paramList           
            inParamList.Add(customerId)

            MyBase.LoadDataSet(tempFaultDS, inParamList, sprocName)

            '' If no record found means no account/rent statement exist and w'll display user msg
            If tempFaultDS.Tables(0).Rows.Count = 0 Then

                tempFaultBO.IsFlagStatus = False

                tempFaultBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                tempFaultBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "DeleteTempFault"
        Public Sub DeleteTempFault(ByRef tempFaultBO As TempFaultBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there
            Dim sprocName As String = SprocNameConstants.DeleteTempFault

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type
            'Create TerminationBO parameters
            Dim tempFaultId As ParameterBO = New ParameterBO("TempFaultID", tempFaultBO.TempFaultId, DbType.Int32)
            Dim customerId As ParameterBO = New ParameterBO("CustomerID", tempFaultBO.CustomerId, DbType.Int32)
            Dim countRecords As ParameterBO = New ParameterBO("CountRecords", Nothing, DbType.Int32)


            '' Adding object to paramList           
            inParamList.Add(tempFaultId)
            inParamList.Add(customerId)
            outParamList.Add(countRecords)


            'Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                tempFaultBO.IsFlagStatus = False
            Else
                tempFaultBO.CountRecords = qryResult

                tempFaultBO.IsFlagStatus = True

            End If


        End Sub
#End Region

#Region "UpdateFaultQuantity"
        Public Sub UpdateFaultQuantity(ByRef tempFaultBO As TempFaultBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.UpdateFaultQuantity

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TempFaultBO parameters           
            Dim tempFaultId As ParameterBO = New ParameterBO("TempFaultId", tempFaultBO.TempFaultId, DbType.Int32)
            Dim quantity As ParameterBO = New ParameterBO("Quantity", tempFaultBO.Quantity, DbType.Int32)
            Dim tmpFault As ParameterBO = New ParameterBO("TmpFault", tempFaultBO.TempFaultId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(tempFaultId)
            inParamList.Add(quantity)
            outParamList.Add(tmpFault)

            '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                tempFaultBO.IsFlagStatus = False

                tempFaultBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else

                tempFaultBO.IsFlagStatus = True

            End If
        End Sub
#End Region

#Region "UpdateFaultContractor"
        Public Sub UpdateFaultContractor(ByRef tempFaultBO As TempFaultBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.UpdateFaultContractor

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TempFaultBO parameters           
            Dim tempFaultId As ParameterBO = New ParameterBO("TempFaultId", tempFaultBO.TempFaultId, DbType.Int32)
            Dim itemActionId As ParameterBO
            Dim itemStatusId As ParameterBO
            Dim contractor As ParameterBO
            If tempFaultBO.Contractor = 0 Then
                contractor = New ParameterBO("ORGID", Nothing, DbType.Int32)
                itemActionId = New ParameterBO("ITEMACTIONID", 1, DbType.Int32)
                itemStatusId = New ParameterBO("ITEMSTATUSID", 1, DbType.Int32)

            Else
                contractor = New ParameterBO("ORGID", tempFaultBO.Contractor, DbType.Int32)
                itemActionId = New ParameterBO("ITEMACTIONID", 2, DbType.Int32)
                itemStatusId = New ParameterBO("ITEMSTATUSID", 2, DbType.Int32)
            End If

            Dim tmpFault As ParameterBO = New ParameterBO("TmpFault", tempFaultBO.TempFaultId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(tempFaultId)
            inParamList.Add(contractor)
            inParamList.Add(itemActionId)
            inParamList.Add(itemStatusId)
            outParamList.Add(tmpFault)


            '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                tempFaultBO.IsFlagStatus = False

                tempFaultBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else

                tempFaultBO.IsFlagStatus = True

            End If
        End Sub
#End Region

#Region "saveFinalFaultBasket"
        Public Function saveFinalFaultBasket(ByRef finalFaultBO As FinalFaultBasketBO) As Integer

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.SaveFinalFaultBasketWithJournal

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TempFaultBO parameters           
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", finalFaultBO.CustomerId, DbType.Int32)
            Dim prefContactTime As ParameterBO = New ParameterBO("PreferredContactDetails", finalFaultBO.PrefContactTime, DbType.String)
            Dim submitDate As ParameterBO = New ParameterBO("SubmitDate", finalFaultBO.SubmitDate, DbType.DateTime)
            Dim iamHappy As ParameterBO = New ParameterBO("IamHappy", finalFaultBO.IamHappy, DbType.Int32)
            Dim propertyId As ParameterBO = New ParameterBO("PropertyId", finalFaultBO.PropertyId, DbType.AnsiString)
            Dim tenancyId As ParameterBO = New ParameterBO("TenancyId", finalFaultBO.TenancyId, DbType.Int32)
            Dim userId As ParameterBO = New ParameterBO("UserID", finalFaultBO.UserId, DbType.Int32)

            Dim faultBasketId As ParameterBO = New ParameterBO("FaultBasketId", Nothing, DbType.Int32)


            '' Adding object to paramList
            inParamList.Add(customerId)
            inParamList.Add(prefContactTime)
            inParamList.Add(submitDate)
            inParamList.Add(iamHappy)
            inParamList.Add(propertyId)
            inParamList.Add(tenancyId)
            inParamList.Add(userId)
            outParamList.Add(faultBasketId)

            '' Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TempFaultId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then

                finalFaultBO.IsFlagStatus = False

                finalFaultBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
            Else

                finalFaultBO.IsFlagStatus = True

            End If
            Return qryResult
        End Function

#End Region

#Region "LoadBreadCrumb"
        Public Sub LoadBreadCrumb(ByRef breadBO As BreadCrumbBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.LoadBreadCrumb

            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Create TerminationBO parameters
            Dim curPageId As ParameterBO = New ParameterBO("CurPageId", breadBO.CurPageId, DbType.Int32)
            Dim curPageIdType As ParameterBO = New ParameterBO("CurPageIdType", breadBO.CurPageIdType, DbType.String)


            'Adding object to paramList           
            inParamList.Add(curPageId)
            inParamList.Add(curPageIdType)


            ' Passing Array of LocationBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ' If record found 
            If myReader.Read Then


                If Not myReader.IsDBNull(myReader.GetOrdinal("LocationID")) Then
                    breadBO.LocationId = myReader.GetInt32(myReader.GetOrdinal("LocationID"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("LocationName")) Then
                    breadBO.LocationName = myReader.GetString(myReader.GetOrdinal("LocationName"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("AreaID")) Then
                    breadBO.AreaId = myReader.GetInt32(myReader.GetOrdinal("AreaID"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("AreaName")) Then
                    breadBO.AreaName = myReader.GetString(myReader.GetOrdinal("AreaName"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("ElementID")) Then
                    breadBO.ElementId = myReader.GetInt32(myReader.GetOrdinal("ElementID"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("ElementName")) Then
                    breadBO.ElementName = myReader.GetString(myReader.GetOrdinal("ElementName"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("FaultID")) Then
                    breadBO.FaultId = myReader.GetInt32(myReader.GetOrdinal("FaultID"))
                End If

                If Not myReader.IsDBNull(myReader.GetOrdinal("Description")) Then
                    breadBO.FaultName = myReader.GetString(myReader.GetOrdinal("Description"))
                End If

                breadBO.IsFlagStatus = True

            Else
                ' If No record found
                breadBO.IsFlagStatus = False


            End If

        End Sub

#End Region
#End Region
#End Region




    End Class
End Namespace
