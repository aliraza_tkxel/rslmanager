'' Class Name -- CustomerDAL
'' Base Class -- BaseDAL
'' Summary --    Contains Functionality related to Customer/Tenant
'' Methods --    AuthenticateCustomer
'' Created By  -- Muanwar Nadeem

''---------------------------------
Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation
Imports Microsoft.Practices.EnterpriseLibrary.Logging.Filters

Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess

    Public Class EnquiryDAL : Inherits BaseDAL

#Region "Functions"
        ''No function defined yet...
#End Region

#Region "Methods"

#Region "GetTerminationInfo"
        Public Sub GetTerminationInfo(ByRef custAddressBO As CustomerAddressBO, ByRef terminBO As TerminationBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressTermination

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", terminBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillTerminationBO(myDataReader, terminBO)

                '' TO DO: GetCustomerAddress method is already exists. Using Polymorphism, need to move
                '' this code on
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
                    custAddressBO.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
                    custAddressBO.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
                    custAddressBO.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
                    custAddressBO.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
                    custAddressBO.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PostCode")) Then
                    custAddressBO.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("PostCode"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("COUNTY")) Then
                    custAddressBO.Coutnty = myDataReader.GetString(myDataReader.GetOrdinal("COUNTY"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEL")) Then
                    custAddressBO.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("TEL"))
                End If

                terminBO.IsFlagStatus = True
            Else
                terminBO.IsFlagStatus = False
            End If

        End Sub
        Private Sub FillTerminationBO(ByRef myDataReader As IDataReader, ByRef terminBO As TerminationBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                terminBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MovingOutDate")) Then
                terminBO.MovingOutDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("MovingOutDate"))
            End If

        End Sub
#End Region

#Region "Load Enquiry Log DataSet"
        Public Sub EnquiryLogData(ByRef enquiryDS As DataSet, ByRef enqryLogSearchBO As EnquiryLogSearchBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim tenancyParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            tenancyParam = New ParameterBO("noOfRows", enqryLogSearchBO.RowCount, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("offSet", enqryLogSearchBO.PageIndex, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortColumn", enqryLogSearchBO.SortBy, DbType.String)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortOrder", enqryLogSearchBO.SortOrder, DbType.String)
            paramList.Add(tenancyParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object
            If enqryLogSearchBO.IsSearch Then
                If enqryLogSearchBO.TenancyID <> "" Then
                    tenancyParam = New ParameterBO("tenancyID", CType(enqryLogSearchBO.TenancyID, Int32), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                tenancyParam = New ParameterBO("firstName", enqryLogSearchBO.FirstName, DbType.String)
                paramList.Add(tenancyParam)
                tenancyParam = New ParameterBO("lastName", enqryLogSearchBO.LastName, DbType.String)
                paramList.Add(tenancyParam)

                If enqryLogSearchBO.DateValue <> "12:00:00 AM" Then
                    tenancyParam = New ParameterBO("date", enqryLogSearchBO.DateValue, DbType.Date)
                    paramList.Add(tenancyParam)
                End If

                If enqryLogSearchBO.Nature <> "" Then
                    tenancyParam = New ParameterBO("nature", CType(enqryLogSearchBO.Nature, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If enqryLogSearchBO.Status <> "" Then
                    tenancyParam = New ParameterBO("status", CType(enqryLogSearchBO.Status, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                tenancyParam = New ParameterBO("text", enqryLogSearchBO.TextValue, DbType.String)
                paramList.Add(tenancyParam)
                sprocName = SprocNameConstants.EnquiryLogSearch
            Else
                sprocName = SprocNameConstants.EnquiryLogSelectAllList
            End If

            MyBase.LoadDataSet(enquiryDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If enquiryDS.Tables(0).Rows.Count = 0 Then

                enqryLogSearchBO.IsFlagStatus = False
                enqryLogSearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                enqryLogSearchBO.IsFlagStatus = True
            End If
        End Sub

        Public Function GetEnquiryLogRowCount() As Integer

            Dim sprocName As String = SprocNameConstants.GetRowCount

            Dim paramList As ParameterList = New ParameterList()

            '' used to return no. of results found
            Dim rowCount As Integer = 0

            Dim tenancyParam As ParameterBO

            tenancyParam = New ParameterBO("tableName", "TO_ENQUIRY_LOG", DbType.String)
            paramList.Add(tenancyParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)
                ''  custHeadBO.isFlagStatus = True

            End If

            Return rowCount

        End Function

        Public Function GetEnquiryLogSearchRowCount(ByRef enqryLogSearchBO As EnquiryLogSearchBO, ByVal countType As String) As Integer
            Dim sprocName As String
            If (countType.Equals("Response")) Then
                sprocName = SprocNameConstants.GetResponseSearchRowCount
            Else
                sprocName = SprocNameConstants.GetSearchRowCount
            End If

            Dim paramList As ParameterList = New ParameterList()

            '' used to return no. of results found
            Dim rowCount As Integer = 0

            Dim tenancyParam As ParameterBO

            If enqryLogSearchBO.TenancyID <> "" Then
                tenancyParam = New ParameterBO("tenancyID", CType(enqryLogSearchBO.TenancyID, Int32), DbType.Int32)
                paramList.Add(tenancyParam)
            End If

            tenancyParam = New ParameterBO("firstName", enqryLogSearchBO.FirstName, DbType.String)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("lastName", enqryLogSearchBO.LastName, DbType.String)
            paramList.Add(tenancyParam)

            If enqryLogSearchBO.DateValue <> "12:00:00 AM" Then
                tenancyParam = New ParameterBO("date", enqryLogSearchBO.DateValue, DbType.Date)
                paramList.Add(tenancyParam)
            End If

            If enqryLogSearchBO.Nature <> "" Then
                tenancyParam = New ParameterBO("nature", CType(enqryLogSearchBO.Nature, Integer), DbType.Int32)
                paramList.Add(tenancyParam)
            End If

            If enqryLogSearchBO.Status <> "" Then
                tenancyParam = New ParameterBO("status", CType(enqryLogSearchBO.Status, Integer), DbType.Int32)
                paramList.Add(tenancyParam)
            End If

            tenancyParam = New ParameterBO("text", enqryLogSearchBO.TextValue, DbType.String)
            paramList.Add(tenancyParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If

            Return rowCount

        End Function

#End Region

#Region "Load Customer response DataSet"
        Public Sub CustomerResponseData(ByRef enquiryDS As DataSet, ByRef enqryLogSearchBO As EnquiryLogSearchBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim tenancyParam As ParameterBO

            '' rowindex, pgaeindex and sortby will be common to both type of searches
            tenancyParam = New ParameterBO("noOfRows", enqryLogSearchBO.RowCount, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("offSet", enqryLogSearchBO.PageIndex, DbType.Int32)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortColumn", enqryLogSearchBO.SortBy, DbType.String)
            paramList.Add(tenancyParam)
            tenancyParam = New ParameterBO("sortOrder", enqryLogSearchBO.SortOrder, DbType.String)
            paramList.Add(tenancyParam)

            '' if search is based on username tenancyid date nature etc thn we will get
            '' these values from  EnquiryLogSearchBO object
            If enqryLogSearchBO.IsSearch Then
                If enqryLogSearchBO.TenancyID <> "" Then
                    tenancyParam = New ParameterBO("tenancyID", CType(enqryLogSearchBO.TenancyID, Int32), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                tenancyParam = New ParameterBO("firstName", enqryLogSearchBO.FirstName, DbType.String)
                paramList.Add(tenancyParam)
                tenancyParam = New ParameterBO("lastName", enqryLogSearchBO.LastName, DbType.String)
                paramList.Add(tenancyParam)

                If enqryLogSearchBO.DateValue <> "12:00:00 AM" Then
                    tenancyParam = New ParameterBO("date", enqryLogSearchBO.DateValue, DbType.Date)
                    paramList.Add(tenancyParam)
                End If

                If enqryLogSearchBO.Nature <> "" Then
                    tenancyParam = New ParameterBO("nature", CType(enqryLogSearchBO.Nature, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                If enqryLogSearchBO.Status <> "" Then
                    tenancyParam = New ParameterBO("status", CType(enqryLogSearchBO.Status, Integer), DbType.Int32)
                    paramList.Add(tenancyParam)
                End If

                tenancyParam = New ParameterBO("text", enqryLogSearchBO.TextValue, DbType.String)
                paramList.Add(tenancyParam)

                sprocName = SprocNameConstants.CustomerResponseSearch
            Else
                sprocName = SprocNameConstants.CustomerResponseSelectAllList
            End If


            MyBase.LoadDataSet(enquiryDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and w'll display user msg
            If enquiryDS.Tables(0).Rows.Count = 0 Then

                enqryLogSearchBO.IsFlagStatus = False
                enqryLogSearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                enqryLogSearchBO.IsFlagStatus = True
            End If
        End Sub

#End Region

#Region "GetRowCount Function"

        '' Private method used to return no. of results
        Private Function GetRowCount(ByRef myDataRecord As IDataRecord) As Integer

            Return myDataRecord.GetInt32(myDataRecord.GetOrdinal("numOfRows"))

        End Function

#End Region

#Region "Delete Customer Record"
        Public Sub DeleteCustomerEnquiry(ByRef termBO As TerminationBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.DeleteCustomerEnquiry

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters enquiryLogID
            Dim enqryLogID As ParameterBO = New ParameterBO("enquiryLogID", termBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enqryLogID)

            Dim outParamList As ParameterList = New ParameterList()

            Dim rowCount As Integer = 0

            '' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            rowCount = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            termBO.IsFlagStatus = True
        End Sub
#End Region

#Region "Delete Customer Response Record"
        Public Sub DeleteCustomerEnquiryResponse(ByRef cstResponseBO As CustomerResponseBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.DeleteCustomerEnquiryResponse

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters custResponseId
            Dim custResponseId As ParameterBO = New ParameterBO("custResponseId", cstResponseBO.CustomerResponseId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(custResponseId)

            Dim outParamList As ParameterList = New ParameterList()

            Dim rowCount As Integer = 0

            '' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            rowCount = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            cstResponseBO.IsFlagStatus = True
        End Sub
#End Region

#Region "GetCustomerIDByEnquiryLog"
        Public Sub GetCustomerIDByEnquiryLog(ByRef enqryLog As EnquiryLogBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.GetCustomerIDByEnquiryLog

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters enquiryLogID
            Dim enqryLogID As ParameterBO = New ParameterBO("enquiryLogID", enqryLog.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enqryLogID)

            Dim outParamList As ParameterList = New ParameterList()

            Dim rowCount As Integer = 0

            '' Passing List of IN-Parameters, OutParameters & sproc-name to base-class method: SaveRecord
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerID")) Then
                    enqryLog.CustomerId = myDataReader.GetInt32(myDataReader.GetOrdinal("CustomerID"))
                End If

                enqryLog.IsFlagStatus = True
            Else
                enqryLog.IsFlagStatus = False
            End If
        End Sub
#End Region

#Region "MessageAlertSystem"

        '#Region "GetCustomerEnquiryCount"

        '        Public Sub GetCustomerEnquiryCount(ByRef enqBO As EnquiryLogBO)

        '            '' NOTE:-
        '            '' We'll send back exception to BLL and will set flags + log exceptions there

        '            Dim sprocName As String = SprocNameConstants.GetCustomerEnquiryCount

        '            ''
        '            '' This List will hold instances of type ParameterBO
        '            Dim inParamList As ParameterList = New ParameterList()


        '            '' Creating ParameterBO objects and passing prameters Name, Value and Type

        '            '' Create EnquiryLogBO parameters
        '            Dim customerID As ParameterBO = New ParameterBO("CustomerId", enqBO.CustomerId, DbType.Int32)

        '            '' Adding object to paramList
        '            inParamList.Add(customerID)


        '            ''Out parameter will return the enquiry log count
        '            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

        '            If myDataReader.Read Then

        '                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerEnquiries")) Then
        '                    enqBO.CustomerEnquiryCount = myDataReader.GetInt32(myDataReader.GetOrdinal("CustomerEnquiries"))
        '                End If
        '                enqBO.IsFlagStatus = True
        '            Else
        '                enqBO.IsFlagStatus = False
        '            End If


        '        End Sub

        '#End Region

        '#Region "GetEnquiryResponseCount"

        '        Public Sub GetEnquiryResponseCount(ByRef enqBO As EnquiryLogBO)

        '            '' NOTE:-
        '            '' We'll send back exception to BLL and will set flags + log exceptions there

        '            Dim sprocName As String = SprocNameConstants.GetEnquiryResponseCount

        '            ''
        '            '' This List will hold instances of type ParameterBO
        '            Dim inParamList As ParameterList = New ParameterList()


        '            '' Creating ParameterBO objects and passing prameters Name, Value and Type

        '            '' Create EnquiryLogBO parameters
        '            Dim customerID As ParameterBO = New ParameterBO("CustomerId", enqBO.CustomerId, DbType.Int32)

        '            '' Adding object to paramList
        '            inParamList.Add(customerID)


        '            ''Out parameter will return the enquiry log count
        '            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

        '            If myDataReader.Read Then

        '                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("ResponseCount")) Then
        '                    enqBO.EnquiryResponseCount = myDataReader.GetInt32(myDataReader.GetOrdinal("ResponseCount"))
        '                End If
        '                enqBO.IsFlagStatus = True
        '            Else
        '                enqBO.IsFlagStatus = False
        '            End If


        '        End Sub

        '#End Region

        '#Region "GetCustomerEnquiries"

        '        Public Function GetCustomerEnquiries(ByVal customerID As Integer, ByVal responseStatus As String) As DataSet

        '            '' NOTE:-
        '            '' We'll send back exception to BLL and will set flags + log exceptions there

        '            Dim sprocName As String = SprocNameConstants.GetCustomerEnquiries

        '            ''
        '            '' This List will hold instances of type ParameterBO
        '            Dim inParamList As ParameterList = New ParameterList()

        '            '' Creating ParameterBO objects and passing prameters Name, Value and Type

        '            '' Create EnquiryLogBO parameters
        '            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", customerID, DbType.Int32)

        '            '' Create EnquiryLogBO parameters
        '            Dim responseStatuseParam As ParameterBO
        '            If responseStatus.Equals("All") Then
        '                responseStatuseParam = New ParameterBO("ResponseStatus", Nothing, DbType.String)
        '            Else
        '                responseStatuseParam = New ParameterBO("ResponseStatus", responseStatus, DbType.String)
        '            End If


        '            '' Adding object to paramList
        '            inParamList.Add(customerIdParam)
        '            inParamList.Add(responseStatuseParam)

        '            ''Out parameter will return the enquiry log count
        '            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

        '            Dim dsEnq As DataSet = New DataSet
        '            MyBase.LoadDataSet(dsEnq, inParamList, sprocName)

        '            Return dsEnq

        '        End Function

        '#End Region

        '#Region "GetEnquiryResponses"

        '        Public Function GetEnquiryResponses(ByRef enqLogBO As EnquiryLogBO) As JournalList

        '            '' NOTE:-
        '            '' We'll send back exception to BLL and will set flags + log exceptions there

        '            Dim sprocName As String = SprocNameConstants.GetEnquiryResponse

        '            ''
        '            '' This List will hold instances of type ParameterBO
        '            Dim inParamList As ParameterList = New ParameterList()

        '            '' Creating ParameterBO objects and passing prameters Name, Value and Type

        '            '' Create EnquiryLogBO parameters
        '            Dim enqIdParam As ParameterBO = New ParameterBO("EnquiryLogID", enqLogBO.EnquiryLogId, DbType.Int32)


        '            '' Adding object to paramList
        '            inParamList.Add(enqIdParam)

        '            ''Out parameter will return the enquiry log count
        '            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)
        '            Dim lstJournal As New JournalList

        '            While myDataReader.Read
        '                Dim jBo As New JournalBO

        '                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LASTACTIONDATE")) Then
        '                    jBo.LastActionDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("LASTACTIONDATE"))
        '                End If

        '                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NOTES")) Then
        '                    jBo.Notes = myDataReader.GetString(myDataReader.GetOrdinal("NOTES"))
        '                End If

        '                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HistoryId")) Then
        '                    jBo.HistoryID = myDataReader.GetInt32(myDataReader.GetOrdinal("HistoryId"))
        '                End If

        '                lstJournal.Add(jBo)

        '            End While

        '            Return lstJournal

        '        End Function

        '#End Region

        '#Region "GetLetter"

        '        Public Sub GetLetter(ByVal storedProcedure As String, ByRef objLetterBO As LetterBO)

        '            '' NOTE:-
        '            '' We'll send back exception to BLL and will set flags + log exceptions there

        '            Dim sprocName As String = storedProcedure

        '            ''
        '            '' This List will hold instances of type ParameterBO
        '            Dim inParamList As ParameterList = New ParameterList()

        '            '' Creating ParameterBO objects and passing prameters Name, Value and Type

        '            '' Create LetterBO parameters
        '            Dim enqIdParam As ParameterBO = New ParameterBO("HistoryID", objLetterBO.HistoryID, DbType.Int32)


        '            '' Adding object to paramList
        '            inParamList.Add(enqIdParam)

        '            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

        '            If myDataReader.Read Then
        '                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LetterContents")) Then
        '                    objLetterBO.LetterContents = myDataReader.GetString(myDataReader.GetOrdinal("LetterContents"))
        '                End If
        '            End If


        '        End Sub

        '#End Region

#End Region

#Region "Create Customer Response Jornal"

        Public Sub CreateCustomerResponseJornal(ByRef custResBO As CustomerResponseBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.CreatCustomerResponseJournal

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters enquiryLogId
            Dim enquiryLogId As ParameterBO = New ParameterBO("enquiryLogId", custResBO.EnquiryId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters journalId
            Dim journalId As ParameterBO = New ParameterBO("journalId", custResBO.JournalId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters CustomerResponseId
            Dim responseId As ParameterBO = New ParameterBO("responseId", custResBO.CustomerResponseId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters ItemNatureId
            Dim nature As ParameterBO = New ParameterBO("nature", custResBO.ItemNatureId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters Notes
            Dim responseNotes As ParameterBO = New ParameterBO("responseNotes", custResBO.Notes, DbType.String)

            '' Creating output ParameterBO objects and passing prameters resultId
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)

            '' Adding object to input paramList
            inParamList.Add(enquiryLogId)
            inParamList.Add(journalId)
            inParamList.Add(responseId)
            inParamList.Add(nature)
            inParamList.Add(responseNotes)

            ''Adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of customerResponseBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                custResBO.IsFlagStatus = False
            Else
                custResBO.IsFlagStatus = True
            End If
        End Sub

#End Region

#End Region

    End Class

End Namespace

