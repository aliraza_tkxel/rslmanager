'' Class Name -- ContractorPortalDAL
'' Base Class -- BaseDAL
'' Summary --    Contains Functionality related to ContractorPortal/RSL
'' Created By  -- Waseem Hassan

''---------------------------------
Imports System

Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess
    Public Class ContractorPortalDAL : Inherits BaseDAL



#Region "Job Sheet Methods - Coded By Noor Muhammad - January,2009 "

#Region "Get Job Sheet Data"

        Public Sub GetJobSheetData(ByVal jobSheetDS As DataSet, ByVal conBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String

            '' This List will hold instances of type ContractorPortalBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim faultParam As ParameterBO

            '' PageSize, pageindex and sortby will be common to both type of searches
            faultParam = New ParameterBO("noOfRows", conBO.PageSize, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("offSet", conBO.PageIndex, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortColumn", conBO.SortBy, DbType.String)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortOrder", conBO.SortOrder, DbType.String)
            paramList.Add(faultParam)

            'Pass the organization/contracator id to SP
            faultParam = New ParameterBO("orgId", conBO.OrgId, DbType.Int32)
            paramList.Add(faultParam)

            'If user selected the Location then pass it to SP otherwise pass nothing
            If conBO.LocationId > 0 Then
                faultParam = New ParameterBO("locationId", conBO.LocationId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Area then pass it to SP otherwise pass nothing
            If conBO.AreaId > 0 Then
                faultParam = New ParameterBO("areaId", conBO.AreaId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Element then pass it to SP otherwise pass nothing
            If conBO.ElementId > 0 Then
                faultParam = New ParameterBO("elementId", conBO.ElementId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the team then pass it to SP otherwise pass nothing
            If conBO.TeamId > 0 Then
                faultParam = New ParameterBO("teamId", conBO.TeamId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the team then pass it to SP otherwise pass nothing
            If conBO.UserId > 0 Then
                faultParam = New ParameterBO("userId", conBO.UserId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Priority then pass it to SP otherwise pass nothing
            If conBO.PriorityId > 0 Then
                faultParam = New ParameterBO("priorityId", conBO.PriorityId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Status then pass it to SP otherwise pass nothing
            If conBO.StatusId > 0 Then
                faultParam = New ParameterBO("status", conBO.StatusId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Patch then pass it to SP otherwise pass nothing
            If conBO.PatchId > 0 Then
                faultParam = New ParameterBO("patch", conBO.PatchId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Scheme then pass it to SP otherwise pass nothing
            If conBO.SchemeId > 0 Then
                faultParam = New ParameterBO("scheme", conBO.SchemeId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user mentioned the Postcode then pass it to SP otherwise pass nothing
            If Not conBO.PostCode = String.Empty Then
                faultParam = New ParameterBO("postcode", conBO.PostCode, DbType.String)
                paramList.Add(faultParam)
            End If

            'If user selected the Due Date then pass it to SP otherwise pass nothing
            If conBO.Due <> Nothing Then
                faultParam = New ParameterBO("due", conBO.Due, DbType.String)
                paramList.Add(faultParam)
            End If

            If conBO.JsNumber <> Nothing Then
                faultParam = New ParameterBO("jsnumber", conBO.JsNumber, DbType.Int32)
                paramList.Add(faultParam)
            End If

            sprocName = SprocNameConstants.GetJobSheetData

            MyBase.LoadDataSet(jobSheetDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If jobSheetDS.Tables(0).Rows.Count = 0 Then

                conBO.IsFlagStatus = False
                conBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                conBO.IsFlagStatus = True
            End If
        End Sub
#End Region

#Region "Update Selected Job Sheet Faults"

        Public Sub UpdateSelectedJobSheetFaults(ByRef selectedFaults As ArrayList, ByRef conBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            Dim itemCounter As Integer = 0
            Dim loopCounter As Integer
            Dim faultParam As ParameterBO = New ParameterBO()

            'Set the stored procedure name
            sprocName = SprocNameConstants.UpdateSelectedJobSheetFaults

            For loopCounter = 1 To selectedFaults.Count()

                Dim inParamList As ParameterList = New ParameterList()
                Dim outParamList As ParameterList = New ParameterList()

                faultParam = New ParameterBO("OrgId", conBO.OrgId, DbType.Int32)
                inParamList.Add(faultParam)
                faultParam = New ParameterBO("FaultLogId", selectedFaults.Item(itemCounter), DbType.Int32)
                inParamList.Add(faultParam)
                faultParam = New ParameterBO("IsSelected", 1, DbType.Int32)
                inParamList.Add(faultParam)

                faultParam = New ParameterBO("IsUpdated", Nothing, DbType.Int32)
                outParamList.Add(faultParam)

                MyBase.SaveRecord(inParamList, outParamList, sprocName)

                ''Out parameter will return the FaultLogId in case of success or -1 otherwise
                Dim qryResult As Integer = outParamList.Item(0).Value

                ''In the case of invalid information provided
                If qryResult = -1 Then
                    conBO.IsFlagStatus = False
                    conBO.UserMsg = UserInfoMsgConstants.DataSaveFailed
                Else
                    conBO.IsFlagStatus = True
                End If

                itemCounter = itemCounter + 1

            Next
        End Sub
#End Region

#Region "Get Job Sheet Fault Count"
        Public Function GetJobSheetFaultCount(ByRef conBO As ContractorPortalBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            Dim rowCount As Integer = 0

            '' This List will hold instances of type ContractorPortalBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim faultParam As ParameterBO

            'Pass the organization/contracator id to SP
            faultParam = New ParameterBO("orgId", conBO.OrgId, DbType.Int32)
            paramList.Add(faultParam)

            'If user selected the Location then pass it to SP otherwise pass nothing
            If conBO.LocationId > 0 Then
                faultParam = New ParameterBO("locationId", conBO.LocationId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Area then pass it to SP otherwise pass nothing
            If conBO.AreaId > 0 Then
                faultParam = New ParameterBO("areaId", conBO.AreaId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Element then pass it to SP otherwise pass nothing
            If conBO.ElementId > 0 Then
                faultParam = New ParameterBO("elementId", conBO.ElementId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the team then pass it to SP otherwise pass nothing
            If conBO.TeamId > 0 Then
                faultParam = New ParameterBO("teamId", conBO.TeamId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the team then pass it to SP otherwise pass nothing
            If conBO.UserId > 0 Then
                faultParam = New ParameterBO("userId", conBO.UserId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Priority then pass it to SP otherwise pass nothing
            If conBO.PriorityId > 0 Then
                faultParam = New ParameterBO("priorityId", conBO.PriorityId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Status then pass it to SP otherwise pass nothing
            If conBO.StatusId > 0 Then
                faultParam = New ParameterBO("status", conBO.StatusId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Patch then pass it to SP otherwise pass nothing
            If conBO.PatchId > 0 Then
                faultParam = New ParameterBO("patch", conBO.PatchId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Scheme then pass it to SP otherwise pass nothing
            If conBO.SchemeId > 0 Then
                faultParam = New ParameterBO("scheme", conBO.SchemeId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user mentioned the Postcode then pass it to SP otherwise pass nothing
            If Not conBO.PostCode = String.Empty Then
                faultParam = New ParameterBO("postcode", conBO.PostCode, DbType.String)
                paramList.Add(faultParam)
            End If

            'If user selected the Due Date then pass it to SP otherwise pass nothing
            If conBO.Due <> Nothing Then
                faultParam = New ParameterBO("due", conBO.Due, DbType.String)
                paramList.Add(faultParam)
            End If

            If conBO.JsNumber <> Nothing Then
                faultParam = New ParameterBO("jsnumber", conBO.JsNumber, DbType.Int32)
                paramList.Add(faultParam)
            End If

            sprocName = SprocNameConstants.GetJobSheetFaultCount

            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            If myReader.Read Then
                rowCount = Me.GetRowCount(myReader)
            End If

            Return rowCount
        End Function
#End Region

#Region "Get JobSheet Summary Popup data"
        ''' <summary>
        ''' to get Job sheet summary against every reported fault
        ''' </summary>
        ''' <param name="JobSheetDS"> dataset to be filled against a js number</param>
        ''' <param name="jsNumber">required JS number</param>
        ''' <remarks></remarks>
        Public Sub GetJSSummaryPopupData(ByRef JobSheetDS As DataSet, ByRef jsNumber As String)


            Dim sProc As String

            Dim JSsummaryParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim JSSummaryParamBO As ParameterBO

            JSSummaryParamBO = New ParameterBO("JSNumber", jsNumber, DbType.String)
            JSsummaryParameterList.Add(JSSummaryParamBO)

            sProc = SprocNameConstants.GetJSSummaryPopupData

            MyBase.LoadDataSet(JobSheetDS, JSsummaryParameterList, sProc)


        End Sub
#End Region
        
#Region "Get JobSheet Summary Popup Asbestos data"

        ''' <summary>
        ''' to get Job sheet summary against every reported fault
        ''' </summary>
        ''' <param name="JobSheetAsbestosDS"> dataset to be filled against a js number</param>
        ''' <param name="jsNumber">required JS number</param>
        ''' <remarks></remarks>
        Public Sub GetJSSummaryPopupAsbestosData(ByRef JobSheetAsbestosDS As DataSet, ByRef jsNumber As String)
            Dim sProc As String
            Dim JSsummaryAsbestosParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()
            Dim JSSummaryAsbestosParamBO As ParameterBO
            JSSummaryAsbestosParamBO = New ParameterBO("JSNumber", jsNumber, DbType.String)
            JSsummaryAsbestosParameterList.Add(JSSummaryAsbestosParamBO)
            sProc = SprocNameConstants.GetJSSummaryPopupAsbestosData
            MyBase.LoadDataSet(JobSheetAsbestosDS, JSsummaryAsbestosParameterList, sProc)
        End Sub

#End Region

#End Region

        '-------------------------------------------------------------------------------------------------------------

#Region " Assignment To Be Arranged  - Coded By Usman Sharif - Januray, 2009"



#Region "Get TBA Vulnerability Data"
        Public Sub GetTBAVulnerabilityData(ByRef TBADS As DataSet, ByRef CustomerId As Integer)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim TBAParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            TBAParam = New ParameterBO("CustomerId", CustomerId, DbType.Int32)
            paramList.Add(TBAParam)

            sprocName = SprocNameConstants.GetTBACustomerVulnerability

            MyBase.LoadDataSet(TBADS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If TBADS.Tables(0).Rows.Count = 0 Then
            End If
            'TBASearchBO.IsFlagStatus = False
            'TBASearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            'Else
            'TBASearchBO.IsFlagStatus = True
            'End If
        End Sub
#End Region

#Region "Get TBA Risk Data"
        Public Sub GetTBARiskData(ByRef TBADS As DataSet, ByRef CustomerId As Integer)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim TBAParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            TBAParam = New ParameterBO("CustomerId", CustomerId, DbType.Int32)
            paramList.Add(TBAParam)

            sprocName = SprocNameConstants.GetTBACustomerRisk

            MyBase.LoadDataSet(TBADS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If TBADS.Tables(0).Rows.Count = 0 Then
            End If
            'TBASearchBO.IsFlagStatus = False
            'TBASearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            'Else
            'TBASearchBO.IsFlagStatus = True
            'End If
        End Sub
#End Region

#Region "Get TBA Gas Servicing Data"
        Public Sub GetTBAGasServicingData(ByRef TBADS As DataSet, ByRef CustomerId As Integer)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim TBAParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            TBAParam = New ParameterBO("CustomerId", CustomerId, DbType.Int32)
            paramList.Add(TBAParam)

            sprocName = SprocNameConstants.GetTBAGasServicingData

            MyBase.LoadDataSet(TBADS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If TBADS.Tables(0).Rows.Count = 0 Then
            End If
            'TBASearchBO.IsFlagStatus = False
            'TBASearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            'Else
            'TBASearchBO.IsFlagStatus = True
            'End If
        End Sub
#End Region

#Region "Get TBA Customer Communication Data"
        Public Sub GetTBACommunicationData(ByRef TBADS As DataSet, ByRef CustomerId As Integer)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim TBAParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            TBAParam = New ParameterBO("CustomerId", CustomerId, DbType.Int32)
            paramList.Add(TBAParam)

            sprocName = SprocNameConstants.GetTBACommunicationData

            MyBase.LoadDataSet(TBADS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If TBADS.Tables(0).Rows.Count = 0 Then
            End If
            'TBASearchBO.IsFlagStatus = False
            'TBASearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            'Else
            'TBASearchBO.IsFlagStatus = True
            'End If
        End Sub
#End Region

#Region "Get TBA Search Data"
        Public Sub GetTBASearchData(ByRef TBADS As DataSet, ByRef tbaSearchBO As TBASearchBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim TBAParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            TBAParam = New ParameterBO("noOfRows", tbaSearchBO.RowCount, DbType.Int32)
            paramList.Add(TBAParam)
            TBAParam = New ParameterBO("offSet", tbaSearchBO.PageIndex, DbType.Int32)
            paramList.Add(TBAParam)
            TBAParam = New ParameterBO("sortColumn", tbaSearchBO.SortBy, DbType.String)
            paramList.Add(TBAParam)
            TBAParam = New ParameterBO("sortOrder", tbaSearchBO.SortOrder, DbType.String)
            paramList.Add(TBAParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object
            If tbaSearchBO.IsSearch = True Then

                If tbaSearchBO.LocationID <> "" Then
                    TBAParam = New ParameterBO("locationId", CType(tbaSearchBO.LocationID, Int32), DbType.Int32)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("locationId", Nothing, DbType.Int32)
                    paramList.Add(TBAParam)
                End If

                If tbaSearchBO.AreaID <> "" And tbaSearchBO.AreaID <> "0" And tbaSearchBO.AreaID <> "-1" Then
                    TBAParam = New ParameterBO("areaId", CType(tbaSearchBO.AreaID, Int32), DbType.String)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("areaId", Nothing, DbType.String)
                    paramList.Add(TBAParam)
                End If

                If tbaSearchBO.ElementID <> "" And tbaSearchBO.ElementID <> "-1" And tbaSearchBO.ElementID <> "0" Then
                    TBAParam = New ParameterBO("elementId", CType(tbaSearchBO.ElementID, Int32), DbType.Int32)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("elementId", Nothing, DbType.Int32)
                    paramList.Add(TBAParam)
                End If

                If tbaSearchBO.PriorityID <> "" And tbaSearchBO.PriorityID <> "-1" Then
                    TBAParam = New ParameterBO("priorityId", CType(tbaSearchBO.PriorityID, Int32), DbType.Int32)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("priorityId", Nothing, DbType.Int32)
                    paramList.Add(TBAParam)
                End If

                If tbaSearchBO.Status <> "" Then
                    TBAParam = New ParameterBO("status", CType(tbaSearchBO.Status, Integer), DbType.Int32)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("status", Nothing, DbType.Int32)
                    paramList.Add(TBAParam)
                End If
                If tbaSearchBO.Due <> "" Then
                    TBAParam = New ParameterBO("due", CType(tbaSearchBO.Due, String), DbType.AnsiString)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("due", Nothing, DbType.String)
                    paramList.Add(TBAParam)
                End If
                If tbaSearchBO.PatchId <> "" Then
                    TBAParam = New ParameterBO("patch", CType(tbaSearchBO.PatchId, Integer), DbType.Int32)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("patch", Nothing, DbType.Int32)
                    paramList.Add(TBAParam)
                End If
                If tbaSearchBO.SchemeId <> "" Then
                    TBAParam = New ParameterBO("scheme", CType(tbaSearchBO.SchemeId, Integer), DbType.Int32)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("scheme", Nothing, DbType.Int32)
                    paramList.Add(TBAParam)
                End If
                If tbaSearchBO.Postcode <> "" Then
                    TBAParam = New ParameterBO("postcode", CType(tbaSearchBO.Postcode, String), DbType.String)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("postcode", Nothing, DbType.String)
                    paramList.Add(TBAParam)
                End If
                If tbaSearchBO.UserId <> "" Then
                    TBAParam = New ParameterBO("user", CType(tbaSearchBO.UserId, Integer), DbType.Int32)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("user", Nothing, DbType.Int32)
                    paramList.Add(TBAParam)
                End If
                If tbaSearchBO.OrgId <> "" Then
                    TBAParam = New ParameterBO("ORGID", CType(tbaSearchBO.OrgId, Integer), DbType.Int32)
                    paramList.Add(TBAParam)
                Else
                    TBAParam = New ParameterBO("ORGID", Nothing, DbType.Int32)
                    paramList.Add(TBAParam)
                End If
                If tbaSearchBO.JsNumber <> Nothing Then
                    TBAParam = New ParameterBO("jsnumber", tbaSearchBO.JsNumber, DbType.Int32)
                    paramList.Add(TBAParam)
                End If
                sprocName = SprocNameConstants.GetTBASearchNew
            Else
                sprocName = SprocNameConstants.GetTBASearchNew
            End If

            MyBase.LoadDataSet(TBADS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If TBADS.Tables(0).Rows.Count = 0 Then

                tbaSearchBO.IsFlagStatus = False
                tbaSearchBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                tbaSearchBO.IsFlagStatus = True
            End If
        End Sub
#End Region

#Region "Get Assignment TBA Search Row Count "
        Public Function GetAssignmentTBASearchRowCount(ByRef tbaSearchBO As TBASearchBO) As Integer
            Dim sprocName As String
            Dim rowCount As Integer = 0

            sprocName = SprocNameConstants.GetTBASearchRowCountNew
            Dim paramList As ParameterList = New ParameterList()

            Dim TBAParam As ParameterBO

            If tbaSearchBO.LocationID <> "" Then
                TBAParam = New ParameterBO("locationId", CType(tbaSearchBO.LocationID, Int32), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("locationId", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If

            If tbaSearchBO.AreaID <> "" And tbaSearchBO.AreaID <> "0" And tbaSearchBO.AreaID <> "-1" Then
                TBAParam = New ParameterBO("areaId", CType(tbaSearchBO.AreaID, Int32), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("areaId", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If

            If tbaSearchBO.ElementID <> "" And tbaSearchBO.ElementID <> "-1" And tbaSearchBO.ElementID <> "0" Then
                TBAParam = New ParameterBO("elementId", CType(tbaSearchBO.ElementID, Int32), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("elementId", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If

            If tbaSearchBO.PriorityID <> "" And tbaSearchBO.PriorityID <> "-1" Then
                TBAParam = New ParameterBO("priorityId", CType(tbaSearchBO.PriorityID, Int32), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("priorityId", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If

            If tbaSearchBO.Status <> "" Then
                TBAParam = New ParameterBO("status", CType(tbaSearchBO.Status, Integer), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("status", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If
            If tbaSearchBO.Due <> "" Then
                TBAParam = New ParameterBO("due", CType(tbaSearchBO.Due, String), DbType.AnsiString)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("due", Nothing, DbType.AnsiString)
                paramList.Add(TBAParam)
            End If
            If tbaSearchBO.PatchId <> "" Then
                TBAParam = New ParameterBO("patch", CType(tbaSearchBO.PatchId, Integer), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("patch", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If
            If tbaSearchBO.SchemeId <> "" Then
                TBAParam = New ParameterBO("scheme", CType(tbaSearchBO.SchemeId, Integer), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("scheme", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If

            If tbaSearchBO.Postcode <> "" Then
                TBAParam = New ParameterBO("postcode", CType(tbaSearchBO.Postcode, String), DbType.String)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("postcode", Nothing, DbType.String)
                paramList.Add(TBAParam)
            End If

            If tbaSearchBO.UserId <> "" Then
                TBAParam = New ParameterBO("user", CType(tbaSearchBO.UserId, Integer), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("user", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If
            If tbaSearchBO.OrgId <> "" Then
                TBAParam = New ParameterBO("ORGID", CType(tbaSearchBO.OrgId, Integer), DbType.Int32)
                paramList.Add(TBAParam)
            Else
                TBAParam = New ParameterBO("ORGID", Nothing, DbType.Int32)
                paramList.Add(TBAParam)
            End If

            If tbaSearchBO.JsNumber <> Nothing Then
                TBAParam = New ParameterBO("jsnumber", tbaSearchBO.JsNumber, DbType.Int32)
                paramList.Add(TBAParam)
            End If
            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If

            Return rowCount

        End Function
#End Region

#Region "Save Appointment"

        Public Sub SaveAppointment(ByRef appointmentBO As AppointmentTBABO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.SaveAppointmentTBA

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()


            '' Creating input ParameterBO objects and passing prameters jsNumber
            Dim jsNumber As ParameterBO = New ParameterBO("JSNUMBER", appointmentBO.JSNumber, DbType.String)

            '' Creating input ParameterBO objects and passing prameters appointmentDate
            Dim appointmentDate As ParameterBO = New ParameterBO("APPOINTMENTDATE", appointmentBO.AppointmentDate, DbType.String)

            '' Creating input ParameterBO objects and passing prameters operativeId
            Dim operativeId As ParameterBO = New ParameterBO("OPERATIVEID", appointmentBO.OperativeId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters letterId
            Dim letterId As ParameterBO = New ParameterBO("LETTERID", appointmentBO.LetterId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters appointmentStageId
            Dim appointMentStageId As ParameterBO = New ParameterBO("APPOINTMENTSTAGEID", appointmentBO.AppointmentStageId, DbType.Int32)

            '' Creating input ParameterBO objects and passing prameters lastActionDate
            Dim lastActionDate As ParameterBO = New ParameterBO("LASTACTIONDATE", appointmentBO.LastActionDate, DbType.String)

            '' Creating input ParameterBO objects and passing prameters notes
            Dim notes As ParameterBO = New ParameterBO("NOTES", appointmentBO.Notes, DbType.String)

            '' Creating input ParameterBO objects and passing prameters appointmentTime
            Dim appointmentTime As ParameterBO = New ParameterBO("APPOINTMENTTIME", appointmentBO.AppointmentTime, DbType.String)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(jsNumber)
            inParamList.Add(appointmentDate)
            inParamList.Add(operativeId)
            inParamList.Add(letterId)
            inParamList.Add(appointMentStageId)
            inParamList.Add(lastActionDate)
            inParamList.Add(notes)
            inParamList.Add(appointmentTime)

            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                appointmentBO.IsFlagStatus = False
            Else
                appointmentBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#End Region

        '-------------------------------------------------------------------------------------------------------------

#Region "Manage Appointment & Work Completion - Coded By Waseem Hasan - Januray, 2009 "

#Region "Get Manage Appointment Search Row Count "
        Public Function GetManageAppointmentSearchRowCount(ByRef cpBO As ContractorPortalBO) As Integer
            Dim sprocName As String
            Dim rowCount As Integer = 0

            sprocName = SprocNameConstants.GetManageAppointmentSearchRowCount
            Dim paramList As ParameterList = New ParameterList()

            Dim manageAppointmentParams As ParameterBO

            If cpBO.OrgId <> -1 Then
                manageAppointmentParams = New ParameterBO("ORGID", CType(cpBO.OrgId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.LocationId <> -1 Then
                manageAppointmentParams = New ParameterBO("locationId", CType(cpBO.LocationId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If
            If cpBO.AreaId <> -1 Then
                manageAppointmentParams = New ParameterBO("areaId", CType(cpBO.AreaId, Int32), DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.ElementId <> -1 Then
                manageAppointmentParams = New ParameterBO("elementId", CType(cpBO.ElementId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.TeamId <> -1 Then
                manageAppointmentParams = New ParameterBO("teamId", CType(cpBO.TeamId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.UserId <> -1 Then
                manageAppointmentParams = New ParameterBO("userId", CType(cpBO.UserId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PatchId <> -1 Then
                manageAppointmentParams = New ParameterBO("patchId", CType(cpBO.PatchId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.SchemeId <> -1 Then
                manageAppointmentParams = New ParameterBO("schemeId", CType(cpBO.SchemeId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PriorityId <> -1 Then
                manageAppointmentParams = New ParameterBO("priorityId", CType(cpBO.PriorityId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.StatusId <> -1 Then
                manageAppointmentParams = New ParameterBO("statusId", CType(cpBO.StatusId, Integer), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.StageId <> -1 Then
                manageAppointmentParams = New ParameterBO("stageId", CType(cpBO.StageId, Integer), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.Due <> Nothing Then
                manageAppointmentParams = New ParameterBO("due", cpBO.Due, DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PostCode <> Nothing Then
                manageAppointmentParams = New ParameterBO("postCode", CType(cpBO.PostCode, String), DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.JsNumber <> Nothing Then
                manageAppointmentParams = New ParameterBO("jsnumber", cpBO.JsNumber, DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If
            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If

            Return rowCount

        End Function
#End Region

#Region "Get Work Completion Search Row Count "
        Public Function GetWorkCompletionSearchRowCount(ByRef cpBO As ContractorPortalBO) As Integer
            Dim sprocName As String
            Dim rowCount As Integer = 0

            sprocName = SprocNameConstants.GetWorkCompletionSearchRowCount
            Dim paramList As ParameterList = New ParameterList()

            Dim manageAppointmentParams As ParameterBO

            If cpBO.OrgId <> -1 Then
                manageAppointmentParams = New ParameterBO("ORGID", CType(cpBO.OrgId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.LocationId <> -1 Then
                manageAppointmentParams = New ParameterBO("locationId", CType(cpBO.LocationId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If
            If cpBO.AreaId <> -1 Then
                manageAppointmentParams = New ParameterBO("areaId", CType(cpBO.AreaId, Int32), DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.ElementId <> -1 Then
                manageAppointmentParams = New ParameterBO("elementId", CType(cpBO.ElementId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.TeamId <> -1 Then
                manageAppointmentParams = New ParameterBO("teamId", CType(cpBO.TeamId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.UserId <> -1 Then
                manageAppointmentParams = New ParameterBO("userId", CType(cpBO.UserId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PatchId <> -1 Then
                manageAppointmentParams = New ParameterBO("patchId", CType(cpBO.PatchId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.SchemeId <> -1 Then
                manageAppointmentParams = New ParameterBO("schemeId", CType(cpBO.SchemeId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PriorityId <> -1 Then
                manageAppointmentParams = New ParameterBO("priorityId", CType(cpBO.PriorityId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.StatusId <> -1 Then
                manageAppointmentParams = New ParameterBO("statusId", CType(cpBO.StatusId, Integer), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.StageId <> -1 Then
                manageAppointmentParams = New ParameterBO("stageId", CType(cpBO.StageId, Integer), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.Due <> Nothing Then
                manageAppointmentParams = New ParameterBO("due", cpBO.Due, DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PostCode <> Nothing Then
                manageAppointmentParams = New ParameterBO("postCode", CType(cpBO.PostCode, String), DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.JsNumber <> Nothing Then
                manageAppointmentParams = New ParameterBO("jsnumber", cpBO.JsNumber, DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If

            Return rowCount

        End Function
#End Region

#Region "Get Manage Appointment Search Data "
        Public Sub GetManageAppointmentSearchData(ByRef MADS As DataSet, ByRef cpBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim manageAppointmentParams As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            manageAppointmentParams = New ParameterBO("noOfRows", cpBO.RowCount, DbType.Int32)
            paramList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("offSet", cpBO.PageIndex, DbType.Int32)
            paramList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("sortColumn", cpBO.SortBy, DbType.String)
            paramList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("sortOrder", cpBO.SortOrder, DbType.String)
            paramList.Add(manageAppointmentParams)

            If cpBO.OrgId <> -1 Then
                manageAppointmentParams = New ParameterBO("ORGID", CType(cpBO.OrgId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.LocationId <> -1 Then
                manageAppointmentParams = New ParameterBO("locationId", CType(cpBO.LocationId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If
            If cpBO.AreaId <> -1 Then
                manageAppointmentParams = New ParameterBO("areaId", CType(cpBO.AreaId, Int32), DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.ElementId <> -1 Then
                manageAppointmentParams = New ParameterBO("elementId", CType(cpBO.ElementId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.TeamId <> -1 Then
                manageAppointmentParams = New ParameterBO("teamId", CType(cpBO.TeamId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.UserId <> -1 Then
                manageAppointmentParams = New ParameterBO("userId", CType(cpBO.UserId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PatchId <> -1 Then
                manageAppointmentParams = New ParameterBO("patchId", CType(cpBO.PatchId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.SchemeId <> -1 Then
                manageAppointmentParams = New ParameterBO("schemeId", CType(cpBO.SchemeId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PriorityId <> -1 Then
                manageAppointmentParams = New ParameterBO("priorityId", CType(cpBO.PriorityId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.StatusId <> -1 Then
                manageAppointmentParams = New ParameterBO("statusId", CType(cpBO.StatusId, Integer), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.StageId <> -1 Then
                manageAppointmentParams = New ParameterBO("stageId", CType(cpBO.StageId, Integer), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            'If cpBO.Due <> "12:00:00 AM" Then
            '    manageAppointmentParams = New ParameterBO("due", cpBO.Due, DbType.String)
            '    paramList.Add(manageAppointmentParams)
            'End If

            If cpBO.Due <> Nothing Then
                manageAppointmentParams = New ParameterBO("due", cpBO.Due, DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PostCode <> Nothing Then
                manageAppointmentParams = New ParameterBO("postCode", CType(cpBO.PostCode, String), DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.JsNumber <> Nothing Then
                manageAppointmentParams = New ParameterBO("jsnumber", cpBO.JsNumber, DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            sprocName = SprocNameConstants.GetManageAppointmentSearchRecords

            MyBase.LoadDataSet(MADS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If MADS.Tables(0).Rows.Count = 0 Then

                cpBO.IsFlagStatus = False
                cpBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                cpBO.IsFlagStatus = True
            End If
        End Sub
#End Region

#Region "Get Work Completion Search Data "
        Public Sub GetWorkCompletionSearchData(ByRef WCDS As DataSet, ByRef cpBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim manageAppointmentParams As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            manageAppointmentParams = New ParameterBO("noOfRows", cpBO.RowCount, DbType.Int32)
            paramList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("offSet", cpBO.PageIndex, DbType.Int32)
            paramList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("sortColumn", cpBO.SortBy, DbType.String)
            paramList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("sortOrder", cpBO.SortOrder, DbType.String)
            paramList.Add(manageAppointmentParams)

            If cpBO.OrgId <> -1 Then
                manageAppointmentParams = New ParameterBO("ORGID", CType(cpBO.OrgId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.LocationId <> -1 Then
                manageAppointmentParams = New ParameterBO("locationId", CType(cpBO.LocationId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If
            If cpBO.AreaId <> -1 Then
                manageAppointmentParams = New ParameterBO("areaId", CType(cpBO.AreaId, Int32), DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.ElementId <> -1 Then
                manageAppointmentParams = New ParameterBO("elementId", CType(cpBO.ElementId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.TeamId <> -1 Then
                manageAppointmentParams = New ParameterBO("teamId", CType(cpBO.TeamId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.UserId <> -1 Then
                manageAppointmentParams = New ParameterBO("userId", CType(cpBO.UserId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PatchId <> -1 Then
                manageAppointmentParams = New ParameterBO("patchId", CType(cpBO.PatchId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.SchemeId <> -1 Then
                manageAppointmentParams = New ParameterBO("schemeId", CType(cpBO.SchemeId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PriorityId <> -1 Then
                manageAppointmentParams = New ParameterBO("priorityId", CType(cpBO.PriorityId, Int32), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.StatusId <> -1 Then
                manageAppointmentParams = New ParameterBO("statusId", CType(cpBO.StatusId, Integer), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.StageId <> -1 Then
                manageAppointmentParams = New ParameterBO("stageId", CType(cpBO.StageId, Integer), DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.Due <> Nothing Then
                manageAppointmentParams = New ParameterBO("due", cpBO.Due, DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.PostCode <> Nothing Then
                manageAppointmentParams = New ParameterBO("postCode", CType(cpBO.PostCode, String), DbType.String)
                paramList.Add(manageAppointmentParams)
            End If

            If cpBO.JsNumber <> Nothing Then
                manageAppointmentParams = New ParameterBO("jsnumber", cpBO.JsNumber, DbType.Int32)
                paramList.Add(manageAppointmentParams)
            End If

            sprocName = SprocNameConstants.GetWorkCompletionSearchRecords

            MyBase.LoadDataSet(WCDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If WCDS.Tables(0).Rows.Count = 0 Then

                cpBO.IsFlagStatus = False
                cpBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                cpBO.IsFlagStatus = True
            End If
        End Sub
#End Region

#Region "Update Manage Appointment Data "
        Public Sub UpdateManageAppointmentData(ByRef cpBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim inparamList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim manageAppointmentParams As ParameterBO
            Dim outParamList As ParameterList = New ParameterList()

            '' rowindex, pageindex and sortby will be common to both type of searches
            manageAppointmentParams = New ParameterBO("appointmentId", cpBO.AppointmentId, DbType.Int32)
            inparamList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("stageId", cpBO.StageId, DbType.Int32)
            inparamList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("appointmentDate", cpBO.AppointmentDate, DbType.String)
            inparamList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("appointmentTime", cpBO.Time, DbType.String)
            inparamList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("lastActioned", cpBO.LastActioned, DbType.String)
            inparamList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("operativeId", cpBO.OperativeId, DbType.Int32)
            inparamList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("appointmentNotes", cpBO.Notes, DbType.String)
            inparamList.Add(manageAppointmentParams)
            manageAppointmentParams = New ParameterBO("letterId", cpBO.LetterId, DbType.Int32)
            inparamList.Add(manageAppointmentParams)
            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            sprocName = SprocNameConstants.UpdateManageAppointmentRecord

            '' Passing Array of cpBO and sproc-name to base class method: SaveRecord
            MyBase.SaveRecord(inparamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                cpBO.IsFlagStatus = False
            Else
                cpBO.IsFlagStatus = True
            End If
        End Sub
#End Region

#Region "Get Repair Data "
        Public Sub GetRepairData(ByRef cpBO As ContractorPortalBO, ByRef ds As DataSet)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            Dim paramlist As New ParameterList()

            sprocName = SprocNameConstants.GetRepairData
            MyBase.LoadDataSet(ds, paramlist, sprocName)

        End Sub
#End Region

#Region "Get Vat Rate "
        Public Sub GetVatRate(ByRef cpBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            sprocName = SprocNameConstants.GetVatRateValue

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters vatId
            Dim vatId As ParameterBO = New ParameterBO("vatId", cpBO.VatId, DbType.Int32)

            ''Adding objects to input paramList
            inParamList.Add(vatId)

            Dim reader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            While reader.Read()
                If Not reader.IsDBNull(reader.GetOrdinal("VATRATE")) Then
                    cpBO.VatRate = reader.GetDouble(reader.GetOrdinal("VATRATE"))
                Else
                    cpBO.IsFlagStatus = False
                End If
            End While

        End Sub
#End Region

#Region "Save Repair Record "
        Public Sub SaveRepairRecord(ByRef cpBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            sprocName = SprocNameConstants.SaveRepairRecord

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters 
            Dim repairParams As ParameterBO

            repairParams = New ParameterBO("repairDetail", cpBO.RepairDescription, DbType.String)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("net", cpBO.Net, DbType.Double)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("vatType", cpBO.VatId, DbType.Int32)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("vat", cpBO.Vat, DbType.Double)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("total", cpBO.Total, DbType.Double)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(repairParams)

            MyBase.SelectRecord(inParamList, outParamList, sprocName)


            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                cpBO.IsFlagStatus = False
            Else
                cpBO.IsFlagStatus = True
            End If


        End Sub
#End Region

#Region "Save Repairs Against Fault "
        Public Sub SaveRepairsAgainstFault(ByRef conBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            sprocName = SprocNameConstants.SaveRepairAgainstFault

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters 
            Dim repairParams As ParameterBO

            repairParams = New ParameterBO("RepairId", conBO.RepairId, DbType.Int32)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("FaultLogId", conBO.FaultLogId, DbType.Int32)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("Qty", conBO.Quantity, DbType.Int32)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("Result", Nothing, DbType.String)
            outParamList.Add(repairParams)

            Dim qryResult As Integer = MyBase.SaveRecord(inParamList, outParamList, sprocName)


            ''In the case of invalid information provided
            If qryResult < 0 Then
                conBO.IsFlagStatus = False
            Else
                conBO.IsFlagStatus = True
            End If


        End Sub
#End Region

#Region "Save Invoicing Record "
        Public Sub SaveInvoicingRecord(ByRef cpBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            sprocName = SprocNameConstants.SaveInvoicingRecord

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters 
            Dim repairParams As ParameterBO

            repairParams = New ParameterBO("FaultLogId", cpBO.FaultLogId, DbType.Int32)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("completionDate", cpBO.CompletionDate, DbType.Date)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("completedBy", cpBO.CompletedBy, DbType.Int32)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("orgId", cpBO.OrgId, DbType.Int32)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("appointmentId", cpBO.AppointmentId, DbType.Int32)
            inParamList.Add(repairParams)

            repairParams = New ParameterBO("time", cpBO.Time, DbType.String)
            inParamList.Add(repairParams)

            'repairParams = New ParameterBO("repairDetail", cpBO.RepairDescription, DbType.String)
            'inParamList.Add(repairParams)

            'repairParams = New ParameterBO("net", cpBO.Net, DbType.Double)
            'inParamList.Add(repairParams)

            'repairParams = New ParameterBO("vatId", cpBO.VatId, DbType.Int32)
            'inParamList.Add(repairParams)

            'repairParams = New ParameterBO("vat", cpBO.Vat, DbType.Double)
            'inParamList.Add(repairParams)

            'repairParams = New ParameterBO("gross", cpBO.Total, DbType.Double)
            'inParamList.Add(repairParams)

            repairParams = New ParameterBO("RESULT", Nothing, DbType.String)
            outParamList.Add(repairParams)

            Dim qryResult As Integer = MyBase.SaveRecord(inParamList, outParamList, sprocName)


            ''In the case of invalid information provided
            If outParamList.Item(0).Value < 0 Then
                cpBO.IsFlagStatus = False
            Else
                cpBO.IsFlagStatus = True
            End If


        End Sub
#End Region

#Region "Get Repair Record "
        Public Sub GetRepairRecord(ByRef cpBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            sprocName = SprocNameConstants.GetRepairRecord

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters 
            Dim repairParams As ParameterBO

            repairParams = New ParameterBO("repairDetail", cpBO.RepairDescription, DbType.String)
            inParamList.Add(repairParams)

            Dim reader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            While reader.Read()
                If Not reader.IsDBNull(reader.GetOrdinal("FaultRepairListID")) Then
                    cpBO.RepairId = reader.GetInt32(reader.GetOrdinal("FaultRepairListID"))
                    cpBO.Net = reader.GetDouble(reader.GetOrdinal("NetCost"))
                    cpBO.VatRate = reader.GetInt32(reader.GetOrdinal("VatRateID"))
                    cpBO.Vat = reader.GetDouble(reader.GetOrdinal("Vat"))
                    cpBO.Total = reader.GetDouble(reader.GetOrdinal("Gross"))
                Else
                    cpBO.IsFlagStatus = False
                End If
            End While


        End Sub
#End Region

#End Region

        '-------------------------------------------------------------------------------------------------------------

#Region "Generic Methods & Functions"
        ''' <summary>
        ''' Private method used to return no. of results
        ''' </summary>
        ''' <param name="myDataRecord"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function GetRowCount(ByRef myDataRecord As IDataRecord) As Integer

            Return myDataRecord.GetInt32(myDataRecord.GetOrdinal("numOfRows"))

        End Function
#End Region

        '-------------------------------------------------------------------------------------------------------------

#Region "Invoicing Methods - Coded By Tahir Gul - January,2009 "

#Region "Get Invoice Search Row Count "

        ''' <summary>
        ''' will return the number of rows returned
        ''' </summary>
        ''' <param name="invoiceSearchBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>

        Public Function GetInvoiceSearchRowCount(ByRef invoiceSearchBO As InvoiceBO) As Integer

            Dim sProc As String = SprocNameConstants.GetInvoiceSearchRowCount
            Dim rowCount As Integer = 0

            Dim invoicingParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim invoiceParamBO As ParameterBO

            If (invoiceSearchBO.LocationId <> -1) Then
                invoiceParamBO = New ParameterBO("locationId", CType(invoiceSearchBO.LocationId, Integer), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoiceSearchBO.AreaId <> -1) Then
                invoiceParamBO = New ParameterBO("areaId", CType(invoiceSearchBO.AreaId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoiceSearchBO.ElementId <> -1) Then
                invoiceParamBO = New ParameterBO("elementId", CType(invoiceSearchBO.ElementId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoiceSearchBO.PriorityId <> -1) Then
                invoiceParamBO = New ParameterBO("priorityId", CType(invoiceSearchBO.PriorityId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoiceSearchBO.StatusId <> -1) Then
                invoiceParamBO = New ParameterBO("statusId", CType(invoiceSearchBO.StatusId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)

            End If

            If (invoiceSearchBO.UserId <> -1) Then
                invoiceParamBO = New ParameterBO("userId", CType(invoiceSearchBO.UserId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If


            If (invoiceSearchBO.OrgId <> -1) Then
                invoiceParamBO = New ParameterBO("orgId", CType(invoiceSearchBO.OrgId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)

            End If

            If (invoiceSearchBO.PostCode <> Nothing) Then
                invoiceParamBO = New ParameterBO("postCode", CType(invoiceSearchBO.PostCode, String), DbType.String)
                invoicingParameterList.Add(invoiceParamBO)

            End If

            If (invoiceSearchBO.DueDate <> Nothing) Then
                invoiceParamBO = New ParameterBO("dueDate", CType(invoiceSearchBO.DueDate, String), DbType.String)
                invoicingParameterList.Add(invoiceParamBO)

            End If

            If (invoiceSearchBO.PatchId <> -1) Then
                invoiceParamBO = New ParameterBO("patchId", CType(invoiceSearchBO.PatchId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)

            End If

            If (invoiceSearchBO.SchemeId <> -1) Then
                invoiceParamBO = New ParameterBO("schemeId", CType(invoiceSearchBO.SchemeId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoiceSearchBO.StageId <> -1) Then
                invoiceParamBO = New ParameterBO("stageId", CType(invoiceSearchBO.StageId, Int32), DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            Dim myReader As IDataReader = MyBase.SelectRecord(invoicingParameterList, sProc)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If

            Return rowCount

        End Function
#End Region

#Region "Process Invoice "
        Public Sub CreateBatchforInvoicing(ByRef FaultList As String, ByRef conBO As ContractorPortalBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.SaveBatchInvoicing

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            
            Dim faultParam As ParameterBO = New ParameterBO("OrgId", conBO.OrgId, DbType.Int32)
            inParamList.Add(faultParam)

            '' Creating input ParameterBO objects and passing prameters jsNumber
            Dim Faults As ParameterBO = New ParameterBO("FaultList", FaultList, DbType.String)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)


            ''Adding objects to input paramList
            inParamList.Add(Faults)

            ''adding object to output paramList
            outParamList.Add(result)

            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

        End Sub
#End Region
#Region "Get Invoicing Contract Portal DAL "

        ''' <summary>
        ''' To get the number of result based on the search criteria so that to populate the data grid. 
        ''' </summary>
        ''' <param name="invoiceDS"></param>
        ''' <param name="invoice"></param>
        ''' <remarks></remarks>

        Public Sub GetInvoicingContractPortalDAL(ByRef invoiceDS As DataSet, ByRef invoice As InvoiceBO)


            Dim sProc As String

            Dim invoicingParameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim invoiceParamBO As ParameterBO

            invoiceParamBO = New ParameterBO("noOfRows", invoice.RowCount, DbType.Int32)
            invoicingParameterList.Add(invoiceParamBO)
            invoiceParamBO = New ParameterBO("offSet", invoice.PageIndex, DbType.Int32)
            invoicingParameterList.Add(invoiceParamBO)
            invoiceParamBO = New ParameterBO("sortColumn", invoice.SortBy, DbType.String)
            invoicingParameterList.Add(invoiceParamBO)
            invoiceParamBO = New ParameterBO("sortOrder", invoice.SortOrder, DbType.String)
            invoicingParameterList.Add(invoiceParamBO)

            If (invoice.LocationId <> -1) Then
                invoiceParamBO = New ParameterBO("locationId", invoice.LocationId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.AreaId <> -1) Then
                invoiceParamBO = New ParameterBO("areaId", invoice.AreaId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.ElementId <> -1) Then
                invoiceParamBO = New ParameterBO("elementId", invoice.ElementId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.PriorityId <> -1) Then
                invoiceParamBO = New ParameterBO("priorityId", invoice.PriorityId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.StatusId <> -1) Then
                invoiceParamBO = New ParameterBO("statusId", invoice.StatusId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.UserId <> -1) Then
                invoiceParamBO = New ParameterBO("userId", invoice.UserId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.OrgId <> -1) Then
                invoiceParamBO = New ParameterBO("orgId", invoice.OrgId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.PostCode <> String.Empty) Then
                invoiceParamBO = New ParameterBO("postCode", invoice.PostCode, DbType.String)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.DueDate <> String.Empty) Then
                invoiceParamBO = New ParameterBO("dueDate", invoice.DueDate, DbType.String)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.PatchId <> -1) Then
                invoiceParamBO = New ParameterBO("patchId", invoice.PatchId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.SchemeId <> -1) Then
                invoiceParamBO = New ParameterBO("schemeId", invoice.SchemeId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            If (invoice.StageId <> -1) Then
                invoiceParamBO = New ParameterBO("stageId", invoice.StageId, DbType.Int32)
                invoicingParameterList.Add(invoiceParamBO)
            End If

            sProc = SprocNameConstants.GetInvoicingGridResult

            MyBase.LoadDataSet(invoiceDS, invoicingParameterList, sProc)

            ' to check if stored procedure has got no problem

            If invoiceDS.Tables(0).Rows.Count = 0 Then

                invoice.IsFlagStatus = False
                invoice.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                invoice.IsFlagStatus = True
            End If

        End Sub
#Region "Populate Inner Repair Grid in Invoicng Grid"
        Public Sub PopulateInnerRepairGrid(ByVal faultLogId As Integer, ByRef repairDS As DataSet)

            Dim sprocName As String = SprocNameConstants.ReactiveRepairNatureGrid
            Dim faultParam As ParameterBO
            Dim paramList As ParameterList = New ParameterList()
            faultParam = New ParameterBO("faultLogId", faultLogId, DbType.Int32)
            paramList.Add(faultParam)

            MyBase.LoadDataSetNature(repairDS, paramList, sprocName)

        End Sub
#End Region

#End Region
#End Region

        '-------------------------------------------------------------------------------------------------------------

#Region "Monitoring Methods - Coded By Noor Muhammad - January,2009 "

#Region "Get Monitoring Search Panel Data"
        Public Sub GetMonitoringSearchPanelData(ByRef conBO As ContractorPortalBO, ByRef monitorDS As DataSet)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String

            '' This List will hold instances of type ContractorPortalBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim faultParam As ParameterBO

            '*************
            '**************

            faultParam = New ParameterBO("noOfRows", conBO.RowCount, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("offSet", conBO.PageIndex, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortColumn", conBO.SortBy, DbType.String)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortOrder", conBO.SortOrder, DbType.String)
            paramList.Add(faultParam)
            'Pass the organization/contracator id to SP
            faultParam = New ParameterBO("orgId", conBO.OrgId, DbType.Int32)
            paramList.Add(faultParam)

            'If user selected the Location then pass it to SP otherwise pass nothing
            If conBO.LocationId > 0 Then
                faultParam = New ParameterBO("locationId", conBO.LocationId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Area then pass it to SP otherwise pass nothing
            If conBO.AreaId > 0 Then
                faultParam = New ParameterBO("areaId", conBO.AreaId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Element then pass it to SP otherwise pass nothing
            If conBO.ElementId > 0 Then
                faultParam = New ParameterBO("elementId", conBO.ElementId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the team then pass it to SP otherwise pass nothing
            If conBO.TeamId > 0 Then
                faultParam = New ParameterBO("teamId", conBO.TeamId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the team then pass it to SP otherwise pass nothing
            If conBO.UserId > 0 Then
                faultParam = New ParameterBO("userId", conBO.UserId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Priority then pass it to SP otherwise pass nothing
            If conBO.PriorityId > 0 Then
                faultParam = New ParameterBO("priorityId", conBO.PriorityId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Status then pass it to SP otherwise pass nothing
            If conBO.StatusId > 0 Then
                faultParam = New ParameterBO("status", conBO.StatusId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Patch then pass it to SP otherwise pass nothing
            If conBO.PatchId > 0 Then
                faultParam = New ParameterBO("patch", conBO.PatchId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user selected the Scheme then pass it to SP otherwise pass nothing
            If conBO.SchemeId > 0 Then
                faultParam = New ParameterBO("scheme", conBO.SchemeId, DbType.Int32)
                paramList.Add(faultParam)
            End If

            'If user mentioned the Postcode then pass it to SP otherwise pass nothing
            If Not conBO.PostCode = String.Empty Then
                faultParam = New ParameterBO("postcode", conBO.PostCode, DbType.String)
                paramList.Add(faultParam)
            End If

            'If user selected the Due Date then pass it to SP otherwise pass nothing
            If conBO.Due <> String.Empty Then
                faultParam = New ParameterBO("due", conBO.Due, DbType.String)
                paramList.Add(faultParam)
            End If

            sprocName = SprocNameConstants.GetMonitoringSearchPanelData

            MyBase.LoadDataSet(monitorDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If monitorDS.Tables(0).Rows.Count = 0 Then

                conBO.IsFlagStatus = False
                conBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                conBO.IsFlagStatus = True
            End If
        End Sub
#End Region

        '#Region "Get Monitoring Time Base Data"
        '        Public Sub GetMonitoringTimeBaseData(ByRef conBO As ContractorPortalBO, ByRef monitorDS As DataSet, ByVal selectedDateOption As String)
        '            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
        '            Dim sprocName As String

        '            '' This List will hold instances of type ContractorPortalBO
        '            Dim paramList As ParameterList = New ParameterList()
        '            'checking if this are the query is from search button or not
        '            Dim faultParam As ParameterBO

        '            '' if search is based on username tenancyid date nature etc then we will get
        '            '' these values from  EnquiryLogSearchBO object

        '            'Pass the organization/contracator id to SP
        '            faultParam = New ParameterBO("orgId", conBO.OrgId, DbType.Int32)
        '            paramList.Add(faultParam)

        '            'Pass the selected date option id to SP
        '            faultParam = New ParameterBO("selectedDateOption", selectedDateOption, DbType.String)
        '            paramList.Add(faultParam)

        '            sprocName = SprocNameConstants.GetMonitoringTimeBaseData

        '            MyBase.LoadDataSet(monitorDS, paramList, sprocName)

        '            '' If no record found means no account/rent statement exist and we'll display user msg
        '            If monitorDS.Tables(0).Rows.Count = 0 Then

        '                conBO.IsFlagStatus = False
        '                conBO.UserMsg = UserInfoMsgConstants.RecordNotExist
        '            Else
        '                conBO.IsFlagStatus = True
        '            End If
        '        End Sub
        '#End Region

#Region "Get Monitoring TimeBase Fault Count"
        Public Sub GetMonitoringTimeBaseFaultCount(ByRef conBO As ContractorPortalBO, ByRef conArrayList As ArrayList)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String

            '' This List will hold instances of type ContractorPortalBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            Dim faultParam As ParameterBO

            'Pass the organization/contracator id to SP
            faultParam = New ParameterBO("orgId", conBO.OrgId, DbType.Int32)
            inParamList.Add(faultParam)
            faultParam = New ParameterBO("allJobsCount", Nothing, DbType.Int32)
            outParamList.Add(faultParam)
            faultParam = New ParameterBO("overDueJobsCount", Nothing, DbType.Int32)
            outParamList.Add(faultParam)
            faultParam = New ParameterBO("sevenDaysJobsCount", Nothing, DbType.Int32)
            outParamList.Add(faultParam)
            faultParam = New ParameterBO("oneFourDaysJobsCount", Nothing, DbType.Int32)
            outParamList.Add(faultParam)
            faultParam = New ParameterBO("twoOneDaysJobsCount", Nothing, DbType.Int32)
            outParamList.Add(faultParam)
            faultParam = New ParameterBO("twoEightDaysJobsCount", Nothing, DbType.Int32)
            outParamList.Add(faultParam)


            sprocName = SprocNameConstants.GetMonitoringTimeBaseFaultCount

            outParamList = MyBase.SelectRecord(inParamList, outParamList, sprocName)

            Dim counter As Integer
            For counter = 0 To outParamList.Count() - 1
                conArrayList.Add(outParamList.Item(counter).Value)
            Next
        End Sub
#End Region

#End Region

        '-------------------------------------------------------------------------------------------------------------

#Region "Contractor Protal - Fault Reports - Coded By Noor Muhammad - Feburary,2009 "

#Region "Get Fault Reports"

        'Public Sub GetFaultReports(ByRef faultRepBO As FaultReportsBO, ByVal faultRepDS As DataSet)
        '    '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
        '    Dim sprocName As String

        '    '' This List will hold instances of type ContractorPortalBO
        '    Dim paramList As ParameterList = New ParameterList()
        '    'checking if this are the query is from search button or not
        '    Dim faultParam As ParameterBO

        '    '' PageSize, pageindex and sortby will be common to both type of searches
        '    faultParam = New ParameterBO("noOfRows", faultRepBO.PageSize, DbType.Int32)
        '    paramList.Add(faultParam)
        '    faultParam = New ParameterBO("offSet", faultRepBO.PageIndex, DbType.Int32)
        '    paramList.Add(faultParam)
        '    faultParam = New ParameterBO("sortColumn", faultRepBO.SortBy, DbType.String)
        '    paramList.Add(faultParam)
        '    faultParam = New ParameterBO("sortOrder", faultRepBO.SortOrder, DbType.String)
        '    paramList.Add(faultParam)

        '    'Pass the organization/contracator id to SP
        '    faultParam = New ParameterBO("orgId", faultRepBO.OrgId, DbType.Int32)
        '    paramList.Add(faultParam)

        '    'Pass the fault status to SP
        '    faultParam = New ParameterBO("faultStatus", faultRepBO.FaultStatus, DbType.Int32)
        '    paramList.Add(faultParam)

        '    sprocName = SprocNameConstants.GetFaultReports

        '    MyBase.LoadDataSet(faultRepDS, paramList, sprocName)

        '    '' If no record found means no account/rent statement exist and we'll display user msg
        '    If faultRepDS.Tables(0).Rows.Count = 0 Then

        '        faultRepBO.IsFlagStatus = False
        '        faultRepBO.UserMsg = UserInfoMsgConstants.RecordNotExist
        '    Else
        '        faultRepBO.IsFlagStatus = True
        '    End If
        'End Sub
#End Region

#Region "Get Fault Reports Count"

        'Public Function GetFaultReportsCount(ByRef faultRepBO As FaultReportsBO)
        '    '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
        '    Dim sprocName As String
        '    Dim rowCount As Integer = 0

        '    '' This List will hold instances of type ContractorPortalBO
        '    Dim paramList As ParameterList = New ParameterList()
        '    'checking if this are the query is from search button or not
        '    Dim faultParam As ParameterBO

        '    'Pass the organization/contracator id to SP
        '    faultParam = New ParameterBO("orgId", faultRepBO.OrgId, DbType.Int32)
        '    paramList.Add(faultParam)

        '    'Pass the fault status to SP
        '    faultParam = New ParameterBO("faultStatus", faultRepBO.FaultStatus, DbType.Int32)
        '    paramList.Add(faultParam)

        '    sprocName = SprocNameConstants.GetFaultReportsCount

        '    Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

        '    If myReader.Read Then
        '        rowCount = Me.GetRowCount(myReader)
        '    End If

        '    Return rowCount
        'End Function

#End Region
#End Region

        '-------------------------------------------------------------------------------------------------------------

#Region "Contractor Portal - PreInspection Report & Reactive Repairs- Coded By Tahir Gul - Feburary,2009"

#Region "Get Pre Inspection DATA "

        Public Sub GetPreInspectionDATA(ByRef prINDS As DataSet, ByRef PreIBO As PreInspectionBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type ReportedFaultSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim preInspectionParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            preInspectionParam = New ParameterBO("noOfRows", PreIBO.RowCount, DbType.Int32)
            paramList.Add(preInspectionParam)
            preInspectionParam = New ParameterBO("offSet", PreIBO.PageIndex, DbType.Int32)
            paramList.Add(preInspectionParam)
            preInspectionParam = New ParameterBO("sortColumn", PreIBO.SortBy, DbType.String)
            paramList.Add(preInspectionParam)
            preInspectionParam = New ParameterBO("sortOrder", PreIBO.SortOrder, DbType.String)
            paramList.Add(preInspectionParam)
            preInspectionParam = New ParameterBO("UserId", PreIBO.UserId, DbType.Int16)
            paramList.Add(preInspectionParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object
            sprocName = SprocNameConstants.GetPreInspectionReport

            MyBase.LoadDataSet(prINDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If prINDS.Tables(0).Rows.Count = 0 Then

                PreIBO.IsFlagStatus = False
                PreIBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                PreIBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "Get Pre Inspection Row Count "

        Public Function GetPreInspectionRowCount(ByRef prInspBO As PreInspectionBO)

            Dim sprocName As String = SprocNameConstants.GetPreInspectionReportRowCount
            '' This List will hold instances of type PreInspectionBO
            Dim rowCount As Integer = 0
            Dim paramList As ParameterList = New ParameterList()

            Dim preInspectionParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            preInspectionParam = New ParameterBO("noOfRows", prInspBO.RowCount, DbType.Int32)
            paramList.Add(preInspectionParam)
            preInspectionParam = New ParameterBO("offSet", prInspBO.PageIndex, DbType.Int32)
            paramList.Add(preInspectionParam)
            preInspectionParam = New ParameterBO("sortColumn", prInspBO.SortBy, DbType.String)
            paramList.Add(preInspectionParam)
            preInspectionParam = New ParameterBO("sortOrder", prInspBO.SortOrder, DbType.String)
            paramList.Add(preInspectionParam)
            preInspectionParam = New ParameterBO("UserId", prInspBO.UserId, DbType.Int16)
            paramList.Add(preInspectionParam)

            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If
            Return rowCount

        End Function
#End Region

#Region "Contractor Portal -- Reactive Repair DATA"

#Region "Get Reactive Repair Data "

        Public Sub GetReactiveRepairDate(ByRef reactiveDS As DataSet, ByVal customerId As Integer, ByVal tenancyId As Integer)

            Dim sprocName As String = SprocNameConstants.GetReactiveRepairDATA

            Try
                Dim paramList As ParameterList = New ParameterList()
                Dim paramBO As ParameterBO
                paramBO = New ParameterBO("customerId", customerId, DbType.Int32)
                paramList.Add(paramBO)
                paramBO = New ParameterBO("tenancyId", tenancyId, DbType.Int32)
                paramList.Add(paramBO)
                MyBase.LoadDataSet(reactiveDS, paramList, sprocName)

                'MyBase.LoadDataSetReactiveRepair(reactiveDS, Nothing, sprocName)


            Catch ex As Exception

            End Try

        End Sub
#End Region

#Region "Get Reactive Repair Nature "

        Public Sub GetReactiveRepairNature(ByVal faultLogID As Integer, ByRef reactiveDS As DataSet)

            Dim sprocName As String = SprocNameConstants.ReactiveRepairNatureGrid

            Try
                Dim paramList As ParameterList = New ParameterList()
                Dim paramBO As ParameterBO
                paramBO = New ParameterBO("faultLogID", faultLogID, DbType.Int32)
                paramList.Add(paramBO)

                'MyBase.LoadDataSet(reactiveDS, paramList, sprocName)
                MyBase.LoadDataSetNature(reactiveDS, paramList, sprocName)

                'MyBase.LoadDataSetReactiveRepair(reactiveDS, Nothing, sprocName)

            Catch ex As Exception

            End Try

        End Sub
#End Region

#Region "Contractor Portal-- Reactive Repair Journal  DATA"

        Public Function GetReactiveRepairJournalDate(ByRef reactiveDS As DataSet, ByVal id As Integer, ByVal reactInsBO As PreInspectionBO) As Boolean

            Dim sprocName As String = SprocNameConstants.GetReactiveRepairJournalDATA
            Dim result
            Dim outputValue As Boolean

            Try
                Dim inParamJournalBO As ParameterBO
                Dim outParamBO As ParameterBO

                Dim InParamJournalList As ParameterList = New ParameterList()
                Dim outParamJournalList As ParameterList = New ParameterList()

                inParamJournalBO = New ParameterBO("FaultLogId", id, DbType.Int32)
                InParamJournalList.Add(inParamJournalBO)

                inParamJournalBO = New ParameterBO("sortColumn", reactInsBO.SortBy, DbType.String)
                InParamJournalList.Add(inParamJournalBO)

                inParamJournalBO = New ParameterBO("sortOrder", reactInsBO.SortOrder, DbType.String)
                InParamJournalList.Add(inParamJournalBO)


                outParamBO = New ParameterBO("result", Nothing, DbType.Int32)

                outParamJournalList.Add(outParamBO)

                'MyBase.LoadDataSet(reactiveDS, InParamJournalList, sprocName)

                MyBase.LoadDataSet(reactiveDS, InParamJournalList, outParamJournalList, sprocName)

                result = outParamJournalList.Item(0).Value()
                If result = -1 Then
                    outputValue = False
                Else
                    outputValue = True
                End If

            Catch ex As Exception

            End Try

            Return outputValue

        End Function
#End Region

#Region "Get Property Reactive Repair Data "

        Public Sub GetPropertyReactiveRepairData(ByRef reactiveDS As DataSet, ByVal propertyId As String)

            Dim sprocName As String = SprocNameConstants.GetPropertyReactiveRepairData
            Dim paramList As ParameterList = New ParameterList()
            Dim paramBO As ParameterBO
            paramBO = New ParameterBO("propertyId", propertyId, DbType.String)
            paramList.Add(paramBO)

            MyBase.LoadDataSet(reactiveDS, paramList, sprocName)

           

        End Sub
#End Region

#End Region



#End Region

#Region "Contractor Portal - PostInspection Report - Coded By Usman Sharif - Feburary,2009"

#Region "Get PostInpsection Data"
        Public Sub GetPostInspectionDATA(ByRef postDS As DataSet, ByRef postBO As PostInspectionBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type ReportedFaultSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim postInspectionParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            postInspectionParam = New ParameterBO("noOfRows", postBO.RowCount, DbType.Int32)
            paramList.Add(postInspectionParam)
            postInspectionParam = New ParameterBO("offSet", postBO.PageIndex, DbType.Int32)
            paramList.Add(postInspectionParam)
            postInspectionParam = New ParameterBO("sortColumn", postBO.SortBy, DbType.String)
            paramList.Add(postInspectionParam)
            postInspectionParam = New ParameterBO("sortOrder", postBO.SortOrder, DbType.String)
            paramList.Add(postInspectionParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  PostInpectionBOSearchBO object
            sprocName = SprocNameConstants.GetPostInspectionReport

            MyBase.LoadDataSet(postDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If postDS.Tables(0).Rows.Count = 0 Then

                postBO.IsFlagStatus = False
                postBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                postBO.IsFlagStatus = True
            End If

        End Sub
#End Region

#Region "Get PostInspection Row Count"
        Public Function GetPostInspectionRowCount(ByRef postBO As PostInspectionBO)

            Dim sprocName As String = SprocNameConstants.GetPostInspectionReportRowCount
            '' This List will hold instances of type PreInspectionBO
            Dim rowCount As Integer = 0
            Dim paramList As ParameterList = New ParameterList()

            Dim postInspectionParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            postInspectionParam = New ParameterBO("noOfRows", postBO.RowCount, DbType.Int32)
            paramList.Add(postInspectionParam)
            postInspectionParam = New ParameterBO("offSet", postBO.PageIndex, DbType.Int32)
            paramList.Add(postInspectionParam)
            postInspectionParam = New ParameterBO("sortColumn", postBO.SortBy, DbType.String)
            paramList.Add(postInspectionParam)
            postInspectionParam = New ParameterBO("sortOrder", postBO.SortOrder, DbType.String)
            paramList.Add(postInspectionParam)
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then
                rowCount = Me.GetRowCount(myReader)
            End If
            Return rowCount

        End Function
#End Region


#Region "Save PostInspection Record"

        Public Sub SavePostInspectionRecord(ByRef piBO As PreInspectBO)


            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SprocNameConstants.AddPostInspectionRecord
            '' This List will hold instances of type PreInspectBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not

            Dim userId As ParameterBO = New ParameterBO("USERID", piBO.UserId, DbType.Int32)
            inParamList.Add(userId)

            Dim inspectionDate As ParameterBO = New ParameterBO("INSPECTIONDATE", piBO.InspectionDate, DbType.DateTime)
            inParamList.Add(inspectionDate)

            Dim time As ParameterBO = New ParameterBO("time", piBO.Time, DbType.String)
            inParamList.Add(time)


            Dim netCost As ParameterBO = New ParameterBO("NETCOST", piBO.NetCost, DbType.Double)
            inParamList.Add(netCost)

            If piBO.DueDate <> Nothing Then
                Dim dueDate As ParameterBO = New ParameterBO("DUEDATE", piBO.DueDate, DbType.DateTime)
                inParamList.Add(dueDate)
            End If

            Dim notes As ParameterBO = New ParameterBO("NOTES", piBO.Notes, DbType.String)
            inParamList.Add(notes)

            If (piBO.Approved <> -1) Then

                Dim approved As ParameterBO = New ParameterBO("APPROVED", piBO.Approved, DbType.Int32)
                inParamList.Add(approved)

            Else

                Dim approved As ParameterBO = New ParameterBO("APPROVED", Nothing, DbType.Int32)
                inParamList.Add(approved)

            End If

            Dim recharge As ParameterBO = New ParameterBO("Recharge", piBO.Recharge, DbType.Boolean)
            inParamList.Add(recharge)

            Dim tempFaultId As ParameterBO = New ParameterBO("FAULTlOGID", piBO.TempFaultId, DbType.Int32)
            inParamList.Add(tempFaultId)

            Dim repairId As ParameterBO = New ParameterBO("RepairId", piBO.RepairId, DbType.Int32)
            inParamList.Add(repairId)

            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            '' Passing Array of piBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the PREINSPECTONID in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                piBO.IsFlagStatus = False
            Else
                piBO.IsFlagStatus = True
            End If



        End Sub


        Public Sub SavePostInspectionRecordUpdate(ByRef piBO As PreInspectBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            'Dim sprocName As String = SprocNameConstants.AddPostInspectionRecord
            Dim sprocName As String = SprocNameConstants.UpdatePostInspectionPopUp
            '' This List will hold instances of type PreInspectBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not

            Dim userId As ParameterBO = New ParameterBO("USERID", piBO.UserId, DbType.Int32)
            inParamList.Add(userId)

            Dim inspectionDate As ParameterBO = New ParameterBO("INSPECTIONDATE", piBO.InspectionDate, DbType.DateTime)
            inParamList.Add(inspectionDate)

            Dim time As ParameterBO = New ParameterBO("time", piBO.Time, DbType.String)
            inParamList.Add(time)


            Dim netCost As ParameterBO = New ParameterBO("NETCOST", piBO.NetCost, DbType.Double)
            inParamList.Add(netCost)

            If piBO.DueDate <> Nothing Then
                Dim dueDate As ParameterBO = New ParameterBO("DUEDATE", piBO.DueDate, DbType.DateTime)
                inParamList.Add(dueDate)
            End If

            Dim notes As ParameterBO = New ParameterBO("NOTES", piBO.Notes, DbType.String)
            inParamList.Add(notes)

            Dim approved As ParameterBO = New ParameterBO("APPROVED", piBO.Approved, DbType.Int32)
            inParamList.Add(approved)

            Dim reason As ParameterBO = New ParameterBO("reason", piBO.Reason, DbType.String)
            inParamList.Add(reason)

            Dim tempFaultId As ParameterBO = New ParameterBO("FAULTlOGID", piBO.TempFaultId, DbType.Int32)
            inParamList.Add(tempFaultId)

            Dim repairId As ParameterBO = New ParameterBO("RepairId", piBO.RepairId, DbType.Int32)
            inParamList.Add(repairId)

            Dim recharge As ParameterBO = New ParameterBO("Recharge", piBO.Recharge, DbType.Boolean)
            inParamList.Add(recharge)

            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            '' Passing Array of piBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ' ''Out parameter will return the PREINSPECTONID in case of success or -1 otherwise
            'Dim qryResult As Integer = outParamList.Item(0).Value

            ' ''In the case of invalid information provided
            'If qryResult = -1 Then
            '    piBO.IsFlagStatus = False
            'Else
            '    piBO.IsFlagStatus = True
            'End If

        End Sub

#End Region


#End Region

#Region "Cancelled Job Report - Coded By Usman Sharif - April 2009"

#Region "Get Cancelled Job Search Data"

        Public Sub UpdateCancelReportGrid(ByVal faultLogId As Integer, ByVal reason As String, ByVal userId As Integer)

            Dim sprocName As String = SprocNameConstants.UpdateCancelReportGrid
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()
            Dim cancelParamBO As ParameterBO

            Dim paramBO() As ParameterBO = {New ParameterBO("faultLogID", faultLogId, DbType.Int32), New ParameterBO("reason", reason, DbType.String), New ParameterBO("userId", userId, DbType.Int32)}

            'If faultLogId <> 0 Then
            '    cancelParamBO = New ParameterBO("faultLogID", faultLogId, DbType.Int32)
            '    inParamList.Add(cancelParamBO)
            'End If

            'If reason <> "" Then

            '    cancelParamBO = New ParameterBO("reason", reason, DbType.String)
            '    inParamList.Add(cancelParamBO)

            'End If
            inParamList.AddRange(paramBO)

            cancelParamBO = New ParameterBO("result", Nothing, DbType.Int32)

            outParamList.Add(cancelParamBO)

            MyBase.SaveRecord(inParamList, outParamList, sprocName)

        End Sub
        Public Sub GetCancelledJobSearchData(ByRef cancelledDS As DataSet, ByRef cancelledBO As CancelledJobBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim cancelledParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            cancelledParam = New ParameterBO("noOfRows", cancelledBO.RowCount, DbType.Int32)
            paramList.Add(cancelledParam)
            cancelledParam = New ParameterBO("offSet", cancelledBO.PageIndex, DbType.Int32)
            paramList.Add(cancelledParam)
            cancelledParam = New ParameterBO("sortColumn", cancelledBO.SortBy, DbType.String)
            paramList.Add(cancelledParam)
            cancelledParam = New ParameterBO("sortOrder", cancelledBO.SortOrder, DbType.String)
            paramList.Add(cancelledParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object
            If cancelledBO.IsSearch = True Then

                If cancelledBO.FirstName <> "" Then
                    cancelledParam = New ParameterBO("firstName", cancelledBO.FirstName, DbType.String)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("firstName", Nothing, DbType.String)
                    paramList.Add(cancelledParam)
                End If

                If cancelledBO.LastName <> "" Then
                    cancelledParam = New ParameterBO("lastName", cancelledBO.LastName, DbType.String)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("lastName", Nothing, DbType.String)
                    paramList.Add(cancelledParam)
                End If

                If cancelledBO.TenancyId <> -1 And cancelledBO.TenancyId <> "-1" And cancelledBO.TenancyId <> "0" Then
                    cancelledParam = New ParameterBO("tenancyId", CType(cancelledBO.TenancyId, Double), DbType.Double)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("tenancyId", Nothing, DbType.Double)
                    paramList.Add(cancelledParam)
                End If

                If cancelledBO.Status <> "" And cancelledBO.Status <> "-1" Then
                    cancelledParam = New ParameterBO("status", CType(cancelledBO.Status, Int32), DbType.Int32)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("status", Nothing, DbType.Int32)
                    paramList.Add(cancelledParam)
                End If

                If cancelledBO.Text <> "" Then
                    cancelledParam = New ParameterBO("text", CType(cancelledBO.Text, String), DbType.String)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("text", Nothing, DbType.Int32)
                    paramList.Add(cancelledParam)
                End If
                If cancelledBO.DueDate <> "" Then
                    cancelledParam = New ParameterBO("date", CType(cancelledBO.DueDate, String), DbType.AnsiString)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("date", Nothing, DbType.String)
                    paramList.Add(cancelledParam)
                End If

                sprocName = SprocNameConstants.GetCancelledJobReportData
            Else
                sprocName = SprocNameConstants.GetCancelledJobReportData
            End If

            MyBase.LoadDataSet(cancelledDS, paramList, sprocName)

            '' If no record found means no account/rent statement exist and we'll display user msg
            If cancelledDS.Tables(0).Rows.Count = 0 Then

                cancelledBO.IsFlagStatus = False
                cancelledBO.UserMsg = UserInfoMsgConstants.RecordNotExist
            Else
                cancelledBO.IsFlagStatus = True
            End If
        End Sub
#End Region

#Region "Get Cancelled Job Serach Row Count "
        Public Function GetCancelledJobSearchRowCount(ByRef cancelledBO As CancelledJobBO) As Integer
            Dim sprocName As String
            Dim rowCount As Integer = 0

            sprocName = SprocNameConstants.GetCancelledJobReportRowCount
            Dim paramList As ParameterList = New ParameterList()

            Dim cancelledParam As ParameterBO

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object
            If cancelledBO.IsSearch = True Then

                If cancelledBO.FirstName <> "" Then
                    cancelledParam = New ParameterBO("firstName", cancelledBO.FirstName, DbType.String)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("firstName", Nothing, DbType.String)
                    paramList.Add(cancelledParam)
                End If

                If cancelledBO.LastName <> "" Then
                    cancelledParam = New ParameterBO("lastName", cancelledBO.LastName, DbType.String)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("lastName", Nothing, DbType.String)
                    paramList.Add(cancelledParam)
                End If

                If cancelledBO.TenancyId <> -1 And cancelledBO.TenancyId <> "-1" And cancelledBO.TenancyId <> "0" Then
                    cancelledParam = New ParameterBO("tenancyId", CType(cancelledBO.TenancyId, Double), DbType.Double)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("tenancyId", Nothing, DbType.Double)
                    paramList.Add(cancelledParam)
                End If

                If cancelledBO.Status <> "" And cancelledBO.Status <> "-1" Then
                    cancelledParam = New ParameterBO("status", CType(cancelledBO.Status, Int32), DbType.Int32)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("status", Nothing, DbType.Int32)
                    paramList.Add(cancelledParam)
                End If

                If cancelledBO.Text <> "" Then
                    cancelledParam = New ParameterBO("text", CType(cancelledBO.Text, Integer), DbType.Int32)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("text", Nothing, DbType.Int32)
                    paramList.Add(cancelledParam)
                End If
                If cancelledBO.DueDate <> "" Then
                    cancelledParam = New ParameterBO("date", CType(cancelledBO.DueDate, String), DbType.AnsiString)
                    paramList.Add(cancelledParam)
                Else
                    cancelledParam = New ParameterBO("date", Nothing, DbType.String)
                    paramList.Add(cancelledParam)
                End If
            End If

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.GetRowCount(myReader)

            End If

            Return rowCount

        End Function
#End Region

#End Region

    End Class
End Namespace

