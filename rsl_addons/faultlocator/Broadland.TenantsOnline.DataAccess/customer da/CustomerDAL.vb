'' Class Name -- CustomerDAL
'' Base Class -- BaseDAL
'' Summary --    Contains Functionality related to Customer/Tenant
'' Methods --    AuthenticateCustomer
'' Created By  -- Muanwar Nadeem

''---------------------------------
Imports System

Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Imports Broadland.TenantsOnline.BusinessObject
Imports Broadland.TenantsOnline.Utilities

Namespace Broadland.TenantsOnline.DataAccess

    Public Class CustomerDAL : Inherits BaseDAL

#Region "Functions"

#Region "GetCustomerInfo"

        Public Function GetCustomerInfo(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailsInfo As Boolean) As CustomerDetailsBO

            Dim customerAddressBO As CustomerAddressBO = New CustomerAddressBO()
            custDetailsBO.Address = customerAddressBO



            ''Get customer Info as Name and address
            GetCustomerDetailsInfo(custDetailsBO, getDetailsInfo)

            ''Get customer Account Balance to be displayed on welcome screen account section
            GetCustomerAccountBalance(custDetailsBO)

            ''Get customer Gas Servicing appointment date to be displayed on welcome screen
            GasServicingAppointmentDate(custDetailsBO)

            custDetailsBO.IsFlagStatus = True

            Return custDetailsBO

        End Function

        Public Sub GetCustomerDetailsInfo(ByRef custDetailsBO As CustomerDetailsBO, ByVal getDetailsInfo As Boolean)


            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerInfo

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' ParameterBO for userName
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", custDetailsBO.Id, DbType.Int16)

            '' Adding object to paramList
            inParamList.Add(customerIdParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ''Set BO attributes by reading from data reader
            If getDetailsInfo Then

                ''Get Customer information of first record returned
                If (myDataReader.Read) Then
                    Me.FillCustomerDetailsBODetailed(myDataReader, custDetailsBO)
                End If

            Else

                ''Get Customer information of first record returned
                If (myDataReader.Read) Then
                    Me.FillCustomerDetailsBOBasic(myDataReader, custDetailsBO)
                End If

            End If

            custDetailsBO.IsFlagStatus = True

        End Sub

#Region "PrivateUtilityMethod"

        ''service method with takes a datareader and parse it for
        ''values to be set for customerDetailsObject
        Private Sub FillCustomerDetailsBOBasic(ByRef myDataReader As IDataReader, ByRef custDetailsBO As CustomerDetailsBO)


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerFirstName")) Then
                custDetailsBO.FirstName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerFirstName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerLastName")) Then
                custDetailsBO.LastName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerLastName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("DOB")) Then
                custDetailsBO.DateOfBirth = myDataReader.GetDateTime(myDataReader.GetOrdinal("DOB"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Email")) Then
                custDetailsBO.Address.Email = myDataReader.GetString(myDataReader.GetOrdinal("Email"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Telephone")) Then
                custDetailsBO.Address.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("Telephone"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("mobile")) Then
                custDetailsBO.Address.TelephoneMobile = myDataReader.GetString(myDataReader.GetOrdinal("mobile"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
                custDetailsBO.Address.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
                custDetailsBO.Address.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
                custDetailsBO.Address.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
                custDetailsBO.Address.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
                custDetailsBO.Address.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("County")) Then
                custDetailsBO.Address.Coutnty = myDataReader.GetString(myDataReader.GetOrdinal("County"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Postcode")) Then
                custDetailsBO.Address.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("Postcode"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Gender")) Then
                custDetailsBO.Gender = myDataReader.GetString(myDataReader.GetOrdinal("Gender"))
            End If



        End Sub

        ''service method with takes a datareader and parse it for
        ''values to be set for customerDetailsObject
        Private Sub FillCustomerDetailsBODetailed(ByRef myDataReader As IDataReader, ByRef custDetailsBO As CustomerDetailsBO)

            ''fill the BO for basic attributes
            FillCustomerDetailsBOBasic(myDataReader, custDetailsBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TitleVal")) Then
                custDetailsBO.TitleValue = myDataReader.GetInt32(myDataReader.GetOrdinal("TitleVal"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerFirstName")) Then
                custDetailsBO.FirstName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerFirstName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerLastName")) Then
                custDetailsBO.LastName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerLastName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("DOB")) Then
                custDetailsBO.DateOfBirth = myDataReader.GetDateTime(myDataReader.GetOrdinal("DOB"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Email")) Then
                custDetailsBO.Address.Email = myDataReader.GetString(myDataReader.GetOrdinal("Email"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Telephone")) Then
                custDetailsBO.Address.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("Telephone"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("mobile")) Then
                custDetailsBO.Address.TelephoneMobile = myDataReader.GetString(myDataReader.GetOrdinal("mobile"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
                custDetailsBO.Address.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
                custDetailsBO.Address.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
                custDetailsBO.Address.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
                custDetailsBO.Address.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
                custDetailsBO.Address.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("County")) Then
                custDetailsBO.Address.Coutnty = myDataReader.GetString(myDataReader.GetOrdinal("County"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Postcode")) Then
                custDetailsBO.Address.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("Postcode"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Gender")) Then
                custDetailsBO.Gender = myDataReader.GetString(myDataReader.GetOrdinal("Gender"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NINumber")) Then
                custDetailsBO.NINumber = myDataReader.GetString(myDataReader.GetOrdinal("NINumber"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Occupation")) Then
                custDetailsBO.Occupation = myDataReader.GetString(myDataReader.GetOrdinal("Occupation"))
            End If



            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TakeHomePay")) Then
                custDetailsBO.TakeHomePay = myDataReader.GetString(myDataReader.GetOrdinal("TakeHomePay"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MaritalStatus")) Then
                custDetailsBO.MaritalStatus = myDataReader.GetString(myDataReader.GetOrdinal("MaritalStatus"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Ethnicity")) Then
                custDetailsBO.Ethnicity = myDataReader.GetString(myDataReader.GetOrdinal("Ethnicity"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Religion")) Then
                custDetailsBO.Religion = myDataReader.GetString(myDataReader.GetOrdinal("Religion"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("SexualOrientation")) Then
                custDetailsBO.SexualOrientation = myDataReader.GetString(myDataReader.GetOrdinal("SexualOrientation"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("FirstLanguage")) Then
                custDetailsBO.MainLanguage = myDataReader.GetString(myDataReader.GetOrdinal("FirstLanguage"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Communication")) Then
                custDetailsBO.Communication = myDataReader.GetString(myDataReader.GetOrdinal("Communication"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PreferedCommunicationStyle")) Then
                custDetailsBO.PreferCommunicationStyle = myDataReader.GetString(myDataReader.GetOrdinal("PreferedCommunicationStyle"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Disability")) Then
                custDetailsBO.Disability = myDataReader.GetString(myDataReader.GetOrdinal("Disability"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NoOfOccupantsBelow18")) Then
                custDetailsBO.NoOfOccupantsBelow18 = myDataReader.GetInt32(myDataReader.GetOrdinal("NoOfOccupantsBelow18"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NoOfOccupantsAbove18")) Then
                custDetailsBO.NoOfOccupantsAbove18 = myDataReader.GetInt32(myDataReader.GetOrdinal("NoOfOccupantsAbove18"))
            End If


        End Sub


#End Region

#End Region

#Region "GetCustomerDetail Funciton for Fault Locator"

        Public Sub GetCustomerContactDetail(ByRef custDetailsBO As CustomerDetailsBO)


            '' NOTE:-
            ''       We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerContactDetail

            '' 
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()

            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' ParameterBO for userName
            Dim customerIdParam As ParameterBO = New ParameterBO("CustomerId", custDetailsBO.Id, DbType.Int16)

            '' Adding object to paramList
            inParamList.Add(customerIdParam)

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            ''Get Customer information of first record returned
            If (myDataReader.Read) Then
                Me.FillCustomerContactDetails(myDataReader, custDetailsBO)
            End If

            custDetailsBO.IsFlagStatus = True

        End Sub

#End Region

#End Region

#Region "Methods"

        Private Sub GetCustomerAccountBalance(ByRef custDetailsBO As CustomerDetailsBO)
            Dim sprocName As String = SprocNameConstants.GetCustomerAccountBalance

            ''Instantiate the input parameters list
            Dim inParamList As ParameterList = New ParameterList

            ''Instantiate the ParameterBO wich will hold the parameter info
            Dim tenancyIdParam As ParameterBO = New ParameterBO("TenancyId", custDetailsBO.Tenancy, DbType.String)

            ''Add input parameters to parameter list
            inParamList.Add(tenancyIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If (myDataReader.Read) Then
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("AccountBalance")) Then
                    custDetailsBO.AccountBalance = myDataReader.GetDecimal(myDataReader.GetOrdinal("AccountBalance"))
                End If
            End If


        End Sub

        Private Sub GasServicingAppointmentDate(ByRef custDetailsBO As CustomerDetailsBO)

            Dim sprocName As String = SprocNameConstants.GetServicingAppointmentDate

            ''Instantiate the input parameters list
            Dim inParamList As ParameterList = New ParameterList

            ''Instantiate the ParameterBO wich will hold the parameter info
            Dim tenancyIdParam As ParameterBO = New ParameterBO("TenancyId", custDetailsBO.Tenancy, DbType.String)

            ''Add input parameters to parameter list
            inParamList.Add(tenancyIdParam)

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If (myDataReader.Read) Then
                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("GasServicingAppointmentDate")) Then
                    custDetailsBO.GasServicingAppointmentDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("GasServicingAppointmentDate"))
                Else
                    custDetailsBO.GasServicingAppointmentDate = Nothing
                End If
            End If

        End Sub

#Region "GetTerminationInfo"
        Public Sub GetTerminationInfo(ByRef custAddressBO As CustomerAddressBO, ByRef terminBO As TerminationBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressTermination

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", terminBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillTerminationBO(myDataReader, terminBO)

                '' TO DO: GetCustomerAddress method is already exists. Using Polymorphism, need to move
                '' this code on
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)

                terminBO.IsFlagStatus = True
            Else
                terminBO.IsFlagStatus = False
            End If


        End Sub
        Private Sub FillTerminationBO(ByRef myDataReader As IDataReader, ByRef terminBO As TerminationBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                terminBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("MovingOutDate")) Then
                terminBO.MovingOutDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("MovingOutDate"))
            End If

        End Sub
#End Region

#Region "GetComplaintInfo"
        Public Sub GetComplaintInfo(ByRef custAddressBO As CustomerAddressBO, ByRef cmpltBO As ComplaintBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressComplaint

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", cmpltBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillComplaintBO(myDataReader, cmpltBO)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                cmpltBO.IsFlagStatus = True
            Else
                cmpltBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetASBInfo"
        Public Sub GetASBInfo(ByRef custAddressBO As CustomerAddressBO, ByRef abBO As AsbBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressASB

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", abBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillASBBO(myDataReader, abBO)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                abBO.IsFlagStatus = True
            Else
                abBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "Get GarageParking Info"
        Public Sub GetGarageParkingInfo(ByRef custAddressBO As CustomerAddressBO, ByRef gpBO As GarageParkingBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressGarageParking

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", gpBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillGarageParkingBO(myDataReader, gpBO)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                gpBO.IsFlagStatus = True
            Else
                gpBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetAbandonInfo"
        Public Sub GetAbandonInfo(ByRef custAddressBO As CustomerAddressBO, ByRef abndBO As AbandonBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressAbandon

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", abndBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillAbandonBO(myDataReader, abndBO)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                abndBO.IsFlagStatus = True
            Else
                abndBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetTransferInfo"
        Public Sub GetTransferInfo(ByRef custAddressBO As CustomerAddressBO, ByRef houseTrans As HouseMoveBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCustomerAddressTransfer

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", houseTrans.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillHouseMoveBO(myDataReader, houseTrans)
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)
                houseTrans.IsFlagStatus = True
            Else
                houseTrans.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetCstRequestInfo"
        Public Sub GetCstRequestInfo(ByRef custAddressBO As CustomerAddressBO, ByRef cstReqBO As CstBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetCstRequestInfo

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", cstReqBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                    cstReqBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NatureDescription")) Then
                    cstReqBO.RequestNature = myDataReader.GetString(myDataReader.GetOrdinal("NatureDescription"))
                End If

                cstReqBO.IsFlagStatus = True
            Else
                cstReqBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetClearDirectDebitRentInfo"
        Public Sub GetClearDirectDebitRentInfo(ByRef custAddressBO As CustomerAddressBO, ByRef enqBO As EnquiryLogBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetClearArrearInfo

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", enqBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                    enqBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
                End If
                enqBO.IsFlagStatus = True
            Else
                enqBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "GetClearArrearInfo"
        Public Sub GetClearArrearInfo(ByRef custAddressBO As CustomerAddressBO, ByRef enqBO As EnquiryLogBO)
            '' NOTE:-
            '' We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.GetClearArrearInfo

            ''
            '' This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()


            '' Creating ParameterBO objects and passing prameters Name, Value and Type

            '' Create TerminationBO parameters
            Dim enquiryLogID As ParameterBO = New ParameterBO("enqLogID", enqBO.EnquiryLogId, DbType.Int32)

            '' Adding object to paramList
            inParamList.Add(enquiryLogID)



            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, sprocName)

            If myDataReader.Read Then
                Me.FillCustomerAddressBO(myDataReader, custAddressBO)

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                    enqBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
                End If
                enqBO.IsFlagStatus = True
            Else
                enqBO.IsFlagStatus = False
            End If


        End Sub
#End Region

#Region "Fill Complaint BO"
        Private Sub FillComplaintBO(ByRef myDataReader As IDataReader, ByRef cmpltBO As ComplaintBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                cmpltBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CategoryText")) Then
                cmpltBO.CategoryText = myDataReader.GetString(myDataReader.GetOrdinal("CategoryText"))
            End If

        End Sub
#End Region

#Region "Fill ASB BO"
        Private Sub FillASBBO(ByRef myDataReader As IDataReader, ByRef abBO As AsbBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                abBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CategoryText")) Then
                abBO.CategoryText = myDataReader.GetString(myDataReader.GetOrdinal("CategoryText"))
            End If

        End Sub
#End Region

#Region "Fill GarageParking BO"
        Private Sub FillGarageParkingBO(ByRef myDataReader As IDataReader, ByRef gpBO As GarageParkingBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                gpBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NameText")) Then
                gpBO.LookUpValueText = myDataReader.GetString(myDataReader.GetOrdinal("NameText"))
            End If

        End Sub
#End Region

#Region "FillAbandonBO"
        Private Sub FillAbandonBO(ByRef myDataReader As IDataReader, ByRef abndBO As AbandonBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                abndBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NameText")) Then
                abndBO.LookUpValueText = myDataReader.GetString(myDataReader.GetOrdinal("NameText"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LOCATION")) Then
                abndBO.Location = myDataReader.GetString(myDataReader.GetOrdinal("LOCATION"))
            End If

        End Sub
#End Region

#Region "FillHouseMoveBO"
        Private Sub FillHouseMoveBO(ByRef myDataReader As IDataReader, ByRef houseTransBO As HouseMoveBO)

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                houseTransBO.Description = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LocalAuthority")) Then
                houseTransBO.LocalAuthority = myDataReader.GetString(myDataReader.GetOrdinal("LocalAuthority"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Development")) Then
                houseTransBO.Development = myDataReader.GetString(myDataReader.GetOrdinal("Development"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("NOOFBEDROOMS")) Then
                houseTransBO.NoOfBedrooms = myDataReader.GetInt32(myDataReader.GetOrdinal("NOOFBEDROOMS"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("OCCUPANTSNOGREATER18")) Then
                houseTransBO.OccupantsNoGreater18 = myDataReader.GetInt32(myDataReader.GetOrdinal("OCCUPANTSNOGREATER18"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("OCCUPANTSNOBELOW18")) Then
                houseTransBO.OccupantsNoBelow18 = myDataReader.GetInt32(myDataReader.GetOrdinal("OCCUPANTSNOBELOW18"))

            End If

        End Sub

#End Region

#Region "Fill Customer Address BO for enquiry log"
        Private Sub FillCustomerAddressBO(ByRef myDataReader As IDataReader, ByRef custAddressBO As CustomerAddressBO)
            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
                custAddressBO.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
                custAddressBO.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
                custAddressBO.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
                custAddressBO.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
                custAddressBO.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PostCode")) Then
                custAddressBO.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("PostCode"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("COUNTY")) Then
                custAddressBO.Coutnty = myDataReader.GetString(myDataReader.GetOrdinal("COUNTY"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TEL")) Then
                custAddressBO.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("TEL"))
            End If
        End Sub
#End Region

        ''service method with takes a datareader and parse it for
        ''values to be set for customerDetailsObject
        Private Sub FillCustomerContactDetails(ByRef myDataReader As IDataReader, ByRef custDetailsBO As CustomerDetailsBO)


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerFirstName")) Then
                custDetailsBO.FirstName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerFirstName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerMiddleName")) Then
                custDetailsBO.MiddleName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerMiddleName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("CustomerLastName")) Then
                custDetailsBO.LastName = myDataReader.GetString(myDataReader.GetOrdinal("CustomerLastName"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Email")) Then
                custDetailsBO.Address.Email = myDataReader.GetString(myDataReader.GetOrdinal("Email"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Telephone")) Then
                custDetailsBO.Address.Telephone = myDataReader.GetString(myDataReader.GetOrdinal("Telephone"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("mobile")) Then
                custDetailsBO.Address.TelephoneMobile = myDataReader.GetString(myDataReader.GetOrdinal("mobile"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("HouseNumber")) Then
                custDetailsBO.Address.HouseNumber = myDataReader.GetString(myDataReader.GetOrdinal("HouseNumber"))
            End If


            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")) Then
                custDetailsBO.Address.Address1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")) Then
                custDetailsBO.Address.Address2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Address3")) Then
                custDetailsBO.Address.Address3 = myDataReader.GetString(myDataReader.GetOrdinal("Address3"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TownCity")) Then
                custDetailsBO.Address.TownCity = myDataReader.GetString(myDataReader.GetOrdinal("TownCity"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("County")) Then
                custDetailsBO.Address.Coutnty = myDataReader.GetString(myDataReader.GetOrdinal("County"))
            End If

            If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Postcode")) Then
                custDetailsBO.Address.PostCode = myDataReader.GetString(myDataReader.GetOrdinal("Postcode"))
            End If

        End Sub

#Region "SaveCustomerContactDetail"

        Public Sub UpdateCustomerContactDetail(ByRef cusBO As CustomerBO)

            'NOTE:-
            'We'll send back exception to BLL and will set flags + log exceptions there

            Dim sprocName As String = SprocNameConstants.UpdateCustomerContactDetail


            'This List will hold instances of type ParameterBO
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            'Creating ParameterBO objects and passing prameters Name, Value and Type

            'Saving all this in  parameter bo
            Dim customerId As ParameterBO = New ParameterBO("CustomerId", cusBO.Id, DbType.String)
            Dim firstName As ParameterBO = New ParameterBO("FirstName", cusBO.FirstName, DbType.String)
            Dim middleName As ParameterBO = New ParameterBO("MiddleName", cusBO.MiddleName, DbType.String)
            Dim lastName As ParameterBO = New ParameterBO("LastName", cusBO.LastName, DbType.String)
            Dim houseNumber As ParameterBO = New ParameterBO("HouseNumber", cusBO.Address.HouseNumber, DbType.String)
            Dim address1 As ParameterBO = New ParameterBO("Address1", cusBO.Address.Address1, DbType.String)
            Dim address2 As ParameterBO = New ParameterBO("Address2", cusBO.Address.Address2, DbType.String)
            Dim address3 As ParameterBO = New ParameterBO("Address3", cusBO.Address.Address3, DbType.String)
            Dim townCity As ParameterBO = New ParameterBO("TownCity", cusBO.Address.TownCity, DbType.String)
            Dim county As ParameterBO = New ParameterBO("County", cusBO.Address.Coutnty, DbType.String)
            Dim postCode As ParameterBO = New ParameterBO("PostCode", cusBO.Address.PostCode, DbType.String)
            Dim telephone As ParameterBO = New ParameterBO("Telephone", cusBO.Address.Telephone, DbType.String)
            Dim telephoneMobile As ParameterBO = New ParameterBO("TelephoneMobile", cusBO.Address.TelephoneMobile, DbType.String)
            Dim email As ParameterBO = New ParameterBO("Email", cusBO.Address.Email, DbType.String)
            Dim custId As ParameterBO = New ParameterBO("CustId", Nothing, DbType.String)


            'Adding object to paramList
            inParamList.Add(customerId)
            inParamList.Add(firstName)
            inParamList.Add(middleName)
            inParamList.Add(lastName)
            inParamList.Add(houseNumber)
            inParamList.Add(address1)
            inParamList.Add(address2)
            inParamList.Add(address3)
            inParamList.Add(townCity)
            inParamList.Add(county)
            inParamList.Add(postCode)
            inParamList.Add(telephone)
            inParamList.Add(telephoneMobile)
            inParamList.Add(email)
            outParamList.Add(custId)


            'Passing In parameter list, out parameter list and sproc-name to base class method: SaveRecord
            Dim rowCount As Integer = MyBase.SaveRecord(inParamList, outParamList, sprocName)

            'If Row-count is greater than 0, means email successfully changed
            If rowCount > 0 Then

                'customerID output parameter is stored in paramlist at zero(0) index
                'Retrieving value of customerID output parameter after execution
                cusBO.Id = outParamList(0).Value
                cusBO.IsFlagStatus = True

            End If

        End Sub

#End Region

#End Region

    End Class

End Namespace

