USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[RD_GETFAULTSTATUS]    Script Date: 08/21/2013 19:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC [dbo].[RD_GETFAULTSTATUS]
-- Author:		<Ahmed Mehmood>
-- Create date: <24/7/2013>
-- Description:	<Returns todays appointments of an operative>
-- =============================================
CREATE PROCEDURE [dbo].[RD_GETFAULTSTATUS]

AS
	SELECT FaultStatusID AS id,Description AS val
	FROM FL_FAULT_STATUS 
	WHERE FL_FAULT_STATUS.ACTIVE =1
	














