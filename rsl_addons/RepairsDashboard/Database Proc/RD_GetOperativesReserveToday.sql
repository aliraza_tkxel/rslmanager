USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[RD_GetOperativesReserveToday]    Script Date: 08/21/2013 19:55:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC RD_GetOperativesReserveToday
-- Author:		<Ahmed Mehmood>
-- Create date: <24/7/2013>
-- Description:	<Returns todays appointments of an operative>
-- =============================================
ALTER PROCEDURE [dbo].[RD_GetOperativesReserveToday] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from

	SET NOCOUNT ON;

	SELECT DISTINCT E__EMPLOYEE.EMPLOYEEID EmployeeId , E__EMPLOYEE.FIRSTNAME +' '+ E__EMPLOYEE.LASTNAME  Name	
	FROM	FL_CO_APPOINTMENT 
			INNER JOIN E__EMPLOYEE ON FL_CO_APPOINTMENT.OperativeID = E__EMPLOYEE.EMPLOYEEID        		
    WHERE	CONVERT(VARCHAR(10),FL_CO_APPOINTMENT.AppointmentDate,110) = CONVERT(VARCHAR(10),GETDATE(),110)     		
       		 	
END


