﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ReportsArea
    
    '''<summary>
    '''updPanelReportArea control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPanelReportArea As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblReportHeading control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblReportHeading As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''pnlSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearch As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''txtSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearch As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TextBoxWatermarkExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender1 As Global.AjaxControlToolkit.TextBoxWatermarkExtender
    
    '''<summary>
    '''btnGO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGO As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlPriorityControls control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPriorityControls As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''ddlScheme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlScheme As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFrom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''calFromDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calFromDate As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''txtFromDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TextBoxWatermarkExtender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender2 As Global.AjaxControlToolkit.TextBoxWatermarkExtender
    
    '''<summary>
    '''lblTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''calToDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calToDate As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''txtToDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtToDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TextBoxWatermarkExtender3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender3 As Global.AjaxControlToolkit.TextBoxWatermarkExtender
    
    '''<summary>
    '''txtSearchPriorityList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchPriorityList As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''TextBoxWatermarkExtender4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBoxWatermarkExtender4 As Global.AjaxControlToolkit.TextBoxWatermarkExtender
    
    '''<summary>
    '''btnGenerate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGenerate As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlDropDown control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDropDown As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''ddlStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStatus As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlReports control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlReports As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''CompleteFaultListControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CompleteFaultListControl As Global.RepairsDashboard.CompleteFaultListControl
    
    '''<summary>
    '''InProgressFaultList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents InProgressFaultList As Global.RepairsDashboard.InProgressFaultsListControl
    
    '''<summary>
    '''FaultOverdueListControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FaultOverdueListControl As Global.RepairsDashboard.FaultOverdueListControl
    
    '''<summary>
    '''EmergencyFaultsListControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents EmergencyFaultsListControl As Global.RepairsDashboard.EmergencyFaultsListControl
    
    '''<summary>
    '''ReportedFaultsListControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ReportedFaultsListControl As Global.RepairsDashboard.ReportedFaultsListControl
    
    '''<summary>
    '''PriorityAndCompletionListControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PriorityAndCompletionListControl As Global.RepairsDashboard.PriorityAndCompletionListControl
    
    '''<summary>
    '''FaultsToBeArrangedControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FaultsToBeArrangedControl As Global.RepairsDashboard.FaultsToBeArrangedListControl
End Class
