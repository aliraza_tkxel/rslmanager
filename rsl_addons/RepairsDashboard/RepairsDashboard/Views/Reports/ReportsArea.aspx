﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/RDMasterPage.Master"
    CodeBehind="ReportsArea.aspx.vb" Inherits="RepairsDashboard.ReportsArea" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/Reports/CompleteFaultListControl.ascx" TagName="CompleteFaultListControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Reports/InProgressFaultsListControl.ascx" TagName="InProgressFaultList"
    TagPrefix="uc3" %>
<%@ Register Src="~/Controls/Reports/FaultOverdueListControl.ascx" TagName="FaultOverdueListControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/Controls/Reports/EmergencyFaultsListControl.ascx" TagName="EmergencyFaultsListControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/Controls/Reports/ReportedFaultsListControl.ascx" TagName="ReportedFaultsListControl"
    TagPrefix="uc6" %>
<%@ Register Src="~/Controls/Reports/PriorityAndCompletionListControl.ascx" TagName="PriorityAndCompletionListControl"
    TagPrefix="uc7" %>
<%@ Register Src="~/Controls/Reports/FaultsToBeArrangedListControl.ascx" TagName="FaultsToBeArrangedControl"
    TagPrefix="uc8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/GridViewCustomPager.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <style type="text/css">
        .style1
        {
            width: 152px;
        }
        
        
        .searchTextDefault
        {
            color: Gray;
            font-style: italic;
        }
        
        .wideControl
        {
            padding: 5px;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
           width: 150px !important;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            //if ($("#<%= txtSearch.ClientID %>").val()) {
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }

        function PrintJobSheet() {
            javascript: window.open('../Faults/PrintSubContractorJobSheetSummaryUpdate.aspx?JobSheetNumber=' + document.getElementById("lblJSN").innerHTML);
            return false;
        }

        $(document).ready(function () {
            var ddlStatusElement = document.getElementById("ContentPlaceHolder1_ddlStatus");
            if (ddlStatusElement == null) {
                $("#divStatus").hide()
            }
            var dllReportsElement = document.getElementById("ContentPlaceHolder1_ddlReports");
            if (dllReportsElement == null) {
                $("#divReports").hide()
            }
        });
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelReportArea" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <div class="portlet">
                <div class="header pnlHeading">
                    <asp:Label ID="lblReportHeading" CssClass="header-label" Text="Reported Faults Report" runat="server" Font-Bold="true" />
                    <div class="field right">
                        <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;" Visible="True">
                            <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3px 10px !important; margin:-4px 10px 0 0;" AutoCompleteType="Search" 
                                class="searchbox styleselect-control searchbox right" onkeyup="TypingInterval();" PlaceHolder="Search User" runat="server">
                            </asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText">
                            </ajaxToolkit:TextBoxWatermarkExtender>
                            <asp:Button runat="server" ID="btnGO" UseSubmitBehavior="false" Text="GO" 
                                CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0; position: absolute; margin: 38px 0px 0 -51px;" />
                        </asp:Panel>
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                    <div style="overflow:auto;">
                        <asp:Panel ID="pnlPriorityControls" runat="server" HorizontalAlign="Right" Visible="true">
                            <%-- Scheme --%>
                            <div class="form-control">
                                <div class="select_div">
                                    <div class="">
                                        <asp:DropDownList ID="ddlScheme" runat="server" AutoPostBack="False">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <%-- From Date --%>
                            <div class="form-control">
                                <div class="text_div">
                                    <div class="label">
                                        <asp:Label ID="lblFrom" runat= "server" Text ="From:" />
                                    </div>
                                    <div class="field">
                                        <asp:CalendarExtender ID="calFromDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                            PopupButtonID="imgCalDate" PopupPosition="TopLeft" TargetControlID="txtFromDate"
                                            TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                            TargetControlID="txtFromDate" WatermarkText=" " WatermarkCssClass="searchTextDefault">
                                        </ajaxToolkit:TextBoxWatermarkExtender>
                                    </div>
                                </div>
                            </div>
                            <%-- To Date --%>
                            <div class="form-control">
                                <div class="text_div">
                                    <div class="label">
                                        <asp:Label ID="lblTo" runat= "server" Text ="To:" />
                                    </div>
                                    <div class="field">
                                        <asp:CalendarExtender ID="calToDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                            PopupButtonID="imgCalDate" PopupPosition="TopLeft" TargetControlID="txtToDate"
                                            TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:TextBox ID="txtToDate" runat="server" ></asp:TextBox>
                                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                                            TargetControlID="txtToDate" WatermarkText=" " WatermarkCssClass="searchTextDefault">
                                        </ajaxToolkit:TextBoxWatermarkExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="form-control right">
                                <div class="field right">
                                    <%-- Search Text --%>
                                    <asp:TextBox ID="txtSearchPriorityList" AutoPostBack="false" style="padding:3px 10px !important; position: absolute; margin: -31px 0px 0 -86px;" AutoCompleteType="Search" 
                                        class="searchbox styleselect-control searchbox right" PlaceHolder="Quick find" runat="server">
                                    </asp:TextBox>
                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server"
                                        TargetControlID="txtSearchPriorityList" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText wideControl">
                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                    <%-- Search Button --%>
                                    <asp:Button runat="server" ID="btnGenerate" UseSubmitBehavior="false" Text="Generate" 
                                        CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: 10px 10px 0 0; " />
                                </div>
                            </div>
                            
                        </asp:Panel>
                        <asp:Panel ID="pnlDropDown" runat="server" style="float:left">
                            
                            
                        </asp:Panel>
                    </div>
                    <div class="form-control" id="divStatus">
                        <div class="select_div">
                            <div class="">
                                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" Visible="False">
                                    <asp:ListItem Value="-1">Please select status</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-control right" id="divReports">
                        <div class="select_div">
                            <div class="">
                                <asp:DropDownList ID="ddlReports" runat="server" AutoPostBack="True" Visible="False">
                                    <asp:ListItem Value="0">In Progress</asp:ListItem>
                                    <asp:ListItem Value="1">Emergency Faults</asp:ListItem>
                                    <asp:ListItem Value="2">Faults Overdue Report</asp:ListItem>
                                    <asp:ListItem Value="3">Completed Faults</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div style="">
                        <uc2:CompleteFaultListControl ID="CompleteFaultListControl" runat="server" Visible="False" />
                        <uc3:InProgressFaultList ID="InProgressFaultList" runat="server" Visible="False" />
                        <uc4:FaultOverdueListControl ID="FaultOverdueListControl" runat="server" Visible="false" />
                        <uc5:EmergencyFaultsListControl ID="EmergencyFaultsListControl" runat="server" Visible="false" />
                        <uc6:ReportedFaultsListControl ID="ReportedFaultsListControl" runat="server" Visible="True" />
                        <uc7:PriorityAndCompletionListControl ID="PriorityAndCompletionListControl" runat="server"
                            Visible="false" />
                             <uc8:FaultsToBeArrangedControl ID="FaultsToBeArrangedControl" runat="server"
                            Visible="false" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
