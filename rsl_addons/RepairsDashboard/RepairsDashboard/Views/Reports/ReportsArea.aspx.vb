﻿Imports RD_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports RD_BusinessLogic
Imports RD_BusinessObject

Public Class ReportsArea
    Inherits PageBase

#Region "Properties"

#End Region

#Region "Events Handling"

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                ViewState.Add("QStringValue", Request.QueryString(PathConstants.Report))
                populateSchemeDropdownList()
                changeReport()
            Else
                If Request.QueryString(PathConstants.Report) <> ViewState.Item("QStringValue") Then
                    ddlStatus.SelectedIndex = 0
                    txtSearch.Text = ""
                End If
                ViewState("QStringValue") = Request.QueryString(PathConstants.Report)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Text Field Search Text Changed Event"

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged
        Try
            searchResults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            updPanelReportArea.Update()
        End Try
    End Sub

#End Region

#Region "Dropdown List Reports Selected Index Changed Event"

    Protected Sub ddlReports_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlReports.SelectedIndexChanged

        Try

            changeReport()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            updPanelReportArea.Update()
        End Try

    End Sub

#End Region

#Region "Dropdown List Status Selected Index Changed Event"

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged

        Try
            changeStatus()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            updPanelReportArea.Update()
        End Try

    End Sub

#End Region

#Region "Generate button click event"
    ''' <summary>
    ''' Generate button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenerate.Click
        Try
            populatePriorityAndCompletionFaultsList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            updPanelReportArea.Update()
        End Try
    End Sub

#End Region

#Region "Filtered Data by Scheme And Block"
    Protected Sub btnGO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGO.Click _
                , ReportedFaultsListControl.FiltersChanged _
                , CompleteFaultListControl.FiltersChanged _
                , EmergencyFaultsListControl.FiltersChanged _
                , FaultOverdueListControl.FiltersChanged _
                , InProgressFaultList.FiltersChanged
        If Request.QueryString(PathConstants.Report) = "ip" Then
            populateInprogressFaultsList()
        ElseIf Request.QueryString(PathConstants.Report) = "em" Then
            pnlMessage.Visible = False
            ddlStatus.SelectedIndex = 0
            Dim resultDataSet As DataSet = New DataSet()
            EmergencyFaultsListControl.populateEmergencyFaultList(resultDataSet, txtSearch.Text, CInt(ddlStatus.SelectedItem.Value), True)
            If resultDataSet.Tables(0).Rows.Count = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FilterReportNotFound, True)
            End If

        ElseIf Request.QueryString(PathConstants.Report) = "rf" Then
            populateReportedFaultsList()
        ElseIf Request.QueryString(PathConstants.Report) = "od" Then
            populateFaultsOverdueList()
        ElseIf Request.QueryString(PathConstants.Report) = "cmp" Then
            populateCompleteFaultsList()
        End If

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Region to populate lists"

#Region "Populate Complete Faults List"
    ''' <summary>
    ''' Populate Complete Faults List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateCompleteFaultsList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.CompletedFaultsReport
        CompleteFaultListControl.Visible = True

        CompleteFaultListControl.populateCompletedFaultList(resultDataSet, search, True)
        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.CompleteDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Priority and Completion List"
    ''' <summary>
    ''' Populate Priority and Completion List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePriorityAndCompletionFaultsList()

        Dim resultDataSet As DataSet = New DataSet()
        pnlPriorityControls.Visible = True
        pnlSearch.Visible = False
        lblReportHeading.Text = ApplicationConstants.PriorityAndCompletionReport
        PriorityAndCompletionListControl.Visible = True

        Dim objPrioritySearchBO As PrioritySearchBO = New PrioritySearchBO()
        objPrioritySearchBO.SchemeId = Convert.ToInt32(ddlScheme.SelectedValue)
        objPrioritySearchBO.FromDate = txtFromDate.Text
        objPrioritySearchBO.ToDate = txtToDate.Text
        objPrioritySearchBO.SearchText = txtSearchPriorityList.Text

        SessionManager.setPrioritySearchBO(objPrioritySearchBO)

        PriorityAndCompletionListControl.populatePriorityList(resultDataSet)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

    End Sub

#End Region

#Region "Populate Reported Faults List"
    ''' <summary>
    ''' Populate Reported Faults List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReportedFaultsList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.ReportedFaultsReport
        ReportedFaultsListControl.Visible = True

        ReportedFaultsListControl.populateReportedFaultList(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.ReportedDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Inprogress Faults List"
    ''' <summary>
    ''' Populate Inprogress Faults List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateInprogressFaultsList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.InProgressFaultsReport
        InProgressFaultList.populateInProgressFaultList(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If
        InProgressFaultList.Visible = True
        ViewState.Add(ViewStateConstants.InProgressDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Faults to be arrange List"
    Sub populateFaultsToBeArrangeList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.FaultsToBeArrangeReport
        btnGO.Visible = False
        FaultsToBeArrangedControl.Visible = True

        FaultsToBeArrangedControl.populateFaultToBeArrangeList(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.FaultToBeArrangeDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Faults Overdue List"
    ''' <summary>
    ''' Populate Complete Faults List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateFaultsOverdueList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.OverdueFaultsReport
        FaultOverdueListControl.Visible = True

        FaultOverdueListControl.populateFaultOverdueFaultList(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.FaultsOverdueDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Emergency Faults List"

    Sub populateEmergencyFaultsList()

        Dim search As String = txtSearch.Text
        Dim statusId As Integer = -1
        lblReportHeading.Text = ApplicationConstants.EmergencyFaultsReport
        ddlStatus.Visible = True
        EmergencyFaultsListControl.Visible = True

        Dim resultDataSet As DataSet = New DataSet()
        EmergencyFaultsListControl.populateEmergencyFaultList(resultDataSet, search, statusId, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.EmergencyDataSet, resultDataSet)

    End Sub

#End Region

#End Region

#Region "Setup control properties"

    Sub setupControlProperties()
        txtFromDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
    End Sub

#End Region

#Region "Validate input fields"
    ''' <summary>
    ''' Validate input fields
    ''' </summary>
    ''' <remarks></remarks>
    Sub validateFields()

        Dim fromDate As String = txtFromDate.Text
        Dim toDate As String = txtToDate.Text

        If (Not String.IsNullOrEmpty(fromDate) And Not String.IsNullOrWhiteSpace(toDate)) Then

            If (Date.Compare(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate)) < 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidDate, True)
            End If

        End If

    End Sub

#End Region

#Region "Search Results"

    Sub searchResults()
        Dim resultDataSet As DataSet = New DataSet()
        pnlMessage.Visible = False

        If Request.QueryString(PathConstants.Report) = "ip" Then
            InProgressFaultList.populateInProgressFaultList(resultDataSet, txtSearch.Text, True)
        ElseIf Request.QueryString(PathConstants.Report) = "em" Then
            ddlStatus.SelectedIndex = 0
            EmergencyFaultsListControl.populateEmergencyFaultList(resultDataSet, txtSearch.Text, CInt(ddlStatus.SelectedItem.Value), True)
        ElseIf Request.QueryString(PathConstants.Report) = "rf" Then
            ReportedFaultsListControl.populateReportedFaultList(resultDataSet, txtSearch.Text, True)
        ElseIf Request.QueryString(PathConstants.Report) = "od" Then
            FaultOverdueListControl.populateFaultOverdueFaultList(resultDataSet, txtSearch.Text, True)
        ElseIf Request.QueryString(PathConstants.Report) = "cmp" Then
            CompleteFaultListControl.populateCompletedFaultList(resultDataSet, txtSearch.Text, True)
        ElseIf Request.QueryString(PathConstants.Report) = "fa" Then
            lblReportHeading.Text = ApplicationConstants.FaultsToBeArrangeReport
            FaultsToBeArrangedControl.populateFaultToBeArrangeList(resultDataSet, txtSearch.Text, True)
            FaultsToBeArrangedControl.Visible = True
        End If

        If resultDataSet.Tables.Count > 0 And resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

    End Sub

#End Region

#Region "Change Report"

    Sub changeReport()

        'Hide All reports
        CompleteFaultListControl.Visible = False
        EmergencyFaultsListControl.Visible = False
        FaultOverdueListControl.Visible = False
        InProgressFaultList.Visible = False
        PriorityAndCompletionListControl.Visible = False
        ReportedFaultsListControl.Visible = False
        btnGO.Visible = True
        pnlMessage.Visible = False
        txtSearch.Text = ""
        ddlStatus.SelectedIndex = 0
        ddlStatus.Visible = False
        pnlSearch.Visible = True
        pnlPriorityControls.Visible = False

        If Request.QueryString(PathConstants.Report) = "ip" Then
            If Not IsPostBack Then
                InProgressFaultList.loadFinancialYears()
            End If
            populateInprogressFaultsList()
        ElseIf Request.QueryString(PathConstants.Report) = "rf" Then
            If Not IsPostBack Then
                ReportedFaultsListControl.loadFinancialYears()
            End If
            populateReportedFaultsList()
        ElseIf Request.QueryString(PathConstants.Report) = "fa" Then            
            populateFaultsToBeArrangeList()
        ElseIf Request.QueryString(PathConstants.Report) = "em" Then
            If Not IsPostBack Then
                EmergencyFaultsListControl.loadFinancialYears()
            End If            
            populateStatusList()
            populateEmergencyFaultsList()
        ElseIf Request.QueryString(PathConstants.Report) = "od" Then
            If Not IsPostBack Then
                FaultOverdueListControl.loadFinancialYears()
            End If
            populateFaultsOverdueList()
        ElseIf Request.QueryString(PathConstants.Report) = "cmp" Then
            If Not IsPostBack Then
                CompleteFaultListControl.loadFinancialYears()
            End If
            populateCompleteFaultsList()
        ElseIf Request.QueryString(PathConstants.Report) = "pc" Then
            populatePriorityAndCompletionFaultsList()
        End If

    End Sub

#End Region

#Region "Change Status"

    Sub changeStatus()
        Dim resultDataSet As DataSet = New DataSet()
        EmergencyFaultsListControl.populateEmergencyFaultList(resultDataSet, txtSearch.Text, CInt(ddlStatus.SelectedItem.Value), True)
    End Sub

#End Region

#Region "Populate Reports via Query string"
    Sub populateReports()

        If Request.QueryString(PathConstants.Report) = "ip" Then
            ddlReports.SelectedIndex = 0
        ElseIf Request.QueryString(PathConstants.Report) = "em" Then
            ddlReports.SelectedIndex = 1
        ElseIf Request.QueryString(PathConstants.Report) = "od" Then
            ddlReports.SelectedIndex = 2
        ElseIf Request.QueryString(PathConstants.Report) = "cmp" Then
            ddlReports.SelectedIndex = 3
        End If
        changeReport()

    End Sub


#End Region

#Region "Populate dropdowns"

#Region "Populate Emergency Fault Status List"

    Sub populateStatusList()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportsBl As ReportsBL = New ReportsBL()

        objReportsBl.getFaultStatusList(resultDataSet)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            ddlStatus.Enabled = False
        Else
            Dim ddlDataSet = resultDataSet.Tables(0)

            For i As Integer = 0 To ddlDataSet.Rows.Count - 1
                Dim listItem As New ListItem()
                listItem.Text = ddlDataSet.Rows(i).Item(1).ToString()
                listItem.Value = ddlDataSet.Rows(i).Item(0).ToString()
                ddlStatus.Items.Add(listItem)
            Next

        End If

    End Sub

#End Region

#Region "Populate scheme"
    ''' <summary>
    ''' Populate all schemes
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateSchemeDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportsBL As ReportsBL = New ReportsBL()
        objReportsBL.getSchemeList(resultDataSet)

        ddlScheme.DataSource = resultDataSet.Tables(0).DefaultView
        ddlScheme.DataValueField = "DevelopmentId"
        ddlScheme.DataTextField = "SchemeName"
        ddlScheme.DataBind()


        Dim item As ListItem = New ListItem("Select a Scheme", "-1")
        ddlScheme.Items.Insert(0, item)
    End Sub

#End Region

#Region "Populate Lookup"

    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values from given lookuplist.
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="lstLookup">LookUpList: LookUpList to Bind with dropdown list</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with lookup values in lookup list.</remarks>
    ''' 
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByRef lstLookup As List(Of LookUpBO))
        ddlLookup.Items.Clear()
        If (Not (lstLookup Is Nothing) AndAlso (lstLookup.Count > 0)) Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.DataBind()
        End If
    End Sub

#End Region

#End Region

#End Region

End Class