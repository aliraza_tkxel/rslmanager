﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="../../MasterPage/RDMasterPage.Master"
    CodeBehind="CompletedJobsheet.aspx.vb" Inherits="RepairsDashboard.CompletedJobsheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .itemPhoto
        {
            width: 167px;
            height: 160px;
            float: left;
            margin: 8px;
            font-size: small;
        }
        
        .modalPopupImage
        {
            background-color: #ffffdd;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
        }
        
        .modalBackground
        {
            background-color: White;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="updPnlCompleteJobsheet">
        <ContentTemplate>
            <div style="border-width: 1px; border-style: solid; border-color: Black;">
                <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
                <div style="height: auto; overflow: auto; clear: both; padding: 10px;">
                    <table cellpadding="5" cellspacing="5" id="jobsheet_detail_table" style="width: 100%;
                        margin-top: 10px;">
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lblRecallDetailsFor" runat="server" Font-Bold="true" Text="Job sheet summary for : "></asp:Label>
                                <asp:Label ID="lblClientNameHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
                                &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Font-Bold="true"
                                    Text=""></asp:Label>
                                &nbsp;,
                                <asp:Label ID="lblClientCityHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
                                &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Font-Bold="true" Text=" Tel :"> </asp:Label>
                                <asp:Label ID="lblClientTelPhoneNumberHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblFaultId" runat="server" ClientIDMode="Static" Font-Bold="true"></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Contractor :
                            </td>
                            <td>
                                <asp:Label ID="lblContractor" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Operative :
                            </td>
                            <td>
                                <asp:Label ID="lblOperativeName" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Priority :
                            </td>
                            <td>
                                <asp:Label ID="lblFaultPriority" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Completion Due :
                            </td>
                            <td>
                                <asp:Label ID="lblFaultCompletionDateTime" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Order Date :
                            </td>
                            <td>
                                <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                            </td>
                            <td>
                                Tel :
                                <asp:Label ID="lblClientTelPhoneNumber" runat="server" Font-Bold="true" Text=" "></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Location :
                            </td>
                            <td>
                                <asp:Label ID="lblFaultLocation" runat="server"></asp:Label>
                            </td>
                            <td>
                                Mobile:
                                <asp:Label ID="lblClientMobileNumber" runat="server" Font-Bold="true" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Description :
                            </td>
                            <td>
                                <asp:Label ID="lblFaultDescription" runat="server" Font-Bold="true" Text=""></asp:Label>
                            </td>
                            <td>
                                Email :
                                <asp:Label ID="lblClientEmailId" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Trade :
                            </td>
                            <td>
                                <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Note :
                            </td>
                            <td>
                                <asp:TextBox ID="lblFaultOperatorNote" runat="server" CssClass="roundcornerby5" Enabled="false"
                                    Height="65px" TextMode="MultiLine" Width="248px"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Repair Notes :
                            </td>
                            <td>
                                <asp:TextBox ID="txtBoxRepairNotes" runat="server" CssClass="roundcornerby5" Enabled="false"
                                    Height="65px" TextMode="MultiLine" Width="248px"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Repair details:</b>
                            </td>
                            <td>
                                <asp:GridView ID="grdRepairDetail" runat="server" AutoGenerateColumns="False" BorderColor="White"
                                    BorderStyle="None" ShowHeader="false" BorderWidth="0px" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRepairTime" runat="server" Font-Bold="True" Text='<%# Bind("CompletionDate","{0:d/MM/yyyy}") %>'></asp:Label>
                                                <b>:</b>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRepairDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                          
                            <td colspan="3">
                                <div style="max-height: 360px;margin-left:200px; overflow: auto;">
                                    <asp:ListView runat="server" OnItemDataBound="lstViewPhotos_ItemDatabound" ID="lstViewPhotos"
                                        GroupItemCount="4">
                                        <LayoutTemplate>
                                            <table id="groupPlaceholderContainer" runat="server" border="0" cellpadding="0" cellspacing="0"
                                                style="border-collapse: collapse; width: 100%;">
                                                <tr id="groupPlaceholder" runat="server">
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <GroupTemplate>
                                            <tr id="itemPlaceholderContainer" runat="server">
                                                <td id="itemPlaceholder" runat="server">
                                                </td>
                                            </tr>
                                        </GroupTemplate>
                                        <ItemTemplate>
                                            <div class="itemPhoto">
                                                <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                <asp:ImageButton ID="imgFaultImage" runat="server" AlternateText='<%# Eval("ImageName") %>'
                                                    Width="156px" Height="120px" CommandName="ShowImage" CommandArgument='<%# Eval("ImageName")%>'
                                                    Style="margin: 5px;" />
                                                <div style="float: left; font-size: 10px;">
                                                    &nbsp
                                                    <%# Eval("Status") %>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <EmptyItemTemplate>
                                        </EmptyItemTemplate>
                                    </asp:ListView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Follow On Work : </b>
                            </td>
                            <td>
                                <asp:TextBox ID="lblFollowOnWork" runat="server" CssClass="roundcornerby5" Enabled="false"
                                    Height="65px" TextMode="MultiLine" Width="248px"></asp:TextBox>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right;">
                                <asp:Button ID="btnBack" runat="server" Text=" < Back to Completed Faults Report" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
            </div>
            <asp:Label ID="lblPopUpShowImage" runat="server"></asp:Label>
            <ajaxToolkit:ModalPopupExtender ID="mdlPopUpViewImage" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblPopUpShowImage" PopupControlID="pnlLargeImage"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlLargeImage" runat="server" CssClass="modalPopupImage">
                <div style='width: 100%;'>
                    <asp:ImageButton ID="btnClose" runat="server" Style="position: absolute; top: -17px;
                        right: -16px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                    <asp:Image ID="imgDynamicImage" runat="server" CssClass="imgDynamicImage" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
