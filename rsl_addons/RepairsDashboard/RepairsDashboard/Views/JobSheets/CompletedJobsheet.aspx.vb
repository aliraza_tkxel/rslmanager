﻿Imports RD_BusinessLogic
Imports RD_BusinessObject
Imports RD_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class CompletedJobsheet
    Inherits PageBase

    Public uploadedImageUri As String

#Region "Events"

#Region "Page load Event"
    ''' <summary>
    ''' Page load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                Me.getSetQueryStringParams()
                If uiMessageHelper.IsError = False Then
                    Dim jobSheetNumber As String = SessionManager.getJobSheetNumber()
                    Me.populateJobSheetDetail(jobSheetNumber)
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Back button event"
    ''' <summary>
    ''' Back button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Response.Redirect(PathConstants.ReportsPath + "?" + PathConstants.Report + "=cmp")
    End Sub

#End Region

#Region "lstView Photos Item Command"
    ''' <summary>
    ''' lstView Photos Item Command
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lstViewPhotos.ItemCommand
        Try
            If e.CommandName = "ShowImage" Then
                Dim fileName As String = e.CommandArgument.ToString()
                imgDynamicImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId().ToString() + "/Images/" + fileName
                'mdlPopUpViewImage.X = 100
                'mdlPopUpViewImage.Y = 20
                mdlPopUpViewImage.Show()
            End If
        Catch ex As Exception
            ExceptionHelper.IsError = True
            ExceptionHelper.ExcpetionMessage = ex.Message
            If ExceptionHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If ExceptionHelper.IsError = True Then
                ExceptionHelper.setMessage(lblMessage, pnlMessage, ExceptionHelper.ExcpetionMessage, True)
            End If
        End Try
    End Sub
#End Region

#Region "lstView Photos Item data bound"
    ''' <summary>
    ''' lstView Photos Item data bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemDatabound(ByVal sender As Object, ByVal e As ListViewItemEventArgs) Handles lstViewPhotos.ItemDataBound
        Try

            If e.Item.ItemType = ListViewItemType.DataItem Then

                Dim imgFaultImage As Image = CType(e.Item.FindControl("imgFaultImage"), Image)
                Dim hdnImageName As HiddenField = CType(e.Item.FindControl("hdnImageName"), HiddenField)
                imgFaultImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId().ToString() + "/Images/" + hdnImageName.Value

            End If

        Catch ex As Exception
            ExceptionHelper.IsError = True
            ExceptionHelper.ExcpetionMessage = ex.Message
            If ExceptionHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If ExceptionHelper.IsError = True Then
                ExceptionHelper.setMessage(lblMessage, pnlMessage, ExceptionHelper.ExcpetionMessage, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"


#Region "Get Existed Images"
    ''' <summary>
    ''' Get Existed Images
    ''' </summary>
    ''' <param name="dtImages"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getExistedImages(ByVal dtImages As DataTable) As DataTable

        For Each dr As DataRowView In dtImages.DefaultView
            Dim imageName As String = dr("ImageName").ToString()
            Dim fullPath As String = Server.MapPath(GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId() + "/Images/") + imageName

            If Not System.IO.File.Exists(fullPath) Then
                dr.Row.Delete()
            End If
        Next

        dtImages.AcceptChanges()

        Return dtImages

    End Function

#End Region

#Region "Populate fault images"
    ''' <summary>
    ''' Populate fault images
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateFaultImages()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportsBl As ReportsBL = New ReportsBL()
        Dim jsn As String = lblFaultId.Text
        Dim propertyId As String = SessionManager.getPropertyId()

        objReportsBl.getCompletedFaultImages(resultDataSet, jsn)
        lstViewPhotos.DataSource = getExistedImages(resultDataSet.Tables(0))
        lstViewPhotos.DataBind()

    End Sub

#End Region

#Region "Get Complete Fault Detail"

    ''' <summary>
    ''' Get Complete Fault Detail 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populateJobSheetDetail(ByVal jsn As String)

        Try
            ' Reset Job sheet controls
            resetJobsheetControls()

            Dim resultDataSet As DataSet = New DataSet()
            Dim objReportsBl As ReportsBL = New ReportsBL()
            objReportsBl.getFollowOnWorkDetail(resultDataSet, jsn)

            ' Job Sheet Detail
            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)

            Else

                lblFaultId.Text = resultDataSet.Tables(0).Rows(0)(0).ToString
                lblContractor.Text = resultDataSet.Tables(0).Rows(0)(1).ToString
                lblOperativeName.Text = resultDataSet.Tables(0).Rows(0)(2).ToString
                lblFaultPriority.Text = resultDataSet.Tables(0).Rows(0)(3).ToString
                lblFaultCompletionDateTime.Text = resultDataSet.Tables(0).Rows(0)(4).ToString
                lblOrderDate.Text = resultDataSet.Tables(0).Rows(0)(5).ToString
                lblFaultLocation.Text = resultDataSet.Tables(0).Rows(0)(6).ToString
                lblFaultDescription.Text = resultDataSet.Tables(0).Rows(0)(7).ToString
                lblFaultOperatorNote.Text = resultDataSet.Tables(0).Rows(0)(8).ToString
                lblTrade.Text = resultDataSet.Tables(0).Rows(0)(12).ToString
                txtBoxRepairNotes.Text = resultDataSet.Tables(0).Rows(0)(14).ToString
                SessionManager.setPropertyId(resultDataSet.Tables(0).Rows(0)("PropertyId").ToString)
                populateFaultImages()

            End If

            ' Customer Info
            If (resultDataSet.Tables(1).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)

            Else

                ' Header Customer Labels
                lblClientNameHeader.Text = resultDataSet.Tables(1).Rows(0)(0).ToString
                lblClientStreetAddressHeader.Text = resultDataSet.Tables(1).Rows(0)(1).ToString
                lblClientCityHeader.Text = resultDataSet.Tables(1).Rows(0)(2).ToString
                lblClientTelPhoneNumberHeader.Text = resultDataSet.Tables(1).Rows(0)(5).ToString

                ' Body customer Labels

                lblClientName.Text = resultDataSet.Tables(1).Rows(0)(0).ToString
                lblClientStreetAddress.Text = resultDataSet.Tables(1).Rows(0)(1).ToString
                lblClientCity.Text = resultDataSet.Tables(1).Rows(0)(2).ToString
                lblClientPostCode.Text = resultDataSet.Tables(1).Rows(0)(3).ToString
                lblClientRegion.Text = resultDataSet.Tables(1).Rows(0)(4).ToString
                lblClientTelPhoneNumber.Text = resultDataSet.Tables(1).Rows(0)(5).ToString
                lblClientMobileNumber.Text = resultDataSet.Tables(1).Rows(0)(6).ToString
                lblClientEmailId.Text = resultDataSet.Tables(1).Rows(0)(7).ToString

            End If

            ' Repair Detail
            If (Not resultDataSet.Tables(2).Rows.Count = 0) Then
                grdRepairDetail.DataSource = resultDataSet.Tables(2)
                grdRepairDetail.DataBind()

            End If

            ' Follow On Work
            If (Not resultDataSet.Tables(3).Rows.Count = 0) Then
                lblFollowOnWork.Text = resultDataSet.Tables(3).Rows(0)(0).ToString
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Reset Job sheet control "
    ''' <summary>
    ''' Reset Job sheet control
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub resetJobsheetControls()

        ' Job Sheet Detail
        lblFaultId.Text = String.Empty
        lblContractor.Text = String.Empty
        lblOperativeName.Text = String.Empty
        lblFaultPriority.Text = String.Empty
        lblFaultCompletionDateTime.Text = String.Empty
        lblOrderDate.Text = String.Empty
        lblFaultLocation.Text = String.Empty
        lblFaultDescription.Text = String.Empty
        lblFaultOperatorNote.Text = String.Empty
        lblTrade.Text = String.Empty
        txtBoxRepairNotes.Text = String.Empty
        ' Header Customer Labels
        lblClientNameHeader.Text = String.Empty
        lblClientStreetAddressHeader.Text = String.Empty
        lblClientCityHeader.Text = String.Empty
        lblClientTelPhoneNumberHeader.Text = String.Empty

        ' Body customer Labels

        lblClientName.Text = String.Empty
        lblClientStreetAddress.Text = String.Empty
        lblClientCity.Text = String.Empty
        lblClientPostCode.Text = String.Empty
        lblClientRegion.Text = String.Empty
        lblClientTelPhoneNumber.Text = String.Empty
        lblClientMobileNumber.Text = String.Empty
        lblClientEmailId.Text = String.Empty


        grdRepairDetail.DataSource = Nothing

        lblFollowOnWork.Text = String.Empty

    End Sub

#End Region

#Region "get Set Query String Params"
    Private Sub getSetQueryStringParams()
        If Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
            SessionManager.setJobSheetNumber(Convert.ToString(Request.QueryString(PathConstants.Jsn)))

        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidJobSheetNumber
        End If
    End Sub
#End Region

#End Region
End Class
