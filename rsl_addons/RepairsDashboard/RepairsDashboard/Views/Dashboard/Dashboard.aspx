﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="../../MasterPage/RDMasterPage.Master"
    CodeBehind="Dashboard.aspx.vb" Inherits="RepairsDashboard.Dashboard" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/Dashboard/MapControl.ascx" TagName="MapControl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Dashboard/OperativeAppointmentsListControl.ascx" TagName="OperativeAppointmentsListControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/Controls/JobSheets/NoEntryJobSheet.ascx" TagName="NoEntryJobSheet"
    TagPrefix="uc3" %>
<asp:Content ID="content" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <link href="../../Styles/Dashboard.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(window).load(function () {

            loadCounts("getCountInProgress", '#loadingInProgress', '#<%= lblInProgress.ClientID %>')
            loadCounts("getCountNoEntries", '#loadingNoEntry', '#<%= lblNoEntries.ClientID %>')
            loadCounts("getCountFollowOnWork", '#loadingFollowOnWork', '#<%= lblFollowOnWork.ClientID %>')
            loadCounts("getCountFaultsOverDue", '#loadingFaultsOverDue', '#<%= lblFaultsOverDue.ClientID %>')

            loadCounts("getCountReportedFaults", '#loadingReported', '#<%= lblReportedFaults.ClientID %>')
            loadCounts("getCountCompletedFaults", '#loadingCompleted', '#<%= lblCompletedFaults.ClientID %>')
            loadCounts("getCountAssignToContractor", '#loadingContractor', '#<%= lblassignedToContractor.ClientID %>')
            loadCounts("getCountEmergencyfaults", '#loadingEmergency', '#<%= lblEmergencyFaults.ClientID %>')
            loadCounts("getCountRecallReported", '#loadingRecall', '#<%= lblRecallReported.ClientID %>')
        });
        function loadCounts(url, loading, label) {

            var postData = JSON.stringify({
        });
        $.ajax({
            type: "POST",
            url: "Dashboard.aspx/" + url,
            data: postData,
            contentType: "application/json",
            dataType: "json",
            beforeSend: function (xhr) {
                $(loading).show();
            },
            complete: function () {
                $(loading).hide();
            },

            success: function (result) {
                $(label).text(result.d);
            }
        });
    }
    function populateMarker() {

        var postData = JSON.stringify({
            operativeId: $('#<%= ddloperatives.ClientID %>').val()
        });
        $.ajax({
            type: "POST",
            url: "Dashboard.aspx/updateMarker",
            data: postData,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                var result = data.d;
                var items = [];

                $.each(result, function (i, item) {
                   
                    if (result.length == 1) {
                        items.push('[{"JSN": "' + item[0] + '","Address": "' + item[1] + '","Postcode": "' + item[2] + '","StartTime": "' + item[4] + '","TownCity": "' + item[7] + '","County": "' + item[8] + '","JSNGrouped": "' + item[9] + '"}]')
                    }
                    else if (i == 0 && result.length > 1) {
                        items.push('[{"JSN": "' + item[0] + '","Address": "' + item[1] + '","Postcode": "' + item[2] + '","StartTime": "' + item[4] + '","TownCity": "' + item[7] + '","County": "' + item[8] + '","JSNGrouped": "' + item[9] + '"}')
                    }
                    else if (i == result.length - 1 && result.length > 1) {
                        items.push('{"JSN": "' + item[0] + '","Address": "' + item[1] + '","Postcode": "' + item[2] + '","StartTime": "' + item[4] + '","TownCity": "' + item[7] + '","County": "' + item[8] + '","JSNGrouped": "' + item[9] + '"}]')
                    }

                    else {
                        items.push('{"JSN": "' + item[0] + '","Address": "' + item[1] + '","Postcode": "' + item[2] + '","StartTime": "' + item[4] + '","TownCity": "' + item[7] + '","County": "' + item[8] + '","JSNGrouped": "' + item[9] + '"}')
                    }
                });

                addMarkers(items);
                return true;
            }
        });
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlDashboardOuter" runat="server" UpdateMode="Conditional">
        <div class="wrapper">
            <div class="r_main_div" style="padding-left:15px;">
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
                <asp:UpdatePanel ID="updPanelCounter" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="left_part">
                            <div class=".outer-boxes-CallToAction" style="overflow:hidden;">
                                <div class="box">
                                    <div class="text">
                                        In
                                        <br />
                                        Progress:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblInProgress" runat="server" EnableViewState="false">&nbsp;  </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href="../Reports/ReportsArea.aspx?rpt=ip">
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingInProgress">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        No
                                        <br />
                                        Entry:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblNoEntries" runat="server" EnableViewState="False">&nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='<%= lnkNoEntry %>' >
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" /></a>
                                    </div>
                                    <div class="number-loading" id="loadingNoEntry">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Follow<br />
                                        On Works:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblFollowOnWork" runat="server"> &nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='<%= lnkFollowOnWork %>' >
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingFollowOnWork">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                            </div>
                             <div class=".outer-boxes-CallToAction" style="margin-top:9px; overflow:hidden;">
                                <div class="box-red">
                                    <div class="text">
                                        Faults<br />
                                        Overdue:
                                    </div>
                                    <div class="number-faults">
                                        <asp:Label ID="lblFaultsOverDue" runat="server"> &nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='../Reports/ReportsArea.aspx?rpt=od'>
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingFaultsOverDue">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Assigned to
                                        <br />
                                        Contractor:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblassignedToContractor" runat="server"> &nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='<%= lnkContractor %>' >
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingContractor">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Emergency
                                        <br />
                                        Faults:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblEmergencyFaults" runat="server"> &nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='../Reports/ReportsArea.aspx?rpt=em' >
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingEmergency">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                            </div>
                            <div class=".outer-boxes-CallToAction" style="margin-top:9px; overflow:hidden;">
                                <div class="box">
                                    <div class="text">
                                        Reported
                                        <br />
                                        Faults:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblReportedFaults" runat="server"> &nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='../Reports/ReportsArea.aspx?rpt=rf' >
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingReported">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Completed
                                        <br />
                                        Faults:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblCompletedFaults" runat="server"> &nbsp; </asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='../Reports/ReportsArea.aspx?rpt=cmp' >
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingCompleted">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                                <div class="box">
                                    <div class="text">
                                        Recalls
                                        <br />
                                        Reported:
                                    </div>
                                    <div class="number">
                                        <asp:Label ID="lblRecallReported" runat="server"> &nbsp;</asp:Label>
                                    </div>
                                    <div class="r-arrow">
                                        <a href='<%= lnkReCall %>' >
                                            <img src="../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                        </a>
                                    </div>
                                    <div class="number-loading" id="loadingRecall">
                                        <img alt="Please Wait" src="../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="right_Box">
                    <div class="box-left-b" style="margin-top:0; overflow:hidden" >
                        <asp:UpdatePanel ID="updPanelOperatives" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="boxborder-s-right">
                                    <div class="select_div-b">
                                        <asp:Label ID="lblOperative" runat="server" CssClass="label-b" Text="Operative : " style="color: white; line-height: 20px;padding-left:0;" Font-Bold="true"></asp:Label>
                                        <div class="field right" style="width:inherit">
                                            <asp:DropDownList class="styleselect" ID="ddloperatives"
                                                style="padding: 0 0 0 5px; border-radius: 0px; border: 1px solid #b1b1b1;
                                                height: 25px !important;
                                                font-size: 12px !important;
                                                width: 205px !important;" 
                                                runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div style="width: 100%; height: 400px; float: left; margin-bottom:2px;">
                            <uc1:MapControl ID="MapControl" runat="server" Visible="True" />
                        </div>
                        <asp:UpdatePanel ID="updPanelListControl" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="width: 100%;">
                                    <uc2:OperativeAppointmentsListControl ID="OperativeAppointmentsListControl" runat="server"
                                        Visible="True" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
