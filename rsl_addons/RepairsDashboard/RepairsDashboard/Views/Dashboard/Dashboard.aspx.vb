﻿Imports System.Data
Imports System.Data.SqlClient
Imports RD_BusinessLogic
Imports RD_Utilities
Imports RD_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading
Imports System.Configuration

Public Class Dashboard
    Inherits PageBase

#Region "Properties"

    Dim objDashboardBL As DashboardBL = New DashboardBL()
    Dim defaultPageSize As Integer = 1
    Dim countHashtable As Hashtable = New Hashtable()
    Public lnkNoEntry As String = getReportURI(PathConstants.NoEntryReportURL)
    Public lnkFollowOnWork As String = getReportURI(PathConstants.FollowOnWorksReportURL)
    Public lnkReCall As String = getReportURI(PathConstants.ReCallReportURL)
    Public lnkContractor As String = getReportURI(PathConstants.AssignToContractorReportURL)
    Public lnkFaultsOverDue As String = getReportURI(PathConstants.FaultsOverDueReportURL)
    Public lnkCompleted As String = getReportURI(PathConstants.CompletedReportURL)
    Public lnkEmergency As String = getReportURI(PathConstants.EmergencyReportURL)

#End Region

#Region "Events Handling"

#Region "Page Load"
    ''' <summary>
    ''' Event fires when page loads
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
        End If
    End Sub
#End Region

#Region "Page Load Complete"
    ''' <summary>
    ''' Event fires when page loading is completed.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Try
            'Dim lnkBtn As LinkButton = CType(Master.FindControl("lnkBtnAppDahsboard"), LinkButton)
            'lnkBtn.Style.Add(ApplicationConstants.ConWebBackgroundProperty, ApplicationConstants.ConWebBackgroundColor)

            If Not (IsPostBack) Then
                Me.populateOperativeDropdownList()
                Me.populateAppointmentsList()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region

#Region "Operative dropdown index changed"
    ''' <summary>
    ''' Event fires when dropdown value is changed from list operatives.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddloperatives_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddloperatives.SelectedIndexChanged

        Try
            ViewState.Add(ViewStateConstants.OperativeId, Convert.ToInt32(ddloperatives.SelectedValue))
            OperativeAppointmentsListControl.populateAppointmentsList(Convert.ToInt32(ddloperatives.SelectedValue))
            MapControl.populateMarkerData(ddloperatives.SelectedValue)
            updPanelListControl.Update()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            'updPanelOperativeAppointments.Update()
        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate Map with appointments of an operative"
    ''' <summary>
    ''' Populate Map with markers, where each marker is an appointment of an operative.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateMapAppointments()
        MapControl.populateMarkerData(ddloperatives.SelectedValue)        
    End Sub

#End Region

#Region "Populate appointments list of an operative"
    ''' <summary>
    ''' Populate appointments list of an operative
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateAppointmentsList()
        Me.OperativeAppointmentsListControl.populateAppointmentsList(Convert.ToInt32(ddloperatives.SelectedValue))
    End Sub

#End Region

#Region "Populate operative dropdown list"
    ''' <summary>
    ''' Populate dropdown with operatives who have an appointment scheduled today.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateOperativeDropdownList()
        Dim resultDataSet As DataSet = New DataSet()
        objDashboardBL.getOperativesReserveToday(resultDataSet)

        If (resultDataSet.Tables(0).Rows.Count > 0) Then
            ddloperatives.DataSource = resultDataSet.Tables(0).DefaultView
            ddloperatives.DataValueField = "EmployeeId"
            ddloperatives.DataTextField = "Name"
            ddloperatives.DataBind()
        End If

        Dim item As ListItem = New ListItem("Please Select", "-1")
        ddloperatives.Items.Insert(0, item)

    End Sub

#End Region

#Region "get Report URI"
    ''' <summary>
    ''' Gets reports URI
    ''' </summary>
    ''' <param name="key"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getReportURI(ByVal key As String) As String
        If String.IsNullOrEmpty(ConfigurationManager.AppSettings(key)) Then
            Return "#"

        Else
            Return ConfigurationManager.AppSettings(key)
        End If
    End Function
#End Region

#Region "Update Marker"
    ''' <summary>
    ''' Returns the appointments of an operative scheduled today.
    ''' </summary>
    ''' <param name="operativeId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function updateMarker(ByVal operativeId As Integer) As List(Of Object())
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim dsOperativeAppointments As DataSet = New DataSet()
        objDashboardBL.getTodaysAppointmentsOfOperative(dsOperativeAppointments, operativeId)
        Dim lstOutput As New List(Of Object())
        Dim rows As Integer
        rows = dsOperativeAppointments.Tables(0).Rows.Count - 1
        For intRow As Integer = 0 To rows
            lstOutput.Add(dsOperativeAppointments.Tables(0).Rows(intRow).ItemArray())
        Next
        Return lstOutput
    End Function
#End Region

#Region "Update In Progress Number"
    ''' <summary>
    ''' Returns the count of Inprogress appointments.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountInProgress() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsInProgress(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyInProgress)
        Return result
    End Function
#End Region

#Region "Update In No Entry Number"
    ''' <summary>
    ''' Returns the count of No Entry appointments.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountNoEntries() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsNoEntry(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyNoEntry)

        Return result
    End Function
#End Region

#Region "Update In FollowOnWork Number"
    ''' <summary>
    ''' Returns the count of FollowOnWork appointment.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountFollowOnWork() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsFOLLOWON(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyFOLLOWON)
        Return result
    End Function
#End Region

#Region "Update In FaultsOverDue Number"
    ''' <summary>
    ''' Returns the count of FaultsOverDue appointments.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountFaultsOverDue() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsFaultsOverDue(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyFaultsOverDue)
        Return result
    End Function

#End Region

#Region "Update In CompletedFaults Number"
    ''' <summary>
    ''' Returns the number of Completed Faults.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountCompletedFaults() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsCompletedFaults(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyCompletedFaults)
        Return result
    End Function

#End Region

#Region "Update In ReportedFaults Number"
    ''' <summary>
    ''' Returns the number of Completed Faults.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountReportedFaults() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsReportedFaults(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyReportedFaults)
        Return result
    End Function

#End Region

#Region "Update In AssignToContractor Number"
    ''' <summary>
    ''' Returns count of AssignToContractor faults.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountAssignToContractor() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsAssignToContractor(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyAssignToContractor)
        Return result
    End Function

#End Region

#Region "Update In Emergencyfaults Number"
    ''' <summary>
    ''' Returns Emergencyfaults Number
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountEmergencyfaults() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsEmergencyFaults(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyEmergencyfaults)
        Return result
    End Function

#End Region

#Region "Update In RecallReported Number"
    ''' <summary>
    ''' Returns Recall Reported Number.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getCountRecallReported() As String
        Dim objDashboardBL As DashboardBL = New DashboardBL()
        Dim countHashtable As Hashtable = New Hashtable()
        objDashboardBL.getCountsReCall(countHashtable)
        Dim result As String = countHashtable(ApplicationConstants.HashTableKeyReCall)
        Return result
    End Function

#End Region

#End Region

End Class
