﻿Imports RD_BusinessLogic
Imports RD_BusinessObject
Imports RD_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class NoEntryJobSheet
    Inherits UserControlBase

#Region "Properties"

    Dim objReportsBl As ReportsBL = New ReportsBL()

#End Region

#Region "Events"
 
#Region "Page Load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Get No Entry Detail"
    ''' <summary>
    ''' Get No Entry Detail
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <remarks></remarks>
    Sub getNoEntryDetail(ByVal jsn As String, Optional ByVal appointmentType As String = Nothing)

        Try
            If appointmentType = "SbFault" Then
                ifrmJobSheet.Attributes("src") = GeneralHelper.getSbJobSheetSummaryAddress() + jsn
            Else
                ifrmJobSheet.Attributes("src") = GeneralHelper.getJobSheetSummaryAddress() + jsn
            End If
            'ifrmJobSheet.Attributes("src") = GeneralHelper.getJobSheetSummaryAddress() + jsn
            popupNoEntry.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                'uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

End Class