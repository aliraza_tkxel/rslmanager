﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NoEntryJobSheet.ascx.vb"
    Inherits="RepairsDashboard.NoEntryJobSheet" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!-- ModalPopupExtender -->
<asp:Button ID="btnHiddenEntry" UseSubmitBehavior="false" runat="server" Text=""
    Style="display: none;" />
<cc1:ModalPopupExtender ID="popupNoEntry" runat="server" PopupControlID="pnlNoEntry"
    TargetControlID="btnHiddenEntry" CancelControlID="btnCloseJobSheetPopup" BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlNoEntry" runat="server" CssClass="modalPopup" Style="height: 600px;
    width: 900px; overflow: hidden; display: none;">
    <iframe runat="server" id="ifrmJobSheet" class="ifrmJobSheet" style="height: 575px;
        overflow: auto; width: 100%; border: 0px none transparent;" />
    <div style="width: 100%; text-align: right; clear: both; float: right;">
        <asp:Button ID="btnCloseJobSheetPopup" runat="server" Text="Close" />
    </div>
    <br />
</asp:Panel>
