﻿Imports RD_BusinessLogic


Public Class schemeBlockDropdown
    Inherits System.Web.UI.UserControl

#Region "Delegates"

    Public Delegate Sub scheme_changed(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Delegate Sub block_changed(ByVal sender As Object, ByVal e As System.EventArgs)

#End Region

#Region "Control Events"

    Public Event schemeChanged As scheme_changed
    Public Event blockChanged As block_changed

#End Region

#Region "Properties"

    Public ReadOnly Property SchemeId() As Integer
        Get
            Dim _schemeId As Integer = -1
            Integer.TryParse(ddlScheme.SelectedValue, _schemeId)
            Return _schemeId
        End Get
    End Property

    Public ReadOnly Property BlockId() As Integer
        Get
            Dim _blockId As Integer = -1
            Integer.TryParse(ddlBlock.SelectedValue, _blockId)
            Return _blockId
        End Get
    End Property

#End Region

#Region "Event Handling"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            loadDropdowns()
        End If
    End Sub

    Protected Sub ddlScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlScheme.SelectedIndexChanged
        Dim schemeId As Integer = Convert.ToInt32(ddlScheme.SelectedValue)
        loadBlocks(schemeId)
        RaiseEvent schemeChanged(sender, e)
    End Sub

    Protected Sub ddlBlock_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlBlock.SelectedIndexChanged
        RaiseEvent blockChanged(sender, e)
    End Sub

#End Region

#Region "Functions"

    Private Sub loadDropdowns()
        Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim schemeDataSet As DataSet = New DataSet()
        objReportsBL.loadSchemeDropDown(schemeDataSet)
        If (schemeDataSet.Tables(0).Rows.Count > 0) Then
            ddlScheme.DataSource = schemeDataSet
            ddlScheme.DataTextField = "SCHEMENAME"
            ddlScheme.DataValueField = "SCHEMEID"
            ddlScheme.DataBind()
        End If
        Dim item As ListItem = New ListItem("Select Scheme", "-1")
        ddlScheme.Items.Insert(0, item)
        loadBlocks(-1)
    End Sub

    Protected Sub loadBlocks(ByVal schemeId As Integer)
        Dim objReportsBL As ReportsBL = New ReportsBL()
        Dim blockDataSet As DataSet = New DataSet()
        ddlBlock.Items.Clear()

        objReportsBL.loadBlockDropdownbySchemeId(blockDataSet, schemeId)
        ddlBlock.DataSource = blockDataSet
        ddlBlock.DataTextField = "BLOCKNAME"
        ddlBlock.DataValueField = "BLOCKID"
        ddlBlock.DataBind()

        Dim item As ListItem = New ListItem("Select Block", "-1")
        ddlBlock.Items.Insert(0, item)
    End Sub

#End Region

End Class