﻿Imports RD_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports RD_Utilities
Imports System.IO
Imports System.Globalization

Public Class PriorityAndCompletionListControl
    Inherits UserControlBase

#Region "Properties"

    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "FaultLogID", 1, 30)
    Dim totalCount As Integer = 0

#End Region

#Region "Events Handling"

#Region "Page Load Event"
    ''' <summary>
    ''' Fires when page loads
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Grid Priority and Completion List Sorting Event"
    ''' <summary>
    ''' Grid Priority and Completion List Sorting Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdPriorityAndCompletionList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdPriorityAndCompletionList.Sorting

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdPriorityAndCompletionList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim resultDataSet As DataSet = New DataSet()
            populatePriorityList(resultDataSet)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Priority And Completion List RowDataBound"
    ''' <summary>
    ''' Priority And Completion List RowDataBound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdPriorityAndCompletionList_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdPriorityAndCompletionList.RowDataBound
        Try

            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim lblTimeframe As Label = DirectCast(e.Row.FindControl("lblTimeframe"), Label)
                If Not String.IsNullOrEmpty(lblTimeframe.Text) Then
                    Dim timeFrame As Integer = Convert.ToInt32(lblTimeframe.Text)
                    If (timeFrame > 0) Then
                        lblTimeframe.ForeColor = Drawing.Color.Red
                        lblTimeframe.Text = "+" + lblTimeframe.Text
                    Else
                        lblTimeframe.ForeColor = Drawing.Color.Green
                    End If

                    If (timeFrame >= 0 And timeFrame <= 1) Then
                        lblTimeframe.Text = lblTimeframe.Text + " day"
                    Else
                        lblTimeframe.Text = lblTimeframe.Text + " days"
                    End If

                    lblTimeframe.Font.Bold = True
                End If
            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

    Protected Sub grdPriorityAndCompletionList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim resultDataSet As DataSet = New DataSet()
            populatePriorityList(resultDataSet)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)
                Dim resultDataSet As DataSet = New DataSet()
                populatePriorityList(resultDataSet)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Export Full list Event"

    ''' <summary>
    ''' Export list Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportList.Click

        Try
            exportFullList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Validate fields"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub validateFields(ByVal objPrioritySearchBO As PrioritySearchBO)
        Dim startDateTimeText As Date
        Dim endDateTimeText As Date
        Dim isValidDate As Boolean

        If (objPrioritySearchBO.FromDate.Length > 0) Then
            isValidDate = Date.TryParseExact(objPrioritySearchBO.FromDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, startDateTimeText)
            If (Not isValidDate) Then
                Throw New Exception(UserMessageConstants.InvalidStartDate)
            End If
        End If

        If (objPrioritySearchBO.ToDate.Length > 0) Then
            isValidDate = Date.TryParseExact(objPrioritySearchBO.ToDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, endDateTimeText)
            If (Not isValidDate) Then
                Throw New Exception(UserMessageConstants.InvalidEndDate)
            End If
        End If

        If (objPrioritySearchBO.FromDate.Length > 0) And (objPrioritySearchBO.ToDate.Length > 0) Then
            Dim result As Integer = DateTime.Compare(startDateTimeText, endDateTimeText)
            If result > 0 Then
                Throw New Exception(UserMessageConstants.InvalidStartEndDate)
            End If

        End If

    End Sub

#End Region

#Region "Populate Priority List"

    ''' <summary>
    ''' Populate Priority List
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Sub populatePriorityList(ByRef resultDataSet As DataSet)

        Dim objPrioritySearchBO As PrioritySearchBO = SessionManager.getPrioritySearchBO()
        If (IsNothing(objPrioritySearchBO)) Then
            Throw New Exception(UserMessageConstants.ProblemLoadingData)
        End If

        validateFields(objPrioritySearchBO)

        objPageSortBo = getPageSortBoViewState()

        Dim objReportsBl As ReportsBL = New ReportsBL()
        totalCount = objReportsBl.getPriorityAndCompletionFaultList(resultDataSet, objPrioritySearchBO, objPageSortBo)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        grdPriorityAndCompletionList.VirtualItemCount = totalCount
        grdPriorityAndCompletionList.DataSource = resultDataSet
        grdPriorityAndCompletionList.DataBind()

        If (resultDataSet.Tables(0).Rows.Count > 0) Then
            btnExportList.Enabled = True
        Else
            btnExportList.Enabled = False
        End If

        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)


        If grdPriorityAndCompletionList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdPriorityAndCompletionList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Export full List"

    ''' <summary>
    ''' Export full List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportFullList()
        Dim grdFullPriorityList As New GridView()
        grdFullPriorityList.AllowPaging = False
        grdFullPriorityList.AutoGenerateColumns = False

        Dim jsnField As BoundField = New BoundField()
        jsnField.HeaderText = "JSN:"
        jsnField.DataField = "JSN"
        grdFullPriorityList.Columns.Add(jsnField)

        Dim reportedField As BoundField = New BoundField()
        reportedField.HeaderText = "Reported:"
        reportedField.DataField = "Reported"
        grdFullPriorityList.Columns.Add(reportedField)

        Dim addressField As BoundField = New BoundField()
        addressField.HeaderText = "Address:"
        addressField.DataField = "Address"
        grdFullPriorityList.Columns.Add(addressField)

        Dim descriptionField As BoundField = New BoundField()
        descriptionField.HeaderText = "Description:"
        descriptionField.DataField = "Description"
        grdFullPriorityList.Columns.Add(descriptionField)

        Dim priorityField As BoundField = New BoundField()
        priorityField.HeaderText = "Priority:"
        priorityField.DataField = "Priority"
        grdFullPriorityList.Columns.Add(priorityField)

        Dim scheduledField As BoundField = New BoundField()
        scheduledField.HeaderText = "Scheduled:"
        scheduledField.DataField = "Scheduled"
        grdFullPriorityList.Columns.Add(scheduledField)

        Dim byField As BoundField = New BoundField()
        byField.HeaderText = "By:"
        byField.DataField = "Operative"
        grdFullPriorityList.Columns.Add(byField)

        Dim completedField As BoundField = New BoundField()
        completedField.HeaderText = "Completed:"
        completedField.DataField = "Completed"
        grdFullPriorityList.Columns.Add(completedField)

        Dim timeframeField As BoundField = New BoundField()
        timeframeField.HeaderText = "Timeframe:"
        timeframeField.DataField = "TimeFrame"
        grdFullPriorityList.Columns.Add(timeframeField)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objPrioritySearchBO As PrioritySearchBO = SessionManager.getPrioritySearchBO()
        If (IsNothing(objPrioritySearchBO)) Then
            Throw New Exception(UserMessageConstants.ProblemLoadingData)
        End If

        Dim objReportsBl As ReportsBL = New ReportsBL()
        objReportsBl.getFullPriorityAndCompletionFaultList(resultDataSet, objPrioritySearchBO, getPageSortBoViewState())

        grdFullPriorityList.DataSource = resultDataSet.Tables(0)
        grdFullPriorityList.DataBind()

        ExportGridToExcel(grdFullPriorityList)

    End Sub

#End Region

#Region "Export Grid To Excel"

    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "PriorityAndCompletionReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub

#End Region

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

End Class