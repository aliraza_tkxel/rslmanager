﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FinancialYear.ascx.vb"
    Inherits="RepairsDashboard.FinancialYear" %>
<div class="form-control">
    <div class="select_div">
        <div class="">
            <asp:DropDownList runat="server" ID="ddlFinancialYear" AutoPostBack="true"></asp:DropDownList>
        </div>
    </div>
</div>