﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PriorityAndCompletionListControl.ascx.vb"
    Inherits="RepairsDashboard.PriorityAndCompletionListControl" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:UpdatePanel ID="updPanelPriorityList" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
            <cc1:PagingGridView ID="grdPriorityAndCompletionList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                OnRowCreated="grdPriorityAndCompletionList_RowCreated" 
                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
                <Columns>
                    <asp:TemplateField HeaderText="JSN" ItemStyle-CssClass="dashboard" SortExpression="JSN">
                        <ItemTemplate>
                            <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle BorderStyle="None" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reported" SortExpression="Reported">
                        <ItemTemplate>
                            <asp:Label ID="lblReported" runat="server" Text='<%# Bind("Reported") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="190px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Priority" SortExpression="Priority">
                        <ItemTemplate>
                            <asp:Label ID="lblPriority" runat="server" Text='<%# Bind("Priority") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="80px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appointment" SortExpression="Scheduled">
                        <ItemTemplate>
                            <asp:Label ID="lblScheduled" runat="server" Text='<%# Bind("Scheduled") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Operative" SortExpression="Operative">
                        <ItemTemplate>
                            <asp:Label ID="lblBy" runat="server" Text='<%# Bind("Operative") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Completed" SortExpression="Completed">
                        <ItemTemplate>
                            <asp:Label ID="lblCompleted" runat="server" Text='<%# Bind("Completed") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Timeframe" SortExpression="Timeframe">
                        <ItemTemplate>
                            <asp:Label ID="lblTimeframe" runat="server" Text='<%# Bind("Timeframe") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="70px" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
            </cc1:PagingGridView>
        </div>
        <%--Pager Template Start--%>
        <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
            <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                <div class="paging-left">
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                            &lt;&lt;First
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                            &lt;Prev
                        </asp:LinkButton>
                    </span>
                    <span style="padding-right:10px;">
                        <b>Page:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                        of
                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                    </span>
                    <span style="padding-right:20px;">
                        <b>Result:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                        to
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                        of
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                    </span>
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                        </asp:LinkButton>
                    </span>
                </div>
                <div style="float: right;">
                    <span>
                        <asp:Button ID="btnExportList" runat="server" Text="Export Results to XLS" UseSubmitBehavior="False"
                            class="btn btn-xs btn-blue right" style="padding:1px 5px !important;" />
                    </span>
                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                    <div class="field" style="margin-right: 10px;">
                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                    </div>
                    <span>
                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                            class="btn btn-xs btn-blue" style="padding:1px 5px !important; margin-right: 10px;min-width:0px;" OnClick="changePageNumber" />
                    </span>
                </div>
            </div>
        </asp:Panel>
        <%--Pager Template End--%>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportList" />
    </Triggers>
</asp:UpdatePanel>
