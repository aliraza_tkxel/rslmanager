﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FaultsToBeArrangedListControl.ascx.vb"
    Inherits="RepairsDashboard.FaultsToBeArrangedListControl" %>
<%@ Register Src="~/Controls/JobSheets/NoEntryJobSheet.ascx" TagName="NoEntryJobSheet"
    TagPrefix="ucJobSheet" %>
<%@ Register Src="~/Controls/Reports/schemeBlockDropdown.ascx" TagName="SchemeBlockDropdown"
    TagPrefix="ucSchemeBlockddl" %>
<%@ Register Src="~/Controls/Reports/FinancialYear.ascx" TagName="FinancialYear"
    TagPrefix="uc" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="False">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:Panel runat="server" ID="pnlSchemBlockddl">
    <div class="" >
        <ucSchemeBlockddl:SchemeBlockDropdown ID="SchemeBlockDDL" runat="server" Visible="false">
        </ucSchemeBlockddl:SchemeBlockDropdown>
        <uc:FinancialYear ID="ucFinancialYear" runat="server" style="width: 95px;" Visible="false">
        </uc:FinancialYear>
    </div>
</asp:Panel>
<asp:UpdatePanel runat="server" ID="updPanelgrdFaultsToBeArrangedList">
    <ContentTemplate>
        <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
            <cc1:PagingGridView ID="grdFaultsToBeArrangedList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                OnRowCreated="grdFaultsToBeArrangedList_RowCreated" 
                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
                <Columns>
                    <asp:TemplateField HeaderText="TenantName" ItemStyle-CssClass="dashboard" SortExpression="TenantName">
                        <ItemTemplate>
                            <asp:Label ID="lblTenantName" runat="server" Text='<%# Bind("TenantName") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle BorderStyle="None" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                        <ItemTemplate>
                            <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Block" SortExpression="Block">
                        <ItemTemplate>
                            <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PostCode" SortExpression="PostCode">
                        <ItemTemplate>
                            <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("PostCode") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Telephone" SortExpression="Telephone">
                        <ItemTemplate>
                            <asp:Label ID="lblTelephone" runat="server" Text='<%# Bind("Telephone") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mobile" SortExpression="Mobile">
                        <ItemTemplate>
                            <asp:Label ID="lblMobile" runat="server" Text='<%# Bind("Mobile") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fault" SortExpression="Fault">
                        <ItemTemplate>
                            <asp:Label ID="lblFault" runat="server" Text='<%# Bind("Fault") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="20%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reported" SortExpression="Reported">
                        <ItemTemplate>
                            <asp:Label ID="lblReported" runat="server" Text='<%# Bind("ReportedDate") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ReportedBy" SortExpression="ReportedBy">
                        <ItemTemplate>
                            <asp:Label ID="lblReportedBy" runat="server" Text='<%#Bind("ReportedBy")  %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:Button ID="btnViewBasket" runat="server" Text="View Basket" Visible='<%# checkPropertyFault(Eval("FaultType")) %>'
                                CommandArgument='<%#Eval("CustomerId").ToString()+";"+Eval("PropertyId").ToString()%>'
                                OnDataBinding="btnViewBasket_Bind" EnableTheming="True" UseSubmitBehavior="true"
                                CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;" />
                            <asp:Button ID="btnViewSbFaultBasket" runat="server" Text="View Basket" Visible='<%# checkSbFault(Eval("FaultType")) %>'
                                CommandArgument='<%#Eval("SchemeId").ToString() + ";" + Eval("BlockId").ToString()%>'
                                OnDataBinding="btnViewSbBasket_Bind" EnableTheming="True" UseSubmitBehavior="true"
                                CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
            </cc1:PagingGridView>
        </div>
        
        <%--Pager Template Start--%>
        <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
            <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                <div class="paging-left">
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                            &lt;&lt;First
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                            &lt;Prev
                        </asp:LinkButton>
                    </span>
                    <span style="padding-right:10px;">
                        <b>Page:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                        of
                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                    </span>
                    <span style="padding-right:20px;">
                        <b>Result:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                        to
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                        of
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                    </span>
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                        </asp:LinkButton>
                    </span>
                </div>
                <div style="float: right;">
                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                    <div class="field" style="margin-right: 10px;">
                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                    </div>
                    <span>
                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                            class="btn btn-xs btn-blue" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                    </span>
                </div>
            </div>
        </asp:Panel>
        <%--Pager Template End--%>
    </ContentTemplate>
</asp:UpdatePanel>

