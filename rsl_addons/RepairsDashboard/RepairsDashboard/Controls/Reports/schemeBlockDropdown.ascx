﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="schemeBlockDropdown.ascx.vb" Inherits="RepairsDashboard.schemeBlockDropdown" %>
<div class="form-control">
    <div class="select_div">
        <div class="">
            <asp:DropDownList runat="server" ID="ddlScheme" AutoPostBack="true"></asp:DropDownList>
        </div>
    </div>
</div>
<div class="form-control">
    <div class="select_div">
        <div class="">
            <asp:DropDownList runat="server" ID="ddlBlock" AutoPostBack="true"></asp:DropDownList>
        </div>
    </div>
</div>

