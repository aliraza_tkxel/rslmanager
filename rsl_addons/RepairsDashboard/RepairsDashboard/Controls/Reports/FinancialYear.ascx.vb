﻿Imports RD_Utilities

Public Class FinancialYear
    Inherits UserControlBase

#Region "Delegates"

    Public Delegate Sub financialYear_changed(ByVal sender As Object, ByVal e As System.EventArgs)

#End Region

#Region "Control Events"

    Public Event financialYearChanged As financialYear_changed

#End Region

#Region "Properties"

    Public ReadOnly Property FinancialYear() As Integer
        Get
            Dim _FinancialYear As Integer = -1
            Integer.TryParse(ddlFinancialYear.SelectedValue, _FinancialYear)
            Return _FinancialYear
        End Get
    End Property

#End Region

#Region "Events Handling"

    Private Sub ddlFinancialYear_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlFinancialYear.SelectedIndexChanged
        RaiseEvent financialYearChanged(sender, e)
    End Sub

#End Region

#Region "Functions"

    Public Sub loadFinancialYears()
        Dim currentDateTime As Date = Date.Now
        Dim selectedYear As Integer = GeneralHelper.getFinancialYear_UK(currentDateTime)

        ddlFinancialYear.Items.Clear()

        For yearCounter As Integer = 2008 To currentDateTime.Year
            Dim yearItem As New ListItem(String.Format("{0} - {1}", yearCounter, yearCounter + 1), yearCounter)
            yearItem.Selected = If(yearCounter = selectedYear, True, False)
            ddlFinancialYear.Items.Add(yearItem)
        Next

        ddlFinancialYear.Items.Add(New ListItem("All Years", "-1"))
    End Sub

#End Region

End Class