﻿Imports RD_BusinessLogic
Imports RD_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class OperativeAppointmentsListControl
    Inherits UserControlBase


#Region "Properties"
    Dim objDashboardBl As DashboardBL = New DashboardBL()
#End Region

#Region "Events"

#Region "Page load"
    ''' <summary>
    ''' Fires when page loads.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region

#Region "Gridview row databound"
    ''' <summary>
    ''' Gridview row databound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdOperativeAppointments_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdOperativeAppointments.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                e.Row.BackColor = Drawing.Color.White

                Dim row As GridViewRow = e.Row
                Dim lblJSN As Label = DirectCast(row.FindControl("lblJsn"), Label)
                Dim lblAppointmentType As Label = DirectCast(row.FindControl("lblAppointmentType"), Label)
                e.Row.Attributes.Add("onclick", String.Format("javascript:ShowJobsheetSummary('{0}')", lblJSN.Text + ";" + lblAppointmentType.Text))
                'e.Row.Attributes.Add("onmouseover", "mouseIn(this);")
                'e.Row.Attributes.Add("onmouseout", "mouseOut(this);")
                e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'")

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If

        End Try
    End Sub
#End Region

#Region "Gridview row selecting"
    ''' <summary>
    ''' Gridview row selecting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
        Dim argument = Request.Form("__EVENTARGUMENT")
        Dim commandArguments = argument.Split(";")
        Dim jsn As String = commandArguments(0)
        Dim appointmentType As String = commandArguments(1)
        NoEntryJobSheet.getNoEntryDetail(jsn, appointmentType)
        NoEntryJobSheet.Visible = True

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate List with appointments of an operative"
    ''' <summary>
    ''' Populate List with appointments of an operative
    ''' </summary>
    ''' <param name="operativeId"></param>
    ''' <remarks></remarks>
    Sub populateAppointmentsList(ByVal operativeId As Integer)
        ViewState(ViewStateConstants.OperativeId) = operativeId
        Dim resultDataSet As DataSet = New DataSet()
        populateOperativeAppointments(operativeId, ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))

    End Sub

#End Region

#Region "Populate operative appointments grid"
    ''' <summary>
    ''' Populate operative appointments grid
    ''' </summary>
    ''' <param name="operativeId"></param>
    ''' <param name="sortExp"></param>
    ''' <param name="sortDir"></param>
    ''' <remarks></remarks>
    Sub populateOperativeAppointments(ByVal operativeId As Integer, ByVal sortExp As String, ByVal sortDir As String)

        Dim dsOperativeAppointments As DataSet = New DataSet()
        Dim appointmentDataView As New DataView()
        objDashboardBl.getTodaysAppointmentsOfOperative(dsOperativeAppointments, operativeId)
        appointmentDataView = dsOperativeAppointments.Tables(0).DefaultView
        grdOperativeAppointments.DataSource = appointmentDataView
        grdOperativeAppointments.DataBind()

    End Sub
#End Region

#End Region

   
End Class