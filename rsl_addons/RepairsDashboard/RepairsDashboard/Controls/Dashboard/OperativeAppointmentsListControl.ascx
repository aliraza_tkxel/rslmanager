﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OperativeAppointmentsListControl.ascx.vb"
    Inherits="RepairsDashboard.OperativeAppointmentsListControl" %>
<%@ Register Src="~/Controls/JobSheets/NoEntryJobSheet.ascx" TagName="NoEntryJobSheet"
    TagPrefix="uc1" %>
<link href="../../Styles/Dashboard.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">

    function ShowJobsheetSummary(jsn) {
        __doPostBack('<%= btnHidden.UniqueID %>', jsn);
    }
    //Function to change color of grid row on mouseover
    function mouseIn(row) {
        row.style.backgroundColor = '#e6e6e6';
    }
    //Function to change color of grid row on mouseout
    function mouseOut(row) {
        row.style.backgroundColor = '#FFFFFF';
    }
</script>
<asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
<asp:Panel ID="pnlMessage1" runat="server" Visible="False">
    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
</asp:Panel>
<br />
<br />
<asp:GridView ID="grdOperativeAppointments" runat="server" CssClass="dashboard webgrid table table-responsive" AutoGenerateColumns="False"
    ShowHeaderWhenEmpty="true" BorderWidth="0px" BorderStyle="Solid" Width="100%"
    AllowSorting="false" AllowPaging="False" GridLines="None" 
    CellSpacing="10" HorizontalAlign="Left" ClientIDMode="AutoID" CellPadding="2"
    BorderColor="#BBBBBB" EmptyDataText="No Record Found.">
    <Columns>
        <asp:TemplateField HeaderText="JSN:" SortExpression="JSN" ConvertEmptyStringToNull="False" >
            <ItemTemplate>
                <asp:Label ID="lblJsn" runat="server" Text='<%#Eval("JSN") %>'></asp:Label>
            </ItemTemplate>
            <FooterStyle VerticalAlign="Middle"></FooterStyle>
            <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
            <ItemStyle Width="300px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Address:" SortExpression="Address" ConvertEmptyStringToNull="False" >
            <ItemTemplate>
                <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address") %>'></asp:Label>
                <asp:Label ID="lblAppointmentType" runat="server" Text='<%#Eval("AppointmentType") %>' Visible="false"  ></asp:Label>
            </ItemTemplate>
            <FooterStyle VerticalAlign="Middle"></FooterStyle>
            <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
            <ItemStyle Width="60px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode" ConvertEmptyStringToNull="False">
            <ItemTemplate>
                <asp:Label ID="lblPostcode" runat="server" Text='<%#Eval("Postcode").Tostring()%>'></asp:Label>
            </ItemTemplate>
            <FooterStyle VerticalAlign="Middle"></FooterStyle>
            <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
            <ItemStyle Width="80px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Status:" SortExpression="Status" ConvertEmptyStringToNull="False">
            <ItemTemplate>
                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status").Tostring()%>'></asp:Label>
            </ItemTemplate>
            <FooterStyle VerticalAlign="Middle"></FooterStyle>
            <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
            <ItemStyle Width="80px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Time:" SortExpression="Time" ConvertEmptyStringToNull="False">
            <ItemTemplate>
                <asp:Label ID="lblTime" runat="server" Text='<%#Eval("Time").Tostring()%>'></asp:Label>
            </ItemTemplate>
            <FooterStyle VerticalAlign="Middle"></FooterStyle>
            <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
            <ItemStyle Width="80px" />
        </asp:TemplateField>
    </Columns>
    <EditRowStyle Wrap="True" />
    <RowStyle BorderStyle="None" />
</asp:GridView>
<div style="float: left; width: 800px;">
    <uc1:NoEntryJobSheet ID="NoEntryJobSheet" runat="server" Visible="false"></uc1:NoEntryJobSheet>
</div>
