﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MapControl.ascx.vb"
    Inherits="RepairsDashboard.MapControl" %>
<%@ Register Src="~/Controls/JobSheets/NoEntryJobSheet.ascx" TagName="NoEntryJobSheet"
    TagPrefix="ucJobSheet" %>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBc52SCdjgLvwwSOIw0bKNaRnXTfdeniM4&v=3.17"></script>
<script type="text/javascript">

    function loadDefaultMap(lat, lng, index) {
        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    }

    function addMarkers(markersArray) {

        var mapOptions = {
            center: new google.maps.LatLng(52.630886, 1.297355),
            zoom: 7,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

        var markers;
        if (markersArray.length > 0) {
            markers = JSON.parse(markersArray);
            
            for (j = 0; j < markers.length; j++) {
                getGeoLocationAndSetMarker(markers[j], map);
            }

        } else {
            loadDefaultMap(52.630886, 1.297355, -1);
        }
    }
    function infoCallback(infoWindow, marker, map) {
        return function () {
            infoWindow.open(map, marker);
        };
    };
    function pageLoad() {
        populateMarker();

    }

    function getGeoLocationAndSetMarker(data, map) {
        var adrs = "";
        var bounds = new google.maps.LatLngBounds();
        if (data.Appointmenttype == "Fault") {
             adrs = data.Address + ", " + data.TownCity + ", " + data.County
        }
        else {
            adrs = data.Address
        }
       
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': adrs }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var lng = results[0].geometry.location.lng();
                var myLatlng = new google.maps.LatLng(lat, lng);

                if (myLatlng != '') {
                    setupMarker(map, myLatlng, data);
                }
            }
        });

    }

    function setupMarker(map, location, data) {

        var infoWindow = new google.maps.InfoWindow();

            var icon = '';
            var Time = data.StartTime.split(':');

            var marker;

            if (Time[0] > 00 && Time[0] < 12) {
                icon = '../../Images/blue.png'
            }
            else {
                icon = '../../Images/red.png'
            }

            marker = new google.maps.Marker({
                position: location,
                map: map,
                title: data.Address + " " + data.TownCity + " " + data.County,
                icon: icon,
                animation: google.maps.Animation.DROP
            });

            (function (marker, data) {
                google.maps.event.addListener(marker, "mouseover", function (e) {
                    infoWindow.setContent("<b>" + data.JSNGrouped + "</b><br>" + data.Address + ", " + data.TownCity + ", " + data.County + "<b><br>" + data.Postcode + "</b><br> " + data.StartTime);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    __doPostBack("<%=btnJobSheet.UniqueID %>", data.JSN);
                });
            })(marker, data);
        
    }

    function setMarker(map, locationArray, markers) {

        var infoWindow = new google.maps.InfoWindow();
        for (k = 0; k < markers.length; k++) {

            var data = markers[k];
            var icon = '';
            var Time = markers[k].StartTime.split(':');

            var marker;

            if (Time[0] > 00 && Time[0] < 12) {
                icon = '../../Images/blue.png'
            }
            else {
                icon = '../../Images/red.png'
            }

            marker = new google.maps.Marker({
                position: locationArray[k],
                map: map,
                title: markers[k].Address + " " + markers[k].TownCity + " " + markers[k].County,
                icon: icon,
                animation: google.maps.Animation.DROP
            });
          
            (function (marker, data) {
                google.maps.event.addListener(marker, "mouseover", function (e) {
                    infoWindow.setContent("<b>" + data.JSNGrouped + "</b><br>" + data.Address + ", " + data.TownCity + ", " + data.County + "<b><br>" + data.Postcode + "</b><br> " + data.StartTime);
                    infoWindow.open(map, marker);
                });
            })(marker, data);
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    __doPostBack("<%=btnJobSheet.UniqueID %>", data.JSN);
                });
            })(marker, data);
        }
    }
 
</script>
<asp:UpdatePanel ID="updPanelMap" runat="server">
    <ContentTemplate>
        <div id="dvMap" style="width: 100%; height: 400px; float: left; margin-bottom:2px;"></div>
        <div style="float: left; width: 800px;">
            <asp:Button ID="btnJobSheet" runat="server" Text="" Style="display: none;" />
            <ucJobSheet:NoEntryJobSheet ID="NoEntryJobSheet" runat="server" Visible="false"></ucJobSheet:NoEntryJobSheet>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
