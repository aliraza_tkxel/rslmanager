﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports RD_BusinessLogic
Imports RD_Utilities
Imports RD_BusinessObject

Public Class Bridge
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

#Region "Attributes / Data Member"

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
    Dim customerId As Integer = 0
    Dim propertyId As String = String.Empty
    Dim classicUserId As Integer = 0
    Dim isReportView As Boolean = False
    Dim reportToSelect As String = String.Empty

#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Event fires on page load.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Me.loadUser()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Load User"
    ''' <summary>
    ''' Load User
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadUser()

        Dim objUserBo As UserBO = New UserBO()
        Dim objUserBl As UsersBL = New UsersBL()
        Dim resultDataSet As DataSet = New DataSet()
        Me.getQueryStringValues()
        Me.checkClassicAspSession()

        resultDataSet = objUserBl.getEmployeeById(classicUserId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserDoesNotExist, True)
        Else
            Dim isActive As String = resultDataSet.Tables(0).Rows(0).Item("IsActive").ToString()

            If isActive = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UsersAccountDeactivated, True)
            Else
                setUserSession(resultDataSet)
                If isReportView = True AndAlso Not (reportToSelect = String.Empty) Then
                    Response.Redirect(PathConstants.ReportsPath + "?" + PathConstants.Report + "=" + reportToSelect, True)
                ElseIf isReportView Then
                    Response.Redirect(PathConstants.ReportsPath + "?" + PathConstants.Report + "=rf", True)

                Else
                    redirectToAccessGrantedPage()
                End If
                'Response.Redirect(PathConstants.DashboardPath, True)
            End If

        End If
    End Sub
#End Region

#Region "Redirect to access granted page"
    ''' <summary>
    ''' Redirect to access granted page
    ''' </summary>
    ''' <remarks></remarks>
    Sub redirectToAccessGrantedPage()
        ' SessionManager.setUserEmployeeId(760)
        Dim objDashboardBL As DashboardBL = New DashboardBL
        Dim resultDatatable As DataTable
        Dim userId As Integer = SessionManager.getUserEmployeeId()
        Dim pageName As String = String.Empty

        Dim repairsMenu As String = ApplicationConstants.RepairsMenu
        Dim resultDataset As DataSet = New DataSet
        objDashboardBL.getPropertyPageList(resultDataset, userId, repairsMenu)

        Dim accessGrantedPagesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)
        resultDatatable = accessGrantedPagesDt.Clone()
        Dim navigateUrl As String = String.Empty

        Dim query = (From dataRow In accessGrantedPagesDt _
                  Where dataRow.Field(Of String)(ApplicationConstants.GrantPageCoreUrlCol).Trim() <> String.Empty _
                  Order By dataRow.Field(Of Integer)(ApplicationConstants.GrantPageAccessLevelCol), _
                            dataRow.Field(Of Integer)(ApplicationConstants.GrantPageIdCol) Ascending _
                  Select dataRow)

        If query.Count > 0 Then
            resultDatatable = query.CopyToDataTable()

            pageName = resultDatatable.Rows(0).Item(ApplicationConstants.GrantPagePageNameCol)

            If (pageName.Equals(ApplicationConstants.DashboardPage)) Then
                navigateUrl = resultDatatable.Rows(0).Item(ApplicationConstants.GrantPageUrlCol)
            Else
                navigateUrl = resultDatatable.Rows(0).Item(ApplicationConstants.GrantPageCoreUrlCol)
            End If


        Else
            navigateUrl = PathConstants.AccessDeniedPath
        End If

        Response.Redirect(navigateUrl, True)

    End Sub

#End Region

#Region "get Query String Values"

    Private Sub getQueryStringValues()
        If Request.QueryString(PathConstants.Report) IsNot Nothing OrElse Request.QueryString(PathConstants.ReportView) IsNot Nothing Then
            'the user want to go to report area
            isReportView = True
            reportToSelect = Request.QueryString(PathConstants.Report)
        End If
    End Sub

#End Region

#Region "Set User Session"
    ''' <summary>
    ''' Set User Session.
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Private Sub setUserSession(ByRef resultDataSet)

        SessionManager.setRepairsDashboardUserId(classicUserId)
        SessionManager.setUserFullName(resultDataSet.Tables(0).Rows(0).Item("FullName").ToString())
        SessionManager.setUserEmployeeId(resultDataSet.Tables(0).Rows(0).Item("EmployeeId").ToString())

    End Sub
#End Region

#Region "Check Classic Asp Session"
    ''' <summary>
    ''' Check Classic Asp Session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub checkClassicAspSession()

        If ASPSession("USERID") IsNot Nothing Then
            classicUserId = Integer.Parse(ASPSession("USERID").ToString())
        Else
            Me.redirectToLoginPage()
        End If

    End Sub
#End Region

#Region "Redirect To Login Page"
    ''' <summary>
    ''' Redirect To Login Page
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub redirectToLoginPage()
        Response.Redirect(PathConstants.LoginPath, True)
    End Sub
#End Region

#End Region

End Class
