﻿Imports System
Imports RD_BusinessLogic
Imports RD_Utilities

Public Class RDMasterPage
    Inherits System.Web.UI.MasterPage

    Public messageClass As String = "topmessagesuccess"
    Private varBackgroundGrayColor As String = "#e6e6e6"
    Private varBackgroundWhiteColor As String = "#FFFFFF"
    Private varBackgroundProperty As String = "background-color"
    Private varPageUrl As String
    Private varReportUrl As String
    Private objDashboad As DashboardBL = New DashboardBL()

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'highLightMenuItems()

        If Not Page.IsPostBack Then
            populateMenus()
        End If

    End Sub

#End Region

#Region "Repeater Row Data"
    ''' <summary>
    ''' Repeater Row Data
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptPages_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPages.ItemDataBound

        If (e.Item.ItemType = ListItemType.Item) Or _
                (e.Item.ItemType = ListItemType.AlternatingItem) Then

            Dim hdnPageId As HiddenField = CType(e.Item.FindControl("hdnPageId"), HiddenField)
            Dim hdnAccessLevel As HiddenField = CType(e.Item.FindControl("hdnAccessLevel"), HiddenField)
            Dim pnlPageMenu As Panel = CType(e.Item.FindControl("pnlPageMenu"), Panel)
            Dim pageId As Integer = Convert.ToInt32(hdnPageId.Value())
            Dim accessLevel As Integer = Convert.ToInt32(hdnAccessLevel.Value())
            Dim menuPage As Menu = CType(e.Item.FindControl("menuPage"), Menu)
            Dim accessDataSet As DataSet = SessionManager.getPageAccessDataSet()
            Dim pageDt As DataTable = accessDataSet.Tables(ApplicationConstants.AccessGrantedPagesDt)
            Dim resultPageDt As DataTable = pageDt.Clone()
            Dim parentId As Integer

            If (accessLevel = 1) Then

                Dim query = (From dataRow In pageDt _
                    Where dataRow.Field(Of Integer)(ApplicationConstants.GrantPageAccessLevelCol) = 1 _
                    Order By dataRow.Field(Of String)(ApplicationConstants.GrantOrderTextCol) Ascending _
                    Select dataRow)

                If query.Count > 0 Then
                    resultPageDt = query.CopyToDataTable()
                End If

            Else

                Dim parentExpression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId)
                Dim parentRow() As DataRow = pageDt.Select(parentExpression)

                If (parentRow.Count > 0) Then
                    parentId = parentRow(0)(ApplicationConstants.GrantPageParentPageCol)

                    Dim query = (From dataRow In pageDt _
                   Where dataRow.Field(Of Integer?)(ApplicationConstants.GrantPageParentPageCol) = parentId _
                   Order By dataRow.Field(Of String)(ApplicationConstants.GrantOrderTextCol) Ascending _
                   Select dataRow)

                    If query.Count > 0 Then
                        resultPageDt = query.CopyToDataTable()
                    End If

                End If

            End If

            menuPage.Items.Clear()
            For Each row As DataRow In resultPageDt.Rows

                Dim item As MenuItem = New MenuItem()
                item.Text = row(ApplicationConstants.GrantPagePageNameCol)
                item.Value = row(ApplicationConstants.GrantPageIdCol).ToString()
                item.NavigateUrl = row(ApplicationConstants.GrantPageCoreUrlCol)
                If (row(ApplicationConstants.GrantPageIdCol) = pageId) Then
                    item.Selected = True
                End If

                If ((String.IsNullOrEmpty(item.NavigateUrl) Or String.IsNullOrWhiteSpace(item.NavigateUrl)) And _
                   Not checkChildExist(Convert.ToInt32(item.Value), pageDt)) Then
                    item.Enabled = False
                End If

                menuPage.Items.Add(item)
            Next
            menuPage.DataBind()

            If (menuPage.Items.Count = 0 And accessLevel = 1) Then
                pnlPageMenu.Visible = False
            End If

            updPanelPages.Update()

        End If

    End Sub
#End Region

#Region "Page Item clicked"
    ''' <summary>
    ''' Menu Item clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub PageItem_MenuItemClick(ByVal sender As Object, ByVal e As MenuEventArgs)

        'Get page id of the selected item
        'Check navigate url is null or empty
        'if empty get its child and select first child and redirect to it.
        'if not empty then navigate to url

        Dim navigateUrl As String = e.Item.NavigateUrl
        Dim pageId As Integer = Convert.ToInt32(e.Item.Value)
        Dim resultDataset As DataSet = SessionManager.getPageAccessDataSet()
        Dim pageDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)

        If (String.IsNullOrEmpty(navigateUrl) Or String.IsNullOrWhiteSpace(navigateUrl)) Then

            Dim row As DataRow = (From dataRow In pageDt _
             Where dataRow.Field(Of Integer?)(ApplicationConstants.GrantPageParentPageCol) = pageId _
             Order By dataRow.Field(Of String)(ApplicationConstants.GrantOrderTextCol) Ascending _
             Select dataRow).FirstOrDefault()

            navigateUrl = row.Item(ApplicationConstants.GrantPageCoreUrlCol)

        End If

        Response.Redirect(navigateUrl)

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Check child nodes exist"
    ''' <summary>
    ''' Check child nodes exist
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function checkChildExist(ByVal parentId As Integer, ByVal pageDt As DataTable)

        Dim query = (From dataRow In pageDt _
                Where dataRow.Field(Of Integer?)(ApplicationConstants.GrantPageParentPageCol) = parentId _
                Order By dataRow.Field(Of String)(ApplicationConstants.GrantOrderTextCol) Ascending _
                Select dataRow)

        Return IIf(query.Count > 0, True, False)

    End Function

#End Region

#Region "Remove Session Values"
    ''' <summary>
    ''' Remove Session Values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub removeSessionValues()
        Session.RemoveAll()
    End Sub
#End Region

#Region "Populate Menus"
    ''' <summary>
    ''' Populate Menus
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateMenus()
        Dim objDashboard As DashboardBL = New DashboardBL
        Dim userId As Integer = SessionManager.getUserEmployeeId()

        Dim repairsMenu As String = ApplicationConstants.RepairsMenu
        Dim resultDataset As DataSet = New DataSet
        objDashboard.getPropertyPageList(resultDataset, userId, repairsMenu)
        SessionManager.setPageAccessDataSet(resultDataset)

        Dim accessGrantedModulesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedModulesDt)
        Dim accessGrantedMenusDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedMenusDt)
        Dim accessGrantedPagesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)
        Dim randomPageDt As DataTable = resultDataset.Tables(ApplicationConstants.RandomPageDt)

        populateRSLModulesList(accessGrantedModulesDt)
        populateHeaderMenus(accessGrantedMenusDt)
        populatePages(accessGrantedPagesDt, randomPageDt)

    End Sub

#End Region

#Region "Populate Modules"
    ''' <summary>
    ''' Populate Modules
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateRSLModulesList(ByVal accessGrantedModulesDt As DataTable)

        rptRSLMenu.DataSource = accessGrantedModulesDt
        rptRSLMenu.DataBind()

    End Sub

#End Region

#Region "Populate Menu"
    ''' <summary>
    ''' Populate header menu
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateHeaderMenus(ByVal accessGrantedMenusDt As DataTable)
        menuHeader.Items.Clear()
        For Each parentItem As DataRow In accessGrantedMenusDt.Rows
            Dim item As MenuItem = New MenuItem()
            item.Text = parentItem(ApplicationConstants.MenuCol)
            item.Value = parentItem(ApplicationConstants.MenuIdCol).ToString()
            item.SeparatorImageUrl = PathConstants.SeparatorImagePath
            item.NavigateUrl = parentItem(ApplicationConstants.UrlCol)
            If (parentItem(ApplicationConstants.MenuCol).Equals(ApplicationConstants.RepairsMenu)) Then
                item.Selected = True
            End If

            If (String.IsNullOrEmpty(item.NavigateUrl) Or String.IsNullOrWhiteSpace(item.NavigateUrl)) Then
                item.Enabled = False
            End If

            menuHeader.Items.Add(item)
        Next
        menuHeader.DataBind()

    End Sub

#End Region

#Region "Populate Pages"
    ''' <summary>
    ''' Populate Pages
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePages(ByVal accessGrantedPagesDt As DataTable, ByVal randomPageDt As DataTable)

        Dim pageId As Integer
        Dim objDashboard As DashboardBL = New DashboardBL
        Dim pageFound As Boolean = False
        Dim pageLevelDt As DataTable = New DataTable()
        pageLevelDt.Columns.Add(ApplicationConstants.GrantPageIdCol, GetType(Int32))
        pageLevelDt.Columns.Add(ApplicationConstants.GrantPageAccessLevelCol, GetType(Int32))

        If (objDashboard.checkPageExist(accessGrantedPagesDt, pageId)) Then
            pageFound = True
        ElseIf (objDashboard.checkPageExist(randomPageDt, pageId)) Then

            Dim parentPageExpression As String = ApplicationConstants.RandomPageIdCol + " = " + Convert.ToString(pageId)
            Dim parentPageRows() As DataRow = randomPageDt.Select(parentPageExpression)
            If (IsNothing(parentPageRows(0)(ApplicationConstants.RandomParentPageCol)) Or _
                 IsDBNull(parentPageRows(0)(ApplicationConstants.RandomParentPageCol))) Then
                pageFound = False
            Else
                pageId = parentPageRows(0)(ApplicationConstants.RandomParentPageCol)
                pageFound = True
            End If

        End If

        If (pageFound) Then

            ' Getting Child Page and its level so that child level pages also visible
            Dim childPageExpression As String = ApplicationConstants.GrantPageParentPageCol + " = " + Convert.ToString(pageId)
            Dim sortOrder As String = ApplicationConstants.GrantOrderTextCol + " ASC"
            Dim childPageRows() As DataRow = accessGrantedPagesDt.Select(childPageExpression, sortOrder)

            If (childPageRows.Count > 0) Then

                Dim childAccessLevel As Integer = childPageRows(0)(ApplicationConstants.GrantPageAccessLevelCol)
                Dim childPageId As Integer = childPageRows(0)(ApplicationConstants.GrantPageIdCol)
                Dim dr As DataRow = pageLevelDt.NewRow()
                dr(ApplicationConstants.GrantPageIdCol) = childPageId
                dr(ApplicationConstants.GrantPageAccessLevelCol) = childAccessLevel
                pageLevelDt.Rows.Add(dr)
                pageLevelDt.AcceptChanges()

            End If

            ' Getting all parent pages and their levels if exist
            Dim allPageLevelsRetrieved As Boolean = False
            While Not allPageLevelsRetrieved

                Dim pageExpression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId)
                Dim pageRows() As DataRow = accessGrantedPagesDt.Select(pageExpression)
                Dim accessLevel As Integer = pageRows(0)(ApplicationConstants.GrantPageAccessLevelCol)

                Dim dr As DataRow = pageLevelDt.NewRow()
                dr(ApplicationConstants.GrantPageIdCol) = pageId
                dr(ApplicationConstants.GrantPageAccessLevelCol) = accessLevel
                pageLevelDt.Rows.Add(dr)
                pageLevelDt.AcceptChanges()

                If (IsDBNull(pageRows(0)(ApplicationConstants.GrantPageParentPageCol)) Or _
                    IsNothing(pageRows(0)(ApplicationConstants.GrantPageParentPageCol))) Then
                    allPageLevelsRetrieved = True
                Else
                    pageId = pageRows(0)(ApplicationConstants.GrantPageParentPageCol)
                End If

            End While

            Dim query = (From dataRow In pageLevelDt _
                       Order By dataRow.Field(Of Integer)(ApplicationConstants.GrantPageAccessLevelCol) Ascending _
                       Select dataRow)
            Dim resultPageDt As DataTable = pageLevelDt.Clone()
            resultPageDt = query.CopyToDataTable()

            rptPages.DataSource = resultPageDt
            rptPages.DataBind()

        Else

            Dim dr As DataRow = pageLevelDt.NewRow()
            dr(ApplicationConstants.GrantPageIdCol) = ApplicationConstants.NoPageSelected
            dr(ApplicationConstants.GrantPageAccessLevelCol) = ApplicationConstants.PageFirstLevel
            pageLevelDt.Rows.Add(dr)
            pageLevelDt.AcceptChanges()

            rptPages.DataSource = pageLevelDt
            rptPages.DataBind()

        End If


    End Sub

#End Region

#End Region

End Class