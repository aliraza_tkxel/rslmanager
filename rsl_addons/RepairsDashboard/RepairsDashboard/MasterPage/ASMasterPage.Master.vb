﻿Imports System
Imports RD_BusinessLogic
Imports RD_Utilities

Public Class ASMasterPage
    Inherits System.Web.UI.MasterPage

    Public messageClass As String = "topmessagesuccess"
    Private varBackgroundColor As String = "#e6e6e6"
    Private varBackgroundProperty As String = "background-color"
    Private varPageUrl As String
    Private objDashboad As DashboardBL = New DashboardBL()


#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        highLightMenuItems()
        ' lblLoggedInUserName.Text = SessionManager.getUserFullName()
        'lblDate.Text = Date.Today.ToString("dddd") + "," + Date.Today.Day.ToString() + " " + MonthName(Date.Today.Month) + " " + Date.Today.Year.ToString() + " " + DateTime.Now.ToString("hh:mm")       'DateTime.Now.ToString()
        ' Me.setLastLoginDate()

        If Not Page.IsPostBack Then
            populateRSLModulesList()
        End If

    End Sub

#End Region


#Region "Functions"

#Region "high Light Menu Items"

    Private Sub highLightMenuItems()
        lnkBtnAppDahsboard.Style.Add(varBackgroundProperty, varBackgroundColor)
        'updPanelSubAppServicing.Visible = True
        'ApplicationConstants.btnSearchClicked = True
        'Calendar Search Code Starts Here 
        varPageUrl = Request.Url.AbsolutePath.Substring(Request.Url.AbsolutePath.LastIndexOf("/") + 1)
        If varPageUrl = PathConstants.ReportsPath Then
            updPanelSubDashboard.Visible = True

            lnkBtnAppReports.Style.Add(varBackgroundProperty, varBackgroundColor)
            'If (Request.QueryString("ad") <> "Address" And Not IsNothing(Request.QueryString("ad"))) Then
            '    txtBoxAddress.Text = Request.QueryString("ad")
            'End If
            'If (Request.QueryString("pc") <> "Postcode" And Not IsNothing(Request.QueryString("pc"))) Then
            '    txtBoxPostCode.Text = Request.QueryString("pc")
            'End If
            'If (Request.QueryString("pa") <> 0 And Not IsNothing(Request.QueryString("pa"))) Then
            '    'ddlPatch.SelectedItem.Value = Request.QueryString("pa")
            'End If
            'If (Request.QueryString("sc") <> "Please Select Scheme" And Not IsNothing(Request.QueryString("sc"))) Then
            '    'ddlScheme.SelectedItem.Value = Request.QueryString("sc")
            'End If
            'If (Request.QueryString("dd") <> "Due Date" And Request.QueryString("dd") <> "" And Not IsNothing(Request.QueryString("dd"))) Then
            '    txtBoxDueDate.Text = Request.QueryString("dd")
            'End If
            'If (Request.QueryString("ft") <> 0 And Not IsNothing(Request.QueryString("ft"))) Then
            '    ddlFuelType.SelectedItem.Value = Request.QueryString("ft")
            'End If
            'If (Not IsPostBack) Then
            '    Me.getPatches(ddlPatch)
            '    Me.getSchemes(ddlScheme, 0)
            'End If
        End If
        'Calendar Search Code Ends Here


    End Sub
#End Region


#End Region


    Private Sub removeSessionValues()
        Session.RemoveAll()
    End Sub

    




End Class