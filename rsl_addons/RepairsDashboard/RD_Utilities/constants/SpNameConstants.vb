﻿Imports System
Namespace RD_Utilities

    Public Class SpNameConstants

#Region "Master Page"

        Public Shared GetPropertyMenuPages As String = "P_GetPropertyMenuPages"

#End Region

#Region "Reports"

        Public Shared GetCompletedFaultDetails As String = "RD_GetCompletedFaultDetail"
        Public Shared GetCompletedFaultImages As String = "FL_GetCompletedFaultImages"
        Public Shared GetAllSchemes As String = "PLANNED_GetAllSchemes"
        Public Shared GetFollowOnWorksDetails As String = "FL_GetFollowOnWorksDetails"
        Public Shared GetOperativeList As String = "FL_GetOperativeList"
        Public Shared GetSchemeDropDown As String = "PDR_GetDevelopmentScheme"
        Public Shared GetBlockDropDownBySchemeId As String = "PDR_GetBlocks"
        Public Shared GetInProgressFaultList As String = "RD_GetInProgressFaultList"
        Public Shared GetReportedFaultList As String = "RD_GetReportedFaultList"
        Public Shared GetCompletedFaultList As String = "RD_GetCompletedFaultList"
        Public Shared GetPriorityAndCompletionList As String = "RD_GetPriorityAndCompletionList"
        Public Shared GetFaultsOverdueList As String = "RD_GetFaultsOverdueList"
        Public Shared GetEmergencyFaultList As String = "RD_GetEmergencyFaultList"
        Public Shared GetNoEntryList As String = "FL_GetNoEntryList"
        Public Shared GetFollowOnList As String = "FL_GetFollowOnList"
        Public Shared GetSubcontractorList As String = "FL_GetSubcontractorList"
        Public Shared GetRecallList As String = "FL_GetRecallList"
        Public Shared GetFaultToBeArrangeList As String = "RD_GetFaultsToBeArrangeList"

#End Region

#Region "Dashboard"

        Public Shared GetRSLModulesList As String = "P_GetRSLModulesList"
        Public Shared GetOperativesReserveToday As String = "RD_GetOperativesReserveToday"
        Public Shared GetTodaysAppointmentsOfOperative As String = "RD_GetTodaysAppointmentsOfOperative"
        Public Shared GetFaultStatusList As String = "RD_GETFAULTSTATUS"

#End Region

#Region "Login"

        Public Shared GetEmployeeById As String = "FL_GetEmployeeById"

#End Region

    End Class
End Namespace