﻿Namespace RD_Utilities

    Public Class PathConstants

#Region "URL Constants"

        Public Const ReportsPathHighlight As String = "ReportsArea.aspx"
        Public Const ReportsPath As String = "~/Views/Reports/ReportsArea.aspx"
        Public Const RearrangCurrentFault As String = "~/BRSFaultLocator/Views/Faults/ReArrangingCurrentFault.aspx"
        Public Const ReportAreaSrcVal As String = "reportArea"
        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const SearchPath As String = "~/Views/Search/SearchProperty.aspx"
        Public Const DashboardPath As String = "~/Views/Dashboard/Dashboard.aspx"
        Public Const CompletedJobsheetPath As String = "~/Views/JobSheets/CompletedJobsheet.aspx"
        Public Const BRSFaultLocator As String = "~/../BRSFaultLocator/Bridge.aspx?schedule=yes&jsn="
        Public Const BRSFaultLocatorCalender As String = "~/../BRSFaultLocator/Bridge.aspx?cal=yes"
        Public Const SeparatorImagePath As String = "../Images/PropertyModule/sep.jpg"
        Public Const BridgePath As String = "~/Bridge.aspx"
        'Public Const AccessDeniedPath As String = "  ~/../../../../BRSFaultLocator/Bridge.aspx?accessDenied=yes"
        Public Const AccessDeniedPath As String = "~/Views/General/AccessDenied.aspx"
        Public Const FaultSchedulingPath As String = "~/../BRSFaultLocator/Bridge.aspx?pg=faultBasket"
        Public Const FaultSchedulingPathAlternate As String = "~/../../../../BRSFaultLocator/Bridge.aspx?pg=faultBasket"
        Public Const SbFaultSchedulingPathAlternate As String = "~/../../../../BRSFaultLocator/Bridge.aspx?pg=sbfaultBasket"

#End Region

#Region "Query String Constants"

        Public Const Create As String = "Create"
        Public Const Jsn As String = "jsn"
        Public Const Src As String = "src"
        Public Const Report As String = "rpt"
        Public Const ReportView As String = "report"
        Public Const Yes As String = "yes"

#End Region

#Region "appSetting Keys"
        Public Const NoEntryReportURL As String = "NoEntryReportURL"
        Public Const FollowOnWorksReportURL As String = "FollowOnWorksReportURL"
        Public Const ReCallReportURL As String = "ReCallReportURL"
        Public Const AssignToContractorReportURL As String = "AssignToContractorReportURL"
        Public Const FaultsOverDueReportURL As String = "FaultsOverDueReportURL"
        Public Const CompletedReportURL As String = "CompletedReportURL"
        Public Const EmergencyReportURL As String = "EmergencyReportURL"

#End Region

    End Class
End Namespace