﻿Imports System

Namespace RD_Utilities


    Public Class ViewStateConstants

#Region "Page Sort Constants"
        Public Shared SortDirection As String = "SortDirection"
        Public Shared SortExpression As String = "SortExpression"
        Public Shared Ascending As String = "Ascending"
        Public Shared Descending As String = "Descending"
        Public Shared DefaultPageNumber As String = "10"
        Public Shared DefaultPageSize As String = "10"
        Public Shared PageSortBo As String = "PageSortBo"
        Public Shared TotalPageNumbers As String = "TotalPageNumbers"
        Public Shared CurrentPageNumber As String = "CurrentPageNumber"
#End Region

#Region "General "
        Public Shared GridDataSet As String = "GridDataSet"
        Public Shared PropertyId As String = "PropertyId"
        Public Shared AppointmentId As String = "AppointmentId"
        Public Shared ApplianceId As String = "ApplianceId"
        Public Shared ResultDataSet As String = "ResultDataSet"
        Public Shared SlideIndex As String = "SlideIndex"
        Public Shared PopupSlideIndex As String = "PopupSlideIndex"
        Public Shared RedirectTempCount As String = "RedirectTempCount"
        Public Shared FollowOnDataSet As String = "FollowOnDataSet"
        Public Shared SubcontractorDataSet As String = "SubcontractorDataSet"
        Public Shared NoEntryDataSet As String = "NoEntryDataSet"
        Public Shared RecallDataSet As String = "RecallDataSet"
        Public Shared InProgressDataSet As String = "InProgressDataSet"
        Public Shared ReportedDataSet As String = "ReportedDataSet"
        Public Shared FaultToBeArrangeDataSet As String = "FaultToBeArrangeDataSet"
        Public Shared EmergencyDataSet As String = "EmergencyDataSet"
        Public Shared CompleteDataSet As String = "CompleteDataSet"
        Public Shared PriorityDataSet As String = "PriorityDataSet"
        Public Shared FaultsOverdueDataSet As String = "FaultsOverdueDataSet"
        Public Shared Search As String = "Search"
        Public Shared TotalCount As String = "TotalCount"
        Public Shared CustomerId As String = "CustomerId"
        Public Shared OperativeId As String = "OperativeId"
        Public Shared StatusId As String = "StatusId"
        Public Shared FinancialYear As String = "FinancialYear"
        Public Shared ConfirmedJsnList As String = "ConfirmedJsnList"
        Public Shared LastBlockId As String = "LastBlockId"

        Public Shared VirtualItemCount As String = "VirtualItemCount"

        'Changes By Aamir Waheed - May 08,2013 - Start
        ''Public Shared Email As String = "Email"
        Public Shared IsEmailSent As String = "IsEmailSent"
        'Changes By Aamir Waheed - May 08,2013 - End
#End Region

    End Class
End Namespace