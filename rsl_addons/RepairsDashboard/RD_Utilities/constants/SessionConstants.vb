﻿Imports System

Namespace RD_Utilities

    Public Class SessionConstants

#Region "User Session"

        Public Shared ConSessionUserId As String = "SESSION_USER_ID"
        Public Shared RepairsDashboardUserId As String = "RepairsDashboardUserId"
        Public Shared UserFullName As String = "UserFullName"
        Public Shared EmployeeId As String = "UserEmployeeId"
        Public Shared LoggedInUserType As String = "LoggedInUserType"

#End Region

        Public Shared PreviousOperativeId As String = "PreviousOperativeId"
        Public Shared PropertyId As String = "PropertyId"
        Public Shared CustomerId As String = "CustomerId"
        Public Shared AppointmentSummaryFaultList As String = "AppointmentSummaryFaultList"
        Public Shared AppointmentData As String = "AppointmentData"
        Public Shared JobSheetNumber As String = "JobSheetNumber"
        Public Shared SubcontractorJobSheetIndex As String = "SubcontractorJobSheetIndex"
        Public Shared SubcontractorCurrentFaultBO As String = "SubcontractorCurrentFaultBO"
        Public Shared TemporaryFaultBo As String = "TemporaryFaultBo"
        Public Shared TemporaryFaultIds As String = "TemporaryFaultIds"
        Public Shared TempFaultIds As String = "TempFaultIds"
        Public Shared CustomerData As String = "CustomerData"
        Public Shared FollowOnDataSet As String = "FollowOnDataSet"
        Public Shared NoEntryDataSet As String = "NoEntryDataSet"
        Public Shared RecallDataSet As String = "RecallDataSet"
        Public Shared FaultLogId As String = "FaultLogId"
        Public Shared OriginalFaultLogId As String = "OriginalFaultLogId"
        Public Shared IsRecall As String = "isRecall"
        Public Shared RedirectCheck As String = "redirectCheck"
        Public Shared FaultId As String = "FaultId"
        Public Shared Source As String = "source"
        Public Shared FollowOnPageSortData As String = "FollowOnPageSortData"
        Public Shared GroupBy As String = "groupBy"
        Public Shared TempFaultDs As String = "TempFaultDs"
        Public Shared AvailableOperativesDs As String = "AvailableOperativeDs"
        Public Shared SelectedTempFaultDv As String = "SelectedTempFaultDv"
        Public Shared ConfirmedFaults As String = "ConfirmedFaultsDv"
        Public Shared ConfirmedAppointmentIdList As String = "ConfirmedAppointmentIdList"
        Public Shared Jsn As String = "Jsn"
        Public Shared HashTable As String = "hashTable"
        Public Shared AppointmentId As String = "AppointmentId"
        Public Shared PopupOpened As String = "PopupOpened"
        Public Shared isSuccessAdded As String = "isSuccessAdded"
        Public Shared weeklyAptCalendarDT As String = "weeklyAptCalendarDT"
        Public Shared IsAppointmentRearrange As String = "IsAppointmentRearrange"
        Public Shared PathsData As String = "PathsData"
        Public Shared ddlData As String = "ddlData"
        Public Shared OperativesBlock As String = "operativesBlockScreen7"
        Public Shared CalculatedSlots As String = "CalculatedSlots"
        Public Shared SFSCalendar As String = "SFSCalendar"
        Public Shared TempFaultAptBlockList As String = "TempFaultAptBlockList"
        Public Shared ConfirmFaultAptBlockList As String = "ConfirmFaultAptBlockList"
        Public Shared PropertyDistance As String = "PropertyDistance"
    End Class

End Namespace


