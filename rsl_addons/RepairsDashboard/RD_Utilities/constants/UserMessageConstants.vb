﻿
Namespace RD_Utilities


    Public Class UserMessageConstants

#Region "not categorized"
        Public Shared InvalidRecallJsnId As String = "Invalid JSN number."
        Public Shared InvalidPropertyCustomerId As String = "Invalid Property Id Or Customer Id in query string."
        Public Shared InvalidCustomerId As String = "Invalid Customer Id in session."
        Public Shared InvalidSubcontractorJobSheetIndex As String = "Invalid Subcontractor JobSheet index."
        Public Shared InvalidGroupByPropFaults As String = "Invalid input parameters (group by, property , faults)"
        Public Shared InvalidJobSheetNumber As String = "Invalid Job Sheet Number"
        Public Shared NoJobSheetFound As String = "No JobSheet found."
        Public Shared InvalidCustomerData As String = "Customer data not available."
        Public Shared InvalidInput As String = "Invalid selection or input."
        Public Shared RecordNotFound As String = "Record not found."
        Public Shared ProblemLoadingData As String = "Problem loading data."
        Public Shared ErrorDataBaseConnection As String = "Error while connecting with database."
        Public Shared NoRecordFound As String = "No record found."
        Public Shared InvalidUrl As String = "Url is invalid."
        Public Shared UserDoesNotExist As String = "You don't have access to Repair DashBoard. Please contant administrator."
        Public Shared UsersAccountDeactivated As String = "Your access to appliance servicing has been de-activiated. Please contant administrator."
        Public Shared LoginRslManager As String = "Please use the login from rsl manager."
        Public Shared ErrorDocumentUpload As String = "Error occured while uploading the document.Please try again."
        Public Shared EngNotFound As String = "Engineer not found."
        Public Shared EnterReason As String = "Please enter a reason for cancellation."
        Public Shared CancellationError As String = "You have already cancelled these faults."
        Public Shared ClearPanelMessage As String = ""
        Public Shared InvalidJsnList As String = "Invalid JSN List."
        Public Shared InvalidFaultData As String = "Invalid Fault Data."
        Public Shared JobsNotSame As String = "The contractor jobs or the non contractor jobs first cannot be scheduled together. Both these types can not be scheduled together."
        Public Shared NoTempFaultData As String = "No temp fault data in session!"
        Public Shared InvalidTelephone As String = "Invalid Telephone number."
        Public Shared InvalidMobile As String = "Invalid Mobile number."
        Public Shared InvalidEmail As String = "Invalid Email address."
        Public Shared JSNNotAvailable As String = "Job Sheet Number not available."
        Public Shared DurationExceeded As String = "Total time required by faults fixation exceeds the operatives daily timing hours."
        Public Shared DeleteAllFault As String = "Something went wrong in the database. Delete failed in FL_DELETE_ALL_TEMP_FAULT."
        Public Shared AssignedToSubcontractor As String = "Fault successfully assigned to Subcontractor."
        Public Shared InvalidSrc As String = "Invalid Src"
        Public Shared SrcDoesNotExist As String = "Src does not exist"
        Public Shared OperativeNotFound As String = "Unable to find operative"
        Public Shared DurationError As String = "Duration of selected faults should not be greater than 9 hours."
        Public Shared AppointmentIdNotProvided As String = "Inavlid Appointment Id provided in query string."
        Public Shared PushNotificationFailed As String = "Appointment confirmed but failed to push appointment notification."
        Public Shared CustomerRecordNotFound As String = "Customer Record not found"
#End Region

#Region "Generic Messages"
        Public Shared RecordNotExist As String = "No record exists"
#End Region

#Region "Fault"
        Public Shared FaultDoesNotExist As String = "Fault not found"
        Public Shared FaultNotCancelled As String = "Fault could not be cancelled"
        Public Shared FaultDeletedSuccessfuly As String = "Fault Deleted Successfuly"
        Public Shared SelectContractor As String = "Please select contractor"
        Public Shared SelectFault As String = "Please select fault"

#End Region

#Region "Report"
        Public Shared FilterReportNotFound As String = "Record not found. Please select different scheme and block."
        Public Shared ReportNotFound As String = "Reports not found. Please enter different search text."
        Public Shared InvalidDate As String = "To date must not be greater that From date."
        Public Shared InvalidStartDate As String = "Please enter valid Start date."
        Public Shared InvalidEndDate As String = "Please enter valid End date."
        Public Shared InvalidStartEndDate As String = "Start date should be less than End date."
#End Region

#Region "Property"
        Public Shared InvalidPropertyId As String = "Property Id is invalid."
#End Region

#Region "Calendar Popup"
        Public Shared SelectActionDate As String = "Please select date."
#End Region

#Region "Users"

        Public Shared UserSavedSuccessfuly As String = "User Saved Successfuly"
        Public Shared userAlreadyExists As String = "User already exists"
        Public Shared UserNameNotValid As String = "User name is not valid"
        Public Shared ErrorUserUpdate As String = "Error Updating Record"
        Public Shared ErrorUserInsert As String = "Error Inserting Record"
        Public Shared ErrorNoTradeExist As String = "No Trade Exist Against This Fault"
#End Region

#Region "Appointments"
        Public Shared SaveAppointmentFailed As String = "Some error occured in saving appointments"
        Public Shared AppointmentConfirmationFailed As String = "Some error occured in confirming appointments"
        Public Shared RearrangeAppointmentFailed As String = "Some error occured in Rearrange appointments"
        Public Shared AppointmentSavedEmailError As String = "Appointment saved but unable to send email due to : "
        Public Shared NoOperativesExistWithInTime As String = "There are currently no operatives available to attend the reported fault within recommended response time. Either remove the ‘Due Date’ filter, or go back to the fault basket and assign the fault to a contractor"
#End Region

#Region "Add Appointment Popup"
        Public Shared SaveAppointmentSuccessfully As String = "Appointment is saved successfully."
        Public Shared RearrangeAppointmentSuccessfully As String = "Appointment is rearranged successfully."
        Public Shared SaveAmendAppointmentSuccessfully As String = "Appointment is amended successfully."
        Public Shared AppointmentTime As String = "Appointment start time shall always be less than end time."
        Public Shared duplicateAppointmentTime As String = "Another appointment already exist in selected Start/End time."
#End Region

#Region "Customer Detail Custom Control Constants"
        'Change - By Behroz - 19/02/2013 - Start
        Public Shared CustomControlErrorMsg As String = "Customer data does not exist."
        'Change - By Behroz - 19/02/2013 - End
#End Region

#Region "Sub Contractor Report/ Update Popup(s)"

#Region "Sub Contractor Report"
        Public Shared FaultStatusUpdateSucessful As String = "Fault Status Updated Sucessfully."
#End Region

#Region "Save Notes Popup"

        'Headers Start

        Public Shared PopupHeaderSaveNotesCancelFault As String = "Cancel Fault"
        Public Shared PopupHeaderSaveNotesNoEntry As String = "No Entry"
        Public Shared PopupHeaderSaveNotesCompleteFault As String = "Complete Fault"

        'Headers End

        'Heading Notes Start

        Public Shared PopupHeadingSaveNotesCancelFault As String = "Enter the reason for cancellation below:"
        Public Shared PopupHeadingSaveNotesNoEntry As String = "Enter the reason for no entry below:"
        Public Shared PopupHeadingSaveNotesCompleteFault As String = "Enter the notes for completion below:"

        'Heading Notes End

        'Save Message Notes Start

        Public Shared PopupSaveMsgSaveNotesCancelFault As String = "The fault has been canceled."
        Public Shared PopupSaveMsgSaveNotesNoEntry As String = "The fault has been set to no entry."
        Public Shared PopupSaveMsgSaveNotesCompleteFault As String = "The fault has been completed."

        'Save Message Notes End

        Public Shared PopupNotesErrorSaveNotes As String = "Please provide notes."

#End Region

#End Region

#Region "Fault Management Report / Popup(s)"

        Public Shared TradeAlreadyAdded = "The trade has already been added."

#End Region

#Region "Job Sheet Summary Sub Contractor"
        Public Shared FaultSavedinvalidEmail As String = "Fault saved but unable to send email due to invalid email address"
        Public Shared FaultSavedEmailError As String = "Fault saved but unable to send email due to : "

#End Region

#Region " Tenant Information"

        Public Shared noTenantInformationFound As String = "No Tenant Information Found."

#End Region

#Region "Invalid Page Number"
        Public Shared InvalidPageNumber = "Kindly Provide a valid page number."
#End Region

    End Class
End Namespace

