﻿Imports System
Imports System.Text

Namespace RD_Utilities
    Public Class ApplicationConstants

#Region "General"
        Public Shared DropDownDefaultValue As String = "-1"
        Public Shared Title As String = "Title"
        Public Shared LetterPrefix As String = "SL_"
        Public Shared DocPrefix As String = "Doc_"
        Public Shared LetterWord As String = "Letter"
        Public Shared DocumentWord As String = "Document"
        Public Shared DefaultDropDownDataValueField As String = "id"
        Public Shared DefaultDropDownDataTextField As String = "title"
        Public Shared RecentFaultsDtName As String = "RecentFaults"
        Public Shared CustomerDtName As String = "Customer"
        Public Shared PropertyDtName As String = "Property"
        Public Shared TempFaultDataTable As String = "TempFaultDataTable"
        Public Shared ConfirmFaultDataTable As String = "ConfirmFaultDataTable"
        Public Shared RecallFaultsDataTable As String = "RecallFaultsDataTable"
        Public Shared CurrentFaultsDataTable As String = "CurrentFaultsDataTable"
        Public Shared GasAppointmentssDataTable As String = "GasAppointmentssDataTable"
        Public Shared Create As String = "Create"
        Public Shared AppointmentStartTime As String = " 8:00"
        Public Shared AppointmentStartHour As Double = 8
        Public Shared AppointmentEndHour As Double = 17.1

#End Region

#Region "Master Layout"

        Public Shared ConWebBackgroundColor As String = "#e6e6e6"
        Public Shared ConWebBackgroundProperty As String = "background-color"

        Public Shared MenuCol As String = "Menu"
        Public Shared UrlCol As String = "Url"
        Public Shared MenuIdCol As String = "MenuId"
        Public Shared PageFileNameCol As String = "PageFileName"
        Public Shared NoPageSelected As Integer = -1
        Public Shared PageFirstLevel As Integer = 1

        Public Shared ServicingMenu As String = "Servicing"
        Public Shared ResourcesMenu As String = "Resources"
        Public Shared PropertyModuleMenu As String = "Search"
        Public Shared RepairsMenu As String = "Repairs"
        Public Shared MultipleMenus As String = ""

        Public Shared AccessGrantedModulesDt As String = "AccessGrantedModulesDt"
        Public Shared AccessGrantedMenusDt As String = "AccessGrantedMenusDt"
        Public Shared AccessGrantedPagesDt As String = "AccessGrantedPagesDt"
        Public Shared RandomPageDt As String = "RandomPageDt"

        Public Shared GrantModulesModuleIdCol As String = "MODULEID"

        Public Shared GrantMenuModuleIdCol As String = "ModuleId"
        Public Shared GrantMenuMenuIdCol As String = "MenuId"
        Public Shared GrantMenuUrlCol As String = "Url"
        Public Shared GrantMenuDescriptionCol As String = "Menu"

        Public Shared GrantPageParentPageCol As String = "ParentPage"
        Public Shared GrantPageAccessLevelCol As String = "AccessLevel"
        Public Shared GrantPageQueryStringCol As String = "PageQueryString"
        Public Shared GrantPageFileNameCol As String = "PageFileName"
        Public Shared GrantPageUrlCol As String = "PageUrl"
        Public Shared GrantPageCoreUrlCol As String = "PageCoreUrl"
        Public Shared GrantPagePageNameCol As String = "PageName"
        Public Shared GrantPageIdCol As String = "PageId"
        Public Shared GrantPageMenuIdCol As String = "MenuId"
        Public Shared GrantPageModuleIdCol As String = "ModuleId"
        Public Shared GrantOrderTextCol As String = "ORDERTEXT"
        Public Shared GrantPageMenuNameCol As String = "MenuName"

        Public Shared RandomParentPageCol As String = "ParentPage"
        Public Shared RandomAccessLevelCol As String = "AccessLevel"
        Public Shared RandomQueryStringCol As String = "PageQueryString"
        Public Shared RandomFileNameCol As String = "PageFileName"
        Public Shared RandomPageNameCol As String = "PageName"
        Public Shared RandomPageIdCol As String = "PageId"

#End Region

#Region "Schedule Calendar"
        Public Shared daysArray() As String = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}
        Public Shared appointmentTime() As String = {"00:00", "00:15", "00:30", "00:45", "01:00", "01:15", "01:30", "01:45", "02:00", "02:15", "02:30", "02:45", "03:00", "03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30", "05:45", "06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45"}
        Public Shared startDate As Date = Date.Today
        Public Shared endDate As Date = Date.Today
        Public Shared weekDays As Integer
        Public Shared isSuccessAdded As Boolean = False
        Public Shared isMoved As Boolean = False
        Public Shared isPopupOpened As Boolean = False
        Public Shared calendarAppointment As String = "calendarAppointment"
        Public Shared faultJSN As String = "faultJSN"

#End Region

#Region "Page Sort"

        Public Shared Ascending As String = "ASC"
        Public Shared Descending As String = "DESC"
        Public Shared DefaultPageNumber As String = 1
        Public Shared DefaultPageSize As String = 5
        Public Shared PageSortBo As String = "PageSortBo"
#End Region

#Region "Page Names"
        'The value of these should be same as those are in database table AS_Pages
        Public Shared DashboardPage As String = "Dashboard"
        Public Shared ResourcesPage As String = "Resources"
        Public Shared SchedulingPage As String = "Scheduling"
        Public Shared UsersPage As String = "Users"
        Public Shared StatusPage As String = "Status"
        Public Shared LetterPage As String = "Letter"
        Public Shared AccessPage As String = "Access"
        Public Shared CalendarPage As String = "Calendar"
        Public Shared PropertyModulePage As String = "Property Module"
#End Region

#Region "Basket Selected Fault Ids"
        Public Shared faultIdsList As New ArrayList
        Public Shared TempFaultIds As New Int32

#End Region

#Region "Schedular Search"
        Public Shared JobSheetNumber As String = "JobSheetNumber"
#End Region

#Region "Fault Search/Manegement"

#Region "Paging and Sorting Constants"
        Public Shared faultManagementSortBy As String = "FaultID"
        Public Shared faultManagementResultPerPage As Integer = 10
        Public Shared faultManagementInitialPageIndex As Integer = 0
#End Region

#Region "Trades GridView\Table Column Name For Add Fault Page/Popup"
        Public Shared faultTradeIdColumn As String = "FaultTradeId"
        Public Shared tradeIdColumn As String = "TradeId"
        Public Shared tradeNameColumn As String = "TradeName"
#End Region

#End Region

#Region "Job Sheet Summary Sub Contractor Update"

        'Table names
        Public Shared jobSheetSummaryDetailTable As String = "JobSheetSummaryDetailTable"
        Public Shared jobSheetSummaryAsbestosTable As String = "JobSheetSummaryAsbestosTable"

#End Region

#Region "Recall Details By Fault Log ID"

        'Table Names
        Public Shared PropertyDetailsDT As String = "PropertyDetailsDT"
        Public Shared OriginalFaultDetailsDT As String = "OriginalFaultDetailsDT"
        Public Shared RecallFaultsDetailsDT As String = "RecallFaultsDetailsDT"

#End Region

#Region "Intellegent Scheduling Principle"
        Public Shared EmergencyPriorityName As String = "Emergency"
        Public Shared UrgentPriorityName As String = "Urgent"
#End Region

#Region "Logged In User Type"
        Public Enum LoggedInUserTypes
            Manager = 1
            Scheduler = 2
        End Enum

#End Region

#Region "Dashboard"

        Public Shared HashTableKeyNoEntry As String = "No_Entry"
        Public Shared HashTableKeyFOLLOWON As String = "FOLLOWON"
        Public Shared HashTableKeyInProgress As String = "InProgress"
        Public Shared HashTableKeyAssignToContractor As String = "AssignToContractor"
        Public Shared HashTableKeyCompletedFaults As String = "CompletedFaults"
        Public Shared HashTableKeyReCall As String = "ReCall"
        Public Shared HashTableKeyEmergencyfaults As String = "Emergencyfaults"
        Public Shared HashTableKeyFaultsOverDue As String = "FaultsOverDue"
        Public Shared HashTableKeyReportedFaults As String = "ReportedFaults"

        Public Shared InspectionStatusDropDownNoEntries As Integer = 1
        Public Shared InspectionStatusDropDownLegalProceedings As Integer = 2
        Public Shared InspectionStatusDropDownExpired As Integer = 3
        Public Shared InspectionStatusDropDownAppointmentsToBeArranges As Integer = 4


#End Region

#Region "Report Area Heading"

        Public Shared CompletedFaultsReport As String = "Completed Faults Report"
        Public Shared EmergencyFaultsReport As String = "Emergency Faults Report"
        Public Shared OverdueFaultsReport As String = "Overdue Faults Report"
        Public Shared InProgressFaultsReport As String = "In Progress Faults Report"
        Public Shared ReportedFaultsReport As String = "Reported Faults Report"
        Public Shared PriorityAndCompletionReport As String = "Priority & Completion"
        Public Shared FaultsToBeArrangeReport As String = "Faults To Be Arrange Report"

#End Region

#Region "Report"

        Public Shared isFullList As Boolean = True
        Public Shared propertyFault As String = "Fault"
        Public Shared sbFault As String = "SbFault"

#End Region


    End Class
End Namespace


