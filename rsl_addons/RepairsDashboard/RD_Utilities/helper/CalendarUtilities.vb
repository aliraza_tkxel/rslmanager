﻿Imports System.Data
Imports System.Net
Imports System.IO
Imports System.Xml
Namespace RD_Utilities
    Public Class CalendarUtilities

#Region "Create Columns for Datatable "
        Public Sub createColumns(ByVal colName As String, ByRef varWeeklyDT As DataTable)
            Dim Name As DataColumn = New DataColumn(colName)
            Name.DataType = System.Type.GetType("System.String")
            varWeeklyDT.Columns.Add(Name)

        End Sub
#End Region

#Region "Calculate Distance"
        Public Function CalculateDistance(ByVal propertyFrom As String, ByVal propertyTo As String) As String
            'Pass request to google api with orgin and destination details
            Dim distance As String = "0"
            Try
                Dim request As HttpWebRequest = DirectCast(WebRequest.Create(("http://maps.googleapis.com/maps/api/distancematrix/xml?origins=" + propertyFrom & "&destinations=") + propertyTo & "&mode=Car&units=imperial&sensor=false"), HttpWebRequest)
                Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
                'Get response as stream from httpwebresponse
                Dim resStream As New StreamReader(response.GetResponseStream())
                Dim distanceXml As New XmlDocument()
                distanceXml.LoadXml(resStream.ReadToEnd())

                'Get DistanceMatrixResponse element and its values
                Dim xnList As XmlNodeList = distanceXml.SelectNodes("/DistanceMatrixResponse")
                For Each xn As XmlNode In xnList
                    If xn("status").InnerText.ToString() = "OK" Then
                        distance = distanceXml.DocumentElement.SelectSingleNode("/DistanceMatrixResponse/row/element/distance/text").InnerText
                        If (distance.Contains("ft")) Then
                            '    distance = Convert.ToString(Math.Round(Int(distance.Replace("km", "")) / 1.609), 2)
                            'Else
                            distance = "0"
                        End If
                    End If
                Next
            Catch ex As Exception

            End Try
            Return distance.Replace("mi", "")

        End Function
#End Region

#Region "Formating Date with suffix like 1st, 2nd, 3rd"
        Public Function FormatDayNumberSuffix(ByVal calendarDay As String) As String
            Select Case calendarDay
                Case 1, 21, 31
                    calendarDay = calendarDay + "st"
                Case 2, 22
                    calendarDay = calendarDay + "nd"
                Case 3, 23
                    calendarDay = calendarDay + "rd"
                Case Else
                    calendarDay = calendarDay + "th"
            End Select

            Return calendarDay
        End Function
#End Region

#Region "Calculate Last Monday Of The Month"
        Public Sub calculateMonday()
            For dayCount = 0 To 10
                If (DateAdd("d", -(dayCount), ApplicationConstants.startDate).ToString("dddd") = "Monday") Then
                    ApplicationConstants.startDate = DateAdd("d", -(dayCount), ApplicationConstants.startDate)
                    ApplicationConstants.weekDays = ApplicationConstants.weekDays + dayCount
                    dayCount = 10
                End If
            Next
        End Sub
#End Region

#Region "Calculate Last Sunday of the Month"
        Public Sub calculateSunday()
            For dayCount = 0 To 10
                If (DateAdd("d", dayCount, ApplicationConstants.endDate).ToString("dddd") = "Sunday") Then
                    ApplicationConstants.endDate = DateAdd("d", dayCount, ApplicationConstants.endDate)
                    ApplicationConstants.weekDays = ApplicationConstants.weekDays + dayCount
                    dayCount = 10
                End If
            Next
        End Sub
#End Region

#Region "Format Date Function"

        'this utility functions converts date from mm/dd/yyyy formate to dd/mm/yyyy
        Public Shared Function FormatDate(ByVal str As String) As String
            If Not str.Equals("") And Not (str Is Nothing) Then
                Dim strArr As String() = str.Split(" ")
                Return Convert.ToDateTime(strArr(0)).ToString("dd/MM/yyyy")
            Else
                Return ("")
            End If
        End Function

#End Region

    End Class
End Namespace