﻿Imports System.Web.UI.WebControls
Imports System.Drawing

Namespace RD_Utilities


    Public Class ExceptionHelper

#Region "Exception"
        Public Shared IsExceptionLogged As Boolean = False
        Public Shared IsError As Boolean = False
        Public Shared ExcpetionMessage As String
#End Region

#Region "set Message"

        Public Shared Sub setMessage(ByRef lblMessage As Label, ByRef pnlMessage As Panel, ByVal str As String, ByVal isError As Boolean)
            If isError = True Then
                lblMessage.Text = str
                lblMessage.ForeColor = Color.Red
                lblMessage.Font.Bold = True

            Else

                lblMessage.Text = str
                lblMessage.ForeColor = Color.Green
                lblMessage.Font.Bold = True
            End If
            pnlMessage.Visible = True

        End Sub
#End Region

#Region "reset Message"
        Public Shared Sub resetMessage(ByRef lblMessage As Label, ByRef pnlMessage As Panel)
            lblMessage.Text = String.Empty
            pnlMessage.Visible = False
            ExceptionHelper.IsError = False
        End Sub
#End Region

    End Class
End Namespace