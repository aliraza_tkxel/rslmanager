﻿Imports System.Globalization
Imports System.Web.HttpContext
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Configuration

Namespace RD_Utilities
    Public Class GeneralHelper

#Region "get Path Data Src"

        Public Shared Function getSrc(ByVal pathSrcVal As String)
            Dim pathsData As New Dictionary(Of String, String)
            pathsData = SessionManager.getPathsData()
            If pathsData.ContainsKey(pathSrcVal) Then
                Return "?" + PathConstants.Src + "=" + pathSrcVal
            Else
                Return "?" + PathConstants.Src + "=" + ""
            End If

        End Function
#End Region

#Region "Set Grid Footer"

        Public Shared Sub setGridFooter(ByRef grd As GridView, ByVal PageNumber As Integer, ByVal totalCount As Integer)
            Dim PageStartCount As Integer = 0
            Dim PageEndCount As Integer = 0
            PageStartCount = (grd.PageSize * (PageNumber - 1)) + 1
            PageEndCount = grd.PageSize * PageNumber

            If PageEndCount > totalCount Then
                PageEndCount = totalCount
            End If
            If Not IsNothing(grd.FooterRow) Then
                grd.FooterRow.Cells(2).Text = "Records " + PageStartCount.ToString + " to " + PageEndCount.ToString + " of " + totalCount.ToString
            End If

        End Sub

#End Region

#Region "Set Grid View Pager"

        Public Shared Sub setGridViewPager(ByRef pnlPagination As Panel, ByVal objPageSortBO As PageSortBO)
            pnlPagination.Visible = False

            If objPageSortBO.TotalRecords >= 1 Then
                pnlPagination.Visible = True

                Dim pageNumber As Integer = objPageSortBO.PageNumber
                Dim pageSize As Integer = objPageSortBO.PageSize
                Dim TotalRecords As Integer = objPageSortBO.TotalRecords
                Dim totalPages As Integer = objPageSortBO.TotalPages

                Dim lnkbtnPagerFirst As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerFirst"), LinkButton)
                Dim lnkbtnPagerPrev As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerPrev"), LinkButton)
                Dim lnkbtnPagerNext As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerNext"), LinkButton)
                Dim lnkbtnPagerLast As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerLast"), LinkButton)

                lnkbtnPagerFirst.Enabled = (pageNumber <> 1)
                lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled

                lnkbtnPagerPrev.Enabled = lnkbtnPagerFirst.Enabled
                lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled

                lnkbtnPagerNext.Enabled = (pageNumber <> totalPages)
                lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled

                lnkbtnPagerLast.Enabled = lnkbtnPagerNext.Enabled
                lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled

                Dim lblPagerCurrentPage As Label = DirectCast(pnlPagination.FindControl("lblPagerCurrentPage"), Label)
                lblPagerCurrentPage.Text = pageNumber

                Dim lblPagerTotalPages As Label = DirectCast(pnlPagination.FindControl("lblPagerTotalPages"), Label)
                lblPagerTotalPages.Text = totalPages

                Dim lblPagerRecordStart As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordStart"), Label)
                Dim pagerRecordStart As Integer = ((pageNumber - 1) * pageSize) + 1
                lblPagerRecordStart.Text = pagerRecordStart

                Dim lblPagerRecordEnd As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordEnd"), Label)
                Dim pagerRecordEnd As Integer = pagerRecordStart + pageSize - 1
                lblPagerRecordEnd.Text = IIf(pagerRecordEnd < TotalRecords, pagerRecordEnd, TotalRecords)

                Dim lblPagerRecordTotal As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordTotal"), Label)
                lblPagerRecordTotal.Text = TotalRecords

                'Set Range validator error Message and Maximum Value (Minimun value is 1).
                Dim rangevalidatorPageNumber As RangeValidator = pnlPagination.FindControl("rangevalidatorPageNumber")
                rangevalidatorPageNumber.ErrorMessage = "Enter a page between 1 and " + totalPages.ToString()
                rangevalidatorPageNumber.MaximumValue = totalPages
            End If
        End Sub

#End Region

#Region "get Document Upload Path"
        Public Shared Function getImageUploadPath() As String
            Return ConfigurationManager.AppSettings("ImageUploadPath")
        End Function
#End Region

#Region "get Document Upload Path"
        Public Shared Function getThumbnailUploadPath() As String
            Return ConfigurationManager.AppSettings("ThumbnailUploadPath")
        End Function
#End Region

#Region "Convert Date Time to UK Date format."
        ''' <summary>
        ''' This function 'll return the date in the following format in string (dd/mm/yyyy)
        ''' </summary>
        ''' <param name="aDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToUkDateFormat(ByVal aDate As String)
            Dim format As String = "dd/MM/yyyy"
            Return DateTime.ParseExact(aDate, format, CultureInfo.CreateSpecificCulture("en-UK"))
        End Function
#End Region

#Region "Get Job Sheet Summary Address"

        Public Shared Function getJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/JobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary Address"

        Public Shared Function getSbJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/SbJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get U.K Financial/Fiscal Year from Date"

        Public Shared Function getFinancialYear_UK(ByVal givenDateTime As Date) As Integer
            Return If(givenDateTime.Month >= 4, givenDateTime.Year, givenDateTime.Year - 1)
        End Function

#End Region

    End Class

End Namespace
