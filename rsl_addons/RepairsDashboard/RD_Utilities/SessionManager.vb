﻿Imports System
Imports System.Web.HttpContext
Imports System.Web
Imports RD_BusinessObject


Namespace RD_Utilities

    Public Class SessionManager

#Region "Login In User "

#Region "Set / Get  Repairs Dashboard User Id"

#Region "Set  Repairs Dashboard User Id"
        Public Shared Sub setRepairsDashboardUserId(ByRef repairsDashboardUserId As Integer)
            Current.Session(SessionConstants.RepairsDashboardUserId) = repairsDashboardUserId
        End Sub
#End Region

#Region "get Repairs Dashboard User Id"
        Public Shared Function getRepairsDashboardUserId() As Integer

            If (IsNothing(Current.Session(SessionConstants.RepairsDashboardUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.RepairsDashboardUserId), Integer)
            End If
        End Function
#End Region

#Region "remove Repairs Dashboard User Id"

        Public Sub removeRepairsDashboardUserId()
            If (Not IsNothing(Current.Session(SessionConstants.RepairsDashboardUserId))) Then
                Current.Session.Remove(SessionConstants.RepairsDashboardUserId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Full Name"

#Region "Set User Full Name"

        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionConstants.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"

        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"

        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Current.Session.Remove(SessionConstants.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Employee Id"

#Region "Set User Employee Id"

        Public Shared Sub setUserEmployeeId(ByRef employeeId As String)
            Current.Session(SessionConstants.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get User Employee Id"

        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove User Employee Id"

        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Current.Session.Remove(SessionConstants.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get User Type"

#Region "Set Logged In User Type"

        Public Shared Sub setLoggedInUserType(ByRef userType As String)
            Current.Session(SessionConstants.LoggedInUserType) = userType
        End Sub

#End Region

#Region "get Logged In User Type"

        Public Shared Function getLoggedInUserType() As String
            If (IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.LoggedInUserType), String)
            End If
        End Function

#End Region

#Region "remove Logged In User Type"

        Public Shared Sub removeLoggedInUserType()
            If (Not IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Current.Session.Remove(SessionConstants.LoggedInUserType)
            End If
        End Sub

#End Region

#End Region

#End Region

#Region "Set / Get Property Id"

#Region "Set Property Id"

        Public Shared Sub setPropertyId(ByRef propertyId As String)
            Current.Session(SessionConstants.PropertyId) = propertyId
        End Sub

#End Region

#Region "get Property Id"

        Public Shared Function getPropertyId()
            If (IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.PropertyId), String)
            End If
        End Function

#End Region

#Region "remove Property Id"

        Public Shared Sub removePropertyId()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Current.Session.Remove(SessionConstants.PropertyId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Customer Id"

#Region "Set Customer Id"

        Public Shared Sub setCustomerId(ByRef customerId As Integer)
            Current.Session(SessionConstants.CustomerId) = customerId
        End Sub

#End Region

#Region "get Customer Id"

        Public Shared Function getCustomerId()
            If (IsNothing(Current.Session(SessionConstants.CustomerId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.CustomerId), Integer)
            End If
        End Function

#End Region

#Region "remove Customer Id"

        Public Shared Sub removeCustomerId()
            If (Not IsNothing(Current.Session(SessionConstants.CustomerId))) Then
                Current.Session.Remove(SessionConstants.CustomerId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Customer Data"

#Region "Set Customer Data"

        Public Shared Sub setCustomerData(ByRef customerData As CustomerBO)
            Current.Session(SessionConstants.CustomerData) = customerData
        End Sub

#End Region

#Region "get Customer Data"

        Public Shared Function getCustomerData()
            If (IsNothing(Current.Session(SessionConstants.CustomerData))) Then
                Dim customerData As New CustomerBO
                Return customerData
            Else
                Return CType(Current.Session(SessionConstants.CustomerData), CustomerBO)
            End If
        End Function

#End Region

#Region "remove Customer Data"

        Public Shared Sub removeCustomerData()
            If (Not IsNothing(Current.Session(SessionConstants.CustomerData))) Then
                Current.Session.Remove(SessionConstants.CustomerData)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Paths Data"

#Region "Set Is Paths Data"

        Public Shared Sub setPathsData(ByRef pathsData As Dictionary(Of String, String))
            Current.Session(SessionConstants.PathsData) = pathsData
        End Sub

#End Region

#Region "Get Is Paths Data"

        Public Shared Function getPathsData()
            If (IsNothing(Current.Session(SessionConstants.PathsData))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.PathsData), Dictionary(Of String, String))
            End If
        End Function

#End Region

#Region "remove Is Paths Data"

        Public Shared Sub removePathsData()
            If (Not IsNothing(Current.Session(SessionConstants.PathsData))) Then
                Current.Session.Remove(SessionConstants.PathsData)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Job Sheet Number"

#Region "Set Job Sheet Number"

        Public Shared Sub setJobSheetNumber(ByRef JobSheetNumber As String)
            Current.Session(SessionConstants.JobSheetNumber) = JobSheetNumber
        End Sub

#End Region

#Region "get Job Sheet Number"

        Public Shared Function getJobSheetNumber()
            If (IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.JobSheetNumber), String)
            End If
        End Function

#End Region

#Region "remove Job Sheet Number"

        Public Shared Sub removeJobSheetNumber()
            If (Not IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Current.Session.Remove(SessionConstants.JobSheetNumber)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Priority Search BO"
        Public Shared PrioritySearchBO As String = "PrioritySearchBO"

#Region "Set Priority Search BO"
        Public Shared Sub setPrioritySearchBO(ByRef objPrioritySearchBO As PrioritySearchBO)
            Current.Session(SessionManager.PrioritySearchBO) = objPrioritySearchBO
        End Sub
#End Region

#Region "Get Priority Search BO"
        Public Shared Function getPrioritySearchBO() As PrioritySearchBO
            If (IsNothing(Current.Session(SessionManager.PrioritySearchBO))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionManager.PrioritySearchBO), PrioritySearchBO)
            End If
        End Function
#End Region

#Region "remove Priority Search BO"
        Public Shared Sub removePrioritySearchBO()
            If (Not IsNothing(Current.Session(SessionManager.PrioritySearchBO))) Then
                Current.Session.Remove(SessionManager.PrioritySearchBO)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Page Access DataSet"
        ''' <summary>
        ''' This key will be used to save the Page Access DataSet
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared PageAccessDataSet As String = "PageAccessDataSet"

#Region "Set Page Access DataSet"
        ''' <summary>
        ''' This function shall be used Page Access DataSet in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPageAccessDataSet(ByRef ds As DataSet)
            Current.Session(PageAccessDataSet) = ds
        End Sub

#End Region

#Region "Get Page Access DataSet"
        ''' <summary>
        ''' This function shall be used to get Page Access DataSet from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPageAccessDataSet()
            If (IsNothing(Current.Session(PageAccessDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(PageAccessDataSet), DataSet)
            End If
        End Function

#End Region

#Region "Remove Page Access DataSet"
        ''' <summary>
        ''' This function shall be used to remove saved Core And Out Of Hours Detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePageAccessDataSet()
            If (Not IsNothing(Current.Session(PageAccessDataSet))) Then
                Current.Session.Remove(PageAccessDataSet)
            End If
        End Sub

#End Region

#End Region

    End Class

End Namespace

