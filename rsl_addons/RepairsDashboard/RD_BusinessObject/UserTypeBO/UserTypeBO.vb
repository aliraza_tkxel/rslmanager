﻿Imports System

Public Class UserTypeBO

#Region "Attributes"
    Private _userTypeID As Integer
    Private _description As String


#End Region

#Region "Construtor"

    Public Sub New(ByVal id As Integer, ByVal name As String)
        _userTypeID = id
        _description = name



    End Sub

#End Region

#Region "Properties"
    ' Get / Set property for _id
    Public Property UserTypeID() As Integer

        Get
            Return _userTypeID
        End Get

        Set(ByVal value As Integer)
            _userTypeID = value
        End Set

    End Property
    ' Get / Set property for _address
    Public Property Description() As String

        Get
            Return _description
        End Get

        Set(ByVal value As String)
            _description = value
        End Set

    End Property

#End Region

#Region "Functions"


#End Region

End Class
