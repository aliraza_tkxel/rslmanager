﻿Namespace RD_BusinessObject
    Public Class LookUpBO
#Region "Attributes"

        Private _value As String
        Private _name As String

#End Region

#Region "Constructors"

        Public Sub New(ByVal val As String, ByVal name As String)

            Me.LookUpValue = val
            Me.LookUpName = name

        End Sub

#End Region

#Region "Properties"

        Public Property LookUpValue() As String
            Get
                Return _value
            End Get
            Set(ByVal value As String)
                _value = value
            End Set
        End Property

        Public Property LookUpName() As String
            Get
                Return _name
            End Get
            Set(ByVal value As String)
                _name = value
            End Set
        End Property

#End Region

    End Class
End Namespace