﻿
Public Class PrioritySearchBO

#Region "Attributes"
    Private _schemeId As Integer
    Private _fromDate As String
    Private _toDate As String
    Private _searchText As String
#End Region

#Region "Construtor"
    Public Sub New()

        _fromDate = String.Empty
        _toDate = String.Empty
        _schemeId = -1
        _searchText = String.Empty

    End Sub
#End Region

#Region "Properties"

    ' Get / Set property for _fromDate
    Public Property FromDate() As String

        Get
            Return _fromDate
        End Get

        Set(ByVal value As String)
            _fromDate = value
        End Set

    End Property

    ' Get / Set property for _toDate
    Public Property ToDate() As String

        Get
            Return _toDate
        End Get

        Set(ByVal value As String)
            _toDate = value
        End Set

    End Property

    ' Get / Set property for _schemeId
    Public Property SchemeId() As Integer

        Get
            Return _schemeId
        End Get

        Set(ByVal value As Integer)
            _schemeId = value
        End Set

    End Property

    ' Get / Set property for _searchText
    Public Property SearchText() As String

        Get
            Return _searchText
        End Get

        Set(ByVal value As String)
            _searchText = value
        End Set

    End Property

#End Region

End Class
