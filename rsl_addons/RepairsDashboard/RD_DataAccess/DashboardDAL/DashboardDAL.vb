﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports RD_Utilities
Imports RD_BusinessObject

Namespace RD_DataAccess
    Public Class DashboardDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get list of Operatives who have appointments reserve today"

        ''' <summary>
        ''' Get list of Operatives who have appointments reserve today
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getOperativesReserveToday(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetOperativesReserveToday)

        End Sub

#End Region

#Region "Get todays list of appointments of an operative"

        ''' <summary>
        ''' Get todays list of appointments of an operative
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="operativeId"></param>
        ''' <remarks></remarks>
        Sub getTodaysAppointmentsOfOperative(ByRef resultDataSet As DataSet, ByVal operativeId As Integer)

            Dim parameterList As ParameterList = New ParameterList()
            Dim operativeParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.Int32)
            parameterList.Add(operativeParam)
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetTodaysAppointmentsOfOperative)

        End Sub

#End Region

#Region "Get RSLModules"

        ''' <summary>
        ''' Get RSLModules
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub getRSLModules(ByRef resultDataSet As DataSet, ByRef userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtRSLModules As DataTable = New DataTable()
            dtRSLModules.TableName = "RSLModules"
            resultDataSet.Tables.Add(dtRSLModules)

            Dim userIdParam As ParameterBO = New ParameterBO("USERID", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim orderParam As ParameterBO = New ParameterBO("OrderASC", 1, DbType.Int32)
            parametersList.Add(orderParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetRSLModulesList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRSLModules)

        End Sub

#End Region

#Region "Get Property Page List "

        ''' <summary>
        ''' Get Property Page List 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getPropertyPageList(ByRef resultDataSet As DataSet, ByVal employeeId As Integer, ByVal selectedMenu As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim modulesDt As DataTable = New DataTable()
            modulesDt.TableName = ApplicationConstants.AccessGrantedModulesDt
            resultDataSet.Tables.Add(modulesDt)

            Dim menusDt As DataTable = New DataTable()
            menusDt.TableName = ApplicationConstants.AccessGrantedMenusDt
            resultDataSet.Tables.Add(menusDt)

            Dim pageDt As DataTable = New DataTable()
            pageDt.TableName = ApplicationConstants.AccessGrantedPagesDt
            resultDataSet.Tables.Add(pageDt)

            Dim randomPageDt As DataTable = New DataTable()
            randomPageDt.TableName = ApplicationConstants.RandomPageDt
            resultDataSet.Tables.Add(randomPageDt)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            Dim menuTextParam As ParameterBO = New ParameterBO("menuText", selectedMenu, DbType.String)
            parametersList.Add(menuTextParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyMenuPages)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, modulesDt, menusDt, pageDt, randomPageDt)
        End Sub

#End Region

#End Region

    End Class
End Namespace





