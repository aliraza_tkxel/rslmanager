﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports RD_BusinessObject
Imports RD_Utilities

Namespace RD_DataAccess

    Public Class ReportsDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get Follow On List"

        Public Function getFollowOnList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.String)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.String)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetFollowOnList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get FollowOn Work Detail"
        ''' <summary>
        ''' Get FollowOn Work Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="jobSheetNumber"></param>
        ''' <remarks></remarks>
        Public Sub getFollowOnWorkDetail(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As New ParameterList()

            Dim dtJobSheetDetail As DataTable = New DataTable()
            Dim dtCustomerDetail As DataTable = New DataTable()
            Dim dtRepairDetail As DataTable = New DataTable()
            Dim dtFollowOnWorkDetail As DataTable = New DataTable()

            dtJobSheetDetail.TableName = "JobSheetDetail"
            dtCustomerDetail.TableName = "CustomerDetail"
            dtRepairDetail.TableName = "RepairDetail"
            dtFollowOnWorkDetail.TableName = "FollowOnWorkDetail"

            resultDataSet.Tables.Add(dtJobSheetDetail)
            resultDataSet.Tables.Add(dtCustomerDetail)
            resultDataSet.Tables.Add(dtRepairDetail)
            resultDataSet.Tables.Add(dtFollowOnWorkDetail)

            Dim followOnParam As New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
            parametersList.Add(followOnParam)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFollowOnWorksDetails)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtJobSheetDetail, dtCustomerDetail, dtRepairDetail, dtFollowOnWorkDetail)


        End Sub

#End Region

#Region "Get Operative List"
        ''' <summary>
        ''' Get Operative List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getOperativeList(ByRef resultDataSet As DataSet)

            Dim parametersList As New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetOperativeList)

        End Sub

#End Region

#Region "Get No Entry List"

        Public Function getNoEntryList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.String)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.String)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetNoEntryList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Subcontractor List"

        Public Function getSubcontractorList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal statusId As Integer, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim statusIdParam As New ParameterBO("statusId", statusId, DbType.Int32)
            parametersList.Add(statusIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetSubcontractorList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Recall List"

        Public Function getRecallList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal operativeId As Integer, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim operativeIdParam As New ParameterBO("operativeId", operativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.String)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.String)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetRecallList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Paused Fault List"
        ''' <summary>
        ''' Get Paused Fault List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getInProgressFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeidParam As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeidParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetInProgressFaultList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Reported Fault List"

        Public Function getReportedFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeidParam As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeidParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetReportedFaultList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "get Faults To Be Arranged List"

        Public Function getFaultsToBeArrangedList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetFaultToBeArrangeList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region



#Region "Get Completed Fault List"
        ''' <summary>
        ''' Get Completed Fault List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getCompletedFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeidParam As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeidParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetCompletedFaultList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Completed Fault Detail"
        ''' <summary>
        ''' Get Completed Fault Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="jobSheetNumber"></param>
        ''' <remarks></remarks>
        Public Sub getCompletedFaultDetail(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As New ParameterList()

            Dim dtJobSheetDetail As DataTable = New DataTable()
            Dim dtCustomerDetail As DataTable = New DataTable()
            Dim dtRepairDetail As DataTable = New DataTable()
            Dim dtFollowOnWorkDetail As DataTable = New DataTable()

            dtJobSheetDetail.TableName = "JobSheetDetail"
            dtCustomerDetail.TableName = "CustomerDetail"
            dtRepairDetail.TableName = "RepairDetail"
            dtFollowOnWorkDetail.TableName = "FollowOnWorkDetail"

            resultDataSet.Tables.Add(dtJobSheetDetail)
            resultDataSet.Tables.Add(dtCustomerDetail)
            resultDataSet.Tables.Add(dtRepairDetail)
            resultDataSet.Tables.Add(dtFollowOnWorkDetail)

            Dim followOnParam As New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
            parametersList.Add(followOnParam)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetCompletedFaultDetails)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtJobSheetDetail, dtCustomerDetail, dtRepairDetail, dtFollowOnWorkDetail)


        End Sub

#End Region

#Region "Get FaultsOverdue List"

        ''' <summary>
        ''' Get FaultsOverdue List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getFaultsOverdueList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeidParam As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeidParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetFaultsOverdueList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Emergency Fault List"

        Public Function getEmergencyFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal statusId As Integer, ByVal financialYear As Integer, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim statusIdParam As New ParameterBO("statusId", statusId, DbType.Int32)
            parametersList.Add(statusIdParam)

            Dim fYearParam As New ParameterBO("@financialYear", financialYear, DbType.Int32)
            parametersList.Add(fYearParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeidParam As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeidParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)


            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetEmergencyFaultList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get fault Status List"

        Public Sub getFaultStatusList(ByRef resultDataSet As DataSet)

            Dim parametersList As New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetFaultStatusList)

        End Sub

#End Region

#Region "Get completed fault images"
        ''' <summary>
        ''' Get completed fault images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getCompletedFaultImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As New ParameterList()
            Dim jobsheetParam As New ParameterBO("jobsheetnumber", jobSheetNumber, DbType.String)
            parametersList.Add(jobsheetParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetCompletedFaultImages)

        End Sub

#End Region

#Region "Get scheme list"
        ''' <summary>
        ''' Get scheme list
        ''' </summary>
        ''' <remarks></remarks>
        Sub getSchemeList(ByRef resultDataSet As DataSet)
            Dim parametersList As New ParameterList()
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAllSchemes)
        End Sub

#End Region

#Region "Get Priority And Completion List"
        ''' <summary>
        ''' Get Priority And Completion List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPrioritySearchBO"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getPriorityAndCompletionFaultList(ByRef resultDataSet As DataSet, ByVal objPrioritySearchBO As PrioritySearchBO, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim schemeIdParam As New ParameterBO("schemeId", objPrioritySearchBO.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim fromDateParam As New ParameterBO("fromDate", objPrioritySearchBO.FromDate, DbType.String)
            parametersList.Add(fromDateParam)

            Dim toDateParam As New ParameterBO("toDate", objPrioritySearchBO.ToDate, DbType.String)
            parametersList.Add(toDateParam)

            Dim searchedTextParam As New ParameterBO("searchedText", objPrioritySearchBO.SearchText, DbType.String)
            parametersList.Add(searchedTextParam)

            Dim isFullListParam As New ParameterBO("isFullList", Not ApplicationConstants.isFullList, DbType.Boolean)
            parametersList.Add(isFullListParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetPriorityAndCompletionList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Full Priority And Completion List"
        ''' <summary>
        ''' Get Full Priority And Completion List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPrioritySearchBO"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getFullPriorityAndCompletionFaultList(ByRef resultDataSet As DataSet, ByVal objPrioritySearchBO As PrioritySearchBO, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim schemeIdParam As New ParameterBO("schemeId", objPrioritySearchBO.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim fromDateParam As New ParameterBO("fromDate", objPrioritySearchBO.FromDate, DbType.String)
            parametersList.Add(fromDateParam)

            Dim toDateParam As New ParameterBO("toDate", objPrioritySearchBO.ToDate, DbType.String)
            parametersList.Add(toDateParam)

            Dim searchedTextParam As New ParameterBO("searchedText", objPrioritySearchBO.SearchText, DbType.String)
            parametersList.Add(searchedTextParam)

            Dim isFullListParam As New ParameterBO("isFullList", ApplicationConstants.isFullList, DbType.Boolean)
            parametersList.Add(isFullListParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetPriorityAndCompletionList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Load Scheme DropDown"
        ''' <summary>
        ''' Load Scheme dropdown for all reports
        ''' </summary>
        ''' <param name="schemeDataSet"></param>
        ''' <remarks></remarks>
        Sub loadSchemeDropDown(ByRef schemeDataSet As DataSet)
            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim developmentId As New ParameterBO("developmentId", -1, DbType.Int32)
            parametersList.Add(developmentId)

            MyBase.LoadDataSet(schemeDataSet, parametersList, outParametersList, SpNameConstants.GetSchemeDropDown)

        End Sub
#End Region

#Region "Load Block Dropdown by SchemeId"
        ''' <summary>
        ''' Load Block dropdown by scheme id
        ''' </summary>
        ''' <param name="blockDataSet"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Sub loadBlockDropdownbySchemeId(ByRef blockDataSet As DataSet, ByVal schemeId As Integer)
            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim ParamschemeId As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(ParamschemeId)

            MyBase.LoadDataSet(blockDataSet, parametersList, outParametersList, SpNameConstants.GetBlockDropDownBySchemeId)
        End Sub

#End Region

#End Region

    End Class

End Namespace
