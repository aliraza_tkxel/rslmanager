﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports RD_Utilities
Imports RD_DataAccess
Imports System.Collections.Specialized


Namespace RD_BusinessLogic

    Public Class DashboardBL
        Dim objDashboardDAL As DashboardDAL = New DashboardDAL()

#Region "Counts inProgress"

        ''' <summary>
        ''' Getting the count of the "InProgress"
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsInProgress(ByRef resultHashTable As Hashtable)
            Try

                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                Dim rsInProgress As DataSet = New DataSet()
                Dim objReportDAL As New ReportsDAL()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Recorded", 1, 0)

                Dim inProgressCount As Integer = objReportDAL.getInProgressFaultList(rsInProgress, search:=String.Empty, objPageSortBo:=objPageSortBo, schemeId:=-1, blockId:=-1, financialYear:=currentFinancialYear)
                'objDashboardDAL.getCountInProgress(rsInProgress)
                resultHashTable.Add(ApplicationConstants.HashTableKeyInProgress, inProgressCount)
            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub

#End Region

#Region "Counts NoEntry"

        ''' <summary>
        ''' Returns the count of No Entry appointments.
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsNoEntry(ByRef resultHashTable As Hashtable)
            Try
                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                Dim rsNoEntry As DataSet = New DataSet()

                Dim objReportDAL As New ReportsDAL()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Recorded", 1, 0)

                Dim noEntryCount As Integer = objReportDAL.getNoEntryList(rsNoEntry, search:=String.Empty, objPageSortBo:=objPageSortBo, schemeId:=-1, blockId:=-1, financialYear:=currentFinancialYear)

                'objDashboardDAL.getCountNoEntry(rsNoEntry)
                resultHashTable.Add(ApplicationConstants.HashTableKeyNoEntry, noEntryCount)

            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub

#End Region

#Region "Counts FOLLOWON"

        ''' <summary>
        ''' Getting the count of the "FOLLOWON"
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsFOLLOWON(ByRef resultHashTable As Hashtable)
            Try
                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                'Getting the count of the "FOLLOWON"
                Dim rsFOLLOWON As DataSet = New DataSet()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Recorded", 1, 0)
                Dim objReportDAL As New ReportsDAL()

                Dim followOnCount As Integer = objReportDAL.getFollowOnList(rsFOLLOWON, search:=String.Empty, objPageSortBo:=objPageSortBo, schemeId:=-1, blockId:=-1, financialYear:=currentFinancialYear)
                'objDashboardDAL.getCountFollowON(rsFOLLOWON)

                resultHashTable.Add(ApplicationConstants.HashTableKeyFOLLOWON, followOnCount)
            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub
#End Region

#Region "Counts Assign To Contractor"

        ''' <summary>
        ''' Getting the count of the "Assign To Contractor"
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsAssignToContractor(ByRef resultHashTable As Hashtable)
            Try
                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                'Getting the count of the "Assign To Contractor"
                Dim rsAssignToContractor As DataSet = New DataSet()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Reported", 1, 0)
                Dim objReportDAL As New ReportsDAL()

                Dim assignedToContractorStatusId As Integer = 2

                Dim assignToContractorCount As Integer = objReportDAL.getSubcontractorList(rsAssignToContractor, search:=String.Empty, objPageSortBo:=objPageSortBo, statusId:=assignedToContractorStatusId, schemeId:=-1, blockId:=-1, financialYear:=currentFinancialYear)
                'objDashboardDAL.getCountAssignToContractor(rsAssignToContractor)
                resultHashTable.Add(ApplicationConstants.HashTableKeyAssignToContractor, assignToContractorCount)
            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub

#End Region

#Region "Counts CompletedFaults "

        ''' <summary>
        ''' Getting the count of the "Completed Faults"
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsCompletedFaults(ByRef resultHashTable As Hashtable)
            Try
                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                'Getting the count of the "Completed Faults"
                Dim rsCompletedFaults As DataSet = New DataSet()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "FaultLogID", 1, 0)
                Dim objReportDAL As New ReportsDAL()

                Dim completedFaultsCount As Integer = objReportDAL.getCompletedFaultList(rsCompletedFaults, search:=String.Empty, objPageSortBo:=objPageSortBo, schemeId:=-1, blockId:=-1, financialYear:=currentFinancialYear)

                'objDashboardDAL.getCountCompletedFaults(rsCompletedFaults)
                resultHashTable.Add(ApplicationConstants.HashTableKeyCompletedFaults, completedFaultsCount)
            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub

#End Region

#Region "Counts ReportedFaults "
        ''' <summary>
        ''' Getting the count of the "Completed Faults"
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsReportedFaults(ByRef resultHashTable As Hashtable)
            Try
                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                'Getting the count of the "Reported Faults"
                Dim rsReportedFaults As DataSet = New DataSet()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "FaultLogID", 1, 0)
                Dim objReportDAL As New ReportsDAL()

                Dim reportedFaultsCount As Integer = objReportDAL.getReportedFaultList(rsReportedFaults, search:=String.Empty, objPageSortBo:=objPageSortBo, schemeId:=-1, blockId:=-1, financialYear:=currentFinancialYear)
                'objDashboardDAL.getCountReportedFaults(rsReportedFaults)
                resultHashTable.Add(ApplicationConstants.HashTableKeyReportedFaults, reportedFaultsCount)
            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub
#End Region

#Region "Counts ReCall"

        ''' <summary>
        ''' Getting the count of the "ReCall"
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsReCall(ByRef resultHashTable As Hashtable)
            Try
                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                'Getting the count of the "ReCall"
                Dim rsReCall As DataSet = New DataSet()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Recorded", 1, 0)
                Dim objReportDAL As New ReportsDAL()

                Dim reCallCount As Integer = objReportDAL.getRecallList(rsReCall, search:=String.Empty, operativeId:=-1, objPageSortBo:=objPageSortBo, schemeId:=-1, blockId:=-1, financialYear:=currentFinancialYear)
                'objDashboardDAL.getCountReCall(rsReCall)
                resultHashTable.Add(ApplicationConstants.HashTableKeyReCall, reCallCount)
            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub

#End Region

#Region "Counts FaultsOverDue"

        ''' <summary>
        ''' Getting the count of the "FaultsOverDue"
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsFaultsOverDue(ByRef resultHashTable As Hashtable)
            Try
                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                'Getting the count of the "FaultsOverDue"
                Dim rsFaultsOverDue As DataSet = New DataSet()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JSN", 1, 0)
                Dim objReportDAL As New ReportsDAL()

                Dim overdueFaultsCount As Integer = objReportDAL.getFaultsOverdueList(rsFaultsOverDue, search:=String.Empty, objPageSortBo:=objPageSortBo, schemeId:=-1, blockId:=-1, financialYear:=currentFinancialYear)
                'objDashboardDAL.getCountFaultsOverDue(rsFaultsOverDue)
                resultHashTable.Add(ApplicationConstants.HashTableKeyFaultsOverDue, overdueFaultsCount)

            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub

#End Region

#Region "Counts EmergencyFaults"

        ''' <summary>
        ''' Getting the count of the "Emergency Faults"
        ''' </summary>
        ''' <param name="resultHashTable"></param>
        ''' <remarks></remarks>
        Public Sub getCountsEmergencyFaults(ByRef resultHashTable As Hashtable)
            Try
                Dim currentFinancialYear As Integer = GeneralHelper.getFinancialYear_UK(Date.Now)

                'Getting the count of the "Emergency Faults"
                Dim rsEmergencyFaults As DataSet = New DataSet()
                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Recorded", 1, 0)
                Dim objReportDAL As New ReportsDAL()

                Dim emergencyFaultsCount As Integer = objReportDAL.getEmergencyFaultList(rsEmergencyFaults, search:=String.Empty, statusId:=-1, financialYear:=currentFinancialYear, objPageSortBo:=objPageSortBo, schemeId:=-1, blockId:=-1)
                'objDashboardDAL.getCountEmergencyfaults(rsEmergencyFaults)
                resultHashTable.Add(ApplicationConstants.HashTableKeyEmergencyfaults, emergencyFaultsCount)
            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub

#End Region

#Region "Get list of Operatives who have appointments reserve today"
        ''' <summary>
        ''' Get list of Operatives who have appointments reserve today
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getOperativesReserveToday(ByRef resultDataSet As DataSet)
            objDashboardDAL.getOperativesReserveToday(resultDataSet)
        End Sub
#End Region

#Region "Get todays list of appointments of an operative"
        ''' <summary>
        ''' Get today's list of appointments of an operative.
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="operativeId"></param>
        ''' <remarks></remarks>
        Sub getTodaysAppointmentsOfOperative(ByRef resultDataSet As DataSet, ByVal operativeId As Integer)
            objDashboardDAL.getTodaysAppointmentsOfOperative(resultDataSet, operativeId)
        End Sub
#End Region

#Region "Get RSLModules"
        ''' <summary>
        ''' Returns RSL Modules list
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub getRSLModules(ByRef resultDataSet As DataSet, ByVal userId As String)
            objDashboardDAL.getRSLModules(resultDataSet, Convert.ToInt32(userId))
        End Sub
#End Region

#Region "Get Property Page List"
        ''' <summary>
        ''' Get Property Page List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="employeeId"></param>
        ''' <param name="selectedMenu"></param>
        ''' <remarks></remarks>
        Sub getPropertyPageList(ByRef resultDataSet As DataSet, ByVal employeeId As Integer, ByVal selectedMenu As String)
            objDashboardDAL.getPropertyPageList(resultDataSet, employeeId, selectedMenu)
        End Sub
#End Region

#Region "Check Page Access"
        ''' <summary>
        ''' Check Page Access
        ''' </summary>
        ''' <remarks></remarks>
        Function checkPageAccess(ByVal menu As String)

            Dim userId As Integer = SessionManager.getUserEmployeeId()
            Dim isAccessGranted As Boolean = False
            Dim pageId As Integer

            Dim resultDataset As DataSet = New DataSet
            getPropertyPageList(resultDataset, userId, menu)

            Dim accessGrantedModulesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedModulesDt)
            Dim accessGrantedMenusDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedMenusDt)
            Dim accessGrantedPagesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)
            Dim randomPageDt As DataTable = resultDataset.Tables(ApplicationConstants.RandomPageDt)

            If (checkPageExist(accessGrantedPagesDt, pageId)) Then
                isAccessGranted = checkPageHierarchyAccessRights(accessGrantedModulesDt, accessGrantedMenusDt, accessGrantedPagesDt, pageId)
            ElseIf (checkPageExist(randomPageDt, pageId)) Then
                randomPageDt.Merge(accessGrantedPagesDt)
                isAccessGranted = checkPageHierarchyAccessRights(accessGrantedModulesDt, accessGrantedMenusDt, randomPageDt, pageId)
            End If

            Return isAccessGranted

        End Function

#End Region

#Region "Check Page Exist"
        ''' <summary>
        ''' Check Page Exist
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkPageExist(ByVal pagesDt As DataTable, ByRef pageId As Integer)

            Dim pageFound As Boolean = False
            Dim pageFileName = HttpContext.Current.Request.Url.AbsolutePath.Substring(HttpContext.Current.Request.Url.AbsolutePath.LastIndexOf("/") + 1)
            Dim pageQueryString As NameValueCollection = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())

            Dim query = (From dataRow In pagesDt _
                       Where dataRow.Field(Of String)(ApplicationConstants.GrantPageFileNameCol).ToLower() = pageFileName.ToLower() _
                       Order By dataRow.Field(Of Integer)(ApplicationConstants.GrantPageAccessLevelCol) Descending _
                       Select dataRow)
            Dim resultPageDt As DataTable = pagesDt.Clone()


            If query.Count > 0 Then
                resultPageDt = query.CopyToDataTable()

                For Each row As DataRow In resultPageDt.Rows
                    Dim fileName As String = row(ApplicationConstants.GrantPageFileNameCol)
                    Dim url As String = row(ApplicationConstants.GrantPageQueryStringCol)
                    Dim queryString As NameValueCollection = HttpUtility.ParseQueryString(url)

                    If (queryString.Count = 0 And pageQueryString.Count = 0) Then
                        pageFound = True
                        pageId = row(ApplicationConstants.GrantPageIdCol)
                        Exit For
                    Else
                        Dim matchCount As Integer = 0
                        For Each key As String In queryString.AllKeys
                            If (queryString(key).Equals(pageQueryString(key))) Then
                                matchCount = matchCount + 1
                            End If
                        Next key

                        If (queryString.Count = matchCount) Then
                            pageId = row(ApplicationConstants.GrantPageIdCol)
                            pageFound = True
                            Exit For
                        End If

                    End If

                Next

            End If

            Return pageFound

        End Function

#End Region

#Region "Check Page Hierarchy Access Rights"
        ''' <summary>
        ''' Check Page Hierarchy Access Rights
        ''' First check in pages levels e.g (level 3 , level 2 , level 1)
        ''' Second check in menu
        ''' Third check in modules
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkPageHierarchyAccessRights(ByVal modulesDt As DataTable, ByVal menusDt As DataTable, ByVal pagesDt As DataTable, ByVal pageId As Integer)

            Dim isAccessGranted As Boolean = False

            If (checkInPageLevelsAccessRights(pagesDt, pageId)) Then

                Dim pageExpression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId)
                Dim pageRows() As DataRow = pagesDt.Select(pageExpression)
                Dim linkedMenuId As Integer = pageRows(0)(ApplicationConstants.GrantPageMenuIdCol)
                Dim linkedModuleId As Integer = pageRows(0)(ApplicationConstants.GrantPageModuleIdCol)

                Dim menuExpression As String = ApplicationConstants.GrantMenuMenuIdCol + " = " + Convert.ToString(linkedMenuId)
                Dim menuRows() As DataRow = menusDt.Select(menuExpression)

                If (menuRows.Count > 0) Then

                    Dim moduleExpression As String = ApplicationConstants.GrantModulesModuleIdCol + " = " + Convert.ToString(linkedModuleId)
                    Dim moduleRows() As DataRow = modulesDt.Select(moduleExpression)

                    If (moduleRows.Count > 0) Then
                        isAccessGranted = True
                    End If

                End If

            End If

            Return isAccessGranted
        End Function

#End Region

#Region "Check In Page Levels Access Rights"
        ''' <summary>
        ''' Check In Page Levels Access Rights from bottom to top
        ''' </summary>
        ''' <param name="pagesDt"></param>
        ''' <param name="pageId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkInPageLevelsAccessRights(ByVal pagesDt As DataTable, ByVal pageId As Integer)

            Dim parentPageId As Integer
            Dim expression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId)
            Dim rows() As DataRow = pagesDt.Select(expression)

            If (IsDBNull(rows(0)(ApplicationConstants.GrantPageParentPageCol)) Or _
                IsNothing(rows(0)(ApplicationConstants.GrantPageParentPageCol))) Then
                Return True
            Else
                parentPageId = rows(0)(ApplicationConstants.GrantPageParentPageCol)
                Dim parentExpression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(parentPageId)
                Dim parentRow() As DataRow = pagesDt.Select(parentExpression)

                If (parentRow.Count > 0) Then
                    Return checkInPageLevelsAccessRights(pagesDt, parentPageId)
                Else
                    Return False
                End If

            End If

        End Function

#End Region

    End Class

End Namespace