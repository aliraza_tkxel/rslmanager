﻿Imports System
Imports RD_BusinessObject
Imports RD_DataAccess
Imports RD_Utilities

Namespace RD_BusinessLogic

    Public Class StatusBL
#Region "Attributes"
        Dim objStatusDAL As StatusDAL = New StatusDAL()
#End Region

#Region "Functions"

#Region "Get Status"
        Public Sub getStatus(ByRef resultDataSet As DataSet)
            Try

                objStatusDAL.getStatus(resultDataSet)

            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub

        Public Sub getStatusList(ByRef statusList As List(Of StatusBO))
            Try

                objStatusDAL.getStatusList(statusList)

            Catch ex As Exception
                ExceptionHelper.IsExceptionLogged = False
                Throw
            End Try
        End Sub
#End Region

#Region "Add Status"
        Public Sub addStatus(ByVal inspectionType As Integer, ByVal statusTitle As String, ByVal selectedRanking As Integer)

            objStatusDAL.addStatus(inspectionType, statusTitle, selectedRanking)

        End Sub

#End Region

#Region "Get Action"
        Public Sub getActions(ByRef resultDataSet As DataSet)
            objStatusDAL.getActions(resultDataSet)
        End Sub
#End Region

#Region "Get Actions By StatusId"
        Public Sub getActionsByStatusId(ByVal statusId As Integer, ByVal resultDataSet As DataSet)
            objStatusDAL.getActionsByStatusId(statusId, resultDataSet)
        End Sub
#End Region

#Region "get Action Ranking By StatusId"
        Public Sub getActionRankingByStatusId(ByRef resultDataSet As DataSet, ByVal statusId As Integer)
            objStatusDAL.getActionRankingByStatusId(resultDataSet, statusId)
        End Sub
#End Region

#End Region




    End Class
End Namespace

