﻿Imports System
Imports RD_Utilities
Imports RD_DataAccess

Namespace RD_BusinessLogic

    Public Class ReportsBL

        Dim objReportsDAL As ReportsDAL = New ReportsDAL

#Region "Functions"

#Region "Get FollowOn Work Detail"

        ''' <summary>
        ''' Get FollowOn Work Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="jobSheetNumber"></param>
        ''' <remarks></remarks>
        Public Sub getFollowOnWorkDetail(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            objReportsDAL.getFollowOnWorkDetail(resultDataSet, jobSheetNumber)

        End Sub

#End Region

#Region "Get In Progress Fault List"

        ''' <summary>
        ''' Get Paused Fault List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getInProgressFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)
            Return objReportsDAL.getInProgressFaultList(resultDataSet, search, objPageSortBo, schemeId, blockId, financialYear)
        End Function

#End Region

#Region "Get Reported Fault List"

        ''' <summary>Get Reported Fault List</summary>
        ''' <remarks></remarks>
        Public Function getReportedFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Return objReportsDAL.getReportedFaultList(resultDataSet, search, objPageSortBo, schemeId, blockId, financialYear)

        End Function

#End Region

#Region "Get Faults to be arranged List"

       
        Public Function getFaultsToBeArrangedList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO)

            Return objReportsDAL.getFaultsToBeArrangedList(resultDataSet, search, objPageSortBo)

        End Function

#End Region


#Region "Get Completed Fault List"

        ''' <summary>
        ''' Get Completed Fault List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getCompletedFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)
            Return objReportsDAL.getCompletedFaultList(resultDataSet, search, objPageSortBo, schemeId, blockId, financialYear)
        End Function

#End Region

#Region "Get Completed Fault Detail"

        'TODO: Verify and remove this unused function.
        ''' <summary>
        ''' Get FollowOn Work Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="jobSheetNumber"></param>
        ''' <remarks></remarks>
        Public Sub getCompletedFaultDetail(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            objReportsDAL.getCompletedFaultDetail(resultDataSet, jobSheetNumber)

        End Sub

#End Region

#Region "Get FaultsOverdue List"

        ''' <summary>
        ''' Get FaultsOverdue List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getFaultsOverdueList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Return objReportsDAL.getFaultsOverdueList(resultDataSet, search, objPageSortBo, schemeId, blockId, financialYear)

        End Function

#End Region

#Region "Get Emergency Fault List"

        Public Function getEmergencyFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal statusId As Integer, ByVal financialYear As Integer, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer)

            Return objReportsDAL.getEmergencyFaultList(resultDataSet, search, statusId, financialYear, objPageSortBo, schemeId, blockId)

        End Function

#End Region

#Region "Get Fault Status List"

        Public Sub getFaultStatusList(ByRef resultDataSet As DataSet)

            objReportsDAL.getFaultStatusList(resultDataSet)

        End Sub

#End Region

#Region "Get completed fault images"
        ''' <summary>
        ''' Get completed fault images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getCompletedFaultImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            objReportsDAL.getCompletedFaultImages(resultDataSet, jobSheetNumber)
        End Sub

#End Region

#Region "Get scheme list"
        ''' <summary>
        ''' Get scheme list
        ''' </summary>
        ''' <remarks></remarks>
        Sub getSchemeList(ByRef resultDataSet As DataSet)
            objReportsDAL.getSchemeList(resultDataSet)
        End Sub

#End Region

#Region "Get Priority And Completion List"
        ''' <summary>
        ''' Get Priority And Completion List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPrioritySearchBO"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getPriorityAndCompletionFaultList(ByRef resultDataSet As DataSet, ByVal objPrioritySearchBO As PrioritySearchBO, ByRef objPageSortBo As PageSortBO)

            Return objReportsDAL.getPriorityAndCompletionFaultList(resultDataSet, objPrioritySearchBO, objPageSortBo)

        End Function

#End Region

#Region "Get Full Priority And Completion List"
        ''' <summary>
        ''' Get Priority And Completion List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objPrioritySearchBO"></param>
        ''' <param name="objPageSortBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getFullPriorityAndCompletionFaultList(ByRef resultDataSet As DataSet, ByVal objPrioritySearchBO As PrioritySearchBO, ByRef objPageSortBo As PageSortBO)

            Return objReportsDAL.getFullPriorityAndCompletionFaultList(resultDataSet, objPrioritySearchBO, objPageSortBo)

        End Function

#End Region

#Region "Get Schemes for Scheme Dropdown"

        Sub loadSchemeDropDown(ByRef schemeDataSet As DataSet)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.loadSchemeDropDown(schemeDataSet)
        End Sub

#End Region

#Region "Get blocks for a scheme by SchemeId"

        Sub loadBlockDropdownbySchemeId(ByRef blockDataSet As DataSet, ByVal schemeId As Integer)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.loadBlockDropdownbySchemeId(blockDataSet, schemeId)
        End Sub

#End Region

#End Region

    End Class

End Namespace
