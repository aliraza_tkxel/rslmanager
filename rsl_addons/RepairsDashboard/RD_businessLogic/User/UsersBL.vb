﻿Imports System
Imports RD_BusinessObject
Imports System.Data.SqlClient
Imports RD_DataAccess
Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports RD_Utilities



Namespace RD_BusinessLogic

    Public Class UsersBL

#Region "Attributes"
        Dim objUserDAL As UsersDAL = New UsersDAL()
#End Region

#Region "Functions"

#Region "Returns Employee detail"
        ''' <summary>
        ''' Returns Employee detail.
        ''' </summary>
        ''' <param name="employeeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function getEmployeeById(ByVal employeeId As Integer) As DataSet
            Return objUserDAL.getEmployeeById(employeeId)
        End Function
#End Region
      
#End Region

    End Class

End Namespace