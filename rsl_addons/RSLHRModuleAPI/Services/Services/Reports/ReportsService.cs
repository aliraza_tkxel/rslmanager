﻿using NetworkModel;
using NetwrokModel.Reports;
using System.Linq;
using Utilities;

namespace Services.Services.Reports
{
    public class ReportsService : BaseService
    {
        public ReportsService()
        {

        }

        public EstablishmentResponse PopulateEstablishmentReport(CommonReportsFilterRequest model)
        {
            EstablishmentResponse response = new EstablishmentResponse();
            response.establishmentList = this.RepositryUnit.reportsRepository.PopulateEstablishmentReport(model);

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var partFullTime = RepositryUnit.partFullTimeRepository.GetAll();
            response.lookUps.partFullTime = partFullTime.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.PARTFULLTIMEID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            return response;
        }

        public SicknessReportResponse PopulateSicknessReport(SicknessRequest model)
        {
            SicknessReportResponse response = new SicknessReportResponse();
            response.sicknessList = this.RepositryUnit.reportsRepository.PopulateSicknessReport(model);

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var reasonData = this.RepositryUnit.absenceReasonRepository.GetAll().ToList();
            response.lookUps.reasonLookUps = reasonData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.SID }).ToList().OrderBy(x => x.lookUpDescription).ToList();


            return response;
        }

        public SicknessExportResponse SicknessExport(SicknessRequest model)
        {
            SicknessExportResponse response = new SicknessExportResponse();
            response.sicknessExportList = this.RepositryUnit.reportsRepository.SicknessExport(model);

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var reasonData = this.RepositryUnit.absenceReasonRepository.GetAll().ToList();
            response.lookUps.reasonLookUps = reasonData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.SID }).ToList().OrderBy(x => x.lookUpDescription).ToList();


            return response;
        }

        public AnnualLeaveReportResponse populateAnnualLeaveReport(AnnualLeaveRequest model)
        {
            AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();
            response.annualLeaveListing = RepositryUnit.reportsRepository.populateAnnualLeaveReport(model);
            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId  }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            return response;
        }
        public AnnualLeaveReportResponse GetTeamsByDirectorate(int directorateId)
        {
            AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();
            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            if(directorateId!=0)
            {
                response.lookUps.teamLookUps = teamData.Where(x => x.DIRECTORATEID == directorateId && x.ACTIVE==1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            } 
            else
            {
                response.lookUps.teamLookUps = teamData.Where(x =>x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            }
            return response;
        }
        public LeaverReportResponse PopulateLeaverReport(LeaverReportRequest model)
        {
            LeaverReportResponse response = new LeaverReportResponse();

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            response.leaverReportListing = RepositryUnit.reportsRepository.PopulateLeaverReport(model);
            response.fromDate = response.leaverReportListing.fromDate;
            response.toDate = response.leaverReportListing.toDate;
            return response;
        }

        public NewStarterResponse PopulateNewStarterReport(CommonReportsFilterRequest model)
        {
            NewStarterResponse response = new NewStarterResponse();
            response.newStarterList = RepositryUnit.reportsRepository.PopulateNewStarterReport(model);

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            response.fromDate = response.newStarterList.fromDate;
            response.toDate = response.newStarterList.toDate;
            return response;
        }

        public MyStaffList PopulateMyStaffReport(MyStaffEmployeeRequest model)
        {
            return RepositryUnit.reportsRepository.PopulateMyStaffReport(model);
        }

        public MyTeamListing PopulateMyTeamList(CommonRequest model)
        {

            return RepositryUnit.reportsRepository.PopulateMyTeamList(model);
        }

        public MyTeamEmployeeListing GetEmployeeByTeam(MyTeamsEmployeeRequest model)
        {
            return RepositryUnit.reportsRepository.GetEmployeeByTeam(model);
        }
        public RemunerationStatementsResponse PopulateRemunerationStatement(CommonReportsFilterRequest model)
        {
            RemunerationStatementsResponse response = new RemunerationStatementsResponse();
            response.statementList = RepositryUnit.reportsRepository.PopulateRemunerationStatement(model);

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            return response;
        }
        public RemunerationStatementDocument GetRemunerationStatementDocument(int employeeId)
        {
            return RepositryUnit.reportsRepository.GetRemunerationStatementDocument(employeeId);
        }

        public TrainingListingResponse GetMandatoryTrainings(CommonReportsFilterRequest model)
        {
            TrainingListingResponse response = new TrainingListingResponse();
            response = RepositryUnit.reportsRepository.GetMandatoryTrainings(model);

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var partFullTime = RepositryUnit.partFullTimeRepository.GetAll();
            response.lookUps.partFullTime = partFullTime.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.PARTFULLTIMEID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            Dispose();
            return response;
        }

        public HealthReportResponse GetHealthList(CommonRequest model)
        {
            HealthReportResponse response = new HealthReportResponse();
            response.healthReportListing = RepositryUnit.reportsRepository.GetHealthList(model);

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            return response;
        }

        public EmployeeJournalResponse GETEmployeeJournal(JournalRequest model)
        {
            return RepositryUnit.reportsRepository.GETEmployeeJournal(model);

        }

        public DeclarationOfInterestsResponse GetDeclarationOfInterestsReport(DeclarationOfInterestsRequest model)
        {
            var response = new DeclarationOfInterestsResponse();
            response = RepositryUnit.reportsRepository.GetDeclarationOfInterestsReport(model);

            var fiscalYearData = RepositryUnit.fIscalYearRepository.GetAll();
            response.lookUps.fiscalYears = fiscalYearData.Select(x => new LookUpResponseModel() { lookUpDescription = x.YStart.Value.Year.ToString() + "-" + x.YEnd.Value.Year.ToString(), lookUpId = x.YRange }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var declerationOfInterestStatusData = RepositryUnit.declerationOfInterestStatusRepository.GetAll().ToList();
            response.lookUps.doiStatus = declerationOfInterestStatusData.Where(d=>!d.Description.Equals(ApplicationConstants.doiPendingReview))
                                                                .Select(x => new LookUpResponseModel() { lookUpId = x.DOIStatusId, lookUpDescription = x.Description })
                                                                .ToList().OrderBy(x => x.lookUpDescription)
                                                                .ToList();

            return response;

        }
        public GiftApprovalResponse GiftApprovalList(GiftApprovalRequest model)
        {
            var response = new GiftApprovalResponse();
            response = RepositryUnit.reportsRepository.GiftApprovalList(model);
            var directorateData = RepositryUnit.directorateRepository.GetAll();
            response.lookUps.directorates = directorateData.Where(x => x.DIRECTORATENAME.Length > 0).Select(x => new LookUpResponseModel() { lookUpId = x.DIRECTORATEID, lookUpDescription = x.DIRECTORATENAME }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var giftsStatus = RepositryUnit.giftStatusRepository.GetAll().ToList();
            response.lookUps.giftStatus = giftsStatus
                                    .Select(x => new LookUpResponseModel() { lookUpId = x.GiftStatusId, lookUpDescription = x.Description })
                                    .ToList().OrderBy(x => x.lookUpDescription)
                                    .ToList();
            var employeeData = RepositryUnit.trainingsRepository.GetAllEmployees();
            response.lookUps.employees = employeeData.Select(x => new LookUpResponseModel() { lookUpDescription = x.FIRSTNAME + " " + x.LASTNAME, lookUpId = x.EMPLOYEEID }).ToList().OrderBy(x => x.lookUpId).ToList();


            return response;

        }

        public AnnualLeaveReportResponse GetEmployeesByDirectorate(int directorateId)
        {
            AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();
            response = RepositryUnit.reportsRepository.GetEmployeesByDirectorate(directorateId);
            return response;
        }

    }
}
