﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilities;

namespace Services.Services.Absence
{
    public class AbsenceService : BaseService
    {
        public AbsenceService()
        {

        }

        public List<AbsenceLeave> GetAbsenceSickness(AbsenceRequest request)
        {
            List<AbsenceLeave> absenceSickness = RepositryUnit.absenceRepository.GetSicknessAbsenceData(request);
           
            return absenceSickness;
        }
        public List<StaffMemberSickness> GetOwnAbsenceSickness(int absenceHistoryId)
        {
            List<StaffMemberSickness> absenceSickness = RepositryUnit.absenceRepository.GetOwnSicknessAbsenceData(absenceHistoryId);

            return absenceSickness;
        }

        public List<StaffMemberSickness> GetStaffMembersSicknessSummary(AbsenceRequest request)
        {
            List<StaffMemberSickness> absenceSickness = RepositryUnit.absenceRepository.GetStaffMembersAndSicknessStatusSummary(request);

            return absenceSickness;
        }

        public List<LookUpResponseModel> GetSicknessReasons()
        {
            var data = RepositryUnit.absenceReasonRepository.GetAll().ToList().OrderBy(x=> x.DESCRIPTION).ToList();

            List<LookUpResponseModel> sicknessResons = data.Select(x => new LookUpResponseModel() { lookUpId = x.SID, lookUpDescription = x.DESCRIPTION }).ToList();
            return sicknessResons;
        }

        public dynamic RecordSickness(RecordAbsenceRequest request)
        {
            dynamic result = RepositryUnit.absenceRepository.RecordSickness(request);
            this.RepositryUnit.Commit();
            return result;
        }

        public AnnualLeaveResponce GetAnnualLeaveDetail(AbsenceRequest request)
        {
            AnnualLeaveResponce response = RepositryUnit.absenceRepository.GetAnnualLeaveDetail(request);
            //this.RepositryUnit.Commit();
            return response;
        }

        public dynamic RecordAnnualLeave(RecordAbsenceRequest request)
        {
            dynamic responce = RepositryUnit.absenceRepository.RecordAnnualLeave(request);
            this.RepositryUnit.Commit();
            return responce;
        }

        public List<AbsenceLeave> GetLeaveOfAbsence(AbsenceRequest request, string callBy = ApplicationConstants.callByIos)
        {
            List<AbsenceLeave> responce = RepositryUnit.absenceRepository.GetLeavesOfAbsence(request);
            var removeableLstWeb = responce.Where(e => e.nature == "Training").ToList();
            foreach (var item in removeableLstWeb)
            {
                responce.Remove(item);
            }
            if (callBy.ToLower() == ApplicationConstants.callByIos)
            {
                var removeableLst = responce.Where(e => e.nature == "Birthday Leave" || e.nature == "Personal Day").ToList();
                foreach (var item in removeableLst)
                {
                    responce.Remove(item);
                }
            }
            //this.RepositryUnit.Commit();
            return responce;
        }
        public List<AbsenceLeave> GetLeaveOfToil(int employeeId, string callBy = ApplicationConstants.callByIos)
        {
            List<AbsenceLeave> responce = RepositryUnit.absenceRepository.GetLeavesOfToil(employeeId);
            return responce;
        }
        public List<MyTeamLeaveOfToilPending> GetMyTeamLeavesOfToilRequested (int employeeId)
        {
            List<MyTeamLeaveOfToilPending> responce = RepositryUnit.absenceRepository.GetMyTeamLeavesOfToilRequested(employeeId);
            //this.RepositryUnit.Commit();
            return responce;
        }
        //public List<AbsenceLeave> GetLeavesOfAbsenceRequested(int employeeId)
        //{
        //    List<AbsenceLeave> responce = RepositryUnit.absenceRepository.GetLeavesOfAbsenceRequested(employeeId);
        //    //this.RepositryUnit.Commit();
        //    return responce;
        //}

        public List<MyTeamLeaveOfAbsencePending> GetMyTeamLeavesOfAbsenceRequested(int employeeId)
        {
            List<MyTeamLeaveOfAbsencePending> responce = RepositryUnit.absenceRepository.GetMyTeamLeavesOfAbsenceRequested(employeeId);
            //this.RepositryUnit.Commit();
            return responce;
        }

        public List<MyTeamLeavesPending> GetMyLeavesOfAbsenceRequested(int employeeId)
        {
            List<MyTeamLeaveOfAbsencePending> response = RepositryUnit.absenceRepository.GetMyLeavesOfAbsenceRequested(employeeId);
            List<MyTeamLeavesPending> myLeaveOfAbsence = new List<MyTeamLeavesPending>();
            foreach (MyTeamLeaveOfAbsencePending item in response)
            {
                MyTeamLeavesPending pending = new MyTeamLeavesPending();
                pending.absenceHistoryId = item.absenceHistoryId;
                pending.applicantId = item.applicantId;
                pending.applicantName = item.applicantName;
                pending.duration = item._duration;
                pending.endDate = item.endDate;
                pending.holType = item.holType;
                pending.leaveDescription = item.leaveDescription;
                pending.natureId = item.natureId;
                pending.startDate = item.startDate;
                pending.status = item.status;
                pending.statusId = item.statusId;
                myLeaveOfAbsence.Add(pending);
            }

            return myLeaveOfAbsence;
        }

        public List<MyTeamLeavesPending> GetMyLeavesRequested(int employeeId)
        {
            return RepositryUnit.absenceRepository.GetMyLeavesRequested(employeeId); ;
        }

        public List<DateTime> GetBankHolidays()
        {
            var data = RepositryUnit.bankHolidayRepository.GetAll().ToList();

            List<DateTime> bankHolidays = data.Select(x => x.BHDATE).ToList();

            return bankHolidays;
        }

        public List<LookUpResponseModel> GetLeaveOfAbsenceNatures(string callBy = ApplicationConstants.callByIos)
        {
            var data = RepositryUnit.absenceNatureRepository.GetAll();
            var excludedNature = new List<string> { "Sickness", "Annual Leave","Internal Meeting","Training", "BRS TOIL Recorded", "BRS Time Off in Lieu", "BHG TOIL" };

            if (callBy == ApplicationConstants.callByIos)
            {
                excludedNature.Add("Personal Day");
                excludedNature.Add("Birthday Leave");
                //data = data.Where(a => a.DESCRIPTION != "Personal Day" && a.DESCRIPTION != "Birthday Leave").ToList();
            }
            
            List<LookUpResponseModel> leaveOfAbsenceNatures = data.Where(x => !excludedNature.Contains(x.DESCRIPTION) && x.ITEMID==1).Select(x => new LookUpResponseModel() { lookUpId = x.ITEMNATUREID, lookUpDescription = x.DESCRIPTION }).OrderBy(x=> x.lookUpDescription).ToList();
            return leaveOfAbsenceNatures;
        }

        public dynamic RecordLeaveOfAbsence(RecordAbsenceRequest request)
        {
            dynamic responce = RepositryUnit.absenceRepository.RecordLeaveOfAbsence(request);
            //this.RepositryUnit.Commit();
            return responce;
        }

        public dynamic RecordLeave(RecordAbsenceRequest request)
        {
            dynamic responce = RepositryUnit.absenceRepository.RecordSickAnnualLeave(request);
            //this.RepositryUnit.Commit();
            return responce;
        }
        public List<LeaveTaken> GetLeavesTaken(AbsenceRequest request)
        {
            List <LeaveTaken>  responce = RepositryUnit.absenceRepository.GetLeavesTaken(request);            
            return responce;
        }
        public List<LeaveTaken> GetLeavesRequested(AbsenceRequest request)
        {
            List<LeaveTaken> responce = RepositryUnit.absenceRepository.GetLeavesRequested(request);
            return responce;
        }
        public List<LeaveTaken> GetPersonalBirthdaysRequested(AbsenceRequest request, string leaveType)
        {
            List<LeaveTaken> responce = RepositryUnit.absenceRepository.GetBirthdaysPersonalDaysRequested(request, leaveType);
            return responce;
        }
        public List<MyTeamLeavesPending> GetMyTeamLeavesRequested(int employeeId)
        {
            List<MyTeamLeavesPending> responce = RepositryUnit.absenceRepository.GetMyTeamLeavesRequested(employeeId);
            return responce;
        }

        public List<MyTeamLeavesPending> GetMyTeamLeavesRequestedWeb(int employeeId)
        {
            List<MyTeamLeavesPending> responce = RepositryUnit.absenceRepository.GetMyTeamLeavesRequestedWeb(employeeId);
            return responce;
        }

        public List<AbsenceDetail> GetAbsenceDetail(int absenceHistoryId)
        {
            List<AbsenceDetail> response = RepositryUnit.absenceRepository.GetAbsenceDetail(absenceHistoryId);
            return response;
        }

        public List<MyTeamLeavesPending> GetMyTeamBirthdayLeavesRequested(int employeeId,string leaveType)
        {
            List<MyTeamLeavesPending> responce = RepositryUnit.absenceRepository.GetMyTeamBirthdaysPersonalDaysRequested(employeeId, leaveType);
            return responce;
        }

        public string ChangeLeaveStatus(LeaveStatusChangeRequest leaveStatusChangeRequest)
        {
            var response = RepositryUnit.absenceRepository.ChangeLeaveStatus(leaveStatusChangeRequest);
            if (!response.Equals(ApplicationConstants.absenceError) && !response.Equals(UserMessageConstants.RescheduleBRSAppointmentMessage))
                RepositryUnit.Commit();
            return response;
        }

        public string ChangeSicknessLeaveStatus(LeaveSicknessStatusChangeRequest leaveStatusChangeRequest)
        {
            var response = RepositryUnit.absenceRepository.ChangeSicknessLeaveStatus(leaveStatusChangeRequest);
            if (!response.Equals(ApplicationConstants.absenceError) && !response.Equals(UserMessageConstants.RescheduleBRSAppointmentMessage))
                RepositryUnit.Commit();
            return response;
        }

        public string ChangeSicknessLeaveStatusWeb(LeaveSicknessStatusChangeRequestWeb leaveStatusChangeRequest)
        {
            var response = RepositryUnit.absenceRepository.ChangeSicknessLeaveStatusWeb(leaveStatusChangeRequest);
            if (!response.Equals(ApplicationConstants.absenceError) 
                && !response.Equals(UserMessageConstants.RescheduleBRSAppointmentMessage)
                && !response.Equals(UserMessageConstants.SickLeaveErrorMessage)
                && !response.Equals(UserMessageConstants.SickLeaveAlreadyBookedMessage))
                RepositryUnit.Commit();
            return response;
        }

        public bool AddToilRecord(RecordToilRequest request)
        {
            bool responce = RepositryUnit.absenceRepository.AddToilRecord(request);
            return responce;
        }

        public bool AddInternalMeetingRecord(RecordInternalMeetingRequest request)
        {
            bool responce = RepositryUnit.absenceRepository.AddInternalMeetingRecord(request);
            return responce;
        }

        public ToilLeaveResponce GetToilLeave(AbsenceRequest request)
        {
            ToilLeaveResponce responce = RepositryUnit.absenceRepository.GetToilLeaves(request);
            return responce;
        }

        public List<InternalMeetingsLeaveResponse> GetInternalMeetingLeave(AbsenceRequest request)
        {
            List<InternalMeetingsLeaveResponse> responce = RepositryUnit.absenceRepository.GetInternalMeetingLeave(request);
            return responce;
        }

        public string ChangeInternalLeaveStatus(InternalLeaveStatusRequest request)
        {
            string response = RepositryUnit.absenceRepository.ChangeInternalLeaveStatus(request);
            if (!response.Equals(ApplicationConstants.absenceError) && !response.Equals(UserMessageConstants.RescheduleBRSAppointmentMessage) && !response.Equals(ApplicationConstants.overlap))
                RepositryUnit.Commit();
            return response;

        }

        public List<LookUpResponseModel> GetleaveActions()
        {
            List<LookUpResponseModel> responce = new List<LookUpResponseModel>();            
            var actionData = RepositryUnit.absenceActionRepository.GetAll().ToList();
            responce = actionData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.ITEMACTIONID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            return responce;
        }
        public List<LookUpResponseModel> GetSicknessActions()
        {
            List<LookUpResponseModel> responce = new List<LookUpResponseModel>();
            var actionData = RepositryUnit.absenceActionRepository.GetAll().ToList();
            responce = actionData.Where(x => x.STATUS==1 || x.STATUS == 2|| x.STATUS == 5).Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.ITEMACTIONID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            return responce;
        }

        public AnnualLeaveResponce GetLeaveYearDate(int userId)
        {
            var leaveYearDate = RepositryUnit.absenceRepository.getLeaveYearDate(userId);
            return leaveYearDate;
        }

    }
}
