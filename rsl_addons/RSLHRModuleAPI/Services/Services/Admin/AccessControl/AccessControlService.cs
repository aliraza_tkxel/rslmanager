﻿using NetworkModel;

namespace Services.Services.Admin.Teams
{
    public class AccessControlService : BaseService
    {

        public AccessControlService()
        {
        }

        public AccessControlResponse GetEmployee(int employeeId)
        {
            AccessControlResponse response = new AccessControlResponse();
            response = RepositryUnit.accessControlRepository.GetEmployee(employeeId);
            return response;
        }

        public string UpdateLogin(AccessControl detail)
        {
            string response=RepositryUnit.accessControlRepository.UpdateLogin(detail);
            return response;
        }
        public void UnlockAccount(int employeeId)
        {
            RepositryUnit.accessControlRepository.UnlockAccount(employeeId);
        }
    }
}
