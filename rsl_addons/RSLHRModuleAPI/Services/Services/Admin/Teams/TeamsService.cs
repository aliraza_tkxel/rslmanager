﻿using NetworkModel;

namespace Services.Services.Admin.Teams
{
    public class TeamsService : BaseService
    {

        public TeamsService()
        {
        }




        public TeamsResponse GetTeamsList(TeamsRequest model)
        {
            TeamsResponse response = new TeamsResponse();
            response = RepositryUnit.teamsRepository.GetTeams(model);
            return response;
        }

        public TeamsResponse GetTeam(int teamId)
        {
            TeamsResponse response = new TeamsResponse();
            response = RepositryUnit.teamsRepository.GetTeam(teamId);
            return response;
        }

        public string SaveTeam(Team detail)
        {
            string response = RepositryUnit.teamsRepository.SaveTeam(detail);
            return response;
        }

        public TeamLookUps LoadDMLookups()
        {
            TeamLookUps lookUps = new TeamLookUps();

            lookUps.managers = RepositryUnit.teamsRepository.GetManagers();
            lookUps.directorates = RepositryUnit.teamsRepository.GetDirectorates();
            lookUps.directors = RepositryUnit.teamsRepository.GetDirectors();
            return lookUps;
        }

    }
}
