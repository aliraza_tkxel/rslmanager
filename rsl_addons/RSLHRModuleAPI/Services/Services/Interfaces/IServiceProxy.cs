﻿
using Services.Services;
using Services.Services.Absence;
using Services.Services.Admin.Teams;
using Services.Services.Employees;
using Services.Services.Employees.PostTab;
using Services.Services.MyDetailService;
using Services.Services.Push;
using Services.Services.Remitance;
using Services.Services.Reports;
using Services.Services.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IServiceProxy: IDisposable
    {
        AuthenticationService AuthenticationService { get; }
        NavigationService NavigationService { get; }

        SharedService SharedService { get; }

        EmployeesService EmployeesService { get; }

        MyDetailService MyDetailService { get; }

        HealthService HealthService { get; }

        SkillsService SkillsService { get; }
        PostTabService PostTabService { get; }
        BenefitsService BenefitsService { get; }
        ReportsService ReportsService { get; }

        TeamsService TeamsService { get; }
        AccessControlService AccessControlService { get; }
        AbsenceService AbsenceService { get; }
        DocumentsService DocumentsService { get; }
        TrainingsService TrainingsService { get; }

        GiftService GiftService { get; }
		DeclerationOfInterestService DeclerationOfInterestService { get; }

        DashboardService DashboardService { get; }
        DrivingLicenceService DrivingLicenceService { get; }
        RemitanceService RemitanceService { get; }
        PushService PushService { get; }
    }
}
