﻿namespace Services.Services.Push
{
    public class PushService : BaseService
    {
        public PushService()
        {

        }

        #region send Push Notification for Fault Appointment
        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>


        public bool sendPushNotificationFaultNoteUpdates(int appointmentId, string serverinUse)
        {

            return RepositryUnit.pushRepository.sendPushNotificationFaultNoteUpdates(appointmentId, serverinUse);

        }

        #endregion

        public bool sendPushNotificationVoidInspections(int operativeId, string serverinUse, string propertyAddress)
        {
            return RepositryUnit.pushRepository.sendPushNotificationVoidInspections(operativeId, serverinUse, propertyAddress);

        }
    }
}
