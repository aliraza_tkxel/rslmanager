﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class NavigationService : BaseService
    {
        public NavigationResponseModel GetMenus(NavigationRequestModel menuRequestModel)
        {
            NavigationResponseModel response = new NavigationResponseModel();
            response = RepositryUnit.navigationRepository.GetMenus(menuRequestModel);
            return response;
        }

    }
}
