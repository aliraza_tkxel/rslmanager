﻿using NetworkModel;
using NetwrokModel.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DashboardService : BaseService
    {
        public DashboardService()
        {

        }

        public List<LookUpResponseModel> GetTeamLookUp()
        {
            List<LookUpResponseModel> response = new List<LookUpResponseModel>();
            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList().OrderBy(x => x.TEAMNAME).ToList();
            response = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList();

            return response;
        }

        public int GetMainDetailAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.GetMainDetailAlert(teamId);
        }
        public int GetFullTimeStaffAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.GetFullTimeStaffAlert(teamId);
        }

        public int GetAnnualLeaveAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.getAnnualLeaveStats(teamId);
        }
        public int GetSicknessAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.GetSicknessAlert(teamId);
        }
        public int GetNewStarterAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.GetNewStarterAlert(teamId);
        }
        public int GetHolidayAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.GetHolidayAlert(teamId);
        }


        public int GetPayPointAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.GetPayPointAlert(teamId);
        }
        public int GetMandatoryTrainingAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.GetMandatoryTrainingAlert(teamId);
        }
        public int GetHealthAlert(int teamId)
        {
            return RepositryUnit.dashboardRepository.GetHealthAlert(teamId);
        }

        public List<VacanciesListResponse> GetVacanciesList()
        {
            return RepositryUnit.dashboardRepository.GetVacanciesList();
        }

        public DashboardStatsResponse GetDashboardStats(AbsenceRequest request)
        {
            return RepositryUnit.dashboardRepository.GetDashboardStats(request);
        }


    }
}
