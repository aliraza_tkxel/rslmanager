﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DocumentsService : BaseService
    {
        public DocumentsService()
        {

        }

        public void AddEmployeeDocument(AddDocumentRequest request)
        {
            this.RepositryUnit.documentsRepository.AddEmployeeDocument(request);
        }

        public DocumentsResponse GetEmployeeDocuments(DocumentSearchRequest request)
        {
            DocumentsResponse response = new DocumentsResponse();
            response.documentList = this.RepositryUnit.documentsRepository.GetEmployeeDocuments(request);
            var documentTypeData = RepositryUnit.documentTypeRepository.GetAll().ToList();
            response.lookUps.documentType = documentTypeData.Select(x => new LookUpResponseModel() { lookUpDescription = x.Description, lookUpId = x.TypeId }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var documentVisibility = RepositryUnit.documentVisibilityRepository.GetAll().Where(x => x.ACTIVE == 1);
            response.lookUps.documentVisibility = documentVisibility.Select(x => new LookUpResponseModel() { lookUpDescription = x.DOCVISIBILITYDESCRIPTION, lookUpId = x.DOCVISIBILITYID}).ToList();
            return response;

        }

        public void DeleteEmployeeDocument(AddDocumentRequest request)
        {
            this.RepositryUnit.documentsRepository.DeleteEmployeeDocument(request);
        }
    }

}
