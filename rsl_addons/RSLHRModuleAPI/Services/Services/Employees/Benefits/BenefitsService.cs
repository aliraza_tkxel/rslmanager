﻿using NetworkModel;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class BenefitsService : BaseService
    {
        public BenefitsService()
        {

        }
        public BenefitsDetailResponse GetEmployeeBenefits(int employeeId)
        {
            BenefitsDetailResponse response = new BenefitsDetailResponse();
            response.benefits = null;
            if (employeeId > 0)
                response.benefits = this.RepositryUnit.benefitsRepository.GetEmployeeBenefits(employeeId);

            var benefitTypeData = this.RepositryUnit.benefitTypeRepository.GetAll().ToList().OrderBy(x => x.BenefitTypeDesc).ToList();
            var fiscalYearData = this.RepositryUnit.fIscalYearRepository.GetAll().ToList().OrderBy(x => x.YRange).ToList();
            response.lookUps.benefitTypeList = benefitTypeData.Select(x => new LookUpResponseModel() { lookUpDescription = x.BenefitTypeDesc, lookUpId = x.BenefitTypeID }).ToList();
            response.lookUps.fiscalYearList = fiscalYearData.Select(x => new LookUpResponseModel() { lookUpDescription = (x.YStart.Value.Year.ToString()+'/'+ x.YEnd.Value.Year.ToString()), lookUpId = x.YRange }).ToList();

            var medicalOrgData = this.RepositryUnit.medicalOrgRepository.GetAll().ToList().OrderBy(x => x.MEDICALORGNAME).ToList();
            response.lookUps.medicalOrganizationList = medicalOrgData.Where(x=> x.MEDICALORGNAME!= "BUPA" && x.MEDICALORGNAME != "Norwich Union").Select(x => new LookUpResponseModel() { lookUpDescription = x.MEDICALORGNAME, lookUpId = x.MEDICALORGID }).ToList();

            var medicalTypeList = this.RepositryUnit.medicalTypeRepository.GetAll().ToList().OrderBy(x => x.MEDICALTYPENAME).ToList();
            response.lookUps.medicalTypeList = medicalTypeList.Select(x => new LookUpResponseModel() { lookUpDescription = x.MEDICALTYPENAME, lookUpId = x.MEDICALTYPEID }).ToList();


            var schemeData = this.RepositryUnit.schemesRepository.GetAll().ToList().OrderBy(x => x.SchemeDesc).ToList();
            response.lookUps.schemesList = schemeData.Select(x => new LookUpResponseModel() { lookUpDescription = x.SchemeDesc, lookUpId = x.SchemeID }).ToList().OrderBy(x=> x.lookUpDescription).ToList();

            return response;
        }
        public  BenefitVehicle  GetBenefitVehicle(int Id)
        {
            BenefitVehicle bv = new BenefitVehicle();
            bv=  this.RepositryUnit.benefitsRepository.GetBenefitsVehicle(Id);
            return bv;
        }
        public void AddAmendBenefits(BenefitsDetail model)
        {
            this.RepositryUnit.benefitsRepository.AddAmendBenefits(model);
        }
        public BenefitGeneral GetBenefitsGeneral(GeneralDataRequest data)
        {
            BenefitGeneral bv = new BenefitGeneral();
            bv = this.RepositryUnit.benefitsRepository.GetBenefitsGeneral(data);
            return bv;
        }

        public List<BenefitVehicle> GetBenefitsVehicles(GeneralDataRequest data)
        {

            var bv = this.RepositryUnit.benefitsRepository.GetBenefitsVehicles(data);
            return bv;
        }
        public BenefitHealth GetBenefitsHealth(GeneralDataRequest data)
        {
            BenefitHealth bv = new BenefitHealth();
            bv = this.RepositryUnit.benefitsRepository.GetBenefitsHealth(data);
            return bv;
        }
        public BenefitPension GetBenefitsPension(GeneralDataRequest data)
        {
            BenefitPension bv = new BenefitPension();
            bv = this.RepositryUnit.benefitsRepository.GetBenefitsPension(data);
            return bv;
        }
        public BenefitPRP GetBenefitsPRP(GeneralDataRequest data)
        {
            BenefitPRP bv = new BenefitPRP();
            bv = this.RepositryUnit.benefitsRepository.GetBenefitsPRP(data);
            return bv;
        }
        public List<BenefitHealthHistory> GetBenefitsHealthHistory(int BenefitHealthId)
        {
            List<BenefitHealthHistory> bhh = new List<BenefitHealthHistory>();
            bhh = this.RepositryUnit.benefitsRepository.GetBenefitHealthHistory(BenefitHealthId);
            return bhh;
        }
        public System.Collections.Generic.List<BenefitSubscription> GetBenefitsSubscription(GeneralDataRequest data)
        {
            System.Collections.Generic.List<BenefitSubscription> bv = new List<BenefitSubscription>();
            bv = this.RepositryUnit.benefitsRepository.GetBenefitsSubscription(data);
            return bv;
        }
        public void DeleteBenefitV5(BenefitsDetail request)
        {

            this.RepositryUnit.benefitsRepository.DeleteBenefitV5(request);

        }

       public CurrentFinancialYear GetCurrentFinancialYear(int Id)
        {
            CurrentFinancialYear bv = new CurrentFinancialYear();
            bv = this.RepositryUnit.benefitsRepository.GetCurrentFinancialYear(Id);
            return bv;

        }

    }
}
