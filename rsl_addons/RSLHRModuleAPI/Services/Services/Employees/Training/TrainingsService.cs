﻿using NetworkModel;
using NetwrokModel.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Services
{
    public class TrainingsService : BaseService
    {
        public TrainingsService()
        {

        }
        public bool AddEmployeeTrainings(AddEmployeeTrainings request)
        {
            return RepositryUnit.trainingsRepository.AddEmployeeTrainings(request);
        }
        public void AddEmployeeGroupTrainings(AddEmployeeGroupTraining request)
        {
            RepositryUnit.trainingsRepository.AddEmployeeGroupTrainings(request);
        }

        public AddEmployeeGroupTraining GetGroupDetails(int groupId)
        {
            AddEmployeeGroupTraining response = new AddEmployeeGroupTraining();
            response = RepositryUnit.trainingsRepository.GetGroupDetails(groupId);
            return response;
        }
        public void AddEmployeeInGroupTraining(AddOrRemoveEmployeeInGroupTraining request)
        {
            RepositryUnit.trainingsRepository.AddEmployeeInGroupTraining(request);
        }
        public void RemoveEmployeeFromGroupTraining(AddOrRemoveEmployeeInGroupTraining request)
        {
            RepositryUnit.trainingsRepository.RemoveEmployeeFromGroupTraining(request);
        }
        public GroupTrainingListResponse GetGroupTrainingsList(GroupTrainingApprovalList request)
        {
            GroupTrainingListResponse response = new GroupTrainingListResponse();
            response.groupTrainingList = RepositryUnit.trainingsRepository.GetGroupTrainingsList(request);
            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            return response;
        }
        public void UpdateGroupTrainingInfo(AddEmployeeGroupTraining request)
        {
            RepositryUnit.trainingsRepository.UpdateGroupTrainingInfo(request);
        }
        public List<LookUpResponseModel> GetTeamsByDirectorates(int directorateId)
        {
            var response = new List<LookUpResponseModel>();
            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            if (directorateId != 0)
            {
                response = teamData.Where(x => x.DIRECTORATEID == directorateId && x.ACTIVE==1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            }
            else
            {
                response = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            }
            return response;
        }

        public AnnualLeaveReportResponse GetAllDirectorates()
        {
            AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();
            var directorateData = RepositryUnit.trainingsRepository.GetAllDirectorates();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DIRECTORATENAME, lookUpId = x.DIRECTORATEID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            return response;
        }
        public TrainingApprovalLookupResponse GetTrainingApprovalLookups()
        {
            TrainingApprovalLookupResponse response = new TrainingApprovalLookupResponse();
            var directorateData = RepositryUnit.trainingsRepository.GetAllDirectorates();
            var employeeData = RepositryUnit.trainingsRepository.GetAllEmployees();
            var statusData = RepositryUnit.trainingsRepository.GetAllStatuses();

            response.lookUps.directorates = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DIRECTORATENAME, lookUpId = x.DIRECTORATEID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            response.lookUps.employees = employeeData.Select(x => new LookUpResponseModel() { lookUpDescription = x.FIRSTNAME+" "+x.LASTNAME, lookUpId = x.EMPLOYEEID }).ToList().OrderBy(x => x.lookUpId).ToList();
            response.lookUps.trainingStatus = statusData.Select(x => new LookUpResponseModel() { lookUpDescription = x.Title, lookUpId = x.StatusId }).ToList().OrderBy(x => x.lookUpId).ToList();

            return response;

        }
        public GroupTrainingApprovalListResponse GetTrainingApprovalList(TrainingApprovalListRequest request)
        {
            GroupTrainingApprovalListResponse response = new GroupTrainingApprovalListResponse();

            //if (request.isDirector && request.directorateId == 0 && !request.isSearch)
            //{
            //    request.directorateId = response.directorate = RepositryUnit.employeeRepository.GetDirectorateByEmpId(request.loginUserId);
            //}

            var statusData = RepositryUnit.trainingsRepository.GetAllStatuses();
            if (request.isHR && !request.isSearch)
            {
                request.statusId = statusData.FirstOrDefault(e => e.Title == ApplicationConstants.grpTrainingStatusExecSupported).StatusId;
            }

            response.trainingApprovalList = RepositryUnit.trainingsRepository.GetTrainingApprovalList(request);
            var employeeData = RepositryUnit.trainingsRepository.GetAllEmployees();

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamlookups = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorates = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            response.lookUps.employees = employeeData.Select(x => new LookUpResponseModel() { lookUpDescription = x.FIRSTNAME + " " + x.LASTNAME, lookUpId = x.EMPLOYEEID }).ToList().OrderBy(x => x.lookUpId).ToList();
            response.lookUps.trainingStatus = statusData.Select(x => new LookUpResponseModel() { lookUpDescription = x.Title, lookUpId = x.StatusId }).ToList().OrderBy(x => x.lookUpId).ToList();
            return response;
        }
        public GroupTrainingApprovalListResponse GetEmployeesByTeam(int teamId)
        {
            GroupTrainingApprovalListResponse response = new GroupTrainingApprovalListResponse();
            var employeeData = RepositryUnit.trainingsRepository.GetEmployeesByTeam(teamId);
            response.lookUps.employees = employeeData.Select(x => new LookUpResponseModel() { lookUpDescription = x.FIRSTNAME + " " + x.LASTNAME, lookUpId = x.EMPLOYEEID }).ToList().OrderBy(x => x.lookUpId).ToList();
            return response;
        }
        public EmployeesResponse GetEmployeesList(EmployeesTrainingRequest model)
        {
            EmployeesResponse response = new EmployeesResponse();
            if ((model.directorateId == null || model.directorateId == 0) && (model.teamId == null || model.teamId == 0))
            {
                response = RepositryUnit.trainingsRepository.GetTrainingEmployees(model);
            }
            else if (model.teamId != null && model.teamId != 0) 
            {
                response = RepositryUnit.trainingsRepository.GetTrainingEmployees(model);
            }
            else if (model.directorateId != null && model.directorateId != 0)
            {
                response = RepositryUnit.trainingsRepository.GetTrainingEmployees(model);
            }
            return response;
        }
        public EmployeesResponse GetGroupTrainingList(GroupTrainingApprovalList model)
        {
            EmployeesResponse response = new EmployeesResponse();
            if (model.directorateId == null && model.teamId == null)
            {
                //response = RepositryUnit.trainingsRepository.GetTrainingEmployees(model);
            }
            else if (model.teamId != null)
            {
                //response = RepositryUnit.trainingsRepository.GetTrainingEmployeesByTeamId(model);
            }
            else if (model.directorateId != null)
            {
                //response = RepositryUnit.trainingsRepository.GetTrainingEmployeesByDirectorateId(model);
            }
            return response;
        }
        public TrainingListingResponse GetEmployeesTrainings(CommonRequest model)
        {
            return RepositryUnit.trainingsRepository.GetEmployeesTrainings(model);
        }

        public TrainingListingResponse GetEmployeesTrainings(int employeeId)
        {
            return RepositryUnit.trainingsRepository.GetEmployeesTrainings(employeeId);
        }

        public void UpdateEmployeeTraining(UpdateEmployeeTraining request)
        {
            RepositryUnit.trainingsRepository.UpdateEmployeeTraining(request);
        }

        public void ApproveDeclineGroupTrainings(GroupTrainingApproveDecline request)
        {
            RepositryUnit.trainingsRepository.ApproveDeclineGroupTrainings(request);
        }
        

    }
}
