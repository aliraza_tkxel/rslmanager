﻿using NetworkModel;
using NetworkModel.NetwrokModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class SkillsService : BaseService
    {
        public SkillsService()
        {

        }

        public SkillsListingResponse GetEmployeesSkills(CommonRequest model)
        {
            SkillsListingResponse response = new SkillsListingResponse();
            response = this.RepositryUnit.skillsRepository.GetEmployeesSkills(model);

            var qualificationLevelData = this.RepositryUnit.qualificationLevelRepository.GetAll().ToList();
            response.lookUps.qualificationLevel = qualificationLevelData.Select(x => new LookUpResponseModel() { lookUpDescription = x.QualLevelDesc, lookUpId = x.QualLevelId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var languageTypeslData = this.RepositryUnit.languageTypesRepository.GetAll().ToList();
            response.lookUps.languageTypes = languageTypeslData.Select(x => new LookUpResponseModel() { lookUpDescription = x.Title, lookUpId = x.LanguageTypeId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var equivalentLevelData = this.RepositryUnit.equivalentLevelRepository.GetAll().ToList();
            response.lookUps.equivalentLevel = equivalentLevelData.Select(x => new LookUpResponseModel() { lookUpDescription = x.Description, lookUpId = x.EquivalentLevelId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            response.languageSkills = RepositryUnit.skillsRepository.GetLanguageSkills(model.employeeId);
            return response;
        }
        public List<LanguageSkills> GetEmployeeLanguageSkills(int employeeId)
        {
            if(employeeId == 0)
            {
                return null;
            }
            List<LanguageSkills> response = RepositryUnit.skillsRepository.GetLanguageSkills(employeeId);
            return response;
        }
        public void AddAmendEmployeeSkills(SkillsAndQualificationsDetail request)
        {
            this.RepositryUnit.skillsRepository.AddAmendEmployeeSkills(request);
        }

        public void DeleteEmployeeSkills(DeleteEmployeeSkills request)
        {
            this.RepositryUnit.skillsRepository.DeleteEmployeeSkills(request);
        }
        public void AddLanguageSkills(List<LanguageSkills> request)
        {
            RepositryUnit.skillsRepository.AddLanguageSkills(request);
        }
    }
}
