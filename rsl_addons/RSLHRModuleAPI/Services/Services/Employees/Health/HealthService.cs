﻿using NetworkModel;
using NetworkModel.NetwrokModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class HealthService : BaseService
    {
        public HealthService()
        {

        }

        public Dictionary<int,string> getAllDisabilities()
        {
            Dictionary<int, string> diffsAndDisbs = new Dictionary<int, string>();
            var disabilityHealthProblemData = RepositryUnit.disabilityAndDifficultyRepository.GetAll().ToList();
            for (int count = 0; count < disabilityHealthProblemData.Count; count++)
            {
                diffsAndDisbs.Add(disabilityHealthProblemData.ElementAt(count).DIFFDISID, disabilityHealthProblemData.ElementAt(count).DESCRIPTION);
            }
            return diffsAndDisbs;
        }
        public HealthListResponse getDifficultiesAndDisablities(int employeeId)
        {
            HealthListResponse response = new HealthListResponse();
            // Get data for absence nature
            var disabilityHealthProblemData = RepositryUnit.disabilityAndDifficultyRepository.GetAll().ToList();
            //response.lookUps.disabilityHealthProblem = disabilityHealthProblemData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.DIFFDISID }).ToList();
            response.lookUps.disabilityHealthProblem = disabilityHealthProblemData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.DIFFDISID, dType = x.DTYPE }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            //response.lookUps.learningDifficulty = disabilityHealthProblemData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.DIFFDISID, dType = x.DTYPE }).Where(x => x.dType == 2).ToList().OrderBy(x => x.lookUpDescription).ToList();
            // remove parameter type to populate Disablities when type = 1 and learningDifficulties when type = 2
            response.Disablities = this.RepositryUnit.healthRepository.getDifficultiesAndDisablitiesList(employeeId); 
            // learningDifficulties now populate in "Disablities"
            //response.learningDifficulties = this.RepositryUnit.healthRepository.getDifficultiesAndDisablitiesList(employeeId, 2);
            return response;
        }

        public void AddAmendHealth(HealthRequest request)
        {

            this.RepositryUnit.healthRepository.AddAmendHealth(request);

        }

        public void DeleteDisabilityHealth(HealthRequest request)
        {

            this.RepositryUnit.healthRepository.DeleteDisabilityHealth(request);

        }

    }
}
