﻿using NetworkModel;
using NetworkModel.NetwrokModel.Common;
using System.Collections.Generic;
using System.Linq;

namespace Services.Services.Employees.PostTab
{
    public class PostTabService : BaseService
    {
        public EmployeeLookUps LoadPostTabLookups(int employeeId)
        {
            EmployeeLookUps lookUps = new EmployeeLookUps();
            var teamData = RepositryUnit.teamRepository.GetAll().ToList();
            lookUps.team = teamData.Where(x => x.TEAMID != 1 && x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpId = x.TEAMID, lookUpDescription = x.TEAMNAME }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var jobDetail = GetEmployeeJobDetail(employeeId);

            var jobRoleData = RepositryUnit.jobRoleRepository.GetAll().ToList();
            if (jobDetail != null && jobDetail.team != null)
            {
                jobRoleData = RepositryUnit.jobRoleRepository.GetJobRoleLookUp(jobDetail.team.Value).ToList();
            }

            lookUps.jobRole = jobRoleData.Select(x => new LookUpResponseModel() { lookUpId = x.JobRoleId, lookUpDescription = x.JobeRoleDescription }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var holidayRuleData = RepositryUnit.holidayRuleRepository.GetAll().ToList();
            var rejectStatus = GetHolidayRuleRejectedIds();
            lookUps.holidayRule = holidayRuleData.Where(x => !rejectStatus.Contains(x.EID)).Select(x => new LookUpResponseModel() { lookUpId = x.EID, lookUpDescription = x.HolidayRule }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            lookUps.lineManager = RepositryUnit.postRepository.GetLineManagers(employeeId);
            lookUps.contractType = RepositryUnit.postRepository.GetContractType();

            var dayHourData = RepositryUnit.dayHourRepository.GetAll();
            lookUps.dayHour = dayHourData.Select(x => new LookUpResponseModel() { lookUpId = x.Sid, lookUpDescription = x.Indicator }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var gradePointData = RepositryUnit.gradePointRepository.GetAll();
            var gradeData = RepositryUnit.gradeRepository.GetAll();

            if (jobDetail != null)
            {
                gradePointData = gradePointData.Where(e => e.ISBRS == jobDetail.ISBRS || e.ISBRS == 2);
                gradeData = gradeData.Where(e => e.ISBRS == jobDetail.ISBRS || e.ISBRS == 2);
            }

            lookUps.gradePoint = gradePointData.Select(x => new LookUpResponseModel() { lookUpId = x.POINTID, lookUpDescription = x.POINTDESCRIPTION }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            lookUps.grade = gradeData.Where(x => x.ACTIVE == true).Select(x => new LookUpResponseModel() { lookUpId = x.GRADEID, lookUpDescription = x.DESCRIPTION }).ToList().OrderBy(x => x.lookUpDescription).ToList();


            var partFullTime = RepositryUnit.partFullTimeRepository.GetAll();
            lookUps.partFullTime = partFullTime.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.PARTFULLTIMEID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var placeOfWork = RepositryUnit.placeOfWorkRepository.GetAll();
            lookUps.placeOfWork = placeOfWork.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.PLACEID }).ToList().OrderBy(x => x.lookUpDescription).ToList();


            var noticePeriodData = RepositryUnit.noticePeriodRepository.GetAll();
            lookUps.noticePeriod = noticePeriodData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.NOTICEPERIODID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var probationPeriodData = RepositryUnit.probationPeriodRepository.GetAll().ToList();
            lookUps.probationPeriod = probationPeriodData.Select(x => new LookUpResponseModel() { lookUpId = x.PROBATIONPERIODID, lookUpDescription = x.DESCRIPTION }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            return lookUps;
        }

        public EmployeeLookUps GetGradePointLookup(int employeeId, int? isBRS)
        {
            EmployeeLookUps lookUps = new EmployeeLookUps();

            var gradePointData = RepositryUnit.gradePointRepository.GetAll();
            gradePointData = gradePointData.Where(e => e.ISBRS == isBRS || e.ISBRS == 2); // 2 for show option for both BHA and BRS
            lookUps.gradePoint = gradePointData.Select(x => new LookUpResponseModel() { lookUpId = x.POINTID, lookUpDescription = x.POINTDESCRIPTION }).ToList().OrderBy(x => x.lookUpDescription).ToList();


            var gradeData = RepositryUnit.gradeRepository.GetAll();
            gradeData = gradeData.Where(e => e.ISBRS == isBRS || e.ISBRS == 2);
            lookUps.grade = gradeData.Where(x => x.ACTIVE == true).Select(x => new LookUpResponseModel() { lookUpId = x.GRADEID, lookUpDescription = x.DESCRIPTION }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            return lookUps;
        }

        #region Post Tab

        public EmployeeDocument UpdateEmployeeReferenceDocument(EmployeeDocument request)
        {
            EmployeeDocument response = new EmployeeDocument();
            response = RepositryUnit.postRepository.UpdateEmployeeReferenceDocument(request);
            this.RepositryUnit.Commit();
            return response;
        }

        public List<EmployeeDocument> GetEmployeeReferenceDocuments(int employeeId)
        {
            List<EmployeeDocument> documents = new List<EmployeeDocument>();
            documents = RepositryUnit.postRepository.GetEmployeeReferenceDocuments(employeeId);
            // this.RepositryUnit.Commit();
            return documents;
        }

        public JobPostDetail GetEmployeeJobDetail(int employeeId)
        {
            JobPostDetail detail = new JobPostDetail();
            detail = RepositryUnit.postRepository.GetJobDetail(employeeId);
            detail.workingPattern = RepositryUnit.postRepository.GetEmpCurrentWorkingPattren(employeeId);
            detail.annualLeaveEndDate = RepositryUnit.absenceRepository.getAnnualLeaveEndDate(employeeId);
            detail.notes = RepositryUnit.postRepository.GetJobDetailNote(employeeId);
            //this.RepositryUnit.Commit();
            return detail;
        }

        public JobPostDetail AmendJobDetail(JobPostDetail detail)
        {
            JobPostDetail _detail = new JobPostDetail();
            _detail = RepositryUnit.postRepository.AmendJobDetail(detail);
            this.RepositryUnit.Commit();
            return _detail;
        }

        public List<LookUpResponseModel> GetJobRoleAgainstTeamId(int teamId)
        {
            List<LookUpResponseModel> detail = new List<LookUpResponseModel>();
            detail = RepositryUnit.postRepository.GetRolesByTeamId(teamId);

            return detail;
        }

        public List<JobRoleHistory> GetEmployeeJobRoleHistory(int employeeId)
        {
            List<JobRoleHistory> detail = new List<JobRoleHistory>();
            detail = RepositryUnit.postRepository.GetEmployeeJobRoleHistory(employeeId);
            //this.RepositryUnit.Commit();
            return detail;
        }

        public string GetLoggedInUserTeam(int employeeId)
        {
            var team = RepositryUnit.postRepository.GetLoggedInUserTeam(employeeId);
            //this.RepositryUnit.Commit();
            return team;
        }


        public WorkingHoursParent GetEmployeeWorkingHours(int employeeId)
        {
            WorkingHoursParent detail = new WorkingHoursParent();
            detail = RepositryUnit.postRepository.GetEmployeeWorkingHours(employeeId);
            //this.RepositryUnit.Commit();
            return detail;
        }
        public object GetEmployeeDaysTimings(int employeeId, int wid,string day)
        {
            var detail = RepositryUnit.postRepository.GetEmployeeDaysTimings(employeeId, wid,day);
            return detail;
        }

        public WorkingHoursParent GetEmpCurrentWorkingPattren(int employeeId)
        {
            WorkingHoursParent detail = new WorkingHoursParent();
            detail = RepositryUnit.postRepository.GetEmpCurrentWorkingPattren(employeeId);
            //this.RepositryUnit.Commit();
            return detail;
        }

        public LeaveApplyResponse GetWorkingPattern(int employeeId)
        {
            LeaveApplyResponse response = new LeaveApplyResponse();
            if (employeeId > 0)
            {
                WorkingHoursParent workingPattern = new WorkingHoursParent();
                workingPattern = RepositryUnit.postRepository.GetEmpCurrentWorkingPattren(employeeId);
                if (workingPattern != null && workingPattern.workingHours != null)
                {
                    foreach (EmployeeWorkingHours EWH in workingPattern.workingHours)
                    {
                        response.workingPattern.Add(new EmployeeWorkingPattern() { dayName = "MON", workingHours = EWH.mon });
                        response.workingPattern.Add(new EmployeeWorkingPattern() { dayName = "TUE", workingHours = EWH.tue });
                        response.workingPattern.Add(new EmployeeWorkingPattern() { dayName = "WED", workingHours = EWH.wed });
                        response.workingPattern.Add(new EmployeeWorkingPattern() { dayName = "THU", workingHours = EWH.thu });
                        response.workingPattern.Add(new EmployeeWorkingPattern() { dayName = "FRI", workingHours = EWH.fri });
                        response.workingPattern.Add(new EmployeeWorkingPattern() { dayName = "SAT", workingHours = EWH.sat });
                        response.workingPattern.Add(new EmployeeWorkingPattern() { dayName = "SUN", workingHours = EWH.sun });
                    }
                }
            }
            return response;
        }

        public WorkingHoursParent AmendEmployeeWorkingHours(WorkingHoursParent detail)
        {
            WorkingHoursParent _detail = new WorkingHoursParent();
            _detail = RepositryUnit.postRepository.AmendEmployeeWorkingHours(detail);
            this.RepositryUnit.Commit();
            return detail;
        }

        public SalaryAmendment AddSalaryAmmendment(SalaryAmendment salary)
        {
            SalaryAmendment _detail = new SalaryAmendment();
            _detail = RepositryUnit.postRepository.AddAmmendmentLetter(salary);
            this.RepositryUnit.Commit();
            return _detail;
        }

        public bool RemoveSalaryAmmendment(int salaryAmendmentId)
        {

            bool success = RepositryUnit.postRepository.RemoveAmmendmentLetter(salaryAmendmentId);
            this.RepositryUnit.Commit();
            return success;
        }

        public bool UpdateEmployementContract(UpdateEmployeeContractRoleRequest request)
        {
            bool success = RepositryUnit.postRepository.UpdateEmployeeContract(request);
            this.RepositryUnit.Commit();
            return success;
        }

        #endregion

        private List<int> GetHolidayRuleRejectedIds()
        {
            return new List<int> { 6, 7, 8, 9, 13 };
        }

        public string GetDirectorForTeamId(int teamId)
        {
            var response = "";
            response = RepositryUnit.postRepository.GetDirectorForTeamId(teamId);

            return response;
        }

        public HolidayRule GetHolidayRule(int eid)
        {
            return RepositryUnit.postRepository.GetHolidayRule(eid);
        }

        public List<JobDetailNoteResponse> GetJobDetailNote(int employeeId)
        {
            List<JobDetailNoteResponse> list = new List<JobDetailNoteResponse>();
            list = RepositryUnit.postRepository.GetJobDetailNote(employeeId);
                        
            return list;
        }

        public JobDetailNoteResponse SaveJobDetailNote(JobDetailNoteResponse request)
        {
            return RepositryUnit.postRepository.SaveJobDetailNote(request);
        }
    }
}
