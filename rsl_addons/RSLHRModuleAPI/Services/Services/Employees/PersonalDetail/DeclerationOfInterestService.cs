﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DeclerationOfInterestService : BaseService
    {
        public DeclerationOfInterestService()
        {

        }
        public DeclarationOfInterestResponse GetDeclarationOfInterest(int employeeId)
        {
            DeclarationOfInterestResponse response = new DeclarationOfInterestResponse();

            var relationshipData = RepositryUnit.employeeDoiRelationshipRepository.GetAll().ToList();
            response.lookUps.relationship = relationshipData.Select(x => new LookUpResponseModel() { lookUpDescription = x.Name, lookUpId = x.RelationshipId }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            response.declarationOfInterest = this.RepositryUnit.declerationOfInterestRepository.GetDeclarationOfInterest(employeeId);
            return response;
        }

        public DeclarationOfInterestResponse GetDeclarationOfInterestById(int interestId)
        {
            DeclarationOfInterestResponse response = new DeclarationOfInterestResponse();

            var relationshipData = RepositryUnit.employeeDoiRelationshipRepository.GetAll().ToList();
            response.lookUps.relationship = relationshipData.Select(x => new LookUpResponseModel() { lookUpDescription = x.Name, lookUpId = x.RelationshipId }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            response.declarationOfInterest = this.RepositryUnit.declerationOfInterestRepository.GetDeclarationOfInterestById(interestId);
            return response;
        }

        public bool UpdateDeclarationOfInterestStatus(UpdateDeclationOfInterestRequest request)
        {
            bool responce = RepositryUnit.declerationOfInterestRepository.UpdateDeclarationOfInterestStatus(request);
            return responce;
        }

        public void AddDeclarationOfInterest(DeclarationOfInterestDataRequest request)
        {

            this.RepositryUnit.declerationOfInterestRepository.AddDeclarationOfInterest(request);

        }

    }
}
