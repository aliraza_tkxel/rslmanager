﻿using NetworkModel;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Services.Services
{
    public class GiftService : BaseService
    {
        public GiftService ()
        {

        }

        public List<Gift> GetGiftsForEmployee(int employeeId)
        {

            return RepositryUnit.giftRepository.GetGiftsForEmployee(employeeId);
        }

        public GiftMasterResponse GetEmployeeGiftMaster(GiftsRequest request)
        {
            var response = new GiftMasterResponse();
            response = RepositryUnit.giftRepository.GetEmployeeGiftMaster(request);
            var giftStatusData = RepositryUnit.giftStatusRepository.GetAll();
            response.giftStatuses = giftStatusData.Select(x => new LookUpResponseModel() { lookUpId = x.GiftStatusId, lookUpDescription = x.Description }).ToList();
            return response;
        }

        public bool AddGift (GiftAndHospitality giftObj)
        {
            return RepositryUnit.giftRepository.AddGifts(giftObj);
        }

        public bool UpdateGiftStatus(Gift giftObj)
        {
            return RepositryUnit.giftRepository.UpdateGiftStatus(giftObj);
        }

        public PaypointDetail GetPaypointDetailForEmplyee(int employeeId)
        {
            PaypointDetail detail = RepositryUnit.giftRepository.GetPaypointForEmployee(employeeId);
            var gradePointData = RepositryUnit.gradePointRepository.GetAll();
            var gradeData = RepositryUnit.gradeRepository.GetAll();
            if (detail != null && detail.isBRS != null)
            {
                gradePointData = gradePointData.Where(e => e.ISBRS == detail.isBRS.Value);
                gradeData = gradeData.Where(e => e.ISBRS == detail.isBRS.Value);
            }

            detail.gradePointLookups = gradePointData.Select(x => new LookUpResponseModel() { lookUpId = x.POINTID, lookUpDescription = x.POINTDESCRIPTION }).ToList();
            detail.gradeLookups = gradeData.Where(x => x.ACTIVE == true).Select(x => new LookUpResponseModel() { lookUpId = x.GRADEID, lookUpDescription = x.DESCRIPTION }).ToList();
            return detail;
        }

        public PaypointDetail GetPaypointDetailByPaypointId(int paypointId)
        {
            PaypointDetail detail = RepositryUnit.giftRepository.GetPaypointDetailByPaypointId(paypointId);
            var gradePointData = RepositryUnit.gradePointRepository.GetAll();
            var gradeData = RepositryUnit.gradeRepository.GetAll();
            if (detail != null && detail.isBRS != null)
            {
                gradePointData = gradePointData.Where(e => e.ISBRS == detail.isBRS.Value);
                gradeData = gradeData.Where(e => e.ISBRS == detail.isBRS.Value);
            }

            detail.gradePointLookups = gradePointData.Select(x => new LookUpResponseModel() { lookUpId = x.POINTID, lookUpDescription = x.POINTDESCRIPTION }).ToList();
            detail.gradeLookups = gradeData.Where(x => x.ACTIVE == true).Select(x => new LookUpResponseModel() { lookUpId = x.GRADEID, lookUpDescription = x.DESCRIPTION }).ToList();
            return detail;
        }

        public bool AddPaypointDetailForEmplyee(Paypoint payPoint)
        {
            bool result = RepositryUnit.giftRepository.AddPayPointSubmission(payPoint);
            
            return result;
        }

        public PayppointReportResponce GetPaypointReport(PaypointReportRequest request)
        {
            PayppointReportResponce response = RepositryUnit.giftRepository.GetAllPayPoints(request);

            var teamData = this.RepositryUnit.teamRepository.GetAll().ToList();
            response.lookUps.teamLookUps = teamData.Where(x => x.ACTIVE == 1).Select(x => new LookUpResponseModel() { lookUpDescription = x.TEAMNAME, lookUpId = x.TEAMID }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            var directorateData = this.RepositryUnit.teamsRepository.GetDirectorates().ToList();
            response.lookUps.directorateLookUps = directorateData.Select(x => new LookUpResponseModel() { lookUpDescription = x.lookUpDescription, lookUpId = x.lookUpId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var fiscalYearData = RepositryUnit.fIscalYearRepository.GetAll().OrderByDescending(e=>e.YRange);
            response.lookUps.fiscalYears = fiscalYearData.Select(x => new LookUpResponseModel() { lookUpDescription = x.YStart.Value.Year.ToString() + "-" + x.YEnd.Value.Year.ToString(), lookUpId = x.YRange }).OrderBy(x => x.lookUpDescription).ToList();
            var payPointStatusData = RepositryUnit.paypointStatusRepository.GetAll().ToList();
            response.lookUps.paypointStatus = payPointStatusData.Select(x => new LookUpResponseModel() { lookUpId = x.PayPointStatusId, lookUpDescription = x.Description }).ToList().OrderBy(x => x.lookUpDescription).ToList();
            return response;
        }

        public bool UpdatePaypointStatus(UpdatePaypointRequest request)
        {
            bool responce = RepositryUnit.giftRepository.UpdatePaypointStatus(request);
            return responce;
        }

        public decimal GetGradePointSalary(GradePointSalary gradePoint)
        {
            decimal responce = RepositryUnit.giftRepository.GetGradePointSalary(gradePoint);
            return responce;
        }
    }
}
