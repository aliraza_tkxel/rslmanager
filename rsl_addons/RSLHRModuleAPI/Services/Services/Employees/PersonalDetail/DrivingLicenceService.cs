﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DrivingLicenceService : BaseService
    {
        public DrivingLicenceService()
        {
        }
        public DrivingLicenceResponse GetDrivingLicence(int employeeId)
        {
            DrivingLicenceResponse response = new DrivingLicenceResponse();
            response.drivingLicence = RepositryUnit.drivingLicenceRepository.GetDrivingLicence(employeeId);
            response.disqualificationsList = RepositryUnit.drivingLicenceRepository.GetLicenceDisqualifications(employeeId);
            return response;
        }
        public void AddUpdateDrivingLicence(DrivingLicenceResponse request)
        {
            RepositryUnit.drivingLicenceRepository.AddUpdateDrivingLicence(request);
        }
        public bool UpdateDrivingLicenceImage(LicenceImageRequest request)
        {
            bool success = RepositryUnit.drivingLicenceRepository.UpdateDrivingLicenceImage(request);            
            return success;
        }
    }
}
