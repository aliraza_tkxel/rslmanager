﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Services.Employees
{
    public class EmployeesService : BaseService
    {

        public EmployeesService()
        {
        }



        public string GetEmployeeName(int employeeId)
        {
            try
            {
                PersonalDetailResponce res = GetEmployeeDetail(employeeId);
                return res.employeeDetail.firstName + " " + res.employeeDetail.lastName;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        public EmployeesResponse GetEmployeesList(EmployeesRequest model)
        {
            EmployeesResponse response = new EmployeesResponse();
            response = RepositryUnit.employeeRepository.GetEmployees(model);
            return response;
        }

        public PersonalDetailResponce GetEmployeeDetail(int employeeId)
        {
            PersonalDetailResponce response = new PersonalDetailResponce();
            response = RepositryUnit.employeeRepository.GetEmployeeDetail(employeeId);
            //this.RepositryUnit.Commit();
            return response;
        }

        public EmployeeContactDetail GetEmployeeContactDetail(int employeeId)
        {
            EmployeeContactDetail response = new EmployeeContactDetail();
            response = RepositryUnit.employeeRepository.GetEmployeeContactDetail(employeeId);
            //  this.RepositryUnit.Commit();
            return response;
        }
        public ContactDetail GetEmployeeContactDetailForMobile(int employeeId)
        {
            ContactDetail response = new ContactDetail();
            response = RepositryUnit.employeeRepository.GetEmployeeContactDetailForMobile(employeeId);
            //  this.RepositryUnit.Commit();
            return response;
        }


        public EmployeePersonalDetail UpdateEmployeePersonalDetail(EmployeePersonalDetail detail)
        {
            EmployeePersonalDetail response = new EmployeePersonalDetail();
            response = RepositryUnit.employeeRepository.UpdateEmployeePersonalDetail(detail);
            this.RepositryUnit.Commit();
            return response;
        }

        public EmployeeContactDetail UpdateContactPersonalDetail(EmployeeContactDetail detail)
        {
            EmployeeContactDetail response = new EmployeeContactDetail();
            response = RepositryUnit.employeeRepository.UpdateEmployeeContactDetail(detail);
            this.RepositryUnit.Commit();
            return response;
        }
        public Dictionary<int, string> GetEthnicityList()
        {
            Dictionary<int, string> descriptions = new Dictionary<int, string>();
            var ethinicityData = RepositryUnit.ethinicityRepository.GetAll().ToList();
            for (int count = 0; count < ethinicityData.Count; count++)
            {
                descriptions.Add(ethinicityData.ElementAt(count).ETHID,ethinicityData.ElementAt(count).DESCRIPTION);
            }
            return descriptions;
        }
        public Dictionary<int, string> GetreligionDataList()
        {
            Dictionary<int, string> descriptions = new Dictionary<int, string>();
            var religionData = RepositryUnit.religionRepository.GetAll().ToList();
            for (int count = 0; count < religionData.Count; count++)
            {
                descriptions.Add(religionData.ElementAt(count).RELIGIONID ,religionData.ElementAt(count).DESCRIPTION);
            }
            return descriptions;
        }
        public Dictionary<int, string> GetsexualOrientationDataList()
        {
            Dictionary<int, string> descriptions = new Dictionary<int, string>();
            var sexualOrientationData = RepositryUnit.sexualOrientationRepository.GetAll().ToList();
            for (int count = 0; count < sexualOrientationData.Count; count++)
            {
                descriptions.Add(sexualOrientationData.ElementAt(count).SEXUALORIENTATIONID, sexualOrientationData.ElementAt(count).DESCRIPTION);
            }
            return descriptions;
        }
        public Dictionary<int, string> GetmaritalStatusDataList()
        {
            Dictionary<int, string> descriptions = new Dictionary<int, string>();
            var maritalStatusData = RepositryUnit.maritalStatusRepository.GetAll().ToList();
            for (int count = 0; count < maritalStatusData.Count; count++)
            {
                descriptions.Add(maritalStatusData.ElementAt(count).MARITALSTATUSID, maritalStatusData.ElementAt(count).DESCRIPTION);
            }
            return descriptions;
        }
        public Dictionary<int, string> GetTitlesDataList()
        {
            Dictionary<int, string> descriptions = new Dictionary<int, string>();
            var titles = RepositryUnit.titleRepository.GetAll().ToList();
            for (int count = 0; count < titles.Count; count++)
            {
                descriptions.Add(titles.ElementAt(count).TITLEID, titles.ElementAt(count).DESCRIPTION);
            }
            return descriptions;
        }
        
        public EmployeeImage UpdateEmployeeImage(EmployeeImage request)
        {
            EmployeeImage response = new EmployeeImage();
            response = RepositryUnit.employeeRepository.UpdateEmployeeImage(request);
            this.RepositryUnit.Commit();
            return response;
        }


        public EmployeesResponse GetEmployeesMainDetail(EmployeesRequest model)
        {
            return RepositryUnit.employeeRepository.GetEmployeesMainDetail(model);
        }


    }
}
