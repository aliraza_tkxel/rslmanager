﻿using NetworkModel;
using NetworkModel.NetwrokModel.Authentication;
using NetworkModel.NetwrokModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AuthenticationService : BaseService
    {
        public LoginResponse AuthenticateUser(LoginRequest request)
        {
            LoginResponse response = new LoginResponse();
            response = RepositryUnit.authenticationRepository.AuthenticateUser(request);
            this.RepositryUnit.Commit();
            return response;
        }
    }
}
