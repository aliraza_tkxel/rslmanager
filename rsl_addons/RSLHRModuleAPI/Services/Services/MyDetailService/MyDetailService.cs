﻿using NetworkModel;

namespace Services.Services.MyDetailService
{
    public class MyDetailService : BaseService
    {
        public UserDetail GetMyDetail(int employeeId)
        {
            UserDetail responce = new UserDetail();
            responce = RepositryUnit.myDetailRepository.GetMydetail(employeeId);
            this.RepositryUnit.Commit();
            return responce;
        }

        public MyDetailResponce AmendDetail(UserDetail request)
        {
            MyDetailResponce responce = new MyDetailResponce();
            responce = RepositryUnit.myDetailRepository.AmendUserDetail(request);
            this.RepositryUnit.Commit();
            return responce;
        }


        public AnnualLeaveDetail GetAnnualLeaveDetail(AbsenceRequest request)
        {
            AnnualLeaveDetail responce = new AnnualLeaveDetail();
            responce = RepositryUnit.myDetailRepository.GetAnnualLeaveDetail(request);
            this.RepositryUnit.Commit();
            return responce;
        }
    }
}
