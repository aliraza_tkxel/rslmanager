﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.Remitance
{
    public class RemitanceService : BaseService
    {
        public RemitanceService()
        {

        }

        public List<InvoiceDetail> GetInvoiceDetail(int supplierId, string processDate, int paymentTypeId)
        {
            try
            {
                List<InvoiceDetail> invoiceDetail = RepositryUnit.remitanceRepository.getInvoiceDetail(processDate, supplierId, paymentTypeId);
                return invoiceDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Organization GetSupplierData(int supplierId)
        {
            try
            {
                Organization organization = RepositryUnit.remitanceRepository.getSupplierData(supplierId);
                return organization;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
