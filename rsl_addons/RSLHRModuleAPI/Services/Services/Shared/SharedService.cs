﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Services.Services.Shared
{
    public class SharedService : BaseService
    {
        public SharedLookUps loadSharedLookups(int? userId=0)
        {
            SharedLookUps sharedLookups = new SharedLookUps();

            // Get data for absence nature
            var natureData = RepositryUnit.absenceNatureRepository.GetAll().ToList();
            sharedLookups.absenceNature = natureData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.ITEMNATUREID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            // Get data for absence Status
            var statusData = RepositryUnit.absenceStatusRepository.includeItems();     
            sharedLookups.leaveStatus = statusData.Where(x => x.E_ITEM.DESCRIPTION == ApplicationConstants.StatusItemAbsence).Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.ITEMSTATUSID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            // Get data for absence Actions
            var actionData = RepositryUnit.absenceActionRepository.GetAll().ToList();
            sharedLookups.leaveActions = actionData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.ITEMACTIONID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            // get data for absense reason
            var absenceReasonData = RepositryUnit.absenceReasonRepository.GetAll().ToList();
            sharedLookups.absenceReason = absenceReasonData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.SID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            // get data for ethinicity
            var ethinicityData = RepositryUnit.ethinicityRepository.GetAll().ToList();
            sharedLookups.ethnicity = ethinicityData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.ETHID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            // get data for religion

            var religionData = RepositryUnit.religionRepository.GetAll().ToList();
            sharedLookups.religion = religionData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.RELIGIONID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            // get data for sexual orientation

            var sexualOrientationData = RepositryUnit.sexualOrientationRepository.GetAll().ToList();
            sharedLookups.sexualOrientation = sexualOrientationData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION, lookUpId = x.SEXUALORIENTATIONID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            // get data for marital statis

            var maritalStatusData = RepositryUnit.maritalStatusRepository.GetAll().ToList();
            sharedLookups.maritalStatus = maritalStatusData.Select(x => new LookUpResponseModel() { lookUpDescription = x.DESCRIPTION , lookUpId = x.MARITALSTATUSID }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            //get data for my staff
            var myStaffData = RepositryUnit.reportsRepository.GetMyStaffList(userId).ToList();
            sharedLookups.myStaff = myStaffData.Select(x => new LookUpResponseModel() { lookUpDescription = x.fullName, lookUpId = x.employeeId }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var titleData = RepositryUnit.titleRepository.GetAll().ToList();
            sharedLookups.title = titleData.Select(x => new LookUpResponseModel() {lookUpId = x.TITLEID, lookUpDescription = x.DESCRIPTION }).ToList().OrderBy(x => x.lookUpDescription).ToList();

            var data = RepositryUnit.bankHolidayRepository.GetAll().ToList();
            List<DateTime> bankHolidays = data.Where(x => x.BHDATE >= DateTime.Now).Select(x => x.BHDATE).ToList();
            sharedLookups.bankHolidays = bankHolidays;

            
            return sharedLookups;
        }
    }
}
