﻿using System;
using Services.Services.Absence;
using Services.Services.Employees;
using Services.Services.Employees.PostTab;
using Services.Services.MyDetailService;
using Services.Services.Reports;
using Services.Services.Shared;
using Services.Services;
using Services.Services.Admin.Teams;
using Services.Services.Remitance;
using Services.Services.Push;

namespace Services
{
    public class ServicesProxy : IServiceProxy
    {
        private AuthenticationService _authenticationService;
        private NavigationService _navigationService;

        private EmployeesService _employeesService;
        private SharedService _sharedService;

        private MyDetailService _myDetailService;
        private HealthService _healthService;
        private SkillsService _skillsService;
        private PostTabService _postTabService;
        private BenefitsService _benefitsService;
        private ReportsService _reportsService;
        private AbsenceService _absenceService;
        private TeamsService _teamsService;
        private AccessControlService _accessControlService;
        private DocumentsService _documentsService;
        private TrainingsService _trainingsService;
        private GiftService _giftService;
        private DeclerationOfInterestService _declerationOfInterestService;
        private DashboardService _dashboardService;
        private DrivingLicenceService _drivingLicenceService;
        private RemitanceService _remitanceService;
        private PushService _pushService;
        public AuthenticationService AuthenticationService
        {
            get
            {
                if (_authenticationService == null)
                {
                    _authenticationService = new AuthenticationService();
                }
                return _authenticationService;
            }
        }

        public NavigationService NavigationService
        {
            get
            {
                if (_navigationService == null)
                {
                    _navigationService = new NavigationService();
                }
                return _navigationService;
            }
        }

        public EmployeesService EmployeesService
        {
            get
            {
                if (_employeesService == null)
                {
                    _employeesService = new EmployeesService();
                }
                return _employeesService;
            }
        }

        public TeamsService TeamsService
        {
            get
            {
                if (_teamsService == null)
                {
                    _teamsService = new TeamsService();
                }
                return _teamsService;
            }
        }
        public AccessControlService AccessControlService
        {
            get
            {
                if (_accessControlService == null)
                {
                    _accessControlService = new AccessControlService();
                }
                return _accessControlService;
            }
        }
        public SharedService SharedService
        {
            get
            {
                if (_sharedService == null)
                {
                    _sharedService = new SharedService();
                }
                return _sharedService;
            }
        }

        public MyDetailService MyDetailService
        {
            get
            {
                if (_myDetailService == null)
                {
                    _myDetailService = new MyDetailService();
                }
                return _myDetailService;
            }
        }

        public HealthService HealthService
        {
            get
            {
                if (_healthService == null)
                {
                    _healthService = new HealthService();
                }
                return _healthService;
            }
        }
        public SkillsService SkillsService
        {
            get
            {
                if (_skillsService == null)
                {
                    _skillsService = new SkillsService();
                }
                return _skillsService;
            }
        }

        public PostTabService PostTabService
        {
            get
            {
                if (_postTabService == null)
                {
                    _postTabService = new PostTabService();
                }
                return _postTabService;
            }
        }

        public BenefitsService BenefitsService
        {
            get
            {
                if (_benefitsService == null)
                {
                    _benefitsService = new BenefitsService();
                }
                return _benefitsService;
            }
        }
        public ReportsService ReportsService
        {
            get
            {
                if (_reportsService == null)
                {
                    _reportsService = new ReportsService();
                }
                return _reportsService;
            }
        }

        public AbsenceService AbsenceService
        {
            get
            {
                if (_absenceService == null)
                {
                    _absenceService = new AbsenceService();
                }
                return _absenceService;
            }
        }

        public DocumentsService DocumentsService
        {
            get
            {
                if (_documentsService == null)
                {
                    _documentsService = new DocumentsService();
                }
                return _documentsService;
            }
        }
        public TrainingsService TrainingsService
        {
            get
            {
                if (_trainingsService == null)
                {
                    _trainingsService = new TrainingsService();
                }
                return _trainingsService;
            }
        }


        public GiftService GiftService
        {
            get
            {
                if (_giftService == null)
                {
                    _giftService = new GiftService();
                }
                return _giftService;
            }
        }

        public DeclerationOfInterestService DeclerationOfInterestService
        {
            get
            {
                if (_declerationOfInterestService == null)
                {
                    _declerationOfInterestService = new DeclerationOfInterestService();
                }
                return _declerationOfInterestService;
            }
        }

        public DashboardService DashboardService
        {
            get
            {
                if (_dashboardService == null)
                {
                    _dashboardService = new DashboardService();
                }
                return _dashboardService;
            }
        }
        public DrivingLicenceService DrivingLicenceService
        {
            get
            {
                if (_drivingLicenceService == null)
                {
                    _drivingLicenceService = new DrivingLicenceService();
                }
                return _drivingLicenceService;
            }
        }

        public RemitanceService RemitanceService
        {
            get
            {
                if (_remitanceService == null)
                {
                    _remitanceService = new RemitanceService();
                }
                return _remitanceService;
            }
        }

        public PushService PushService
        {
            get
            {
                if (_pushService == null)
                {
                    _pushService = new PushService();
                }
                return _pushService;
            }
        }

        public void Dispose()
        {
            _benefitsService = null;
            _authenticationService = null;
            _documentsService = null;
            _employeesService = null;
            _healthService = null;
            _myDetailService = null;
            _postTabService = null;
            _reportsService = null;
            _sharedService = null;
            _skillsService = null;
            _giftService = null;
            _dashboardService = null;
            _drivingLicenceService = null;
            _remitanceService = null;
            _pushService = null;
        }
    }
}
