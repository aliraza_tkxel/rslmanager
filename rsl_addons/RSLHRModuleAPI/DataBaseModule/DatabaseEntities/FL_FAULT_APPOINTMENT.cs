//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBaseModule.DatabaseEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class FL_FAULT_APPOINTMENT
    {
        public int FaultAppointmentId { get; set; }
        public int FaultLogId { get; set; }
        public int AppointmentId { get; set; }
    
        public virtual FL_CO_APPOINTMENT FL_CO_APPOINTMENT { get; set; }
        public virtual FL_FAULT_LOG FL_FAULT_LOG { get; set; }
    }
}
