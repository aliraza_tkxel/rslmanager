//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBaseModule.DatabaseEntities
{
    using System;
    
    public partial class E_GetPayPointReport_Result
    {
        public string team { get; set; }
        public string directorate { get; set; }
        public string gradeDescription { get; set; }
        public string gradePointDescription { get; set; }
        public string name { get; set; }
        public string submittedByName { get; set; }
        public string reviewDate { get; set; }
        public Nullable<System.DateTime> paypointReviewDate { get; set; }
        public string statusDisplay { get; set; }
        public string status { get; set; }
        public int employeeId { get; set; }
        public int payPointId { get; set; }
        public Nullable<int> supportedBy { get; set; }
        public Nullable<int> authorizedBy { get; set; }
        public string reason { get; set; }
        public string imagePath { get; set; }
    }
}
