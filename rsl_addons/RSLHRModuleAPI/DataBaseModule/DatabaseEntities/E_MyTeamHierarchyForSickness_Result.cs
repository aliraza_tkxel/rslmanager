//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBaseModule.DatabaseEntities
{
    using System;
    
    public partial class E_MyTeamHierarchyForSickness_Result
    {
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public string EmployeeFName { get; set; }
        public string EmployeeLName { get; set; }
        public Nullable<int> LineManagerId { get; set; }
        public Nullable<System.DateTime> ReturnDate { get; set; }
        public Nullable<double> Duration { get; set; }
        public string Unit { get; set; }
        public string LeaveStatus { get; set; }
        public Nullable<int> ItemNatureId { get; set; }
        public string NatureDescription { get; set; }
        public Nullable<int> AbsenceHistoryId { get; set; }
        public string Reason { get; set; }
        public Nullable<int> ReasonId { get; set; }
        public Nullable<System.DateTime> AnticipatedReturnDate { get; set; }
        public Nullable<int> StatusId { get; set; }
        public string holyType { get; set; }
        public string notes { get; set; }
    }
}
