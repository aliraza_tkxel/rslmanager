//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBaseModule.DatabaseEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class E_NEXTOFKIN
    {
        public int NEXTOFKINID { get; set; }
        public Nullable<int> EMPLOYEEID { get; set; }
        public string FNAME { get; set; }
        public string SNAME { get; set; }
        public string CONTACT { get; set; }
        public string HOUSENUMBER { get; set; }
        public string STREET { get; set; }
        public string POSTALTOWN { get; set; }
        public string COUNTY { get; set; }
        public string POSTCODE { get; set; }
        public string HOMETEL { get; set; }
        public string MOBILE { get; set; }
        public Nullable<int> LASTACTIONUSER { get; set; }
        public Nullable<System.DateTime> LASTACTIONTIME { get; set; }
    
        public virtual E__EMPLOYEE E__EMPLOYEE { get; set; }
    }
}
