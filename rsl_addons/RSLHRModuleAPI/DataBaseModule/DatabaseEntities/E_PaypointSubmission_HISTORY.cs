//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBaseModule.DatabaseEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class E_PaypointSubmission_HISTORY
    {
        public int PaypointHistoryId { get; set; }
        public int PaypointId { get; set; }
        public Nullable<double> Salary { get; set; }
        public Nullable<int> Grade { get; set; }
        public Nullable<int> GradePoint { get; set; }
        public Nullable<System.DateTime> DateProposed { get; set; }
        public string DetailRationale { get; set; }
        public Nullable<bool> Approved { get; set; }
        public Nullable<bool> Supported { get; set; }
        public Nullable<bool> Authorized { get; set; }
        public Nullable<System.DateTime> ApprovedDate { get; set; }
        public Nullable<System.DateTime> SupportedDate { get; set; }
        public Nullable<System.DateTime> AuthorizedDate { get; set; }
        public Nullable<int> ApprovedBy { get; set; }
        public Nullable<int> SupportedBy { get; set; }
        public Nullable<int> AuthorizedBy { get; set; }
        public Nullable<int> employeeId { get; set; }
        public Nullable<int> paypointStatusId { get; set; }
        public Nullable<int> fiscalYearId { get; set; }
        public string Reason { get; set; }
        public Nullable<bool> IsNotSubmitted { get; set; }
    }
}
