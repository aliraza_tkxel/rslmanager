﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using Utilities;

namespace DataBaseModule.DataRepository
{
    public class BenefitsRepository : BaseRepository<E_BENEFITS>
    {
        public BenefitsRepository(Entities dbContext) : base(dbContext)
        {

        }

        #region Helpers

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }

        private F_FISCALYEARS getFiscalYear(int yRange)
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YRange == yRange).FirstOrDefault();

            return fYear;
        }


        #endregion

        public BenefitsDetail GetEmployeeBenefits(int employeeId)
        {
            BenefitsDetail response = new BenefitsDetail();         
            var data = (from S in DbContext.E_BENEFITS
                        where S.EMPLOYEEID == employeeId 
                        select new BenefitsDetail
                        {
                            benefitId = S.BENEFITID,
                            employeeId = S.EMPLOYEEID,
                            ///////Accomodation////////////////
                            accomodationRent = S.ACCOMODATIONRENT,
                            councilTax = S.COUNCILTAX,
                            heating = S.HEATING,
                            lineRental = S.LINERENTAL,
                            ////////////Company Car///////////
                            carMake = S.CARMAKE,
                            model = S.MODEL,
                            listPrice = S.LISTPRICE,
                            contHireCharge = S.CONTHIRECHARGE,
                            empContribution = S.EMPCONTRIBUTION,
                            excessContribution = S.EXCESSCONTRIBUTION,
                            carStartDate = S.CARSTARTDATE,
                            co2Emissions = S.CO2EMISSIONS,
                            fuel = S.FUEL,
                            compEmpContribution = S.COMPEMPCONTRIBUTION,
                            drivingLicenceNo = S.DRIVINGLICENCENO,
                            motCertNo = S.MOTCERTNO,
                            insuranceNo = S.INSURANCENO,
                            insuranceRenewalDate = S.INSURANCERENEWALDATE,
                            drivingLicenceImage = S.DrivingLicenceImage,
                            //////////// Essential User////////////////// 
                            carAllowance = S.CARALLOWANCE,
                            engineSize = S.ENGINESIZE,
                            additionalDriver = S.AdditionalDriver,
                            ecuTopUp = S.ECUTopUp,
                            motRenewalDate = S.MOTRenewalDate,
                            toolAllowance = S.ToolAllowance,
                            firstAid = S.FirstAid,
                            unionSubscription = S.UnionSubscription,
                            lifeAssurance = S.LifeAssurance,
                            enhancedHolidayEntitlement = S.EnhancedHolidayEntitlement,
                            carParkingFacilities = S.CarParkingFacilities,
                            eyeCareAssistance = S.EyeCareAssistance,
                            employeeAssistanceProgramme = S.EmployeeAssistanceProgramme,
                            enhancedSickPay = S.EnhancedSickPay,
                            learningAndDevelopment = S.LearningAndDevelopment,
                            professionalSubscriptions = S.ProfessionalSubscriptions,
                            childcareVouchers = S.ChildcareVouchers,
                            fluAndHepBJabs = S.FluAndHepBJabs,
                            contractedOut = S.CONTRACTEDOUT,
                            carLoan = S.LOANINFORMATION,
                            //---------General------------------
                            professionalFees = S.PROFESSIONALFEES,
                            telephoneAllowance = S.TELEPHONEALLOWANCE,
                            firstAidAllowance = S.FIRSTAIDALLOWANCE,
                            callOutAllowance = S.CALLOUTALLOWANCE,

                            medicalOrgId = S.MEDICALORGID,
                            taxableBenefit = S.TAXABLEBENEFIT,
                            annualPremium = S.ANNUALPREMIUM,
                            additionalMembers = S.ADDITIONALMEMBERS,
                            additionalMembers2 = S.ADDITIONALMEMBERS2,
                            additionalMembers3 = S.ADDITIONALMEMBERS3,
                            additionalMembers4 = S.ADDITIONALMEMBERS4,
                            additionalMembers5 = S.ADDITIONALMEMBERS5,
                            groupSchemeRef = S.GROUPSCHEMEREF,
                            membershipNo = S.MEMBERSHIPNO,
                            /////////////////////// Pension/////////
                            memNumber = S.MEMNUMBER,
                            scheme = S.SCHEME,
                            schemeNumber = S.SCHEMENUMBER,
                            salaryPercent = S.SALARYPERCENT,
                            employeeContribution = S.EMPLOYEECONTRIBUTION,
                            employerContribution = S.EMPLOYERCONTRIBUTION,
                            avc = S.AVC,
                            lastActionUser = S.LASTACTIONUSER,

                        });

            var fData = getCurrentFiscalYear();


            var vehiclelst = (from S in DbContext.E_BENEFITSVEHICLE
                              where S.EMPLOYEEID == employeeId &&   S.FISCALYEAR == fData.YRange
                              select S).ToList();
           
            var gData = (from G in DbContext.E_BENEFITSGENERAL
                         where G.EMPLOYEEID == employeeId && G.FISCALYEAR == fData.YRange
                         select G).FirstOrDefault();
            if (gData != null)
            {
                BenefitGeneral general = new BenefitGeneral();
                general.benefitGeneralId = gData.BENEFITGENERALID;
                general.fiscalYear = gData.FISCALYEAR;
                general.employeeId = gData.EMPLOYEEID;
                general.benefitId = gData.BENEFITID;
                general.callOutAllowance = gData.CALLOUTALLOWANCE;
                general.birthdayLeave = gData.BIRTHDAYLEAVE;
                general.carParking = gData.CARPARKINGFACILITIES;
                general.childCareVouchers = gData.CHILDCAREVOUCHERS;
                general.employeeAssistance = gData.EMPLOYEEASSISTANCE;
                general.enhancedHoliday = gData.ENHANCEDHOLIDAY;
                general.enhancedSickPay = gData.ENHANCEDSICKPAY;
                general.ECU = gData.ECU;
                general.EnhancedFamilyLeave = gData.EnhancedFamilyLeave;
                general.PHI = gData.PHI;
                general.Other = gData.Other;
                general.Overtime = gData.Overtime;
                general.eyeCareAssistance = gData.EYECAREASSISTANCE;
                general.firstAiderAllowance = gData.FIRSTAIDERALLOWANCE;
                general.fluHepBJab = gData.FLUHEPBJABS;
                general.learningAndDevelopment = gData.LEARNINGDEVELOPMENT;
                general.lifeAssurance = gData.LIFEASSURANCE;
                general.personalDay = gData.PERSONALDAY;
                general.VoluntaryDay = gData.VOLUNTARYDAY;
                general.birthdayLeaveDate = gData.BIRTHDAYLEAVEDATE.HasValue ? gData.BIRTHDAYLEAVEDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.carParkingDate = gData.CARPARKINGFACILITIESDATE.HasValue ? gData.CARPARKINGFACILITIESDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.childCareVouchersDate = gData.CHILDCAREVOUCHERSDATE.HasValue ? gData.CHILDCAREVOUCHERSDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.employeeAssistanceDate = gData.EMPLOYEEASSISTANCEDATE.HasValue ? gData.EMPLOYEEASSISTANCEDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.enhancedHolidayDate = gData.ENHANCEDHOLIDAYDATE.HasValue ? gData.ENHANCEDHOLIDAYDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.enhancedSickPayDate = gData.ENHANCEDSICKPAYDATE.HasValue ? gData.ENHANCEDSICKPAYDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.eyeCareAssistanceDate = gData.EYECAREASSISTANCEDATE.HasValue ? gData.EYECAREASSISTANCEDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.firstAiderAllowanceDate = gData.FIRSTAIDERALLOWANCEDATE.HasValue ? gData.FIRSTAIDERALLOWANCEDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.fluHepBJabDate = gData.FLUHEPBJABSDATE.HasValue ? gData.FLUHEPBJABSDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.learningAndDevelopmentDate = gData.LEARNINGDEVELOPMENTDATE.HasValue ? gData.LEARNINGDEVELOPMENTDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.lifeAssuranceDate = gData.LIFEASSURANCEDATE.HasValue ? gData.LIFEASSURANCEDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.personalDayDate = gData.PERSONALDAYDATE.HasValue ? gData.PERSONALDAYDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.VoluntaryDayDate = gData.VOLUNTARYDAYDATE.HasValue ? gData.VOLUNTARYDAYDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.callOutAllowanceDate = gData.CALLOUTALLOWANCEDATE.HasValue ? gData.CALLOUTALLOWANCEDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.ECUDate = gData.ECUDate.HasValue ? gData.ECUDate.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.EnhancedFamilyLeaveDate = gData.EnhancedFamilyLeaveDate.HasValue ? gData.EnhancedFamilyLeaveDate.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.PHIDate = gData.PHIDate.HasValue ? gData.PHIDate.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.OtherDate = gData.OtherDate.HasValue ? gData.OtherDate.Value.ToString("dd/MM/yyyy") : String.Empty;
                general.OvertimeDate = gData.OvertimeDate.HasValue ? gData.OvertimeDate.Value.ToString("dd/MM/yyyy") : String.Empty;

                general.isReadOnly = false;
            }
            {

            }
            if (data.Count() > 0)
            {
                response = data.FirstOrDefault();
                if (vehiclelst.Count() > 0)
                {
                    response.vehiclelst = new List<BenefitVehicle>();
                    foreach (E_BENEFITSVEHICLE item in vehiclelst)
                    {
                        var bv = new BenefitVehicle();
                        bv.benefitVehicleId = item.BENEFITVEHICLEID;
                        bv.fiscalYear = item.FISCALYEAR.HasValue ? item.FISCALYEAR.Value : 0;
                        bv.employeeId = item.EMPLOYEEID;
                        bv.benefitId = item.BENEFITID;
                        bv.carStartDate = item.CARSTARTDATE.HasValue ? item.CARSTARTDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                        bv.carEndDate = item.CARENDDATE.HasValue ? item.CARENDDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                        bv.carRegistration = item.CARREGISTRATION;
                        bv.carMake = item.CARMAKE;
                        bv.model = item.CARMODEL;
                        bv.fuel = item.FUEL;
                        bv.engineSize = item.ENGINESIZE;
                        bv.additionalDriver = item.AdditionalDriver;
                        bv.motRenewalDate = item.MOTRENEWALDATE.HasValue ? item.MOTRENEWALDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                        bv.Type = item.Type;
                        if (item.V5PATH != null && item.V5PATH.Length > 0)
                        {
                            bv.V5Path = FileHelper.getLogicalRefDocUploadPath(item.V5PATH, item.EMPLOYEEID);
                            bv.V5Path = item.V5PATH;
                        }
                        bv.insuranceCompany = item.INSURANCECOMPANY;
                        bv.insuranceRenewalDate = item.INSURANCERENEWALDATE.HasValue ? item.INSURANCERENEWALDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                        bv.insuranceReimbursement = item.INSURANCEREIMBURSEMENT;

                        bv.dvlaOnline = item.DVLAONLINE;
                        bv.dvlaOnlineDate = item.DVLAONLINEDATE.HasValue ? item.DVLAONLINEDATE.Value.ToString("dd/MM/yyyy") : String.Empty;
                        if (item.POLICYSUMMARYPATH != null && item.POLICYSUMMARYPATH.Length > 0)
                        {
                            bv.policySummaryPath = FileHelper.getLogicalRefDocUploadPath(item.POLICYSUMMARYPATH, item.EMPLOYEEID);
                            bv.Summarydoc = item.POLICYSUMMARYPATH;
                        }
                        response.vehiclelst.Add(bv);
                    }
                }
            }
            return response;
        }
        public void AddAmendBenefits(BenefitsDetail model)
        {


            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    E_BENEFITS benefit = new E_BENEFITS();
                    var benefitResult = (from S in DbContext.E_BENEFITS where (S.EMPLOYEEID == model.employeeId) select S);
                    if (benefitResult.Count() > 0)
                    {
                        benefit = benefitResult.FirstOrDefault();
                    }
                    benefit.EMPLOYEEID = model.employeeId;
                    //Accomodation=model.//Accomodation;
                    benefit.ACCOMODATIONRENT = model.accomodationRent;
                    benefit.COUNCILTAX = model.councilTax;
                    benefit.HEATING = model.heating;
                    benefit.LINERENTAL = model.lineRental;

                    benefit.CARMAKE = model.carMake;
                    benefit.MODEL = model.model;
                    benefit.CONTHIRECHARGE = model.contHireCharge;
                    benefit.EMPCONTRIBUTION = model.empContribution;
                    benefit.EXCESSCONTRIBUTION = model.excessContribution;
                    benefit.CARSTARTDATE = model.carStartDate;
                    benefit.LISTPRICE = model.listPrice;
                    benefit.LOANINFORMATION = model.carLoan;
                    benefit.CO2EMISSIONS = model.co2Emissions;
                    benefit.FUEL = model.fuel;
                    benefit.COMPEMPCONTRIBUTION = model.compEmpContribution;
                    benefit.DRIVINGLICENCENO = model.drivingLicenceNo;
                    benefit.MOTCERTNO = model.motCertNo;
                    benefit.INSURANCENO = model.insuranceNo;
                    benefit.INSURANCERENEWALDATE = model.insuranceRenewalDate;
                    benefit.DrivingLicenceImage = model.drivingLicenceImage;

                    benefit.CARALLOWANCE = model.carAllowance;
                    benefit.ENGINESIZE = model.engineSize;
                    benefit.AdditionalDriver = model.additionalDriver;
                    benefit.ECUTopUp = model.ecuTopUp;
                    benefit.MOTRenewalDate = model.motRenewalDate;
                    benefit.ToolAllowance = model.toolAllowance;
                    benefit.FirstAid = model.firstAid;
                    benefit.UnionSubscription = model.unionSubscription;
                    benefit.LifeAssurance = model.lifeAssurance;
                    benefit.EnhancedHolidayEntitlement = model.enhancedHolidayEntitlement;
                    benefit.CarParkingFacilities = model.carParkingFacilities;
                    benefit.EyeCareAssistance = model.eyeCareAssistance;
                    benefit.EmployeeAssistanceProgramme = model.employeeAssistanceProgramme;
                    benefit.EnhancedSickPay = model.enhancedSickPay;
                    benefit.LearningAndDevelopment = model.learningAndDevelopment;
                    benefit.ProfessionalSubscriptions = model.professionalSubscriptions;
                    benefit.ChildcareVouchers = model.childcareVouchers;
                    benefit.FluAndHepBJabs = model.fluAndHepBJabs;

                    benefit.PROFESSIONALFEES = model.professionalFees;
                    benefit.TELEPHONEALLOWANCE = model.telephoneAllowance;
                    benefit.FIRSTAIDALLOWANCE = model.firstAidAllowance;
                    benefit.CALLOUTALLOWANCE = model.callOutAllowance;

                    benefit.MEDICALORGID = model.medicalOrgId;
                    benefit.TAXABLEBENEFIT = model.taxableBenefit;
                    benefit.ANNUALPREMIUM = model.annualPremium;
                    benefit.ADDITIONALMEMBERS = model.additionalMembers;
                    benefit.ADDITIONALMEMBERS2 = model.additionalMembers2;
                    benefit.ADDITIONALMEMBERS3 = model.additionalMembers3;
                    benefit.ADDITIONALMEMBERS4 = model.additionalMembers4;
                    benefit.ADDITIONALMEMBERS5 = model.additionalMembers5;
                    benefit.GROUPSCHEMEREF = model.groupSchemeRef;
                    benefit.MEMBERSHIPNO = model.membershipNo;

                    benefit.MEMNUMBER = model.memNumber;
                    benefit.SCHEME = model.scheme;
                    benefit.SCHEMENUMBER = model.schemeNumber;
                    benefit.SALARYPERCENT = model.salaryPercent;
                    benefit.EMPLOYEECONTRIBUTION = model.employeeContribution;
                    benefit.EMPLOYERCONTRIBUTION = model.employerContribution;

                    benefit.AVC = model.avc;
                    benefit.CONTRACTEDOUT = model.contractedOut;
                    benefit.LASTACTIONUSER = model.lastActionUser;
                    benefit.LASTACTIONTIME = DateTime.Now;
                    if (benefitResult.Count() == 0)
                    {
                        DbContext.E_BENEFITS.Add(benefit);
                        
                    }
                    DbContext.SaveChanges();
                    if(model.benefitId==0)
                    {
                        model.benefitId = benefit.BENEFITID;

                    }
                    if (model.requestType == "vehicle")
                    {
                        List<E_BENEFITSVEHICLE> vehicles = new List<E_BENEFITSVEHICLE>();
                        vehicles = (from V in DbContext.E_BENEFITSVEHICLE where (V.BENEFITID == benefit.BENEFITID) select V).ToList();
                        if (vehicles.Count() > 0)
                        {
                            foreach (E_BENEFITSVEHICLE item in vehicles)
                            {
                                if (model.vehicle != null)
                                {
                                    if (item.BENEFITVEHICLEID == model.vehicle.benefitVehicleId)
                                    {
                                        item.EMPLOYEEID = model.vehicle.employeeId;
                                        item.BENEFITVEHICLEID = model.vehicle.benefitVehicleId;
                                        item.FISCALYEAR = model.vehicle.fiscalYear;
                                        item.BENEFITID = model.vehicle.benefitId;
                                        if (!string.IsNullOrEmpty(model.vehicle.carStartDate))
                                            item.CARSTARTDATE = DateTime.ParseExact(model.vehicle.carStartDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.vehicle.carEndDate))
                                            item.CARENDDATE = DateTime.ParseExact(model.vehicle.carEndDate, "dd/MM/yyyy", null);
                                        item.CARREGISTRATION = model.vehicle.carRegistration;
                                        item.CARMAKE = model.vehicle.carMake;
                                        item.CARMODEL = model.vehicle.model;
                                        item.FUEL = model.vehicle.fuel;
                                        item.ENGINESIZE = model.vehicle.engineSize;
                                        item.AdditionalDriver = model.vehicle.additionalDriver;
                                        if (!string.IsNullOrEmpty(model.vehicle.motRenewalDate))
                                            item.MOTRENEWALDATE = DateTime.ParseExact(model.vehicle.motRenewalDate, "dd/MM/yyyy", null);
                                        item.V5PATH = model.vehicle.V5Path;
                                        item.INSURANCECOMPANY = model.vehicle.insuranceCompany;
                                        if (!string.IsNullOrEmpty(model.vehicle.insuranceRenewalDate))
                                            item.INSURANCERENEWALDATE = DateTime.ParseExact(model.vehicle.insuranceRenewalDate, "dd/MM/yyyy", null);
                                        item.INSURANCEREIMBURSEMENT = model.vehicle.insuranceReimbursement;
                                        item.POLICYSUMMARYPATH = model.vehicle.policySummaryPath;
                                        item.DVLAONLINE = model.vehicle.dvlaOnline;
                                        if (!string.IsNullOrEmpty(model.vehicle.dvlaOnlineDate))
                                            item.DVLAONLINEDATE = DateTime.ParseExact(model.vehicle.dvlaOnlineDate, "dd/MM/yyyy", null);
                                        item.Type = model.vehicle.Type;

                                    }

                                }
                            }

                            if (model.vehicle.benefitVehicleId == 0)

                            {
                                E_BENEFITSVEHICLE benefitV = new E_BENEFITSVEHICLE();
                                benefitV.BENEFITVEHICLEID = model.vehicle.benefitVehicleId;
                                benefitV.BENEFITID = model.vehicle.benefitId;
                                benefitV.EMPLOYEEID = model.vehicle.employeeId;
                                if (!string.IsNullOrEmpty(model.vehicle.carStartDate))
                                    benefitV.CARSTARTDATE = DateTime.ParseExact(model.vehicle.carStartDate, "dd/MM/yyyy", null);
                                if (!string.IsNullOrEmpty(model.vehicle.carEndDate))
                                    benefitV.CARENDDATE = DateTime.ParseExact(model.vehicle.carEndDate, "dd/MM/yyyy", null);
                                benefitV.CARREGISTRATION = model.vehicle.carRegistration;
                                benefitV.CARMAKE = model.vehicle.carMake;
                                benefitV.CARMODEL = model.vehicle.model;
                                benefitV.FUEL = model.vehicle.fuel;
                                benefitV.ENGINESIZE = model.vehicle.engineSize;
                                benefitV.AdditionalDriver = model.vehicle.additionalDriver;
                                if (!string.IsNullOrEmpty(model.vehicle.motRenewalDate))
                                    benefitV.MOTRENEWALDATE = DateTime.ParseExact(model.vehicle.motRenewalDate, "dd/MM/yyyy", null);
                                benefitV.V5PATH = model.vehicle.V5Path;
                                benefitV.INSURANCECOMPANY = model.vehicle.insuranceCompany;
                                if (!string.IsNullOrEmpty(model.vehicle.insuranceRenewalDate))
                                    benefitV.INSURANCERENEWALDATE = DateTime.ParseExact(model.vehicle.insuranceRenewalDate, "dd/MM/yyyy", null);
                                benefitV.INSURANCEREIMBURSEMENT = model.vehicle.insuranceReimbursement;
                                benefitV.POLICYSUMMARYPATH = model.vehicle.policySummaryPath;
                                benefitV.DVLAONLINE = model.vehicle.dvlaOnline;
                                if (!string.IsNullOrEmpty(model.vehicle.dvlaOnlineDate))
                                    benefitV.DVLAONLINEDATE = DateTime.ParseExact(model.vehicle.dvlaOnlineDate, "dd/MM/yyyy", null);
                                benefitV.FISCALYEAR = model.vehicle.fiscalYear;
                                benefitV.Type = model.vehicle.Type;
                                DbContext.E_BENEFITSVEHICLE.Add(benefitV);

                            }
                        }
                        else
                        {
                            var item = new E_BENEFITSVEHICLE();
                            item.EMPLOYEEID = model.vehicle.employeeId;
                            item.FISCALYEAR = model.vehicle.fiscalYear;
                            item.BENEFITID = benefit.BENEFITID;
                            if (!string.IsNullOrEmpty(model.vehicle.carStartDate))
                                item.CARSTARTDATE = DateTime.ParseExact(model.vehicle.carStartDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.vehicle.carEndDate))
                                item.CARENDDATE = DateTime.ParseExact(model.vehicle.carEndDate, "dd/MM/yyyy", null);
                            item.CARREGISTRATION = model.vehicle.carRegistration;
                            item.CARMAKE = model.vehicle.carMake;
                            item.CARMODEL = model.vehicle.model;
                            item.FUEL = model.vehicle.fuel;
                            item.ENGINESIZE = model.vehicle.engineSize;
                            item.AdditionalDriver = model.vehicle.additionalDriver;
                            if (!string.IsNullOrEmpty(model.vehicle.motRenewalDate))
                                item.MOTRENEWALDATE = DateTime.ParseExact(model.vehicle.motRenewalDate, "dd/MM/yyyy", null);
                            item.V5PATH = model.vehicle.V5Path;
                            item.INSURANCECOMPANY = model.vehicle.insuranceCompany;
                            if (!string.IsNullOrEmpty(model.vehicle.insuranceRenewalDate))
                                item.INSURANCERENEWALDATE = DateTime.ParseExact(model.vehicle.insuranceRenewalDate, "dd/MM/yyyy", null);
                            item.INSURANCEREIMBURSEMENT = model.vehicle.insuranceReimbursement;
                            item.POLICYSUMMARYPATH = model.vehicle.policySummaryPath;
                            item.DVLAONLINE = model.vehicle.dvlaOnline;
                            if (!string.IsNullOrEmpty(model.vehicle.dvlaOnlineDate))
                                item.DVLAONLINEDATE = DateTime.ParseExact(model.vehicle.dvlaOnlineDate, "dd/MM/yyyy", null);
                            item.Type = model.vehicle.Type;
                            DbContext.E_BENEFITSVEHICLE.Add(item);

                        }


                        DbContext.SaveChanges();
                    }


                    if (model.requestType == "general")
                    {
                        List<E_BENEFITSGENERAL> benefitGenerals = new List<E_BENEFITSGENERAL>();
                        benefitGenerals = (from V in DbContext.E_BENEFITSGENERAL where (V.BENEFITID == benefit.BENEFITID) select V).ToList();
                        if (benefitGenerals.Count() > 0)
                        {
                            foreach (E_BENEFITSGENERAL item in benefitGenerals)
                            {
                                if (model.general != null)
                                {
                                    if (item.BENEFITGENERALID == model.general.benefitGeneralId)
                                    {

                                        item.BENEFITGENERALID = model.general.benefitGeneralId;
                                        item.FISCALYEAR = model.general.fiscalYear;
                                        item.EMPLOYEEID = model.general.employeeId;
                                        item.BENEFITID = model.general.benefitId;
                                        item.CALLOUTALLOWANCE = model.general.callOutAllowance;
                                        item.BIRTHDAYLEAVE = model.general.birthdayLeave;
                                        item.CARPARKINGFACILITIES = model.general.carParking;
                                        item.CHILDCAREVOUCHERS = model.general.childCareVouchers;
                                        item.EMPLOYEEASSISTANCE = model.general.employeeAssistance;
                                        item.ENHANCEDHOLIDAY = model.general.enhancedHoliday;
                                        item.ENHANCEDSICKPAY = model.general.enhancedSickPay;
                                        item.EYECAREASSISTANCE = model.general.eyeCareAssistance;
                                        item.FIRSTAIDERALLOWANCE = model.general.firstAiderAllowance;
                                        item.FLUHEPBJABS = model.general.fluHepBJab;
                                        item.LEARNINGDEVELOPMENT = model.general.learningAndDevelopment;
                                        item.LIFEASSURANCE = model.general.lifeAssurance;
                                        item.PERSONALDAY = model.general.personalDay;
                                        item.VOLUNTARYDAY = model.general.VoluntaryDay;
                                        item.ECU = model.general.ECU;
                                        item.EnhancedFamilyLeave = model.general.EnhancedFamilyLeave;
                                        item.PHI = model.general.PHI;
                                        item.Other = model.general.Other;
                                        item.Overtime = model.general.Overtime;
                                        if (!string.IsNullOrEmpty(model.general.callOutAllowanceDate))
                                            item.CALLOUTALLOWANCEDATE = DateTime.ParseExact(model.general.callOutAllowanceDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.birthdayLeaveDate))
                                            item.BIRTHDAYLEAVEDATE = DateTime.ParseExact(model.general.birthdayLeaveDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.carParkingDate))
                                            item.CARPARKINGFACILITIESDATE = DateTime.ParseExact(model.general.carParkingDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.childCareVouchersDate))
                                            item.CHILDCAREVOUCHERSDATE = DateTime.ParseExact(model.general.childCareVouchersDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.employeeAssistanceDate))
                                            item.EMPLOYEEASSISTANCEDATE = DateTime.ParseExact(model.general.employeeAssistanceDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.enhancedHolidayDate))
                                            item.ENHANCEDHOLIDAYDATE = DateTime.ParseExact(model.general.enhancedHolidayDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.enhancedSickPayDate))
                                            item.ENHANCEDSICKPAYDATE = DateTime.ParseExact(model.general.enhancedSickPayDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.eyeCareAssistanceDate))
                                            item.EYECAREASSISTANCEDATE = DateTime.ParseExact(model.general.eyeCareAssistanceDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.firstAiderAllowanceDate))
                                            item.FIRSTAIDERALLOWANCEDATE = DateTime.ParseExact(model.general.firstAiderAllowanceDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.fluHepBJabDate))
                                            item.FLUHEPBJABSDATE = DateTime.ParseExact(model.general.fluHepBJabDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.learningAndDevelopmentDate))
                                            item.LEARNINGDEVELOPMENTDATE = DateTime.ParseExact(model.general.learningAndDevelopmentDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.lifeAssuranceDate))
                                            item.LIFEASSURANCEDATE = DateTime.ParseExact(model.general.lifeAssuranceDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.personalDayDate))
                                            item.PERSONALDAYDATE = DateTime.ParseExact(model.general.personalDayDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.VoluntaryDayDate))
                                            item.VOLUNTARYDAYDATE = DateTime.ParseExact(model.general.VoluntaryDayDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.ECUDate))
                                            item.ECUDate = DateTime.ParseExact(model.general.ECUDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.EnhancedFamilyLeaveDate))
                                            item.EnhancedFamilyLeaveDate = DateTime.ParseExact(model.general.EnhancedFamilyLeaveDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.PHIDate))
                                            item.PHIDate = DateTime.ParseExact(model.general.PHIDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.OtherDate))
                                            item.OtherDate = DateTime.ParseExact(model.general.OtherDate, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.general.OvertimeDate))
                                            item.OvertimeDate = DateTime.ParseExact(model.general.OvertimeDate, "dd/MM/yyyy", null);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var general = new E_BENEFITSGENERAL();
                            general.BENEFITGENERALID = model.general.benefitGeneralId;
                            general.FISCALYEAR = model.general.fiscalYear;
                            general.EMPLOYEEID = model.general.employeeId;
                            general.BENEFITID = model.benefitId;
                            general.CALLOUTALLOWANCE = model.general.callOutAllowance;
                            general.BIRTHDAYLEAVE = model.general.birthdayLeave;
                            general.CARPARKINGFACILITIES = model.general.carParking;
                            general.CHILDCAREVOUCHERS = model.general.childCareVouchers;
                            general.EMPLOYEEASSISTANCE = model.general.employeeAssistance;
                            general.ENHANCEDHOLIDAY = model.general.enhancedHoliday;
                            general.ENHANCEDSICKPAY = model.general.enhancedSickPay;
                            general.EYECAREASSISTANCE = model.general.eyeCareAssistance;
                            general.FIRSTAIDERALLOWANCE = model.general.firstAiderAllowance;
                            general.FLUHEPBJABS = model.general.fluHepBJab;
                            general.LEARNINGDEVELOPMENT = model.general.learningAndDevelopment;
                            general.LIFEASSURANCE = model.general.lifeAssurance;
                            general.PERSONALDAY = model.general.personalDay;
                            general.VOLUNTARYDAY = model.general.VoluntaryDay;
                            general.ECU = model.general.ECU;
                            general.EnhancedFamilyLeave = model.general.EnhancedFamilyLeave;
                            general.PHI = model.general.PHI;
                            general.Other = model.general.Other;
                            general.Overtime = model.general.Overtime;
                            if (!string.IsNullOrEmpty(model.general.callOutAllowanceDate))
                                general.CALLOUTALLOWANCEDATE = DateTime.ParseExact(model.general.callOutAllowanceDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.birthdayLeaveDate))
                                general.BIRTHDAYLEAVEDATE = DateTime.ParseExact(model.general.birthdayLeaveDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.carParkingDate))
                                general.CARPARKINGFACILITIESDATE = DateTime.ParseExact(model.general.carParkingDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.childCareVouchersDate))
                                general.CHILDCAREVOUCHERSDATE = DateTime.ParseExact(model.general.childCareVouchersDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.employeeAssistanceDate))
                                general.EMPLOYEEASSISTANCEDATE = DateTime.ParseExact(model.general.employeeAssistanceDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.enhancedHolidayDate))
                                general.ENHANCEDHOLIDAYDATE = DateTime.ParseExact(model.general.enhancedHolidayDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.enhancedSickPayDate))
                                general.ENHANCEDSICKPAYDATE = DateTime.ParseExact(model.general.enhancedSickPayDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.eyeCareAssistanceDate))
                                general.EYECAREASSISTANCEDATE = DateTime.ParseExact(model.general.eyeCareAssistanceDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.firstAiderAllowanceDate))
                                general.FIRSTAIDERALLOWANCEDATE = DateTime.ParseExact(model.general.firstAiderAllowanceDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.fluHepBJabDate))
                                general.FLUHEPBJABSDATE = DateTime.ParseExact(model.general.fluHepBJabDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.learningAndDevelopmentDate))
                                general.LEARNINGDEVELOPMENTDATE = DateTime.ParseExact(model.general.learningAndDevelopmentDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.lifeAssuranceDate))
                                general.LIFEASSURANCEDATE = DateTime.ParseExact(model.general.lifeAssuranceDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.personalDayDate))
                                general.PERSONALDAYDATE = DateTime.ParseExact(model.general.personalDayDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.VoluntaryDayDate))
                                general.VOLUNTARYDAYDATE = DateTime.ParseExact(model.general.VoluntaryDayDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.ECUDate))
                                general.ECUDate = DateTime.ParseExact(model.general.ECUDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.EnhancedFamilyLeaveDate))
                                general.EnhancedFamilyLeaveDate = DateTime.ParseExact(model.general.EnhancedFamilyLeaveDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.PHIDate))
                                general.PHIDate = DateTime.ParseExact(model.general.PHIDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.OtherDate))
                                general.OtherDate = DateTime.ParseExact(model.general.OtherDate, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.general.OvertimeDate))
                                general.OvertimeDate = DateTime.ParseExact(model.general.OvertimeDate, "dd/MM/yyyy", null);
                            DbContext.E_BENEFITSGENERAL.Add(general);
                        }
                        DbContext.SaveChanges();
                    }

                    if (model.requestType == "health")
                    {
                        List<E_BENEFITSHEALTH> benefitHealth = new List<E_BENEFITSHEALTH>();
                        benefitHealth = (from V in DbContext.E_BENEFITSHEALTH where (V.BENEFITID == benefit.BENEFITID) select V).ToList();
                        if (benefitHealth.Count() > 0)
                        {
                            foreach (E_BENEFITSHEALTH item in benefitHealth)
                            {
                                if (model.health != null)
                                {
                                    if (item.BENEFITHEALTHID == model.health.benefitHealthId)
                                    {

                                        item.BENEFITHEALTHID = model.health.benefitHealthId;
                                        item.FISCALYEAR = model.health.fiscalYear;
                                        item.EMPLOYEEID = model.health.employeeId;
                                        item.BENEFITID = model.health.benefitId;
                                        item.MEDICALORGID = model.health.medicalOrgId;
                                        item.ISPRIVATEMEDICAL = model.health.isPrivateMedical;
                                        item.EXCESSPAID = model.health.excessPaid;
                                        item.ANNUALPREMIUM = model.health.annualPremium;
                                        item.MEMBERSHIPNUMBER = model.health.membershipNumber;
                                        if (!string.IsNullOrEmpty(model.health.effectiveFrom))
                                            item.EFFECTIVEFROM = DateTime.ParseExact(model.health.effectiveFrom, "dd/MM/yyyy", null);
                                        if (!string.IsNullOrEmpty(model.health.endDate))
                                            item.ENDDATE = DateTime.ParseExact(model.health.endDate, "dd/MM/yyyy", null);

                                    }
                                }
                            }
                        }
                        else
                        {
                            var item = new E_BENEFITSHEALTH();
                            item.BENEFITHEALTHID = model.health.benefitHealthId;
                            item.FISCALYEAR = model.health.fiscalYear;
                            item.EMPLOYEEID = model.health.employeeId;
                            item.BENEFITID = model.benefitId;
                            item.MEDICALORGID = model.health.medicalOrgId;
                            item.ISPRIVATEMEDICAL = model.health.isPrivateMedical;
                            item.EXCESSPAID = model.health.excessPaid;
                            item.ANNUALPREMIUM = model.health.annualPremium;
                            item.MEMBERSHIPNUMBER = model.health.membershipNumber;
                            if (!string.IsNullOrEmpty(model.health.effectiveFrom))
                                item.EFFECTIVEFROM = DateTime.ParseExact(model.health.effectiveFrom, "dd/MM/yyyy", null);
                            if (!string.IsNullOrEmpty(model.health.endDate))
                                item.ENDDATE = DateTime.ParseExact(model.health.endDate, "dd/MM/yyyy", null);
                            DbContext.E_BENEFITSHEALTH.Add(item);
                        }
                        DbContext.SaveChanges();
                    }
                    if (model.requestType == "pension")
                    {
                        List<E_BENEFITSPENSION> benefitPension = new List<E_BENEFITSPENSION>();
                        benefitPension = (from V in DbContext.E_BENEFITSPENSION where (V.BENEFITID == benefit.BENEFITID) select V).ToList();
                        if (benefitPension.Count() > 0)
                        {
                            foreach (E_BENEFITSPENSION item in benefitPension)
                            {
                                if (model.pension != null)
                                {
                                    if (item.BENEFITSPENSIONID  == model.pension.benefitPensionId)
                                    {

                                        item.BENEFITSPENSIONID = model.pension.benefitPensionId;
                                        item.FISCALYEAR = model.pension.fiscalYear;
                                        item.EMPLOYEEID = model.pension.employeeId;
                                        item.BENEFITID = model.pension.benefitId;
                                        item.ACCOMODATIONRENT = model.pension.accomodationRent;
                                        item.COUNCILTAX = model.pension.councilTax;
                                        item.LINERENTAL = model.pension.lineRental;
                                        item.HEATING = model.pension.heating;
                                        item.SALARYPERCENT = model.pension.salaryPercent;
                                        item.EMPLOYEECONTRIBUTION = model.pension.employeeContribution;
                                        item.EMPLOYERCONTRIBUTION = model.pension.employerContribution;
                                        item.AVC = model.pension.avc;
                                        item.CONTRACTEDOUT = model.pension.contractedOut;
                                    }
                                }
                            }
                        }
                        else
                        {
                            var item = new E_BENEFITSPENSION();
                            item.BENEFITSPENSIONID = model.pension.benefitPensionId;
                            item.FISCALYEAR = model.pension.fiscalYear;
                            item.EMPLOYEEID = model.pension.employeeId;
                            item.BENEFITID = model.pension.benefitId;
                            item.ACCOMODATIONRENT = model.pension.accomodationRent;
                            item.COUNCILTAX = model.pension.councilTax;
                            item.LINERENTAL = model.pension.lineRental;
                            item.HEATING = model.pension.heating;
                            item.SALARYPERCENT = model.pension.salaryPercent;
                            item.EMPLOYEECONTRIBUTION = model.pension.employeeContribution;
                            item.EMPLOYERCONTRIBUTION = model.pension.employerContribution;
                            item.AVC = model.pension.avc;
                            item.CONTRACTEDOUT = model.pension.contractedOut;
                            DbContext.E_BENEFITSPENSION.Add(item);
                        }
                        DbContext.SaveChanges();
                    }
                    if (model.requestType == "PRP")
                    {
                        List<E_BENEFITSPRP> benefitPRP = new List<E_BENEFITSPRP>();
                        benefitPRP = (from V in DbContext.E_BENEFITSPRP where (V.BENEFITID == benefit.BENEFITID) select V).ToList();
                        if (benefitPRP.Count() > 0)
                        {
                            foreach (E_BENEFITSPRP item in benefitPRP)
                            {
                                if (model.prp != null)
                                {
                                    if (item.BENEFITSPRPID == model.prp.benefitPRPId)
                                    {

                                        item.BENEFITSPRPID = model.prp.benefitPRPId;
                                        item.FISCALYEAR = model.prp.fiscalYear;
                                        item.EMPLOYEEID = model.prp.employeeId;
                                        item.BENEFITID = model.prp.benefitId;
                                        item.Q1 = model.prp.q1;
                                        item.Q2 = model.prp.q2;
                                        item.Q3 = model.prp.q3;
                                        item.Q4 = model.prp.q4;
                                    }
                                }
                            }
                        }
                        else
                        {
                            var item = new E_BENEFITSPRP();
                            item.BENEFITSPRPID = model.prp.benefitPRPId;
                            item.FISCALYEAR = model.prp.fiscalYear;
                            item.EMPLOYEEID = model.prp.employeeId;
                            item.BENEFITID = model.prp.benefitId;
                            item.Q1 = model.prp.q1;
                            item.Q2 = model.prp.q2;
                            item.Q3 = model.prp.q3;
                            item.Q4 = model.prp.q4;
                            DbContext.E_BENEFITSPRP.Add(item);
                        }
                        DbContext.SaveChanges();
                    }
                    if (model.requestType == "subscription")
                    {
                        int fiscalYear = 0;
                        foreach (var subVM in model.subscriptionlst)
                        {

                            fiscalYear = subVM.fiscalYear;
                        }

                        List<E_BENEFITSSUBSCRIPTION> subList = new List<E_BENEFITSSUBSCRIPTION>();
                        subList = (from S in DbContext.E_BENEFITSSUBSCRIPTION where (S.BENEFITID == model.benefitId && S.EMPLOYEEID == model.employeeId && S.FISCALYEAR == fiscalYear) select S).ToList();
                        if (subList.Count() > 0)
                        {
                            foreach (E_BENEFITSSUBSCRIPTION item in subList)
                            {
                                DbContext.E_BENEFITSSUBSCRIPTION.Remove(item);
                            }
                        }

                        foreach (var item in model.subscriptionlst)
                        {
                            E_BENEFITSSUBSCRIPTION sub = new E_BENEFITSSUBSCRIPTION();
                            sub.BENEFITID = model.benefitId;
                            sub.EMPLOYEEID = model.employeeId;
                            sub.SUBSCRIPTIONTITLE = item.subscriptionTitle;
                            sub.SUBSCRIPTIONCOST = item.subscriptionCost;
                            sub.FISCALYEAR = item.fiscalYear;
                            sub.BENEFITSUBSCRIPTIONID = 0;
                            DbContext.E_BENEFITSSUBSCRIPTION.Add(sub);

                        }

                        DbContext.SaveChanges();

                    }


                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();



                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }




            }


            ////////////////////



        }
        public BenefitVehicle GetBenefitsVehicle(int Id)
        {
            BenefitVehicle vehicle = new BenefitVehicle();
            var S = (from v in DbContext.E_BENEFITSVEHICLE
                     where v.BENEFITVEHICLEID == Id
                     select v).FirstOrDefault();
            if (S != null)
            {
                vehicle.benefitVehicleId = S.BENEFITVEHICLEID;
                vehicle.benefitId = S.BENEFITID;
                vehicle.employeeId = S.EMPLOYEEID;
                vehicle.fiscalYear = S.FISCALYEAR.HasValue ? S.FISCALYEAR.Value : 0;
                if (S.CARSTARTDATE.HasValue)
                {
                    DateTime startDate = (DateTime)S.CARSTARTDATE;
                    vehicle.carStartDate = startDate.ToString("dd/MM/yyyy");
                }
                if (S.CARENDDATE.HasValue)
                {
                    DateTime endDate = (DateTime)S.CARENDDATE;
                    vehicle.carEndDate = endDate.ToString("dd/MM/yyyy");
                }
                if (S.MOTRENEWALDATE.HasValue)
                {
                    DateTime motDate = (DateTime)S.MOTRENEWALDATE;
                    vehicle.motRenewalDate = motDate.ToString("dd/MM/yyyy");
                }
                if (S.INSURANCERENEWALDATE.HasValue)
                {
                    DateTime insuranceDate = (DateTime)S.INSURANCERENEWALDATE;
                    vehicle.insuranceRenewalDate = insuranceDate.ToString("dd/MM/yyyy");
                }
                if (S.DVLAONLINEDATE.HasValue)
                {
                    DateTime dvlaDate = (DateTime)S.DVLAONLINEDATE;
                    vehicle.dvlaOnlineDate = dvlaDate.ToString("dd/MM/yyyy");
                }
                vehicle.carRegistration = S.CARREGISTRATION;
                vehicle.carMake = S.CARMAKE;
                vehicle.model = S.CARMODEL;
                vehicle.fuel = S.FUEL;
                vehicle.engineSize = S.ENGINESIZE;
                vehicle.additionalDriver = S.AdditionalDriver;
                vehicle.V5Path = S.V5PATH;
                vehicle.insuranceCompany = S.INSURANCECOMPANY;
                vehicle.insuranceReimbursement = S.INSURANCEREIMBURSEMENT;
                // vehicle.policySummaryPath = S.POLICYSUMMARYPATH;
                if (S.V5PATH != null && S.V5PATH.Length > 0)
                {
                    vehicle.V5Path = FileHelper.getLogicalRefDocUploadPath(S.V5PATH, S.EMPLOYEEID);
                    vehicle.V5doc = S.V5PATH;
                }
                if (S.POLICYSUMMARYPATH != null && S.POLICYSUMMARYPATH.Length > 0)
                {
                    vehicle.policySummaryPath = FileHelper.getLogicalRefDocUploadPath(S.POLICYSUMMARYPATH, S.EMPLOYEEID);
                    vehicle.Summarydoc = S.POLICYSUMMARYPATH;
                }
                vehicle.dvlaOnline = S.DVLAONLINE;
                vehicle.Type = S.Type;
            }
            var fiscalYear = getFiscalYear(vehicle.fiscalYear);
            if (fiscalYear.YStart <= DateTime.Now && DateTime.Now <= fiscalYear.YEnd)
            {
                vehicle.isReadOnly = false;
            }
            else
            {
                vehicle.isReadOnly = true;
            }
            return vehicle;
        }
        public void DeleteBenefitV5(BenefitsDetail request)
        {
            //HealthResponse response = new HealthResponse();
            //List<E_BENEFITSVEHICLE> disablityObj = new List<E_BENEFITSVEHICLE>();
            string reviewDate = String.Empty;
            if (request.benefitId > 0)
            {
                // var disablityResult = (from e in DbContext.E_BENEFITS where (e.BENEFITID == request.benefitId) select e).FirstOrDefault();
                var disablityResult = DbContext.E_BENEFITS
                        .Where(b => b.BENEFITID == request.benefitId)
                        .Include(b => b.E_BENEFITSVEHICLE)
                        .FirstOrDefault();
                foreach (E_BENEFITSVEHICLE ve in disablityResult.E_BENEFITSVEHICLE)
                {
                    if (ve.BENEFITVEHICLEID == request.vehicle.benefitVehicleId)
                    {
                        ve.V5PATH = String.Empty;
                        DbContext.SaveChanges();
                    }
                }
            }


        }
        public BenefitGeneral GetBenefitsGeneral(GeneralDataRequest data)
        {
            BenefitGeneral general = new BenefitGeneral();
            var G = (from v in DbContext.E_BENEFITSGENERAL
                     where v.EMPLOYEEID == data.employeeId && v.FISCALYEAR == data.fiscalYear && v.BENEFITID == data.benefitId
                     select v).FirstOrDefault();
            if (G != null)
            {
                general.benefitGeneralId = G.BENEFITGENERALID;
                general.fiscalYear = G.FISCALYEAR;
                general.employeeId = G.EMPLOYEEID;
                general.benefitId = G.BENEFITID;
                general.callOutAllowance = G.CALLOUTALLOWANCE;
                general.birthdayLeave = G.BIRTHDAYLEAVE;
                general.carParking = G.CARPARKINGFACILITIES;
                general.childCareVouchers = G.CHILDCAREVOUCHERS;
                general.employeeAssistance = G.EMPLOYEEASSISTANCE;
                general.enhancedHoliday = G.ENHANCEDHOLIDAY;
                general.enhancedSickPay = G.ENHANCEDSICKPAY;
                general.eyeCareAssistance = G.EYECAREASSISTANCE;
                general.firstAiderAllowance = G.FIRSTAIDERALLOWANCE;
                general.fluHepBJab = G.FLUHEPBJABS;
                general.learningAndDevelopment = G.LEARNINGDEVELOPMENT;
                general.lifeAssurance = G.LIFEASSURANCE;
                general.personalDay = G.PERSONALDAY;
                general.VoluntaryDay = G.VOLUNTARYDAY;
                general.ECU = G.ECU;
                general.EnhancedFamilyLeave = G.EnhancedFamilyLeave;
                general.PHI = G.PHI;
                general.Other = G.Other;
                general.Overtime = G.Overtime;
                if (G.BIRTHDAYLEAVEDATE.HasValue)
                {
                    DateTime birthady = (DateTime)G.BIRTHDAYLEAVEDATE;
                    general.birthdayLeaveDate = birthady.ToString("dd/MM/yyyy");
                }
                if (G.CARPARKINGFACILITIESDATE.HasValue)
                {
                    DateTime carDate = (DateTime)G.CARPARKINGFACILITIESDATE;
                    general.carParkingDate = carDate.ToString("dd/MM/yyyy");
                }
                if (G.CHILDCAREVOUCHERSDATE.HasValue)
                {
                    DateTime childCare = (DateTime)G.CHILDCAREVOUCHERSDATE;
                    general.childCareVouchersDate = childCare.ToString("dd/MM/yyyy");

                }
                if (G.EMPLOYEEASSISTANCEDATE.HasValue)
                {
                    DateTime employeeAssistance = (DateTime)G.EMPLOYEEASSISTANCEDATE;
                    general.employeeAssistanceDate = employeeAssistance.ToString("dd/MM/yyyy");
                }
                if (G.ENHANCEDHOLIDAYDATE.HasValue)
                {
                    DateTime enhanced = (DateTime)G.ENHANCEDHOLIDAYDATE;
                    general.enhancedHolidayDate = enhanced.ToString("dd/MM/yyyy");
                }
                if (G.EYECAREASSISTANCEDATE.HasValue)
                {
                    DateTime eyeCare = (DateTime)G.EYECAREASSISTANCEDATE;
                    general.eyeCareAssistanceDate = eyeCare.ToString("dd/MM/yyyy");
                }
                if (G.ENHANCEDSICKPAYDATE.HasValue)
                {
                    DateTime enhancedSick = (DateTime)G.ENHANCEDSICKPAYDATE;
                    general.enhancedSickPayDate = enhancedSick.ToString("dd/MM/yyyy");
                }
                if (G.FIRSTAIDERALLOWANCEDATE.HasValue)
                {
                    DateTime firstAider = (DateTime)G.FIRSTAIDERALLOWANCEDATE;
                    general.firstAiderAllowanceDate = firstAider.ToString("dd/MM/yyyy");
                }
                if (G.FLUHEPBJABSDATE.HasValue)
                {
                    DateTime flu = (DateTime)G.FLUHEPBJABSDATE;
                    general.fluHepBJabDate = flu.ToString("dd/MM/yyyy");
                }
                if (G.LEARNINGDEVELOPMENTDATE.HasValue)
                {
                    DateTime learning = (DateTime)G.LEARNINGDEVELOPMENTDATE;
                    general.learningAndDevelopmentDate = learning.ToString("dd/MM/yyyy");
                }
                if (G.LIFEASSURANCEDATE.HasValue)
                {
                    DateTime life = (DateTime)G.LIFEASSURANCEDATE;
                    general.lifeAssuranceDate = life.ToString("dd/MM/yyyy");
                }
                if (G.PERSONALDAYDATE.HasValue)
                {
                    DateTime personal = (DateTime)G.PERSONALDAYDATE;
                    general.personalDayDate = personal.ToString("dd/MM/yyyy");
                }
                if (G.VOLUNTARYDAYDATE.HasValue)
                {
                    DateTime voluntary = (DateTime)G.VOLUNTARYDAYDATE;
                    general.VoluntaryDayDate = voluntary.ToString("dd/MM/yyyy");
                }
                if (G.CALLOUTALLOWANCEDATE.HasValue)
                {
                    DateTime callOut = (DateTime)G.CALLOUTALLOWANCEDATE;
                    general.callOutAllowanceDate = callOut.ToString("dd/MM/yyyy");
                }
                if (G.FIRSTAIDERALLOWANCEDATE.HasValue)
                {
                    DateTime firstAider = (DateTime)G.FIRSTAIDERALLOWANCEDATE;
                    general.firstAiderAllowanceDate = firstAider.ToString("dd/MM/yyyy");
                }

                if (G.ECUDate.HasValue)
                {
                    DateTime ecuD = (DateTime)G.ECUDate;
                    general.ECUDate = ecuD.ToString("dd/MM/yyyy");
                }
                if (G.EnhancedFamilyLeaveDate.HasValue)
                {
                    DateTime eflD = (DateTime)G.EnhancedFamilyLeaveDate;
                    general.EnhancedFamilyLeaveDate = eflD.ToString("dd/MM/yyyy");
                }
                if (G.PHIDate.HasValue)
                {
                    DateTime phiD = (DateTime)G.PHIDate;
                    general.PHIDate = phiD.ToString("dd/MM/yyyy");
                }
                if (G.OtherDate.HasValue)
                {
                    DateTime oD = (DateTime)G.OtherDate;
                    general.OtherDate = oD.ToString("dd/MM/yyyy");
                }
                if (G.OvertimeDate.HasValue)
                {
                    DateTime otD = (DateTime)G.OvertimeDate;
                    general.OvertimeDate = otD.ToString("dd/MM/yyyy");
                }

            }
            var fiscalYear = getFiscalYear(data.fiscalYear);
            if (fiscalYear.YStart <= DateTime.Now && DateTime.Now <= fiscalYear.YEnd)
            {
                general.isReadOnly = false;
            }
            else
            {
                general.isReadOnly = true;
            }
            return general;
        }

        public List<BenefitVehicle> GetBenefitsVehicles(GeneralDataRequest data)
        {
            BenefitGeneral general = new BenefitGeneral();
            var lst = new List<BenefitVehicle>();
            var vlist = (from v in DbContext.E_BENEFITSVEHICLE
                     where v.EMPLOYEEID == data.employeeId && v.FISCALYEAR == data.fiscalYear && v.BENEFITID == data.benefitId
                     select v).ToList();
            if (vlist != null)
            {

                foreach (var S in vlist)
                {

                    var vehicle = new BenefitVehicle();
                    vehicle.benefitVehicleId = S.BENEFITVEHICLEID;
                    vehicle.benefitId = S.BENEFITID;
                    vehicle.employeeId = S.EMPLOYEEID;
                    vehicle.fiscalYear = S.FISCALYEAR.HasValue ? S.FISCALYEAR.Value : 0;
                    if (S.CARSTARTDATE.HasValue)
                    {
                        DateTime startDate = (DateTime)S.CARSTARTDATE;
                        vehicle.carStartDate = startDate.ToString("dd/MM/yyyy");
                    }
                    if (S.CARENDDATE.HasValue)
                    {
                        DateTime endDate = (DateTime)S.CARENDDATE;
                        vehicle.carEndDate = endDate.ToString("dd/MM/yyyy");
                    }
                    if (S.MOTRENEWALDATE.HasValue)
                    {
                        DateTime motDate = (DateTime)S.MOTRENEWALDATE;
                        vehicle.motRenewalDate = motDate.ToString("dd/MM/yyyy");
                    }
                    if (S.INSURANCERENEWALDATE.HasValue)
                    {
                        DateTime insuranceDate = (DateTime)S.INSURANCERENEWALDATE;
                        vehicle.insuranceRenewalDate = insuranceDate.ToString("dd/MM/yyyy");
                    }
                    if (S.DVLAONLINEDATE.HasValue)
                    {
                        DateTime dvlaDate = (DateTime)S.DVLAONLINEDATE;
                        vehicle.dvlaOnlineDate = dvlaDate.ToString("dd/MM/yyyy");
                    }
                    vehicle.carRegistration = S.CARREGISTRATION;
                    vehicle.carMake = S.CARMAKE;
                    vehicle.model = S.CARMODEL;
                    vehicle.fuel = S.FUEL;
                    vehicle.engineSize = S.ENGINESIZE;
                    vehicle.additionalDriver = S.AdditionalDriver;
                    vehicle.V5Path = S.V5PATH;
                    vehicle.insuranceCompany = S.INSURANCECOMPANY;
                    vehicle.insuranceReimbursement = S.INSURANCEREIMBURSEMENT;
                    // vehicle.policySummaryPath = S.POLICYSUMMARYPATH;
                    if (S.V5PATH != null && S.V5PATH.Length > 0)
                    {
                        vehicle.V5Path = FileHelper.getLogicalRefDocUploadPath(S.V5PATH, S.EMPLOYEEID);
                        vehicle.V5doc = S.V5PATH;
                    }
                    if (S.POLICYSUMMARYPATH != null && S.POLICYSUMMARYPATH.Length > 0)
                    {
                        vehicle.policySummaryPath = FileHelper.getLogicalRefDocUploadPath(S.POLICYSUMMARYPATH, S.EMPLOYEEID);
                        vehicle.Summarydoc = S.POLICYSUMMARYPATH;
                    }
                    vehicle.dvlaOnline = S.DVLAONLINE;
                    vehicle.Type = S.Type;
                    lst.Add(vehicle);


                }
               
            }
            return lst;
        }

        public BenefitHealth GetBenefitsHealth(GeneralDataRequest data)
        {
            BenefitHealth health = new BenefitHealth();
            var item = (from v in DbContext.E_BENEFITSHEALTH
                     where v.EMPLOYEEID == data.employeeId && v.FISCALYEAR == data.fiscalYear && v.BENEFITID == data.benefitId
                     select v).FirstOrDefault();
            if (item != null)
            {

               health.benefitHealthId = item.BENEFITHEALTHID;
               health.fiscalYear = item.FISCALYEAR;
               health.employeeId = item.EMPLOYEEID;
               health.benefitId = item.BENEFITID;
               health.medicalOrgId = item.MEDICALORGID;
               health.isPrivateMedical = item.ISPRIVATEMEDICAL;
               health.excessPaid = item.EXCESSPAID;
               health.annualPremium = item.ANNUALPREMIUM;
               health.membershipNumber = item.MEMBERSHIPNUMBER;
                if (item.EFFECTIVEFROM.HasValue)
                {
                    DateTime effective = (DateTime)item.EFFECTIVEFROM;
                    health.effectiveFrom = effective.ToString("dd/MM/yyyy");
                }
                if (item.ENDDATE.HasValue)
                {
                    DateTime end = (DateTime)item.ENDDATE;
                    health.endDate = end.ToString("dd/MM/yyyy");
                }
            }
            var fiscalYear = getFiscalYear(data.fiscalYear);
            if (fiscalYear.YStart <= DateTime.Now && DateTime.Now <= fiscalYear.YEnd)
            {
                health.isReadOnly = false;
            }
            else
            {
                health.isReadOnly = true;
            }
            return health;
        }

        public BenefitPension GetBenefitsPension(GeneralDataRequest data)
        {
            BenefitPension pension = new BenefitPension();
            var item = (from v in DbContext.E_BENEFITSPENSION
                        where v.EMPLOYEEID == data.employeeId && v.FISCALYEAR == data.fiscalYear && v.BENEFITID == data.benefitId
                        select v).FirstOrDefault();
            if (item != null)
            {

                pension.benefitPensionId = item.BENEFITSPENSIONID;
                pension.fiscalYear = item.FISCALYEAR;
                pension.employeeId = item.EMPLOYEEID;
                pension.benefitId = item.BENEFITID;
                pension.accomodationRent = item.ACCOMODATIONRENT;
                pension.councilTax = item.COUNCILTAX;
                pension.lineRental = item.LINERENTAL;
                pension.heating = item.HEATING;
                pension.salaryPercent = item.SALARYPERCENT;
                pension.employeeContribution = item.EMPLOYEECONTRIBUTION;
                pension.employerContribution = item.EMPLOYERCONTRIBUTION;
                pension.avc = item.AVC;
                pension.contractedOut = item.CONTRACTEDOUT;
            }
            var fiscalYear = getFiscalYear(data.fiscalYear);
            if (fiscalYear.YStart <= DateTime.Now && DateTime.Now <= fiscalYear.YEnd)
            {
                pension.isReadOnly = false;
            }
            else
            {
                pension.isReadOnly = true;
            }
            return pension;
        }

        public BenefitPRP GetBenefitsPRP(GeneralDataRequest data)
        {
            BenefitPRP prp = new BenefitPRP();
            var item = (from v in DbContext.E_BENEFITSPRP
                        where v.EMPLOYEEID == data.employeeId && v.FISCALYEAR == data.fiscalYear && v.BENEFITID == data.benefitId
                        select v).FirstOrDefault();
            if (item != null)
            {

                prp.benefitPRPId = item.BENEFITSPRPID;
                prp.fiscalYear = item.FISCALYEAR;
                prp.employeeId = item.EMPLOYEEID;
                prp.benefitId = item.BENEFITID;
                prp.q1 = item.Q1;
                prp.q2 = item.Q2;
                prp.q3 = item.Q3;
                prp.q4 = item.Q4;
            }
            var fiscalYear = getFiscalYear(data.fiscalYear);
            if (fiscalYear.YStart <= DateTime.Now && DateTime.Now <= fiscalYear.YEnd)
            {
                prp.isReadOnly = false;
            }
            else
            {
                prp.isReadOnly = true;
            }
            return prp;
        }

        public List<BenefitHealthHistory> GetBenefitHealthHistory(int BenefitHealthId)
        {
            List<BenefitHealthHistory> HealthHistory = new List<BenefitHealthHistory>();

            var MedicalType= (from MT in DbContext.E_MEDICALTYPE
                              select MT).ToList();
            var MedicalORG = (from MO in DbContext.E_MEDICALORG
                               select MO).ToList();
            var fiscalYears = (from f in DbContext.F_FISCALYEARS
                         select f).ToList();
            var historyList = (from bhh in DbContext.E_BENEFITSHEALTHHISTORY
                        where bhh.BENEFITHEALTHID == BenefitHealthId 
                        select bhh).ToList();
            if (historyList != null)
            {
                foreach (var history in historyList)
                {
                    BenefitHealthHistory HistoryObject = new BenefitHealthHistory();
                    HistoryObject.fiscalYear = history.FISCALYEAR;
                    HistoryObject.employeeId = history.EMPLOYEEID;
                    HistoryObject.benefitId = history.BENEFITID;
                    HistoryObject.medicalOrgId = int.Parse(history.MEDICALORGID.ToString());
                    HistoryObject.isPrivateMedical = history.ISPRIVATEMEDICAL;
                    HistoryObject.excessPaid = decimal.Parse(string.Format("{0:#.##}", history.EXCESSPAID));
                    HistoryObject.annualPremium = decimal.Parse(string.Format("{0:#.##}", history.ANNUALPREMIUM));
                    HistoryObject.membershipNumber = history.MEMBERSHIPNUMBER;
                    if (history.EFFECTIVEFROM.HasValue)
                    {
                        DateTime effective = (DateTime)history.EFFECTIVEFROM;
                        HistoryObject.effectiveFrom = effective.ToString("dd/MM/yyyy");
                    }
                    if (history.ENDDATE.HasValue)
                    {
                        DateTime end = (DateTime)history.ENDDATE;
                        HistoryObject.endDate = end.ToString("dd/MM/yyyy");
                    }
                    HistoryObject.Type = MedicalType.Where(x => x.MEDICALTYPEID == HistoryObject.isPrivateMedical).FirstOrDefault().MEDICALTYPENAME;
                    HistoryObject.Provider = MedicalORG.Where(x => x.MEDICALORGID == HistoryObject.medicalOrgId).FirstOrDefault().MEDICALORGNAME;
                    HistoryObject.fiscalYears = fiscalYears.Where(x => x.YRange == HistoryObject.fiscalYear).FirstOrDefault().YStart.Value.Year.ToString() + "/" +
                        fiscalYears.Where(x => x.YRange == HistoryObject.fiscalYear).FirstOrDefault().YEnd.Value.Year.ToString();
                    HealthHistory.Add(HistoryObject);
                }   
            }
            return HealthHistory;
        }
        public List<BenefitSubscription> GetBenefitsSubscription(GeneralDataRequest request)
        {
            List<BenefitSubscription> respopnse = new List<BenefitSubscription>();
            BenefitSubscription sub;
            var data = (from v in DbContext.E_BENEFITSSUBSCRIPTION
                     join f in DbContext.F_FISCALYEARS on v.FISCALYEAR equals f.YRange
                     where v.EMPLOYEEID == request.employeeId && v.FISCALYEAR == request.fiscalYear && v.BENEFITID == request.benefitId

                     select new BenefitSubscription
                     {
                         benefitId = v.BENEFITID,
                         fiscalYear = v.FISCALYEAR,
                         employeeId = v.EMPLOYEEID,
                         subscriptionCost = v.SUBSCRIPTIONCOST,
                         subscriptionTitle = v.SUBSCRIPTIONTITLE,
                         YStart=f.YStart,
                         YEnd=f.YEnd
                     });
            if (data.Count() > 0)
            {
                respopnse = data.ToList();
                foreach (BenefitSubscription item in respopnse)
                {
                    item.fiscalYearName = item.YStart.Value.Year.ToString() + '/' + item.YEnd.Value.Year.ToString();
                }
            }
            //x.YStart.Value.Year.ToString()+'/'+ x.YEnd.Value.Year.ToString()
            //var fiscalYear = (from F in DbContext.F_FISCALYEARS
            //                  where F.YRange == data.fiscalYear
            //                  select F).FirstOrDefault();
            //if (fiscalYear.YStart <= DateTime.Now && DateTime.Now <= fiscalYear.YEnd)
            //{
            //    general.isReadOnly = false;
            //}
            //else
            //{
            //    general.isReadOnly = true;
            //}


            return respopnse;
        }
        public CurrentFinancialYear GetCurrentFinancialYear(int Id)
        {

            if (Id == 0)
            {
                return (from f in DbContext.F_FISCALYEARS
                        orderby f.YStart descending
                        select new CurrentFinancialYear
                        {
                            YStart = f.YStart,
                            YEnd = f.YEnd,
                            YRange = f.YRange


                        }).FirstOrDefault();
            }
            else
            {
                return (from f in DbContext.F_FISCALYEARS
                        where f.YRange == Id
                        select new CurrentFinancialYear
                        {
                            YStart = f.YStart,
                            YEnd = f.YEnd,
                            YRange = f.YRange


                        }).FirstOrDefault();


            }

        }

    }
}
