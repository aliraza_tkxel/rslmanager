﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository 
{
   public  class SchemesRepository : BaseRepository<G_SCHEME>
    {
        public SchemesRepository(Entities dbContext) : base(dbContext)
        {

        }

    }
}
