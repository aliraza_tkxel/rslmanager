﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using Utilities;

namespace DataBaseModule.DataRepository.Employees.Absence
{
    public class AbsenceRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public AbsenceRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }

        #region >>> heplers <<<

        #endregion

        public List<AbsenceLeave> GetSicknessAbsenceData(AbsenceRequest request)
        {
            AbsenceLeave absData = new AbsenceLeave();

            var data = (from abs in DbContext.E_AbsenseSickness(request.employeeId, null)
                        select new AbsenceLeave()
                        {
                            endDate = abs.RETURNDATE,
                            nature = abs.Nature,
                            absentDays = abs.DAYS_ABS,
                            reason = abs.Reason,
                            startDate = abs.STARTDATE,
                            status = abs.Status,
                            holidayType = abs.HolType,
                            natureId = abs.NatureId,
                            reasonId = abs.ReasonId,
                            statusId = abs.StatusId,
                            anticipatedReturnDate = abs.AnticipatedReturnDate,
                            absenceHistoryId = abs.AbsenceHistoryId,
                            notes = abs.Notes,
                            unit=abs.DURATION_TYPE
                        }).ToList();
            return data;
        }

        public List<StaffMemberSickness> GetOwnSicknessAbsenceData(int absenceHistoryId)
        {
            var LINEMANAGER = (from JD in DbContext.E_JOBDETAILS
                               join j in DbContext.E_JOURNAL on JD.EMPLOYEEID equals j.EMPLOYEEID
                               join abs in DbContext.E_ABSENCE on j.JOURNALID equals abs.JOURNALID
                               where abs.ABSENCEHISTORYID == absenceHistoryId
                               select JD.LINEMANAGER).FirstOrDefault();
            var leaveStatus = (from ea in DbContext.E_ABSENCE
                               join es in DbContext.E_STATUS on ea.ITEMSTATUSID equals es.ITEMSTATUSID
                               where ea.ABSENCEHISTORYID == absenceHistoryId
                               select es.DESCRIPTION).FirstOrDefault();
            var data = (from staffOfficer in DbContext.E_MyTeamHierarchyForSickness(LINEMANAGER,leaveStatus)
                        where staffOfficer.AbsenceHistoryId == absenceHistoryId
                        select new StaffMemberSickness()
                        {
                            staffMemberFName = staffOfficer.EmployeeFName,
                            staffMemberLName = staffOfficer.EmployeeLName,
                            staffMemberId = staffOfficer.EmployeeId,
                            lineManagerId = staffOfficer.LineManagerId,
                            startDate = staffOfficer.StartDate,
                            statusId = staffOfficer.StatusId,
                            status = staffOfficer.LeaveStatus,
                            reasonId = staffOfficer.ReasonId,
                            absentDays = staffOfficer.Duration,
                            anticipatedReturnDate = staffOfficer.AnticipatedReturnDate,
                            endDate = staffOfficer.ReturnDate,
                            holidayType = staffOfficer.holyType,
                            nature = staffOfficer.NatureDescription,
                            natureId = staffOfficer.ItemNatureId,
                            reason = staffOfficer.Reason,
                            absenceHistoryId = staffOfficer.AbsenceHistoryId,
                            notes = staffOfficer.notes,
                            duration=staffOfficer.Duration,
                            unit=staffOfficer.Unit
                        }).ToList();
            return data;
        }

        public List<StaffMemberSickness> GetStaffMembersAndSicknessStatusSummary(AbsenceRequest request)
        {
            StaffMemberSickness absData = new StaffMemberSickness();

            var data = (from staffOfficer in DbContext.E_MyTeamHierarchyForSickness(request.employeeId,"Absent")
                        select new StaffMemberSickness()
                        {
                            staffMemberFName = staffOfficer.EmployeeFName,
                            staffMemberLName = staffOfficer.EmployeeLName,
                            staffMemberId = staffOfficer.EmployeeId,
                            lineManagerId = staffOfficer.LineManagerId,
                            startDate = staffOfficer.StartDate,
                            statusId = staffOfficer.StatusId,
                            status = staffOfficer.LeaveStatus,
                            reasonId = staffOfficer.ReasonId,
                            absentDays = staffOfficer.Duration,
                            anticipatedReturnDate = staffOfficer.AnticipatedReturnDate,
                            endDate = staffOfficer.ReturnDate,
                            holidayType = staffOfficer.holyType,
                            nature = staffOfficer.NatureDescription,
                            natureId = staffOfficer.ItemNatureId,
                            reason = staffOfficer.Reason,
                            absenceHistoryId = staffOfficer.AbsenceHistoryId,
                            notes = staffOfficer.notes,
                            unit=staffOfficer.Unit
                        }).ToList();
            return data;
        }

        public dynamic RecordSickness(RecordAbsenceRequest request)
        {

            var fYear = getCurrentFiscalYear();
            var startDate = GeneralHelper.GetDateTimeFromString(request.startDate);
            dynamic response = new ExpandoObject();
            //if (DbContext.E_JOURNAL.Where(u => u.EMPLOYEEID == request.employeeId && fYear.YStart<= startDate && startDate <= fYear.YEnd  && u.CURRENTITEMSTATUSID == DbContext.E_ACTION.Where(a=>a.DESCRIPTION=="Approve").Select(a=>a.ITEMACTIONID).FirstOrDefault()).Any())
            //{
            //    return false;
            //}
            DateTime endDate = DateTime.Parse(request.endDate);
            DateTime nowDate = DateTime.Now;

            if (endDate.Date == nowDate.Date)
            {
                string SickLeaveAvailable = checkIfSicknessLeaveAvailable(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()),request.holType);
                if (!SickLeaveAvailable.Equals(""))
                {
                    response.isSuccess = false;
                    response.message = SickLeaveAvailable;
                    return response;
                }
            }
            var data = (from h in DbContext.E_ABSENCE
                        where request.absenceHistoryId == h.ABSENCEHISTORYID
                        select h).FirstOrDefault();

            int? itemId, itemNatureId, itemStatusId = 0;

            var itemIdData = (from n in DbContext.E_NATURE
                              where n.DESCRIPTION == "Sickness"
                              select n).FirstOrDefault();
            itemId = itemIdData.ITEMID;
            itemNatureId = itemIdData.ITEMNATUREID;
            itemStatusId = (from s in DbContext.E_STATUS
                            where s.DESCRIPTION == "Absent"
                            select s).FirstOrDefault().ITEMSTATUSID;


            DatabaseEntities.E_JOURNAL journal = new E_JOURNAL();
            journal.EMPLOYEEID = request.employeeId;
            journal.ITEMID = itemId;
            journal.ITEMNATUREID = itemNatureId;
            if (endDate.Date < nowDate.Date)
            {
                journal.CURRENTITEMSTATUSID = 2;
            }
            else
            {
                journal.CURRENTITEMSTATUSID = itemStatusId;
            }
            journal.CREATIONDATE = DateTime.Now;
            journal.REASONID = request.reasonId;
            DbContext.E_JOURNAL.Add(journal);
            DbContext.SaveChanges();

            int itemActionId = (from a in DbContext.E_ACTION
                                where a.DESCRIPTION == "Apply"
                                select a).FirstOrDefault().ITEMACTIONID;

            DatabaseEntities.E_ABSENCE absence = new E_ABSENCE();
            absence.JOURNALID = journal.JOURNALID;

                absence.DURATION = request.duration;
                if (endDate.Date < nowDate.Date)
                {
                    absence.RETURNDATE = GeneralHelper.GetDateTimeFromString(request.endDate);
                    absence.ITEMSTATUSID = 2;
                    absence.ITEMACTIONID = 2;
                }
                else
                {
                    absence.ITEMSTATUSID = itemStatusId;
                    absence.ITEMACTIONID = itemActionId;
                }
            absence.LASTACTIONDATE = DateTime.Now;
            absence.LASTACTIONUSER = request.recordedBy;
            absence.STARTDATE = GeneralHelper.GetDateTimeFromString(request.startDate);
            if (request.anticipatedReturnDate != null)
            {
                absence.AnticipatedReturnDate = GeneralHelper.GetDateTimeFromString(request.anticipatedReturnDate);
            }

            
            absence.NOTES = request.notes;
            absence.REASONID = request.reasonId;
            absence.HOLTYPE = request.holType;
            absence.REASON = "Sickness";
            absence.DURATION_TYPE = getEmpHolidayIndicatorDescription(request.employeeId);
            DbContext.E_ABSENCE.Add(absence);
            DbContext.SaveChanges();
            response.isSuccess = true;            
            return response;
        }

        public AnnualLeaveResponce GetAnnualLeaveDetail(AbsenceRequest request)
        {
            DateTime? fiscalYear = null;
            if (!string.IsNullOrEmpty(request.fiscalYearDate))
            {
                fiscalYear = GeneralHelper.GetDateTimeFromString(request.fiscalYearDate);
            }
            var data = (from stats in DbContext.E_LeaveStats(request.employeeId, null, true, fiscalYear) select stats).FirstOrDefault();
            AnnualLeaveResponce response = new AnnualLeaveResponce();
            var alstartDate = (from d in DbContext.E_JOBDETAILS
                               where request.employeeId == d.EMPLOYEEID
                               select d).FirstOrDefault().ALSTARTDATE;
            var holidayEntitlementDays = (from e in DbContext.E_JOBDETAILS
                                          where e.EMPLOYEEID == request.employeeId
                                          select e).FirstOrDefault().HOLIDAYENTITLEMENTDAYS;
            if (holidayEntitlementDays == null)
                holidayEntitlementDays = (from e in DbContext.E_JOBDETAILS
                                          where e.EMPLOYEEID == request.employeeId
                                          select e).FirstOrDefault().HOLIDAYENTITLEMENTHOURS / 8;
            var holidayEntitlementhours = (from e in DbContext.E_JOBDETAILS
                                           where e.EMPLOYEEID == request.employeeId
                                           select e).FirstOrDefault().HOLIDAYENTITLEMENTHOURS;            

            var annualLeaveData = (from absence in DbContext.E_AbsenseAnnual(request.employeeId, fiscalYear)
                                   select new AbsenceAnnual()
                                   {
                                       absenceHistoryId = absence.ABSENCEHISTORYID,
                                       endDate = absence.RETURNDATE,
                                       status = absence.Status,
                                       absentDays = (double)absence.DAYS_ABS,
                                       nature = absence.Nature,
                                       startDate = absence.STARTDATE,
                                       holidayType = absence.HolType,
                                       lastActionDatetime = absence.LastActionDate,
                                       unit = data.Unit,
                                       durationType = absence.DurationType

                                   }).ToList();
            var EndDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE_ByYear(request.employeeId,fiscalYear)
                           select new AbsenceAnnual()
                           {
                               endDate = absence.ENDDATE
                           }).FirstOrDefault().endDate;
            double LeavesRequested = 0;
            foreach (var al in annualLeaveData)
            {
                if (al.endDate <= EndDate)
                {
                    if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                    {
                        LeavesRequested = LeavesRequested + double.Parse(al.absentDays.ToString());
                    }
                }
                else if (al.endDate >= EndDate && al.startDate <= EndDate)
                {
                    if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                    {
                        double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                        LeavesRequested = LeavesRequested + LeaveDaysCount;
                    }
                }
            }
            data.LEAVE_AVAILABLE = data.ANNUAL_LEAVE_DAYS - LeavesRequested;

            var query = DbContext.E_GetBankHoliday(request.employeeId, fiscalYear).ToList();
            foreach (var BH in query)
            {
                EmployeeWorkingHours WH = new EmployeeWorkingHours();
                WH = (from wh in DbContext.E_WorkingHours_History
                      where wh.EmployeeId == request.employeeId && BH.STARTDATE >= wh.StartDate && (BH.STARTDATE <= wh.EndDate || wh.EndDate == null)
                      select new EmployeeWorkingHours()
                      {
                          mon = wh.Mon,
                          tue = wh.Tue,
                          wed = wh.Wed,
                          thu = wh.Thu,
                          fri = wh.Fri,
                          sat = wh.Sat,
                          sun = wh.Sun
                      }).FirstOrDefault();
                if (WH != null)
                {
                    switch (BH.STARTDATE.ToString("dddd"))
                    {
                        case "Monday":
                            if (WH.mon > 0 && data.Unit.Equals("hrs"))
                            {
                                BH.DAYS_ABS = WH.mon.Value;
                            }
                            break;
                        case "Tuesday":
                            if (WH.tue > 0 && data.Unit.Equals("hrs"))
                            {
                                BH.DAYS_ABS = WH.tue.Value;
                            }
                            break;
                        case "Wednesday":
                            if (WH.wed > 0 && data.Unit.Equals("hrs"))
                            {
                                BH.DAYS_ABS = WH.wed.Value;
                            }
                            break;
                        case "Thursday":
                            if (WH.thu > 0 && data.Unit.Equals("hrs"))
                            {
                                BH.DAYS_ABS = WH.thu.Value;
                            }
                            break;
                        case "Friday":
                            if (WH.fri > 0 && data.Unit.Equals("hrs"))
                            {
                                BH.DAYS_ABS = WH.fri.Value;
                            }
                            break;
                        case "Saturday":
                            if (WH.sat > 0 && data.Unit.Equals("hrs"))
                            {
                                BH.DAYS_ABS = WH.sat.Value;
                            }
                            break;
                        case "Sunday":
                            if (WH.sun > 0 && data.Unit.Equals("hrs"))
                            {
                                BH.DAYS_ABS = WH.sun.Value;
                            }
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    BH.DAYS_ABS = 0;
                }
            }
            foreach (var item in query)
            {
                annualLeaveData.Add(new AbsenceAnnual()
                {
                    absenceHistoryId = item.ABSENCEHISTORYID,
                    endDate = item.RETURNDATE,
                    status = item.Status,
                    absentDays = item.DAYS_ABS,
                    nature = item.Nature,
                    startDate= item.STARTDATE,
                    holidayType = item.HolType,
                    lastActionDatetime = item.LastActionDate,
                    unit = data.Unit,
                    durationType = item.DurationType,
                });
            }
            response.absentees = annualLeaveData.OrderByDescending(e => e.startDate).ToList();

            if (data != null)
            {
                AnnualLeaveDetail detail = new AnnualLeaveDetail()
                {
                    annualLeaves = Convert.ToDecimal(data.ANNUAL_LEAVE_DAYS),
                    unit = data.Unit,
                    startDate = alstartDate,
                    leavesBooked = Convert.ToDecimal(data.LeaveBooked),
                    leavesRequested = Convert.ToDecimal(data.LeaveRequested),
                    leavesTaken = Convert.ToDecimal(data.LeaveTaken),
                    leavesRemaining = Convert.ToDecimal(data.LEAVE_AVAILABLE + data.CARRY_FWD),
                    totalAllowance = Convert.ToDecimal(data.ANNUAL_LEAVE_DAYS + data.CARRY_FWD + data.BNK_HD),
                    bankHoliday = Convert.ToDecimal(data.BNK_HD),
                    carryForward = Convert.ToDecimal(data.CARRY_FWD),
                    timeOfInLieuOwed = Convert.ToDecimal(data.TIME_OFF_IN_LIEU_OWED),
                    toil = Convert.ToDecimal(data.TOIL),
                    holidayEntitlementDays = holidayEntitlementDays,
                    holidayEntitlementHours = holidayEntitlementhours
                };

                response.leaveDetail = detail;
            }

            var leaveYearDate = getLeaveYearDate(request.employeeId, 2);
            if (leaveYearDate != null)
            {
                response.leaveYearStart = leaveYearDate.leaveYearStart;
                response.leaveYearEnd = leaveYearDate.leaveYearEnd;
            }

            var totalBankHolidays = decimal.Parse(query.Sum(x => x.DAYS_ABS).Value.ToString());
            totalBankHolidays = totalBankHolidays - response.leaveDetail.bankHoliday;
            response.leaveDetail.leavesRemaining = response.leaveDetail.leavesRemaining - totalBankHolidays;
	    
	    response.leaveDetail.leavesRemaining = Math.Round(Decimal.Parse(response.leaveDetail.leavesRemaining.ToString()),2);

            return response;
        }


        public dynamic RecordAnnualLeave(RecordAbsenceRequest request)
        {
            var message = string.Empty;
            dynamic response = new ExpandoObject();
            var isAvailable = checkIfLeaveAvailable(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(request.endDate).ToShortDateString()), request.duration, request.holType);
            if (!isAvailable)
            {
                response.isSuccess = isAvailable;
                response.message = UserMessageConstants.LeaveAlreadyBookedMessage;
                return response;
            }
            // check here for BRS members appointments

            //var isBRSAppointment = checkIfBRSAppointment(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(request.endDate).ToShortDateString()), request.duration, request.holType);
            //if (!isBRSAppointment)
            //{
            //    response.isSuccess = isBRSAppointment;
            //    response.message = UserMessageConstants.LeaveBRSAppointmentBookedMessage;
            //    return response;
            //}

            var data = (from h in DbContext.E_ABSENCE
                        where request.absenceHistoryId == h.ABSENCEHISTORYID
                        select h).FirstOrDefault();

            int? itemId, itemNatureId, itemStatusId = 0;

            var itemIdData = (from n in DbContext.E_NATURE
                              where n.DESCRIPTION == "Annual Leave"
                              select n).FirstOrDefault();
            itemId = itemIdData.ITEMID;
            itemNatureId = itemIdData.ITEMNATUREID;
            itemStatusId = (from s in DbContext.E_STATUS
                            where s.DESCRIPTION == "Pending"
                            select s).FirstOrDefault().ITEMSTATUSID;

            DatabaseEntities.E_JOURNAL journal = new E_JOURNAL();
            journal.EMPLOYEEID = request.employeeId;
            journal.ITEMID = itemId;
            journal.ITEMNATUREID = itemNatureId;
            journal.CURRENTITEMSTATUSID = itemStatusId;
            journal.CREATIONDATE = DateTime.Now;
            journal.REASONID = request.reasonId;
            DbContext.E_JOURNAL.Add(journal);
            DbContext.SaveChanges();

            int itemActionId = (from a in DbContext.E_ACTION
                                where a.DESCRIPTION == "Apply"
                                select a).FirstOrDefault().ITEMACTIONID;

            DatabaseEntities.E_ABSENCE absence = new E_ABSENCE();
            absence.JOURNALID = journal.JOURNALID;
            absence.ITEMSTATUSID = itemStatusId;
            absence.ITEMACTIONID = itemActionId;
            absence.LASTACTIONDATE = DateTime.Now;
            absence.LASTACTIONUSER = request.recordedBy;

            absence.STARTDATE = GeneralHelper.GetDateTimeFromString(request.startDate);
            absence.RETURNDATE = GeneralHelper.GetDateTimeFromString(request.endDate);
            absence.DURATION = request.duration;
            absence.NOTES = request.notes;
            absence.REASONID = request.reasonId;
            absence.HOLTYPE = request.holType;
            absence.REASON = request.reason;
            absence.DURATION_TYPE = getEmpHolidayIndicatorDescription(request.employeeId);

            DbContext.E_ABSENCE.Add(absence);
            DbContext.SaveChanges();

            response.isSuccess = true;
            response.message = UserMessageConstants.AnnualLeaveMarkedSuccessMessage;

            //sendEmailNotification to line manager
            var emailData = (from E in DbContext.E__EMPLOYEE
                             join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                             join LM in DbContext.E__EMPLOYEE on J.LINEMANAGER equals LM.EMPLOYEEID
                             join LMC in DbContext.E_CONTACT on LM.EMPLOYEEID equals LMC.EMPLOYEEID
                             where E.EMPLOYEEID==request.employeeId
                             select new PaypointEmailNotification()
                             {
                                 employeeName = E.FIRSTNAME + " " + E.LASTNAME,
                                 lineManagerName = LM.FIRSTNAME + " " + LM.LASTNAME,
                                 lineManagerEmail = LMC.WORKEMAIL

                             }).FirstOrDefault();


            if (emailData != null && !string.IsNullOrEmpty(emailData.lineManagerEmail))
            {
                string body = string.Empty;
                body = "Dear " + emailData.lineManagerName + ", <br /><br />";
                body = body + "Please log onto BBS and approve or decline an annual leave application from <br/>";
                body = body + emailData.employeeName + "<br/><br/>";
                body = body + "Thanks<br/>";
                body = body + "HR Team";
                EmailHelper.sendHtmlFormattedEmail(emailData.lineManagerName, emailData.lineManagerEmail, "Annual Leave Request", body);
            }

            return response;
        }

        public List<AbsenceLeave> GetLeavesOfAbsence(AbsenceRequest request)
        {
            AbsenceLeave absData = new AbsenceLeave();

            var data = (from abs in DbContext.E_LeaveOfAbsense(request.employeeId, null)
                        select new AbsenceLeave()
                        {
                            endDate = abs.RETURNDATE,
                            nature = abs.Nature,
                            absentDays = abs.DAYS_ABS,
                            startDate = abs.STARTDATE,
                            status = abs.Status,
                            holidayType = abs.HolType,
                            statusId = abs.StatusId,
                            natureId = abs.NatureId,
                            lastActionDatetime = abs.LastActionDate,
                            absenceHistoryId = abs.AbsenceHistoryId,
                            unit = abs.Unit

                        }).ToList();
            return data;
        }
        public List<AbsenceLeave> GetLeavesOfToil(int employeeId)
        {
            try
            {
                var data = (from abs in DbContext.E_ToilLeave(employeeId)
                            select new AbsenceLeave()
                            {
                                endDate = abs.RETURNDATE,
                                nature = abs.Nature,
                                absentDays = abs.DAYS_ABS,
                                startDate = abs.STARTDATE,
                                status = abs.Status,
                                holidayType = abs.HolType,
                                statusId = abs.StatusId,
                                natureId = abs.NatureId,
                                lastActionDatetime = abs.LastActionDate,
                                absenceHistoryId = abs.AbsenceHistoryId,
                                unit = abs.Unit

                            }).ToList();
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public List<MyTeamLeaveOfAbsencePending> GetMyTeamLeavesOfAbsenceRequested(int employeeId)
        {

            var data = (from abs in DbContext.E_MyTeamLeaveOfAbsenseRequested(employeeId)
                        select new MyTeamLeaveOfAbsencePending()
                        {
                            startDate = abs.StartDate,
                            leaveDescription = abs.Nature,
                            //duration = abs.DAYS_ABS,
                            endDate = abs.ReturnDate,
                            status = abs.Status,
                            holType = abs.HolType,
                            statusId = this.getAbsenceStatusIdByDescription(abs.Status),
                            applicantName = abs.FirstName + ' ' + abs.LastName,
                            applicantId = abs.EmpId,
                            absenceHistoryId = abs.AbsenceHistoryId,
                            _duration = abs.Duration,
                            duration = abs.Duration,
                            natureId = abs.ItemNatureId,
                            unit = abs.Unit
                        }).ToList();
            return data;
        }
        public List<MyTeamLeaveOfToilPending> GetMyTeamLeavesOfToilRequested(int employeeId)
        {

            var data = (from abs in DbContext.E_MyTeamLeaveOfToilRequested(employeeId)
                        select new MyTeamLeaveOfToilPending()
                        {
                            startDate = abs.StartDate,
                            leaveDescription = abs.Nature,
                            endDate = abs.ReturnDate,
                            status = abs.Status,
                            holType = abs.HolType,
                            statusId = this.getAbsenceStatusIdByDescription(abs.Status),
                            applicantName = abs.FirstName + ' ' + abs.LastName,
                            applicantId = abs.EmpId,
                            absenceHistoryId = abs.AbsenceHistoryId,
                            _duration = abs.Duration,
                            duration = abs.Duration,
                            natureId = abs.ItemNatureId,
                            unit = abs.Unit
                        }).ToList();
            return data;
        }

        public List<MyTeamLeaveOfAbsencePending> GetMyLeavesOfAbsenceRequested(int employeeId)
        {

            var data = (from abs in DbContext.E_MyLeaveOfAbsenseRequested(employeeId)
                        select new MyTeamLeaveOfAbsencePending()
                        {
                            startDate = abs.StartDate,
                            leaveDescription = abs.Nature,
                            endDate = abs.ReturnDate,
                            status = abs.Status,
                            holType = abs.HolType,
                            statusId = this.getAbsenceStatusIdByDescription(abs.Status),
                            applicantName = abs.FirstName + ' ' + abs.LastName,
                            applicantId = abs.EmpId,
                            absenceHistoryId = abs.AbsenceHistoryId,
                            _duration = abs.Duration,
                            natureId = abs.ItemNatureId
                        }).ToList();
            return data;
        }

        public List<MyTeamLeavesPending> GetMyTeamLeavesRequested(int employeeId)
        {
            var data = (from leaves in DbContext.E_MyTeamLeavesRequested(employeeId)
                        select new MyTeamLeavesPending()
                        {
                            duration = leaves.Duration,
                            endDate = leaves.ReturnDate,
                            leaveDescription = leaves.NatureDescription,
                            natureId = leaves.ItemNatureId,
                            startDate = leaves.StartDate,
                            unit = leaves.Unit,
                            status = leaves.LeaveStatus,
                            statusId = this.getAbsenceStatusIdByDescription(leaves.LeaveStatus),
                            applicantName = leaves.FirstName + ' ' + leaves.LastName,
                            applicantId = leaves.EmpId,
                            absenceHistoryId = leaves.AbsenceHistoryId,
                            holType = leaves.HolType
                        }).ToList();

            var myStaffHierarchy = DbContext.FN_GetTeamHierarchyForEmployeeSickness(employeeId).Where(e => e.LineManagerId != employeeId).Select(e => e.LineManagerId).Distinct().ToList();
            var currentDate = GeneralHelper.GetDateTimeFromString(DateTime.Now.ToShortDateString());
            if (myStaffHierarchy != null && myStaffHierarchy.Count > 0)
            {
                foreach (var myStaff in myStaffHierarchy)
                {
                    // check if line manager is on leave 
                    var isEmployeeApplyLeave = DbContext.E_AnnualLeaveCount(myStaff.Value);
                    if (isEmployeeApplyLeave.Any())
                    {
                        var myStaffData = (from leaves in DbContext.E_MyTeamLeavesRequested(myStaff.Value)
                                           select new MyTeamLeavesPending()
                                           {
                                               duration = leaves.Duration,
                                               endDate = leaves.ReturnDate,
                                               leaveDescription = leaves.NatureDescription,
                                               natureId = leaves.ItemNatureId,
                                               startDate = leaves.StartDate,
                                               unit = leaves.Unit,
                                               status = leaves.LeaveStatus,
                                               statusId = this.getAbsenceStatusIdByDescription(leaves.LeaveStatus),
                                               applicantName = leaves.FirstName + ' ' + leaves.LastName,
                                               applicantId = leaves.EmpId,
                                               absenceHistoryId = leaves.AbsenceHistoryId,
                                               holType = leaves.HolType
                                           }).ToList();

                        foreach (var item in myStaffData)
                        {
                            data.Add(item);
                        }
                    }
                }
            }

            return data;
        }

        public List<MyTeamLeavesPending> GetMyTeamLeavesRequestedWeb(int employeeId)
        {
            var data = (from leaves in DbContext.E_MyTeamLeavesRequested(employeeId)
                        select new MyTeamLeavesPending()
                        {
                            duration = leaves.Duration,
                            endDate = leaves.ReturnDate,
                            leaveDescription = leaves.NatureDescription,
                            natureId = leaves.ItemNatureId,
                            startDate = leaves.StartDate,
                            unit = leaves.Unit,
                            status = leaves.LeaveStatus,
                            statusId = this.getAbsenceStatusIdByDescription(leaves.LeaveStatus),
                            applicantName = leaves.FirstName + ' ' + leaves.LastName,
                            applicantId = leaves.EmpId,
                            absenceHistoryId = leaves.AbsenceHistoryId,
                            holType = leaves.HolType
                        }).ToList();
            return data;
        }

        public List<AbsenceDetail> GetAbsenceDetail(int absenceHistoryId)
        {
            var data = (from leaves in DbContext.E_GetAbsenceDetailByAbsenceHistoryId(absenceHistoryId)
                        select new AbsenceDetail()
                        {
                            duration = leaves.Duration,
                            endDate = leaves.ReturnDate,
                            leaveDescription = leaves.NatureDescription,
                            natureId = leaves.ItemNatureId,
                            startDate = leaves.StartDate,
                            unit = leaves.Unit,
                            status = leaves.LeaveStatus,
                            statusId = this.getAbsenceStatusIdByDescription(leaves.LeaveStatus),
                            applicantName = leaves.FirstName + ' ' + leaves.LastName,
                            applicantId = leaves.EmpId,
                            absenceHistoryId = leaves.AbsenceHistoryId,
                            holType = leaves.HolType,
                            title = leaves.Title,
                            _notes = leaves.SubmitNote,
                            notes = leaves.Notes,
                            _reason = leaves.Reason
                        }).ToList();
            return data;
        }

        public List<MyTeamLeavesPending> GetMyLeavesRequested(int employeeId)
        {
            var data = (from leaves in DbContext.E_MyLeavesRequested(employeeId)
                        select new MyTeamLeavesPending()
                        {
                            duration = leaves.Duration,
                            endDate = leaves.ReturnDate,
                            leaveDescription = leaves.NatureDescription,
                            natureId = leaves.ItemNatureId,
                            startDate = leaves.StartDate,
                            unit = leaves.Unit,
                            status = leaves.LeaveStatus,
                            statusId = this.getAbsenceStatusIdByDescription(leaves.LeaveStatus),
                            applicantName = leaves.FirstName + ' ' + leaves.LastName,
                            applicantId = leaves.EmpId,
                            absenceHistoryId = leaves.AbsenceHistoryId,
                            holType = leaves.HolType
                        }).ToList();
            return data;
        }

        public List<MyTeamLeavesPending> GetMyTeamBirthdaysPersonalDaysRequested(int employeeId, string leaveType)
        {
            var data = (from leaves in DbContext.E_MyTeamBirthdayPersonalLeavesRequested(employeeId, leaveType)
                        select new MyTeamLeavesPending()
                        {
                            duration = leaves.Duration,
                            endDate = leaves.ReturnDate,
                            leaveDescription = leaves.NatureDescription,
                            natureId = leaves.ItemNatureId,
                            startDate = leaves.StartDate,
                            unit = leaves.Unit,
                            status = leaves.LeaveStatus,
                            statusId = this.getAbsenceStatusIdByDescription(leaves.LeaveStatus),
                            applicantName = leaves.FirstName + ' ' + leaves.LastName,
                            applicantId = leaves.EmpId,
                            absenceHistoryId = leaves.AbsenceHistoryId,
                            holType = leaves.HolType
                        }).ToList();
            return data;
        }

        public dynamic RecordLeaveOfAbsence(RecordAbsenceRequest request)
        {
            var message = string.Empty;
            dynamic response = new ExpandoObject();
            var leaveNature = GetNatureDescription((int)request.natureId);

            if (leaveNature == "Personal Day" || leaveNature == "Birthday Leave")
            {
                var isLeaveAvailabe = CheckPersonalBirthDayAvailed(request.employeeId, leaveNature, GeneralHelper.GetDateTimeFromString(request.startDate));
                if (!isLeaveAvailabe)
                {
                    response.isSuccess = isLeaveAvailabe;
                    if (leaveNature == "Personal Day")
                    {
                        response.message = UserMessageConstants.PersonalDayAlreadyAvailed;
                    }
                    if (leaveNature == "Birthday Leave")
                    {
                        response.message = UserMessageConstants.BirthdayAlreadyAvailed;
                    }
                    return response;
                }
            }

            var isAvailable = checkIfLeaveAvailable(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(request.endDate).ToShortDateString()), request.duration, request.holType);
            if (!isAvailable)
            {
                response.isSuccess = isAvailable;
                response.message = UserMessageConstants.LeaveAlreadyBookedMessage;
                return response;
            }
            //var isBRSAppointment = checkIfBRSAppointment(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(request.endDate).ToShortDateString()), request.duration, request.holType);
            //if (!isBRSAppointment)
            //{
            //    response.isSuccess = isBRSAppointment;
            //    response.message = UserMessageConstants.LeaveBRSAppointmentBookedMessage;
            //    return response;
            //}
            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    var data = (from h in DbContext.E_ABSENCE
                                where request.absenceHistoryId == h.ABSENCEHISTORYID
                                select h).FirstOrDefault();

                    int? itemId, itemNatureId, itemStatusId = 0;

                    var itemIdData = (from n in DbContext.E_NATURE
                                      where n.ITEMNATUREID == request.natureId
                                      select n).FirstOrDefault();
                    itemId = itemIdData.ITEMID;
                    itemNatureId = itemIdData.ITEMNATUREID;
                    itemStatusId = (from s in DbContext.E_STATUS
                                    where s.DESCRIPTION == "Pending"
                                    select s).FirstOrDefault().ITEMSTATUSID;


                    DatabaseEntities.E_JOURNAL journal = new E_JOURNAL();
                    journal.EMPLOYEEID = request.employeeId;
                    journal.ITEMID = itemId;
                    journal.ITEMNATUREID = itemNatureId;
                    journal.CURRENTITEMSTATUSID = itemStatusId;
                    journal.CREATIONDATE = DateTime.Now;
                    //journal.REASONID = request.reasonId;
                    //journal.TITLE = request.notes;
                    DbContext.E_JOURNAL.Add(journal);
                    DbContext.SaveChanges();

                    int itemActionId = (from a in DbContext.E_ACTION
                                        where a.DESCRIPTION == "Apply"
                                        select a).FirstOrDefault().ITEMACTIONID;

                    if (itemIdData.DESCRIPTION == "Birthday Leave")
                    {
                        message = UserMessageConstants.BirthdayRecordedMessage;
                    }
                    else if (itemIdData.DESCRIPTION == "Personal Day")
                    {
                        message = UserMessageConstants.PersonaldayRecordedMessage;
                    }
                    else
                    {
                        message = UserMessageConstants.LeaveMarkedSuccessMessage;
                    }

                    DatabaseEntities.E_ABSENCE absence = new E_ABSENCE();
                    absence.JOURNALID = journal.JOURNALID;
                    absence.ITEMSTATUSID = itemStatusId;
                    absence.ITEMACTIONID = itemActionId;
                    absence.LASTACTIONDATE = DateTime.Now;
                    absence.LASTACTIONUSER = request.recordedBy;
                    absence.STARTDATE = GeneralHelper.GetDateTimeFromString(request.startDate);
                    absence.DURATION = request.duration;
                    absence.NOTES = request.notes;
                    absence.REASONID = request.reasonId;
                    absence.HOLTYPE = request.holType;
                    absence.REASON = request.reason;
                    absence.RETURNDATE = GeneralHelper.GetDateTimeFromString(request.endDate);
                    absence.DURATION_TYPE = getEmpHolidayIndicatorDescription(request.employeeId);

                    DbContext.E_ABSENCE.Add(absence);
                    DbContext.SaveChanges();
                    /*code*/
                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }
            }

            response.isSuccess = true;
            response.message = message;
            return response;
        }

        public dynamic RecordSickAnnualLeave(RecordAbsenceRequest request)
        {
            var message = string.Empty;
            var leaveNature = GetNatureDescription((int)request.natureId);
            dynamic response = new ExpandoObject();

            if (leaveNature == "Personal Day" || leaveNature == "Birthday Leave")
            {
                var isLeaveAvailabe = CheckPersonalBirthDayAvailed(request.employeeId, leaveNature, GeneralHelper.GetDateTimeFromString(request.startDate));
                if (!isLeaveAvailabe)
                {
                    response.isSuccess = isLeaveAvailabe;
                    if (leaveNature == "Personal Day")
                    {
                        response.message = UserMessageConstants.PersonalDayAlreadyAvailed;
                    }
                    if (leaveNature == "Birthday Leave")
                    {
                        response.message = UserMessageConstants.BirthdayAlreadyAvailed;
                    }
                    return response;
                }
            }

            string SickLeaveAvailable = "";
            var isAvailable = checkIfLeaveAvailable(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(request.endDate).ToShortDateString()), request.duration, request.holType);
            if (!(DateTime.Parse(request.endDate) < DateTime.Now))
            {
                SickLeaveAvailable = checkIfSicknessLeaveAvailable(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()),request.holType);
            }
            if (leaveNature != "Sickness")
            {
                if (!isAvailable)
                {
                    response.isSuccess = isAvailable;
                    response.message = message;
                    return response;
                }
            }
            else
            {

                if (!SickLeaveAvailable.Equals(""))
                {
                    response.isSuccess = false;
                    response.message = SickLeaveAvailable;
                    return response;
                }
            }
            if (leaveNature == "Annual Leave")
            {
                //var isBRSAppointment = checkIfBRSAppointment(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(request.endDate).ToShortDateString()), request.duration, request.holType);
                //if (!isBRSAppointment)
                //{
                //    response.isSuccess = isBRSAppointment;
                //    response.message = UserMessageConstants.LeaveBRSAppointmentBookedMessage;
                //    return response;
                //}
            }

            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    var data = (from h in DbContext.E_ABSENCE
                                where request.absenceHistoryId == h.ABSENCEHISTORYID
                                select h).FirstOrDefault();

                    int? itemId, itemNatureId, itemStatusId = 0;

                    var itemIdData = (from n in DbContext.E_NATURE
                                      where n.ITEMNATUREID == request.natureId
                                      select n).FirstOrDefault();
                    if (itemIdData.DESCRIPTION == "Sickness")
                    {
                        itemStatusId = (from s in DbContext.E_STATUS
                                        where s.DESCRIPTION == "Absent"
                                        select s).FirstOrDefault().ITEMSTATUSID;
                    }
                    else
                    {
                        itemStatusId = (from s in DbContext.E_STATUS
                                        where s.DESCRIPTION == "Pending"
                                        select s).FirstOrDefault().ITEMSTATUSID;
                    }
                    itemId = itemIdData.ITEMID;
                    itemNatureId = itemIdData.ITEMNATUREID;
                    DatabaseEntities.E_JOURNAL journal = new E_JOURNAL();
                    journal.EMPLOYEEID = request.employeeId;
                    journal.ITEMID = itemId;
                    journal.ITEMNATUREID = itemNatureId;
                    journal.CURRENTITEMSTATUSID = itemStatusId;
                    journal.CREATIONDATE = DateTime.Now;
                    if (itemIdData.DESCRIPTION == "Sickness")
                    {
                        journal.REASONID = request.reasonId;
                        message = UserMessageConstants.SicknessRecordedMessage;
                    } else if (itemIdData.DESCRIPTION == "Birthday Leave")
                    {
                        message = UserMessageConstants.BirthdayRecordedMessage;
                    } else if (itemIdData.DESCRIPTION == "Personal Day")
                    {
                        message = UserMessageConstants.PersonaldayRecordedMessage;
                    } else
                    {
                        message = UserMessageConstants.LeaveMarkedSuccessMessage;
                    }

                    //journal.TITLE = request.notes;
                    DbContext.E_JOURNAL.Add(journal);
                    DbContext.SaveChanges();

                    int itemActionId = (from a in DbContext.E_ACTION
                                        where a.DESCRIPTION == "Apply"
                                        select a).FirstOrDefault().ITEMACTIONID;

                    DatabaseEntities.E_ABSENCE absence = new E_ABSENCE();
                    absence.JOURNALID = journal.JOURNALID;
                    absence.ITEMSTATUSID = itemStatusId;
                    absence.ITEMACTIONID = itemActionId;
                    absence.LASTACTIONDATE = DateTime.Now;
                    absence.LASTACTIONUSER = request.recordedBy;
                    absence.STARTDATE = GeneralHelper.GetDateTimeFromString(request.startDate);
                    if (request.anticipatedReturnDate != null)
                    {
                        absence.AnticipatedReturnDate = GeneralHelper.GetDateTimeFromString(request.anticipatedReturnDate);
                    }
                    absence.DURATION = request.duration;
                    absence.NOTES = request.notes;
                    absence.REASONID = request.reasonId;
                    absence.HOLTYPE = request.holType;
                    absence.REASON = request.reason;
                    if (itemIdData.DESCRIPTION != "Sickness")
                    {
                        absence.RETURNDATE = GeneralHelper.GetDateTimeFromString(request.endDate);
                    }
                    absence.DURATION_TYPE = getEmpHolidayIndicatorDescription(request.employeeId);

                    DbContext.E_ABSENCE.Add(absence);
                    DbContext.SaveChanges();
                    /*code*/
                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();
                    if (leaveNature == "Annual Leave")
                    {
                        //sendEmailNotification to line manager
                        var emailData = (from E in DbContext.E__EMPLOYEE
                                         join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                                         join LM in DbContext.E__EMPLOYEE on J.LINEMANAGER equals LM.EMPLOYEEID
                                         join LMC in DbContext.E_CONTACT on LM.EMPLOYEEID equals LMC.EMPLOYEEID
                                         where E.EMPLOYEEID == request.employeeId
                                         select new PaypointEmailNotification()
                                         {
                                             employeeName = E.FIRSTNAME + " " + E.LASTNAME,
                                             lineManagerName = LM.FIRSTNAME + " " + LM.LASTNAME,
                                             lineManagerEmail = LMC.WORKEMAIL

                                         }).FirstOrDefault();


                        if (emailData != null && !string.IsNullOrEmpty(emailData.lineManagerEmail))
                        {
                            string body = string.Empty;
                            body = "Dear " + emailData.lineManagerName + ", <br /><br />";
                            body = body + "Please log onto BBS and approve or decline an annual leave application from <br/>";
                            body = body + emailData.employeeName + "<br/><br/>";
                            body = body + "Thanks<br/>";
                            body = body + "HR Team";
                            EmailHelper.sendHtmlFormattedEmail(emailData.lineManagerName, emailData.lineManagerEmail, "Annual Leave Request", body);
                        }
                    }
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }
            }
            response.isSuccess = true;
            response.message = message;
            return response;
        }
        public string GetNatureDescription(int NatureId)
        {
            var itemIdData = (from n in DbContext.E_NATURE
                              where n.ITEMNATUREID == NatureId
                              select n).FirstOrDefault();

            return itemIdData.DESCRIPTION;
        }

        public string GetActionDescription(int ActionId)
        {
            var itemIdData = (from n in DbContext.E_ACTION
                              where n.ITEMACTIONID == ActionId
                              select n).FirstOrDefault();

            return itemIdData.DESCRIPTION;
        }

        public List<AbsenceLeave> GetMaternityLeave(int employeeId)
        {
            AbsenceLeave absData = new AbsenceLeave();

            var data = (from abs in DbContext.E_MaternityLeaves(employeeId)
                        select new AbsenceLeave()
                        {
                            nature = abs.Nature,
                            absentDays = abs.DAYS_ABS,
                            startDate = abs.STARTDATE,
                            status = abs.Status,
                            holidayType = abs.HolType
                        }).ToList();
            return data;
        }
        public List<LeaveTaken> GetLeavesTaken(AbsenceRequest request)
        {
            DateTime? fiscalYear = null;
            if (!string.IsNullOrEmpty(request.fiscalYearDate))
            {
                fiscalYear = GeneralHelper.GetDateTimeFromString(request.fiscalYearDate);
            }
            var data = (from leaves in DbContext.E_LeavesTaken(request.employeeId, fiscalYear)
                        select new LeaveTaken()
                        {
                            duration = leaves.duration,
                            endDate = leaves.RETURNDATE,
                            leaveDescription = leaves.DESCRIPTION,
                            natureId = leaves.ITEMNATUREID,
                            startDate = leaves.STARTDATE,
                            unit = leaves.unit,
                            status = leaves.Status,
                            statusId = this.getAbsenceStatusIdByDescription(leaves.Status),
                            absenceHistoryId = leaves.AbsenceHistoryId
                        }).ToList();
           
            var query = DbContext.E_GetBankHoliday(request.employeeId, fiscalYear ?? DateTime.Now.Date).ToList();
            foreach (var item in query)
            {
                data.Add(new LeaveTaken()
                {
                    duration = (decimal)item.DAYS_ABS,
                    endDate = item.RETURNDATE,
                    leaveDescription = "Bank Holiday",
                    natureId = 0,
                    startDate = item.STARTDATE,
                    unit = item.DurationType,
                    status = item.Status,
                    statusId = 0,
                    absenceHistoryId = 0
                });
            }
            data = data.OrderByDescending(e => e.startDate).ToList();

            return data;
        }
        public List<LeaveTaken> GetLeavesRequested(AbsenceRequest request)
        {
            DateTime? fiscalYear = null;
            if (!string.IsNullOrEmpty(request.fiscalYearDate))
            {
                fiscalYear = GeneralHelper.GetDateTimeFromString(request.fiscalYearDate);
            }

            var data = (from leaves in DbContext.E_LeavesRequested(request.employeeId, fiscalYear)
                        select new LeaveTaken()
                        {
                            duration = leaves.duration,
                            endDate = leaves.RETURNDATE,
                            leaveDescription = leaves.DESCRIPTION,
                            natureId = leaves.ITEMNATUREID,
                            startDate = leaves.STARTDATE,
                            unit = leaves.unit,
                            status = ApplicationConstants.absenceStatusPending,
                            statusId = this.getAbsenceStatusIdByDescription(ApplicationConstants.absenceStatusPending)
                            , absenceHistoryId = leaves.AbsenceHistoryId
                        }).ToList();
            return data;
        }
        public List<LeaveTaken> GetBirthdaysPersonalDaysRequested(AbsenceRequest request, string leaveType)
        {
            var data = (from leaves in DbContext.E_BirthdayPersonalLeavesRequested(request.employeeId, leaveType, null)
                        select new LeaveTaken()
                        {
                            duration = leaves.duration,
                            endDate = leaves.RETURNDATE,
                            leaveDescription = leaves.DESCRIPTION,
                            natureId = leaves.ITEMNATUREID,
                            startDate = leaves.STARTDATE,
                            unit = leaves.unit,
                            status = ApplicationConstants.absenceStatusPending,
                            statusId = this.getAbsenceStatusIdByDescription(ApplicationConstants.absenceStatusPending)
                            , absenceHistoryId = leaves.AbsenceHistoryId
                        }).ToList();
            return data;
        }
        public string ChangeLeaveStatus(LeaveStatusChangeRequest leaveStatusChangeRequest)
        {
            //Get E_ABSENCE DATA WITH ITS JOURNAL ENTRY
            var absenceData = DbContext.E_ABSENCE.Include("E_JOURNAL").Where(x => x.ABSENCEHISTORYID == leaveStatusChangeRequest.absenceHistoryId).FirstOrDefault();

            if (absenceData == null)
            {
                return ApplicationConstants.absenceError;
            }
            if (leaveStatusChangeRequest.actionId == 5)
            {
                var isBRSAppointment = checkIfBRSAppointment(int.Parse(absenceData.E_JOURNAL.EMPLOYEEID.ToString()), Convert.ToDateTime(Convert.ToDateTime(absenceData.STARTDATE).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(absenceData.RETURNDATE).ToShortDateString()), float.Parse(absenceData.DURATION.ToString()), absenceData.HOLTYPE);
                if (!isBRSAppointment)
                {
                    return UserMessageConstants.RescheduleBRSAppointmentMessage;
                }
            }
            //Check Existing Leave
            var startDate = absenceData.STARTDATE == null ? DateTime.Now : (DateTime)absenceData.STARTDATE;
            var returnDate = absenceData.RETURNDATE == null ? DateTime.Now : (DateTime)absenceData.RETURNDATE;

            var result = DbContext.Database.SqlQuery<double>("select dbo.E_CHECK_EXISTINGLEAVE_UPDATE({0},{1},{2},{3},{4}, {5})", absenceData.E_JOURNAL.EMPLOYEEID, startDate.ToString("yyyy-dd-MM"), returnDate.ToString("yyyy-dd-MM"), absenceData.DURATION, absenceData.HOLTYPE, absenceData.JOURNALID).FirstOrDefault();

            if (DbContext.E_ACTION.Where(x => x.DESCRIPTION == ApplicationConstants.leaveActionCancel || x.DESCRIPTION == ApplicationConstants.leaveActionDecline).Select(y => y.ITEMACTIONID).ToList().Contains(leaveStatusChangeRequest.actionId))
            {
                result = 0;
            }

            //Get Hours for the duration
            double? leave_H = 0.0;
            if (result == 0)
            {
                leave_H = DbContext.E_BOOK_ANNUAL_LEAVE_HRS(absenceData.E_JOURNAL.EMPLOYEEID, absenceData.STARTDATE, absenceData.RETURNDATE, absenceData.DURATION.ToString()).FirstOrDefault();
                leave_H = leave_H == null ? 0.0 : leave_H;
            }

            //Add new Absence entry for Status Change
            var newAbsenceRecord = new E_ABSENCE();
            newAbsenceRecord.JOURNALID = absenceData.JOURNALID;
            newAbsenceRecord.CERTNO = leaveStatusChangeRequest.certNo;
            newAbsenceRecord.DRNAME = leaveStatusChangeRequest.drName;
            newAbsenceRecord.DURATION = absenceData.DURATION;
            if (absenceData.DURATION_HRS != null && absenceData.DURATION_HRS > 0)
            {
                newAbsenceRecord.DURATION_HRS = absenceData.DURATION_HRS;
            }
            else
            {
                newAbsenceRecord.DURATION_HRS = leave_H;
            }
            newAbsenceRecord.HOLTYPE = absenceData.HOLTYPE;
            newAbsenceRecord.ITEMACTIONID = leaveStatusChangeRequest.actionId;
            newAbsenceRecord.ITEMSTATUSID = leaveStatusChangeRequest.actionId;
            newAbsenceRecord.LASTACTIONDATE = DateTime.Now;
            newAbsenceRecord.LASTACTIONUSER = leaveStatusChangeRequest.actionBy;
            newAbsenceRecord.NOTES = leaveStatusChangeRequest.notes;
            newAbsenceRecord.REASON = absenceData.REASON;
            newAbsenceRecord.REASONID = absenceData.REASONID;
            newAbsenceRecord.RETURNDATE = absenceData.RETURNDATE;
            newAbsenceRecord.STARTDATE = absenceData.STARTDATE;
            if (absenceData.E_JOURNAL.EMPLOYEEID.HasValue)
            {
                newAbsenceRecord.DURATION_TYPE = getEmpHolidayIndicatorDescription(absenceData.E_JOURNAL.EMPLOYEEID.Value);
            }

            DbContext.E_ABSENCE.Add(newAbsenceRecord);

            //Change status in Journal of the Leave
            var journal = DbContext.E_JOURNAL.Where(j => j.JOURNALID == (int)absenceData.JOURNALID).FirstOrDefault();
            journal.CURRENTITEMSTATUSID = leaveStatusChangeRequest.actionId;

            var dataResult =
            (from abs in DbContext.E_ABSENCE
             join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
             where abs.ABSENCEHISTORYID == leaveStatusChangeRequest.absenceHistoryId
             select j).FirstOrDefault();

            if (GetNatureDescription((int)dataResult.ITEMNATUREID) == "Personal Day" && GetActionDescription(leaveStatusChangeRequest.actionId) == "Approve")
            {
                DeclinePersonalLeaves(leaveStatusChangeRequest, leave_H, (int)dataResult.EMPLOYEEID, (int)dataResult.JOURNALID);
            }

            // Send E-mail to Employee
            int actionBy = leaveStatusChangeRequest.actionBy;
            int actionId = leaveStatusChangeRequest.actionId;
            int? LineManagerId= (from JD in DbContext.E_JOBDETAILS
                                 where JD.EMPLOYEEID == (int)dataResult.EMPLOYEEID
                                 select JD.LINEMANAGER).FirstOrDefault();

            if(LineManagerId!=null && LineManagerId==actionBy)
            {
                if (actionId == 4 || actionId == 5)
                {
                    string action = actionId == 4 ? "Declined": "Approved";
                    ////sendEmailNotification to employee

                    var emailData = (from E in DbContext.E__EMPLOYEE
                                     join EC in DbContext.E_CONTACT on E.EMPLOYEEID equals EC.EMPLOYEEID
                                     join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                                     join LM in DbContext.E__EMPLOYEE on J.LINEMANAGER equals LM.EMPLOYEEID
                                     where E.EMPLOYEEID == (int)dataResult.EMPLOYEEID
                                     select new CancelLeaveEmailNotification()
                                     {                                         
                                         employeeId = E.EMPLOYEEID,
                                         employeeName = E.FIRSTNAME + " " + E.LASTNAME,
                                         employeeEmail = EC.WORKEMAIL,
                                         lineManagerName=LM.FIRSTNAME + " " + LM.LASTNAME

                                     }).FirstOrDefault();


                    if (emailData != null && !string.IsNullOrEmpty(emailData.employeeEmail))
                    {
                        string body = string.Empty;
                        body = "Dear " + emailData.employeeName + ", <br /><br />";
                        body = body + "Your request for annual leave on the " + absenceData.STARTDATE.Value.Date.ToShortDateString()+"-" + absenceData.RETURNDATE.Value.Date.ToShortDateString()+" ";
                        body = body + "has been " + action + " by " + emailData.lineManagerName+ ".<br /><br />";
                        body = body + "You are able to amend or cancel up to and including the start date of the leave request using the My Broadland App or the My Job module.  <br/><br/>";
                        body = body + "Thanks<br/>";
                        body = body + "HR Team";
                        EmailHelper.sendHtmlFormattedEmail(emailData.employeeName, emailData.employeeEmail, "Annual Leave Request", body);
                    }
                }
            }

            return getAbsenceStatusDescriptionById(leaveStatusChangeRequest.actionId);
        }


        public void DeclinePersonalLeaves(LeaveStatusChangeRequest leaveStatusChangeRequest, double? leave_H, int empID, int journalId)
        {



            var declineLeaves = DbContext.E_JOURNAL.Include("E_ABSENCE").Where(x => x.EMPLOYEEID == empID && x.JOURNALID != journalId && x.ITEMNATUREID == (DbContext.E_NATURE.Where(a => a.DESCRIPTION == "Personal Day").Select(a => a.ITEMNATUREID).FirstOrDefault()) && x.CURRENTITEMSTATUSID != (DbContext.E_ACTION.Where(a => a.DESCRIPTION == "Decline").Select(a => a.ITEMACTIONID).FirstOrDefault())).ToList();
            var fYear = getCurrentFiscalYear();
            // var absentslst=declineLeaves.E_ABSENCE.Where(a => a.STARTDATE >= fYear.YStart && a.STARTDATE <= fYear.YEnd).ToList();

            //Add new Absence entry for Status Change
            foreach (E_JOURNAL jnl in declineLeaves)
            {

                foreach (E_ABSENCE absenceData in jnl.E_ABSENCE.ToList()) {
                    if (fYear.YStart <= absenceData.STARTDATE && absenceData.STARTDATE <= fYear.YEnd) {
                        var newAbsenceRecord = new E_ABSENCE();
                        newAbsenceRecord.JOURNALID = absenceData.JOURNALID;
                        newAbsenceRecord.CERTNO = leaveStatusChangeRequest.certNo;
                        newAbsenceRecord.DRNAME = leaveStatusChangeRequest.drName;
                        newAbsenceRecord.DURATION = absenceData.DURATION;
                        newAbsenceRecord.DURATION_HRS = leave_H;
                        newAbsenceRecord.HOLTYPE = absenceData.HOLTYPE;
                        newAbsenceRecord.ITEMACTIONID = DbContext.E_ACTION.Where(j => j.DESCRIPTION == "Decline").Select(j => j.ITEMACTIONID).FirstOrDefault();
                        newAbsenceRecord.ITEMSTATUSID = DbContext.E_ACTION.Where(j => j.DESCRIPTION == "Decline").Select(j => j.ITEMACTIONID).FirstOrDefault();
                        newAbsenceRecord.LASTACTIONDATE = DateTime.Now;
                        newAbsenceRecord.LASTACTIONUSER = leaveStatusChangeRequest.actionBy;
                        newAbsenceRecord.NOTES = leaveStatusChangeRequest.notes;
                        newAbsenceRecord.REASON = absenceData.REASON;
                        newAbsenceRecord.REASONID = absenceData.REASONID;
                        newAbsenceRecord.RETURNDATE = absenceData.RETURNDATE;
                        newAbsenceRecord.STARTDATE = absenceData.STARTDATE;
                        if (absenceData.E_JOURNAL.EMPLOYEEID.HasValue)
                        {
                            newAbsenceRecord.DURATION_TYPE = getEmpHolidayIndicatorDescription(absenceData.E_JOURNAL.EMPLOYEEID.Value);
                        }

                        DbContext.E_ABSENCE.Add(newAbsenceRecord);

                        //Change status in Journal of the Leave
                        var journal = DbContext.E_JOURNAL.Where(j => j.JOURNALID == (int)absenceData.JOURNALID).FirstOrDefault();
                        journal.CURRENTITEMSTATUSID = DbContext.E_ACTION.Where(j => j.DESCRIPTION == "Decline").Select(j => j.ITEMACTIONID).FirstOrDefault();
                    }
                }
            }





        }

        public string ChangeSicknessLeaveStatus(LeaveSicknessStatusChangeRequest leaveStatusChangeRequest)
        {
            //Get E_ABSENCE DATA WITH ITS JOURNAL ENTRY
            var absenceData = DbContext.E_ABSENCE.Include("E_JOURNAL").Where(x => x.ABSENCEHISTORYID == leaveStatusChangeRequest.absenceHistoryId).FirstOrDefault();
            leaveStatusChangeRequest.actionId = DbContext.E_ACTION.Where(x => x.DESCRIPTION == ApplicationConstants.lastDayOfAbsence).Select(x => x.ITEMACTIONID).FirstOrDefault();
            if (absenceData == null)
            {
                return ApplicationConstants.absenceError;
            }
            if (leaveStatusChangeRequest.actionId == 2)
            {
                var isBRSAppointment = checkIfBRSAppointment(int.Parse(absenceData.E_JOURNAL.EMPLOYEEID.ToString()), Convert.ToDateTime(Convert.ToDateTime(absenceData.STARTDATE).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(absenceData.RETURNDATE).ToShortDateString()), float.Parse(absenceData.DURATION.ToString()), absenceData.HOLTYPE);
                if (!isBRSAppointment)
                {
                    return UserMessageConstants.RescheduleBRSAppointmentMessage;
                }
            }
            //Add new Absence entry for Status Change
            var newAbsenceRecord = new E_ABSENCE();
            newAbsenceRecord.JOURNALID = absenceData.JOURNALID;
            newAbsenceRecord.CERTNO = leaveStatusChangeRequest.certNo;
            newAbsenceRecord.DRNAME = leaveStatusChangeRequest.drName;
            if (!(leaveStatusChangeRequest.endDate == null))
                newAbsenceRecord.DURATION = leaveStatusChangeRequest.duration;
            else
                newAbsenceRecord.DURATION = absenceData.DURATION;
            //newAbsenceRecord.DURATION_HRS = leave_H;
            newAbsenceRecord.HOLTYPE = absenceData.HOLTYPE;
            if (!(leaveStatusChangeRequest.endDate == null))
            {
                newAbsenceRecord.ITEMACTIONID = leaveStatusChangeRequest.actionId;
                newAbsenceRecord.ITEMSTATUSID = leaveStatusChangeRequest.actionId;
            }
            else
            {
                newAbsenceRecord.ITEMACTIONID = absenceData.ITEMACTIONID;
                newAbsenceRecord.ITEMSTATUSID = absenceData.ITEMSTATUSID;
            }
            newAbsenceRecord.LASTACTIONDATE = DateTime.Now;
            newAbsenceRecord.LASTACTIONUSER = leaveStatusChangeRequest.actionBy;
            newAbsenceRecord.NOTES = leaveStatusChangeRequest.notes;
            newAbsenceRecord.REASON = absenceData.REASON;
            newAbsenceRecord.REASONID = leaveStatusChangeRequest.reasonId;
            newAbsenceRecord.RETURNDATE = leaveStatusChangeRequest.endDate == null ? (DateTime?)null : GeneralHelper.GetDateTimeFromString(leaveStatusChangeRequest.endDate);
            newAbsenceRecord.STARTDATE = leaveStatusChangeRequest.startDate == null ? absenceData.STARTDATE : GeneralHelper.GetDateTimeFromString(leaveStatusChangeRequest.startDate);
            newAbsenceRecord.AnticipatedReturnDate = leaveStatusChangeRequest.anticipatedReturnDate == null ? absenceData.AnticipatedReturnDate : GeneralHelper.GetDateTimeFromString(leaveStatusChangeRequest.anticipatedReturnDate);
            if (absenceData.E_JOURNAL.EMPLOYEEID.HasValue)
            {
                newAbsenceRecord.DURATION_TYPE = absenceData.DURATION_TYPE;
            }

            DbContext.E_ABSENCE.Add(newAbsenceRecord);

            //Change status in Journal of the Leave
            var journal = DbContext.E_JOURNAL.Where(j => j.JOURNALID == (int)absenceData.JOURNALID).FirstOrDefault();
            if (!(leaveStatusChangeRequest.endDate == null))
                journal.CURRENTITEMSTATUSID = leaveStatusChangeRequest.actionId;
            else
                journal.CURRENTITEMSTATUSID = absenceData.E_JOURNAL.CURRENTITEMSTATUSID;

            return getAbsenceStatusDescriptionById(leaveStatusChangeRequest.actionId);
        }
        public string ChangeSicknessLeaveStatusWeb(LeaveSicknessStatusChangeRequestWeb leaveStatusChangeRequest)
        {
            //Get E_ABSENCE DATA WITH ITS JOURNAL ENTRY
            var absenceData = DbContext.E_ABSENCE.Include("E_JOURNAL").Where(x => x.ABSENCEHISTORYID == leaveStatusChangeRequest.absenceHistoryId).FirstOrDefault();

            if (absenceData == null)
            {
                return ApplicationConstants.absenceError;
            }
            if (leaveStatusChangeRequest.actionId != 20)
            {
                string leaveRangeCheck = CheckIfSicknessDatesCorrect(leaveStatusChangeRequest, absenceData);
                if (leaveRangeCheck != "")
                {
                    return leaveRangeCheck;
                }
            }
            if (leaveStatusChangeRequest.actionId == 2)
            {
                var isBRSAppointment = checkIfBRSAppointment(int.Parse(absenceData.E_JOURNAL.EMPLOYEEID.ToString()), Convert.ToDateTime(Convert.ToDateTime(absenceData.STARTDATE).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(absenceData.RETURNDATE).ToShortDateString()), float.Parse(absenceData.DURATION.ToString()), absenceData.HOLTYPE);
                if (!isBRSAppointment)
                {
                    return UserMessageConstants.RescheduleBRSAppointmentMessage;
                }
            }
            //Add new Absence entry for Status Change
            var newAbsenceRecord = new E_ABSENCE();
            newAbsenceRecord.JOURNALID = absenceData.JOURNALID;
            newAbsenceRecord.CERTNO = leaveStatusChangeRequest.certNo;
            newAbsenceRecord.DRNAME = leaveStatusChangeRequest.drName;
            if (!(leaveStatusChangeRequest.endDate == null))
                newAbsenceRecord.DURATION = leaveStatusChangeRequest.duration;
            else
                newAbsenceRecord.DURATION = absenceData.DURATION;
            //newAbsenceRecord.DURATION_HRS = leave_H;
            newAbsenceRecord.HOLTYPE = leaveStatusChangeRequest.holtype;

            newAbsenceRecord.ITEMACTIONID = leaveStatusChangeRequest.actionId;
            newAbsenceRecord.ITEMSTATUSID = leaveStatusChangeRequest.actionId;
            newAbsenceRecord.LASTACTIONDATE = DateTime.Now;
            newAbsenceRecord.LASTACTIONUSER = leaveStatusChangeRequest.actionBy;
            newAbsenceRecord.NOTES = leaveStatusChangeRequest.notes;
            newAbsenceRecord.REASON = absenceData.REASON;
            newAbsenceRecord.REASONID = leaveStatusChangeRequest.reasonId;
            newAbsenceRecord.RETURNDATE = leaveStatusChangeRequest.endDate == null ? (DateTime?)null : GeneralHelper.GetDateTimeFromString(leaveStatusChangeRequest.endDate);
            newAbsenceRecord.STARTDATE = leaveStatusChangeRequest.startDate == null ? absenceData.STARTDATE : GeneralHelper.GetDateTimeFromString(leaveStatusChangeRequest.startDate);
            newAbsenceRecord.AnticipatedReturnDate = leaveStatusChangeRequest.anticipatedReturnDate == null ? absenceData.AnticipatedReturnDate : GeneralHelper.GetDateTimeFromString(leaveStatusChangeRequest.anticipatedReturnDate);
            if (absenceData.E_JOURNAL.EMPLOYEEID.HasValue)
            {
                newAbsenceRecord.DURATION_TYPE = absenceData.DURATION_TYPE;
            }

            DbContext.E_ABSENCE.Add(newAbsenceRecord);

            //Change status in Journal of the Leave
            var journal = DbContext.E_JOURNAL.Where(j => j.JOURNALID == (int)absenceData.JOURNALID).FirstOrDefault();

            journal.CURRENTITEMSTATUSID = leaveStatusChangeRequest.actionId;


            return getAbsenceStatusDescriptionById(leaveStatusChangeRequest.actionId);
        }

        public string CheckIfSicknessDatesCorrect(LeaveSicknessStatusChangeRequestWeb leaveStatusChangeRequest,E_ABSENCE absenceData)
        {
            DateTime StartDateObj = new DateTime();
            StartDateObj = DateTime.Parse(leaveStatusChangeRequest.startDate);
            DateTime EndDateObj = new DateTime();
            if(!string.IsNullOrEmpty(leaveStatusChangeRequest.endDate ))
            {
                EndDateObj = DateTime.Parse(leaveStatusChangeRequest.endDate);
            }
            else
            {
                EndDateObj = DateTime.Now;
            }
            //Apply or Log Status
            if (leaveStatusChangeRequest.actionId == 1 || leaveStatusChangeRequest.actionId == 3)
            {                
                var ReturnedAbsenceList = (from abs in DbContext.E_ABSENCE
                                    join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                    where abs.ABSENCEHISTORYID!=leaveStatusChangeRequest.absenceHistoryId
                                    && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID == 2
                                    && abs.RETURNDATE != null
                                    && StartDateObj > abs.STARTDATE
                                    && StartDateObj < abs.RETURNDATE
                                    && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                where abs1.JOURNALID == j.JOURNALID
                                                                select abs1.ABSENCEHISTORYID).ToList().Max()
                                           select abs).ToList();
                if (ReturnedAbsenceList.Count>0)
                {
                    return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                }
                ReturnedAbsenceList = (from abs in DbContext.E_ABSENCE
                                            join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                            where abs.ABSENCEHISTORYID!=leaveStatusChangeRequest.absenceHistoryId
                                            && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID == 2
                                            && abs.RETURNDATE != null
                                            && (StartDateObj == abs.STARTDATE
                                            || StartDateObj == abs.RETURNDATE)
                                            && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                        where abs1.JOURNALID == j.JOURNALID
                                                                        select abs1.ABSENCEHISTORYID).ToList().Max()
                                       select abs).ToList();
                foreach (var v in ReturnedAbsenceList)
                {
                    if (v.STARTDATE == StartDateObj)
                    {
                        if (leaveStatusChangeRequest.holtype.Equals("F") || leaveStatusChangeRequest.holtype.Equals("F-F") || leaveStatusChangeRequest.holtype.Equals("F-M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if (v.HOLTYPE.Equals("F") || v.HOLTYPE.Equals("F-F") || v.HOLTYPE.Equals("F-M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if ((v.HOLTYPE.Equals("A") || v.HOLTYPE.Equals("A-F") || v.HOLTYPE.Equals("A-M"))
                            && (leaveStatusChangeRequest.holtype.Equals("A") || leaveStatusChangeRequest.holtype.Equals("A-F") || leaveStatusChangeRequest.holtype.Equals("A-M")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if (v.HOLTYPE.Equals("M") && 
                            (leaveStatusChangeRequest.holtype.Equals("M")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                    }
                    if (v.RETURNDATE == StartDateObj)
                    {
                        if ((v.HOLTYPE.Equals("F-M") || v.HOLTYPE.Equals("M") || v.HOLTYPE.Equals("A-M")) 
                            && (leaveStatusChangeRequest.holtype.Equals("M") || leaveStatusChangeRequest.holtype.Equals("F")
                            || leaveStatusChangeRequest.holtype.Equals("F-F") || leaveStatusChangeRequest.holtype.Equals("F-M")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                    }
                }
                int ReturnedSickLeavecount = 0;
                var NonReturnedSickLeaves = (from abs in DbContext.E_ABSENCE
                                                join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                                where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                                && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                                && (abs.RETURNDATE == null && StartDateObj > abs.STARTDATE)
                                                && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                            where abs1.JOURNALID == j.JOURNALID
                                                                            select abs1.ABSENCEHISTORYID).ToList().Max()
                                             select abs).ToList();
                var distinctJournalIds = NonReturnedSickLeaves.GroupBy(x => x.JOURNALID).Distinct();
                foreach (var jId in distinctJournalIds)
                {
                    int count = 0;
                    count = (from abs in DbContext.E_ABSENCE
                                join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID 
                                && j.ITEMNATUREID == 1 && (abs.ITEMACTIONID == 2 || abs.ITEMACTIONID == 20)
                                && j.JOURNALID == jId.Key
                                && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                            where abs1.JOURNALID == j.JOURNALID
                                                            select abs1.ABSENCEHISTORYID).ToList().Max()
                             select abs).Count();
                    ReturnedSickLeavecount = ReturnedSickLeavecount + count;
                }
                if (distinctJournalIds.Count() > ReturnedSickLeavecount)
                {
                    return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                }

                ReturnedSickLeavecount = 0;
                var NonReturnedSickLeave = (from abs in DbContext.E_ABSENCE
                                            join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                            where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                            && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                            && (abs.RETURNDATE == null && StartDateObj == abs.STARTDATE)
                                            && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                        where abs1.JOURNALID == j.JOURNALID
                                                                        select abs1.ABSENCEHISTORYID).ToList().Max()
                                            select abs).FirstOrDefault();
                if (NonReturnedSickLeave != null)
                {
                    int count = (from abs in DbContext.E_ABSENCE
                                    join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                    where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                    && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID 
                                    && j.ITEMNATUREID == 1 && (abs.ITEMACTIONID == 2 || abs.ITEMACTIONID == 20)
                                    && j.JOURNALID == NonReturnedSickLeave.JOURNALID
                                    && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                where abs1.JOURNALID == j.JOURNALID
                                                                select abs1.ABSENCEHISTORYID).ToList().Max()
                                 select abs).Count();

                    if (count == 0)
                    {
                        if (leaveStatusChangeRequest.holtype.Equals("F") || leaveStatusChangeRequest.holtype.Equals("F-F") || leaveStatusChangeRequest.holtype.Equals("F-M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if (NonReturnedSickLeave.HOLTYPE.Equals("F") || NonReturnedSickLeave.HOLTYPE.Equals("F-F")
                            || NonReturnedSickLeave.HOLTYPE.Equals("F-M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if ((NonReturnedSickLeave.HOLTYPE.Equals("A") || NonReturnedSickLeave.HOLTYPE.Equals("A-F") || NonReturnedSickLeave.HOLTYPE.Equals("A-M")) 
                         && (leaveStatusChangeRequest.holtype.Equals("A") || leaveStatusChangeRequest.holtype.Equals("A-F") || leaveStatusChangeRequest.holtype.Equals("A-M")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if (NonReturnedSickLeave.HOLTYPE.Equals("M") && leaveStatusChangeRequest.holtype.Equals("M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                    }
                }
                var TopStartLeave = (from abs in DbContext.E_ABSENCE
                                        join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                        where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                        && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID 
                                        && j.ITEMNATUREID == 1 
                                        && abs.STARTDATE > StartDateObj && abs.ITEMSTATUSID != 20
                                        && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                    where abs1.JOURNALID == j.JOURNALID
                                                                    select abs1.ABSENCEHISTORYID).ToList().Max()
                                     select abs).OrderBy(x => x.STARTDATE).FirstOrDefault();

                if (TopStartLeave != null)
                {
                    return UserMessageConstants.SickLeaveErrorMessage;
                }
                else
                {
                    return "";
                }
            }
            //Returned Status
            else if (leaveStatusChangeRequest.actionId == 2)
            {
                var ReturnedAbsenceList = (from abs in DbContext.E_ABSENCE
                                           join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                           where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                           && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID == 2
                                           && abs.RETURNDATE != null
                                           && StartDateObj > abs.STARTDATE
                                           && StartDateObj < abs.RETURNDATE
                                           && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                       where abs1.JOURNALID == j.JOURNALID
                                                                       select abs1.ABSENCEHISTORYID).ToList().Max()
                                           select abs).ToList();
                if (ReturnedAbsenceList.Count > 0)
                {
                    return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                }
                ReturnedAbsenceList = (from abs in DbContext.E_ABSENCE
                                       join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                       where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                       && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID == 2
                                       && abs.RETURNDATE != null &&
                    ((StartDateObj == abs.STARTDATE || StartDateObj == abs.RETURNDATE)
                  || (EndDateObj == abs.STARTDATE || EndDateObj == abs.RETURNDATE))
                                       && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                   where abs1.JOURNALID == j.JOURNALID
                                                                   select abs1.ABSENCEHISTORYID).ToList().Max()
                                       select abs).ToList();
                foreach (var v in ReturnedAbsenceList)
                {                    
                    //start Date
                    if (v.STARTDATE == StartDateObj)
                    {
                        if (leaveStatusChangeRequest.holtype.Equals("F") || leaveStatusChangeRequest.holtype.Equals("F-F") || leaveStatusChangeRequest.holtype.Equals("F-M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if (v.HOLTYPE.Equals("F") || v.HOLTYPE.Equals("F-F") || v.HOLTYPE.Equals("F-M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if ((v.HOLTYPE.Equals("A") || v.HOLTYPE.Equals("A-F") || v.HOLTYPE.Equals("A-M"))
                             && (leaveStatusChangeRequest.holtype.Equals("A") || leaveStatusChangeRequest.holtype.Equals("A-F") || leaveStatusChangeRequest.holtype.Equals("A-M")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if (v.HOLTYPE.Equals("M") && leaveStatusChangeRequest.holtype.Equals("M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                    }
                    if (v.RETURNDATE == StartDateObj)
                    {
                        if ((v.HOLTYPE.Equals("F-M") || v.HOLTYPE.Equals("M") || v.HOLTYPE.Equals("A-M"))
                            && (leaveStatusChangeRequest.holtype.Equals("M") || leaveStatusChangeRequest.holtype.Equals("F")
                            || leaveStatusChangeRequest.holtype.Equals("F-F") || leaveStatusChangeRequest.holtype.Equals("F-M")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                    }
                    //end Date
                    if (v.STARTDATE == EndDateObj)
                    {

                        if ((v.HOLTYPE.Equals("F") || v.HOLTYPE.Equals("F-F") || v.HOLTYPE.Equals("F-M") || v.HOLTYPE.Equals("M"))
                            && (leaveStatusChangeRequest.holtype.Equals("M") || leaveStatusChangeRequest.holtype.Equals("A-M") || leaveStatusChangeRequest.holtype.Equals("F-M")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                    }
                    if (v.RETURNDATE == EndDateObj)
                    {
                        if ((v.HOLTYPE.Equals("F-F") || v.HOLTYPE.Equals("F") || v.HOLTYPE.Equals("A") || v.HOLTYPE.Equals("A-F")) && leaveStatusChangeRequest.holtype!="M")
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if(v.HOLTYPE.Equals("F-M")|| v.HOLTYPE.Equals("A-M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if(v.HOLTYPE.Equals("M") && (leaveStatusChangeRequest.holtype.Equals("F-M") || leaveStatusChangeRequest.holtype.Equals("A-M") || leaveStatusChangeRequest.holtype.Equals("M")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                    }
                }


                int ReturnedSickLeavecount = 0;
                var NonReturnedSickLeaves = (from abs in DbContext.E_ABSENCE
                                             join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                             where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                             && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                             && (abs.RETURNDATE == null && StartDateObj > abs.STARTDATE)
                                             && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                         where abs1.JOURNALID == j.JOURNALID
                                                                         select abs1.ABSENCEHISTORYID).ToList().Max()
                                             select abs).ToList();
                var distinctJournalIds = NonReturnedSickLeaves.GroupBy(x => x.JOURNALID).Distinct();
                foreach (var jId in distinctJournalIds)
                {
                    int count = 0;
                    count = (from abs in DbContext.E_ABSENCE
                             join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                             where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                             && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID
                             && j.ITEMNATUREID == 1 && (abs.ITEMACTIONID == 2 || abs.ITEMACTIONID == 20)
                             && j.JOURNALID == jId.Key
                             && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                         where abs1.JOURNALID == j.JOURNALID
                                                         select abs1.ABSENCEHISTORYID).ToList().Max()
                             select abs).Count();
                    ReturnedSickLeavecount = ReturnedSickLeavecount + count;
                }
                if (distinctJournalIds.Count() > ReturnedSickLeavecount)
                {
                    return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                }

                ReturnedSickLeavecount = 0;
                var NonReturnedSickLeave = (from abs in DbContext.E_ABSENCE
                                            join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                            where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                            && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                            && (abs.RETURNDATE == null && StartDateObj == abs.STARTDATE)
                                            && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                        where abs1.JOURNALID == j.JOURNALID
                                                                        select abs1.ABSENCEHISTORYID).ToList().Max()
                                            select abs).FirstOrDefault();
                if (NonReturnedSickLeave != null)
                {
                    int count = (from abs in DbContext.E_ABSENCE
                                 join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                 where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                 && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID
                                 && j.ITEMNATUREID == 1 && (abs.ITEMACTIONID == 2 || abs.ITEMACTIONID == 20)
                                 && j.JOURNALID == NonReturnedSickLeave.JOURNALID
                                 && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                             where abs1.JOURNALID == j.JOURNALID
                                                             select abs1.ABSENCEHISTORYID).ToList().Max()
                                 select abs).Count();
                    if (count == 0)
                    {
                        if(StartDateObj!=EndDateObj)
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if (!leaveStatusChangeRequest.holtype.Equals("M"))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        if (leaveStatusChangeRequest.holtype.Equals("M") && 
                            (NonReturnedSickLeave.HOLTYPE.Equals("M") || NonReturnedSickLeave.HOLTYPE.Equals("F") || NonReturnedSickLeave.HOLTYPE.Equals("F-M") || NonReturnedSickLeave.HOLTYPE.Equals("F-F")))
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                    }
                }
                var TopStartLeave = (from abs in DbContext.E_ABSENCE
                                     join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                     where abs.ABSENCEHISTORYID != leaveStatusChangeRequest.absenceHistoryId
                                     && j.EMPLOYEEID == absenceData.E_JOURNAL.EMPLOYEEID
                                     && j.ITEMNATUREID == 1
                                     && StartDateObj < abs.STARTDATE
                                     && EndDateObj > abs.STARTDATE
                                     && abs.ITEMSTATUSID != 20
                                     && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                 where abs1.JOURNALID == j.JOURNALID
                                                                 select abs1.ABSENCEHISTORYID).ToList().Max()
                                     select abs).OrderBy(x => x.STARTDATE).FirstOrDefault();

                if (TopStartLeave != null)
                {
                    return UserMessageConstants.SickLeaveErrorMessage;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }
        }
        public bool AddToilRecord(RecordToilRequest request)
        {
            if (request.leaveType != "BRS TOIL Recorded")
            {
                var isAvailable = checkIfLeaveAvailable(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), request.duration, "F-F");
                if (!isAvailable)
                {
                    return isAvailable;
                }
            }
            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {


                    int? itemId, itemNatureId, itemStatusId = 0, itemActionId = 0;

                    var itemIdData = (from n in DbContext.E_NATURE
                                      where n.DESCRIPTION == request.leaveType
                                      select n).FirstOrDefault();
                    // recorded by manager
                    if (request.leaveType == "BRS TOIL Recorded")
                    {
                        itemStatusId = (from s in DbContext.E_STATUS
                                        where s.DESCRIPTION == "Approved"
                                        select s).FirstOrDefault().ITEMSTATUSID;
                        itemActionId = (from a in DbContext.E_ACTION
                                        where a.DESCRIPTION == "Approve"
                                        select a).FirstOrDefault().ITEMACTIONID;
                    }
                    else
                    {

                        itemStatusId = (from s in DbContext.E_STATUS
                                        where s.DESCRIPTION == "Pending" // Pending
                                        select s).FirstOrDefault().ITEMSTATUSID;
                        itemActionId = (from a in DbContext.E_ACTION
                                        where a.DESCRIPTION == "Apply" // Apply
                                        select a).FirstOrDefault().ITEMACTIONID;
                    }
                    itemId = itemIdData.ITEMID;
                    itemNatureId = itemIdData.ITEMNATUREID;

                    DatabaseEntities.E_JOURNAL journal = new E_JOURNAL();

                    journal.EMPLOYEEID = request.employeeId;
                    journal.ITEMID = itemId;
                    journal.ITEMNATUREID = itemNatureId;
                    journal.CURRENTITEMSTATUSID = itemStatusId;
                    journal.CREATIONDATE = DateTime.Now;
                    //journal.TITLE = request.notes;
                    DbContext.E_JOURNAL.Add(journal);
                    DbContext.SaveChanges();



                    DatabaseEntities.E_ABSENCE absence = new E_ABSENCE();
                    absence.JOURNALID = journal.JOURNALID;
                    absence.ITEMSTATUSID = itemStatusId;
                    absence.ITEMACTIONID = itemActionId;
                    absence.LASTACTIONDATE = DateTime.Now;
                    absence.LASTACTIONUSER = request.recordedBy;
                    absence.STARTDATE = Convert.ToDateTime(request.startDate + " " + request.from);
                    absence.RETURNDATE = Convert.ToDateTime(request.startDate + " " + request.to);
                    absence.NOTES = request.notes;
                    absence.REASON = request.notes;
                    absence.DURATION_HRS = request.duration;
                    absence.DURATION = request.duration;

                    // its fixed >> DURATION_TYPE is hours for toil.
                    //absence.DURATION_TYPE = getEmpHolidayIndicatorDescription(request.employeeId);
                    absence.DURATION_TYPE = "Hours";

                    DbContext.E_ABSENCE.Add(absence);
                    DbContext.SaveChanges();
                    /*code*/
                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();
                }

                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }
            }

            return true;
        }

        public bool AddInternalMeetingRecord(RecordInternalMeetingRequest request)
        {
            //Check if leave is already available on the given day.
            var isAvailable = checkIfInternalMeetingAvailable(request.employeeId, Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(request.startDate).ToShortDateString()), request.from, request.to, null);
            if (!isAvailable)
            {
                return isAvailable;
            }
            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    //Get item nature and item status for applying internal meeting
                    int? itemId, itemNatureId, itemStatusId = 0, itemActionId = 0;

                    var itemIdData = (from n in DbContext.E_NATURE
                                      where n.DESCRIPTION == ApplicationConstants.internalMeeting
                                      select n).FirstOrDefault();

                    itemStatusId = (from s in DbContext.E_STATUS
                                    where s.DESCRIPTION == "Pending"
                                    select s).FirstOrDefault().ITEMSTATUSID;
                    itemActionId = (from a in DbContext.E_ACTION
                                    where a.DESCRIPTION == "Apply"
                                    select a).FirstOrDefault().ITEMACTIONID;

                    itemId = itemIdData.ITEMID;
                    itemNatureId = itemIdData.ITEMNATUREID;

                    DatabaseEntities.E_JOURNAL journal = new E_JOURNAL();

                    journal.EMPLOYEEID = request.employeeId;
                    journal.ITEMID = itemId;
                    journal.ITEMNATUREID = itemNatureId;
                    journal.CURRENTITEMSTATUSID = itemStatusId;
                    journal.CREATIONDATE = DateTime.Now;
                    journal.TITLE = request.title;
                    DbContext.E_JOURNAL.Add(journal);
                    DbContext.SaveChanges();

                    DatabaseEntities.E_ABSENCE absence = new E_ABSENCE();
                    absence.JOURNALID = journal.JOURNALID;
                    absence.ITEMSTATUSID = itemStatusId;
                    absence.ITEMACTIONID = itemActionId;
                    absence.LASTACTIONDATE = DateTime.Now;
                    absence.LASTACTIONUSER = request.recordedBy;
                    absence.STARTDATE = Convert.ToDateTime(request.startDate + " " + request.from);
                    absence.RETURNDATE = Convert.ToDateTime(request.startDate + " " + request.to);
                    absence.NOTES = request.notes;
                    absence.REASON = request.notes;
                    absence.DURATION_HRS = request.duration;
                    absence.DURATION = request.duration;
                    // its fixed >> DURATION_TYPE is hours for toil.
                    //absence.DURATION_TYPE = getEmpHolidayIndicatorDescription(request.employeeId);
                    absence.DURATION_TYPE = "Hours";

                    DbContext.E_ABSENCE.Add(absence);
                    DbContext.SaveChanges();
                    /*code*/
                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();
                }

                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }
            }

            return true;
        }

        public ToilLeaveResponce GetToilLeaves(AbsenceRequest request)
        {
            ToilLeaveResponce responce = new ToilLeaveResponce();

            var data = (from abs in DbContext.E_LeaveOfToil(request.employeeId, null)
                        select new AbsenceLeave()
                        {
                            absenceHistoryId = abs.ABSENCEHISTORYID,
                            endDate = abs.RETURNDATE,
                            nature = abs.Nature,
                            absentDays = abs.DAYS_ABS,
                            startDate = abs.STARTDATE,
                            status = abs.Status,
                            holidayType = abs.HolType
                        }).ToList();
            responce.toilLeaves = data;

            var lineManagerData = (from d in DbContext.E_JOBDETAILS
                                   where d.EMPLOYEEID == request.employeeId
                                   select d).FirstOrDefault();

            var leaveStats = (from stats in DbContext.E_LeaveStats(request.employeeId, null, true, null) select stats).FirstOrDefault();

            responce.timeOfInlieuOwed = leaveStats.TIME_OFF_IN_LIEU_OWED;
            responce.toilHours = leaveStats.TOIL;
            responce.lineManager = lineManagerData.EMPLOYEEID;
            return responce;
        }

        public List<InternalMeetingsLeaveResponse> GetInternalMeetingLeave(AbsenceRequest request)
        {
            var data = (from abs in DbContext.E_LeaveOfInternalMeeting(request.employeeId, null)
                        select new InternalMeetingsLeaveResponse()
                        {
                            endDate = abs.RETURNDATE,
                            nature = abs.Nature,
                            absentHours = abs.Duration,
                            startDate = abs.STARTDATE,
                            status = abs.Status,
                            holidayType = abs.HolType,
                            absenceHistoryId = abs.ABSENCEHISTORYID,
                            reason = abs.Reason,
                            title = abs.Title
                        }).ToList();

            return data;
        }
        
        public string ChangeInternalLeaveStatus(InternalLeaveStatusRequest leaveStatusChangeRequest)
        {
            //Get E_ABSENCE DATA WITH ITS JOURNAL ENTRY
            var absenceData = DbContext.E_ABSENCE.Include("E_JOURNAL").Where(x => x.ABSENCEHISTORYID == leaveStatusChangeRequest.absenceHistoryId).FirstOrDefault();

            if (absenceData == null)
            {
                return ApplicationConstants.absenceError;
            }
            if (leaveStatusChangeRequest.actionId == 5)
            {
                var isBRSAppointment = checkIfBRSAppointment(int.Parse(absenceData.E_JOURNAL.EMPLOYEEID.ToString()), Convert.ToDateTime(Convert.ToDateTime(absenceData.STARTDATE).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(absenceData.RETURNDATE).ToShortDateString()), float.Parse(absenceData.DURATION.ToString()), absenceData.HOLTYPE);
                if (!isBRSAppointment)
                {
                    return UserMessageConstants.RescheduleBRSAppointmentMessage;
                }
            }
            //Check Existing Leave
            var startDate = absenceData.STARTDATE == null ? DateTime.Now : (DateTime)absenceData.STARTDATE;
            var returnDate = absenceData.RETURNDATE == null ? DateTime.Now : (DateTime)absenceData.RETURNDATE;

            if (leaveStatusChangeRequest.actionId != 20 && leaveStatusChangeRequest.actionId != 4) // not canceled & declined
            {
                var result = checkIfInternalMeetingAvailable(int.Parse(absenceData.E_JOURNAL.EMPLOYEEID.ToString()), Convert.ToDateTime(Convert.ToDateTime(leaveStatusChangeRequest.startDate).ToShortDateString()), Convert.ToDateTime(Convert.ToDateTime(leaveStatusChangeRequest.startDate).ToShortDateString()), leaveStatusChangeRequest.from, leaveStatusChangeRequest.to, leaveStatusChangeRequest.absenceHistoryId);
                if (!result)
                {
                    return ApplicationConstants.overlap;
                }
            }
            
            //Add new Absence entry for Status Change
            var newAbsenceRecord = new E_ABSENCE();
            newAbsenceRecord = absenceData;

            newAbsenceRecord.JOURNALID = absenceData.JOURNALID;
            newAbsenceRecord.DURATION = leaveStatusChangeRequest.duration;
            newAbsenceRecord.ITEMACTIONID = leaveStatusChangeRequest.actionId;
            newAbsenceRecord.ITEMSTATUSID = leaveStatusChangeRequest.actionId;
            newAbsenceRecord.LASTACTIONDATE = DateTime.Now;
            newAbsenceRecord.LASTACTIONUSER = leaveStatusChangeRequest.actionBy;
            newAbsenceRecord.NOTES = leaveStatusChangeRequest.notes;
            newAbsenceRecord.REASON = absenceData.REASON;
            newAbsenceRecord.RETURNDATE = Convert.ToDateTime(leaveStatusChangeRequest.endDate + " " + leaveStatusChangeRequest.to);
            newAbsenceRecord.STARTDATE = Convert.ToDateTime(leaveStatusChangeRequest.startDate + " " + leaveStatusChangeRequest.from);
            absenceData.E_JOURNAL.TITLE = leaveStatusChangeRequest.title;

            DbContext.E_ABSENCE.Add(newAbsenceRecord);

            //Change status in Journal of the Leave
            var journal = DbContext.E_JOURNAL.Where(j => j.JOURNALID == (int)absenceData.JOURNALID).FirstOrDefault();
            journal.CURRENTITEMSTATUSID = leaveStatusChangeRequest.actionId;

            var dataResult =
            (from abs in DbContext.E_ABSENCE
             join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
             where abs.ABSENCEHISTORYID == leaveStatusChangeRequest.absenceHistoryId
             select j).FirstOrDefault();

            // Send E-mail to Employee
            int actionBy = leaveStatusChangeRequest.actionBy;
            int actionId = leaveStatusChangeRequest.actionId;
            int? LineManagerId = (from JD in DbContext.E_JOBDETAILS
                                  where JD.EMPLOYEEID == (int)dataResult.EMPLOYEEID
                                  select JD.LINEMANAGER).FirstOrDefault();

            if (LineManagerId != null && LineManagerId == actionBy)
            {
                if (actionId == 4 || actionId == 5)
                {
                    string action = actionId == 4 ? "Declined" : "Approved";
                    ////sendEmailNotification to employee

                    var emailData = (from E in DbContext.E__EMPLOYEE
                                     join EC in DbContext.E_CONTACT on E.EMPLOYEEID equals EC.EMPLOYEEID
                                     join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                                     join LM in DbContext.E__EMPLOYEE on J.LINEMANAGER equals LM.EMPLOYEEID
                                     where E.EMPLOYEEID == (int)dataResult.EMPLOYEEID
                                     select new CancelLeaveEmailNotification()
                                     {
                                         employeeId = E.EMPLOYEEID,
                                         employeeName = E.FIRSTNAME + " " + E.LASTNAME,
                                         employeeEmail = EC.WORKEMAIL,
                                         lineManagerName = LM.FIRSTNAME + " " + LM.LASTNAME

                                     }).FirstOrDefault();


                    if (emailData != null && !string.IsNullOrEmpty(emailData.employeeEmail))
                    {
                        string body = string.Empty;
                        body = "Dear " + emailData.employeeName + ", <br /><br />";
                        body = body + "Your request for annual leave on the " + absenceData.STARTDATE.Value.Date.ToShortDateString() + "-" + absenceData.RETURNDATE.Value.Date.ToShortDateString() + " ";
                        body = body + "has been " + action + " by " + emailData.lineManagerName + ".<br /><br />";
                        body = body + "You are able to amend or cancel up to and including the start date of the leave request using the My Broadland App or the My Job module.  <br/><br/>";
                        body = body + "Thanks<br/>";
                        body = body + "HR Team";
                        EmailHelper.sendHtmlFormattedEmail(emailData.employeeName, emailData.employeeEmail, "Annual Leave Request", body);
                    }
                }
            }

            return getAbsenceStatusDescriptionById(leaveStatusChangeRequest.actionId);
        }

        private bool checkIfLeaveAvailable(int employeeId, DateTime startDate, DateTime returnDate, float? duraiton, string holidayType)
        {
            bool isAVailable = true;
            string sDate = startDate.ToString("yyyy-dd-MM 00:00:00.000");
            string eDate = returnDate.ToString("yyyy-dd-MM 23:59:59.999");
            var data = DbContext.Database.SqlQuery<double>("select dbo.E_CHECK_EXISTINGLEAVE({0},{1},{2},{3},{4})", employeeId, sDate, eDate, duraiton, holidayType).FirstOrDefault();
            if (data > 0)
            {
                isAVailable = false;
            }
            return isAVailable;
        }
        private bool checkIfInternalMeetingAvailable(int employeeId, DateTime startDate, DateTime returnDate, string from, string to, int? absenseHistoryId)
        {
            bool isAVailable = true;
            var LeaveCount = 0;
            string holType = "";
            var tempStartDateTime = Convert.ToDateTime(startDate.ToShortDateString() + " " + from);
            var tempEndDateTime = Convert.ToDateTime(startDate.ToShortDateString() + " " + to);

            // leave date time is in morning or afternoon or a full day leave.

            if (tempStartDateTime.Hour < 12 && tempEndDateTime.Hour <= 12)
            {
                holType = "M";
            }
            
            if (tempStartDateTime.Hour >= 12 && tempEndDateTime.Hour > 12)
            {
                holType = "A";
            }

            if (tempStartDateTime.Hour < 12 && tempEndDateTime.Hour > 12)
            {
                holType = "F";
            }

            // check employee is on anual leave on the same date we are going to add internal leave.
            var data = (from abs in DbContext.E_ABSENCE
                              join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                              where j.EMPLOYEEID == employeeId
                              && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                          where abs1.JOURNALID == j.JOURNALID
                                                          select abs1.ABSENCEHISTORYID).ToList().Max()
                              && (
                              (j.ITEMNATUREID == 1 && abs.ITEMSTATUSID == 2 && startDate >= abs.STARTDATE && startDate <= abs.RETURNDATE)
                              ||
                              ((abs.ITEMSTATUSID == 5 || abs.ITEMSTATUSID == 3) && j.ITEMNATUREID != 48 && startDate >= abs.STARTDATE && startDate <= abs.RETURNDATE)
                              )
                              select abs).ToList();

            if (data != null && data.Count > 0)
            {
                //LeaveCount = data.Count;

                foreach (var v in data)
                {
                    if (v.STARTDATE == startDate)
                    {
                        if (holType.Equals("F-M"))
                        {
                            LeaveCount = 1;
                            break;
                        }
                        if (v.HOLTYPE.Equals("F") || v.HOLTYPE.Equals("F-F") || v.HOLTYPE.Equals("F-M"))
                        {
                            LeaveCount = 1;
                            break;
                        }
                        if ((v.HOLTYPE.Equals("A") || v.HOLTYPE.Equals("A-F") || v.HOLTYPE.Equals("A-M"))
                           && (holType.Equals("A") || holType.Equals("A-F") || holType.Equals("A-M")))
                        {
                            LeaveCount = 1;
                            break;
                        }
                        if (v.HOLTYPE.Equals("M") && (holType.Equals("M") || holType.Equals("F") || holType.Equals("F-F")))
                        {
                            LeaveCount = 1;
                            break;
                        }
                    }
                    if (v.RETURNDATE == startDate)
                    {
                        if ((v.HOLTYPE.Equals("F-M") || v.HOLTYPE.Equals("A-M")) && holType.Equals("M"))
                        {
                            LeaveCount = 1;
                            break;
                        }
                    }
                }
            }

            if (LeaveCount > 0)
            {
                isAVailable = false;
                return isAVailable;
            }
            else
            {
                // check employee is on leave and never be returned.
                LeaveCount = 0;
                var NonReturnedSickLeave = (from abs in DbContext.E_ABSENCE
                                            join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                            where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                            && (abs.RETURNDATE == null && startDate >= abs.STARTDATE)
                                            select abs).ToList();
                var distinctJournalIds = NonReturnedSickLeave.GroupBy(x => x.JOURNALID).Distinct();
                foreach (var jId in distinctJournalIds)
                {
                    int count = 0;
                    count = (from abs in DbContext.E_ABSENCE
                             join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                             where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && (abs.ITEMACTIONID == 2 || abs.ITEMACTIONID == 20)
                             && j.JOURNALID == jId.Key
                             select abs).Count();
                    LeaveCount = LeaveCount + count;
                }

                if (distinctJournalIds.Count() > LeaveCount)
                {
                    isAVailable = false;
                    return isAVailable;
                }
                else
                {
                    var tempStartDate = Convert.ToDateTime(startDate.ToShortDateString() + " " + from);
                    var tempEndDate = Convert.ToDateTime(startDate.ToShortDateString() + " " + to);

                    // check internal meeting leave overlap.
                    var internalLeaveCount = 0;
                    if (absenseHistoryId.HasValue)
                    {
                        internalLeaveCount = (from abs in DbContext.E_ABSENCE
                                              join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                              where j.EMPLOYEEID == employeeId
                                              && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                          where abs1.JOURNALID == j.JOURNALID
                                                                          select abs1.ABSENCEHISTORYID).ToList().Max()
                                              && abs.ABSENCEHISTORYID != absenseHistoryId.Value
                                              && j.ITEMNATUREID == 48
                                              && (tempStartDate >= abs.STARTDATE && tempStartDate < abs.RETURNDATE ||
                                                tempEndDate > abs.STARTDATE && tempEndDate <= abs.RETURNDATE)
                                              && (abs.ITEMSTATUSID != 20 && abs.ITEMSTATUSID != 4)
                                              select abs).Count();
                    }
                    else
                    {
                        internalLeaveCount = (from abs in DbContext.E_ABSENCE
                                              join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                              where j.EMPLOYEEID == employeeId
                                              && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                          where abs1.JOURNALID == j.JOURNALID
                                                                          select abs1.ABSENCEHISTORYID).ToList().Max()
                                              && j.ITEMNATUREID == 48
                                              && (tempStartDate >= abs.STARTDATE && tempStartDate < abs.RETURNDATE || 
                                                tempEndDate > abs.STARTDATE && tempEndDate <= abs.RETURNDATE)
                                              && (abs.ITEMSTATUSID != 20 && abs.ITEMSTATUSID != 4)
                                              select abs).Count();
                    }

                    if (internalLeaveCount > 0)
                    {
                        isAVailable = false;
                    }
                    else
                    {
                        isAVailable = true;
                    }
                }
            }

            return isAVailable;
        }
        private string checkIfSicknessLeaveAvailable(int employeeId, DateTime startDate,string holType)
        {

            //For returned Leaves
            int ReturnedSickLeavecount = 0;
                //Returned leaves Started before current date and ended after current date
                ReturnedSickLeavecount = (from abs in DbContext.E_ABSENCE
                                          join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                          where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                          && (abs.RETURNDATE != null && startDate > abs.STARTDATE && startDate < abs.RETURNDATE )
                                          && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                      where abs1.JOURNALID== j.JOURNALID
                                                                      select abs1.ABSENCEHISTORYID).ToList().Max()
                                                                      
                                          select abs).Count();
            if (ReturnedSickLeavecount > 0)
            {
                return UserMessageConstants.SickLeaveAlreadyBookedMessage;
            }
            else 
            {
                //Returned leaves Started on current date or ended on current date
                var ReturnedSickLeaves = (from abs in DbContext.E_ABSENCE
                                          join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                          where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                          && (abs.RETURNDATE != null && (startDate == abs.STARTDATE || startDate == abs.RETURNDATE))
                                          && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                      where abs1.JOURNALID == j.JOURNALID
                                                                      select abs1.ABSENCEHISTORYID).ToList().Max()
                                          select abs).ToList();
                foreach(var v in ReturnedSickLeaves)
                {
                    if(v.STARTDATE==startDate)
                    {
                        if(holType.Equals("F-M"))
                        {
                            ReturnedSickLeavecount = 1;
                            break;
                        }
                        if(v.HOLTYPE.Equals("F")|| v.HOLTYPE.Equals("F-F")|| v.HOLTYPE.Equals("F-M"))
                        {
                            ReturnedSickLeavecount =1;
                            break;
                        }
                        if ((v.HOLTYPE.Equals("A") || v.HOLTYPE.Equals("A-F") || v.HOLTYPE.Equals("A-M")) 
                           && (holType.Equals("A") ||   holType.Equals("A-F") ||   holType.Equals("A-M")))
                        {
                            ReturnedSickLeavecount = 1;
                            break;
                        }
                        if (v.HOLTYPE.Equals("M") && (holType.Equals("M") || holType.Equals("F") || holType.Equals("F-F")))
                        {
                            ReturnedSickLeavecount = 1;
                            break;
                        }
                    }
                    if(v.RETURNDATE==startDate)
                    {
                        if ((v.HOLTYPE.Equals("F-M") || v.HOLTYPE.Equals("M") || v.HOLTYPE.Equals("A-M")) 
                            && (holType.Equals("M") || holType.Equals("F") || holType.Equals("F-F") || holType.Equals("F-M")))
                        {
                            ReturnedSickLeavecount = 1;
                            break;
                        }
                    }
                }
                if (ReturnedSickLeavecount > 0)
                {
                    return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                }
                //For non returned Leaves
                else
                {
                    //Non Returned leaves Started before current date
                    ReturnedSickLeavecount = 0;
                    var NonReturnedSickLeaves = (from abs in DbContext.E_ABSENCE
                                                join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                                where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                                && (abs.RETURNDATE == null && startDate > abs.STARTDATE)
                                                && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                            where abs1.JOURNALID == j.JOURNALID
                                                                            select abs1.ABSENCEHISTORYID).ToList().Max()
                                                 select abs).ToList();
                    var distinctJournalIds = NonReturnedSickLeaves.GroupBy(x => x.JOURNALID).Distinct();
                    foreach (var jId in distinctJournalIds)
                    {
                        int count = 0;
                        count = (from abs in DbContext.E_ABSENCE
                                 join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                 where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && (abs.ITEMACTIONID == 2 || abs.ITEMACTIONID == 20)
                                 && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                             where abs1.JOURNALID == j.JOURNALID
                                                             select abs1.ABSENCEHISTORYID).ToList().Max()
                                 && j.JOURNALID == jId.Key
                                 select abs).Count();
                        ReturnedSickLeavecount = ReturnedSickLeavecount + count;
                    }
                    if (distinctJournalIds.Count() > ReturnedSickLeavecount)
                    {
                        return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                    }
                    else
                    {
                        //Non Returned leaves Started on current date
                        ReturnedSickLeavecount = 0;
                        var NonReturnedSickLeave = (from abs in DbContext.E_ABSENCE
                                                    join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                                    where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && abs.ITEMSTATUSID != 20
                                                    && (abs.RETURNDATE == null && startDate == abs.STARTDATE)
                                                    && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                                where abs1.JOURNALID == j.JOURNALID
                                                                                select abs1.ABSENCEHISTORYID).ToList().Max()
                                                    select abs).FirstOrDefault();
                        if (NonReturnedSickLeave != null)
                        {
                            int count = (from abs in DbContext.E_ABSENCE
                                         join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                         where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && (abs.ITEMACTIONID == 2 || abs.ITEMACTIONID == 20)
                                         && j.JOURNALID == NonReturnedSickLeave.JOURNALID
                                         && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                     where abs1.JOURNALID == j.JOURNALID
                                                                     select abs1.ABSENCEHISTORYID).ToList().Max()
                                         select abs).Count();
                            if (count == 0)
                            {
                                if (holType.Equals("F-M"))
                                {
                                    ReturnedSickLeavecount = ReturnedSickLeavecount + 1;
                                }
                                if (NonReturnedSickLeave.HOLTYPE.Equals("F") || NonReturnedSickLeave.HOLTYPE.Equals("F-F")
                                    || NonReturnedSickLeave.HOLTYPE.Equals("F-M"))
                                {
                                    ReturnedSickLeavecount = ReturnedSickLeavecount + 1;
                                }
                                if (
                                (NonReturnedSickLeave.HOLTYPE.Equals("A") || NonReturnedSickLeave.HOLTYPE.Equals("A-F")|| NonReturnedSickLeave.HOLTYPE.Equals("A-M"))
                                && (holType.Equals("A") || holType.Equals("A-F") || holType.Equals("A-M")))
                                {
                                    ReturnedSickLeavecount = ReturnedSickLeavecount + 1;
                                }
                                if (NonReturnedSickLeave.HOLTYPE.Equals("M") && (holType.Equals("M") || holType.Equals("F")|| holType.Equals("F-F")))
                                {
                                    ReturnedSickLeavecount = ReturnedSickLeavecount + 1;
                                }
                            }
                        }
                        if (ReturnedSickLeavecount > 0)
                        {
                            return UserMessageConstants.SickLeaveAlreadyBookedMessage;
                        }
                        //
                        else
                        {
                            var TopStartLeave = (from abs in DbContext.E_ABSENCE
                                                 join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                                 where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && abs.STARTDATE >= startDate && abs.ITEMSTATUSID != 20
                                                 && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                             where abs1.JOURNALID == j.JOURNALID
                                                                             select abs1.ABSENCEHISTORYID).ToList().Max()
                                                 select abs).OrderBy(x => x.STARTDATE).FirstOrDefault();

                            var TopEndLeave = (from abs in DbContext.E_ABSENCE
                                               join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                               where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && abs.RETURNDATE <= startDate && abs.ITEMSTATUSID != 20
                                               && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                           where abs1.JOURNALID == j.JOURNALID
                                                                           select abs1.ABSENCEHISTORYID).ToList().Max()
                                               select abs).OrderByDescending(x => x.RETURNDATE).FirstOrDefault();
                            if (TopStartLeave != null && TopEndLeave != null )
                            {
                                if (TopEndLeave.ABSENCEHISTORYID == TopStartLeave.ABSENCEHISTORYID)
                                {
                                    if (TopStartLeave.HOLTYPE.Equals("M") && (holType.Equals("A") || holType.Equals("A-F") || holType.Equals("A-M")))
                                        return "";
                                }
                            }
                            if (TopStartLeave != null)
                            {
                                DateTime MaxReturnDate = new DateTime();
                                string returnDayBookedSlot = "";
                                string startDayFreeSlot = "";
                                MaxReturnDate = TopStartLeave.STARTDATE.Value;
                                if (TopStartLeave.HOLTYPE.Equals("F") || TopStartLeave.HOLTYPE.Equals("F-F") 
                                    || TopStartLeave.HOLTYPE.Equals("F-M") || TopStartLeave.HOLTYPE.Equals("M"))
                                {
                                    returnDayBookedSlot = "F";
                                }
                                else if ((TopStartLeave.HOLTYPE.Equals("A") || TopStartLeave.HOLTYPE.Equals("A-F")
                                    || TopStartLeave.HOLTYPE.Equals("A-M")))
                                {
                                    var MorningSlotCount= (from abs in DbContext.E_ABSENCE
                                                           join j in DbContext.E_JOURNAL on abs.JOURNALID equals j.JOURNALID
                                                           where j.EMPLOYEEID == employeeId && j.ITEMNATUREID == 1 && abs.RETURNDATE == TopStartLeave.STARTDATE 
                                                           && (abs.HOLTYPE.Equals("M") || abs.HOLTYPE.Equals("A-M") || abs.HOLTYPE.Equals("F-M"))
                                                           && abs.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                                       where abs1.JOURNALID == j.JOURNALID
                                                                                       select abs1.ABSENCEHISTORYID).ToList().Max()
                                                           select abs).OrderBy(x => x.STARTDATE).Count();
                                    if (MorningSlotCount > 0)
                                    {
                                        returnDayBookedSlot = "F";
                                    }
                                    else
                                    {
                                        returnDayBookedSlot = "A";
                                    }
                                }
                                DateTime MinStartDate = new DateTime();

                                if (TopEndLeave != null)
                                {
                                    if (TopEndLeave.HOLTYPE.Equals("F") || TopEndLeave.HOLTYPE.Equals("F-F") 
                                        || TopEndLeave.HOLTYPE.Equals("A") || TopEndLeave.HOLTYPE.Equals("A-F"))                                    
                                    {
                                        MinStartDate = TopEndLeave.RETURNDATE.Value;
                                        startDayFreeSlot = "F";
                                    }
                                    else if (TopEndLeave.HOLTYPE.Equals("M") || TopEndLeave.HOLTYPE.Equals("F-M") || TopEndLeave.HOLTYPE.Equals("A-M"))
                                    {
                                        MinStartDate = TopEndLeave.RETURNDATE.Value;
                                        startDayFreeSlot = "A";
                                    }
                                }
                                else
                                {
                                    startDayFreeSlot = "F";
                                }
                                return string.Format(UserMessageConstants.EnterReturnDate, MaxReturnDate.ToShortDateString(), MinStartDate.ToShortDateString(), returnDayBookedSlot, startDayFreeSlot);

                            }
                            else
                            {
                                return "";
                            }
                        }

                    }
                }
            } 
        }
 
        private bool checkIfBRSAppointment(int employeeId, DateTime startDate, DateTime returnDate, float? duraiton, string holidayType)
        {
            bool isAVailable = true;

            var isBRS = (from emp in DbContext.E__EMPLOYEE
                         join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                         join hl in DbContext.E_HOLIDAYRULE on jd.HOLIDAYRULE equals hl.EID
                         where hl.HolidayRule == "BRS" && emp.EMPLOYEEID == employeeId
                         select emp).Any();

            if (!isBRS)
                return isAVailable;

            //      >>> AS_APPOINTMENTS <<<
            var data = (from apt in DbContext.AS_APPOINTMENTS
                        join aptj in DbContext.AS_JOURNAL on apt.JournalId equals aptj.JOURNALID
                        join apts in DbContext.AS_Status on aptj.STATUSID equals apts.StatusId
                        let appStartDate = EntityFunctions.TruncateTime(apt.APPOINTMENTDATE)
                        where (appStartDate >= startDate || appStartDate <= returnDate) &&
                               (apts.Title != "Cancelled" && apt.APPOINTMENTSTATUS != "Complete") &&
                               apt.ASSIGNEDTO == employeeId &&
                               aptj.ISCURRENT == true
                        select apt).Any();

            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }

            //      >>> FL_CO_APPOINTMENT <<<
            data = (
                from aptc in DbContext.FL_CO_APPOINTMENT
                join apt in DbContext.FL_FAULT_APPOINTMENT on aptc.AppointmentID equals apt.AppointmentId
                join aptj in DbContext.FL_FAULT_LOG on apt.FaultLogId equals aptj.FaultLogID
                join apts in DbContext.FL_FAULT_STATUS on aptj.StatusID equals apts.FaultStatusID
                let appStartDate = EntityFunctions.TruncateTime(aptc.AppointmentDate)
                where (appStartDate >= startDate && appStartDate <= returnDate) &&
                        (apts.Description != "Cancelled" && apts.Description != "Complete") &&
                        aptc.OperativeID == employeeId
                select apt.AppointmentId).Any();
            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }
            //.Concat(
            data = (from apt in DbContext.PDR_APPOINTMENTS
                    join aptj in DbContext.PDR_JOURNAL on apt.JOURNALID equals aptj.JOURNALID
                    join apts in DbContext.PDR_STATUS on aptj.STATUSID equals apts.STATUSID
                    let appStartDate = EntityFunctions.TruncateTime(apt.APPOINTMENTSTARTDATE)
                    let appEndDate = EntityFunctions.TruncateTime(apt.APPOINTMENTENDDATE)
                    where ((appStartDate >= startDate && appStartDate <= returnDate) || (appEndDate >= startDate && appEndDate <= returnDate)) &&
                            (apts.TITLE != "Cancelled" && apt.APPOINTMENTSTATUS != "Complete" && apt.APPOINTMENTSTATUS != "Cancelled") &&
                            apt.ASSIGNEDTO == employeeId
                    select apt.APPOINTMENTID).Any();
            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }
            //.Concat(
            data = (from apt in DbContext.PLANNED_APPOINTMENTS
                    join aptj in DbContext.PLANNED_JOURNAL on apt.JournalId equals aptj.JOURNALID
                    join apts in DbContext.PLANNED_STATUS on aptj.STATUSID equals apts.STATUSID
                    let appStartDate = EntityFunctions.TruncateTime(apt.APPOINTMENTDATE)
                    let appEndDate = EntityFunctions.TruncateTime(apt.APPOINTMENTENDDATE)
                    where ((appStartDate >= startDate && appStartDate <= returnDate) || (appEndDate >= startDate && appEndDate <= returnDate)) &&
                            (apts.TITLE != "Cancelled" && apt.APPOINTMENTSTATUS != "Complete" && apt.APPOINTMENTSTATUS != "Cancelled") &&
                            apt.ASSIGNEDTO == employeeId
                    select apt.APPOINTMENTID).Any();
            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }
            //.Concat(
            data = (from apt in DbContext.AS_APPOINTMENTS
                    join aptj in DbContext.AS_JOURNAL on apt.JournalId equals aptj.JOURNALID
                    join apts in DbContext.AS_Status on aptj.STATUSID equals apts.StatusId
                    let appStartDate = EntityFunctions.TruncateTime(apt.APPOINTMENTDATE)
                    where (appStartDate >= startDate && appStartDate <= returnDate) &&
                            (apts.Title != "Cancelled" && apt.APPOINTMENTSTATUS != "Complete") &&
                            apt.ASSIGNEDTO == employeeId &&
                            aptj.ISCURRENT == true
                    select apt.APPOINTMENTID).Any();

            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }

            return isAVailable;
        }

        private string getEmpHolidayIndicatorDescription(int employeeId)
        {
            return (from emp in DbContext.E__EMPLOYEE
                    join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                    join ehi in DbContext.E_HOLIDAYENTITLEMENT_INDICATOR on jd.HolidayIndicator equals ehi.Sid
                    where emp.EMPLOYEEID == employeeId
                    select ehi.Indicator).FirstOrDefault();
        }

        private int getAbsenceStatusIdByDescription(string description)
        {
            var statusId = DbContext.E_STATUS.Where(x => x.DESCRIPTION == description).FirstOrDefault().ITEMSTATUSID;

            return statusId;
        }

        private string getAbsenceStatusDescriptionById(int Id)
        {
            var statusDescription = DbContext.E_STATUS.Where(x => x.ITEMSTATUSID == Id).FirstOrDefault().DESCRIPTION;

            return statusDescription;
        }

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }

        public bool CheckPersonalBirthDayAvailed(int empID, string leaveType, DateTime date)
        {
            var personalLeaves = DbContext.E_JOURNAL.Include("E_ABSENCE").Where(x => x.EMPLOYEEID == empID
                    && x.ITEMNATUREID == (DbContext.E_NATURE.Where(a => a.DESCRIPTION == leaveType).Select(a => a.ITEMNATUREID).FirstOrDefault())
                    && (x.CURRENTITEMSTATUSID == (DbContext.E_ACTION.Where(a => a.DESCRIPTION == "Log").Select(a => a.ITEMACTIONID).FirstOrDefault()) ||
                        x.CURRENTITEMSTATUSID == (DbContext.E_ACTION.Where(a => a.DESCRIPTION == "Last Day of Absence").Select(a => a.ITEMACTIONID).FirstOrDefault()) ||
                        x.CURRENTITEMSTATUSID == (DbContext.E_ACTION.Where(a => a.DESCRIPTION == "Approve").Select(a => a.ITEMACTIONID).FirstOrDefault()) ||
                        x.CURRENTITEMSTATUSID == (DbContext.E_ACTION.Where(a => a.DESCRIPTION == "Apply").Select(a => a.ITEMACTIONID).FirstOrDefault()))).ToList();
            
            //var fYear = DbContext.EMPLOYEE_ANNUAL_START_END_DATE_ByYear(empID, date).FirstOrDefault();
            DateTime? startDate = new DateTime();
            DateTime? endDate = new DateTime();
            //if (fYear != null)
            //{
            //    startDate = fYear.STARTDATE;
            //    endDate = fYear.ENDDATE;
            //}

            // changes for leave can apply in any year(might be in future or in any yeat) 
            if (date.Month < 4)
            {
                startDate = new DateTime(date.Year - 1, 4, 1);
                endDate = new DateTime(date.Year, 3, 31);
            }
            else if (date.Month >= 4)
            {
                startDate = new DateTime(date.Year, 4, 1);
                endDate = new DateTime(date.Year + 1, 3, 31);
            }

           
            foreach (E_JOURNAL jnl in personalLeaves)
            {

                foreach (E_ABSENCE absenceData in jnl.E_ABSENCE.ToList())
                {
                    if (startDate <= absenceData.STARTDATE && absenceData.STARTDATE <= endDate)
                    {
                        return false;
                    }
                }
            }


            return true;


        }

        public string getAnnualLeaveEndDate(int empId)
        {
            string startDate = string.Empty;
            string endDate = string.Empty;
            var fYear = DbContext.EMPLOYEE_ANNUAL_START_END_DATE(empId).FirstOrDefault();

            if (fYear != null)
            {
                if (fYear.STARTDATE.HasValue)
                {
                    startDate = fYear.STARTDATE.Value.ToShortDateString();
                }
                if (fYear.ENDDATE.HasValue)
                {
                    endDate = fYear.ENDDATE.Value.ToShortDateString();
                }
            }

            return endDate;
        }

        public AnnualLeaveResponce getLeaveYearDate(int empId, int addYear)
        {
            AnnualLeaveResponce annualLeaveResponce = new AnnualLeaveResponce();
            var fYear = DbContext.EMPLOYEE_ANNUAL_START_END_DATE_ByYear(empId, DateTime.Now.AddYears(addYear)).FirstOrDefault();

            if (fYear != null)
            {
                if (fYear.STARTDATE.HasValue)
                {
                    annualLeaveResponce.leaveYearStart = fYear.STARTDATE.Value.ToShortDateString();
                }
                if (fYear.ENDDATE.HasValue)
                {
                    annualLeaveResponce.leaveYearEnd = fYear.ENDDATE.Value.ToShortDateString();
                }
            }

            return annualLeaveResponce;
        }

        public AnnualLeaveResponce getLeaveYearDate(int empId)
        {
            AnnualLeaveResponce annualLeaveResponce = new AnnualLeaveResponce();
            var fYear = DbContext.EMPLOYEE_ANNUAL_START_END_DATE_ByYear(empId, DateTime.Now).FirstOrDefault();

            if (fYear != null)
            {
                if (fYear.STARTDATE.HasValue)
                {
                    annualLeaveResponce.leaveYearStart = fYear.STARTDATE.Value.ToShortDateString();
                }
                if (fYear.ENDDATE.HasValue)
                {
                    annualLeaveResponce.leaveYearEnd = fYear.ENDDATE.Value.ToShortDateString();
                }
            }

            return annualLeaveResponce;
        }

    }
}
