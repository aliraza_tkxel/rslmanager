﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using Utilities;

namespace DataBaseModule.DataRepository.Employees
{
    public class EmployeesRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public EmployeesRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }

        #region Helpers

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }

        #endregion

        public PersonalDetailResponce GetEmployeeDetail(int employeeId)
        {

            PersonalDetailResponce respone = new PersonalDetailResponce();

            EmployeePersonalDetail detail = new EmployeePersonalDetail();
            bool isFiscalYearEntered = false;
            var fiscalYearEntry = getCurrentFiscalYear();
            var gift = (from g in DbContext.E_GiftsAndHospitalities
                        where g.fiscalYearId == fiscalYearEntry.YRange && g.EmployeeId == employeeId
                        select g).ToList();
            if(gift.Count > 0)
            {
                isFiscalYearEntered = true;
            }

            var data = (from e in DbContext.E__EMPLOYEE
                        join jd in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals jd.EMPLOYEEID into jd_join
                        from jd in jd_join.DefaultIfEmpty()
                        join ps in DbContext.E_PaypointSubmission on e.EMPLOYEEID equals ps.employeeId into ps_join
                        from ps in ps_join.DefaultIfEmpty().OrderByDescending(e => e.PaypointId)
                        join s in DbContext.E_PayPointStatus on ps.paypointStatusId equals s.PayPointStatusId into s_join
                        from s in s_join.DefaultIfEmpty()
                        join doi in DbContext.E_DOI on e.EMPLOYEEID equals doi.EmployeeId into doi_join
                        from doi in doi_join.Where(e => e.FiscalYear == fiscalYearEntry.YRange).DefaultIfEmpty()
                        join doiS in DbContext.E_DOI_Status on doi.Status equals doiS.DOIStatusId into doiS_join
                        from doiS in doiS_join.DefaultIfEmpty()
                        where e.EMPLOYEEID == employeeId
                        select new EmployeePersonalDetail()
                        {
                            employeeId = e.EMPLOYEEID,
                            aka = e.AKA,
                            _dbsDate = e.DBS_Date,
                            _dob = e.DOB,
                            ethnicity = e.ETHNICITY,
                            firstName = e.FIRSTNAME,
                            gender = e.GENDER,
                            lastName = e.LASTNAME,
                            maritalStatus = e.MARITALSTATUS,
                            niNumber = e.NiNumber,
                            middleName = e.MIDDLENAME,
                            reference = e.Reference,
                            religion = e.RELIGION,
                            sexualOrientation = e.SEXUALORIENTATION,
                            title = e.TITLE,
                            imagePath = e.IMAGEPATH,
                            profile = e.PROFILE,
                            isFiscalYearGiftEntered = isFiscalYearEntered,
                            paypointReviewDate = jd.PayPointReviewDate.ToString(),
                            statusDisplay = (s.Description == "Submitted") ? "Submitted by Manager" :
                                     (s.Description == ApplicationConstants.supported) ? "Supported by Director" :
                                     (s.Description == ApplicationConstants.authorized) ? "Authorised by Exec" :
                                     (s.Description == ApplicationConstants.declined && ps.SupportedBy != null && ps.AuthorizedBy == null) ? "Declined by Director" :
                                     (s.Description == ApplicationConstants.declined && ps.AuthorizedBy != null) ? "Declined by Exec" : s.Description,
                            actionDate = (ps.AuthorizedDate != null) ? ps.AuthorizedDate :
                                     (ps.SupportedDate != null) ? ps.SupportedDate :
                                     (ps.ApprovedDate != null) ? ps.ApprovedDate : jd.PayPointReviewDate,
                            doiCreationDate = doi.CreatedOn,
                            doiUpdationDate = doi.UpdatedOn,
                            doiReviewedDate = doi.LineManagerReviewDate,
                            doiStatusId = doiS.DOIStatusId,
                            doiStatusName = doiS.Description,
                        }).ToList();
            detail = data.FirstOrDefault();
            if (data != null && data.Count > 1)
            {
                detail = data.LastOrDefault();
            }
            if (detail != null)
            {
                respone.employeeDetail = detail;
                if (detail._dob.HasValue)
                {
                    respone.employeeDetail.dob = detail._dob.Value.ToShortDateString();
                }
                if (detail._dbsDate.HasValue)
                {
                    respone.employeeDetail.dbsDate = detail._dbsDate.Value.ToShortDateString();
                }
                var managerData = (from e in DbContext.E__EMPLOYEE
                                   join jd in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals jd.EMPLOYEEID
                                   where e.EMPLOYEEID == employeeId && jd.ACTIVE == 1
                                   select new { jd.ISDIRECTOR, jd.ISMANAGER }).FirstOrDefault();
                if (managerData != null)
                {
                    if (managerData.ISDIRECTOR == 1 || managerData.ISMANAGER == 1)
                        detail.isManager = true;
                }
                if (detail.imagePath != null && detail.imagePath.Length > 0)
                {
                    detail.imagePath = FileHelper.getLogicalEmployeeProfileImagePath(detail.imagePath);
                }
                //var doiResult = (from S in DbContext.E_DOI where (S.EmployeeId == employeeId && S.IsActive == false && S.IsActive == false && S.FiscalYear == fiscalYearEntry.YRange && S.DocumentId > 0) select S);
                //if (doiResult != null && doiResult.Count() > 0)
                //{
                //    respone.employeeDetail.isDoiCreated = true;
                //}
                detail.paypointReviewDate = detail.paypointReviewDate == "" ? "" : Convert.ToDateTime(detail.paypointReviewDate).ToString("dd/MM/yyyy");

                var empTrainingCount = (from EET in DbContext.E_EmployeeTrainings
                                        join ES in DbContext.E_EmployeeTrainingStatus on EET.Status equals ES.StatusId
                                        join E in DbContext.E__EMPLOYEE on EET.EmployeeId equals E.EMPLOYEEID
                                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                                        from T in T_join.DefaultIfEmpty()
                                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                                        from D in D_join.DefaultIfEmpty()
                                        where
                                           ((EET.ProfessionalQualification == null || EET.ProfessionalQualification == 0) && EET.TotalCost <= 1000)
                                            && EET.Active == true
                                            && J.ACTIVE == 1
                                            && (J.LINEMANAGER == employeeId)
                                            && (ES.Title == ApplicationConstants.grpTrainingStatusRequested) // ApplicationConstants.grpTrainingStatusSubmitted
                                        select EET);

                if (empTrainingCount != null)
                {
                    detail.trainingCount = empTrainingCount.Count();
                }
            }
           
           
            if (respone.employeeDetail == null)
            {
                respone.employeeDetail = new EmployeePersonalDetail();
            }
            return respone;
        }
        public ContactDetail GetEmployeeContactDetailForMobile(int employeeId)
        {
            ContactDetail responce = new ContactDetail();

            var data = (from e in DbContext.E_CONTACT
                        where e.EMPLOYEEID == employeeId
                        select new ContactDetail()
                        {
                            addressLine1 = e.ADDRESS1,
                            addressLine2 = e.ADDRESS2,
                            addressLine3 = e.ADDRESS3,
                            county = e.COUNTY,
                            postalTown = e.POSTALTOWN,
                            emergencyContactNumber = e.EMERGENCYCONTACTTEL,
                            emergencyContactPerson = e.EMERGENCYCONTACTNAME,
                            emergencyContactRelationship = e.EmergencyContactRelationship,
                            homeEmail = e.HOMEEMAIL,
                            homePhone = e.HOMETEL,
                            mobilePersonal = e.MobilePersonal,
                            mobile = e.MOBILE,
                            workEmail = e.WORKEMAIL,
                            workMobile = e.WORKMOBILE.TrimEnd(),
                            workPhone = e.WORKDD
                        }).FirstOrDefault();
            if (data != null)
            {
                responce = data;
            }

            return responce;
        }

        public EmployeeContactDetail GetEmployeeContactDetail(int employeeId)
        {
            EmployeeContactDetail responce = new EmployeeContactDetail();

            var data = (from e in DbContext.E_CONTACT
                        where e.EMPLOYEEID == employeeId
                        select new EmployeeContactDetail()
                        {
                            contactId = e.CONTACTID,
                            employeeId = e.EMPLOYEEID,
                            address1 = e.ADDRESS1,
                            address2 = e.ADDRESS2,
                            address3 = e.ADDRESS3,
                            city = e.POSTALTOWN,
                            county = e.COUNTY,
                            emergencyContact = e.EMERGENCYCONTACTNAME,
                            emergencyInfo = e.EMERGENCYINFO,
                            emergencyTel = e.EMERGENCYCONTACTTEL,
                            extension = e.WORKEXT,
                            homeEmail = e.HOMEEMAIL,
                            homePhone = e.HOMETEL,
                            mobile = e.MOBILE,
                            postalCode = e.POSTCODE,
                            workDD = e.WORKDD,
                            workEmail = e.WORKEMAIL,
                            workMobile = e.WORKMOBILE,
                            nextOfKin = e.NextOfKin,
                            mobilePersonal = e.MobilePersonal,
                            emergencyRelationShip = e.EmergencyContactRelationship
                        }).FirstOrDefault();
            if (data != null)
            {
                responce = data;
            }

            return responce;
        }



        public EmployeesResponse GetEmployees(EmployeesRequest model)
        {
            EmployeesResponse response = new EmployeesResponse();

            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        where
                        // (J.ACTIVE == model.status || J.ACTIVE == null) &&
                          (J.TEAM != 1 ||
                          J.TEAM == null) &&
                          ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                        select new Employee
                        {
                            name = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            employeeId = E.EMPLOYEEID,
                            jobTitle = (ET.JobeRoleDescription ?? "N/A"),
                            grade = (G.DESCRIPTION ?? "-"),
                            status = J.ACTIVE

                        });
            if (model.status == 0)
            {
                data = data.Where(J => J.status == null || J.status == 0);
            }
            else if (model.status == 1)
            {
                data = data.Where(J => J.status == 1);
            }

            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"name";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            // Making final response
            List<Employee> employees = new List<Employee>();
            foreach (var item in finalData)
            {
                Employee _employee = new Employee();
                _employee.employeeId = item.employeeId;
                _employee.name = item.name;
                _employee.team = item.team;
                _employee.jobTitle = item.jobTitle;
                _employee.grade = item.grade;
                employees.Add(_employee);
            }
            response.employees = employees;

            return response;
        }


        public EmployeePersonalDetail UpdateEmployeePersonalDetail(EmployeePersonalDetail detail)
        {
            var employee = (from e in DbContext.E__EMPLOYEE
                            where e.EMPLOYEEID == detail.employeeId
                            select e).FirstOrDefault();
            if (employee == null)
            {
                employee = new DatabaseEntities.E__EMPLOYEE();
                employee.CREATIONDATE = DateTime.Now;
                employee = DbContext.E__EMPLOYEE.Add(employee);
            }
            employee.FIRSTNAME = detail.firstName;
            employee.MIDDLENAME = detail.middleName;
            employee.LASTNAME = detail.lastName;
            employee.TITLE = detail.title;
            employee.AKA = detail.aka;
            employee.DOB = GeneralHelper.GetDateTimeFromString(detail.dob);
            employee.GENDER = detail.gender;
            employee.NiNumber = detail.niNumber;
            if (!string.IsNullOrEmpty(detail.dbsDate))
            {
                employee.DBS_Date = GeneralHelper.GetDateTimeFromString(detail.dbsDate);
            }
            employee.Reference = detail.reference;
            employee.MARITALSTATUS = detail.maritalStatus;
            employee.ETHNICITY = detail.ethnicity;
            employee.RELIGION = detail.religion;
            employee.SEXUALORIENTATION = detail.sexualOrientation;
            employee.PROFILE = detail.profile;
            employee.IMAGEPATH = detail.imagePath;
            employee.LASTACTIONTIME = DateTime.Now;
            DbContext.SaveChanges();
            detail.employeeId = employee.EMPLOYEEID;
            return detail;
        }


        public EmployeeContactDetail UpdateEmployeeContactDetail(EmployeeContactDetail detail)
        {
            var employee = (from e in DbContext.E_CONTACT
                            where e.EMPLOYEEID == detail.employeeId
                            select e).FirstOrDefault();
            if (employee == null)
            {
                employee = new DatabaseEntities.E_CONTACT();
                DbContext.E_CONTACT.Add(employee);
            }
            employee.EMPLOYEEID = detail.employeeId;
            employee.ADDRESS1 = detail.address1;
            employee.ADDRESS2 = detail.address2;
            employee.ADDRESS3 = detail.address3;
            employee.COUNTY = detail.county;
            employee.POSTALTOWN = detail.city;
            employee.POSTCODE = detail.postalCode;
            employee.HOMETEL = detail.homePhone;
            employee.HOMEEMAIL = detail.homeEmail;
            employee.MOBILE = detail.mobile;
            employee.MobilePersonal = detail.mobilePersonal;
            employee.WORKMOBILE = detail.workMobile;
            employee.WORKDD = detail.workDD;
            employee.WORKEXT = detail.extension;
            employee.WORKEMAIL = detail.workEmail;
            employee.EMERGENCYCONTACTNAME = detail.emergencyContact;
            employee.EMERGENCYCONTACTTEL = detail.emergencyTel;
            employee.EMERGENCYINFO = detail.emergencyInfo;
            employee.EmergencyContactRelationship = detail.emergencyRelationShip;
            employee.LASTACTIONTIME = DateTime.Now;
            employee.NextOfKin = detail.nextOfKin;
            DbContext.SaveChanges();
            detail.contactId = employee.CONTACTID;

            return detail;
        }

        public EmployeeImage UpdateEmployeeImage(EmployeeImage detail)
        {
            var employee = (from e in DbContext.E__EMPLOYEE
                            where e.EMPLOYEEID == detail.employeeId
                            select e).FirstOrDefault();
            if (employee != null)
            {
                if (!string.IsNullOrEmpty(detail.imageName))
                {
                    employee.IMAGEPATH = detail.imageName;
                }                
                employee.PROFILE = detail.profile;
                DbContext.SaveChanges();
            }
            return detail;
        }



        public EmployeesResponse GetEmployeesMainDetail(EmployeesRequest model)
        {
            EmployeesResponse response = new EmployeesResponse();
            var empHistory = (from Emp in DbContext.E__EMPLOYEE_HISTORY
                              select Emp.EMPLOYEEID).Distinct();

            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join H in empHistory on E.EMPLOYEEID equals H                       
                        // (J.ACTIVE == model.status || J.ACTIVE == null) &&
                           where (J.TEAM != 1 || J.TEAM == null) &&
                          ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                        select new Employee
                        {
                            name = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            employeeId = E.EMPLOYEEID,
                            jobTitle = (ET.JobeRoleDescription ?? "N/A"),
                            grade = (G.DESCRIPTION ?? "-"),
                            status = J.ACTIVE,
                            teamId = T.TEAMID

                        });
            //if (model.status == 0)
            //{
            //    data = data.Where(J => J.status == null || J.status == 0);
            //}
            //else if (model.status == 1)
            //{
            //    data = data.Where(J => J.status == 1);
            //}

            if (model.teamId > 0)
            {
                data = data.Where(J => J.teamId == model.teamId);
            }

            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"name";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            // Making final response
            List<Employee> employees = new List<Employee>();
            foreach (var item in finalData)
            {
             
                Employee _employee = new Employee();
                _employee.employeeId = item.employeeId;
                _employee.name = item.name;
                _employee.team = item.team;
                _employee.jobTitle = item.jobTitle;
                _employee.grade = item.grade;
                employees.Add(_employee);
            }
            response.employees = employees;

            return response;
        }

        public int GetDirectorateByEmpId(int employeeId)
        {
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        where
                         (E.EMPLOYEEID == employeeId)
                        select T.DIRECTORATEID).FirstOrDefault();

            if (data.HasValue)
            {
                return data.Value;
            }

            return 0;
        }

    }




}
