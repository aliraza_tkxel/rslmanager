﻿using DataBaseModule.DatabaseEntities;
using DataBaseModule.DataRepository;
using NetworkModel;
using NetworkModel.NetwrokModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataBaseModule.DataRepository
{
    public class HealthRepository : BaseRepository<E_DIFFDISID>
    {
        public HealthRepository(Entities dbContext) : base(dbContext)
        {

        }

        public List<DifficultiesAndDisablities> getDifficultiesAndDisablitiesList(int employeeId)//, int type
        {
            List<DifficultiesAndDisablities> response = new List<DifficultiesAndDisablities>();
            var data = (from d in DbContext.E_DIFFDISID
                        join e in DbContext.E__EMPLOYEE on d.EMPLOYEEID equals e.EMPLOYEEID
                        join t in DbContext.E_DIFFDIS on d.DIFFDIS equals t.DIFFDISID
                        join log in DbContext.E__EMPLOYEE on d.LASTACTIONUSER equals log.EMPLOYEEID
                        where d.EMPLOYEEID == employeeId 
                        // && t.DTYPE == type
                        select new DifficultiesAndDisablities
                        {
                            description = t.DESCRIPTION,
                            disablityId = d.DIFFDIS,
                            notes = d.NOTES,
                            notifiedDateString = d.NotifiedDate,
                            lastActionTime = d.LASTACTIONTIME,
                            firstName = log.FIRSTNAME,
                            lastName = log.LASTNAME,
                            diffDisabilityId=d.DISABILITYID,
                            loggedBy = d.LASTACTIONUSER,
                            isLongTerm=d.ISLONGTERM,
                            impactOnWork=d.IMPACTONWORK,
                            isOccupationalHealth = d.ISOCCUPATIONALHEALTH,
                            reviewDateString = d.REVIEWDATE,
                            documentTitle=d.DOCUMENTTITLE,
                            docPath = d.DOCUMENTPATH,
                            docName = d.DOCUMENTPATH,
                            isDelete =d.ISDELETE



                        });
            if (data.Count() > 0)
            {
                response = data.ToList();
                response= response.Where(item => item.isDelete == false || item.isDelete == null).ToList();
                foreach (DifficultiesAndDisablities item in response)
                {
                    DateTime logdate = (DateTime)item.lastActionTime;
                    if (item.notifiedDateString.HasValue)
                    {
                        DateTime notifiedDate = (DateTime)item.notifiedDateString;
                        item.notifiedDate = notifiedDate.ToString("dd/MM/yyyy");
                    }
                    if (item.reviewDateString.HasValue)
                    {
                        DateTime reviewDate = (DateTime)item.reviewDateString;
                        item.reviewDate = reviewDate.ToString("dd/MM/yyyy");
                    }
                    item.loggedByString = logdate.ToString("dd/MM/yyyy") + "( " + item.firstName[0] + " " + item.lastName + ")";

                    if (item.docPath != null && item.docPath.Length > 0)
                    {
                        item.docPath = FileHelper.getLogicalDisabilityHealthDocPath(item.docPath, employeeId);
                       
                    }

                }
            }
            return response;
        }

        public void AddAmendHealth(HealthRequest request)
        {
            HealthResponse response = new HealthResponse();
            E_DIFFDISID disablityObj = new E_DIFFDISID();
            string reviewDate=String.Empty;
            string dateNotified = String.Empty;
            if (request.diffDisabilityId > 0)
            {
                var disablityResult = (from e in DbContext.E_DIFFDISID where (e.DISABILITYID == request.diffDisabilityId) select e).FirstOrDefault();
                disablityObj = disablityResult;
            }
            else
            {
                disablityObj.EMPLOYEEID = request.employeeId;
            }
            if (!String.IsNullOrEmpty(request.notifiedDate))
            {
                dateNotified = DateTime.ParseExact(request.notifiedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                        .ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
            if (!String.IsNullOrEmpty(request.reviewDate))
            {
                reviewDate = DateTime.ParseExact(request.reviewDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                      .ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
            disablityObj.DIFFDIS = request.disablityId;
            disablityObj.LASTACTIONTIME = DateTime.Now;

            disablityObj.LASTACTIONUSER = request.loggedBy;
            disablityObj.NOTES = request.notes;

            if (!String.IsNullOrEmpty(dateNotified))
            {
                disablityObj.NotifiedDate = DateTime.ParseExact(dateNotified, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
            disablityObj.ISLONGTERM = request.isLongTerm;
            disablityObj.IMPACTONWORK = request.impactOnWork;
            disablityObj.ISOCCUPATIONALHEALTH = request.isOccupationalHealth;

            if (!String.IsNullOrEmpty(reviewDate))
            {
                disablityObj.REVIEWDATE = DateTime.ParseExact(reviewDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
            disablityObj.DOCUMENTTITLE = request.documentTitle;
            disablityObj.DOCUMENTPATH = request.docPath;
            if (request.diffDisabilityId == 0)
            {
                DbContext.E_DIFFDISID.Add(disablityObj);
            }
            DbContext.SaveChanges();

        }


        public void DeleteDisabilityHealth(HealthRequest request)
        {
            HealthResponse response = new HealthResponse();
            E_DIFFDISID disablityObj = new E_DIFFDISID();
            string reviewDate = String.Empty;
            if (request.diffDisabilityId > 0)
            {
                var disablityResult = (from e in DbContext.E_DIFFDISID where (e.DISABILITYID == request.diffDisabilityId) select e).FirstOrDefault();
                disablityObj = disablityResult;
            }
            disablityObj.DIFFDIS = request.disablityId;
            disablityObj.LASTACTIONTIME = DateTime.Now;
            disablityObj.ISDELETE = true;
            DbContext.SaveChanges();
        }
    }
}
