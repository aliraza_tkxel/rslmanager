﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository
{
    public class DisabilityAndDifficultyRepository: BaseRepository<E_DIFFDIS>
    {
        public DisabilityAndDifficultyRepository(Entities dbContext) : base(dbContext)
        {

        }
    }
}
