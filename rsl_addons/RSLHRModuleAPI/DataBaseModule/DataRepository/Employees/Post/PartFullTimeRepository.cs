﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Employees.PostTabRepositories
{
    public class PartFullTimeRepository : BaseRepository <E_PARTFULLTIME> 
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public PartFullTimeRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    }
}
