﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Employees.PostTabRepositories
{
    public class GradePointRepository : BaseRepository <E_GRADE_POINT>
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public GradePointRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    }
}
