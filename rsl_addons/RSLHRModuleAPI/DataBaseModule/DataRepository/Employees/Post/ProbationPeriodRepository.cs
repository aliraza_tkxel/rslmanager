﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Employees.PostTabRepositories
{
    public class ProbationPeriodRepository : BaseRepository<E_PROBATIONPERIOD>
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public ProbationPeriodRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    }
}
