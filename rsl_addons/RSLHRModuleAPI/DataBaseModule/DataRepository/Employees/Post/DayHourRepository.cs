﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Employees.PostTabRepositories
{
    public class DayHourRepository : BaseRepository<E_HOLIDAYENTITLEMENT_INDICATOR>
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public DayHourRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    }
}
