﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using Utilities;

namespace DataBaseModule.DataRepository.Employees.PostTabRepositories
{
    public class PostRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public PostRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }

        #region Helpers

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }

        private string GetDirectorNameByTeamId(int? teamId)
        {
            if (teamId.HasValue)
            {
                return (from t in DbContext.E_TEAM
                                  join e in DbContext.E__EMPLOYEE on t.DIRECTOR equals e.EMPLOYEEID
                                  where t.TEAMID == teamId.Value
                                  select e.FIRSTNAME + " " + e.LASTNAME).FirstOrDefault();
            }

            return "";
        }

        private void UpdateCoreHours(int employeeId, EmployeeWorkingHours EWH, List<DayTimingViewModel> DayTimings)
        {
            foreach (var item in DayTimings)
            {
                var queryableCoreHore = DbContext.E_CORE_WORKING_HOURS.Where(e => e.EMPLOYEEID == employeeId);
                switch (item.day)
                {
                    case "mon":
                        queryableCoreHore = queryableCoreHore.Where(e => e.DAYID == 1);
                        break;

                    case "tue":
                        queryableCoreHore = queryableCoreHore.Where(e => e.DAYID == 2);
                        break;

                    case "wed":
                        queryableCoreHore = queryableCoreHore.Where(e => e.DAYID == 3);
                        break;

                    case "thu":
                        queryableCoreHore = queryableCoreHore.Where(e => e.DAYID == 4);
                        break;

                    case "fri":
                        queryableCoreHore = queryableCoreHore.Where(e => e.DAYID == 5);
                        break;

                    case "sat":
                        queryableCoreHore = queryableCoreHore.Where(e => e.DAYID == 6);
                        break;

                    case "sun":
                        queryableCoreHore = queryableCoreHore.Where(e => e.DAYID == 7);
                        break;
                    default:
                        break;
                }

                var updCoreHour = queryableCoreHore.FirstOrDefault();
                if (updCoreHour != null)
                {
                    updCoreHour.STARTTIME = item.StartTime;
                    updCoreHour.ENDTIME = item.EndTime;
                    updCoreHour.CREATEDDATE = EWH.createdOn;
                    updCoreHour.CREATEDBY = EWH.createdBy;
                }
                else
                {
                    updCoreHour = new E_CORE_WORKING_HOURS();

                    switch (item.day)
                    {
                        case "mon":
                            updCoreHour.DAYID = 1;
                            break;

                        case "tue":
                            updCoreHour.DAYID = 2;
                            break;

                        case "wed":
                            updCoreHour.DAYID = 3;
                            break;

                        case "thu":
                            updCoreHour.DAYID = 4;
                            break;

                        case "fri":
                            updCoreHour.DAYID = 5;
                            break;

                        case "sat":
                            updCoreHour.DAYID = 6;
                            break;

                        case "sun":
                            updCoreHour.DAYID = 7;
                            break;
                        default:
                            break;
                    }

                    updCoreHour.STARTTIME = item.StartTime;
                    updCoreHour.ENDTIME = item.EndTime;
                    updCoreHour.CREATEDDATE = EWH.createdOn;
                    updCoreHour.CREATEDBY = EWH.createdBy;
                    updCoreHour.EMPLOYEEID = employeeId;

                    DbContext.E_CORE_WORKING_HOURS.Add(updCoreHour);
                }

                DbContext.SaveChanges();
            }
        }

        private void EntryInPayPoint(string payPointReviewDate, int employeeId)
        {
            //entry in paypoint as waiting submission
            var statusId = DbContext.E_PayPointStatus.Where(e => e.Description == ApplicationConstants.waitingSubmission).FirstOrDefault().PayPointStatusId;

            var fiscalYearId = getCurrentFiscalYear().YRange;
            DbContext.SaveChanges();

            var paypointSubmission = new E_PaypointSubmission();
            paypointSubmission.employeeId = employeeId;
            paypointSubmission.paypointStatusId = statusId;
            paypointSubmission.fiscalYearId = fiscalYearId;


            DbContext.E_PaypointSubmission.Add(paypointSubmission);
            DbContext.SaveChanges();

            var emailData = (from E in DbContext.E__EMPLOYEE
                             join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                             join LM in DbContext.E__EMPLOYEE on J.LINEMANAGER equals LM.EMPLOYEEID
                             join LMC in DbContext.E_CONTACT on LM.EMPLOYEEID equals LMC.EMPLOYEEID
                             join JRT in DbContext.E_JOBROLETEAM on E.JobRoleTeamId equals JRT.JobRoleTeamId
                             join T in DbContext.E_TEAM on E.JobRoleTeamId equals T.TEAMID
                             join D in DbContext.E__EMPLOYEE on T.DIRECTOR equals D.EMPLOYEEID
                             where J.ACTIVE == 1
                             select new PaypointEmailNotification()
                             {
                                 employeeName = E.FIRSTNAME + " " + E.LASTNAME,
                                 lineManagerName = LM.FIRSTNAME + " " + LM.LASTNAME,
                                 lineManagerEmail = LMC.WORKEMAIL,
                                 directorName = D.FIRSTNAME + " " + D.LASTNAME,
                                 nextPayPointReviewDate = J.PayPointReviewDate

                             }).FirstOrDefault();

            // send emailnotification
            if (emailData != null && !string.IsNullOrEmpty(emailData.lineManagerEmail))
            {
                string body = string.Empty;
                body = "Dear " + emailData.lineManagerName + ", <br /><br />";
                body = body + "<p>" + emailData.employeeName + " is eligible for a Pay Point Review on the " + payPointReviewDate + ".If you wish to submit an application to " + emailData.directorName + " please go to the employee record for " + emailData.employeeName + " and select the Pay Point in the Personal Details.A new screen will appear and you can then complete the required information.</ p > " +
                         "               <p>The application will be submitted to " + emailData.directorName + " and if they approve the application it will be considered by the Exec team. </p> " +
                         "				 <p>Further notifications will be issued on the progress of the submission.  </p> " +
                         "				 <p>Kind regards " +
                         "					<br /> " +
                         "					HR Team " +
                         "				 </p> ";
                EmailHelper.sendHtmlFormattedEmail(emailData.lineManagerName, emailData.lineManagerEmail, "Pay Point Application", body);
            }
        }

        #endregion

        #region Post Tab

        public EmployeeDocument UpdateEmployeeReferenceDocument(EmployeeDocument detail)
        {
            HR_Documents document = new HR_Documents();
            var documentData = (from h in DbContext.HR_Documents
                                where h.DocumentId == detail.documentId
                                select h);

            int referenceDocType = (from r in DbContext.HR_DocumentType
                                    where r.Description == "References"
                                    select r.TypeId).FirstOrDefault();

            string createdByName = (from e in DbContext.E__EMPLOYEE
                                    where e.EMPLOYEEID == detail.employeeId
                                    select e.FIRSTNAME + " " + e.LASTNAME).FirstOrDefault();

            if (documentData.Count() > 0)
            {
                document = documentData.FirstOrDefault();

            }
            document.DocumentTypeId = referenceDocType;
            document.EmployeeID = detail.employeeId;
            document.DocumentDate = DateTime.Now;
            document.IsActive = true;
            document.CreateDate = DateTime.Now;
            document.CreateBy = detail.createdBy;
            document.DocumentPath = detail.documentPath;
            if (documentData.Count() == 0)
            {
                DbContext.HR_Documents.Add(document);
            }
            DbContext.SaveChanges();
            detail.documentId = document.DocumentId;
            detail.createdByName = createdByName;

            return detail;
        }

        public List<EmployeeDocument> GetEmployeeReferenceDocuments(int employeeId)
        {


            List<EmployeeDocument> documents = new List<EmployeeDocument>();
            documents = (from h in DbContext.HR_Documents
                         join E in DbContext.E__EMPLOYEE on h.CreateBy equals E.EMPLOYEEID
                         where h.EmployeeID == employeeId
                         select new EmployeeDocument()
                         {
                             createdByName = E.FIRSTNAME + " " + E.LASTNAME,
                             createdBy = h.CreateBy,
                             documentId = h.DocumentId,
                             documetTypeId = h.DocumentTypeId,
                             employeeId = h.EmployeeID,
                             createdDate = h.CreateDate.ToString(),
                             documentPath = h.DocumentPath,
                         }).ToList();

            foreach (EmployeeDocument doc in documents)
            {
                doc.documentPath = FileHelper.getLogicalDocumentPath(doc.documentPath, employeeId);
                DateTime createdDate = Convert.ToDateTime(doc.createdDate);
                doc.createdDate = createdDate.ToString("dd/MM/yyyy");
            }

            return documents;
        }


        public List<LookUpResponseModel> GetLineManagers(int employeeId)
        {
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                        where J.ACTIVE == 1 && (J.ISMANAGER == 1 || J.ISDIRECTOR == 1) && J.EMPLOYEEID != employeeId
                        select new LookUpResponseModel()
                        {
                            lookUpId = E.EMPLOYEEID,
                            lookUpDescription = E.FIRSTNAME + " " + E.LASTNAME
                        }).ToList();



            return data;
        }
        public List<LookUpResponseModel> GetContractType()
        {
            var data = (from CT in DbContext.E_ContractType
                        select new LookUpResponseModel()
                        {
                            lookUpId = CT.ContractTypeId,
                            lookUpDescription = CT.ContractTypeDescription
                        }).ToList();



            return data;
        }


        public SalaryAmendment AddAmmendmentLetter(SalaryAmendment salaryAmendment)
        {
            var data = (from s in DbContext.E_SalaryAmendment
                        where s.SalaryAmendmentId == salaryAmendment.salaryAmendmentId
                        select s).FirstOrDefault();
            if (data == null)
            {
                E_SalaryAmendment salary = new E_SalaryAmendment();
                DbContext.E_SalaryAmendment.Add(salary);
                salary.EmployeeId = salaryAmendment.employeeId;
                salary.AmendmentFileName = salaryAmendment.fileName;

                salary.SalaryAmendmentFile = salaryAmendment.filePath;
                salaryAmendment.salaryAmendmentId = salary.SalaryAmendmentId;
                salaryAmendment.filePath = FileHelper.getLogicalSalaryAmendmenttPath(salaryAmendment.filePath);
                DbContext.SaveChanges();
                return salaryAmendment;
            }
            return null;
        }

        public bool RemoveAmmendmentLetter(int salaryAmendmentId)
        {
            var data = (from s in DbContext.E_SalaryAmendment
                        where s.SalaryAmendmentId == salaryAmendmentId
                        select s).FirstOrDefault();
            if (data != null)
            {
                DbContext.E_SalaryAmendment.Remove(data);
                DbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public JobPostDetail GetJobDetail(int employeeId)
        {
            // this code remove because bank holiday now calculate based on leave year instead of fiscal year.

            //var fData = getCurrentFiscalYear();

            var fData = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(employeeId)
                           select new AbsenceAnnual()
                           {
                               endDate = absence.ENDDATE,
                               startDate = absence.STARTDATE
                           }).FirstOrDefault();
            if (fData == null)
            {
                fData = new AbsenceAnnual();
            }

            double bankHolidays = DbContext.G_BANKHOLIDAYS.Where(x => x.BHDATE >= fData.startDate && x.BHDATE <= fData.endDate && (x.BHA == 1 || x.MeridianEast == 1)).Count();
            var data = DbContext.E_JOBDETAILS.Where(x=>x.EMPLOYEEID == employeeId).Include(x=>x.E_LEAVINGREASONS).FirstOrDefault();

            var salaryAmendmentData = (from s in DbContext.E_SalaryAmendment
                                       where s.EmployeeId == employeeId
                                       select new SalaryAmendment()
                                       {
                                           employeeId = s.EmployeeId,
                                           salaryAmendmentId = s.SalaryAmendmentId,
                                           fileName = s.AmendmentFileName,
                                           filePath = s.SalaryAmendmentFile
                                       }).ToList();
            foreach (SalaryAmendment amendment in salaryAmendmentData)
            {
                amendment.filePath = FileHelper.getLogicalSalaryAmendmenttPath(amendment.fileName);
            }


            var roleFile = (from e in DbContext.E__EMPLOYEE
                            where e.EMPLOYEEID == employeeId
                            select e.ROLEPATH).FirstOrDefault();
            var employeeContract = (from e in DbContext.E__EMPLOYEE
                                    where e.EMPLOYEEID == employeeId
                                    select e.EmploymentContracts).FirstOrDefault();

            JobPostDetail _detail = new JobPostDetail();
            _detail.employeeId = employeeId;



            if (data != null)
            {
                var resposedata = new JobPostDetail();
                resposedata.active = data.ACTIVE;
                resposedata.salary = data.SALARY;

                // the value swap because "bankHoliday" is dependant on entire system 

                

                resposedata.bankHoliday = bankHolidays;
                resposedata.autoHolidayAdjustment = data.BANKHOLIDAY;
                resposedata.carryForward = data.CARRYFORWARD;
                resposedata.detailsOfRole = data.DETAILSOFROLE;
                resposedata.employeeId = data.EMPLOYEEID;
                resposedata.employeeLimit = data.EMPLOYEELIMIT;
                resposedata.leavingReason = data.E_LEAVINGREASONS.Description == ApplicationConstants.leavingReasonInvoluntary ? 0 : 1;
                resposedata.foreignNationalNumber = data.FOREIGNNATIONALNUMBER;
                resposedata.grade = data.GRADE;
                resposedata.gradePoint = data.GRADEPOINT;
                resposedata.holidayRule = data.HOLIDAYRULE;
                resposedata.holidayEntitlementDays = data.HOLIDAYENTITLEMENTDAYS;
                resposedata.holidayEntitlementHours = data.HOLIDAYENTITLEMENTHOURS;
                resposedata.hours = data.HOURS;
                resposedata.isDirector = data.ISDIRECTOR;
                resposedata.isManager = data.ISMANAGER;
                resposedata.jobDetailsId = data.JOBDETAILSID;
                resposedata.jobRoleId = data.JobRoleId;
                resposedata.lastActionUser = data.LASTACTIONUSER;
                resposedata.lineManager = data.LINEMANAGER;
                resposedata.noticePeriod = data.NOTICEPERIOD;
                resposedata.officeLocation = data.PlaceOfWork;
                resposedata.partFullTime = data.PARTFULLTIME;
                resposedata.payrollNumber = data.PAYROLLNUMBER;
                resposedata.probationPeriod = data.PROBATIONPERIOD;
                resposedata.taxCode = data.TAXCODE;
                resposedata.taxOffice = data.TAXOFFICE;
                resposedata.team = data.TEAM;
                resposedata.totalLeave = data.TOTALLEAVE;
                resposedata.holidayIndicator = data.HolidayIndicator;
                resposedata.fte = data.FTE;
                
                resposedata.saleryAmendments = salaryAmendmentData;
                resposedata.roleFile = FileHelper.getLogicalRoleFilePath(roleFile);
                resposedata.employeeContract = FileHelper.getLogicalEmploymentContractPath(employeeContract);
                resposedata.teamDirectorName = GetDirectorNameByTeamId(data.TEAM);
                resposedata.fixedTermContract = data.FixedTermContract;
                resposedata.maxHolidayEntitlementDays =data.MAXHOLIDAYENTITLEMENTDAYS;
                resposedata.maxHolidayEntitlementHours =data.MAXHOLIDAYENTITLEMENTHOURS;
                resposedata.carryForwardHours =data.CARRYFORWARDHOURS;
                resposedata.totalLeaveHours =data.TOTALLEAVEHOURS;
                resposedata.ISBRS =data.ISBRS;
                resposedata.contractType = data.ContractType;
                resposedata.noteCount = DbContext.E_JobDetailNote.Where(e => e.EMPLOYEEID == data.EMPLOYEEID).Count();

                if (data.ALSTARTDATE.HasValue)
                {
                    resposedata.alStartDate = data.ALSTARTDATE.Value.ToShortDateString();
                }

                if (data.DATEOFNOTICE.HasValue)
                {
                    resposedata.dateOfNotice = data.DATEOFNOTICE.Value.ToShortDateString();
                }

                if (data.ENDDATE.HasValue)
                {
                    resposedata.endDate = data.ENDDATE.Value.ToShortDateString();
                }
                if (data.LASTACTIONTIME.HasValue)
                {
                    resposedata.lastActiontime = data.LASTACTIONTIME.Value.ToShortDateString();
                }
                if (data.REVIEWDATE.HasValue)
                {
                    resposedata.reviewDate = data.REVIEWDATE.Value.ToShortDateString();
                }
                if (data.STARTDATE.HasValue)
                {
                    resposedata.startDate = data.STARTDATE.Value.ToShortDateString();
                }
                if (data.PayPointReviewDate.HasValue)
                {
                    resposedata.payPointReviewDate = data.PayPointReviewDate.Value.ToShortDateString();
                }
                if (data.TupeIdDate.HasValue)
                {
                    resposedata.tupeInDate = data.TupeIdDate.Value.ToShortDateString();
                }
                if (data.TupeOutDate.HasValue)
                {
                    resposedata.tupeOutDate = data.TupeOutDate.Value.ToShortDateString();
                }
                
                return resposedata;
            }

            return _detail;
        }

        public bool UpdateEmployeeContract(UpdateEmployeeContractRoleRequest request)
        {
            var data = (from e in DbContext.E__EMPLOYEE
                        where e.EMPLOYEEID == request.employeeId
                        select e).FirstOrDefault();
            if (data != null)
            {
                data.EmploymentContracts = request.filePath;
                data.ROLEPATH = request.rolePath;
                data.LASTACTIONUSER = request.updatedBy;
                data.LASTACTIONTIME = DateTime.Now;
            }

            DbContext.SaveChanges();
            return true;
        }




        public JobPostDetail AmendJobDetail(JobPostDetail detail)
        {
            var data = (from d in DbContext.E_JOBDETAILS
                        where d.EMPLOYEEID == detail.employeeId
                        select d).FirstOrDefault();
            var isPayPointNotificationSend = false;
            bool HistoryCheck = true;

            bool UnitCheck = false;
            string CurrentUnit = "";
            string UpdatedUnit = "";
            DatabaseEntities.E_JOBDETAILS jobdetail;
            if (data == null)
            {
                jobdetail = new E_JOBDETAILS();
                DbContext.E_JOBDETAILS.Add(jobdetail);

            }
            else
            {
                jobdetail = data;
            }

            //Checking whether Job Role was updated or not.
            if (data != null)
            {
                if (data.TEAM == detail.team && data.JobRoleId == detail.jobRoleId)
                {
                    HistoryCheck = false;
                }
                if (jobdetail.HolidayIndicator != detail.holidayIndicator)
                {
                    UnitCheck = true;
                    CurrentUnit = jobdetail.HolidayIndicator == null || jobdetail.HolidayIndicator == 1 ? "Days" : "Hours";
                    UpdatedUnit = detail.holidayIndicator == null || detail.holidayIndicator == 1 ? "Days" : "Hours";
                }
            }
                        
            jobdetail.ACTIVE = detail.active;
            jobdetail.SALARY = detail.salary;

            // the value swap because "bankHoliday" is dependant on entire system for leave calculation
            jobdetail.BANKHOLIDAY = detail.autoHolidayAdjustment;
            jobdetail.CARRYFORWARD = detail.carryForward;
            jobdetail.DETAILSOFROLE = detail.detailsOfRole;
            jobdetail.EMPLOYEEID = detail.employeeId;
            jobdetail.EMPLOYEELIMIT = detail.employeeLimit;
            jobdetail.LeavingReason = detail.leavingReason == 0 ? DbContext.E_LEAVINGREASONS.Where(x => x.Description.Equals(ApplicationConstants.leavingReasonInvoluntary)).Select(x => x.LeavingReasonId).FirstOrDefault() :
                                            DbContext.E_LEAVINGREASONS.Where(x => x.Description.Equals(ApplicationConstants.leavingReasonVolunatry)).Select(x => x.LeavingReasonId).FirstOrDefault();
            jobdetail.FOREIGNNATIONALNUMBER = detail.foreignNationalNumber;
            jobdetail.GRADE = detail.grade;
            jobdetail.GRADEPOINT = detail.gradePoint;
            jobdetail.HOLIDAYRULE = detail.holidayRule;
            jobdetail.HOLIDAYENTITLEMENTDAYS = detail.holidayEntitlementDays;
            jobdetail.HOLIDAYENTITLEMENTHOURS = detail.holidayEntitlementHours;
            jobdetail.HOURS = detail.hours;
            jobdetail.ISMANAGER = detail.isManager;
            jobdetail.ISDIRECTOR = detail.isDirector;
            //jobdetail.JOBDETAILSID = detail.jobDetailsId;
            jobdetail.JobRoleId = detail.jobRoleId;
            jobdetail.LASTACTIONTIME = DateTime.Now;
            jobdetail.LASTACTIONUSER = detail.lastActionUser;
            jobdetail.LINEMANAGER = detail.lineManager;
            jobdetail.PlaceOfWork = detail.officeLocation == 0 ? null : detail.officeLocation;
            jobdetail.NOTICEPERIOD = detail.noticePeriod;
            jobdetail.PARTFULLTIME = detail.partFullTime;
            jobdetail.PAYROLLNUMBER = detail.payrollNumber;
            jobdetail.PROBATIONPERIOD = detail.probationPeriod;
            jobdetail.TAXCODE = detail.taxCode;
            jobdetail.TAXOFFICE = detail.taxOffice;
            jobdetail.TEAM = detail.team;
            jobdetail.HolidayIndicator = detail.holidayIndicator;
            jobdetail.FTE = detail.fte;
            jobdetail.TOTALLEAVE = detail.totalLeave;
            jobdetail.FixedTermContract = detail.fixedTermContract;
            jobdetail.CARRYFORWARDHOURS = detail.carryForwardHours;
            jobdetail.TOTALLEAVEHOURS = detail.totalLeaveHours;
            jobdetail.MAXHOLIDAYENTITLEMENTDAYS = detail.maxHolidayEntitlementDays;
            jobdetail.MAXHOLIDAYENTITLEMENTHOURS = detail.maxHolidayEntitlementHours;
            jobdetail.ISBRS = detail.ISBRS;
            jobdetail.ContractType = detail.contractType;

            if (!string.IsNullOrEmpty(detail.alStartDate))
            {
                jobdetail.ALSTARTDATE = GeneralHelper.GetDateTimeFromString(detail.alStartDate);
            }
            else
            {
                jobdetail.ALSTARTDATE = null;
            }

            if (!string.IsNullOrEmpty(detail.endDate))
            {
                jobdetail.ENDDATE = GeneralHelper.GetDateTimeFromString(detail.endDate);
            }
            else
            {
                jobdetail.ENDDATE = null;
            }

            if (!string.IsNullOrEmpty(detail.reviewDate))
            {
                jobdetail.REVIEWDATE = GeneralHelper.GetDateTimeFromString(detail.reviewDate);
            }
            else
            {
                jobdetail.REVIEWDATE = null;
            }

            if (!string.IsNullOrEmpty(detail.startDate))
            {
                jobdetail.STARTDATE = GeneralHelper.GetDateTimeFromString(detail.startDate);
            }
            else
            {
                jobdetail.STARTDATE = null;
            }

            if (!string.IsNullOrEmpty(detail.tupeInDate))
            {
                jobdetail.TupeIdDate = GeneralHelper.GetDateTimeFromString(detail.tupeInDate);
            }
            else
            {
                jobdetail.TupeIdDate = null;
            }

            if (!string.IsNullOrEmpty(detail.tupeOutDate))
            {
                jobdetail.TupeOutDate = GeneralHelper.GetDateTimeFromString(detail.tupeOutDate);
            }
            else
            {
                jobdetail.TupeOutDate = null;
            }

            if (!string.IsNullOrEmpty(detail.payPointReviewDate))
            {
                var tempdate = GeneralHelper.GetDateTimeFromString(detail.payPointReviewDate);
                if (jobdetail.PayPointReviewDate != tempdate)
                {
                    isPayPointNotificationSend = true;
                }

                jobdetail.PayPointReviewDate = tempdate;
            }
            else
            {
                jobdetail.PayPointReviewDate = null;
            }

            if (!string.IsNullOrEmpty(detail.dateOfNotice))
            {
                jobdetail.DATEOFNOTICE = GeneralHelper.GetDateTimeFromString(detail.dateOfNotice);
            }
            else
            {
                jobdetail.DATEOFNOTICE = null;
            }

            DbContext.SaveChanges();
            detail.jobDetailsId = jobdetail.JOBDETAILSID;
            //Updating E_CARRYFORWARDHISTORY
            var Absence = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(detail.employeeId)
                           select new AbsenceAnnual()
                           {
                               startDate = absence.STARTDATE,
                               endDate=absence.ENDDATE
                           }).FirstOrDefault();

            var CFHData = (from cfh in DbContext.E_CARRYFORWARDHISTORY
                        where cfh.EMPLOYEEID == detail.employeeId && cfh.LEAVEYEARSTARTDATE== Absence.startDate && cfh.LEAVEYEARENDDATE== Absence.endDate
                           select cfh).FirstOrDefault();
            E_CARRYFORWARDHISTORY carryForwardHistory;
            if (CFHData == null)
            {
                carryForwardHistory = new E_CARRYFORWARDHISTORY();
                DbContext.E_CARRYFORWARDHISTORY.Add(carryForwardHistory);
                carryForwardHistory.EMPLOYEEID = detail.employeeId;
                carryForwardHistory.LEAVEYEARSTARTDATE = Absence.startDate;
                carryForwardHistory.LEAVEYEARENDDATE = Absence.endDate;
                carryForwardHistory.CREATEDBY = detail.lastActionUser;
                carryForwardHistory.CREATIONDATE = DateTime.Now;
            }
            else
            {
                carryForwardHistory = CFHData;                
            }
            carryForwardHistory.LASTACTIONTIME = DateTime.Now;
            carryForwardHistory.LASTACTIONUSER = detail.lastActionUser;
            carryForwardHistory.CARRYFORWARD = (detail.carryForward ?? 0);
            carryForwardHistory.CARRYFORWARDHOURS = (detail.carryForwardHours ?? 0);
            DbContext.SaveChanges();


            // updating history

            if(detail.jobRoleId != null && detail.team != null && HistoryCheck== true)
            {
                var jobRoleTeamId = DbContext.E_JOBROLETEAM.Where(x => x.JobRoleId == detail.jobRoleId && x.TeamId == detail.team).FirstOrDefault().JobRoleTeamId;
                if(jobRoleTeamId > 0)
                {
                    var employee = DbContext.E__EMPLOYEE.Where(c => c.EMPLOYEEID == detail.employeeId).FirstOrDefault();
                    if (employee != null)
                    {
                        employee.JobRoleTeamId = jobRoleTeamId;
                        employee.LASTACTIONUSER = detail.lastActionUser;
                        employee.LASTACTIONTIME = DateTime.Now;
                        DbContext.SaveChanges();
                    }

                    var jobHistory = new E_EMPLOYEEJOBROLEHISTORY();
                    jobHistory.JobRoleTeamId = jobRoleTeamId;
                    jobHistory.EmployeeId = detail.employeeId;
                    jobHistory.CreatedDate = DateTime.Now;
                    jobHistory.CreatedBy = detail.lastActionUser;
                    DbContext.E_EMPLOYEEJOBROLEHISTORY.Add(jobHistory);
                    DbContext.SaveChanges();
                }
            }
            //DatabaseEntities.E_JOBROLETEAM jobRoleTeamobj = new E_JOBROLETEAM();
            //jobRoleTeamobj.JobRoleId = detail.jobRoleId;
            //jobRoleTeamobj.TeamId = detail.team;
            //jobRoleTeamobj.IsActive = true;
            //DbContext.E_JOBROLETEAM.Add(jobRoleTeamobj);
            //DbContext.SaveChanges();

            //DatabaseEntities.E_EMPLOYEEJOBROLEHISTORY jobHistory = new E_EMPLOYEEJOBROLEHISTORY();
            //jobHistory.JobRoleTeamId = jobRoleTeamobj.JobRoleTeamId;
            //jobHistory.EmployeeId = detail.employeeId;
            //jobHistory.CreatedDate = DateTime.Now;
            //jobHistory.CreatedBy = detail.lastActionUser;
            //DbContext.E_EMPLOYEEJOBROLEHISTORY.Add(jobHistory);
            //DbContext.SaveChanges();

            if (isPayPointNotificationSend)
            {
                var allowedStatus = DbContext.E_PayPointStatus.Where(e => e.Description == ApplicationConstants.submitted ||
                                       e.Description == ApplicationConstants.supported ||
                                       e.Description == ApplicationConstants.waitingSubmission).Select(e => e.PayPointStatusId).ToList();

                var isAlreadySubmitted = DbContext.E_PaypointSubmission.Where(e => e.employeeId == detail.employeeId).OrderByDescending(e => e.PaypointId).ToList();
                if (isAlreadySubmitted != null && isAlreadySubmitted.Count() > 0)
                {
                    if (allowedStatus.Where(e => e == isAlreadySubmitted.FirstOrDefault().paypointStatusId).Count() == 0)
                    {
                        EntryInPayPoint(jobdetail.PayPointReviewDate.Value.ToShortDateString(), detail.employeeId);
                    }
                    else
                    {
                        // if already exist in state "Waiting Submission, Supported, Submitted" do we update E_PaypointSubmission record?
                        // if yes do here  
                    }
                }
                else
                {
                    EntryInPayPoint(jobdetail.PayPointReviewDate.Value.ToShortDateString(), detail.employeeId);
                }
            }

            if(UnitCheck==true)
            {
                CancelLeaves(detail.employeeId, detail.lastActionUser);
                UpdateLeaves(detail.employeeId, CurrentUnit, UpdatedUnit);
            }
            return detail;
        }

        public void UpdateLeaves(int employeeId,string CurrentUnit, string UpdatedUnit)
        {
            var ApprovedLeaves = (from AL in DbContext.E_ApprovedLeaves(employeeId) select AL).ToList();
            double Days_ABS = 0.0;
            double Hrs_ABS = 0.0;
            foreach (var AL in ApprovedLeaves)
            {
                EmployeeWorkingHours WH = new EmployeeWorkingHours();
                WH = (from wh in DbContext.E_WorkingHours_History
                      where wh.EmployeeId == employeeId && AL.STARTDATE >= wh.StartDate && (AL.STARTDATE <= wh.EndDate || wh.EndDate == null)
                      select new EmployeeWorkingHours()
                      {
                          mon = wh.Mon,
                          tue = wh.Tue,
                          wed = wh.Wed,
                          thu = wh.Thu,
                          fri = wh.Fri,
                          sat = wh.Sat,
                          sun = wh.Sun
                      }).FirstOrDefault();
                if (WH != null)
                {
                    if (AL.STARTDATE.Value == AL.RETURNDATE.Value)
                    {
                        switch (AL.STARTDATE.Value.ToString("dddd"))
                        {
                            case "Monday":
                                if (WH.mon > 0)
                                {
                                    if (AL.HolType.Equals("A") || AL.HolType.Equals("M"))
                                    {
                                        Days_ABS = 0.5; Hrs_ABS = WH.mon.Value / 2;
                                    }
                                    if (AL.HolType.Equals("F"))
                                    {
                                        Days_ABS = 1.0; Hrs_ABS = WH.mon.Value;
                                    }
                                }
                                break;

                            case "Tuesday":
                                if (WH.tue > 0)
                                {
                                    if (AL.HolType.Equals("A") || AL.HolType.Equals("M"))
                                    {
                                        Days_ABS = 0.5; Hrs_ABS = WH.tue.Value / 2;
                                    }
                                    if (AL.HolType.Equals("F"))
                                    {
                                        Days_ABS = 1.0; Hrs_ABS = WH.tue.Value;
                                    }
                                }
                                break;

                            case "Wednesday":
                                if (WH.wed > 0)
                                {
                                    if (AL.HolType.Equals("A") || AL.HolType.Equals("M"))
                                    {
                                        Days_ABS = 0.5; Hrs_ABS = WH.wed.Value / 2;
                                    }
                                    if (AL.HolType.Equals("F"))
                                    {
                                        Days_ABS = 1.0; Hrs_ABS = WH.wed.Value;
                                    }
                                }
                                break;

                            case "Thursday":
                                if (WH.thu > 0)
                                {
                                    if (AL.HolType.Equals("A") || AL.HolType.Equals("M"))
                                    {
                                        Days_ABS = 0.5; Hrs_ABS = WH.thu.Value / 2;
                                    }
                                    if (AL.HolType.Equals("F"))
                                    {
                                        Days_ABS = 1.0; Hrs_ABS = WH.thu.Value;
                                    }
                                }
                                break;

                            case "Friday":
                                if (WH.fri > 0)
                                {
                                    if (AL.HolType.Equals("A") || AL.HolType.Equals("M"))
                                    {
                                        Days_ABS = 0.5; Hrs_ABS = WH.fri.Value / 2;
                                    }
                                    if (AL.HolType.Equals("F"))
                                    {
                                        Days_ABS = 1.0; Hrs_ABS = WH.fri.Value;
                                    }
                                }
                                break;

                            case "Saturday":
                                if (WH.sat > 0)
                                {
                                    if (AL.HolType.Equals("A") || AL.HolType.Equals("M"))
                                    {
                                        Days_ABS = 0.5; Hrs_ABS = WH.sat.Value / 2;
                                    }
                                    if (AL.HolType.Equals("F"))
                                    {
                                        Days_ABS = 1.0; Hrs_ABS = WH.sat.Value;
                                    }
                                }
                                break;

                            case "Sunday":
                                if (WH.sun > 0)
                                {
                                    if (AL.HolType.Equals("A") || AL.HolType.Equals("M"))
                                    {
                                        Days_ABS = 0.5; Hrs_ABS = WH.sun.Value / 2;
                                    }
                                    if (AL.HolType.Equals("F"))
                                    {
                                        Days_ABS = 1.0; Hrs_ABS = WH.sun.Value;
                                    }
                                }
                                break;

                            default:
                                break;
                        }
                    }
                    else
                    {
                        for (DateTime d = AL.STARTDATE.Value; d <= AL.RETURNDATE.Value; d = d.AddDays(1))
                        {
                            int bankHoliday = DbContext.G_BANKHOLIDAYS.Where(x => x.BHDATE == d).Count();
                            if (bankHoliday == 0)
                            {
                                switch (d.ToString("dddd"))
                                {
                                    case "Monday":
                                        if (WH.mon > 0)
                                        {
                                            if (AL.HolType.Equals("F-M"))
                                            {
                                                if (d == AL.RETURNDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.mon.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.mon.Value;
                                                }

                                            }
                                            if (AL.HolType.Equals("A-F"))
                                            {
                                                if (d == AL.STARTDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.mon.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.mon.Value;
                                                }
                                            }
                                            if (AL.HolType.Equals("F-F"))
                                            {
                                                Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.mon.Value;
                                            }
                                        }
                                        break;

                                    case "Tuesday":
                                        if (WH.tue > 0)
                                        {
                                            if (AL.HolType.Equals("F-M"))
                                            {
                                                if (d == AL.RETURNDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.tue.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.tue.Value;
                                                }

                                            }
                                            if (AL.HolType.Equals("A-F"))
                                            {
                                                if (d == AL.STARTDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.tue.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.tue.Value;
                                                }
                                            }
                                            if (AL.HolType.Equals("F-F"))
                                            {
                                                Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.tue.Value;
                                            }
                                        }
                                        break;

                                    case "Wednesday":
                                        if (WH.wed > 0)
                                        {
                                            if (AL.HolType.Equals("F-M"))
                                            {
                                                if (d == AL.RETURNDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.wed.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.wed.Value;
                                                }

                                            }
                                            if (AL.HolType.Equals("A-F"))
                                            {
                                                if (d == AL.STARTDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.wed.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.wed.Value;
                                                }
                                            }
                                            if (AL.HolType.Equals("F-F"))
                                            {
                                                Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.wed.Value;
                                            }
                                        }
                                        break;

                                    case "Thursday":
                                        if (WH.thu > 0)
                                        {
                                            if (AL.HolType.Equals("F-M"))
                                            {
                                                if (d == AL.RETURNDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.thu.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.thu.Value;
                                                }

                                            }
                                            if (AL.HolType.Equals("A-F"))
                                            {
                                                if (d == AL.STARTDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.thu.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.thu.Value;
                                                }
                                            }
                                            if (AL.HolType.Equals("F-F"))
                                            {
                                                Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.thu.Value;
                                            }
                                        }
                                        break;

                                    case "Friday":
                                        if (WH.fri > 0)
                                        {
                                            if (AL.HolType.Equals("F-M"))
                                            {
                                                if (d == AL.RETURNDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.fri.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.fri.Value;
                                                }

                                            }
                                            if (AL.HolType.Equals("A-F"))
                                            {
                                                if (d == AL.STARTDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.fri.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.fri.Value;
                                                }
                                            }
                                            if (AL.HolType.Equals("F-F"))
                                            {
                                                Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.fri.Value;
                                            }
                                        }
                                        break;

                                    case "Saturday":
                                        if (WH.sat > 0)
                                        {
                                            if (AL.HolType.Equals("F-M"))
                                            {
                                                if (d == AL.RETURNDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.sat.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.sat.Value;
                                                }

                                            }
                                            if (AL.HolType.Equals("A-F"))
                                            {
                                                if (d == AL.STARTDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.sat.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.sat.Value;
                                                }
                                            }
                                            if (AL.HolType.Equals("F-F"))
                                            {
                                                Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.sat.Value;
                                            }
                                        }
                                        break;

                                    case "Sunday":
                                        if (WH.sun > 0)
                                        {
                                            if (AL.HolType.Equals("F-M"))
                                            {
                                                if (d == AL.RETURNDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.sun.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.sun.Value;
                                                }

                                            }
                                            if (AL.HolType.Equals("A-F"))
                                            {
                                                if (d == AL.STARTDATE.Value)
                                                {
                                                    Days_ABS = Days_ABS + 0.5; Hrs_ABS = Hrs_ABS + WH.sun.Value / 2;
                                                }
                                                else
                                                {
                                                    Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.sun.Value;
                                                }
                                            }
                                            if (AL.HolType.Equals("F-F"))
                                            {
                                                Days_ABS = Days_ABS + 1.0; Hrs_ABS = Hrs_ABS + WH.sun.Value;
                                            }
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    var data = (from AB in DbContext.E_ABSENCE
                                where AB.ABSENCEHISTORYID == AL.ABSENCEHISTORYID
                                select AB).FirstOrDefault();

                    E_ABSENCE absence;
                    absence = data;
                    if (UpdatedUnit.Equals("Hours"))
                    {
                        absence.DURATION = Hrs_ABS;
                    }
                    else if (UpdatedUnit.Equals("Days"))
                    {
                        absence.DURATION = Days_ABS;
                    }
                    absence.DURATION_TYPE = UpdatedUnit;
                    Days_ABS = 0.0;
                    Hrs_ABS = 0.0;
                }
            }
            DbContext.SaveChanges();
        }
        public void CancelLeaves(int employeeId,int? lastActionUser)
        {
            var employeeLeave = DbContext.E_AnnualLeaveCancelWorkingPatternChange(employeeId).ToList();
            if (employeeLeave != null)
            {
                var lstAbsence = employeeLeave;

                if (lstAbsence != null && lstAbsence.Count > 0)
                {
                    var leaveStatus = (from ls in DbContext.E_STATUS where ls.DESCRIPTION == "Cancelled" select ls.ITEMSTATUSID).FirstOrDefault();
                    foreach (var item in employeeLeave)
                    {
                        var leaveStatusChangeRequest = new LeaveStatusChangeRequest();
                        leaveStatusChangeRequest.absenceHistoryId = item.Value;
                        leaveStatusChangeRequest.actionId = leaveStatus;
                        leaveStatusChangeRequest.certNo = "";
                        leaveStatusChangeRequest.drName = "";
                        leaveStatusChangeRequest.notes = "Working pattern change";
                        if (lastActionUser.HasValue)
                        {
                            leaveStatusChangeRequest.actionBy = lastActionUser.Value;
                        }
                        ChangeLeaveStatus(leaveStatusChangeRequest);
                    }

                    //sendEmailNotification to employee
                    var emailData = (from e in DbContext.E__EMPLOYEE
                                     join ec in DbContext.E_CONTACT on e.EMPLOYEEID equals ec.EMPLOYEEID
                                     where e.EMPLOYEEID == employeeId
                                     select new CancelLeaveEmailNotification()
                                     {
                                         employeeName = e.FIRSTNAME + " " + e.LASTNAME,
                                         employeeEmail = ec.WORKEMAIL,
                                         employeeId = e.EMPLOYEEID,
                                     }).FirstOrDefault();

                    sendEmailNotification(emailData);
                }
            }
        }
        public List<LookUpResponseModel> GetRolesByTeamId(int teamId)
        {

            var data = (from e in DbContext.E_JOBROLE
                        join t in DbContext.E_JOBROLETEAM on e.JobRoleId equals t.JobRoleId
                        where t.isDeleted == false && t.TeamId == teamId
                        select new LookUpResponseModel()
                        {
                            lookUpId = e.JobRoleId,
                            lookUpDescription = e.JobeRoleDescription
                        }).ToList();

            return data;
        }

        public List<JobRoleHistory> GetEmployeeJobRoleHistory(int emplyeeId)
        {
            List<JobRoleHistory> history = new List<JobRoleHistory>();

            var data = (from eh in DbContext.E_EMPLOYEEJOBROLEHISTORY
                        join t in DbContext.E_JOBROLETEAM on eh.JobRoleTeamId equals t.JobRoleTeamId
                        join j in DbContext.E_JOBROLE on t.JobRoleId equals j.JobRoleId
                        join et in DbContext.E_TEAM on t.TeamId equals et.TEAMID
                        join ee in DbContext.E__EMPLOYEE on eh.EmployeeId equals ee.EMPLOYEEID
                        join lau in DbContext.E__EMPLOYEE on eh.CreatedBy equals lau.EMPLOYEEID into lau_join
                        from lau in lau_join.DefaultIfEmpty()
                        where ee.EMPLOYEEID == emplyeeId
                        orderby eh.CreatedDate ascending
                        select new JobRoleHistory()
                        {
                            team = et.TEAMNAME,
                            jobRole = j.JobeRoleDescription,
                            createdDate = eh.CreatedDate,
                            createdby = lau.FIRSTNAME == null ? "-" : lau.FIRSTNAME + " " + lau.LASTNAME
                        }).ToList();

            if (data != null)
            {
                for(int i=0;i<data.Count;i++)
                {
                    if (i == 0)
                    {
                        history.Add(data[i]);
                    }
                    else
                    {
                        if (data[i].team != data[i - 1].team)
                        {
                            history.Add(data[i]);
                        }
                        else if (data[i].team == data[i - 1].team && data[i].jobRole != data[i - 1].jobRole)
                        {
                            history.Add(data[i]);
                        }
                    }
                }
            }
            return history;
        }

        public string GetLoggedInUserTeam(int emplyeeId)
        {
            var data = (from e in DbContext.E__EMPLOYEE
                        join ej in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals ej.EMPLOYEEID
                        join et in DbContext.E_TEAM on ej.TEAM equals et.TEAMID
                        where e.EMPLOYEEID == emplyeeId
                        select new 
                        {
                            team = et.TEAMNAME,
                        }).FirstOrDefault();


            if (data == null)
            {
                return string.Empty;
            }
            return data.team;
        }

        public WorkingHoursParent GetEmpCurrentWorkingPattren(int employeeId)
        {
            WorkingHoursParent response = new WorkingHoursParent();
            var workingHoursData = (from h in DbContext.E_WorkingHours
                                    join e in DbContext.E__EMPLOYEE on h.CREATEDBY equals e.EMPLOYEEID
                                    into e_join
                                    from e in e_join.DefaultIfEmpty()
                                    where h.EmployeeId == employeeId
                                    select new EmployeeWorkingHours()
                                    {
                                        createdByName = e.FIRSTNAME + " " + e.LASTNAME,
                                        createdBy = h.CREATEDBY,
                                        createdOn = h.CREATEDON,
                                        employeeId = employeeId,
                                        fri = h.Fri,
                                        mon = h.Mon,
                                        sat = h.Sat,
                                        sun = h.Sun,
                                        thu = h.Thu,
                                        tue = h.Tue,
                                        wed = h.Wed,
                                        lastActionUser = h.CREATEDBY,
                                        _startDate = h.STARTDATE,
                                        total = h.Total,
                                        groupNumber=h.GroupNumber,
                                        wId=h.Wid,
                                        DayTimings=h.DaysTimings
                                    }).ToList();
            int WN = 1;
            foreach(EmployeeWorkingHours ewh in workingHoursData )
            {
                if (ewh._startDate.HasValue)
                {
                    ewh.startDate = ewh._startDate.Value.ToShortDateString();
                }

                ewh.weekNumber = WN;
                WN = WN + 1;
                if (ewh.DayTimings!=null)
                {
                    var dayTimeing = ewh.DayTimings.Split('_');
                    foreach (var item in dayTimeing)
                    {
                        if (item.Contains("mon"))
                        {
                            ewh.MonDayTimings = item;
                        }
                        if (item.Contains("tue"))
                        {
                            ewh.TueDayTimings = item;
                        }
                        if (item.Contains("wed"))
                        {
                            ewh.WedDayTimings = item;
                        }
                        if (item.Contains("thu"))
                        {
                            ewh.ThuDayTimings = item;
                        }
                        if (item.Contains("fri"))
                        {
                            ewh.FriDayTimings = item;
                        }
                        if (item.Contains("sat"))
                        {
                            ewh.SatDayTimings = item;
                        }
                        if (item.Contains("sun"))
                        {
                            ewh.SunDayTimings = item;
                        }
                    }
                }                
            }
            response.workingHours=workingHoursData;
            return response;
        }

        public WorkingHoursParent GetEmployeeWorkingHours(int employeeId)
        {
            WorkingHoursParent response = new WorkingHoursParent();
            var workingHoursData = (from h in DbContext.E_WorkingHours
                                    join e in DbContext.E__EMPLOYEE on h.CREATEDBY equals e.EMPLOYEEID
                                    into e_join
                                    from e in e_join.DefaultIfEmpty()
                                    where h.EmployeeId == employeeId
                                    select new EmployeeWorkingHours()
                                    {
                                        createdByName = e.FIRSTNAME + " " + e.LASTNAME,
                                        createdByFname = e.FIRSTNAME.Substring(0, 1),
                                        createdByLname = e.LASTNAME,
                                        createdBy = h.CREATEDBY,
                                        createdOn = h.CREATEDON,
                                        employeeId = employeeId,
                                        fri = h.Fri,
                                        mon = h.Mon,
                                        sat = h.Sat,
                                        sun = h.Sun,
                                        thu = h.Thu,
                                        tue = h.Tue,
                                        wed = h.Wed,
                                        lastActionUser = h.CREATEDBY,
                                        _startDate = h.STARTDATE,
                                        total = h.Total,
                                        groupNumber = h.GroupNumber,
                                        wId=h.Wid,
                                        DayTimings=h.DaysTimings
                                    }).ToList();

            var workingHoursHistory = (from h in DbContext.E_WorkingHours_History
                                       join e in DbContext.E__EMPLOYEE on h.CreatedBy equals e.EMPLOYEEID
                                       into e_join
                                       from e in e_join.DefaultIfEmpty()
                                       where h.EmployeeId == employeeId
                                       select new EmployeeWorkingHours()
                                       {
                                           employeeId = employeeId,
                                           _startDate = h.StartDate,
                                           _endDate = h.EndDate,
                                           createdByName = (e.FIRSTNAME ?? " -") + " " + e.LASTNAME,
                                           createdByFname = e.FIRSTNAME.Substring(0, 1),
                                           createdByLname = e.LASTNAME,
                                           createdBy = h.CreatedBy,
                                           createdOn = h.ctimestamp,
                                           mon = h.Mon,
                                           fri = h.Fri,
                                           sat = h.Sat,
                                           sun = h.Sun,
                                           thu = h.Thu,
                                           total = h.Total,
                                           tue = h.Tue,
                                           wed = h.Wed,
                                           groupNumber = h.GroupNumber
                                       }).ToList();

            foreach (EmployeeWorkingHours ewh in workingHoursData)
            {
                if (ewh._startDate.HasValue)
                {
                    ewh.startDate = ewh._startDate.Value.ToShortDateString();
                }

                if (ewh.DayTimings != null)
                {
                    var dayTimeing = ewh.DayTimings.Split('_');
                    foreach (var item in dayTimeing)
                    {
                        if (item.Contains("mon"))
                        {
                            ewh.MonDayTimings = item;
                        }
                        if (item.Contains("tue"))
                        {
                            ewh.TueDayTimings = item;
                        }
                        if (item.Contains("wed"))
                        {
                            ewh.WedDayTimings = item;
                        }
                        if (item.Contains("thu"))
                        {
                            ewh.ThuDayTimings = item;
                        }
                        if (item.Contains("fri"))
                        {
                            ewh.FriDayTimings = item;
                        }
                        if (item.Contains("sat"))
                        {
                            ewh.SatDayTimings = item;
                        }
                        if (item.Contains("sun"))
                        {
                            ewh.SunDayTimings = item;
                        }
                    }
                }
            }

            foreach (EmployeeWorkingHours ewh in workingHoursHistory)
            {
                if (ewh._startDate.HasValue)
                {
                    ewh.startDate = ewh._startDate.Value.ToShortDateString();
                }

                if (ewh._endDate.HasValue)
                {
                    ewh.endDate = ewh._endDate.Value.ToShortDateString();
                }
            }
            response.workingHours = workingHoursData;
            response.workingHoursHistory = workingHoursHistory;
            return response;
        }

        public object GetEmployeeDaysTimings(int employeeId, int wid,string day)
        {
            string response = "";
            var workingHoursData = (from h in DbContext.E_WorkingHours
                                    join e in DbContext.E__EMPLOYEE on h.CREATEDBY equals e.EMPLOYEEID
                                    into e_join
                                    from e in e_join.DefaultIfEmpty()
                                    where h.EmployeeId == employeeId && h.Wid==wid
                                    select new EmployeeWorkingHours()
                                    {
                                        DayTimings = h.DaysTimings
                                    }).FirstOrDefault();

            if (workingHoursData != null && workingHoursData.DayTimings!=null)
            {
                var dayTimeing = workingHoursData.DayTimings.Split('_');
                foreach (var item in dayTimeing)
                {
                    if (item.Contains(day))
                    {
                        response = item;
                        break;
                    }
                }
            }
            else
            {
                response = null;
            }
            var dayTiming = new JavaScriptSerializer().DeserializeObject(response);
            return dayTiming;
        }

        public WorkingHoursParent AmendEmployeeWorkingHours(WorkingHoursParent workingHours)
        {
            WorkingHoursParent response = new WorkingHoursParent();
            double dataTotalHours = 0;

            var data = (from h in DbContext.E_WorkingHours
                        where h.EmployeeId == workingHours.employeeId
                        select h).ToList();
            if (data != null && data.Count > 0)
            {
                dataTotalHours = double.Parse(
                                     data.Where(x => x.Wid == (data.Select(y => y.Wid).Max())
                                     ).FirstOrDefault().Total.ToString());
            }

            bool TotalCheck = false;
            
            int groupNumber = 0;
            if (data != null && data.Count > 0)
            {
                if (data.First().GroupNumber!= null)
                {
                    groupNumber = int.Parse(data.First().GroupNumber.ToString());
                }               
                DbContext.E_WorkingHours.RemoveRange(data);
            }
            groupNumber = groupNumber + 1;
            DbContext.SaveChanges();

            

            workingHours.workingHours.ForEach(z => z.groupNumber = groupNumber);

            var DayTimings = new List<DayTimingViewModel>();
            var coreHours = DbContext.E_CORE_WORKING_HOURS.Where(e => e.EMPLOYEEID == workingHours.employeeId).ToList();
            var interation = 0;

            var HistoryEndDate = GeneralHelper.GetDateTimeFromString(workingHours.workingHours.FirstOrDefault().startDate);
            foreach (EmployeeWorkingHours EWH in workingHours.workingHours)
            {
                //interation = 0;
                DatabaseEntities.E_WorkingHours dataObject = new E_WorkingHours();
                dataObject.STARTDATE = GeneralHelper.GetDateTimeFromString(EWH.startDate);
                dataObject.Mon = EWH.mon;
                dataObject.Tue = EWH.tue;
                dataObject.Wed = EWH.wed;
                dataObject.Thu = EWH.thu;
                dataObject.Fri = EWH.fri;
                dataObject.Sat = EWH.sat;
                dataObject.Sun = EWH.sun;
                dataObject.Total = EWH.total;
                dataObject.CREATEDBY = EWH.createdBy;
                dataObject.CREATEDON = EWH.createdOn;
                dataObject.EmployeeId = workingHours.employeeId;
                dataObject.GroupNumber = groupNumber;
                dataObject.DaysTimings = EWH.DayTimings;
                DbContext.E_WorkingHours.Add(dataObject);

                DbContext.SaveChanges();
                EWH.wId = dataObject.Wid;


                if (interation == 0)
                {
                    // update core hours for employess

                    if (workingHours.workingHours[0].DayTimings.Contains("_"))
                    {
                        foreach (var item in workingHours.workingHours[0].DayTimings.Split('_'))
                        {
                            DayTimings.Add(new JavaScriptSerializer().Deserialize<DayTimingViewModel>(item));
                        }
                    }
                    else
                    {
                        DayTimings.Add(new JavaScriptSerializer().Deserialize<DayTimingViewModel>(workingHours.workingHours[0].DayTimings));
                    }

                    DayTimings.ForEach(e => e.employeeId = workingHours.employeeId);

                    if (coreHours != null && coreHours.Count > 0)
                    {
                        // update core hours
                        UpdateCoreHours(workingHours.employeeId, EWH, DayTimings);
                    }
                    else
                    {
                        // add core hours for employees
                        E_CORE_WORKING_HOURS coreHour = new E_CORE_WORKING_HOURS();
                        for (int i = 1; i <= 7; i++)
                        {
                            coreHour.DAYID = i;
                            coreHour.EMPLOYEEID = workingHours.employeeId;
                            coreHour.STARTTIME = "09:00";
                            coreHour.ENDTIME = "17:00";
                            coreHour.CREATEDDATE = EWH.createdOn;
                            coreHour.CREATEDBY = EWH.createdBy;
                            if (i == 6 || i == 7)
                            {
                                coreHour.STARTTIME = "";
                                coreHour.ENDTIME = "";
                            }
                            DbContext.E_CORE_WORKING_HOURS.Add(coreHour);
                        }
                        DbContext.SaveChanges();

                        // update core hours
                        UpdateCoreHours(workingHours.employeeId, EWH, DayTimings);
                    }
                }

                ++interation;
            }
            DbContext.SaveChanges();

            var jobDetailData = (from j in DbContext.E_JOBDETAILS
                                 where j.EMPLOYEEID == workingHours.employeeId
                                 select j).FirstOrDefault();

            if (jobDetailData != null)
            {
                jobDetailData.HOURS = workingHours.total;
            }
            DbContext.SaveChanges();

            var pastHistoryData = (from h in DbContext.E_WorkingHours_History
                                   join e in DbContext.E__EMPLOYEE on h.CreatedBy equals e.EMPLOYEEID
                                   into e_join
                                   from e in e_join.DefaultIfEmpty()
                                   where h.EmployeeId == workingHours.employeeId && h.EndDate == null
                                   select h).ToList();
            if (pastHistoryData != null)
            {
                foreach (E_WorkingHours_History pdh in pastHistoryData)
                {
                    pdh.EndDate = HistoryEndDate;
                    HistoryEndDate = HistoryEndDate.AddDays(7);
                }                
            }
            DbContext.SaveChanges();

            foreach (EmployeeWorkingHours EWH in workingHours.workingHours)
            {
                E_WorkingHours_History wHours = new E_WorkingHours_History();
                DbContext.E_WorkingHours_History.Add(wHours);
                wHours.StartDate = GeneralHelper.GetDateTimeFromString(EWH.startDate);
                wHours.EndDate = null;
                wHours.Mon = EWH.mon;
                wHours.Tue = EWH.tue;
                wHours.Wed = EWH.wed;
                wHours.Thu = EWH.thu;
                wHours.Fri = EWH.fri;
                wHours.Sat = EWH.sat;
                wHours.Sun = EWH.sun;
                wHours.Total = EWH.total;
                wHours.CreatedBy = EWH.createdBy;
                wHours.ctimestamp = DateTime.Now;
                wHours.GroupNumber = groupNumber;
                wHours.EmployeeId = workingHours.employeeId;
            }

            DbContext.SaveChanges();
            foreach (EmployeeWorkingHours EWH in workingHours.workingHours)
            {
                workingHours.workingHoursHistory.Add(EWH);
            }

            var dataLatest = (from h in DbContext.E_WorkingHours
                        where h.EmployeeId == workingHours.employeeId
                        select h).ToList();
            double dataLatestTotalHours = double.Parse(
                                 dataLatest.Where(x => x.Wid == (dataLatest.Select(y => y.Wid).Max())
                                 ).FirstOrDefault().Total.ToString());
            if (dataTotalHours != dataLatestTotalHours)
            {
                // cancel leave and send email to line manager 
                var employeeLeave = DbContext.E_AnnualLeaveCancelWorkingPatternChange(workingHours.employeeId).ToList();
                if (employeeLeave != null)
                {
                    var lstAbsence = employeeLeave;

                    if (lstAbsence != null && lstAbsence.Count > 0)
                    {
                        var leaveStatus = (from ls in DbContext.E_STATUS where ls.DESCRIPTION == "Cancelled" select ls.ITEMSTATUSID).FirstOrDefault();
                        foreach (var item in employeeLeave)
                        {
                            var leaveStatusChangeRequest = new LeaveStatusChangeRequest();
                            leaveStatusChangeRequest.absenceHistoryId = item.Value;
                            leaveStatusChangeRequest.actionId = leaveStatus;
                            leaveStatusChangeRequest.certNo = "";
                            leaveStatusChangeRequest.drName = "";
                            leaveStatusChangeRequest.notes = "Working pattern change";
                            if (workingHours.lastActionUser.HasValue)
                            {
                                leaveStatusChangeRequest.actionBy = workingHours.lastActionUser.Value;
                            }

                            ChangeLeaveStatus(leaveStatusChangeRequest);
                        }

                        //sendEmailNotification to employee
                        var emailData = (from e in DbContext.E__EMPLOYEE
                                         join ec in DbContext.E_CONTACT on e.EMPLOYEEID equals ec.EMPLOYEEID
                                         where e.EMPLOYEEID == workingHours.employeeId
                                         select new CancelLeaveEmailNotification()
                                         {
                                             employeeName = e.FIRSTNAME + " " + e.LASTNAME,
                                             employeeEmail = ec.WORKEMAIL,
                                             employeeId = e.EMPLOYEEID,
                                         }).FirstOrDefault();

                        sendEmailNotification(emailData);
                    }
                }
            }

            return workingHours;
        }

        public string GetDirectorForTeamId(int teamId)
        {
            return GetDirectorNameByTeamId(teamId);
        }

        public HolidayRule GetHolidayRule(int eid)
        {
            var response = new HolidayRule();
            var data = (from t in DbContext.E_HOLIDAYRULE
                     where t.EID == eid
                     select t).FirstOrDefault();
            if (data != null)
            {
                response.eId = data.EID;
                response.HolidayDay = data.Holiday_Days;
                response.holidayRule = data.HolidayRule;
                response.maxHolidayDay = data.Max_Holiday_Days;
            }
            return response;
        }

        public string ChangeLeaveStatus(LeaveStatusChangeRequest leaveStatusChangeRequest)
        {
            //Get E_ABSENCE DATA WITH ITS JOURNAL ENTRY
            var absenceData = DbContext.E_ABSENCE.Include("E_JOURNAL").Where(x => x.ABSENCEHISTORYID == leaveStatusChangeRequest.absenceHistoryId).FirstOrDefault();

            if (absenceData == null)
            {
                return ApplicationConstants.absenceError;
            }

            //Check Existing Leave
            var startDate = absenceData.STARTDATE == null ? DateTime.Now : (DateTime)absenceData.STARTDATE;
            var returnDate = absenceData.RETURNDATE == null ? DateTime.Now : (DateTime)absenceData.RETURNDATE;

            var result = DbContext.Database.SqlQuery<double>("select dbo.E_CHECK_EXISTINGLEAVE_UPDATE({0},{1},{2},{3},{4}, {5})", absenceData.E_JOURNAL.EMPLOYEEID, startDate.ToString("yyyy-dd-MM"), returnDate.ToString("yyyy-dd-MM"), absenceData.DURATION, absenceData.HOLTYPE, absenceData.JOURNALID).FirstOrDefault();

            if (DbContext.E_ACTION.Where(x => x.DESCRIPTION == ApplicationConstants.leaveActionCancel || x.DESCRIPTION == ApplicationConstants.leaveActionDecline).Select(y => y.ITEMACTIONID).ToList().Contains(leaveStatusChangeRequest.actionId))
            {
                result = 0;
            }

            //Get Hours for the duration
            double? leave_H = 0.0;
            if (result == 0)
            {
                leave_H = DbContext.E_BOOK_ANNUAL_LEAVE_HRS(absenceData.E_JOURNAL.EMPLOYEEID, absenceData.STARTDATE, absenceData.RETURNDATE, absenceData.DURATION.ToString()).FirstOrDefault();
                leave_H = leave_H == null ? 0.0 : leave_H;
            }

            //Add new Absence entry for Status Change
            var newAbsenceRecord = new E_ABSENCE();
            newAbsenceRecord.JOURNALID = absenceData.JOURNALID;
            newAbsenceRecord.CERTNO = leaveStatusChangeRequest.certNo;
            newAbsenceRecord.DRNAME = leaveStatusChangeRequest.drName;
            newAbsenceRecord.DURATION = absenceData.DURATION;
            if (absenceData.DURATION_HRS != null && absenceData.DURATION_HRS > 0)
            {
                newAbsenceRecord.DURATION_HRS = absenceData.DURATION_HRS;
            }
            else
            {
                newAbsenceRecord.DURATION_HRS = leave_H;
            }
            newAbsenceRecord.HOLTYPE = absenceData.HOLTYPE;
            newAbsenceRecord.ITEMACTIONID = leaveStatusChangeRequest.actionId;
            newAbsenceRecord.ITEMSTATUSID = leaveStatusChangeRequest.actionId;
            newAbsenceRecord.LASTACTIONDATE = DateTime.Now;
            newAbsenceRecord.LASTACTIONUSER = leaveStatusChangeRequest.actionBy;
            newAbsenceRecord.NOTES = leaveStatusChangeRequest.notes;
            newAbsenceRecord.REASON = absenceData.REASON;
            newAbsenceRecord.REASONID = absenceData.REASONID;
            newAbsenceRecord.RETURNDATE = absenceData.RETURNDATE;
            newAbsenceRecord.STARTDATE = absenceData.STARTDATE;
            if (absenceData.E_JOURNAL.EMPLOYEEID.HasValue)
            {
                newAbsenceRecord.DURATION_TYPE = getEmpHolidayIndicatorDescription(absenceData.E_JOURNAL.EMPLOYEEID.Value);
            }

            DbContext.E_ABSENCE.Add(newAbsenceRecord);

            //Change status in Journal of the Leave
            var journal = DbContext.E_JOURNAL.Where(j => j.JOURNALID == (int)absenceData.JOURNALID).FirstOrDefault();
            journal.CURRENTITEMSTATUSID = leaveStatusChangeRequest.actionId;
            DbContext.SaveChanges();
            return ApplicationConstants.supported;
        }

        private string getEmpHolidayIndicatorDescription(int employeeId)
        {
            return (from emp in DbContext.E__EMPLOYEE
                    join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                    join ehi in DbContext.E_HOLIDAYENTITLEMENT_INDICATOR on jd.HolidayIndicator equals ehi.Sid
                    where emp.EMPLOYEEID == employeeId
                    select ehi.Indicator).FirstOrDefault();
        }

        private void sendEmailNotification(CancelLeaveEmailNotification emailObj)
        {
            if (emailObj != null && emailObj.employeeEmail != null)
            {
                string body = string.Empty;
                body = "Dear " + emailObj.employeeName + ", <br /><br />";
                body = body + "Further to the recent agreement to change your working hours, BBS has now been updated with your new working pattern. <br /><br />";
                body = body + "As a result all leave booked after the effective date of your new hours / working pattern has been automatically cancelled on the system. <br />";
                body = body + "You therefore now need to re-book this leave so that BBS deducts the correct amount of leave based on your new hours. <br /><br />";
                body = body + "Any queries, please contact a member of the HR Team. <br /><br />";
                body = body + "Thanks";
                body = string.Format(body, emailObj.employeeName);

                string subject = EmailSubjectConstants.LeaveCancelation;
                EmailHelper.sendHtmlFormattedEmail(emailObj.employeeName, emailObj.employeeEmail, subject, body);
            }
        }

        public List<JobDetailNoteResponse> GetJobDetailNote(int employeeId)
        {
            var response = (from jn in DbContext.E_JobDetailNote
                    join emp in DbContext.E__EMPLOYEE on jn.CreatedBy equals emp.EMPLOYEEID
                    where jn.EMPLOYEEID == employeeId
                    select new JobDetailNoteResponse()
                    {
                        employeeId = jn.EMPLOYEEID,
                        jobDetailNoteID = jn.JobDetailNoteID,
                        createdBy = jn.CreatedBy,
                        _createdDate = jn.CreatedDate,
                        note = jn.Note,
                        createdByName = emp.FIRSTNAME + " " + emp.LASTNAME
                    }).OrderByDescending(e => e._createdDate).ToList();

            if (response != null && response.Count > 0)
            {
                foreach (var item in response)
                {
                    item.createdDate = item._createdDate.ToString("dd/MM/yyyy HH:mm");
                }
            }

            return response;
        }

        public JobDetailNoteResponse SaveJobDetailNote(JobDetailNoteResponse request)
        {
            DatabaseEntities.E_JobDetailNote jobDetailNote = new E_JobDetailNote();

            jobDetailNote.CreatedBy = request.createdBy;
            jobDetailNote.CreatedDate = DateTime.Now;//GeneralHelper.GetDateTimeFromString(request.createdDate);
            jobDetailNote.EMPLOYEEID = request.employeeId;
            jobDetailNote.Note = request.note;

            DbContext.E_JobDetailNote.Add(jobDetailNote);
            DbContext.SaveChanges();
            request.jobDetailNoteID = jobDetailNote.JobDetailNoteID;

            return request;
        }


        #endregion
    }
}
