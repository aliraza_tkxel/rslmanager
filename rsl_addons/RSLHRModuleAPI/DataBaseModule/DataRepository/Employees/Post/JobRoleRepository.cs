﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Employees.PostTabRepositories
{
    public class JobRoleRepository : BaseRepository<E_JOBROLE>
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public JobRoleRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }

        public List<E_JOBROLE> GetJobRoleLookUp(int teamId)
        {
            var jobRoleIds = DbContext.E_JOBROLETEAM.Where(e => e.TeamId == teamId && e.isDeleted==false).Select(e => e.JobRoleId).ToList();
            return DbContext.E_JOBROLE.Where(e => jobRoleIds.Contains(e.JobRoleId)).ToList();
        }

    }
}
