﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository
{
    public class DocumentTypeRepository : BaseRepository<HR_DocumentType>
    {
        public DocumentTypeRepository(Entities dbContext) : base(dbContext)
        {

        }

    }
}
