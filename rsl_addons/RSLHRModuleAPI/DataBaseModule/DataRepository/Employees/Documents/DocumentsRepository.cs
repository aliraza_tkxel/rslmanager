﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using NetwrokModel.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataBaseModule.DataRepository
{
    public class DocumentsRepository : BaseRepository<HR_Documents>
    {
        public DocumentsRepository(Entities dbContext) : base(dbContext)
        {

        }


        public void AddEmployeeDocument(AddDocumentRequest request)
        {
            HR_Documents document = new HR_Documents();
            var documentData = (from h in DbContext.HR_Documents
                                where h.DocumentId == request.documentId
                                select h);

            if (documentData.Count() > 0)
            {
                document = documentData.FirstOrDefault();

            }
            document.DocumentTypeId = request.documentTypeId;
            document.EmployeeID = request.employeeId;
            if (request.documentDate != string.Empty || request.documentDate != null)
                document.DocumentDate = Convert.ToDateTime(request.documentDate);
            if (request.documentExpiry != string.Empty && request.documentExpiry != null)
                document.ExpiryDate = Convert.ToDateTime(request.documentExpiry);
            document.IsActive = true;
            document.CreateDate = DateTime.Now;
            document.CreateBy = request.createdBy;
            document.DocumentPath = request.documentPath;
            document.Keywords = request.keyWords;
            document.Title = request.title;
            document.EmployeeVisibility = request.employeeVisibility;
            document.DocumentVisibility = request.documentVisibilityId;
            if (documentData.Count() == 0)
            {
                DbContext.HR_Documents.Add(document);
            }
            DbContext.SaveChanges();
        }

        public void DeleteEmployeeDocument(AddDocumentRequest request)
        {
            HR_Documents document = new HR_Documents();
            var documentData = (from h in DbContext.HR_Documents
                                where h.DocumentId == request.documentId
                                select h);

            if (documentData.Count() > 0)
            {
                document = documentData.FirstOrDefault();

            }
            document.IsActive = false;

            DbContext.SaveChanges();
        }

        public DocumentList GetEmployeeDocuments(DocumentSearchRequest request)
        {

            DocumentList response = new DocumentList();
            //var query = new list HR_Documents
            var data = (from h in DbContext.HR_Documents
                        join e in DbContext.E__EMPLOYEE on h.CreateBy equals e.EMPLOYEEID
                        join t in DbContext.HR_DocumentType on h.DocumentTypeId equals t.TypeId
                        where h.EmployeeID == request.employeeId && h.IsActive == true
                        && (h.DocumentTypeId == request.documetTypeId || request.documetTypeId == null)
                        && (h.Keywords.Contains(request.keyWords) || request.keyWords == null)
                        && (h.Title.Contains(request.title) || request.title == null)
                        select new DocumentDetails()
                        {
                            firstName = e.FIRSTNAME,
                            lastName = e.LASTNAME,
                            documentId = h.DocumentId,
                            documetTypeId = h.DocumentTypeId,
                            employeeId = h.EmployeeID,
                            createdDate = h.CreateDate.ToString(),
                            documentPath = h.DocumentPath,
                            documentDateTime = h.DocumentDate,
                            documentExpiryTime = h.ExpiryDate,
                            documentType = t.Description,
                            title = h.Title,
                            employeeVisibility = h.EmployeeVisibility,
                            createdByName = e.FIRSTNAME + " " + e.LASTNAME,
                            documentVisibilityId = h.DocumentVisibility
                        }).ToList();
            // for employee view (condition apply 'employeeVisibility = true') other vise get all documents 
            if (request.employeeVisibility == true)
            {
                data = data.Where(e => e.employeeVisibility == null || e.employeeVisibility == true).ToList();
            }

            var loggedInUserTeam = getLoggedInUserTeam(request.loggedInUser);

            var loggedInUserStaff = getLoggedInUserStaff(request.loggedInUser);

            var isInMyStaff = loggedInUserStaff.Where(x => x.employeeId == request.employeeId).Count() > 0 ? true : false;

            var tempData = new List<DocumentDetails>(data);

            var CEO = (from j in DbContext.E_JOBROLE
                       join e in DbContext.E_JOBDETAILS on j.JobeRoleDescription equals e.JOBTITLE
                       where j.JobeRoleDescription == "Group Chief Executive"
                       select e.EMPLOYEEID).FirstOrDefault();

            var docVisibility = DbContext.E_DOCVISIBILITY.Select(x => new { Id = x.DOCVISIBILITYID, Description = x.DOCVISIBILITYDESCRIPTION }).ToList();

            foreach (var temp in tempData)
            {
                if (temp.documentVisibilityId == docVisibility.Where(x => x.Description == ApplicationConstants.documentVisibilityHROnly).FirstOrDefault().Id
                    && (loggedInUserTeam.ToString() != "HR Services"))
                {
                    data.RemoveAt(data.IndexOf(temp));
                }
                else if (temp.documentVisibilityId == docVisibility.Where(x => x.Description == ApplicationConstants.documentVisibilityHRCEO).FirstOrDefault().Id
                    && (loggedInUserTeam.ToString() != "HR Services" && request.loggedInUser != CEO))
                {
                    data.RemoveAt(data.IndexOf(temp));
                }
                else if (temp.documentVisibilityId == docVisibility.Where(x => x.Description == ApplicationConstants.documentVisibilityHRExec).FirstOrDefault().Id
                    && (loggedInUserTeam.ToString() != "HR Services" && loggedInUserTeam.ToString() != "Executive Team"))
                {
                    data.RemoveAt(data.IndexOf(temp));
                }
                else if (temp.documentVisibilityId == docVisibility.Where(x => x.Description == ApplicationConstants.documentVisibilityManager).FirstOrDefault().Id
                    && (loggedInUserTeam.ToString() != "HR Services" && loggedInUserTeam.ToString() != "Executive Team" && !isInMyStaff))
                {
                    data.RemoveAt(data.IndexOf(temp));
                }
            }

            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / request.pagination.pageSize);
            page.pageNo = request.pagination.pageNo;
            page.pageSize = request.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (request.sortBy == null)
            {
                _sortBy = @"documentId";
            }
            else
            {
                if (request.sortBy == "documentDate")
                {
                    _sortBy = "documentDateTime";
                }
                else if (request.sortBy == "documentExpiry")
                {
                    _sortBy = "documentExpiryTime";
                }
                else
                {
                    _sortBy = request.sortBy;
                }

            }
            // Getting sorted by sort parameter
            if (request.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
            }
            foreach (DocumentDetails doc in finalData)
            {
                doc.documentPath = FileHelper.getLogicalDocumentPath(doc.documentPath, request.employeeId);

                DateTime createdDate = Convert.ToDateTime(doc.createdDate);
                doc.createdDate = createdDate.ToString("dd/MM/yyyy");
                if (doc.documentDateTime.HasValue)
                {
                    DateTime documentDate = Convert.ToDateTime(doc.documentDateTime);
                    doc.documentDate = documentDate.ToString("dd/MM/yyyy");
                }
                if (doc.documentExpiryTime.HasValue)
                {
                    DateTime documentExpiry = Convert.ToDateTime(doc.documentExpiryTime);
                    doc.documentExpiry = documentExpiry.ToString("dd/MM/yyyy");
                }
            }
            response.documentList = finalData;
            return response;
        }

        private string getLoggedInUserTeam(int employeeId)
        {
            return (from e in DbContext.E__EMPLOYEE
             join ej in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals ej.EMPLOYEEID
             join et in DbContext.E_TEAM on ej.TEAM equals et.TEAMID
             where e.EMPLOYEEID == employeeId
                    select new
             {
                 team = et.TEAMNAME,
             }).FirstOrDefault().team;
        }

        private List<MyStaffDetail> getLoggedInUserStaff(int employeeId)
        {
            return (from E in DbContext.E__EMPLOYEE
                    join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                    from J in J_join.DefaultIfEmpty()
                    join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                    from T in T_join.DefaultIfEmpty()
                    join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                    from ET in ET_join.DefaultIfEmpty()
                    join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                    from ETH in ETH_join.DefaultIfEmpty()
                    where J.LINEMANAGER == employeeId && J.ACTIVE == 1
                    select new MyStaffDetail
                    {
                        employeeId = E.EMPLOYEEID,
                        fullName = E.FIRSTNAME + " " + E.LASTNAME,
                        jobTitle = ET.JobeRoleDescription,
                        paypointReviewDate = J.REVIEWDATE,
                        appraisalDateTime = J.APPRAISALDATE

                    }).ToList();
        }

    }
}
