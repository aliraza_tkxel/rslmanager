﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using NetworkModel.NetwrokModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository
{
    public class SkillsRepository : BaseRepository<E_QUALIFICATIONSANDSKILLS>
    {
        public SkillsRepository(Entities dbContext) : base(dbContext)
        {

        }

        public SkillsListingResponse GetEmployeesSkills(CommonRequest model)
        {
            SkillsListingResponse response = new SkillsListingResponse();

            var data = (from S in DbContext.E_QUALIFICATIONSANDSKILLS
                        join E in DbContext.E__EMPLOYEE on S.EMPLOYEEID equals E.EMPLOYEEID
                        join L in DbContext.E_QUALIFICATIONLEVEL on S.QualLevelID equals L.QualLevelId into J_join
                        from J in J_join.DefaultIfEmpty()
                        where S.EMPLOYEEID == model.employeeId && S.IsActive == true
                        select new SkillsAndQualificationsDetail
                        {
                            employeeId = S.EMPLOYEEID,
                            // isSpoken = (S.IsSpoken ?? false),
                            createdBy = S.LASTACTIONUSER,
                            levelDescription = (J.QualLevelDesc ?? "-"),
                            qualificationDateTime = S.QUALIFICATIONDATE,
                            qualificationId = S.QUALIFICATIONSID,
                            qualLevelID = S.QualLevelID,
                            result = S.RESULT,
                            subject = S.SUBJECT,
                            equivalentLevel = S.EquivalentLevel
                        });


            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.skills.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"qualificationId";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            foreach (SkillsAndQualificationsDetail item in finalData)
            {
                var profQualList = (from S in DbContext.E_ProfessionalQualifications where (S.QualificationId == item.qualificationId) select S.Description).ToList();
                if (profQualList.Count() > 0)
                    item.professionalQualification = profQualList;

                if (item.qualificationDateTime.HasValue)
                {
                    DateTime startDate = (DateTime)item.qualificationDateTime;
                    item.qualificationDate = startDate.ToString("dd/MM/yyyy");
                }

            }
            response.skills.skillsList = finalData;

            return response;
        }

        public void AddAmendEmployeeSkills(SkillsAndQualificationsDetail request)
        {
            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    E_QUALIFICATIONSANDSKILLS skillObj = new E_QUALIFICATIONSANDSKILLS();
                    if (request.qualificationId > 0)
                    {
                        var skillResult = (from S in DbContext.E_QUALIFICATIONSANDSKILLS where (S.QUALIFICATIONSID == request.qualificationId) select S).FirstOrDefault();
                        skillObj = skillResult;
                    }

                    skillObj.EMPLOYEEID = request.employeeId;
                    skillObj.IsActive = true;

                    skillObj.LASTACTIONTIME = DateTime.Now;
                    skillObj.LASTACTIONUSER = request.createdBy;

                    if (request.qualificationDate != null && request.qualificationDate != string.Empty)
                        skillObj.QUALIFICATIONDATE = Convert.ToDateTime(request.qualificationDate);
                    skillObj.QualLevelID = request.qualLevelID;
                    skillObj.RESULT = request.result;
                    skillObj.SUBJECT = request.subject;
                    skillObj.EquivalentLevel = request.equivalentLevel;
                    if (request.qualificationId == 0)
                    {
                        DbContext.E_QUALIFICATIONSANDSKILLS.Add(skillObj);
                    }
                    DbContext.SaveChanges();

                    List<E_ProfessionalQualifications> profQualList = new List<E_ProfessionalQualifications>();
                    profQualList = (from S in DbContext.E_ProfessionalQualifications where (S.QualificationId == skillObj.QUALIFICATIONSID) select S).ToList();
                    if (profQualList.Count() > 0)
                    {
                        foreach (E_ProfessionalQualifications item in profQualList)
                        {
                            DbContext.E_ProfessionalQualifications.Remove(item);
                        }
                    }

                    foreach (var item in request.professionalQualification)
                    {
                        E_ProfessionalQualifications profQual = new E_ProfessionalQualifications();
                        profQual.Description = item;
                        profQual.Active = true;
                        profQual.QualificationId = skillObj.QUALIFICATIONSID;
                        DbContext.E_ProfessionalQualifications.Add(profQual);

                    }
                    DbContext.SaveChanges();
                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }

            }
        }

        public void DeleteEmployeeSkills(DeleteEmployeeSkills request)
        {
            E_QUALIFICATIONSANDSKILLS skillObj = new E_QUALIFICATIONSANDSKILLS();
            if (request.qualificationId > 0)
            {
                var skillResult = (from S in DbContext.E_QUALIFICATIONSANDSKILLS where (S.QUALIFICATIONSID == request.qualificationId) select S).FirstOrDefault();
                skillObj = skillResult;
            }
            skillObj.IsActive = false;
            skillObj.LASTACTIONUSER = request.updatedBy;
            skillObj.LASTACTIONTIME = DateTime.Now;
            DbContext.SaveChanges();
        }

        public List<LanguageSkills> GetLanguageSkills(int employeeId)
        {
            List<LanguageSkills> response = new List<LanguageSkills>();
            var data = (from S in DbContext.E_EmployeeLanguageSkills
                        join t in DbContext.E_LanguageTypes on S.LanguageTypeId equals t.LanguageTypeId
                        where S.EmployeeId == employeeId
                        select new LanguageSkills
                        {
                            languageId=S.LanguageId,
                            employeeId = S.EmployeeId,
                            isSpoken = (S.IsSpoken ?? false),
                            createdBy = S.CreatedBy,
                            languageOther = S.Language,
                            isWritten = (S.IsWritten ?? false),
                            languageTypeId = S.LanguageTypeId,
                            language = t.Title
                        });
            if (data.Count() > 0)
            {
                response = data.ToList();
                foreach (LanguageSkills item in response)
                {
                    if (item.isWritten == true && item.isSpoken == true)
                    {
                        item.spokenWritten = "Spoken/Written";
                    }
                    else if (item.isSpoken == true)
                    {
                        item.spokenWritten = "Spoken";
                    }
                    else if (item.isWritten == true)
                    {
                        item.spokenWritten = "Written";
                    }
                    if (item.language == "Other")
                    {
                        item.language = item.languageOther;
                    }

                }
            }

            return response;
        }

        public void AddLanguageSkills(List<LanguageSkills> request)
        {
            if (request.Count > 0)
            {
                int empId = (int)request[0].employeeId;
                var catData = (from c in DbContext.E_EmployeeLanguageSkills
                              where c.EmployeeId == empId
                               select c);
                if (catData.Count() > 0)
                {
                    DbContext.E_EmployeeLanguageSkills.RemoveRange(catData.ToList());
                }
                List<E_EmployeeLanguageSkills> skillsList = new List<E_EmployeeLanguageSkills>();
                foreach (LanguageSkills item in request)
                {
                    E_EmployeeLanguageSkills skill = new E_EmployeeLanguageSkills();
                    skill.CreatedBy = item.createdBy;
                    skill.CreatedDate = DateTime.Now;
                    skill.EmployeeId = item.employeeId;
                    skill.IsSpoken = item.isSpoken;
                    skill.IsWritten = item.isWritten;
                    skill.Language = item.languageOther;
                    skill.LanguageTypeId = item.languageTypeId;
                    skillsList.Add(skill);
                }
                DbContext.E_EmployeeLanguageSkills.AddRange(skillsList);
                DbContext.SaveChanges();
            }
        }

    }
}
