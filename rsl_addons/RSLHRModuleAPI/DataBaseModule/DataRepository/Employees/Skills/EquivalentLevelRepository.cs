﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Employees
{
    public class EquivalentLevelRepository : BaseRepository<E_EquivalentLevel>
    {
        public EquivalentLevelRepository(Entities dbContext) : base(dbContext)
        {

        }
    }
}
