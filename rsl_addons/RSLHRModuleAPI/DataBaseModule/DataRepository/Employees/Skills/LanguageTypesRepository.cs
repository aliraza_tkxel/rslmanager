﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository
{
   public class LanguageTypesRepository : BaseRepository<E_LanguageTypes>
    {
        public LanguageTypesRepository(Entities dbContext) : base(dbContext)
        {

        }
    }
}
