﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataBaseModule.DataRepository
{
    public class GiftRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public GiftRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }

        #region Helpers

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }
        private F_FISCALYEARS getFiscalYear(int yRange)
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YRange == yRange).FirstOrDefault();

            return fYear;
        }


        private int GetTeamDirector(int employeeId)
        {
            var directorId = (from e in DbContext.E__EMPLOYEE
                                 join jrt in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals jrt.EMPLOYEEID
                              join t in DbContext.E_TEAM on jrt.TEAM equals t.TEAMID
                                 where e.EMPLOYEEID == employeeId
                                 select t.DIRECTOR).FirstOrDefault();
            if (directorId.HasValue)
            {
                return directorId.Value;
            }

            return 0;
        }

        private int GetChiefExecutive()
        {
            return (from j in DbContext.E_JOBROLE
                           join e in DbContext.E_JOBDETAILS on j.JobeRoleDescription equals e.JOBTITLE
                           where j.JobeRoleDescription == "Group Chief Executive"
                           select e.EMPLOYEEID).FirstOrDefault();
        }

        private void SendEmailNotification(PaypointEmailNotification emailObj)
        {
            if (emailObj != null && emailObj.lineManagerEmail != null)
            {
                string body = string.Empty;
                body = "Dear " + emailObj.lineManagerName + ", <br /><br />";
                if (emailObj.status == ApplicationConstants.authorized)
                {
                    body = body + "The request for {0}’s pay point to be increased from {1} to {2} has been approved by the Group CEO and the Executive Team, ";
                    body = body + "therefore their new salary will be £{3}. This will take eﬀect from the next pay day  <br /><br />";
                    body = body + "You may let {0} know – HR will also be sending a letter to them confirming the change. <br /><br />";
                    body = body + "If you have any questions on the content of this email, please contact a member of the HR Team. ";
                    body = string.Format(body, emailObj.employeeName, emailObj.currentGradePoint, emailObj.newGradePoint, emailObj.newSalary);
                }

                if (emailObj.status == "Declined")
                {
                    body = body + "The Pay Point Application for {0} has not been successful. They are eligible for a further application from {1}.";
                    var date = "";
                    if (emailObj.nextPayPointReviewDate.HasValue)
                    {
                        date = emailObj.nextPayPointReviewDate.Value.ToShortDateString();
                    }
                    body = string.Format(body, emailObj.employeeName, date);
                }

                string subject = "Pay Point Application";
                EmailHelper.sendHtmlFormattedEmail(emailObj.lineManagerName, emailObj.lineManagerEmail, subject, body);
            }
        }

        private void SendEmailNotificationForGifts(GiftsEmailNotification emailObj)
        {
            if (emailObj != null && emailObj.employeeEmail != null)
            {
                string body = string.Empty;
                body = "Dear " + emailObj.employeeName + ", <br /><br />";
                if (emailObj.status == "Approved")
                {

                    body = body + "The Gift &Hospitality item you submitted for approval for the following has been <b>{0}</b>: <br />";
                    body = body + "Details: {1} <br />";
                    body = body + "Received: {2} <br />";
                    body = body + "Value: £{3} <br />";
                    body = body + "By: {4} <br />";
                    body = string.Format(body, emailObj.status, emailObj.details, emailObj.givenReceivedDate, emailObj.value, emailObj.lineManagerName);
                }

                if (emailObj.status == "Rejected")
                {
                    body = body + "The Gift &Hospitality item you submitted for approval for the following has been <b>{0}</b>: <br />";
                    body = body + "Details: {1} <br />";
                    body = body + "Received: {2} <br />";
                    body = body + "Value: £{3} <br />";
                    body = body + "By: {4} <br />";
                    body = body + "Reason: {5} <br />";
                    body = string.Format(body, emailObj.status, emailObj.details, emailObj.givenReceivedDate, emailObj.value, emailObj.lineManagerName, emailObj.reason);
                }

                if (emailObj.status == "Declared")
                {
                    //The Gift &Hospitality item you submitted has been Declared: 
                    //Details: Lunch at Starbucks Received:  12 / 07 / 2018 Date of declaration: 16 / 07 / 2018 Value:   £8.50 By:   < Line Manager / HR Team member Name >
                    body = body + "The Gift &Hospitality item you submitted has been <b>{0}</b>: <br />";
                    body = body + "Details: {1} <br />";
                    body = body + "Received: {2} <br />";
                    body = body + "Date of declaration: {3} <br />";
                    body = body + "Value: £{4} <br />";
                    body = body + "By: {5} <br />";
                    body = string.Format(body, emailObj.status, emailObj.details, emailObj.givenReceivedDate, emailObj.dateOfDeclaration, emailObj.value, emailObj.lineManagerName);
                }

                if (!string.IsNullOrEmpty(emailObj.acceptedMessage))
                {
                    body = body + emailObj.acceptedMessage + "<br />";
                }
                string subject = string.Format(EmailSubjectConstants.GiftStatus, emailObj.status);
                EmailHelper.sendHtmlFormattedEmail(emailObj.employeeName, emailObj.employeeEmail, subject, body);
            }
        }

        #endregion

        public List<Gift> GetGiftsForEmployee(int employeeId)
        {
            var fiscalYear = getCurrentFiscalYear();

            var data = (from g in DbContext.E_GiftsAndHospitalities
                        join gs in DbContext.E_Gift_Status on g.GiftStatusId equals gs.GiftStatusId
                        where g.EmployeeId == employeeId && g.CreatedDate >= fiscalYear.YStart
                        select new Gift()
                        {
                            giftId = g.GiftId,
                            accepted = g.Accepted,
                            approximateValue = g.ApproximateValue,
                            createdDate = g.CreatedDate,
                            dateGivenReceived = g.DateGivenReceived,
                            dateOfDeclaration = g.DateOfDeclaration,
                            details = g.Details,
                            isGivenReceived = g.IsGivenReceived,
                            notApprovedDate = g.NotApprovedDate,
                            notes = g.Notes,
                            reason = g.Reason,
                            offerToBy = g.OfferToBy,
                            status = gs.Description,
                            employeeId = g.EmployeeId,
                            fiscalYearId = g.fiscalYearId,
                            isAccepted = g.IsAccepted
                        }).ToList();
            if (data == null || data.Count == 0)
            {
                return new List<Gift>();
            }
            else
            {
                foreach (var item in data)
                {
                    if (item.isGivenReceived == ApplicationConstants.isGiftReceived)
                    {
                        item.dateRevieved = item.dateGivenReceived;
                        item.offeredTo = item.offerToBy;
                    }
                    else if (item.isGivenReceived == ApplicationConstants.isGiftGiven)
                    {
                        item.dateGiven = item.dateGivenReceived;
                        item.offeredBy = item.offerToBy;
                    }
                    
                }
                return data;
            }
        }

        public GiftMasterResponse GetEmployeeGiftMaster(GiftsRequest request)
        {
            var response = new GiftMasterResponse();

            var fiscalYear = getCurrentFiscalYear();

            if (string.IsNullOrEmpty(request.from))
            {
                response.from = fiscalYear.YStart.Value.ToShortDateString();
            }
            else
            {
                fiscalYear.YStart = GeneralHelper.GetDateTimeFromString(request.from);
                response.from = request.from;
            }

            if (string.IsNullOrEmpty(request.to))
            {
                response.to = fiscalYear.YEnd.Value.ToShortDateString();
            }
            else
            {
                fiscalYear.YEnd = GeneralHelper.GetDateTimeFromString(request.to);
                response.to = request.to;
            }

            var data = (from g in DbContext.E_GiftsAndHospitalities
                        join gs in DbContext.E_Gift_Status on g.GiftStatusId equals gs.GiftStatusId
                        where g.EmployeeId == request.employeeId && 
                        (g.GiftStatusId == request.statusId || request.statusId == 0 ) &&
                        (g.IsGivenReceived == request.typeId || request.typeId == 0) &&
                        (g.DateGivenReceived >= fiscalYear.YStart && g.DateGivenReceived <= fiscalYear.YEnd)
                        select new Gift()
                        {
                            giftId = g.GiftId,
                            accepted = g.Accepted,
                            approximateValue = g.ApproximateValue,
                            createdDate = g.CreatedDate,
                            dateGivenReceived = g.DateGivenReceived,
                            dateOfDeclaration = g.DateOfDeclaration,
                            details = g.Details,
                            isGivenReceived = g.IsGivenReceived,
                            notApprovedDate = g.NotApprovedDate,
                            notes = g.Notes,
                            offerToBy = g.OfferToBy,
                            status = gs.Description,
                            employeeId = g.EmployeeId,
                            fiscalYearId = g.fiscalYearId,
                            isAccepted = g.IsAccepted
                        }).ToList();
            if (data != null && data.Count > 0)
            {
                response.giftList = data;
            }

            return response;
        }

        public bool AddGifts(GiftAndHospitality giftsObj)
        {
            // adding document
            //int documentId = AddGiftDocument(giftsObj.documentPath, giftsObj.employeeId, giftsObj.createdBy);
            var fiscalYear = getCurrentFiscalYear();

            var statusId = DbContext.E_Gift_Status.FirstOrDefault(e => e.Description == "Pending Approval").GiftStatusId;
            foreach (Gift gift in giftsObj.gifts)
            {
                var data = (from g in DbContext.E_GiftsAndHospitalities
                            where g.GiftId == gift.giftId
                            select g).FirstOrDefault();

                E_GiftsAndHospitalities newGift;
                if (data == null)
                {
                    newGift = new E_GiftsAndHospitalities();
                }
                else
                {
                    newGift = data;
                }
                newGift.Accepted = gift.accepted;
                newGift.ApproximateValue = gift.approximateValue;
                newGift.CreatedDate = gift.createdDate;
                newGift.DateGivenReceived = gift.dateGivenReceived;
                newGift.DateOfDeclaration = gift.dateOfDeclaration;
                newGift.Details = gift.details;
                newGift.EmployeeId = gift.employeeId;
                newGift.IsGivenReceived = gift.isGivenReceived;
                newGift.NotApprovedDate = gift.notApprovedDate;
                newGift.Notes = gift.notes;
                newGift.OfferToBy = gift.offerToBy;
                newGift.fiscalYearId = fiscalYear.YRange;
                //newGift.documentId = documentId;
                newGift.EmployeeId = giftsObj.employeeId;
                newGift.GiftStatusId = statusId;
                newGift.IsAccepted = gift.isAccepted;

                DbContext.E_GiftsAndHospitalities.Add(newGift);
                DbContext.SaveChanges();
            }

            return true;
        }

        public bool UpdateGiftStatus(Gift giftObj)
        {
            var statusId = DbContext.E_Gift_Status.FirstOrDefault(e => e.Description == giftObj.status).GiftStatusId;

            var data = (from g in DbContext.E_GiftsAndHospitalities
                        join emp in DbContext.E__EMPLOYEE on g.EmployeeId equals emp.EMPLOYEEID
                        join empContact in DbContext.E_CONTACT on emp.EMPLOYEEID equals empContact.EMPLOYEEID
                        join lineManager in DbContext.E__EMPLOYEE on giftObj.updatedBy equals lineManager.EMPLOYEEID
                        where g.GiftId == giftObj.giftId
                        select new { g, emp, lineManager, empContact }).FirstOrDefault();

            if (giftObj.notApprovedDate == null)
            {
                giftObj.notApprovedDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            }

            if (data != null)
            {
                data.g.GiftStatusId = statusId;
                data.g.UpdatedBy = giftObj.updatedBy;
                data.g.UpdatedDate = giftObj.updatedDate;
                //data.g.DateOfDeclaration = giftObj.dateOfDeclaration;
                data.g.NotApprovedDate = giftObj.notApprovedDate;
                data.g.Reason = giftObj.reason;
            }
            DbContext.SaveChanges();
            // send email notification to employee
            var emailobj = new GiftsEmailNotification();

            if (data.g.DateGivenReceived != null)
            {
                emailobj.givenReceivedDate = data.g.DateGivenReceived.Value.ToShortDateString();
            }

            if (giftObj.dateOfDeclaration != null)
            {
                emailobj.dateOfDeclaration = giftObj.dateOfDeclaration.Value.ToShortDateString();
            }

            if (data.g.ApproximateValue != null)
            {
                emailobj.value = data.g.ApproximateValue.Value.ToString();
            }

            if (data.g.IsGivenReceived == ApplicationConstants.isGiftReceived)
            {
                if (data.g.IsAccepted.HasValue && data.g.IsAccepted.Value == 1)
                {
                    emailobj.acceptedMessage = "I accepted this: Gift/Hospitality";
                }
                else
                {
                    emailobj.acceptedMessage = "I did not accept this: Gift/Hospitality";
                }
            }
            else if (data.g.IsGivenReceived == ApplicationConstants.isGiftGiven)
            {
                if (data.g.IsAccepted.HasValue && data.g.IsAccepted.Value == 1)
                {
                    emailobj.acceptedMessage = "They accepted this: Gift/Hospitality";
                }
                else
                {
                    emailobj.acceptedMessage = "They did not accept this: Gift/Hospitality";
                }
            }

            emailobj.status = giftObj.status;
            emailobj.details = data.g.Details;
            emailobj.reason = giftObj.reason;
            emailobj.employeeName = data.emp.FIRSTNAME + " " + data.emp.LASTNAME;
            emailobj.employeeEmail = data.empContact.WORKEMAIL; 
            emailobj.lineManagerName = data.lineManager.FIRSTNAME + " " + data.lineManager.LASTNAME;

            SendEmailNotificationForGifts(emailobj);

            return true;
        }

        public decimal GetGradePointSalary(GradePointSalary gradePoint)
        {
            var fiscalYearId = getCurrentFiscalYear().YRange;

            var salary = (from gps in DbContext.E_GradePointSalary
                              where gps.Grade == gradePoint.grade &&
                              gps.GradePoint == gradePoint.point &&
                              gps.fiscalYearId == fiscalYearId
                              select gps.Salary).FirstOrDefault();

            return Convert.ToDecimal(salary);
        }

        //private int AddGiftDocument(string name, int employeeId, int createdBy)
        //{
        //    int referenceDocType = (from r in DbContext.HR_DocumentType
        //                            where r.Description == "Gifts & Hospitality"
        //                            select r.TypeId).FirstOrDefault();
        //    HR_Documents document = new HR_Documents();
        //    document.DocumentTypeId = referenceDocType;
        //    document.EmployeeID = employeeId;
        //    document.DocumentDate = DateTime.Now;
        //    document.IsActive = true;
        //    document.CreateDate = DateTime.Now;
        //    document.CreateBy = createdBy;
        //    document.DocumentPath = name;
        //    DbContext.HR_Documents.Add(document);
        //    DbContext.SaveChanges();
        //    return document.DocumentId;
        //}

        public PaypointDetail GetPaypointForEmployee(int employeeId)
        {
            var data = (from d in DbContext.E_JOBDETAILS
                        join jr in DbContext.E_JOBROLE on d.JobRoleId equals jr.JobRoleId
                        join g in DbContext.E_GRADE on d.GRADE equals g.GRADEID into g_join
                        from g in g_join.DefaultIfEmpty() 
                        join gp in DbContext.E_GRADE_POINT on d.GRADEPOINT equals gp.POINTID into gp_join
                        from gp in gp_join.DefaultIfEmpty()
                        where d.EMPLOYEEID == employeeId
                        select new { d, g, gp.POINTDESCRIPTION, jr }).FirstOrDefault();
            var employeeData = (from e in DbContext.E__EMPLOYEE
                                where e.EMPLOYEEID == employeeId
                                select e).FirstOrDefault();
            int declined = (from s in DbContext.E_PayPointStatus
                            where s.Description == ApplicationConstants.declined
                            select s).FirstOrDefault().PayPointStatusId;
            int authorized = (from s in DbContext.E_PayPointStatus
                              where s.Description == ApplicationConstants.authorized
                              select s).FirstOrDefault().PayPointStatusId;

            var payPointObject = (from p in DbContext.E_PaypointSubmission
                                  join esub in DbContext.E__EMPLOYEE on p.ApprovedBy equals esub.EMPLOYEEID into esub_join
                                  from esub in esub_join.DefaultIfEmpty()
                                  join esup in DbContext.E__EMPLOYEE on p.SupportedBy equals esup.EMPLOYEEID into esup_join
                                  from esup in esup_join.DefaultIfEmpty()
                                  join eauth in DbContext.E__EMPLOYEE on p.AuthorizedBy equals eauth.EMPLOYEEID into eauth_join
                                  from eauth in eauth_join.DefaultIfEmpty()
                                  join g in DbContext.E_GRADE on p.Grade equals g.GRADEID into g_join
                                  from g in g_join.DefaultIfEmpty()
                                  join gp in DbContext.E_GRADE_POINT on p.GradePoint equals gp.POINTID into gp_join
                                  from gp in gp_join.DefaultIfEmpty()
                                  join s in DbContext.E_PayPointStatus on p.paypointStatusId equals s.PayPointStatusId into s_join
                                  from s in s_join.DefaultIfEmpty()
                                  where p.employeeId == employeeId && (p.paypointStatusId != declined && p.paypointStatusId != authorized)
                                  select new { p, esub, esup, eauth, g, gp, s }).FirstOrDefault();

            //var gradeAndPoint = (from d in DbContext.E_JOBDETAILS
            //                     join g in DbContext.E_GRADE on d.GRADE equals g.GRADEID
            //                     join gp in DbContext.E_GRADE_POINT on d.GRADEPOINT equals gp.POINTID
            //                     where d.EMPLOYEEID == employeeId
            //                     select new { g, gp }).FirstOrDefault();

            PaypointDetail detail = new PaypointDetail();
            detail.payPoint = new Paypoint();
            detail.employeeName = employeeData.FIRSTNAME + " " + employeeData.LASTNAME;

            if (data != null)
            {
                detail.isBRS = data.d.ISBRS;
                detail.grade = data.g.DESCRIPTION;
                detail.payPoint.grade = data.g.GRADEID;
                detail.gradePoint = data.POINTDESCRIPTION;
                if (data.d.SALARY != null)
                {
                    detail.salary = Math.Round(Convert.ToDecimal(data.d.SALARY.ToString()), 2);

                }

                detail.jobTitle = data.jr.JobeRoleDescription;
                detail.lineManager = data.d.LINEMANAGER;
            }

            detail.ceoId = GetChiefExecutive();
            detail.execDirectorId = GetTeamDirector(employeeId);

            if (payPointObject == null || payPointObject.p.paypointStatusId == 5)
            {
                detail.payPoint = new Paypoint();
                if (data != null)
                {
                    detail.payPoint.grade = data.g.GRADEID;
                    detail.payPoint.currentSalary = detail.salary;
                }

                if (payPointObject != null)
                {
                    detail.payPoint.paypointId = payPointObject.p.PaypointId;
                    detail.payPoint.paypointStatusId = payPointObject.s.PayPointStatusId;
                    detail.payPoint.paypointStatusDescription = payPointObject.s.Description;
                }

                return detail;
            }
            detail.payPoint.approved = payPointObject.p.Approved;
            detail.payPoint.approvedBy = payPointObject.p.ApprovedBy;
            detail.payPoint.approvedDate = Convert.ToDateTime(payPointObject.p.ApprovedDate).ToString("dd/MM/yyyy");
            detail.payPoint.authorized = payPointObject.p.Authorized;
            detail.payPoint.authorizedBy = payPointObject.p.AuthorizedBy;
            detail.payPoint.authorizedDate = Convert.ToDateTime(payPointObject.p.AuthorizedDate).ToString("dd/MM/yyyy");
            detail.payPoint.dateProposed = Convert.ToDateTime(payPointObject.p.DateProposed).ToString("dd/MM/yyyy");
            detail.payPoint.detailRationale = payPointObject.p.DetailRationale;
            detail.payPoint.gradeDescription = payPointObject.g.DESCRIPTION;
            if (payPointObject.gp != null)
            {
                detail.payPoint.gradePointDescription = payPointObject.gp.POINTDESCRIPTION;
            }
            detail.payPoint.proposedSalary = Math.Round(Convert.ToDecimal(payPointObject.p.Salary), 2);
            detail.payPoint.supported = payPointObject.p.Supported;
            detail.payPoint.supportedBy = payPointObject.p.SupportedBy;
            detail.payPoint.supportedDate = Convert.ToDateTime(payPointObject.p.SupportedDate).ToString("dd/MM/yyyy");
            detail.payPoint.employeeId = payPointObject.p.employeeId;
            detail.payPoint.paypointId = payPointObject.p.PaypointId;
            detail.payPoint.approvedByName = payPointObject.esub.FIRSTNAME + " " + payPointObject.esub.LASTNAME;
            detail.payPoint.paypointStatusDescription = payPointObject.s.Description;
            detail.payPoint.reason = payPointObject.p.Reason;
            detail.payPoint.isNotSubmitted = payPointObject.p.IsNotSubmitted;

            if (payPointObject.esup != null)
            {
                detail.payPoint.supportedByName = payPointObject.esup.FIRSTNAME + " " + payPointObject.esup.LASTNAME;
            }
            if (payPointObject.eauth != null)
            {
                detail.payPoint.authorizedByName = payPointObject.eauth.FIRSTNAME + " " + payPointObject.eauth.LASTNAME;
            }
            detail.payPoint.currentSalary = detail.salary;
            if (data != null)
            {
                detail.payPoint.grade = data.g.GRADEID;
            }

            return detail;
        }

        public PaypointDetail GetPaypointDetailByPaypointId(int paypointId)
        {
            var payPointObject = (from p in DbContext.E_PaypointSubmission_HISTORY
                                  join esub in DbContext.E__EMPLOYEE on p.ApprovedBy equals esub.EMPLOYEEID into esub_join
                                  from esub in esub_join.DefaultIfEmpty()
                                  join esup in DbContext.E__EMPLOYEE on p.SupportedBy equals esup.EMPLOYEEID into esup_join
                                  from esup in esup_join.DefaultIfEmpty()
                                  join eauth in DbContext.E__EMPLOYEE on p.AuthorizedBy equals eauth.EMPLOYEEID into eauth_join
                                  from eauth in eauth_join.DefaultIfEmpty()
                                  join g in DbContext.E_GRADE on p.Grade equals g.GRADEID into g_join
                                  from g in g_join.DefaultIfEmpty()
                                  join gp in DbContext.E_GRADE_POINT on p.GradePoint equals gp.POINTID into gp_join
                                  from gp in gp_join.DefaultIfEmpty()
                                  join s in DbContext.E_PayPointStatus on p.paypointStatusId equals s.PayPointStatusId into s_join
                                  from s in s_join.DefaultIfEmpty()
                                  where p.PaypointId == paypointId
                                  orderby p.PaypointHistoryId descending
                                  select new { p, esub, esup, eauth, g, gp, s }).FirstOrDefault();

            if (payPointObject == null)
            {
                return new PaypointDetail();
            }

            var data = (from d in DbContext.E_JOBDETAILS
                        join jr in DbContext.E_JOBROLE on d.JobRoleId equals jr.JobRoleId
                        join g in DbContext.E_GRADE on d.GRADE equals g.GRADEID into g_join
                        from g in g_join.DefaultIfEmpty()
                        join gp in DbContext.E_GRADE_POINT on d.GRADEPOINT equals gp.POINTID into gp_join
                        from gp in gp_join.DefaultIfEmpty()
                        where d.EMPLOYEEID == payPointObject.p.employeeId
                        select new { d, jr }).FirstOrDefault();
            var employeeData = (from e in DbContext.E__EMPLOYEE
                                where e.EMPLOYEEID == payPointObject.p.employeeId
                                select e).FirstOrDefault();
           
            PaypointDetail detail = new PaypointDetail();
            detail.payPoint = new Paypoint();
            detail.employeeName = employeeData.FIRSTNAME + " " + employeeData.LASTNAME;

            if (data != null)
            {
                detail.isBRS = data.d.ISBRS;
                if (data.d.SALARY != null)
                {
                    detail.salary = Math.Round(Convert.ToDecimal(data.d.SALARY.ToString()), 2);

                }

                detail.jobTitle = data.jr.JobeRoleDescription;
                detail.lineManager = data.d.LINEMANAGER;
            }

            detail.ceoId = GetChiefExecutive();
            detail.execDirectorId = GetTeamDirector(payPointObject.p.employeeId.Value);

            if (payPointObject == null || payPointObject.p.paypointStatusId == 5)
            {
                detail.payPoint = new Paypoint();
                if (data != null)
                {
                    detail.payPoint.currentSalary = detail.salary;
                }

                if (payPointObject != null)
                {
                    detail.payPoint.paypointId = payPointObject.p.PaypointId;
                    detail.payPoint.paypointStatusId = payPointObject.s.PayPointStatusId;
                    detail.payPoint.paypointStatusDescription = payPointObject.s.Description;
                    detail.payPoint.reason = payPointObject.p.Reason;
                    detail.payPoint.isNotSubmitted = payPointObject.p.IsNotSubmitted;
                }

                return detail;
            }

            detail.payPoint.approved = payPointObject.p.Approved;
            detail.payPoint.approvedBy = payPointObject.p.ApprovedBy;
            detail.payPoint.approvedDate = Convert.ToDateTime(payPointObject.p.ApprovedDate).ToString("dd/MM/yyyy");
            detail.payPoint.authorized = payPointObject.p.Authorized;
            detail.payPoint.authorizedBy = payPointObject.p.AuthorizedBy;
            detail.payPoint.authorizedDate = Convert.ToDateTime(payPointObject.p.AuthorizedDate).ToString("dd/MM/yyyy");
            detail.payPoint.dateProposed = Convert.ToDateTime(payPointObject.p.DateProposed).ToString("dd/MM/yyyy");
            detail.payPoint.detailRationale = payPointObject.p.DetailRationale;
            detail.payPoint.gradeDescription = payPointObject.g.DESCRIPTION;
            detail.payPoint.gradePointDescription = payPointObject.gp.POINTDESCRIPTION;
            detail.payPoint.proposedSalary = Math.Round(Convert.ToDecimal(payPointObject.p.Salary), 2);
            detail.payPoint.supported = payPointObject.p.Supported;
            detail.payPoint.supportedBy = payPointObject.p.SupportedBy;
            detail.payPoint.supportedDate = Convert.ToDateTime(payPointObject.p.SupportedDate).ToString("dd/MM/yyyy");
            detail.payPoint.employeeId = payPointObject.p.employeeId;
            detail.payPoint.paypointId = payPointObject.p.PaypointId;
            detail.payPoint.approvedByName = payPointObject.esub.FIRSTNAME + " " + payPointObject.esub.LASTNAME;
            detail.payPoint.paypointStatusDescription = payPointObject.s.Description;
            detail.payPoint.reason = payPointObject.p.Reason;
            detail.payPoint.isNotSubmitted = payPointObject.p.IsNotSubmitted;

            if (payPointObject.esup != null)
            {
                detail.payPoint.supportedByName = payPointObject.esup.FIRSTNAME + " " + payPointObject.esup.LASTNAME;
            }
            if (payPointObject.eauth != null)
            {
                detail.payPoint.authorizedByName = payPointObject.eauth.FIRSTNAME + " " + payPointObject.eauth.LASTNAME;
            }
            detail.payPoint.currentSalary = detail.salary;
            
            return detail;


        }


        public bool AddPayPointSubmission(Paypoint payPointObj)
        {

            var fiscalYearId = getCurrentFiscalYear().YRange;
            var status = (from s in DbContext.E_PayPointStatus
                          where s.Description == ApplicationConstants.submitted
                          select s).FirstOrDefault();

            DatabaseEntities.E_PaypointSubmission payPoint;

            if (payPointObj.paypointId > 0)
            {
                payPoint = DbContext.E_PaypointSubmission.FirstOrDefault(e => e.PaypointId == payPointObj.paypointId);
            }
            else
            {
                payPoint = new E_PaypointSubmission();
            }

            payPoint.paypointStatusId = status.PayPointStatusId;
            payPoint.Reason = payPointObj.reason;
            payPoint.IsNotSubmitted = payPointObj.isNotSubmitted;
            if (payPointObj.approved.HasValue && payPointObj.approved.Value)
            {
                payPoint.Approved = payPointObj.approved;
                payPoint.ApprovedBy = payPointObj.approvedBy;
                if (payPointObj.approvedDate != null && payPointObj.approvedDate != string.Empty)
                    payPoint.ApprovedDate = Convert.ToDateTime(payPointObj.approvedDate);
            }

            if (payPointObj.waiting.HasValue && payPointObj.waiting.Value)
            {
                status = (from s in DbContext.E_PayPointStatus
                              where s.Description == ApplicationConstants.waitingSubmission
                              select s).FirstOrDefault();
                payPoint.paypointStatusId = status.PayPointStatusId;
                payPoint.Reason = payPointObj.reason;
                payPoint.IsNotSubmitted = true;
            }
            else
            {
                payPoint.IsNotSubmitted = false;
            }

            payPoint.Authorized = payPointObj.authorized;
            payPoint.AuthorizedBy = payPointObj.authorizedBy;
            if (payPointObj.authorizedDate != null && payPointObj.authorizedDate != string.Empty)
                payPoint.AuthorizedDate = Convert.ToDateTime(payPointObj.authorizedDate);
            if (payPointObj.dateProposed != null && payPointObj.dateProposed != string.Empty)
                payPoint.DateProposed = Convert.ToDateTime(payPointObj.dateProposed);
            payPoint.DetailRationale = payPointObj.detailRationale;
            payPoint.employeeId = payPointObj.employeeId;
            payPoint.Grade = payPointObj.grade;
            payPoint.GradePoint = payPointObj.gradePoint;
            payPoint.Salary = Convert.ToDouble(payPointObj.proposedSalary);
            payPoint.Supported = payPointObj.supported;
            payPoint.SupportedBy = payPointObj.supportedBy;
            if (payPointObj.supportedDate != null && payPointObj.supportedDate != string.Empty)
                payPoint.SupportedDate = Convert.ToDateTime(payPointObj.supportedDate);
            payPoint.fiscalYearId = fiscalYearId;

            if (payPointObj.paypointId == 0)
                DbContext.E_PaypointSubmission.Add(payPoint);
            
            DbContext.SaveChanges();
            return true;
        }

        public PayppointReportResponce GetAllPayPoints(PaypointReportRequest request)
        {
            var fData = getCurrentFiscalYear();
            DateTime? StartDateRange = null;
            DateTime? EndDateRange = null;

            if (request.fiscalYearId != 0)
            {
                fData = getFiscalYear(request.fiscalYearId);
            }

            if (!string.IsNullOrEmpty(request.fiscalYearStartDate))
            {
                StartDateRange = DateTime.Parse(request.fiscalYearStartDate);
            }

            if (!string.IsNullOrEmpty(request.fiscalYearEndDate))
            {
                EndDateRange = DateTime.Parse(request.fiscalYearEndDate);
            }

            PayppointReportResponce response = new PayppointReportResponce();
            response.PayPointList = new PayPointListing();
            var data = DbContext.E_GetPayPointReport(request.searchText, request.status, request.teamId, request.directorate, request.fiscalYearId, StartDateRange, EndDateRange, request.notSubmitted)
                .Select(e => new PayPointReport()
                {
                    team = e.team,
                    directorate = e.directorate,
                    gradeDescription = e.gradeDescription,
                    gradePointDescription = e.gradePointDescription,
                    name = e.name,
                    submittedByName = e.submittedByName,
                    reviewDate = e.reviewDate,
                    //paypointReviewDate = (DateTime)e.paypointReviewDate,
                    statusDisplay = e.statusDisplay,
                    status = e.status,
                    employeeId = e.employeeId,
                    payPointId = e.payPointId,
                    SupportedBy = e.supportedBy,
                    AuthorizedBy = e.authorizedBy,
                    imagePath = e.imagePath,
                    reason = e.reason
                }).ToList();

            Pagination page = new Pagination();
            page.totalRows = data.Count();
            page.totalPages = (int)Math.Ceiling((double)data.Count / request.pagination.pageSize);
            page.pageNo = request.pagination.pageNo;
            page.pageSize = request.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.PayPointList.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = new List<PayPointReport>();

            string _sortBy = @"";
            if (request.sortBy == null || request.sortBy == "reviewDate")
            {
                _sortBy = @"paypointReviewDate";
            }
            else
            {
                _sortBy = request.sortBy;
            }
            // Getting sorted by sort parameter
            if (request.sortOrder == "ASC")
            {
                finalData = data.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
            }
            else
            {
                finalData = data.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
            }

            List<PaypointReportDetail> payPoints = new List<PaypointReportDetail>();
            foreach (var item in finalData)
            {
                PaypointReportDetail report = new PaypointReportDetail();
                report.directorate = item.directorate;
                report.grade = item.grade;
                report.gradePoint = item.gradePoint;
                report.name = item.name;
                report.reviewDate = item.reviewDate == "" ? "" : Convert.ToDateTime(item.reviewDate).ToString("dd/MM/yyyy");
                report.status = item.status;
                report.statusDisplay = item.statusDisplay;
                report.employeeId = item.employeeId;
                report.payPointId = item.payPointId;
                report.gradeDescription = item.gradeDescription;
                report.gradePointDescription = item.gradePointDescription;
                report.execDirectorId = GetTeamDirector(item.employeeId);
                report.team = item.team;
                report.submittedByName = item.submittedByName;
                report.reason = item.reason;
                if (item.imagePath != null && item.imagePath.Length > 0)
                {
                    report.imagePath = FileHelper.getLogicalEmployeeProfileImagePath(item.imagePath);
                }
                
                payPoints.Add(report);
            }
            response.PayPointList.payPoints = payPoints;
            response.PayPointList.pagination = page;
            response.ceoId = GetChiefExecutive();
           // response.execDirectorId = GetTeamDirector();
            return response;
        }

        public bool UpdatePaypointStatus(UpdatePaypointRequest request)
        {
            var data = (from p in DbContext.E_PaypointSubmission
                        where p.PaypointId == request.paypointId
                        select p).FirstOrDefault();


            if (data != null)
            {
                var statusData = (from s in DbContext.E_PayPointStatus
                                  where s.PayPointStatusId == request.statusId
                                  select s).FirstOrDefault();

                if (statusData == null)
                {
                    if (request.statusId == 0)
                    {
                        data.SupportedBy = request.lastActionUSer;
                        data.Supported = false;
                        data.SupportedDate = DateTime.Now;
                    }
                }
                else
                {
                    if (statusData.Description == ApplicationConstants.supported)
                    {

                        data.SupportedBy = request.lastActionUSer;
                        data.SupportedDate = DateTime.Now;
                        data.Supported = true;
                        data.paypointStatusId = request.statusId;

                    }
                    else if (statusData.Description == ApplicationConstants.authorized)
                    {
                        data.AuthorizedBy = request.lastActionUSer;
                        data.AuthorizedDate = DateTime.Now;
                        data.Authorized = true;
                        data.paypointStatusId = request.statusId;
                    }
                    else if (statusData.Description == ApplicationConstants.declined && data.SupportedBy == null)
                    {
                        data.SupportedBy = request.lastActionUSer;
                        data.SupportedDate = DateTime.Now;
                        data.Supported = true;
                        data.paypointStatusId = request.statusId;
                    }
                    else if (statusData.Description == ApplicationConstants.declined)
                    {
                        data.AuthorizedBy = request.lastActionUSer;
                        data.AuthorizedDate = DateTime.Now;
                        data.Authorized = false;
                        data.paypointStatusId = request.statusId;
                    }

                    if (statusData.Description == ApplicationConstants.authorized || statusData.Description == ApplicationConstants.declined)
                    {
                        var jobDetail = DbContext.E_JOBDETAILS.Where(e => e.EMPLOYEEID == data.employeeId).FirstOrDefault();
                        if (jobDetail != null && jobDetail.PayPointReviewDate.HasValue)
                        {
                            // pay point review update when CEO authorize
                            if (statusData.Description == ApplicationConstants.authorized)
                            {
                                jobDetail.PayPointReviewDate = jobDetail.PayPointReviewDate.Value.AddMonths(6);

                                //Point 4 Rule for dateProposed
                                //DateProposed should default to the 1st date of each quarter starting 1 January.
                                //For example if the submission is 2 Feb then the proposed date to be effective from should be 1 April
                                //dateProposed should not be editable
                                // important: may effect in POST tab.
                                // information update when CEO authorize

                                if (data.GradePoint == 4)
                                {
                                    var today = DateTime.Now;
                                    jobDetail.PayPointReviewDate = new DateTime((today.Year), 3, 1);
                                    if (today.Month >= 3)
                                    {
                                        jobDetail.PayPointReviewDate = new DateTime((today.Year + 1), 3, 1);
                                    }
                                }

                                // this should be updated when proposed date has arrived.
                                // SQl job written
                                // proposed date was changed to first date of requested month, e.g. user submit 14-04-1028 then proposed date to be effective from is 01-04-2018
                                // in that case SQL job never executed.
                                // so uncomment below code again.
                                jobDetail.GRADE = data.Grade;
                                jobDetail.GRADEPOINT = data.GradePoint;
                                jobDetail.PREVIOUSSALARY = jobDetail.SALARY;
                                jobDetail.SALARY = Convert.ToDecimal(data.Salary);

                                // when CEO mention the next date of eligibilty while authorization
                                if (!string.IsNullOrEmpty(request.nextEligibleDate))
                                {
                                    jobDetail.PayPointReviewDate = GeneralHelper.GetDateTimeFromString(request.nextEligibleDate);
                                }
                            }
                            

                            // send email to line manager when CEO authorized or decline
                            if (data.AuthorizedBy != null)
                            {
                                var emailData = (from e in DbContext.E__EMPLOYEE
                                                 join jd in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals jd.EMPLOYEEID
                                                 join lm in DbContext.E__EMPLOYEE on data.ApprovedBy equals lm.EMPLOYEEID into lm_join
                                                 from lm in lm_join.DefaultIfEmpty()
                                                 join lmc in DbContext.E_CONTACT on lm.EMPLOYEEID equals lmc.EMPLOYEEID
                                                 join cgp in DbContext.E_GRADE_POINT on jd.GRADEPOINT equals cgp.POINTID
                                                 join ngp in DbContext.E_GRADE_POINT on data.GradePoint equals ngp.POINTID
                                                 join pph in DbContext.E_PaypointSubmission_HISTORY on data.PaypointId equals pph.PaypointId into pph_join
                                                 from pph in pph_join.DefaultIfEmpty()
                                                 where e.EMPLOYEEID == data.employeeId
                                                 select new PaypointEmailNotification()
                                                 {
                                                     employeeName = e.FIRSTNAME + " " + e.LASTNAME,
                                                     lineManagerName = lm.FIRSTNAME + " " + lm.LASTNAME,
                                                     lineManagerEmail = lmc.WORKEMAIL,
                                                     currentGradePoint = cgp.POINTDESCRIPTION,
                                                     newGradePoint = ngp.POINTDESCRIPTION,
                                                     newSalary = data.Salary.HasValue ? data.Salary.Value.ToString() : "",
                                                     nextPayPointReviewDate = jobDetail.PayPointReviewDate,
                                                     paypointId = data.PaypointId,
                                                     status = statusData.Description
                                                 }).FirstOrDefault();

                                SendEmailNotification(emailData);
                            }
                        }
                    }
                }

                data.Reason = request.reason;
            }
            DbContext.SaveChanges();
            return true;
        }
    }
}
