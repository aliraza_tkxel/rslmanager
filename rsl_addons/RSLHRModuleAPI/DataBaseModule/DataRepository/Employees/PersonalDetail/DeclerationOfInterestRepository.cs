﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataBaseModule.DataRepository
{
    public class DeclerationOfInterestRepository : BaseRepository<E_DOI>
    {
        public DeclerationOfInterestRepository(Entities dbContext) : base(dbContext)
        {

        }

        #region >>> Helpers <<<

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }


        private int AddDoiDocument(string documentPath, int employeeId, int createdBy)
        {
            HR_Documents document = new HR_Documents();
            int referenceDocType = (from r in DbContext.HR_DocumentType
                                    where r.Description == "DOI"
                                    select r.TypeId).FirstOrDefault();
            int docVisibility = (from r in DbContext.E_DOCVISIBILITY
                                 where r.DOCVISIBILITYDESCRIPTION == "All"
                                 select r.DOCVISIBILITYID).FirstOrDefault();
            document.DocumentTypeId = referenceDocType;
            document.EmployeeID = employeeId;
            document.DocumentDate = DateTime.Now;
            document.IsActive = true;
            document.CreateDate = DateTime.Now;
            document.CreateBy = createdBy;
            document.DocumentPath = documentPath;
            document.EmployeeVisibility = true;
            document.DocumentVisibility = docVisibility;
            DbContext.HR_Documents.Add(document);
            DbContext.SaveChanges();
            return document.DocumentId;

        }

        //sendEmailNotification To Line Manager
        private void sendEmailNotification(DeclarationOfInterestEmailNotification emailObj)
        {
            if (emailObj != null && emailObj.lineManagerEmail != null)
            {
                string body = string.Empty;
                body = "Dear " + emailObj.lineManagerName + ", <br /><br />";
                body = body + "A declaration of interest form has been submitted by {0}. ";
                body = body + "This has been added to the Declaration of Interest Report for approval.";
                body = string.Format(body, emailObj.employeeName);

                string subject = EmailSubjectConstants.DeclarationOfInterest;
                EmailHelper.sendHtmlFormattedEmail(emailObj.lineManagerName, emailObj.lineManagerEmail, subject, body);
            }
        }

        private E_DOI_HISTORY entryDOIHistory(E_DOI objDOI)
        {
            E_DOI_HISTORY doiHistoryObj = new E_DOI_HISTORY();
            doiHistoryObj.EmployeeId = objDOI.EmployeeId;
            doiHistoryObj.CreatedBy = objDOI.CreatedBy;
            doiHistoryObj.CreatedOn = objDOI.CreatedOn;
            doiHistoryObj.FiscalYear = objDOI.FiscalYear;
            doiHistoryObj.InterestId = objDOI.InterestId;

            doiHistoryObj.EmpName1 = objDOI.EmpName1;
            doiHistoryObj.EmpRole1 = objDOI.EmpRole1;
            doiHistoryObj.EmpRel1 = objDOI.EmpRel1;
            doiHistoryObj.EmpName2 = objDOI.EmpName2;
            doiHistoryObj.EmpRole2 = objDOI.EmpRole2;
            doiHistoryObj.EmpRel2 = objDOI.EmpRel2;
            doiHistoryObj.RoleName = objDOI.RoleName;
            doiHistoryObj.RoleRole = objDOI.RoleRole;
            doiHistoryObj.RoleRel = objDOI.RoleRel;
            doiHistoryObj.ResidingAddress1 = objDOI.ResidingAddress1;
            doiHistoryObj.ResidingAddress2 = objDOI.ResidingAddress2;
            doiHistoryObj.ResidingPostcode = objDOI.ResidingPostcode;
            doiHistoryObj.HomeName1 = objDOI.HomeName1;
            doiHistoryObj.HomeAddress1 = objDOI.HomeAddress1;
            doiHistoryObj.HomeRel1 = objDOI.HomeRel1;
            doiHistoryObj.HomeName2 = objDOI.HomeName2;
            doiHistoryObj.HomeAddress2 = objDOI.HomeAddress2;
            doiHistoryObj.HomeRel2 = objDOI.HomeRel2;
            doiHistoryObj.TypeOfEmployment = objDOI.TypeOfEmployment;
            doiHistoryObj.Employer = objDOI.Employer;
            doiHistoryObj.IsSecondaryEmployment = objDOI.IsSecondaryEmployment;

            doiHistoryObj.ConOrganizationP6 = objDOI.ConOrganizationP6;
            doiHistoryObj.ConService = objDOI.ConService;
            doiHistoryObj.IsConApprovedByLeadership = objDOI.IsConApprovedByLeadership;
            doiHistoryObj.LocalAuthName1 = objDOI.LocalAuthName1;
            doiHistoryObj.LocalAuthOrganization1 = objDOI.LocalAuthOrganization1;
            doiHistoryObj.LocalAuthRel1 = objDOI.LocalAuthRel1;
            doiHistoryObj.LocalAuthName2 = objDOI.LocalAuthName2;
            doiHistoryObj.LocalAuthOrganization2 = objDOI.LocalAuthOrganization2;
            doiHistoryObj.LocalAuthRel2 = objDOI.LocalAuthRel2;
            doiHistoryObj.IsDirectorP8 = objDOI.IsDirectorP8;
            doiHistoryObj.ConOrganizationP8 = objDOI.ConOrganizationP8;
            doiHistoryObj.ConIndvidual = objDOI.ConIndvidual;
            doiHistoryObj.ConRelP8 = objDOI.ConRelP8;
            doiHistoryObj.IsDirectorP9 = objDOI.IsDirectorP9;
            doiHistoryObj.ConOrganizationP9 = objDOI.ConOrganizationP9;

            doiHistoryObj.Trustee1 = objDOI.Trustee1;
            doiHistoryObj.Trustee2 = objDOI.Trustee2;
            doiHistoryObj.Trustee3 = objDOI.Trustee3;
            doiHistoryObj.CountyTown = objDOI.CountyTown;
            doiHistoryObj.Appointments = objDOI.Appointments;
            doiHistoryObj.BeneficialInterest = objDOI.BeneficialInterest;
            doiHistoryObj.IsInterested = objDOI.IsInterested;
            doiHistoryObj.OtherInterest = objDOI.OtherInterest;
            doiHistoryObj.IsActive = objDOI.IsActive;
            doiHistoryObj.Status = objDOI.Status;
            doiHistoryObj.Comment = objDOI.Comment;
            doiHistoryObj.UpdatedBy = objDOI.UpdatedBy;
            doiHistoryObj.UpdatedOn = objDOI.UpdatedOn;
            doiHistoryObj.Status = objDOI.Status;
            doiHistoryObj.DocumentId = objDOI.DocumentId;
            doiHistoryObj.LineManagerReviewDate = objDOI.LineManagerReviewDate;
            doiHistoryObj.LineManagerComments = objDOI.LineManagerComments;

            return doiHistoryObj;
        }

        #endregion

        public void AddDeclarationOfInterest(DeclarationOfInterestDataRequest model)
        {
            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    E_DOI doiObj = new E_DOI();
                    var isNew = false;
                    int fiscalYear = 0;
                    fiscalYear = getCurrentFiscalYear().YRange;
                    if (model.employeeId > 0)
                    {
                        var doiResult = (from S in DbContext.E_DOI where (S.EmployeeId == model.employeeId && S.IsActive == true && S.FiscalYear == fiscalYear) select S).FirstOrDefault();
                        if(doiResult!= null)
                        doiObj = doiResult;
                    }

                    if (doiObj.InterestId == 0)
                    {
                        doiObj.EmployeeId = model.employeeId;
                        doiObj.CreatedBy = model.createdBy;
                        doiObj.CreatedOn = model.createdOn;
                        doiObj.IsActive = model.isActive;
                        doiObj.FiscalYear = fiscalYear;
                    }

                    if (model.page == 1)
                    {
                        doiObj.EmpName1 = model.page1.empName1;
                        doiObj.EmpRole1 = model.page1.empRole1;
                        doiObj.EmpRel1 = model.page1.empRel1;
                        doiObj.EmpName2 = model.page1.empName2;
                        doiObj.EmpRole2 = model.page1.empRole2;
                        doiObj.EmpRel2 = model.page1.empRel2;
                        doiObj.RoleName = model.page1.roleName;
                        doiObj.RoleRole = model.page1.roleRole;
                        doiObj.RoleRel = model.page1.roleRel;
                        doiObj.ResidingAddress1 = model.page1.residingAddress1;
                        doiObj.ResidingAddress2 = model.page1.residingAddress2;
                        doiObj.ResidingPostcode = model.page1.residingPostcode;
                        doiObj.HomeName1 = model.page1.homeName1;
                        doiObj.HomeAddress1 = model.page1.homeAddress1;
                        doiObj.HomeRel1 = model.page1.homeRel1;
                        doiObj.HomeName2 = model.page1.homeName2;
                        doiObj.HomeAddress2 = model.page1.homeAddress2;
                        doiObj.HomeRel2 = model.page1.homeRel2;
                        doiObj.TypeOfEmployment = model.page1.typeOfEmployment;
                        doiObj.Employer = model.page1.employer;
                        doiObj.IsSecondaryEmployment = model.page1.isSecondaryEmployment;
                    }
                    else if (model.page == 2)
                    {
                        doiObj.ConOrganizationP6 = model.page2.conOrganizationP6;
                        doiObj.ConService = model.page2.conService;
                        doiObj.IsConApprovedByLeadership = model.page2.isConApprovedByLeadership;
                        doiObj.LocalAuthName1 = model.page2.localAuthName1;
                        doiObj.LocalAuthOrganization1 = model.page2.localAuthOrganization1;
                        doiObj.LocalAuthRel1 = model.page2.localAuthRel1;
                        doiObj.LocalAuthName2 = model.page2.localAuthName2;
                        doiObj.LocalAuthOrganization2 = model.page2.localAuthOrganization2;
                        doiObj.LocalAuthRel2 = model.page2.localAuthRel2;
                        doiObj.IsDirectorP8 = model.page2.isDirectorP8;
                        doiObj.ConOrganizationP8 = model.page2.conOrganizationP8;
                        doiObj.ConIndvidual = model.page2.conIndvidual;
                        doiObj.ConRelP8 = model.page2.conRelP8;
                        doiObj.IsDirectorP9 = model.page2.isDirectorP9;
                        doiObj.ConOrganizationP9 = model.page2.conOrganizationP9;
                    }
                    else if (model.page == 3)
                    {
                        doiObj.Trustee1 = model.page3.trustee1;
                        doiObj.Trustee2 = model.page3.trustee2;
                        doiObj.Trustee3 = model.page3.trustee3;
                        doiObj.CountyTown = model.page3.countyTown;
                        doiObj.Appointments = model.page3.appointments;
                        doiObj.BeneficialInterest = model.page3.beneficialInterest;
                        doiObj.IsInterested = model.page3.isInterested;
                        doiObj.OtherInterest = model.page3.otherInterest;
                        doiObj.IsActive = true;
                        if (doiObj.Status == null)
                        {
                            isNew = true;
                            doiObj.UpdatedOn = DateTime.Now;
                            var emailData = (from e in DbContext.E__EMPLOYEE
                                             join jd in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals jd.EMPLOYEEID
                                             join lm in DbContext.E__EMPLOYEE on jd.LINEMANAGER equals lm.EMPLOYEEID into lm_join
                                             from lm in lm_join.DefaultIfEmpty()
                                             join lmc in DbContext.E_CONTACT on lm.EMPLOYEEID equals lmc.EMPLOYEEID
                                             where e.EMPLOYEEID == doiObj.EmployeeId
                                             select new DeclarationOfInterestEmailNotification()
                                             {
                                                 employeeName = e.FIRSTNAME + " " + e.LASTNAME,
                                                 lineManagerName = lm.FIRSTNAME + " " + lm.LASTNAME,
                                                 lineManagerEmail = lmc.WORKEMAIL,
                                                 employeeId = e.EMPLOYEEID,
                                                 interestId = doiObj.InterestId,
                                                 lineManagerId = lm.EMPLOYEEID
                                             }).FirstOrDefault();
                            sendEmailNotification(emailData);
                        }
                        doiObj.Status = DbContext.E_DOI_Status.FirstOrDefault(e => e.Description == "Pending Review").DOIStatusId;
                        doiObj.DocumentId = AddDoiDocument(model.page3.documentPath, model.employeeId, model.createdBy);
                    }
                    if (doiObj.InterestId <= 0)
                    {
                        DbContext.E_DOI.Add(doiObj);
                    }
                    // entry in history table
                    if (isNew)
                    {
                        E_DOI_HISTORY doiHistoryObj = entryDOIHistory(doiObj);
                        DbContext.E_DOI_HISTORY.Add(doiHistoryObj);
                    }
                    DbContext.SaveChanges();
                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }

            }
        }

        public DeclarationOfInterestDataRequest GetDeclarationOfInterest(int employeeId)
        {
            DeclarationOfInterestDataRequest response = new DeclarationOfInterestDataRequest();
            E_DOI doiObj = new E_DOI();
            int fiscalYear = 0;
            fiscalYear = getCurrentFiscalYear().YRange;
            var doiResult = (from doi in DbContext.E_DOI
                             join ds in DbContext.E_DOI_Status on doi.Status equals ds.DOIStatusId into ds_join
                             from ds in ds_join.DefaultIfEmpty()
                             where (doi.EmployeeId == employeeId &&
                                doi.IsActive == true &&
                                //ds.Description == "Submitted" &&
                                doi.FiscalYear == fiscalYear) select doi);
            if (doiResult.Count() > 0)
            {
                doiObj = doiResult.FirstOrDefault();
                response.employeeId = doiObj.EmployeeId;
                response.createdBy = doiObj.CreatedBy;
                response.interestId = doiObj.InterestId;
                response.isActive = doiObj.IsActive;
                response.page1.empName1 = doiObj.EmpName1;
                response.page1.empRole1 = doiObj.EmpRole1;
                response.page1.empRel1 = doiObj.EmpRel1;
                response.page1.empName2 = doiObj.EmpName2;
                response.page1.empRole2 = doiObj.EmpRole2;
                response.page1.empRel2 = doiObj.EmpRel2;
                response.page1.roleName = doiObj.RoleName;
                response.page1.roleRole = doiObj.RoleRole;
                response.page1.roleRel = doiObj.RoleRel;
                response.page1.residingAddress1 = doiObj.ResidingAddress1;
                response.page1.residingAddress2 = doiObj.ResidingAddress2;
                response.page1.residingPostcode = doiObj.ResidingPostcode;
                response.page1.homeName1 = doiObj.HomeName1;
                response.page1.homeAddress1 = doiObj.HomeAddress1;
                response.page1.homeRel1 = doiObj.HomeRel1;
                response.page1.homeName2 = doiObj.HomeName2;
                response.page1.homeAddress2 = doiObj.HomeAddress2;
                response.page1.homeRel2 = doiObj.HomeRel2;
                response.page1.typeOfEmployment = doiObj.TypeOfEmployment;
                response.page1.employer = doiObj.Employer;
                response.page1.isSecondaryEmployment = doiObj.IsSecondaryEmployment;

                response.page2.conOrganizationP6 = doiObj.ConOrganizationP6;
                response.page2.conService = doiObj.ConService;
                response.page2.isConApprovedByLeadership = doiObj.IsConApprovedByLeadership;
                response.page2.localAuthName1 = doiObj.LocalAuthName1;
                response.page2.localAuthOrganization1 = doiObj.LocalAuthOrganization1;
                response.page2.localAuthRel1 = doiObj.LocalAuthRel1;
                response.page2.localAuthName2 = doiObj.LocalAuthName2;
                response.page2.localAuthOrganization2 = doiObj.LocalAuthOrganization2;
                response.page2.localAuthRel2 = doiObj.LocalAuthRel2;
                response.page2.isDirectorP8 = doiObj.IsDirectorP8;
                response.page2.conOrganizationP8 = doiObj.ConOrganizationP8;
                response.page2.conIndvidual = doiObj.ConIndvidual;
                response.page2.conRelP8 = doiObj.ConRelP8;
                response.page2.isDirectorP9 = doiObj.IsDirectorP9;
                response.page2.conOrganizationP9 = doiObj.ConOrganizationP9;

                response.page3.trustee1 = doiObj.Trustee1;
                response.page3.trustee2 = doiObj.Trustee2;
                response.page3.trustee3 = doiObj.Trustee3;
                response.page3.countyTown = doiObj.CountyTown;
                response.page3.appointments = doiObj.Appointments;
                response.page3.beneficialInterest = doiObj.BeneficialInterest;
                response.page3.isInterested = doiObj.IsInterested;
                response.page3.otherInterest = doiObj.OtherInterest;
            }
            return response;
        }

        public DeclarationOfInterestDataRequest GetDeclarationOfInterestById(int interestId)
        {
            DeclarationOfInterestDataRequest response = new DeclarationOfInterestDataRequest();
            E_DOI doiObj = new E_DOI();
            var doiResult = (from doi in DbContext.E_DOI
                             join ds in DbContext.E_DOI_Status on doi.Status equals ds.DOIStatusId into ds_join
                             from ds in ds_join.DefaultIfEmpty()
                             where (doi.InterestId == interestId)
                             select doi);
            if (doiResult.Count() > 0)
            {
                doiObj = doiResult.FirstOrDefault();

                if (doiObj.EmployeeId > 0)
                {
                    response.employeeFullName = DbContext.E__EMPLOYEE
                                                         .Where(e => e.EMPLOYEEID == doiObj.EmployeeId)
                                                         .Select(s => s.FIRSTNAME + " " + s.LASTNAME)
                                                         .FirstOrDefault();
                }

                response.employeeId = doiObj.EmployeeId;
                response.createdBy = doiObj.CreatedBy;
                response.interestId = doiObj.InterestId;
                response.isActive = doiObj.IsActive;
                response.comment = doiObj.Comment;
                response.updatedOn = doiObj.UpdatedOn;
                response.reviewComments = doiObj.LineManagerComments;
                response.reviewDate = doiObj.LineManagerReviewDate;
                response.page1.empName1 = doiObj.EmpName1;
                response.page1.empRole1 = doiObj.EmpRole1;
                response.page1.empRel1 = doiObj.EmpRel1;
                response.page1.empName2 = doiObj.EmpName2;
                response.page1.empRole2 = doiObj.EmpRole2;
                response.page1.empRel2 = doiObj.EmpRel2;
                response.page1.roleName = doiObj.RoleName;
                response.page1.roleRole = doiObj.RoleRole;
                response.page1.roleRel = doiObj.RoleRel;
                response.page1.residingAddress1 = doiObj.ResidingAddress1;
                response.page1.residingAddress2 = doiObj.ResidingAddress2;
                response.page1.residingPostcode = doiObj.ResidingPostcode;
                response.page1.homeName1 = doiObj.HomeName1;
                response.page1.homeAddress1 = doiObj.HomeAddress1;
                response.page1.homeRel1 = doiObj.HomeRel1;
                response.page1.homeName2 = doiObj.HomeName2;
                response.page1.homeAddress2 = doiObj.HomeAddress2;
                response.page1.homeRel2 = doiObj.HomeRel2;
                response.page1.typeOfEmployment = doiObj.TypeOfEmployment;
                response.page1.employer = doiObj.Employer;
                response.page1.isSecondaryEmployment = doiObj.IsSecondaryEmployment;

                response.page2.conOrganizationP6 = doiObj.ConOrganizationP6;
                response.page2.conService = doiObj.ConService;
                response.page2.isConApprovedByLeadership = doiObj.IsConApprovedByLeadership;
                response.page2.localAuthName1 = doiObj.LocalAuthName1;
                response.page2.localAuthOrganization1 = doiObj.LocalAuthOrganization1;
                response.page2.localAuthRel1 = doiObj.LocalAuthRel1;
                response.page2.localAuthName2 = doiObj.LocalAuthName2;
                response.page2.localAuthOrganization2 = doiObj.LocalAuthOrganization2;
                response.page2.localAuthRel2 = doiObj.LocalAuthRel2;
                response.page2.isDirectorP8 = doiObj.IsDirectorP8;
                response.page2.conOrganizationP8 = doiObj.ConOrganizationP8;
                response.page2.conIndvidual = doiObj.ConIndvidual;
                response.page2.conRelP8 = doiObj.ConRelP8;
                response.page2.isDirectorP9 = doiObj.IsDirectorP9;
                response.page2.conOrganizationP9 = doiObj.ConOrganizationP9;

                response.page3.trustee1 = doiObj.Trustee1;
                response.page3.trustee2 = doiObj.Trustee2;
                response.page3.trustee3 = doiObj.Trustee3;
                response.page3.countyTown = doiObj.CountyTown;
                response.page3.appointments = doiObj.Appointments;
                response.page3.beneficialInterest = doiObj.BeneficialInterest;
                response.page3.isInterested = doiObj.IsInterested;
                response.page3.otherInterest = doiObj.OtherInterest;
            }
            return response;
        }
        
        public bool UpdateDeclarationOfInterestStatus(UpdateDeclationOfInterestRequest request)
        {
            var data = (from p in DbContext.E_DOI
                        where p.InterestId == request.interestId
                        select p).FirstOrDefault();


            if (data != null)
            {
                var statusData = (from s in DbContext.E_DOI_Status
                                  where s.Description == request.status
                                  select s).FirstOrDefault();

                if (statusData != null)
                {
                    var status = request.status ?? string.Empty;
                    data.IsActive = status.Equals(ApplicationConstants.doiApproved) ? false : true;
                    data.Status = statusData.DOIStatusId;
                    data.UpdatedBy = request.lastActionUser;                    

                    if (status.Equals(ApplicationConstants.doiSubmitted))
                    {
                        data.LineManagerReviewDate = request.reviewDate;
                        data.LineManagerComments = request.reviewComments;
                    }
                    else
                    {
                        data.Comment = request.comment;
                        data.UpdatedOn = request.updatedOn;
                    }

                }

                E_DOI_HISTORY doiHistoryObj = entryDOIHistory(data);
                DbContext.E_DOI_HISTORY.Add(doiHistoryObj);
            }

            DbContext.SaveChanges();
            return true;
        }
    }
}
