﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository
{
    public class EmployeeDoiRelationshipRepository : BaseRepository<E_DOI_Relationship>
    {
        public EmployeeDoiRelationshipRepository(Entities dbContext) : base(dbContext)
        {

        }
    }
}
