﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataBaseModule.DataRepository
{
    public class DrivingLicenceRepository : BaseRepository<E_DrivingLicence>
    {

        public DrivingLicenceRepository(Entities dbContext) : base(dbContext)
        {

        }

        public DrivingLicence GetDrivingLicence(int employeeId)
        {
            DrivingLicence response = new DrivingLicence();
            var data = (from d in DbContext.E_DrivingLicence
                        where d.EmployeeId == employeeId
                        select new DrivingLicence()
                        {
                            drivingLicenceId = d.DrivingLicenceId,
                            createdBy = d.CreatedBy,
                            drivingLicenceIssueNumber = d.DrivingLicenceIssueNumber,
                            drivingLicenceNumber = d.DrivingLicenceNumber,
                            drivingLicenceType = d.DrivingLicenceType,
                            employeeId = d.EmployeeId,
                            expiryDate = d.ExpiryDate,
                            medicalHistory = d.MedicalHistory,
                            licenceImage = d.LicenceImage
                        });
            if (data.Count() > 0)
            {
                response = data.FirstOrDefault();

                if (response.expiryDate.HasValue)
                {
                    response.expiry = response.expiryDate.Value.ToShortDateString();
                }

                var categoryData = (from d in DbContext.E_DrivingLicenceCategories
                                    where d.DrivingLicenceId == response.drivingLicenceId
                                    select new DrivingLicenceCategories()
                                    {
                                        category = d.Category,
                                        code = d.Code
                                    });
                if (categoryData.Count() > 0)
                {
                    response.categories = categoryData.ToList();
                }
            }
            return response;
        }

        public List<LicenceDisqualifications> GetLicenceDisqualifications(int employeeId)
        {
            List<LicenceDisqualifications> response = new List<LicenceDisqualifications>();
            var data = (from d in DbContext.E_LicenceDisqualifications
                        where d.EmployeeId == employeeId
                        select new LicenceDisqualifications()
                        {
                            code = d.Code,
                            employeeId = d.EmployeeId,
                            expiryDate = d.ExpiryDate,
                            issuedDate = d.IssuedDate,
                            numberofPoints = d.NumberofPoints,
                            reason = d.Reason
                        });
            if (data.Count() > 0)
            {
                response = data.ToList();
            }
            return response;
        }

        public void AddUpdateDrivingLicence(DrivingLicenceResponse request)
        {
            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    E_DrivingLicence drivingLicence = new DatabaseEntities.E_DrivingLicence();
                    
                    var disqualificationsData = from c in DbContext.E_LicenceDisqualifications
                                                where c.EmployeeId == request.drivingLicence.employeeId
                                                select c;
                    if (disqualificationsData.Count() > 0)
                    {
                        DbContext.E_LicenceDisqualifications.RemoveRange(disqualificationsData.ToList());
                    }

                    var data = (from S in DbContext.E_DrivingLicence where (S.EmployeeId == request.drivingLicence.employeeId) select S);
                    if (data.Count() > 0)
                    {
                        drivingLicence = data.FirstOrDefault();
                    }
                    drivingLicence.CreatedBy = request.drivingLicence.createdBy;
                    drivingLicence.CreatedDate = DateTime.Now;
                    drivingLicence.DrivingLicenceIssueNumber = request.drivingLicence.drivingLicenceIssueNumber;
                    drivingLicence.DrivingLicenceNumber = request.drivingLicence.drivingLicenceNumber;
                    drivingLicence.DrivingLicenceType = request.drivingLicence.drivingLicenceType;
                    drivingLicence.EmployeeId = request.drivingLicence.employeeId;
                    drivingLicence.MedicalHistory = request.drivingLicence.medicalHistory;
                    if (!string.IsNullOrEmpty(request.drivingLicence.expiry))
                    {
                        drivingLicence.ExpiryDate = GeneralHelper.GetDateTimeFromString(request.drivingLicence.expiry);
                    }
                    if (data.Count() <= 0)
                    {
                        DbContext.E_DrivingLicence.Add(drivingLicence);
                    }
                    var catData = from c in DbContext.E_DrivingLicenceCategories
                                  where c.DrivingLicenceId == request.drivingLicence.drivingLicenceId
                                  select c;
                    if (catData.Count() > 0)
                    {
                        DbContext.E_DrivingLicenceCategories.RemoveRange(catData.ToList());
                    }
                    //Add data in E_DrivingLicenceCategories
                    List<E_DrivingLicenceCategories> catList = new List<DatabaseEntities.E_DrivingLicenceCategories>();
                    foreach (var item in request.drivingLicence.categories)
                    {
                        E_DrivingLicenceCategories cat = new DatabaseEntities.E_DrivingLicenceCategories();
                        cat.Category = item.category;
                        cat.Code = item.code;
                        cat.DrivingLicenceId = drivingLicence.DrivingLicenceId;
                        catList.Add(cat);
                    }
                    DbContext.E_DrivingLicenceCategories.AddRange(catList);
                    //Add data in E_LicenceDisqualifications

                    List<E_LicenceDisqualifications> disqualificationsList = new List<DatabaseEntities.E_LicenceDisqualifications>();
                    foreach (var item in request.disqualificationsList)
                    {
                        E_LicenceDisqualifications disqualification = new DatabaseEntities.E_LicenceDisqualifications();
                        disqualification.Code = item.code;
                        disqualification.EmployeeId = item.employeeId;
                        disqualification.ExpiryDate = item.expiryDate;
                        disqualification.IssuedDate = item.issuedDate;
                        disqualification.NumberofPoints = item.numberofPoints;
                        disqualification.Reason = item.reason;
                        disqualificationsList.Add(disqualification);
                    }

                    DbContext.E_LicenceDisqualifications.AddRange(disqualificationsList);

                    DbContext.SaveChanges();
                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }

            }

        }

        public bool UpdateDrivingLicenceImage(LicenceImageRequest request)
        {
            var data = (from e in DbContext.E_DrivingLicence
                        where e.EmployeeId == request.employeeId
                        select e).FirstOrDefault();
            if (data != null)
            {
                data.LicenceImage = request.filePath;
                data.CreatedBy = request.updatedBy;
                data.CreatedDate = DateTime.Now;
            }
            else
            {
                E_DrivingLicence licence = new DatabaseEntities.E_DrivingLicence();
                licence.LicenceImage = request.filePath;
                licence.CreatedBy = request.updatedBy;
                licence.CreatedDate = DateTime.Now;
                licence.EmployeeId = request.employeeId;
                DbContext.E_DrivingLicence.Add(licence);
            }

            DbContext.SaveChanges();
            return true;
        }
    }
}
