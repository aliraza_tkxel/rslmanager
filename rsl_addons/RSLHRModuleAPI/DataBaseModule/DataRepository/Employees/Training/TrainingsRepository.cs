﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataBaseModule.DataRepository
{
    public class TrainingsRepository : BaseRepository<E_EmployeeTrainings>
    {
        public object RepositryUnit { get; private set; }

        public TrainingsRepository(Entities dbContext) : base(dbContext)
        {

        }

        #region >>> Helper <<<

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }

        //sendEmailNotification To Line Manager
        private void sendEmailNotification(TrainingApprovalEmailNotification emailObj, TrainingEnum trainingEnum)
        {
            if (emailObj != null && emailObj.lineManagerEmail != null)
            {
                string body = string.Empty;
                string subject = "";
                body = "Dear " + emailObj.lineManagerName + ", <br /><br />";

                if (emailObj.trainingStatus!=null && emailObj.trainingStatus.Contains("Declined"))
                {
                    subject = string.Format(EmailSubjectConstants.TrainingDeclined, emailObj.courseName);
                    if (emailObj.trainingStatus == ApplicationConstants.grpTrainingStatusExecDeclined)
                    {                        
                        body = body + string.Format("This is confirmation that training course {0} for {1} has been {2} by {3}.", emailObj.courseName, emailObj.employeeName, emailObj.statusTitle, emailObj.directorName) + "<br /><br />";
                    }

                    if (emailObj.trainingStatus == ApplicationConstants.grpTrainingStatusHRDeclined)
                    {
                        body = string.Format(body + "The training course {0} has been declined by HR. ", emailObj.courseName);
                        body = body + "Please contact your line manager or a member of the HR team to discuss further. <br /><br />";
                    }

                    body = body + "Thanks <br />";
                    body = body + "HR Team";
                }
                else
                {
                    if (trainingEnum == TrainingEnum.Hr)
                    {
                        subject = string.Format(EmailSubjectConstants.TrainingApprovalRequired, emailObj.courseName);
                        body = string.Format(body + "A new training course for {0} has been recorded and added to the ‘Training Approval’ as it is above the £1000.00 threshold and/or is a Professional Qualification. <br />", emailObj.employeeName);
                        body = body + string.Format("Course details: {0}. <br /><br />", emailObj.notes);
                        body = body + "Please review the course on the ‘Training Approval List’.";
                    }

                    if (trainingEnum == TrainingEnum.LineManager)
                    {
                        string totalCost = Math.Round(Convert.ToDouble(emailObj.totalCost), 2).ToString();
                        subject = string.Format(EmailSubjectConstants.TrainingApprovalRequired, emailObj.courseName);
                        body = string.Format(body + "A new training course {0} for {1} has been submitted and now requires your approval or rejection.The cost of the training course is £{2}. <br />", emailObj.courseName, emailObj.employeeName, totalCost);
                        body = body + "Thanks <br /><br />";
                        body = body + "HR Team";
                    }

                    if (trainingEnum == TrainingEnum.GroupTraining)
                    {
                        subject = string.Format(EmailSubjectConstants.TrainingApproval, emailObj.courseName);
                        body = string.Format(body + " We are pleased to confirm that your request to attend the {0} has been approved. <br /><br />", emailObj.courseName);
                        body = body + "Please contact your manager to progress your request further. <br /><br />";
                        body = body + "Thanks <br /><br />";
                        body = body + "HR Team";
                    }

                    if (trainingEnum == TrainingEnum.LineManagerGroupTraining)
                    {
                        subject = string.Format(EmailSubjectConstants.GroupTrainingApproval, emailObj.courseName);
                        body = string.Format(body + " {0} has been booked to attend the following training course: <br /><br />", emailObj.employeeName);
                        body = body + string.Format("Course: {0}. <br />", emailObj.courseName);
                        body = body + string.Format("Date(s): {0} to {1}. <br /><br />", emailObj.startDate, emailObj.endDate);
                        body = body + "If you have any queries, please contact the HR Team. <br /><br />";
                        body = body + "Thanks <br /><br />";
                        body = body + "HR Team";
                    }

                    if (trainingEnum == TrainingEnum.EmployeeGroupTraining)
                    {
                        subject = string.Format(EmailSubjectConstants.GroupTrainingApproval, emailObj.courseName);
                        body = body + " You have been booked to attend the following training course:  <br /><br />";
                        body = body + string.Format("Course: {0}. <br />", emailObj.courseName);
                        body = body + string.Format("Date(s): {0} to {1}. <br /><br />", emailObj.startDate, emailObj.endDate);
                        body = body + "Full details as per your Outlook calendar entry.<br />";
                        body = body + "If you have any queries, please contact the HR Team. <br /><br />";
                        body = body + "Thanks <br /><br />";
                        body = body + "HR Team";
                    }

                    if (trainingEnum == TrainingEnum.AppointmentRearranged)
                    {
                        subject = string.Format(EmailSubjectConstants.AppointmentRearranged, emailObj.courseName);
                        body = string.Format(body + "The {0} training has been arranged for {1} from {2} to {3} who already has appointments scheduled that need to be rearranged. <br /><br />", emailObj.courseName, emailObj.employeeName, emailObj.startDate, emailObj.endDate);
                        body = body + "Please rearrange their appointment(s) at the earliest opportunity. <br /><br />";
                    }

                    if (trainingEnum == TrainingEnum.TrainingAlreadyExist)
                    {
                        subject = EmailSubjectConstants.TrainingAlreadyExist;
                        body = string.Format(body + "The {0} training conflicts with an already scheduled training for {1} from {2} to {3}. Scheduling the new training has been aborted for {4}.<br /><br />", emailObj.courseName, emailObj.employeeName, emailObj.startDate, emailObj.endDate, emailObj.employeeName);
                    }

                    if (trainingEnum == TrainingEnum.ExecApproveDecline)
                    {
                        subject = string.Format(EmailSubjectConstants.TrainingApproved, emailObj.courseName);
                        body = body + string.Format("This is confirmation that training course {0} for {1} has been {2} by {3}.", emailObj.courseName, emailObj.employeeName, emailObj.statusTitle, emailObj.directorName) + "<br /><br />";
                        body = body + "Regards <br /><br />";
                        body = body + "HR Team";
                    }

                    if (trainingEnum == TrainingEnum.HRApproved)
                    {
                        subject = string.Format(EmailSubjectConstants.TrainingApproved, emailObj.courseName);
                        body = body + string.Format("The training course {0} has been {1} by HR.", emailObj.courseName, emailObj.statusTitle) + "<br /><br />";
                        body = body + "Thanks <br /> <br />";
                        body = body + "HR Team";
                    }

                }

                EmailHelper.sendHtmlFormattedEmail(emailObj.lineManagerName, emailObj.lineManagerEmail, subject, body);
            }
        }

        private void SendEmailNotification(E_EmployeeTrainings trainingObj, string status)
        {
            if (trainingObj != null)
            {
                var empData = (from e in DbContext.E__EMPLOYEE
                               join c in DbContext.E_CONTACT on e.EMPLOYEEID equals c.EMPLOYEEID
                               where e.EMPLOYEEID == trainingObj.EmployeeId
                               select new { e.EMPLOYEEID, e.FIRSTNAME, e.LASTNAME, c.WORKEMAIL }).FirstOrDefault();
                if (empData != null && empData.WORKEMAIL != null)
                {
                    string subject = "Your Training has been " + status;
                    string body = "Dear " + empData.FIRSTNAME + " " + empData.LASTNAME + ", </br> This is confirmation that your " + trainingObj.Course + " training has been " + status + ". If you have any questions please discuss with your line manager.";

                    EmailHelper.sendHtmlFormattedEmail(empData.FIRSTNAME + " " + empData.LASTNAME, empData.WORKEMAIL, subject, body);
                }
            }
        }

        private EmployeeDetails getEmployeeDetails(int employeeId)
        {
            try
            {
                var data = (from E in DbContext.E__EMPLOYEE
                            join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                            from J in J_join.DefaultIfEmpty()
                            join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                            from T in T_join.DefaultIfEmpty()
                            join EC in DbContext.E_CONTACT on E.EMPLOYEEID equals EC.EMPLOYEEID into EC_join
                            from EC in EC_join.DefaultIfEmpty()
                            join LM in DbContext.E__EMPLOYEE on J.LINEMANAGER equals LM.EMPLOYEEID into LM_join
                            from LM in LM_join.DefaultIfEmpty()
                            join LMC in DbContext.E_CONTACT on LM.EMPLOYEEID equals LMC.EMPLOYEEID into LMC_join
                            from LMC in LMC_join.DefaultIfEmpty()
                            where E.EMPLOYEEID == employeeId

                            select new EmployeeDetails()
                            {
                                employeeId = E.EMPLOYEEID,
                                employeeName = E.FIRSTNAME + " " + E.LASTNAME,
                                teamName = T.TEAMNAME,
                                employeeEmail = EC.WORKEMAIL,
                                lineManagerId = LM.EMPLOYEEID,
                                lineManagerName = LM.FIRSTNAME + " " + LM.LASTNAME,
                                lineManagerEmail = LMC.WORKEMAIL,
                                directorId = T.DIRECTOR
                            }).FirstOrDefault();
                return data;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private EmployeeDetails getGroupTrainingEmployeeDetail(int employeeId)
        {
            try
            {
                var data = (from E in DbContext.E__EMPLOYEE
                            join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                            join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                            from T in T_join.DefaultIfEmpty()
                            join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                            from D in D_join.DefaultIfEmpty()
                            join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                            from G in G_join.DefaultIfEmpty()
                            join ET in DbContext.E_JOBROLE on J.JobRoleId equals ET.JobRoleId into ET_join
                            from ET in ET_join.DefaultIfEmpty()
                            where E.EMPLOYEEID == employeeId

                            select new EmployeeDetails()
                            {
                                employeeId = E.EMPLOYEEID,
                                employeeName = E.FIRSTNAME + " " + E.LASTNAME,
                                teamName = T.TEAMNAME
                            }).FirstOrDefault();
                return data;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string GetStatusName(int statusId)
        {
            try
            {
                E_EmployeeTrainingStatus status = DbContext.E_EmployeeTrainingStatus.FirstOrDefault(x => x.StatusId == statusId);
                if (status != null)
                {
                    return status.Title;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private bool checkIfAppointment(int employeeId, DateTime startDate, DateTime returnDate)
        {
            bool isAVailable = true;

            //      >>> AS_APPOINTMENTS <<<
            var data = (from apt in DbContext.AS_APPOINTMENTS
                        join aptj in DbContext.AS_JOURNAL on apt.JournalId equals aptj.JOURNALID
                        join apts in DbContext.AS_Status on aptj.STATUSID equals apts.StatusId
                        let appStartDate = EntityFunctions.TruncateTime(apt.APPOINTMENTDATE)
                        where (appStartDate >= startDate || appStartDate <= returnDate) &&
                               (apts.Title != "Cancelled" && apt.APPOINTMENTSTATUS != "Complete") &&
                               apt.ASSIGNEDTO == employeeId &&
                               aptj.ISCURRENT == true
                        select apt).Any();

            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }

            //      >>> FL_CO_APPOINTMENT <<<
            data = (
                from aptc in DbContext.FL_CO_APPOINTMENT
                join apt in DbContext.FL_FAULT_APPOINTMENT on aptc.AppointmentID equals apt.AppointmentId
                join aptj in DbContext.FL_FAULT_LOG on apt.FaultLogId equals aptj.FaultLogID
                join apts in DbContext.FL_FAULT_STATUS on aptj.StatusID equals apts.FaultStatusID
                let appStartDate = EntityFunctions.TruncateTime(aptc.AppointmentDate)
                where (appStartDate >= startDate && appStartDate <= returnDate) &&
                        (apts.Description != "Cancelled" && apts.Description != "Complete") &&
                        aptc.OperativeID == employeeId
                select apt.AppointmentId).Any();
            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }
            //.Concat(
            data = (from apt in DbContext.PDR_APPOINTMENTS
                    join aptj in DbContext.PDR_JOURNAL on apt.JOURNALID equals aptj.JOURNALID
                    join apts in DbContext.PDR_STATUS on aptj.STATUSID equals apts.STATUSID
                    let appStartDate = EntityFunctions.TruncateTime(apt.APPOINTMENTSTARTDATE)
                    let appEndDate = EntityFunctions.TruncateTime(apt.APPOINTMENTENDDATE)
                    where ((appStartDate >= startDate && appStartDate <= returnDate) || (appEndDate >= startDate && appEndDate <= returnDate)) &&
                            (apts.TITLE != "Cancelled" && apt.APPOINTMENTSTATUS != "Complete" && apt.APPOINTMENTSTATUS != "Cancelled") &&
                            apt.ASSIGNEDTO == employeeId
                    select apt.APPOINTMENTID).Any();
            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }
            //.Concat(
            data = (from apt in DbContext.PLANNED_APPOINTMENTS
                    join aptj in DbContext.PLANNED_JOURNAL on apt.JournalId equals aptj.JOURNALID
                    join apts in DbContext.PLANNED_STATUS on aptj.STATUSID equals apts.STATUSID
                    let appStartDate = EntityFunctions.TruncateTime(apt.APPOINTMENTDATE)
                    let appEndDate = EntityFunctions.TruncateTime(apt.APPOINTMENTENDDATE)
                    where ((appStartDate >= startDate && appStartDate <= returnDate) || (appEndDate >= startDate && appEndDate <= returnDate)) &&
                            (apts.TITLE != "Cancelled" && apt.APPOINTMENTSTATUS != "Complete" && apt.APPOINTMENTSTATUS != "Cancelled") &&
                            apt.ASSIGNEDTO == employeeId
                    select apt.APPOINTMENTID).Any();
            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }
            //.Concat(
            data = (from apt in DbContext.AS_APPOINTMENTS
                    join aptj in DbContext.AS_JOURNAL on apt.JournalId equals aptj.JOURNALID
                    join apts in DbContext.AS_Status on aptj.STATUSID equals apts.StatusId
                    let appStartDate = EntityFunctions.TruncateTime(apt.APPOINTMENTDATE)
                    where (appStartDate >= startDate && appStartDate <= returnDate) &&
                            (apts.Title != "Cancelled" && apt.APPOINTMENTSTATUS != "Complete") &&
                            apt.ASSIGNEDTO == employeeId &&
                            aptj.ISCURRENT == true
                    select apt.APPOINTMENTID).Any();

            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }

            //data = (from et in DbContext.E_EmployeeTrainings
            //            join ets in DbContext.E_EmployeeTrainingStatus on et.Status equals ets.StatusId
            //            let appStartDate = EntityFunctions.TruncateTime(et.StartDate)
            //            let appEndDate = EntityFunctions.TruncateTime(et.EndDate)
            //            where ((appStartDate >= startDate && appStartDate <= returnDate) || (appEndDate >= startDate && appEndDate <= returnDate)) &&
            //                   (ets.Title != ApplicationConstants.grpTrainingStatusRemoved && ets.Title != ApplicationConstants.grpTrainingStatusDeclined) &&
            //                   et.EmployeeId == employeeId && et.Active == true
            //            select et).Any();

            //if (data)
            //{
            //    isAVailable = false;
            //    return isAVailable;
            //}

            return isAVailable;
        }

        private bool checkIfTraining(int employeeId, string startDate, string returnDate)
        {
            var _startDate = new DateTime();
            var _endDate = new DateTime();

            if (!string.IsNullOrEmpty(startDate))
                _startDate = Convert.ToDateTime(startDate);
            if (!string.IsNullOrEmpty(returnDate))
                _endDate = Convert.ToDateTime(returnDate);
            bool isAVailable = true;
            var data = (from et in DbContext.E_EmployeeTrainings
                        join ets in DbContext.E_EmployeeTrainingStatus on et.Status equals ets.StatusId
                        let appStartDate = EntityFunctions.TruncateTime(et.StartDate)
                        let appEndDate = EntityFunctions.TruncateTime(et.EndDate)
                        where ((appStartDate >= _startDate && appStartDate <= _endDate) || (appEndDate >= _startDate && appEndDate <= _endDate)) &&
                               (ets.Title != ApplicationConstants.grpTrainingStatusRemoved && ets.Title != ApplicationConstants.grpTrainingStatusDeclined && ets.Title != ApplicationConstants.grpTrainingStatusExecDeclined && ets.Title != ApplicationConstants.grpTrainingStatusHRDeclined) &&
                               et.EmployeeId == employeeId && et.Active == true
                        select et).Any();

            if (data)
            {
                isAVailable = false;
                return isAVailable;
            }

            return isAVailable;
        }

        #endregion

        #region >>> Training <<<

        public bool AddEmployeeTrainings(AddEmployeeTrainings request)
        {
            // send email to line manager when his employee has appointment and book training on same date.  
            if (!checkIfTraining(request.employeeId.Value, request.startDate, request.endDate))
            {
                var model = new TrainingApprovalEmailNotification();
                model.trainingStatus = request.status;
                var empDetail = getEmployeeDetails(request.employeeId.Value);
                if (!string.IsNullOrEmpty(empDetail.lineManagerEmail))
                {
                    model.courseName = request.course;
                    model.employeeName = empDetail.employeeName;
                    model.lineManagerId = empDetail.lineManagerId;
                    model.lineManagerName = ApplicationConstants.hrTeamTitle;
                    model.lineManagerEmail = ApplicationConstants.hrTeamEmailAddress;
                    model.startDate = request.startDate;
                    model.endDate = request.endDate;
                    sendEmailNotification(model, TrainingEnum.TrainingAlreadyExist);
                }

                return false;
            }

            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    E_EmployeeTrainings trainingObj = new E_EmployeeTrainings();
                    var statusId = (from S in DbContext.E_EmployeeTrainingStatus where (S.Title == request.status) select S).FirstOrDefault().StatusId;
                    trainingObj.AdditionalNotes = request.additionalNotes;
                    trainingObj.EmployeeId = request.employeeId;
                    trainingObj.Course = request.course;
                    trainingObj.Createdby = request.createdby;
                    trainingObj.CreatedDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(request.endDate))
                        trainingObj.EndDate = Convert.ToDateTime(request.endDate);
                    if (!string.IsNullOrEmpty(request.expiry))
                        trainingObj.Expiry = Convert.ToDateTime(request.expiry);
                    trainingObj.IsMandatoryTraining = request.isMandatoryTraining;
                    trainingObj.IsSubmittedBy = request.isSubmittedBy;
                    trainingObj.Justification = request.justification;
                    trainingObj.Location = request.location;
                    trainingObj.ProviderName = request.providerName;
                    trainingObj.ProviderWebsite = request.providerWebsite;
                    if (request.startDate != string.Empty || request.startDate != null)
                        trainingObj.StartDate = Convert.ToDateTime(request.startDate);
                    //trainingObj.StartDate = request.startDateTime;
                    trainingObj.Status = statusId;
                    trainingObj.TotalCost = request.totalCost;
                    trainingObj.TotalNumberOfDays = request.totalNumberOfDays;
                    trainingObj.Venue = request.venue;
                    trainingObj.Active = true;
                    trainingObj.ProfessionalQualification = request.professionalQualification;
                    trainingObj.Postcode = request.postcode;
                    DbContext.E_EmployeeTrainings.Add(trainingObj);
                    DbContext.SaveChanges();

                    E_EmployeeApprovedTrainings approvedTraining = new E_EmployeeApprovedTrainings();
                    approvedTraining.TrainingId = trainingObj.TrainingId;
                    approvedTraining.Approvedtraining = request.approvedtraining;
                    approvedTraining.IsRemuneration = request.isRemuneration;
                    approvedTraining.RemunerationCost = request.remunerationCost;
                    DbContext.E_EmployeeApprovedTrainings.Add(approvedTraining);


                    DbContext.SaveChanges();
                    dbContextTransaction.Commit();
                    dbContextTransaction.Dispose();

                    // send email to Director when Professional Qualification is checked and total cost > 1000
                    if (trainingObj.TotalCost > 1000 || request.professionalQualification > 0)
                    {
                        // email to director for Exec Approal.
                        //todo:
                        var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                        var employeeName = empDetail.employeeName;
                        var model = new TrainingApprovalEmailNotification();
                        if (empDetail.directorId.HasValue)
                        {
                            empDetail = getEmployeeDetails(empDetail.directorId.Value);

                            if (empDetail != null && !string.IsNullOrEmpty(empDetail.employeeEmail))
                            {
                                model.courseName = trainingObj.Course;
                                model.trainingStatus = request.status;
                                model.employeeName = employeeName;
                                model.notes = trainingObj.AdditionalNotes;
                                // send to Director

                                model.lineManagerId = empDetail.employeeId;
                                model.lineManagerName = empDetail.employeeName;
                                model.lineManagerEmail = empDetail.employeeEmail;
                                // yes we set TrainingEnum.Hr but it send email to director to be notify the director and the email is same as we send previous to HR 
                                sendEmailNotification(model, TrainingEnum.Hr);
                            }
                        }

                        //// get hr list 
                        //var hrTeam = (from e in DbContext.E__EMPLOYEE
                        //              join ec in DbContext.E_CONTACT on e.EMPLOYEEID equals ec.EMPLOYEEID
                        //              join ej in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals ej.EMPLOYEEID
                        //              join et in DbContext.E_TEAM on ej.TEAM equals et.TEAMID
                        //              where et.TEAMNAME == "HR Services" && ej.ACTIVE == 1
                        //              select new
                        //              {
                        //                  name = et.TEAMNAME,
                        //                  employeeId = e.EMPLOYEEID,
                        //                  workEmail = ec.WORKEMAIL,
                        //                  employeeName = e.FIRSTNAME + " " + e.LASTNAME
                        //              }).ToList();

                        //var model = new TrainingApprovalEmailNotification();
                        //var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);

                        //model.courseName = trainingObj.Course;
                        //model.employeeName = empDetail.employeeName;
                        //foreach (var item in hrTeam)
                        //{
                        //    if (!string.IsNullOrEmpty(item.workEmail))
                        //    {
                        //        model.lineManagerId = item.employeeId;
                        //        model.lineManagerName = item.employeeName;
                        //        model.lineManagerEmail = item.workEmail;

                        //        sendEmailNotification(model, TrainingEnum.Hr);
                        //    }
                        //}
                    }

                    // send email to Line Manager when Professional Qualification is not checked and total cost <= 1000
                    if (trainingObj.TotalCost <= 1000 && request.professionalQualification <= 0)
                    {
                        var model = new TrainingApprovalEmailNotification();
                        model.trainingStatus = request.status;
                        var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                        if (!string.IsNullOrEmpty(empDetail.lineManagerEmail))
                        {
                            model.courseName = trainingObj.Course;
                            model.employeeName = empDetail.employeeName;
                            model.lineManagerId = empDetail.lineManagerId;
                            model.lineManagerName = empDetail.lineManagerName;
                            model.lineManagerEmail = empDetail.lineManagerEmail;
                            model.totalCost = trainingObj.TotalCost;
                            sendEmailNotification(model, TrainingEnum.LineManager);
                        }
                    }

                    // send email to line manager when his employee has appointment and book training on same date.  
                    if (!checkIfAppointment(trainingObj.EmployeeId.Value, trainingObj.StartDate.Value, trainingObj.EndDate.Value))
                    {
                        var model = new TrainingApprovalEmailNotification();
                        model.trainingStatus = request.status;
                        var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                        if (!string.IsNullOrEmpty(empDetail.lineManagerEmail))
                        {
                            model.courseName = trainingObj.Course;
                            model.employeeName = empDetail.employeeName;
                            model.lineManagerId = empDetail.lineManagerId;
                            model.lineManagerName = ApplicationConstants.hrTeamTitle;
                            model.lineManagerEmail = ApplicationConstants.hrTeamEmailAddress;
                            model.startDate = trainingObj.StartDate?.ToShortDateString();
                            model.endDate = trainingObj.EndDate?.ToShortDateString();
                            sendEmailNotification(model, TrainingEnum.AppointmentRearranged);
                        }
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.Message);
                }

            }
        }




        public TrainingListingResponse GetEmployeesTrainings(CommonRequest model)
        {
            TrainingListingResponse response = new TrainingListingResponse();

            var data = (from T in DbContext.E_EmployeeTrainings
                        join E in DbContext.E__EMPLOYEE on T.EmployeeId equals E.EMPLOYEEID
                        //join A in DbContext.E_EmployeeApprovedTrainings on T.TrainingId equals A.TrainingId into J_join
                        //from J in J_join.DefaultIfEmpty()
                        join S in DbContext.E_EmployeeTrainingStatus on T.Status equals S.StatusId
                        where T.EmployeeId == model.employeeId && T.Active == true && S.Title != ApplicationConstants.grpTrainingStatusRemoved
                        select new AddEmployeeTrainings
                        {
                            employeeId = T.EmployeeId,
                            additionalNotes = T.AdditionalNotes,
                            course = T.Course,
                            startDateTime = T.StartDate,
                            endDateTime = T.EndDate,
                            expiryDate = T.Expiry,
                            isMandatoryTraining = T.IsMandatoryTraining,
                            isSubmittedBy = T.IsSubmittedBy,
                            justification = T.Justification,
                            location = T.Location,
                            providerName = T.ProviderName,
                            providerWebsite = T.ProviderWebsite,
                            totalCost = T.TotalCost,
                            totalNumberOfDays = T.TotalNumberOfDays,
                            trainingId = T.TrainingId,
                            venue = T.Venue,
                            status = S.Title,
                            renewDateTime = T.Expiry,
                            recordedDateTime = T.CreatedDate,
                            createdby = T.Createdby,
                            postcode = T.Postcode,
                            professionalQualification = T.ProfessionalQualification
                        });


            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"trainingId";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            foreach (AddEmployeeTrainings item in finalData)
            {
                DateTime startDate = Convert.ToDateTime(item.startDateTime);
                item.startDate = startDate.ToString("dd/MM/yyyy");

                DateTime endDate = Convert.ToDateTime(item.endDateTime);
                item.endDate = endDate.ToString("dd/MM/yyyy");

                item.expiry = item.expiryDate?.ToString("dd/MM/yyyy");

                if (item.renewDateTime != null)
                {
                    DateTime renewDateTime = Convert.ToDateTime(item.renewDateTime);
                    item.renewDate = renewDateTime.ToString("dd/MM/yyyy");
                }

                if (item.recordedDateTime != null)
                {
                    DateTime recordedDateTime = Convert.ToDateTime(item.recordedDateTime);
                    item.recordedDate = recordedDateTime.ToString("dd/MM/yyyy");
                }

                E__EMPLOYEE createdByEmployee = DbContext.E__EMPLOYEE.FirstOrDefault(x => x.EMPLOYEEID == item.createdby);
                item.createdByName = createdByEmployee != null 
                                    ? createdByEmployee.FIRSTNAME + " " + createdByEmployee.LASTNAME
                                    : ApplicationConstants.notAvailableTitle;

                var profQualList = (from S in DbContext.E_EmployeeApprovedTrainings
                                    where (S.TrainingId == item.trainingId)
                                    select new ApprovedTraining
                                    {
                                        approvedtraining = S.Approvedtraining,
                                        isRemuneration = S.IsRemuneration,
                                        remunerationCost = S.RemunerationCost
                                    }).ToList();
                if (profQualList.Count() > 0)
                {
                    item.approvedtraining = profQualList.FirstOrDefault()?.approvedtraining;
                    item.isRemuneration = profQualList.FirstOrDefault()?.isRemuneration;
                    item.remunerationCost = profQualList.FirstOrDefault()?.remunerationCost;
                }

                //item.approvedtraining = profQualList;
            }
            response.trainingList = finalData;
            return response;
        }

        public void UpdateEmployeeTraining(UpdateEmployeeTraining request)
        {
            E_EmployeeTrainings trainingObj = new E_EmployeeTrainings();
            if (request.trainingId > 0)
            {
                var trainingResult = (from S in DbContext.E_EmployeeTrainings where (S.TrainingId == request.trainingId) select S).FirstOrDefault();
                trainingObj = trainingResult;

                var statusId = (from S in DbContext.E_EmployeeTrainingStatus where (S.Title == request.status) select S).FirstOrDefault().StatusId;
                trainingObj.Active = request.isActive;
                trainingObj.Updatedby = request.updatedBy;
                trainingObj.UpdateDate = DateTime.Now;
                trainingObj.Status = statusId;
                DbContext.SaveChanges();
                if (request.isActive == true)
                {

                    
                    #region "Email to Director when Manager Supported"

                    if (request.status == ApplicationConstants.grpTrainingStatusManagerSupported)
                    {
                        var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                        if (empDetail != null && empDetail.directorId.HasValue)
                        {
                            var empDirDetail = getEmployeeDetails(empDetail.directorId.Value);

                            if (empDirDetail != null && !string.IsNullOrEmpty(empDirDetail.employeeEmail))
                            {
                                string body = string.Empty;
                                string totalCost = Math.Round(Convert.ToDouble(trainingObj.TotalCost), 2).ToString();
                                body = "Dear " + empDirDetail.employeeName + ", <br /><br />";
                                body = body + "A new training course "+ trainingObj.Course + " for " + empDetail.employeeName + " has been supported by " + empDetail.lineManagerName + " and now requires your approval or rejection. The cost of the training course is £" + totalCost + ". <br /><br />";
                                body = body + "Please access the Training Approval Report from your Whiteboard alert. <br /><br />";
                                body = body + "Regards <br /><br />";
                                body = body + "HR Team";

                                EmailHelper.sendHtmlFormattedEmail(empDirDetail.employeeName, empDirDetail.employeeEmail, string.Format(EmailSubjectConstants.TrainingApprovalRequired, trainingObj.Course), body);
                            }
                        }
                    }

                    #endregion

                    #region "Email to Employee & Line Manager when Exec Approved or Exec Declined"

                    if (request.status == ApplicationConstants.trainingStatusExecDeclined
                        || request.status == ApplicationConstants.trainingStatusExecApproved)
                    {

                        var statusTitle = (request.status == ApplicationConstants.trainingStatusExecApproved)
                            ? ApplicationConstants.trainingApprovedTitle
                            : ApplicationConstants.trainingDeclinedTitle;

                        #region "Email to Employee"

                        SendEmailNotification(trainingObj, statusTitle);

                        #endregion

                        #region "Email to Line Manager"

                        var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                        if (empDetail != null && empDetail.directorId.HasValue)
                        {
                            var empDirDetail = getEmployeeDetails(empDetail.directorId.Value);

                            if (empDirDetail != null && !string.IsNullOrEmpty(empDetail.lineManagerEmail))
                            {
                                string body = string.Empty;
                                string totalCost = Math.Round(Convert.ToDouble(trainingObj.TotalCost), 2).ToString();
                                body = "Dear " + empDetail.lineManagerName + ", <br /><br />";
                                body = body + string.Format("This is confirmation that training {0} for {1} has been {2} by {3}.", trainingObj.Course, empDetail.employeeName, statusTitle, empDirDetail.employeeName) + "<br /><br />";
                                body = body + "Regards <br /><br />";
                                body = body + "HR Team";

                                EmailHelper.sendHtmlFormattedEmail(empDetail.lineManagerName, empDetail.lineManagerEmail, string.Format(EmailSubjectConstants.LineManagerExecStatus, statusTitle), body);
                            }
                        }

                        #endregion                        

                    }

                    #endregion

                    #region "Email to HR for final approval after the Exec supported"

                    if (request.status == ApplicationConstants.grpTrainingStatusExecSupported)
                    {
                        if (trainingObj.ProfessionalQualification >= 1 || trainingObj.TotalCost > 1000)
                        {
                            var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                            if (empDetail != null && empDetail.directorId.HasValue)
                            {
                                var empDirDetail = getEmployeeDetails(empDetail.directorId.Value);

                                if (empDirDetail != null)
                                {
                                    string mainBody = string.Empty;
                                    string subject = string.Empty;
                                    string body = string.Empty;
                                    string totalCost = Math.Round(Convert.ToDouble(trainingObj.TotalCost), 2).ToString();
                                    if (trainingObj.ProfessionalQualification >= 1)
                                    {
                                        mainBody = mainBody + string.Format("The cost of the training course is £{0} and it's a professional qualification.", totalCost);
                                    }
                                    else
                                    {
                                        mainBody = mainBody + string.Format("The cost of the training course is £{0}.", totalCost);
                                    }

                                    subject = string.Format(EmailSubjectConstants.TrainingApprovalRequired, trainingObj.Course);
                                    body = body + string.Format("A new training course for {0} has been supported by Exec director {1} and now requires your approval or rejection. " + mainBody + " <br /><br />", empDetail.employeeName, empDirDetail.employeeName);
                                    body = body + "Please review the course on the ‘Training Approval List’.";
                                    EmailHelper.sendEmail(body, subject, ApplicationConstants.hrTeamEmailAddress);
                                }
                            }
                        }
                    }

                    #endregion

                }
            }
        }

        #endregion

        #region >>> Group Training <<<

        public AddEmployeeGroupTraining GetGroupDetails(int groupId)
        {
            try
            {
                AddEmployeeGroupTraining response = new AddEmployeeGroupTraining();
                // get group details
                response = (from EET in DbContext.E_EmployeeTrainings
                            join EAP in DbContext.E_EmployeeApprovedTrainings on EET.TrainingId equals EAP.TrainingId
                            join ES in DbContext.E_EmployeeTrainingStatus on EET.Status equals ES.StatusId
                            where EET.GroupId == groupId
                            select new AddEmployeeGroupTraining
                            {
                                trainingId = EET.TrainingId,
                                additionalNotes = EET.AdditionalNotes,
                                course = EET.Course,
                                createdby = EET.Createdby,
                                createdDateTime = EET.CreatedDate,
                                startDateTime = EET.StartDate,
                                endDateTime = EET.EndDate,
                                expiryDate = EET.Expiry,
                                isMandatoryTraining = EET.IsMandatoryTraining,
                                isSubmittedBy = EET.IsSubmittedBy,
                                professionalQualification = EET.ProfessionalQualification,
                                justification = EET.Justification,
                                location = EET.Location,
                                providerName = EET.ProviderName,
                                providerWebsite = EET.ProviderWebsite,
                                status = ES.Title,
                                totalCost = EET.TotalCost,
                                totalNumberOfDays = EET.TotalNumberOfDays,
                                venue = EET.Venue,
                                groupId = EET.GroupId,
                                postcode = EET.Postcode,
                                approvedtraining = EAP.Approvedtraining,
                                isRemuneration = EAP.IsRemuneration,
                                remunerationCost = EAP.RemunerationCost,
                            }).FirstOrDefault();

                response.createdDate = response.createdDateTime?.ToString("dd/MM/yyyy");
                response.expiry = response.expiryDate?.ToString("dd/MM/yyyy");
                response.startDate = response.startDateTime?.ToString("dd/MM/yyyy");
                response.endDate = response.endDateTime?.ToString("dd/MM/yyyy");

                // get group employee details
                List<E_EmployeeTrainings> employeeTrainings = DbContext.E_EmployeeTrainings.Where(x => x.GroupId == groupId && x.Status != 4).ToList();
                for (int count = 0; count < employeeTrainings.Count(); count++)
                {
                    EmployeeDetails empDetail = new EmployeeDetails();
                    empDetail = getGroupTrainingEmployeeDetail((int)employeeTrainings.ElementAt(count).EmployeeId);
                    if (empDetail != null)
                    {
                        response.employeeDetails.Add(empDetail);
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        // For IOS Request
        public TrainingListingResponse GetEmployeesTrainings(int employeeId)
        {
            TrainingListingResponse response = new TrainingListingResponse();
            var currentFiscalYear = getCurrentFiscalYear();
            var fiscalStartDate = currentFiscalYear.YStart;
            var fiscalEndDate = currentFiscalYear.YEnd;
            var data = (from T in DbContext.E_EmployeeTrainings
                        join E in DbContext.E__EMPLOYEE on T.EmployeeId equals E.EMPLOYEEID
                        //join A in DbContext.E_EmployeeApprovedTrainings on T.TrainingId equals A.TrainingId into J_join
                        //from J in J_join.DefaultIfEmpty()
                        join S in DbContext.E_EmployeeTrainingStatus on T.Status equals S.StatusId
                        where T.EmployeeId == employeeId && T.Active == true && DbFunctions.TruncateTime(T.StartDate) >= DbFunctions.TruncateTime(fiscalStartDate) && DbFunctions.TruncateTime(T.StartDate) <= DbFunctions.TruncateTime(fiscalEndDate) &&
                        S.Title != ApplicationConstants.grpTrainingStatusRemoved
                        select new AddEmployeeTrainings
                        {
                            employeeId = T.EmployeeId,
                            additionalNotes = T.AdditionalNotes,
                            course = T.Course,
                            startDateTime = T.StartDate,
                            endDateTime = T.EndDate,
                            expiryDate = T.Expiry,
                            isMandatoryTraining = T.IsMandatoryTraining,
                            isSubmittedBy = T.IsSubmittedBy,
                            justification = T.Justification,
                            location = T.Location,
                            providerName = T.ProviderName,
                            providerWebsite = T.ProviderWebsite,
                            totalCost = T.TotalCost,
                            totalNumberOfDays = T.TotalNumberOfDays,
                            trainingId = T.TrainingId,
                            venue = T.Venue,
                            status = S.Title,
                            renewDateTime = T.Expiry,
                            recordedDateTime = T.CreatedDate,
                            professionalQualification = T.ProfessionalQualification
                        });



            var finalData = data.ToList();
            foreach (AddEmployeeTrainings item in finalData)
            {
                DateTime startDate = Convert.ToDateTime(item.startDateTime);
                item.startDate = startDate.ToString("dd/MM/yyyy");

                DateTime endDate = Convert.ToDateTime(item.endDateTime);
                item.endDate = endDate.ToString("dd/MM/yyyy");

                DateTime expiryDate = Convert.ToDateTime(item.expiryDate);
                item.expiry = expiryDate.ToString("dd/MM/yyyy");

                DateTime recordedDate = Convert.ToDateTime(item.recordedDateTime);
                item.recordedDate = recordedDate.ToString("dd/MM/yyyy");

                var profQualList = (from S in DbContext.E_EmployeeApprovedTrainings
                                    where (S.TrainingId == item.trainingId)
                                    select new ApprovedTraining
                                    {
                                        approvedtraining = S.Approvedtraining,
                                        isRemuneration = S.IsRemuneration,
                                        remunerationCost = S.RemunerationCost
                                    }).ToList();
                if (profQualList.Count() > 0)
                {
                    item.approvedtraining = profQualList.FirstOrDefault()?.approvedtraining;
                    item.isRemuneration = profQualList.FirstOrDefault()?.isRemuneration;
                    item.remunerationCost = profQualList.FirstOrDefault()?.remunerationCost;
                }
            }
            response.trainingList = finalData;
            return response;
        }

        public EmployeesResponse GetTrainingEmployees(EmployeesTrainingRequest model)
        {
            EmployeesResponse response = new EmployeesResponse();

            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on J.JobRoleId equals ET.JobRoleId into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        where
                            J.ACTIVE == 1 &&
                            (D.DIRECTORATEID == model.directorateId || model.directorateId == 0 || model.directorateId == null) &&
                            (J.TEAM == model.teamId || model.teamId == 0 || model.teamId == null) &&
                            ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                        select new Employee
                        {
                            name = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            employeeId = E.EMPLOYEEID,
                            jobTitle = (ET.JobeRoleDescription ?? "N/A"),
                            status = J.ACTIVE
                        });

            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / 15);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"name";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            // Making final response
            List<Employee> employees = new List<Employee>();
            foreach (var item in finalData)
            {
                Employee _employee = new Employee();
                _employee.employeeId = item.employeeId;
                _employee.name = item.name;
                _employee.team = item.team;
                _employee.jobTitle = item.jobTitle;
                _employee.grade = item.grade;
                employees.Add(_employee);
            }
            response.employees = employees;

            return response;
        }

        public void AddEmployeeGroupTrainings(AddEmployeeGroupTraining request)
        {

            try
            {
                var maxgroupId = DbContext.E_EmployeeTrainings.Max(a => a.GroupId).HasValue? DbContext.E_EmployeeTrainings.Max(a => a.GroupId).Value:0;
                int newGroupId = Convert.ToInt32(maxgroupId + 1);
                var statusId = 0;
                //if ((request.professionalQualification.HasValue && request.professionalQualification.Value == 1) ||
                //    request.totalCost > ApplicationConstants.trainingCostLimit)
                //{
                //    // send email to HR Team

                //    statusId = DbContext.E_EmployeeTrainingStatus.FirstOrDefault(e => e.Title == ApplicationConstants.grpTrainingStatusPending).StatusId;
                //}
                //else
                //{
                //    statusId = DbContext.E_EmployeeTrainingStatus.FirstOrDefault(e => e.Title == ApplicationConstants.grpTrainingStatusSubmitted).StatusId;
                //}

                // when HR save then training directly approved for employee
                // send email to HR Team
                statusId = DbContext.E_EmployeeTrainingStatus.FirstOrDefault(e => e.Title == ApplicationConstants.grpTrainingStatusHRApproved).StatusId;

                for (int count = 0; count < request.employeeId.Count(); count++)
                {

                    // send email to HR when employee has appointment and book training on same date.  (US836)
                    if (!checkIfTraining(request.employeeId[count].Value, request.startDate, request.endDate))
                    {
                        var _model = new TrainingApprovalEmailNotification();
                        _model.trainingStatus = request.status;
                        var _empDetail = getEmployeeDetails(request.employeeId[count].Value);
                        if (!string.IsNullOrEmpty(_empDetail.lineManagerEmail))
                        {
                            _model.courseName = request.course;
                            _model.employeeName = _empDetail.employeeName;
                            _model.lineManagerId = _empDetail.lineManagerId;
                            _model.lineManagerName = ApplicationConstants.hrTeamTitle;
                            _model.lineManagerEmail = ApplicationConstants.hrTeamEmailAddress;
                            _model.startDate = request.startDate;
                            _model.endDate = request.endDate;
                            sendEmailNotification(_model, TrainingEnum.TrainingAlreadyExist);
                        }

                        continue;
                    }

                    E_EmployeeTrainings trainingObj = new E_EmployeeTrainings();
                    trainingObj.AdditionalNotes = request.additionalNotes;
                    trainingObj.EmployeeId = request.employeeId[count];
                    trainingObj.Course = request.course;
                    trainingObj.Createdby = request.createdby;
                    trainingObj.CreatedDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(request.endDate))
                        trainingObj.EndDate = Convert.ToDateTime(request.endDate);
                    if (!string.IsNullOrEmpty(request.expiry))
                        trainingObj.Expiry = Convert.ToDateTime(request.expiry);
                    trainingObj.IsMandatoryTraining = request.isMandatoryTraining;
                    trainingObj.IsSubmittedBy = request.isSubmittedBy;
                    trainingObj.ProfessionalQualification = request.professionalQualification;
                    trainingObj.Justification = request.justification;
                    trainingObj.Location = request.location;
                    trainingObj.ProviderName = request.providerName;
                    trainingObj.ProviderWebsite = request.providerWebsite;
                    if (request.startDate != string.Empty || request.startDate != null)
                        trainingObj.StartDate = Convert.ToDateTime(request.startDate);
                    //trainingObj.StartDate = request.startDateTime;
                    trainingObj.Status = statusId;
                    trainingObj.TotalCost = request.totalCost;
                    trainingObj.TotalNumberOfDays = request.totalNumberOfDays;
                    trainingObj.Venue = request.venue;
                    trainingObj.Active = true;
                    trainingObj.Postcode = request.postcode;
                    trainingObj.GroupId = newGroupId; // adding a new group id for each employees record

                    DbContext.E_EmployeeTrainings.Add(trainingObj);
                    DbContext.SaveChanges();

                    E_EmployeeApprovedTrainings approvedTraining = new E_EmployeeApprovedTrainings();
                    approvedTraining.TrainingId = trainingObj.TrainingId;
                    approvedTraining.Approvedtraining = request.approvedtraining;
                    approvedTraining.IsRemuneration = request.isRemuneration;
                    approvedTraining.RemunerationCost = request.remunerationCost;
                    DbContext.E_EmployeeApprovedTrainings.Add(approvedTraining);
                    DbContext.SaveChanges();

                    // send email to employee and his line manager
                    var model = new TrainingApprovalEmailNotification();
                    model.trainingStatus = request.status;
                    model.startDate = trainingObj.StartDate?.ToShortDateString();
                    model.endDate = trainingObj.EndDate?.ToShortDateString();

                    var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);

                    if (empDetail != null && !string.IsNullOrEmpty(empDetail.lineManagerEmail))
                    {
                        model.courseName = trainingObj.Course;
                        model.employeeName = empDetail.employeeName;
                        // send to line manager
                        model.lineManagerId = empDetail.lineManagerId;
                        model.lineManagerName = empDetail.lineManagerName;
                        model.lineManagerEmail = empDetail.lineManagerEmail;
                        sendEmailNotification(model, TrainingEnum.LineManagerGroupTraining);
                        // send to employee
                        model.lineManagerId = empDetail.employeeId;
                        model.lineManagerName = empDetail.employeeName;
                        model.lineManagerEmail = empDetail.employeeEmail;
                        sendEmailNotification(model, TrainingEnum.EmployeeGroupTraining);
                    }

                    // send email to HR when employee has appointment and book training on same date.  (US836)
                    if (!checkIfAppointment(trainingObj.EmployeeId.Value, trainingObj.StartDate.Value, trainingObj.EndDate.Value))
                    {
                        model = new TrainingApprovalEmailNotification();
                        empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                        if (!string.IsNullOrEmpty(empDetail.lineManagerEmail))
                        {
                            model.courseName = trainingObj.Course;
                            model.employeeName = empDetail.employeeName;
                            model.lineManagerId = empDetail.lineManagerId;
                            model.lineManagerName = ApplicationConstants.hrTeamTitle;
                            model.lineManagerEmail = ApplicationConstants.hrTeamEmailAddress;
                            model.startDate = trainingObj.StartDate?.ToShortDateString();
                            model.endDate = trainingObj.EndDate?.ToShortDateString();
                            sendEmailNotification(model, TrainingEnum.AppointmentRearranged);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }

        public GroupTrainingList GetGroupTrainingsList(GroupTrainingApprovalList request)
        {
            try
            {
                GroupTrainingList response = new GroupTrainingList();
                response.groupTrainings = new List<GroupTraining>();
                var data = (from ET in DbContext.E_EmployeeTrainings
                            join J in DbContext.E_JOBDETAILS on ET.EmployeeId equals J.EMPLOYEEID into EJ_join
                            from J in EJ_join.DefaultIfEmpty()
                            join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                            from T in T_join.DefaultIfEmpty()
                            join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                            from D in D_join.DefaultIfEmpty()
                            where ET.GroupId != null && ET.Course.Contains(request.searchText) &&
                                (D.DIRECTORATEID == request.directorateId || request.directorateId == 0 || request.directorateId == null) &&
                                (J.TEAM == request.teamId || request.teamId == 0 || request.teamId == null)
                            && ET.Status != 4 // the training record should be active and not removed
                            group ET by ET.GroupId into groups
                            select new
                            {
                                key = groups.Key,
                                count = groups.Count(),
                                trainner = groups.ToList()
                            }).ToList();

                foreach (var item in data)
                {
                    string createdBy = "N/A";
                    int? createdById = item.trainner.ElementAt(0).Createdby;
                    if (createdById.HasValue)
                    {
                        createdBy = DbContext.E__EMPLOYEE
                                             .Where(e => e.EMPLOYEEID == createdById)
                                             .Select(s => s.FIRSTNAME + " " + s.LASTNAME)
                                             .FirstOrDefault();
                    }

                    GroupTraining trainingObj = new GroupTraining();
                    trainingObj.attendees = item.count;
                    trainingObj.courseName = item.trainner.ElementAt(0).Course;
                    trainingObj.createdDate = item.trainner.ElementAt(0).CreatedDate;
                    trainingObj.recordedDate = item.trainner.ElementAt(0).CreatedDate?.ToString("dd/MM/yyyy");
                    trainingObj.startDate = item.trainner.ElementAt(0).StartDate?.ToString("dd/MM/yyyy");
                    trainingObj.renewDate = item.trainner.ElementAt(0).Expiry?.ToString("dd/MM/yyyy");
                    trainingObj.providerName = item.trainner.ElementAt(0).ProviderName;
                    trainingObj.createdBy = createdBy;
                    trainingObj.isMandatory = item.trainner.ElementAt(0).IsMandatoryTraining;
                    trainingObj.groupId = item.trainner.ElementAt(0).GroupId;
                    response.groupTrainings.Add(trainingObj);
                }

                Pagination page = new Pagination();
                page.totalRows = data.Count();
                page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / 15);
                page.pageNo = request.pagination.pageNo;
                page.pageSize = request.pagination.pageSize;
                int _page = (page.pageNo - 1);

                response.pagination = page;

                // Counting skip
                int _skip = _page * page.pageSize;
                string _sortBy = @"createdDate";
                var finalData = response.groupTrainings.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
                response.groupTrainings = finalData;

                return response;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void UpdateGroupTrainingInfo(AddEmployeeGroupTraining request)
        {
            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    if (request.groupId != null)
                    {
                        int trainingStatus = 0;
                        if (request.status != null)
                        {
                            var status = DbContext.E_EmployeeTrainingStatus.FirstOrDefault(a => a.Title.Equals(request.status));
                            trainingStatus = status.StatusId;
                        }
                        var removed = DbContext.E_EmployeeTrainingStatus.FirstOrDefault(a => a.Title.Equals("Removed"));
                        int removedStatus = removed.StatusId;
                        var trainingGroup = DbContext.E_EmployeeTrainings.Where(group => group.GroupId == request.groupId).ToList();
                        var trainingGroupStatus = trainingGroup.Select(x =>new { x.EmployeeId,x.Status}).ToList();
                        var hrApprovedStatusId = DbContext.E_EmployeeTrainingStatus.FirstOrDefault(e => e.Title == ApplicationConstants.grpTrainingStatusHRApproved).StatusId;

                        foreach (var item in trainingGroup)
                        {
                            item.AdditionalNotes = request.additionalNotes;
                            item.Course = request.course;
                            item.StartDate = Convert.ToDateTime(request.startDate);
                            item.EndDate = Convert.ToDateTime(request.endDate);
                            item.Createdby = request.createdby;
                            if (!string.IsNullOrEmpty(request.expiry))
                                item.Expiry = Convert.ToDateTime(request.expiry);
                            item.IsMandatoryTraining = request.isMandatoryTraining;
                            item.IsSubmittedBy = request.isSubmittedBy;
                            item.Justification = request.justification;
                            item.Location = request.location;
                            item.ProfessionalQualification = request.professionalQualification;
                            item.ProviderName = request.providerName;
                            item.ProviderWebsite = request.providerWebsite;
                            item.Status = trainingStatus;
                            item.TotalCost = request.totalCost;
                            item.TotalNumberOfDays = request.totalNumberOfDays;
                            item.Venue = request.venue;
                            item.Postcode = request.postcode;

                            if (!request.employeeId.Contains(item.EmployeeId))
                            {
                                item.Status = removedStatus;
                            } // remove the rows that are not present in updated request

                        }
                        // get the ids of current employees
                        int[] currentEmployeesArray = new int[trainingGroup.Count()];

                        for (int count = 0; count < trainingGroup.Count(); count++)
                        {
                            currentEmployeesArray[count] = Convert.ToInt32(trainingGroup.ElementAt(count).EmployeeId);
                        }
                        // add the rows that are not present in updated request
                        for (int count = 0; count < request.employeeId.Count(); count++)
                        {
                            if (!currentEmployeesArray.Contains(Convert.ToInt32(request.employeeId[count])))
                            {
                                var employeeId = request.employeeId[count];

                                #region "Email to HR (Training timing conflict with another training)"

                                if (!checkIfTraining(employeeId.Value, request.startDate, request.endDate))
                                {
                                    var objEmail = new TrainingApprovalEmailNotification();
                                    objEmail.trainingStatus = ApplicationConstants.grpTrainingStatusHRApproved;
                                    var empDetail = getEmployeeDetails(employeeId.Value);
                                    if (!string.IsNullOrEmpty(empDetail.lineManagerEmail))
                                    {
                                        objEmail.courseName = request.course;
                                        objEmail.employeeName = empDetail.employeeName;
                                        objEmail.lineManagerId = empDetail.lineManagerId;
                                        objEmail.lineManagerName = ApplicationConstants.hrTeamTitle;
                                        objEmail.lineManagerEmail = ApplicationConstants.hrTeamEmailAddress;
                                        objEmail.startDate = request.startDate;
                                        objEmail.endDate = request.endDate;
                                        sendEmailNotification(objEmail, TrainingEnum.TrainingAlreadyExist);
                                    }

                                    continue;
                                }

                                #endregion

                                #region "Add Employee Training"   

                                E_EmployeeTrainings trainingObj = new E_EmployeeTrainings();
                                trainingObj = trainingGroup.ElementAt(0);
                                trainingObj.EmployeeId = employeeId.Value;
                                trainingObj.GroupId = request.groupId;
                                trainingObj.Status = hrApprovedStatusId;
                                DbContext.E_EmployeeTrainings.Add(trainingObj);
                                DbContext.SaveChanges();

                                #endregion

                                #region "Add in E_EmployeeApprovedTrainings"

                                E_EmployeeApprovedTrainings approvedTraining = new E_EmployeeApprovedTrainings();
                                approvedTraining.TrainingId = trainingObj.TrainingId;
                                approvedTraining.Approvedtraining = request.approvedtraining;
                                approvedTraining.IsRemuneration = request.isRemuneration;
                                approvedTraining.RemunerationCost = request.remunerationCost;
                                DbContext.E_EmployeeApprovedTrainings.Add(approvedTraining);
                                DbContext.SaveChanges();

                                #endregion

                                #region "Email to HR (Training timing conflict with an existing appointment) (US836)"                                
                                if (!checkIfAppointment(trainingObj.EmployeeId.Value, trainingObj.StartDate.Value, trainingObj.EndDate.Value))
                                {
                                    var model = new TrainingApprovalEmailNotification();
                                    var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                                    if (!string.IsNullOrEmpty(empDetail.lineManagerEmail))
                                    {
                                        model.courseName = trainingObj.Course;
                                        model.employeeName = empDetail.employeeName;
                                        model.lineManagerId = empDetail.lineManagerId;
                                        model.lineManagerName = ApplicationConstants.hrTeamTitle;
                                        model.lineManagerEmail = ApplicationConstants.hrTeamEmailAddress;
                                        model.startDate = trainingObj.StartDate?.ToShortDateString();
                                        model.endDate = trainingObj.EndDate?.ToShortDateString();
                                        sendEmailNotification(model, TrainingEnum.AppointmentRearranged);
                                    }
                                }
                                #endregion

                            }
                        }

                        DbContext.SaveChanges();
                        dbContextTransaction.Commit();
                        dbContextTransaction.Dispose();

                        #region "Email to Employee and Line Manager when group training is updated (US836)"

                        var trainingGroupMembers = DbContext.E_EmployeeTrainings.Where(group => group.GroupId == request.groupId && group.Status != removedStatus).ToList();

                        foreach (var member in trainingGroupMembers)
                        {
                            var model = new TrainingApprovalEmailNotification();
                            model.trainingStatus = request.status;
                            model.startDate = member.StartDate?.ToShortDateString();
                            model.endDate = member.EndDate?.ToShortDateString();

                            var empDetail = getEmployeeDetails(member.EmployeeId.Value);
                            int? Id = 0;
                            if (trainingGroupStatus.FirstOrDefault(x => x.EmployeeId == member.EmployeeId) != null)
                            {
                                Id = trainingGroupStatus.FirstOrDefault(x => x.EmployeeId == member.EmployeeId).EmployeeId;
                            }
                            if (Id > 0)
                            {
                                var Status = trainingGroupStatus.FirstOrDefault(x=>x.EmployeeId==member.EmployeeId).Status;
                                if (Status != member.Status)
                                {
                                    if (empDetail != null && !string.IsNullOrEmpty(empDetail.lineManagerEmail))
                                    {
                                        model.courseName = member.Course;
                                        model.employeeName = empDetail.employeeName;

                                        // send to line manager
                                        model.lineManagerId = empDetail.lineManagerId;
                                        model.lineManagerName = empDetail.lineManagerName;
                                        model.lineManagerEmail = empDetail.lineManagerEmail;
                                        sendEmailNotification(model, TrainingEnum.LineManagerGroupTraining);

                                        // send to employee
                                        model.lineManagerId = empDetail.employeeId;
                                        model.lineManagerName = empDetail.employeeName;
                                        model.lineManagerEmail = empDetail.employeeEmail;
                                        sendEmailNotification(model, TrainingEnum.EmployeeGroupTraining);
                                    }
                                }
                            }
                            else
                            {
                                if (empDetail != null && !string.IsNullOrEmpty(empDetail.lineManagerEmail))
                                {
                                    model.courseName = member.Course;
                                    model.employeeName = empDetail.employeeName;

                                    // send to line manager
                                    model.lineManagerId = empDetail.lineManagerId;
                                    model.lineManagerName = empDetail.lineManagerName;
                                    model.lineManagerEmail = empDetail.lineManagerEmail;
                                    sendEmailNotification(model, TrainingEnum.LineManagerGroupTraining);

                                    // send to employee
                                    model.lineManagerId = empDetail.employeeId;
                                    model.lineManagerName = empDetail.employeeName;
                                    model.lineManagerEmail = empDetail.employeeEmail;
                                    sendEmailNotification(model, TrainingEnum.EmployeeGroupTraining);
                                }
                            }

                        }

                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw;
                }
            }
        }

        public void ApproveDeclineGroupTrainings(GroupTrainingApproveDecline request)
        {
            E_EmployeeTrainings trainingObj = new E_EmployeeTrainings();

            if (request.trainingId != null && request.trainingId.Length > 0)
            {
                var _statusExecApprove = (from S in DbContext.E_EmployeeTrainingStatus where (S.Title == request.status) select S).FirstOrDefault();
                var _statusExecSupport = (from S in DbContext.E_EmployeeTrainingStatus where (S.Title == ApplicationConstants.grpTrainingStatusExecSupported) select S).FirstOrDefault();

                foreach (var trainingId in request.trainingId)
                {
                    var trainingResult = (from S in DbContext.E_EmployeeTrainings where (S.TrainingId == trainingId) select S).FirstOrDefault();
                    trainingObj = trainingResult;

                    var status = _statusExecApprove;

                    if (request.status == ApplicationConstants.grpTrainingStatusExecApproved)
                    {
                        if (trainingObj.ProfessionalQualification >= 1 || trainingObj.TotalCost > 1000)
                        {
                            // set status as "Exec Supported"
                            status = _statusExecSupport;
                        }
                    }


                    trainingObj.Updatedby = request.updatedBy;
                    trainingObj.UpdateDate = DateTime.Now;
                    trainingObj.Status = status.StatusId;
                    DbContext.SaveChanges();

                    #region "Email to Employee & Line Manager when Exec Approved or Exec Declined"

                    if (request.status == ApplicationConstants.trainingStatusExecDeclined
                        || status.Title == ApplicationConstants.trainingStatusExecApproved)
                    {

                        var model = new TrainingApprovalEmailNotification();
                        model.trainingStatus = request.status;

                        var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                        if (empDetail != null && empDetail.directorId.HasValue)
                        {
                            var empDirDetail = getEmployeeDetails(empDetail.directorId.Value);

                            if (empDirDetail != null && !string.IsNullOrEmpty(empDetail.lineManagerEmail))
                            {
                                model.courseName = trainingObj.Course;
                                model.employeeName = empDetail.employeeName;
                                model.directorName = empDirDetail.employeeName;
                                model.statusTitle = (model.trainingStatus == ApplicationConstants.trainingStatusExecApproved)
                                ? ApplicationConstants.trainingApprovedTitle
                                : ApplicationConstants.trainingDeclinedTitle;
                                
                                #region "Email to Line Manager"

                                model.lineManagerId = empDetail.lineManagerId;
                                model.lineManagerName = empDetail.lineManagerName;
                                model.lineManagerEmail = empDetail.lineManagerEmail;
                                sendEmailNotification(model, TrainingEnum.ExecApproveDecline);

                                #endregion
                                                                
                                #region "Email to Employee"

                                SendEmailNotification(trainingObj, model.statusTitle);

                                #endregion
                                 
                            }
                        }
                    }

                    #endregion

                    #region "Email to Employee & Line Manager when HR Approved or HR Declined"

                    if (request.status == ApplicationConstants.grpTrainingStatusHRApproved
                        || request.status == ApplicationConstants.grpTrainingStatusHRDeclined)
                    {
                        if (trainingObj.ProfessionalQualification >= 1 || trainingObj.TotalCost > 1000)
                        {

                            var model = new TrainingApprovalEmailNotification();
                            model.trainingStatus = request.status;

                            var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                            if (empDetail != null && empDetail.directorId.HasValue)
                            {
                                var empDirDetail = getEmployeeDetails(empDetail.directorId.Value);

                                if (empDirDetail != null && !string.IsNullOrEmpty(empDetail.lineManagerEmail))
                                {
                                    model.courseName = trainingObj.Course;
                                    model.employeeName = empDetail.employeeName;
                                    model.statusTitle = (model.trainingStatus == ApplicationConstants.grpTrainingStatusHRApproved)
                                    ? ApplicationConstants.trainingApprovedTitle
                                    : ApplicationConstants.trainingDeclinedTitle;

                                    if (request.status == ApplicationConstants.grpTrainingStatusHRDeclined)
                                    {
                                        #region "Send to line manager"
                                        model.lineManagerId = empDetail.lineManagerId;
                                        model.lineManagerName = empDetail.lineManagerName;
                                        model.lineManagerEmail = empDetail.lineManagerEmail;
                                        sendEmailNotification(model, TrainingEnum.HRDeclined);
                                        #endregion

                                        #region "Send to employee"
                                        model.lineManagerId = empDetail.employeeId;
                                        model.lineManagerName = empDetail.employeeName;
                                        model.lineManagerEmail = empDetail.employeeEmail;
                                        sendEmailNotification(model, TrainingEnum.HRDeclined);
                                        #endregion

                                        #region "Send to director"
                                        model.lineManagerId = empDirDetail.employeeId;
                                        model.lineManagerName = empDirDetail.employeeName;
                                        model.lineManagerEmail = empDirDetail.employeeEmail;
                                        sendEmailNotification(model, TrainingEnum.HRDeclined);
                                        #endregion
                                    }
                                    else
                                    {
                                        #region "Send to employee"
                                        model.lineManagerId = empDetail.employeeId;
                                        model.lineManagerName = empDetail.employeeName;
                                        model.lineManagerEmail = empDetail.employeeEmail;
                                        sendEmailNotification(model, TrainingEnum.HRApproved);
                                        #endregion
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    #region "Email to HR for final approval after the Exec supported"

                    if (status.Title == ApplicationConstants.grpTrainingStatusExecSupported)
                    {
                        if (trainingObj.ProfessionalQualification >= 1 || trainingObj.TotalCost > 1000)
                        {
                            var empDetail = getEmployeeDetails(trainingObj.EmployeeId.Value);
                            if (empDetail != null && empDetail.directorId.HasValue)
                            {
                                var empDirDetail = getEmployeeDetails(empDetail.directorId.Value);

                                if (empDirDetail != null)
                                {
                                    string mainBody = string.Empty;
                                    string subject = string.Empty;
                                    string body = string.Empty;

                                    string totalCost = Math.Round(Convert.ToDouble(trainingObj.TotalCost), 2).ToString();

                                    if (trainingObj.ProfessionalQualification >= 1)
                                    {
                                        mainBody = mainBody + string.Format("The cost of the training course is £{0} and it's a professional qualification.", totalCost);
                                    }
                                    else
                                    {
                                        mainBody = mainBody + string.Format("The cost of the training course is £{0}.", totalCost);
                                    }

                                    subject = string.Format(EmailSubjectConstants.TrainingApprovalRequired, trainingObj.Course);
                                    body = body + string.Format("A new training course for {0} has been supported by Exec director {1} and now requires your approval or rejection. " + mainBody + " <br /><br />", empDetail.employeeName, empDirDetail.employeeName);
                                    body = body + "Please review the course on the ‘Training Approval List’.";
                                    EmailHelper.sendEmail(body, subject, ApplicationConstants.hrTeamEmailAddress);
                                }
                            }
                        }
                    }

                    #endregion

                }
            }

        }

        public void RemoveEmployeeFromGroupTraining(AddOrRemoveEmployeeInGroupTraining request)
        {
            using (var dbContextTransaction = DbContext.Database.BeginTransaction())
            {
                try
                {
                    var group = DbContext.E_EmployeeTrainings.FirstOrDefault(emp => emp.GroupId == request.groupId);
                    if (group == null)
                    {
                        // If group does not exists
                        dbContextTransaction.Rollback();
                        dbContextTransaction.Dispose();
                        throw new ArgumentException("No such group exists");
                    }
                    var employee = DbContext.E_EmployeeTrainings.FirstOrDefault(emp => emp.EmployeeId == request.employeeid && emp.GroupId == request.groupId);
                    if (employee != null)
                    {
                        // Employee already exists in this group then remove
                        DbContext.E_EmployeeTrainings.Remove(employee);
                        DbContext.SaveChanges();
                        dbContextTransaction.Commit();
                        dbContextTransaction.Dispose();
                    }
                    else
                    {
                        dbContextTransaction.Rollback();
                        dbContextTransaction.Dispose();
                        throw new ArgumentException("User does not exist in this group");
                    }

                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    dbContextTransaction.Dispose();
                    throw new ArgumentException(ex.ToString());
                }
            }
        }

        public void AddEmployeeInGroupTraining(AddOrRemoveEmployeeInGroupTraining request)
        {
            try
            {
                var group = DbContext.E_EmployeeTrainings.FirstOrDefault(emp => emp.GroupId == request.groupId);
                if (group == null)
                {
                    // If group does not exists
                    throw new ArgumentException("No such group exists");
                }
                var employee = DbContext.E_EmployeeTrainings.FirstOrDefault(emp => emp.EmployeeId == request.employeeid && emp.GroupId == request.groupId);
                if (employee != null)
                {
                    // Employee already exists in this group
                    throw new ArgumentException(UserMessageConstants.AddEmployeeGroupAlreadyExists);
                }
                else
                {
                    // if group exists but the employee does not exist in the group already, then add
                    var data = DbContext.E_EmployeeTrainings.FirstOrDefault(row => row.GroupId == request.groupId);
                    E_EmployeeTrainings training = new E_EmployeeTrainings();
                    training = data;
                    training.EmployeeId = request.employeeid;

                    DbContext.E_EmployeeTrainings.Add(training);
                    DbContext.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public List<E_EmployeeTrainingStatus> GetAllStatuses()
        {
            try
            {
                return DbContext.E_EmployeeTrainingStatus.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<E__EMPLOYEE> GetAllEmployees()
        {
            return DbContext.E__EMPLOYEE.ToList();
        }
        public List<E__EMPLOYEE> GetEmployeesByTeam(int teamId)
        {
            var employee = (from E in DbContext.E__EMPLOYEE
                            join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                            where J.TEAM == teamId || teamId == 0
                            select E).ToList();
            return employee;
        }
        public List<E_DIRECTORATES> GetAllDirectorates()
        {
            try
            {
                return DbContext.E_DIRECTORATES.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region >>> Report Training Approval List <<<

        public List<TrainingApprovalList> GetTrainingApprovalList(TrainingApprovalListRequest request)
        {
            try
            {
                //var directorate = 0;
                List<TrainingApprovalList> trainingApprovalList = new List<TrainingApprovalList>();
                List<E_EmployeeTrainings> groupTrainingList = new List<E_EmployeeTrainings>();

                var query = (from EET in DbContext.E_EmployeeTrainings
                             join ES in DbContext.E_EmployeeTrainingStatus on EET.Status equals ES.StatusId
                             join E in DbContext.E__EMPLOYEE on EET.EmployeeId equals E.EMPLOYEEID
                             join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                             join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                             from T in T_join.DefaultIfEmpty()
                                 //join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                                 // from D in D_join.DefaultIfEmpty()
                             where
                             //EET.GroupId != null &&
                             //(EET.ProfessionalQualification == 1 || EET.TotalCost > 1000)
                             EET.Active == true
                             && J.ACTIVE == 1
                             //&& ES.Title == "Pending"
                             && (EET.EmployeeId == request.employeeId || request.employeeId == 0)
                             //&& (D.DIRECTORATEID == request.directorateId || request.directorateId == 0)
                             && (T.TEAMID == request.teamId || request.teamId == 0)
                             //&& (EET.Status == request.statusId || ES.Title == ApplicationConstants.grpTrainingStatusRequested)
                             && ((EET.Course).Contains(request.searchText))
                             select new { EET, ES, T });

                // set query according to isHR
                if (!request.isHR && !request.isDirector)
                {
                    request.isHR = true;
                }

                if (request.statusId > 0)
                {
                    query = query.Where(e => e.EET.Status == request.statusId);
                }
                if (request.directorateId > 0)
                {
                    query = query.Where(e => e.T.DIRECTORATEID == request.directorateId);
                }
                if (request.isHR)
                {
                    query = query.Where(e => (((e.EET.ProfessionalQualification == 1 || e.EET.TotalCost > 1000) && e.ES.Title == ApplicationConstants.grpTrainingStatusExecSupported) ||
                                    ((e.EET.ProfessionalQualification == 1 || e.EET.TotalCost > 1000) && e.EET.Status == request.statusId)));
                }

                // set query according to isDirector
                if (request.isDirector)
                {
                    var data = (from E in DbContext.E__EMPLOYEE
                                join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                                join ET in DbContext.E_EmployeeTrainings on E.EMPLOYEEID equals ET.EmployeeId
                                join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID }
                                where T.DIRECTOR == request.loginUserId && J.ACTIVE == 1
                                select E.EMPLOYEEID);

                    List<int> employeeIds = data.ToList();

                    query = query.Where(e => (
                    ((e.EET.ProfessionalQualification == 1 || e.EET.TotalCost > 1000) && e.ES.Title == ApplicationConstants.grpTrainingStatusRequested) ||

                    ((e.EET.ProfessionalQualification <= 0 || e.EET.TotalCost <= 1000) && e.ES.Title == ApplicationConstants.grpTrainingStatusManagerSupported))
                      && (employeeIds.Contains((int)e.EET.EmployeeId))
                        );
                }

                groupTrainingList = query.Select(e => e.EET).ToList();

                //if (request.statusId > 0)
                //{
                //    groupTrainingList = groupTrainingList.Where(e => e.Status == request.statusId).ToList();
                //}

                foreach (var item in groupTrainingList)
                {
                    string status = "";
                    status = GetStatusName(item.Status.Value);
                    string directorateName = "No Directorate";
                    string teamName = "No Team";
                    int employeeId = item.EmployeeId.Value;
                    E_JOBDETAILS job = DbContext.E_JOBDETAILS.FirstOrDefault(x => x.EMPLOYEEID == employeeId);
                    if (job != null) // getting the employees directorates name, using E_TEAM, E_JOBDETAILS and finally E_DIRECTORATES
                    {
                        if (job.TEAM != null)
                        {
                            E_TEAM team = DbContext.E_TEAM.FirstOrDefault(x => x.TEAMID == job.TEAM);
                            teamName = team.TEAMNAME;
                            if (team.DIRECTORATEID != null)
                            {
                                E_DIRECTORATES direc = DbContext.E_DIRECTORATES.FirstOrDefault(x => x.DIRECTORATEID == team.DIRECTORATEID);
                                directorateName = direc.DIRECTORATENAME;
                            }
                        }
                    }
                    int createdEmployeeId = item.Createdby.Value;
                    E__EMPLOYEE createdByEmployee = DbContext.E__EMPLOYEE.FirstOrDefault(x => x.EMPLOYEEID == createdEmployeeId);

                    TrainingApprovalList trainingApprovalItem = new TrainingApprovalList();
                    trainingApprovalItem.trainingId = item.TrainingId;
                    trainingApprovalItem.employeeId = employeeId;
                    trainingApprovalItem.cost = Convert.ToDouble(item.TotalCost ?? 0);
                    trainingApprovalItem.createdBy = createdByEmployee.FIRSTNAME + " " + createdByEmployee.LASTNAME;
                    trainingApprovalItem.directorateName = directorateName;
                    trainingApprovalItem.teamName = teamName;
                    trainingApprovalItem.status = status;
                    trainingApprovalItem.isMandatory = item.IsMandatoryTraining.Value;
                    if (item.ProfessionalQualification == 1)
                    {
                        trainingApprovalItem.professionalQualification = true;
                    }
                    else
                    {
                        trainingApprovalItem.professionalQualification = false;
                    }
                    trainingApprovalItem.trainingCourseName = item.Course;
                    E__EMPLOYEE traineeEmployeeName = DbContext.E__EMPLOYEE.FirstOrDefault(x => x.EMPLOYEEID == employeeId);
                    if (traineeEmployeeName != null)
                    {
                        trainingApprovalItem.employeeName = traineeEmployeeName.FIRSTNAME + " " + traineeEmployeeName.LASTNAME;
                    }
                    else
                    {
                        trainingApprovalItem.employeeName = " ";
                    }

                    trainingApprovalList.Add(trainingApprovalItem);
                }

                return trainingApprovalList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region removed methods



        //public EmployeesResponse GetTrainingEmployeesByDirectorateId(EmployeesTrainingRequest model)
        //{

        //    EmployeesResponse response = new EmployeesResponse();

        //    var data = (from E in DbContext.E__EMPLOYEE
        //                join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
        //                join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
        //                from T in T_join.DefaultIfEmpty()
        //                join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
        //                from D in D_join.DefaultIfEmpty()
        //                join ET in DbContext.E_JOBROLE on J.JobRoleId equals ET.JobRoleId into ET_join
        //                from ET in ET_join.DefaultIfEmpty()
        //                where J.ACTIVE == 1 &&
        //                    (D.DIRECTORATEID == model.directorateId || model.directorateId == null) &&
        //                    (J.TEAM == model.teamId || model.teamId == null) &&
        //                    ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
        //                select new Employee
        //                {
        //                    name = (E.FIRSTNAME + " " + E.LASTNAME),
        //                    team = (T.TEAMNAME ?? "No Team"),
        //                    employeeId = E.EMPLOYEEID,
        //                    jobTitle = (ET.JobeRoleDescription ?? "N/A"),
        //                    status = J.ACTIVE

        //                });

        //    if (model.status == 0)
        //    {
        //        data = data.Where(J => J.status == null || J.status == 0);
        //    }
        //    else if (model.status == 1)
        //    {
        //        data = data.Where(J => J.status == 1);
        //    }

        //    Pagination page = new Pagination();
        //    page.totalRows = data.ToList().Count();
        //    page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / 15);
        //    page.pageNo = model.pagination.pageNo;
        //    page.pageSize = model.pagination.pageSize;
        //    int _page = (page.pageNo - 1);

        //    response.pagination = page;

        //    // Counting skip
        //    int _skip = _page * page.pageSize;
        //    var finalData = data.ToList();

        //    string _sortBy = @"";
        //    if (model.sortBy == null)
        //    {
        //        _sortBy = @"name";
        //    }
        //    else
        //    {
        //        _sortBy = model.sortBy;
        //    }
        //    // Getting sorted by sort parameter
        //    if (model.sortOrder == "ASC")
        //    {
        //        finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
        //    }
        //    else
        //    {
        //        finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
        //    }

        //    // Making final response
        //    List<Employee> employees = new List<Employee>();
        //    foreach (var item in finalData)
        //    {
        //        Employee _employee = new Employee();
        //        _employee.employeeId = item.employeeId;
        //        _employee.name = item.name;
        //        _employee.team = item.team;
        //        _employee.jobTitle = item.jobTitle;
        //        _employee.grade = item.grade;
        //        employees.Add(_employee);
        //    }
        //    response.employees = employees;

        //    return response;
        //}
        //public EmployeesResponse GetTrainingEmployeesByTeamId(EmployeesTrainingRequest model)
        //{

        //    EmployeesResponse response = new EmployeesResponse();

        //    var data = (from E in DbContext.E__EMPLOYEE
        //                join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
        //                join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
        //                from T in T_join.DefaultIfEmpty()
        //                join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
        //                from D in D_join.DefaultIfEmpty()
        //                join ET in DbContext.E_JOBROLE on J.JobRoleId equals ET.JobRoleId into ET_join
        //                from ET in ET_join.DefaultIfEmpty()
        //                where J.ACTIVE == 1 &&
        //                    (D.DIRECTORATEID == model.directorateId || model.directorateId == null) &&
        //                    (J.TEAM == model.teamId || model.teamId == null) &&
        //                    ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
        //                select new Employee
        //                {
        //                    name = (E.FIRSTNAME + " " + E.LASTNAME),
        //                    team = (T.TEAMNAME ?? "No Team"),
        //                    employeeId = E.EMPLOYEEID,
        //                    jobTitle = (ET.JobeRoleDescription ?? "N/A"),
        //                    status = J.ACTIVE

        //                });

        //    if (model.status == 0)
        //    {
        //        data = data.Where(J => J.status == null || J.status == 0);
        //    }
        //    else if (model.status == 1)
        //    {
        //        data = data.Where(J => J.status == 1);
        //    }

        //    Pagination page = new Pagination();
        //    page.totalRows = data.ToList().Count();
        //    page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / 15);
        //    page.pageNo = model.pagination.pageNo;
        //    page.pageSize = model.pagination.pageSize;
        //    int _page = (page.pageNo - 1);

        //    response.pagination = page;

        //    // Counting skip
        //    int _skip = _page * page.pageSize;
        //    var finalData = data.ToList();

        //    string _sortBy = @"";
        //    if (model.sortBy == null)
        //    {
        //        _sortBy = @"name";
        //    }
        //    else
        //    {
        //        _sortBy = model.sortBy;
        //    }
        //    // Getting sorted by sort parameter
        //    if (model.sortOrder == "ASC")
        //    {
        //        finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
        //    }
        //    else
        //    {
        //        finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
        //    }

        //    // Making final response
        //    List<Employee> employees = new List<Employee>();
        //    foreach (var item in finalData)
        //    {
        //        Employee _employee = new Employee();
        //        _employee.employeeId = item.employeeId;
        //        _employee.name = item.name;
        //        _employee.team = item.team;
        //        _employee.jobTitle = item.jobTitle;
        //        _employee.grade = item.grade;
        //        employees.Add(_employee);
        //    }
        //    response.employees = employees;

        //    return response;
        //}

        //public List<E_EmployeeTrainings> GetTrainingEmployeesByDirectorateId(int directorateId)
        //{
        //    var data = (from EET in DbContext.E_EmployeeTrainings
        //                join E in DbContext.E__EMPLOYEE on EET.EmployeeId equals E.EMPLOYEEID
        //                join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
        //                join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
        //                from T in T_join.DefaultIfEmpty()
        //                join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
        //                from D in D_join.DefaultIfEmpty()
        //                where
        //                  // (J.ACTIVE == model.status || J.ACTIVE == null) &&
        //                  (J.TEAM != 1 ||
        //                  J.TEAM == null) &&
        //                   ((D.DIRECTORATEID == directorateId))
        //                   && EET.GroupId != null
        //                select EET).ToList();
        //    return data;
        //}


        #endregion

    }
}
