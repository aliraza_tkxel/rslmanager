﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Remitance
{
    public class RemitanceRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public RemitanceRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }

        public Organization getSupplierData(int supplierId)
        {
            try
            {
                var data = (from org in DbContext.S_GetOrganizationData(supplierId)
                            select new Organization()
                            {
                                name = org.NAME,
                                address = org.ADDRESS1,
                                address2 = org.ADDRESS2,
                                address3 = org.ADDRESS3,
                                townCity = org.TOWNCITY,
                                postCode = org.POSTCODE,
                                county = org.COUNTY
                            }).FirstOrDefault();

                return data as Organization;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<InvoiceDetail> getInvoiceDetail(string processDate, int supplierId, int paymentTypeId)
        {
            try
            {
                var data = (from inv in DbContext.S_GetInvoiceData(processDate, supplierId, paymentTypeId)
                            select new InvoiceDetail()
                            {
                                totalGross = (Double)inv.TOTAL_GROSS,
                                purchaseCount = (Double)inv.PURCHASE_COUNT,
                                taxDate = (DateTime)inv.TAXDATE,
                                invoiceNumber = inv.INVOICENUMBER,
                                orderId = Convert.ToInt32(inv.ORDERID)

                            }).ToList();

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
