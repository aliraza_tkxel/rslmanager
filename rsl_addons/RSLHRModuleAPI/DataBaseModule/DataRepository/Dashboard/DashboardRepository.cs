﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using NetwrokModel.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataBaseModule.DataRepository.Dashboard
{
    public class DashboardRepository
    {
        public Entities DbContext { get; set; }

        public DashboardRepository(Entities dbContext)
        {
            this.DbContext = dbContext;

        }

        #region Helpers

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }

        #endregion

        #region Get Main Detail Alert
        public int GetMainDetailAlert(int teamId)
        {
            int result = 0;

            var empHistory = (from Emp in DbContext.E__EMPLOYEE_HISTORY
                              select Emp.EMPLOYEEID).Distinct();

            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join H in empHistory on E.EMPLOYEEID equals H
                        where (J.TEAM != 1 || J.TEAM == null)
                        // (J.ACTIVE == model.status || J.ACTIVE == null) &&
                        // ((J.TEAM == teamId) || (J.TEAM != teamId || teamId==0 || J.TEAM !=1 || J.TEAM == null))
                        select new Employee
                        {
                            name = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            employeeId = E.EMPLOYEEID,
                            jobTitle = (ET.JobeRoleDescription ?? "N/A"),
                            grade = (G.DESCRIPTION ?? "-"),
                            status = J.ACTIVE,
                            teamId = J.TEAM

                        });
            if (data.Count() > 0)
            {
                if (teamId > 0)
                {
                    data = data.Where(J => J.teamId == teamId);
                }

                result = data.Count();
            }
            return result;
        }
        #endregion

        #region Get Full Time Staff Alert
        public int GetFullTimeStaffAlert(int teamId)
        {
            int result = 0;

            var fullTime = (from f in DbContext.E_PARTFULLTIME
                            where f.DESCRIPTION == "Full Time"
                            select f).FirstOrDefault().PARTFULLTIMEID;

            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                        from ETH in ETH_join.DefaultIfEmpty()
                        where (J.ENDDATE == null && J.ACTIVE == 1 && J.PARTFULLTIME != null)
                        select new EstablishmentDetail
                        {
                            fullName = (E.FIRSTNAME + " " + E.LASTNAME),
                            directorate = (T.TEAMNAME ?? "No Team"),
                            employeeId = E.EMPLOYEEID,
                            jobTitle = (ET.JobeRoleDescription ?? "N/A"),
                            grade = (G.DESCRIPTION ?? "-"),
                            dobString = E.DOB,
                            endDateString = J.ENDDATE,
                            ethnicity = (ETH.DESCRIPTION ?? "-"),
                            gender = E.GENDER,
                            salary = J.SALARY,
                            startDateString = J.STARTDATE,
                            isDisciplinary = false,
                            status = J.ACTIVE,
                            teamId = J.TEAM
                        });
            if (data.Count() > 0)
            {
                if (teamId > 0)
                {
                    data = data.Where(J => J.teamId == teamId);
                }
                result = data.Count();
            }
            return result;
        }
        #endregion


        #region Get Annual Leave Stats

        public int getAnnualLeaveStats(int teamId)
        {
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                        join EL in DbContext.E__EMPLOYEE on J.LINEMANAGER equals EL.EMPLOYEEID
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        where J.ACTIVE == 1 && (J.PARTFULLTIME == 1 || J.PARTFULLTIME == 2) &&
                         (J.TEAM == teamId || teamId == 0)
                        select E).ToList();
            return data.Count();
        }

        #endregion


        #region Get Sickness Alert
        public int GetSicknessAlert(int teamId)
        {
            int result = 0;

            SicknessList response = new SicknessList();
            var maxResults = from p in DbContext.E_ABSENCE
                             where p.STARTDATE.Value.Year == DateTime.Now.Year
                             group p by p.JOURNALID into g
                             select new { JournalId = g.Key, MaxABSENCEHISTORYID = g.Max(s => s.ABSENCEHISTORYID) };

            var data = (from E in DbContext.E__EMPLOYEE
                        join ej in DbContext.E_JOURNAL on E.EMPLOYEEID equals ej.EMPLOYEEID
                        join ea in DbContext.E_ABSENCE on ej.JOURNALID equals ea.JOURNALID
                        join m in maxResults on ea.ABSENCEHISTORYID equals m.MaxABSENCEHISTORYID
                        join r in DbContext.E_ABSENCEREASON on ea.REASONID equals r.SID
                        join es in DbContext.E_STATUS on ej.CURRENTITEMSTATUSID equals es.ITEMSTATUSID
                        join en in DbContext.E_NATURE on ej.ITEMNATUREID equals en.ITEMNATUREID
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        where ea.ITEMSTATUSID != 20 && ea.DURATION != null && T.TEAMID != 1 && T.ACTIVE == 1 &&
                         ej.ITEMNATUREID == 1 && (J.TEAM == teamId || teamId == 0) && (ea.RETURNDATE == null)
                        select new SicknessDetail
                        {
                            fullName = (E.FIRSTNAME + " " + E.LASTNAME),
                            directorate = (T.TEAMNAME ?? "No Team"),
                            employeeId = E.EMPLOYEEID,
                            reason = (r.DESCRIPTION ?? "No reason available."),
                            startDateString = ea.STARTDATE,
                            actualReturnString = ea.RETURNDATE,
                            duration = ea.DURATION,
                            journalId = ej.JOURNALID,
                            returnNotes = ea.NOTES,
                            endDateString = J.ENDDATE,
                            status = J.ACTIVE
                        });
            if (data.Count() > 0)
            {
                result = data.Count();
            }
            return result;
        }
        #endregion

        #region Get New Starter Alert
        public int GetNewStarterAlert(int teamId)
        {
            int result = 0;

            NewStarterListing response = new NewStarterListing();
            DateTime jobStartDate = DateTime.Now.AddMonths(-2);
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on J.GRADE equals G.GRADEID into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on J.JobRoleId equals ET.JobRoleId into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                        from ETH in ETH_join.DefaultIfEmpty()

                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()


                        where J.ENDDATE == null && J.STARTDATE >= jobStartDate &&
                         (J.TEAM == teamId || teamId == 0)
                        select new NewStarterDetail
                        {
                            directorate = (T.TEAMNAME ?? "No Team"),
                            dobDate = E.DOB,
                            employeeId = E.EMPLOYEEID,
                            ethnicity = (ETH.DESCRIPTION ?? "N/A"),
                            fullName = E.FIRSTNAME + " " + E.LASTNAME,
                            gender = (E.GENDER ?? "-"),
                            grade = G.DESCRIPTION,
                            jobTitle = ET.JobeRoleDescription,
                            salary = J.SALARY,
                            reviewDateTime = J.REVIEWDATE,
                            startDateTime = J.STARTDATE,
                            status = J.ACTIVE,
                            endDateString = J.ENDDATE

                        });
            if (data.Count() > 0)
            {

                var fiscalYear = getCurrentFiscalYear();
                if (fiscalYear != null)
                {
                    F_FISCALYEARS fy = fiscalYear;
                    DateTime? fromDate = fy.YStart;
                    DateTime? toDate = fy.YEnd;
                    toDate = toDate.Value.AddHours(23);
                    toDate = toDate.Value.AddMinutes(59);
                    toDate = toDate.Value.AddSeconds(59);
                    data = data.Where(J => (J.startDateTime >= fromDate && J.startDateTime <= toDate));
                }
                result = data.Count();
            }
            return result;
        }
        #endregion

        #region Get Holiday Alert
        public int GetHolidayAlert(int teamId)
        {
            int result = 0;

            SicknessList response = new SicknessList();
            var maxResults = from p in DbContext.E_ABSENCE
                             where p.STARTDATE.Value.Year == DateTime.Now.Year
                             group p by p.JOURNALID into g
                             select new { JournalId = g.Key, MaxABSENCEHISTORYID = g.Max(s => s.ABSENCEHISTORYID) };

            var data = (from E in DbContext.E__EMPLOYEE
                        join ej in DbContext.E_JOURNAL on E.EMPLOYEEID equals ej.EMPLOYEEID
                        join ea in DbContext.E_ABSENCE on ej.JOURNALID equals ea.JOURNALID
                        join m in maxResults on ea.ABSENCEHISTORYID equals m.MaxABSENCEHISTORYID
                        join r in DbContext.E_ABSENCEREASON on ea.REASONID equals r.SID
                        join es in DbContext.E_STATUS on ej.CURRENTITEMSTATUSID equals es.ITEMSTATUSID
                        join en in DbContext.E_NATURE on ej.ITEMNATUREID equals en.ITEMNATUREID
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        where ea.ITEMSTATUSID != 20 && ea.DURATION != null && J.TEAM != 1 && T.ACTIVE == 1 &&
                         ej.ITEMNATUREID == 2 && (J.TEAM == teamId || teamId == 0)
                         && (ea.STARTDATE >= DateTime.Now && ea.RETURNDATE <= DateTime.Now)
                        select new SicknessDetail
                        {
                            fullName = (E.FIRSTNAME + " " + E.LASTNAME),
                            directorate = (T.TEAMNAME ?? "No Team"),
                            employeeId = E.EMPLOYEEID,
                            reason = (r.DESCRIPTION ?? "No reason available."),
                            startDateString = ea.STARTDATE,
                            actualReturnString = ea.RETURNDATE,
                            duration = ea.DURATION,
                            journalId = ej.JOURNALID,
                            returnNotes = ea.NOTES,
                            endDateString = J.ENDDATE,
                            status = J.ACTIVE
                        });
            if (data.Count() > 0)
            {
                result = data.Count();
            }
            return result;
        }
        #endregion

        #region Get PayPoint Alert
        public int GetPayPointAlert(int teamId)
        {
            int result = 0;
            var fData = getCurrentFiscalYear();

            var data = (from p in DbContext.E_PaypointSubmission
                        join e in DbContext.E__EMPLOYEE on p.employeeId equals e.EMPLOYEEID
                        join j in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID
                        join g in DbContext.E_GRADE on j.GRADE equals g.GRADEID
                        join gp in DbContext.E_GRADE_POINT on j.GRADEPOINT equals gp.POINTID
                        join T in DbContext.E_TEAM on j.TEAM equals T.TEAMID into T_join
                        from T in T_join.DefaultIfEmpty()
                        join s in DbContext.E_PayPointStatus on p.paypointStatusId equals s.PayPointStatusId into s_join
                        from s in s_join.DefaultIfEmpty()
                        where  ((j.TEAM == teamId || teamId == 0 ) && p.fiscalYearId == fData.YRange)
                
                        select new PayPointReport()
                        {
                            directorate = T.TEAMNAME,
                            grade = j.GRADE,
                            gradePoint = j.GRADEPOINT,
                            name = e.FIRSTNAME + " " + e.LASTNAME,
                            reviewDate = j.REVIEWDATE.ToString(),
                            status = s.Description
                        });
            if (data.Count() > 0)
            {
                result = data.Count();
            }
            return result;
        }
        #endregion

        #region Get Mandatory Training Alert
        public int GetMandatoryTrainingAlert(int teamId)

        {
            int result = 0;

            var data = (from T in DbContext.E_EmployeeTrainings
                        join E in DbContext.E__EMPLOYEE on T.EmployeeId equals E.EMPLOYEEID
                        join S in DbContext.E_EmployeeTrainingStatus on T.Status equals S.StatusId
                        join j in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals j.EMPLOYEEID
                        join t in DbContext.E_TEAM on j.TEAM equals t.TEAMID
                        where T.IsMandatoryTraining == true && T.Active == true
                        && (t.TEAMID == teamId || teamId == 0)
                        && T.Expiry > DateTime.Now
                        select new AddEmployeeTrainings
                        {
                            employeeId = T.EmployeeId,
                            additionalNotes = T.AdditionalNotes,
                            course = T.Course,
                            startDateTime = T.StartDate,
                            endDateTime = T.EndDate,
                            expiryDate = T.Expiry,
                            isMandatoryTraining = T.IsMandatoryTraining,
                            isSubmittedBy = T.IsSubmittedBy,
                            justification = T.Justification,
                            location = T.Location,
                            providerName = T.ProviderName,
                            providerWebsite = T.ProviderWebsite,
                            totalCost = T.TotalCost,
                            totalNumberOfDays = T.TotalNumberOfDays,
                            trainingId = T.TrainingId,
                            venue = T.Venue,
                            status = S.Title
                        });

            if (data.Count() > 0)
            {
                result = data.Count();
            }
            return result;
        }
        #endregion

        #region Get Health Alert
        public int GetHealthAlert(int teamId)
        {
            int result = 0;
            var data = (from d in DbContext.E_DIFFDISID
                        join dh in DbContext.E_DIFFDISID_HISTORY on d.DISABILITYID equals dh.DISABILITYID
                        join e in DbContext.E__EMPLOYEE on d.EMPLOYEEID equals e.EMPLOYEEID
                        join t in DbContext.E_DIFFDIS on d.DIFFDIS equals t.DIFFDISID
                        join log in DbContext.E__EMPLOYEE on d.LASTACTIONUSER equals log.EMPLOYEEID
                        join j in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID

                        join T in DbContext.E_TEAM on new { TEAMID = (int)j.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()

                        where j.TEAM == teamId || teamId == 0
                        select new DifficultiesAndDisablities
                        {
                            description = t.DESCRIPTION,
                            disablityId = d.DIFFDIS,
                            notes = d.NOTES,
                            notifiedDateString = d.NotifiedDate,
                            lastActionTime = d.LASTACTIONTIME,
                            firstName = log.FIRSTNAME,
                            lastName = log.LASTNAME,
                            diffDisabilityId = d.DISABILITYID,
                            loggedBy = d.LASTACTIONUSER

                        });

            if (data.Count() > 0)
            {
                var fiscalYear = getCurrentFiscalYear();
                if (fiscalYear != null)
                {
                    F_FISCALYEARS fy = fiscalYear;
                    DateTime? fromDate = fy.YStart;
                    DateTime? toDate = DateTime.Now;
                    data = data.Where(J => (J.lastActionTime >= fromDate && J.lastActionTime <= toDate));
                }
                result = data.Count();
            }
            return result;
        }
        #endregion

        #region Get Dashboard Stats
        public DashboardStatsResponse GetDashboardStats(AbsenceRequest request)
        {
            var response = new DashboardStatsResponse();
            DateTime? fiscalYear = null;
            if (!string.IsNullOrEmpty(request.fiscalYearDate))
            {
                fiscalYear = GeneralHelper.GetDateTimeFromString(request.fiscalYearDate);
            }
            var myPendingLeavesCount = DbContext.E_LeavesRequested(request.employeeId, fiscalYear).ToList().Count();
            var myStaffPendingLeavesCount = DbContext.E_MyTeamLeavesRequested(request.employeeId).ToList().Count();

            var myPendingOtherLeavesCount = DbContext.E_LeaveOfAbsense(request.employeeId, fiscalYear)
                .Where(x => x.Status == ApplicationConstants.absenceStatusPending &&
                    x.Nature != "Personal Day" && x.Nature != "Birthday Leave"
                ).ToList().Count();
            var myStaffPendingOtherLeavesCount = DbContext.E_MyTeamLeaveOfAbsenseRequested(request.employeeId).ToList().Count();

            var myBirthdayCount = DbContext.E_BirthdayPersonalLeavesRequested(request.employeeId, "Birthday Leave", fiscalYear).ToList().Count();
            var myPersonalDayCount = DbContext.E_BirthdayPersonalLeavesRequested(request.employeeId, "Personal Day", fiscalYear).ToList().Count();

            var myStaffBirthdayCount = DbContext.E_MyTeamBirthdayPersonalLeavesRequested(request.employeeId, "Birthday Leave").ToList().Count();
            var myStaffPersonalCount = DbContext.E_MyTeamBirthdayPersonalLeavesRequested(request.employeeId, "Personal Day").ToList().Count();

            var myToilLeaves = DbContext.E_MyLeaveOfToil(request.employeeId).ToList().Count();
            var mySTaffToilLeaves = DbContext.E_MyStaffLeaveOfToil(request.employeeId).ToList().Count();

            var annualLeaves = new DashBoardStatsLookup { itemName = ApplicationConstants.dashboardStatAnnualLeave, itemValue = (Convert.ToInt32(myPendingLeavesCount) + Convert.ToInt32(myStaffPendingLeavesCount)).ToString() };
            var otherLeaves = new DashBoardStatsLookup { itemName = ApplicationConstants.dashboardStatOtherLeave, itemValue = (Convert.ToInt32(myPendingOtherLeavesCount) + Convert.ToInt32(myStaffPendingOtherLeavesCount)).ToString() };


            var birthDayPersonal = new DashBoardStatsLookup { itemName = ApplicationConstants.dashboardStatBirthdayLeave, itemValue = (Convert.ToInt32(myBirthdayCount)+ Convert.ToInt32(myStaffBirthdayCount)).ToString() };
            var staffbirthDayPersonal = new DashBoardStatsLookup { itemName = ApplicationConstants.staffbirthDayPersonal, itemValue = (Convert.ToInt32(myPersonalDayCount) + Convert.ToInt32(myStaffPersonalCount)).ToString() };

            var ToilLeaves = new DashBoardStatsLookup { itemName = ApplicationConstants.dashboardStatToilLeave, itemValue = (Convert.ToInt32(myToilLeaves) + Convert.ToInt32(mySTaffToilLeaves)).ToString() };

            var data = (from stats in DbContext.E_LeaveStats_IOS(request.employeeId, null, true, fiscalYear) select stats).FirstOrDefault();
            double sicknessCount = 0.0;
            if (data != null)
            {
                sicknessCount = data.Sickness;
            }
            var sickness = new DashBoardStatsLookup { itemName = ApplicationConstants.dashboardStatSickness, itemValue = (sicknessCount).ToString() };


            response.ApprovalStats.Add(annualLeaves);
            response.ApprovalStats.Add(otherLeaves);
            response.ApprovalStats.Add(birthDayPersonal);
            response.ApprovalStats.Add(staffbirthDayPersonal);
            response.ApprovalStats.Add(ToilLeaves);
            response.ApprovalStats.Add(sickness);
            return response;
        }
        #endregion



        public List<VacanciesListResponse> GetVacanciesList()
        {

            List<VacanciesListResponse> response = new List<VacanciesListResponse>();
            var data = (from e in DbContext.I_JOBS_SELECT() select e).ToList();

            if (data.Count() > 0)
            {
                List<MyTeamEmployeeDetail> empList = new List<MyTeamEmployeeDetail>();

                foreach (var item in data)
                {
                    VacanciesListResponse emp = new VacanciesListResponse()
                    {
                        description = item.Description,
                        jobId = item.JobId,
                        jobType = item.JobType,
                        startDate = item.StartDate,
                        teamName = item.TeamName,
                        title = item.Title
                    };
                    response.Add(emp);
                }

            }
            return response;
        }
    }
}
