﻿using NetworkModel;
using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;
using System.Globalization;

namespace DataBaseModule.DataRepository
{
    public class AuthenticationRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public AuthenticationRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;

        }
        #region Authenticate a user
        public LoginResponse AuthenticateUser(LoginRequest request)
        {
            LoginResponse response = new LoginResponse();
            int userId = 0;
            if (CheckUserTeamAndRole(request.userName) == true)
            {
                var data = (from lg in DbContext.SP_NET_CHECKLOGIN(request.userName, request.password)
                            select lg).ToList();

                if (data.Count() > 0)
                {
                    userId = data.Select(x => x.LOGINID).FirstOrDefault();
                
                    response.userInfo.employeeFullName = data.Select(x => x.FIRSTNAME).FirstOrDefault() + " " + data.Select(x => x.LASTNAME).FirstOrDefault();
                    response.userInfo.isActive = data.Select(x => x.ACTIVE).FirstOrDefault();
                    response.userInfo.userId = data.Select(x => x.EMPLOYEEID).FirstOrDefault();
                    response.userInfo.userName = data.Select(x => x.LOGIN).FirstOrDefault();
                    response.userInfo.teamId = data.Select(x => x.TEAMID).FirstOrDefault();
                    response.userInfo.employmentNature = data.Select(x => x.HolidayIndicator).FirstOrDefault();

                    //get gender and AL-START-DATE of logged in user
                    response.userInfo.gender = DbContext.E__EMPLOYEE.Where(x => x.EMPLOYEEID == response.userInfo.userId).Select(x => x.GENDER).FirstOrDefault();
                    var details = (from e in DbContext.E__EMPLOYEE
                        join jdtl in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals jdtl.EMPLOYEEID
                        where e.EMPLOYEEID == response.userInfo.userId
                        select new
                        {
                            e.GENDER,
                            jdtl.ALSTARTDATE
                        }).FirstOrDefault();
                    response.userInfo.gender = details.GENDER;
                    response.userInfo.alStartDate = details.ALSTARTDATE;
                    // Getting job role

                    var jobRoleData = (from e in DbContext.E__EMPLOYEE
                                       join jrt in DbContext.E_JOBROLETEAM on new { JobRoleTeamId = (int)e.JobRoleTeamId } equals new { JobRoleTeamId = jrt.JobRoleTeamId }
                                       where
                                         e.EMPLOYEEID ==  response.userInfo.userId
                                       select new
                                       {
                                           e.EMPLOYEEID,
                                           jrt.E_JOBROLE.JobeRoleDescription,
                                           e.DOB
                                       }).FirstOrDefault();

                    if (jobRoleData != null)
                    {
                        response.userInfo.dob = jobRoleData.DOB;

                        string _dob = response.userInfo.dob.ToString();
                        if (_dob != null && _dob != "")
                        {
                            string finalDateTime = Convert.ToDateTime(_dob).ToString(ApplicationConstants.ApplicationDateForamt);
                            DateTime dt = DateTime.ParseExact(finalDateTime, ApplicationConstants.ApplicationDateForamt, CultureInfo.InvariantCulture);
                            response.userInfo.dob = dt;
                        }

                        

                        response.userInfo.jobRole = jobRoleData.JobeRoleDescription;
                    }
                    
                }

            }
            if (userId == 0)
            {
                SP_NET_SETLOGIN_THRESHOLD_Result loginThreshold = setLoginThreshold(request.userName);//set threshold limits

                if (loginThreshold != null)
                {

                    if (loginThreshold.Locked == true)//access lockdown msg
                    {

                        throw new ArgumentException(UserMessageConstants.AccessLockdownMsg);
                    }
                    else if (loginThreshold.Threshold == 4)//access lockdown warning msg
                    {
                        throw new ArgumentException(UserMessageConstants.AccessLockdownWarningMsg);
                    }
                    else//wrong UN and password msg
                    {
                        throw new ArgumentException(UserMessageConstants.LoginFailureMsg);
                    }

                }
                else //sussess msg
                {
                    throw new ArgumentException(UserMessageConstants.LoginScccessMsg);
                }
            }
            else
            {
                resetLoginThreshold(request.userName);//reset threshold limist in case of correct UN & password          
            }

           
            return response;
        }

        #endregion

        #region Reset login threshold           
        public void resetLoginThreshold(String username)
        {
            var userRecord = DbContext.AC_LOGINS.Where(usr => usr.LOGIN == username);

            if (userRecord.Count() > 0)
            {
                AC_LOGINS user = userRecord.First();
                user.THRESHOLD = 0;
                user.ISLOCKED = false;
                DbContext.SaveChanges();
            }
        }
        #endregion 

        #region Set Login Threshold      
        public SP_NET_SETLOGIN_THRESHOLD_Result setLoginThreshold(string userName)
        {

            var data = (from lst in DbContext.SP_NET_SETLOGIN_THRESHOLD(userName)
                        select lst).ToList();


            SP_NET_SETLOGIN_THRESHOLD_Result loginThreshold = null;
            if (data.Count() > 0)
            {
                loginThreshold = data.FirstOrDefault();
            }

            return loginThreshold;
        }

        #endregion

        #region Email Forgotten Password
        public EmailTemplateResponseModel getUserInfoByEmail(string emailAddress)
        {
            var data = (from info in DbContext.SP_GetUserInfoByEmail(emailAddress)
                        select new EmailTemplateResponseModel
                        {
                            login = info.LOGIN,
                            password = info.PASSWORD,
                            workEmail = info.WORKEMAIL
                        }
                               ).ToList();
            return data.FirstOrDefault();
        }
        #endregion

        #region Check whether the user have frontline team and Neighbourhood Officer role
        public bool CheckUserTeamAndRole(string username)
        {
            var data = (from emp in DbContext.E__EMPLOYEE
                        join login in DbContext.AC_LOGINS on emp.EMPLOYEEID equals login.EMPLOYEEID
                        join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                        join ejrt in DbContext.E_JOBROLETEAM on emp.JobRoleTeamId equals ejrt.JobRoleTeamId
                        join et in DbContext.E_TEAM on ejrt.TeamId equals et.TEAMID
                        //join ejrt in DbContext.E_JOBROLETEAM on et.TEAMID equals ejrt.TeamId
                        join ejr in DbContext.E_JOBROLE on ejrt.JobRoleId equals ejr.JobRoleId
                        where (jd.ACTIVE == 1)
                        // && et.TEAMNAME == ApplicationConstants.FrontlineUserType
                        && (et.ACTIVE == 1)
                        //&& ejr.JobeRoleDescription == ApplicationConstants.Neighbourhoodofficer
                        && login.LOGIN == username
                        orderby emp.FIRSTNAME, emp.LASTNAME
                        select emp
                        ).FirstOrDefault();
            if (data != null && data.EMPLOYEEID > 0)
            {
                return true;
            }
            return false;

        }

        #endregion

    }
}
