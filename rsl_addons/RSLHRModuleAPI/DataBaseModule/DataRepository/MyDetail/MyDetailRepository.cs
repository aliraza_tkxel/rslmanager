﻿using NetworkModel;
using System;
using System.Globalization;
using System.Linq;
using Utilities;

namespace DataBaseModule.DataRepository.MyDetail
{
    public class MyDetailRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public MyDetailRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }

        public UserDetail GetMydetail(int employeeId)
        {
            
            UserDetail result = (from e in DbContext.E__EMPLOYEE
                          join c in DbContext.E_CONTACT on e.EMPLOYEEID equals c.EMPLOYEEID
                          where e.EMPLOYEEID == employeeId
                          select new UserDetail()
                          {
                              personalDetail = new EmployeePersonalDetail()
                              {
                                  employeeId = e.EMPLOYEEID,
                                  firstName = e.FIRSTNAME,
                                  lastName = e.LASTNAME,
                                  _dob = e.DOB,
                                  gender = e.GENDER,
                                  maritalStatus = e.MARITALSTATUS,
                                  ethnicity = e.ETHNICITY,
                                  religion = e.RELIGION,
                                  sexualOrientation = e.SEXUALORIENTATION
                              },
                              contactDetail = new ContactDetail()
                              {
                                  addressLine1 = c.ADDRESS1,
                                  addressLine2 = c.ADDRESS2,
                                  addressLine3 = c.ADDRESS3,
                                  county = c.COUNTY,
                                  postalTown = c.POSTALTOWN,
                                  emergencyContactNumber = c.EMERGENCYCONTACTTEL,
                                  emergencyContactPerson = c.EMERGENCYCONTACTNAME,
                                  emergencyContactRelationship = c.EmergencyContactRelationship,
                                  homeEmail = c.HOMEEMAIL,
                                  homePhone = c.HOMETEL,
                                  mobilePersonal = c.MobilePersonal,
                                  mobile = c.MOBILE,
                                  workEmail = c.WORKEMAIL,
                                  workMobile = c.WORKMOBILE.TrimEnd(), 
                                  workPhone = c.WORKDD,
                                  postalCode = c.POSTCODE
                              }
                          }).FirstOrDefault();


            if (result.personalDetail._dob.HasValue)
            {
                result.personalDetail.dob = result.personalDetail._dob.Value.ToShortDateString();
            }
            return result;
        }

        public MyDetailResponce AmendUserDetail(UserDetail detail)
        {
            MyDetailResponce responce = new MyDetailResponce();

            var personalDetailResult = (from e in DbContext.E__EMPLOYEE
             where (e.EMPLOYEEID == detail.personalDetail.employeeId)
             select e).FirstOrDefault();

            personalDetailResult.FIRSTNAME = detail.personalDetail.firstName;
            personalDetailResult.LASTNAME = detail.personalDetail.lastName;
            personalDetailResult.DOB = GeneralHelper.GetDateTimeFromString(detail.personalDetail.dob);
            personalDetailResult.GENDER = detail.personalDetail.gender;
            personalDetailResult.ETHNICITY = detail.personalDetail.ethnicity;
            personalDetailResult.MARITALSTATUS = detail.personalDetail.maritalStatus;
            personalDetailResult.SEXUALORIENTATION = detail.personalDetail.sexualOrientation;
            personalDetailResult.RELIGION = detail.personalDetail.religion;
            DbContext.SaveChanges();

            var contactDetailResult = (from c in DbContext.E_CONTACT
                                       where c.EMPLOYEEID == detail.personalDetail.employeeId
                                       select c).FirstOrDefault();

            contactDetailResult.ADDRESS1 = detail.contactDetail.addressLine1;
            contactDetailResult.ADDRESS2 = detail.contactDetail.addressLine2;
            contactDetailResult.ADDRESS3 = detail.contactDetail.addressLine3;
            contactDetailResult.COUNTY = detail.contactDetail.county;
            contactDetailResult.POSTALTOWN = detail.contactDetail.postalTown;
            contactDetailResult.EMERGENCYCONTACTTEL = detail.contactDetail.emergencyContactNumber;
            contactDetailResult.EMERGENCYCONTACTNAME = detail.contactDetail.emergencyContactPerson;
            contactDetailResult.EmergencyContactRelationship = detail.contactDetail.emergencyContactRelationship;
            contactDetailResult.HOMEEMAIL = detail.contactDetail.homeEmail;
            contactDetailResult.HOMETEL = detail.contactDetail.homePhone;
            contactDetailResult.MOBILE = detail.contactDetail.mobile;
            contactDetailResult.MobilePersonal = detail.contactDetail.mobilePersonal;
            contactDetailResult.WORKEMAIL = detail.contactDetail.workEmail;
            contactDetailResult.WORKMOBILE = detail.contactDetail.workMobile;
            contactDetailResult.WORKDD = detail.contactDetail.workPhone;
            contactDetailResult.POSTCODE = detail.contactDetail.postalCode;

            DbContext.SaveChanges();
            
            responce.isSuccessFul = true;
            responce.message = UserMessageConstants.UserDetailUpdatedSuccessMessage;
            return responce;
        }

        public AnnualLeaveDetail GetAnnualLeaveDetail(AbsenceRequest request)
        {
            DateTime? fiscalYear = null;
            DateTime? EndDate = null;
            if (!string.IsNullOrEmpty(request.fiscalYearDate))
            {
                fiscalYear = GeneralHelper.GetDateTimeFromString(request.fiscalYearDate);
                EndDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE_ByYear(request.employeeId, fiscalYear)
                               select new AbsenceAnnual()
                               {
                                   endDate = absence.ENDDATE
                               }).FirstOrDefault().endDate;
            }
            else
            {
                EndDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(request.employeeId)
                           select new AbsenceAnnual()
                           {
                               endDate = absence.ENDDATE
                           }).FirstOrDefault().endDate;
            }

            var alstartDate = (from d in DbContext.E_JOBDETAILS
                               where d.EMPLOYEEID == request.employeeId
                               select d).FirstOrDefault().ALSTARTDATE;
            var data = (from stats in DbContext.E_LeaveStats_IOS(request.employeeId, null, true, fiscalYear) select stats).FirstOrDefault();

            var holidayEntitlementDays = (from e in DbContext.E_JOBDETAILS
                                          where e.EMPLOYEEID == request.employeeId
                                          select e).FirstOrDefault().HOLIDAYENTITLEMENTDAYS;
            if (holidayEntitlementDays == null)
                holidayEntitlementDays = (from e in DbContext.E_JOBDETAILS
                                          where e.EMPLOYEEID == request.employeeId
                                          select e).FirstOrDefault().HOLIDAYENTITLEMENTHOURS / 8;
            var holidayEntitlementhours = (from e in DbContext.E_JOBDETAILS
                                           where e.EMPLOYEEID == request.employeeId
                                           select e).FirstOrDefault().HOLIDAYENTITLEMENTHOURS;

            var annualLeaveData = (from absence in DbContext.E_AbsenseAnnual(request.employeeId, fiscalYear)
                                   select new AbsenceAnnual()
                                   {
                                       absenceHistoryId = absence.ABSENCEHISTORYID,
                                       endDate = absence.RETURNDATE,
                                       status = absence.Status,
                                       absentDays = (double)absence.DAYS_ABS,
                                       nature = absence.Nature,
                                       startDate = absence.STARTDATE,
                                       holidayType = absence.HolType,
                                       lastActionDatetime = absence.LastActionDate,
                                       unit = data.Unit,
                                       durationType = absence.DurationType

                                   }).ToList();
            double LeavesRequested = 0;
            double LeavesTaken = 0;
            double LeavesBooked = 0;
            double LeavesAvailable = 0;
            

            foreach (var al in annualLeaveData)
            {
                if (al.endDate <= EndDate)
                {
                    if (al.status.Equals("Approved") && al.startDate > DateTime.Now.Date)
                    {
                        LeavesBooked = LeavesBooked + double.Parse(al.absentDays.ToString());
                    }
                    if (al.status.Equals("Approved") && al.startDate <= DateTime.Now.Date)
                    {
                        LeavesTaken = LeavesTaken + double.Parse(al.absentDays.ToString());
                    }
                    if (al.status.Equals("Pending"))
                    {
                        LeavesRequested = LeavesRequested + double.Parse(al.absentDays.ToString());
                    }
                }
                else if (al.endDate >= EndDate && al.startDate <= EndDate)
                {
                    if (al.status.Equals("Approved") && al.startDate > DateTime.Now.Date)
                    {
                        double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                        LeavesBooked = LeavesBooked + LeaveDaysCount;
                    }
                    if (al.status.Equals("Approved") && al.startDate <= DateTime.Now.Date)
                    {
                        double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                        LeavesTaken = LeavesTaken + LeaveDaysCount;
                    }
                    if (al.status.Equals("Pending"))
                    {
                        double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                        LeavesRequested = LeavesRequested - LeaveDaysCount;
                    }
                }
            }

            var query = DbContext.E_GetBankHoliday(request.employeeId, fiscalYear).ToList();
            data.LEAVE_AVAILABLE = data.ANNUAL_LEAVE_DAYS - LeavesRequested;

            LeavesAvailable = data.ANNUAL_LEAVE_DAYS - LeavesRequested-LeavesBooked-LeavesTaken;

            foreach (var item in query)
            {
                LeavesBooked = LeavesBooked + double.Parse(item.DAYS_ABS.ToString()); 
            }
            data.LEAVE_AVAILABLE = LeavesAvailable;
            data.LeaveBooked = LeavesBooked;
            data.LeaveTaken = LeavesTaken;
            data.LeaveRequested = LeavesRequested;
            if (data != null)
            {
                var response=new AnnualLeaveDetail()
                {   
                    unit = data.Unit,                 
                    startDate = alstartDate,
                    leavesBooked =  Convert.ToDecimal(data.LeaveBooked),
                    leavesRequested = Convert.ToDecimal(data.LeaveRequested),
                    leavesTaken = Convert.ToDecimal(data.LeaveTaken),
                    leavesRemaining = Convert.ToDecimal(data.LEAVE_AVAILABLE + data.CARRY_FWD),
                    totalAllowance = Convert.ToDecimal(data.ANNUAL_LEAVE_DAYS + data.CARRY_FWD + data.BNK_HD),
                    bankHoliday = Convert.ToDecimal(data.BNK_HD),
                    carryForward = Convert.ToDecimal(data.CARRY_FWD),
                    timeOfInLieuOwed = Convert.ToDecimal(data.TIME_OFF_IN_LIEU_OWED),
                    holidayEntitlementDays = holidayEntitlementDays == null ? 0 : holidayEntitlementDays,
                    holidayEntitlementHours = holidayEntitlementhours,
                    sickness = data.Sickness
                };
                var totalBankHolidays = (from e in DbContext.E_BOOKED_BANK_HOLIDAY(request.employeeId, fiscalYear)
                                         select e.Value).FirstOrDefault();
                totalBankHolidays = totalBankHolidays - response.bankHoliday;
                response.leavesRemaining = response.leavesRemaining - totalBankHolidays;
                return response;                
            }
            return new AnnualLeaveDetail();
        }
    }
}
