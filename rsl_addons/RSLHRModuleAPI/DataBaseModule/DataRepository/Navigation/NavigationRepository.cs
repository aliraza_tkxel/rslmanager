﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule
{
    public class NavigationRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public NavigationRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;

        }

        #region Displaying Menu on top bar
        public NavigationResponseModel GetMenus(NavigationRequestModel objMenuRequestBO)
        {
            //Althought a store procedure exist which did same,but we need to skip customer from menu as per requirement document. so for that purpose, this function is created
            NavigationResponseModel objMenusResponseBO = new NavigationResponseModel();
            //start getting moduleId from module name
            int moduleId = int.Parse((from module in DbContext.AC_MODULES                                     
                                      // join moduleAccess in DbContext.AC_MODULES_ACCESS  on module.MODULEID equals moduleAccess.ModuleId
                                      where module.DESCRIPTION == objMenuRequestBO.moduleName
                                      select module.MODULEID).FirstOrDefault().ToString());
            //end getting moduleId from module name


            //start filling menus list
            var menus = (from emp in DbContext.E__EMPLOYEE
                         join access in DbContext.AC_MENUS_ACCESS on emp.JobRoleTeamId equals access.JobRoleTeamId
                         join menu in DbContext.AC_MENUS on access.MenuId equals menu.MENUID
                         join module in DbContext.AC_MODULES on menu.MODULEID equals module.MODULEID
                         where menu.ACTIVE == 1
                         && emp.EMPLOYEEID == objMenuRequestBO.userId
                         && module.MODULEID == moduleId
                         select new HRMenuesDataModel
                         {
                             menuId = menu.MENUID,
                             menuName = menu.DESCRIPTION,
                             path = menu.PAGE

                         }
                        );
            //end filling menus list

            if (menus.Count() > 0)
            {
                //start filling sub menus list
                var MenusAndSubMenuList = menus.ToList();
                foreach (var item in MenusAndSubMenuList)
                {
                    item.subMenus = GetSubMenus(item.menuId, objMenuRequestBO.userId);
                }
                //end filling sub menus list

                //start creating response, cosisting of menu,subMenus and Customer module menus
              
                objMenusResponseBO.menues = MenusAndSubMenuList.ToList();

            }
            objMenusResponseBO.hrModuleMenus = GetSideMenus(objMenuRequestBO.userId);//get customer module menus
            return objMenusResponseBO;
        }
        #endregion

        #region Get Sub Menus on top bar
        public List<HRModuleSubMenuDataModel> GetSubMenus(int menuId, int userId)
        {
            var data = (from emp in DbContext.E__EMPLOYEE
                        join access in DbContext.AC_PAGES_ACCESS on emp.JobRoleTeamId equals access.JobRoleTeamId
                        join page in DbContext.AC_PAGES on access.PageId equals page.PAGEID
                        join menu in DbContext.AC_MENUS on page.MENUID equals menu.MENUID
                        join module in DbContext.AC_MODULES on menu.MODULEID equals module.MODULEID
                        where page.ACTIVE == 1
                        && page.LINK == 1
                        && emp.EMPLOYEEID == userId
                        && menu.MENUID == menuId
                        select new HRModuleSubMenuDataModel
                        {
                            subMenuName = page.DESCRIPTION,
                            menuId = menu.MENUID,
                            path = string.Concat("/", module.DIRECTORY, "/", page.PAGE)
                        }
                        );
            var sortedList = data.OrderBy(x => x.subMenuName).ToList();
            return sortedList;
            //return data.ToList();
        }
        #endregion

        #region Get Customer module Menus
        public List<HRModuleDataModel> GetSideMenus(int userId)
        {
            var data = (from sideMenus in DbContext.P_GetRSLModulesList(userId, 1)
                        select new HRModuleDataModel
                        {
                            moduleId = sideMenus.MODULEID,
                            moduleName = sideMenus.DESCRIPTION,
                            url = sideMenus.THEPATH
                        }
                );
            return data.ToList();
        }
        #endregion
    }
}
