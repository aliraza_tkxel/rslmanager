﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Utilities;

namespace DataBaseModule.DataRepository.Admin.Teams
{
    public class AccessControlRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public AccessControlRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }
        public AccessControlResponse GetEmployee(int employeeId)
        {
            AccessControlResponse response = new AccessControlResponse();
            var employee = (from E in DbContext.E__EMPLOYEE
                            join L in DbContext.AC_LOGINS on E.EMPLOYEEID equals L.EMPLOYEEID into L_join
                            from L in L_join.DefaultIfEmpty()
                            join C in DbContext.E_CONTACT on E.EMPLOYEEID equals C.EMPLOYEEID into C_join
                            from C in C_join.DefaultIfEmpty()
                            where E.EMPLOYEEID == employeeId
                            select new AccessControl
                            {
                                firstName = E.FIRSTNAME,
                                lastName = E.LASTNAME,
                                userName=L.LOGIN,
                                loginId=L.LOGINID,
                                employeeId=E.EMPLOYEEID,
                                EXPIRES=L.EXPIRES,
                                isActive=L.ACTIVE,
                                isLocked=L.ISLOCKED
                            }).FirstOrDefault();
            response.confirmation = "";
            response.password = "";
            response.lastName = employee.lastName;
            response.firstName = employee.firstName;
            response.isActive = employee.isActive;
            response.isLocked = employee.isLocked;
            if(employee.loginId==null)
            {
                response.isActive = 1;
                response.isLocked = false;
            }
            response.loginId = employee.loginId;
            response.userName = employee.userName;
            response.employeeId = employee.employeeId;
            response.EXPIRES = employee.EXPIRES;
            return response;
        }
        public string UpdateLogin(AccessControl request)
        {
            string returnMsg = "";
            var login = (from L in DbContext.AC_LOGINS
                         where L.EMPLOYEEID==request.employeeId
                         select L).FirstOrDefault();
            if (login==null)
            {
                login = new AC_LOGINS();
                DbContext.AC_LOGINS.Add(login);
            }
            var loginList = (from L in DbContext.AC_LOGINS
                             where L.LOGIN == request.userName
                             select L);
            int count = loginList.Where(m => m.LOGIN == request.userName && m.EMPLOYEEID != request.employeeId).Count();
            if(count==0)
            {
                login.EMPLOYEEID = request.employeeId;
                if(request.loginId==null)
                {
                    login.DATECREATED = DateTime.Now;
                    login.CREATEDBY = request.currentUserId;
                }
                login.DATEMODIFIED = DateTime.Now;
                login.DATEUPDATED = DateTime.Now;
                login.EXPIRES = DateTime.Now.AddMonths(3);
                login.MODIFIEDBY = request.currentUserId;
                login.LOGIN = request.userName;
                login.ISLOCKED = false;
                login.THRESHOLD = 0;
                login.PASSWORD = request.password;
                login.ACTIVE = request.isActive;
                DbContext.SaveChanges();
                returnMsg = UserMessageConstants.LoginUpdateSuccessMessage;
            }
            else
            {
                returnMsg = "A record with the username " + request.userName + " is already in use. Please try another.";
            }
            return returnMsg;
        }
        public void UnlockAccount(int employeeId)
        {
            var login = (from L in DbContext.AC_LOGINS
                         where L.EMPLOYEEID == employeeId
                         select L).FirstOrDefault();
            login.ISLOCKED = false;
            login.THRESHOLD = 0;
            DbContext.SaveChanges();
        }
    }




}
