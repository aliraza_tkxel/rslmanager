﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Utilities;

namespace DataBaseModule.DataRepository.Admin.Teams
{
    public class TeamsRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public TeamsRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }


        private IQueryable<E_TEAM> nameExist(string name)
        {
            var teamData = (from e in DbContext.E_TEAM
                            where e.TEAMNAME == name
                            select e);

            return teamData;
        }

        public TeamsResponse GetTeams(TeamsRequest model)
        {
            TeamsResponse response = new TeamsResponse();
            var data = (from T in DbContext.E_TEAM
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        join E1 in DbContext.E__EMPLOYEE on T.DIRECTOR equals E1.EMPLOYEEID into E1_join
                        from E1 in E1_join.DefaultIfEmpty()
                        join E2 in DbContext.E__EMPLOYEE on T.MANAGER equals E2.EMPLOYEEID into E2_join
                        from E2 in E2_join.DefaultIfEmpty()
                        where T.ACTIVE == model.status
                        orderby T.TEAMNAME
                        select new Team
                        {
                            teamName = (T.TEAMNAME),
                            teamId = (T.TEAMID),
                            directorate = D.DIRECTORATENAME == null ? "N/A" : D.DIRECTORATENAME,
                            mainFunction = T.MAINFUNCTION.Length < 40 ? T.MAINFUNCTION :
                                           T.MAINFUNCTION.Substring(1, 40) + "..",
                            description = T.DESCRIPTION == null || T.DESCRIPTION.Equals("") ? "N/A" :
                                          T.DESCRIPTION.Length < 40 ? T.DESCRIPTION :
                                          T.DESCRIPTION.Substring(1, 40) + "..",
                            teamDirector = E1.FIRSTNAME == null && E1.LASTNAME == null ? "N/A" :
                                          E1.FIRSTNAME + " " + E1.LASTNAME,
                            teamManager = E2.FIRSTNAME == null && E2.LASTNAME == null ? "N/A" :
                                          E2.FIRSTNAME + " " + E2.LASTNAME
                        });

            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"teamName";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            // Making final response
            List<Team> teams = new List<Team>();
            foreach (var item in finalData)
            {
                Team _team = new Team();
                _team.teamId = item.teamId;
                _team.directorateId = item.directorateId;
                _team.teamName = item.teamName;
                _team.teamManager = item.teamManager;
                _team.teamDirector = item.teamDirector;
                _team.mainFunction = item.mainFunction;
                _team.description = item.description;
                _team.directorate = item.directorate;
                _team.directorId = item.directorId;
                _team.managerId = item.managerId;
                _team.active = item.active;
                teams.Add(_team);
            }
            response.teams = teams;

            return response;
        }
        public TeamsResponse GetTeam(int teamId)
        {
            TeamsResponse response = new TeamsResponse();
            var team = (from T in DbContext.E_TEAM
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        join E1 in DbContext.E__EMPLOYEE on T.DIRECTOR equals E1.EMPLOYEEID into E1_join
                        from E1 in E1_join.DefaultIfEmpty()
                        join E2 in DbContext.E__EMPLOYEE on T.MANAGER equals E2.EMPLOYEEID into E2_join
                        from E2 in E2_join.DefaultIfEmpty()
                        where T.TEAMID == teamId
                        orderby T.TEAMNAME
                        select new Team
                        {
                            teamName = (T.TEAMNAME),
                            teamId = (T.TEAMID),
                            directorate = D.DIRECTORATENAME == null ? "N/A" : D.DIRECTORATENAME,
                            directorateId = D.DIRECTORATENAME == null ? 0 : D.DIRECTORATEID,
                            active = T.ACTIVE,
                            mainFunction = T.MAINFUNCTION,
                            description = T.DESCRIPTION,
                            teamDirector = E1.FIRSTNAME == null && E1.LASTNAME == null ? "N/A" :
                                          E1.FIRSTNAME + " " + E1.LASTNAME,
                            teamManager = E2.FIRSTNAME == null && E2.LASTNAME == null ? "N/A" :
                                          E2.FIRSTNAME + " " + E2.LASTNAME,
                            directorId = E1.FIRSTNAME == null && E1.LASTNAME == null ? 0 :
                                          E1.EMPLOYEEID,
                            managerId = E2.FIRSTNAME == null && E2.LASTNAME == null ? 0 :
                                          E2.EMPLOYEEID
                        });
            var finalData = team.ToList();
            List<Team> teams = new List<Team>();
            foreach (var item in finalData)
            {
                Team _team = new Team();
                _team.teamId = item.teamId;
                _team.directorateId = item.directorateId;
                _team.teamName = item.teamName;
                _team.teamManager = item.teamManager;
                _team.teamDirector = item.teamDirector;
                _team.mainFunction = item.mainFunction;
                _team.description = item.description;
                _team.directorate = item.directorate;
                _team.directorId = item.directorId;
                _team.managerId = item.managerId;
                _team.active = item.active;
                teams.Add(_team);
            }
            response.teams = teams;
            response.pagination = null;
            return response;
        }

        public List<LookUpResponseModel> GetManagers()
        {
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                        where J.ACTIVE == 1 && (J.ISMANAGER == 1)
                        select new LookUpResponseModel()
                        {
                            lookUpId = E.EMPLOYEEID,
                            lookUpDescription = E.FIRSTNAME + " " + E.LASTNAME
                        }).ToList();



            return data;
        }
        public List<LookUpResponseModel> GetDirectorates()
        {
            var directorates = (from D in DbContext.E_DIRECTORATES
                                select new LookUpResponseModel()
                                {
                                    lookUpId = D.DIRECTORATEID,
                                    lookUpDescription = D.DIRECTORATENAME
                                }).ToList();
            return directorates;
        }
        public List<LookUpResponseModel> GetDirectors()
        {
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                        where J.ACTIVE == 1 && (J.ISDIRECTOR == 1)
                        select new LookUpResponseModel()
                        {
                            lookUpId = E.EMPLOYEEID,
                            lookUpDescription = E.FIRSTNAME + " " + E.LASTNAME
                        }).ToList();



            return data;
        }

        public string SaveTeam(Team detail)
        {
            string returnMsg = "";
            E_TEAM team = new E_TEAM();
            var teamData = (from e in DbContext.E_TEAM
                            where e.TEAMID == detail.teamId
                            select e);
            if (teamData.Count() > 0)
            {
                team = teamData.FirstOrDefault();
            }

            var teamExist = (from e in DbContext.E_TEAM
                            where e.TEAMNAME == detail.teamName
                             select e);

            if (detail.teamId == 0 && !teamExist.Any())
            {
                // add new team
                team.CREATIONDATE = DateTime.Now;
                team.TEAMNAME = detail.teamName;
                team.DIRECTORATEID = detail.directorateId;
                team.DIRECTOR = detail.directorId;
                team.MANAGER = detail.managerId;
                team.MAINFUNCTION = detail.mainFunction;
                team.ACTIVE = detail.active;
                team.DESCRIPTION = detail.description;
                DbContext.E_TEAM.Add(team);

                DbContext.SaveChanges();
                detail.teamId = team.TEAMID;
                returnMsg = UserMessageConstants.TeamSaveSuccessMessage;
            }
            else if (team.TEAMID == detail.teamId && teamExist.Where(e => e.TEAMID == team.TEAMID).Any())
            {
                team.CREATIONDATE = DateTime.Now;
                team.TEAMNAME = detail.teamName;
                team.DIRECTORATEID = detail.directorateId;
                team.DIRECTOR = detail.directorId;
                team.MANAGER = detail.managerId;
                team.MAINFUNCTION = detail.mainFunction;
                team.ACTIVE = detail.active;
                team.DESCRIPTION = detail.description;

                DbContext.SaveChanges();
                detail.teamId = team.TEAMID;
                returnMsg = UserMessageConstants.TeamSaveSuccessMessage;
            }
            else if (!teamExist.Any())
            {
                team.CREATIONDATE = DateTime.Now;
                team.TEAMNAME = detail.teamName;
                team.DIRECTORATEID = detail.directorateId;
                team.DIRECTOR = detail.directorId;
                team.MANAGER = detail.managerId;
                team.MAINFUNCTION = detail.mainFunction;
                team.ACTIVE = detail.active;
                team.DESCRIPTION = detail.description;


                DbContext.SaveChanges();
                detail.teamId = team.TEAMID;
                returnMsg = UserMessageConstants.TeamSaveSuccessMessage;
            }
            else
            {
                returnMsg = "A Team with the name "+ detail.teamName + " already exists.";
            }
            return returnMsg;
        }
    }




}
