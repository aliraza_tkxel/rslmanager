﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Shared
{
    public class TitleRepository : BaseRepository<G_TITLE>
    {
        public TitleRepository(Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }


    }
}