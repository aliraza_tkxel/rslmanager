﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseModule.DatabaseEntities;

namespace DataBaseModule.DataRepository.Shared
{
    public class PaypointStatusRepository : BaseRepository<E_PayPointStatus>
    {
        // public DatabaseEntities.Entities DbContext { get; set; }

        public PaypointStatusRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }


    }
}