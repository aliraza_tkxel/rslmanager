﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseModule.DatabaseEntities;

namespace DataBaseModule.DataRepository.Shared
{
    public class DeclerationOfInterestStatusRepository : BaseRepository<E_DOI_Status>
    {
        public DeclerationOfInterestStatusRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    }
}
