﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Shared
{
    public class FIscalYearRepository :BaseRepository<DatabaseEntities.F_FISCALYEARS>
    {
        // public DatabaseEntities.Entities DbContext { get; set; }

        public FIscalYearRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }

        public DatabaseEntities.F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }

    }
}