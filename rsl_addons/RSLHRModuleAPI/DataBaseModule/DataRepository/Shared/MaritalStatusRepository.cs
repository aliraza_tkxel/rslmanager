﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Shared
{
    public class MaritalStatusRepository : BaseRepository<G_MARITALSTATUS>
    {
        public MaritalStatusRepository(Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    }
}
