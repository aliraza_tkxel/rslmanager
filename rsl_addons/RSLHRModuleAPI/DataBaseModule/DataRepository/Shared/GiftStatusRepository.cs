﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseModule.DatabaseEntities;

namespace DataBaseModule.DataRepository.Shared
{
    public class GiftStatusRepository : BaseRepository<E_Gift_Status>
    {
        public GiftStatusRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    }
}
