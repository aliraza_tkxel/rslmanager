﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBaseModule.DatabaseEntities;

namespace DataBaseModule.DataRepository.Shared
{
    public class AbsenceReasonRepository : BaseRepository<E_ABSENCEREASON>
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public AbsenceReasonRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }

    }
}
