﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Shared
{
    public class AbsenceActionRepository : BaseRepository<E_ACTION>
    {
        // public DatabaseEntities.Entities DbContext { get; set; }

        public AbsenceActionRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    
    }
}
