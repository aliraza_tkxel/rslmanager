﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Shared
{
    public class AbsenceStatusRepository: BaseRepository<E_STATUS>
    {
        public AbsenceStatusRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }

        public List<E_STATUS> includeItems()
        {
            return DbContext.E_STATUS.Include("E_ITEM").ToList();
        }
    }
}
