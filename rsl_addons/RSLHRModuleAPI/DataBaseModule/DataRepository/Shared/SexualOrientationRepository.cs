﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Shared
{
    public class SexualOrientationRepository : BaseRepository<G_SEXUALORIENTATION>
    {
     
        public SexualOrientationRepository(Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }
    }
}
