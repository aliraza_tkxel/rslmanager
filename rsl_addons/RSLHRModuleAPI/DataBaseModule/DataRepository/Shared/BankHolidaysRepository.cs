﻿using DataBaseModule.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository.Shared
{
    public class BankHolidaysRepository : BaseRepository<G_BANKHOLIDAYS>
    {
        

        public BankHolidaysRepository(DatabaseEntities.Entities dbContext) : base(dbContext)
        {
            this.DbContext = dbContext;
        }

        public bool IsBankHolidayExist(DateTime runningDate)
        {
            bool result = false;
            var data = (from p in DbContext.G_BANKHOLIDAYS
                        where p.BHDATE == runningDate
                        select p);
            if (data.Count() > 0)
                result = true;
            return result;

        }
    }
}
