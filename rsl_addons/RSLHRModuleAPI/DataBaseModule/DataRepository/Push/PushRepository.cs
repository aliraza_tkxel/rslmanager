﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Utilities;
using PushSharp.Core;
using PushSharp.Apple;
using PushSharp;
using Newtonsoft.Json.Linq;
using System.Reflection;

namespace DataBaseModule.DataRepository.Push
{
    public class PushRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }
        protected readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public PushRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
        }
        #region send Push Notification for Fault Appointment
        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>


        public bool sendPushNotificationFaultNoteUpdates(int appointmentId, string serverinUse)
        {
            try
            {

                bool isNotificationSent = false;

                var appDataList = (from appointment in DbContext.FL_CO_APPOINTMENT
                                   join login in DbContext.AC_LOGINS on appointment.OperativeID equals login.EMPLOYEEID
                                   where appointment.AppointmentID == appointmentId
                                   select new { appointment.AppointmentDate, appointment.Time, appointment.EndTime, login.DEVICETOKEN });

                if (appDataList.Count() > 0)
                {
                    var appData = appDataList.First();

                    string deviceToken = appData.DEVICETOKEN;

                    if (deviceToken != null)
                    {
                        if (deviceToken == "0" || deviceToken == "")
                        {
                            return false;
                        }



                        var propDataList = (from flt in DbContext.FL_CO_APPOINTMENT
                                            join flta in DbContext.FL_FAULT_APPOINTMENT on flt.AppointmentID equals flta.AppointmentId
                                            where flta.AppointmentId == appointmentId && flt.AppointmentStatus != "Complete"
                                            select new { flt.AppointmentID, flt.AppointmentDate });


                        if (propDataList.Count() > 0)
                        {
                            string propertyString = string.Empty;



                            string PNString = "Some modifications have beeen made to your assigned appointments. Please refresh your appointments to update the data before proceeding.";

                            string serverPath = HttpContext.Current.Server.MapPath("");



                            serverPath = serverPath.Remove(serverPath.LastIndexOf('\\') + 1) + "RSLHRModuleApi\\";

                            //This is the dev certificate file name with path.
                            string filePath = serverPath + "APNS_DEV_PSAOfflineCertOnly.p12";

                            //In case the server in use is Test or Live or UAT change the file name accordingly
                            if (serverinUse.Equals(ApplicationConstants.BHG_Test_Name))
                                filePath = serverPath + "APNS_TEST_PSAOfflineCertOnly.p12";
                            else if (serverinUse.Equals(ApplicationConstants.BHG_Live_Name))
                                filePath = serverPath + "APNS_LIVE_PSAOfflineCertOnly.p12";
                            else if (serverinUse.Equals(ApplicationConstants.BHG_UAT_Name))
                                filePath = serverPath + "APNS_UAT_PSAOfflineCertOnly.p12";

                            log.Info("File Path: " + filePath);

                            var config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, filePath, "tkxel");
                            var apnsBroker = new ApnsServiceBroker(config);
                            // Wire up events
                            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                            {

                                aggregateEx.Handle(ex =>
                                {

                                    // See what kind of exception it was to further diagnose
                                    if (ex is ApnsNotificationException)
                                    {
                                        var notificationException = (ApnsNotificationException)ex;
                                        log.Info("Exception: " + ex.ToString());
                                        // Deal with the failed notification
                                        var apnsNotification = notificationException.Notification;
                                        var statusCode = notificationException.ErrorStatusCode;
                                        log.Info("StackTrace: " + ex.StackTrace.ToString());
                                        throw ex;


                                    }
                                    else
                                    {
                                        log.Info("StackTrace: " + ex.StackTrace.ToString());
                                        throw ex;
                                        // Inner exception might hold more useful information like an ApnsConnectionException           

                                    }

                                    // Mark it as handled
                                    return true;
                                });
                            };

                            apnsBroker.OnNotificationSucceeded += (notification) =>
                            {
                                isNotificationSent = true;
                            };

                            // Start the broker
                            apnsBroker.Start();


                            // Queue a notification to send
                            apnsBroker.QueueNotification(new ApnsNotification
                            {
                                DeviceToken = deviceToken,
                                Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + PNString + "\",\"badge\":\"1\"}}")
                            });

                            // Stop the broker, wait for it to finish   
                            // This isn't done after every message, but after you're
                            // done with the broker
                            apnsBroker.Stop();
                            // PushSharp.Core.IServiceBroker()


                        }

                    }
                }
                return isNotificationSent;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion
        /// <summary>
        /// Ticket# 572 - sendPushNotificationVoidInspections 
        /// </summary>
        /// <param name="operativeId"></param>
        /// <param name="serverinUse"></param>
        /// <param name="propertyAddress"></param>
        /// <returns></returns>
        public bool sendPushNotificationVoidInspections(int operativeId, string serverinUse, string propertyAddress)
        {
            try
            {

                bool isNotificationSent = false;

                var appDataList = (from login in DbContext.AC_LOGINS
                                   where login.EMPLOYEEID == operativeId
                                   select new { login.DEVICETOKEN });

                if (appDataList.Count() > 0)
                {
                    var appData = appDataList.First();

                    string deviceToken = appData.DEVICETOKEN;

                    if (deviceToken != null)
                    {
                        if (deviceToken == "0" || deviceToken == "")
                        {
                            return false;
                        }



                        //var propDataList = (from flt in DbContext.FL_CO_APPOINTMENT
                        //                    join flta in DbContext.FL_FAULT_APPOINTMENT on flt.AppointmentID equals flta.AppointmentId
                        //                    where flta.AppointmentId == appointmentId && flt.AppointmentStatus != "Complete"
                        //                    select new { flt.AppointmentID, flt.AppointmentDate });


                        //if (propDataList.Count() > 0)
                        //{
                        string propertyString = string.Empty;



                        string PNString = "There has been a change to the appointment for "+propertyAddress+". please refresh your app";

                        string serverPath = HttpContext.Current.Server.MapPath("");



                        serverPath = serverPath.Remove(serverPath.LastIndexOf('\\') + 1) + "RSLHRModuleApi\\";

                        //This is the dev certificate file name with path.
                        string filePath = serverPath + "APNS_DEV_PSAOfflineCertOnly.p12";

                        //In case the server in use is Test or Live or UAT change the file name accordingly
                        if (serverinUse.Equals(ApplicationConstants.BHG_Test_Name))
                            filePath = serverPath + "APNS_TEST_PSAOfflineCertOnly.p12";
                        else if (serverinUse.Equals(ApplicationConstants.BHG_Live_Name))
                            filePath = serverPath + "APNS_LIVE_PSAOfflineCertOnly.p12";
                        else if (serverinUse.Equals(ApplicationConstants.BHG_UAT_Name))
                            filePath = serverPath + "APNS_UAT_PSAOfflineCertOnly.p12";

                        log.Info("File Path: " + filePath);

                        var config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, filePath, "tkxel");
                        var apnsBroker = new ApnsServiceBroker(config);
                        // Wire up events
                        apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                        {

                            aggregateEx.Handle(ex =>
                            {

                                    // See what kind of exception it was to further diagnose
                                    if (ex is ApnsNotificationException)
                                {
                                    var notificationException = (ApnsNotificationException)ex;
                                    log.Info("Exception: " + ex.ToString());
                                        // Deal with the failed notification
                                        var apnsNotification = notificationException.Notification;
                                    var statusCode = notificationException.ErrorStatusCode;
                                    log.Info("StackTrace: " + ex.StackTrace.ToString());
                                    throw ex;


                                }
                                else
                                {
                                    log.Info("StackTrace: " + ex.StackTrace.ToString());
                                    throw ex;
                                        // Inner exception might hold more useful information like an ApnsConnectionException           

                                    }

                                    // Mark it as handled
                                    return true;
                            });
                        };

                        apnsBroker.OnNotificationSucceeded += (notification) =>
                        {
                            isNotificationSent = true;
                        };

                        // Start the broker
                        apnsBroker.Start();


                        // Queue a notification to send
                        apnsBroker.QueueNotification(new ApnsNotification
                        {
                            DeviceToken = deviceToken,
                            Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + PNString + "\",\"badge\":\"1\"}}")
                        });

                        // Stop the broker, wait for it to finish   
                        // This isn't done after every message, but after you're
                        // done with the broker
                        apnsBroker.Stop();
                        // PushSharp.Core.IServiceBroker()


                        //}

                    }
                }
                return isNotificationSent;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}

