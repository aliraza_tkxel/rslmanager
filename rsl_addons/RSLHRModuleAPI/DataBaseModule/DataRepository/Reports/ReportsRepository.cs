﻿using DataBaseModule.DatabaseEntities;
using NetworkModel;
using NetwrokModel.Reports;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Utilities;


namespace DataBaseModule.DataRepository
{
    public class ReportsRepository
    {
        public DatabaseEntities.Entities DbContext { get; set; }

        public ReportsRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;

        }

        #region Helpers

        private F_FISCALYEARS getCurrentFiscalYear()
        {
            var fYear = DbContext.F_FISCALYEARS.Where(e => e.YStart <= DateTime.Now && DateTime.Now <= e.YEnd).FirstOrDefault();

            return fYear;
        }

        #endregion 


        #region Establishment Report
        public EstablishmentList PopulateEstablishmentReport(CommonReportsFilterRequest model)
        {
            EstablishmentList response = new EstablishmentList();

            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                        from ETH in ETH_join.DefaultIfEmpty()
                        where
                          // J.ACTIVE == model.status &&
                          (T.DIRECTORATEID == model.directorate || model.directorate == null) &&
                          (ET.JobeRoleDescription != "Board Member") &&
                         (J.TEAM == model.team || model.team == null) &&
                          ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText)) &&
                          ((J.PARTFULLTIME == model.fullTime || model.fullTime == 0) && J.PARTFULLTIME != null)
                        select new EstablishmentDetail
                        {
                            fullName = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            directorate = (D.DIRECTORATENAME ?? "No Directorate"),
                            employeeId = E.EMPLOYEEID,
                            jobTitle = (ET.JobeRoleDescription ?? "N/A"),
                            grade = (G.DESCRIPTION ?? "-"),
                            dobString = E.DOB,
                            endDateString = J.ENDDATE,
                            ethnicity = (ETH.DESCRIPTION ?? "-"),
                            gender = E.GENDER,
                            salary = J.SALARY,
                            startDateString = J.STARTDATE,
                            isDisciplinary = false,
                            status = J.ACTIVE,
                            address = E.E_CONTACT.FirstOrDefault().ADDRESS1 + " " + E.E_CONTACT.FirstOrDefault().ADDRESS2
                            + " " + E.E_CONTACT.FirstOrDefault().POSTCODE + " " + E.E_CONTACT.FirstOrDefault().COUNTY
                        });

            if (model.employeeType == "Previous")
            {
                data = data.Where(J => J.endDateString != null && J.status == 0);
            }
            else if (model.employeeType == "Current")
            {
                data = data.Where(J => J.endDateString == null && J.status == 1);
            }
            if (model.fromDate != null && model.toDate != null && model.fromDate != "" && model.toDate != "")
            {
                DateTime? fromDate = Convert.ToDateTime(model.fromDate);
                DateTime? toDate = Convert.ToDateTime(model.toDate);
                data = data.Where(J => J.startDateString >= fromDate && J.startDateString <= toDate);
            }
            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            response.totalSalary = GeneralHelper.currencyFormat(finalData.Select(e => e.salary).Sum().ToString()); // CalculateTotalSalary();
            response.totalStaff = string.Format("{0}(M: {1} / F:{2})"
                , finalData.Count().ToString()
                , finalData.Where(e => e.gender == "Male").Count().ToString()
                , finalData.Where(e => e.gender == "Female").Count().ToString()); //CalculateTotalStaff();
            response.newStarter = GetNewStarter();  //GetNewStarter();
            response.leaver = Getleaver(); //Getleaver();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"fullName";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            if (model.sortBy == "startDate")
                _sortBy = "startDateString";
            else if (model.sortBy == "endDate")
                _sortBy = "endDateString";
            else if (model.sortBy == "dob")
                _sortBy = "dobString";
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            foreach (var item in finalData)
            {
                item.isSickness = IsSickness(item.employeeId);
                if (item.startDateString.HasValue)
                {
                    DateTime startDate = (DateTime)item.startDateString;
                    item.startDate = startDate.ToString("dd/MM/yyyy");
                }
                if (item.endDateString.HasValue)
                {
                    DateTime endDate = (DateTime)item.endDateString;
                    item.endDate = endDate.ToString("dd/MM/yyyy");
                }
                if (item.dobString.HasValue)
                {
                    DateTime dobString = (DateTime)item.dobString;
                    item.dob = dobString.ToString("dd/MM/yyyy");
                }
                if (item.gender == "Male")
                {
                    item.gender = "Male";
                }
                else if (item.gender == "Female")
                {
                    item.gender = "Female";
                }
                else
                {
                    item.gender = "-";
                }
            }

            response.establishmentList = finalData;
            //response.totalSalary = CalculateTotalSalary(); //finalData 
            //response.totalStaff = CalculateTotalStaff();
            //response.newStarter = GetNewStarter();
            //response.leaver = Getleaver();

            return response;
        }

        private string CalculateTotalSalary()
        {
            string result = string.Empty;
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        where J.ACTIVE == 1 && J.ENDDATE == null
                        select J).ToList();
            if (data.Count() > 0)
            {
                var salary = data.Select(e => e.SALARY).Sum();
                result = GeneralHelper.currencyFormat(salary.ToString());
            }
            return result;
        }
        private string CalculateTotalStaff()
        {
            string result = string.Empty;
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                            //where J.ACTIVE == 1 && J.ENDDATE == null
                        select E).ToList();
            if (data.Count() > 0)
            {
                int totalStaff = data.Count();
                int maleStaff = data.Where(e => e.GENDER == "Male").Count();
                int femaleStaff = data.Where(e => e.GENDER == "Female").Count();
                result = totalStaff.ToString() + "(M: " + maleStaff.ToString() + " / F:" + femaleStaff.ToString() + ")";
            }
            return result;
        }

        private int GetNewStarter()
        {
            int result = 0;
            DateTime startDate = DateTime.Now.AddMonths(-2);
            var data = (from stats in DbContext.E__EMPLOYEE_GET_NEW_STARTERS() select stats).ToList();
            if (data.Count() > 0)
                result = data.Count();

            return result;


        }

        private int Getleaver()
        {
            int result = 0;
            DateTime endDate = DateTime.Now.AddMonths(-2);
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        where J.ENDDATE >= endDate
                        select E).ToList();
            if (data.Count() > 0)
                result = data.Count();

            return result;
        }
        private bool IsSickness(int employeeId)
        {
            bool result = false;
            var maxResults = from p in DbContext.E_ABSENCE
                             group p by p.JOURNALID into g
                             select new { JournalId = g.Key, MaxABSENCEHISTORYID = g.Max(s => s.ABSENCEHISTORYID) };


            var data = (from ea in DbContext.E_ABSENCE
                        join m in maxResults on ea.ABSENCEHISTORYID equals m.MaxABSENCEHISTORYID
                        join ej in DbContext.E_JOURNAL on ea.JOURNALID equals ej.JOURNALID
                        join es in DbContext.E_STATUS on ej.CURRENTITEMSTATUSID equals es.ITEMSTATUSID
                        join en in DbContext.E_NATURE on ej.ITEMNATUREID equals en.ITEMNATUREID
                        where ej.EMPLOYEEID == employeeId && ej.ITEMNATUREID == 1 && (ea.RETURNDATE == null && ea.STARTDATE.Value.Year == DateTime.Now.Year)
                        select ea).ToList();
            if (data.Count() > 0)
                result = true;

            return result;
        }
        #endregion


        #region Sickness Report
        // Sickness Export
        public SicknessExportList SicknessExport(SicknessRequest model)
        {

            SicknessExportList response = new SicknessExportList();
            int CurrentYear = DateTime.Now.Year;
            DateTime sicknessStartDate = new DateTime(CurrentYear, 1, 1);
            DateTime sicknessEndDate = new DateTime(CurrentYear, 12, 31).AddHours(23).AddMinutes(59).AddSeconds(59);
            if (model.fromDate != "" && model.toDate != "" && model.fromDate != null && model.toDate != null)
            {
                sicknessStartDate = DateTime.Parse(model.fromDate);
                sicknessEndDate = DateTime.Parse(model.toDate);
            }
            var maxResults = from p in DbContext.E_ABSENCE
                             where (
                             (
                             (p.STARTDATE >= sicknessStartDate && p.STARTDATE <= sicknessEndDate) || (p.RETURNDATE == null)
                             )
                             && p.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                       where abs1.JOURNALID == p.JOURNALID
                                                       select abs1.ABSENCEHISTORYID).ToList().Max()
                             )
                             group p by p.JOURNALID into g
                             select new { JournalId = g.Key, MaxABSENCEHISTORYID = g.Max(s => s.ABSENCEHISTORYID) };

            var data = (from E in DbContext.E__EMPLOYEE
                        join ej in DbContext.E_JOURNAL on E.EMPLOYEEID equals ej.EMPLOYEEID
                        join ea in DbContext.E_ABSENCE on ej.JOURNALID equals ea.JOURNALID
                        join m in maxResults on ea.ABSENCEHISTORYID equals m.MaxABSENCEHISTORYID
                        join r in DbContext.E_ABSENCEREASON on ea.REASONID equals r.SID
                        join es in DbContext.E_STATUS on ej.CURRENTITEMSTATUSID equals es.ITEMSTATUSID
                        join en in DbContext.E_NATURE on ej.ITEMNATUREID equals en.ITEMNATUREID
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        join FPT in DbContext.E_PARTFULLTIME on J.PARTFULLTIME equals FPT.PARTFULLTIMEID into FPT_join
                        from FPT in FPT_join.DefaultIfEmpty()
                        where ea.ITEMSTATUSID != 20 && ea.DURATION != null && J.TEAM != 1 && T.ACTIVE == 1 &&
                         ej.ITEMNATUREID == 1 &&
                          (T.DIRECTORATEID == model.directorate || model.directorate == null) &&
                         (J.TEAM == model.team || model.team == null) &&
                          (ea.REASONID == model.reasonId || model.reasonId == null) &&
                          ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                          && J.ACTIVE==1
                        select new SicknessExportDetail
                        {
                            employeeId = E.EMPLOYEEID,
                            firstName = E.FIRSTNAME,
                            surName=E.LASTNAME,
                            full_partTime = (FPT.DESCRIPTION ?? ""),
                            jobTitle = (J.JOBTITLE ?? ""),
                            dept= (T.TEAMNAME ?? ""),
                            duration=ea.DURATION,
                            reason= (r.DESCRIPTION ?? ""),
                            employeeStartDateString=J.STARTDATE,
                            startDateString =ea.STARTDATE,
                            endDateString=ea.RETURNDATE,
                            absenceHistoryId=ea.ABSENCEHISTORYID,
                            status=J.ACTIVE,
                            absType=ea.DURATION_TYPE
                        });

            if (model.employeeType == "Previous")
            {
                data = data.Where(J => J.status == 0);
            }
            else if (model.employeeType == "Current")
            {
                data = data.Where(J => J.status == 1);
            }
            else
            {
                data = data.Where(J => J.status != null);
            }            
            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            foreach (var item in finalData)
            {
                if (item.startDateString.HasValue)
                {
                    DateTime startDate = (DateTime)item.startDateString;
                    item.startDate = startDate.ToString("dd/MM/yyyy");
                }
                if (item.endDateString.HasValue)
                {
                    DateTime endDateString = (DateTime)item.endDateString;
                    item.endDate = endDateString.ToString("dd/MM/yyyy");
                }
                if (item.employeeStartDateString.HasValue)
                {
                    DateTime employeeStartDateString = (DateTime)item.employeeStartDateString;
                    item.employeeStartDate = employeeStartDateString.ToString("dd/MM/yyyy");
                }
                if (item.endDateString.HasValue)
                {
                    DateTime lastDateOfAbsence = (DateTime)item.endDateString.Value.AddDays(-1);
                    if (lastDateOfAbsence.DayOfWeek == DayOfWeek.Saturday)
                    {
                        lastDateOfAbsence = lastDateOfAbsence.AddDays(-1);
                    }
                    if (lastDateOfAbsence.DayOfWeek == DayOfWeek.Sunday)
                    {
                        lastDateOfAbsence = lastDateOfAbsence.AddDays(-2);
                    }
                    item.lastDateOfAbsence = lastDateOfAbsence.ToString("dd/MM/yyyy");
                }
                var absence = (from abs in DbContext.E_AbsenseSickness(item.employeeId, null)
                               select new AbsenceLeave()
                               {
                                   absentDays = abs.DAYS_ABS,
                                   startDate = abs.STARTDATE,
                                   status = abs.Status,
                                   absenceHistoryId = abs.AbsenceHistoryId,
                                   unit=abs.DURATION_TYPE
                               }).ToList().Where(x => x.absenceHistoryId == item.absenceHistoryId).FirstOrDefault();
                if (absence != null)
                {
                    item.durationString = absence.absentDays.ToString() + " ("+absence.unit+")";
                } 
                else
                {
                    item.durationString = item.duration.ToString() + " (" + item.absType + ")";
                }
            }
            if (model.isLongTerm == true)
            {
                finalData = finalData.Where(j => j.duration > 10).ToList();
            }
            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"surName";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            if (model.sortBy == "startDate")
                _sortBy = "startDateString";
            else if (model.sortBy == "endDate")
                _sortBy = "endDateString";
            else if (model.sortBy == "employeeStartDate")
                _sortBy = "employeeStartDateString";

            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            response.sicknessExportList = finalData;
            return response;
        }
        // SicknessList
        public SicknessList PopulateSicknessReport(SicknessRequest model)
        {

            SicknessList response = new SicknessList();
            int CurrentYear = DateTime.Now.Year;
            DateTime sicknessStartDate = new DateTime(CurrentYear, 1, 1);
            DateTime sicknessEndDate = new DateTime(CurrentYear, 12, 31).AddHours(23).AddMinutes(59).AddSeconds(59);
            if (model.fromDate != "" && model.toDate != "" && model.fromDate != null && model.toDate != null)
            {
                sicknessStartDate = DateTime.Parse(model.fromDate);
                sicknessEndDate = DateTime.Parse(model.toDate);
            }
            var maxResults = from p in DbContext.E_ABSENCE
                             where (
                             (
                             (p.STARTDATE >= sicknessStartDate && p.STARTDATE <= sicknessEndDate) || (p.RETURNDATE == null)
                             ) 
                             && p.ABSENCEHISTORYID == (from abs1 in DbContext.E_ABSENCE
                                                                                  where abs1.JOURNALID == p.JOURNALID
                                                                                  select abs1.ABSENCEHISTORYID).ToList().Max()
                             )
                             group p by p.JOURNALID into g
                             select new { JournalId = g.Key, MaxABSENCEHISTORYID = g.Max(s => s.ABSENCEHISTORYID) };

            var data = (from E in DbContext.E__EMPLOYEE
                        join ej in DbContext.E_JOURNAL on E.EMPLOYEEID equals ej.EMPLOYEEID
                        join ea in DbContext.E_ABSENCE on ej.JOURNALID equals ea.JOURNALID
                        join m in maxResults on ea.ABSENCEHISTORYID equals m.MaxABSENCEHISTORYID
                        join r in DbContext.E_ABSENCEREASON on ea.REASONID equals r.SID
                        join es in DbContext.E_STATUS on ej.CURRENTITEMSTATUSID equals es.ITEMSTATUSID
                        join en in DbContext.E_NATURE on ej.ITEMNATUREID equals en.ITEMNATUREID
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        where ea.ITEMSTATUSID != 20 && ea.DURATION != null && J.TEAM != 1 && T.ACTIVE == 1 &&
                         ej.ITEMNATUREID == 1 &&
                          (T.DIRECTORATEID == model.directorate || model.directorate == null) &&
                         (J.TEAM == model.team || model.team == null) &&
                          (ea.REASONID == model.reasonId || model.reasonId == null) &&
                          ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                          && J.ACTIVE==1
                        select new SicknessDetail
                        {
                            fullName = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            directorate = (D.DIRECTORATENAME ?? "No Directorate"),
                            employeeId = E.EMPLOYEEID,
                            reason = (r.DESCRIPTION ?? "No reason available."),
                            startDateString = ea.STARTDATE,
                            actualReturnString = ea.RETURNDATE,
                            duration = ea.DURATION,
                            journalId = ej.JOURNALID,
                            returnNotes = ea.NOTES,
                            endDateString = J.ENDDATE,
                            status = J.ACTIVE,
                            anticipatedReturnString = ea.AnticipatedReturnDate,
                            absenceHistoryId = ea.ABSENCEHISTORYID,
                            absType = ea.DURATION_TYPE
                        });

            if (model.employeeType == "Previous")
            {
                data = data.Where(J => J.status == 0);
            }
            else if (model.employeeType == "Current")
            {
                data = data.Where(J => J.status == 1);
            }
            else
            {
                data = data.Where(J => J.status != null);
            }
            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();
			
            foreach (var item in finalData)
            {
                if (item.startDateString.HasValue)
                {
                    DateTime startDate = (DateTime)item.startDateString;
                    item.startDate = startDate.ToString("dd/MM/yyyy");
                }
                if (item.actualReturnString.HasValue)
                {
                    DateTime actualReturnDate = (DateTime)item.actualReturnString;
                    item.actualReturnDate = actualReturnDate.ToString("dd/MM/yyyy");
                }
                if (item.anticipatedReturnString.HasValue)
                {
                    DateTime anticipatedReturnDate = (DateTime)item.anticipatedReturnString;
                    item.anticipatedReturnDate = anticipatedReturnDate.ToString("dd/MM/yyyy");
                }
                if (item.actualReturnString.HasValue)
                {
                    var WH = (from wh in DbContext.E_WorkingHours_History
                          where item.actualReturnString >= wh.StartDate && (item.actualReturnString <= wh.EndDate || wh.EndDate == null)
                          && wh.EmployeeId==item.employeeId
                          select wh).FirstOrDefault();
                    if (WH != null)
                    {
                        Dictionary<string, double> whDictionary = new Dictionary<string, double>();
                        whDictionary.Add("Monday", Double.Parse(WH.Mon.ToString()));
                        whDictionary.Add("Tuesday", Double.Parse(WH.Tue.ToString()));
                        whDictionary.Add("Wednesday", Double.Parse(WH.Wed.ToString()));
                        whDictionary.Add("Thursday", Double.Parse(WH.Thu.ToString()));
                        whDictionary.Add("Friday", Double.Parse(WH.Fri.ToString()));


                        bool value = false;
                        DateTime lastDateOfAbsence = new DateTime();
                        for (DateTime d = (DateTime)item.actualReturnString.Value.AddDays(-1); value == false; d=d.AddDays(-1))
                        {
                            if (whDictionary.Where(x => x.Key.Equals(d.ToString("dddd"))).FirstOrDefault().Value > 0)
                            {
                                lastDateOfAbsence = d;
                                value = true;
                            }
                        }
                        item.lastDateOfAbsence = lastDateOfAbsence.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        DateTime lastDateOfAbsence = (DateTime)item.actualReturnString.Value.AddDays(-1);
                        if (lastDateOfAbsence.DayOfWeek == DayOfWeek.Saturday)
                        {
                            lastDateOfAbsence = lastDateOfAbsence.AddDays(-1);
                        }
                        if (lastDateOfAbsence.DayOfWeek == DayOfWeek.Sunday)
                        {
                            lastDateOfAbsence = lastDateOfAbsence.AddDays(-2);
                        }
                        item.lastDateOfAbsence = lastDateOfAbsence.ToString("dd/MM/yyyy");
                    }
                    
                }
                var absence =(from abs in DbContext.E_AbsenseSickness(item.employeeId, null)
                               select new AbsenceLeave()
                               {
                                   absentDays = abs.DAYS_ABS,
                                   startDate = abs.STARTDATE,
                                   status = abs.Status,
                                   absenceHistoryId=abs.AbsenceHistoryId,
                                   unit = abs.DURATION_TYPE
                               }).ToList().Where(x => x.absenceHistoryId == item.absenceHistoryId).FirstOrDefault();
                if (absence != null)
                {
                    item.durationString = absence.absentDays.ToString() + " (" + absence.unit + ")";
                }
                else
                {
                    item.durationString = item.duration.ToString() + " (" + item.absType + ")";
                }
            }
            if (model.isLongTerm == true)
            {
                finalData = finalData.Where(j => j.duration > 10).ToList();
            }
            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"fullName";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            if (model.sortBy == "startDate")
                _sortBy = "startDateString";
            else if (model.sortBy == "actualReturnDate")
                _sortBy = "actualReturnString";

            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            response.sicknessList = finalData;
            return response;
        }

        #endregion

        #region Annual Leave Report
        public AnnualLeaveReport populateAnnualLeaveReport(AnnualLeaveRequest model)
        {
            AnnualLeaveReport response = new AnnualLeaveReport();
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                        join EL in DbContext.E__EMPLOYEE on J.LINEMANAGER equals EL.EMPLOYEEID
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        where J.ACTIVE == 1 && (J.PARTFULLTIME == 1 || J.PARTFULLTIME == 2) &&
                        (T.DIRECTORATEID == model.directorate || model.directorate == null) &&
                         (J.TEAM == model.team || model.team == null) &&
                         ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                        select new AnnualLeaveListing
                        {
                            employeeId = E.EMPLOYEEID,
                            payRoleNum = (J.PAYROLLNUMBER ?? "No Pay Roll Number"),
                            lineManager = EL.FIRSTNAME + " " + EL.LASTNAME,
                            fullName = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            directorate = (D.DIRECTORATENAME ?? "No Directorate"),
                            startDate = J.STARTDATE,
                            EmploymentStartDateString=J.STARTDATE,
                            contractedHours=J.HOURS
                        });

            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();
            string _sortBy = @"";
            if (model.sortBy == "")
            {
                _sortBy = @"fullName";
            }
            else
            {
                _sortBy = model.sortBy;
            }


            // Getting sorted by sort parameter

            foreach (var item in finalData)
            {
                DateTime runningDate = DateTime.Now;

                if (model.year != runningDate.Year)
                {
                    if (item.startDate.Value.Day == 29 && item.startDate.Value.Month == 2)
                    {
                        item.startDate = item.startDate.Value.AddDays(-1);
                    }
                    runningDate = DateTime.Parse(item.startDate.Value.Day.ToString() + "/" + item.startDate.Value.Month.ToString() + "/" + model.year.ToString());
                }

                item.AlStartDate = getLeaveYearDate(item.employeeId);
                if (item.EmploymentStartDateString.HasValue)
                {
                    item.EmploymentStartDate = item.EmploymentStartDateString.Value.ToString("dd/MM/yyyy");
                }
                //Annual Leave data update
                var LeaveStatsData = (from stats in DbContext.E_LeaveStats(item.employeeId, null, true, runningDate.Date) select stats).FirstOrDefault();
                //Annual Leave data update
                var leavedata = (from stats in DbContext.E_AnnualLeaveReport(item.employeeId, runningDate.Date) select stats).FirstOrDefault();
                if (leavedata != null)
                {
                    var totalBankHolidays = (from e in DbContext.E_BOOKED_BANK_HOLIDAY(item.employeeId, runningDate.Date)
                                             select e.Value).FirstOrDefault();
                    totalBankHolidays = totalBankHolidays - Decimal.Parse(LeaveStatsData.BNK_HD.ToString());
                    var leaveAvailable = leavedata.ANNUAL_LEAVE_DAYS - leavedata.LeaveRequested - leavedata.LeaveTaken - leavedata.LeaveBooked;

                    item.totalAllowance = leavedata.ANNUAL_LEAVE_DAYS;
                    item.balanceRemaining = leaveAvailable + LeaveStatsData.CARRY_FWD - Double.Parse(totalBankHolidays.ToString()); // + LeaveStatsData.BNK_HD
                    item.daysBooked = leavedata.LeaveBooked;
                    item.daysRequested = leavedata.LeaveRequested;
                    item.daysTaken = leavedata.LeaveTaken;
                    item.carryForward = LeaveStatsData.CARRY_FWD;
                }
            }
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            response.reportListing = finalData;

            return response;
        }
        public string getLeaveYearDate(int empId)
        {
            string Responce = "";
            var fYear = DbContext.EMPLOYEE_ANNUAL_START_END_DATE_ByYear(empId, DateTime.Now).FirstOrDefault();

            if (fYear != null)
            {
                if (fYear.STARTDATE.HasValue)
                {
                    Responce = fYear.STARTDATE.Value.ToShortDateString();
                }
            }

            return Responce;


        }
        #endregion

        #region Leaver Report
        public LeaverReportListing PopulateLeaverReport(LeaverReportRequest model)
        {
            F_FISCALYEARS fiscalYear = new F_FISCALYEARS();
            fiscalYear = getCurrentFiscalYear();
            if (fiscalYear == null)
            {
                fiscalYear = new F_FISCALYEARS();
            }
            LeaverReportListing response = new LeaverReportListing();
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()
                        where J.ENDDATE <= DateTime.Now &&
                         (T.DIRECTORATEID == model.directorate || model.directorate == null) &&
                         (J.TEAM == model.team || model.team == null) &&
                         ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                        select new LeaverReportDetail
                        {
                            employeeId = E.EMPLOYEEID,
                            fullName = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            startDateTime = J.STARTDATE,
                            leaveDateTime = J.ENDDATE,
                            directorate = (D.DIRECTORATENAME ?? "No Directorate"),
                            reason = "N/A"
                        });
            if (model.fromDate != null && model.toDate != null && model.fromDate != "" && model.toDate != "")
            {
                DateTime? fromDate = Convert.ToDateTime(model.fromDate);
                DateTime? toDate = Convert.ToDateTime(model.toDate);
                data = data.Where(J => J.leaveDateTime >= fromDate && J.leaveDateTime <= toDate);
            }
            if (model.fromDate == null && model.toDate == null)
            {
                DateTime fromDate = fiscalYear.YStart.Value;
                data = data.Where(J => J.leaveDateTime >= fromDate && J.leaveDateTime <= fiscalYear.YEnd);
                response.fromDate = fiscalYear.YStart.Value;
                response.toDate = fiscalYear.YEnd.Value;
            }
            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"leaveDateTime";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            if (model.sortBy == "startDate")
                _sortBy = "startDateTime";
            else if (model.sortBy == "leaveDate")
                _sortBy = "leaveDateTime";

            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            foreach (var item in finalData)
            {

                if (item.startDateTime.HasValue)
                {
                    DateTime startDate = (DateTime)item.startDateTime;
                    item.startDate = startDate.ToString("dd/MM/yyyy");
                }
                if (item.leaveDateTime.HasValue)
                {
                    DateTime endDate = (DateTime)item.leaveDateTime;
                    item.leaveDate = endDate.ToString("dd/MM/yyyy");
                }
            }
            response.reportListing = finalData;
            return response;

        }
        #endregion

        #region New Starter Report
        public NewStarterListing PopulateNewStarterReport(CommonReportsFilterRequest model)
        {
            NewStarterListing response = new NewStarterListing();
            //F_FISCALYEARS fiscalYear = new F_FISCALYEARS();
            //fiscalYear = DbContext.F_FISCALYEARS.OrderByDescending(x => x.YRange).First();

            //if (fiscalYear == null)
            //{
            //    fiscalYear = new F_FISCALYEARS();
            //}
            DateTime jobStartDate = DateTime.Now.AddMonths(-2).Date;
            DateTime jobEndDate = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            if (model.fromDate!=null && model.fromDate !="")
            {
                jobStartDate = DateTime.Parse(model.fromDate);
                if (model.toDate != null && model.toDate != "")
                {
                    jobEndDate = DateTime.Parse(model.toDate);
                }
            }
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on J.GRADE equals G.GRADEID into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on J.JobRoleId equals ET.JobRoleId into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                        from ETH in ETH_join.DefaultIfEmpty()

                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()

                        where J.ENDDATE == null && J.STARTDATE >= jobStartDate && J.STARTDATE <= jobEndDate &&
                        (T.DIRECTORATEID == model.directorate || model.directorate == null) &&
                         (J.TEAM == model.team || model.team == null)
                        && ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                        select new NewStarterDetail
                        {
                            directorate = (D.DIRECTORATENAME ?? "No Directorate"),
                            team = (T.TEAMNAME ?? "No Team"),
                            dobDate = E.DOB,
                            employeeId = E.EMPLOYEEID,
                            ethnicity = (ETH.DESCRIPTION ?? "N/A"),
                            fullName = E.FIRSTNAME + " " + E.LASTNAME,
                            gender = (E.GENDER ?? "-"),
                            grade = G.DESCRIPTION,
                            jobTitle = ET.JobeRoleDescription,
                            salary = J.SALARY,
                            reviewDateTime = J.REVIEWDATE,
                            startDateTime = J.STARTDATE,
                            status = J.ACTIVE,
                            endDateString = J.ENDDATE

                        });
            if (data.Count() > 0)
            {
                if (model.employeeType == "Previous")
                {
                    data = data.Where(J => J.endDateString != null && J.status == 0);
                }
                else if (model.employeeType == "Current")
                {
                    data = data.Where(J => J.endDateString == null || J.status == 1);
                }
                Pagination page = new Pagination();
                page.totalRows = data.ToList().Count();
                page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
                page.pageNo = model.pagination.pageNo;
                page.pageSize = model.pagination.pageSize;
                int _page = (page.pageNo - 1);

                response.pagination = page;

                // Counting skip
                int _skip = _page * page.pageSize;
                var finalData = data.ToList();

                string _sortBy = @"";
                if (model.sortBy == null)
                {
                    _sortBy = @"fullName";
                }
                else
                {
                    _sortBy = model.sortBy;
                }
                if (model.sortBy == "startDate")
                    _sortBy = "startDateTime";
                else if (model.sortBy == "reviewDate")
                    _sortBy = "reviewDateTime";
                else if (model.sortBy == "reviewDate")
                    _sortBy = "dobDate";
                // Getting sorted by sort parameter
                if (model.sortOrder == "ASC")
                {
                    finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                else
                {
                    finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }

                foreach (var item in finalData)
                {
                    if (item.gender == "Male")
                    {
                        item.gender = "M";
                    }
                    else if (item.gender == "Female")
                    {
                        item.gender = "F";
                    }
                    else
                    {
                        item.gender = "-";
                    }
                    if (item.startDateTime.HasValue)
                    {
                        DateTime startDate = (DateTime)item.startDateTime;
                        item.startDate = startDate.ToString("dd/MM/yyyy");
                    }
                    if (item.reviewDateTime.HasValue)
                    {
                        DateTime reviewDateTime = (DateTime)item.reviewDateTime;
                        item.reviewDate = reviewDateTime.ToString("dd/MM/yyyy");
                    }
                    if (item.dobDate.HasValue)
                    {
                        DateTime dobDate = (DateTime)item.dobDate;
                        item.dob = dobDate.ToString("dd/MM/yyyy");
                    }
                }
                response.reportListing = finalData;
            }
            if (model.fromDate != null && model.fromDate != "")
            {
                response.fromDate = Convert.ToDateTime(model.fromDate);
            }
            if (model.toDate != null && model.toDate != "")
            {
                response.toDate = Convert.ToDateTime(model.toDate);
            }
            if (model.fromDate == null || model.fromDate == "")
            {
                response.fromDate = jobStartDate;
            }
            if (model.toDate == null || model.toDate == "")
            {
                response.toDate = jobEndDate;
            }
            return response;
        }
        #endregion

        #region Populate My Staff List
        public MyStaffList PopulateMyStaffReport(MyStaffEmployeeRequest model)
        {
            MyStaffList response = new MyStaffList();
            var fData = getCurrentFiscalYear();

            DateTime jobStartDate = DateTime.Now.AddMonths(-10);

            if (model.isDirector)
            {
                var employeeData = (from E in DbContext.E__EMPLOYEE
                                    join PS in DbContext.E_PaypointSubmission on E.EMPLOYEEID equals PS.employeeId
                                    join PSS in DbContext.E_PayPointStatus on PS.paypointStatusId equals PSS.PayPointStatusId
                                    join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                                    from J in J_join.DefaultIfEmpty()
                                    join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                                    from T in T_join.DefaultIfEmpty()
                                    join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                                    from D in D_join.DefaultIfEmpty()
                                    join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                                    from G in G_join.DefaultIfEmpty()
                                    join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                                    from ET in ET_join.DefaultIfEmpty()
                                    join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                                    from ETH in ETH_join.DefaultIfEmpty()
                                    join DOI in DbContext.E_DOI on E.EMPLOYEEID equals DOI.EmployeeId into DOI_join
                                    from DOI in DOI_join.Where(e => e.FiscalYear == fData.YRange).DefaultIfEmpty()
                                    join DOIs in DbContext.E_DOI_Status on DOI.Status equals DOIs.DOIStatusId into DOIs_join
                                    from DOIs in DOIs_join.DefaultIfEmpty()
                                    join Gift in DbContext.E_GiftsAndHospitalities on E.EMPLOYEEID equals Gift.EmployeeId into gift_join
                                    from Gift in gift_join.Where(e => e.GiftStatusId == 4).DefaultIfEmpty()
                                    join gStatus in DbContext.E_Gift_Status on Gift.GiftStatusId equals gStatus.GiftStatusId into gStatus_join
                                    from gStatus in gStatus_join.DefaultIfEmpty()
                                    where T.DIRECTOR == model.employeeId && J.ACTIVE == 1 && PSS.Description == "Submitted" &&
                                    PS.fiscalYearId == fData.YRange && T.DIRECTOR == model.employeeId && J.ACTIVE == 1
                                    select new MyStaffDetail
                                    {
                                        employeeId = E.EMPLOYEEID,
                                        fullName = E.FIRSTNAME + " " + E.LASTNAME,
                                        grade = G.DESCRIPTION,
                                        jobTitle = ET.JobeRoleDescription,
                                        paypointReviewDate = J.PayPointReviewDate,
                                        appraisalDateTime = J.APPRAISALDATE,
                                        doiStatus = DOIs.Description,
                                        interestId = DOI == null ? null : (int?)DOI.InterestId,
                                        giftStatus = gStatus.Description

                                    }).Distinct();

                if (employeeData.Count() > 0)
                {

                    Pagination page = new Pagination();
                    page.totalRows = employeeData.Count();
                    page.totalPages = (int)Math.Ceiling((double)employeeData.Count() / model.pagination.pageSize);
                    page.pageNo = model.pagination.pageNo;
                    page.pageSize = model.pagination.pageSize;
                    int _page = (page.pageNo - 1);

                    response.pagination = page;

                    // Counting skip
                    int _skip = _page * page.pageSize;
                    var finalData = employeeData.ToList();

                    string _sortBy = @"";
                    if (model.sortBy == null)
                    {
                        _sortBy = @"fullName";
                    }
                    else
                    {
                        _sortBy = model.sortBy;
                    }
                    if (model.sortBy == "paypointReview")
                        model.sortBy = "paypointReviewDate";
                    else if (model.sortBy == "appraisal")
                        model.sortBy = "appraisalDateTime";

                    // Getting sorted by sort parameter
                    if (model.sortOrder == "ASC")
                    {
                        finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                    }
                    else
                    {
                        finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                    }

                    foreach (var item in finalData)
                    {
                        if (item.paypointReviewDate.HasValue)
                        {
                            DateTime paypointReviewDate = (DateTime)item.paypointReviewDate;
                            item.paypointReview = paypointReviewDate.ToString("dd/MM/yyyy");
                        }
                        if (item.appraisalDateTime.HasValue)
                        {
                            DateTime appraisalDateTime = (DateTime)item.appraisalDateTime;
                            item.appraisal = appraisalDateTime.ToString("dd/MM/yyyy");
                        }
                        //Get the Annual leave and remaining leaves w.r.t carry forword and Toil
                        var leavedata = (from stats in DbContext.E_LeaveStats(item.employeeId, null, true,null) select stats).FirstOrDefault();
                        if (leavedata != null)
                        {
                            var annualLeaveData = (from absence in DbContext.E_AbsenseAnnual(item.employeeId, null)
                                                   select new AbsenceAnnual()
                                                   {
                                                       absenceHistoryId = absence.ABSENCEHISTORYID,
                                                       endDate = absence.RETURNDATE,
                                                       status = absence.Status,
                                                       absentDays = (double)absence.DAYS_ABS,
                                                       nature = absence.Nature,
                                                       startDate = absence.STARTDATE,
                                                       holidayType = absence.HolType,
                                                       lastActionDatetime = absence.LastActionDate,
                                                       unit = leavedata.Unit,
                                                       durationType = absence.DurationType
                                                   }).ToList();
                            var EndDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.employeeId)
                                           select new AbsenceAnnual()
                                           {
                                               endDate = absence.ENDDATE
                                           }).FirstOrDefault().endDate;
                            var StartDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.employeeId)
                                             select new AbsenceAnnual()
                                             {
                                                 startDate = absence.STARTDATE
                                             }).FirstOrDefault().startDate;
                            double LeavesRequested = 0;
                            foreach (var al in annualLeaveData)
                            {
                                if (al.endDate <= EndDate)
                                {
                                    if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                    {
                                        LeavesRequested = LeavesRequested + double.Parse(al.absentDays.ToString());
                                    }
                                }
                                else if (al.endDate > EndDate && al.startDate <= EndDate)
                                {
                                    if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                    {
                                        double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                                        LeavesRequested = LeavesRequested + LeaveDaysCount;
                                    }
                                }
                            }
                            var totalBankHolidays = (from e in DbContext.E_BOOKED_BANK_HOLIDAY(item.employeeId, StartDate)
                                                     select e.Value).FirstOrDefault();
                            totalBankHolidays = totalBankHolidays - Decimal.Parse(leavedata.BNK_HD.ToString());
                            leavedata.LEAVE_AVAILABLE = leavedata.ANNUAL_LEAVE_DAYS - LeavesRequested;

                            item.remainingLeave = Math.Round(Convert.ToDecimal(leavedata.LEAVE_AVAILABLE + leavedata.CARRY_FWD)) - totalBankHolidays;
                            item.annualLeave = Math.Round(Convert.ToDecimal(leavedata.ANNUAL_LEAVE_DAYS));
                        }
                        var isEmployeeOnLeave = DbContext.E_AnnualLeaveCount(item.employeeId).FirstOrDefault();
                        if (isEmployeeOnLeave != null)
                        {
                            var otherLeavesNature = DbContext.E_NATURE.ToList();
                            var excludedNature = new List<string> { "Sickness", "Annual Leave", "Internal Meeting", "BRS TOIL Recorded", "BRS Time Off in Lieu", "BHG TOIL" };
                            List<string> leaveOfAbsenceNatures = otherLeavesNature.Where(x => !excludedNature.Contains(x.DESCRIPTION) && x.ITEMID == 1).Select(x => new string(x.DESCRIPTION.ToArray())).ToList();
                            if (leaveOfAbsenceNatures.Contains(isEmployeeOnLeave.Nature))
                            {
                                item.leaveType = "Other Leave";
                            }
                            else
                            {
                                item.leaveType = isEmployeeOnLeave.Nature;
                            }
                        }
                        var isEmployeeHasPendingLeave = DbContext.E_GetPendingAbsence(item.employeeId).FirstOrDefault();
                        if (isEmployeeHasPendingLeave != null)
                        {
                            item.Pendingleave = isEmployeeHasPendingLeave.Nature;
                        }

                        var empTrainingCount = (from EET in DbContext.E_EmployeeTrainings
                                                join ES in DbContext.E_EmployeeTrainingStatus on EET.Status equals ES.StatusId
                                                join E in DbContext.E__EMPLOYEE on EET.EmployeeId equals E.EMPLOYEEID
                                                join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                                                join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                                                from T in T_join.DefaultIfEmpty()
                                                join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                                                from D in D_join.DefaultIfEmpty()
                                                where
                                                   ((EET.ProfessionalQualification == null || EET.ProfessionalQualification == 0) && EET.TotalCost <= 1000)
                                                    && EET.Active == true
                                                    && J.ACTIVE == 1
                                                    && (EET.EmployeeId == item.employeeId)
                                                    && (ES.Title == ApplicationConstants.grpTrainingStatusRequested) //grpTrainingStatusSubmitted
                                                select EET);

                        if (empTrainingCount != null)
                        {
                            item.trainingApproval = empTrainingCount.Count();
                        }
                    }

                    response.employeeList = finalData;

                }
                else
                {
                    response.employeeList = new List<MyStaffDetail>();
                }
                return response;
            }

            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                        from ETH in ETH_join.DefaultIfEmpty()
                        join DOI in DbContext.E_DOI on E.EMPLOYEEID equals DOI.EmployeeId into DOI_join
                        from DOI in DOI_join.Where(e => e.FiscalYear == fData.YRange).DefaultIfEmpty()
                        join DOIs in DbContext.E_DOI_Status on DOI.Status equals DOIs.DOIStatusId into DOIs_join
                        from DOIs in DOIs_join.DefaultIfEmpty()
                        join Gift in DbContext.E_GiftsAndHospitalities on E.EMPLOYEEID equals Gift.EmployeeId into gift_join
                        from Gift in gift_join.Where(e => e.GiftStatusId == 4).DefaultIfEmpty()
                        join gStatus in DbContext.E_Gift_Status on Gift.GiftStatusId equals gStatus.GiftStatusId into gStatus_join
                        from gStatus in gStatus_join.DefaultIfEmpty()
                        where J.LINEMANAGER == model.employeeId && J.ACTIVE == 1
                        select new MyStaffDetail
                        {
                            employeeId = E.EMPLOYEEID,
                            fullName = E.FIRSTNAME + " " + E.LASTNAME,
                            grade = G.DESCRIPTION,
                            jobTitle = ET.JobeRoleDescription,
                            paypointReviewDate = J.PayPointReviewDate,
                            appraisalDateTime = J.APPRAISALDATE,
                            doiStatus = DOIs.Description,
                            interestId = DOI == null ? null : (int?)DOI.InterestId,
                            giftStatus = gStatus.Description

                        }).Distinct();
            if (data.Count() > 0)
            {

                Pagination page = new Pagination();
                page.totalRows = data.Count();
                page.totalPages = (int)Math.Ceiling((double)data.Count() / model.pagination.pageSize);
                page.pageNo = model.pagination.pageNo;
                page.pageSize = model.pagination.pageSize;
                int _page = (page.pageNo - 1);

                response.pagination = page;

                // Counting skip
                int _skip = _page * page.pageSize;
                var finalData = data.ToList();

                string _sortBy = @"";
                if (model.sortBy == null)
                {
                    _sortBy = @"fullName";
                }
                else
                {
                    _sortBy = model.sortBy;
                }
                if (model.sortBy == "paypointReview")
                    model.sortBy = "paypointReviewDate";
                else if (model.sortBy == "appraisal")
                    model.sortBy = "appraisalDateTime";

                // Getting sorted by sort parameter
                if (model.sortOrder == "ASC")
                {
                    finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                else
                {
                    finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }

                foreach (var item in finalData)
                {
                    if (item.paypointReviewDate.HasValue)
                    {
                        DateTime paypointReviewDate = (DateTime)item.paypointReviewDate;
                        item.paypointReview = paypointReviewDate.ToString("dd/MM/yyyy");
                    }
                    if (item.appraisalDateTime.HasValue)
                    {
                        DateTime appraisalDateTime = (DateTime)item.appraisalDateTime;
                        item.appraisal = appraisalDateTime.ToString("dd/MM/yyyy");
                    }
                    //Get the Annual leave and remaining leaves w.r.t carry forword and Toil
                    var leavedata = (from stats in DbContext.E_LeaveStats(item.employeeId, null, true, null) select stats).FirstOrDefault();
                    if (leavedata != null)
                    {
                        var annualLeaveData = (from absence in DbContext.E_AbsenseAnnual(item.employeeId, null)
                                               select new AbsenceAnnual()
                                               {
                                                   absenceHistoryId = absence.ABSENCEHISTORYID,
                                                   endDate = absence.RETURNDATE,
                                                   status = absence.Status,
                                                   absentDays = (double)absence.DAYS_ABS,
                                                   nature = absence.Nature,
                                                   startDate = absence.STARTDATE,
                                                   holidayType = absence.HolType,
                                                   lastActionDatetime = absence.LastActionDate,
                                                   unit = leavedata.Unit,
                                                   durationType = absence.DurationType
                                               }).ToList();
                        var EndDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.employeeId)
                                       select new AbsenceAnnual()
                                       {
                                           endDate = absence.ENDDATE
                                       }).FirstOrDefault().endDate;
                        var StartDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.employeeId)
                                       select new AbsenceAnnual()
                                       {
                                           startDate = absence.STARTDATE
                                       }).FirstOrDefault().startDate;
                        double LeavesRequested = 0;
                        foreach (var al in annualLeaveData)
                        {
                            if (al.endDate <= EndDate)
                            {
                                if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                {
                                    LeavesRequested = LeavesRequested + double.Parse(al.absentDays.ToString());
                                }
                            }
                            else if (al.endDate > EndDate && al.startDate <= EndDate)
                            {
                                if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                {
                                    double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                                    LeavesRequested = LeavesRequested + LeaveDaysCount;
                                }
                                
                            }
                        }
                        var totalBankHolidays = (from e in DbContext.E_BOOKED_BANK_HOLIDAY(item.employeeId, StartDate)
                                                 select e.Value).FirstOrDefault();
                        totalBankHolidays = totalBankHolidays - Decimal.Parse(leavedata.BNK_HD.ToString());
                        leavedata.LEAVE_AVAILABLE = leavedata.ANNUAL_LEAVE_DAYS - LeavesRequested;

                        item.remainingLeave = Convert.ToDecimal(leavedata.LEAVE_AVAILABLE + leavedata.CARRY_FWD) - totalBankHolidays; 
                        item.annualLeave = Convert.ToDecimal(leavedata.ANNUAL_LEAVE_DAYS);
                    }
                    var isEmployeeOnLeave = DbContext.E_AnnualLeaveCount(item.employeeId).FirstOrDefault();
                    if (isEmployeeOnLeave != null)
                    {
                        var otherLeavesNature = DbContext.E_NATURE.ToList();
                        var excludedNature = new List<string> { "Sickness", "Annual Leave", "Internal Meeting", "BRS TOIL Recorded", "BRS Time Off in Lieu", "BHG TOIL" };
                        List<string> leaveOfAbsenceNatures = otherLeavesNature.Where(x => !excludedNature.Contains(x.DESCRIPTION) && x.ITEMID == 1).Select(x => new string(x.DESCRIPTION.ToArray())).ToList();
                        if (leaveOfAbsenceNatures.Contains(isEmployeeOnLeave.Nature))
                        {
                            item.leaveType = "Other Leave";
                        }
                        else
                        {
                            item.leaveType = isEmployeeOnLeave.Nature;
                        }
                    }
                    var isEmployeeHasPendingLeave = DbContext.E_GetPendingAbsence(item.employeeId).FirstOrDefault();
                    if (isEmployeeHasPendingLeave != null)
                    {
                        item.Pendingleave = isEmployeeHasPendingLeave.Nature;
                    }

                    var empTrainingCount = (from EET in DbContext.E_EmployeeTrainings
                                            join ES in DbContext.E_EmployeeTrainingStatus on EET.Status equals ES.StatusId
                                            join E in DbContext.E__EMPLOYEE on EET.EmployeeId equals E.EMPLOYEEID
                                            join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID
                                            join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                                            from T in T_join.DefaultIfEmpty()
                                            join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                                            from D in D_join.DefaultIfEmpty()
                                            where
                                               ((EET.ProfessionalQualification == null || EET.ProfessionalQualification == 0) && EET.TotalCost <= 1000)
                                                && EET.Active == true
                                                && J.ACTIVE == 1
                                                && (EET.EmployeeId == item.employeeId)
                                                && (ES.Title == ApplicationConstants.grpTrainingStatusRequested) // grpTrainingStatusSubmitted
                                            select EET);

                    if (empTrainingCount != null)
                    {
                        item.trainingApproval = empTrainingCount.Count();
                    }
                }

                response.employeeList = finalData;

                #region Other Staff Member 

                var myTeamHierarchyList = new List<MyStaffDetail>();
                var isCEO = false;
                isCEO = (from E in DbContext.E__EMPLOYEE
                         join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                         from J in J_join.DefaultIfEmpty()
                         where E.EMPLOYEEID == model.employeeId && J.JOBTITLE == "Group Chief Executive"
                         select E.EMPLOYEEID).Any();

                if (!isCEO)
                {
                    var myStaffHierarchy = DbContext.FN_GetTeamHierarchyForEmployeeSickness(model.employeeId).Where(e => e.LineManagerId != model.employeeId).Select(e => e.LineManagerId).Distinct().ToList();
                    var currentDate = GeneralHelper.GetDateTimeFromString(DateTime.Now.ToShortDateString());
                    if (myStaffHierarchy != null && myStaffHierarchy.Count > 0)
                    {
                        foreach (var myStaff in myStaffHierarchy)
                        {
                            // check line manager in my staff list is on leave
                            var isEmployeeApplyLeave = DbContext.E_AnnualLeaveCount(myStaff.Value);
                            if (isEmployeeApplyLeave.Any())
                            {
                                var myStaffData = (from E in DbContext.E__EMPLOYEE
                                                   join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                                                   from J in J_join.DefaultIfEmpty()
                                                   join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                                                   from T in T_join.DefaultIfEmpty()
                                                   join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                                                   from G in G_join.DefaultIfEmpty()
                                                   join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                                                   from ET in ET_join.DefaultIfEmpty()
                                                   join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                                                   from ETH in ETH_join.DefaultIfEmpty()
                                                   join DOI in DbContext.E_DOI on E.EMPLOYEEID equals DOI.EmployeeId into DOI_join
                                                   from DOI in DOI_join.Where(e => e.FiscalYear == fData.YRange).DefaultIfEmpty()
                                                   join DOIs in DbContext.E_DOI_Status on DOI.Status equals DOIs.DOIStatusId into DOIs_join
                                                   from DOIs in DOIs_join.DefaultIfEmpty()
                                                   join Gift in DbContext.E_GiftsAndHospitalities on E.EMPLOYEEID equals Gift.EmployeeId into gift_join
                                                   from Gift in gift_join.Where(e => e.GiftStatusId == 4).DefaultIfEmpty()
                                                   join gStatus in DbContext.E_Gift_Status on Gift.GiftStatusId equals gStatus.GiftStatusId into gStatus_join
                                                   from gStatus in gStatus_join.DefaultIfEmpty()
                                                   where J.LINEMANAGER == myStaff.Value && J.ACTIVE == 1
                                                   select new MyStaffDetail
                                                   {
                                                       employeeId = E.EMPLOYEEID,
                                                       fullName = E.FIRSTNAME + " " + E.LASTNAME,
                                                       grade = G.DESCRIPTION,
                                                       jobTitle = ET.JobeRoleDescription,
                                                       paypointReviewDate = J.PayPointReviewDate,
                                                       appraisalDateTime = J.APPRAISALDATE,
                                                       doiStatus = DOIs.Description,
                                                       giftStatus = gStatus.Description

                                                   }).Distinct().ToList();

                                foreach (var item in myStaffData)
                                {
                                    var employeeOtherLeaves = DbContext.E_GetPendingAbsence(item.employeeId).FirstOrDefault();

                                    if (employeeOtherLeaves != null)
                                    {
                                        if (item.paypointReviewDate.HasValue)
                                        {
                                            DateTime paypointReviewDate = (DateTime)item.paypointReviewDate;
                                            item.paypointReview = paypointReviewDate.ToString("dd/MM/yyyy");
                                        }
                                        if (item.appraisalDateTime.HasValue)
                                        {
                                            DateTime appraisalDateTime = (DateTime)item.appraisalDateTime;
                                            item.appraisal = appraisalDateTime.ToString("dd/MM/yyyy");
                                        }
                                        //Get the Annual leave and remaining leaves w.r.t carry forword and Toil
                                        var leavedata = (from stats in DbContext.E_LeaveStats(item.employeeId, null, true,null) select stats).FirstOrDefault();
                                        if (leavedata != null)
                                        {
                                            var annualLeaveData = (from absence in DbContext.E_AbsenseAnnual(item.employeeId, null)
                                                                   select new AbsenceAnnual()
                                                                   {
                                                                       absenceHistoryId = absence.ABSENCEHISTORYID,
                                                                       endDate = absence.RETURNDATE,
                                                                       status = absence.Status,
                                                                       absentDays = (double)absence.DAYS_ABS,
                                                                       nature = absence.Nature,
                                                                       startDate = absence.STARTDATE,
                                                                       holidayType = absence.HolType,
                                                                       lastActionDatetime = absence.LastActionDate,
                                                                       unit = leavedata.Unit,
                                                                       durationType = absence.DurationType
                                                                   }).ToList();
                                            var EndDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.employeeId)
                                                           select new AbsenceAnnual()
                                                           {
                                                               endDate = absence.ENDDATE
                                                           }).FirstOrDefault().endDate;
                                            var StartDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.employeeId)
                                                             select new AbsenceAnnual()
                                                             {
                                                                 startDate = absence.STARTDATE
                                                             }).FirstOrDefault().startDate;
                                            double LeavesRequested = 0;
                                            foreach (var al in annualLeaveData)
                                            {
                                                if (al.endDate <= EndDate)
                                                {
                                                    if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                                    {
                                                        LeavesRequested = LeavesRequested + double.Parse(al.absentDays.ToString());
                                                    }
                                                }
                                                else if (al.endDate > EndDate && al.startDate <= EndDate)
                                                {
                                                    if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                                    {
                                                        double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                                                        LeavesRequested = LeavesRequested + LeaveDaysCount;
                                                    }

                                                }
                                            }
                                            var totalBankHolidays = (from e in DbContext.E_BOOKED_BANK_HOLIDAY(item.employeeId, StartDate)
                                                                     select e.Value).FirstOrDefault();
                                            totalBankHolidays = totalBankHolidays - Decimal.Parse(leavedata.BNK_HD.ToString());
                                            leavedata.LEAVE_AVAILABLE = leavedata.ANNUAL_LEAVE_DAYS - LeavesRequested;

                                            item.remainingLeave = Convert.ToDecimal(leavedata.LEAVE_AVAILABLE + leavedata.CARRY_FWD) - totalBankHolidays;
                                            item.annualLeave = Convert.ToDecimal(leavedata.ANNUAL_LEAVE_DAYS);
                                        }
                                        var isEmployeeOnLeave = DbContext.E_AnnualLeaveCount(item.employeeId).FirstOrDefault();
                                        if (isEmployeeOnLeave != null)
                                        {
                                            item.leaveType = isEmployeeOnLeave.Nature;
                                        }

                                        item.Pendingleave = employeeOtherLeaves.Nature;
                                        myTeamHierarchyList.Add(item);

                                    }
                                }
                            }
                        }
                    }
                }


                response.myTeamHierarchyList = myTeamHierarchyList;

                #endregion

            }

            return response;
        }

        #endregion

        #region Get My Staff List
        public List<MyStaffDetail> GetMyStaffList(int? employeeId)
        {
            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                        from ETH in ETH_join.DefaultIfEmpty()
                        where J.LINEMANAGER == employeeId && J.ACTIVE == 1
                        select new MyStaffDetail
                        {
                            employeeId = E.EMPLOYEEID,
                            fullName = E.FIRSTNAME + " " + E.LASTNAME,
                            grade = G.DESCRIPTION,
                            jobTitle = ET.JobeRoleDescription,
                            paypointReviewDate = J.PayPointReviewDate,
                            appraisalDateTime = J.APPRAISALDATE

                        });

            return data.ToList();
        }
        #endregion

        #region Get  Team List for line manager
        public MyTeamListing PopulateMyTeamList(CommonRequest model)
        {
            MyTeamListing response = new MyTeamListing();

            //LineManagerIDs including the Employee and Employees directly under her/him
            var LineManagerIDs = (from staff in DbContext.FN_GetTeamHierarchyForEmployeeSickness(model.employeeId)
                                  select staff.LineManagerId).ToList().GroupBy(x => x.Value).Select(y => y.Key).ToList();
            //LineManagers' Data
            List<E_GetMyTeamList_Result> data = new List<E_GetMyTeamList_Result>();
            foreach (int LMID in LineManagerIDs)
            {
                var tempData = (from e in DbContext.E_GetMyTeamList(LMID)
                                select e).ToList();
                foreach (var td in tempData)
                {
                    data.Add(td);
                }
            }

            if (data.Count() > 0)
            {
                data = data.OrderBy(x => x.TEAMID).ToList();
                //Grouping Data on the base of Team Data
                var data1 = data.GroupBy(x => x.TEAMID).Select(grp => grp.ToList()).ToList();

                var TeamIDs = data.GroupBy(x => x.TEAMID).Select(y => y.Key).ToList();
                List<E_GetMyTeamsEmployeeList_Result> dataEmpList = new List<E_GetMyTeamsEmployeeList_Result>();
                List<MyTeamEmployeeDetail> empList = new List<MyTeamEmployeeDetail>();
                foreach (int tId in TeamIDs)
                {
                    foreach (int LMID in LineManagerIDs)
                    {
                        var tempData = (from e in DbContext.E_GetMyTeamsEmployeeList(LMID, tId)
                                        select e).ToList();
                        foreach (var td in tempData)
                        {
                            dataEmpList.Add(td);
                        }
                    }
                }
                if (dataEmpList.Count > 0)
                {
                    dataEmpList = dataEmpList.OrderBy(x => x.EMPLOYEEID).ToList();
                    dataEmpList = dataEmpList.GroupBy(x => x.EMPLOYEEID).Select(g => g.FirstOrDefault()).ToList();
                    foreach (var item in dataEmpList)
                    {
                        MyTeamEmployeeDetail emp = new MyTeamEmployeeDetail()
                        {
                            employeeId = item.EMPLOYEEID,
                            holRequest = item.HOLREQUEST,
                            teamName = item.TEAMNAME,
                        };

                        var leavedata = (from stats in DbContext.E_LeaveStats(item.EMPLOYEEID, null, true,null) select stats).FirstOrDefault();
                        if (leavedata != null)
                        {
                            var annualLeaveData = (from absence in DbContext.E_AbsenseAnnual(item.EMPLOYEEID, null)
                                                   select new AbsenceAnnual()
                                                   {
                                                       endDate = absence.RETURNDATE,
                                                       status = absence.Status,
                                                       absentDays = (double)absence.DAYS_ABS,
                                                       startDate = absence.STARTDATE,
                                                       unit = leavedata.Unit
                                                   }).ToList();
                            var EndDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.EMPLOYEEID)
                                           select new AbsenceAnnual()
                                           {
                                               endDate = absence.ENDDATE
                                           }).FirstOrDefault().endDate;
                            var StartDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.EMPLOYEEID)
                                             select new AbsenceAnnual()
                                             {
                                                 startDate = absence.STARTDATE
                                             }).FirstOrDefault().startDate;
                            double LeavesRequested = 0;
                            foreach (var al in annualLeaveData)
                            {
                                if (al.endDate <= EndDate)
                                {
                                    if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                    {
                                        LeavesRequested = LeavesRequested + double.Parse(al.absentDays.ToString());
                                    }
                                }
                                else if (al.endDate > EndDate && al.startDate <= EndDate)
                                {
                                    if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                    {
                                        double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                                        LeavesRequested = LeavesRequested + LeaveDaysCount;
                                    }

                                }
                            }
                            var totalBankHolidays = (from e in DbContext.E_BOOKED_BANK_HOLIDAY(item.EMPLOYEEID, StartDate)
                                                     select e.Value).FirstOrDefault();
                            totalBankHolidays = totalBankHolidays - Decimal.Parse(leavedata.BNK_HD.ToString());
                            leavedata.LEAVE_AVAILABLE = leavedata.ANNUAL_LEAVE_DAYS - LeavesRequested;

                            var RemainingLeave = Convert.ToDecimal(leavedata.LEAVE_AVAILABLE + leavedata.CARRY_FWD) - totalBankHolidays;
                            if (leavedata.Unit.Equals("days"))
                            {
                                emp.remainingLeaveDays = Double.Parse(RemainingLeave.ToString());
                                emp.remainingLeaveHRS = 0;
                            }
                            else
                            {
                                emp.remainingLeaveHRS = Double.Parse(RemainingLeave.ToString());
                                emp.remainingLeaveDays = 0;
                            }

                        }
                        else
                        {
                            emp.remainingLeaveDays = 0;
                            emp.remainingLeaveHRS = 0;
                        }
                        empList.Add(emp);
                    }
                }

                List<MyTeamDetail> FinalData = new List<MyTeamDetail>();

                var TeamNameGroups = empList.GroupBy(x => x.teamName).Select(grp => grp.ToList()).ToList();
                var TeamResults = new List<MyTeamDetail>();
                foreach (var temp in TeamNameGroups)
                {
                    MyTeamDetail obj = new MyTeamDetail();
                    obj.leaveDays = temp.Sum(x => x.remainingLeaveDays).ToString();
                    obj.leaveHRS = temp.Sum(x => x.remainingLeaveHRS).ToString();
                    obj.noOfEmployee = temp.Count.ToString();
                    obj.teamName = temp.FirstOrDefault().teamName;
                    TeamResults.Add(obj);
                }
                // Performing final Calculations
                foreach (var d in data1)
                {
                    MyTeamDetail obj = new MyTeamDetail();
                    obj.salary = "0";

                    foreach (var ed in d)
                    {
                        obj.teamName = ed.TEAMNAME;
                        obj.teamId = ed.TEAMID;
                        obj.salary = (double.Parse(obj.salary) + double.Parse(ed.SALARY)).ToString();
                        obj.sicknessAbsence = obj.sicknessAbsence + ed.SICKNESSABSENCE;
                    }
                    FinalData.Add(obj);

                }

                Pagination page = new Pagination();

                FinalData = FinalData.OrderBy(x => x.teamName).ToList();
                TeamResults = TeamResults.OrderBy(x => x.teamName).ToList();

                for (int i = 0; i < FinalData.Count; i++)
                {
                    FinalData[i].leaveDays = TeamResults[i].leaveDays;
                    FinalData[i].leaveHRS = TeamResults[i].leaveHRS;
                    FinalData[i].noOfEmployee = TeamResults[i].noOfEmployee;
                }
                page.totalRows = FinalData.Count();
                page.totalPages = (int)Math.Ceiling((double)FinalData.Count / model.pagination.pageSize);
                page.pageNo = model.pagination.pageNo;
                page.pageSize = model.pagination.pageSize;
                int _page = (page.pageNo - 1);

                response.pagination = page;

                // Counting skip
                int _skip = _page * page.pageSize;
                var finalData = FinalData;

                string _sortBy = @"";
                if (model.sortBy == null)
                {
                    _sortBy = @"teamName";
                }
                else
                {
                    _sortBy = model.sortBy;
                }
                // Getting sorted by sort parameter
                if (model.sortOrder == "ASC")
                {
                    finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                else
                {
                    finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                response.teamList = finalData;
            }
            return response;

        }
        #endregion

        #region Get Team's Employee List
        public MyTeamEmployeeListing GetEmployeeByTeam(MyTeamsEmployeeRequest model)
        {
            MyTeamEmployeeListing response = new MyTeamEmployeeListing();

            ///LineManagerIDs including the Employee and Employees directly under her/him
            var LineManagerIDs = (from staff in DbContext.FN_GetTeamHierarchyForEmployeeSickness(model.employeeId)
                                  select staff.LineManagerId).ToList().GroupBy(x => x.Value).Select(y => y.Key).ToList();
            // Getting Employees' Data
            List<E_GetMyTeamsEmployeeList_Result> data = new List<E_GetMyTeamsEmployeeList_Result>();
            foreach (int LMID in LineManagerIDs)
            {
                var tempData = (from e in DbContext.E_GetMyTeamsEmployeeList(LMID, model.teamId)
                                select e).ToList();
                foreach (var td in tempData)
                {
                    data.Add(td);
                }
            }
            
            if (data.Count() > 0)
            {
                data = data.GroupBy(x => x.EMPLOYEEID).Select(g => g.FirstOrDefault()).ToList();
                List<MyTeamEmployeeDetail> empList = new List<MyTeamEmployeeDetail>();
                foreach (var item in data)
                {
                    MyTeamEmployeeDetail emp = new MyTeamEmployeeDetail()
                    {
                        appraisalDate = item.APPRAISALDATE,
                        employeeId = item.EMPLOYEEID,
                        fullName = item.FULLNAME,
                        grade = item.GRADE,
                        holRequest = item.HOLREQUEST,
                        jobTitle = item.JOBTITLE,
                        remainingLeave = item.REMAININGLEAVE,
                        teamName = item.TEAMNAME,
                    };

                    var leavedata = (from stats in DbContext.E_LeaveStats(item.EMPLOYEEID, null, true,null) select stats).FirstOrDefault();
                    if (leavedata != null)
                    {
                        var annualLeaveData = (from absence in DbContext.E_AbsenseAnnual(item.EMPLOYEEID, null)
                                               select new AbsenceAnnual()
                                               {
                                                   endDate = absence.RETURNDATE,
                                                   status = absence.Status,
                                                   absentDays = (double)absence.DAYS_ABS,
                                                   startDate = absence.STARTDATE
                                               }).ToList();
                        var EndDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.EMPLOYEEID)
                                       select new AbsenceAnnual()
                                       {
                                           endDate = absence.ENDDATE
                                       }).FirstOrDefault().endDate;
                        var StartDate = (from absence in DbContext.EMPLOYEE_ANNUAL_START_END_DATE(item.EMPLOYEEID)
                                         select new AbsenceAnnual()
                                         {
                                             startDate = absence.STARTDATE
                                         }).FirstOrDefault().startDate;
                        double LeavesRequested = 0;
                        foreach (var al in annualLeaveData)
                        {
                            if (al.endDate <= EndDate)
                            {
                                if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                {
                                    LeavesRequested = LeavesRequested + double.Parse(al.absentDays.ToString());
                                }
                            }
                            else if (al.endDate > EndDate && al.startDate <= EndDate)
                            {
                                if (al.status.Equals("Pending") || al.status.Equals("Approved"))
                                {
                                    double LeaveDaysCount = (EndDate - al.startDate).Value.TotalDays + 1;
                                    LeavesRequested = LeavesRequested + LeaveDaysCount;
                                }

                            }
                        }
                        var totalBankHolidays = (from e in DbContext.E_BOOKED_BANK_HOLIDAY(item.EMPLOYEEID, StartDate)
                                                 select e.Value).FirstOrDefault();
                        totalBankHolidays = totalBankHolidays - Decimal.Parse(leavedata.BNK_HD.ToString());
                        leavedata.LEAVE_AVAILABLE = leavedata.ANNUAL_LEAVE_DAYS - LeavesRequested;

                        var RemainingLeave = Convert.ToDecimal(leavedata.LEAVE_AVAILABLE + leavedata.CARRY_FWD) - totalBankHolidays;
                        emp.remainingLeave = Double.Parse(RemainingLeave.ToString());
                    }
                    empList.Add(emp);
                }

                Pagination page = new Pagination();
                page.totalRows = empList.Count();
                page.totalPages = (int)Math.Ceiling((double)empList.Count / model.pagination.pageSize);
                page.pageNo = model.pagination.pageNo;
                page.pageSize = model.pagination.pageSize;
                int _page = (page.pageNo - 1);

                response.pagination = page;

                // Counting skip
                int _skip = _page * page.pageSize;
                var finalData = empList;
                string _sortBy = @"";
                if (model.sortBy == null)
                {
                    _sortBy = @"teamName";
                }
                else
                {
                    _sortBy = model.sortBy;
                }
                // Getting sorted by sort parameter
                if (model.sortOrder == "ASC")
                {
                    finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                else
                {
                    finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                
                response.employeeList = finalData;
            }


            return response;
        }
        #endregion

        #region Populate Remuneration Statement
        public RemunerationStatementsListing PopulateRemunerationStatement(CommonReportsFilterRequest model)
        {
            RemunerationStatementsListing response = new RemunerationStatementsListing();

            var data = (from E in DbContext.E__EMPLOYEE
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        join T in DbContext.E_TEAM on new { TEAMID = (int)J.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join G in DbContext.E_GRADE on new { GRADE = (int)J.GRADE } equals new { GRADE = G.GRADEID } into G_join
                        from G in G_join.DefaultIfEmpty()
                        join ET in DbContext.E_JOBROLE on new { JobRoleId = (int)J.JobRoleId } equals new { JobRoleId = ET.JobRoleId } into ET_join
                        from ET in ET_join.DefaultIfEmpty()
                        join ETH in DbContext.G_ETHNICITY on E.ETHNICITY equals ETH.ETHID into ETH_join
                        from ETH in ETH_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()

                        where
                          // J.ACTIVE == model.status &&
                          (T.DIRECTORATEID == model.directorate || model.directorate == null) &&
                         (J.TEAM == model.team || model.team == null) &&
                          ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                        select new RemunerationStatementsDetail
                        {
                            fullName = (E.FIRSTNAME + " " + E.LASTNAME),
                            team = (T.TEAMNAME ?? "No Team"),
                            directorate = (D.DIRECTORATENAME ?? "No Directorate"),
                            employeeId = E.EMPLOYEEID,
                            jobTitle = (ET.JobeRoleDescription ?? "N/A"),
                            grade = (G.DESCRIPTION ?? "-"),
                            dobString = E.DOB,
                            endDateString = J.ENDDATE,
                            ethnicity = (ETH.DESCRIPTION ?? "-"),
                            gender = E.GENDER,
                            salary = J.SALARY,
                            startDateString = J.STARTDATE,
                            isDisciplinary = false,
                            status = J.ACTIVE

                        });

            if (model.employeeType == "Previous")
            {
                data = data.Where(J => J.endDateString != null || J.status == 0);
            }
            else if (model.employeeType == "Current")
            {
                data = data.Where(J => J.endDateString == null || J.status == 1);
            }
            if (model.fromDate != null && model.toDate != null && model.fromDate != "" && model.toDate != "")
            {
                DateTime? fromDate = Convert.ToDateTime(model.fromDate);
                DateTime? toDate = Convert.ToDateTime(model.toDate);
                data = data.Where(J => J.startDateString >= fromDate && J.endDateString <= toDate);
            }
            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"fullName";
            }
            else
            {
                _sortBy = model.sortBy;
            }
            if (model.sortBy == "startDate")
                _sortBy = "startDateString";
            else if (model.sortBy == "endDate")
                _sortBy = "endDateString";
            else if (model.sortBy == "dob")
                _sortBy = "dobString";
            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            foreach (var item in finalData)
            {
                item.isSickness = IsSickness(item.employeeId);
                if (item.startDateString.HasValue)
                {
                    DateTime startDate = (DateTime)item.startDateString;
                    item.startDate = startDate.ToString("dd/MM/yyyy");
                }
                if (item.endDateString.HasValue)
                {
                    DateTime endDate = (DateTime)item.endDateString;
                    item.endDate = endDate.ToString("dd/MM/yyyy");
                }
                if (item.dobString.HasValue)
                {
                    DateTime dobString = (DateTime)item.dobString;
                    item.dob = dobString.ToString("dd/MM/yyyy");
                }
            }
            response.employeeList = finalData;
            response.totalSalary = CalculateTotalSalary();
            response.totalStaff = CalculateTotalStaff();
            response.newStarter = GetNewStarter();
            response.leaver = Getleaver();

            return response;
        }
        #endregion

        #region Remuneration Statement document
        public RemunerationStatementDocument GetRemunerationStatementDocument(int employeeId)
        {
            RemunerationStatementDocument response = new RemunerationStatementDocument();
            var data = (from S in DbContext.E_BENEFITS
                        join E in DbContext.E__EMPLOYEE on S.EMPLOYEEID equals E.EMPLOYEEID
                        join J in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals J.EMPLOYEEID into J_join
                        from J in J_join.DefaultIfEmpty()
                        where S.EMPLOYEEID == employeeId
                        select new RemunerationStatementDocument
                        {
                            fullName = E.FIRSTNAME + " " + E.LASTNAME,
                            employeeId = S.EMPLOYEEID,
                            //////////// Essential User////////////////// 
                            carAllowance = (S.CARALLOWANCE == null ? 0 : S.CARALLOWANCE),
                            lifeAssurance = (S.LifeAssurance == null ? 0 : S.LifeAssurance),
                            enhancedHolidayEntitlement = (S.EnhancedHolidayEntitlement == null ? 0 : S.EnhancedHolidayEntitlement),
                            carParkingFacilities = (S.CarParkingFacilities == null ? 0 : S.CarParkingFacilities),
                            eyeCareAssistance = (S.EyeCareAssistance == null ? 0 : S.EyeCareAssistance),
                            employeeAssistanceProgramme = S.EmployeeAssistanceProgramme,
                            enhancedSickPay = (S.EnhancedSickPay == null ? 0 : S.EnhancedSickPay),
                            learningAndDevelopment = (S.LearningAndDevelopment == null ? 0 : S.LearningAndDevelopment),
                            professionalSubscriptions = (S.ProfessionalSubscriptions == null ? 0 : S.ProfessionalSubscriptions),
                            childcareVouchers = (S.ChildcareVouchers == null ? 0 : S.ChildcareVouchers),
                            fluAndHepBJabs = (S.FluAndHepBJabs == null ? 0 : S.FluAndHepBJabs),
                            /////////////////////// Pension/////////
                            employerContribution = (S.EMPLOYERCONTRIBUTION == null ? 0 : S.EMPLOYERCONTRIBUTION),
                            salary = (J.SALARY == null ? 0 : J.SALARY),
                            phcm = 0,
                            phi = 0


                        });
            if (data.Count() > 0)
            {
                response = data.FirstOrDefault();
                decimal employeeAssistanceProgramme = 0;
                if (response.employeeAssistanceProgramme != null && Decimal.TryParse(response.employeeAssistanceProgramme, out employeeAssistanceProgramme))
                {
                    employeeAssistanceProgramme = Convert.ToDecimal(response.employeeAssistanceProgramme);
                }
                response.totalRemunerationPackage = response.carAllowance + response.carParkingFacilities + response.childcareVouchers + response.employerContribution + response.eyeCareAssistance
                    + response.fluAndHepBJabs + response.learningAndDevelopment + response.lifeAssurance + response.professionalSubscriptions + response.salary + employeeAssistanceProgramme;
                var fiscalYear = getCurrentFiscalYear();
                if (fiscalYear != null)
                {
                    F_FISCALYEARS fy = fiscalYear;
                    response.fiscalStart = fy.YStart.Value.Year;
                    response.fiscalEnd = fy.YEnd.Value.Year;
                }
            }
            return response;
        }

        #endregion

        #region Maindatory Trainings
        public TrainingListingResponse GetMandatoryTrainings(CommonReportsFilterRequest model)
        {
            TrainingListingResponse response = new TrainingListingResponse();




            var data = (from T in DbContext.E_EmployeeTrainings
                        join E in DbContext.E__EMPLOYEE on T.EmployeeId equals E.EMPLOYEEID
                        join EE in DbContext.E__EMPLOYEE on T.Createdby equals EE.EMPLOYEEID
                        join S in DbContext.E_EmployeeTrainingStatus on T.Status equals S.StatusId
                        join j in DbContext.E_JOBDETAILS on E.EMPLOYEEID equals j.EMPLOYEEID
                        join t in DbContext.E_TEAM on j.TEAM equals t.TEAMID into t_join
                        from t in t_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on t.DIRECTORATEID equals D.DIRECTORATEID into d_join
                        from D in d_join.DefaultIfEmpty()

                        where T.IsMandatoryTraining == true && T.Active == true
                        && ((E.FIRSTNAME + " " + E.LASTNAME).Contains(model.searchText))
                        && (D.DIRECTORATEID == model.directorate || model.directorate == null)
                        && (t.TEAMID == model.team || model.team == null)
                        && T.Expiry > DateTime.Now
                        select new AddEmployeeTrainings
                        {
                            employeeId = T.EmployeeId,
                            employeeName = E.FIRSTNAME + " " + E.LASTNAME,
                            additionalNotes = T.AdditionalNotes,
                            course = T.Course,
                            startDateTime = T.StartDate,
                            endDateTime = T.EndDate,
                            expiryDate = T.Expiry,
                            isMandatoryTraining = T.IsMandatoryTraining,
                            isSubmittedBy = T.IsSubmittedBy,
                            justification = T.Justification,
                            location = T.Location,
                            endDateString = j.ENDDATE,
                            providerName = T.ProviderName,
                            providerWebsite = T.ProviderWebsite,
                            totalCost = T.TotalCost,
                            jobStatus = j.ACTIVE,
                            totalNumberOfDays = T.TotalNumberOfDays,
                            trainingId = T.TrainingId,
                            venue = T.Venue,
                            status = S.Title,
                            jobTitle = j.JOBTITLE,
                            directorateName = D.DIRECTORATENAME,
                            teamName = t.TEAMNAME,
                            byName = EE.FIRSTNAME + " " + EE.LASTNAME
                        });
            data = data.OrderBy(x => x.expiryDate);
            if (model.employeeType == "Previous")
            {
                data = data.Where(J => J.endDateString != null && J.jobStatus == 0);
            }
            else if (model.employeeType == "Current")
            {
                data = data.Where(J => J.endDateString == null && J.jobStatus == 1);
            }
            if (model.fromDate != null && model.toDate != null && model.fromDate != "" && model.toDate != "")
            {
                DateTime? fromDate = Convert.ToDateTime(model.fromDate);
                DateTime? toDate = Convert.ToDateTime(model.toDate);
                data = data.Where(J => J.expiryDate >= fromDate && J.expiryDate <= toDate);
            }
            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
            page.pageNo = model.pagination.pageNo;
            page.pageSize = model.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (model.sortBy == null)
            {
                _sortBy = @"expiryDate";
            }
            else
            {
                _sortBy = model.sortBy;
            }

            foreach (AddEmployeeTrainings item in finalData)
            {
                DateTime startDate = Convert.ToDateTime(item.startDateTime);
                item.startDate = startDate.ToString("dd/MM/yyyy");

                DateTime endDate = Convert.ToDateTime(item.endDateTime);
                item.endDate = endDate.ToString("dd/MM/yyyy");

                DateTime expiryDate = Convert.ToDateTime(item.expiryDate);
                item.expiry = expiryDate.ToString("dd/MM/yyyy");
                var profQualList = (from S in DbContext.E_EmployeeApprovedTrainings
                                    where (S.TrainingId == item.trainingId)
                                    select new ApprovedTraining
                                    {
                                        approvedtraining = S.Approvedtraining,
                                        isRemuneration = S.IsRemuneration,
                                        remunerationCost = S.RemunerationCost
                                    }).ToList();
                if (profQualList.Count() > 0)
                {
                    item.isRemuneration = profQualList.FirstOrDefault()?.isRemuneration;
                    item.remunerationCost = profQualList.FirstOrDefault()?.remunerationCost;
                    item.approvedtraining = profQualList.FirstOrDefault()?.approvedtraining;
                }
            }

            // Getting sorted by sort parameter
            if (model.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
            }

            response.trainingList = finalData;
            return response;
        }

        #endregion

        public HealthReportListing GetHealthList(CommonRequest model)
        {
            HealthReportListing response = new HealthReportListing();
            var fiscalYear = getCurrentFiscalYear();
            var data = (from d in DbContext.E_DIFFDISID
                        join dh in DbContext.E_DIFFDISID_HISTORY on d.DISABILITYID equals dh.DISABILITYID
                        join e in DbContext.E__EMPLOYEE on d.EMPLOYEEID equals e.EMPLOYEEID
                        join t in DbContext.E_DIFFDIS on d.DIFFDIS equals t.DIFFDISID
                        join log in DbContext.E__EMPLOYEE on d.LASTACTIONUSER equals log.EMPLOYEEID
                        join j in DbContext.E_JOBDETAILS on e.EMPLOYEEID equals j.EMPLOYEEID

                        join T in DbContext.E_TEAM on new { TEAMID = (int)j.TEAM } equals new { TEAMID = T.TEAMID } into T_join
                        from T in T_join.DefaultIfEmpty()
                        join D in DbContext.E_DIRECTORATES on T.DIRECTORATEID equals D.DIRECTORATEID into D_join
                        from D in D_join.DefaultIfEmpty()

                        where (T.DIRECTORATEID == model.directorate || model.directorate == null) &&
                         (j.TEAM == model.teamId || model.teamId == null) &&

                        ((e.FIRSTNAME + " " + e.LASTNAME).Contains(model.searchText))
                        select new HealthReportDetail
                        {
                            description = t.DESCRIPTION,
                            disablityId = d.DIFFDIS,
                            notes = d.NOTES,
                            notifiedDateString = d.NotifiedDate,
                            lastActionTime = d.LASTACTIONTIME,
                            firstName = log.FIRSTNAME,
                            lastName = log.LASTNAME,
                            diffDisabilityId = d.DISABILITYID,
                            loggedBy = d.LASTACTIONUSER,
                            disabilityTypeId = t.DTYPE,
                            fullName = e.FIRSTNAME + " " + e.LASTNAME,
                            employeeId = e.EMPLOYEEID,
                            team = (T.TEAMNAME ?? "No Team"),
                            directorate = (D.DIRECTORATENAME ?? "No Directorate")
                        });
            if (data.Count() > 0)
            {
                if (!string.IsNullOrEmpty(model.disabilityType))
                {
                    if (model.disabilityType == "1")
                    {
                        data = data.Where(J => J.disabilityTypeId == 1);
                    }
                    else if (model.disabilityType == "2")
                    {
                        data = data.Where(J => J.disabilityTypeId == 2);
                    }
                }

                if (model.fromDate != "" && model.toDate != "" && model.fromDate != null && model.toDate != null)
                {
                    DateTime? fromDate = Convert.ToDateTime(model.fromDate);
                    DateTime? toDate = Convert.ToDateTime(model.toDate);
                    data = data.Where(J => (J.lastActionTime >= fromDate && J.lastActionTime <= toDate));
                }
                else
                {
                    if (fiscalYear != null)
                    {
                        F_FISCALYEARS fy = fiscalYear;
                        DateTime? fromDate = fy.YStart;
                        DateTime? toDate = DateTime.Now;
                        data = data.Where(J => (J.lastActionTime >= fromDate && J.lastActionTime <= toDate));
                    }
                }
                Pagination page = new Pagination();
                page.totalRows = data.ToList().Count();
                page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / model.pagination.pageSize);
                page.pageNo = model.pagination.pageNo;
                page.pageSize = model.pagination.pageSize;
                int _page = (page.pageNo - 1);

                response.pagination = page;

                if (fiscalYear != null)
                {
                    F_FISCALYEARS fy = fiscalYear;
                    response.fromDate = fy.YStart.Value;
                    response.toDate = DateTime.Now;
                }
                // Counting skip
                int _skip = _page * page.pageSize;
                var finalData = data.ToList();

                string _sortBy = @"";
                if (model.sortBy == null)
                {
                    _sortBy = @"disablityId";
                }
                else
                {
                    _sortBy = model.sortBy;
                }
                // Getting sorted by sort parameter
                if (model.sortOrder == "ASC")
                {
                    finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                else
                {
                    finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                foreach (HealthReportDetail item in finalData)
                {
                    DateTime logdate = (DateTime)item.lastActionTime;
                    if (item.notifiedDateString.HasValue)
                    {
                        DateTime notifiedDate = (DateTime)item.notifiedDateString;
                        item.notifiedDate = notifiedDate.ToString("dd/MM/yyyy");
                    }
                    if (item.disabilityTypeId == 1)
                    {
                        item.disabilityType = "Disability or Health Problem";
                    }
                    else if (item.disabilityTypeId == 2)
                    {
                        item.disabilityType = "Learning Difficulties";
                    }
                    item.loggedByString = logdate.ToString("dd/MM/yyyy") + "( " + item.firstName[0] + " " + item.lastName + ")";
                }
                response.employeeList = finalData;
            }
            return response;
        }


        public EmployeeJournalResponse GETEmployeeJournal(JournalRequest model)
        {
            EmployeeJournalResponse response = new EmployeeJournalResponse();
            var data = (from e in DbContext.E__EmployeeJournal(model.employeeId)
                        select e).ToList();
            if (data.Count() > 0)
            {
                if (model.fromDate != null && model.toDate != null && model.fromDate != "" && model.toDate != "")
                {

                    DateTime? fromDate = Convert.ToDateTime(model.fromDate).Date;
                    DateTime? toDate = Convert.ToDateTime(model.toDate).Date;
                    data = data.Where(J => J.LastActionDateString?.Date >= fromDate && J.LastActionDateString?.Date <= toDate).ToList();
                }

                if (model.itemType != null && model.itemType != string.Empty)
                {
                    data = data.Where(J => J.ITEM == model.itemType).ToList();
                }
                if (model.searchText != null && model.searchText != string.Empty)
                {
                    data = data.Where(J => J.NATURE.Contains(model.searchText)).ToList();
                }

                List<EmployeeJournalList> empList = new List<EmployeeJournalList>();

                foreach (var item in data)
                {
                    EmployeeJournalList emp = new EmployeeJournalList()
                    {
                        creationDate = item.CreationDate,
                        lastActionDate = item.LastActionDate,
                        employeeId = item.employeeId,
                        item = item.ITEM,
                        nature = item.NATURE,
                        notes = item.NOTES,
                        redirectTab = item.REDIR,
                        status = item.STATUS,
                        title = item.TITLE,
                        creationDateString = item.CreationDateString,
                        lastActionDateString = item.LastActionDateString,
                        documentPath = item.DocumentPath,
                        doiStatus = item.DoiStatus,
                        itemId = item.ItemId
                    };

                    if (!string.IsNullOrEmpty(emp.documentPath) && emp.employeeId.HasValue)
                    {
                        emp.documentPath = FileHelper.getLogicalDocumentPath(emp.documentPath, emp.employeeId.Value);
                    }

                    empList.Add(emp);
                }

                Pagination page = new Pagination();
                page.totalRows = empList.Count();
                page.totalPages = (int)Math.Ceiling((double)empList.Count / model.pagination.pageSize);
                page.pageNo = model.pagination.pageNo;
                page.pageSize = model.pagination.pageSize;
                int _page = (page.pageNo - 1);

                response.pagination = page;

                // Counting skip
                int _skip = _page * page.pageSize;
                var finalData = empList;

                string _sortBy = @"";
                if (model.sortBy == null)
                {
                    _sortBy = @"lastActionDateString";
                }
                else
                {
                    _sortBy = model.sortBy;
                }
                if (model.sortBy == "lastActionDate")
                    _sortBy = "lastActionDateString";

                if (model.sortBy == "creationDate")
                    _sortBy = "creationDateString";

                // Getting sorted by sort parameter
                if (model.sortOrder == "ASC")
                {
                    finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                else
                {
                    finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(model.pagination.pageSize).ToList();
                }
                response.employeeList = finalData;
            }

            return response;
        }

        #region declaration of interest

        public DeclarationOfInterestsResponse GetDeclarationOfInterestsReport(DeclarationOfInterestsRequest request)
        {
            var fData = getCurrentFiscalYear();
            if (request.fiscalYearId == 0)
            {
                request.fiscalYearId = fData.YRange;
            }

            var pendingReviewStatus = DbContext.E_DOI_Status.Where(s => s.Description.Equals(ApplicationConstants.doiPendingReview)).Select(t => t.DOIStatusId).FirstOrDefault();

            DeclarationOfInterestsResponse response = new DeclarationOfInterestsResponse();
            response.declarationOfInterestsListing = new DeclarationOfInterestListing();
            var data = (from doi in DbContext.E_DOI
                        join emp in DbContext.E__EMPLOYEE on doi.EmployeeId equals emp.EMPLOYEEID
                        join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                        join lm in DbContext.E__EMPLOYEE on jd.LINEMANAGER equals lm.EMPLOYEEID
                        join jr in DbContext.E_JOBROLE on jd.JobRoleId equals jr.JobRoleId into jr_join
                        from jr in jr_join.DefaultIfEmpty()
                        join t in DbContext.E_TEAM on jd.TEAM equals t.TEAMID into t_join
                        from t in t_join.DefaultIfEmpty()


                        join d in DbContext.E_DIRECTORATES on t.DIRECTORATEID equals d.DIRECTORATEID into d_join
                        from d in d_join.DefaultIfEmpty()
                        join dir in DbContext.E__EMPLOYEE on t.DIRECTOR equals dir.EMPLOYEEID into dir_join
                        from dir in dir_join.DefaultIfEmpty()
                        join s in DbContext.E_DOI_Status on doi.Status equals s.DOIStatusId into s_join
                        from s in s_join.DefaultIfEmpty()
                        join doc in DbContext.HR_Documents on doi.DocumentId equals doc.DocumentId into doc_join
                        from doc in doc_join.DefaultIfEmpty()
                        where (
                                (emp.FIRSTNAME + " " + emp.LASTNAME).Contains(request.searchText)
                                && (doi.FiscalYear == request.fiscalYearId || request.fiscalYearId == 0)
                                && (doi.Status == request.status || request.status == 0)
                                && (t.DIRECTORATEID == request.directorate || request.directorate == null || request.directorate == 0)
                                && (jd.TEAM == request.teamId || request.teamId == null || request.teamId == 0)
                                && (lm.EMPLOYEEID == request.lineManagerId || request.lineManagerId == 0)
                                && (doi.Status != pendingReviewStatus)
                                )
                        select new DeclarationOfInterestsData()
                        {
                            interestId = doi.InterestId,
                            employeeId = emp.EMPLOYEEID,
                            employeeName = emp.FIRSTNAME + " " + emp.LASTNAME,
                            lineManagerId = lm.EMPLOYEEID,
                            lineManagerName = lm.FIRSTNAME + " " + lm.LASTNAME,
                            directorId = dir.EMPLOYEEID,
                            directorName = dir.FIRSTNAME + " " + dir.LASTNAME,
                            directorateId = d.DIRECTORATEID,
                            directorateName = (d.DIRECTORATENAME ?? "No Directorate"),
                            teamId = t.TEAMID,
                            teamName = (t.TEAMNAME ?? "No Team"),
                            jobRoleId = jr.JobRoleId,
                            jobRoleName = jr.JobeRoleDescription,
                            statusId = s.DOIStatusId,
                            statusName = s.Description,
                            submittedDate = doi.CreatedOn,
                            updatedDate = doi.UpdatedOn,
                            documentId = doi.DocumentId,
                            documentPath = doc.DocumentPath,
                            comment = doi.Comment,
                            reviewComments = doi.LineManagerComments,
                            reviewDate = doi.LineManagerReviewDate
                        }).ToList();


            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / request.pagination.pageSize);
            page.pageNo = request.pagination.pageNo;
            page.pageSize = request.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.declarationOfInterestsListing.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (request.sortBy == null)
            {
                _sortBy = @"employeeName";
            }
            else
            {
                _sortBy = request.sortBy;
            }
            // Getting sorted by sort parameter
            if (request.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
            }

            List<DeclarationOfInterestsData> doiList = new List<DeclarationOfInterestsData>();
            foreach (var item in finalData)
            {
                DeclarationOfInterestsData report = new DeclarationOfInterestsData();
                report.interestId = item.interestId;
                report.directorId = item.directorId;
                report.directorName = item.directorName;
                report.directorateId = item.directorateId;
                report.directorateName = item.directorateName;
                report.teamId = item.teamId;
                report.teamName = item.teamName;
                report.employeeName = item.employeeName;
                report.jobRoleId = item.jobRoleId;
                report.jobRoleName = item.jobRoleName;
                report.lineManagerId = item.lineManagerId;
                report.lineManagerName = item.lineManagerName;
                report.statusId = item.statusId;
                report.statusName = item.statusName;
                report.updatedDate = item.updatedDate;
                report.submittedDate = item.submittedDate;
                report.employeeId = item.employeeId;
                report.documentId = item.documentId;
                report.comment = item.comment;
                report.reviewComments = item.reviewComments;
                report.reviewDate = item.reviewDate;
                report.documentPath = FileHelper.getLogicalDocumentPath(item.documentPath, item.employeeId);

                doiList.Add(report);
            }
            response.declarationOfInterestsListing.DOIs = doiList;
            response.declarationOfInterestsListing.pagination = page;
            return response;
        }


        #endregion

        #region gifts

        public AnnualLeaveReportResponse GetEmployeesByDirectorate(int directorateId)
        {
            AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();

            var data = (from emp in DbContext.E__EMPLOYEE
                        join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                        join t in DbContext.E_TEAM on jd.TEAM equals t.TEAMID into t_join
                        from t in t_join.DefaultIfEmpty()
                        join d in DbContext.E_DIRECTORATES on t.DIRECTORATEID equals d.DIRECTORATEID into d_join
                        from d in d_join.DefaultIfEmpty()
                        join dir in DbContext.E__EMPLOYEE on t.DIRECTOR equals dir.EMPLOYEEID into dir_join
                        from dir in dir_join.DefaultIfEmpty()
                        where (
                                d.DIRECTORATEID == directorateId && jd.ACTIVE == 1 &&
                                (jd.TEAM != 1 || jd.TEAM == null)
                        )
                        select new LookUpResponseModel()
                        {
                            lookUpId = emp.EMPLOYEEID,
                            lookUpDescription = emp.FIRSTNAME + " " + emp.LASTNAME,
                        }).ToList();

            response.lookUps.employeeLookUps = data;

            return response;
        }

        public GiftApprovalResponse GiftApprovalList(GiftApprovalRequest request)
        {
            var fData = getCurrentFiscalYear();
            if (request.fromDate == null)
            {
                request.fromDate = fData.YStart;
            }

            if (request.toDate == null)
            {
                request.toDate = DateTime.Now.Date;
            }

            GiftApprovalResponse response = new GiftApprovalResponse();

            response.fromDate = request.fromDate.Value.ToShortDateString();
            response.toDate = request.toDate.Value.ToShortDateString();

            var data = (from gift in DbContext.E_GiftsAndHospitalities
                        join emp in DbContext.E__EMPLOYEE on gift.EmployeeId equals emp.EMPLOYEEID
                        join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                        join lm in DbContext.E__EMPLOYEE on jd.LINEMANAGER equals lm.EMPLOYEEID
                        join jr in DbContext.E_JOBROLE on jd.JobRoleId equals jr.JobRoleId into jr_join
                        from jr in jr_join.DefaultIfEmpty()
                        join t in DbContext.E_TEAM on jd.TEAM equals t.TEAMID into t_join
                        from t in t_join.DefaultIfEmpty()
                        join d in DbContext.E_DIRECTORATES on t.DIRECTORATEID equals d.DIRECTORATEID into d_join
                        from d in d_join.DefaultIfEmpty()
                        join dir in DbContext.E__EMPLOYEE on t.DIRECTOR equals dir.EMPLOYEEID into dir_join
                        from dir in dir_join.DefaultIfEmpty()
                        join s in DbContext.E_Gift_Status on gift.GiftStatusId equals s.GiftStatusId into s_join
                        from s in s_join.DefaultIfEmpty()
                        where (
                                (emp.FIRSTNAME + " " + emp.LASTNAME).Contains(request.searchText)
                                && ((gift.DateGivenReceived >= request.fromDate || request.fromDate == null) && (gift.DateGivenReceived <= request.toDate || request.toDate == null))
                                && (gift.GiftStatusId == request.statusId || request.statusId == 0)
                                && (emp.EMPLOYEEID == request.employeeId || request.employeeId == 0)
                                && (d.DIRECTORATEID == request.directorateId || request.directorateId == 0)
                                )
                        select new GiftApprovalData()
                        {
                            giftId = gift.GiftId,
                            employeeId = emp.EMPLOYEEID,
                            employeeName = emp.FIRSTNAME + " " + emp.LASTNAME,
                            directorateId = d.DIRECTORATEID,
                            directorateName = d.DIRECTORATENAME,
                            statusId = s.GiftStatusId,
                            statusName = s.Description,
                            details = gift.Details,
                            dateGivenReceived = gift.DateGivenReceived,
                            value = gift.ApproximateValue,
                        }).ToList();

            Pagination page = new Pagination();
            page.totalRows = data.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)data.ToList().Count / request.pagination.pageSize);
            page.pageNo = request.pagination.pageNo;
            page.pageSize = request.pagination.pageSize;
            int _page = (page.pageNo - 1);

            response.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = data.ToList();

            string _sortBy = @"";
            if (request.sortBy == null)
            {
                _sortBy = @"employeeName";
            }
            else
            {
                _sortBy = request.sortBy;
            }
            // Getting sorted by sort parameter
            if (request.sortOrder == "ASC")
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(_sortBy).GetValue(c, null)).Skip(_skip).Take(request.pagination.pageSize).ToList();
            }

            List<GiftApprovalData> giftList = new List<GiftApprovalData>();
            foreach (var item in finalData)
            {
                GiftApprovalData report = new GiftApprovalData();
                report.employeeName = item.employeeName;
                report.giftId = item.giftId;
                report.statusId = item.statusId;
                report.statusName = item.statusName;
                report.employeeId = item.employeeId;
                report.directorateName = item.directorateName;
                report.employeeName = item.employeeName;
                report.details = item.details;
                report.dateGivenReceived = item.dateGivenReceived;
                report.value = item.value;
                if (item.dateGivenReceived.HasValue)
                {
                    report.receivedGivenDate = item.dateGivenReceived.Value.ToShortDateString();
                }

                giftList.Add(report);
            }
            response.giftsAppovalList = giftList;
            response.pagination = page;
            //response.ceoId = GetChiefExecutive();
            // response.execDirectorId = GetTeamDirector();
            return response;
        }


        #endregion


    }
}
