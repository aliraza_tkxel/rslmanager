﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository
{
    public interface IBaseRepository<T> : IDisposable where T : class
    {
        DatabaseEntities.Entities  DbContext { get; set; }

        IQueryable<T> AsQueryable();

        IEnumerable<T> GetAll();

        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);

        T Single(Expression<Func<T, bool>> predicate);

        T SingleOrDefault(Expression<Func<T, bool>> predicate);

        T First(Expression<Func<T, bool>> predicate);

        T GetById(int id);

        T Update(T entity, int key);

        T Add(T entity);

        void Delete(T entity);

        void Attach(T entity);

        T UpdateStringTypeId(T entity, string key);
    }
}
