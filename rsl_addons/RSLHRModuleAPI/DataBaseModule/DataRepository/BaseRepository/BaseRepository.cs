﻿using DataBaseModule.DataRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.DataRepository
{
   public  abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly DbSet<T> _entities;

        public DatabaseEntities.Entities  DbContext { get; set; }

        public BaseRepository(DatabaseEntities.Entities dbContext)
        {
            this.DbContext = dbContext;
            _entities = this.DbContext.Set<T>();
        }

        public virtual IQueryable<T> AsQueryable()
        {
            return _entities.AsQueryable();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _entities.AsNoTracking();
        }


        public virtual IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return _entities.AsNoTracking().Where(predicate);
        }

        public virtual IEnumerable<T> FindWithTracking(Expression<Func<T, bool>> predicate)
        {
            return _entities.Where(predicate);
        }



        public virtual T Single(Expression<Func<T, bool>> predicate)
        {
            return _entities.Single(predicate);
        }

        public virtual T SingleOrDefault(Expression<Func<T, bool>> predicate)
        {
            return _entities.SingleOrDefault(predicate);
        }

        public virtual T First(Expression<Func<T, bool>> predicate)
        {
            return _entities.First(predicate);
        }

        public virtual T GetById(int id)
        {
            return _entities.Find(id);
        }

        public virtual T Add(T entity)
        {
            return _entities.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            _entities.Remove(entity);
        }

        public virtual void Attach(T entity)
        {
            _entities.Attach(entity);
        }

        public virtual void Dispose()
        {
            //throw new NotImplementedException();
        }

        public virtual T Update(T entity, int key)
        {
            if (entity == null)
                return null;

            var existing = _entities.Find(key);
            if (existing != null)
            {
                this.DbContext.Entry(existing).CurrentValues.SetValues(entity);
                //_context.SaveChanges();
            }
            return existing;
        }



        //findFn for string type id       

        public T UpdateStringTypeId(T entity, string key)
        {
            if (entity == null)
                return null;

            var existing = _entities.Find(key);
            if (existing != null)
            {
                this.DbContext.Entry(existing).CurrentValues.SetValues(entity);
                //_context.SaveChanges();
            }
            return existing;
        }
    }
}
