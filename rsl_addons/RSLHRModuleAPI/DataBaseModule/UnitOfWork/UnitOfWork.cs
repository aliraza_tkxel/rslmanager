﻿using DataBaseModule.DataRepository;
using DataBaseModule.DataRepository.Admin.Teams;
using DataBaseModule.DataRepository.Dashboard;
using DataBaseModule.DataRepository.Employees;
using DataBaseModule.DataRepository.Employees.Absence;
using DataBaseModule.DataRepository.Employees.PostTabRepositories;
using DataBaseModule.DataRepository.MyDetail;
using DataBaseModule.DataRepository.Push;
using DataBaseModule.DataRepository.Remitance;
using DataBaseModule.DataRepository.Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.UnitOfWork
{
    public class UnitOfWork : Disposable, IUnitOfWork
    {
        private DatabaseEntities.Entities dataContext = null;
        //start authentication repo
        private AuthenticationRepository _authenticationRepository;
        //end authentication repo
        private NavigationRepository _navigationRepository;

        private AbsenceNatureRepository _absenceNatureRepository;

        private AbsenceReasonRepository _absenceReasonRepository;

        private AbsenceStatusRepository _absenceStatusRepository;

        private AbsenceActionRepository _absenceActionRepository;

        private EmployeesRepository _employeeRepository;

        private EthinicityRepository _ethinicityRepository;

        private ReligionRepository _religionRepository;

        private SexualOrientationRepository _sexualOrientationRepository;

        private MaritalStatusRepository _maritalStatusRepository;

        private MyDetailRepository _myDetailRepository;

        private TitleRepository _titleRepository;
        private HealthRepository _healthRepository;
        private DisabilityAndDifficultyRepository _disabilityAndDifficultyRepository;
        private QualificationTypeRepository _qualificationTypeRepository;
        private SkillsRepository _skillsRepository;
        private QualificationLevelRepository _qualificationLevelRepository;
        private DayHourRepository _dayHourRepository;
        private GradePointRepository _gradePointRepository;
        private GradeRepository _gradeRepository;
        private PlaceOfWorkRepository _placeOfWorkRepository;
        private PartFullTimeRepository _partFullTimeRepository;
        private ProbationPeriodRepository _probationPeriodRepository;

        private HolidayRuleRepository _holidayRuleRepository;
        private JobRoleRepository _jobRoleRepository;

        private NoticePeriodRepository _noticePeriodRepository;

        private BenefitsRepository _benefitsRepository;
        private BenefitTypeRepository _benefitTypeRepository;
        private MedicalOrgRepository _medicalOrgRepository;
        private MedicalTypeRepository _medicalTypeRepository;
        private SchemesRepository _schemesRepository;
        private ReportsRepository _reportsRepository;
        private TeamRepository _teamRepository;
        private TeamsRepository _teamsRepository;
        private AccessControlRepository _accessControlRepository;
        private AbsenceRepository _absenceRepository;
        private BankHolidaysRepository _bankHolidayRepository;
        private PostRepository _postRepository;

        private EquivalentLevelRepository _equivalentLevelRepository;
        private DocumentsRepository _documentsRepository;
        private DocumentTypeRepository _documentTypeRepository;
        private DocumentVisibilityRepository _documentVisibilityRepository;
        private TrainingsRepository _trainingsRepository;
        private GiftRepository _giftRepository;
        private GiftStatusRepository _giftStatusRepository;

        private DeclerationOfInterestRepository _declerationOfInterestRepository;
        private EmployeeDoiRelationshipRepository _employeeDoiRelationshipRepository;

        private FIscalYearRepository _fIscalYearRepository;

        private PaypointStatusRepository _paypointStatusRepository;
        private DashboardRepository _dashboardRepository;
        private DrivingLicenceRepository _drivingLicenceRepository;
        private LanguageTypesRepository _languageTypesRepository;
        private DeclerationOfInterestStatusRepository _declerationOfInterestStatusRepository;
        private RemitanceRepository _remitanceRepository;
        private DirectorateRepository _directorateRepository;
        private PushRepository _pushRepository;
        public UnitOfWork()
        {
            if (dataContext == null)
            {
                dataContext = new DatabaseEntities.Entities();
                dataContext.Configuration.ProxyCreationEnabled = false;
            }
        }

        public AuthenticationRepository authenticationRepository
        {
            get { return (_authenticationRepository == null) ? _authenticationRepository = new AuthenticationRepository(dataContext) : _authenticationRepository; }
        }

        public NavigationRepository navigationRepository
        {
            get { return (_navigationRepository == null) ? _navigationRepository = new NavigationRepository(dataContext) : _navigationRepository; }
        }

        public EmployeesRepository employeeRepository
        {
            get { return (_employeeRepository == null) ? _employeeRepository = new EmployeesRepository(dataContext) : _employeeRepository; }
        }

        public AbsenceNatureRepository absenceNatureRepository
        {
            get { return (_absenceNatureRepository == null) ? _absenceNatureRepository = new AbsenceNatureRepository(dataContext) : _absenceNatureRepository; }
        }

        public EthinicityRepository ethinicityRepository
        {
            get { return (_ethinicityRepository == null) ? _ethinicityRepository = new EthinicityRepository(dataContext) : _ethinicityRepository; }
        }


        public AbsenceReasonRepository absenceReasonRepository
        {
            get { return (_absenceReasonRepository == null) ? _absenceReasonRepository = new AbsenceReasonRepository(dataContext) : _absenceReasonRepository; }
        }

        public ReligionRepository religionRepository
        {
            get { return (_religionRepository == null) ? _religionRepository = new ReligionRepository(dataContext) : _religionRepository; }
        }


        public SexualOrientationRepository sexualOrientationRepository
        {
            get { return (_sexualOrientationRepository == null) ? _sexualOrientationRepository = new SexualOrientationRepository(dataContext) : _sexualOrientationRepository; }
        }

        public MaritalStatusRepository maritalStatusRepository
        {
            get { return (_maritalStatusRepository == null) ? _maritalStatusRepository = new MaritalStatusRepository(dataContext) : _maritalStatusRepository; }
        }

        public MyDetailRepository myDetailRepository
        {
            get { return (_myDetailRepository == null) ? _myDetailRepository = new MyDetailRepository(dataContext) : _myDetailRepository; }
        }

        public TitleRepository titleRepository
        {
            get { return (_titleRepository == null) ? _titleRepository = new TitleRepository(dataContext) : _titleRepository; }
        }

        public HealthRepository healthRepository
        {
            get { return (_healthRepository == null) ? _healthRepository = new HealthRepository(dataContext) : _healthRepository; }
        }

        public DisabilityAndDifficultyRepository disabilityAndDifficultyRepository
        {
            get { return (_disabilityAndDifficultyRepository == null) ? _disabilityAndDifficultyRepository = new DisabilityAndDifficultyRepository(dataContext) : _disabilityAndDifficultyRepository; }
        }

        public SkillsRepository skillsRepository
        {
            get { return (_skillsRepository == null) ? _skillsRepository = new SkillsRepository(dataContext) : _skillsRepository; }
        }

        public QualificationTypeRepository qualificationTypeRepository
        {
            get { return (_qualificationTypeRepository == null) ? _qualificationTypeRepository = new QualificationTypeRepository(dataContext) : _qualificationTypeRepository; }
        }
        public QualificationLevelRepository qualificationLevelRepository
        {
            get { return (_qualificationLevelRepository == null) ? _qualificationLevelRepository = new QualificationLevelRepository(dataContext) : _qualificationLevelRepository; }
        }

        public HolidayRuleRepository holidayRuleRepository
        {
            get { return (_holidayRuleRepository == null) ? _holidayRuleRepository = new HolidayRuleRepository(dataContext) : _holidayRuleRepository; }
        }

        public JobRoleRepository jobRoleRepository
        {
            get { return (_jobRoleRepository == null) ? _jobRoleRepository = new JobRoleRepository(dataContext) : _jobRoleRepository; }
        }



        public DayHourRepository dayHourRepository
        {
            get { return (_dayHourRepository == null) ? _dayHourRepository = new DayHourRepository(dataContext) : _dayHourRepository; }
        }


        public GradePointRepository gradePointRepository
        {
            get { return (_gradePointRepository == null) ? _gradePointRepository = new GradePointRepository(dataContext) : _gradePointRepository; }
        }

        public GradeRepository gradeRepository
        {
            get { return (_gradeRepository == null) ? _gradeRepository = new GradeRepository(dataContext) : _gradeRepository; }
        }

        public PartFullTimeRepository partFullTimeRepository
        {
            get { return (_partFullTimeRepository == null) ? _partFullTimeRepository = new PartFullTimeRepository(dataContext) : _partFullTimeRepository; }
        }

        public PlaceOfWorkRepository placeOfWorkRepository
        {
            get { return (_placeOfWorkRepository == null) ? _placeOfWorkRepository = new PlaceOfWorkRepository(dataContext) : _placeOfWorkRepository; }
        }

        public NoticePeriodRepository noticePeriodRepository
        {
            get { return (_noticePeriodRepository == null) ? _noticePeriodRepository = new NoticePeriodRepository(dataContext) : _noticePeriodRepository; }
        }

        public ProbationPeriodRepository probationPeriodRepository
        {
            get { return (_probationPeriodRepository == null) ? _probationPeriodRepository = new ProbationPeriodRepository(dataContext) : _probationPeriodRepository; }
        }

        public BenefitsRepository benefitsRepository
        {
            get { return (_benefitsRepository == null) ? _benefitsRepository = new BenefitsRepository(dataContext) : _benefitsRepository; }
        }
        public BenefitTypeRepository benefitTypeRepository
        {
            get { return (_benefitTypeRepository == null) ? _benefitTypeRepository = new BenefitTypeRepository(dataContext) : _benefitTypeRepository; }
        }
        public MedicalOrgRepository medicalOrgRepository
        {
            get { return (_medicalOrgRepository == null) ? _medicalOrgRepository = new MedicalOrgRepository(dataContext) : _medicalOrgRepository; }
        }
        public MedicalTypeRepository medicalTypeRepository
        {
            get { return (_medicalTypeRepository == null) ? _medicalTypeRepository = new MedicalTypeRepository(dataContext) : _medicalTypeRepository; }
        }
        public SchemesRepository schemesRepository
        {
            get { return (_schemesRepository == null) ? _schemesRepository = new SchemesRepository(dataContext) : _schemesRepository; }
        }
        public ReportsRepository reportsRepository
        {
            get { return (_reportsRepository == null) ? _reportsRepository = new ReportsRepository(dataContext) : _reportsRepository; }
        }

        public TeamRepository teamRepository
        {
            get { return (_teamRepository == null) ? _teamRepository = new TeamRepository(dataContext) : _teamRepository; }
        }

        public TeamsRepository teamsRepository
        {
            get { return (_teamsRepository == null) ? _teamsRepository = new TeamsRepository(dataContext) : _teamsRepository; }
        }

        public AccessControlRepository accessControlRepository
        {
            get { return (_accessControlRepository == null) ? _accessControlRepository = new AccessControlRepository(dataContext) : _accessControlRepository; }
        }
        public AbsenceRepository absenceRepository
        {
            get { return (_absenceRepository == null) ? _absenceRepository = new AbsenceRepository(dataContext) : _absenceRepository; }
        }

        public BankHolidaysRepository bankHolidayRepository
        {
            get { return (_bankHolidayRepository == null) ? _bankHolidayRepository = new BankHolidaysRepository(dataContext) : _bankHolidayRepository; }
        }

        public EquivalentLevelRepository equivalentLevelRepository
        {
            get { return (_equivalentLevelRepository == null) ? _equivalentLevelRepository = new EquivalentLevelRepository(dataContext) : _equivalentLevelRepository; }
        }
        public PostRepository postRepository
        {
            get { return (_postRepository == null) ? _postRepository = new PostRepository(dataContext) : _postRepository; }
        }

        public DocumentsRepository documentsRepository
        {
            get { return (_documentsRepository == null) ? _documentsRepository = new DocumentsRepository(dataContext) : _documentsRepository; }
        }
        public DocumentTypeRepository documentTypeRepository
        {
            get { return (_documentTypeRepository == null) ? _documentTypeRepository = new DocumentTypeRepository(dataContext) : _documentTypeRepository; }
        }

        public DocumentVisibilityRepository documentVisibilityRepository
        {
            get { return (_documentVisibilityRepository == null) ? _documentVisibilityRepository = new DocumentVisibilityRepository(dataContext) : _documentVisibilityRepository; }
        }

        public TrainingsRepository trainingsRepository
        {
            get { return (_trainingsRepository == null) ? _trainingsRepository = new TrainingsRepository(dataContext) : _trainingsRepository; }
        }

        public GiftRepository giftRepository
        {
            get { return (_giftRepository == null) ? _giftRepository = new GiftRepository(dataContext) : _giftRepository; }
        }
        public DeclerationOfInterestRepository declerationOfInterestRepository
        {
            get { return (_declerationOfInterestRepository == null) ? _declerationOfInterestRepository = new DeclerationOfInterestRepository(dataContext) : _declerationOfInterestRepository; }
        }

        public EmployeeDoiRelationshipRepository employeeDoiRelationshipRepository
        {
            get { return (_employeeDoiRelationshipRepository == null) ? _employeeDoiRelationshipRepository = new EmployeeDoiRelationshipRepository(dataContext) : _employeeDoiRelationshipRepository; }
        }

        public FIscalYearRepository fIscalYearRepository
        {
            get { return (_fIscalYearRepository == null) ? _fIscalYearRepository = new FIscalYearRepository(dataContext) : _fIscalYearRepository; }
        }


        public PaypointStatusRepository paypointStatusRepository
        {
            get { return (_paypointStatusRepository == null) ? _paypointStatusRepository = new PaypointStatusRepository(dataContext) : _paypointStatusRepository; }
        }

        public DashboardRepository dashboardRepository
        {
            get { return (_dashboardRepository == null) ? _dashboardRepository = new DashboardRepository(dataContext) : _dashboardRepository; }
        }

        public AbsenceStatusRepository absenceStatusRepository
        {
            get { return (_absenceStatusRepository == null) ? _absenceStatusRepository = new AbsenceStatusRepository(dataContext) : _absenceStatusRepository; }
        }

        public AbsenceActionRepository absenceActionRepository
        {
            get { return (_absenceActionRepository == null) ? _absenceActionRepository = new AbsenceActionRepository(dataContext) : _absenceActionRepository; }
        }

        public DrivingLicenceRepository drivingLicenceRepository
        {
            get { return (_drivingLicenceRepository == null) ? _drivingLicenceRepository = new DrivingLicenceRepository(dataContext) : _drivingLicenceRepository; }
        }
        public LanguageTypesRepository languageTypesRepository
        {
            get { return (_languageTypesRepository == null) ? _languageTypesRepository = new LanguageTypesRepository(dataContext) : _languageTypesRepository; }
        }

        public DeclerationOfInterestStatusRepository declerationOfInterestStatusRepository
        {
            get { return (_declerationOfInterestStatusRepository == null) ? _declerationOfInterestStatusRepository = new DeclerationOfInterestStatusRepository(dataContext) : _declerationOfInterestStatusRepository; }
        }

        public RemitanceRepository remitanceRepository
        {
            get { return (_remitanceRepository == null) ? _remitanceRepository = new RemitanceRepository(dataContext) : _remitanceRepository; }
        }

        public GiftStatusRepository giftStatusRepository
        {
            get { return (_giftStatusRepository == null) ? _giftStatusRepository = new GiftStatusRepository(dataContext) : _giftStatusRepository; }
        }

        public DirectorateRepository directorateRepository
        {
            get { return (_directorateRepository == null) ? _directorateRepository = new DirectorateRepository(dataContext) : _directorateRepository; }
        }
        public PushRepository pushRepository
        {
            get { return (_pushRepository == null) ? _pushRepository = new PushRepository(dataContext) : _pushRepository; }
        }
        int IUnitOfWork.Commit()
        {
            return dataContext.SaveChanges();
        }
        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }

    }
}
