﻿using DataBaseModule.DataRepository;
using DataBaseModule.DataRepository.Admin.Teams;
using DataBaseModule.DataRepository.Dashboard;
using DataBaseModule.DataRepository.Employees;
using DataBaseModule.DataRepository.Employees.Absence;
using DataBaseModule.DataRepository.Employees.PostTabRepositories;
using DataBaseModule.DataRepository.MyDetail;
using DataBaseModule.DataRepository.Push;
using DataBaseModule.DataRepository.Remitance;
using DataBaseModule.DataRepository.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseModule.UnitOfWork
{
    public interface IUnitOfWork
    {
        #region start authenticatio repo       
        AuthenticationRepository authenticationRepository { get; }
        #endregion
        NavigationRepository navigationRepository { get; }

        EmployeesRepository employeeRepository { get; }

        AbsenceNatureRepository absenceNatureRepository { get; }

        AbsenceStatusRepository absenceStatusRepository { get; }

        AbsenceActionRepository absenceActionRepository { get; }

        AbsenceReasonRepository absenceReasonRepository { get; }

        EthinicityRepository ethinicityRepository { get; }

        ReligionRepository religionRepository { get; }

        SexualOrientationRepository sexualOrientationRepository { get; }

        MaritalStatusRepository maritalStatusRepository { get; }

        MyDetailRepository myDetailRepository { get; }
        TitleRepository titleRepository { get; }
        HealthRepository healthRepository { get; }
        DisabilityAndDifficultyRepository disabilityAndDifficultyRepository { get; }
        SkillsRepository skillsRepository { get; }
        QualificationLevelRepository qualificationLevelRepository { get; }
        QualificationTypeRepository qualificationTypeRepository { get; }

        HolidayRuleRepository holidayRuleRepository { get; }

        JobRoleRepository jobRoleRepository { get; }

        DayHourRepository dayHourRepository { get; }

        GradePointRepository gradePointRepository { get; }
        GradeRepository gradeRepository { get; }

        PartFullTimeRepository partFullTimeRepository { get; }

        PlaceOfWorkRepository placeOfWorkRepository { get; }

        NoticePeriodRepository noticePeriodRepository { get; }

        ProbationPeriodRepository probationPeriodRepository { get; }


        BenefitsRepository benefitsRepository { get; }
        BenefitTypeRepository benefitTypeRepository { get; }
        MedicalOrgRepository medicalOrgRepository { get; }
        MedicalTypeRepository medicalTypeRepository { get; }
        SchemesRepository schemesRepository { get; }
        ReportsRepository reportsRepository { get; }
        TeamRepository teamRepository { get; }

        TeamsRepository teamsRepository { get; }

        AccessControlRepository accessControlRepository { get; }

        AbsenceRepository absenceRepository { get; }

        BankHolidaysRepository bankHolidayRepository { get; }
        PostRepository postRepository { get; }
        EquivalentLevelRepository equivalentLevelRepository { get; }

        DocumentsRepository documentsRepository { get; }
        DocumentTypeRepository documentTypeRepository { get; }
        DocumentVisibilityRepository documentVisibilityRepository { get; }
        TrainingsRepository trainingsRepository { get; }
        GiftRepository giftRepository { get; }
        DeclerationOfInterestRepository declerationOfInterestRepository { get; }
        EmployeeDoiRelationshipRepository employeeDoiRelationshipRepository { get; }
        DashboardRepository dashboardRepository { get; }
        FIscalYearRepository fIscalYearRepository { get; }
        PaypointStatusRepository paypointStatusRepository { get; }
        DrivingLicenceRepository drivingLicenceRepository { get; }
        LanguageTypesRepository languageTypesRepository { get; }
        DeclerationOfInterestStatusRepository declerationOfInterestStatusRepository { get; }
        RemitanceRepository remitanceRepository { get; }
        GiftStatusRepository giftStatusRepository { get; }
        DirectorateRepository directorateRepository { get; }
        PushRepository pushRepository { get; }
        int Commit();
    }
}
