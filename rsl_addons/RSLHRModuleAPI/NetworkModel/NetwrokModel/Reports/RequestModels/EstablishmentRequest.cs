﻿using NetworkModel;

namespace NetwrokModel.Reports
{
    public class CommonReportsFilterRequest
    {
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int? directorate { get; set; }
        public int? team { get; set; }
        public string employeeType { get; set; }
        public string searchText { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public Pagination pagination { get; set; }

        public int fullTime { get; set; }

    }
}
