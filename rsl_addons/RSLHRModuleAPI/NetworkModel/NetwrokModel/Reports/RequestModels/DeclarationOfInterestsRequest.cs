﻿using NetworkModel;

namespace NetwrokModel.Reports
{
    public class DeclarationOfInterestsRequest
    {
        public string searchText { get; set; }
        public int status { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public int fiscalYearId { get; set; }
        public int? teamId { get; set; }
        public int? directorate { get; set; }
        public int lineManagerId { get; set; }
        public Pagination pagination { get; set; }
    }
}
