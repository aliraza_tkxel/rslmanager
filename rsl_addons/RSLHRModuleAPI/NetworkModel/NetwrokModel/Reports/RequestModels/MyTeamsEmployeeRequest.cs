﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class MyTeamsEmployeeRequest
    {
        public int employeeId { get; set; }
        public int teamId { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }

        public Pagination pagination { get; set; }
    }
}
