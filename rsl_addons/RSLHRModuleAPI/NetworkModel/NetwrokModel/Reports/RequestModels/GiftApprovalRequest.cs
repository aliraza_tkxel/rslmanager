﻿using NetworkModel;
using System;

namespace NetwrokModel.Reports
{
    public class GiftApprovalRequest
    {
        public string searchText { get; set; }
        public int statusId { get; set; }
        public int fiscalYearId { get; set; }
        public int? directorateId { get; set; }
        public int employeeId { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; } 
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public Pagination pagination { get; set; }
    }
}
