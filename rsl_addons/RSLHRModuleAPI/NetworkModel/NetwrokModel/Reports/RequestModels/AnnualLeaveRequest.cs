﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class AnnualLeaveRequest
    {
        public int? directorate { get; set; }
        public int? team { get; set; }
        public int? year { get; set; }
        public string employeeType { get; set; }
        public string searchText { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public Pagination pagination { get; set; }
    }
}
