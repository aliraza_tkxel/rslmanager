﻿namespace NetwrokModel.Reports
{
    public class HealthReportResponse
    {
        public HealthReportListing healthReportListing { get; set; }
        public HealthLookUps lookUps { get; set; } = new HealthLookUps();
    }
}