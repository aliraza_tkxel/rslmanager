﻿namespace NetwrokModel.Reports
{
    public class SicknessExportResponse
    {
        public SicknessExportList sicknessExportList { get; set; }
        public SicknessLookUps lookUps { get; set; } = new SicknessLookUps();
    }
}
