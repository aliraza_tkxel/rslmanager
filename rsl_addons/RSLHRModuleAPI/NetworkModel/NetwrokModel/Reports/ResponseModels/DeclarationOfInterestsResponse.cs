﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class DeclarationOfInterestsResponse
    {
        public DeclarationOfInterestListing declarationOfInterestsListing { get; set; }

        public DeclarationOfInterestsReportLookUps lookUps { get; set; } = new DeclarationOfInterestsReportLookUps();
        public int lineManagerId { get; set; }
    }
}
