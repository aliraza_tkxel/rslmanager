﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class VacanciesListResponse
    {
        public long jobId { get; set; }
        public string description { get; set; }
        public string title { get; set; }
        public string startDate { get; set; }
        public string jobType { get; set; }       
        public string teamName { get; set; }
    }
}
