﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class NewStarterResponse
    {
        public NewStarterListing newStarterList { get; set; }
        public NewStarterReportLookUps lookUps { get; set; } = new NewStarterReportLookUps();
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }
}
