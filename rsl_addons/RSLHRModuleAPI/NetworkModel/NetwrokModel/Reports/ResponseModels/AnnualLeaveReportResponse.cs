﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class AnnualLeaveReportResponse
    {
        public AnnualLeaveReport annualLeaveListing { get; set; }
        public AnnualLeaveLookUps lookUps { get; set; } = new AnnualLeaveLookUps();
    }
}
