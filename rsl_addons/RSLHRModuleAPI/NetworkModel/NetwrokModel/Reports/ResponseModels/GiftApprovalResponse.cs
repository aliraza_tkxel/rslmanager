﻿using NetworkModel;
using System;
using System.Collections.Generic;

namespace NetwrokModel.Reports
{
    public class GiftApprovalResponse
    {
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int lineManagerId { get; set; }
        public int statusId { get; set; }
        public int employeeId { get; set; }
        public List<GiftApprovalData> giftsAppovalList { get; set; }
        public GiftsApprovalLookUps lookUps { get; set; } = new GiftsApprovalLookUps();
        public Pagination pagination { get; set; }

        
    }

    public class GiftsApprovalLookUps
    {
        public List<LookUpResponseModel> directorates { get; set; }
        public List<LookUpResponseModel> giftStatus { get; set; }
        public List<LookUpResponseModel> employees { get; set; }
    }
}
