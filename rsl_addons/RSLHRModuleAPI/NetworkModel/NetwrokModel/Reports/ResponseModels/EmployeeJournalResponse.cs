﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class EmployeeJournalResponse
    {
        public List<EmployeeJournalList> employeeList { get; set; }
        public Pagination pagination { get; set; }
    }


    public class EmployeeJournalList
    {
        public int? employeeId { get; set; }
        public string creationDate { get; set; }
        public string lastActionDate { get; set; }
        public string item { get; set; }
        public string nature { get; set; }
        public string title { get; set; }
        public string notes { get; set; }
        public string status { get; set; }
        public string itemId { get; set; }
        public string redirectTab { get; set; }
        public DateTime? creationDateString { get; set; }
        public DateTime? lastActionDateString { get; set; }

        public string documentPath { get; set; }
        public string doiStatus { get; set; }
    }
}
