﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class RemunerationStatementsResponse
    {
        public RemunerationStatementsListing statementList { get; set; }
        public RemunerationStatementsLookUps lookUps { get; set; } = new RemunerationStatementsLookUps();
    }
}
