﻿namespace NetwrokModel.Reports
{
    public class SicknessReportResponse
    {
        public SicknessList sicknessList { get; set; }
        public SicknessLookUps lookUps { get; set; } = new SicknessLookUps();
    }
}
