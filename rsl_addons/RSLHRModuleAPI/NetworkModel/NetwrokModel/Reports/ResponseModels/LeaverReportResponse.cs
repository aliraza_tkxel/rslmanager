﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
  public   class LeaverReportResponse
    {
        public LeaverReportListing leaverReportListing { get; set; }
        public LeaverReportLookUps lookUps { get; set; } = new LeaverReportLookUps();
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }

    }
}
