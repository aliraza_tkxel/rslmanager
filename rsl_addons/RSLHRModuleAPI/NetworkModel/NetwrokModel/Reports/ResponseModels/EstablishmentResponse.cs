﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class EstablishmentResponse
    {
        public EstablishmentList establishmentList { get; set; }
        public EstablishmentlookUps lookUps { get; set; } = new EstablishmentlookUps();
    }
}
