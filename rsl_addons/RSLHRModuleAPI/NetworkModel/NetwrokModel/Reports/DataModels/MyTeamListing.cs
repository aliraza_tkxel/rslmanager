﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class MyTeamListing
    {
        public List<MyTeamDetail> teamList { get; set; }
        public Pagination pagination { get; set; }
    }
    public class MyTeamDetail
    {
        public int teamId { get; set; }
        public string teamName { get; set; }
        public string noOfEmployee { get; set; }
        public string leave { get; set; }
        public string leaveDays { get; set; }
        public string leaveHRS { get; set; }
        public string salary { get; set; }
        public double sicknessAbsence { get; set; }
    }
}
