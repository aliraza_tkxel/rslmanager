﻿using Newtonsoft.Json;
using System;

namespace NetwrokModel.Reports
{
    public class SicknessExportDetail
    {
        public int employeeId { get; set; }
        public string firstName { get; set; }
        public string surName { get; set; }
        public string full_partTime { get; set; }
        public string jobTitle { get; set; }
        public string dept { get; set; }

        public string durationString { get; set; }
        [JsonIgnore]
        public double? duration { get; set; }
        [JsonIgnore]
        public string absType { get; set; }

        public string reason { get; set; }

        public string employeeStartDate { get; set; }
        [JsonIgnore]
        public DateTime? employeeStartDateString { get; set; }

        public string startDate { get; set; }
        [JsonIgnore]
        public DateTime? startDateString { get; set; }

        public string endDate { get; set; }
        [JsonIgnore]
        public DateTime? endDateString { get; set; }

        public string lastDateOfAbsence { get; set; }

        [JsonIgnore]
        public int? status { get; set; }

        [JsonIgnore]
        public int absenceHistoryId { get; set; }
    }
}
