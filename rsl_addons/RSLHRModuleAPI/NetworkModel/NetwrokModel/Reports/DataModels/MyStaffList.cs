﻿using NetworkModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class MyStaffList
    {
        public List<MyStaffDetail> employeeList { get; set; }
        public List<MyStaffDetail> myTeamHierarchyList { get; set; }
        public Pagination pagination { get; set; }
    }
    public class MyStaffDetail
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string jobTitle { get; set; }
        
        public decimal? annualLeave { get; set; }
        public decimal? remainingLeave { get; set; }
        public string grade { get; set; }
        public string paypointReview { get; set; }
        public string appraisal { get; set; }
        public string doiStatus { get; set; }
        public Nullable<int> interestId { get; set; }
        public string leaveType { get; set; }
        public string Pendingleave { get; set; }
        public int trainingApproval { get; set; }
        public string giftStatus { get; set; }


        [JsonIgnore]
        public DateTime? paypointReviewDate { get; set; }
        [JsonIgnore]
        public DateTime? appraisalDateTime { get; set; }

    }
}
