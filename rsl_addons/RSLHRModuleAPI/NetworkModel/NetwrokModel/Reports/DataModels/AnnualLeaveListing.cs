﻿using NetworkModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class AnnualLeaveListing
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public double? totalAllowance { get; set; }
        public double? daysBooked { get; set; }
        public double? daysRequested { get; set; }
        public double? daysTaken { get; set; }
        public double? carryForward { get; set; }
        public double? contractedHours { get; set; }
        public double? balanceRemaining { get; set; }
        [JsonIgnore]
        public DateTime?  startDate { get; set; }
        [JsonIgnore]
        public DateTime? AlStartDateString { get; set; }
        public string AlStartDate { get; set; }

        public DateTime? EmploymentStartDateString { get; set; }
        public string EmploymentStartDate { get; set; }

        public string payRoleNum { get; set; }
        public string lineManager { get; set; }
    }
    public class AnnualLeaveReport
    {
        public List<AnnualLeaveListing> reportListing { get; set; }
        public Pagination pagination { get; set; }
    }

    public class AnnualLeaveLookUps
    {
        public List<LookUpResponseModel> employeeLookUps { get; set; }
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }
    }
}
