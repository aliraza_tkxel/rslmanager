﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class DeclarationOfInterestListing
    {
        public List<DeclarationOfInterestsData> DOIs { get; set; }
        public Pagination pagination { get; set; }
    }
    public class DeclarationOfInterestsData
    {
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public int? jobRoleId { get; set; }
        public string jobRoleName { get; set; }
        public int lineManagerId { get; set; }
        public string lineManagerName { get; set; }
        public string reviewComments { get; set; }
        public DateTime? reviewDate { get; set; }
        public int? teamId { get; set; }
        public string teamName { get; set; }
        public int? directorId { get; set; }
        public string directorName { get; set; }
        public int? directorateId { get; set; }
        public string directorateName { get; set; }
        public int? statusId { get; set; }
        public string statusName { get; set; }
        public DateTime? updatedDate { get; set; }
        public DateTime submittedDate { get; set; }
        public string submitted { get; set; }
        public string updated { get; set; }
        public string documentPath { get; set; }
        public int? documentId { get; set; }
        public string comment { get; set; }
    }
    public class DeclarationOfInterestsReportLookUps
    {
        public List<LookUpResponseModel> fiscalYears { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> doiStatus { get; set; }
    }
}
