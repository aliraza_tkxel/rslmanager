﻿using NetworkModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
   public class HealthReportListing
    {
        public List<HealthReportDetail> employeeList { get; set; }
        public Pagination    pagination { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
    }
    public class HealthReportDetail
    {
        public int? disablityId { get; set; }
        public int diffDisabilityId { get; set; }
        public int employeeId { get; set; }
        public string description { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public string notes { get; set; }
        public int? loggedBy { get; set; }

        public string loggedByString { get; set; }
        [JsonIgnore]
        public DateTime? lastActionTime { get; set; }
        public string notifiedDate { get; set; }
        [JsonIgnore]
        public DateTime? notifiedDateString { get; set; }

        [JsonIgnore]
        public string firstName { get; set; }
        [JsonIgnore]
        public string lastName { get; set; }
        [JsonIgnore]
        public int? disabilityTypeId { get; set; }
        public string disabilityType { get; set; }
        public string fullName{ get; set; }

    }
    public class HealthLookUps
    {
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }
    }
}
