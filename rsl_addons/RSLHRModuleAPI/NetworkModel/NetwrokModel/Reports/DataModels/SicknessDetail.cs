﻿using Newtonsoft.Json;
using System;

namespace NetwrokModel.Reports
{
    public class SicknessDetail
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public string reason { get; set; }
        public  int absenceHistoryId { get; set; }

        public string startDate { get; set; }
        [JsonIgnore]
        public DateTime? startDateString { get; set; }
        [JsonIgnore]
        public DateTime? anticipatedReturnString { get; set; }
        public string anticipatedReturnDate { get; set; }

        [JsonIgnore]
        public DateTime? actualReturnString { get; set; }
        public string actualReturnDate { get; set; }

        public string lastDateOfAbsence { get; set; }



        public string durationString { get; set; }
        [JsonIgnore]
        public double? duration { get; set; }
        [JsonIgnore]
        public string absType { get; set; }



        public string returnNotes { get; set; }

        public int? journalId { get; set; }

        [JsonIgnore]
        public DateTime? endDateString { get; set; }

        [JsonIgnore]
        public int? status { get; set; }
    }
}
