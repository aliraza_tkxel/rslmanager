﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class MyTeamEmployeeListing
    {
        public List<MyTeamEmployeeDetail> employeeList { get; set; }
        public Pagination pagination { get; set; }
    }
    public class MyTeamEmployeeDetail
    {
        public string teamName { get; set; }
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string jobTitle { get; set; }
        public string grade { get; set; }
        public double remainingLeave { get; set; }
        public double remainingLeaveDays { get; set; }
        public double remainingLeaveHRS { get; set; }
        public string appraisalDate { get; set; }
        public string holRequest { get; set; }
    }
}
