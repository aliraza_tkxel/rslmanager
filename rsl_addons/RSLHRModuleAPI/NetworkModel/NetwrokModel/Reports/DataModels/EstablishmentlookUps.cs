﻿using NetworkModel;
using System.Collections.Generic;

namespace NetwrokModel.Reports
{
    public   class EstablishmentlookUps
    {
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> partFullTime { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }

    }
}
