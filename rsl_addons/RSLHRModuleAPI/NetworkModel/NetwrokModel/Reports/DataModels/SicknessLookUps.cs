﻿using NetworkModel;
using System.Collections.Generic;

namespace NetwrokModel.Reports
{
    public class SicknessLookUps
    {
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> reasonLookUps { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }
    }
}
