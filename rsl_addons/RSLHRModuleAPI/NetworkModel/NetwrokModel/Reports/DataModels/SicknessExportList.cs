﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class SicknessExportList
    {
        public List<SicknessExportDetail> sicknessExportList { get; set; }
        public Pagination pagination { get; set; } = new Pagination();
    }
}
