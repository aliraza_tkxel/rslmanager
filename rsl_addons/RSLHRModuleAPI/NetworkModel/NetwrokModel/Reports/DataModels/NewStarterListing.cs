﻿using NetworkModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class NewStarterListing
    {
        public List<NewStarterDetail> reportListing { get; set; }
        public Pagination pagination { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }

    public class NewStarterDetail
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public string jobTitle { get; set; }
        public string gender { get; set; }
        public string ethnicity { get; set; }
        public string dob { get; set; }
        public decimal? salary { get; set; }
        public string grade { get; set; }
        public string startDate { get; set; }
        public string reviewDate { get; set; }

        [JsonIgnore]
        public DateTime? startDateTime { get; set; }
        [JsonIgnore]
        public DateTime? reviewDateTime { get; set; }
        [JsonIgnore]
        public DateTime? dobDate { get; set; }
        [JsonIgnore]
        public DateTime? endDateString { get; set; }
        [JsonIgnore]
        public int? status { get; set; }
    }

    public class NewStarterReportLookUps
    {
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }
    }
}
