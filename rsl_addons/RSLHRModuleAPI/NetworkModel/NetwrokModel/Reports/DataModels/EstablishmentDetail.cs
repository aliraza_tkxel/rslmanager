﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class EstablishmentDetail
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public string jobTitle { get; set; }
        public string gender { get; set; }
        public string ethnicity { get; set; }
        public string address { get; set; }
        public string dob { get; set; }
        [JsonIgnore]
        public DateTime? dobString { get; set; }
        public decimal? salary { get; set; }
        public string grade { get; set; }
        public string startDate { get; set; }
        [JsonIgnore]
        public DateTime? startDateString { get; set; }
        [JsonIgnore]
        public DateTime? endDateString { get; set; }
        public string endDate { get; set; }
        public bool isDisciplinary { get; set; }

        public bool isSickness { get; set; }
        [JsonIgnore]
        public int? status { get; set; }
        [JsonIgnore]
        public int? teamId { get; set; }
    }
}
