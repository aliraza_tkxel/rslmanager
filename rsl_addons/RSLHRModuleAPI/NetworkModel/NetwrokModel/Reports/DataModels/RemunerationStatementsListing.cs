﻿using NetworkModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class RemunerationStatementsListing
    {
        public List<RemunerationStatementsDetail> employeeList { get; set; }
        public Pagination pagination { get; set; } = new Pagination();
        public string totalSalary { get; set; }
        public string totalStaff { get; set; }
        public int newStarter { get; set; }
        public int leaver { get; set; }
    }
    public class RemunerationStatementsDetail
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public string jobTitle { get; set; }
        public string gender { get; set; }
        public string ethnicity { get; set; }
        public string dob { get; set; }
        [JsonIgnore]
        public DateTime? dobString { get; set; }
        public decimal? salary { get; set; }
        public string grade { get; set; }
        public string startDate { get; set; }
        [JsonIgnore]
        public DateTime? startDateString { get; set; }
        [JsonIgnore]
        public DateTime? endDateString { get; set; }
        public string endDate { get; set; }
        public bool isDisciplinary { get; set; }

        public bool isSickness { get; set; }
        [JsonIgnore]
        public int? status { get; set; }
    }
    public class RemunerationStatementsLookUps
    {
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }
    }
}
