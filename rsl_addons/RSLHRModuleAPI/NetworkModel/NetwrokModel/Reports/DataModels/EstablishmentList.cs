﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class EstablishmentList
    {
        public List<EstablishmentDetail> establishmentList { get; set; }
        public Pagination pagination { get; set; } = new Pagination();
        public string totalSalary { get; set; }
        public string totalStaff { get; set; }
        public int newStarter { get; set; }
        public int leaver { get; set; }

        public List<LookUpResponseModel> partFullTime { get; set; }

    }
}
