﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class SicknessList
    {
        public List<SicknessDetail> sicknessList { get; set; }
        public Pagination pagination { get; set; } = new Pagination();
    }
}
