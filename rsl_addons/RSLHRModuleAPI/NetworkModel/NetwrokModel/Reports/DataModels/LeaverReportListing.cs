﻿using NetworkModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetwrokModel.Reports
{
    public class LeaverReportDetail
    {
        public int employeeId { get; set; }
        public string fullName { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        [JsonIgnore]
        public DateTime? startDateTime { get; set; }
        public string startDate { get; set; }
        [JsonIgnore]
        public DateTime? leaveDateTime { get; set; }
        public string leaveDate { get; set; }
        public string reason { get; set; }

    }
    public class LeaverReportListing
    {
        public List<LeaverReportDetail> reportListing { get; set; }
        public Pagination pagination { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
    }

    public class LeaverReportLookUps
    {
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }
    }
}
