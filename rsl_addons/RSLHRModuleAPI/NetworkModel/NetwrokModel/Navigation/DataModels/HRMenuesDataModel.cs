﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class HRMenuesDataModel
    {
        public int menuId { get; set; }
        public string menuName { get; set; }
        public string path { get; set; }
        public List<HRModuleSubMenuDataModel> subMenus { get; set; }
    }
}
