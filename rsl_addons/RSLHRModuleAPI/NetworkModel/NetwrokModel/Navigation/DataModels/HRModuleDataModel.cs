﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class HRModuleDataModel
    {
        public int moduleId { get; set; }
        public string moduleName { get; set; }
        public string url { get; set; }
    }
}
