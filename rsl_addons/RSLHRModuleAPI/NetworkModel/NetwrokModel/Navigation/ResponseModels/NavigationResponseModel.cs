﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class NavigationResponseModel
    {
        public List<HRMenuesDataModel> menues { get; set; }
        public List<HRModuleDataModel> hrModuleMenus { get; set; }
    }
}
