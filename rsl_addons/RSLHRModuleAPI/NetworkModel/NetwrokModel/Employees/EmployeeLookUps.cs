﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class EmployeeLookUps
    {
        public List<LookUpResponseModel> team { get; set; }
        public List<LookUpResponseModel> jobRole { get; set; }
        public List<LookUpResponseModel> holidayRule { get; set; }
        public List<LookUpResponseModel> lineManager { get; set; }
        public List<LookUpResponseModel> gradePoint { get; set; }
        public List<LookUpResponseModel> grade { get; set; }
        public List<LookUpResponseModel> dayHour { get; set; }
        public List<LookUpResponseModel> partFullTime { get; set; }
        public List<LookUpResponseModel> noticePeriod { get; set; }
        public List<LookUpResponseModel> placeOfWork { get; set; }
        public List<LookUpResponseModel> probationPeriod { get; set; }
        public List<LookUpResponseModel> contractType { get; set; }
    }
}
