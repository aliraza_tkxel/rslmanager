﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class DifficultiesAndDisablities
    {
        public int? disablityId { get; set; }
        public int diffDisabilityId { get; set; }

        public string description { get; set; }
        public string notes { get; set; }
        public int? loggedBy { get; set; }
       
        public string loggedByString { get; set; }
        [JsonIgnore]
        public DateTime? lastActionTime { get; set; }
        public string notifiedDate { get; set; }
        [JsonIgnore]
        public DateTime? notifiedDateString { get; set; }

        [JsonIgnore]
        public string firstName { get; set; }
        [JsonIgnore]
        public string lastName { get; set; }
        public bool? isLongTerm { get; set; }
        public string impactOnWork { get; set; }
        public bool? isOccupationalHealth { get; set; }
        public string reviewDate { get; set; }
        [JsonIgnore]
        public DateTime? reviewDateString { get; set; }
        public string documentTitle { get; set; }
        public string docPath { get; set; }

        public string docName { get; set; }
        public bool?  isDelete {get; set;}
    }
}
