﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
   public  class HealthLookUps
    {
        public List<LookUpResponseModel> disabilityHealthProblem { get; set; }
        public List<LookUpResponseModel> learningDifficulty { get; set; }
    }
}
