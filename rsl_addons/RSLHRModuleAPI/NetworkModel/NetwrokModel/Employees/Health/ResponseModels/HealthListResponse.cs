﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class HealthListResponse
    {

        public HealthLookUps lookUps { get; set; } = new HealthLookUps();
        public List<DifficultiesAndDisablities> Disablities { set; get; } = new List<DifficultiesAndDisablities>();
        public List<DifficultiesAndDisablities> learningDifficulties { set; get; } = new List<DifficultiesAndDisablities>();
    }
}
