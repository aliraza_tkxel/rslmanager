﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class EmployeesResponse
    {
        public List<Employee> employees { get; set; }
        public Pagination pagination { get; set; }
    }
}
