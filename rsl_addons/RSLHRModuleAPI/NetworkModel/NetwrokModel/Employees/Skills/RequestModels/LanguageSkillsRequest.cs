﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class LanguageSkillsRequest
    {
      public  List<LanguageSkills> languages { get; set; } = new List<LanguageSkills>();
    }
}
