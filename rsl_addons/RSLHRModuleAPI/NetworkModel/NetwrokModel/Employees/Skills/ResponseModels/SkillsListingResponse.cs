﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
   public class SkillsListingResponse
    {
        public SkillsAndQualifications skills { get; set; } = new SkillsAndQualifications();
        public SkillsLookUps lookUps { get; set; } = new SkillsLookUps();
        public List<LanguageSkills> languageSkills { get; set; } = new List<LanguageSkills>();
    }
}
