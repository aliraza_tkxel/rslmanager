﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
   public  class SkillsAndQualifications
    {
        public List<SkillsAndQualificationsDetail> skillsList { get; set; }
        public Pagination pagination { get; set; }

    }
}
