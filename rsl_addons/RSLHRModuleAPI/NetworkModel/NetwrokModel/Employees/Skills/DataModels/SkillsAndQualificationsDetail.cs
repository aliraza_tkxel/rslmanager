﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class SkillsAndQualificationsDetail
    {
        public int qualificationId { get; set; }
        public int employeeId { get; set; }
        public int? qualLevelID { get; set; }
        public int? equivalentLevel { get; set; }
        public string levelDescription { get; set; }
        public string subject { get; set; }
        public string result { get; set; }
        public string qualificationDate { get; set; }
        [JsonIgnore]
        public DateTime? qualificationDateTime { get; set; }

        public List<string> professionalQualification { get; set; }


        public int? createdBy { get; set; }



    }
    public class LanguageSkills
    {
        public int? languageId { get; set; }
        public string language { get; set; }
        public string spokenWritten { get; set; }
        public bool? isSpoken { get; set; }
        public bool? isWritten { get; set; }
        public int? employeeId { get; set; }
        public int? createdBy { get; set; }
        public int? languageTypeId { get; set; }
        public string languageOther { get; set; }

    }
}
