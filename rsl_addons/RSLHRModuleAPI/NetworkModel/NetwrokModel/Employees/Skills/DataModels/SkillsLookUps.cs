﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class SkillsLookUps
    {
        public List<LookUpResponseModel> qualificationLevel { get; set; }
        public List<LookUpResponseModel> equivalentLevel { get; set; }
        public List<LookUpResponseModel> languageTypes { get; set; }
    }
}
