﻿using System.Collections.Generic;

namespace NetworkModel
{
    public class BenefitsDetailResponse
    {

        public BenefitsDetailResponse()
        {
            this.vehiclelst =new  List<BenefitVehicle>();
        }
        public BenefitsDetail benefits { get; set; }
        public BenefitsLookUps lookUps { get; set; } = new BenefitsLookUps();
        public List<BenefitVehicle>  vehiclelst { get; set; }

    }
}
