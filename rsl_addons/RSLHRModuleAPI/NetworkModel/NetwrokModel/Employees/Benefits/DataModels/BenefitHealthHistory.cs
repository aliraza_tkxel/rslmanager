﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class BenefitHealthHistory
    {
        public int benefitId { get; set; }
        public int? employeeId { get; set; }
        public int fiscalYear { get; set; }
        public string fiscalYears { get; set; }
        public int medicalOrgId { get; set; }
        public int? isPrivateMedical { get; set; }
        public string Type { get; set; }
        public string Provider { get; set; }
        public decimal? excessPaid { get; set; }
        public string effectiveFrom { get; set; }
        public string endDate { get; set; }
        public decimal? annualPremium { get; set; }
        public string membershipNumber { get; set; }
    }
}
