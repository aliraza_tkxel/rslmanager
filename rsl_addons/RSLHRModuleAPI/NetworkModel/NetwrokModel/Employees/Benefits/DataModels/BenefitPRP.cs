﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
   public class BenefitPRP
    {
        public int benefitPRPId { get; set; }
        public int benefitId { get; set; }
        public int? employeeId { get; set; }
        public int fiscalYear { get; set; }
        public decimal? q1 { get; set; }
        public decimal? q2 { get; set; }
        public decimal? q3 { get; set; }
        public decimal? q4 { get; set; }     
        public bool isReadOnly { get; set; }

    }
}
