﻿using System.Collections.Generic;

namespace NetworkModel
{
    public class BenefitsLookUps
    {
        public List<LookUpResponseModel> medicalOrganizationList { get; set; }
        public List<LookUpResponseModel> medicalTypeList { get; set; }
        public List<LookUpResponseModel> schemesList { get; set; }

        public List<LookUpResponseModel> benefitTypeList { get; set; }

        public List<LookUpResponseModel> fiscalYearList { get; set; }
    }
}
