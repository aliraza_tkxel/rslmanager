﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
   public class GeneralDataRequest
    {
        public int fiscalYear { get; set; }
        public int? employeeId { get; set; }
        public int benefitId { get; set; }
    }
}
