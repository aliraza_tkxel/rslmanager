﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
   public class BenefitSubscription
    {
        public int benefitSubscriptionId { get; set; }
        public int fiscalYear { get; set; }
        public string fiscalYearName { get; set; }
        public int benefitId { get; set; }
        public int? employeeId { get; set; }
        public string subscriptionTitle { get; set; }
        public decimal? subscriptionCost { get; set; } 
        public bool isReadOnly { get; set; }
        [JsonIgnore]
        public DateTime? YStart { get; set; }
        [JsonIgnore]
        public DateTime? YEnd { get; set; }

    }
}
