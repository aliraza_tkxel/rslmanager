﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
  public  class CurrentFinancialYear
    {
        public int YRange { get; set; }
        public DateTime? YStart { get; set; }
        public DateTime? YEnd { get; set; }
    }
}
