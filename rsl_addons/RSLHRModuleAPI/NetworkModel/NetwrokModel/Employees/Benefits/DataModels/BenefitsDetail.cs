﻿using System;
using System.Collections.Generic;

namespace NetworkModel
{
    public class BenefitsDetail
    {
        public BenefitsDetail()
        {
            vehicle = new BenefitVehicle();
            general = new BenefitGeneral();
            health = new BenefitHealth();
            vehiclelst = new List<BenefitVehicle>();
            subscription = new BenefitSubscription();
            subscriptionlst = new List<BenefitSubscription>(); 
    }
        public int benefitId { get; set; }
        public int employeeId { get; set; }

        //Accomodation
        public decimal? accomodationRent { get; set; }
        public decimal? councilTax { get; set; }
        public decimal? heating { get; set; }
        public decimal? lineRental { get; set; }
        //Company Car
        public string carMake { get; set; }
        public string model { get; set; }
        public decimal? contHireCharge { get; set; }
        public decimal? empContribution { get; set; }
        public decimal? excessContribution { get; set; }
        public decimal? listPrice { get; set; }
        public DateTime? carStartDate { get; set; }
        public double? co2Emissions { get; set; }
        public string fuel { get; set; }
        public decimal? compEmpContribution { get; set; }
        public string drivingLicenceNo { get; set; }
        public string motCertNo { get; set; }
        public string insuranceNo { get; set; }
        public DateTime? insuranceRenewalDate { get; set; }
        public string drivingLicenceImage { get; set; }
        // Essential User
        public decimal? carAllowance { get; set; }
        public string engineSize { get; set; }
        public string additionalDriver { get; set; }
        public decimal? ecuTopUp { get; set; }
        public DateTime? motRenewalDate { get; set; }
        public decimal? toolAllowance { get; set; }
        public decimal? firstAid { get; set; }
        public decimal? unionSubscription { get; set; }
        public decimal? lifeAssurance { get; set; }
        public int? enhancedHolidayEntitlement { get; set; }
        public decimal? carParkingFacilities { get; set; }
        public decimal? eyeCareAssistance { get; set; }
        public string employeeAssistanceProgramme { get; set; }
        public decimal? enhancedSickPay { get; set; }
        public decimal? learningAndDevelopment { get; set; }
        public decimal? professionalSubscriptions { get; set; }
        public decimal? childcareVouchers { get; set; }
        public decimal? fluAndHepBJabs { get; set; }
        //---------General------------------
        public decimal? professionalFees { get; set; }
        public decimal? telephoneAllowance { get; set; }
        public decimal? firstAidAllowance { get; set; }
        public decimal? callOutAllowance { get; set; }

        /////////////////////// BUPA Medical Insurance///////////////
        public int? medicalOrgId { get; set; }
        public decimal? taxableBenefit { get; set; }
        public decimal? annualPremium { get; set; }
        public string additionalMembers { get; set; }
        public string additionalMembers2 { get; set; }
        public string additionalMembers3 { get; set; }
        public string additionalMembers4 { get; set; }
        public string additionalMembers5 { get; set; }
        public string groupSchemeRef { get; set; }
        public string membershipNo { get; set; }

        /////////////////////// Pension/////////
        public string memNumber { get; set; }
        public int? scheme { get; set; }
        public string schemeNumber { get; set; }

        public double? salaryPercent { get; set; }

        public decimal? employeeContribution { get; set; }
        public decimal? employerContribution { get; set; }

        public decimal? avc { get; set; }
        public int? contractedOut { get; set; }
        
        public int? lastActionUser { get; set; }
        public int? carLoan { get; set; } 
        public string requestType { get; set; }
        public BenefitVehicle vehicle { get; set; }
        public BenefitGeneral general { get; set; }
        public BenefitHealth health { get; set; }
        public BenefitPension pension { get; set; }
        public BenefitPRP prp { get; set; }
        public List<BenefitVehicle> vehiclelst { get; set; }

        public BenefitSubscription subscription { get; set; }
        public List<BenefitSubscription> subscriptionlst { get; set; } 
    }
}
