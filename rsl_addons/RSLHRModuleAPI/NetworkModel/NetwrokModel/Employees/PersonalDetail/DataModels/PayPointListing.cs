﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class PayPointListing
    {
        public List<PaypointReportDetail> payPoints { get; set; }
        public Pagination pagination { get; set; }
    }
    public class PaypointReportDetail
    {
        public string name { get; set; }
        public string directorate { get; set; }
        public string team { get; set; }
        public int? grade { get; set; }
        public int? gradePoint { get; set; }
        public string gradeDescription { get; set; }
        public string gradePointDescription { get; set; }
        public string reviewDate { get; set; }
        public string status { get; set; }
        public string statusDisplay { get; set; }
        public string submittedByName { get; set; }
        public int employeeId { get; set; }
        public int payPointId { get; set; }
        public int? execDirectorId { get; set; }
        public int? SupportedBy { get; set; }
        public int? AuthorizedBy { get; set; }
        public string imagePath { get; set; }
        public string reason { get; set; }
    }
    public class PaypointReportLookUps
    {
        public List<LookUpResponseModel> fiscalYears { get; set; }
        public List<LookUpResponseModel> directorateLookUps { get; set; }
        public List<LookUpResponseModel> teamLookUps { get; set; }
        public List<LookUpResponseModel> paypointStatus { get; set; }
    }
}
