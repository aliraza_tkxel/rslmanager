﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GiftsEmailNotification
    {
        public int giftId { get; set; }
        public string employeeName { get; set; }
        public string employeeEmail { get; set; }
        public string details { get; set; }
        public string givenReceivedDate { get; set; }
        public string value { get; set; }
        public string status { get; set; }
        public string reason { get; set; }
        public string dateOfDeclaration { get; set; }
        public string lineManagerName { get; set; }
        public string lineManagerEmail { get; set; }
        public string acceptedMessage { get; set; }
    }
}
