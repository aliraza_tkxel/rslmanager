﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class Gift
    {
        public int giftId { get; set; }
        public string details { get; set; }
        public DateTime? dateGivenReceived { get; set; }
        public string offerToBy { get; set; }
        public double? approximateValue { get; set; }
        public bool? accepted { get; set; }
        public DateTime? notApprovedDate { get; set; }
        public DateTime? dateOfDeclaration { get; set; }
        public string notes { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? dateRevieved { get; set; }
        public DateTime? dateGiven { get; set; }
        public string offeredBy { get; set; }
        public string offeredTo { get; set; }
        public int? isGivenReceived { get; set; }
        public int? employeeId { get; set; }
        public int? fiscalYearId { get; set; }
        public string status { get; set; }
        public string createdBy { get; set; }
        public int updatedBy { get; set; }
        public DateTime? updatedDate { get; set; }
        public string reason { get; set; }
        public int? isAccepted { get; set; }

    }
}
