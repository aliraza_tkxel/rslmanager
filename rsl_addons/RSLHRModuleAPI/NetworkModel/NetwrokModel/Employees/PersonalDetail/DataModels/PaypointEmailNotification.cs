﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class PaypointEmailNotification
    {
        public int paypointId { get; set; }
        public string employeeName { get; set; }
        public string currentGradePoint { get; set; }
        public string newGradePoint { get; set; }
        public string newSalary { get; set; }
        public DateTime? nextPayPointReviewDate { get; set; }
        public string lineManagerName { get; set; }
        public string lineManagerEmail { get; set; }
        public string status { get; set; }

        public int? employeeId { get; set; }
        public int? lineManagerId { get; set; }
        public int? directorId { get; set; }
        public string directorName { get; set; }
    }
}
