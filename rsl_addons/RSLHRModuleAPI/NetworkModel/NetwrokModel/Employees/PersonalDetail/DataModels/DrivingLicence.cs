﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel 
{
   public class DrivingLicence
    {
        public int drivingLicenceId { get; set; }
        public int? employeeId { get; set; }
        public string drivingLicenceType { get; set; }
        public string drivingLicenceNumber { get; set; }
        public string drivingLicenceIssueNumber { get; set; }
        public DateTime? expiryDate { get; set; }
        public string expiry { get; set; }
        public string medicalHistory { get; set; }
        public int? createdBy { get; set; }
        public string licenceImage { get; set; }

        public List<DrivingLicenceCategories> categories { get; set; } = new List<DrivingLicenceCategories>();
    }
}
