﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class EmployeePersonalDetail
    {
        public int employeeId { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public int? title { get; set; }
        public string aka { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public string niNumber { get; set; }
        public string dbsDate { get; set; }
        public string reference { get; set; }
        public int? maritalStatus { get; set; }
        public int? ethnicity { get; set; }
        public int? religion { get; set; }
        public int? sexualOrientation { get; set; }
        public string imagePath { get; set; }
        public string profile { get; set; }
        public bool isFiscalYearGiftEntered { get; set; }
        public string paypointReviewDate { get; set; }
        public bool isManager { get; set; }
        public bool? isDoiCreated { get; set; }
        public DateTime? actionDate { get; set; }
        public string statusDisplay { get; set; }
        public int paypointId { get; set; }

        public DateTime? _dob { get; set; }
        public DateTime? _dbsDate { get; set; }
        public DateTime? doiCreationDate { get; set; }
        public DateTime? doiUpdationDate { get; set; }
        public DateTime? doiReviewedDate { get; set; }
        
        public int? doiStatusId { get; set; }
        public string doiStatusName { get; set; }
        public int trainingCount { get; set; }

    }
}
