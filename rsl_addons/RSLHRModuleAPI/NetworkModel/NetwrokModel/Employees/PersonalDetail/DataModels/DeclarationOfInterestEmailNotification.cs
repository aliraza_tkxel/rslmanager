﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class DeclarationOfInterestEmailNotification
    {
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public int lineManagerId { get; set; }
        public string lineManagerName { get; set; }
        public string lineManagerEmail { get; set; }
    }
}
