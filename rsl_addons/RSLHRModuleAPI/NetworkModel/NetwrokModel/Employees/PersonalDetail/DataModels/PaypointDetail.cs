﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class PaypointDetail
    {
        public int ceoId { get; set; }
        public int execDirectorId { get; set; }
        public int? lineManager { get; set; }
        public string employeeName { get; set; }
        public string jobTitle { get; set; }
        public decimal? salary { get; set; }
        public string grade { get; set; }
        public string gradePoint { get; set; }
        public int? isBRS { get; set; }
        public Paypoint payPoint { get; set; }       
        public List<LookUpResponseModel> gradePointLookups { get; set; }
        public List<LookUpResponseModel> gradeLookups { get; set; }

    }
}
