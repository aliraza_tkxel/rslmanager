﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel 
{
   public class LicenceDisqualifications
    {
        public int? employeeId { get; set; }
        public int? numberofPoints { get; set; }
        public string code { get; set; }
        public string reason { get; set; }
        public DateTime? issuedDate { get; set; }
        public DateTime? expiryDate { get; set; }
    }
}
