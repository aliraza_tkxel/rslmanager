﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GiftAndHospitality
    {
        public List<Gift> gifts { get; set; }
        public int fiscalYearId { get; set; }
        public string documentPath { get; set; }
        public int createdBy { get; set; }
        public int employeeId { get; set; }

    }
}
