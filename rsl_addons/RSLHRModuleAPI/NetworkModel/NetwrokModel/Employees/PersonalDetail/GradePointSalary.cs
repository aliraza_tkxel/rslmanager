﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GradePointSalary
    {
        public int employeeId { get; set; }
        public int grade { get; set; }
        public int point { get; set; }
        public int? fiscalYear { get; set; }
    }
}
