﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class PayppointReportResponce
    {
        public PayPointListing PayPointList { get; set; }
        public PaypointReportLookUps lookUps { get; set; } = new PaypointReportLookUps();
        public int ceoId { get; set; }
        public int execDirectorId { get; set; }


    }
}
