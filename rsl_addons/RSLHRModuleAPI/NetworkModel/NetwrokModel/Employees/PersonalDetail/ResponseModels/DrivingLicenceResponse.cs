﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class DrivingLicenceResponse
    {
        public DrivingLicence drivingLicence { get; set; }
        public List<LicenceDisqualifications> disqualificationsList { get; set; }
    }
}
