﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class PersonalDetailResponce
    {
        public EmployeePersonalDetail employeeDetail { get; set; }
        public SharedLookUps lookUps { get; set; }
    }
}
