﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GiftMasterResponse
    {
        public int typeId { get; set; }
        public int statusId { get; set; }
        public string @from { get; set; }
        public string to { get; set; }
        public List<Gift> giftList { get; set; }
        public Pagination pagination { get; set; } = new Pagination();
        public List<LookUpResponseModel> giftStatuses { get; set; }
    }
}
