﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
   public  class DeclarationOfInterestResponse
    {
        public DeclarationOfInterestDataRequest declarationOfInterest { get; set; }
        public DeclarationOfInterestLookUpscs lookUps { get; set; } = new DeclarationOfInterestLookUpscs();
    }
}
