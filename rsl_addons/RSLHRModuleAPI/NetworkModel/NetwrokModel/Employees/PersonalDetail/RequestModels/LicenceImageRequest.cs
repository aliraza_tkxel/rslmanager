﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class LicenceImageRequest
    {

        public int employeeId { get; set; }
        public string filePath { get; set; }
        public int updatedBy { get; set; }


    }
}
