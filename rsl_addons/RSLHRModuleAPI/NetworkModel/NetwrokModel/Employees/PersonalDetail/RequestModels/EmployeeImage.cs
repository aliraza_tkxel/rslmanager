﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class EmployeeImage
    {
        public int employeeId { get; set; }
        public string imageName { get; set; }
        public string profile { get; set; }
    }
}
