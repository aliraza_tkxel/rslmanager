﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GiftsRequest
    {
        public int employeeId { get; set; }
        public int statusId { get; set; }
        public int typeId { get; set; }
        public string from { get; set; }
        public string to { get; set; }
    }
}
