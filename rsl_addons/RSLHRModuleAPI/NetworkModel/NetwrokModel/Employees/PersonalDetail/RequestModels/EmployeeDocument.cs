﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class EmployeeDocument
    {
        public int documentId { get; set; }
        public int documetTypeId { get; set; }
        public int? employeeId { get; set; }
        public string createdDate { get; set; }
        public int? createdBy { get; set; }
        public string documentPath { get; set; }
        public string createdByName { get; set; }


    }
}
