﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class DeclarationOfInterestDataRequest
    {
        public int page { get; set; }
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public string employeeFullName { get; set; }
        public int createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public bool? isActive { get; set; }
        public string comment { get; set; }
        public DateTime? updatedOn { get; set; }
        public string reviewComments { get; set; }
        public DateTime? reviewDate { get; set; }

        public DOIPage1 page1 { get; set; } = new DOIPage1();
        public DOIPage2 page2 { get; set; } = new DOIPage2();
        public DOIPage3 page3 { get; set; } = new DOIPage3();
    }
}
