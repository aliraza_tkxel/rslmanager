﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class UpdatePaypointRequest
    {
        public int paypointId { get; set; }
        public int lastActionUSer { get; set; }    
        public int statusId { get; set; }
        public string reason { get; set; }
        public string nextEligibleDate { get; set; }
    }
}
