﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class UpdateDeclationOfInterestRequest
    {
        public int interestId { get; set; }
        public int employeeId { get; set; }
        public int lastActionUser { get; set; }
        public int statusId { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
        public string reviewComments { get; set; }
        public DateTime? reviewDate { get; set; }

        public DateTime? updatedOn { get; set; }
    }
}
