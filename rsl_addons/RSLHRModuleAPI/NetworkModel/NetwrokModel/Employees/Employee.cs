﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class Employee
    {
        public int employeeId { get; set; }
        public string name { get; set; }
        public string team { get; set; }
        public string jobTitle { get; set; }
        public string grade { get; set; }
        [JsonIgnore]
        public int? status { get; set; }
        [JsonIgnore]
        public int? teamId { get; set; }
    }
}
