﻿using NetworkModel.NetwrokModel.Common;

namespace NetworkModel
{
    public class UpdateEmployeeDetailResponce: BaseResponce
    {
        public EmployeeContactDetail contactDetail { get; set; }
        public EmployeePersonalDetail personalDetail { get; set; }
    }
}
