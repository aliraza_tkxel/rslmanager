﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class HolidayRule
    {
        public int eId { get; set; }
        public string holidayRule { get; set; }
        public double? HolidayDay { get; set; }
        public double? maxHolidayDay { get; set; }
    }
}
