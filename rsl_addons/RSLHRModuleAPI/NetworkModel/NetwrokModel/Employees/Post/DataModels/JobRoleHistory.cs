﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class JobRoleHistory
    {
        public DateTime? createdDate { get; set; }
        public string jobRole { get; set; }
        public string team { get; set; }
        public string createdby { get; set; }
    }
}
