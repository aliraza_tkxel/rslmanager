﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class DayTimingViewModel
    {
        public int employeeId { get; set; }
        public int weekNumber { get; set; }
        public string day { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public double duration { get; set; } = 1;
        public string LunchStartTime { get; set; }
        public string LunchEndTime { get; set; }
    }
}
