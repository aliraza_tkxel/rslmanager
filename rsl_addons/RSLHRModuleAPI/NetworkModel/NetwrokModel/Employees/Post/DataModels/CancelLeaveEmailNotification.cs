﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class CancelLeaveEmailNotification
    {
        public string employeeEmail { get; set; }
        public string employeeName { get; set; }
        public int employeeId { get; set; }
        public string lineManagerName { get; set;}
    }
}
