﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class JobPostDetail
    {
        public int jobDetailsId { get; set; }

        public int employeeId { get; set; }

        public string startDate { get; set; }

        public string endDate { get; set; }

        public int leavingReason { get; set; }

        public string detailsOfRole { get; set; }

        public int? team { get; set; }

        public int? lineManager { get; set; }
        public int? contractType { get; set; }

        public int? grade { get; set; }

        public decimal? salary { get; set; }

        public string taxOffice { get; set; }

        public string taxCode { get; set; }

        public string payrollNumber { get; set; }

        public double? holidayEntitlementDays { get; set; }

        public double? holidayEntitlementHours { get; set; }

        public string reviewDate { get; set; }


        public int? probationPeriod { get; set; }

        public int? partFullTime { get; set; }

        public double? hours { get; set; }

        public double? employeeLimit { get; set; }

        public int? noticePeriod { get; set; }

        public string dateOfNotice { get; set; }

        public string foreignNationalNumber { get; set; }

        public int? active { get; set; }

        public int? isDirector { get; set; }

        public int? isManager { get; set; }

        public int? gradePoint { get; set; }

        public int? officeLocation { get; set; }

        public int? holidayRule { get; set; }

        public double? carryForward { get; set; }

        public double? bankHoliday { get; set; }
        public double? autoHolidayAdjustment { get; set; }

        public double? totalLeave { get; set; }

        public string alStartDate { get; set; }

        public int? jobRoleId { get; set; }

        public int? lastActionUser { get; set; }

        public string lastActiontime { get; set; }

        public string tupeInDate { get; set; }
        public string tupeOutDate { get; set; }
        public string payPointReviewDate { get; set; }

        public int? holidayIndicator { get; set; }

        public double? fte { get; set; }

        public List<SalaryAmendment> saleryAmendments { get; set; }

        public WorkingHoursParent workingPattern { get; set; } = new WorkingHoursParent();
        public List<JobDetailNoteResponse> notes { get; set; } = new List<JobDetailNoteResponse>(); 
        public string roleFile { get; set; }
        public string employeeContract { get; set; }
        public string teamDirectorName { get; set; }
        public int fixedTermContract { get; set; }
        public double? carryForwardHours { get; set; }
        public double? totalLeaveHours { get; set; }
        public double? maxHolidayEntitlementDays { get; set; }
        public double? maxHolidayEntitlementHours { get; set; }
        public int? ISBRS { get; set; }
        public string annualLeaveEndDate { get; set; }
        public int noteCount { get; set; }
    }
}
