﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class SalaryAmendment
    {
        public int employeeId { get; set; }
        public int salaryAmendmentId { get; set; }
        public string fileName { get; set; }
        public string filePath { get; set; }

    }
}
