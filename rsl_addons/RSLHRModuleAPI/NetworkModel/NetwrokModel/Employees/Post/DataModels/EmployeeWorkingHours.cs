﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class EmployeeWorkingHours
    {
        public int employeeId { get; set; }
        public int wId { get; set; }
        public double? mon { get; set; }
        public double? tue { get; set; }
        public double? wed { get; set; }
        public double? thu { get; set; }
        public double? fri { get; set; }
        public double? sat { get; set; }
        public double? sun { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }

        public DateTime? _startDate { get; set; }
        public DateTime? _endDate { get; set; }
        public int? createdBy { get; set; }
        public DateTime? createdOn { get; set; }
        public string createdByName { get; set; }
        public int? lastActionUser { get; set; }
        public double? total { get; set; }
        public int? groupNumber { get; set; }
        public int weekNumber { get; set; }
        public object MonDayTimings { get; set; }
        public object TueDayTimings { get; set; }
        public object WedDayTimings { get; set; }
        public object ThuDayTimings { get; set; }
        public object FriDayTimings { get; set; }
        public object SatDayTimings { get; set; }
        public object SunDayTimings { get; set; }
        public string DayTimings { get; set; }

        public string createdByFname { get; set; }
        public string createdByLname { get; set; }
    }
}
