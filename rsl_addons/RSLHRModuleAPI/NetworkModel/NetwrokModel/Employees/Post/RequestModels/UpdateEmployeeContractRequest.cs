﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class UpdateEmployeeContractRoleRequest
    {
        public int employeeId { get; set; }
        public string filePath { get; set; }
        public string rolePath { get; set; }
        public int  updatedBy { get; set; }
    }
}
