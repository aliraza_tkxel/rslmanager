﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class JobDetailNoteResponse
    {
        public int jobDetailNoteID { get; set; }
        public int employeeId { get; set; }
        public DateTime _createdDate { get; set; }
        public string createdDate { get; set; }
        public int createdBy { get; set; }
        public string createdByName { get; set; }
        public string note { get; set; }
    }
}
