﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class PostTabResponse
    {
        public JobPostDetail jobDetail { get; set; }
        public EmployeeLookUps postTabLookups { get; set; }
    }
}
