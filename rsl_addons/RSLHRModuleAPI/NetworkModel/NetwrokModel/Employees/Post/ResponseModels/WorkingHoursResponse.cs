﻿using NetworkModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class WorkingHoursParent
    {
        public List<EmployeeWorkingHours> workingHours { get; set; }
        public List<EmployeeWorkingHours> workingHoursHistory { get; set; }
        public int employeeId { get; set; }
        public int? lastActionUser { get; set; }
        public double? total { get; set; }
    }
}
