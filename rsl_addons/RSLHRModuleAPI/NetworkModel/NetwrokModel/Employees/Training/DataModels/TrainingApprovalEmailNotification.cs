﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class TrainingApprovalEmailNotification
    {
        public int trainingId { get; set; }
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public string directorName { get; set; }
        public int lineManagerId { get; set; }
        public string lineManagerName { get; set; }
        public string lineManagerEmail { get; set; }
        public string courseName { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string trainingStatus { get; set; }
        public string statusTitle { get; set; }
        public string UpdateBy { get; set; }
        public string notes { get; set; }
        public Nullable<decimal> totalCost { get; set; }
    }
}
