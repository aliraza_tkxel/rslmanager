﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class ApprovedTraining
    {
        public string approvedtraining { get; set; }
        public bool? isRemuneration { get; set; }
        public decimal? remunerationCost { get; set; }
    }
}
