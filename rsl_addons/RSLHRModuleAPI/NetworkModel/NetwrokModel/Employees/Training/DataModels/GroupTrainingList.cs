﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GroupTrainingList
    {
        public List<GroupTraining> groupTrainings { get; set; }
        public Pagination pagination { get; set; }
    }
}
