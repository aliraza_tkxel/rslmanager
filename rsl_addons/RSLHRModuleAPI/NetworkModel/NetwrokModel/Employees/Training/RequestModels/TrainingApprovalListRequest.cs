﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class TrainingApprovalListRequest
    {
        public string searchText { get; set; }
        public int statusId { get; set; }
        public int directorateId { get; set; }
        public int teamId { get; set; }
        public int employeeId { get; set; } = 0;
        public bool isHR { get; set; }
        public bool isDirector { get; set; }
        public int loginUserId { get; set; }
        public bool isSearch { get; set; }
    }
}
