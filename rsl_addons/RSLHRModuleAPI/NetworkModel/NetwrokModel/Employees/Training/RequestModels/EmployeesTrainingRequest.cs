﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class EmployeesTrainingRequest
    {
        public string searchText { get; set; }
        public int? status { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public int? teamId { get; set; }

        public int? directorateId { get; set; }
        public Pagination pagination { get; set; }
    }
}
