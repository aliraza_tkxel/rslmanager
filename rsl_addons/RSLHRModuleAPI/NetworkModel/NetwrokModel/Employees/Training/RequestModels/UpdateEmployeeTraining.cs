﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class UpdateEmployeeTraining
    {
        public int trainingId { get; set; }
        public int? updatedBy { get; set; }
        public bool? isActive { get; set; }
        public string status { get; set; }
    }
}
