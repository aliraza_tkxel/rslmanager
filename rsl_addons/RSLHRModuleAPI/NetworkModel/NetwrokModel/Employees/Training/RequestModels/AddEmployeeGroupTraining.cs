﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace NetworkModel
{
    public class AddEmployeeGroupTraining
    {

        public int trainingId { get; set; }
        public int?[] employeeId { get; set; }
        public string[] employeeName { get; set; }
        public string justification { get; set; }
        public string course { get; set; }
        [JsonIgnore]
        public DateTime? startDateTime { get; set; }
        [JsonIgnore]
        public DateTime? endDateTime { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int? groupId { get; set; }
        public int? totalNumberOfDays { get; set; }
        public string providerName { get; set; }
        public string providerWebsite { get; set; }
        public string location { get; set; }
        public string venue { get; set; }
        public decimal? totalCost { get; set; }
        public int? isSubmittedBy { get; set; }
        public bool? isMandatoryTraining { get; set; }
        [JsonIgnore]
        public DateTime? expiryDate { get; set; }
        public string expiry { get; set; }
        [JsonIgnore]
        public DateTime? createdDateTime { get; set; }
        public string createdDate { get; set; }
        public string additionalNotes { get; set; }
        public string status { get; set; }        
        public int? createdby { get; set; }
        public string approvedtraining { get; set; }
        public bool? isRemuneration { get; set; }
        public int? professionalQualification { get; set; }
        public decimal? remunerationCost { get; set; }
        public string postcode { get; set; }
        public List<EmployeeDetails> employeeDetails { get; set; } = new List<EmployeeDetails>();
    }
}
