﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GroupTrainingApproveDecline
    {
        public int?[] trainingId { get; set; }
        public int?[] employeeId { get; set; }
        public int statusId { get; set; }
        public string status { get; set; }
        public int updatedBy { get; set; }
        
    }
}
