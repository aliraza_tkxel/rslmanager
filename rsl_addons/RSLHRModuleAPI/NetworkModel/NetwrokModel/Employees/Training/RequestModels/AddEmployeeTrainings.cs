﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class AddEmployeeTrainings
    {
        public int trainingId { get; set; }
        public int? employeeId { get; set; }
        public string employeeName { get; set; }
        public string justification { get; set; }
        public string course { get; set; }
        [JsonIgnore]
        public DateTime? startDateTime { get; set; }
        [JsonIgnore]
        public DateTime? endDateTime { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        [JsonIgnore]
        public DateTime? startDateString { get; set; }
        [JsonIgnore]
        public DateTime? endDateString { get; set; }
        public int? totalNumberOfDays { get; set; }
        public string providerName { get; set; }
        public string providerWebsite { get; set; }
        public string location { get; set; }
        public string venue { get; set; }
        public int? jobStatus { get; set; }
        public decimal? totalCost { get; set; }
        public int? isSubmittedBy { get; set; }
        public bool? isMandatoryTraining { get; set; }
        [JsonIgnore]
        public DateTime? expiryDate { get; set; }
        public string expiry { get; set; }
        public string additionalNotes { get; set; }
        public string jobTitle { get; set; }  
        public string directorateName { get; set; }  
        public string teamName { get; set; }    
        public string byName { get; set; }   
        public string status { get; set; }
        public int? createdby { get; set; }
        public string approvedtraining { get; set; }
        public bool? isRemuneration { get; set; }
        public decimal? remunerationCost { get; set; }
        [JsonIgnore]
        public DateTime? recordedDateTime { get; set; }
        public string recordedDate { get; set; }
        [JsonIgnore]
        public DateTime? renewDateTime { get; set; }
        public string renewDate { get; set; }
        public string createdByName { get; set; }
        [JsonIgnore]
        public DateTime? createdDateTime { get; set; }
        public string createdDate { get; set; }
        public int? professionalQualification { get; set; }
        public string postcode { get; set; }
        //public List<ApprovedTraining> approvedTraining { get; set; }

    }
}
