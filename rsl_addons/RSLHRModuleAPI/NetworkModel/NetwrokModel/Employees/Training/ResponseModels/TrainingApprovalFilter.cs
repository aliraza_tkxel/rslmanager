﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class TrainingApprovalFilter
    {
        public List<LookUpResponseModel> directorates { get; set; }
        public List<LookUpResponseModel> teamlookups { get; set; }
        public List<LookUpResponseModel> employees { get; set; }
        public List<LookUpResponseModel> trainingStatus { get; set; }
    }
}
