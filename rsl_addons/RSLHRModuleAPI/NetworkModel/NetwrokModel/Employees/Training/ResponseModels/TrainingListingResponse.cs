﻿using NetwrokModel.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class TrainingListingResponse
    {
        public List<AddEmployeeTrainings> trainingList { get; set; }
        public EstablishmentlookUps lookUps { get; set; } = new EstablishmentlookUps();
        public Pagination pagination { get; set; }

    }
}
