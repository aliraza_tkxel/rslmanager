﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GroupTrainingApprovalListResponse
    {
        public List<TrainingApprovalList> trainingApprovalList { get; set; } = new List<TrainingApprovalList>();
        public TrainingApprovalFilter lookUps { get; set; } = new TrainingApprovalFilter();

        public int directorate { get; set; }
    }
}
