﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class TrainingApprovalLookupResponse
    {
        public TrainingApprovalFilter lookUps { get; set; } = new TrainingApprovalFilter();
    }
}
