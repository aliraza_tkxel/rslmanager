﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class EmployeeDetails
    {
        public int employeeId { get; set; }
        public string employeeName { get; set; }
        public string employeeEmail { get; set; }
        public string teamName { get; set; }
        public string lineManagerName { get; set; }
        public int lineManagerId { get; set; }
        public string lineManagerEmail { get; set; }
        public int? directorId { get; set; }
    }
}
