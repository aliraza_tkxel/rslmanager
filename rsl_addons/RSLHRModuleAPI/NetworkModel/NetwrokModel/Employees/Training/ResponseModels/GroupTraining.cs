﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GroupTraining
    {
        public string recordedDate { get; set; }
        public DateTime? createdDate { get; set; }
        public string startDate { get; set; }
        public string courseName { get; set; }
        public string providerName { get; set; }
        public int attendees { get; set; }
        public string renewDate { get; set; }
        public string createdBy { get; set; }
        public bool? isMandatory { get; set; }
        public int? groupId { get; set; }

    }
}
