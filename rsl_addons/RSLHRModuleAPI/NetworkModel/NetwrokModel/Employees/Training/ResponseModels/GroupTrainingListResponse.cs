﻿using NetwrokModel.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class GroupTrainingListResponse
    {
        public GroupTrainingList groupTrainingList { get; set; } = new GroupTrainingList();
        public AnnualLeaveLookUps lookUps { get; set; } = new AnnualLeaveLookUps();

    }
}
