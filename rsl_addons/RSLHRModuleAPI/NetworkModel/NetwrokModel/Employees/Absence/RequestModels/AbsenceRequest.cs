﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class AbsenceRequest
    {
        public int employeeId { get; set; }
        public string fiscalYearDate { get; set; }
    }
}
