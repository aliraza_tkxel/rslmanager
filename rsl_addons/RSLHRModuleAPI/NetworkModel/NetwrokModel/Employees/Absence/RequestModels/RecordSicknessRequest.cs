﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class RecordAbsenceRequest
    {
        public int employeeId { get; set; }
        public int absenceHistoryId { get; set; }
        public int reasonId { get; set; }
        public string startDate { get; set; }
        [JsonIgnore]
        public DateTime? startDateString { get; set; }
        public string endDate { get; set; }

        [JsonIgnore]
        public DateTime? endDateString { get; set; }
        public float? duration  { get; set; }
        public int recordedBy { get; set; }
        public string notes { get; set; }
        public string holType { get; set; }
        public string reason { get; set; }
        public int natureId { get; set; }
        public string anticipatedReturnDate { get; set; }
        [JsonIgnore]
        public DateTime? anticipatedReturnDateString { get; set; }

    }
}
