﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class RecordInternalMeetingRequest
    {
        public string startDate { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public float duration { get; set; }
        public int recordedBy { get; set; }
        public int employeeId { get; set; }
        public string notes { get; set; }
        public string title { get; set; }
    }
}
