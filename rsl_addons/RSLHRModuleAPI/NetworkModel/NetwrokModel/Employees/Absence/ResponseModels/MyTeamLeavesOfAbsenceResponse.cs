﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class MyTeamLeavesOfAbsenceResponse
    {
        public List<MyTeamLeaveOfAbsencePending> myTeamLeavesPending { get; set; }
        public List<LookUpResponseModel> leavesNatures { get; set; }
        public List<DateTime> bankHolidays { get; set; }
    }
}
