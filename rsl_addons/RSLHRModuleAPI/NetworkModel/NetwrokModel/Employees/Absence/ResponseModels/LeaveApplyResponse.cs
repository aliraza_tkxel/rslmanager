﻿using NetworkModel.NetwrokModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class LeaveApplyResponse
    {
        public List<EmployeeWorkingPattern> workingPattern { get; set; } = new List<EmployeeWorkingPattern>();
    }
}
