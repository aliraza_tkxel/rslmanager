﻿using System;
using System.Collections.Generic;

namespace NetworkModel
{
    public class AnnualLeaveResponce
    {
        public AnnualLeaveDetail leaveDetail { get; set; }

        public List<AbsenceAnnual> absentees { get; set; }

        public List<DateTime> bankHolidays { get; set; }

        public WorkingHoursParent workingPattren { get; set; }

        public string leaveYearStart { get; set; }

        public string leaveYearEnd { get; set; }
    }
}
