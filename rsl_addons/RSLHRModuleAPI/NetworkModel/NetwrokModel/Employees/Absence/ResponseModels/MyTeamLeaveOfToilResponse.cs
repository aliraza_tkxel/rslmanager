﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class MyTeamLeaveOfToilResponse
    {
        public List<MyTeamLeaveOfToilPending> myTeamLeavesPending { get; set; }
        public List<LookUpResponseModel> leavesNatures { get; set; }
        public List<DateTime> bankHolidays { get; set; }
    }
}
