﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class MyTeamLeavesResponse
    {
        public List<MyTeamLeavesPending> myTeamLeavesPending { get; set; }
    }
}
