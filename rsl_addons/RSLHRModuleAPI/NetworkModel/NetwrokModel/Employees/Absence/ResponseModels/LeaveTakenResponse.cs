﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class LeaveTakenResponse
    {
        public List<LeaveTaken> leaveTakenList { get; set; }
    }
}
