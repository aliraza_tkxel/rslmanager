﻿using System;
using System.Collections.Generic;

namespace NetworkModel
{
    public class LeaveOfAbsenceResponce
    {
        public List<AbsenceLeave> absentees { get; set; }
        public List<LookUpResponseModel> leavesNatures { get; set; }
        public List<DateTime> bankHolidays { get; set; }
        public double? toilsOwed { get; set; }
        public WorkingHoursParent workingPattren { get; set; }
    }
}
