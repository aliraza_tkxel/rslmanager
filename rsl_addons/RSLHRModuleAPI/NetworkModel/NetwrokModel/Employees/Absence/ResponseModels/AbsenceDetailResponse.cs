﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class AbsenceDetailResponse
    {
        public List<AbsenceDetail> myTeamLeavesPending { get; set; }

        public List<LookUpResponseModel> leaveActions { get; set; }
    }
}
