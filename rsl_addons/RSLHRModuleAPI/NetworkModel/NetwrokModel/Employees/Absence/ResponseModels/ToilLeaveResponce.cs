﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class ToilLeaveResponce
    {
        public List<AbsenceLeave> toilLeaves { get; set; }
        public double? timeOfInlieuOwed { get; set; }
        public double? toilHours { get; set; }
        public int lineManager { get; set; }

    }
}
