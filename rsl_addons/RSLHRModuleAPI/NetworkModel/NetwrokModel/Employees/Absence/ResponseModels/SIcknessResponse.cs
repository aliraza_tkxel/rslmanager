﻿using System;
using System.Collections.Generic;

namespace NetworkModel
{
    public class SicknessResponse
    {
        public List<AbsenceLeave> sicknessAbsences { get; set; }
        public List<LookUpResponseModel> absenceReasons { get; set; }
        public List<LookUpResponseModel> sicknessActions { get; set; }
        public List<DateTime> bankHolidays { get; set; }
        public List<StaffMemberSickness> staffMembers { get; set; }
    }
}
