﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class InternalMeetingsLeaveResponse
    {
        public double? absentHours { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }       
        public string nature { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public string holidayType { get; set; }
        public int? absenceHistoryId { get; set; }
        public string title { get; set; }

    }
}
