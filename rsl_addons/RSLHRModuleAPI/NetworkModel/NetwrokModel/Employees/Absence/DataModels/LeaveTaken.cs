﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class LeaveTaken
    {
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public decimal? duration { get; set; }
        public string unit { get; set; }
        public int? natureId { get; set; }
        public string leaveDescription { get; set; }
        public int statusId { get; set; }
        public string status { get; set; }
        public int? absenceHistoryId { get; set; }
    }
}
