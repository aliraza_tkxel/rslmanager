﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class MyTeamLeavesPending
    {
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public double? duration { get; set; }
        public string unit { get; set; }
        public int? natureId { get; set; }
        public string leaveDescription { get; set; }
        public int statusId { get; set; }
        public string status { get; set; }
        public int? absenceHistoryId { get; set; }
        public int? applicantId { get; set; }
        public string applicantName { get; set; }
        public string holType { get; set; }
    }
}
