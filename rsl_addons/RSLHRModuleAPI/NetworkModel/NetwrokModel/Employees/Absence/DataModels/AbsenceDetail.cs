﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class AbsenceDetail
    {
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public decimal? duration { get; set; }
        public string unit { get; set; }
        public int? natureId { get; set; }
        public string leaveDescription { get; set; }
        public int statusId { get; set; }
        public string status { get; set; }
        public int? absenceHistoryId { get; set; }
        public int? applicantId { get; set; }
        public string applicantName { get; set; }
        public string holType { get; set; }
        public string title { get; set; }
        public string notes { get; set; }
        public string _notes { get; set; }
        public string _reason { get; set; }
    }
}
