﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class MyTeamLeaveOfAbsencePending
    {
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public int statusId { get; set; }
        public Nullable<int> applicantId { get; set; }
        public Nullable<int> natureId { get; set; }
        public string leaveDescription { get; set; }
        public Nullable<double> duration { get; set; }
        public string holType { get; set; }
        public string applicantName { get; set; }
        public Nullable<int> absenceHistoryId { get; set; }
        public Nullable<double> _duration { get; set; }
        public string unit { get; set; }
    }
}
