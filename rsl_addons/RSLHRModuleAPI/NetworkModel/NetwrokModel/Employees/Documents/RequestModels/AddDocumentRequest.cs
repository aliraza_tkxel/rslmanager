﻿namespace NetworkModel
{
    public class AddDocumentRequest
    {
        public int? documentId { get; set; }
        public int documentTypeId { get; set; }
        public int documentVisibilityId { get; set; }
        public string title { get; set; }
        public string documentDate { get; set; }
        public string documentExpiry { get; set; }
        public int employeeId { get; set; }        
        public int? createdBy { get; set; }
        public string documentPath { get; set; }
        public string keyWords { get; set; }
        public bool? employeeVisibility { get; set; } = true;

    }
}
