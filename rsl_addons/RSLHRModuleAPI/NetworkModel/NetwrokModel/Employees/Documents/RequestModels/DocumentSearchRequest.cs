﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
   public class DocumentSearchRequest
    {
        public int employeeId { get; set; }
        public int? documetTypeId { get; set; }
        public string title { get; set; }
        public string keyWords { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public bool employeeVisibility { get; set; } = true;
        public int loggedInUser { get; set; }
        public Pagination pagination { get; set; }

    }
}
