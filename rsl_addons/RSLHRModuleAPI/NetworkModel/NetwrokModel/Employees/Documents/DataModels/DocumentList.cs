﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class DocumentList
    {
        public List<DocumentDetails> documentList { get; set; }

        public Pagination pagination { get; set; }
    }
}
