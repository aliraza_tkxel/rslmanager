﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class DocumentsLookUps
    {
        public List<LookUpResponseModel> documentType { get; set; }
        public List<LookUpResponseModel> documentVisibility { get; set; }
    }
}
