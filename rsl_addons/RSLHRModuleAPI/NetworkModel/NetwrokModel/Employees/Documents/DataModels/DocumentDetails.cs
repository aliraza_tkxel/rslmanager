﻿using Newtonsoft.Json;
using System;

namespace NetworkModel
{
    public class DocumentDetails
    {
        public int documentId { get; set; }
        public int documetTypeId { get; set; }
        public int? documentVisibilityId { get; set; }
        public string documentType { get; set; }
        public string title { get; set; }
        public string documentDate { get; set; }
        public string documentExpiry { get; set; }

        public DateTime? documentDateTime { get; set; }
        public DateTime? documentExpiryTime { get; set; }

        public int? employeeId { get; set; }
        public string createdDate { get; set; }
      
        public string documentPath { get; set; }
        public string createdByName { get; set; }
        public bool? employeeVisibility { get; set; }
        [JsonIgnore]
        public string firstName { get; set; }
        [JsonIgnore]
        public string lastName { get; set; }
    }
}
