﻿using System;
using System.Collections.Generic;

namespace NetworkModel
{
    public class DocumentsResponse
    {
        public DocumentList documentList { get; set; }
        public DocumentsLookUps lookUps { get; set; } = new DocumentsLookUps();

    }
}
