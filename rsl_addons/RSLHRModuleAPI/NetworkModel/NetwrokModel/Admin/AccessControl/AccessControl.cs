﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class AccessControl
    {
        public string userName { get; set; }
        public int? loginId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public DateTime? EXPIRES { get; set; }
        public string confirmation { get; set; }
        public int? isActive { get; set; }
        public bool? isLocked { get; set; }
        public int employeeId { get; set; }
        public int currentUserId { get; set; }
    }
}
