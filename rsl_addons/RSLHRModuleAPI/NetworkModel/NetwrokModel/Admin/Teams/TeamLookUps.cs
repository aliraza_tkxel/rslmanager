﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class TeamLookUps
    {
        public List<LookUpResponseModel> managers { get; set; }
        public List<LookUpResponseModel> directorates { get; set; }
        public List<LookUpResponseModel> directors { get; set; }
    }
}
