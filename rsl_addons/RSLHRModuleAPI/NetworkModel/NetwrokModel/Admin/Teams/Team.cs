﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class Team
    {
        public int teamId { get; set; }
        public string teamName { get; set; }
        public string directorate { get; set; }
        public string teamDirector { get; set; }
        public string teamManager { get; set; }
        public string mainFunction { get; set; }
        public string description { get; set; }
        public int? directorateId { get; set; }
        public int? directorId { get; set; }
        public int? managerId { get; set; }
        public int? active { get; set; }
    }
}
