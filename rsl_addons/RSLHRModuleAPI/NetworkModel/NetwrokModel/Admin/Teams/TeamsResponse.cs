﻿using NetworkModel;
using NetworkModel.NetwrokModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class TeamsResponse : BaseResponce
    {
        public List<Team> teams { get; set; }
        public TeamLookUps DMLookups { get; set; }
        public Pagination pagination { get; set; }
    }
}
