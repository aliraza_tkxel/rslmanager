﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class SharedLookUps
    {
        public List<LookUpResponseModel> absenceNature { get; set; }
        public List<LookUpResponseModel> leaveStatus { get; set; }
        public List<LookUpResponseModel> leaveActions { get; set; }
        public List<LookUpResponseModel> absenceReason{ get; set; }
        public List<LookUpResponseModel> ethnicity { get; set; }
        public List<LookUpResponseModel> sexualOrientation { get; set; }
        public List<LookUpResponseModel> maritalStatus { get; set; }
        public List<LookUpResponseModel> myStaff { get; set; }
        public List<LookUpResponseModel> religion{ get; set; }
        public List<LookUpResponseModel> title { get; set; }
        public List<DateTime> bankHolidays { get; set; }
    }
}

