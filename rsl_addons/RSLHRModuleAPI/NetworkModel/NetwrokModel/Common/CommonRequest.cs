﻿namespace NetworkModel
{
    public   class CommonRequest
    {
        
        public int employeeId { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public int? teamId { get; set; }
        public int? directorate { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string disabilityType { get; set; }
        public string searchText { get; set; }
        public Pagination pagination { get; set; }
    }


  public   class JournalRequest
    {

        public string fromDate { get; set; }
        public string toDate { get; set; }
        public int employeeId { get; set; }
        public string itemType { get; set; }
        public string searchText { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        

        public Pagination pagination { get; set; }
    }


}
