﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class DashboardStatsResponse
    {
        public List<DashBoardStatsLookup> ApprovalStats { get; set; }
        public DashboardStatsResponse()
        {
            ApprovalStats = new List<DashBoardStatsLookup>();
        }
    }
}
