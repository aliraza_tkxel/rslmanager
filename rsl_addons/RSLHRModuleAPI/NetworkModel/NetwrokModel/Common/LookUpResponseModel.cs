﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class LookUpResponseModel
    {
        public int lookUpId { get; set; }
        public string lookUpDescription { get; set; }
        //Only use in E_DIFFDIS Entity and Health Searvice
        [JsonIgnore]
        public int? dType { get; set; }
    }
}
