﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class Pagination
    {

        
        public int pageSize { get; set; }
        public int pageNo { get; set; }
        public int totalRows { get; set; }
        public int totalPages { get; set; }
    }
}


