﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class LeaveStatusChangeRequest
    {
        public int actionBy { get; set; }
        public int absenceHistoryId { get; set; }
        public int actionId { get; set; }
        public string certNo { get; set; }
        public string drName { get; set; }
        public string notes { get; set; }
    }
}
