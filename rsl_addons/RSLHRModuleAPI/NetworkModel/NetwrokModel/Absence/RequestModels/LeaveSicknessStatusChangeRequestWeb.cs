﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class LeaveSicknessStatusChangeRequestWeb
    {
        public int actionBy { get; set; }
        public int absenceHistoryId { get; set; }
        public int actionId { get; set; }
        public string certNo { get; set; }
        public string drName { get; set; }
        public string notes { get; set; }
        public int? reasonId { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string anticipatedReturnDate { get; set; }
        public double? duration { get; set; }
        public string holtype { get; set; }
    }
}
