﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class InvoiceDetail
    {
        public Double totalGross { get; set; }
        public Double purchaseCount { get; set; }
        public DateTime taxDate { get; set; }
        public string invoiceNumber { get; set; }
        public int orderId { get; set; }
    }
}
