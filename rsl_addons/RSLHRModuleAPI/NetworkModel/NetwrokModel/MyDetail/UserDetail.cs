﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class UserDetail
    {
        public ContactDetail contactDetail { get; set; }
        public EmployeePersonalDetail personalDetail { get; set; }

        public UserDetail()
        {
            contactDetail = new ContactDetail();
            personalDetail = new EmployeePersonalDetail();
        }
    }
}
