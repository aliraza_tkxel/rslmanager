﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class ContactDetail
    {
        public string workMobile { get; set; }
        public string addressLine2 { get; set; }
        public string workEmail { get; set; }
        public string homeEmail { get; set; }
        public string addressLine3 { get; set; }
        public string mobile { get; set; }
        public string mobilePersonal { get; set; }
        public string emergencyContactNumber { get; set; }
        public string emergencyContactPerson { get; set; }
        public string emergencyContactRelationship { get; set; }
        public string county { get; set; }
        public string workPhone { get; set; }
        public string postalTown { get; set; }
        public string homePhone { get; set; }
        public string addressLine1 { get; set; }
        public string postalCode { get; set; }
    }
}
