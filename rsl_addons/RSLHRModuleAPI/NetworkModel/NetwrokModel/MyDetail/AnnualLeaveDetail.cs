﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class AnnualLeaveDetail
    {
        public decimal? annualLeaves { get; set; }
        public decimal totalAllowance { get; set; }
        public decimal? leavesBooked { get; set; }
        public decimal? leavesRequested { get; set; }
        public decimal? leavesTaken { get; set; }
        public decimal? leavesRemaining { get; set; }
        public DateTime? startDate { get; set; }
        public string unit { get; set; }
        public decimal  timeOfInLieuOwed { get; set; }
        public decimal toil { get; set; }
        public decimal carryForward { get; set; }
        public decimal bankHoliday { get; set; }
        public double? holidayEntitlementDays { get; set; }
        public double? holidayEntitlementHours { get; set; }
        public double? sickness { get; set; }


    }
}
