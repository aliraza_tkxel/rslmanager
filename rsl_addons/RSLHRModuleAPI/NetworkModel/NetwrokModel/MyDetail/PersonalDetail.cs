﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    public class PersonalDetail
    {
        public string gender { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public int? ethnicity { get; set; }
        public DateTime? dob { get; set; }
        public int? sexualOrientation { get; set; }
        public int? religion { get; set; }
        public int? employeeId { get; set; }
        public int? maritalStatus { get; set; }
    }
}
