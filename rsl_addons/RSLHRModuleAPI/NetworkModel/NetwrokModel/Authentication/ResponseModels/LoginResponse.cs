﻿using NetworkModel;
using NetworkModel.NetwrokModel.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel
{
    /// <summary>
    /// Login Response for Client
    /// </summary>
   public  class LoginResponse
    {
        public UserInfo userInfo { get; set; }

        public SharedLookUps lookUps { get; set; }

        public LoginResponse()
        {
            userInfo = new UserInfo();
        }
    }
}
