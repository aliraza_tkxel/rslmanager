﻿using NetworkModel.NetwrokModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkModel.NetwrokModel.Authentication
{
    public class UserInfo
    {
        public string employeeFullName { get; set; }
        public int? isActive { get; set; }
        public int? userId { get; set; }
        public string userName { get; set; }
        public int? teamId { get; set; }
        public string gender { get; set; }
        public string jobRole { get; set; }
        public DateTime? dob { get; set; }
        public string employmentNature { get; set; }
        public DateTime? alStartDate { get; set; }
        public string leaveYearStart { get; set; }
        public string leaveYearEnd { get; set; }
    }
}
