﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class GeneralHelper
    {

        #region "Set currency Format"
        public static string currencyFormat(string value)
        {
            if (value != "")
            {
                string result = "£" + Convert.ToDecimal(value).ToString("#,##0.00");
                return result;
            }
            else
            {
                return "-";
            }
        }
        #endregion

        #region "Find differences"
        public static List<Tuple<string, object, object>> Differences<T>(T current, T original)
        {
            var diffs = new List<Tuple<string, object, object>>();

            MethodInfo areEqualMethod = typeof(GeneralHelper).GetMethod("AreEqual", BindingFlags.Static | BindingFlags.NonPublic);

            foreach (PropertyInfo prop in typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                object x = prop.GetValue(current);
                object y = prop.GetValue(original);
                bool areEqual = (bool)areEqualMethod.MakeGenericMethod(prop.PropertyType).Invoke(null, new object[] { x, y });

                if (!areEqual)
                {
                    diffs.Add(Tuple.Create(prop.Name, x, y));
                }
            }

            return diffs;
        }
        #endregion
        private static bool AreEqual<T>(T x, T y)
        {
            return EqualityComparer<T>.Default.Equals(x, y);
        }

        public static DateTime GetDateTimeFromString(string dt)
        {
            string[] formats = { "dd/MM/yyyy" };
            var dateTime = DateTime.ParseExact(dt, formats, new CultureInfo("en-GB"), DateTimeStyles.None);
            return dateTime;
        }
    }
}
