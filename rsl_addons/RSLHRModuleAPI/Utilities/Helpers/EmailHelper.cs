﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class EmailHelper
    {
        public static void sendHtmlFormattedEmail(String recepientName, String recepientEmail, String subject, String body)
        {
            try
            {
                body = "<font size='3' face='arial'>" + body + "</font>";
                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail, recepientName));

                //The SmtpClient gets configuration from Web.Config
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);

            }
            catch (IOException)
            {
                throw new ArgumentException(UserMessageConstants.ErrorSendingEmailMsg);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        public static void sendEmail(string body, string subject, string recepientEmail)
        {
            try
            {
                body = "<font size='3' face='arial'>" + body + "</font>";
                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));

                //The SmtpClient gets configuration from Web.Config
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);

            }
            catch (IOException)
            {
                throw new ArgumentException(UserMessageConstants.ErrorSendingEmailMsg);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
    }
}
