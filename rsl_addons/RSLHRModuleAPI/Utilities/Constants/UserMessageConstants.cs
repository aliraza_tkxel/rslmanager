﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class UserMessageConstants
    {
        //Login Messages
        public const string LoginEmailSubjectMsg = "RSL Manager Login Information";
        public const string LoginScccessMsg = "Authenticated";
        public const string LoginFailureMsg = "Invalid username or password!";
        public const string AccessLockdownMsg = "Your account is now locked, please contact the Systems Team for assistance.";
        public const string AccessLockdownWarningMsg = "You have one more login attempt before your access is locked down!";
        public const string IncorrectUsernamePasswordMsg = "Your username or password do not appear to be correct.";
        public const string ErrorSendingEmailMsg = "An error occurred while sending email, please try again.";
        public const string EmailIdNotMatchMsg = "Your email address does not match any of our records, please try again.";
        public const string TerminationExist = "This tenancy is already terminated, and cannot be terminated again";
        public const string ProfilePicUploadedSuccessfully = "Image has been uploaded successfully.";
        public const string TenancyAlreadyExist = "Tenancy already exist";
        public const string PersonalDetailUpdatedSuccessfully = "Personal detail is updated successfully.";
        public const string ContactDetailUpdatedSuccessfully = "Contact detail is updated successfully.";
        public const string AddEmployeeInGroupTrainingSuccessMessage = "Employee is added to group training successfully.";
        public const string RemoveEmployeeFromGroupTrainingSuccessMessage = "Employee is removed from group training successfully.";
        public const string AddEmployeeGroupAlreadyExists = "This employee already exists in this group.";
        public const string UserDetailUpdatedSuccessMessage = "Your detail is updated successfully.";
        public const string HealthSuccessMessage = "Your detail is saved successfully.";
        public const string UpdateProfileSuccessMessage = "Profile is saved successfully.";
        public const string DocumentSuccessMessage = "Document is saved successfully.";
        public const string DocumentDeleteSuccessMessage = "Document is deleted successfully.";
        public const string SkillSuccessMessage = "Skill/Qual is saved successfully.";
        public const string SkillDeleteSuccessMessage = "Skill/Qual is deleted successfully.";
        public const string BenefitSuccessMessage = "Benefit is saved successfully.";
        public const string SalaryAmendmentRemovedSuccessMessage = "Salary Amendment is removed successfully.";
        public const string SalaryAmendmentAddSuccessMessage = "Salary Amendment is added successfully.";
        public const string DocumentUpdateSuccessMessage = "Document is added successfully.";  
        public const string SicknessMarkedSuccessMessage = "Sickness recorded successfully.";
        public const string AnnualLeaveMarkedSuccessMessage = "Annual leave recorded successfully.";
        public const string LeaveAlreadyBookedMessage = "You have already booked the date(s).";
        public const string SickLeaveAlreadyBookedMessage = "You have already booked the date(s) for Sickness Leave(s).";
        public const string SickLeaveErrorMessage = "You have already booked sickness leaves for dates greater than this date.";
        public const string FullDayAlreadyBookedMessage = "A Full Day Sickness leave is already booked for the date.";
        public const string MorningAlreadyBookedMessage = "A Morning Shift Sickness leave is already booked for the date.";
        public const string AfternoonAlreadyBookedMessage = "An Afternoon Shift Sickness leave is already booked for the date.";
        public const string EnterReturnDate = "Please enter a return date smaller than the start date of the current non returned leave. __DATE__{0}{1} __SLOT__{2}{3}";
        public const string LeaveBRSAppointmentBookedMessage = "You have appointments in the booked date(s).";
        public const string RescheduleBRSAppointmentMessage = "There are existing appointment(s) in the operative’s calendar in the booked date(s) and will need to be rescheduled.";
        public const string PersonalDayAlreadyAvailed = "You have already requested Personal day.";
        public const string BirthdayAlreadyAvailed = "You have already requested Birthday Leave.";
        public const string LeaveMarkedSuccessMessage = "Leave recorded successfully.";
        public const string LeaveAbsenceMarkedSuccessMessage = "Leave recorded successfully and TOILS Owed are: {0}";
        public const string SicknessRecordedMessage = "Sickness Recorded.";
        public const string BirthdayRecordedMessage = "Birthday leave recorded successfully.";
        public const string BirthdayRecordedFiscalYear = "Birthday leave is not in Fiscal Year";
        public const string PersonaldayRecordedMessage = "Personal Day leave recorded successfully.";
        public const string TrainingSuccessMessage = "Training is saved successfully.";
        public const string TrainingConflictMessage = "Training conflict with already scheduled training.";
        public const string GroupTrainingSuccessMessage = "Group Training is saved successfully.";
        public const string GroupTrainingUpdateSuccessMessage = "Group Training is updated successfully.";
        public const string TrainingUpdateSuccessMessage = "Training is updated successfully.";
        public const string EmployeeGiftSuccessMessage = "Employee gifts/s added successfully.";
        public const string EmployeeGiftEditSuccessMessage = "Employee gifts/s updated successfully.";
        public const string EmployeePaypointSuccessMessage = "Employee pay point added successfully.";
		public const string DoiSuccessMessage = "Declaration Of Interest is saved successfully.";
        public const string TOILAddSuccess = "Toil added successfully.";
        public const string PaypointUpdateSuccess = "Paypoint updated successfully.";
        public const string DoiStatusSuccessMessage = "DOI updated successfully.";
        public const string UpdatingLeaveError = "Error while updating leave";
        public const string LeaveUpdatedMessage = "Leave {0} successfully.";
        public const string SicknessUpdatedMessage = "Sickness updated.";
        public const string LicenceSuccessMessage = "Driving Licence is saved successfully.";
        public const string LicenceImageSuccessMessage = "Driving licence Image is added successfully.";
        public const string LanguageSkillSuccessMessage = "Language skills is saved successfully.";
        public const string TeamSaveSuccessMessage = "Team is saved successfully.";
        public const string TeamDeleteSuccessMessage = "Team is deleted successfully.";
        public const string LoginUpdateSuccessMessage = "User Login is updated successfully.";
        public const string UnlockAccountSuccessMessage = "Account is unlocked successfully.";
        public const string JobDetailNoteFailureMessage = "Note didn't add successfully.";
    }
}
