﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public enum TrainingEnum
    {
        Hr,
        LineManager,
        GroupTraining,
        AppointmentRearranged,
        TrainingAlreadyExist,
        LineManagerGroupTraining,
        EmployeeGroupTraining,
        ExecApproveDecline,
        HRDeclined,
        HRApproved
    }
}
