﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class ApplicationConstants
    {
        #region property search constants       
        public const string ParameterName = "Quantity";
        public const string ItemName = "Bedrooms";
        public const string propertyStatus = "Available to rent";
        public const string propertySubStatusPendingTenancy = "Pending Tenancy";
        public const string propertySubStatusNotAvailable = "N/A";
        public const string propertyStatusLet = "Let";

        #endregion

        #region Absence Constants
        public const string absenceStatusApproved = "Approved";
        public const string absenceStatusPending = "Pending";
        public const string StatusItemAbsence = "Absence";
        public const string absenceError = "Error";
        public const string leaveActionCancel = "Cancel";
        public const string leaveActionDecline = "Decline";
        public const string lastDayOfAbsence = "Return Date";
        public const string internalMeeting = "Internal Meeting";
        public const string overlap = "Overlap";


        #endregion

        #region Date Format

        public const string ApplicationDateForamt = "dd-MM-yyyy";

        #endregion

        #region
        public const string dashboardStatAnnualLeave = "Annual Leave";
        public const string dashboardStatOtherLeave = "Other Leave";
        public const string dashboardStatBirthdayLeave = "Birthday Leave";
        public const string staffbirthDayPersonal = "Personal day Leave";
        public const string dashboardStatToilLeave = "Toil Leave";
        public const string dashboardStatSickness = "Sickness";
        #endregion

        #region Call By
        public const string callByWeb = "web";
        public const string callByIos = "ios";
        public const string callBy = "callBy";
        #endregion

        #region Leaving Reasons
        public const string leavingReasonVolunatry = "Volunatry";
        public const string leavingReasonInvoluntary = "Involuntary";
        #endregion

        #region
        public const string documentVisibilityAll = "Volunatry";
        public const string documentVisibilityManager = "Manager";
        public const string documentVisibilityHROnly = "HR Only";
        public const string documentVisibilityHRCEO = "HR & CEO";
        public const string documentVisibilityHRExec = "HR & Exec";
        #endregion

        #region >>> Pay Point Status <<<

        public const string submitted = "Submitted";
        public const string supported = "Supported";
        public const string authorized = "Authorised";
        public const string declined = "Declined";
        public const string waitingSubmission = "Waiting Submission";

        #endregion

        #region >>> Group Training <<<
        public const int trainingCostLimit = 1000;
        //public const string grpTrainingStatusSubmitted = "Submitted";
        //public const string grpTrainingStatusApproved = "Approved";
        //public const string grpTrainingStatusDeclined = "Declined";
        //public const string grpTrainingStatusPending = "Pending";

        public const string grpTrainingStatusRemoved = "Removed";
        public const string grpTrainingStatusRequested = "Requested";
        //public const string grpTrainingStatusSupported = "Supported";
        public const string grpTrainingStatusManagerSupported = "Manager Supported";
        public const string grpTrainingStatusExecSupported = "Exec Supported";
        public const string grpTrainingStatusDeclined = "Declined";
        public const string grpTrainingStatusExecApproved = "Exec Approved";
        public const string grpTrainingStatusExecDeclined = "Exec Declined";
        public const string grpTrainingStatusHRApproved = "HR Approved";
        public const string grpTrainingStatusHRDeclined = "HR Declined";

        #endregion

        #region >>> Declaration of Interest Status <<<

        public const string doiSubmitted = "Submitted";
        public const string doiPending = "Pending";
        public const string doiDeclined = "Declined";
        public const string doiApproved = "Approved";
        public const string doiPendingReview = "Pending Review";

        #endregion

        #region >>> Personal Detail <<<

        public const int isGiftReceived = 1;
        public const int isGiftGiven = 2;

        #endregion

        #region >>> Gifts Status <<<

        public const string giftRejected = "Rejected";
        public const string giftDeclared = "Declared";
        public const string giftApproved = "Approved";
        public const string giftPendingApproval = "Pending Approval";

        #endregion

        #region >>> Push Service <<<

        public static string BHG_Dev_Name = "devcrm";
        public static string BHG_Test_Name = "testcrm";
        public static string BHG_Live_Name = "crm";
        public static string BHG_UAT_Name = "uatcrm";
        public const string PushNotificationDeviceTokenIsEmpty = "Push notification: Device token is empty";

        #endregion

        #region Email Constants
        public const string hrTeamTitle = "HR Team";
        public static string hrTeamEmailAddress = ConfigurationManager.AppSettings["hrteambbs"].ToString();
        #endregion


        #region Training
        public const string trainingApprovedTitle = "Approved";
        public const string trainingDeclinedTitle = "Declined";
        public const string trainingStatusExecApproved = "Exec Approved";
        public const string trainingStatusExecDeclined = "Exec Declined";
        public const string notAvailableTitle = "N/A";
        #endregion



    }
}
