﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class EmailSubjectConstants
    {
        public const string DeclarationOfInterest = "Declaration of Interest for Approval";
        public const string LoginEmailSubjectMsg = "RSL Manager Login Information";
        public const string LeaveCancelation = "Working Pattern Amendment";
        public const string TrainingApprovalRequired = "Training Approval Required : {0}";
        public const string TrainingApproval = "Training Approval: {0}";
        public const string AppointmentRearranged = "{0} Appointments to be Rearranged";
        public const string TrainingAlreadyExist = "Training on same dates";
        public const string GiftStatus = "Gift & Hospitality Item : {0}";
        public const string TrainingDeclined = "Training Declined : {0}";
        public const string TrainingApproved = "Training Approved : {0}";
        public const string LineManagerExecStatus = "Training has been : {0}";
        public const string GroupTrainingApproval = "Group Training Approval: {0}";
    }
}
