﻿using NetworkModel;
using NetwrokModel.Reports;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BHGHRModuleAPI.Controllers.Dashboard
{
    public class DashboardController : ABaseController
    {
        public DashboardController(IServiceProxy service) : base(service)
        {

        }

        #region Get Main Detail Alert
        [HttpGet]
        public HttpResponseMessage GetMainDetailAlert(int teamId)
        {
            log.Debug("Enter:");
            try
            {
                int response = Services.DashboardService.GetMainDetailAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Main Detail Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Full Time Staff Alert
        [HttpGet]
        public HttpResponseMessage GetFullTimeStaffAlert(int teamId)
        {

            try
            {
                int response = Services.DashboardService.GetFullTimeStaffAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Full Time Staff Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region Get Annual leave Alert
        [HttpGet]
        public HttpResponseMessage GetAnnualLeaveAlert(int teamId)
        {

            try
            {
                int response = Services.DashboardService.GetAnnualLeaveAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Annual leave Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Sickness Alert
        [HttpGet]
        public HttpResponseMessage GetSicknessAlert(int teamId)
        {

            try
            {
                int response = Services.DashboardService.GetSicknessAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Sickness Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get New Starter Alert
        [HttpGet]
        public HttpResponseMessage GetNewStarterAlert(int teamId)
        {

            try
            {
                int response = Services.DashboardService.GetNewStarterAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get New Starter Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Holiday Alert
        [HttpGet]
        public HttpResponseMessage GetHolidayAlert(int teamId)
        {

            try
            {
                int response = Services.DashboardService.GetHolidayAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Holiday Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get PayPoint Alert
        [HttpGet]
        public HttpResponseMessage GetPayPointAlert(int teamId)
        {

            try
            {
                int response = Services.DashboardService.GetPayPointAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get PayPoint Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Mandatory Training Alert
        [HttpGet]
        public HttpResponseMessage GetMandatoryTrainingAlert(int teamId)
        {

            try
            {
                int response = Services.DashboardService.GetMandatoryTrainingAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Mandatory Training Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region Get Health Alert
        [HttpGet]
        public HttpResponseMessage GetHealthAlert(int teamId)
        {

            try
            {
                int response = Services.DashboardService.GetHealthAlert(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Health Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Vacancies List
        [HttpGet]
        public HttpResponseMessage GetVacanciesList()
        {
            List<VacanciesListResponse> response = new List<VacanciesListResponse>();
            try
            {
                response = Services.DashboardService.GetVacanciesList();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Vacancies List--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        [HttpGet]
        public HttpResponseMessage GetTeamLookUp()
        {
            List<LookUpResponseModel> response = new List<LookUpResponseModel>();
            try
            {
               response= Services.DashboardService.GetTeamLookUp();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Health Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #region Get Dashboard Stats
        [HttpPost]
        public HttpResponseMessage GetDashboardStats(AbsenceRequest request)
        {

            try
            {
                var response = Services.DashboardService.GetDashboardStats(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Health Alert--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

    }
}
