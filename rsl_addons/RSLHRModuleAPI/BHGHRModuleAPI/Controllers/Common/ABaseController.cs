﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Mvc;

namespace BHGHRModuleAPI.Controllers
{
    public class ABaseController : ApiController
    {
        protected readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IServiceProxy Services { get; set; }
        public ABaseController(IServiceProxy services)
        {
            this.Services = services;
        }     

        public string GetRouteData(string key)
        {
            var keyValue = Request.GetRouteData().Values[key];
            if (keyValue != null)
            {
                return keyValue.ToString();
            }
            return string.Empty;
        }

        public string GetTableRow(string key, string newValue, string oldValue)
        {
            return string.Format("<tr><td style='text-align:center;border: 1px solid black;'><font size='3' face='arial'>{0}</font></td><td style='text-align:center;border: 1px solid black;'><font size='3' face='arial'>{1}</font></td><td style='text-align:center;border: 1px solid black;'><font size='3' face='arial'>{2}</font></td></tr>", key, newValue ?? "empty", oldValue ?? "empty");
        }

        public string GetTableHeader()
        {
            return "<table style='width: 100 %;border: 1px solid black;'><th><font size='3' face='arial'>Item</font></th><th><font size='3' face='arial'>New value</font></th><th><font size='3' face='arial'>Old value</font></th>";
        }
    }
}
