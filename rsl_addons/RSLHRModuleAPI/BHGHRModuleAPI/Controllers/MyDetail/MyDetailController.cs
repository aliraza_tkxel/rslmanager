﻿using log4net;
using NetworkModel;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers
{

    public class MyDetailController : ABaseController
    {

        // private readonly ILog log = LogManager.GetLogger(typeof(MyDetailController));
        public MyDetailController(IServiceProxy service) : base(service)
        {

        }

        [HttpGet]
        public HttpResponseMessage GetMyDetail(int employeeId)
        {
            try
            {
                UserDetail responce = new UserDetail();
                responce = Services.MyDetailService.GetMyDetail(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get My detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }


        }
        public PersonalDetailResponce GetEmployeePersonalDetailForDifference(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                if (employeeId != 0)
                {
                    PersonalDetailResponce response = new PersonalDetailResponce();
                    response = Services.EmployeesService.GetEmployeeDetail(employeeId);
                    SharedLookUps lookups = Services.SharedService.loadSharedLookups(employeeId);
                    response.lookUps = lookups;
                    return response;
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Persoanal Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return null;
            }
        }

        [HttpPost]
        public HttpResponseMessage AmendMyDetail([FromBody] UserDetail request)
        {
            try
            {
                var religions = Services.EmployeesService.GetreligionDataList();
                var sexualOrientations = Services.EmployeesService.GetsexualOrientationDataList();
                var maritalStatuses = Services.EmployeesService.GetmaritalStatusDataList();
                var ethnicities = Services.EmployeesService.GetEthnicityList();
                var titles = Services.EmployeesService.GetTitlesDataList();

                if (request.personalDetail.employeeId != 0)
                {
                    var result = GetEmployeePersonalDetailForDifference(request.personalDetail.employeeId);
                    EmployeePersonalDetail oldPersonal = result.employeeDetail;
                    var current = request.personalDetail;

                    if (current.firstName != null)
                        current.firstName = current.firstName.Trim();
                    if (current.lastName != null)
                        current.lastName = current.lastName.Trim();

                    List<Tuple<string, object, object>> differences = GeneralHelper.Differences(current, oldPersonal);
                    string[] list = { "firstName", "lastName", "dob", "gender", "maritalStatus", "ethnicity", "religion", "sexualOrientation" };
                    var differencesList = differences.Where(dif => list.Contains(dif.Item1)).ToList();
                    if (differencesList.Count > 0)
                    {
                        string body = "<p>The personal details for <b>" + oldPersonal.firstName + " " + oldPersonal.lastName + "</b> have been amended and the ﬁeld that has been changed.</p><br>";
                        body += GetTableHeader();
                        foreach (var val in differencesList)
                        {
                            switch (val.Item1)
                            {
                                case "maritalStatus":
                                    if (maritalStatuses[Convert.ToInt32(val.Item2)] != null && maritalStatuses[Convert.ToInt32(val.Item3)] != null)
                                        body += GetTableRow(val.Item1, maritalStatuses[Convert.ToInt32(val.Item2)], maritalStatuses[Convert.ToInt32(val.Item3)]);
                                    break;
                                case "ethnicity":
                                    if (ethnicities[Convert.ToInt32(val.Item2)] != null && ethnicities[Convert.ToInt32(val.Item3)] != null)
                                        body += GetTableRow(val.Item1, ethnicities[Convert.ToInt32(val.Item2)], ethnicities[Convert.ToInt32(val.Item3)]);
                                    break;
                                case "religion":
                                    if (Convert.ToInt32(val.Item2) != 0 && religions[Convert.ToInt32(val.Item2)] != null && religions[Convert.ToInt32(val.Item3)] != null)
                                        body += GetTableRow(val.Item1, religions[Convert.ToInt32(val.Item2)], religions[Convert.ToInt32(val.Item3)]);
                                    break;
                                case "sexualOrientation":
                                    if (sexualOrientations[Convert.ToInt32(val.Item2)] != null && sexualOrientations[Convert.ToInt32(val.Item3)] != null)
                                        body += GetTableRow(val.Item1, sexualOrientations[Convert.ToInt32(val.Item2)], sexualOrientations[Convert.ToInt32(val.Item3)]);
                                    break;
                                default:
                                    body += GetTableRow(val.Item1, ""+val.Item2+"", ""+val.Item3+"");
                                    break;
                            }
                        }
                        body += "</table>";
                        string subject = "Personal Detail Changes";
                        //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                        string receiverEmail = ApplicationConstants.hrTeamEmailAddress;
                        EmailHelper.sendEmail(body, subject, receiverEmail);
                    }
                }
                checkContactDetailChanges(request);

                MyDetailResponce responce = new MyDetailResponce();
                responce = Services.MyDetailService.AmendDetail(request);
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Amend My Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        public ContactDetail GetEmployeeConatctDetailForDifference(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                ContactDetail response = new ContactDetail();
                response = Services.EmployeesService.GetEmployeeContactDetailForMobile(employeeId);
                return response;
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return null;
            }
        }
        public void checkContactDetailChanges(UserDetail request)
        {
            try
            {
                var old = GetEmployeeConatctDetailForDifference(request.personalDetail.employeeId);
                string employeeName = Services.EmployeesService.GetEmployeeName(request.personalDetail.employeeId);
                var current = request.contactDetail;
                if (current.addressLine1 != null)
                {
                    current.addressLine1 = current.addressLine1.Trim();
                }
                if (current.addressLine2 != null)
                {
                    current.addressLine2 = current.addressLine2.Trim();
                }
                if (current.addressLine3 != null)
                {
                    current.addressLine3 = current.addressLine3.Trim();
                }
                if (current.county != null)
                {
                    current.county = current.county.Trim();
                }
                if (current.emergencyContactNumber != null)
                {
                    current.emergencyContactNumber = current.emergencyContactNumber.Trim();
                }
                if (current.emergencyContactPerson != null)
                {
                    current.emergencyContactPerson = current.emergencyContactPerson.Trim();
                }
                if (current.emergencyContactRelationship != null)
                {
                    current.emergencyContactRelationship = current.emergencyContactRelationship.Trim();
                }
                if (current.homeEmail != null)
                {
                    current.homeEmail = current.homeEmail.Trim();
                }
                if (current.homePhone != null)
                {
                    current.homePhone = current.homePhone.Trim();
                }
                if (current.mobile != null)
                {
                    current.mobile = current.mobile.Trim();
                }
                if (current.mobilePersonal != null)
                {
                    current.mobilePersonal = current.mobilePersonal.Trim();
                }
                if (current.postalTown != null)
                {
                    current.postalTown = current.postalTown.Trim();
                }
                if (current.workEmail != null)
                {
                    current.workEmail = current.workEmail.Trim();
                }
                if (current.workMobile != null)
                {
                    current.workMobile = current.workMobile.Trim();
                }
                if (current.workPhone != null)
                {
                    current.workPhone = current.workPhone.Trim();
                }
                if (request.personalDetail.employeeId != 0)
                {
                    var differences = GeneralHelper.Differences(current, old);
                    string[] list = { "address1", "address2", "address3", "workMobile", "workEmail", "homeEmail", "mobile", "mobilePersonal", "emergencyContactNumber", "emergencyContactPerson", "emergencyContactRelationship", "county", "workPhone", "postalTown", "emergencyContact", "emergencyTel", "homePhone" };
                    var differencesList = differences.Where(dif => list.Contains(dif.Item1)).ToList();
                    if (differencesList.Count > 0)
                    {
                        string body = "<p>The employee contact for " + employeeName + " has been updated with the following changes: </p><br>";
                        body += GetTableHeader();
                        foreach (var val in differencesList)
                        {
                            string newVal = "" + val.Item2 ?? "";
                            string oldVal = "" + val.Item3 ?? "";
                            if (!string.IsNullOrWhiteSpace(newVal) || !string.IsNullOrWhiteSpace(oldVal))
                                body += GetTableRow(val.Item1, "" + val.Item2 + "", "" + val.Item3 + "");
                        }
                        body += "</table>";
                        string subject = "Contact Detail Changes";
                        //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                        string receiverEmail = ApplicationConstants.hrTeamEmailAddress;
                        EmailHelper.sendEmail(body, subject, receiverEmail);
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        [HttpPost]
        public HttpResponseMessage GetLeaveStats(AbsenceRequest request)
        {
            try
            {
                AnnualLeaveDetail responce = new AnnualLeaveDetail();
                responce = Services.MyDetailService.GetAnnualLeaveDetail(request);
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leave Stats--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
    }
}
