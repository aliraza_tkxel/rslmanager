﻿using NetworkModel;
using log4net;
using Services;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;
using System.Linq;


namespace BHGHRModuleAPI.Controllers
{
    public class AccessControlController : ABaseController
    {

        public AccessControlController(IServiceProxy service) : base(service)
        {

        }

        #region Get Employee Profile
        public HttpResponseMessage GetEmployee(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                AccessControlResponse response = new AccessControlResponse();
                response = Services.AccessControlService.GetEmployee(employeeId);
                response.isSuccessFul = true;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Profile--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region Update Login
        [HttpPost]
        public HttpResponseMessage UpdateLogin([FromBody] AccessControl request)
        {
            log.Debug("Enter:");
            try
            {
                AccessControlResponse response = new AccessControlResponse();
                string resp = Services.AccessControlService.UpdateLogin(request);
                response.message = resp;
                response.employeeId = request.employeeId;
                if (resp.Equals(UserMessageConstants.LoginUpdateSuccessMessage))
                {
                    response.isSuccessFul = true;
                }
                else
                {
                    response.isSuccessFul = false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Update Login--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Unlock Account
        [HttpPost]
        public HttpResponseMessage UnlockAccount([FromBody] int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                AccessControlResponse response = new AccessControlResponse();
                Services.AccessControlService.UnlockAccount(employeeId);
                response.message = UserMessageConstants.UnlockAccountSuccessMessage;
                response.employeeId = employeeId;
                response.isSuccessFul = true;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Unlock Account--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion
    }
}
