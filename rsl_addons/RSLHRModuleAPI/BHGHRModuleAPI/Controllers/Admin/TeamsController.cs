﻿using NetworkModel;
using log4net;
using Services;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;
using System.Linq;


namespace BHGHRModuleAPI.Controllers
{
    public class TeamsController : ABaseController
    {

        public TeamsController(IServiceProxy service) : base(service)
        {

        }

        #region Get Teams
        [HttpPost]
        public HttpResponseMessage GetTeamsList([FromBody] TeamsRequest request)
        {
            log.Debug("Enter:");
            try
            {
                TeamsResponse response = new TeamsResponse();
                response = Services.TeamsService.GetTeamsList(request);
                response.isSuccessFul = true;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Teams List--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        public HttpResponseMessage GetTeam(int teamId)
        {
            log.Debug("Enter:");
            try
            {
                TeamsResponse response = new TeamsResponse();
                response = Services.TeamsService.GetTeam(teamId);
                response.isSuccessFul = true;
                response.DMLookups = Services.TeamsService.LoadDMLookups();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Team--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region Save a Team
        [HttpPost]
        public HttpResponseMessage SaveTeam([FromBody] Team request)
        {
            log.Debug("Enter:");
            try
            {

                TeamsResponse response = new TeamsResponse();
                string resp = Services.TeamsService.SaveTeam(request);
                response.message = resp;
                if (resp.Equals(UserMessageConstants.TeamSaveSuccessMessage))
                {
                    response.isSuccessFul = true;
                }
                else
                {
                    response.isSuccessFul = false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Update Team--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion
    }
}
