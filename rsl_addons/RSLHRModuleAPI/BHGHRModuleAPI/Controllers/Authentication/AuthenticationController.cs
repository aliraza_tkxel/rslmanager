﻿using NetworkModel;
using log4net;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BHGHRModuleAPI.Controllers
{
    public class AuthenticationController : ABaseController
    {
        //private readonly ILog log = LogManager.GetLogger(typeof(AuthenticationController));
        public AuthenticationController(IServiceProxy service) : base(service)
        {

        }
        #region Authenticate a user
        [HttpPost]
        public HttpResponseMessage AuthenticateUser([FromBody] LoginRequest request)
        {
            log.Debug("Enter:");
            try
            {
                LoginResponse response = new LoginResponse();
                response = Services.AuthenticationService.AuthenticateUser(request);
                SharedLookUps lookups = Services.SharedService.loadSharedLookups(response.userInfo.userId);
                response.lookUps = lookups;
                if (response.userInfo.userId.HasValue)
                {
                    AnnualLeaveResponce leaveYearDate = Services.AbsenceService.GetLeaveYearDate(response.userInfo.userId.Value);
                    if (leaveYearDate != null && !string.IsNullOrEmpty(leaveYearDate.leaveYearStart))
                    {
                        response.userInfo.leaveYearStart = leaveYearDate.leaveYearStart;
                        response.userInfo.leaveYearEnd = leaveYearDate.leaveYearEnd;
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Authenticate User--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion
    }
}
