﻿using log4net;
using NetworkModel;
using NetwrokModel.Reports;
using Services;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BHGHRModuleAPI.Controllers.Reports
{
    public class ReportsController : ABaseController
    {

        // private readonly ILog log = LogManager.GetLogger(typeof(ReportsController));

        public ReportsController(IServiceProxy service) : base(service)
        {

        }

        [HttpPost]
        public HttpResponseMessage PopulateEstablishmentReport([FromBody]CommonReportsFilterRequest model)
        {

            try
            {
                EstablishmentResponse response = new EstablishmentResponse();
                response = Services.ReportsService.PopulateEstablishmentReport(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Populate Establishment Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage PopulateSicknessReport([FromBody]SicknessRequest model)
        {

            try
            {
                SicknessReportResponse response = new SicknessReportResponse();
                response = Services.ReportsService.PopulateSicknessReport(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Populate Sickness Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage populateAnnualLeaveReport([FromBody]AnnualLeaveRequest model)
        {
            AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();
            try
            {

                response = Services.ReportsService.populateAnnualLeaveReport(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Populate annual Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpGet]
        public HttpResponseMessage GetTeamsByDirectorate(int directorateId)
        {
            AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();
            try
            {
                response = Services.ReportsService.GetTeamsByDirectorate(directorateId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Teams--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage PopulateLeaverReport(LeaverReportRequest model)
        {
            LeaverReportResponse response = new LeaverReportResponse();
            try
            {

                response = Services.ReportsService.PopulateLeaverReport(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Populate Leaver Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetSicknessExport([FromBody]SicknessRequest model)
        {
            try
            {

                SicknessExportResponse response = new SicknessExportResponse();
                response = Services.ReportsService.SicknessExport(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Sickness Export--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage PopulateNewStarterReport([FromBody]CommonReportsFilterRequest model)
        {
            NewStarterResponse response = new NewStarterResponse();
            try
            {

                response = Services.ReportsService.PopulateNewStarterReport(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Populate New Starter Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage GetMyStaffList([FromBody]MyStaffEmployeeRequest model)
        {
            MyStaffList response = new MyStaffList();
            try
            {

                response = Services.ReportsService.PopulateMyStaffReport(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Populate My Staff Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage GetMyTeamList(CommonRequest model)
        {
            MyTeamListing response = new MyTeamListing();
            try
            {

                response = Services.ReportsService.PopulateMyTeamList(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Populate My Team Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetEmployeeByTeam(MyTeamsEmployeeRequest model)
        {
            MyTeamEmployeeListing response = new MyTeamEmployeeListing();
            try
            {
                response = Services.ReportsService.GetEmployeeByTeam(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Team's Employee List--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage PopulateRemunerationStatement(CommonReportsFilterRequest model)
        {
            RemunerationStatementsResponse response = new RemunerationStatementsResponse();
            try
            {

                response = Services.ReportsService.PopulateRemunerationStatement(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Populate Remuneration Statement--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpGet]
        public HttpResponseMessage GetRemunerationStatementDocument(int employeeId)
        {
            RemunerationStatementDocument response = new RemunerationStatementDocument();
            try
            {

                response = Services.ReportsService.GetRemunerationStatementDocument(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Remuneration Statement Document--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetMandatoryTrainings(CommonReportsFilterRequest model)
        {

            TrainingListingResponse response = new TrainingListingResponse();
            try
            {

                response = Services.ReportsService.GetMandatoryTrainings(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Mandatory Trainings--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetHealthList(CommonRequest model)
        {

            HealthReportResponse response = new HealthReportResponse();
            try
            {

                response = Services.ReportsService.GetHealthList(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Health List--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GETEmployeeJournal(JournalRequest model)
        {

            EmployeeJournalResponse response = new EmployeeJournalResponse();
            try
            {

                response = Services.ReportsService.GETEmployeeJournal(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Journal--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetDeclarationOfInterestsReport(DeclarationOfInterestsRequest model)
        {

            DeclarationOfInterestsResponse response = new DeclarationOfInterestsResponse();
            try
            {

                response = Services.ReportsService.GetDeclarationOfInterestsReport(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Declaration Of Interests Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GiftApprovalList(GiftApprovalRequest model)
        {

            GiftApprovalResponse response = new GiftApprovalResponse();
            try
            {

                response = Services.ReportsService.GiftApprovalList(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Declaration Of Interests Report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage EmployeesByDirectorate(int directorateId)
        {

            AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();
            try
            {
                response = Services.ReportsService.GetEmployeesByDirectorate(directorateId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Teams--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
    }
}
