﻿using log4net;
using NetworkModel;
using NetworkModel.NetwrokModel.Common;
using Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers.AbsenceController
{
    public class AbsenceController : ABaseController
    {
        #region >>> Prop & Const <<<

        public AbsenceController(IServiceProxy service) : base(service)
        {

        }

        #endregion

        #region >>> Helpers <<<



        #endregion

        #region >>> Action Methods <<<

        #region >>> Annual <<<

        [HttpPost]
        public HttpResponseMessage GetAnnualLeaveDetail(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {

                AnnualLeaveResponce responce = Services.AbsenceService.GetAnnualLeaveDetail(request);
                responce.bankHolidays = Services.AbsenceService.GetBankHolidays();
                responce.workingPattren = Services.PostTabService.GetEmpCurrentWorkingPattren(request.employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage RecordAnnualLeave(RecordAbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                BaseResponce response = new BaseResponce();
                dynamic result = Services.AbsenceService.RecordAnnualLeave(request);

                string message = "";
                if (result.isSuccess == true && result.message != "")
                {
                    message = result.message;
                }
                else if (result.isSuccess == false && result.message != "")
                {
                    message = result.message;
                }
                else
                {
                    message = UserMessageConstants.LeaveAlreadyBookedMessage;
                }
                response.isSuccessFul = result.isSuccess;
                response.message = message;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region >>> Sickness <<<

        [HttpPost]
        public HttpResponseMessage GetSicknessAbsenceWeb(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                SicknessResponse response = new SicknessResponse();

                List<AbsenceLeave> absenceSickness = Services.AbsenceService.GetAbsenceSickness(request);
                List<StaffMemberSickness> staffMembers = Services.AbsenceService.GetStaffMembersSicknessSummary(request);
                response.sicknessAbsences = absenceSickness;
                response.staffMembers = staffMembers;
                response.absenceReasons = Services.AbsenceService.GetSicknessReasons();
                response.bankHolidays = Services.AbsenceService.GetBankHolidays();
                response.sicknessActions = Services.AbsenceService.GetSicknessActions();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetSicknessAbsence(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                AbsenceRequest request = new AbsenceRequest();
                request.employeeId = employeeId;
                SicknessResponse response = new SicknessResponse();

                List<AbsenceLeave> absenceSickness = Services.AbsenceService.GetAbsenceSickness(request);
                List<StaffMemberSickness> staffMembers = Services.AbsenceService.GetStaffMembersSicknessSummary(request);
                response.sicknessAbsences = absenceSickness;
                response.staffMembers = staffMembers;
                response.absenceReasons = Services.AbsenceService.GetSicknessReasons();
                response.bankHolidays = Services.AbsenceService.GetBankHolidays();
                response.sicknessActions = Services.AbsenceService.GetSicknessActions();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetOwnSicknessAbsence(int absenceHistoryId)
        {
            log.Debug("Enter:");
            try
            {
                SicknessResponse response = new SicknessResponse();

                List<AbsenceLeave> absenceSickness = new List<AbsenceLeave>();
                List<StaffMemberSickness> staffMembers = Services.AbsenceService.GetOwnAbsenceSickness(absenceHistoryId);
                response.sicknessAbsences = absenceSickness;
                response.staffMembers = staffMembers;
                response.absenceReasons = Services.AbsenceService.GetSicknessReasons();
                response.bankHolidays = Services.AbsenceService.GetBankHolidays();
                response.sicknessActions = Services.AbsenceService.GetSicknessActions();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        public HttpResponseMessage RecordSickness(RecordAbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                dynamic result = Services.AbsenceService.RecordSickness(request);
                string message = "";
                if(result.isSuccess == true)
                {
                    message = UserMessageConstants.SicknessMarkedSuccessMessage;
                }
                else
                {
                    message = result.message;
                }
                return Request.CreateResponse(HttpStatusCode.OK, message);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage ChangeSicknessLeaveStatus(LeaveSicknessStatusChangeRequest leaveStatusChangeRequest)
        {
            log.Debug("Enter:");
            try
            {
                BaseResponce response = new BaseResponce();
                var status = Services.AbsenceService.ChangeSicknessLeaveStatus(leaveStatusChangeRequest);
                if (status.Equals(UserMessageConstants.RescheduleBRSAppointmentMessage))
                {
                    response.message = status;
                    response.isSuccessFul = false;
                }
                else
                {
                    response.message = status == ApplicationConstants.absenceError ? UserMessageConstants.UpdatingLeaveError : string.Format(UserMessageConstants.SicknessUpdatedMessage, status);
                    response.isSuccessFul = status == ApplicationConstants.absenceError ? false : true;
                }
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage ChangeSicknessLeaveStatusWeb(LeaveSicknessStatusChangeRequestWeb leaveStatusChangeRequest)
        {
            log.Debug("Enter:");
            try
            {
                BaseResponce response = new BaseResponce();
                var status = Services.AbsenceService.ChangeSicknessLeaveStatusWeb(leaveStatusChangeRequest);
                if (status.Equals(UserMessageConstants.RescheduleBRSAppointmentMessage) 
                    || status.Equals(UserMessageConstants.SickLeaveErrorMessage)
                    || status.Equals(UserMessageConstants.SickLeaveAlreadyBookedMessage))
                {
                    response.message = status;
                    response.isSuccessFul = false;
                }
                else
                {
                    response.message = status == ApplicationConstants.absenceError ? UserMessageConstants.UpdatingLeaveError : string.Format(UserMessageConstants.LeaveUpdatedMessage, status);
                    response.isSuccessFul = status == ApplicationConstants.absenceError ? false : true;
                }
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region >>> Leave of Absence <<<

        [HttpPost]
        public HttpResponseMessage GetLeaveOfAbsenceWeb(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                LeaveOfAbsenceResponce responce = new LeaveOfAbsenceResponce();
                List<AbsenceLeave> leaves = Services.AbsenceService.GetLeaveOfAbsence(request, GetRouteData(ApplicationConstants.callBy));

                //RequestContext.Current.RouteData.Values["requestfrom"];
                responce.absentees = leaves;
                responce.leavesNatures = Services.AbsenceService.GetLeaveOfAbsenceNatures(GetRouteData(ApplicationConstants.callBy));
                responce.bankHolidays = Services.AbsenceService.GetBankHolidays();
                responce.workingPattren = Services.PostTabService.GetEmpCurrentWorkingPattren(request.employeeId);
                responce.toilsOwed = Services.AbsenceService.GetToilLeave(request).timeOfInlieuOwed;
                responce.toilsOwed = responce.toilsOwed == null ? 0 : responce.toilsOwed;
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leave Of Absence Web--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetLeaveOfAbsence(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                LeaveOfAbsenceResponce responce = new LeaveOfAbsenceResponce();
                List<AbsenceLeave> leaves = Services.AbsenceService.GetLeaveOfAbsence(request, GetRouteData(ApplicationConstants.callBy));
                
                //RequestContext.Current.RouteData.Values["requestfrom"];
                responce.absentees = leaves;
                responce.leavesNatures = Services.AbsenceService.GetLeaveOfAbsenceNatures(GetRouteData(ApplicationConstants.callBy));
                responce.bankHolidays = Services.AbsenceService.GetBankHolidays();
                responce.workingPattren = Services.PostTabService.GetEmpCurrentWorkingPattren(request.employeeId);
                responce.toilsOwed = Services.AbsenceService.GetToilLeave(request).timeOfInlieuOwed;
                responce.toilsOwed = responce.toilsOwed == null ? 0 : responce.toilsOwed;
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leave Of Absence--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        


        //public HttpResponseMessage GetLeavesOfAbsenceRequested(int employeeId)
        //{
        //    log.Debug("Enter:");
        //    try
        //    {
        //        LeaveOfAbsenceResponce responce = new LeaveOfAbsenceResponce();
        //        List<AbsenceLeave> leaves = Services.AbsenceService.GetLeavesOfAbsenceRequested(employeeId);
        //        responce.absentees = leaves;
        //        responce.leavesNatures = Services.AbsenceService.GetLeaveOfAbsenceNatures();
        //        responce.bankHolidays = Services.AbsenceService.GetBankHolidays();
        //        return Request.CreateResponse(HttpStatusCode.OK, responce);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("---------------Get Employees Contact Detail--------------------------");
        //        log.Error(ex.StackTrace);
        //        log.Logger.Repository.Shutdown();
        //        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
        //    }
        //}

        public HttpResponseMessage RecordLeaveOfAbsence(RecordAbsenceRequest request)
        {

            try
            {
                BaseResponce response = new BaseResponce();
                dynamic result = Services.AbsenceService.RecordLeaveOfAbsence(request);
                string message = "";

                if (result.isSuccess == true && result.message != "")
                {
                    message = result.message;
                }
                else if (result.isSuccess == false && result.message != "")
                {
                    message = result.message;
                }
                else
                {
                    message = UserMessageConstants.LeaveAlreadyBookedMessage;
                }
                response.isSuccessFul = result.isSuccess;
                response.message = message;

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------record leave of  absence--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region >>> My Team Leaves <<<

        public HttpResponseMessage GetMyTeamLeavesOfAbsenceRequested(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                MyTeamLeavesOfAbsenceResponse responce = new MyTeamLeavesOfAbsenceResponse();
                List<MyTeamLeaveOfAbsencePending> leaves = Services.AbsenceService.GetMyTeamLeavesOfAbsenceRequested(employeeId);
                responce.myTeamLeavesPending = leaves;
                responce.leavesNatures = Services.AbsenceService.GetLeaveOfAbsenceNatures(GetRouteData(ApplicationConstants.callBy));
                responce.bankHolidays = Services.AbsenceService.GetBankHolidays();
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        public HttpResponseMessage GetMyTeamLeavesOfToilRequested(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                MyTeamLeaveOfToilResponse responce = new MyTeamLeaveOfToilResponse();
                List<MyTeamLeaveOfToilPending> leaves = Services.AbsenceService.GetMyTeamLeavesOfToilRequested(employeeId);
                responce.myTeamLeavesPending = leaves;
                responce.leavesNatures = Services.AbsenceService.GetLeaveOfAbsenceNatures(GetRouteData(ApplicationConstants.callBy));
                responce.bankHolidays = Services.AbsenceService.GetBankHolidays();
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetMyTeamLeavesRequested(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                MyTeamLeavesResponse response = new MyTeamLeavesResponse();
                response.myTeamLeavesPending = Services.AbsenceService.GetMyTeamLeavesRequested(employeeId);

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        public HttpResponseMessage GetMyTeamBirthdayLeavesRequested(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                MyTeamLeavesResponse response = new MyTeamLeavesResponse();
                response.myTeamLeavesPending = Services.AbsenceService.GetMyTeamBirthdayLeavesRequested(employeeId, "Birthday Leave");
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        public HttpResponseMessage GetMyTeamPersonalDayLeavesRequested(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                MyTeamLeavesResponse response = new MyTeamLeavesResponse();
                response.myTeamLeavesPending = Services.AbsenceService.GetMyTeamBirthdayLeavesRequested(employeeId, "Personal Day");

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        public HttpResponseMessage GetMyTeamLeavesRequestedWeb(int employeeId)
        {
            // this method need get information pending leave.
            // we get information by leavehistoryid form web.
            log.Debug("Enter:");

            try
            {
                MyTeamLeavesResponseWeb response = new MyTeamLeavesResponseWeb();
                List<MyTeamLeavesPending> myTeamLeavesPending = Services.AbsenceService.GetMyTeamLeavesRequestedWeb(employeeId);
                List<MyTeamLeaveOfAbsencePending> leaves = Services.AbsenceService.GetMyTeamLeavesOfAbsenceRequested(employeeId);
                foreach (MyTeamLeaveOfAbsencePending item in leaves)
                {
                    MyTeamLeavesPending pending = new MyTeamLeavesPending();
                    pending.absenceHistoryId = item.absenceHistoryId;
                    pending.applicantId = item.applicantId;
                    pending.applicantName = item.applicantName;
                    pending.duration = item._duration;
                    pending.endDate = item.endDate;
                    pending.holType = item.holType;
                    pending.leaveDescription = item.leaveDescription;
                    pending.natureId = item.natureId;
                    pending.startDate = item.startDate;
                    pending.status = item.status;
                    pending.statusId = item.statusId;
                    myTeamLeavesPending.Add(pending);
                }

                // Add Employees own Leave Of Absence and Annual Leaves
                List<MyTeamLeavesPending> myLeaveOfAbsence = Services.AbsenceService.GetMyLeavesOfAbsenceRequested(employeeId);
                myTeamLeavesPending.AddRange(myLeaveOfAbsence);

                // Add Employees own Annual Leaves
                List<MyTeamLeavesPending> myLeaves = Services.AbsenceService.GetMyLeavesRequested(employeeId);
                myTeamLeavesPending.AddRange(myLeaves);

                response.myTeamLeavesPending = myTeamLeavesPending;
                response.leaveActions = Services.AbsenceService.GetleaveActions();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetAbsenceDetailByAbsenceHistoryId(int absenceHistoryId)
        {

            log.Debug("Enter:");

            try
            {
                AbsenceDetailResponse response = new AbsenceDetailResponse();
                List<AbsenceDetail> absenceDetail = Services.AbsenceService.GetAbsenceDetail(absenceHistoryId);                
                response.myTeamLeavesPending = absenceDetail;
                response.leaveActions = Services.AbsenceService.GetleaveActions();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region >>> Toil <<<
        [HttpPost]
        public HttpResponseMessage GetToilLeaves(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {

                ToilLeaveResponce leaves = Services.AbsenceService.GetToilLeave(request);

                return Request.CreateResponse(HttpStatusCode.OK, leaves);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Toil leaves List--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        // for IOS
        public HttpResponseMessage GetLeaveOfToil(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                LeaveOfAbsenceResponce responce = new LeaveOfAbsenceResponce();
                List<AbsenceLeave> leaves = Services.AbsenceService.GetLeaveOfToil(employeeId, GetRouteData(ApplicationConstants.callBy));


                //RequestContext.Current.RouteData.Values["requestfrom"];
                responce.absentees = leaves;
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);

            }

        }

        public HttpResponseMessage AddToilRecord(RecordToilRequest request)
        {
            log.Debug("Enter:");
            try
            {
                string message = "";
                bool isAdded = Services.AbsenceService.AddToilRecord(request);
                
                //var isCalledByIos = Request.RequestUri.AbsolutePath.ToLower().Contains(ApplicationConstants.callByIos);
                //var uri = Request.RequestUri.AbsolutePath;
                if (GetRouteData(ApplicationConstants.callBy) == ApplicationConstants.callByIos)
                {
                    BaseResponce response = new BaseResponce();
                    message = UserMessageConstants.LeaveAbsenceMarkedSuccessMessage;
                    response.isSuccessFul = isAdded;
                    AbsenceRequest absenceRequest = new AbsenceRequest();
                    absenceRequest.employeeId = request.employeeId; 
                    var toilsOwed = Services.AbsenceService.GetToilLeave(absenceRequest).timeOfInlieuOwed;
                    toilsOwed = toilsOwed == null ? 0 : toilsOwed;
                    if (request.leaveType != "BRS TOIL Recorded")
                    {
                        message = message.Replace("recorded", "requested");
                    }
                    
                    response.message = isAdded == true ? string.Format(message, toilsOwed) : UserMessageConstants.LeaveAlreadyBookedMessage;
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    message = isAdded == true ? UserMessageConstants.LeaveMarkedSuccessMessage : UserMessageConstants.LeaveAlreadyBookedMessage;
                    return Request.CreateResponse(HttpStatusCode.OK, message);
                }


            }
            catch (Exception ex)
            {
                log.Error("---------------Add toil absence--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region "Internal Meetings"

        [HttpPost]
        public HttpResponseMessage GetInternalMeetings(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                var leaves = Services.AbsenceService.GetInternalMeetingLeave(request);

                return Request.CreateResponse(HttpStatusCode.OK, leaves);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Internal Meetings leaves List--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage AddInternalMeetingRecord(RecordInternalMeetingRequest request)
        {
            log.Debug("Enter:");
            try
            {
                string message = "";
                bool isAdded = Services.AbsenceService.AddInternalMeetingRecord(request);

                //var isCalledByIos = Request.RequestUri.AbsolutePath.ToLower().Contains(ApplicationConstants.callByIos);
                //var uri = Request.RequestUri.AbsolutePath;
                
                message = isAdded == true ? UserMessageConstants.LeaveMarkedSuccessMessage : UserMessageConstants.LeaveAlreadyBookedMessage;
                return Request.CreateResponse(HttpStatusCode.OK, message);

            }
            catch (Exception ex)
            {
                log.Error("---------------Add Internal Meeting absence--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage ChangeInternalLeaveStatus(InternalLeaveStatusRequest request)
        {
            log.Debug("Enter:");
            try
            {
                BaseResponce response = new BaseResponce();
                var status = Services.AbsenceService.ChangeInternalLeaveStatus(request);
                if (status.Equals(UserMessageConstants.RescheduleBRSAppointmentMessage))
                {
                    response.message = status;
                    response.isSuccessFul = false;
                }
                else
                {
                    response.message = status == ApplicationConstants.absenceError ? UserMessageConstants.UpdatingLeaveError :
                       status == ApplicationConstants.overlap ? UserMessageConstants.LeaveAlreadyBookedMessage :
                        string.Format(UserMessageConstants.LeaveUpdatedMessage, status);
                    response.isSuccessFul = status == ApplicationConstants.absenceError ? false :
                        status == ApplicationConstants.overlap ? false : true;
                }
                

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Internal Meetings leaves List--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region >>> IOS Requests <<<
        public HttpResponseMessage AddInternalMeetingRecord_IOS(RecordInternalMeetingRequest request)
        {
            log.Debug("Enter:");
            try
            {
                BaseResponce response = new BaseResponce();
                string message = "";
                bool isAdded = Services.AbsenceService.AddInternalMeetingRecord(request);

                //var isCalledByIos = Request.RequestUri.AbsolutePath.ToLower().Contains(ApplicationConstants.callByIos);
                //var uri = Request.RequestUri.AbsolutePath;

                message = isAdded == true ? UserMessageConstants.LeaveMarkedSuccessMessage : UserMessageConstants.LeaveAlreadyBookedMessage;
                response.isSuccessFul = isAdded;
                response.message = message;

                return Request.CreateResponse(HttpStatusCode.OK, response);

            }
            catch (Exception ex)
            {
                log.Error("---------------Add Internal Meeting absence--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetLeavesTaken(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                LeaveTakenResponse response = new LeaveTakenResponse();
                response.leaveTakenList = Services.AbsenceService.GetLeavesTaken(request);

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetLeavesRequested(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                LeaveTakenResponse response = new LeaveTakenResponse();
                response.leaveTakenList = Services.AbsenceService.GetLeavesRequested(request);

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetBirthDayLeaveRequested(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                LeaveTakenResponse response = new LeaveTakenResponse();
                response.leaveTakenList = Services.AbsenceService.GetPersonalBirthdaysRequested(request, "Birthday Leave");

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetPersonalDayLeaveRequested(AbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                LeaveTakenResponse response = new LeaveTakenResponse();
                response.leaveTakenList = Services.AbsenceService.GetPersonalBirthdaysRequested(request, "Personal Day");

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetWorkingPattern(int employeeId)
        {
            log.Debug("Enter: GetLeaveApply");
            try
            {
                WorkingHoursParent response = new WorkingHoursParent();
                response = Services.PostTabService.GetEmpCurrentWorkingPattren(employeeId);

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        public HttpResponseMessage RecordAnnualSickLeave(RecordAbsenceRequest request)
        {
            log.Debug("Enter:");
            try
            {
                BaseResponce response = new BaseResponce();

                dynamic result = Services.AbsenceService.RecordLeave(request);

                string message = "";
                //message = result.isSuccess == true ? result.message : UserMessageConstants.LeaveAlreadyBookedMessage;
                if (result.isSuccess == true && result.message != "")
                {
                    message = result.message;
                }
                else if (result.isSuccess == false && result.message != "")
                {
                    message = result.message;
                }
                else
                {
                    message = UserMessageConstants.LeaveAlreadyBookedMessage;
                }
                response.isSuccessFul = result.isSuccess;
                response.message = message;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------record annual sickness--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage ChangeLeaveStatus(LeaveStatusChangeRequest leaveStatusChangeRequest)
        {
            log.Debug("Enter:");
            try
            {
                BaseResponce response = new BaseResponce();
                var status = Services.AbsenceService.ChangeLeaveStatus(leaveStatusChangeRequest);
                if (status.Equals(UserMessageConstants.RescheduleBRSAppointmentMessage))
                {
                    response.message = status;
                    response.isSuccessFul = false;
                }
                else
                {
                    response.message = status == ApplicationConstants.absenceError ? UserMessageConstants.UpdatingLeaveError : string.Format(UserMessageConstants.LeaveUpdatedMessage, status);
                    response.isSuccessFul = status == ApplicationConstants.absenceError ? false : true;
                }
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Leaves taken Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

    }
}
