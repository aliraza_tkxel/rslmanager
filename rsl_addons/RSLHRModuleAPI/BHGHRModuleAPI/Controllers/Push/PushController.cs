﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Services;
using System.Web;
using log4net.Repository.Hierarchy;
using Utilities;

namespace BHGHRModuleAPI.Controllers.Push
{
    public class PushController : ABaseController
    {
        public PushController(IServiceProxy services) : base(services)
        {
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        #region send Push Notification for modification of Fault Appointment and Appointment Notes
        /// <summary>
        /// This function will send Push Notification for modification of Fault Appointment and Appointment Notes
        /// </summary>
        /// <returns>Success or failure</returns>
        [HttpGet]
        [Route("FaultNotes")]
        public HttpResponseMessage sendPushNotificationFaultNoteUpdates(int appointmentId)
        {
            try
            {
                string host = HttpContext.Current.Request.Url.Host.ToString();

                log.Info("Host: " + host);

                string serverinUse = String.Empty;

                if (host.StartsWith(ApplicationConstants.BHG_Live_Name))
                    serverinUse = ApplicationConstants.BHG_Live_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_Test_Name))
                    serverinUse = ApplicationConstants.BHG_Test_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_UAT_Name))
                    serverinUse = ApplicationConstants.BHG_UAT_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_Dev_Name))
                    serverinUse = ApplicationConstants.BHG_Dev_Name;
                else //else case is only for local use.
                    serverinUse = ApplicationConstants.BHG_Dev_Name;

                log.Info("Server in Use: " + serverinUse);

                log.Info("Push notification call appointmentId: " + appointmentId);

                bool response = Services.PushService.sendPushNotificationFaultNoteUpdates(appointmentId, serverinUse);
                return Request.CreateResponse(HttpStatusCode.OK, response);

            }
            catch (Exception ex)
            {
                log.Error("---------------sendPushNotificationFaultNoteUpdates--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region send Push Notification for modification of Fault Appointment and Appointment Notes
        /// <summary>
        /// This function will send Push Notification for modification of Fault Appointment and Appointment Notes
        /// </summary>
        /// <returns>Success or failure</returns>
        [HttpGet]
        [Route("VoidTermination")]
        public HttpResponseMessage sendPushNotificationVoidInspections(int operativeId, string propertyAddress)
        {
            try
            {
                string host = HttpContext.Current.Request.Url.Host.ToString();

                log.Info("Host: " + host);

                string serverinUse = String.Empty;

                if (host.StartsWith(ApplicationConstants.BHG_Live_Name))
                    serverinUse = ApplicationConstants.BHG_Live_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_Test_Name))
                    serverinUse = ApplicationConstants.BHG_Test_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_UAT_Name))
                    serverinUse = ApplicationConstants.BHG_UAT_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_Dev_Name))
                    serverinUse = ApplicationConstants.BHG_Dev_Name;
                else //else case is only for local use.
                    serverinUse = ApplicationConstants.BHG_Dev_Name;

                log.Info("Server in Use: " + serverinUse);

                log.Info("Push notification call property Address: " + propertyAddress);

                bool response = Services.PushService.sendPushNotificationVoidInspections(operativeId, serverinUse, propertyAddress);
                return Request.CreateResponse(HttpStatusCode.OK, response);

            }
            catch (Exception ex)
            {
                log.Error("---------------sendPushNotificationFaultNoteUpdates--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion
    }
}