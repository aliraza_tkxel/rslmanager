﻿using NetworkModel;
using log4net;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BHGHRModuleAPI.Controllers.Navigation
{
    public class NavigationController : ABaseController
    {
        //private readonly ILog log = LogManager.GetLogger(typeof(NavigationController));
        public NavigationController(IServiceProxy service) : base(service)
        {

        }

        #region Get Menus for HR Modules
        [HttpPost]
        public HttpResponseMessage GetMenus([FromBody] NavigationRequestModel request)
        {
            log.Debug("Enter:");
            try
            {
                NavigationResponseModel response = new NavigationResponseModel();
                response = Services.NavigationService.GetMenus(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Menus--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

    }
}
