﻿using NetworkModel;
using NetworkModel.NetwrokModel.Common;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleAPI.Controllers
{
    public class RemitanceController : ABaseController
    {
        #region >>> Prop & Const <<<

        public RemitanceController(IServiceProxy service) : base(service)
        {

        }

        #endregion

        [HttpGet]
        public HttpResponseMessage GetSupplierData(int supplierId)
        {
            log.Debug("Enter: GetSupplierData");
            try
            {
                Organization response = new Organization();
                response = Services.RemitanceService.GetSupplierData(supplierId);


                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Supplier Data--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetInvoiceDetail(int supplierId, string processDate, int paymentTypeId)
        {
            log.Debug("Enter: GetInvoiceDetail");
            try
            {
                List<InvoiceDetail> response = new List<InvoiceDetail>();
                response = Services.RemitanceService.GetInvoiceDetail(supplierId, processDate, paymentTypeId);


                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Invoice Detail--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
    }
}