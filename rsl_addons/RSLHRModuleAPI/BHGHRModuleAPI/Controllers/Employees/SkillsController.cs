﻿using log4net;
using NetworkModel;
using Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers.Employees
{
    public class SkillsController : ABaseController
    {
        // private readonly ILog log = LogManager.GetLogger(typeof(SkillsController));

        public SkillsController(IServiceProxy service) : base(service)
        {

        }
        [HttpPost]
        public HttpResponseMessage GetEmployeesSkills([FromBody]CommonRequest model)
        {
            try
            {
                SkillsListingResponse response = new SkillsListingResponse();
                response = Services.SkillsService.GetEmployeesSkills(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);

            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        public SkillsListingResponse GetEmployeesSkillsForDifference([FromBody]CommonRequest model)
        {
            try
            {
                SkillsListingResponse response = new SkillsListingResponse();
                response = Services.SkillsService.GetEmployeesSkills(model);
                return response;

            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return null;
            }
        }

        [HttpPost]
        public HttpResponseMessage AddAmendEmployeeSkills([FromBody]SkillsAndQualificationsDetail request)
        {
            try
            {
                string body = "New skills has been added to " + Services.EmployeesService.GetEmployeeName(request.employeeId); ;
                string subject = "New Skills added.";
                //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                string receiverEmail = ApplicationConstants.hrTeamEmailAddress;
                EmailHelper.sendEmail(body, subject, receiverEmail);

                Services.SkillsService.AddAmendEmployeeSkills(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.SkillSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage DeleteEmployeeSkills([FromBody]DeleteEmployeeSkills request)
        {
            try
            {

                Services.SkillsService.DeleteEmployeeSkills(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.SkillDeleteSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage AddLanguageSkills([FromBody]LanguageSkillsRequest request)
        {
            try
            {
                List<LanguageSkills> languageSkills = new List<LanguageSkills>();
                languageSkills = Services.SkillsService.GetEmployeeLanguageSkills(request.languages[0].employeeId ?? 0);
                if (request.languages[0].employeeId != null)
                {
                    bool flag = false;
                    int empID = request.languages[0].employeeId ?? 0;
                    var current = request;
                    if (languageSkills.Count != request.languages.Count)
                    {
                        flag = true;
                    }
                    else
                    {
                        List<string> oldLangs = new List<string>();
                        List<string> newLangs = new List<string>();
                        for (int count = 0; count < languageSkills.Count; count++)
                        {
                            oldLangs.Add(languageSkills[count].language);
                            newLangs.Add(request.languages[count].language);
                        }
                        for (int count = 0; count < oldLangs.Count; count++)
                        {
                            if (!oldLangs.Contains(newLangs[count]))
                            {
                                flag = true;
                            }
                        }
                    }
                    if (flag == true)
                    {
                        string body = "The new language has been added to " + Services.EmployeesService.GetEmployeeName(empID); ;
                        string subject = "New language added.";
                        //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                        string receiverEmail = ApplicationConstants.hrTeamEmailAddress;
                        EmailHelper.sendEmail(body, subject, receiverEmail);
                    }
                }

                Services.SkillsService.AddLanguageSkills(request.languages);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.LanguageSkillSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add  Employee Language Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }




    }
}
