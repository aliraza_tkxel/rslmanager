﻿using log4net;
using NetworkModel;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers.Employees
{
    public class DocumentsController : ABaseController
    {
        //private readonly ILog log = LogManager.GetLogger(typeof(DocumentsController));

        public DocumentsController(IServiceProxy service) : base(service)
        {

        }
        [HttpPost]
        public HttpResponseMessage AddEmployeeDocument([FromBody]AddDocumentRequest request)
        {
            try
            {
                Services.DocumentsService.AddEmployeeDocument(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.DocumentSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Employee Document--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetEmployeeDocuments([FromBody]DocumentSearchRequest request)
        {
            try
            {
                DocumentsResponse response = new DocumentsResponse();
                response=Services.DocumentsService.GetEmployeeDocuments(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Document--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage DeleteEmployeeDocument([FromBody]AddDocumentRequest request)
        {
            try
            {
                Services.DocumentsService.DeleteEmployeeDocument(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.DocumentDeleteSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Employee Document--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
    }
}
