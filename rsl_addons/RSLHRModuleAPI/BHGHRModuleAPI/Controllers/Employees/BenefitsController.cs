﻿using log4net;
using NetworkModel;
using Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers.Employees
{
    public class BenefitsController : ABaseController
    {
       // private readonly ILog log = LogManager.GetLogger(typeof(BenefitsController));
        public BenefitsController(IServiceProxy service) : base(service)
        {

        }
        [HttpGet]
        public HttpResponseMessage GetEmployeeBenefits([FromUri]int employeeId)
        {
            try
            {
                BenefitsDetailResponse response = new BenefitsDetailResponse();
                response = Services.BenefitsService.GetEmployeeBenefits(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Benefits--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage AddAmendBenefits([FromBody]BenefitsDetail model)
        {
            try
            {

                Services.BenefitsService.AddAmendBenefits(model);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.BenefitSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Employee Benefits--------------------------");
                log.Error(ex.StackTrace);
                LogManager.Shutdown();
                Services.BenefitsService.Dispose();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpGet]
        public HttpResponseMessage GetBenefitVehicle([FromUri]int Id)
        {
            try
            {
                BenefitVehicle response = new BenefitVehicle();
                response=Services.BenefitsService.GetBenefitVehicle(Id);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage GetBenefitGeneral([FromBody]GeneralDataRequest data)
        {
            try
            {
                BenefitGeneral response = new BenefitGeneral();
                response = Services.BenefitsService.GetBenefitsGeneral(data);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetBenefitVehicles([FromBody]GeneralDataRequest data)
        {
            try
            {
                
                var response = Services.BenefitsService.GetBenefitsVehicles(data);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }



        [HttpPost]
        public HttpResponseMessage GetBenefitHealth([FromBody]GeneralDataRequest data)
        {
            try
            {
                BenefitHealth response = new BenefitHealth();
                response = Services.BenefitsService.GetBenefitsHealth(data);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetBenefitPension([FromBody]GeneralDataRequest data)
        {
            try
            {
                BenefitPension response = new BenefitPension();
                response = Services.BenefitsService.GetBenefitsPension(data);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage GetBenefitPRP([FromBody]GeneralDataRequest data)
        {
            try
            {
                BenefitPRP response = new BenefitPRP();
                response = Services.BenefitsService.GetBenefitsPRP(data);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetBenefitsHealthHistory([FromUri]int BenefitHealthId)
        {
            try
            {
                List<BenefitHealthHistory> response = new List<BenefitHealthHistory>();
                response = Services.BenefitsService.GetBenefitsHealthHistory(BenefitHealthId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Benefit Health History--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage GetBenefitSubscription([FromBody]GeneralDataRequest data)
        {
            try
            {
                List<BenefitSubscription> response = new List<BenefitSubscription>();
                response = Services.BenefitsService.GetBenefitsSubscription(data);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage DeleteBenefitV5([FromBody]BenefitsDetail request)
        {
            try
            {

                Services.BenefitsService.DeleteBenefitV5(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.UserDetailUpdatedSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Health Record--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }


        }
        [HttpGet]
        public HttpResponseMessage GetCurrentFinancialYear(int Id)
        {
            try
            {
                CurrentFinancialYear response = new CurrentFinancialYear();
                response = Services.BenefitsService.GetCurrentFinancialYear(Id);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Employee Skills--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
    }
}
