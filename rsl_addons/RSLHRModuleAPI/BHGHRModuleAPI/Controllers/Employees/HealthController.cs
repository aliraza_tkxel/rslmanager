﻿using log4net;
using NetworkModel;
using NetworkModel.NetwrokModel;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers
{
    public class HealthController : ABaseController
    {
        //private readonly ILog log = LogManager.GetLogger(typeof(HealthController));

        public HealthController(IServiceProxy service) : base(service)
        {

        }
        public HealthRequest getDisabilityDetailForDifference(HealthRequest req)
        {
            try
            {
                HealthRequest result = new HealthRequest();
                HealthListResponse response = new HealthListResponse();
                response = Services.HealthService.getDifficultiesAndDisablities(req.employeeId);
                DifficultiesAndDisablities disability = new DifficultiesAndDisablities();
                disability = response.Disablities.Where(x => x.diffDisabilityId == req.diffDisabilityId).FirstOrDefault();

                result.disablityId = disability.disablityId ?? 0;
                result.docPath = disability.docPath;
                result.documentTitle = disability.documentTitle;
                result.impactOnWork = disability.impactOnWork;
                result.isLongTerm = disability.isLongTerm;
                result.isOccupationalHealth = disability.isOccupationalHealth;
                result.loggedBy = disability.loggedBy ?? 0;
                result.loggedByString = disability.loggedByString;
                result.notes = disability.notes;
                result.reviewDate = disability.reviewDate;
                result.notifiedDate = disability.notifiedDate;
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpGet]
        public HttpResponseMessage GetDifficultiesAndDisablities(int employeeId)
        {
            try
            {
                HealthListResponse response = new HealthListResponse();
                response = Services.HealthService.getDifficultiesAndDisablities(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------get Difficulties And Disablities--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }


        }

        [HttpPost]
        public HttpResponseMessage AddAmendHealthRecord([FromBody]HealthRequest request)
        {
            try
            {
                var diffs = Services.HealthService.getAllDisabilities();
                if (request.diffDisabilityId == 0) // request.diffDisabilityId tells that a new disability is being added for an employee
                {
                    string body = "<p>The Health details have been updated for " + Services.EmployeesService.GetEmployeeName(request.employeeId) + "</p>";
                    body += "<p>The amendments are as follows: </p><br>";
                    body += "<p><b>Disability or health condition</b>: " + diffs[Convert.ToInt32(request.disablityId)] + "</p>";
                    body += "<p><b>Logged</b>: " + request.loggedByString + "</p>";
                    body += "<p><b>Notified</b>: " + request.notifiedDate + "</p>";
                    body += "<p><b>Details</b>: " + request.notes + "</p>";
                    body += "<p><b>Impact on Work</b>: " + request.impactOnWork + "</p>";
                    if (request.isLongTerm != null && request.isLongTerm == true)
                    {
                        body += "<p><b>Long term</b>: Yes </p>";
                    }
                    else
                    {
                        body += "<p><b>Long term</b>: No </p>";
                    }
                    body += "<p><b>Occupational Health Referral: </b>" + request.isOccupationalHealth + "</p>";
                    body += "<p><b>Review:</b> " + request.reviewDate + "</p>";
                    string subject = "New health condition added.";
                    //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                    string receiverEmail = ApplicationConstants.hrTeamEmailAddress;



                    EmailHelper.sendEmail(body, subject, receiverEmail);
                }
                else
                {
                    string[] list = { "disablityId", "notes", "loggedBy", "loggedByString", "notifiedDate", "isLongTerm", "impactOnWork", "isOccupationalHealth", "reviewDate" };
                    var current = request;
                    if (request.employeeId != 0)
                    {
                        var old = getDisabilityDetailForDifference(request);
                        var differences = GeneralHelper.Differences(current, old);
                        var differencesList = differences.Where(dif => list.Contains(dif.Item1)).ToList();
                        if (differencesList.Count > 0)
                        {
                            string body = "<p>The Health details have been updated for <b>" + Services.EmployeesService.GetEmployeeName(request.employeeId) + "</b></p>";
                            body += "<p>The amendments are as follows: </p><br>";
                            body += GetTableHeader();
                            foreach (var val in differencesList)
                            {
                                if (val.Item1 == "disablityId")
                                {
                                    body += GetTableRow("Disability or health condition", diffs[Convert.ToInt32((int)val.Item2)], diffs[Convert.ToInt32((int)val.Item3)]);
                                }
                                else
                                {
                                    body += GetTableRow(val.Item1, "" + val.Item2 + "", "" + val.Item3 + "");
                                }
                            }
                            body += "</table>";
                            string subject = "Health condition updated.";
                            //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                            string receiverEmail = ApplicationConstants.hrTeamEmailAddress;
                            EmailHelper.sendEmail(body, subject, receiverEmail);
                        }
                    }

                }
                Services.HealthService.AddAmendHealth(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.UserDetailUpdatedSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Health Record--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }


        }


        [HttpPost]
        public HttpResponseMessage DeleteDisabilityHealthRecord([FromBody]HealthRequest request)
        {
            try
            {

                Services.HealthService.DeleteDisabilityHealth(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.UserDetailUpdatedSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Amend Health Record--------------------------");
                log.Error(ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }


        }
    }
}
