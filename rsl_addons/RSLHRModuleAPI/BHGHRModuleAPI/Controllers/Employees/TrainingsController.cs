﻿using NetworkModel;
using NetwrokModel.Reports;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers.Employees
{
    public class TrainingsController : ABaseController
    {
        public TrainingsController(IServiceProxy service) : base(service)
        {

        }
        [HttpPost]
        public HttpResponseMessage AddEmployeeTrainings([FromBody]AddEmployeeTrainings request)
        {
            try
            {
                string employeeName = Services.EmployeesService.GetEmployeeName(request.employeeId ?? 0);
                if (employeeName != null)
                {
                    string body = "<p>The training item <b>" + request.course + "</b> has been added to <b>" + employeeName + "</b> record.  </p>";
                    string subject = "New Training Item Added";
                    //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                    string receiverEmail = ApplicationConstants.hrTeamEmailAddress;
                    EmailHelper.sendEmail(body, subject, receiverEmail);
                }
                var response = Services.TrainingsService.AddEmployeeTrainings(request);
                if (response == false)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.TrainingConflictMessage);
                }
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.TrainingSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Employee Training--------------------------");
                log.Error(ex.Message + " " + ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #region >>> Lookup <<<

        [HttpGet]
        public HttpResponseMessage GetAllDirectorates()
        {
            try
            {
                AnnualLeaveReportResponse response = new AnnualLeaveReportResponse();
                response = Services.TrainingsService.GetAllDirectorates();
                return Request.CreateResponse(HttpStatusCode.OK, response);

            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Training--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetTeamsByDirectorates(int directorateId)
        {
            try
            {
                var response = new List<LookUpResponseModel>();
                response = Services.TrainingsService.GetTeamsByDirectorates(directorateId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Training--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetEmployeesList(EmployeesTrainingRequest request)
        {
            log.Debug("Enter:");
            try
            {
                EmployeesResponse response = new EmployeesResponse();
                response = Services.TrainingsService.GetEmployeesList(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees List--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetTrainingApprovalLookups()
        {
            try
            {
                TrainingApprovalLookupResponse response = new TrainingApprovalLookupResponse();
                response = Services.TrainingsService.GetTrainingApprovalLookups();
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region >>> Group Training <<<

        [HttpPost]
        public HttpResponseMessage AddEmpployeeGroupTrainings([FromBody]AddEmployeeGroupTraining employeGroupTrainingModel)
        {
            try
            {
                Services.TrainingsService.AddEmployeeGroupTrainings(employeGroupTrainingModel);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.GroupTrainingSuccessMessage);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }

        }
        [HttpPost]
        public HttpResponseMessage RemoveEmployeeFromGroupTraining(AddOrRemoveEmployeeInGroupTraining request)
        {
            try
            {
                Services.TrainingsService.RemoveEmployeeFromGroupTraining(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.RemoveEmployeeFromGroupTrainingSuccessMessage);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage AddEmployeeInGroupTraining(AddOrRemoveEmployeeInGroupTraining request)
        {
            try
            {
                Services.TrainingsService.AddEmployeeInGroupTraining(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.AddEmployeeInGroupTrainingSuccessMessage);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage UpdateGroupTrainingInfo(AddEmployeeGroupTraining request)
        {
            try
            {
                Services.TrainingsService.UpdateGroupTrainingInfo(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.GroupTrainingUpdateSuccessMessage);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.ToString());
            }
        }


        [HttpGet]
        public HttpResponseMessage GetGroupDetails(int groupId)
        {
            try
            {
                AddEmployeeGroupTraining response = new AddEmployeeGroupTraining();
                response = Services.TrainingsService.GetGroupDetails(groupId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Training--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetGroupTrainingsList(GroupTrainingApprovalList request)
        {
            try
            {
                GroupTrainingListResponse response = new GroupTrainingListResponse();
                response = Services.TrainingsService.GetGroupTrainingsList(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Training--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region >>> Report <<<
        [HttpGet]
        public HttpResponseMessage GetEmployeesByTeam(int teamId)
        {
            GroupTrainingApprovalListResponse response = new GroupTrainingApprovalListResponse();
            try
            {
                response = Services.TrainingsService.GetEmployeesByTeam(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Teams--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetTrainingApprovalList(TrainingApprovalListRequest request)
        {
            try
            {
                GroupTrainingApprovalListResponse response = new GroupTrainingApprovalListResponse();
                response = Services.TrainingsService.GetTrainingApprovalList(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        [HttpPost]
        public HttpResponseMessage GetEmployeesTrainings([FromBody]CommonRequest model)
        {
            try
            {
                var response = Services.TrainingsService.GetEmployeesTrainings(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Training--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetEmployeesTrainings(int employeeId)
        {
            try
            {
                var response = Services.TrainingsService.GetEmployeesTrainings(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Training--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage UpdateEmployeeTraining([FromBody]UpdateEmployeeTraining request)
        {
            try
            {
                Services.TrainingsService.UpdateEmployeeTraining(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.TrainingUpdateSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Training--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage ApproveDeclineGroupTrainings([FromBody]GroupTrainingApproveDecline request)
        {
            try
            {
                Services.TrainingsService.ApproveDeclineGroupTrainings(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.TrainingUpdateSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Training--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
    }
}
