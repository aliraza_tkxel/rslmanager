﻿using log4net;
using NetworkModel;
using Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers.Employees
{

    public class PostController : ABaseController
    {
        //private readonly ILog log = LogManager.GetLogger(typeof(PostController));

        public PostController(IServiceProxy service) : base(service)
        {

        }
        #region Post Tab Data

        #region Update Emoployee document

        [HttpPost]
        public HttpResponseMessage UpdateEmployeeReferenceDocument(EmployeeDocument request)
        {

            try
            {

                Services.PostTabService.UpdateEmployeeReferenceDocument(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.DocumentSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("--------------- Update Employee document --------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Get Employee Documents

        public HttpResponseMessage GetEmployeeReferenceDocuments(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                List<EmployeeDocument> documents = new List<EmployeeDocument>();
                documents = Services.PostTabService.GetEmployeeReferenceDocuments(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, documents);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Documents--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Get Employee Job Details

        public HttpResponseMessage GetEmployeeJobDetails(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                PostTabResponse response = new PostTabResponse();
                JobPostDetail detail = new JobPostDetail();
                detail = Services.PostTabService.GetEmployeeJobDetail(employeeId);
                response.jobDetail = detail;
                response.postTabLookups = Services.PostTabService.LoadPostTabLookups(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Job Details--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion



        #region Amend Employee Job Details

        public HttpResponseMessage AmendJobDetail(JobPostDetail detail)
        {
            log.Debug("Enter:");
            try
            {

                JobPostDetail _detail = new JobPostDetail();
                _detail = Services.PostTabService.AmendJobDetail(detail);

                return Request.CreateResponse(HttpStatusCode.OK, _detail);
            }
            catch (Exception ex)
            {
                log.Error("---------------Amend Employee Job Details--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion



        #region Get Jobroles against team Id

        public HttpResponseMessage GetJobRolesForTeamId(int teamId)
        {
            log.Debug("Enter:");
            try
            {
                List<LookUpResponseModel> detail = new List<LookUpResponseModel>();
                detail = Services.PostTabService.GetJobRoleAgainstTeamId(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, detail);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Jobroles against team Id--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetDirectorForTeamId(int teamId)
        {
            log.Debug("Enter:");
            try
            {
                var response = "";
                response = Services.PostTabService.GetDirectorForTeamId(teamId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Director against team Id--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Get Employee Job Role History 

        public HttpResponseMessage GetEmployeeJobRoleHistory(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                List<JobRoleHistory> detail = new List<JobRoleHistory>();
                detail = Services.PostTabService.GetEmployeeJobRoleHistory(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, detail);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Job Role History--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Get Logged In User Team 

        public HttpResponseMessage GetLoggedInUserTeam(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                var team = Services.PostTabService.GetLoggedInUserTeam(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, team);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Logged In User Team--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region >>> Working Pattren

        #region Get Employee Days Timings

        public HttpResponseMessage GetEmployeeDaysTimings(int employeeId, int wid,string day)
        {
            log.Debug("Enter:");
            try
            {
                var response = Services.PostTabService.GetEmployeeDaysTimings(employeeId, wid,day);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Days Timings--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion
        #region Get Employee Working Hours

        public HttpResponseMessage GetEmployeeWorkingHours(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                WorkingHoursParent response = new WorkingHoursParent();
                response = Services.PostTabService.GetEmployeeWorkingHours(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Working Hours--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Get Employee Current Working Pattern

        public HttpResponseMessage GetEmpCurrentWorkingPattren(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                WorkingHoursParent response = new WorkingHoursParent();
                response = Services.PostTabService.GetEmpCurrentWorkingPattren(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Current Working Pattern--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Amend Employee Working Hours

        public HttpResponseMessage AmendEmployeeWorkingHours(WorkingHoursParent details)
        {
            log.Debug("Enter:");
            try
            {
                WorkingHoursParent response = new WorkingHoursParent();
                response = Services.PostTabService.AmendEmployeeWorkingHours(details);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Amend Employee Working Hours--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #endregion



        #region Add salary Ammendment Document

        public HttpResponseMessage AddSalaryAmmendment(SalaryAmendment salary)
        {
            log.Debug("Enter:");
            try
            {
                SalaryAmendment response = new SalaryAmendment();
                response = Services.PostTabService.AddSalaryAmmendment(salary);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.SalaryAmendmentAddSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add salary Ammendment Document--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        [HttpGet]
        public HttpResponseMessage RemoveSalaryAmendment(int salaryAmendmentId)
        {
            log.Debug("Enter:");
            try
            {

                bool success = Services.PostTabService.RemoveSalaryAmmendment(salaryAmendmentId);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.SalaryAmendmentRemovedSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Remove Salary Amendment--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }


        public HttpResponseMessage UpdateEmployeeContractRoleDocument(UpdateEmployeeContractRoleRequest request)
        {
            log.Debug("Enter:");
            try
            {

                bool success = Services.PostTabService.UpdateEmployementContract(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.DocumentUpdateSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Update Employee Contract Role Document--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetHolidayRule(int eid)
        {
            log.Debug("Enter:");
            try
            {
                var response = new HolidayRule();
                response = Services.PostTabService.GetHolidayRule(eid);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Holiday Rule--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage GetGradePointLookup(int eid, int? isBRS)
        {
            log.Debug("Enter:");
            try
            {
                GradePointLookupResponse response = new GradePointLookupResponse();
                response.postTabLookups = Services.PostTabService.GetGradePointLookup(eid, isBRS);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Grade Point Lookup--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage getJobDetailNote(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                List<JobDetailNoteResponse> response = new List<JobDetailNoteResponse>();
                response = Services.PostTabService.GetJobDetailNote(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Job Detail Note--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage saveJobDetailNote(JobDetailNoteResponse request)
        {
            log.Debug("Enter:");
            try
            {
                JobDetailNoteResponse response = Services.PostTabService.SaveJobDetailNote(request);

                if (response.jobDetailNoteID > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Services.PostTabService.GetJobDetailNote(request.employeeId));
                }

                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.JobDetailNoteFailureMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Save Job Detail Note--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion
    }
}
