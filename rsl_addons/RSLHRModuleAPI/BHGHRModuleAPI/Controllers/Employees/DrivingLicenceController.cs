﻿using log4net;
using NetworkModel;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;

namespace BHGHRModuleAPI.Controllers.Employees
{
    public class DrivingLicenceController : ABaseController 
    {
        public DrivingLicenceController(IServiceProxy service) : base(service)
        {

        }
        
        [HttpGet]
        public HttpResponseMessage GetDrivingLicence([FromUri]int employeeId)
        {
            try
            {
                DrivingLicenceResponse response = new DrivingLicenceResponse();
                response = Services.DrivingLicenceService.GetDrivingLicence(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employee Driving Licence--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        [HttpPost]
        public HttpResponseMessage AddUpdateDrivingLicence([FromBody]DrivingLicenceResponse model)
        {
            try
            {

                Services.DrivingLicenceService.AddUpdateDrivingLicence(model);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.LicenceSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Update Driving Licence--------------------------");
                log.Error(ex.StackTrace);
                LogManager.Shutdown();
                Services.BenefitsService.Dispose();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        public HttpResponseMessage UpdateDrivingLicenceImage(LicenceImageRequest request)
        {
            log.Debug("Enter:");
            try
            {

                bool success = Services.DrivingLicenceService.UpdateDrivingLicenceImage(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.LicenceImageSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
    }
}
