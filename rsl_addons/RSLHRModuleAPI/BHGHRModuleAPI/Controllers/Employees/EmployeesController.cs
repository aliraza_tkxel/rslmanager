﻿using NetworkModel;
using log4net;
using Services;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Utilities;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleAPI.Controllers
{
    public class EmployeesController : ABaseController
    {
        //  private readonly ILog log = LogManager.GetLogger(typeof(EmployeesController));

        public EmployeesController(IServiceProxy service) : base(service)
        {

        }

        #region GetEmployees
        [HttpPost]
        public HttpResponseMessage GetEmployeesList([FromBody] EmployeesRequest request)
        {
            log.Debug("Enter:");
            try
            {
                EmployeesResponse response = new EmployeesResponse();
                response = Services.EmployeesService.GetEmployeesList(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees List--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Persoanal Detail
        public PersonalDetailResponce GetEmployeePersonalDetailForDifference(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                PersonalDetailResponce response = new PersonalDetailResponce();
                response = Services.EmployeesService.GetEmployeeDetail(employeeId);
                SharedLookUps lookups = Services.SharedService.loadSharedLookups(employeeId);
                response.lookUps = lookups;
                return response;
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Persoanal Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return null;
            }
        }
        public HttpResponseMessage GetEmployeePersonalDetail(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                PersonalDetailResponce response = new PersonalDetailResponce();
                response = Services.EmployeesService.GetEmployeeDetail(employeeId);
                SharedLookUps lookups = Services.SharedService.loadSharedLookups(employeeId);
                response.lookUps = lookups;
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Persoanal Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Get Employee Contact Detail

        public EmployeeContactDetail GetEmployeeConatctDetailForDifference(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                EmployeeContactDetail response = new EmployeeContactDetail();
                response = Services.EmployeesService.GetEmployeeContactDetail(employeeId);
                return response;
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return null;
            }
        }
        public HttpResponseMessage GetEmployeeConatctDetail(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                EmployeeContactDetail response = new EmployeeContactDetail();
                response = Services.EmployeesService.GetEmployeeContactDetail(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Contact Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Update employee personal detail
        [HttpPost]
        public HttpResponseMessage UpdateEmployeePersonalDetail(EmployeePersonalDetail request)
        {
            log.Debug("Enter:");
            try
            {
                var religions = Services.EmployeesService.GetreligionDataList();
                var sexualOrientations = Services.EmployeesService.GetsexualOrientationDataList();
                var maritalStatuses = Services.EmployeesService.GetmaritalStatusDataList();
                var ethnicities = Services.EmployeesService.GetEthnicityList();
                var titles = Services.EmployeesService.GetTitlesDataList();

                if (request.employeeId != 0)
                {
                    var result = GetEmployeePersonalDetailForDifference(request.employeeId);
                    int defaultOldValue = 0;
                    EmployeePersonalDetail old = result.employeeDetail;
                    var current = request;
                    current.firstName = current.firstName.Trim();
                    current.lastName = current.lastName.Trim();
                    if (!string.IsNullOrEmpty(current.middleName))
                    {
                        current.middleName = current.middleName.Trim();
                    }
                    old.firstName = old.firstName.Trim();
                    old.lastName = old.lastName.Trim();
                    if (!string.IsNullOrEmpty(old.middleName))
                    {
                        old.middleName = old.middleName.Trim();
                    }
                    List<Tuple<string, object, object>> differences = GeneralHelper.Differences(current, old);
                    string[] list = { "title", "firstName", "middleName", "lastName", "aka", "dob", "gender", "niNumber", "dbsDate", "reference", "maritalStatus", "ethnicity", "religion", "sexualOrientation" };
                    var differencesList = differences.Where(dif => list.Contains(dif.Item1)).ToList();

                    if (differencesList.Count > 0)
                    {
                        string body = "<p>The personal details for <b>" + old.firstName + " " + old.lastName + "</b> have been amended and the ﬁeld that has been changed.</p><br>";
                        body += GetTableHeader();
                        foreach (var val in differencesList)
                        {
                            defaultOldValue = -1;
                            

                            switch (val.Item1)
                            {
                                case "maritalStatus":
                                    if (val.Item3 != null)
                                    {
                                        defaultOldValue = Convert.ToInt32(val.Item3);
                                    }
                                    if (maritalStatuses[Convert.ToInt32(val.Item2)] != null && defaultOldValue != -1 && maritalStatuses[defaultOldValue] != null)
                                        body += GetTableRow(val.Item1, maritalStatuses[Convert.ToInt32(val.Item2)], maritalStatuses[defaultOldValue]);
                                    break;
                                case "title":
                                    if (val.Item3 != null)
                                    {
                                        defaultOldValue = Convert.ToInt32(val.Item3);
                                    }
                                    if (titles[Convert.ToInt32(val.Item2)] != null && defaultOldValue != -1 && titles[defaultOldValue] != null)
                                        body += GetTableRow(val.Item1, titles[Convert.ToInt32(val.Item2)], titles[defaultOldValue]);
                                    break;
                                case "ethnicity":
                                    if (val.Item3 != null)
                                    {
                                        defaultOldValue = Convert.ToInt32(val.Item3);
                                    }
                                    if (ethnicities[Convert.ToInt32(val.Item2)] != null && defaultOldValue != -1 && ethnicities[defaultOldValue] != null)
                                        body += GetTableRow(val.Item1, ethnicities[Convert.ToInt32(val.Item2)], ethnicities[defaultOldValue]);
                                    break;
                                case "religion":
                                    if (val.Item3 != null)
                                    {
                                        defaultOldValue = Convert.ToInt32(val.Item3);
                                    }
                                    if (Convert.ToInt32(val.Item2) != 0 && religions[Convert.ToInt32(val.Item2)] != null && defaultOldValue != -1 && religions[defaultOldValue] != null)
                                        body += GetTableRow(val.Item1, religions[Convert.ToInt32(val.Item2)], religions[defaultOldValue]);
                                    break;
                                case "sexualOrientation":
                                    if (val.Item3 != null)
                                    {
                                        defaultOldValue = Convert.ToInt32(val.Item3);
                                    }
                                    if (sexualOrientations[Convert.ToInt32(val.Item2)] != null && defaultOldValue != -1 && sexualOrientations[defaultOldValue] != null)
                                        body += GetTableRow(val.Item1, sexualOrientations[Convert.ToInt32(val.Item2)], sexualOrientations[defaultOldValue]);
                                    break;
                                default:
                                    body += GetTableRow(val.Item1, ""+val.Item2+"", "" + val.Item3+"");
                                    break;
                            }
                        }
                        body += "</table>";
                        string subject = "Personal Detail Changes";
                        //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                        string receiverEmail = ApplicationConstants.hrTeamEmailAddress;

                      

                        EmailHelper.sendEmail(body, subject, receiverEmail);
                    }
                }


                UpdateEmployeeDetailResponce responce = new UpdateEmployeeDetailResponce();
                EmployeePersonalDetail detail = new EmployeePersonalDetail();
                detail = Services.EmployeesService.UpdateEmployeePersonalDetail(request);
                responce.personalDetail = detail;
                responce.isSuccessFul = true;
                responce.message = UserMessageConstants.PersonalDetailUpdatedSuccessfully;
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Update employee personal detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Update employee contact detail
        [HttpPost]
        public HttpResponseMessage UpdateEmployeeContactDetail(EmployeeContactDetail request)
        {
            log.Debug("Enter:");
            try
            {
                if (request.employeeId != 0)
                {
                    var old = GetEmployeeConatctDetailForDifference(request.employeeId);
                    string employeeName = Services.EmployeesService.GetEmployeeName(request.employeeId);
                    var current = request;
                    var differences = GeneralHelper.Differences(current, old);
                    string[] list = { "address1", "address2", "address3", "city", "county", "postalCode", "homePhone", "homeEmail", "mobile", "mobilePersonal", "workDD", "workEmail", "emergencyContact", "nextOfKin", "emergencyContact", "emergencyTel", "emergencyRelationShip", "emergencyInfo" };
                    var differencesList = differences.Where(dif => list.Contains(dif.Item1)).ToList();

                    if (differencesList.Count > 0)
                    {
                        string body = "<p>The employee contact for " + employeeName + " has been updated with the following changes: </p><br>";
                        body += GetTableHeader();
                        foreach (var val in differencesList)
                        {
                            body += GetTableRow(val.Item1, "" + val.Item2 + "", "" + val.Item3 + "");
                        }
                        body += "</table>";
                        string subject = "Contact Detail Changes";
                        //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                        string receiverEmail = ApplicationConstants.hrTeamEmailAddress;



                        EmailHelper.sendEmail(body, subject, receiverEmail);
                    }
                }
                UpdateEmployeeDetailResponce responce = new UpdateEmployeeDetailResponce();
                EmployeeContactDetail detail = new EmployeeContactDetail();
                detail = Services.EmployeesService.UpdateContactPersonalDetail(request);
                responce.contactDetail = detail;
                responce.isSuccessFul = true;
                responce.message = UserMessageConstants.ContactDetailUpdatedSuccessfully;
                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Update employee contact detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Update emoployee image

        [HttpPost]
        public HttpResponseMessage UpdateEmployeeImage(EmployeeImage request)
        {

            try
            {

                Services.EmployeesService.UpdateEmployeeImage(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.UpdateProfileSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Update employee image--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Get Employee 's
        public HttpResponseMessage GetEmployeeGifts(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                List<Gift> gifts = Services.GiftService.GetGiftsForEmployee(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, gifts);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Gifts--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetEmployeeGiftMaster(GiftsRequest request)
        {
            log.Debug("Enter:");
            try
            {
                GiftMasterResponse gifts = Services.GiftService.GetEmployeeGiftMaster(request);
                return Request.CreateResponse(HttpStatusCode.OK, gifts);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Gifts--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Post Employee Gifts
        public HttpResponseMessage AddEmployeeGifts(GiftAndHospitality giftObj)
        {
            log.Debug("Enter:");
            try
            {
                string employeeName = Services.EmployeesService.GetEmployeeName(giftObj.employeeId);
                string body = "<p>A Gifts and Hospitality form has been submitted by " + employeeName + ". </p>";
                string subject = "Gifts and hospitality form Submitted";
                //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                string receiverEmail = ApplicationConstants.hrTeamEmailAddress;


                EmailHelper.sendEmail(body, subject, receiverEmail);
                bool result = Services.GiftService.AddGift(giftObj);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.EmployeeGiftSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Gifts--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Update Gifts Status
        public HttpResponseMessage UpdateGiftStatus(Gift giftObj)
        {
            log.Debug("Enter:");
            try
            {
                bool result = Services.GiftService.UpdateGiftStatus(giftObj);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.EmployeeGiftEditSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Gifts--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Get Employee Paypoint Submission
        public HttpResponseMessage GetEmployeePaypointSubmission(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                PaypointDetail detail = Services.GiftService.GetPaypointDetailForEmplyee(employeeId);
                return Request.CreateResponse(HttpStatusCode.OK, detail);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Paypoint--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Get Paypoint by Paypoint Id
        public HttpResponseMessage GetPaypointById(int paypointId)
        {
            log.Debug("Enter:");
            try
            {
                PaypointDetail detail = Services.GiftService.GetPaypointDetailByPaypointId(paypointId);
                return Request.CreateResponse(HttpStatusCode.OK, detail);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Paypoint--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Get Grade Point Salary
        [HttpPost]
        public HttpResponseMessage GetGradePointSalary(GradePointSalary gradePoint)
        {
            log.Debug("Enter:");
            try
            {
                decimal detail = Services.GiftService.GetGradePointSalary(gradePoint);
                return Request.CreateResponse(HttpStatusCode.OK, detail);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Paypoint--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Post Employee Paypoint Submission
        public HttpResponseMessage AddEmployeePaypointSubmission(Paypoint payPoint)
        {
            log.Debug("Enter:");
            try
            {
                bool result = Services.GiftService.AddPaypointDetailForEmplyee(payPoint);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.EmployeePaypointSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Employees Paypoint--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Get Declaration Of Interes
        [HttpGet]
        public HttpResponseMessage GetDeclarationOfInterest(int employeeId)
        {

            try
            {
                DeclarationOfInterestResponse response = new DeclarationOfInterestResponse();
                response = Services.DeclerationOfInterestService.GetDeclarationOfInterest(employeeId);

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Declaration Of Interes--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetDeclarationOfInterestById(int interestId)
        {

            try
            {
                DeclarationOfInterestResponse response = new DeclarationOfInterestResponse();
                response = Services.DeclerationOfInterestService.GetDeclarationOfInterestById(interestId);

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Declaration Of Interes--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        [HttpPost]
        public HttpResponseMessage UpdateDeclarationOfInterestStatus(UpdateDeclationOfInterestRequest request)
        {
            try
            {
                string body = "";
                body = "A Declaration of Interest has been submitted by <b>" + Services.EmployeesService.GetEmployeeName(request.employeeId) + ".</b>";
                string subject = "Declaration of interest submitted by " + Services.EmployeesService.GetEmployeeName(request.employeeId);
                //string receiverEmail = "hrteamsbbs@broadlandgroup.org"; // TODO
                string receiverEmail = ApplicationConstants.hrTeamEmailAddress;



                EmailHelper.sendEmail(body, subject, receiverEmail);

                bool responce = Services.DeclerationOfInterestService.UpdateDeclarationOfInterestStatus(request);
                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.DoiStatusSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Update Declaration Of Interest Status--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Add Declaration Of Interest
        [HttpPost]
        public HttpResponseMessage AddDeclarationOfInterest(DeclarationOfInterestDataRequest request)
        {

            try
            {

                Services.DeclerationOfInterestService.AddDeclarationOfInterest(request);

                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.DoiSuccessMessage);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Declaration Of Interest--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        [HttpPost]
        public HttpResponseMessage GetEmployeesMainDetail(EmployeesRequest model)
        {
            EmployeesResponse response = new EmployeesResponse();
            try
            {

                response = Services.EmployeesService.GetEmployeesMainDetail(model);

                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Employees Main Detail--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #region Get Paypoint report
        [HttpPost]
        public HttpResponseMessage GetPaypointReport(PaypointReportRequest request)
        {

            try
            {

                PayppointReportResponce responce = Services.GiftService.GetPaypointReport(request);

                return Request.CreateResponse(HttpStatusCode.OK, responce);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Paypoint report--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion


        #region Update paypoint
        [HttpPost]
        public HttpResponseMessage UpdatePaypoint(UpdatePaypointRequest request)
        {

            try
            {

                bool responce = Services.GiftService.UpdatePaypointStatus(request);

                return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.PaypointUpdateSuccess);
            }
            catch (Exception ex)
            {
                log.Error("---------------Update paypoint--------------------------");
                log.Error(ex.StackTrace);
                log.Logger.Repository.Shutdown();
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

    }
}
