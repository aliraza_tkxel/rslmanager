﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BHGHRModuleAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "web",
                routeTemplate: "api/{callBy}/{controller}/{action}/{id}",
                defaults: new { callBy = "web", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
               name: "ios",
               routeTemplate: "api/{callBy}/{controller}/{action}/{id}",
               defaults: new { callBy = "ios", id = RouteParameter.Optional }
           );

            config.Routes.MapHttpRoute(
               name: "Default",
               routeTemplate: "{controller}/{action}/{id}",
               defaults: new { controller = "Push", id = RouteParameter.Optional }
           );
        }
    }
}
