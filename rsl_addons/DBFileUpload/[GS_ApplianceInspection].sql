USE [ArrearsUK]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GS_ApplianceInspection_PS_Appointment]') AND parent_object_id = OBJECT_ID(N'[dbo].[GS_ApplianceInspection]'))
ALTER TABLE [dbo].[GS_ApplianceInspection] DROP CONSTRAINT [FK_GS_ApplianceInspection_PS_Appointment]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_ApplianceInspection]    Script Date: 05/22/2012 12:05:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GS_ApplianceInspection]') AND type in (N'U'))
DROP TABLE [dbo].[GS_ApplianceInspection]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_ApplianceInspection]    Script Date: 05/22/2012 12:05:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GS_ApplianceInspection](
	[APPLIANCEINSPECTIONID] [int] IDENTITY(1,1) NOT NULL,
	[APPLIANCEID] [int] NOT NULL,
	[APPOINTMENTID] [int] NOT NULL,
	[ISINSPECTED] [bit] NOT NULL,
	[COMBUSTIONREADING] [varchar](15) NULL,
	[OPERATINGPRESSURE] [varchar](15) NULL,
	[SAFETYDEVICEOPERATIONAL] [varchar](10) NULL,
	[SPILLAGETEST] [varchar](10) NULL,
	[SMOKEPELLET] [varchar](10) NULL,
	[ADEQUATEVENTILATION] [varchar](10) NULL,
	[FLUEVISUALCONDITION] [varchar](10) NULL,
	[SATISFACTORYTERMINATION] [varchar](10) NULL,
	[FLUEPERFORMANCECHECKS] [varchar](10) NULL,
	[APPLIANCESERVICED] [varchar](10) NULL,
	[APPLIANCESAFETOUSE] [varchar](10) NULL,
	[INSPECTIONDATE] [smalldatetime] NULL,
 CONSTRAINT [PK_GS_ApplianceInspection] PRIMARY KEY CLUSTERED 
(
	[APPLIANCEINSPECTIONID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GS_ApplianceInspection]  WITH CHECK ADD  CONSTRAINT [FK_GS_ApplianceInspection_PS_Appointment] FOREIGN KEY([APPOINTMENTID])
REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO

ALTER TABLE [dbo].[GS_ApplianceInspection] CHECK CONSTRAINT [FK_GS_ApplianceInspection_PS_Appointment]
GO


