USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[PS_PropertyAgent]    Script Date: 05/22/2012 12:06:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PS_PropertyAgent]') AND type in (N'U'))
DROP TABLE [dbo].[PS_PropertyAgent]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[PS_PropertyAgent]    Script Date: 05/22/2012 12:06:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PS_PropertyAgent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LandlordName] [varchar](50) NOT NULL,
	[Address] [varchar](100) NULL,
	[PhoneNo] [varchar](20) NULL,
 CONSTRAINT [PK_PS_PropertyAgent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


