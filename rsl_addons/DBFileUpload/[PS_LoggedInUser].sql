USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[PS_LoggedInUser]    Script Date: 05/22/2012 12:05:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PS_LoggedInUser]') AND type in (N'U'))
DROP TABLE [dbo].[PS_LoggedInUser]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[PS_LoggedInUser]    Script Date: 05/22/2012 12:05:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PS_LoggedInUser](
	[LoggedInUsername] [varchar](50) NOT NULL,
	[LoggedInUserDateTime] [datetime] NULL,
	[LoggedInUserSalt] [varchar](50) NULL,
 CONSTRAINT [PK_PS_LoggedInUser] PRIMARY KEY CLUSTERED 
(
	[LoggedInUsername] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


