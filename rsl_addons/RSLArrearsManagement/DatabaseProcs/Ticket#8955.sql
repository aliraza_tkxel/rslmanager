-- =============================================      
--Written By:Aqib Javed      
--Created Date: <06/8/2015>      
-- Description: <Written to fix the issues specified in Ticket#8955>      
     
-- =============================================      
Update AM_Status set Title ='Initial Case Monitoring' where Title ='First Detection'
Update AC_PAGES set DESCRIPTION ='Income Management' where DESCRIPTION ='Arrears Management'
/* DELETING Helloo Outcome
   Note: There are number of Cases associated with 'Helloo' OUTCOME and deleting it will also delete the cases and their history which can't be roll back.
*/
SELECT ACT.*
FROM AM_LookupCode ALC
INNER JOIN AM_CaseHistory CH ON ALC.LookupCodeId = CH.OutcomeLookupCodeId
LEFT JOIN AM_Activity ACT ON CH.CaseHistoryId = ACT.CaseHistoryId
LEFT JOIN AM_CaseAndStandardLetter CSL ON CH.CaseHistoryId = CSL.CaseHistoryId
WHERE ALC.CodeName = 'Helloo'

BEGIN TRAN

DELETE CSL
FROM AM_LookupCode ALC
INNER JOIN AM_CaseHistory CH ON ALC.LookupCodeId = CH.OutcomeLookupCodeId
INNER JOIN AM_CaseAndStandardLetter CSL ON CH.CaseHistoryId = CSL.CaseHistoryId
WHERE ALC.CodeName = 'Helloo'

DELETE ACT
FROM AM_LookupCode ALC
INNER JOIN AM_CaseHistory CH ON ALC.LookupCodeId = CH.OutcomeLookupCodeId
INNER JOIN AM_Activity ACT ON CH.CaseHistoryId = ACT.CaseHistoryId
WHERE ALC.CodeName = 'Helloo'

DELETE CH
FROM AM_LookupCode ALC
INNER JOIN AM_CaseHistory CH ON ALC.LookupCodeId = CH.OutcomeLookupCodeId
WHERE ALC.CodeName = 'Helloo'

DELETE AM_LookupCode
WHERE CodeName = 'Helloo'

COMMIT TRAN