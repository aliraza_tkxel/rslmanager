/*
   Friday, January 1, 20167:06:37 PM
   User: TKxel
   Server: 10.0.1.75
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AM_Status
	DROP CONSTRAINT FK_AM_Status_AM_LookupCode1
GO
ALTER TABLE dbo.AM_Status
	DROP CONSTRAINT FK_AM_Status_AM_LookupCode2
GO
ALTER TABLE dbo.AM_Action
	DROP CONSTRAINT FK_AM_Action_AM_LookupCode1
GO
ALTER TABLE dbo.AM_Activity
	DROP CONSTRAINT FK_AM_Activity_AM_LookupCode1
GO
ALTER TABLE dbo.AM_PaymentPlan
	DROP CONSTRAINT FK_AM_PaymentPlan_AM_LookupCode1
GO
ALTER TABLE dbo.AM_LookupCode SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AM_LookupCode', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AM_LookupCode', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AM_LookupCode', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.AM_PaymentPlan SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AM_PaymentPlan', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AM_PaymentPlan', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AM_PaymentPlan', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.AM_Activity SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AM_Activity', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AM_Activity', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AM_Activity', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.AM_Action SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AM_Action', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AM_Action', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AM_Action', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.AM_Status SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AM_Status', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AM_Status', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AM_Status', 'Object', 'CONTROL') as Contr_Per 