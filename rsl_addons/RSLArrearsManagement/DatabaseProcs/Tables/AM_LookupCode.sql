Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_NAME = N'AM_LookupCode')
		BEGIN
			PRINT 'Table Exists';

			IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[AM_LookupCode]' ) AND name = 'IsActive')
				BEGIN
					ALTER TABLE AM_LookupCode ADD IsActive Bit Default 1 
					PRINT 'IsActive Column added successfully!';
				END--if
			ELSE
				BEGIN
					PRINT 'IsActive Column alreasy Exists';
				END	--else
		END --if
	
	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;   	
		END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	PRINT (@ErrorMessage)
END CATCH
GO
---==================================================
--===================================================
BEGIN TRANSACTION
BEGIN TRY 
	IF EXISTs(Select * from AM_LookupCode where IsActive is null)
		BEGIN
			Update AM_LookupCode set IsActive=1 where IsActive is null
			PRINT 'IsActive Column Updated Successfully!';
		END--if

	IF @@TRANCOUNT > 0
		BEGIN     
			COMMIT TRANSACTION;   	
		END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   	
		END
	DECLARE @ErrorMessageIsActive NVARCHAR(4000);
	DECLARE @ErrorSeverityIsActive INT;
	DECLARE @ErrorStateIsActive INT;

	SELECT @ErrorMessageIsActive = ERROR_MESSAGE(),
	@ErrorSeverityIsActive = ERROR_SEVERITY(),
	@ErrorStateIsActive = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessageIsActive, @ErrorSeverityIsActive, @ErrorStateIsActive);
	PRINT (@ErrorMessageIsActive)
END CATCH