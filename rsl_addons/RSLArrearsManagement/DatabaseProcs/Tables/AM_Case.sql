Use RSLBHALive
Go
BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'AM_Case')
BEGIN
  PRINT 'Table Exists';
 
 
 
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_AM_Case_AM_Resource')
	BEGIN  
		ALTER TABLE dbo.AM_Case ADD CONSTRAINT
				FK_AM_Case_AM_Resource FOREIGN KEY
				(
				InitiatedBy
				) REFERENCES dbo.AM_Resource
				(
				ResourceId
				) ON UPDATE  NO ACTION 
				 ON DELETE  NO ACTION 
				 
		PRINT('FK_AM_Case_AM_Resource added successfully!')		 
	END
ELSE
	BEGIN
	PRINT('FK_AM_Case_AM_Resource Already Exist!')		 
	END
	
--======================================================================================================================	
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_AM_Case_AM_Resource1')
	BEGIN  
		ALTER TABLE dbo.AM_Case ADD CONSTRAINT
			FK_AM_Case_AM_Resource1 FOREIGN KEY
			(
			ModifiedBy
			) REFERENCES dbo.AM_Resource
			(
			ResourceId
			) ON UPDATE  NO ACTION 
			 ON DELETE  NO ACTION 
	
				 
		PRINT('FK_AM_Case_AM_Resource1 added successfully!')		 
	END
ELSE
	BEGIN
	PRINT('FK_AM_Case_AM_Resource1 Already Exist!')		 
	END
--======================================================================================================================	
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_AM_Case_AM_Resource2')
	BEGIN  
		ALTER TABLE dbo.AM_Case ADD CONSTRAINT
			FK_AM_Case_AM_Resource2 FOREIGN KEY
			(
			CaseManager
			) REFERENCES dbo.AM_Resource
			(
			ResourceId
			) ON UPDATE  NO ACTION 
			 ON DELETE  NO ACTION 
	
				 
		PRINT('FK_AM_Case_AM_Resource2 added successfully!')		 
	END
ELSE
	BEGIN
	PRINT('FK_AM_Case_AM_Resource2 Already Exist!')		 
	END
--======================================================================================================================	
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_NAME ='FK_AM_Case_AM_Resource3')
	BEGIN  
		ALTER TABLE dbo.AM_Case ADD CONSTRAINT
			FK_AM_Case_AM_Resource3 FOREIGN KEY
			(
			CaseOfficer
			) REFERENCES dbo.AM_Resource
			(
			ResourceId
			) ON UPDATE  NO ACTION 
			 ON DELETE  NO ACTION 
	
				 
		PRINT('FK_AM_Case_AM_Resource3 added successfully!')		 
	END
ELSE
	BEGIN
	PRINT('FK_AM_Case_AM_Resource3 Already Exist!')		 
	END
 
END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH