USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AM_SP_GetHistoricalBalanceListExportToExcel]    Script Date: 03/28/2014 11:34:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Ali Raza>  
-- Create date: <13 Mar, 2014>  
-- Description: <Historical Balance List>  
-- ===============================================  
  

Create PROCEDURE [dbo].[AM_SP_GetHistoricalBalanceListExportToExcel]  
 @skipIndex int = 0,  
 @pageSize int = 50,  
 @caseOwnedById int=0,  
 @regionId int = 0,  
 @suburbId int = 0,  
    @customerStatus int = 0,  
    @assetType int =0,  
    @months int=0,  
    @years int=0,  
 @sortBy     varchar(100),  
    @sortDirection varchar(10)  
      
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
declare @orderbyClause varchar(50)  
declare @query varchar(8000)  
declare @subQuery varchar(8000)  
declare @RegionSuburbClause varchar(8000)  
  
 DECLARE @SQL_CLAUSE VARCHAR(300)  
DECLARE @SQL_STR VARCHAR(8000)  
DECLARE @THEDATE SMALLDATETIME  
DECLARE @REQUIREDDATE SMALLDATETIME  
DECLARE @CURRENTDATESTRING VARCHAR (40)  
DECLARE @COMPAREDATE SMALLDATETIME  
DECLARE @NEXTMONTHDATE SMALLDATETIME  
DECLARE @RESPECTIVEENDDATE SMALLDATETIME  
DECLARE @YEAREND SMALLDATETIME  
DECLARE @YEARSTART SMALLDATETIME  
DECLARE @HB_DR MONEY  
DECLARE @TENANCYSTART SMALLDATETIME  
DECLARE @LASTPAYMENTENDDATE SMALLDATETIME  
DECLARE @ADJ_MONTH INT  
DECLARE @NON_REQUIRED_DAYS INT  
DECLARE @A_12TH_OF_A_YEAR FLOAT  
DECLARE @TOTAL_HB_OWED_ON_DATE MONEY  
DECLARE @HB_OWED MONEY  
DECLARE @ADJ_YEARSTART SMALLDATETIME  
DECLARE @ADJ_YEAREND SMALLDATETIME  
DECLARE @MASTERAMOUNT MONEY  
DECLARE @CUSTOMERID INT  
DECLARE @CUSTOMERTYPE INT  
DECLARE @TENANCYID INT  
DECLARE @IN_CUSTOMERID INT  
DECLARE @FULLNAME VARCHAR(200)  
DECLARE @MAXVALUERANGE VARCHAR(200)  
DECLARE @MINVALUERANGE VARCHAR(200)  
DECLARE @MAXVALUERANGE_INNER VARCHAR(200)  
DECLARE @MINVALUERANGE_INNER VARCHAR(200)  
DECLARE @FR_ASATDATE VARCHAR(200)  
DECLARE @C_STATUS VARCHAR(200)  
DECLARE @NORECORDS INT 


CREATE TABLE #TBL_ESTHB (  
TenancyId  INT,     
CustomerName NVARCHAR(500),  
CustomerName2 VARCHAR(500),  
JointTenancyCount int,   
CustomerAddress VARCHAR(1000),  
RentBalance float ,   
HB float,   
TotalCost float,  
NetRentBalance float,   
Total float,  
ADVHB float  
)  
IF(@caseOwnedById = 0 )  
BEGIN  
 IF(@regionId = 0 and @suburbId = 0)  
 BEGIN  
  SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = P_DEVELOPMENT.PATCHID'  
 END  
 ELSE IF(@regionId > 0 and @suburbId = 0)  
 BEGIN  
  SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)   
 END  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_DEVELOPMENT.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') '   
 END  
END  
ELSE  
BEGIN  
  
IF(@regionId = 0 and @suburbId = 0)  
 BEGIN  
  SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID IN (SELECT PatchId   
              FROM AM_ResourcePatchDevelopment   
              WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true'')'  
 END  
 ELSE IF(@regionId > 0 and @suburbId = 0)  
 BEGIN  
  SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID IN (SELECT PatchId   
              FROM AM_ResourcePatchDevelopment   
              WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true'') AND P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)   
 END  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'P_DEVELOPMENT.PATCHID IN (SELECT PatchId   
              FROM AM_ResourcePatchDevelopment   
              WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true'') AND P_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_DEVELOPMENT.DEVELOPMENTID = ' + convert(varchar(10), @suburbId ) + ') '   
 END  
END  
  
SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection  
SET @query =   
    '
     INSERT INTO #TBL_ESTHB   
        (  
    TENANCYID,CustomerName,CustomerName2,JointTenancyCount,CustomerAddress,RentBalance,HB,TotalCost, NetRentBalance, Total ) 
    (
    SELECT    
       
     Max(AM_HistoricalBalanceList.TenancyId) as TenancyId,  
       
     (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName  
       FROM AM_Customer_Rent_Parameters  
       WHERE TenancyId = AM_HistoricalBalanceList.TenancyId  
       ORDER BY CustomerId ASC) as CustomerName,  
  
         (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName  
       FROM AM_Customer_Rent_Parameters  
       WHERE TenancyId = AM_HistoricalBalanceList.TenancyId  
       ORDER BY CustomerId DESC) as CustomerName2,  
  
         (SELECT Count(DISTINCT CustomerId)  
       FROM AM_Customer_Rent_Parameters  
       WHERE TenancyId = AM_HistoricalBalanceList.TenancyId) as JointTenancyCount,   
   
       
     ( SELECT  ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') + '', '' +  ISNULL(P__PROPERTY.TOWNCITY,'''')  
     FROM   P__PROPERTY   
         INNER JOIN  C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID  
         INNER JOIN P_DEVELOPMENT ON P_DEVELOPMENT.DEVELOPMENTID=P__PROPERTY.DEVELOPMENTID  
     WHERE C_TENANCY.TENANCYID = AM_HistoricalBalanceList.TENANCYID) AS CustomerAddress,   
       
     Max(ISNULL(AM_HistoricalBalanceList.RentBalance,0.00)) AS RentBalance,  
     Max(ISNULL(AM_HistoricalBalanceList.HB,0.00)) AS HB,  
     Max(ISNULL(AM_HistoricalBalanceList.SLB,0.00)) AS TotalCost,  
     Max(ABS(ISNULL(AM_HistoricalBalanceList.RentBalance,0.00)) - ABS(ISNULL(AM_HistoricalBalanceList.HB,0.00))) AS NetRentBalance,  
     Max(ABS(ISNULL(AM_HistoricalBalanceList.RentBalance,0.00)) + ABS(ISNULL(AM_HistoricalBalanceList.SLB,0.00))) As Total          
          
    FROM   
                    AM_HistoricalBalanceList   
     INNER JOIN  C__CUSTOMER ON AM_HistoricalBalanceList.CustomerId = C__CUSTOMER.CustomerId    
     INNER JOIN  G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId  
     INNER JOIN C_TENANCY ON AM_HistoricalBalanceList.TENANCYID = C_TENANCY.TenancyId  
     INNER JOIN P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID   
     INNER JOIN P_DEVELOPMENT ON P_DEVELOPMENT.DEVELOPMENTID=P__PROPERTY.DEVELOPMENTID  
    WHERE   
              Datepart(yyyy,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0))=CASE WHEN 0='+Convert(varchar(50), @years)+' THEN DATEPART(yyyy,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0)) ELSE '+Convert(varchar(50), @years)+' END   
                       and Datepart(mm,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0))=CASE WHEN 0 = '+Convert(varchar(50), @months)+' THEN DATEPART(mm,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0)) ELSE '+Convert(varchar(50), @months)+' END   
                       and '+ @RegionSuburbClause +'   
                       and C__CUSTOMER.CUSTOMERTYPE = CASE WHEN 0='+Convert(varchar(50), @customerStatus)+' THEN C__CUSTOMER.CUSTOMERTYPE ELSE '+Convert(varchar(50), @customerStatus)+' END   
        and P__PROPERTY.ASSETTYPE=CASE WHEN 0= '+Convert(varchar(50), @assetType)+'  THEN P__PROPERTY.ASSETTYPE ELSE '+Convert(varchar(50), @assetType)+' END   
        and AM_HistoricalBalanceList.TenancyId NOT IN ('  
       SET @subQuery = 'SELECT TenancyId FROM(  
          SELECT TOP('+convert(varchar(10),@skipIndex)+')   
          Max(AM_HistoricalBalanceList.CustomerId) AS CustomerId,  
          Max(AM_HistoricalBalanceList.TenancyId) as TenancyId,  
            
      (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName  
       FROM AM_Customer_Rent_Parameters  
       WHERE TenancyId = AM_HistoricalBalanceList.TenancyId  
       ORDER BY CustomerId ASC) as CustomerName,  
  
         (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName  
       FROM AM_Customer_Rent_Parameters  
       WHERE TenancyId = AM_HistoricalBalanceList.TenancyId  
       ORDER BY CustomerId DESC) as CustomerName2,  
  
         (SELECT Count(DISTINCT CustomerId)  
       FROM AM_Customer_Rent_Parameters  
       WHERE TenancyId = AM_HistoricalBalanceList.TenancyId) as JointTenancyCount,   
            
          ( SELECT  ISNULL(P__PROPERTY.HOUSENUMBER,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS1,'''') + '' '' + ISNULL(P__PROPERTY.ADDRESS2,'''') + '', '' +  ISNULL(P__PROPERTY.TOWNCITY,'''')  
          FROM   P__PROPERTY   
              INNER JOIN  C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID  
              INNER JOIN P_DEVELOPMENT ON P_DEVELOPMENT.DEVELOPMENTID=P__PROPERTY.DEVELOPMENTID  
          WHERE C_TENANCY.TENANCYID = AM_HistoricalBalanceList.TENANCYID) AS CustomerAddress,   
            
     Max(ISNULL(AM_HistoricalBalanceList.RentBalance,0.00)) AS RentBalance,  
     Max(ISNULL(AM_HistoricalBalanceList.HB,0.00)) AS HB,  
     Max(ISNULL(AM_HistoricalBalanceList.SLB,0.00)) AS TotalCost,  
     Max(ABS(ISNULL(AM_HistoricalBalanceList.RentBalance,0.00)) - ABS(ISNULL(AM_HistoricalBalanceList.HB,0.00))) AS NetRentBalance,  
     Max(ABS(ISNULL(AM_HistoricalBalanceList.RentBalance,0.00)) + ABS(ISNULL(AM_HistoricalBalanceList.SLB,0.00))) As Total          
  
  
          FROM AM_HistoricalBalanceList   
          INNER JOIN  C__CUSTOMER ON AM_HistoricalBalanceList.CustomerId = C__CUSTOMER.CustomerId    
          INNER JOIN  G_TITLE ON C__CUSTOMER.Title = G_TITLE.TitleId  
          INNER JOIN C_TENANCY ON AM_HistoricalBalanceList.TENANCYID = C_TENANCY.TenancyId  
          INNER JOIN P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID   
          INNER JOIN P_DEVELOPMENT ON P_DEVELOPMENT.DEVELOPMENTID=P__PROPERTY.DEVELOPMENTID  
          WHERE  Datepart(yyyy,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0))=CASE WHEN 0='+Convert(varchar(50), @years)+' THEN DATEPART(yyyy,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0)) ELSE '+Convert(varchar(50), @years)+' END   
              and Datepart(mm,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0))=CASE WHEN 0 = '+Convert(varchar(50), @months)+' THEN DATEPART(mm,ISNULL(AM_HistoricalBalanceList.AccountTimeStamp,0)) ELSE '+Convert(varchar(50), @months)+' END   
              and '+ @RegionSuburbClause +'   
              and C__CUSTOMER.CUSTOMERTYPE = CASE WHEN 0='+Convert(varchar(50), @customerStatus)+' THEN C__CUSTOMER.CUSTOMERTYPE ELSE '+Convert(varchar(50), @customerStatus)+' END   
              and P__PROPERTY.ASSETTYPE=CASE WHEN 0= '+Convert(varchar(50), @assetType)+'  THEN P__PROPERTY.ASSETTYPE ELSE '+Convert(varchar(50), @assetType)+' END   
   GROUP BY AM_HistoricalBalanceList.TenancyId   
            '  + @orderbyClause + ') as Temp  
         ) GROUP BY AM_HistoricalBalanceList.TenancyId   )
         '  
  
print(@query + @subQuery + @orderbyClause);  
exec(@query + @subQuery + @orderbyClause);  
   
  
  
-- START A CURSOR TO LOOP THROUGH THE CUSTOMERS IN EACH TENANCY  
-- : WE DO THIOS BECAUSE EACH CUSTOMER MIGHT HAVE THERE OWN HB SET UP  
--  : WE TRY TO NOT ADD IN AN HB VALUE WHICH HAS NOT BEEN ENDED ON AN ENDED TENANCY WHOS CUSTOMER HAVE SET UP A NEW TENANCY AND HB SCHEDULE  
  
  SET @SQL_STR = ' DECLARE CUSTOMER_HB CURSOR   
         FAST_FORWARD   
       FOR   
    SELECT  HBI.CUSTOMERID ,T.TENANCYID, HBI.initialSTARTDATE , ISNULL(HBA.ENDDATE,DATEADD(D,-1,HBI.INITIALSTARTDATE)),  
     HB_DR =   
     CASE   
      WHEN HBA.HBID IS NOT NULL THEN ((ABS(HBA.HB)/ (DATEDIFF(D,HBA.STARTDATE,HBA.ENDDATE)+1)))  
      WHEN HBA.HBID IS NULL THEN ((ABS(HBI.INITIALPAYMENT)/ (DATEDIFF(D,HBI.INITIALSTARTDATE,HBI.INITIALENDDATE)+1)))  
     END       
    FROM C_TENANCY T  WITH (NOLOCK)  
     INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID  
     INNER JOIN F_HBINFORMATION HBI ON HBI.TENANCYID = T.TENANCYID  
     LEFT JOIN F_HBACTUALSCHEDULE HBA ON HBA.HBID = HBI.HBID  
         AND HBA.HBROW = (SELECT MAX(HBROW) FROM F_HBACTUALSCHEDULE WHERE HBID = HBA.HBID AND VALIDATED = 1)  
     LEFT JOIN F_HBACTUALSCHEDULE HBA2 ON HBA2.HBID = HBI.HBID  
         AND HBA2.HBROW = (SELECT min(HBROW) FROM F_HBACTUALSCHEDULE WHERE HBID = HBA2.HBID AND VALIDATED is null)  
       
    WHERE  HBI.ACTUALENDDATE IS NULL '   
  
   
 EXECUTE (@SQL_STR)   
    
 OPEN CUSTOMER_HB  
 FETCH  NEXT FROM CUSTOMER_HB INTO @CUSTOMERID,@TENANCYID, @TENANCYSTART,@LASTPAYMENTENDDATE, @HB_DR  
   WHILE  @@FETCH_STATUS = 0  
      BEGIN  
    set @MASTERAMOUNT = 0  
      
    SET @REQUIREDDATE =  '1 ' + DATENAME(M, GETDATE()) + ' ' + CAST(DATEPART( YYYY, GETDATE()) AS VARCHAR)  
      
    SET @CURRENTDATESTRING = '1 ' + DATENAME(M, @TENANCYSTART)  
      
    SET @COMPAREDATE = @CURRENTDATESTRING + ' ' + CAST(DATEPART(YYYY,GETDATE()) AS VARCHAR)  
      
    SET @NEXTMONTHDATE = DATEADD(M,1,@REQUIREDDATE)  
      
    IF (@REQUIREDDATE) >= (@COMPAREDATE)  
     BEGIN  
     SET @YEARSTART = @COMPAREDATE       
     SET @RESPECTIVEENDDATE = DATEADD(YYYY, 1 , @YEARSTART)   
     SET @RESPECTIVEENDDATE = DATEADD(D, -1 , @RESPECTIVEENDDATE)  
     SET @YEAREND = @RESPECTIVEENDDATE       
      
     END  
    ELSE  
     BEGIN  
     SET @YEARSTART = @CURRENTDATESTRING + CAST((DATEPART( YYYY, GETDATE())-1) AS VARCHAR)  
     SET @RESPECTIVEENDDATE = DATEADD(YYYY, 1 , @YEARSTART)  
     SET @RESPECTIVEENDDATE = DATEADD(D, -1 , @RESPECTIVEENDDATE)      
     SET @YEAREND = @RESPECTIVEENDDATE  
      
     END  
      
    SET @ADJ_MONTH = DATEDIFF(M, @YEARSTART, GETDATE())+1  
      
    IF (DATEADD(D,1,@LASTPAYMENTENDDATE)) < @YEARSTART  
     BEGIN  
       
     SET @NON_REQUIRED_DAYS = 0  
     SET @A_12TH_OF_A_YEAR = 0.0  
     SET @TOTAL_HB_OWED_ON_DATE = 0  
     SET @A_12TH_OF_A_YEAR = (DATEDIFF(D, @YEARSTART, @YEAREND)+1)  
     set @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12         
     SET @HB_OWED = ((@ADJ_MONTH * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR  
       
     SET @ADJ_YEARSTART = DATEADD(YYYY, -1, @YEARSTART)  
     SET @ADJ_YEAREND = DATEADD(YYYY, -1, @YEAREND)  
       
     --loop whilst we still have whole years to the LAST_PAYMENT_END  
      WHILE (@LASTPAYMENTENDDATE < @ADJ_YEARSTART)  
       BEGIN  
       SET @HB_OWED = @HB_OWED + (@HB_DR * (DATEDIFF(D, @ADJ_YEARSTART, @ADJ_YEAREND)+1))     
       SET @ADJ_YEARSTART = DATEADD(YYYY, -1, @ADJ_YEARSTART)  
       SET @ADJ_YEAREND = DATEADD(YYYY, -1, @ADJ_YEAREND)  
  
        END    
        
      SET @NON_REQUIRED_DAYS = DATEDIFF(D, @ADJ_YEARSTART, @LASTPAYMENTENDDATE)  
        
      SET @A_12TH_OF_A_YEAR = DATEDIFF(D, @ADJ_YEARSTART, @ADJ_YEAREND)+1  
      SET @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR /12  
        
      SET @TOTAL_HB_OWED_ON_DATE = @HB_OWED + (((12 * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR)  
         
      SET @MASTERAMOUNT = @MASTERAMOUNT +  ISNULL(@TOTAL_HB_OWED_ON_DATE,0)  
             
        
      SET @TOTAL_HB_OWED_ON_DATE = 0  
      SET @HB_OWED = 0.00  
     END  
    ELSE  
     BEGIN  
     SET @A_12TH_OF_A_YEAR = 0.0  
     SET @NON_REQUIRED_DAYS = DATEDIFF(D, @YEARSTART, @LASTPAYMENTENDDATE)+1  
       
     SET @A_12TH_OF_A_YEAR = (DATEDIFF(D, @YEARSTART, @YEAREND)+1)  
     set @A_12TH_OF_A_YEAR = @A_12TH_OF_A_YEAR / 12  
     SET @TOTAL_HB_OWED_ON_DATE = (((@ADJ_MONTH * @A_12TH_OF_A_YEAR) - @NON_REQUIRED_DAYS) * @HB_DR)  
     SET @MASTERAMOUNT = @MASTERAMOUNT + ISNULL(@TOTAL_HB_OWED_ON_DATE,0)  
       
     SET @TOTAL_HB_OWED_ON_DATE = 0  
       
            
     END  
     
    -- IF ANY HB HAS BEEN PAID FOR THE FUTURE THEN WE NEED TO ADD THIS TO THE GROSS COST  
    -- SO THEN WE DONT ADD IT TO THE ANTICPATED HB  
    IF ISNULL(@MASTERAMOUNT,0) < 0  
     BEGIN  
       
     UPDATE #TBL_ESTHB SET   ADVHB = ISNULL(ADVHB,0) + ISNULL(@MASTERAMOUNT,0) WHERE ISNULL(TENANCYID,0) = @TENANCYID  
     END  
    ELSE  
    BEGIN    
     UPDATE #TBL_ESTHB SET   HB = ISNULL(HB,0) + ISNULL(ABS(@MASTERAMOUNT),0) WHERE ISNULl(TENANCYID,0) = @TENANCYID  
     END   
  
   set @MASTERAMOUNT = 0  
   FETCH NEXT FROM CUSTOMER_HB INTO  @CUSTOMERID,@TENANCYID,@TENANCYSTART,@LASTPAYMENTENDDATE, @HB_DR  
  END  
 END  
 CLOSE CUSTOMER_HB  
 DEALLOCATE CUSTOMER_HB  
   

  
DECLARE @END_SQL  VARCHAR(8000)
SET @END_SQL =' SELECT   TenancyId,CustomerName,CustomerName2,JointTenancyCount,CustomerAddress,
ISNULL(RentBalance,0.00) as RentBalance, ISNULL(HB,0.00) as HB, ISNULL(TotalCost,0.00) as TotalCost, ISNULL(NetRentBalance,0.00) as NetRentBalance, 
ISNULL(Total,0.00) as Total,ISNULL(ADVHB,0.00) as ADVHB 
FROM #TBL_ESTHB ' 
print @END_SQL  
EXECUTE (@END_SQL)
  
DROP TABLE #TBL_ESTHB    
  
