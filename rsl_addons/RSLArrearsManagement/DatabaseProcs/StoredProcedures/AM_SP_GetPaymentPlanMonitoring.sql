USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AM_SP_GetPaymentPlanMonitoring]    Script Date: 02/19/2016 10:51:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- =============================================  
ALTER PROCEDURE [dbo].[AM_SP_GetPaymentPlanMonitoring]  
   @postCode varchar(50)='',  
   @assignedToId int = 0,  
   @regionId int = 0,  
   @suburbId int = 0,  
   @allAssignedFlag bit,  
   @allRegionFlag bit,  
   @allSuburbFlag bit,  
   @skipIndex int = 0,  
   @pageSize int = 10,  
   @missedCheck varchar(15)='',  
   @sortBy     varchar(100),  
   @sortDirection varchar(10)  
AS  
BEGIN  
  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
declare @orderbyClause varchar(50)  
declare @query varchar(MAX)  
declare @subQuery varchar(MAX)  
declare @RegionSuburbClause varchar(8000)  
declare @MissedClause varchar(8000),  
		@rowNumberQuery varchar(MAX),
		@finalQuery varchar(MAX),
		@offset int,
		@limit int  
 
SET @offset = 1+(@skipIndex-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1 
        
IF(@assignedToId = -1 )  
BEGIN  
  
 IF(@regionId = -1 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'  
 END  
 ELSE IF(@regionId > 0 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)   
 END  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') '   
 END  
  
    ELSE IF(@regionId = -1 and @suburbId > 0)  
    BEGIN  
          SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId)   
    END  
   
 IF(@missedCheck='')  
  BEGIN  
   SET @MissedClause='-1=-1'  
        END  
    ELSE  
        BEGIN  
   SET @MissedClause='AM_PaymentPlan.PaymentPlanId IN (select distinct AM_MissedPayments.PaymentPlanId   
                FROM AM_MissedPayments   
                INNER JOIN AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId)'  
       END  
         
  
END  
ELSE   
BEGIN  
  
IF(@regionId = -1 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (   SELECT SCHEMEID   
                 FROM AM_ResourcePatchDevelopment   
                 WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '   
                 AND IsActive=''true'')   
         OR PDR_DEVELOPMENT.PATCHID IN    (SELECT PatchId   
                 FROM AM_ResourcePatchDevelopment   
                 WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '  
                 AND IsActive=''true''))'  
 END  
 ELSE IF(@regionId > 0 and @suburbId = -1)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) + '   
           AND (PDR_DEVELOPMENT.PATCHID IN    (SELECT PatchId   
                 FROM AM_ResourcePatchDevelopment   
                 WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '  
                 AND IsActive=''true''))'   
 END  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +'   
           AND (PDR_DEVELOPMENT.PATCHID IN    (SELECT PatchId   
                 FROM AM_ResourcePatchDevelopment   
                 WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '  
                 AND IsActive=''true''))   
           AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ')   
           AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID   
                 FROM AM_ResourcePatchDevelopment   
                 WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '   
                 AND IsActive=''true'')) '  
 END  
    ELSE IF(@regionId = -1 and @suburbId > 0)  
    BEGIN  
          SET @RegionSuburbClause =  'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId) +'  
                  AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID   
                 FROM AM_ResourcePatchDevelopment   
                 WHERE ResourceId =' + convert(varchar(10), @assignedToId )+ '   
                 AND IsActive=''true'')) '   
    END  
  
IF(@missedCheck='')  
  BEGIN  
   SET @MissedClause=' AM_Case.IsActive=''true'' '  
        END  
    ELSE  
        BEGIN  
   SET @MissedClause='AM_PaymentPlan.PaymentPlanId IN (select distinct AM_MissedPayments.PaymentPlanId   
                FROM AM_MissedPayments   
                INNER JOIN AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId)'  
       END  
  
END  
  
  
SET @orderbyClause = 'ORDER BY ' + ' ' + @sortBy + ' ' + @sortDirection  
SET @query =   
'SELECT     Max(customer.CUSTOMERID) AS CUSTOMERID,  
          Max(AM_PaymentPlan.TennantId) AS TennantId,   
          Max(AM_PaymentPlan.PaymentPlanId) AS PaymentPlanId,   
          Max(AM_PaymentPlan.StartDate) AS StartDate,   
          Max(AM_PaymentPlan.EndDate) AS EndDate,   
          Max(AM_PaymentPlan.FrequencyLookupCodeId) AS FrequencyLookupCodeId,  
          Max(AM_PaymentPlan.AmountToBeCollected) AS AmountToBeCollected,   
          Max(AM_PaymentPlan.ReviewDate) AS ReviewDate,   
          Max(AM_PaymentPlan.FirstCollectionDate) AS FirstCollectionDate,   
          MAX(AM_PaymentPlan.LastPaymentDate) AS LastPaymentDate,  
       (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName  
        FROM AM_Customer_Rent_Parameters  
        WHERE TenancyId = AM_PaymentPlan.TennantId  
        ORDER BY CustomerId ASC) as CustomerName,  
  
       (SELECT TOP 1 ISNULL(Title,'''') + '' '' + LEFT(ISNULL(FIRSTNAME, ''''), 1)+'' ''+ ISNULL(LASTNAME, '''') as CName  
        FROM AM_Customer_Rent_Parameters  
        WHERE TenancyId = AM_PaymentPlan.TennantId  
        ORDER BY CustomerId DESC) as CustomerName2,  
  
        (SELECT Count(DISTINCT CustomerId)  
        FROM AM_Customer_Rent_Parameters  
        WHERE TenancyId = AM_PaymentPlan.TennantId) as JointTenancyCount,  
            
        Max(customer.RentBalance) AS RentBalance,  
            
          ISNULL((SELECT P_FINANCIAL.Totalrent   
              FROM C_TENANCY INNER JOIN  
              P__PROPERTY ON C_TENANCY.PropertyId = P__PROPERTY.PropertyID INNER JOIN  
              P_FINANCIAL ON P_FINANCIAL.PropertyId = P__PROPERTY.PropertyID  
              WHERE C_TENANCY.TENANCYID = AM_PaymentPlan.TennantId), 0.00) as WeeklyRentAmount,  
                                 (SELECT TOP 1 CaseId   
          FROM AM_CaseHistory   
          WHERE TennantId = AM_PaymentPlan.TennantId and IsActive = 1 ORDER BY CaseHistoryId DESC )as CaseId,  
          MIN(AM_MissedPayments.MissedPaymentDetectionDate) AS FirstMissedDate  
           
  
  
    FROM         AM_PaymentPlan INNER JOIN  
        AM_Customer_Rent_Parameters customer on AM_PaymentPlan.TennantId = customer.TenancyId INNER JOIN          
        C_Tenancy ON customer.TenancyId = C_Tenancy.TenancyId INNER JOIN  
        P__Property ON C_Tenancy.PropertyId = P__Property.PropertyId   
        LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID  
        INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID   
        INNER JOIN C_ADDRESS ON customer.customerId=C_ADDRESS.CUSTOMERID  
        INNER JOIN AM_Case ON AM_PaymentPlan.PaymentPlanId=AM_Case.PaymentPlanId LEFT JOIN  
        AM_MissedPayments ON dbo.AM_Case.PaymentPlanId = dbo.AM_MissedPayments.PaymentPlanId  
Where AM_PaymentPlan.IsCreated = ''true''   
   AND '+ @RegionSuburbClause +'  
   AND C_ADDRESS.POSTCODE = CASE WHEN ''''='''+@postCode+''' THEN C_ADDRESS.POSTCODE ELSE '''+@postCode+''' END   
   --AND AM_Case.IsActive=''true''  
   AND AM_CASE.IsPaymentPlan=1  
      AND '+@MissedClause+ '  
GROUP BY AM_PaymentPlan.TennantId'  
 
  
  
  --=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortBy+ CHAR(10) +@sortDirection+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@query+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
  
  
print(@finalQuery);  
exec(@query);   
                  
  
END  
  
  
  