USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AM_SP_GetStatusActionReportCount]    Script Date: 02/19/2016 10:50:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetStatusActionReportCount]
@assignedToId	int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,
		@allAssignedFlag	bit,
		@allRegionFlag	bit,
		@allSuburbFlag	bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF @assignedToId <= 0 
	BEGIN 
		SET @assignedToId=-1
	END
IF @regionId <= 0 
	BEGIN 
		SET @regionId=-1
	END
IF @suburbId <= 0 
	BEGIN 
		SET @suburbId=-1
	END

SELECT AM_Status.StatusId, AM_Status.Title + ' : ' + AM_Action.Title AS Title, isnull(SUM(AM_CaseHistory.ActionIgnoreCount), 0) as ActionIgnoreCount, 
							   isnull(SUM(AM_CaseHistory.ActionRecordedCount), 0) as ActionRecordCount
					FROM         AM_Action INNER JOIN
										  AM_CaseHistory ON AM_Action.ActionId = AM_CaseHistory.ActionId INNER JOIN
										  AM_Status ON AM_Action.StatusId = AM_Status.StatusId AND AM_CaseHistory.StatusId = AM_Status.StatusId INNER JOIN
										  C_TENANCY ON AM_CaseHistory.TennantId = C_TENANCY.TENANCYID  
								 INNER join AM_Resource R ON AM_CaseHistory.CaseOfficer = R.ResourceId or AM_CaseHistory.CaseManager =R.ResourceId 
								 INNER JOIN (Select DISTINCT D.PatchID, ResourceID,S.SCHEMEID, D.DEVELOPMENTID
											 from AM_ResourcePatchDevelopment RPD
											 Inner JOIN P_SCHEME S on RPD.SCHEMEID = S.SCHEMEID
											 INNER JOIN PDR_DEVELOPMENT D ON S.DEVELOPMENTID = D.DEVELOPMENTID
											 Where RPD.SCHEMEID IS NOT NULL AND  RPD.ISACTIVE=1
											 AND ( ResourceId =  @assignedToId OR @assignedToId <= 0)
											 ) RPD ON R.ResourceId = RPD.ResourceId 
					WHERE		1=1 and (@regionId =-1 OR RPD.PATCHID = @regionId )
					
	AND (@assignedToId = -1 OR  RPD.PATCHID IN (
								  SELECT PatchId FROM AM_ResourcePatchDevelopment 
								  WHERE ResourceId =@assignedToId AND IsActive=1
								  ))
	and (@suburbId = -1 OR RPD.SCHEMEID = @suburbId) 
	AND (@assignedToId = -1 OR RPD.SCHEMEID IN (
										SELECT SCHEMEID 
										FROM AM_ResourcePatchDevelopment 
										WHERE ResourceId = @assignedToId  AND IsActive=1
										))
					GROUP BY AM_Action.Title , AM_Status.Title , AM_Status.StatusId

END

-- exec AM_SP_GetStatusActionReport null, null, null, 1,1,1





