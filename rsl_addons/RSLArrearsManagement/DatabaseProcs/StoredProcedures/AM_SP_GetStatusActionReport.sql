USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AM_SP_GetStatusActionReport]    Script Date: 02/19/2016 10:50:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AM_SP_GetStatusActionReport]
		@assignedToId	int = 0,
		@regionId	int = 0,
		@suburbId	int = 0,
		@allAssignedFlag	bit,
		@allRegionFlag	bit,
		@allSuburbFlag	bit,
		@skipIndex	int = 0,
		@pageSize	int = 10
AS
BEGIN

declare @searchCriteria VARCHAR(MAX)    
declare @orderbyClause varchar(50)  
declare @query varchar(MAX)  
declare @subQuery varchar(MAX),
		@rowNumberQuery varchar(MAX),
		@finalQuery varchar(MAX),
		@offset int,
		@limit int  
SET @offset = 1+(@skipIndex-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1 
				
SET @searchCriteria= '1=1'
IF(@assignedToId >0)    
 BEGIN 
   SET @searchCriteria =@searchCriteria +  ' and (AM_CaseHistory.CaseOfficer = '+convert(varchar(10), @assignedToId)+' or AM_CaseHistory.CaseManager = '+ convert(varchar(10), @assignedToId)+' ) '   
   
 END  
 IF(@regionId > 0)    
 BEGIN 
   SET @searchCriteria = @searchCriteria + ' AND RPD.PATCHID = '+ convert(varchar(10), @regionId)    
   
 END 
 IF(@suburbId > 0)    
 BEGIN    
  SET @searchCriteria = @searchCriteria + ' and RPD.SCHEMEID = '    + convert(varchar(10), @suburbId)
 END 
 
 SET @query = ' SELECT TOP('+convert(varchar(10),@limit)+')   AM_Status.StatusId, AM_Status.Title + '' : '' + AM_Action.Title AS Title, isnull(SUM(AM_CaseHistory.ActionIgnoreCount),0) as ActionIgnoreCount, 
							   isnull(SUM(AM_CaseHistory.ActionRecordedCount), 0) as ActionRecordCount
					FROM         AM_Action INNER JOIN
										  AM_CaseHistory ON AM_Action.ActionId = AM_CaseHistory.ActionId 
										  INNER JOIN AM_Status ON AM_Action.StatusId = AM_Status.StatusId AND AM_CaseHistory.StatusId = AM_Status.StatusId 
										  INNER JOIN C_TENANCY ON AM_CaseHistory.TennantId = C_TENANCY.TENANCYID 
										  INNER join AM_Resource R ON AM_CaseHistory.CaseOfficer = R.ResourceId or AM_CaseHistory.CaseManager =R.ResourceId 
										  INNER JOIN (Select DISTINCT D.PatchID, ResourceID,S.SCHEMEID, D.DEVELOPMENTID
											 from AM_ResourcePatchDevelopment RPD
											 Inner JOIN P_SCHEME S on RPD.SCHEMEID = S.SCHEMEID
											 INNER JOIN PDR_DEVELOPMENT D ON S.DEVELOPMENTID = D.DEVELOPMENTID
											 Where RPD.SCHEMEID IS NOT NULL AND  RPD.ISACTIVE=1
											 AND ( ResourceId =  '+convert(varchar(10), @assignedToId)+' OR '+convert(varchar(10), @assignedToId)+' <= 0)
											 ) RPD ON R.ResourceId = RPD.ResourceId 
					WHERE		('+ @searchCriteria +')
								
					GROUP BY AM_Action.Title , AM_Status.Title , AM_Status.StatusId '
				
				
				
			
  
  --=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by StatusId ) as row	
								FROM ('+CHAR(10)+@query+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
  	
				
				
				
				
				
				
				
				
					print(@finalQuery) 
					Exec (@finalQuery)
 
 END
