  
  
  
ALTER PROCEDURE [dbo].[AM_SP_OverdueActionsAlertCount]  
  
 @caseOwnedById int=0,  
 @regionId  int=0,  
 @suburbId int=0  
   
  
AS  
 BEGIN  
  
  declare @RegionSuburbClause varchar(8000)  
  declare @query varchar(8000)  
  declare @missedPaymentQuery varchar(6000)  
  --IF(@caseOwnedById <= 0 )  
  --BEGIN  
  -- SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'  
  --END   
  --ELSE  
  --BEGIN  
  -- SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT DevelopmentId   
  --             FROM AM_ResourcePatchDevelopment   
  --             WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'')   
  --         OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId   
  --             FROM AM_ResourcePatchDevelopment   
  --             WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ 'AND IsActive=''true''))'  
  --END  
  IF(@caseOwnedById = -1 )  
BEGIN  
 IF(@regionId = -1 and @suburbId = -1)  
  
 BEGIN  
  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = PDR_DEVELOPMENT.PATCHID'  
  
 END  
  
 ELSE IF(@regionId > 0 and @suburbId = -1)  
  
 BEGIN  
  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)   
  
 END  
  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
  
 BEGIN  
  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') '   
  
 END  
  
    ELSE IF(@regionId = -1 and @suburbId > 0)  
  
    BEGIN  
  
          SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId)   
  
    END  
  
  
  
END  
  
ELSE   
  
BEGIN  
  
  
  
IF(@regionId = -1 and @suburbId = -1)  
  
 BEGIN  
  
  SET @RegionSuburbClause = '(P_SCHEME.SCHEMEID IN (SELECT SCHEMEID   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true''))'  
  
 END  
 ELSE IF(@regionId > 0 and @suburbId = -1)  
  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId)+ ' AND (P_SCHEME.SCHEMEID IN (SELECT SCHEMEID   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'') OR PDR_DEVELOPMENT.PATCHID IN (SELECT PatchId   
               FROM AM_ResourcePatchDevelopment   
               WHERE ResourceId =' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true''))'  
  
 END  
  
 ELSE IF(@regionId > 0 and @suburbId > 0)  
 BEGIN  
  SET @RegionSuburbClause = 'PDR_DEVELOPMENT.PATCHID = ' + convert(varchar(10), @regionId) +' AND (P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId ) + ') '   
  
 END  
    ELSE IF(@regionId = -1 and @suburbId > 0)  
    BEGIN  
          SET @RegionSuburbClause = 'P_SCHEME.SCHEMEID = ' + convert(varchar(10), @suburbId)   
    END  
  
END  
  
    
  
if @caseOwnedById  = -1  
  BEGIN  
   SET @missedPaymentQuery = 'select distinct AM_MissedPayments.TenantId  
          FROM AM_MissedPayments   
          INNER JOIN AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId  
          WHERE IsActive=''true'''  
  END  
 ELSE  
  BEGIN  
  IF(@regionId = -1 and @suburbId = -1)  
   SET @missedPaymentQuery = 'select distinct AM_MissedPayments.TenantId  
        FROM AM_MissedPayments   
        INNER JOIN AM_Case on AM_MissedPayments.TenantId = AM_Case.tenancyId  
        WHERE AM_Case.CaseOfficer= ' + convert(varchar(10), @caseOwnedById )+ ' AND IsActive=''true'''  
  
          
  END     
  
  
  
      --SET @query='SELECT COUNT(*) as recordCount   
  
      --  FROM(   
  
      --     SELECT COUNT(*) as recordCount  
  
     SET @query= ' SELECT distinct  COUNT(*)tenancyid  
         FROM  AM_FindOverDueActionCase   
         INNER JOIN C_TENANCY on AM_FindOverDueActionCase.TenancyId = C_TENANCY.TENANCYID   
         INNER JOIN P__PROPERTY on P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID   
         LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID  
       INNER JOIN PDR_DEVELOPMENT ON PDR_DEVELOPMENT.DEVELOPMENTID = P__PROPERTY.DEVELOPMENTID   
       WHERE 1=1 AND  ' + @RegionSuburbClause + ' '  
  
         
  
         
  
         
  
      ----  Where 1=1   
  
      ----   AND  ' + @RegionSuburbClause + '   
  
      ----   AND AM_Case.IsActive = 1   
  
      ----   --AND AM_CASE.IsPaymentPlan<>1  
  
      ----   AND (C_TENANCY.ENDDATE>GETDATE() OR C_TENANCY.ENDDATE IS NULL)  
  
      ----   AND AM_Case.CaseId IN (  
  
      ----       SELECT AM_CaseHistory.CaseId     
  
      ----       FROM AM_CaseHistory   
  
      ----        INNER JOIN AM_Action ON AM_CaseHistory.ActionId = AM_Action.ActionId   
  
      ----        INNER JOIN AM_Status ON AM_CaseHistory.StatusId = AM_Status.StatusId   
  
      ----        INNER JOIN AM_Resource ON AM_CaseHistory.CaseOfficer = AM_Resource.ResourceId   
  
      ----        INNER JOIN E__EMPLOYEE ON AM_Resource.EmployeeId = E__EMPLOYEE.EMPLOYEEID   
  
      ----        INNER JOIN C_CUSTOMERTENANCY ON AM_CaseHistory.TennantId = C_CUSTOMERTENANCY.TENANCYID   
  
      ----        INNER JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID   
  
      ----        INNER JOIN AM_LookupCode ON AM_Action.RecommendedFollowupPeriodFrequencyLookup = AM_LookupCode.LookupCodeId  
  
      ----                                               WHERE AM_CaseHistory.IsActive = 1   
  
      ----                                               and dbo.AM_FN_CHECK_HISTORICAL_DUE_DATE(0, ''Days'', AM_CaseHistory.ActionReviewDate ) = ''true''   
  
      ----                                           and AM_CaseHistory.CaseHistoryId = dbo.AM_FN_Get_Last_Case_History_Id(AM_CaseHistory.CaseId))  
  
      ----                                           OR AM_Case.TenancyId IN(' + @missedPaymentQuery + ')   
  
      ---- GROUP BY AM_Case.TenancyId) as TEMP'  
                                    
  
      --      SET @query= 'SELECT distinct  COUNT(*)tenancyid  
  
      --                 FROM AM_OverDueActionCase   
  
      --        INNER JOIN AM_CASE on AM_OverDueActionCase.caseid=am_case.CaseId   
  
      --           OR AM_Case.TenancyId IN ('+@missedPaymentQuery+') '                                             
  
  
  
 PRINT @query;  
 EXEC(@query);  
END  
  
  
  
--EXEC [AM_SP_OverdueActionsAlertCount] @caseOwnedById=160  
  
  
  
  
  
  
  