/*
   Tuesday, January 5, 20163:37:23 PM
   User: tkxel
   Server: 10.0.1.75
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AM_Case
	DROP CONSTRAINT FK_AM_Case_AM_Resource
GO
ALTER TABLE dbo.AM_Case
	DROP CONSTRAINT FK_AM_Case_AM_Resource1
GO
ALTER TABLE dbo.AM_Resource SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AM_Resource', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AM_Resource', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AM_Resource', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.AM_Case ADD CONSTRAINT
	FK_AM_Case_AM_Resource FOREIGN KEY
	(
	InitiatedBy
	) REFERENCES dbo.AM_Resource
	(
	ResourceId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AM_Case ADD CONSTRAINT
	FK_AM_Case_AM_Resource1 FOREIGN KEY
	(
	ModifiedBy
	) REFERENCES dbo.AM_Resource
	(
	ResourceId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AM_Case SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AM_Case', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AM_Case', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AM_Case', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.AM_CaseHistory ADD CONSTRAINT
	FK_AM_CaseHistory_AM_Resource FOREIGN KEY
	(
	CaseManager
	) REFERENCES dbo.AM_Resource
	(
	ResourceId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AM_CaseHistory ADD CONSTRAINT
	FK_AM_CaseHistory_AM_Resource1 FOREIGN KEY
	(
	CaseOfficer
	) REFERENCES dbo.AM_Resource
	(
	ResourceId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AM_CaseHistory ADD CONSTRAINT
	FK_AM_CaseHistory_AM_Resource2 FOREIGN KEY
	(
	InitiatedById
	) REFERENCES dbo.AM_Resource
	(
	ResourceId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AM_CaseHistory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AM_CaseHistory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AM_CaseHistory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AM_CaseHistory', 'Object', 'CONTROL') as Contr_Per 