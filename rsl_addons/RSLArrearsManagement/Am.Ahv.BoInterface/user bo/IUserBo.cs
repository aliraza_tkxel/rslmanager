﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.BoInterface;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;
namespace Am.Ahv.BoInterface.user_bo
{
    public interface IUserBo:IBaseBO<E__EMPLOYEE,int>
    {
        E__EMPLOYEE GetEmployeeByName(string Name);
        List<E__EMPLOYEE> GetEmployeeList(string Name);
        IQueryable GetUserByTeamID(int TeamID);
        E__EMPLOYEE GetUserByTeamName(string Name, int TeamID);
        E__EMPLOYEE GetEmployeeByExactName(string Name);
        E__EMPLOYEE GetFullNarrowedSearch(String Name, int Type, int TeamID);
        List<E__EMPLOYEE> GetTeamMembers(int TeamID);
        string GetNameById(int userID);
        List<E__EMPLOYEE> GetSupportWorkers(int TeamID);
        IQueryable GetSingleEmployee(int Id);
        string GetEmployeeNameByUserId(int userID);
        string GetEmployeeNameAndTeam(int empId);
    }
}
