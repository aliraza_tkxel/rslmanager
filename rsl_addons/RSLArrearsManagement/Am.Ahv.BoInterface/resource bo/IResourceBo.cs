﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.resourcebo
{
    public interface IResourceBO : IBaseBO<AM_Resource, int>
    {
        IQueryable getResourceDetails(int dbIndex, int pageSize);
        IQueryable getCasePerson(String personType);
        int getRecordsCount();
        void UpdateResourceType(int resourceId, AM_Resource resource);
        IQueryable GetActivePersons(string designation);
        int GetResourceId(int userId);

        IEnumerable<AM_Resource> GetAllResources();

        string GetResourceName(int userId);
        bool IsResourceExist(int EmployeeId);
        bool UpdateResource(AM_Resource Resource);
        string GetLastLoggedInDate(int UserId);
        string getJobTitle(int resourceId);
        string getWorkEmail(int employeeId);
        string getWorkDirectDial(int employeeId);
    }
}
