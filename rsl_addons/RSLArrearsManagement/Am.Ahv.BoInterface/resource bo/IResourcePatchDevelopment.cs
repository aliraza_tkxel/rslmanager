﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;
namespace Am.Ahv.BoInterface.resource_bo
{
    public interface IResourcePatchDevelopment:IBaseBO<AM_ResourcePatchDevelopment,int>
    {
        AM_ResourcePatchDevelopment AddResource(List<int> PatchIDs, List<int> ResourceIDs, List<int> RegionIDs);
        List<AM_ResourcePatchDevelopment> GetAreasByResourceId(int ResourceID);
    }
}
