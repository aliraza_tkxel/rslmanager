﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;
namespace Am.Ahv.BoInterface.status_bo
{
    public interface IStatusBo:IBaseBO<AM_Status,int>
    {
        AM_Status AddStatus(AM_Status status, AM_StatusHistory statushistory);
        int GetStatusCount();
        void updateStatus(int statusId, AM_Status status, AM_StatusHistory statusHistory);
        AM_Status GetStatusByRank(int Rank);
        int GetStatusRankCount();
        int GetStatusHistoryByStatusId(int StatusId);        
        int GetStatusHistoryIdByStatus(int statusId);
        bool CheckDocumentUpload(int statusId);
        AM_Status GetNextStatus(int statusId);
        AM_Status GetStatusRanking(int statusId);
        bool UpdateStatusWithRank(int statusid, AM_Status status, AM_StatusHistory statusHistory, int nextStatusId, int rank);
        bool IsStatusAssigned(int statusId);
        void UpdateLastStatusAndStatusHistory(int rank, int lastRank);
        AM_StatusHistory GetlastStatusHistoryByStatusId(int statusId);
    }
}
