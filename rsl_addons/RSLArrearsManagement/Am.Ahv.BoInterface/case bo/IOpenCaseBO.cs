﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
using System.Collections;
using System.Data;

namespace Am.Ahv.BoInterface.case_bo
{
    public interface IOpenCaseBO : IBaseBO<AM_Case, int>
    {
        int OpenCase(AM_Case newCase);
        int AddCaseHistoryandDocuments(AM_Case newCase, AM_CaseHistory caseHistory, ArrayList listDocuments, ArrayList listletters, int tenantId, int modifiedBy, DataTable ActionData, bool isPaymentPlanAdded, double marketRent, double rentBalance, DateTime todayDate, double rentAmount);
        void AddDocuments(AM_Documents doc);
        bool CheckCase(int tenantId);
        bool CheckPaymentPlan(int tenantId);
        bool SetDefaulterBit(int tenantId, int modifiedBy);
        void DisableOpenedCase(int caseId);
        int SavePaymentPlanAndOpenNewCase(AM_PaymentPlan paymentPlan, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayments, AM_Case amCase, AM_CaseHistory caseHistory, ArrayList listDocuments, ArrayList listLetters, int tenantId, int modifiedBy, DataTable ActionData);
    }
}
