﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;

namespace Am.Ahv.BoInterface.case_bo
{
    public interface ISuppressedCaseBO : IBaseBO<AM_Case, int>
    {
        IQueryable CasesOwnedBy();
        IQueryable GetAssignedToList();        
        IQueryable GetRegionList(int resourceID);
        IQueryable GetSuppressedCases(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string sortExpression, string sortDir);
        int GetSuppressedCasesCount(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag);
    }
}
