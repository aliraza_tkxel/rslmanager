﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;
namespace Am.Ahv.BoInterface.case_bo
{
   public interface ICaseStandardLetter
    {
        List<AM_CaseAndStandardLetter> GetCaseStandardLetterByCaseHistoryId(int CaseHistoryId);
        AM_CaseAndStandardLetter GetFirstCaseStandardLetter(int CaseHistoryId, int LetterId);
    }
}
