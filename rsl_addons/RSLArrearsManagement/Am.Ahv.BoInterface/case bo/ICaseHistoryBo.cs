﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.case_bo
{
    public interface ICaseHistoryBo:IBaseBO<AM_CaseHistory,int>
    {
        IQueryable GetCaseHistoryByCaseId(int CaseID);
        List<AM_CaseHistory> GetCasehistory(int CaseID);
        List<AM_CaseHistory> GetCaseHistoryByID(int CaseHistoryId);
        IQueryable getCaseList();
        //Not Needed Already Implemented By the Method List<AM_CaseHistory> GetCasehistory(int CaseID);
        //List<AM_CaseHistory> GetCaseHisotyByCaseId(int CaseID);
        List<int> GetCaseHistoryCount(int CaseID);
       // IQueryable getCaseList(int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize);
        IQueryable getCaseOwnerList();
        IQueryable getCaseOwnerList(int regionId);
        IQueryable getCaseOwnerList(int regionId, int suburbId);
      
        bool UpdateHistoryIgnoreByStatusAction(int StatusId, int ActionId, int CaseId);
        bool UpdateHistoryRecordStatusAction(int StatusId, int ActionId, int CaseId);
        bool UpdateHistorySupressedValues(int CaseId, string Reason, DateTime Date, int supressedBy);
        bool UpdateActionCount(int CaseId);
        bool UpdateActionIgnoreCount(int CaseId);
        AM_CaseHistory GetLatestCaseHistory(int CaseId);
        int GetLatestCaseHistoryId(int CaseId);
        int getRecordsCount(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, string _statusTitle, string surname, bool isNisp, string OverdueCheck);
        int getCloseRecordsCount(int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, string _statusTitle, string surname);
        IQueryable getCaseList(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir, bool isNisp, string OverdueCheck);
        IQueryable getCloseCaseList(int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir);
        List<AM_SP_GetOverDueList_Result> getOverDueActionCaseList(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir, bool isNisp, string OverdueCheck);
    }
}
