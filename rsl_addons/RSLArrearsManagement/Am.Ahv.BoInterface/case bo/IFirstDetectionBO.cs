﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;

namespace Am.Ahv.BoInterface.case_bo
{
    public interface IFirstDetectionBO
    {
        IQueryable<AM_SP_getFirstDetectionList_Result> GetFirstDetectionList(string postCode,int caseOwnedId, int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize, string sortExpression, string sortDirection,string address1,string tenancyId);
        List<AM_SP_GetCustomerRentList_Result> GetCustomerRentList();
        AM_SP_GetStatusParameters_Result GetStatusParameters();
        void AddFirstDetection(int _customerId, int _tenancyId, DateTime _date, bool _isDefaulter);
        void AddFirstDetectionHistory(bool _isDefaulter);
        void DeleteFirstDetection(bool _isDefaulter);
        void DeleteFirstDetectionHistory();
        int GetFirstDetectionRecordsCount(string postCode,int caseOwnedBy,int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize,string address1,string tenancyId);
        int CountFirstDetections(int caseOfficerId, int regionId, int suburbId);
        List<AM_SP_GetRentList_Result> GetRentList(int dbIndex, int pageSize);
        int GetRentListCount();
        bool CheckDefaulterTenant(int customerId);
        int GetFirstDetectionListAlterCount(string postCode,int regionId, int suburbId,int resourceId);
        int GetLatestFirstDetectionListAlterCount(int regionId, int suburbId,int resourceId);
        IQueryable<AM_SP_GetPreviousTenantsList_Result> GetPreviousTenants(int caseOwnedId, int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize, string sortExpression, string sortDirection,string address1,string tenancyId);
        int GetPreviousTenantsRecordsCount(int caseOwnedBy, int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize,string address1,string tenancyId);
    }
}
	