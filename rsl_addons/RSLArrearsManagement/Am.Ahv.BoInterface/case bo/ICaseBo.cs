using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;
using System.Collections;
using System.Data;
namespace Am.Ahv.BoInterface.case_bo
{
    public interface ICaseBo : IBaseBO<AM_Case, int>
    {
        AM_Case GetCaseByCaseHistoryId(int CaseHistoryId);
        bool IgnoreCase(int CaseId, string Reason, int ignoredActionStatusId, int IgnoredActionId, bool isModalPopUp);
        Boolean SupressCase(int CaseId, string Reason, DateTime supressdate, int supressedBy);
        int CountMyCases(string postCode,int caseOfficerId, int regionId, int suburbid);
        int OverdueCaseCount(int ResourceId);
        int VcatCaseCount(int ResourceId);
        List<AM_SP_GetReviewList_Result> GetReviewList(int caseOfficer, int statusId, bool overdue, int dbIndex, int pageSize, string sortExpression, string sortDirection);
        int GetReviewListCount(int caseOfficer, int statusId, bool overdue);
        int TenanciesCount();
        decimal GetArrearsCount();
        int GetOpenCaseCount();
        Boolean UpdateActionCount(int CaseId);
        Boolean UpdateIgnoreActionCount(int CaseId);
        IQueryable GetCaseDocuments(int caseId);
        string GetInitiatedByName(int employeeId);
        void DisableDocument(int docId);
        //bool UpdateCase(int caseId, AM_Case amCase, AM_CaseHistory caseHistory, List<AM_StandardLetters> st, DataTable actionData, List<AM_StandardLetters> oldSL);
        bool UpdateCase(int caseId, AM_Case amCase, AM_CaseHistory caseHistory, List<AM_StandardLetterHistory> st, DataTable actionData, List<AM_StandardLetterHistory> oldSL, double marketRent, double rentBalance, DateTime todayDate, double rentAmount);
        bool UpdateCaseContactInfo(int caseId, AM_Case amCase, bool isCustomerEditView);
       
        bool CheckSuppress(int caseId);
        //bool UpdateCase(int caseId, AM_Case amCase, AM_CaseHistory caseHistory, ArrayList documentList, List<AM_StandardLetters> st, DataTable actionData, List<AM_StandardLetters> oldSL);
        bool UpdateCase(int caseId, AM_Case amCase, AM_CaseHistory caseHistory, ArrayList documentList, List<AM_StandardLetterHistory> st, DataTable actionData, List<AM_StandardLetterHistory> oldSL, double marketRent, double rentBalance, DateTime todayDate, double rentAmount);
        DataTable GetCaseStandardLetters(int caseId);
        void DisableCaseStandardLetter(int letterId, int caseId);
        bool IsOutComeExists(int OutCome);
        bool CheckRecoveryAmount(int statusId);
        IQueryable getRegionList(int CaseOwnedById);
        IQueryable getSuburbList(int RegionId, int CaseOwnedById);
        int GetOverdueActionsAlertCount(int ResourceId,int regionid, int suburbid );
        DataTable GetCaseStandardLetterHistory(int caseId);
        List<AM_SP_FindOverDueActionCases_Result> FindOverDueActionCases();
        void SaveOverDueActionCaseRecord(AM_FindOverDueActionCase overDueActionCase);
        void SaveOverDueActionTransaction();
        void DeleteOverDueActionCases();
    }
}
