using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;

namespace Am.Ahv.BoInterface.activity_bo
{
    public interface IActivityBO : IBaseBO<AM_Activity,int>
    {
        IQueryable GetCaseActivities(int caseId);
        List<AM_Activity> GetActivitiesReport(int caseId);
        List<AM_Activity> GetActivitiesByCaseId(int CaseId);
        IQueryable GetOrganisations();
        IQueryable GetContacts(int OrgId);
        string GetContactEmail(int EmployeeId);
        //List<AM_Documents> GetCaseActivityDocuments(int activityId);
        int GetCustomerId(int tennantId);
        int AddActivity(AM_Activity activity, List<string> docs, double marketRent, double rentBalance, DateTime todayDate, double rentAmount);
        IQueryable GetExternalAgencyOrganisations();
    }
}
