﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.customerbo
{
    public interface ICustomerBo : IBaseBO<C__CUSTOMER,int>
    {
        C__CUSTOMER getCustomerDetailsByTenantId(int tenantId);
       C_ADDRESS getAddressByCustomerId(int customerId);
       IQueryable getAddressAndTypeByCustomerId(int customerId);
       F_RENTJOURNAL customersLatestRent(int tenantId);
       C_TENANCY getCustomersTenancyByTenantId(int tenantId);
       AM_SP_GetCustomerInformation_Result GetCustomerInformation(int tenantId, int customerId);
       AM_SP_GetCustomerLastPayment_Result GetCustomerLastPayment(int tenantId, int customerId);
       IQueryable GetCustomerStatus();
       bool RegisterCustomerRentParametersScheduleJobStatus();
       void GetCustomersRentParameters(int timer);
       bool EndCustomerRentParametersScheduleJob();
       bool ErrorCustomerRentParametersScheduleJob(string errorMessage);
       bool IsScheduleJobInProgress();
        #region "Customer Rent Parameeter History"
       bool RegisterCustomerRentParametersHistoryScheduleJobStatus();
       void GetCustomersRentParametersHistory(int timer);
       bool EndCustomerRentParametersHistoryScheduleJob();
       bool ErrorCustomerRentParameterHistorysScheduleJob(string errorMessage);
       bool IsScheduleHistoryJobInProgress();
        #endregion

    }
}
