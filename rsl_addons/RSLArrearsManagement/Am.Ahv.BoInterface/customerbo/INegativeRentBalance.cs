﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.customerbo
{
  public  interface INegativeRentBalance
    {
        int GetNegativeRentBalance(int Timer);
        int GetNegativeBalanceTenantsCount();
        int TotalTenancies();
        int LetPropertiesCount();
    }
}
