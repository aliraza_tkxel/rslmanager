﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.Lookup_bo
{
    public interface ILookupBo : IBaseBO<AM_LookupCode, int>
    {
        List<AM_LookupCode> GetLookupByName(string lookuptype, int DbIndex, int pageSize);
        List<AM_LookupCode> GetLookupByName(string lookuptype);
        int getLookupCount(string typeName);
        void updateLookup(int lookupCodeId, string value, int modifiedBy);
        void addLookup(string value, string type, int createdBy);
        string getLookup(int lookupcodeid);
        string GetUserType(int userId);
        List<AM_LookupCode> GetOutComeLookupByName(string lookuptype);
        bool DeleteOutComeLookUp(int OutComeId);
        void DeleteLookup(int lookupCodeId, int modifiedBy);
    }
}
