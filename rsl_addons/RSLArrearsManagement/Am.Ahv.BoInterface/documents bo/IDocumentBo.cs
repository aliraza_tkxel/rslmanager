﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.documents_bo
{
    public interface IDocumentBo:IBaseBO<AM_Documents,int>
    {
        List<AM_Documents> GetDocumentByCaseId(int CaseId);
        bool IsAttachedByCaseId(int CaseId);
        List<AM_Documents> GetDocumentsByCaseHistoryId(int CaseHistoryId);
        int GetDoucmentCountByCaseId(int CaseId);
        int GetDocumentCountByActivity(int AcitvityId);
        int GetDocumentCountByCaseHistoryId(int CaseHistoryId);
        List<AM_Documents> GetDocumentsByActivityId(int ActivityId);
    }
}
