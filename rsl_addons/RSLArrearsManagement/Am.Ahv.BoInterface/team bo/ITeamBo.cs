﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.team_bo
{
    public interface ITeamBo:IBaseBO<E_TEAM,int>
    {
        E_TEAM GetTeamByEmployeeId(int empId);
    }
}
