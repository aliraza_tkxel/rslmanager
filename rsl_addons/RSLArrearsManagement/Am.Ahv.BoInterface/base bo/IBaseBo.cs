﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BoInterface.basebo
{
    public interface IBaseBO<T, K>
    {
        /// <summary>
        /// To retrieve an entity given entity id
        /// </summary>
        /// <param name="id">Id of entity</param>
        /// <returns>Entity object retrieved. Or null if not found</returns>
        T GetById(K id);


        List<T> GetAll();

        /// <summary>
        /// To persist new entity object.
        /// </summary>
        /// <param name="obj">The object to be persisted</param>
        /// <returns>The entity persisted. Should be detached from context.</returns>
        T AddNew(T obj);

        /// <summary>
        /// To update an entity object
        /// </summary>
        /// <param name="obj">The object to be updated</param>
        /// <returns>The updated object. Should be detached from context</returns>
        T Update(T obj);

        /// <summary>
        /// To delete an object
        /// </summary>
        /// <param name="obj">The object to be deleted</param>
        /// <returns>True if operation was successful. False otherwise</returns>
        bool Delete(T obj);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(K id);


       
    }
}
