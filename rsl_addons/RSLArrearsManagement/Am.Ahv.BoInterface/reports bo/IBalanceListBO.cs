﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;

namespace Am.Ahv.BoInterface.reports_bo
{
    public interface IBalanceListBO
    {
        List<AM_SP_GETRENTBALANCELIST_Result> GetBalanceList(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, int index, int pageSize, string sortExpression, string sortDir, string rentBalanceFrom, string rentBalanceTo);
        List<AM_SP_GetHistoricalBalanceList_Result> GetHistoricalBalanceList(int caseOwnedBy,int region, int suburb, int startIndex, int endIndex, int customerStatus, int assetType, int months, int years, string sortExpression, string sortDir);
        List<AM_SP_GetNoticeToVacateCases_Result> GetNoticeToVacateCasesList();
        int GetBalanceListCount(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, string rentBalanceFrom, string rentBalanceTo);
        int GetHistoricalBalanceListCount(int caseOwnedBy,int region,int suburb,int customerStatus,int assetType,int months,int years);
        List<AM_SP_FindHistoricalBalanceList_Result> FindHistoricalBalanceList(int DbIndex,int PageSize);
        int FindHistoricalBalanceListCount();
        bool CheckHistoricalTable(int CustomerId, int TenancyId, string month, int year);
        AM_HistoricalBalanceList AddHistoricalBalanceList(AM_HistoricalBalanceList obj);

        List<AM_SP_GetBalanceListExportToExcel_Result> GetBalanceListExportToExcel(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType,  string sortExpression, string sortDir);
        List<AM_SP_GetHistoricalBalanceListExportToExcel_Result> GetHistoricalBalanceListExportToExcel(int caseOwnedBy, int region, int suburb, int startIndex, int endIndex, int customerStatus, int assetType, int months, int years, string sortExpression, string sortDir);
        AM_SP_GetAnticipatedHBBalance_Result GetAnticipatedHBBalance(string postcode, int caseOwnedBy, int region, int suburb);
    }
}
