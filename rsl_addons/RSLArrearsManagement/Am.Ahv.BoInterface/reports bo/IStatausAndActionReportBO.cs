﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BoInterface.reports_bo
{
    public interface IStatusAndActionReportBO
    {
        IQueryable GetStatusAndActionReport(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize);
        int GetStatusAndActionReportCount(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag);
    }
}
