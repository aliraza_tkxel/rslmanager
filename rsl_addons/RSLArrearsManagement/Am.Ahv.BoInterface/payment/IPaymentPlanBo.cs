﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
using System.Data;

namespace Am.Ahv.BoInterface.payment
{
    public interface IPaymentPlanBo : IBaseBO<AM_PaymentPlan, int>
    {
        int AddPaymentPlan(AM_PaymentPlan payment, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayment);
        int AddPaymentPlanHistory(AM_PaymentPlanHistory paymentHistory);
        IQueryable GetFrequencyData();
        IQueryable GetPaymentPlanType();
        double GetWeeklyRent(int tenantId);
        bool CheckPaymentPlan(int tenantId);
        bool SetCasePaymentPlan(int tenantId, int modifiedBy);
        int GetPaymentPlanCount();
        List<AM_PaymentPlan> GetPaymentPlanSummary(int tennantId);
        DataTable GetPaymentPlanReview();
        string GetPaymentPlanType(int tenantId);
        AM_PaymentPlan GetPaymentPlan(int tenantId);
        bool UpdatePaymentPlan(int paymentPlanId, AM_PaymentPlan paymentPlan, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayments);
        List<AM_Payment> LoadFlexiblePayments(int paymentPlanId);
        AM_PaymentPlanHistory GetPaymentPlanHistory(int paymentPlanId);
        //List<AM_SP_GetPaymentPlanMonitoring_Result> GetPaymentPlanMonitoring(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize);
        List<AM_Payment> GetPaymentList(int paymentPlanId);
        List<AM_Payment> GetPaymentList(int? paymentPlanId);
        //List<AM_Payment> GetPaymentLists(int paymentPlanId);
        List<F_RENTJOURNAL> GetCustomerRentList(int tenantId, DateTime firstPaymentdate);
        List<F_RENTJOURNAL> GetCustomerRentList(int? tenantId, DateTime firstPaymentdate);
        List<F_RENTJOURNAL> GetCustomerRentLists(int tenantId, DateTime PeriodStartDate,DateTime PeriodEndDate);
       // List<AM_SP_GetPaymentPlanMonitoringCount_Result> GetPaymentPlanMonitoringCount(string postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, string missedCheck);
        int GetPaymentPlanMonitoringCount(string postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, string missedCheck);
        string GetPaymentPlanTotalRentBalance(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag);
        List<AM_PaymentPlanHistory> GetPaymentPlanSummaryHistory(int PaymentPlanHistoryId);
        List<AM_SP_GetPaymentPlanMonitoring_Result> GetPaymentPlanMonitoring(string _postCode,int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string queryRe, string sortExpression, string sortDir);
        //List<AM_SP_GetPaymentPlanMonitoring_Result> GetPaymentPlanMonitorings(string _postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string queryRe, string sortExpression, string sortDir);
        bool getLatestMissedDetectionDate(int newPayment);

        List<F_RENTJOURNAL> GetCustomerRentLists(int? tenantId, DateTime dateTime, DateTime dateTime_2);
        void DeleteZeorMissedPayments();
        bool DeleteOldMissedPayments(int tenancyId);
        List<AM_SP_GetZeroMissedPaymentTenancies_Result> GetZeroMissedPaymentTenancies();
    }
}
