﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;

namespace Am.Ahv.BoInterface.payment
{
    public interface IPayment : IBaseBO<AM_Payment, int>
    {
       List<AM_Payment> GetPaymentDetailsByTenantId(int TenantId);
       List<AM_Payment> GetPaymentDetailsByPaymentPlanHistoryId(int PaymentPlanHistoryId);
       int GetMissedPaymentsAlertCount(string postCode,int regionId, int suburbId,int resourceId);
        
     // List<AM_SP_GetMissedPaymentAlerts_Result> GetMissedPaymentsAlertCount(string postCode,int regionId, int suburbId,int resourceId);
      List<AM_Payment> GetPaymentList(int? paymentPlanId);
      List<F_RENTJOURNAL> GetCustomerRentLists(int? tenantId, DateTime PeriodStartDate, DateTime PeriodEndDate);

    }
}
