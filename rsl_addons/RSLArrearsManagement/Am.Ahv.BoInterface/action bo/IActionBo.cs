﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.BoInterface.actionbo;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.actionbo
{
    public interface IActionBO : IBaseBO<AM_Action, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">Action object</param>
        /// <param name="actionhistory">Action History object</param>
        /// <returns></returns>
        AM_Action AddAction(AM_Action action, AM_ActionHistory actionhistory, List<int> LetterIds);
        int getActionCount(int statusId);
        string getStatus(int statusId);
        AM_Action getAction(int actionId);
        bool updateAction(int actionId, AM_Action action, AM_ActionHistory actionHistory, List<int> LetterIds);
        bool updateAction(int actionId, AM_Action action, AM_ActionHistory actionHistroy, List<int> LettterIds, int ActionId2, int Rank2);
        List<AM_Action> getActionByStatus(int statusId);
        AM_Action GetActionByRank(int Rank);
        int GetActionRankByStatusId(int StatusId);
        AM_Action GetActionByRankStatus(int StatusId, int Rank);
        int GetActionHistoryByActionId(int actionId);
        void DisableActionDocument(int docId, int actionId);
        IQueryable GetActionListByStatusId(int statusId);
        AM_Action GetNextAction(int actionId, int statusId);
        AM_Action GetActionRanking(int actionId);
        bool CheckPaymentPlanMandatory(int actionId);
        bool IsActionAsigned(int ActionId);
        bool UpdateActionRanking(int ActiondId, int Rank);
        bool DeleteAction(int ActionId);
        bool isFollowUpPeriodConsecutive(int statusId,int periodVal, string period,int ranking);
        bool isUpdatedFollowUpPeriodValid(int actionId, int periodVal, string period);
        bool IsIgnoredProceduralActionRemaining(int statusId, int rankId, int currentActionId, int currentStatusId, int previousIgnoredActionId, int isFirstIteration);
        bool IsProceduralActionLeftWithinStage(int stageId, int caseId);
        bool IsProceduralActionLeftWithInStageRange(int currentStageRank);
        AM_Action GetFirstProceduralIgnoredAction(int statusId, int caseId);
        bool IsProceduralActionRecorded(int statusId, int ranking, int caseId);
        bool IsActionAlreadyRecorded(int actionId, int caseId);
    }
}
