﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
namespace Am.Ahv.BoInterface.tenantbo
{
  public  interface ITenantBo
    {
        string GetCustomerContactByTenantID(int TenantId);
    }
}
