using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;
using System.Data;
namespace Am.Ahv.BoInterface.letters_bo
{
 public interface ILettersBo:IBaseBO<AM_StandardLetters,int>
    {
        List<AM_StandardLetters> SearchStandardLetters(int StatusId, int ActionId, String Title, String Code);
        List<AM_LookupCode> GetPersonalization();
        List<AM_LookupCode> GetSignOff();
        DataTable GetTeamMembers(int teamId);
        DataTable GetAllTeamMembers(int teamId);
        AM_SP_GetCustomerInfoForLetter_Result GetCustomerInfo(int tenantId, int customerId, int contactId);
        string GetEmployeeName(int? ResourceId);
        string GetTeamName(int? TeamId);
        List<AM_StandardLetterHistory> GetLettersByActionId(int actionId);
        DataTable GetLettersByActionIdDataTable(int actionId);
        string GetLettersNameByAction(int ActionId);
        List<AM_StandardLetters> GetAllLetters();
        double GetMarketRent(int TenantId);
        List<AM_ActivityAndStandardLetters> GetStandardLetterByActivityId(int ActivityId);
        DataTable GetTeamEmployees(int teamId);
        void AddNewStandardLetter(AM_StandardLetters standardLetter, AM_StandardLetterHistory standardLetterHistory);
        void UpdateStandardLetter(AM_StandardLetters standardLetter, AM_StandardLetterHistory standardLetterHistory);
        int AddNewThirdPartyContact(AM_StandardLetters_ThirdPartyContacts standardLetterThirdPartyContacts);
        int UpdateThirdPartyContact(AM_StandardLetters_ThirdPartyContacts standardLetterThirdPartyContacts);
        AM_StandardLetterHistory GetLatestStandardLetterHistory(int standardLetterId);
        AM_StandardLetterHistory GetStandardLetterHistoryById(int Id);
        int AddEditedStandardLetter(AM_StandardLetterHistory slHistory);
        int GetCaseStandardLetterHistoryId(int stlId, int caseId);
        string ActivityChecker(string ActionId, string StatusId, string CaseId, string CaseHistoryId);
        AM_StandardLetters GetSLetterByHistoryID(int LHistoryID);
        AM_StandardLetterHistory GetSLetterHistoryBySLetterID(int SLetterID);
        AM_StandardLetters_ThirdPartyContacts getThirdPartyContactDetails(int ThirdPartycontactId);
    }
}
