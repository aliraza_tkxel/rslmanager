﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;

namespace Am.Ahv.BoInterface.region_suburb_bo
{
    public interface IRegionBo:IBaseBO<E_PATCH,int>
    {
        List<E_PATCH> GetRegionReport();
        IQueryable GetRegionCount();
        List<AM_SP_Region_Amount_Result>  GetTotalRentByRegion(int ResourceId);
        List<E_PATCH> GetUserRegion(int UserId);
        List<E_PATCH> GetSchemeRegions(List<int?> suburbIds);       
    }
}
