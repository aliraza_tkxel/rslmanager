﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.basebo;
using Am.Ahv.Entities;

namespace Am.Ahv.BoInterface.region_suburb_bo
{
    public interface IAssetTypeBo : IBaseBO<P_ASSETTYPE, int>
    {
        IQueryable GetAssetType();
    }
}
