﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;

namespace Am.Ahv.BoInterface.region_suburb_bo
{
    public interface IStreetBo:IBaseBO<P__PROPERTY,int>
    {
        List<String> GetStreetBySuburb(int SuburbID);
        //List<P__PROPERTY> GetStreetByIds(List<AM_ResourcePatchDevelopment> AreasList);
        List<String> GetAllStreets();
        IQueryable GetStreetList(int resourceId);
    }
}
