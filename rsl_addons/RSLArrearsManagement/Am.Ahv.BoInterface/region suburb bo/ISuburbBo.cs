﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.basebo;

namespace Am.Ahv.BoInterface.region_suburb_bo
{
    public interface ISuburbBo : IBaseBO<P_SCHEME, int>
    {
        List<P_SCHEME> GetSuburbByRegion(int RegionID);
        List<P_SCHEME> GetSuburbByUser(int RegionID, int UserId);
        List<P_SCHEME> GetSuburbByUser(int UserId);
        List<P_SCHEME> GetSuburbByIds(List<AM_ResourcePatchDevelopment> AreasList);
        List<SchemeBo> GetAllSuburbs();
        IQueryable GetSuburbsList(int resourceId);
        IQueryable GetSuburbsListByRegion(string region, int userId);
    }
    public class SchemeBo
    {
        public int SchemeId { get; set; }
        public int? DevelopmentId { get; set; }
        public string SchemeName { get; set; }
        public string DevelopmentName { get; set; }
        public int? PatchId { get; set; }


    }
}
