﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.Utilities;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BoInterface.payment;
using Am.Ahv.BoInterface.customerbo;
namespace Am.Ahv.BusinessManager.Dashboard
{
  public  class arrearssummary
    {
        public int GetTenanciesCount()
        {
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            return CaseBO.TenanciesCount();
        }
        public int GetPaymentPlanCount()
        {
            IPaymentPlanBo PaymentPlanBO = FactoryBO.CreatePaymentPlanBo();
            return PaymentPlanBO.GetPaymentPlanCount();
        }
        public decimal GetArrearsCount()
        {
            try
            {
                ICaseBo CaseBO = FactoryBO.CreateCaseBO();
                decimal value = CaseBO.GetArrearsCount();
                if (value > 0)
                {
                    value = Math.Round(value, 2);
                    return value;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public double GetPlansPercentage()
        {
            ICaseBo CaseBo = FactoryBO.CreateCaseBO();
            double paymentplans = Convert.ToDouble(GetPaymentPlanCount());
            double opencases =Convert.ToDouble( CaseBo.GetOpenCaseCount());
            
            if (opencases != 0)
            {
                double percentage = (paymentplans/ opencases) * 100;
                 percentage= Math.Round(percentage, 2);
                return percentage;
            }
            else
            {
                return 0.0;
            }
          
        }
        public double GetNegativeTenanciesValue()
        {
            try
            {
                INegativeRentBalance negativeRentbalanceBO = FactoryBO.CreateNegativeRentBalance();
                double negativebalance =Convert.ToDouble( negativeRentbalanceBO.GetNegativeBalanceTenantsCount());
                double total = Convert.ToDouble(negativeRentbalanceBO.TotalTenancies());
                
                double value;
                if (negativebalance > 0 && total > 0)
                {
                    value = (negativebalance / total) * 100;
                    value= Math.Round(value, 2);
                    return value;
                }
                else
                {
                    return 0.0;
                }
            }
            catch (DivideByZeroException div)
            {
                throw div;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public int LetPropertiesCount()
        {
            INegativeRentBalance negativerentbalanceBo = FactoryBO.CreateNegativeRentBalance();
            return negativerentbalanceBo.LetPropertiesCount();
        }
      //for running the stored procedure only  
        public int GetNegativeRentBalance(int Timer)
        {
            INegativeRentBalance negativeBO = FactoryBO.CreateNegativeRentBalance();
            return negativeBO.GetNegativeRentBalance(Timer);
        }
    }
}
