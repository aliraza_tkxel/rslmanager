﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.region_suburb_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;
namespace Am.Ahv.BusinessManager.Dashboard
{
 public   class arrearskpi
 {
     public List<AM_SP_Region_Amount_Result> GetRegionAmount(int ResourceId)
     {
         try
         {
             IRegionBo RegionBO = FactoryBO.CreateRegionBO();
             return RegionBO.GetTotalRentByRegion(ResourceId);
         }
         catch (ArgumentException arg)
         {
             throw arg;
         }
         catch (EntityException entitexcepption)
         {
             throw entitexcepption;
         }
         catch (Exception ex)
         {
             throw ex;
         }         
     }
 }
}
