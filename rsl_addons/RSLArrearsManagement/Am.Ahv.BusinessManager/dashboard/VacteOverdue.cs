﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.Entities;
using Am.Ahv.Utilities;
namespace Am.Ahv.BusinessManager.Dashboard
{
    public class vacteoverdue
    {
        public int GetCaseOverDueCount(int ResourceId, int regionid, int suburbid)
        {
            bool allRegionFlag = (regionid > 0 ? false : true);
            bool allOwnerFlag = (ResourceId > 0 ? false : true);
            bool allSuburbFlag = (suburbid > 0 ? false : true);
            ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
            int count = caseBO.getRecordsCount(string.Empty, Convert.ToInt16(regionid), Convert.ToInt16(ResourceId), Convert.ToInt16(suburbid), allRegionFlag, allOwnerFlag, allSuburbFlag, string.Empty, string.Empty, false, "Stages");
            // int count= caseBO.getRecordsCount("", 0, ResourceId, 0, true, false, true, "", "", false, "Stages");

            return count;
        }
        public int GetVacateCount(int ResourceId)
        {
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            int count = CaseBO.VcatCaseCount(ResourceId);
            return count;
        }

        public int GetOverdueActionsAlertCount(int ResourceId, int regionid, int suburbid)
        {
            bool allRegionFlag = (regionid > 0 ? false : true);
            bool allOwnerFlag = (ResourceId > 0 ? false : true);
            bool allSuburbFlag = (suburbid > 0 ? false : true);
            ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
            int count = caseBO.getRecordsCount(string.Empty, Convert.ToInt16(regionid), Convert.ToInt16(ResourceId), Convert.ToInt16(suburbid), allRegionFlag, allOwnerFlag, allSuburbFlag, string.Empty, string.Empty, false, "Actions");

            //ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            // int count = CaseBO.GetOverdueActionsAlertCount(ResourceId,regionid,suburbid);
            return count;
        }
    }
}
