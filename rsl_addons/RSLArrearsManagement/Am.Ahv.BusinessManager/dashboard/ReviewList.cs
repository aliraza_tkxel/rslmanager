﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BoInterface.region_suburb_bo;
using System.Data;
using Am.Ahv.Entities;

namespace Am.Ahv.BusinessManager.Dashboard
{
    public class ReviewList
    {
        #region"Get Review List"

        public List<AM_SP_GetReviewList_Result> GetReviewList(int caseOfficer, int statusId, bool overdue, int dbIndex, int pageSize, string sortExpression, string sortDirection)
        {
            ICaseBo caseBo = FactoryBO.CreateCaseBO();
            return caseBo.GetReviewList(caseOfficer, statusId, overdue, dbIndex, pageSize, sortExpression, sortDirection);
        }

        #endregion

        #region"Get Review List Count"

        public int GetReviewListCount(int caseOfficer, int statusId, bool overdue)
        {
            ICaseBo caseBo = FactoryBO.CreateCaseBO();
            return caseBo.GetReviewListCount(caseOfficer, statusId, overdue);
        }

        #endregion

        #region"Get All Region List"

        public List<E_PATCH> GetAllRegionList()
        {
            try
            {
                IRegionBo regionBo = FactoryBO.CreateRegionBO();
                return regionBo.GetAll();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
