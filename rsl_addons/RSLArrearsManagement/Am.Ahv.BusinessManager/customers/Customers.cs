﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.Customer_bo;
using Am.Ahv.BoInterface.customerbo;
using Am.Ahv.Entities;
using Am.Ahv.BusinessObject.factory_bo;

namespace Am.Ahv.BusinessManager.Customers
{
    public class Customers
    {
        #region"Get Customer Details"

        public ArrayList getCustomerDatails(int tenancyId)
        {
            ArrayList list = new ArrayList();
            CustomerBo customerBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBO();
            C__CUSTOMER customer = customerBO.getCustomerDetailsByTenantId(tenancyId);
            C_ADDRESS address= customerBO.getAddressByCustomerId(customer.CUSTOMERID);
            C_TENANCY tenancy = customerBO.getCustomersTenancyByTenantId(tenancyId);
            F_RENTJOURNAL rentJournal = customerBO.customersLatestRent(tenancyId);
            
            list.Add(customer);
            list.Add(address);
            list.Add(tenancy);
            list.Add(rentJournal);

            return list;            
        }

        #endregion

        #region"get Customer By Tenant Id"

        public C__CUSTOMER getCustomerByTenantId(int tenancyId)
        {
            ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
            C__CUSTOMER customer = iCustomerBo.getCustomerDetailsByTenantId(tenancyId);
            return customer;
        }

        #endregion

        #region"Get Address By Customer Id"

        public C_ADDRESS getAddressByCustomerId(int customerId)
        {
            ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
           C_ADDRESS address= iCustomerBo.getAddressByCustomerId(customerId);

           return address;
        }

        #endregion


        #region"Get Address And Type By Customer Id"

        public IQueryable getAddressAndTypeByCustomerId(int customerId)
        {
            ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
            return iCustomerBo.getAddressAndTypeByCustomerId(customerId);
            
        }

        #endregion

        #region"Get Tenancy Details"

        public C_TENANCY getTenancyDetails(int tenancyId)
        {
            ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
            C_TENANCY tenancy= iCustomerBo.getCustomersTenancyByTenantId(tenancyId);            
            return tenancy;
        }

        #endregion

        #region"Get Customer Information"

        public AM_SP_GetCustomerInformation_Result GetCustomerInformation(int tenantId, int customerId)
        {
            try
            {
                ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
                return iCustomerBo.GetCustomerInformation(tenantId, customerId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Customer Last Payment"

        public AM_SP_GetCustomerLastPayment_Result GetCustomerLastPayment(int tenantId, int customerId)
        {
            try
            {
                ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
                return iCustomerBo.GetCustomerLastPayment(tenantId, customerId);
                
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Customer Status"

        public IQueryable GetCustomerStatus()
        {
            try
            {
                ICustomerBo customerStaus = FactoryBO.CreateCustomerBO();
                return customerStaus.GetCustomerStatus();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Register Customer Rent Parameters Schedule Job Status"

        public bool RegisterCustomerRentParametersScheduleJobStatus()
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                return customer.RegisterCustomerRentParametersScheduleJobStatus();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is GetCustomers rent Parameters Job In progress"

        public Boolean IsGetCustomersRentParametersJobInProgress()
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                return customer.IsScheduleJobInProgress();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Customers Rent Parameters"

        public void GetCustomersRentParameters(int timer)
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                customer.GetCustomersRentParameters(timer);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"End Customer Rent Parameters Schedule Job"

        public bool EndCustomerRentParametersScheduleJob()
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                return customer.EndCustomerRentParametersScheduleJob();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Error Customer Rent Parameters Schedule Job"

        public bool ErrorCustomerRentParametersScheduleJob(string errorMessage)
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                return customer.ErrorCustomerRentParametersScheduleJob(errorMessage);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region"Register Customer Rent Parameters History Schedule Job Status"

        public bool RegisterCustomerRentParametersHistoryScheduleJobStatus()
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                return customer.RegisterCustomerRentParametersHistoryScheduleJobStatus();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is GetCustomers rent Parameters History Job In progress"

        public Boolean IsGetCustomersRentParametersHistoryJobInProgress()
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                return customer.IsScheduleHistoryJobInProgress();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Customers Rent Parameters History"

        public void GetCustomersRentParametersHistory(int timer)
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                customer.GetCustomersRentParametersHistory(timer);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"End Customer Rent Parameters History Schedule Job"

        public bool EndCustomerRentParametersHistoryScheduleJob()
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                return customer.EndCustomerRentParametersHistoryScheduleJob();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Error Customer Rent Parameters History Schedule Job"

        public bool ErrorCustomerRentParametersHistoryScheduleJob(string errorMessage)
        {
            try
            {
                ICustomerBo customer = FactoryBO.CreateCustomerBO();
                return customer.ErrorCustomerRentParameterHistorysScheduleJob(errorMessage);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }

}
