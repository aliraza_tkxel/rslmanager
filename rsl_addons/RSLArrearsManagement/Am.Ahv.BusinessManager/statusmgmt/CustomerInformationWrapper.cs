﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BusinessManager.statusmgmt
{
    public class CustomerInformationWrapper
    {
        private string customerName;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }
        private string customerAddress;

        public string CustomerAddress
        {
            get { return customerAddress; }
            set { customerAddress = value; }
        }
        private string houseNumber;

        public string HouseNumber
        {
            get { return houseNumber; }
            set { houseNumber = value; }
        }
        private string address1;

        public string Address1
        {
            get { return address1; }
            set { address1 = value; }
        }
        private string address2;

        public string Address2
        {
            get { return address2; }
            set { address2 = value; }
        }
        private string townCity;

        public string TownCity
        {
            get { return townCity; }
            set { townCity = value; }
        }
        private string postCode;

        public string PostCode
        {
            get { return postCode; }
            set { postCode = value; }
        }
        private string county;

        public string County
        {
            get { return county; }
            set { county = value; }
        }
        private string suburb;

        public string Suburb
        {
            get { return suburb; }
            set { suburb = value; }
        }
        private string rentBalance;

        public string RentBalance
        {
            get { return rentBalance; }
            set { rentBalance = value; }
        }
        private string amount;

        public string Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        private string lastPayment;

        public string LastPayment
        {
            get { return lastPayment; }
            set { lastPayment = value; }
        }
        private string date;

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        private string lastPaymentDate;

        public string LastPaymentDate
        {
            get { return lastPaymentDate;}
            set { lastPaymentDate =value;}
        }

        private string owedToBHA;

        public string OwedToBHA
        {
            get { return owedToBHA; }
            set { owedToBHA = value; }
        }

        private string lastMonthNetArrears;

        public string LastMonthNetArrears
        {
            get { return lastMonthNetArrears; }
            set { lastMonthNetArrears = value; }
        }


        private string estimatedHB;

        public string EstimatedHB
        {
            get { return estimatedHB; }
            set { estimatedHB = value; }
        }

        private string tenancyId;

        public string TenancyId
        {
            get { return tenancyId; }
            set { tenancyId = value; }
        }


        public CustomerInformationWrapper()
        {
        }
    }
}
