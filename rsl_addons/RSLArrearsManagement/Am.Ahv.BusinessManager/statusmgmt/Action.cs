﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BoInterface.Lookup_bo;
using Am.Ahv.BoInterface.actionbo;
using Am.Ahv.Entities;
using System.Data;
using Am.Ahv.BoInterface.status_bo;
namespace Am.Ahv.BusinessManager.statusmgmt
{
    public class Action
    {
        #region"Get Follow Up Period"

        public List<AM_LookupCode> getFollowupPeriod()
        {
            try
            {
                ILookupBo LookupBO = FactoryBO.CreateLookupBO();
                return LookupBO.GetLookupByName(Am.Ahv.Utilities.constants.ApplicationConstants.reviewperiod);
                // data of review period is same as for Follow up period.
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"get Action Count"

        public int getActionCount(int statusId)
        {
            try
            {
                IActionBO actionBo = FactoryBO.CreateActionBO();
                return actionBo.getActionCount(statusId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Status"

        public string getStatus(int statusId)
        {
            try
            {
                IActionBO actionBo = FactoryBO.CreateActionBO();
                return actionBo.getStatus(statusId);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Add Action"

        public bool addAction(AM_Action action, AM_ActionHistory actionHistory, List<int> LetterIds)
        {
            try
            {
                IActionBO actionBo = FactoryBO.CreateActionBO();
                IStatusBo statusBO = FactoryBO.CreateStatusBO();
                int historyId = statusBO.GetStatusHistoryIdByStatus(action.StatusId);
                if (historyId != -1)
                {
                    action.StatusHistoryId = historyId;
                    actionHistory.StatusHistoryId = historyId;
                    actionBo.AddAction(action, actionHistory, LetterIds);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        #endregion

        #region"Get Action"

        public AM_Action getAction(int actionId)
        {
            try
            {
                IActionBO actionBo = FactoryBO.CreateActionBO();
                return actionBo.getAction(actionId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Action"

        public bool updateAction(int actionId, AM_Action action, AM_ActionHistory actionHistory, List<int> LetterIds, int Ranking)
        {
            try
            {
                IActionBO actionBo = FactoryBO.CreateActionBO();
                IStatusBo statusBo = FactoryBO.CreateStatusBO();


                AM_Action Action2 = actionBo.GetActionByRankStatus(action.StatusId, action.Ranking);
                if (Action2 != null)
                {
                    if (Action2.ActionId != actionId)
                    {
                        int historyId = statusBo.GetStatusHistoryByStatusId(action.StatusId);
                        int historyId2 = statusBo.GetStatusHistoryByStatusId(Action2.StatusId);
                        bool flagvalue = actionBo.IsActionAsigned(actionId);
                        bool flagvalue1 = actionBo.IsActionAsigned(Action2.ActionId);

                        if (!flagvalue && !flagvalue1)
                        {
                            if (historyId != -1 && historyId2 != -1)
                            {
                                action.StatusHistoryId = historyId;
                                actionHistory.StatusHistoryId = historyId;
                                actionBo.updateAction(actionId, action, actionHistory, LetterIds, Action2.ActionId, Ranking);
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region
        public bool updateAction(int actionId, AM_Action action, AM_ActionHistory actionHistory, List<int> LetterIds)
        {
            IActionBO actionBo = FactoryBO.CreateActionBO();
            IStatusBo statusBo = FactoryBO.CreateStatusBO();
            int historyId = statusBo.GetStatusHistoryByStatusId(action.StatusId);
            if (historyId != -1)
            {
                action.StatusHistoryId = historyId;
                actionHistory.StatusHistoryId = historyId;
                actionBo.updateAction(actionId, action, actionHistory, LetterIds);
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion

        #region"Get Action By Status"

        public List<AM_Action> GetActionByStatus(int StatusID)
        {
            try
            {
                IActionBO actionbo = FactoryBO.CreateActionBO();
                return actionbo.getActionByStatus(StatusID);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        #endregion

        #region Get Action Count By StatusID
        public List<int> GetActionCountByStatusID(int StatusID)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                int count = ActionBO.GetActionRankByStatusId(StatusID);
                if (count == -2)
                {
                    List<int> RankList = new List<int>();
                    RankList.Add(1);
                    return RankList;
                }

                else
                    if (count != -1 && count != -2)
                    {
                        List<int> Ranklist = new List<int>();
                        for (int i = 1; i <= count + 1; i++)
                        {
                            Ranklist.Add(i);
                        }
                        return Ranklist;
                    }

                    else
                    {
                        return null;
                    }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public bool IsActionPresent(int StatusID, int Rank)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                AM_Action action = ActionBO.GetActionByRankStatus(StatusID, Rank);
                if (action != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        #endregion

        #region"Get Action Review"

        public AM_Action GetActionReview(int actionId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                AM_Action action = ActionBO.GetById(actionId);
                if (action != null)
                {
                    if (!action.AM_LookupCodeReference.IsLoaded)
                    {
                        action.AM_LookupCodeReference.Load();
                    }
                }
                return action;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Action History By Action Id"

        public int GetActionHistoryByActionId(int actionId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.GetActionHistoryByActionId(actionId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region GetRankByActionId
        public bool GetRankByActionId(int ActionID, int Rank)
        {
            try
            {
                IActionBO actionBO = FactoryBO.CreateActionBO();
                AM_Action action = actionBO.GetById(ActionID);
                if (action.Ranking == Rank)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        #endregion

        #region"Disable Action Document"

        public void DisableActionDocument(int docId, int actionId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                ActionBO.DisableActionDocument(docId, actionId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Next Action"

        public AM_Action GetNextAction(int actionId, int statusId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.GetNextAction(actionId, statusId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Action Ranking"

        public AM_Action GetActionRanking(int actionId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.GetActionRanking(actionId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Action By Rank"

        public AM_Action GetActionByRank(int statusId, int rank)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.GetActionByRankStatus(statusId, rank);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Check Payment Plan Mandatory"

        public bool CheckPaymentPlanMandatory(int actionId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.CheckPaymentPlanMandatory(actionId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Action Attached With Case"

        public bool isActionAttachedwithCase(int actionId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.IsActionAsigned(actionId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Delete Action"

        public bool DeleteAction(int ActionId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.DeleteAction(ActionId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Follow Up Period Consecutive"

        public bool isFollowUpPeriodConsecutive(int statusId,int periodVal, string period,int ranking)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.isFollowUpPeriodConsecutive(statusId,periodVal, period,ranking);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Updated Follow Up Period Valid"

        public bool isUpdatedFollowUpPeriodValid(int actionId, int periodVal, string period)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.isUpdatedFollowUpPeriodValid(actionId,periodVal, period);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Ignored procedural Action Remaining"

        public bool IsIgnoredProceduralActionRemaining(int statusId, int rankId, int currentActionId, int currentStatusId, int previousIgnoredActionId, int isFirstIteration)
        {
            try
            {
                IActionBO actionBo = FactoryBO.CreateActionBO();
                return actionBo.IsIgnoredProceduralActionRemaining(statusId, rankId, currentActionId, currentStatusId, previousIgnoredActionId, isFirstIteration);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
 
        }

        #endregion

        #region"Is Procedural Action Left With in Stage"

        public bool IsProceduralActionLeftWithinStage(int stageId, int caseId)
        {
            try
            {
                IActionBO actionBO = FactoryBO.CreateActionBO();
                return actionBO.IsProceduralActionLeftWithinStage(stageId, caseId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }


        #endregion

        #region"Is Procedural Action Left With In Stage Range"

        public bool IsProceduralActionLeftWithInStageRange(int currentStageRank)
        {
            try
            {
                IActionBO actionBO = FactoryBO.CreateActionBO();
                return actionBO.IsProceduralActionLeftWithInStageRange(currentStageRank);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get First Procedural Ignored Action"

        public AM_Action GetFirstProceduralIgnoredAction(int statusId, int caseId)
        {
            try
            {
                IActionBO actionBO = FactoryBO.CreateActionBO();
                return actionBO.GetFirstProceduralIgnoredAction(statusId, caseId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Procedural Action Recorded"

        public bool IsProceduralActionRecorded(int statusId, int ranking, int caseId)
        {
            try
            {
                IActionBO actionBO = FactoryBO.CreateActionBO();
                return actionBO.IsProceduralActionRecorded(statusId, ranking, caseId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Action Already Recorded"

        public bool IsActionAlreadyRecorded(int actionId, int caseId)
        {
            try
            {
                IActionBO ActionBO = FactoryBO.CreateActionBO();
                return ActionBO.IsActionAlreadyRecorded(actionId, caseId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion
    }
}
