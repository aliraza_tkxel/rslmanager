﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.Lookup_bo;
using Am.Ahv.BoInterface.status_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;
using System.Transactions;
namespace Am.Ahv.BusinessManager.statusmgmt
{
    public class Status
    {
       /// <summary>
       /// Rent Count numbering 1,2,3,4.....
       /// </summary>
       /// <returns>List of Am_LookupCode Type</returns>
        /// 
        #region"Get Rent List"
        
        public List<Am.Ahv.Entities.AM_LookupCode> GetRentList()
        {
            try
            {
                ILookupBo LookupBO = FactoryBO.CreateLookupBO();
                return LookupBO.GetLookupByName(Am.Ahv.Utilities.constants.ApplicationConstants.rentcount);
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Review Period"

        public List<Am.Ahv.Entities.AM_LookupCode> GetReviewPeriod()
        {
            try
            {
                ILookupBo LookupBO = FactoryBO.CreateLookupBO();
                return LookupBO.GetLookupByName(Am.Ahv.Utilities.constants.ApplicationConstants.reviewperiod);
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Save Status"

        public AM_Status SaveStatus(AM_Status status, AM_StatusHistory statushistory)
        {
            try
            {
                IStatusBo statusBo = FactoryBO.CreateStatusBO();
                return statusBo.AddStatus(status, statushistory);
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get All Status"

        public List<AM_Status> GetAllStatus()
        {
            try
            {
                IStatusBo statusBo = FactoryBO.CreateStatusBO();
                return statusBo.GetAll();
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Status Count"

        public int GetStatusCount()
        {
            try
            {
                IStatusBo statusBo = FactoryBO.CreateStatusBO();
                return statusBo.GetStatusCount();
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Status By Id"

        public AM_Status GetStatusById(int statusID)
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                return StatusBO.GetById(statusID);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Status"

        public void updateStatus(int statusId, AM_Status status, AM_StatusHistory statushistory)
        {
            try
            {
                IStatusBo statusBO = FactoryBO.CreateStatusBO();
                statusBO.updateStatus(statusId, status, statushistory);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool updateStatus(int statusId, AM_Status status, AM_StatusHistory statushistory, int rank)
        {
            try
            {
                IStatusBo statusBO = FactoryBO.CreateStatusBO();
                if (!statusBO.IsStatusAssigned(statusId))
                {
                    return false;
                }
                AM_Status lastStatus = statusBO.GetStatusByRank(status.Ranking);
                if (lastStatus != null)
                {
                    if (!statusBO.IsStatusAssigned(lastStatus.StatusId))
                    {
                        return false;
                    }
                    else
                    {
                        if (statusBO.UpdateStatusWithRank(statusId, status, statushistory, lastStatus.StatusId, rank))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Status Ranking"
        //Status Ranking Change
        public List<int> GetStatusRanking()
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                int counter = StatusBO.GetStatusRankCount();
                if (counter != -1)
                {
                    List<int> RankList = new List<int>();
                    for (int i = 2; i <= counter + 1; i++)
                    {
                        RankList.Add(i);
                    }
                    return RankList;
                }
                else
                {
                    return null;
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Is Present"

        public bool IsPresent(int RankId)
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                AM_Status status = StatusBO.GetStatusByRank(RankId);
                if (status != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Status Review Period"

        public AM_Status GetStatusReviewPeriod(int statusId)
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                AM_Status status = StatusBO.GetById(statusId);
                if (!status.AM_LookupCode1Reference.IsLoaded)
                {
                    status.AM_LookupCode1Reference.Load();
                }
                return status;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region Get Rank By Status ID

        public bool GetRankByStatusID(int StatusID, int Rank)
        {
            try
            {
                IStatusBo statusBO = FactoryBO.CreateStatusBO();
                AM_Status status = statusBO.GetById(StatusID);
                if (status.Ranking == Rank)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion 

        #region"Get Status History Id By Status"

        public int GetStatusHistoryIdByStatus(int statusId)
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                return StatusBO.GetStatusHistoryIdByStatus(statusId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Check Document Upload"

        public bool CheckDocumentUpload(int statusId)
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                return StatusBO.CheckDocumentUpload(statusId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Next Status"

        public AM_Status GetNextStatus(int statusId)
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                return StatusBO.GetNextStatus(statusId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Status Ranking"

        public AM_Status GetStatusRanking(int statusId)
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                return StatusBO.GetStatusRanking(statusId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Status By Ranking"

        public AM_Status GetStatusByRanking(int ranking)
        {
            try
            {
                IStatusBo StatusBO = FactoryBO.CreateStatusBO();
                return StatusBO.GetStatusByRank(ranking);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
