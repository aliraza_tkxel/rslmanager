﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.Customer_bo;
using Am.Ahv.BoInterface.customerbo;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.utitility_classes;
using System.Globalization;

namespace Am.Ahv.BusinessManager.statusmgmt
{
    public class CustomersReport
    {
        #region"Get Customer Details"

        public ArrayList getCustomerDatails(int tenancyId)
        {
            ArrayList list = new ArrayList();
            CustomerBo customerBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBO();
            C__CUSTOMER customer = customerBO.getCustomerDetailsByTenantId(tenancyId);
            C_ADDRESS address = customerBO.getAddressByCustomerId(customer.CUSTOMERID);
            C_TENANCY tenancy = customerBO.getCustomersTenancyByTenantId(tenancyId);
            F_RENTJOURNAL rentJournal = customerBO.customersLatestRent(tenancyId);

            list.Add(customer);
            list.Add(address);
            list.Add(tenancy);
            list.Add(rentJournal);

            return list;
        }

        #endregion

        #region"get Customer By Tenant Id"

        public C__CUSTOMER getCustomerByTenantId(int tenancyId)
        {
            ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
            C__CUSTOMER customer = iCustomerBo.getCustomerDetailsByTenantId(tenancyId);
            return customer;
        }

        #endregion

        #region"Get Address By Customer Id"

        public C_ADDRESS getAddressByCustomerId(int customerId)
        {
            ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
            C_ADDRESS address = iCustomerBo.getAddressByCustomerId(customerId);

            return address;
        }

        #endregion

        #region"Get Tenancy Details"

        public C_TENANCY getTenancyDetails(int tenancyId)
        {
            ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
            C_TENANCY tenancy = iCustomerBo.getCustomersTenancyByTenantId(tenancyId);
            return tenancy;
        }

        #endregion

        #region"Get Customer Information"

        public CustomerInformationWrapper GetCustomerInformation(int tenantId, int customerId)
        {
            try
            {
                ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
                AM_SP_GetCustomerInformation_Result result = iCustomerBo.GetCustomerInformation(tenantId, customerId);

                if (result != null)
                {
                    CustomerInformationWrapper customer = new CustomerInformationWrapper();
                    if (result.JointTenancyCount == 1)
                    {
                        customer.CustomerName = result.CustomerName;
                    }
                    else
                    {
                        customer.CustomerName = result.CustomerName + " & " + result.CustomerName2;
                    }
                    customer.CustomerAddress = result.CustomerAddress;
                    customer.HouseNumber = result.HouseNumber;
                    customer.Address1 = result.Address1;
                    customer.Address2 = result.Address2;
                    customer.TownCity = result.TownCity;
                    customer.PostCode = result.PostCode;
                    customer.County = result.County;
                   
                    if (result.LastPayment < 0)
                    {
                        customer.Amount = "£ " + Math.Round(result.LastPayment, 2).ToString("0,0.00");
                    }
                    else
                    {
                        customer.Amount = "(£ " + Math.Round(result.LastPayment, 2).ToString("0,0.00") + ")";
                    }
                    customer.LastPaymentDate = result.LastPaymentDate;
                    customer.Suburb = result.suburb;
                    customer.OwedToBHA = "£ " + Math.Round(Convert.ToDecimal(result.OwedToBHA), 2).ToString("0,0.00");
                    customer.LastMonthNetArrears = "£ " + Math.Round(Convert.ToDecimal(result.LastMonthNetArrears), 2).ToString("0,0.00");
                    customer.EstimatedHB = "£ " + Math.Round(Convert.ToDecimal(result.EstimatedHBDue), 2).ToString("0,0.00");
                    customer.TenancyId = result.TENANCYID.ToString();
                    customer.RentBalance = Math.Round(Convert.ToDecimal(result.RentBalance), 2).ToString("0,0.00");
                    
                    CultureInfo culture = new CultureInfo("en-GB");
                    customer.Date = DateTime.Now.ToString("D", culture);                                                           

                    if (result.RentBalance < 0)
                    {
                        customer.RentBalance = "£ " + Math.Round(result.RentBalance, 2).ToString("0,0.00");
                    }
                    else
                    {
                        customer.RentBalance = "(£ " + Math.Round(result.RentBalance, 2).ToString("0,0.00") + ")";
                    }


                    return customer;
                }
                else
                {
                    return null;
                }

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public AM_SP_GetCustomerInformation_Result CustomerInformation(int tenantId, int customerId)
        {
            try
            {
                ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
                AM_SP_GetCustomerInformation_Result result = iCustomerBo.GetCustomerInformation(tenantId, customerId);

                if (result != null)
                {
                    CustomerInformationWrapper customer = new CustomerInformationWrapper();
                    if (result.JointTenancyCount == 1)
                    {
                        customer.CustomerName = result.CustomerName;
                    }
                    else
                    {
                        customer.CustomerName = result.CustomerName + " & " + result.CustomerName2;
                    }
                    customer.CustomerAddress = result.CustomerAddress;
                    customer.Amount = Math.Round(Convert.ToDecimal(result.LastPayment), 2).ToString("0,0.00");
                    customer.LastPayment = result.LastPaymentDate;
                    customer.Suburb = result.suburb;
                    customer.OwedToBHA = "£ " + Math.Round(Convert.ToDecimal(result.OwedToBHA), 2).ToString("0,0.00");
                    customer.LastMonthNetArrears = "£ " + Math.Round(Convert.ToDecimal(result.LastMonthNetArrears), 2).ToString("0,0.00");
                    customer.EstimatedHB = Math.Round(Convert.ToDecimal(result.EstimatedHBDue), 2).ToString("0,0.00");
                    customer.TenancyId = result.TENANCYID.ToString();
                    customer.RentBalance = Math.Round(Convert.ToDecimal(result.RentBalance), 2).ToString("0,0.00");

                    customer.Date = DateTime.Now.ToString("D");

                    DayOfWeek dw = DateTime.Now.DayOfWeek;
                    double rent = Math.Round(((Convert.ToDouble(result.RentBalance)) - (((Convert.ToDouble(result.rentAmount)) / 7) * Convert.ToInt32(dw))),2);
                    decimal variable = Math.Round(Convert.ToDecimal(rent), 2);

                    customer.RentBalance = Math.Round(Convert.ToDouble(result.RentBalance), 2).ToString("0,0.00");
                  
                    return result;
                }
                else
                {
                    return null;
                }

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region Testing Report
        public List<CustomerInformationWrapper> UserSummary(int TenantId, int customerId)
        {
            List<CustomerInformationWrapper> UserList = new List<CustomerInformationWrapper>(1);
            AM_SP_GetCustomerInformation_Result obj = CustomerInformation(TenantId, customerId);
            CustomerInformationWrapper customer = new CustomerInformationWrapper();
            if (obj.JointTenancyCount == 1)
            {
                customer.CustomerName = obj.CustomerName;
            }
            else
            {
                customer.CustomerName = obj.CustomerName + " & " + obj.CustomerName2;
            }
            customer.CustomerAddress = obj.CustomerAddress;
            customer.LastPayment = obj.LastPaymentDate;
            customer.Amount = obj.LastPayment.ToString();
            customer.Suburb = obj.suburb;
            customer.RentBalance = obj.RentBalance.ToString();
            customer.Date = DateTime.Now.ToString("D");
            
            UserList.Add(customer);
            return UserList;
        }
        #endregion

        #region"Get Customer Last Payment"

        public AM_SP_GetCustomerLastPayment_Result GetCustomerLastPayment(int tenantId, int customerId)
        {
            try
            {
                ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
                return iCustomerBo.GetCustomerLastPayment(tenantId, customerId);

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region CRN & Region Numbers
        public List<AM_SP_GetCustomerLastPayment_Result> GetCrnInfo(int TenantId, int customerId)
        {
            AM_SP_GetCustomerLastPayment_Result obj = GetCustomerLastPayment(TenantId, customerId);
            if (obj != null)
            {
                List<AM_SP_GetCustomerLastPayment_Result> InfoList = new List<AM_SP_GetCustomerLastPayment_Result>();
                InfoList.Add(obj);
                return InfoList;

            }
            else
            {
                return null;
            }

        }
        #endregion
    }
}
