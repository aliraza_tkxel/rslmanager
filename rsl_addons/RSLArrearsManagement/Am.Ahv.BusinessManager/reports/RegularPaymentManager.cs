﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.Utilities;
using Am.Ahv.BoInterface.payment;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BusinessManager.payments;
namespace Am.Ahv.BusinessManager.statusmgmt
{
    public class RegularPaymentManager
    {

        public List<PaymentWrapper> GetPaymentByTenantId(int TenantId)
        {
            IPayment PaymentBO = FactoryBO.CreatePaymentBo();
            List<AM_Payment> PaymentList = PaymentBO.GetPaymentDetailsByTenantId(TenantId);
            if (PaymentList != null)
            {
                List<PaymentWrapper> pw = new List<PaymentWrapper>(PaymentList.Count);
                for (int i = 0; i < PaymentList.Count; i++)
                {
                    if (i == 0)
                    {
                        pw.Add(new PaymentWrapper("1st Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                    if (i == 1)
                    {
                        pw.Add(new PaymentWrapper("2nd Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                    if (i == 2)
                    {
                        pw.Add(new PaymentWrapper("3rd Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                    if (i == PaymentList.Count - 1)
                    {
                        pw.Add(new PaymentWrapper("Final Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                    else
                    {
                        pw.Add(new PaymentWrapper(i + 1 + "th Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                }
                return pw;
            }
            else
            {
                return null;
            }

        }

        public List<PaymentWrapper> GetPaymentByPaymentPlanHistoryId(int PaymentPlanHistoryId)
        {
            IPayment PaymentBO = FactoryBO.CreatePaymentBo();
            List<AM_Payment> PaymentList = PaymentBO.GetPaymentDetailsByPaymentPlanHistoryId(PaymentPlanHistoryId);
            if (PaymentList != null)
            {
                List<PaymentWrapper> pw = new List<PaymentWrapper>(PaymentList.Count);
                for (int i = 0; i < PaymentList.Count; i++)
                {
                    if (i == 0)
                    {
                        pw.Add(new PaymentWrapper("1st Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                    if (i == 1)
                    {
                        pw.Add(new PaymentWrapper("2nd Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                    if (i == 2)
                    {
                        pw.Add(new PaymentWrapper("3rd Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                    if (i == PaymentList.Count - 1)
                    {
                        pw.Add(new PaymentWrapper("Final Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                    else
                    {
                        pw.Add(new PaymentWrapper(i + 1 + "th Payment:", PaymentList[i].Date.ToString("dd/MM/yyyy"), PaymentList[i].Payment));
                        continue;
                    }
                }
                return pw;
            }
            else
            {
                return null;
            }

        }
    }
}
