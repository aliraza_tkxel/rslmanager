﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BusinessManager.reports
{
    public class BalanceListWrapper
    {
        #region"Attributes"

        int ? tenancyId;

        public int ? TenancyId
        {
            get { return tenancyId; }
            set { tenancyId = value; }
        }

        string rentBalance;

        public string RentBalance
        {
            get { return rentBalance; }
            set { rentBalance = value; }
        }

        string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        string customerName;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        string url;

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        bool isActive;

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        #endregion

        #region"Constructor"

        public BalanceListWrapper(int ? _tenancyId, string _name, string _addrress, string _rentbalance, string _status, string _url, bool _isActive)
        {
            TenancyId = _tenancyId;
            CustomerName = _name;
            Address = _addrress;
            RentBalance = _rentbalance;
            Status = _status;
            Url = _url;
            IsActive = _isActive;
        }

        public BalanceListWrapper()
        { }

        #endregion
    }
}
