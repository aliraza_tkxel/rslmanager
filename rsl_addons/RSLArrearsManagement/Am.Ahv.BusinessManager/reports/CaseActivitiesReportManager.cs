﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.activity_bo;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.BusinessManager.resource;
namespace Am.Ahv.BusinessManager.reports
{
    public class CaseActivitiesReportManager
    {
        public List<CaseActivitiesWrapper> GetActivitiesReport(int caseId)
        {
            try
            {
                IActivityBO ac = FactoryBO.CreateActivityBO();
                List<AM_Activity> al = ac.GetActivitiesReport(caseId);
                if (al != null)
                {
                    List<CaseActivitiesWrapper> caseActivities = new List<CaseActivitiesWrapper>(al.Count);
                    for (int i = 0; i < al.Count; i++)
                    {
                        CaseActivitiesWrapper CAW = new CaseActivitiesWrapper();
                        CAW.RecordingDate = al[i].RecordedDate;
                        CAW.Status = al[i].AM_Status.Title;
                        CAW.Title = al[i].Title;
                        CAW.Notes = al[i].Notes;
                        CAW.Activity = al[0].AM_LookupCode.CodeName;
                        Users u = new Users();
                        CAW.RecordedBy = u.GetResourceName(al[i].AM_Resource.EmployeeId);
                        caseActivities.Add(CAW);
                    }
                    return caseActivities;
                }
                return null;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
