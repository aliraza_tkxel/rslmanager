﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.Utilities;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BoInterface.documents_bo;
namespace Am.Ahv.BusinessManager.reports
{
    public class CaseDetailsReport
    {
        public List<AM_CaseHistory> GetCaseHistoryByCaseId(int CaseId)
        {
            try
            {
                ICaseHistoryBo CaseHistoryBO = FactoryBO.CreateCaseHistoryBO();
                List<AM_CaseHistory> CH = CaseHistoryBO.GetCasehistory(CaseId);
                if (CH != null)
                {
                    return LoadActionReference(CH);
                }
                else
                {
                    return CH;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<AM_CaseHistory> LoadActionReference(List<AM_CaseHistory> ChList)
        {
            foreach (AM_CaseHistory item in ChList)
            {
                item.AM_Action.AM_LookupCode1Reference.Load();   
            }
            return ChList;
        }
        public int GetDocumentCountByCaseHistory(int CaseHistoryId)
        {
            //commit
            IDocumentBo DocumentBO = FactoryBO.CreateDocumentBO();
            return DocumentBO.GetDocumentCountByCaseHistoryId(CaseHistoryId);
        }
       

    }
}
