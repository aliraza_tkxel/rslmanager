﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.reports_bo;
using Am.Ahv.BoInterface.region_suburb_bo;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.Utilities.constants;
using System.Web;
using System.Configuration;
using Am.Ahv.Utilities.utitility_classes;
using System.Data;

namespace Am.Ahv.BusinessManager.reports
{
    public class BalanceList
    {
        #region"Get Balance List"

        public List<BalanceListWrapper> GetBalanceList(int startIndex, int endIndex)
        {
            try
            {
                //IBalanceListBO balanceList = FactoryBO.CreateBalalnceListBO();
                //List<AM_SP_GetBalanceList_Result> listBalanceList = new List<AM_SP_GetBalanceList_Result>();
                //BalanceListWrapper balanceListWrapper = new BalanceListWrapper();
                ////listBalanceList = balanceList.GetBalanceList(startIndex, endIndex);
                //List<BalanceListWrapper> listWrapper = new List<BalanceListWrapper>(listBalanceList.Count);
                //if (listBalanceList != null && listBalanceList.Count > 0)
                //{
                //    for (int i = 0; i < listBalanceList.Count; i++)
                //    {
                //        if (listBalanceList[i].IsActive)
                //        {
                //            listWrapper.Add(new BalanceListWrapper(listBalanceList[i].TenancyId, listBalanceList[i].CustomerName, listBalanceList[i].CustomerAddress, Convert.ToDouble(listBalanceList[i].RentBalance).ToString(), listBalanceList[i].StatusTitle, FileOperations.CreateUrl(listBalanceList[i].TenancyId.ToString(), true, listBalanceList[i].CaseId.ToString()), listBalanceList[i].IsActive));
                //        }
                //        else
                //        {
                //            listWrapper.Add(new BalanceListWrapper(listBalanceList[i].TenancyId, listBalanceList[i].CustomerName, listBalanceList[i].CustomerAddress, Convert.ToDouble(listBalanceList[i].RentBalance).ToString(), listBalanceList[i].StatusTitle, FileOperations.CreateUrl(listBalanceList[i].TenancyId.ToString(), false, "0"), listBalanceList[i].IsActive));
                //        }
                //    }
                //}
                //return listWrapper;
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Total Balance List Records Count"

        public int GetTotalBalanceListRecordsCount(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, string rentBalanceFrom, string rentBalanceTo)
        {
            try
            {
                IBalanceListBO balanceList = FactoryBO.CreateBalalnceListBO();
                return balanceList.GetBalanceListCount(_caseOwnedBy,_regionId, _suburbId, _customerStatus, _assetType,rentBalanceFrom,rentBalanceTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Historical Balance List"

        public List<AM_SP_GetHistoricalBalanceList_Result> GetHistoricalBalanceList(int caseOwnedBy,int region, int suburb, int startIndex, int endIndex, int customerStatus, int assetType, int months, int years, string sortExpression, string sortDir)
        {
            try
            {
                IBalanceListBO historicalList = FactoryBO.CreateBalalnceListBO();
                return historicalList.GetHistoricalBalanceList(caseOwnedBy,region, suburb, startIndex, endIndex,customerStatus,assetType,months,years, sortExpression, sortDir);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        public int FindHistoricalBalanceListCount()
        {
            try
            {
                IBalanceListBO historicalList = FactoryBO.CreateBalalnceListBO();
                return historicalList.FindHistoricalBalanceListCount();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_SP_FindHistoricalBalanceList_Result> FindHistoricalBalanceList(int DbIndex,int PageSize)
        {
            try
            {
                IBalanceListBO historicalList = FactoryBO.CreateBalalnceListBO();
                return historicalList.FindHistoricalBalanceList(DbIndex,PageSize);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region"Get Historical Balance List Count"

        public int GetHistoricalBalanceListCount(int caseOwnedBy,int region, int suburb,int customerStatus,int assetType,int months, int years)
        {
            try
            {
                IBalanceListBO historicalList = FactoryBO.CreateBalalnceListBO();
                return historicalList.GetHistoricalBalanceListCount(caseOwnedBy,region, suburb,customerStatus,assetType,months,years);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Notice To Vacate Cases List"

        public List<NoticeToVacateListWrapper> GetNoticeToVacateCasesList()
        {

            try
            {
                IBalanceListBO NoticeToVacateList = FactoryBO.CreateBalalnceListBO();

                List<AM_SP_GetNoticeToVacateCases_Result> res = NoticeToVacateList.GetNoticeToVacateCasesList();
                List<NoticeToVacateListWrapper> ListVacate = new List<NoticeToVacateListWrapper>(res.Count);
                if (res != null && res.Count > 0)
                {
                    double recoveryAmount;
                    for (int i = 0; i < res.Count; i++)
                    {
                        double renbalance = Convert.ToDouble(res[i].RentBalance);// Convert.ToDouble(RentBalance.CalculateRentBalance(Convert.ToDouble(res[i].RentBalance), Convert.ToDouble(res[i].TOTALRENT)));

                        recoveryAmount = RentBalance.CalculateRecoveryAmount(renbalance, Convert.ToDouble(res[i].TOTALRENT));
                        string noticeIssue = "";
                        string noticeexpire = "";
                        if (res[i].NoticeIssueDate != null)
                        {
                            noticeIssue = res[i].NoticeIssueDate.Value.ToString("dd/MM/yyyy");
                        }
                        if (res[i].NoticeExpiryDate != null)
                        {
                            noticeexpire = res[i].NoticeExpiryDate.Value.ToString("dd/MM/yyyy");
                        }
                        ListVacate.Add(new NoticeToVacateListWrapper(res[i].CustomerName, res[i].CustomerAddress, noticeIssue, noticeexpire, recoveryAmount, Convert.ToDouble(res[i].TOTALRENT), res[i].PaymentAgreed));

                    }
                }

                return ListVacate;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region"Get Balance List Report"

        public List<AM_SP_GETRENTBALANCELIST_Result> GetBalanceListReport(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, int index, int pageSize, string sortExpression, string sortDir,string rentBalanceFrom, string rentBalanceTo)
        {
            try
            {
                IBalanceListBO balanceList = FactoryBO.CreateBalalnceListBO();
                return balanceList.GetBalanceList(_caseOwnedBy,_regionId, _suburbId, _customerStatus, _assetType, index, pageSize, sortExpression, sortDir,rentBalanceFrom,rentBalanceTo  );
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Asset Type"

        public IQueryable GetAssetType()
        {
            try
            {
                IAssetTypeBo assetType = FactoryBO.CreateAssetTypeBO();
                return assetType.GetAssetType();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        public bool CheckHistoricalTable(int CustomerId, int TenancyId, string month, int year)
        {
            try
            {
                IBalanceListBO historicalList = FactoryBO.CreateBalalnceListBO();
                return historicalList.CheckHistoricalTable(CustomerId, TenancyId, month, year);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddHistoricalBalanceList(AM_HistoricalBalanceList historicalBalanceList)
        {
            try
            {
                IBalanceListBO newHistoricalBalanceList = FactoryBO.CreateBalalnceListBO();
                newHistoricalBalanceList.AddHistoricalBalanceList(historicalBalanceList);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        #region "Get Balance List Export to excel"

        public List<AM_SP_GetBalanceListExportToExcel_Result> GetBalanceListExportToExcel(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, string sortExpression, string sortDir)
        {
            try
            {
                IBalanceListBO balanceList = FactoryBO.CreateBalalnceListBO();
                return balanceList.GetBalanceListExportToExcel(_caseOwnedBy, _regionId, _suburbId, _customerStatus, _assetType, sortExpression, sortDir);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Historical Balance List"

        public List<AM_SP_GetHistoricalBalanceListExportToExcel_Result > GetHistoricalBalanceListExportToExcel(int caseOwnedBy, int region, int suburb, int startIndex, int endIndex, int customerStatus, int assetType, int months, int years, string sortExpression, string sortDir)
        {
            try
            {
                IBalanceListBO historicalList = FactoryBO.CreateBalalnceListBO();
                return historicalList.GetHistoricalBalanceListExportToExcel(caseOwnedBy, region, suburb, startIndex, endIndex, customerStatus, assetType, months, years, sortExpression, sortDir);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        
        #region"Get Net Rent Balance "

        public AM_SP_GetAnticipatedHBBalance_Result GetAnticipatedHBBalance(string postcode, int caseOwnedBy, int region, int suburb)
        {
            try
            {
                IBalanceListBO historicalList = FactoryBO.CreateBalalnceListBO();
                return historicalList.GetAnticipatedHBBalance(postcode, caseOwnedBy, region, suburb);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
