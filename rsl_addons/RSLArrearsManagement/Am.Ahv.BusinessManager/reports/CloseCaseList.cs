﻿using System;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;
using System.Linq;

namespace Am.Ahv.BusinessManager.reports
{
    public class CloseCaseList
    {
        

        #region"Get Case List"

        public IQueryable getUserList(int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir)
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
                return caseBO.getCloseCaseList(regionId, caseOwnerId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, dbIndex, pageSize, _statusTitle,surname, sortExpression, sortDir);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion


        #region"Get Case Owner List"

        public IQueryable getCaseOwnerList(int regionId)
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
                return caseBO.getCaseOwnerList(regionId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public IQueryable getCaseOwnerList(int regionId, int suburbId)
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
                return caseBO.getCaseOwnerList(regionId, suburbId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get records Count"

        public int getRecordsCount(int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, string _statusTitle,string surname)
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
                return caseBO.getCloseRecordsCount(regionId, caseOwnerId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, _statusTitle, surname);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

    }
}
