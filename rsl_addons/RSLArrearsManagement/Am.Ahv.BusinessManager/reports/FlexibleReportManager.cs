﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessManager.payments;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.payment;
using Am.Ahv.BusinessObject.factory_bo;
namespace Am.Ahv.BusinessManager.statusmgmt
{
    public class FlexibleReportManager
    {
        public List<PaymentPlanWrapper> GetPaymentPlanSummaryWrappered(int tennantId)
        {
            try
            {
                Payments p = new Payments();
                List<AM_PaymentPlan> ListPlan = p.GetPaymentPlanSummary(tennantId);
                if (ListPlan != null)
                {
                    List<PaymentPlanWrapper> ppw = new List<PaymentPlanWrapper>(1);
                    PaymentPlanWrapper wrapper = new PaymentPlanWrapper();

                    if (ListPlan[0].StartDate != null)
                    {
                        wrapper.StartDate = ListPlan[0].StartDate.Value.ToString("dd/MM/yyyy");
                    }
                    wrapper.EndDate = ListPlan[0].EndDate.ToString("dd/MM/yyyy");
                    wrapper.Frequency = ListPlan[0].AM_LookupCode.CodeName;
                    wrapper.AmountToBeCollected = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].AmountToBeCollected), 2).ToString("0,0.00");
                    wrapper.WeeklyRentAmount = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].WeeklyRentAmount), 2).ToString("0,0.00");
                    wrapper.ReviewDate = ListPlan[0].ReviewDate.ToString("dd/MM/yyyy");
                    
                    
                    

                    if (ListPlan[0].FirstCollectionDate != null)
                    {
                        wrapper.FirstCollectionDate = ListPlan[0].FirstCollectionDate.Value.ToString("dd/MM/yyyy");
                    }
                    wrapper.RentBalance = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].RentBalance), 2).ToString("0,0.00");
                    wrapper.LastPayment = ListPlan[0].LastPayment.ToString();
                    if (ListPlan[0].LastPaymentDate.Value != null)
                    {
                        wrapper.LastPaymentDate = ListPlan[0].LastPaymentDate.Value.ToString("dd/MM/yyyy");
                    }
                    wrapper.CreatedDate = ListPlan[0].CreatedDate.ToString("dd/MM/yyyy");
                    wrapper.RentPayable = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].RentPayable), 2).ToString("0,0.00");
                    wrapper.ArrearsCollection = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].ArrearsCollection), 2).ToString("0,0.00");
                    ppw.Add(wrapper);
                    return ppw;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaymentPlanWrapper> GetPaymentPlanSummaryHistory(int PaymentPlanHistoryId)
        {
            try
            {
                IPaymentPlanBo paymentPlan = FactoryBO.CreatePaymentPlanBo();
                List<AM_PaymentPlanHistory> ListPlan = paymentPlan.GetPaymentPlanSummaryHistory(PaymentPlanHistoryId);
                if (ListPlan != null)
                {
                    List<PaymentPlanWrapper> ppw = new List<PaymentPlanWrapper>(1);
                    PaymentPlanWrapper wrapper = new PaymentPlanWrapper();

                    if (ListPlan[0].StartDate != null)
                    {
                        wrapper.StartDate = ListPlan[0].StartDate.Value.ToString("dd/MM/yyyy");
                    }
                    wrapper.EndDate = ListPlan[0].EndDate.ToString("dd/MM/yyyy");
                    wrapper.Frequency = ListPlan[0].AM_LookupCode1.CodeName;
                    wrapper.AmountToBeCollected = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].AmountToBeCollected), 2).ToString("0,0.00");
                    wrapper.WeeklyRentAmount = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].WeeklyRentAmount), 2).ToString("0,0.00");
                    wrapper.ReviewDate = ListPlan[0].ReviewDate.ToString("dd/MM/yyyy");

                    if (ListPlan[0].FirstCollectionDate != null)
                    {
                        wrapper.FirstCollectionDate = ListPlan[0].FirstCollectionDate.Value.ToString("dd/MM/yyyy");
                    }
                    wrapper.RentBalance = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].RentBalance), 2).ToString("0,0.00");
                    wrapper.LastPayment = ListPlan[0].LastPayment.ToString();
                    wrapper.LastPaymentDate = ListPlan[0].LastPaymentDate.Value.ToString("dd/MM/yyyy");
                    wrapper.CreatedDate = ListPlan[0].CreatedDate.ToString("dd/MM/yyyy");
                    wrapper.RentPayable = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].RentPayable), 2).ToString("0,0.00");
                    wrapper.ArrearsCollection = "£ " + Math.Round(Convert.ToDecimal(ListPlan[0].ArrearsCollection), 2).ToString("0,0.00");
                    ppw.Add(wrapper);
                    return ppw;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
