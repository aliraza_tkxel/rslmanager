﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.reports_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;

namespace Am.Ahv.BusinessManager.statusmgmt
{
    public class StatusAndActionReport
    {
        #region"Get Status And Action Report"

        public IQueryable GetStatusAndActionReport(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize)
        {
            try
            {
                IStatusAndActionReportBO statusAndActionReport = FactoryBO.CreateStatusAndActionReportBO();
                return statusAndActionReport.GetStatusAndActionReport(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Status And Action Report Count"

        public int GetStatusAndActionReportCount(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag)
        {
            try
            {
                IStatusAndActionReportBO statusAndActionReport = FactoryBO.CreateStatusAndActionReportBO();
                return statusAndActionReport.GetStatusAndActionReportCount(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

    }
}
