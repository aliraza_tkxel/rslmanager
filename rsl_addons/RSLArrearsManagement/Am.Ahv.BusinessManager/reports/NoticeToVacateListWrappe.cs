﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BusinessManager.reports
{
    public class NoticeToVacateListWrapper
    {
        string customerName;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }
        string customerAddress;

        public string CustomerAddress
        {
            get { return customerAddress; }
            set { customerAddress = value; }
        }
        double recoverAmount;

        public double RecoverAmount
        {
            get { return recoverAmount; }
            set { recoverAmount = value; }
        }
        double paymentAgreed;

        public double PaymentAgreed
        {
            get { return paymentAgreed; }
            set { paymentAgreed = value; }
        }
        string noticeIssueDate;

        public string NoticeIssueDate
        {
            get { return noticeIssueDate; }
            set { noticeIssueDate = value; }
        }
        string noticeExpiryDate;

        public string NoticeExpiryDate
        {
            get { return noticeExpiryDate; }
            set { noticeExpiryDate = value; }
        }
        double? totalRent;

        public double? TotalRent
        {
            get { return totalRent; }
            set { totalRent = value; }
        }

        public NoticeToVacateListWrapper(string _customerName, string _customerAddress, string _issueDate, string _expireDate, double _recoveryAmount, double? _totalRent, double _paymentAgreed)
        {
            CustomerAddress = _customerAddress;
            CustomerName = _customerName;
            NoticeExpiryDate = _expireDate;
            NoticeIssueDate = _issueDate;
            TotalRent = _totalRent;
            RecoverAmount = _recoveryAmount;
            PaymentAgreed = _paymentAgreed;
        }

    }
}
