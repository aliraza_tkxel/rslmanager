﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.Lookup_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;
namespace Am.Ahv.BusinessManager.lookup
{
    public class Lookup
    {
        public List<Am.Ahv.Entities.AM_LookupCode> getLookupListings(string typeName)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                List<Am.Ahv.Entities.AM_LookupCode> list = lookupbo.GetLookupByName(typeName);
                return list;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Am.Ahv.Entities.AM_LookupCode> getLookupListings(string typeName, int DbIndex, int pageSize)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                List<Am.Ahv.Entities.AM_LookupCode> list = lookupbo.GetLookupByName(typeName, DbIndex, pageSize);
                return list;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int getLookupCount(string typeName)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                int count = lookupbo.getLookupCount(typeName);
                return count;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void updateLookup(int lookupCodeId, string value, int modifiedBy)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                lookupbo.updateLookup(lookupCodeId, value, modifiedBy);
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (IndexOutOfRangeException ie)
            {
                throw ie;
            }
            catch (UpdateException u)
            {
                throw u;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void addLookup(string value, string type, int createdBy)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                lookupbo.addLookup(value, type, createdBy);
            }
            catch (ArgumentOutOfRangeException ie)
            {
                throw ie;
            }
            catch (EntitySqlException ee)
            {
                throw ee;
            }
            catch (EntityException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string getLookup(int lookupcodeid)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                string lookup = lookupbo.getLookup(lookupcodeid);
                return lookup;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string GetUserType(int userId)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                return lookupbo.GetUserType(userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteOutComeLookUp(int OutComeId)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                return lookupbo.DeleteOutComeLookUp(OutComeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteLookup(int lookupCodeId,  int modifiedBy)
        {
            try
            {
                ILookupBo lookupbo = FactoryBO.CreateLookupBO();
                lookupbo.DeleteLookup(lookupCodeId, modifiedBy);
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (IndexOutOfRangeException ie)
            {
                throw ie;
            }
            catch (UpdateException u)
            {
                throw u;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
