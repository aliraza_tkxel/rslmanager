﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class CaseActivitiesWrapper
    {
        DateTime? recordingDate;

        public DateTime? RecordingDate
        {
            get { return recordingDate; }
            set { recordingDate = value; }
        }
        string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        string activity;

        public string Activity
        {
            get { return activity; }
            set { activity = value; }
        }
        string title;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        string recordedBy;

        public string RecordedBy
        {
            get { return recordedBy; }
            set { recordedBy = value; }
        }
        string notes;
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }


        public CaseActivitiesWrapper(DateTime? date, string status,string activity,string title,string recordedBy,string notes)
        {
            this.RecordingDate = date;
            this.Status = status;
            this.Activity = activity;
            this.Title = title;
            this.RecordedBy = recordedBy;
            this.Notes = notes;

        }

        public CaseActivitiesWrapper()
        {
 
        }

       
       

    }
}
