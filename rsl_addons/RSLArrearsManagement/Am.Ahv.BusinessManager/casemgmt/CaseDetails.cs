﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.Entities;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BoInterface.user_bo;
using System.Data;
using System.Transactions;
using System.Collections;
namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class CaseDetails
    {
        public List<int> GetCaseHistoryList(int CaseID)
        {
            ICaseHistoryBo CaseHistoryBO = FactoryBO.CreateCaseHistoryBO();
            return CaseHistoryBO.GetCaseHistoryCount(CaseID);
        }
        public AM_Case GetCaseById(int CaseID)
        {
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            return CaseBO.GetById(CaseID);
        }
        public string GetResourceNameById(int ResourceID)
        {
            IUserBo user = FactoryBO.CreateUserBO();
            return user.GetNameById(ResourceID);
        }
        public AM_Case GetCaseByHistoryId(int HistoryId)
        {
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            return CaseBO.GetCaseByCaseHistoryId(HistoryId);
        }
        public AM_CaseHistory GetCaseHistoryById(int HistoryId)
        {
            ICaseHistoryBo CaseHistoryBO = FactoryBO.CreateCaseHistoryBO();
            return CaseHistoryBO.GetById(HistoryId);
        }

        public int CountMyCases(string postCode,int caseOfficerId, int regionId, int suburbid)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.CountMyCases(postCode,caseOfficerId, regionId, suburbid);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IQueryable GetCaseDocuments(int caseId)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.GetCaseDocuments(caseId);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetInitiatedByName(int employeeId)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.GetInitiatedByName(employeeId);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DisableDocument(int docId)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                casebo.DisableDocument(docId);

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region"Disable Case Standard Letter"

        public void DisableCaseStandardLetter(int letterId, int caseId)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                casebo.DisableCaseStandardLetter(letterId, caseId);

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        public bool UpdateCase(int statusId, AM_Case caseObj, AM_CaseHistory caseHistory, List<AM_StandardLetterHistory> st, DataTable actionData, List<AM_StandardLetterHistory> oldSL, double marketRent, double rentBalance, DateTime todayDate, double rentAmount)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.UpdateCase(statusId, caseObj, caseHistory, st, actionData, oldSL, marketRent, rentBalance, todayDate, rentAmount);
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateCaseContactInfo(int caseId, AM_Case caseObj, bool isCustomerEditView)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.UpdateCaseContactInfo(caseId, caseObj, isCustomerEditView);
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region"Update Case"


        public bool UpdateCase(int statusId, AM_Case caseObj, AM_CaseHistory caseHistory, ArrayList documentList, List<AM_StandardLetterHistory> st, DataTable actionData, List<AM_StandardLetterHistory> oldSL, double marketRent, double rentBalance, DateTime todayDate, double rentAmount)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.UpdateCase(statusId, caseObj, caseHistory, documentList, st, actionData, oldSL, marketRent, rentBalance, todayDate, rentAmount);
            }
            catch (TransactionAbortedException transactionaborted)
            {
                throw transactionaborted;
            }
            catch (TransactionException transactionexception)
            {
                throw transactionexception;
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Case Standard Letters"

        public DataTable GetCaseStandardLetters(int caseId)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.GetCaseStandardLetters(caseId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetCaseStandardLetterHistory(int caseId)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.GetCaseStandardLetterHistory(caseId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public bool IsOutcomeFree(int outcomeId)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.IsOutComeExists(outcomeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool CheckRecoveryAmount(int statusId)
        {
            try
            {
                ICaseBo casebo = FactoryBO.CreateCaseBO();
                return casebo.CheckRecoveryAmount(statusId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
