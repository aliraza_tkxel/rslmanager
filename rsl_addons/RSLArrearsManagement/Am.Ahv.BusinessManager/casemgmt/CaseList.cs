﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.payments;
using System.Transactions;

namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class CaseList
    {
        

        #region"Get Case List"

        public IQueryable getUserList(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir, bool isNisp, string OverdueCheck)
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();                
                
                return caseBO.getCaseList(postCode, regionId, caseOwnerId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, dbIndex, pageSize, _statusTitle, surname, sortExpression, sortDir, isNisp, OverdueCheck);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"get OverDue Action Case List"

        public List<AM_SP_GetOverDueList_Result> getOverDueActionCaseList(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, int dbIndex, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir, bool isNisp, string OverdueCheck)
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();

                return caseBO.getOverDueActionCaseList(postCode, regionId, caseOwnerId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, dbIndex, pageSize, _statusTitle, surname, sortExpression, sortDir, isNisp, OverdueCheck);
                    
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion


        #region"Get Case Owner List"


        public IQueryable getCaseOwnerList()
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
                return caseBO.getCaseOwnerList();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public IQueryable getRegionList(int CaseOwnedById)
        {
            try
            {
                ICaseBo CaseBO = FactoryBO.CreateCaseBO();
                return CaseBO.getRegionList(CaseOwnedById);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable getSuburbList(int RegionId,int CaseOwnedById)
        {
            try
            {
                ICaseBo CaseBO = FactoryBO.CreateCaseBO();
                return CaseBO.getSuburbList(RegionId,CaseOwnedById);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IQueryable getCaseOwnerList(int regionId)
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
                return caseBO.getCaseOwnerList(regionId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #region"Get records Count"

        public int getRecordsCount(string postCode, int regionId, int caseOwnerId, int suburbId, bool allRegionFlag, bool allOwnerFlag, bool allSuburbFlag, string _statusTitle, string surname, bool isNisp, string OverdueCheck)
        {
            try
            {
                ICaseHistoryBo caseBO = FactoryBO.CreateCaseHistoryBO();
                return caseBO.getRecordsCount(postCode, regionId, caseOwnerId, suburbId, allRegionFlag, allOwnerFlag, allSuburbFlag, _statusTitle, surname, isNisp, OverdueCheck);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion
        #endregion

        #region Find OverDue Action Cases

        public int FindOverDueActionCases()
        {

            try
            {
                ICaseBo caseBO = FactoryBO.CreateCaseBO();

                //delete old overdue actions cases

                //Find Tenancies of zero missed payment
                Payments zeroMissedPayments = new Payments();
                List<AM_SP_GetZeroMissedPaymentTenancies_Result> zeroMissedList = new List<AM_SP_GetZeroMissedPaymentTenancies_Result>();
                zeroMissedList = zeroMissedPayments.GetZeroMissedPaymentTenancies();
               

                //Find overdue action cases
                List<AM_SP_FindOverDueActionCases_Result> overDueActionCaseList = new List<AM_SP_FindOverDueActionCases_Result>();
                overDueActionCaseList = caseBO.FindOverDueActionCases();

                //Add missed payment 
                if (zeroMissedList.Count() > 0)
                {
                    for (int i = 0; i < zeroMissedList.Count; i++)
                    {
                        AM_SP_FindOverDueActionCases_Result singleOverDueCase = new AM_SP_FindOverDueActionCases_Result();
                        singleOverDueCase = overDueActionCaseList.Where(s => s.TenancyId == zeroMissedList[i].TenantId).SingleOrDefault();
                        if (singleOverDueCase != null)
                        {
                            overDueActionCaseList.Remove(singleOverDueCase);
                        }
                    }
                }


                AM_FindOverDueActionCase overDueActionCase = new AM_FindOverDueActionCase();

                using (TransactionScope trans = new TransactionScope())
                {
                    caseBO.DeleteOverDueActionCases();

                    if (overDueActionCaseList != null && overDueActionCaseList.Count > 0)
                    {

                        for (int i = 0; i < overDueActionCaseList.Count; i++)
                        {
                            overDueActionCase.ActionTitle = overDueActionCaseList[i].ActionTitle;
                            overDueActionCase.CaseId = overDueActionCaseList[i].CaseId;
                            overDueActionCase.Created = DateTime.Now;
                            overDueActionCase.CustomerAddress = overDueActionCaseList[i].CustomerAddress;
                            overDueActionCase.CustomerId = overDueActionCaseList[i].CustomerId;
                            overDueActionCase.CustomerName = overDueActionCaseList[i].CustomerName;
                            overDueActionCase.CustomerName2 = overDueActionCaseList[i].CustomerName2;
                            overDueActionCase.IsSuppressed = overDueActionCaseList[i].IsSuppressed;
                            overDueActionCase.JointTenancyCount = overDueActionCaseList[i].JointTenancyCount;
                            overDueActionCase.OwedToBha = overDueActionCaseList[i].OwedToBHA;
                            overDueActionCase.PaymentPlan = overDueActionCaseList[i].PaymentPlan;
                            overDueActionCase.RentBalance = overDueActionCaseList[i].RentBalance;
                            overDueActionCase.StatusTitle = overDueActionCaseList[i].StatusTitle;
                            overDueActionCase.SuppressedDate = overDueActionCaseList[i].SuppressedDate;
                            overDueActionCase.TenancyId = overDueActionCaseList[i].TenancyId;
                            overDueActionCase.TotalRent = overDueActionCaseList[i].totalRent;
                            overDueActionCase.EstimatedHBDue = overDueActionCaseList[i].EstimatedHBDue;
                            overDueActionCase.FirstName = overDueActionCaseList[i].FirstName;
                            overDueActionCase.LastName = overDueActionCaseList[i].LastName;
                            

                            caseBO.SaveOverDueActionCaseRecord(overDueActionCase);
                        }
                    }
                    caseBO.SaveOverDueActionTransaction();
                    trans.Complete();
                }

                return overDueActionCaseList.Count();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

    }
}
