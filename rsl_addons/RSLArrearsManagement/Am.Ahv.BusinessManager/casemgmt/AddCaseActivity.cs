﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.Lookup_bo;
using Am.Ahv.BoInterface.activity_bo;
using System.Data;
using Am.Ahv.BoInterface.referral_bo;
using Am.Ahv.BoInterface.user_bo;
using Am.Ahv.BusinessObject.user_bo;
using Am.Ahv.Utilities.constants;

namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class AddCaseActivity
    {

        public List<AM_LookupCode> GetActivityTypes()
        {
            try
            {
                ILookupBo LookupBO = FactoryBO.CreateLookupBO();
                return LookupBO.GetLookupByName(ApplicationConstants.activityType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public List<AM_LookupCode> GetOutcomes()
        {
            try
            {

                ILookupBo LookupBO = FactoryBO.CreateLookupBO();
                return LookupBO.GetOutComeLookupByName(ApplicationConstants.outcome);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_LookupCode> GetReferralType()
        {
            try
            {
                ILookupBo LookupBo = FactoryBO.CreateLookupBO();
                return LookupBo.GetLookupByName(ApplicationConstants.referralType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }


        public int SaveActivity(AM_Activity obj,List<string> ListLetters, double marketRent, double rentBalance, DateTime todayDate, double rentAmount)
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.AddActivity(obj, ListLetters, marketRent, rentBalance, todayDate, rentAmount);
            }
            catch(Exception ex)
            {
                throw ex;
            }
           
        }

        public AM_Activity SaveActivity(AM_Activity obj)
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.AddNew(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public AM_Referral SaveReferral(AM_Referral obj)
        {
            try
            {
                IReferralBO referralBo = FactoryBO.CreateReferralBO();
                return referralBo.AddNew(obj);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        #region"Get Case Activities"

        public IQueryable GetCaseActivities(int caseId)
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.GetCaseActivities(caseId);
            }
             catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region"Update Case Activity"
        public AM_Activity UpdateActivity(AM_Activity obj)
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.Update(obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Get Activity By Id"
        public AM_Activity GetActivityById(int Id)
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.GetById(Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get Organisations"
        public IQueryable GetOrganisations()
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.GetOrganisations();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get External Agency Organisations"

        public IQueryable GetExternalAgencyOrganisations()
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.GetExternalAgencyOrganisations();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Contacts"

        public IQueryable GetContacts(int OrgId)
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.GetContacts(OrgId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Contact Email"
        public string GetContactEmail(int EmployeeId)
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.GetContactEmail(EmployeeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get Support Worker"
        public List<E__EMPLOYEE> GetSupportWorker(int teamId)
        {
            try
            {
                IUserBo ubo = FactoryBO.CreateUserBO();
                 return ubo.GetSupportWorkers(teamId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        //#region"Get Case Activity Documents"

        //public List<AM_Documents> GetCaseActivityDocuments(int activityId)
        //{
        //    try
        //    {
        //        IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
        //        return ActivityBo.GetCaseActivityDocuments(activityId);
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //#endregion

        public int GetCustomerId(int tennantId)
        {
            try
            {
                IActivityBO ActivityBo = FactoryBO.CreateActivityBO();
                return ActivityBo.GetCustomerId(tennantId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
