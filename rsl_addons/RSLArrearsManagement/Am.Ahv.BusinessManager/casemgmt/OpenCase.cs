﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;
using System.Collections;

namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class OpenCase
    {
        #region"Open New Case"

        public int OpenNewCase(AM_Case newCase)
        {
            try
            {
                IOpenCaseBO openCase = FactoryBO.createOpenCaseBO();
                return openCase.OpenCase(newCase);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Add Case History"

        public int AddCaseHistoryandDocuments(AM_Case newCase, AM_CaseHistory caseHistory, ArrayList listDocuments, ArrayList listLetters, int tenantId, int modifiedBy, DataTable ActionData, double marketRent, double rentBalance, DateTime todayDate, double rentAmount)
        {
            try
            {
                IOpenCaseBO openCase = FactoryBO.createOpenCaseBO();
                return openCase.AddCaseHistoryandDocuments(newCase, caseHistory, listDocuments, listLetters, tenantId, modifiedBy, ActionData, false, marketRent, rentBalance, todayDate, rentAmount);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Add Documents"

        public void AddDocuments(AM_Documents doc)
        {
            try
            {
                IOpenCaseBO openCase = FactoryBO.createOpenCaseBO();
                openCase.AddDocuments(doc);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Check Case"

        public bool CheckCase(int tenantId)
        {
            try
            {
                IOpenCaseBO openCase = FactoryBO.createOpenCaseBO();
                return openCase.CheckCase(tenantId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Check Payment Plan"

        public bool CheckPaymentPlan(int tenantId)
        {
            try
            {
                IOpenCaseBO openCase = FactoryBO.createOpenCaseBO();
                return openCase.CheckPaymentPlan(tenantId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        #endregion

        #region"Set defaulter Bit"

        public bool SetDefaulterBit(int tenantId, int modifiedBy)
        {
            try
            {
                IOpenCaseBO openCase = FactoryBO.createOpenCaseBO();
                return openCase.SetDefaulterBit(tenantId, modifiedBy);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Disable Open Case"

        public void DisableOpenCase(int caseId)
        {
            try
            {
                IOpenCaseBO openCase = FactoryBO.createOpenCaseBO();
                openCase.DisableOpenedCase(caseId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Save Payment Plan and Open New Case"

        public int SavePaymentPlanAndOpenNewCase(AM_PaymentPlan paymentPlan, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayments, AM_Case amCase, AM_CaseHistory caseHistory,
                                                    ArrayList listDocuments, ArrayList listLetters, int tenantId, int modifiedBy, DataTable ActionData)
        {
            try
            {
                IOpenCaseBO openCase = FactoryBO.createOpenCaseBO();
                return openCase.SavePaymentPlanAndOpenNewCase(paymentPlan, paymentPlanHistory, listPayments, amCase, caseHistory, listDocuments, listLetters, tenantId, modifiedBy, ActionData);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
