﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;

namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class SuppressedCase
    {
        #region"Get Assigned To List"

        public IQueryable GetAssignedToList()
        {
            try
            {
                ISuppressedCaseBO suppressedCase = FactoryBO.CreateSuppressedCaseBO();
                return suppressedCase.GetAssignedToList();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Region List"

        public IQueryable GetRegionList(int resourceId)
        {
            try
            {
                ISuppressedCaseBO suppressedCase = FactoryBO.CreateSuppressedCaseBO();
                return suppressedCase.GetRegionList(resourceId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
 
        }

        #endregion

        #region"Get Suppressed Cases"

        public IQueryable GetSuppressedCases(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string sortExpression, string sortDir)
        {
            try 
            {
                ISuppressedCaseBO suppressedCase = FactoryBO.CreateSuppressedCaseBO();
                return suppressedCase.GetSuppressedCases(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize, sortExpression, sortDir);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Suppressed Cases Count"

        public int GetSuppressedCasesCount(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag)
        {
            try
            {
                ISuppressedCaseBO suppressedCase = FactoryBO.CreateSuppressedCaseBO();
                return suppressedCase.GetSuppressedCasesCount(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Cases owned by"

        public IQueryable CasesOwnedBy()
        {
            try
            {
                ISuppressedCaseBO suppressedCase = FactoryBO.CreateSuppressedCaseBO();
                return suppressedCase.CasesOwnedBy();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion
    }
}
