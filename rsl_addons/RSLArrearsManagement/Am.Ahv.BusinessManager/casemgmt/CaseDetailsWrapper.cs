﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BusinessManager.Casemgmt
{
 public class CaseDetailsWrapper
    {
        DateTime MainDate;

        public DateTime mainDate
        {
            get { return MainDate; }
            set { MainDate = value; }
        }
        string Stage;

        public string stage
        {
            get { return Stage; }
            set { Stage = value; }
        }
        string Action;

        public string action
        {
            get { return Action; }
            set { Action = value; }
        }
        DateTime FollowupDate;

        public DateTime Followupdate
        {
            get { return FollowupDate; }
            set { FollowupDate = value; }
        }
        DateTime NotesDate;

        public DateTime Notesdate
        {
            get { return NotesDate; }
            set { NotesDate = value; }
        }
        string NotesTitle;

        public string Notestitle
        {
            get { return NotesTitle; }
            set { NotesTitle = value; }
        }
        string RecordedBy;

        public string Recordedby
        {
            get { return RecordedBy; }
            set { RecordedBy = value; }
        }
        string Notes;

        public string notes
        {
            get { return Notes; }
            set { Notes = value; }
        }
        int DocumentCount;

        public int Documentcount
        {
            get { return DocumentCount; }
            set { DocumentCount = value; }
        }
        string StandardLetter;

        public string Standardletter
        {
            get { return StandardLetter; }
            set { StandardLetter = value; }
        }
        public CaseDetailsWrapper()
        {
 
        }
       



    }
}
