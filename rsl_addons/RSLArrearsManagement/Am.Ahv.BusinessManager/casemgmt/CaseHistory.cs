﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BoInterface.documents_bo;
using System.Collections;
using Am.Ahv.BoInterface.status_bo;
using Am.Ahv.BoInterface.actionbo;
using Am.Ahv.BoInterface.Lookup_bo;
using Am.Ahv.BoInterface.tenantbo;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BoInterface.activity_bo;
using Am.Ahv.BoInterface.letters_bo;
using Am.Ahv.BoInterface.customerbo;
namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class CaseHistory
    {
        public CaseHistory()
        {

        }
        public IQueryable GetCaseHistory(int caseID)
        {
            ICaseHistoryBo historybo = FactoryBO.CreateCaseHistoryBO();
            try
            {
                return historybo.GetCaseHistoryByCaseId(caseID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<AM_Documents> GetDocumentsByHistory(int CaseHistoryId)
        {
            IDocumentBo docbo = FactoryBO.CreateDocumentBO();
            return docbo.GetDocumentsByCaseHistoryId(CaseHistoryId);
        }
        /// <summary>
        /// Dummy Function for the sake of testing only
        /// </summary>
        /// <param name="caseID"></param>
        /// <returns></returns>
        //public List<AM_CaseHistory> GetCaseHistory(int caseID)
        //{
        //    ICaseHistoryBo historybo = FactoryBO.CreateCaseHistoryBO();
        //    List<AM_CaseHistory> caseHistory = historybo.GetCasehistory(caseID);

        //    return caseHistory;
        //}
        //public void drillingmethod()
        //{
        //    IQueryable Test = GetCaseHistory(3);




        //    Test.Cast<AM_CaseHistory>();
        //     Dictionary<String,String> dict= new Dictionary<string,string>();
        //    IEnumerator i = Test.GetEnumerator();
        //    //while (i.MoveNext())
        //    //{
        //    //    object o = i.Current;
        //    //   // dict.Add(i.ToString(), o.ToString());
        //    //   ////dict.Add((Dictionary<String,String>) i.Current);
        //    //    //String str=dict["ActionTitle"];
        //    //    string typeobj=o.GetType().FullName;
        //    //}
        //    //foreach(var v in Test)
        //    //{
        //    //    string at=v
        //    //}
        //}
        public List<AM_CaseHistory> GetCaseHistoryNotes(int caseHistoryID)
        {
            ICaseHistoryBo cbo = FactoryBO.CreateCaseHistoryBO();
            return cbo.GetCaseHistoryByID(caseHistoryID);
        }
        /// <summary>
        /// The Giant method
        /// </summary>
        /// <param name="CaseID"></param>
        /// <returns></returns>
        /// 

        #region"Generate Alert"

        public Dictionary<string, string> GenrateAlert(AM_Case AmCase, bool isNextStatus, int customerId)
        {
            Dictionary<string, string> DataDictonary = new Dictionary<string, string>();

            /// Intializing the BOs of related entities 
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            IActionBO ActionBO = FactoryBO.CreateActionBO();
            ILookupBo LookupBO = FactoryBO.CreateLookupBO();
            ITenantBo TenantBO = FactoryBO.CreateTenantBO();
            ILettersBo LettersBO = FactoryBO.CreateLettersBO();
            //Getting Case Object With All references loaded
            //AM_Case AmCase = CaseBO.GetById(CaseID);
            if (AmCase != null)
            {
                //Getting New Status By Rank
                AM_Status AmStatus = null;
                AM_Action AMAction = null;
                string letter = string.Empty;

                //Getting New Action By Rank
                //AM_Action AMAction = ActionBO.GetActionByRank(AmCase.AM_Action.Ranking + 1);

                AMAction = ActionBO.GetActionByRankStatus(AmCase.StatusId, AmCase.AM_Action.Ranking + 1);
                DateTime ActionDate = DateTime.Now;
                if (AMAction != null && isNextStatus == false)
                {
                    AMAction.AM_LookupCode1Reference.Load();
                    string duration = AMAction.AM_LookupCode1.CodeName;
                    ActionDate = DateOperations.AddDate(AMAction.RecommendedFollowupPeriod, duration, AmCase.ActionRecordedDate.Value);
                    DataDictonary.Add("Status", AmCase.AM_Status.Title);
                    DataDictonary.Add("Action", AMAction.Title);
                    DataDictonary.Add("ReviewDate", ActionDate.ToString("dd/MM/yyyy"));
                    DataDictonary.Add("StatusId", AmCase.StatusId.ToString());
                    DataDictonary.Add("ActionId", AMAction.ActionId.ToString());

                    letter = LettersBO.GetLettersNameByAction(AMAction.ActionId);
                    if (letter != string.Empty)
                    {
                        DataDictonary.Add("Letter", letter);
                    }
                    else
                    {
                        DataDictonary.Add("Letter", "No Letter Attached");
                    }
                }
                else
                {
                    AmStatus = GetStatusByRank(AmCase.AM_Status.Ranking + 1);
                    if (AmStatus != null)
                    {
                        AMAction = ActionBO.GetActionByRankStatus(AmStatus.StatusId, 1);
                        if (AMAction != null)
                        {
                            AMAction.AM_LookupCode1Reference.Load();
                            string duration = AMAction.AM_LookupCode1.CodeName;
                            ActionDate = DateOperations.AddDate(AMAction.RecommendedFollowupPeriod, duration, AmCase.ActionRecordedDate.Value);
                            DataDictonary.Add("Status", AmStatus.Title);
                            DataDictonary.Add("Action", AMAction.Title);
                            DataDictonary.Add("ReviewDate", ActionDate.ToString("dd/MM/yyyy"));
                            DataDictonary.Add("StatusId", AmStatus.StatusId.ToString());
                            DataDictonary.Add("ActionId", AMAction.ActionId.ToString());

                            letter = LettersBO.GetLettersNameByAction(AMAction.ActionId);
                            if (letter != string.Empty)
                            {
                                DataDictonary.Add("Letter", letter);
                            }
                            else
                            {
                                DataDictonary.Add("Letter", "No Letter Attached");
                            }
                        }
                        else
                        {
                            DataDictonary.Add("Status", AmStatus.Title);
                            DataDictonary.Add("Action", "No Next Action");
                            DataDictonary.Add("ReviewDate", ActionDate.ToString("dd/MM/yyyy"));
                            DataDictonary.Add("Letter", "No Letter Attached");
                            DataDictonary.Add("StatusId", AmStatus.StatusId.ToString());
                            DataDictonary.Add("ActionId", "No Next Action");
                        }
                    }
                    else
                    {
                        DataDictonary.Add("Status", "No Next Stage");
                        DataDictonary.Add("Action", "No Next Action");
                        DataDictonary.Add("ReviewDate", ActionDate.ToString("dd/MM/yyyy"));
                        DataDictonary.Add("Letter", "No Letter Attached");
                        DataDictonary.Add("StatusId", "No Next Status");
                        DataDictonary.Add("ActionId", "No Next Action");
                    }
                }

                AmCase.AM_ActionReference.Load();
                DataDictonary.Add("NextActiondetails", AmCase.AM_Action.NextActionDetails);

                //Getting the TenantPhone Number
                string Contactnumber = TenantBO.GetCustomerContactByTenantID(AmCase.TenancyId);
                if (Contactnumber == null)
                {
                    Contactnumber = string.Empty;
                }
                DataDictonary.Add("Contactnumber", Contactnumber);

                AM_SP_GetCustomerInformation_Result customer = new AM_SP_GetCustomerInformation_Result();
                ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
                customer = iCustomerBo.GetCustomerInformation(AmCase.TenancyId, customerId);

                if (customer != null)
                {
                    if (customer.JointTenancyCount == 1)
                    {
                        DataDictonary.Add("CustomerName", customer.CustomerName);
                    }
                    else
                    {
                        DataDictonary.Add("CustomerName", customer.CustomerName + " & " + customer.CustomerName2);
                    }
                }
                else
                {
                    DataDictonary.Add("CustomerName", string.Empty);
                }

                AmCase.AM_Action.AM_LookupCodeReference.Load();
                string recordedDuration = LookupBO.getLookup(AmCase.AM_Action.NextActionAlertFrequencyLookup);

                //Adding Trigger values to the Case Created Date
                DateTime recordedActionDueDate = DateOperations.AddDate(AmCase.AM_Action.NextActionAlert, recordedDuration, AmCase.ActionRecordedDate.Value);

                DataDictonary.Add("RecordedActionDueDate", recordedActionDueDate.ToString("dd/MM/yyyy"));

                return DataDictonary;
            }
            else
            {
                return null;
            }
        }

        #endregion
        #region"Generate Alert"

        public Dictionary<string, string> GenrateAlert(int CaseID, int customerId)
        {
            Dictionary<string, string> DataDictonary = new Dictionary<string, string>();

            /// Intializing the BOs of related entities 
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            IActionBO ActionBO = FactoryBO.CreateActionBO();
            ILookupBo LookupBO = FactoryBO.CreateLookupBO();
            ITenantBo TenantBO = FactoryBO.CreateTenantBO();
            ILettersBo LettersBO = FactoryBO.CreateLettersBO();
            //Getting Case Object With All references loaded
            AM_Case AmCase = CaseBO.GetById(CaseID);
            if (AmCase != null)
            {
                //Getting New Status By Rank
                AM_Status AmStatus = null;
                AM_Action AMAction = null;
                string letter = string.Empty;

                //Getting New Action By Rank
                //AM_Action AMAction = ActionBO.GetActionByRank(AmCase.AM_Action.Ranking + 1);

                AMAction = ActionBO.GetActionByRankStatus(AmCase.StatusId, AmCase.AM_Action.Ranking + 1);
                DateTime ActionDate = DateTime.Now;
                if (AMAction != null)
                {
                    string duration = LookupBO.getLookup(AMAction.NextActionAlertFrequencyLookup);
                    ActionDate = DateOperations.AddDate(AMAction.NextActionAlert, duration, AmCase.ActionRecordedDate.Value);
                    DataDictonary.Add("Status", AmCase.AM_Status.Title);
                    DataDictonary.Add("Action", AMAction.Title);
                    DataDictonary.Add("ReviewDate", ActionDate.ToString("dd/MM/yyyy"));
                    DataDictonary.Add("StatusId", AmCase.StatusId.ToString());
                    DataDictonary.Add("ActionId", AMAction.ActionId.ToString());
                    letter = LettersBO.GetLettersNameByAction(AMAction.ActionId);
                    if (letter != string.Empty)
                    {
                        DataDictonary.Add("Letter", letter);
                    }
                    else
                    {
                        DataDictonary.Add("Letter", "No Letter Attached");
                    }
                }
                else
                {
                    AmStatus = GetStatusByRank(AmCase.AM_Status.Ranking + 1);
                    if (AmStatus != null)
                    {
                        AMAction = ActionBO.GetActionByRankStatus(AmStatus.StatusId, 1);
                        if (AMAction != null)
                        {
                            string duration = LookupBO.getLookup(AMAction.NextActionAlertFrequencyLookup);
                            ActionDate = DateOperations.AddDate(AMAction.NextActionAlert, duration, AmCase.ActionRecordedDate.Value);
                            DataDictonary.Add("Status", AmStatus.Title);
                            DataDictonary.Add("Action", AMAction.Title);
                            DataDictonary.Add("ReviewDate", ActionDate.ToString("dd/MM/yyyy"));
                            DataDictonary.Add("StatusId", AmStatus.StatusId.ToString());
                            DataDictonary.Add("ActionId", AMAction.ActionId.ToString());

                            letter = LettersBO.GetLettersNameByAction(AMAction.ActionId);
                            if (letter != string.Empty)
                            {
                                DataDictonary.Add("Letter", letter);
                            }
                            else
                            {
                                DataDictonary.Add("Letter", "No Letter Attached");
                            }
                        }
                        else
                        {
                            DataDictonary.Add("Status", AmStatus.Title);
                            DataDictonary.Add("Action", "No Next Action");
                            DataDictonary.Add("ReviewDate", ActionDate.ToString("dd/MM/yyyy"));
                            DataDictonary.Add("Letter", "No Letter Attached");
                            DataDictonary.Add("StatusId", AmStatus.StatusId.ToString());
                            DataDictonary.Add("ActionId", "No Next Action");
                        }
                    }
                    else
                    {
                        DataDictonary.Add("Status", "No Next Stage");
                        DataDictonary.Add("Action", "No Next Action");
                        DataDictonary.Add("ReviewDate", ActionDate.ToString("dd/MM/yyyy"));
                        DataDictonary.Add("Letter", "No Letter Attached");
                        DataDictonary.Add("StatusId", "No Next Status");
                        DataDictonary.Add("ActionId", "No Next Action");
                    }
                }

                //Getting the TenantPhone Number
                string Contactnumber = TenantBO.GetCustomerContactByTenantID(AmCase.TenancyId);
                if (Contactnumber == null)
                {
                    Contactnumber = string.Empty;
                }
                DataDictonary.Add("Contactnumber", Contactnumber);

                AM_SP_GetCustomerInformation_Result customer = new AM_SP_GetCustomerInformation_Result();
                ICustomerBo iCustomerBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateCustomerBo();
                customer = iCustomerBo.GetCustomerInformation(AmCase.TenancyId, customerId);

                if (customer != null)
                {
                    if (customer.JointTenancyCount == 1)
                    {
                        DataDictonary.Add("CustomerName", customer.CustomerName);
                    }
                    else
                    {
                        DataDictonary.Add("CustomerName", customer.CustomerName + " & " + customer.CustomerName2);
                    }
                }
                else
                {
                    DataDictonary.Add("CustomerName", string.Empty);
                }

                AmCase.AM_Action.AM_LookupCodeReference.Load();
                string recordedDuration = LookupBO.getLookup(AmCase.AM_Action.NextActionAlertFrequencyLookup);

                //Adding Trigger values to the Case Created Date
                DateTime recordedActionDueDate = DateOperations.AddDate(AmCase.AM_Action.NextActionAlert, recordedDuration, AmCase.ActionRecordedDate.Value);

                DataDictonary.Add("RecordedActionDueDate", recordedActionDueDate.ToString("dd/MM/yyyy"));

                return DataDictonary;
            }
            else
            {
                return null;
            }
        }

        #endregion

        public bool IsSuppressed(int caseId)
        {
            try
            {
                ICaseBo CaseBO = FactoryBO.CreateCaseBO();
                ILookupBo LookupBO = FactoryBO.CreateLookupBO();

                AM_Case caseObj = CaseBO.GetById(caseId);
                string duration = LookupBO.getLookup(caseObj.AM_Action.NextActionAlertFrequencyLookup);
                if (duration != null)
                {
                    DateTime date = DateOperations.AddDate(caseObj.AM_Action.NextActionAlert, duration, caseObj.ActionRecordedDate.Value);

                    if (!DateOperations.FutureDateValidation(date))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        public bool CheckSuppress(int caseId)
        {
            try
            {
                ICaseBo CaseBO = FactoryBO.CreateCaseBO();
                return CaseBO.CheckSuppress(caseId);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private AM_Status GetStatusByRank(int Rank)
        {
            IStatusBo StatusBO = FactoryBO.CreateStatusBO();
            return StatusBO.GetStatusByRank(Rank);
        }
        /// <summary>
        /// Get Action Ranking
        /// </summary>
        /// <param name="Rank"></param>
        /// <returns></returns>
        private AM_Action GetActionByRank(int Rank)
        {
            IActionBO ActionBO = FactoryBO.CreateActionBO();
            return ActionBO.GetActionByRank(Rank);
        }
        public Boolean IgnoreCase(int CaseId, string Reason, int ignoredActionStatusId, int IgnoredActionId, bool isModalPopUp)
        {
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            return CaseBO.IgnoreCase(CaseId, Reason, ignoredActionStatusId, IgnoredActionId, isModalPopUp);
        }
        public Boolean SupressCase(int CaseId, string Reason, DateTime Date, int supressedBy)
        {
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            ICaseHistoryBo casehistoryBo = FactoryBO.CreateCaseHistoryBO();
            Boolean flag = CaseBO.SupressCase(CaseId, Reason, Date, supressedBy);
            Boolean flag1 = casehistoryBo.UpdateHistorySupressedValues(CaseId, Reason, Date, supressedBy);
            if (flag && flag1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean RecordActionCount(int CaseId)
        {
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            ICaseHistoryBo HistoryBO = FactoryBO.CreateCaseHistoryBO();
            Boolean flag = CaseBO.UpdateActionCount(CaseId);
            bool flag2 = HistoryBO.UpdateActionCount(CaseId);
            AM_Case getcase = CaseBO.GetById(CaseId);
            if (getcase != null)
            {
                Boolean flag1 = HistoryBO.UpdateHistoryIgnoreByStatusAction(getcase.StatusId, getcase.ActionId, CaseId);
                if (flag && flag1 && flag2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public Boolean ActionIgnoreCount(int CaseId)
        {
            ICaseBo CaseBO = FactoryBO.CreateCaseBO();
            ICaseHistoryBo HistoryBO = FactoryBO.CreateCaseHistoryBO();
            Boolean flag = CaseBO.UpdateIgnoreActionCount(CaseId);
            bool flag2 = HistoryBO.UpdateActionIgnoreCount(CaseId);
            AM_Case getcase = CaseBO.GetById(CaseId);
            if (getcase != null)
            {
                Boolean flag1 = HistoryBO.UpdateHistoryIgnoreByStatusAction(getcase.StatusId, getcase.ActionId, CaseId);
                if (flag && flag1 && flag2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public List<AM_Activity> GetActivityByCase(int CaseId)
        {
            IActivityBO ActivityBO = FactoryBO.CreateActivityBO();
            return ActivityBO.GetActivitiesByCaseId(CaseId);
        }

        public int GetLatestCaseHistoryId(int CaseId)
        {
            try
            {
                ICaseHistoryBo HistoryBO = FactoryBO.CreateCaseHistoryBO();
                return HistoryBO.GetLatestCaseHistoryId(CaseId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AM_CaseAndStandardLetter> GetStandardLetterByCaseHistoryId(int CaseHistoryId)
        {
            ICaseStandardLetter CaseStandardLetterBO = FactoryBO.CreateCaseStandardLetter();
            List<AM_CaseAndStandardLetter> sllist = CaseStandardLetterBO.GetCaseStandardLetterByCaseHistoryId(CaseHistoryId);
            if (sllist != null)
            {
                return LoadLetterReference(sllist);
            }
            else
            {
                return null;
            }
        }

        private List<AM_CaseAndStandardLetter> LoadLetterReference(List<AM_CaseAndStandardLetter> sllist)
        {
            foreach (AM_CaseAndStandardLetter item in sllist)
            {
                item.AM_StandardLettersReference.Load();
            }
            return sllist;
        }

        public AM_CaseAndStandardLetter GetFirstCaseStandardLetter(int CaseHistoryId, int LetterId)
        {
            ICaseStandardLetter CaseStandardLetterBO = FactoryBO.CreateCaseStandardLetter();
            AM_CaseAndStandardLetter standardLetter = new AM_CaseAndStandardLetter();
            standardLetter = CaseStandardLetterBO.GetFirstCaseStandardLetter(CaseHistoryId, LetterId);
            return standardLetter;
        }

    }
}
