﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.case_bo;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.Entities;

namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class FirstDetectionList
    {

        #region"Get First Detection List"

        public IQueryable<AM_SP_getFirstDetectionList_Result> GetFirstDetectionList(string postCode,int caseOwnedId, int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize, string sortExpression, string sortDirection,string address1,string tenancyId)
        {
            try
            {
                IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
                return firstDetecion.GetFirstDetectionList(postCode, caseOwnedId, regionId, suburbId, allRegionFlag, allSuburbFlag, surname, index, pageSize, sortExpression, sortDirection, address1, tenancyId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Previous Tenants"

        public IQueryable<AM_SP_GetPreviousTenantsList_Result> GetPreviousTenants(int caseOwnedId, int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize, string sortExpression, string sortDirection,string address1,string tenancyId)
        {
            try
            {
                IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
                return firstDetecion.GetPreviousTenants(caseOwnedId, regionId, suburbId, allRegionFlag, allSuburbFlag, surname, index, pageSize, sortExpression, sortDirection,address1,tenancyId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Customer Rent List"

        public List<AM_SP_GetCustomerRentList_Result> GetCustomerRentList()
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            return firstDetecion.GetCustomerRentList();
        }

        #endregion

        #region"Get Status Parameters"

        public AM_SP_GetStatusParameters_Result GetStatusParameters()
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            return firstDetecion.GetStatusParameters();
        }

        #endregion

        #region"Add First Detection"

        public void AddFirstDetection(int _customerId, int _tenancyId, DateTime _date, bool _isDefaulter)
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            firstDetecion.AddFirstDetection(_customerId, _tenancyId, _date, _isDefaulter);             
        }

        #endregion

        #region"Add First Detection History"
        //updated by:Umair
        //update date:28 11 2011
        public void AddFirstDetectionHistory(bool _isDefaulter)
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            firstDetecion.AddFirstDetectionHistory(_isDefaulter);
        }
        //update end
        #endregion
        
        #region"Delete First Detection"
        //updated by:Umair
        //update date:24 08 2011
        public void DeleteFirstDetection(bool _isDefaulter)
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            firstDetecion.DeleteFirstDetection(_isDefaulter);
        }
        //update end
        #endregion

        #region"Delete First Detection History"
        //updated by:Umair
        //update date:28 11 2011
        public void DeleteFirstDetectionHistory()
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            firstDetecion.DeleteFirstDetectionHistory();
        }
        //update end
        #endregion


        #region"Get First Detection Records Count"

        public int GetFirstDetectionRecordsCount(string postCode,int caseOwnedBy,int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize,string address1,string tenancyId)
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            return firstDetecion.GetFirstDetectionRecordsCount(postCode,caseOwnedBy,regionId, suburbId, allRegionFlag, allSuburbFlag, surname, index, pageSize,address1,tenancyId);
        }

        #endregion

        #region"Get Previous Tenants Records Count"

        public int GetPreviousTenantsRecordsCount(int caseOwnedBy, int regionId, int suburbId, bool allRegionFlag, bool allSuburbFlag, string surname, int index, int pageSize,string address1,string tenancyId)
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            return firstDetecion.GetPreviousTenantsRecordsCount(caseOwnedBy, regionId, suburbId, allRegionFlag, allSuburbFlag, surname, index, pageSize, address1, tenancyId);
        }

        #endregion

        #region"Get First detection List Alert Count"

        public int GetFirstDetectionListAlertCount(string postCode,int regionId, int suburbId,int resourceId)
        {
            try
            {
                IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
                return firstDetecion.GetFirstDetectionListAlterCount(postCode,regionId, suburbId,resourceId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Count First Detections"
        public int CountFirstDetections(int caseOfficerId, int regionId, int suburbId)
        {
            try
            {
                IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
                return firstDetecion.CountFirstDetections(caseOfficerId,regionId,suburbId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region"Get Latest First detection List Alert Count"

        public int GetLatestFirstDetectionListAlterCount(int regionId, int suburbId,int resourceId)
        {
            try
            {
                IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
                return firstDetecion.GetLatestFirstDetectionListAlterCount(regionId, suburbId,resourceId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        public List<AM_SP_GetRentList_Result> GetRentList(int dbIndex, int pageSize)
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            return firstDetecion.GetRentList(dbIndex, pageSize);
        }

        public int GetRentListCount()
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            return firstDetecion.GetRentListCount();
        }

        public bool CheckDefaultertenant(int customerId)
        {
            IFirstDetectionBO firstDetecion = FactoryBO.createFirstDetectionBO();
            return firstDetecion.CheckDefaulterTenant(customerId);
        }

    }
}
