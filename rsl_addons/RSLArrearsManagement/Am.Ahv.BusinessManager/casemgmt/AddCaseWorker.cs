﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.user_bo;
using Am.Ahv.BoInterface.Lookup_bo;
using Am.Ahv.BoInterface.region_suburb_bo;
using Am.Ahv.BoInterface.team_bo;
using Am.Ahv.BoInterface.resourcebo;
using Am.Ahv.BoInterface.resource_bo;
using System.Data;
using Am.Ahv.BusinessObject.base_bo;


namespace Am.Ahv.BusinessManager.Casemgmt
{
    public class AddCaseWorker
    {

        #region"Get All Regions"
        /// <summary>
        /// Returns list of all the regions
        /// edit kar k commit
        /// </summary>
        /// <returns></returns>
        public List<E_PATCH> GetAllRegion()
        {
            try
            {
                IRegionBo RegionBo = FactoryBO.CreateRegionBO();
                return RegionBo.GetAll();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion



        #region"Get All Suburbs"

        public List<P_SCHEME> GetAllSuburbs(int RegionID)
        {
            try
            {
                ISuburbBo SuburbBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateSuburbBO();
                return SuburbBo.GetSuburbByRegion(RegionID);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get All Streets"

        public List<String> GetAllStreets()
        {
            try
            {
                IStreetBo StreetBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateStreetBO();
                return StreetBo.GetAllStreets();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get  Suburbs List for Dashboard"

        public IQueryable GetSuburbsList(int ResourceId)
        {
            try
            {
                ISuburbBo SuburbBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateSuburbBO();
                return SuburbBo.GetSuburbsList(ResourceId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion
#region"Get  Suburbs List for Dashboard"

        public IQueryable GetSuburbsListByRegion(string resource, int userId)
        {
            try
            {
                ISuburbBo SuburbBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateSuburbBO();
                return SuburbBo.GetSuburbsListByRegion(resource, userId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Names"

        /// <summary>
        /// This will be called inside the webmethod of services
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public string[] GetNames(string prefix)
        {
            try
            {
                IUserBo userBO = FactoryBO.CreateUserBO();
                List<E__EMPLOYEE> Userlist = userBO.GetEmployeeList(prefix);
                if (Userlist != null && Userlist.Count > 0)
                {
                    string[] names = new string[Userlist.Count];
                    for (int i = 0; i < names.Length; i++)
                    {
                        // Start - Changes By Aamir Waheed June 6,2013
                        // To Eliminate extra spaces from names.
                        //Action: Commented out old code and write new code with trim function.
                        //names[i] = Userlist[i].FIRSTNAME + " " + Userlist[i].LASTNAME;
                        names[i] = Userlist[i].FIRSTNAME.Trim() + " " + Userlist[i].LASTNAME.Trim();
                        // End - Changes By Aamir Waheed June 6,2013
                    }
                    return names;
                }
                else
                {
                    return null;
                }
            }
            catch (NullReferenceException nullref)
            {
                throw nullref;
            }
            catch (IndexOutOfRangeException indexout)
            {
                throw indexout;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get User Types"

        public List<AM_LookupCode> GetUserTypes()
        {
            ILookupBo LookupBO = FactoryBO.CreateLookupBO();
            return LookupBO.GetLookupByName(Am.Ahv.Utilities.constants.ApplicationConstants.usertype);
        }

        #endregion

        #region"Get All Teams"

        public List<E_TEAM> GetAllTeams()
        {
            ITeamBo teambo = FactoryBO.CreateTeamBO();
            return teambo.GetAll();
        }

        #endregion

        #region"Get team Members"

        public List<E__EMPLOYEE> GetTeamMembers(int TeamID)
        {
            IUserBo UserBo = FactoryBO.CreateUserBO();
            return UserBo.GetTeamMembers(TeamID);
        }

        #endregion

        #region"Get Support Workers"

        public List<E__EMPLOYEE> GetSupportWorkers(int TeamID)
        {
            IUserBo UserBO = FactoryBO.CreateUserBO();
            return UserBO.GetSupportWorkers(TeamID);
        }

        #endregion

        #region"Get Exact Name"

        public E__EMPLOYEE GetExactName(string Name)
        {
            IUserBo UserBO = FactoryBO.CreateUserBO();
            //string firstName = Name.Trim(Name.ToCharArray());
            //Name.Trim (
            E__EMPLOYEE tempemp = UserBO.GetEmployeeByExactName(Name);

            return tempemp;
        }

        #endregion

        #region"Get Narrowed Searched"

        public E__EMPLOYEE GetNarrowSearched(string Name, int TeamID)
        {
            IUserBo UserBO = FactoryBO.CreateUserBO();
            E__EMPLOYEE tempemp = UserBO.GetUserByTeamName(Name, TeamID);
            return tempemp;
        }

        #endregion

        #region"Get Most Narrowed Search"

        public E__EMPLOYEE GetMostNarrowedSearch(string Name, int TeamID, int type)
        {
            IUserBo UserBO = FactoryBO.CreateUserBO();
            E__EMPLOYEE tempemp = UserBO.GetFullNarrowedSearch(Name, type, TeamID);
            return tempemp;
        }

        #endregion

        #region"Add Junction"

        //public AM_Resource AddJunction(AM_Resource Resource, int EmployeeID, List<int> RegionIDs, List<int> SuburbIDs, bool flag)
        public AM_Resource AddJunction(AM_Resource Resource, int EmployeeID, DataTable dt, bool flag)
        {
            try
            {
                IResourceBO ResourceBo = FactoryBO.CreateResourceBO();
                IResourcePatchDevelopment junctionBO = FactoryBO.CreateJuntcionBO();
                AM_ResourcePatchDevelopment junction;
                AM_ResourcePatchDevelopment temp;
                AM_Resource res = null;
                bool flag2 = false;

                if (flag)
                {
                    res = ResourceBo.AddNew(Resource);
                }
                else
                {
                    flag2 = ResourceBo.UpdateResource(Resource);

                }
                if (res != null && flag)
                {
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                junction = new AM_ResourcePatchDevelopment();
                                junction.ResourceId = res.ResourceId;
                                if ((dt.Rows[i]["RegionId"] != null && dt.Rows[i]["RegionId"].ToString() != "")
                                    && (dt.Rows[i]["SuburbId"] != null && dt.Rows[i]["SuburbId"].ToString() != "")
                                     && (dt.Rows[i]["Street"] != null && dt.Rows[i]["Street"].ToString() != ""))
                                {
                                    junction.SCHEMEID = int.Parse(dt.Rows[i]["SuburbId"].ToString());
                                    junction.PatchId = int.Parse(dt.Rows[i]["RegionId"].ToString());
                                    junction.Address = dt.Rows[i]["Street"].ToString();
                                }
                                else if (dt.Rows[i]["Street"] != null && dt.Rows[i]["Street"].ToString() != "")
                                {
                                    junction.Address = dt.Rows[i]["Street"].ToString();

                                }
                                else if (dt.Rows[i]["SuburbId"] != null && dt.Rows[i]["SuburbId"].ToString() != "")
                                {
                                    junction.SCHEMEID = int.Parse(dt.Rows[i]["SuburbId"].ToString());

                                }
                                else if (dt.Rows[i]["RegionId"] != null && dt.Rows[i]["RegionId"].ToString() != "")
                                {
                                    junction.PatchId = int.Parse(dt.Rows[i]["RegionId"].ToString());
                                }
                                junction.IsActive = true;
                                temp = junctionBO.AddNew(junction);
                            }
                        }
                    }
                    return res;
                }
                else
                    if (!flag && flag2)
                    {
                        deleteareas(Resource.ResourceId);
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {

                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    junction = new AM_ResourcePatchDevelopment();
                                    junction.ResourceId = Resource.ResourceId;
                                    if ((dt.Rows[i]["RegionId"] != null && dt.Rows[i]["RegionId"].ToString() != "")
                                        && (dt.Rows[i]["SuburbId"] != null && dt.Rows[i]["SuburbId"].ToString() != "")
                                        && (dt.Rows[i]["Street"] != null && dt.Rows[i]["Street"].ToString() != ""))
                                    {
                                        junction.SCHEMEID = int.Parse(dt.Rows[i]["SuburbId"].ToString());
                                        junction.PatchId = int.Parse(dt.Rows[i]["RegionId"].ToString());
                                        junction.Address = dt.Rows[i]["Street"].ToString();
                                    }
                                    else if (dt.Rows[i]["Street"] != null && dt.Rows[i]["Street"].ToString() != "")
                                    {
                                        junction.Address = dt.Rows[i]["Street"].ToString();

                                    }
                                    else if (dt.Rows[i]["SuburbId"] != null && dt.Rows[i]["SuburbId"].ToString() != "")
                                    {
                                        junction.SCHEMEID = int.Parse(dt.Rows[i]["SuburbId"].ToString());
                                    }
                                    else if (dt.Rows[i]["RegionId"] != null && dt.Rows[i]["RegionId"].ToString() != "")
                                    {

                                        junction.PatchId = int.Parse(dt.Rows[i]["RegionId"].ToString());
                                    }
                                    junction.IsActive = true;
                                    temp = junctionBO.AddNew(junction);
                                }
                            }
                        }
                        return Resource;
                    }
                    else
                    {
                        return null;
                    }
            }
            catch (EntityException entityex)
            {
                throw entityex;
            }
            catch (NullReferenceException nullref)
            {
                throw nullref;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AM_Resource AddJunction(AM_Resource Resource, int EmployeeID, List<int> RegionIDs)
        {
            IResourceBO ResourceBo = FactoryBO.CreateResourceBO();
            IResourcePatchDevelopment junctionBO = FactoryBO.CreateJuntcionBO();
            AM_ResourcePatchDevelopment junction;
            AM_ResourcePatchDevelopment temp;
            AM_Resource res = ResourceBo.AddNew(Resource);
            if (res != null)
            {
                for (int i = 0; i < RegionIDs.Count; i++)
                {
                    junction = new AM_ResourcePatchDevelopment();
                    junction.AM_Resource = res;
                    junction.PatchId = RegionIDs[i];
                    junction.IsActive = true;
                    temp = junctionBO.AddNew(junction);
                }
                return res;
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region"Get Suburb By Id"

        private P_SCHEME GetSuburbById(int suburbID)
        {
            ISuburbBo subrubo = FactoryBO.CreateSuburbBO();
            return subrubo.GetById(suburbID);
        }

        #endregion

        #region"Get region By Id"

        private E_PATCH GetRegionById(int RegionID)
        {
            IRegionBo Rbo = FactoryBO.CreateRegionBO();
            return Rbo.GetById(RegionID);
        }

        #endregion

        #region"Get Streed By Suburb Id"

        public List<String> GetStreetBySuburbId(int suburbID)
        {
            IStreetBo streetBo = FactoryBO.CreateStreetBO();
            return streetBo.GetStreetBySuburb(suburbID);
        }

        #endregion

        #region"Get Greater Count"

        private int GetGreaterCount(List<int> a, List<int> b)
        {
            if (a.Count >= b.Count)
            {
                return a.Count;
            }
            else
            {
                return b.Count;
            }
        }

        #endregion

        #region"Get Employee By Id"

        public E__EMPLOYEE GetEmployeeById(int EmployeeId)
        {
            IUserBo UserBO = FactoryBO.CreateUserBO();
            return UserBO.GetById(EmployeeId);
        }

        #endregion

        #region"Is Resource Exist"

        public bool IsResourceExist(int EmployeeId)
        {
            IResourceBO ResourceBO = FactoryBO.CreateResourceBO();
            return ResourceBO.IsResourceExist(EmployeeId);
        }

        #endregion

        #region"Get Areas"
        //For Getting Regions & Suburbs
        public List<AM_ResourcePatchDevelopment> GetAreas(int ResourceID)
        {
            IResourcePatchDevelopment ResourcePatchBO = FactoryBO.CreateJuntcionBO();
            return ResourcePatchBO.GetAreasByResourceId(ResourceID);
        }

        #endregion

        #region"Get Resource By Id"
        //GetResource
        public AM_Resource GetResourceById(int ResourceID)
        {
            IResourceBO ResourceBO = FactoryBO.CreateResourceBO();
            return ResourceBO.GetById(ResourceID);
        }

        #endregion

        #region"Get Employee Name By Employee Id"
        //GetEmployeeName
        public string GetEmployeeNameByEmployeeID(int EmployeeID)
        {
            IUserBo UserBO = FactoryBO.CreateUserBO();
            return UserBO.GetNameById(EmployeeID);
        }

        #endregion

        #region"Get Suburbs"
        //Get the Regions
        public List<P_SCHEME> GetSuburbs(List<AM_ResourcePatchDevelopment> AreasList)
        {
            ISuburbBo SuburbBO = FactoryBO.CreateSuburbBO();
            return SuburbBO.GetSuburbByIds(AreasList);
        }

        #endregion

        #region"Delete Areas"
        //Delete Areas
        private bool deleteareas(int ResourceID)
        {
            IResourcePatchDevelopment areabo = FactoryBO.CreateJuntcionBO();
            return areabo.Delete(ResourceID);
        }

        #endregion

        #region"Get Team By Employee Id"

        public E_TEAM GetTeamByEmployeeId(int empId)
        {
            try
            {
                ITeamBo teamBO = FactoryBO.CreateTeamBO();
                return teamBO.GetTeamByEmployeeId(empId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Employee Name And Team"

        public string GetEmployeeNameAndTeam(int empId)
        {
            try
            {
                IUserBo userBo = FactoryBO.CreateUserBO();
                return userBo.GetEmployeeNameAndTeam(empId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get All Suburbs"

        public DataTable GetAllSuburbs()
        {
            try
            {
                ISuburbBo suburbBo = FactoryBO.CreateSuburbBO();
                List<SchemeBo> list = suburbBo.GetAllSuburbs();

                DataTable dt = new DataTable();
                dt.Columns.Add("DEVELOPMENTID");
                dt.Columns.Add("DEVELOPMENTNAME");
                dt.Columns.Add("SCHEMENAME");
                dt.Columns.Add("SCHEMEID");
                if (list != null)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        DataRow dr = dt.NewRow();
                        dr["SCHEMENAME"] = list[i].SchemeName;
                        dr["DEVELOPMENTNAME"] = list[i].DevelopmentName;
                        dr["DEVELOPMENTID"] = list[i].DevelopmentId.ToString() + ";" + list[i].PatchId.ToString();
                        dr["SCHEMEID"] = list[i].SchemeId.ToString();
                        dt.Rows.Add(dr);
                    }
                }
                return dt;

            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        
        // changed by nadir


        #region"Get User Suburbs"

        public List<P_SCHEME> GetUserSuburbs(int RegionID, int UserId)
        {
            try
            {
                ISuburbBo SuburbBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateSuburbBO();
                return SuburbBo.GetSuburbByUser(RegionID, UserId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get User Suburbs"

        public List<P_SCHEME> GetUserSuburbs(int UserId)
        {
            try
            {
                ISuburbBo SuburbBo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateSuburbBO();
                return SuburbBo.GetSuburbByUser(UserId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region Get User Region
        public List<E_PATCH> GetUserRegion(int UserId)
        {
         try
            {

                IRegionBo RegionBo = FactoryBO.CreateRegionBO();
                List<E_PATCH> regionList = RegionBo.GetUserRegion(UserId);

                if (regionList.Count == 0)
                {
                    List<P_SCHEME> suburbsList = new List<P_SCHEME>();
                    suburbsList = GetUserSuburbs(UserId);

                    List<int?> suburbIds = new List<int?>();

                    regionList = RegionBo.GetSchemeRegions(suburbIds);
                    if (suburbsList.Count > 0)
                    {
                        for (int i = 0; i <= suburbsList.Count - 1; i++)
                        {                        
                            suburbIds.Add(suburbsList[i].SCHEMEID);
                        }

                        regionList = RegionBo.GetSchemeRegions(suburbIds);
                    }
                
                }
                
                return regionList;
            }
       
     catch (EntityException ee)
     {
        throw ee;
     }
     catch (Exception ex)
     {
        throw ex;
     }
}
    }
}

      #endregion