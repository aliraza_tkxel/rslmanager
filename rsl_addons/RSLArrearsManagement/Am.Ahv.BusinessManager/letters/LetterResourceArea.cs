﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BusinessObject.factory_bo;
using Am.Ahv.BoInterface.status_bo;
using Am.Ahv.BoInterface.actionbo;
using Am.Ahv.BoInterface.letters_bo;
using System.Data;
using Am.Ahv.BoInterface.user_bo;
using Am.Ahv.BoInterface.documents_bo;

namespace Am.Ahv.BusinessManager.letters
{
    public class LetterResourceArea
    {
        #region"Get All Status"

        public List<AM_Status> GetAllStatus()
        {
            IStatusBo statusBo = FactoryBO.CreateStatusBO();
            return statusBo.GetAll();
        }

        #endregion

        #region"Get Action By Status Id"

        public List<AM_Action> GetActionByStatusId(int StatusId)
        {
            IActionBO ActionBO = FactoryBO.CreateActionBO();
            return ActionBO.getActionByStatus(StatusId);
        }

        public IQueryable GetActionListByStatusId(int statusId)
        {
            IActionBO ActionBO = FactoryBO.CreateActionBO();
            return ActionBO.GetActionListByStatusId(statusId);
        }

        #endregion

        #region"Search Standard Letters"

        public List<AM_StandardLetters> SearchStandardLetters(int StatusId, int ActionId, String Title, String Code)
        {
            ILettersBo LettersBO = FactoryBO.CreateLettersBO();
            return LoadEmployeeNames(LettersBO.SearchStandardLetters(StatusId, ActionId, Title, Code));
        }

        #endregion

        #region"Get Personalization"

        public List<AM_LookupCode> GetPersonalization()
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetPersonalization();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Standard Letter Latest History By Letter Id"

        public AM_StandardLetterHistory GetSLetterHistoryBySLetterID(int SLetterID)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetSLetterHistoryBySLetterID(SLetterID);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion


        #region"Get Third Party Contact Details By Id"

        public AM_StandardLetters_ThirdPartyContacts  getThirdPartyContactDetails(int cId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.getThirdPartyContactDetails(cId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get GetStandard Letter By HistoryID"

        public AM_StandardLetters GetSLetterByHistoryID(int LHistoryID)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetSLetterByHistoryID(LHistoryID);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Sign Off"

        public List<AM_LookupCode> GetSignOff()
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetSignOff();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Save Letter"

        public void SaveLetter(AM_StandardLetters letter, AM_StandardLetterHistory standardLetterHistory)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                LettersBO.AddNewStandardLetter(letter, standardLetterHistory);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region"Save Third Party Contact"

        public int SaveThirdPartyContact(AM_StandardLetters_ThirdPartyContacts contact)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.AddNewThirdPartyContact(contact);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Standard Letter"

        public AM_StandardLetters GetStandardLetter(int id)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetById(id);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Standard Letter"

        public void UpdateStandardLetter(AM_StandardLetters letter, AM_StandardLetterHistory standardLetterHistory)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                LettersBO.UpdateStandardLetter(letter, standardLetterHistory);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Third Party Contact"

        public int UpdateThirdPartyContact(AM_StandardLetters_ThirdPartyContacts contact)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.UpdateThirdPartyContact(contact);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Team Members"

        public DataTable GetTeamMembers(int teamId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetTeamEmployees(teamId);
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Team Members"

        public DataTable GetAllTeamMembers(int teamId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetAllTeamMembers(teamId);
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Load Employee Names"

        private List<AM_StandardLetters> LoadEmployeeNames(List<AM_StandardLetters> SLCollection)
        {
            IUserBo UserBo = FactoryBO.CreateUserBO();
            string value;
            foreach (AM_StandardLetters item in SLCollection)
            {
                // Added By Zunair Minhas...
                value = string.Empty;
                if (item.ModifiedBy != null)
                {
                    value = UserBo.GetNameById(item.AM_Resource1.EmployeeId);
                    if (value != null)
                    {
                        item.Body = UserBo.GetNameById(item.AM_Resource1.EmployeeId);
                    }
                    else
                    {
                        item.Body = "";
                    }
                }
                else
                {
                    value = UserBo.GetNameById(item.AM_Resource.EmployeeId);
                    if (value != null)
                    {
                        item.Body = value;
                    }
                    else
                    {
                        item.Body = "";
                    }
                }
            }
            return SLCollection;

        }

        #endregion

        #region"Delete Letter"

        public bool DeleteLetter(int StandardLetterId)
        {
            ILettersBo LettersBO = FactoryBO.CreateLettersBO();
            return LettersBO.Delete(StandardLetterId);
        }

        #endregion

        #region"Get Customer Info"

        public AM_SP_GetCustomerInfoForLetter_Result GetCustomerInfo(int tenantId, int customerId, int contactId = -1)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetCustomerInfo(tenantId, customerId, contactId);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


        #region"Get Employee Name"
        public string GetEmployeeName(int? ResourceId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetEmployeeName(ResourceId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get Team Name"
        public string GetTeamName(int? TeamId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetTeamName(TeamId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get Letters By Action Id"

        public List<AM_StandardLetterHistory> GetLettersByActionId(int actionId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetLettersByActionId(actionId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetLettersByActionIdDataTable(int actionId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetLettersByActionIdDataTable(actionId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get All Letters"
        public List<AM_StandardLetters> GetAllLetters()
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetAllLetters();
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Current Market Rent
        public double GetMarketRent(int TenantID)
        {
            ILettersBo LetterBO = FactoryBO.CreateLettersBO();
            return LetterBO.GetMarketRent(TenantID);
        }
        #endregion

        #region"Get Employee Name"

        public string GetEmployeeNameForLetter(int empId)
        {
            IUserBo UserBo = FactoryBO.CreateUserBO();
            return UserBo.GetNameById(empId);
        }

        #endregion

        #region"Get Standard Letter By Activity Id"

        public List<AM_ActivityAndStandardLetters> GetStandardLetterByActivityId(int ActivityId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetStandardLetterByActivityId(ActivityId);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Latest Standard Letter History"

        public AM_StandardLetterHistory GetLatestStandardLetterHistory(int standardLetterId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetLatestStandardLetterHistory(standardLetterId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Add Edited Standard Letter"

        public int AddEditedStandardLetter(AM_StandardLetterHistory slHistory)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.AddEditedStandardLetter(slHistory);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Standard Letter History By Id"

        public AM_StandardLetterHistory GetStandardLetterHistoryById(int id)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetStandardLetterHistoryById(id);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Case Standard Letter History Id"

        public int GetCaseStandardLetterHistoryId(int stlId, int caseId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.GetCaseStandardLetterHistoryId(stlId, caseId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public string ActivityChecker(string ActionId, string StatusId, string CaseId, string CaseHistoryId)
        {
            try
            {
                ILettersBo LettersBO = FactoryBO.CreateLettersBO();
                return LettersBO.ActivityChecker(ActionId, StatusId, CaseId, CaseHistoryId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
