﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BoInterface.resourcebo;
using System.Data;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.user_bo;
using Am.Ahv.BusinessObject.factory_bo;


namespace Am.Ahv.BusinessManager.resource
{
    public class Users
    {
        #region"Get User Listings"

        public IQueryable getUserListings(int dbIndex, int pageSize)
        {
            try
            {
                IResourceBO resourceBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateResourceBO();
                var resource = resourceBO.getResourceDetails(dbIndex, pageSize);
                return resource;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Case Persons"

        public IQueryable getCasePersons(String personType)
        {
            try
            {
                IResourceBO resourceBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateResourceBO();
                var resource = resourceBO.getCasePerson(personType);
                return resource;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Records Count"

        public int getRecordsCount()
        {
            try
            {
                IResourceBO resourceBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateResourceBO();
                int count = resourceBO.getRecordsCount();
                return count;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Update Resource"

        public void updateResource(int resourceId, AM_Resource resource)
        {
            try
            {
                IResourceBO resourceBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateResourceBO();
                resourceBO.UpdateResourceType(resourceId, resource);
            }
            catch (ArgumentOutOfRangeException argumentException)
            {
                throw argumentException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Active Persons"

        public IQueryable GetActivePersons(string designation)
        {
            try
            {
                IResourceBO resourceBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateResourceBO();
                var resource = resourceBO.GetActivePersons(designation);
                return resource;
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Resource Id"

        public int GetResourceId(int userId)
        {
            try
            {
                IResourceBO resourceBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateResourceBO();
                return resourceBO.GetResourceId(userId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Resource Name"

        public string GetResourceName(int userId)
        {
            try
            {
                IResourceBO resourceBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateResourceBO();
                return resourceBO.GetResourceName(userId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Last Logged In Date"

        public string GetLastLoggedInDate(int userId)
        {
            try
            {
                IResourceBO resourceBO = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreateResourceBO();
                return resourceBO.GetLastLoggedInDate(userId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Single Employee"

        public IQueryable GetSingleEmployee(int Id)
        {
            try
            {
                IUserBo userBo = FactoryBO.CreateUserBO();
                return userBo.GetSingleEmployee(Id);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public string GetEmployeeName(int EmployeeId)
        {
            try
            {
                IUserBo userBo = FactoryBO.CreateUserBO();
                return userBo.GetNameById(EmployeeId);

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public string GetEmployeeNameByUserId(int EmployeeId)
        {
            try
            {
                IUserBo userBo = FactoryBO.CreateUserBO();
                return userBo.GetEmployeeNameByUserId(EmployeeId);

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public string getJobTitle(int employeeId)
        {
            try
            {
                IResourceBO resourceBo = FactoryBO.CreateResourceBO();
                return resourceBo.getJobTitle(employeeId);

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public string getWorkEmail(int employeeId)
        {
            try
            {
                IResourceBO resourceBo = FactoryBO.CreateResourceBO();
                return resourceBo.getWorkEmail(employeeId);

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public string getWorkDirectDial(int employeeId)
        {
            try
            {
                IResourceBO resourceBo = FactoryBO.CreateResourceBO();
                return resourceBo.getWorkDirectDial(employeeId);

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
