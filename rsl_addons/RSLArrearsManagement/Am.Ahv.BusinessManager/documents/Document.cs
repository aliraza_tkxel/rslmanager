﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.documents_bo;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;

namespace Am.Ahv.BusinessManager.documents
{
    public class Document
    {
        #region"Save Documents"

        public AM_Documents SaveDocument(AM_Documents obj)
        {
            IDocumentBo documentBo = FactoryBO.CreateDocumentBO();
            return documentBo.AddNew(obj);
        }

        #endregion

        #region"Get Document Count By Activity"

        public int GetDocumentCountByActivity(int activityId)
        {
            try
            {
                IDocumentBo documentBo = FactoryBO.CreateDocumentBO();
                return documentBo.GetDocumentCountByActivity(activityId);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public List<AM_Documents> GetDocumentsByActivityId(int ActivityId)
        {
            try
            {
                IDocumentBo documentBo = FactoryBO.CreateDocumentBO();
                return documentBo.GetDocumentsByActivityId(ActivityId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
