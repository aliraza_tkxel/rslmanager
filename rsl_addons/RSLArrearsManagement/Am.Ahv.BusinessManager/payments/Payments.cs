using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.BusinessObject.paymentbo;
using Am.Ahv.Entities;
using Am.Ahv.BoInterface.payment;
using Am.Ahv.BusinessObject.factory_bo;
using System.Data;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.utitility_classes;
using System.Linq.Expressions;
using LinqKit;
using System.Globalization;
using System.Threading.Tasks;

namespace Am.Ahv.BusinessManager.payments
{
    public class Payments
    {
        #region"Add New Payment"

        public int AddNewPayment(AM_PaymentPlan payment, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayment)
        {

            //IPaymentPlanBo paymentbo = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreatePaymentPlanBo();

            try
            {
                IPaymentPlanBo paymentbo = FactoryBO.CreatePaymentPlanBo();
                return paymentbo.AddPaymentPlan(payment, paymentPlanHistory, listPayment);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }



        //paymentbo.AddNew(payment);

        #endregion

        #region"Add Payment Plan History"

        public int AddPaymentPlanHistory(AM_PaymentPlanHistory paymentHistory)
        {
            try
            {
                IPaymentPlanBo paymentbo = FactoryBO.CreatePaymentPlanBo();
                return paymentbo.AddPaymentPlanHistory(paymentHistory);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Add Payment"

        public void AddPayment(AM_Payment payment)
        {

            //IPayment newPayment = Am.Ahv.BusinessObject.factory_bo.FactoryBO.CreatePaymentBo();

            try
            {
                IPayment newPayment = FactoryBO.CreatePaymentBo();
                newPayment.AddNew(payment);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        //newPayment.AddNew(payment);
        #endregion

        #region"Get Frequency Data"

        public IQueryable GetFrequencyData()
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.GetFrequencyData();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Payment Plan Type"

        public IQueryable GetPaymentPlanType()
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.GetPaymentPlanType();
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Weekly Rent"

        public double GetWeeklyRent(int tenantId)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.GetWeeklyRent(tenantId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Check Payment Plan"

        public bool CheckPaymentPlan(int tenantId)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.CheckPaymentPlan(tenantId);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Set Case Payment Plan"

        public bool SetCasePaymentPlan(int tenantId, int modifiedBy)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.SetCasePaymentPlan(tenantId, modifiedBy);
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Get Payment Plan Sunnary"
        public List<AM_PaymentPlan> GetPaymentPlanSummary(int tennantId)
        {
            try
            {
                IPaymentPlanBo paymentPlan = FactoryBO.CreatePaymentPlanBo();
                return paymentPlan.GetPaymentPlanSummary(tennantId);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Get Payment Plan Review"

        public DataTable GetPaymentPlanReview()
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.GetPaymentPlanReview();
            }
            catch (EntityException eex)
            {
                throw eex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Payment Plan Type"

        public string GetPaymentPlanType(int tenantId)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.GetPaymentPlanType(tenantId);
            }
            catch (EntityException eex)
            {
                throw eex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Payment Plan Monitoring"

        //public List<PaymentPlanMonitoringWrapper> GetPaymentPlanMonitoringsd(string _postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string queryRe, string sortExpression, string sortDir)
        //{
        //    try
        //    {
        //        IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
        //        List<PaymentPlanMonitoringWrapper> listWrapper = new List<PaymentPlanMonitoringWrapper>();
        //        List<AM_SP_GetPaymentPlanMonitoring_Result> listCustomers = newPayment.GetPaymentPlanMonitoring(_postCode, _assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize, queryRe, sortExpression, sortDir);
        //        if (listCustomers != null && listCustomers.Count > 0)
        //        {
        //            for (int i = 0; i < listCustomers.Count; i++)
        //            {

        //                int toDate = 0;
        //                int remaining = 0;
        //                int missed = 0;
        //                //  int counter = 0;

        //                List<AM_Payment> listPayments = newPayment.GetPaymentList(listCustomers[i].PaymentPlanId);
        //                if (listPayments == null || listPayments.Count == 0)
        //                {
        //                    continue;
        //                }
        //                else
        //                {
        //                    List<F_RENTJOURNAL> listCustomerRent = newPayment.GetCustomerRentList(listCustomers[i].TennantId, listPayments[0].Date);
        //                    if (listCustomerRent == null || listCustomerRent.Count == 0) // Customer has not submitted any payments..
        //                    {
        //                        if (DateOperations.IsPastDate(listPayments[0].Date.ToString("dd/MM/yyyy")) && DateOperations.SubtractTwoDates(DateTime.Now, listPayments[0].Date) < 0)
        //                        {
        //                            toDate += 0;
        //                            missed += 1;
        //                            remaining = listPayments.Count - (missed + toDate);

        //                            for (int innerLoop = 1; innerLoop < listPayments.Count; innerLoop++)
        //                            {
        //                                if (DateOperations.SubtractTwoDates(DateTime.Now, listPayments[innerLoop].Date) < 0)
        //                                {
        //                                    toDate += 0;
        //                                    missed += 1;
        //                                    remaining = listPayments.Count - (missed + toDate);
        //                                }
        //                                else
        //                                {
        //                                    break;
        //                                }
        //                            }
        //                        }
        //                        listWrapper.Add(SetPaymentPlanMonitoringWrapper(listCustomers[i], toDate, remaining, missed));

        //                    }
        //                    else
        //                    {
        //                        int paymentCounter = 0;
        //                        int CustomerRentListCounter = 0;

        //                        for (int j = 0; j < listPayments.Count; j++) // Calculates ToDate, Remining payments, Missed Payments
        //                        {
        //                            if (paymentCounter == listPayments.Count)
        //                            {
        //                                break;
        //                            }


        //                            {
        //                                // this block will be executed when the customers rent journal will be in past. This
        //                                // block will check if there is any missed payments in current payment schedule.
        //                                if (listCustomerRent[0].ACCOUNTTIMESTAMP.Value < listPayments[0].Date)
        //                                {
        //                                    if (DateOperations.IsPastDate(listPayments[0].Date.ToString("dd/MM/yyyy")) && DateOperations.SubtractTwoDates(DateTime.Now, listPayments[0].Date) < 0)
        //                                    {
        //                                        toDate += 0;
        //                                        //paymentCounter += 0;
        //                                        missed += 1;
        //                                        remaining = listPayments.Count - (missed + toDate);

        //                                        for (int innerLoop = 1; innerLoop < listPayments.Count; innerLoop++)
        //                                        {
        //                                            if (DateOperations.SubtractTwoDates(DateTime.Now, listPayments[innerLoop].Date) < 0)
        //                                            {
        //                                                toDate += 0;
        //                                                //paymentCounter += 0;
        //                                                missed += 1;
        //                                                remaining = listPayments.Count - (missed + toDate);
        //                                            }
        //                                            else
        //                                            {
        //                                                break;
        //                                            }
        //                                        }
        //                                        break;
        //                                    }
        //                                    else
        //                                    {
        //                                        toDate += 0;
        //                                        paymentCounter += 0;
        //                                        missed += 0;
        //                                        remaining = listPayments.Count - (missed + toDate);
        //                                        break;
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    if (DateOperations.SubtractTwoDates(DateTime.Now, listPayments[paymentCounter].Date) >= 0)
        //                                    {
        //                                        toDate += 0;
        //                                        paymentCounter += 1;
        //                                        missed += 0;
        //                                        remaining = listPayments.Count - (missed + toDate);
        //                                        break;
        //                                    }
        //                                    if (CustomerRentListCounter >= listCustomerRent.Count)
        //                                    {
        //                                        if (DateOperations.SubtractTwoDates(DateTime.Now, listPayments[paymentCounter].Date) < 0)
        //                                        {
        //                                            toDate += 0;
        //                                            paymentCounter += 1;
        //                                            missed += 1;
        //                                            remaining = listPayments.Count - (missed + toDate);
        //                                            //  break;
        //                                        }
        //                                    }

        //                                    // if the last payment in the schedule. this block will check with the grace date as well.
        //                                    if (paymentCounter == listPayments.Count || (listCustomers[i].FrequencyLookupCodeId != 87))//Daily
        //                                    {
        //                                        if (CustomerRentListCounter < listCustomerRent.Count)
        //                                        {
        //                                            if (((IsDateExistInList(listCustomerRent[CustomerRentListCounter].ACCOUNTTIMESTAMP.Value, listPayments, false)) ||
        //                                                IsDateExistInList(listCustomerRent[CustomerRentListCounter].ACCOUNTTIMESTAMP.Value, listPayments, true)) &&
        //                                            Math.Round(Convert.ToDouble(listCustomerRent[CustomerRentListCounter].AMOUNT), 2) == Math.Round(listPayments[paymentCounter].Payment + Convert.ToDouble(listCustomers[i].WeeklyRentAmount), 2)
        //                                                )
        //                                            {
        //                                                toDate += 1;
        //                                                paymentCounter += 1;
        //                                                missed += 0;
        //                                                remaining = listPayments.Count - (missed + toDate);
        //                                            }
        //                                            else
        //                                            {
        //                                                toDate += 0;
        //                                                paymentCounter += 1;
        //                                                missed += 1;
        //                                                remaining = listPayments.Count - (missed + toDate);
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (CustomerRentListCounter < listCustomerRent.Count)
        //                                        {
        //                                            if (IsDateExistInList(listCustomerRent[CustomerRentListCounter].ACCOUNTTIMESTAMP.Value, listPayments, false) &&
        //                                                Math.Round(Convert.ToDouble(listCustomerRent[CustomerRentListCounter].AMOUNT), 2) == Math.Round(listPayments[paymentCounter].Payment + Convert.ToDouble(listCustomers[i].WeeklyRentAmount), 2)
        //                                                )
        //                                            {
        //                                                toDate += 1;
        //                                                paymentCounter += 1;
        //                                                missed += 0;
        //                                                remaining = listPayments.Count - (missed + toDate);
        //                                            }
        //                                            else
        //                                            {
        //                                                toDate += 0;
        //                                                paymentCounter += 1;
        //                                                missed += 1;
        //                                                remaining = listPayments.Count - (missed + toDate);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                if (listCustomerRent.Count + 1 != CustomerRentListCounter)
        //                                {
        //                                    CustomerRentListCounter += 1;
        //                                }
        //                            }
        //                        }
        //                        listWrapper.Add(SetPaymentPlanMonitoringWrapper(listCustomers[i], toDate, remaining, missed));
        //                    }
        //                }
        //            }
        //        }
        //        return listWrapper;
        //    }
        //    catch (EntityException ee)
        //    {
        //        throw ee;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        public List<PaymentPlanMonitoringWrapper> GetPaymentPlanMonitorings(string _postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string queryRe, string sortExpression, string sortDir, bool showmissed)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();

                //List that stores the missed,todate and remaining flag against the customers
                List<PaymentPlanMonitoringWrapper> listWrapper = new List<PaymentPlanMonitoringWrapper>();

                //Get the list of customer having payment plan
                List<AM_SP_GetPaymentPlanMonitoring_Result> listCustomers = newPayment.GetPaymentPlanMonitoring(_postCode, _assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize, queryRe, sortExpression, sortDir);
                Parallel.ForEach(listCustomers, item => 
                ProcessCustomerList(showmissed,ref listWrapper, item));

                return listWrapper;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ProcessCustomerList(bool showmissed,ref List<PaymentPlanMonitoringWrapper> listWrapper, AM_SP_GetPaymentPlanMonitoring_Result listCustomers)
        {
            try
            {
               // List<PaymentPlanMonitoringWrapper> listWrapper = new List<PaymentPlanMonitoringWrapper>();
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                //loop through customer records having payment plan
                //for (int i = 0; i < listCustomers.Count; i++)
                //{

                int toDate = 0;
                int remaining = 0;
                int missed = 0;

                //get the individual customer payments less than equal than current date
                List<AM_Payment> listPayments = newPayment.GetPaymentList(listCustomers.PaymentPlanId);

                if (listPayments != null || listPayments.Count > 0)
                {

                    //get the customer rent journal records having date greater than equal to first payment date
                    List<F_RENTJOURNAL> listCustomerRent = null;

                    listCustomerRent = newPayment.GetCustomerRentLists(listCustomers.TennantId, listPayments[0].Date, DateTime.Today);


                    if (listCustomerRent == null || listCustomerRent.Count == 0) // Customer has not submitted any payments..
                    {
                        //added equal (=) to grab today date
                        if (listPayments[0].Date >= DateTime.Today)
                        {
                            //mark all the payment as remianing 
                            toDate += 0;
                            missed += 0;
                            remaining = listPayments.Count - (missed + toDate);
                        }
                        else
                        {
                            //mark all the payments less than equal to current date as missed and rest as remaining
                            for (int innerLoop = 0; innerLoop < listPayments.Count; innerLoop++)
                            {
                                //remaining future payments //added equal (=) to grab today date
                                if (listPayments[innerLoop].Date >= DateTime.Today)
                                {
                                    toDate += 0;
                                    missed += 0;
                                    remaining = listPayments.Count - (missed + toDate);
                                }
                                else //missed historic paymemts
                                {
                                    toDate += 0;
                                    missed += 1;
                                    remaining = listPayments.Count - (missed + toDate);


                                }
                            }
                        }

                        //Add the customer flag record to the payment plan report list
                        if (showmissed == true)
                        {

                            if (missed > 0)
                            {
                                listWrapper.Add(SetPaymentPlanMonitoringWrapper(listCustomers, toDate, remaining, missed));
                            }
                        }
                        else
                        {

                            listWrapper.Add(SetPaymentPlanMonitoringWrapper(listCustomers, toDate, remaining, missed));

                        }


                    }
                    else
                    {
                        List<F_RENTJOURNAL> listCustomerRentWithinPeriod;

                        for (int j = 0; j < listPayments.Count; j++) //Loop through all the payments and calculates ToDate, Remining payments, Missed Payments
                        {
                            //clearing out the object 
                            listCustomerRentWithinPeriod = null;
                            //added equal (=) to grab today date
                            if (listPayments[j].Date >= DateTime.Today)
                            {
                                break;
                            }
                            else
                            {
                                //check if its the last or only payment date
                                if (listPayments.Count - 1 == j)
                                {
                                    //get the rent journal records between the last or only payment date and current date
                                    listCustomerRentWithinPeriod = newPayment.GetCustomerRentLists(listCustomers.TennantId, listPayments[j].Date, DateTime.Today);

                                    // Customer has not submitted any payments..
                                    if (listCustomerRent == null || listCustomerRent.Count == 0)
                                    {
                                        // mark this payment as missed 

                                        toDate += 0;
                                        missed += 1;
                                        remaining = listPayments.Count - (missed + toDate);
                                    }
                                    else // if there are any payment made during the period
                                    {
                                        //get the sum of all the amount paid during the period
                                        decimal sumofamountpaid = 0;
                                        for (int k = 0; k < listCustomerRentWithinPeriod.Count; k++)
                                        {
                                            sumofamountpaid += Math.Abs(listCustomerRentWithinPeriod[k].AMOUNT.Value);
                                        }

                                        //if the amount is paid mark as paid
                                        if (listPayments[j].Payment <= Convert.ToDouble(sumofamountpaid))
                                        {
                                            toDate += 1;
                                            missed += 0;
                                            remaining = listPayments.Count - (missed + toDate);
                                        }
                                        else //else mark the amount as missed 
                                        {
                                            toDate += 0;
                                            missed += 1;
                                            remaining = listPayments.Count - (missed + toDate);
                                        }
                                    }
                                }
                                else //if the payment is not the last or only payment
                                {
                                    //get the rent journal records between the last or only payment date and current date
                                    listCustomerRentWithinPeriod = newPayment.GetCustomerRentLists(listCustomers.TennantId, listPayments[j].Date, listPayments[j + 1].Date);

                                    // Customer has not submitted any payments..
                                    if (listCustomerRent == null || listCustomerRent.Count == 0)
                                    {
                                        // mark this payment as missed 

                                        toDate += 0;
                                        missed += 1;
                                        remaining = listPayments.Count - (missed + toDate);
                                    }
                                    else // if there are any payment made during the period
                                    {
                                        //get the sum of all the amount paid during the period
                                        Decimal sumofamountpaid = 0;
                                        for (int k = 0; k < listCustomerRentWithinPeriod.Count; k++)
                                        {
                                            sumofamountpaid += Math.Abs(listCustomerRentWithinPeriod[k].AMOUNT.Value);
                                        }

                                        //if the amount is paid mark as paid
                                        if (listPayments[j].Payment <= Convert.ToDouble(sumofamountpaid))
                                        {
                                            toDate += 1;
                                            missed += 0;
                                            remaining = listPayments.Count - (missed + toDate);
                                        }
                                        else //else mark the amount as missed 
                                        {
                                            toDate += 0;
                                            missed += 1;
                                            remaining = listPayments.Count - (missed + toDate);
                                        }
                                    }
                                }
                            }
                        }

                        //Add the customer flag record to the payment plan report list


                        if (showmissed == true)
                        {

                            if (missed > 0)
                            {
                                listWrapper.Add(SetPaymentPlanMonitoringWrapper(listCustomers, toDate, remaining, missed));
                            }
                        }
                        else
                        {


                            listWrapper.Add(SetPaymentPlanMonitoringWrapper(listCustomers, toDate, remaining, missed));
                        }
                    }
                   
                }
                //listWrapperold.Add(listWrapper);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        #region"Is Date Exist in List"

        public bool IsDateExistInList(DateTime date, List<AM_Payment> list, bool isGrace)
        {
            bool tempBit = false;
            for (int tempdateLoop = 0; tempdateLoop < list.Count; tempdateLoop++)
            {
                if (isGrace)
                {
                    if (DateOperations.SubtractTwoDates(date, list[tempdateLoop].Date.AddDays(1)) == 0)
                    {
                        tempBit = true;
                        break;
                    }
                }
                else
                {
                    if (DateOperations.SubtractTwoDates(date, list[tempdateLoop].Date) == 0)
                    {
                        tempBit = true;
                        break;
                    }
                }
            }
            return tempBit;
        }

        #endregion

        #region"Set Payment Plan Monitoring Wrapper"

        public PaymentPlanMonitoringWrapper SetPaymentPlanMonitoringWrapper(AM_SP_GetPaymentPlanMonitoring_Result result, int toDate, int remainingPayments, int missed)
        {
            PaymentPlanMonitoringWrapper wrapper = new PaymentPlanMonitoringWrapper();

            wrapper.CustomerName = result.CustomerName;
            wrapper.CustomerName2 = result.CustomerName2;
            if (result.JointTenancyCount != null)
            {
                wrapper.JointTenancyCount = result.JointTenancyCount.ToString();
            }
            if (result.EndDate != null)
            {
                CultureInfo ci = new CultureInfo("en-GB");
                DateTime date = Convert.ToDateTime(result.EndDate, ci);
                wrapper.EndDate = date.ToString("dd/MM/yyyy");
                //wrapper.EndDate = result.EndDate.ToString("dd/MM/yyyy");
            }
            wrapper.Missed = missed.ToString();


            if (result.AmountToBeCollected < 0)
            {
                wrapper.PaymentPlan = result.AmountToBeCollected.ToString();
            }
            else
            {
                wrapper.PaymentPlan = result.AmountToBeCollected.ToString();
            }
            wrapper.RemainingPayments = remainingPayments.ToString();
            if (Convert.ToDouble(result.RentBalance) < 0)
            {
                wrapper.RentBalance = Convert.ToDouble(result.RentBalance).ToString();// RentBalance.CalculateRentBalance(Convert.ToDouble(result.RentBalance), Convert.ToDouble(result.WeeklyRentAmount));

            }
            else
            {
                wrapper.RentBalance = Convert.ToDouble(result.RentBalance).ToString();// RentBalance.CalculateRentBalance(Convert.ToDouble(result.RentBalance), Convert.ToDouble(result.WeeklyRentAmount));
            }
            //wrapper.RentBalance = "DB " + "�" + Convert.ToDouble(result.RentBalance).ToString();// RentBalance.CalculateRentBalance(Convert.ToDouble(result.RentBalance), Convert.ToDouble(result.WeeklyRentAmount));
            if (result.ReviewDate != null)
            {
                CultureInfo ci = new CultureInfo("en-GB");
                DateTime date = Convert.ToDateTime(result.ReviewDate, ci);
                wrapper.ReviewDate = date.ToString("dd/MM/yyyy");

                //wrapper.ReviewDate = result.ReviewDate.ToString("dd/MM/yyyy");
            }
            if (result.StartDate != null)
            {
                wrapper.StartDate = result.StartDate.Value.ToString("dd/MM/yyyy");
            }
            if (result.FirstMissedDate != null)
            {
                wrapper.FirstMissedDate = result.FirstMissedDate.Value.ToString("dd/MM/yyyy");
            }

            wrapper.TenancyId = result.TennantId.ToString();
            wrapper.ToDate = toDate.ToString();
            if (result.WeeklyRentAmount < 0)
            {

                wrapper.WeeklyRent = result.WeeklyRentAmount.ToString();
                //wrapper.WeeklyRent = ConsoleColor.Black.ToString();


            }
            else
            {
                wrapper.WeeklyRent = result.WeeklyRentAmount.ToString();
                // wrapper.WeeklyRent = ConsoleColor.Red.ToString();
            }

            if (result.CaseId != null)
            {
                wrapper.CaseId = result.CaseId.ToString();
            }

            if (result.CUSTOMERID != null)
            {
                wrapper.CustomerId = result.CUSTOMERID.ToString();
            }
            return wrapper;
        }

        #endregion

        //#region"Get Payment Plan Monitoring Count"

        //public int GetPaymentPlanMonitoringCount(string _postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int skipindex, string missedCheck, bool showmissed)
        //{
        //    try
        //    {
        //        IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
        //        List<PaymentPlanMonitoringWrapper> listWrapper = new List<PaymentPlanMonitoringWrapper>();

        //        List<AM_SP_GetPaymentPlanMonitoringCount_Result> listCustomers = newPayment.GetPaymentPlanMonitoringCount(_postCode, _assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, skipindex, missedCheck);
        //        if (listCustomers != null && listCustomers.Count > 0)
        //        {
        //            //loop through customer records having payment plan
        //            for (int i = 0; i < listCustomers.Count; i++)
        //            {

        //                int toDate = 0;
        //                int remaining = 0;
        //                int missed = 0;

        //                //get the individual customer payments less than equal than current date
        //                List<AM_Payment> listPayments = newPayment.GetPaymentList(listCustomers[i].PaymentPlanId);

        //                if (listPayments == null || listPayments.Count == 0)
        //                {
        //                    continue;
        //                }
        //                else
        //                {
        //                    //get the customer rent journal records having date greater than equal to first payment date
        //                    List<F_RENTJOURNAL> listCustomerRent = null;

        //                    listCustomerRent = newPayment.GetCustomerRentLists(listCustomers[i].TennantId, listPayments[0].Date, DateTime.Today);


        //                    if (listCustomerRent == null || listCustomerRent.Count == 0) // Customer has not submitted any payments..
        //                    {
        //                        if (listPayments[0].Date > DateTime.Today)
        //                        {
        //                            //mark all the payment as remianing 
        //                            toDate += 0;
        //                            missed += 0;
        //                            remaining = listPayments.Count - (missed + toDate);
        //                        }
        //                        else
        //                        {
        //                            //mark all the payments less than equal to current date as missed and rest as remaining
        //                            for (int innerLoop = 0; innerLoop < listPayments.Count; innerLoop++)
        //                            {
        //                                //remaining future payments 
        //                                if (listPayments[innerLoop].Date > DateTime.Today)
        //                                {
        //                                    toDate += 0;
        //                                    missed += 0;
        //                                    remaining = listPayments.Count - (missed + toDate);
        //                                }
        //                                else //missed historic paymemts
        //                                {
        //                                    toDate += 0;
        //                                    missed += 1;
        //                                    remaining = listPayments.Count - (missed + toDate);

        //                                }
        //                            }
        //                        }

        //                        //Add the customer flag record to the payment plan report list
        //                        if (showmissed == true)
        //                        {

        //                            if (missed > 0)
        //                            {
        //                                listWrapper.Add(SetPaymentPlanMonitoringWrapperCount(listCustomers[i], toDate, remaining, missed));
        //                            }
        //                        }
        //                        else
        //                        {

        //                            listWrapper.Add(SetPaymentPlanMonitoringWrapperCount(listCustomers[i], toDate, remaining, missed));
        //                        }


        //                    }
        //                    else
        //                    {
        //                        List<F_RENTJOURNAL> listCustomerRentWithinPeriod;

        //                        for (int j = 0; j < listPayments.Count; j++) //Loop through all the payments and calculates ToDate, Remining payments, Missed Payments
        //                        {
        //                            //clearing out the object 
        //                            listCustomerRentWithinPeriod = null;

        //                            if (listPayments[j].Date > DateTime.Today)
        //                            {
        //                                break;
        //                            }
        //                            else
        //                            {
        //                                //check if its the last or only payment date
        //                                if (listPayments.Count - 1 == j)
        //                                {
        //                                    //get the rent journal records between the last or only payment date and current date
        //                                    listCustomerRentWithinPeriod = newPayment.GetCustomerRentLists(listCustomers[i].TennantId, listPayments[j].Date, DateTime.Today);

        //                                    // Customer has not submitted any payments..
        //                                    if (listCustomerRent == null || listCustomerRent.Count == 0)
        //                                    {
        //                                        // mark this payment as missed 

        //                                        toDate += 0;
        //                                        missed += 1;
        //                                        remaining = listPayments.Count - (missed + toDate);
        //                                    }
        //                                    else // if there are any payment made during the period
        //                                    {
        //                                        //get the sum of all the amount paid during the period
        //                                        decimal sumofamountpaid = 0;
        //                                        for (int k = 0; k < listCustomerRentWithinPeriod.Count; k++)
        //                                        {
        //                                            sumofamountpaid += Math.Abs(listCustomerRentWithinPeriod[k].AMOUNT.Value);
        //                                        }

        //                                        //if the amount is paid mark as paid
        //                                        if (listPayments[j].Payment <= Convert.ToDouble(sumofamountpaid))
        //                                        {
        //                                            toDate += 1;
        //                                            missed += 0;
        //                                            remaining = listPayments.Count - (missed + toDate);
        //                                        }
        //                                        else //else mark the amount as missed 
        //                                        {
        //                                            toDate += 0;
        //                                            missed += 1;
        //                                            remaining = listPayments.Count - (missed + toDate);
        //                                        }
        //                                    }
        //                                }
        //                                else //if the payment is not the last or only payment
        //                                {
        //                                    //get the rent journal records between the last or only payment date and current date
        //                                    listCustomerRentWithinPeriod = newPayment.GetCustomerRentLists(listCustomers[i].TennantId, listPayments[j].Date, listPayments[j + 1].Date);

        //                                    // Customer has not submitted any payments..
        //                                    if (listCustomerRent == null || listCustomerRent.Count == 0)
        //                                    {
        //                                        // mark this payment as missed 

        //                                        toDate += 0;
        //                                        missed += 1;
        //                                        remaining = listPayments.Count - (missed + toDate);
        //                                    }
        //                                    else // if there are any payment made during the period
        //                                    {
        //                                        //get the sum of all the amount paid during the period
        //                                        Decimal sumofamountpaid = 0;
        //                                        for (int k = 0; k < listCustomerRentWithinPeriod.Count; k++)
        //                                        {
        //                                            sumofamountpaid += Math.Abs(listCustomerRentWithinPeriod[k].AMOUNT.Value);
        //                                        }

        //                                        //if the amount is paid mark as paid
        //                                        if (listPayments[j].Payment <= Convert.ToDouble(sumofamountpaid))
        //                                        {
        //                                            toDate += 1;
        //                                            missed += 0;
        //                                            remaining = listPayments.Count - (missed + toDate);
        //                                        }
        //                                        else //else mark the amount as missed 
        //                                        {
        //                                            toDate += 0;
        //                                            missed += 1;
        //                                            remaining = listPayments.Count - (missed + toDate);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        //Add the customer flag record to the payment plan report list


        //                        if (showmissed == true)
        //                        {

        //                            if (missed > 0)
        //                            {
        //                                listWrapper.Add(SetPaymentPlanMonitoringWrapperCount(listCustomers[i], toDate, remaining, missed));
        //                            }
        //                        }
        //                        else
        //                        {


        //                            listWrapper.Add(SetPaymentPlanMonitoringWrapperCount(listCustomers[i], toDate, remaining, missed));
        //                        }
        //                    }

        //                }
        //            }

        //        }

        //        return listWrapper.Count();
        //    }

        //    catch (EntityException ee)
        //    {
        //        throw ee;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}





        //  #endregion

        #region"GetPaymentPlanTotalRentBalance"

        public string GetPaymentPlanTotalRentBalance(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.GetPaymentPlanTotalRentBalance(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Update Payment Plan"

        public bool UpdatePaymentPlan(int paymentPlanId, AM_PaymentPlan paymentPlan, AM_PaymentPlanHistory paymentPlanHistory, List<AM_Payment> listPayments)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.UpdatePaymentPlan(paymentPlanId, paymentPlan, paymentPlanHistory, listPayments);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Payment Plan"

        public AM_PaymentPlan GetPaymentPlan(int tenantId)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.GetPaymentPlan(tenantId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Load Flexible Payments"

        public List<AM_Payment> LoadFlexiblePayments(int paymentPlanId)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.LoadFlexiblePayments(paymentPlanId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Get Payment Plan History"

        public AM_PaymentPlanHistory GetPaymentPlanHistory(int paymentPlanId)
        {
            try
            {
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                return newPayment.GetPaymentPlanHistory(paymentPlanId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Save Activity"
        private void AddMissedPayments(int _CustomerId, int _TenantId, int _PaymentPlanId, int _PaymentId)
        {
            TKXEL_RSLManager_UKEntities AMDBMgr = new TKXEL_RSLManager_UKEntities();

            AM_MissedPayments MissedPayments = new AM_MissedPayments();
            MissedPayments.CustomerId = _CustomerId;
            MissedPayments.TenantId = _TenantId;
            MissedPayments.PaymentId = _PaymentId;
            MissedPayments.PaymentPlanId = _PaymentPlanId;
            MissedPayments.MissedPaymentDetectionDate = DateTime.Now.Date;

            AMDBMgr.AM_MissedPayments.AddObject(MissedPayments);
            AMDBMgr.SaveChanges();


        }


        #endregion

        #region"Save Activity"
        private void AddZeroMissedPayments(int _CustomerId, int _TenantId, int _PaymentPlanId, int _PaymentId)
        {
            TKXEL_RSLManager_UKEntities AMDBMgr = new TKXEL_RSLManager_UKEntities();

            AM_MissedPaymentsZero MissedPaymentsZero = new AM_MissedPaymentsZero();
            MissedPaymentsZero.CustomerId = _CustomerId;
            MissedPaymentsZero.TenantId = _TenantId;
            MissedPaymentsZero.PaymentId = _PaymentId;
            MissedPaymentsZero.PaymentPlanId = _PaymentPlanId;
            MissedPaymentsZero.MissedPaymentDetectionDate = DateTime.Now.Date;

            AMDBMgr.AM_MissedPaymentsZero.AddObject(MissedPaymentsZero);
            AMDBMgr.SaveChanges();


        }


        #endregion

        #region"Get Latest First detection List Alert Count"

        public int GetMissedPaymentsAlertCount(string postCode, int regionId, int suburbId, int resourceId)
        {
            try
            {
                IPayment MissedPayments = FactoryBO.CreatePaymentBO();
                return MissedPayments.GetMissedPaymentsAlertCount(postCode, regionId, suburbId, resourceId);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion



        //public int CheckMissedPayment()
        //{
        //    try
        //    {
        //        int count = 0;

        //        IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();

        //        //Get the list of all customer having payment plan
        //        List<AM_SP_GetPaymentPlanMonitoring_Result> listCustomers = newPayment.GetPaymentPlanMonitoring("", -1, -1, -1, true, true, true, 0, 10000000, "", "customer.CUSTOMERID", "ASC");

        //        //check if there are no customers having payment plan
        //        if (listCustomers != null && listCustomers.Count > 0)
        //        {
        //            //loop through customer records having payment plan
        //            for (int i = 0; i < listCustomers.Count; i++)
        //            {

        //                int missed = 0;

        //                //get the individual customer payments less than equal than current date
        //                List<AM_Payment> listPayments = newPayment.GetPaymentList(listCustomers[i].PaymentPlanId);

        //                if (listPayments == null || listPayments.Count == 0)
        //                {
        //                    continue;
        //                }
        //                else
        //                {
        //                    //get the customer rent journal records having date greater than equal to first payment date
        //                    List<F_RENTJOURNAL> listCustomerRent = null;

        //                    listCustomerRent = newPayment.GetCustomerRentLists(listCustomers[i].TennantId, listPayments[0].Date, DateTime.Today);

        //                    if (listCustomerRent == null || listCustomerRent.Count == 0) // Customer has not submitted any payments..
        //                    {
        //                        if (listPayments[0].Date > DateTime.Today) // first payment date in furture 
        //                            {
        //                            //mark all the payment as remianing and not missed 
        //                            missed += 0;
        //                            }
        //                        else
        //                            {
        //                            //mark all the payments less than equal to current date as missed and rest as remaining
        //                            for (int innerLoop = 0; innerLoop < listPayments.Count; innerLoop++)
        //                                {
        //                                //remaining future payments are considered as not missed 
        //                                if (listPayments[innerLoop].Date > DateTime.Today)
        //                                    {
        //                                    missed += 0;
        //                                }
        //                                else //missed historic paymemts
        //                                {
        //                                    if (!getLatestMissedDetectionDate(listPayments[innerLoop].PaymentId)) // check if the missed payment has already been recorded
        //                    {

        //                                        if (innerLoop < listPayments.Count - 1 && listPayments[innerLoop + 1].Date <= DateTime.Today) //check if the payment is not the last payment and the next payment is not in future
        //                            {
        //                                            AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[innerLoop].PaymentId); //mark as perminent missed
        //                            }
        //                                        //else if (innerLoop == listPayments.Count - 1 && innerLoop != 0) // if its the last payment and not the only payment    
        //                            //                            {
        //                                        //}
        //                                        //else //if its the only payment
        //                            //                            {
        //                                        //}

        //                                    }

        //                                            missed += 1;

        //                                            }
        //                                        }
        //                                        }

        //                        //increase missed customer counter by 1 if there is any payment missed else increase by 0
        //                        count += (missed > 0) ? 1 : 0;
        //                                }
        //                                else
        //                                {
        //                        List<F_RENTJOURNAL> listCustomerRentWithinPeriod;
        //                        for (int j = 0; j < listPayments.Count; j++) //Loop through all the payments and calculates ToDate, Remining payments, Missed Payments
        //                            {
        //                            //clearing out the object 
        //                            listCustomerRentWithinPeriod = null;

        //                            if (listPayments[j].Date > DateTime.Today)
        //                                {
        //                                    break;
        //                                }
        //                            else
        //                                {
        //                                //check if its the last or only payment date
        //                                if (listPayments.Count - 1 == j)
        //                                    {
        //                                    //get the rent journal records between the last or only payment date and current date
        //                                    listCustomerRentWithinPeriod = newPayment.GetCustomerRentLists(listCustomers[i].TennantId, listPayments[j].Date, DateTime.Today);

        //                                    // Customer has not submitted any payments..
        //                                    if (listCustomerRent == null || listCustomerRent.Count == 0)
        //                                        {
        //                                        // mark this payment as missed 
        //                                        missed += 1;
        //                                }
        //                                    else // if there are any payment made during the period
        //                                {
        //                                        //get the sum of all the amount paid during the period
        //                                        decimal sumofamountpaid = 0;
        //                                        for (int k = 0; k < listCustomerRentWithinPeriod.Count; k++)
        //                                    {
        //                                            sumofamountpaid += Math.Abs(listCustomerRentWithinPeriod[k].AMOUNT.Value);
        //                                        }

        //                                        //if the amount is paid mark as paid and not missed
        //                                        if (listPayments[j].Payment <= Convert.ToDouble(sumofamountpaid))
        //                                        {
        //                                            missed += 0;
        //                                        }
        //                                        else //else mark the amount as missed 
        //                                        {
        //                                            missed += 1;
        //                                    }
        //                                }

        //                                            }
        //                                else //if the payment is not the last or only payment
        //                                {

        //                                    //get the rent journal records between the last or only payment date and current date
        //                                    listCustomerRentWithinPeriod = newPayment.GetCustomerRentLists(listCustomers[i].TennantId, listPayments[j].Date, listPayments[j + 1].Date);

        //                                    // Customer has not submitted any payments..
        //                                    if (listCustomerRent == null || listCustomerRent.Count == 0)
        //                                    {
        //                                        if (!getLatestMissedDetectionDate(listPayments[j].PaymentId)) // check if the missed payment has already been recorded
        //                                        {
        //                                            if (listPayments[j + 1].Date <= DateTime.Today) //check if the next payment is not in future
        //                                            {
        //                                                AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[j].PaymentId); //mark as perminent missed
        //                                        }
        //                                    }

        //                                        // mark this payment as missed 
        //                                        missed += 1;
        //                                }
        //                                    else // if there are any payment made during the period
        //                                {
        //                                        //get the sum of all the amount paid during the period
        //                                        Decimal sumofamountpaid = 0;
        //                                        for (int k = 0; k < listCustomerRentWithinPeriod.Count; k++)
        //                                    {
        //                                            sumofamountpaid += Math.Abs(listCustomerRentWithinPeriod[k].AMOUNT.Value);
        //                                        }

        //                                        //if the amount is paid mark as paid
        //                                        if (listPayments[j].Payment <= Convert.ToDouble(sumofamountpaid))
        //                                        {
        //                                            missed += 0;
        //                                        }
        //                                        else //else mark the amount as missed 
        //                                        {
        //                                            if (!getLatestMissedDetectionDate(listPayments[j].PaymentId)) // check if the missed payment has already been recorded
        //                                        {
        //                                                if (listPayments[j + 1].Date <= DateTime.Today) //check if the next payment is not in future
        //                                            {
        //                                                    AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[j].PaymentId); //mark as perminent missed
        //                                            }
        //                                        }

        //                                            missed += 1;
        //                                    }
        //                                }
        //                            }
        //                            }
        //                        }

        //                        //increase missed customer counter by 1 if there is any payment missed else increase by 0
        //                        count += (missed > 0) ? 1 : 0;
        //                    }
        //                }
        //            }
        //        }
        //        return count;
        //    }                

        //    catch (EntityException ee)
        //    {
        //        throw ee;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}




        public bool getLatestMissedDetectionDate(int newPayment)
        {
            try
            {
                IPaymentPlanBo MissedPayments = FactoryBO.CreatePaymentPlanBo();
                return MissedPayments.getLatestMissedDetectionDate(newPayment);
            }

            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }









        #region"Get Payment Plan Monitoring Count"

        public int GetPaymentPlanMonitoringCount(string _postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, string missedCheck)
        {
            try
            {
                int total = 0;
                Payments newPayment = new Payments();
                List<PaymentPlanMonitoringWrapper> Dataset = newPayment.GetPaymentPlanMonitorings(_postCode, _assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, 1, 1000000, missedCheck, "LastPaymentDate", "DESC", true).OfType<PaymentPlanMonitoringWrapper>().ToList();
                if (Dataset.Count() > 0)
                {
                    return Dataset.Count();
                }
                 //List<PaymentPlanMonitoringWrapper> Dataset=newPayment.GetPaymentPlanMonitorings(_postCode, _assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, missedCheck);
                 return total;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion


        #region"Check Missed Payments"


        // This function is responsible for checking missed payments for the schedule job.


        public int CheckMissedPayment()
        {
            try
            {
                bool delOldPaymentSuccess = false;
                int count = 0;
                IPaymentPlanBo newPayment = FactoryBO.CreatePaymentPlanBo();
                newPayment.DeleteZeorMissedPayments();
                //List<PaymentPlanMonitoringWrapper> listWrapper = new List<PaymentPlanMonitoringWrapper>();

                List<AM_SP_GetPaymentPlanMonitoring_Result> listCustomers = newPayment.GetPaymentPlanMonitoring("", -1, -1, -1, true, true, true, 0, 100000, "", "customer.CUSTOMERID", "ASC");

                if (listCustomers != null && listCustomers.Count > 0)
                {
                    for (int i = 0; i < listCustomers.Count; i++)
                    {

                        int toDate = 0;
                        int remaining = 0;
                        int missed = 0;
                        //  int counter = 0;

                        List<AM_Payment> listPayments = newPayment.GetPaymentList(listCustomers[i].PaymentPlanId);
                        if (listPayments == null || listPayments.Count == 0)
                        {
                            continue;
                        }
                        else
                        {
                            List<F_RENTJOURNAL> listCustomerRent = newPayment.GetCustomerRentList(listCustomers[i].TennantId, listPayments[0].Date);
                            if (listCustomerRent == null || listCustomerRent.Count == 0) // Customer has not submitted any payments..
                            {
                                if (DateOperations.IsPastDate(listPayments[0].Date.ToString("dd/MM/yyyy")) && DateOperations.SubtractTwoDates(DateTime.Now, listPayments[0].Date) < 0)
                                {
                                    toDate += 0;
                                    missed += 1;
                                    remaining = listPayments.Count - (missed + toDate);
                                    if (!getLatestMissedDetectionDate(listPayments[0].PaymentId))
                                    {
                                        if (missed > 0)
                                        {
                                            AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                            count++;
                                        }
                                        //else
                                        //{                                            
                                        //    AddZeroMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                        //    //count++;
                                        //}
                                    }

                                    for (int innerLoop = 1; innerLoop < listPayments.Count; innerLoop++)
                                    {
                                        if (DateOperations.SubtractTwoDates(DateTime.Now, listPayments[innerLoop].Date) < 0)
                                        {
                                            toDate += 0;
                                            missed += 1;
                                            remaining = listPayments.Count - (missed + toDate);
                                            if (!getLatestMissedDetectionDate(listPayments[innerLoop].PaymentId))
                                            {
                                                if (missed > 0)
                                                {
                                                    AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                    count++;
                                                }
                                                //else
                                                //{
                                                //    AddZeroMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                //    //count++;
                                                //}
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    AddZeroMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);

                                    //if customer has not submitted any payments but previously his tenancy was in missed payment list then delete that
                                    delOldPaymentSuccess = newPayment.DeleteOldMissedPayments(listCustomers[i].TennantId);

                                    if (delOldPaymentSuccess == false)
                                    {
                                        throw new Exception("System is unable to delete the old missed payments of tenant id (" + listCustomers[i].TennantId+")");
                                    }
                                }
                                //listWrapper.Add(SetPaymentPlanMonitoringWrapper(listCustomers[i], toDate, remaining, missed));
                            }
                            else
                            {
                                int paymentCounter = 0;
                                int CustomerRentListCounter = 0;

                                for (int j = 0; j < listPayments.Count; j++) // Calculates ToDate, Remining payments, Missed Payments
                                {
                                    if (paymentCounter == listPayments.Count)
                                    {
                                        break;
                                    }

                                    //{
                                    // this block will be executed when the customers rent journal will be in past. This
                                    // block will check if there is any missed payments in current payment schedule.
                                    if (listCustomerRent[0].ACCOUNTTIMESTAMP.Value < listPayments[0].Date)
                                    {
                                        if (DateOperations.IsPastDate(listPayments[0].Date.ToString("dd/MM/yyyy")) && DateOperations.SubtractTwoDates(DateTime.Now, listPayments[0].Date) < 0)
                                        {
                                            toDate += 0;
                                            //paymentCounter += 0;
                                            missed += 1;
                                            remaining = listPayments.Count - (missed + toDate);

                                            if (!getLatestMissedDetectionDate(listPayments[j].PaymentId))
                                            {
                                                if (missed > 0)
                                                {
                                                    AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                    count++;
                                                }
                                                //else
                                                //{
                                                //    AddZeroMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                //    //count++;
                                                //}
                                            }

                                            for (int innerLoop = 1; innerLoop < listPayments.Count; innerLoop++)
                                            {
                                                if (DateOperations.SubtractTwoDates(DateTime.Now, listPayments[innerLoop].Date) < 0)
                                                {
                                                    toDate += 0;
                                                    //paymentCounter += 0;
                                                    missed += 1;
                                                    remaining = listPayments.Count - (missed + toDate);

                                                    if (!getLatestMissedDetectionDate(listPayments[j].PaymentId))
                                                    {
                                                        if (missed > 0)
                                                        {
                                                            AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                            count++;
                                                        }
                                                        //else
                                                        //{
                                                        //    AddZeroMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                        //    //count++;
                                                        //}
                                                    }
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                        else
                                        {
                                            toDate += 0;
                                            paymentCounter += 0;
                                            missed += 0;
                                            remaining = listPayments.Count - (missed + toDate);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (DateOperations.SubtractTwoDates(DateTime.Now, listPayments[paymentCounter].Date) >= 0)
                                        {
                                            toDate += 0;
                                            paymentCounter += 1;
                                            missed += 0;
                                            remaining = listPayments.Count - (missed + toDate);
                                            break;
                                        }
                                        if (CustomerRentListCounter >= listCustomerRent.Count)
                                        {
                                            if (DateOperations.SubtractTwoDates(DateTime.Now, listPayments[paymentCounter].Date) < 0)
                                            {
                                                toDate += 0;
                                                paymentCounter += 1;
                                                missed += 1;
                                                remaining = listPayments.Count - (missed + toDate);

                                                if (!getLatestMissedDetectionDate(listPayments[j].PaymentId))
                                                {
                                                    if (missed > 0)
                                                    {
                                                        AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                        count++;
                                                    }
                                                    //else
                                                    //{
                                                    //    AddZeroMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                    //    //count++;
                                                    //}
                                                }
                                                //  break;
                                            }
                                        }

                                        // if the last payment in the schedule. this block will check with the grace date as well.
                                        if (paymentCounter == listPayments.Count || (listCustomers[i].FrequencyLookupCodeId != 87))//Daily
                                        {
                                            if (CustomerRentListCounter < listCustomerRent.Count)
                                            {
                                                if (((IsDateExistInList(listCustomerRent[CustomerRentListCounter].ACCOUNTTIMESTAMP.Value, listPayments, false)) ||
                                                    IsDateExistInList(listCustomerRent[CustomerRentListCounter].ACCOUNTTIMESTAMP.Value, listPayments, true)) &&
                                                Math.Round(Convert.ToDouble(listCustomerRent[CustomerRentListCounter].AMOUNT), 2) == Math.Round(listPayments[paymentCounter].Payment + Convert.ToDouble(listCustomers[i].WeeklyRentAmount), 2)
                                                    )
                                                {
                                                    toDate += 1;
                                                    paymentCounter += 1;
                                                    missed += 0;
                                                    remaining = listPayments.Count - (missed + toDate);
                                                }
                                                else
                                                {
                                                    toDate += 0;
                                                    paymentCounter += 1;
                                                    missed += 1;
                                                    remaining = listPayments.Count - (missed + toDate);

                                                    if (!getLatestMissedDetectionDate(listPayments[j].PaymentId))
                                                    {
                                                        if (missed > 0)
                                                        {
                                                            AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                            count++;
                                                        }
                                                        //else
                                                        //{
                                                        //    AddZeroMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                        //    //count++;
                                                        //}
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (CustomerRentListCounter < listCustomerRent.Count)
                                            {
                                                if (IsDateExistInList(listCustomerRent[CustomerRentListCounter].ACCOUNTTIMESTAMP.Value, listPayments, false) &&
                                                    Math.Round(Convert.ToDouble(listCustomerRent[CustomerRentListCounter].AMOUNT), 2) == Math.Round(listPayments[paymentCounter].Payment + Convert.ToDouble(listCustomers[i].WeeklyRentAmount), 2)
                                                    )
                                                {
                                                    toDate += 1;
                                                    paymentCounter += 1;
                                                    missed += 0;
                                                    remaining = listPayments.Count - (missed + toDate);
                                                }
                                                else
                                                {
                                                    toDate += 0;
                                                    paymentCounter += 1;
                                                    missed += 1;
                                                    remaining = listPayments.Count - (missed + toDate);
                                                    if (!getLatestMissedDetectionDate(listPayments[j].PaymentId))
                                                    {
                                                        if (missed > 0)
                                                        {
                                                            AddMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                            count++;
                                                        }
                                                        //else
                                                        //{
                                                        //    AddZeroMissedPayments((int)listCustomers[i].CUSTOMERID, (int)listCustomers[i].TennantId, (int)listCustomers[i].PaymentPlanId, listPayments[0].PaymentId);
                                                        //    //count++;
                                                        //}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (listCustomerRent.Count + 1 != CustomerRentListCounter)
                                    {
                                        CustomerRentListCounter += 1;
                                    }

                                }
                            }
                        }
                    }
                }
                return count;


            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #endregion


        #region Get Zero Missed Payment Tenancies
        public List<AM_SP_GetZeroMissedPaymentTenancies_Result> GetZeroMissedPaymentTenancies()
        {
            try
            {
                IPaymentPlanBo payments = FactoryBO.CreatePaymentPlanBo();

                List<AM_SP_GetZeroMissedPaymentTenancies_Result> missedPaymentTenancyList = payments.GetZeroMissedPaymentTenancies();
                return missedPaymentTenancyList;
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion 




    }
}
