﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BusinessManager.payments
{
    public class PaymentPlanMonitoringWrapper
    {
        string tenancyId;

        public string TenancyId
        {
            get { return tenancyId; }
            set { tenancyId = value; }
        }
        string customerName;

        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        string customerName2;
        public string CustomerName2
        {
            get { return customerName2; }
            set { customerName2 = value; }
        }

        string rentBalance;

        public string RentBalance
        {
            get { return rentBalance; }
            set { rentBalance = value; }
        }
        string weeklyRent;

        public string WeeklyRent
        {
            get { return weeklyRent; }
            set { weeklyRent = value; }
        }
        string paymentPlan;

        public string PaymentPlan
        {
            get { return paymentPlan; }
            set { paymentPlan = value; }
        }
        string startDate;

        public string StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        string endDate;

        public string EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        string toDate;

        public string ToDate
        {
            get { return toDate; }
            set { toDate = value; }
        }
        string remainingPayments;

        public string RemainingPayments
        {
            get { return remainingPayments; }
            set { remainingPayments = value; }
        }
        string missed;

        public string Missed
        {
            get { return missed; }
            set { missed = value; }
        }
        string firstMissedDate;

        public string FirstMissedDate
        {
            get { return firstMissedDate; }
            set { firstMissedDate = value; } 
        }


        string reviewDate;

        public string ReviewDate
        {
            get { return reviewDate; }
            set { reviewDate = value; }
        }
        string caseId;

        public string CaseId
        {
            get { return caseId; }
            set { caseId = value; }
        }

        string customerId;

        public string CustomerId
        {
            get { return customerId; }
            set { customerId = value; }
        }



        string jointTenancyCount;

        public string JointTenancyCount
        {
            get { return jointTenancyCount; }
            set { jointTenancyCount = value; }
        }


 

        public PaymentPlanMonitoringWrapper(string _tenancyId, string _customerName,string _customerName2, string _rentBalance, string _weeklyRent, string _paymentPlan,
                                            string _startDate, string _endDate, string _toDate, string _remainingPayments, string _missed, string _reviewDate, string _CaseId, string _CustomerId, string _JointTenancyCount,string _firstMissedDate)
        {
            TenancyId = _tenancyId;
            CustomerName = _customerName;
            CustomerName2 = _customerName2;
            RentBalance = _rentBalance;
            WeeklyRent = _weeklyRent;
            PaymentPlan = _paymentPlan;
            StartDate = _startDate;
            EndDate = _endDate;
            ToDate = _toDate;
            RemainingPayments = _remainingPayments;
            Missed = _missed;
            ReviewDate = _reviewDate;
            CaseId = _CaseId;
            CustomerId = _CustomerId;
            JointTenancyCount = _JointTenancyCount;
            FirstMissedDate = _firstMissedDate;
            
        }

        public PaymentPlanMonitoringWrapper()
        { 
        }
    }
}
