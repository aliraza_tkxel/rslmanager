﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Am.Ahv.BusinessManager.payments
{
    public class PaymentPlanWrapper
    {
        string startDate;

        public string StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        string endDate;

        public string EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }
        string frequency;

        public string Frequency
        {
            get { return frequency; }
            set { frequency = value; }
        }
        string amountToBeCollected;

        public string AmountToBeCollected
        {
            get { return amountToBeCollected; }
            set { amountToBeCollected = value; }
        }
        string weeklyRentAmount;

        public string WeeklyRentAmount
        {
            get { return weeklyRentAmount; }
            set { weeklyRentAmount = value; }
        }
        string reviewDate;

        public string ReviewDate
        {
            get { return reviewDate; }
            set { reviewDate = value; }
        }
        string firstCollectionDate;

        public string FirstCollectionDate
        {
            get { return firstCollectionDate; }
            set { firstCollectionDate = value; }
        }

        string rentBalance;

        public string RentBalance
        {
            get { return rentBalance; }
            set { rentBalance = value; }
        }

        string lastPayment;

        public string LastPayment
        {
            get { return lastPayment; }
            set { lastPayment = value; }
        }

        string lastPaymentDate;

        public string LastPaymentDate
        {
            get { return lastPaymentDate; }
            set { lastPaymentDate = value; }
        }

        string createdDate;

        public string CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        string rentPayable;

        public string RentPayable
        {
            get { return rentPayable; }
            set { rentPayable = value; }
        }

        string arrearsCollection;

        public string ArrearsCollection
        {
            get { return arrearsCollection; }
            set { arrearsCollection = value; }
        }

        string owedToBHA;

        public string OwedToBHA
        {
            get { return owedToBHA; }
            set { owedToBHA = value; }
        }

        string lastMonthNetArrears;

        public string LastMonthNetArrears
        {
            get { return lastMonthNetArrears; }
            set { lastMonthNetArrears = value; }
        }

        public PaymentPlanWrapper(string startdate, string enddate, string freq, string amounttobecollected, string wramount, string rd, string fcd, string _rentBalance, string _lastPaymet, string _lastPaymentDate, string _createdDate, string _rentPayable, string _arrearsCollection, string _OwedToBHA, string _LastMonthNetArrears)
        {
            startDate = startdate;
            endDate = enddate;
            frequency = freq;
            amountToBeCollected = amounttobecollected;
            weeklyRentAmount = wramount;
            reviewDate = rd;
            firstCollectionDate = fcd;
            rentBalance = _rentBalance;
            lastPayment = _lastPaymet;
            lastPaymentDate = _lastPaymentDate;
            createdDate = _createdDate;
            rentBalance = _rentPayable;
            arrearsCollection = _arrearsCollection;
            owedToBHA = _OwedToBHA;
            LastMonthNetArrears = _LastMonthNetArrears;

        }

        public PaymentPlanWrapper()
        {

        }

    }
}
