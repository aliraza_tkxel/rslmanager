﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Am.Ahv.Entities;
namespace Am.Ahv.BusinessManager.payments
{
  public class PaymentWrapper
    {
        string PaymentTitle;

        public string Paymenttitle
        {
            get { return PaymentTitle; }
            set { PaymentTitle = value; }
        }
        string Date;

        public string date
        {
            get { return Date; }
            set { Date = value; }
        }
        double Amount;

        public double amount
        {
            get { return Amount; }
            set { Amount = value; }
        }
        public PaymentWrapper(string title, string date, double amount)
        {
            this.Paymenttitle = title;
            this.date = date;
            this.amount = amount;
        }
    }
}
