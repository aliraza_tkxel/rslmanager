using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.resource;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Web.UI.WebControls;
using System.Linq.Expressions;


namespace Am.Ahv.Website.pagebase
{
    //public class PageBase : MSDN.SessionPage 
    public class PageBase : System.Web.UI.Page
    {
        #region"Attributes"

        private int userID;

        public int UserId
        {
            get { return userID; }
            set { userID = value; }
        }
        public PageBase()
        {                   

        }
        private string userName;

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        bool isExceptionPageBase;

        public bool IsExceptionPageBase
        {
            get { return isExceptionPageBase; }
            set { isExceptionPageBase = value; }
        }

        #endregion
       
        #region"Functions"

        #region"Getters"

        #region"Get Logged In User Id"

        public int GetLoggedInUserID()
        {
            int id = Convert.ToInt32(Session[SessionConstants.LoggedInUserId]);
            return id;
        }

        #endregion

        #region"Get Logged In UserName"

        public string GetLoggedInUserName()
        {            
            return Convert.ToString(Session[SessionConstants.LoggedInUserName]);
        }

        #endregion

        #region"Get Las Logged In Date"

        public string GetLastLoggedInDate()
        {
            return Convert.ToString(Session[SessionConstants.LastLoggedInDate]);
        }

        #endregion

        #region"Get Case Id "

        public int GetCaseId()
        {
            return Convert.ToInt32(Session[SessionConstants.CaseId]);
        }

        #endregion

        #region"Get Rent Balance"

        public double GetRentBalance()
        {
            return Convert.ToDouble(Session[SessionConstants.Rentbalance]);
        }

        #endregion

        #region"Get Tenant Id"

        public int GetTenantId()
        {
            return Convert.ToInt32(Session[SessionConstants.TenantId].ToString());
        }

        #endregion


        #region"Get Manual Addrees Data"

        public List<string> GetManualAddreesData()
        {
            List<string> manualAddData = new List<string>();
            manualAddData.Add(Session[ApplicationConstants.ManualContactName].ToString());
            manualAddData.Add(Session[ApplicationConstants.ManualAddress1].ToString());
            manualAddData.Add(Session[ApplicationConstants.ManualAddress2].ToString());
            manualAddData.Add(Session[ApplicationConstants.ManualAddress3].ToString());
            manualAddData.Add(Session[ApplicationConstants.ManualCityTown].ToString());
            manualAddData.Add(Session[ApplicationConstants.ManualPostCode].ToString());
            return manualAddData;
        }

        #endregion

        #region"Get Is Manual Address"

        public int GetIsManualAddress()
        {
            if (Session[ApplicationConstants.IsManualAddress] != null)
                return Convert.ToInt32(Session[ApplicationConstants.IsManualAddress].ToString());
            else
                return -1;
        }

        #endregion

        #region"Get Customer Contact Address"

        public int GetCustomerContactAddressID()
        {
            if (Session[ApplicationConstants.CustomerContactAdd] != null)
                return Convert.ToInt32(Session[ApplicationConstants.CustomerContactAdd].ToString());
            else
                return -1;
        }

        #endregion

        #region"Get Is Invidual Letter Req"

        public int GetIsInvidualLetterReq()
        {
            if (Session[ApplicationConstants.IsInvidualLetterReq] != null)
                return Convert.ToInt32(Session[ApplicationConstants.IsInvidualLetterReq].ToString());
            else
                return -1;
        }

        #endregion

      

        #region"Get Customer Id"

        public int GetCustomerId()
        {
            return Convert.ToInt32(Session[SessionConstants.CustomerId].ToString());
        }

        #endregion

        #region"Get Weekly Rent Amount"

        public double GetWeeklyRentAmount()
        {
            return Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]);
        }

        #endregion

        #region"Get Status Id"

        public int GetStatusId()
        {
            return Convert.ToInt32(Session[SessionConstants.StatusID]);
        }

        #endregion       


        #region"Get Action Id"
        public int GetActionId()
        {
            return Convert.ToInt32(Session[SessionConstants.ActionId]);
        }
        #endregion

        #region"Get Standard Letter Id"

        public int GetStandardLetterId()
        {
            if (Session[SessionConstants.StandardLetterIdCreateLetter] != null)
            {
                return Convert.ToInt32(Session[SessionConstants.StandardLetterIdCreateLetter]);
            }
            else
            {
                return -1;
            }
        }

        #endregion

        #endregion

        #region"Setters"

        #region"Set Manual Addrees Data"

        public void SetManualAddreesData(List<string> data)
        {
            Session[ApplicationConstants.ManualContactName] = data.ElementAt(0);
            Session[ApplicationConstants.ManualAddress1] = data.ElementAt(1);
            Session[ApplicationConstants.ManualAddress2] = data.ElementAt(2);
            Session[ApplicationConstants.ManualAddress3] = data.ElementAt(3);
            Session[ApplicationConstants.ManualCityTown] = data.ElementAt(4);
            Session[ApplicationConstants.ManualPostCode] = data.ElementAt(5);
        }

        #endregion

        #region"Set Is Manual Address"

        public void SetIsManualAddress(int val)
        {
            Session[ApplicationConstants.IsManualAddress] = val;
        }

        #endregion

        #region"Set Customer Contact Address"

        public void SetCustomerContactAddressID(int val)
        {
            Session[ApplicationConstants.CustomerContactAdd] = val;
        }

        #endregion

        #region"Set Is Invidual Letter Req"

        public void SetIsInvidualLetterReq(int val)
        {
            Session[ApplicationConstants.IsInvidualLetterReq] = val;
        }

        #endregion


        #region"Set User Id"

        public void SetUserId(int id)
        {
            UserId = id;
        }

        #endregion

        #region"Set User Session"

        public void SetUserSession()
        {
            try
            {
                Users resource = new Users();


                if (resource.GetResourceId(UserId) == -1)
                {
                    Session["resourceNull"] = -1;
                }
                else
                {
                    Session[SessionConstants.LoggedInUserId] = resource.GetResourceId(UserId);
                    Session[SessionConstants.LoggedInUserName] = resource.GetResourceName(UserId);
                    Session[SessionConstants.LastLoggedInDate] = resource.GetLastLoggedInDate(UserId);
                }

                //Users resource = new Users();
                //Session[SessionConstants.LoggedInUserId] = resource.GetResourceId(UserId);
                //Session[SessionConstants.LoggedInUserName] = resource.GetResourceName(UserId);
                //Session[SessionConstants.LastLoggedInDate] = resource.GetLastLoggedInDate(UserId);

            }
            catch (NullReferenceException nullException)
            {
                IsExceptionPageBase = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                IsExceptionPageBase = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                IsExceptionPageBase = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
                throw entityException;
            }
            catch (Exception exception)
            {
                IsExceptionPageBase = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
                throw exception;
            }

        }

        #endregion

        #region"Set Case Id"

        public void SetCaseId(int caseId)
        {
            Session[SessionConstants.CaseId] = caseId;
        }

        #endregion

        #region"Set Rent Balance"

        public void SetRentBalance(double rent)
        {
            Session[SessionConstants.Rentbalance] = rent;
        }

        #endregion

        #region"Set Tenant Id"

        public void SetTenantId(int id)
        {
            Session[SessionConstants.TenantId] = id;
        }

        #endregion


        #region"Set Customer Id"

        public void SetCustomerId(int id)
        {
            Session[SessionConstants.CustomerId] = id;
        }

        #endregion

        #region"Set Weekly Rent Amount"

        public void SetWeeklyRentAmount(double rent)
        {
            Session[SessionConstants.WeeklyRentAmount] = rent;
        }

        #endregion

        #region"Set Status Id"
        
        public void SetStatusId(int id)
        {
            Session[SessionConstants.StatusID] = id;
        }

        #endregion

        #region"Set Action Id"

        public void setActionId(int id)
        {
            Session[SessionConstants.ActionId] = id;
        }

        #endregion

        

        #region"Set Standard Letter Id Session"

        public void SetStandardLetterIdSession(int id)
        {
            Session[SessionConstants.StandardLetterIdCreateLetter] = id;
        }

        #endregion       

        #endregion

        #region"Remove Session Values"

        public void RemoveSessionValues()
        {
            Session.RemoveAll();
        }

        #endregion

        #endregion

        #region Sending Email

        public static bool SendEmail(string To, string Subject, string Message, bool SSL)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = Subject;
                mailMessage.Body = Message;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(To);

                //The SmtpClient gets configuration from Web.Config
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);


                //MailMessage mail = new MailMessage();
                //mail.Body = Message;
                //mail.Subject = Subject.ToString();
                //mail.To.Add(To);
                //string username = ConfigurationManager.AppSettings["username"].ToString();
                //string password = ConfigurationManager.AppSettings["password"].ToString();
                //string host = ConfigurationManager.AppSettings["host"].ToString();
                //int port = int.Parse(ConfigurationManager.AppSettings["port"].ToString());
                //string from = ConfigurationManager.AppSettings["EmailSender"].ToString();
                //string subjectFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                //mail.From = new MailAddress(from, subjectFrom, Encoding.UTF8);
                //NetworkCredential cred = new NetworkCredential(username, password);
                //mail.From = new MailAddress(from, subjectFrom, Encoding.UTF8);
                //SmtpClient client = new SmtpClient(host, port);
                //mail.IsBodyHtml = true;
                //client.EnableSsl = SSL;
                //client.UseDefaultCredentials = false;
                //client.Credentials = cred;
                //client.Send(mail);
                return true;
            }
            catch (FormatException formatexception)
            {
                throw formatexception;
            }
                catch(SmtpException smtpexception)
            {
                    throw smtpexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        /// <summary>
        /// send Email Notification to Administrator When
        /// First Detection Shedule job Execute.
        /// </summary>
        /// <param name="count">How many Records Detected</param>
        /// <param name="success">either successfully execute or not</param>
        /// <returns>bool (true or false) - email sent or not</returns>

        #region"Send Email Notification"

        public static bool SendEmailNotification(int count, bool success, string emailSubject, string emailBody)
        {
            try
            {
                bool isSent = false;
                if (success)
                {                    
                    string emailTo = ConfigurationManager.AppSettings["AdministratorEmailAddress"].ToString();
                    while (true)
                    {
                        if (SendEmail(emailTo, emailSubject, emailBody, true))
                        {
                            isSent = true;
                            break;
                        }
                    }
                    return isSent;
                }
                else
                {
                    string emailTo = ConfigurationManager.AppSettings["AdministratorEmailAddress"].ToString();
                    while (true)
                    {
                        if (SendEmail(emailTo,emailSubject ,emailBody, true))
                        {
                            isSent = true;
                            break;
                        }
                    }
                    return isSent;
                }
            }
            catch (Exception ex)
            {
                throw ex;                
            }

        }

        #endregion

        #region"Check Session"

        public bool CheckSession()
        {
           // Session[SessionConstants.LoggedInUserId] = "760";
            if (Session[SessionConstants.LoggedInUserId] == null )
            {
                return false;
            }
            else if (Convert.ToString(Session[SessionConstants.LoggedInUserId]).Equals(string.Empty))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region"Remove Create Letter Cache"

        public void RemoveCreateLetterCache()
        {
            Session.Remove(CacheConstants.StatusCreateLetter);
            Session.Remove(CacheConstants.ActionCreateLetter);
            Session.Remove(CacheConstants.TitleCreateLetter);
            Session.Remove(CacheConstants.CodeCreateLetter);
            Session.Remove(CacheConstants.PersonalizationCreateLetter);
            Session.Remove(ApplicationConstants.Letter);
            Session.Remove(ApplicationConstants.From);
            Session.Remove(ApplicationConstants.SignOff);
            Session.Remove(ApplicationConstants.Team);

        }

        #endregion

        #region Remove LetterResrouceArea Cache
        public void ClearCachedObject()
        {
            try
            {
                Session.Remove(CacheConstants.ActionId);
                Session.Remove(CacheConstants.Code);
                Session.Remove(CacheConstants.SearchValue);
                Session.Remove(CacheConstants.StatusId);
                Session.Remove(CacheConstants.Title);


            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion

        #region"Remove Payment Plan Session"

        public void RemovePaymentPlanSession()
        {
            Session.Remove(SessionConstants.UpdateFlagPaymentPlan);
            Session.Remove(SessionConstants.CreateDatePaymentPlan);
            Session.Remove(SessionConstants.paymentPlanId);
            Session.Remove(SessionConstants.PaymentPlanTypePaymentPlan);
            //Session.Remove(SessionConstants.OwedTBHA);
            //Session.Remove(SessionConstants.WeeklyRentAmount);
            //Session.Remove(SessionConstants.Rentbalance);
            //Session.Remove(SessionConstants.HBCyclePayment);
        }

        #endregion

        #region SortingGrid
        public static IEnumerable<T> SortGrid<T>(IEnumerable<T> dataSource, GridViewSortEventArgs e)
        {
            
            // Save current page index
            //int pageIndex = base.PageIndex;

            // The magic
            var prm = Expression.Parameter(typeof(T), "root");
            
            IEnumerable<T> DataSource = new Sorter<T>().Sort(dataSource, prm, e.SortExpression, e.SortDirection);
            e.Cancel = true;
            return DataSource;
        }
        #endregion
    }
}
