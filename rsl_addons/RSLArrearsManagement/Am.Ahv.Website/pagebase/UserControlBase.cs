﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Am.Ahv.Website.pagebase
{
    public class UserControlBase : System.Web.UI.UserControl
    {
        #region"Attributes"        

        PageBase pageBase = null;

        #endregion

        #region"Functions"

        #region"Getters"

        #region"Get Current Logged In User"

        public int GetCurrentLoggedinUser()
        {
            pageBase = new PageBase();
            return pageBase.GetLoggedInUserID();
        }

        #endregion

        #region"Get Current Logged In User Name"

        public string GetCurrentLoggedInUserName()
        {
            pageBase = new PageBase();
            return pageBase.GetLoggedInUserName();
        }

        #endregion

        #region"Get Case Id"

        public int GetCaseId()
        {
            pageBase = new PageBase();
            return pageBase.GetCaseId();
        }

        #endregion

        #region"Get Rent Balance"

        public double GetRentBalance()
        {
            pageBase = new PageBase();
            return pageBase.GetRentBalance();
        }

        #endregion

        #region"Get Tenant Id"

        public int GetTenantId()
        {
            pageBase = new PageBase();
            return pageBase.GetTenantId();
        }

        #endregion

        #region"Get Weekly Rent Amount"

        public double GetWeeklyRentAmount()
        {
            pageBase = new PageBase();
            return pageBase.GetWeeklyRentAmount();
        }

        #endregion

        #region"Get Status Id"

        public int GetStatusId()
        {
            pageBase = new PageBase();
            return pageBase.GetStatusId();
        }

        #endregion

        #endregion

        #region"Setters"

        #region"Set Case Id"

        public void SetCaseId(int caseId)
        {
            pageBase = new PageBase();
            pageBase.SetCaseId(caseId);
        }

        #endregion

        #region"Set Rent Balance"

        public void SetRentBalance(double rent)
        {
            pageBase = new PageBase();
            pageBase.SetRentBalance(rent);
        }

        #endregion

        #region"Set Tenant Id"

        public void SetTenantId(int id)
        {
            pageBase = new PageBase();
            pageBase.SetTenantId(id);
        }

        #endregion

        #region"Set Weekly Rent Amount"

        public void SetWeeklyRentAmount(double rent)
        {
            pageBase = new PageBase();
            pageBase.SetWeeklyRentAmount(rent);
        }

        #endregion

        #region"Set Status Id"

        public void SetStatusId(int id)
        {
            pageBase = new PageBase();
            pageBase.SetStatusId(id);
        }

        #endregion

        #endregion

        #endregion

        #region Remove LetterResrouceArea Cache
        public void ClearCachedObject()
        {
            try
            {
                Session.Remove(CacheConstants.ActionId);
                Session.Remove(CacheConstants.Code);
                Session.Remove(CacheConstants.SearchValue);
                Session.Remove(CacheConstants.StatusId);
                Session.Remove(CacheConstants.Title);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion

        #region"Remove Create Letter Cache"

        public void RemoveCreateLetterCache()
        {
            PageBase page = new PageBase();
            page.RemoveCreateLetterCache();
        }

        #endregion

    }
}