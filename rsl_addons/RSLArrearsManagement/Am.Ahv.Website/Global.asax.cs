﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Am.Ahv.Utilities.constants;

namespace Am.Ahv.Website
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }
        void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication app = sender as HttpApplication;
            HttpContext context = app.Context;
            HttpWorkerRequest workerRequest = ((IServiceProvider)context).GetService(typeof(HttpWorkerRequest)) as HttpWorkerRequest;
            // Bug fix for MS SSRS Blank.gif 500 server error missing parameter IterationId
            // https://connect.microsoft.com/VisualStudio/feedback/details/556989/
            HttpRequest req = HttpContext.Current.Request;
            if (req.Url.PathAndQuery.StartsWith("/Reserved.ReportViewerWebControl.axd") &&
                !req.Url.ToString().ToLower().Contains("iteration") &&
                !String.IsNullOrEmpty(req.QueryString["ResourceStreamID"]) &&
                req.QueryString["ResourceStreamID"].ToLower().Equals("blank.gif"))
            {
                Context.RewritePath(String.Concat(req.Url.PathAndQuery, "&IterationId=0"));
            }
            if (context.Request.ContentType.IndexOf("multipart/form-data") == -1)
            {
                // Not multi-part form
                return;
            }
            if (workerRequest.HasEntityBody())
            {
                //int length = int.Parse(workerRequest.GetKnownRequestHeader(HttpWorkerRequest.HeaderContentLength));
                //if (length > 2097152)// if the legth is too large
                //{
                //    Response.Redirect(PathConstants.dashboardPath, false);
                //    return;
                //}
            }

          
            
        }
        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
