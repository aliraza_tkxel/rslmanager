﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Am.Ahv.Website.runjobs
{
    public partial class ListJobs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> filesPath = new List<string>();
            string curUrl = Request.Url.ToString();
            string domainName = ExtractDomainNameFromUrl(curUrl);
            
            filesPath.Add(domainName + "secure/casemgt/GetCustomerRentParameters.aspx");
            filesPath.Add(domainName + "secure/casemgt/FindFirstDetectionCustomers.aspx");
            filesPath.Add(domainName + "secure/reports/FindHistoricalBalanceList.aspx");
            filesPath.Add(domainName + "secure/casemgt/FindMissedPayments.aspx");
            filesPath.Add(domainName + "secure/casemgt/FindOverDueActionCases.aspx");
            filesPath.Add(domainName + "secure/casemgt/PaymentPlanReviewNotification.aspx");
            filesPath.Add(domainName + "secure/dashboard/NegativeRentBalance.aspx");
            

            GridView1.DataSource = filesPath;
            GridView1.DataBind();
        }

        public static string ExtractDomainNameFromUrl(string Url)
        {
            if (!Url.Contains("://"))
                Url = "http://" + Url;

            return new Uri(Url).Host + ":" + new Uri(Url).Port  + "/";
        }
    }
}