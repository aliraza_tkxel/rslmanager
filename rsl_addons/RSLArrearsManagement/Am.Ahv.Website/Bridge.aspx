﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bridge.aspx.cs" Inherits="Am.Ahv.Website.Bridge" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style/css/default.css" rel="stylesheet" type="text/css" />
</head>

<body>
   
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:UpdatePanel runat="server" ID="updPnlMessagePanel" UpdateMode="Conditional">
            <ContentTemplate>
             <asp:Label ID="lblmessage" runat="server" Text=""></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
       
    </div>
    </form>
</body>
</html>
