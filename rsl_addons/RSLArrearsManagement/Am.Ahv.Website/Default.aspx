﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Am.Ahv.Website.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <style type="text/css">
        .number-loading
        {
            width: 100%;
            height: auto;
            text-align: center;
            margin-top: 0px;
        }
        .Loding-Image-Dashboard
        {
            width: 50px;
            height: 50px;
        }
    </style>
    <script type="text/javascript">

        $(window).load(function () {
            loadAlerts();

        });
        function loadAlerts() {

            loadCounts("getAnticipatedHBBalanceBalance", '#loadingAnticipated', '#<%= lblAnticipated.ClientID %>')
            loadCounts("getCountMyCasesApi", '#loadingMyCases', '#<%= lblMyCasesCount.ClientID %>')
            loadCounts("getCountFirstDetection", '#loadingFirstDetection', '#<%= lblFirstDetection.ClientID %>')
            loadCounts("getCountOversueStages", '#loadingStagesOverdue', '#<%= lblStagesOverdue.ClientID %>')
            loadCounts("getCountMissedPayments", '#loadingMissedPayment', '#<%= lblMissedPayment.ClientID %>')
            loadCounts("getCountNISPDueToExpire", '#loadingNisp', '#<%= lblNisp.ClientID %>')
            loadCounts("getCountOversueActions", '#loadingOverdueAction', '#<%= lblOverdueAction.ClientID %>')
        }

        function loadCounts(url, loading, label) {
            $(label).hide();
            var postData = JSON.stringify({
            });
            $.ajax({
                type: "POST",
                url: "Default.aspx/" + url,
                data: postData,
                contentType: "application/json",
                dataType: "json",
                beforeSend: function (xhr) {
                    $(loading).show();
                },
                complete: function () {
                    $(loading).hide();
                },

                success: function (result) {
                    $(label).text(result.d);
                    if (url === "getAnticipatedHBBalanceBalance") {
                        loadCounts("getRentNetBalance", '#loadingRentNetBalance', '#<%= lblRentNetBalance.ClientID %>')
                        loadCounts("getRentGrossBalance", '#loadingRentGrossBalance', '#<%= lblRentGrossBalance.ClientID %>')
                    }
                    $(label).show();
                }
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lblCaseOfficer" runat="server" Visible="false"></asp:Label>
    <div style="width: 402px; float: left;">
        <div style="border: 1px solid #000; padding: 10px; width: 360px; margin: 0 0 5px 5px;">
            <%-- <uc5:dashboradsearch id="DashboardSearch" runat="server"></uc5:dashboradsearch>--%>
            <asp:UpdatePanel ID="updpnlDashBoardSearch" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <table border="0" cellpadding="0" style="border-collapse: collapse" width="380">
                        <tr>
                            <td height="7" class="dashboardSearchBoxCell" colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td height="30" class="dashboardSearchBoxCell">
                                Postcode:
                            </td>
                            <td height="30" class="dashboardSearchBoxCell">
                                <asp:TextBox ID="txtPostCode" runat="server" Width="195px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="30" class="dashboardSearchBoxCell">
                                Cases owned by:
                            </td>
                            <td height="30" class="dashboardSearchBoxCell">
                                <asp:DropDownList ID="ddlCaseOwnedBy" runat="server" Width="200px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlCaseOwnedBy_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td height="30" class="dashboardSearchBoxCell">
                                Cases within:
                            </td>
                            <td height="30" class="dashboardSearchBoxCell">
                                <asp:DropDownList ID="ddlCaseRegion" runat="server" Width="200px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlCaseRegion_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td height="30" class="dashboardSearchBoxCell">
                                &nbsp;
                            </td>
                            <td height="30" class="dashboardSearchBoxCell">
                                <asp:DropDownList ID="ddlCaseSuburb" runat="server" Width="200px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td height="30" class="dashboardSearchBoxCell">
                                &nbsp;
                            </td>
                            <td height="30" class="dashboardSearchBoxCell">
                                <p style="margin-left: 5px; margin-right: 7px">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                        CssClass="dashboradSearchBoxButton" onclick="btnSearch_Click" />
                                    <%--<input type="submit" value="Search" name="B1" class="dashboradSearchBoxButton" />--%>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_03.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading"><a href="secure/casemgt/FirstDetectionList.aspx?cmd=FirstDetectionAlert">
                        <span style="text-align: center">Initial Case 
                            <br />
                            Monitoring
                            <br />
                            <asp:Label ID="lblFirstDetection" runat="server" CssClass="dashboardBigNumbersUP"></asp:Label>
                        </span></a></span>
                    <div class="number-loading" id="loadingFirstDetection">
                        <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                    </div>
                </div>
            </div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_04.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading"><a href="secure/casemgt/CaseList.aspx?cmd=overdueStages">
                        <span style="text-align: center">Overdue
                            <br />
                            Stages
                            <br />
                            <asp:Label ID="lblStagesOverdue" runat="server" CssClass="dashboardBigNumbersUP"></asp:Label>
                            <%--<uc1:overduevcate id="overduevacate" runat="server"></uc1:overduevcate>--%>
                        </span></a></span>
                        <div class="number-loading" id="loadingStagesOverdue">
                        <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                    </div>
                </div>
            </div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_03.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading"><a href="secure/reports/PaymentPlanMonitoring.aspx?cmd=MissedPayments">
                        <span style="text-align: center">Missed
                            <br />
                            Payments
                           <br />
                            <asp:Label ID="lblMissedPayment" runat="server" CssClass="dashboardBigNumbersUP"></asp:Label>
                        </span></a></span>
                         <div class="number-loading" id="loadingMissedPayment">
                        <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                    </div>
                </div>
            </div>
            <div style="clear: both;">
            </div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_03.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading"><a href="secure/casemgt/CaseList.aspx?cmd=nisp">
                        <span style="text-align: center">NISPs Due to
                            <br />
                            Expire
                            <br />
                            <asp:Label ID="lblNisp" runat="server" CssClass="dashboardBigNumbersUP"></asp:Label>
                            <%--<uc7:evacate id="evacatdash" runat="server"></uc7:evacate>--%>
                        </span></a></span>
                         <div class="number-loading" id="loadingNisp">
                        <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                    </div>
                </div>
            </div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_03.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading"><a href="secure/casemgt/CaseList.aspx?cmd=overdueActions">
                        <span style="text-align: center">Overdue
                            <br />
                            Actions
                           <br />
                            <asp:Label ID="lblOverdueAction" runat="server" CssClass="dashboardBigNumbersUP"></asp:Label>
                        </span></a></span>
                         <div class="number-loading" id="loadingOverdueAction">
                        <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                    </div>
                </div>
            </div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_03.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading">
                        <a href="secure/casemgt/CaseList.aspx?cmd=mycases"><span style="text-align: center">
                            My
                            <br />
                            Cases
                            <br />
                            <asp:Label ID="lblMyCasesCount" runat="server" CssClass="dashboardBigNumbersUP"></asp:Label>
                        </span></a></span>
                        <div class="number-loading" id="loadingMyCases">
                            <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                        </div>
                   
                </div>
            </div>
            <div style="clear: both;">
            </div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_03.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading"><a href="secure/reports/BalanceListReport.aspx">
                        <span style="text-align: center">Rent Balance<br />
                            (Gross)<br />
                            &nbsp;
                           <br />
                            <asp:Label ID="lblRentGrossBalance" runat="server" CssClass="dashboardsmallBalance"></asp:Label>
                        </span></a></span>
                         <div class="number-loading" id="loadingRentGrossBalance">
                        <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                    </div>
                </div>
            </div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_03.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading"><a href="secure/reports/BalanceListReport.aspx">
                        <span style="text-align: center">Anticipated<br />
                            HB<br />
                            &nbsp;
                            <br />
                            <asp:Label ID="lblAnticipated" runat="server" CssClass="dashboardsmallBalance"></asp:Label>
                            <%--  <uc15:anticipatedhbbalance id="anticipatedHBBalance" runat="server" />--%>
                        </span></a></span>
                         <div class="number-loading" id="loadingAnticipated">
                        <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                    </div>
                </div>
            </div>
            <div class="DashBoard_Alert_Single_Div">
                <img border="0" src="style/images/dashboard_03.jpg" alt="" width="132" height="130" />
                <div class="Dashboard_Alert_Single_Div_Text">
                    <span class="dashboardBigNumbersHeading"><a href="secure/reports/BalanceListReport.aspx">
                        <span style="text-align: center">Rent Balance<br />
                            (Net)<br />
                            &nbsp;
                            <br />
                            <asp:Label ID="lblRentNetBalance" runat="server" CssClass="dashboardsmallBalance"></asp:Label>
                           
                        </span></a></span>
                         <div class="number-loading" id="loadingRentNetBalance">
                        <img alt="Please Wait" src="Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div style="clear: left;">
            </div>
            <div style="padding: 5px 0 0 35px; font-size: 11px; color: #797979;">
                <asp:Label ID="lblVersion" runat="server" Text="Reidmark © 2014 : Arrears Management Dashboard v20.5_01.04.2014"></asp:Label>
            </div>
        </div>
    </div>
    <div style="clear: both;">
    </div>
</asp:Content>
