﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Am.Ahv.Website
{
    public partial class Ping : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["KeepSessionAlive"] = DateTime.Now.ToString();
        }
    }
}