﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Threading;

namespace Am.Ahv.Website
{
    public partial class Bridge : MSDN.SessionPage
    //public partial class Bridge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // ASPSession["USERID"] = "760";

            bool isException = false;
            if (ASPSession["USERID"] != null)
            {
                try
                {
                    if (Request.QueryString["cmd"] != null)
                    {
                        if (Request.QueryString["cmd"] == "rsl")
                        {
                            //int UId = 616;
                            Am.Ahv.BusinessManager.Casemgmt.CaseHistory caseHistoryManager = new BusinessManager.Casemgmt.CaseHistory();
                            int UId = int.Parse(ASPSession["USERID"].ToString());
                            PageBase pageBase = new PageBase();
                            pageBase.SetUserId(UId);
                            pageBase.SetUserSession();
                            //check if resource is 0 or null
                            if (Convert.ToInt16(Session["resourceNull"]) == -1)
                            {
                                this.lblmessage.Text = UserMessageConstants.InvalidUserMessage ;
                                this.updPnlMessagePanel.Update();
                            }
                            else
                            {
                                pageBase.SetTenantId(int.Parse(Request.QueryString["tenid"]));
                                pageBase.SetCaseId(int.Parse(Request.QueryString["caseid"]));
                                Session[SessionConstants.CustomerId] = int.Parse(Request.QueryString["customerid"]);
                                //updated by umair
                                //start update
                                //Response.Redirect(PathConstants.casedetails + "?CaseHistoryId=" + caseHistoryManager.GetLatestCaseHistoryId(int.Parse(Request.QueryString["caseid"])).ToString());
                                Session[SessionConstants.PostCodeSearch] = "";
                                if (Request.QueryString["casetype"] == "prev")
                                {
                                    Response.Redirect(PathConstants.caseDetailsReportPath + "?casetype=inactive");
                                }
                                else
                                {
                                    Response.Redirect(PathConstants.casehistory + "?CaseHistoryId=" + caseHistoryManager.GetLatestCaseHistoryId(int.Parse(Request.QueryString["caseid"])).ToString());
                                }

                                //end update
                            }
                        }
                    }
                    else
                    {
                        //int UserId =616;
                        int UserId = int.Parse(ASPSession["USERID"].ToString());
                        PageBase pageBase = new PageBase();
                        pageBase.SetUserId(UserId);
                        pageBase.SetUserSession();
                        //check if resource is 0 or null
                        if (Convert.ToInt16(Session["resourceNull"]) == -1)
                        {
                            this.lblmessage.Text = UserMessageConstants.InvalidUserMessage ; 
                            this.updPnlMessagePanel.Update();
                        }
                        else
                        {
                            Response.Redirect(PathConstants.dashboardPath);
                        }
                    }
                }
                catch (ThreadAbortException ex)
                { 
                
                }
                catch (Exception ex)
                {
                    this.lblmessage.Text = ex.Message ;
                    isException = true;
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
                finally
                {
                    if (isException == true)
                    {                         
                        this.updPnlMessagePanel.Update();
                    }
                }
            }
            else
            {
                Response.Redirect(PathConstants.LoginPath);
            }
        }
    }
}