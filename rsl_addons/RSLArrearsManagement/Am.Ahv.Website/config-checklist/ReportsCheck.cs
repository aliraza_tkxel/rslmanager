﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Am.Ahv.Website.config_checklist
{
    public class ReportsCheck : Checklist, IVerification
    {
        Collection<String> reportsFiles = new Collection<String>();
        GridView grdFiles = new GridView();

        public ReportsCheck()
        {
            Page page = (Page)HttpContext.Current.Handler;
            grdFiles = (GridView)page.FindControl("grdFiles");
            UpdatePanelFiles = (UpdatePanel)page.FindControl("UpdatePanelFiles");   
        }
        public void checkStatus()
        {
            checkReports();
        }

        public void checkReports()
        {                    
            populateFiles();
            DataTable dt = new DataTable(ConfigChecklistConstants.FILES_TABLES);
            DataRow dr;

            dt.Columns.Add(new DataColumn(ConfigChecklistConstants.REPORTS_FILE_PATH, typeof(string)));
            dt.Columns.Add(new DataColumn(ConfigChecklistConstants.FILE_EXIST_OR_NOT, typeof(string)));
            dt.Columns.Add(new DataColumn(ConfigChecklistConstants.REPORT_MESSAGE, typeof(string)));



            foreach (String filePath in reportsFiles)
            {
                dr = dt.NewRow();
                try
                {
                    string curFile = filePath;
                    dr[0] = filePath;

                    string message = "";

                    dr[1] = message;
                    if (File.Exists(curFile))
                    {
                        message = ConfigChecklistConstants.FILE_EXISTS_MSG;
                        dr[1] = message;

                        FileInfo fInfo = new FileInfo(curFile);
                        long fileSize = fInfo.Length;

                        if (fileSize <= 1024)
                        {
                            dr[2] = ConfigChecklistConstants.FILE_SIZE_SMALL_MSG + Server.MapPath("~") + ConfigChecklistConstants.ROOT_TO_REPORTS_PATH;
                        }
                    }
                    else
                    {
                        message = ConfigChecklistConstants.FILE_DOES_NOT_EXIST_SHORT_MSG;
                        dr[1] = message;
                        dr[2] = ConfigChecklistConstants.FILE_DOESNOT_EXIST_MSG + Server.MapPath("~") + ConfigChecklistConstants.ROOT_TO_REPORTS_PATH;
                    }
                }
                catch (Exception ex)
                {
                    dr[1] = ConfigChecklistConstants.FILE_NOT_FOUND_MSG;
                    dr[2] = ConfigChecklistConstants.FILE_EXCEPTION_MSG + Server.MapPath("~") + ConfigChecklistConstants.ROOT_TO_REPORTS_PATH;
                }

                dt.Rows.Add(dr);
            }

            DataView dv = new DataView(dt);


            grdFiles.DataSource = dv;
            grdFiles.DataBind();
            UpdatePanelFiles.Update();

        }

        private void populateFiles()
        {
            string serverPath = getServerPath();
            string fromRoot2ReportPath = getFromRoot2ReportPath();

            //reportsFiles.Add(serverPath + fromRoot2ReportPath + "abcd.rdlc");
            //reportsFiles.Add(serverPath + fromRoot2ReportPath + "abc.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "BalanceList.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "CustomerSummary.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "CustomerSummery.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "HistoricalBalanceList.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "ParentReport.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "PaymentPlanSummary.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "PaymentSchedule.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "PaymentScheduleReport.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "RegularPaymentReport.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "RegularPaymentSummaryReport.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "ViewActivities.rdlc");
            reportsFiles.Add(serverPath + fromRoot2ReportPath + "ViewActivitiesPrintOut.rdlc");
        }

        private string getServerPath()
        {
            return Server.MapPath("~");
        }

        private string getFromRoot2ReportPath()
        {
            return ConfigChecklistConstants.ROOT_TO_REPORTS_PATH;
        }
    }
}