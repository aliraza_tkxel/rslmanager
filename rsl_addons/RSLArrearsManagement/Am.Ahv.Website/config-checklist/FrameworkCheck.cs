﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Am.Ahv.Website.config_checklist;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Am.Ahv.Website
{
    public class FrameworkCheck : Checklist, IVerification
    {        
        Collection<Version> ver = InstalledDotNetVersions();        
        
        Label lblFrameworkMsg = new Label();
        UpdatePanel UpdatePanelFrameworkMsg = new UpdatePanel();
        Image imgFramework = new Image();
       
        public FrameworkCheck()
        {
            Page page = (Page)HttpContext.Current.Handler;
            lblFrameworkMsg = (Label)page.FindControl("lblFrameworkMsg");
            UpdatePanelFrameworkMsg = (UpdatePanel)page.FindControl("UpdatePanelFrameworkMsg");
            UpdatePanelFrameworkImg = (UpdatePanel)page.FindControl("UpdatePanelFrameworkImg");
            imgFramework = (Image)page.FindControl("imgFramework");
        }
        public void checkStatus()
        {
            checkFrameworkVersion();
            
        }

        private void checkFrameworkVersion()
        {            
            try
            {
                
                Boolean find = true;
                foreach (Version verVal in ver)
                {
                    if (verVal == null && verVal.ToString().Equals("0.0"))
                    {
                        find = false;
                        setError();
                    }
                }

                if (find == true)
                {
                    setSuccess();
                }

            }
            catch (Exception ex)
            {
                setQuestionMark();
            }

            UpdatePanelFrameworkMsg.Update();
            UpdatePanelFrameworkImg.Update();
        }

        private static Collection<Version> InstalledDotNetVersions()
        {
            Collection<Version> versions = new Collection<Version>();
            RegistryKey NDPKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4");
            if (NDPKey != null)
            {
                string[] subkeys = NDPKey.GetSubKeyNames();
                foreach (string subkey in subkeys)
                {
                    GetDotNetVersion(NDPKey.OpenSubKey(subkey), subkey, versions);
                }
            }
            return versions;
        }

        private static void GetDotNetVersion(RegistryKey parentKey, string subVersionName, Collection<Version> versions)
        {
            if (parentKey != null)
            {
                string installed = Convert.ToString(parentKey.GetValue("Install"));
                if (installed == "1")
                {
                    string version = Convert.ToString(parentKey.GetValue("Version"));
                    if (string.IsNullOrEmpty(version))
                    {
                        if (subVersionName.StartsWith("v"))
                            version = subVersionName.Substring(1);
                        else
                            version = subVersionName;
                    }

                    Version ver = new Version(version);
                    versions.Add(ver);
                }
            }
        }

        private void setSuccess()
        {
            lblFrameworkMsg.Text = ConfigChecklistConstants.FRAMEWORK_INSTALLED_MSG;
            this.setGreen(lblFrameworkMsg);
            this.setTickImage(imgFramework);
        }

        private void setError()
        {
            lblFrameworkMsg.Text = ConfigChecklistConstants.FRAMEWORK_NOT_INSTALLED_MSG;
            this.setRed(lblFrameworkMsg);
            this.setCrossImage(imgFramework);
        }

        private void setQuestionMark()
        {
            lblFrameworkMsg.Text = ConfigChecklistConstants.FRAMEWORK_EXCEPTION_MSG;
            this.setRed(lblFrameworkMsg);
            this.setQuestionImage(imgFramework);
        }


    }
}