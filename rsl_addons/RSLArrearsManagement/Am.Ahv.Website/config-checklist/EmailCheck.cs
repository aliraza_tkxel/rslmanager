﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Am.Ahv.Website.config_checklist
{
    public class EmailCheck : Checklist, IVerification
    {
        TextBox txtEmail = new TextBox();
        Label lblEmailVerificationMessage = new Label();
        Image imgEmailVerification = new Image();

        UpdatePanel UpdatePanelEmailMsg = new UpdatePanel();
        UpdatePanel UpdatePanelEmailImage = new UpdatePanel();
        
        public EmailCheck()
        {
            Page page = (Page)HttpContext.Current.Handler;

            txtEmail = (TextBox)page.FindControl("txtEmail");
            lblEmailVerificationMessage = (Label)page.FindControl("lblEmailVerificationMessage");
            imgEmailVerification = (Image)page.FindControl("imgEmailVerification");

            UpdatePanelEmailMsg = (UpdatePanel)page.FindControl("UpdatePanelEmailMsg");
            UpdatePanelEmailImage = (UpdatePanel)page.FindControl("UpdatePanelEmailImage");
        }
        
        public void checkStatus()
        {
            sendEmail();
        }

        private void sendEmail()
        {   
            
            string emailBody = ConfigChecklistConstants.EMAIL_BODY;
            string emailTo = txtEmail.Text;
            bool isSent = false;
            try
            {
            
                while (true)
                {
                    if (Checklist.SendEmail(emailTo, ConfigChecklistConstants.EMAIL_SUBJECT, emailBody, true))
                    {
                        isSent = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                setError(ConfigChecklistConstants.EMAIL_FAILED_MSG + ConfigChecklistConstants.EXCEPTION_MSG_IS+ex.Message + ex.InnerException);
            }

            if (isSent == true)
            {
                setSuccess();
            }
            
            this.UpdatePanelEmailImage.Update();
            this.UpdatePanelEmailMsg.Update();
        }               

        private void setError(string msg)
        {
            lblEmailVerificationMessage.Text = msg;
            this.setRed(lblEmailVerificationMessage);
            this.setCrossImage(imgEmailVerification);
        }

        private void setSuccess()
        {
            lblEmailVerificationMessage.Text = ConfigChecklistConstants.EMAIL_SUCCESS_MSG;
            this.setGreen(lblEmailVerificationMessage);
            this.setTickImage(imgEmailVerification);
        }

        
    }
}