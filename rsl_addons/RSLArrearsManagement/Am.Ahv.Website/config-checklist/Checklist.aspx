﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Checklist.aspx.cs" Inherits="Am.Ahv.Website.Checklist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../../style/css/masterpage.css" rel="stylesheet" type="text/css" />
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 190px;
        }
        .style2
        {
            width: 1248px;
        }
        /* roScripts
Table Design by Mihalcea Romeo
www.roscripts.com
----------------------------------------------- */

table {
		border-collapse:collapse;
		background:#EFF4FB url(style/images/teaser.gif) repeat-x;
		border-left:1px solid #686868;
		border-right:1px solid #686868;
		font:0.8em/145% 'Trebuchet MS',helvetica,arial,verdana;
		color: #333;	
}

td, th {
		padding:5px;
}

caption {
		padding: 0 0 .5em 0;
		text-align: left;
		font-size: 1.4em;
		font-weight: bold;
		text-transform: uppercase;
		color: #333;
		background: transparent;
}

/* =links
----------------------------------------------- */

table a {
		color:#950000;
		text-decoration:none;
}

table a:link {}

table a:visited {
		font-weight:normal;
		color:#666;
		text-decoration: line-through;
}

table a:hover {
		border-bottom: 1px dashed #bbb;
}

/* =head =foot
----------------------------------------------- */

thead th, tfoot th, tfoot td {
		background:#333 url(style/images/llsh.gif) repeat-x;
		color:#fff
}

tfoot td {
		text-align:right
}

/* =body
----------------------------------------------- */

tbody th, tbody td {
		border-bottom: dotted 1px #333;
}

tbody th {
		white-space: nowrap;
}

tbody th a {
		color:#333;
}

.odd {}

tbody tr:hover {
		background:#fafafa
}
    </style>
</head>
<body>
   
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
    
        <table style="width:50%;" align="center" runat="server" id="cktable">
            <tr>
                <td class="style1" align="left" valign="middle">
                    &nbsp;</td>
                <td class="style2" align="center" valign="middle">
                    <asp:Label ID="lblHeading" runat="server" Font-Size="X-Large" 
                        Text="Arrears Management Configuration Checklist"></asp:Label>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    &nbsp;</td>
                <td class="style2" align="center" valign="middle">
                    <asp:Label ID="lblTopMsg" runat="server" ForeColor="Red" 
                        
                        Text="You can turn off this screen by setting &quot;AccessToConfigCheckList&quot; equals to false in web.config"></asp:Label>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblFrameworkLabel" runat="server" Text="Dotnet Framework (4.0):" 
                        Font-Bold="True"></asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelFrameworkMsg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblFrameworkMsg" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <asp:UpdateProgress 
                                ID="UpdateProgressFramework" runat="server" 
                                AssociatedUpdatePanelID="UpdatePanelFrameworkCheckLink">
                                <ProgressTemplate>
                                    <asp:Label ID="lblCheckingFramework" runat="server" ForeColor="Red" 
                                        Text="Checking Framework ..... "></asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:Image ID="imgCheckFrameworkProgress" runat="server" 
                                        ImageUrl="~/style/images/wait.gif" Width="266px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                           
                                    &nbsp;&nbsp;
                                    </td>
                <td align="center" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelFrameworkImg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Image ID="imgFramework" runat="server" style="margin-left: 0px" 
                                ImageUrl="~/style/images/question.png" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelFrameworkCheckLink" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkBtnCheckFramework" runat="server" Height="100%" 
                                onclick="lnkBtnCheckFramework_Click" Width="100px">Check Version Again ...</asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblReportViewerLabel" runat="server" 
                        Text="Report Viewer (10.0):" Font-Bold="True"></asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:Label ID="lblReportViewerMsg" runat="server" 
                        
                        Text="Unable to verify the installation of report viewer. If you didn't install the report viewer then please install it now."></asp:Label>
                </td>
                <td align="center" valign="middle">
                    <asp:Image ID="imgFramework0" runat="server" 
                        ImageUrl="~/style/images/question.png" />
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblDbConnectionLabel" runat="server" Font-Bold="True">Database Connection:</asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelDbConnectionMsg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblDbConnectionMsg" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelDbConnectionImg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Image ID="imgDbConnection" runat="server" 
                                ImageUrl="~/style/images/question.png" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    <asp:LinkButton ID="lnkBtnCheckDb" runat="server" Height="100%" 
                        onclick="lnkBtnCheckDb_Click" Width="100px">Check Database Connection Again ...</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblEnterEmailAddress" runat="server" Font-Bold="True">Enter Your Email Address:</asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelEmail" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEmail" runat="server" Width="235px"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnSendEmail" runat="server" onclick="btnSendEmail_Click" 
                                Text="Send Email" AccessKey="E" UseSubmitBehavior="False"  />
                            &nbsp;<br /> &nbsp;<asp:UpdateProgress ID="UpdateProgressEmail" runat="server" 
                                AssociatedUpdatePanelID="UpdatePanelEmail">
                                <ProgressTemplate>
                                    <asp:Label ID="lblSendingEmail" runat="server" ForeColor="Red" 
                                        Text=" Sending Email ..... "></asp:Label>
                                    &nbsp;&nbsp;
                                    <asp:Image ID="imgEmailProgress" runat="server" 
                                        ImageUrl="~/style/images/wait.gif" Width="266px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                           
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblEmailVerification" runat="server" Font-Bold="True">Email Verification:</asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelEmailMsg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblEmailVerificationMessage" runat="server">Enter your email in above text box to verify either Arrears Management is able to send out email or not.</asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelEmailImage" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Image ID="imgEmailVerification" runat="server" 
                                ImageUrl="~/style/images/question.png" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                </td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblEnterLogin" runat="server" Font-Bold="True" 
                        Text="Enter Login:"></asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:Label ID="lblEnterLoginMsg" runat="server" 
                        
                        Text="Enter login name of the user to whom you want to give access of arrears management system. The login name must exist in AC_LOGINS table in database &amp; user must be active."></asp:Label>
                    <br />
&nbsp;
                    <asp:UpdatePanel ID="UpdatePanelLoginName" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtLoginName" runat="server" Width="235px"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnLoginName" runat="server" onclick="btnLoginName_Click" 
                                Text="Save Login" AccessKey="L" CausesValidation="False" 
                                UseSubmitBehavior="False" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <asp:UpdateProgress ID="UpdateProgressLogin" runat="server" 
                        AssociatedUpdatePanelID="UpdatePanelLoginName">
                        <ProgressTemplate>
                            <asp:Label ID="lblSavingLoginName" runat="server" ForeColor="Red" 
                                Text="Saving Login Name ..... "></asp:Label>
                            &nbsp;
                            <asp:Image ID="imgLoginProgress" runat="server" 
                                ImageUrl="~/style/images/wait.gif" Width="266px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblArrearsAccessLabel" runat="server" Font-Bold="True" 
                        Text="Arrears Access:"></asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelArrearsAccessMsg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblArrearsAccessMsg" runat="server" ForeColor="Red">Use the above login text box to enter login name of the user to whom you want to give access of arrears management system. The login name must exist in AC_LOGINS table in database &amp; user must be active.  If user already exists  &amp; you are unable to login please veirfy that user must be active in AM_Resource table</asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelArrearsAccessImg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Image ID="imgArrearsAccess" runat="server" 
                                ImageUrl="~/style/images/question.png" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblExternalAgencyLabel" runat="server" Font-Bold="True" 
                        Text="External Agency Option:"></asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelExternalAgencyMsg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblExternalAgencyMsg" runat="server">Press this button to save external agency option in database </asp:Label>
                            &nbsp;
                            <br />
                            <br />
                            <asp:Button ID="btnExternalAgencyOption" runat="server" 
                                onclick="btnExternalAgencyOption_Click" Text="Save External Agency Option" 
                                Width="200px" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <asp:UpdateProgress ID="UpdateProgressAgency" runat="server" 
                        AssociatedUpdatePanelID="UpdatePanelExternalAgencyMsg">
                        <ProgressTemplate>
                            <asp:Label ID="lblSavingExternalAgency" runat="server" ForeColor="Red" 
                                Text="Saving External Agency Option ....."></asp:Label>
                            &nbsp;
                            <asp:Image ID="imgExternalAgencyProcess" runat="server" 
                                ImageUrl="~/style/images/wait.gif" Width="266px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
                <td align="center" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelExternalAgnecyImg" runat="server" 
                        UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Image ID="imgExternalAgency" runat="server" Height="25px" 
                                ImageUrl="~/style/images/question.png" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    &nbsp;</td>
                <td class="style2" align="left" valign="middle">
                    <asp:UpdatePanel ID="UpdatePanelFiles" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="grdFiles" runat="server" Font-Size="Small">
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    <asp:LinkButton ID="lnkBtnCheckReportFiles" runat="server" 
                        onclick="lnkBtnCheckReportFiles_Click" Width="100px">Check Report Files Again ...</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    &nbsp;</td>
                <td class="style2" align="left" valign="middle">
                    <asp:LinkButton ID="lnkBtnJobsList" runat="server" 
                        PostBackUrl="~/runjobs/ListJobs.aspx">Go to Schedule Jobs List</asp:LinkButton>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    <asp:Label ID="lblWebConfigLabel" runat="server" Font-Bold="True" 
                        Text="Web.config Configurable Items:"></asp:Label>
                </td>
                <td class="style2" align="left" valign="middle">
                    <asp:Label ID="lblWebConfigMsg" runat="server" 
                        
                        Text="Below is the list of items(keys) which you can change accordingly anytime in web.config." 
                        Font-Size="Large"></asp:Label>
                    <br />
                    <br />
                    <table frame="box" style="width: 100%; font-size: small;">
                        <tr>
                            <td align="center" valign="middle">
                                <asp:Label ID="lblWebConfigKeyHeading" runat="server" Text="Key"></asp:Label>
                            </td>
                            <td align="center" valign="middle">
                                <asp:Label ID="lblWebConfigValueHeading" runat="server" Text="Value"></asp:Label>
                            </td>
                            <td align="center" valign="middle">
                                <asp:Label ID="lblWebConfigDescHeading" runat="server" Text="Descsription"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                ActivityUploadPath</td>
                            <td valign="middle">
                                C:\uploads\activity-documents\</td>
                            <td valign="middle">
                                You can change this path accordingly but make sure, it must be writable. This 
                                path &#39;ll be used to uploead files of activity area</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                CaseDocumentsUploadPath</td>
                            <td valign="middle">
                                C:\uploads\case-documents\</td>
                            <td valign="middle">
                                You can change this path accordingly but make sure, it must be writable. This 
                                path &#39;ll be used to uploead files of case area</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                FileSize</td>
                            <td valign="middle">
                                2097152</td>
                            <td valign="middle">
                                This is the file size of documents which user can upload in the system. User &#39;ll&nbsp; 
                                not be allowed to upload file more than this size.This value represents 2 MB. 
                                You can change it accordingly.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                httpPath</td>
                            <td valign="middle">
                                http://</td>
                            <td valign="middle">
                                You don&#39;t need to change this.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                ChartImageHandler</td>
                            <td valign="middle">
                                <asp:Label ID="Label1" runat="server" 
                                    Text="storage=file;timeout=20;dir=c:\TempImageFiles\;" Width="50px"></asp:Label>
                            </td>
                            <td valign="middle">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="middle" colspan="3">
                                The above value is being used for chart of dashboard. You &#39;ll have to create the 
                                folder at this path <a href="file:///c:/TempImageFiles/">c:\TempImageFiles\</a>. 
                                You can change this accordingly but it must be writable.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                EmailSender</td>
                            <td valign="middle">
                                reimark.net@gmail.com</td>
                            <td valign="middle">
                                This email &#39;ll be marked as &quot;From&quot; of every email that &#39;ll be send out of the 
                                system. You can change it accrodingly.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                host</td>
                            <td valign="middle">
                                smtp.gmail.com</td>
                            <td valign="middle">
                                This is smtp address of server which is being used to send out email from 
                                Arrears system. At this time its gmail, you can change it accordingly.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                port</td>
                            <td valign="middle">
                                587</td>
                            <td valign="middle">
                                This is port provided by gmail to use with above given smtp address. You can change in 
                                according to your own need.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                username</td>
                            <td valign="middle">
                                reidmark.net@gmail.com</td>
                            <td valign="middle">
                                This is the username of the gmail account which is being used to send email. You 
                                can change it anytime.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                password</td>
                            <td valign="middle">
                                reid.test</td>
                            <td valign="middle">
                                This is the password of above mentioned gmail account which is being used to 
                                send email. You can change this password anytime.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                LetterLogo</td>
                            <td valign="middle">
                                ~\style\images\</td>
                            <td valign="middle">
                                You don&#39;t need to change this.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                OrganisationName</td>
                            <td valign="middle">
                                Broadland Housing Association</td>
                            <td valign="middle">
                                This key represents the organisation name. This is being used in the system for 
                                display purpose. You can change it any time.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                OrganisationAddress</td>
                            <td valign="middle">
                                NCFC, Jarrold Stand, Carrow Road, Norwich, NR1 1HU</td>
                            <td valign="middle">
                                This key represents the organisation address. This is being used in the system 
                                for display purpose. You can change it any time.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                OrganisationPhone</td>
                            <td valign="middle">
                                01603 750200 Fax: 01603 750222</td>
                            <td valign="middle">
                                This key represents the organisation phone. This is being used in the system for 
                                display purpose. You can change it any time.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                OrganisationEmail</td>
                            <td valign="middle">
                                enq@broadlandhousing.org</td>
                            <td valign="middle">
                                This key represents the organisation email. This is being used in the system for 
                                display purpose. You can change it any time.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                ConnectionTimer</td>
                            <td valign="middle">
                                900</td>
                            <td valign="middle">
                                This is the max time which system &#39;ll take to perform any operation. You can 
                                change it, if requires.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                CachedTimer</td>
                            <td valign="middle">
                                20</td>
                            <td valign="middle">
                                You don&#39;t need to change this.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                AdministratorEmailAddress</td>
                            <td valign="middle">
                                &nbsp;</td>
                            <td valign="middle">
                                This is the email address which &#39;ll receive the report either schedule job has 
                                been run successfully or not. You can change it according to your need.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                EmailFrom</td>
                            <td valign="middle">
                                TkXel</td>
                            <td valign="middle">
                                This is the sender name of email. You can change it to anytime.</td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                AccessToConfigCheckList</td>
                            <td valign="middle">
                                true</td>
                            <td valign="middle">
                                You can turn off this page by setting this key to false. Please make this value 
                                should be true only if you want to check configuration of the system.</td>
                        </tr>
                    </table>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    &nbsp;</td>
                <td class="style2" align="center" valign="middle">
                    <asp:Label ID="lblStepsOfDeploymentHeading" runat="server" Font-Size="X-Large" 
                        Text="Manual Steps of Deployment"></asp:Label>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1" align="left" valign="middle">
                    &nbsp;</td>
                <td class="style2" align="center" valign="middle">
                    <table frame="box" style="width:100%;">
                        <tr>
                            <td align="left" style="font-size: 14px;">
                                <ol>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">Take back up of web.config 
                                        file</span></li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">Place all the files in rsl 
                                        manager.</span></li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">Replace the web.config file 
                                        and make the changes according to your own need.</span></li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">Run the database scripts 
                                        according to sequence mentioned in email.</span></li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">Open browser and run these 
                                        scheduled jobs just as the below mentioned sequence.<br />
                                        </span>&nbsp;<ol>
                                            <li><span style="font-family: 'trebuchet ms', sans-serif">secure/casemgt/<wbr>GetCustomerRentParameters.aspx</span></li>
                                            <li><span style="font-family:'trebuchet ms', sans-serif">secure/casemgt/<wbr>FindFirstDetectionCustomers.<wbr>aspx</span></li>
                                            <li><span style="font-family: 'trebuchet ms', sans-serif">secure/reports/<wbr>FindHistoricalBalanceList.aspx</span></li>
                                            <li><span style="font-family: 'trebuchet ms', sans-serif">secure/casemgt/<wbr>FindMissedPayments.aspx</span></li>
                                            <li><span style="font-family: 'trebuchet ms', sans-serif">secure/casemgt/<wbr>PaymentPlanReviewNotification.<wbr>aspx</span></li>
                                            <li><span style="font-family: 'trebuchet ms', sans-serif">secure/dashboard/<wbr>NegativeRentBalance.aspx</span></li>
                                        </ol>
                                    </li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">This job &quot;secure/casemgt/<wbr>GetCustomerRentParameters.<wbr>aspx&quot; 
                                        must be run after 30 min.&nbsp;</span></li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">This job &quot;secure/casemgt/<wbr>GetCustomerRentParameters.<wbr>aspx&quot; 
                                        must be run before any other job at the first time.</span></li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">Other jobs must be 
                                        scheduled to run daily.</span></li>
                                    <li>You can schedule these jobs in Windows Task Schuduler. Please make sure that 
                                        these jobs must be run in time. In case of failure, you might face wrong number 
                                        of records or calculation in the system.</li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">If you have received 
                                        CRM.asp then take backup of CRM.asp which is in customer folder.</span></li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">If you have received 
                                        CRM.asp. Replace the CRM.asp and perform the following steps:</span><ol>
                                            <li><span style="font-family:'trebuchet ms', sans-serif">Open the CRM.asp.</span></li>
                                            <li><span style="font-family:'trebuchet ms', sans-serif">Go to line number 375.</span></li>
                                            <li><span style="font-family:'trebuchet ms', sans-serif">Copy the folder name (i.e. 
                                                virtual directory) in which you have placed the Arrears Management Code and 
                                                paste it in the place of &quot;ArrearUK&quot; (DO NOT CHANGE ANYTHING ELSE IN THIS LINE).</span></li>
                                            <li><span style="font-family:'trebuchet ms', sans-serif">Save the file.</span></li>
                                        </ol>
                                    </li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">If you have received 
                                        Scope.asp. Take backup of Scope.asp which is in supplier folder.</span></li>
                                    <li><span style="font-family:'trebuchet ms', sans-serif">Replace the Scope.asp</span></li>
                                </ol>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="center" valign="middle">
                    &nbsp;</td>
                <td align="center" valign="middle">
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
