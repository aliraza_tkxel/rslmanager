﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Am.Ahv.Website.config_checklist
{
    public static class ConfigChecklistConstants
    {
        //framework constants
        public static string FRAMEWORK_INSTALLED_MSG = "Dot net framework Version 4.0 is not installed";
        public static string FRAMEWORK_NOT_INSTALLED_MSG = "Dot net framework Version 4.0 is installed";
        public static string FRAMEWORK_EXCEPTION_MSG = "Error occured while determining the framework 4.0. Please ensure that dot net framework 4 is installed";
        
        //db connection constants
        public static string Db_EXCEPTION_MSG = "Unable to connect with database. Please veirfy that SQL server instance is running & you have changed the connection string in 'Web.config' file. <br /> Exception Message is:";
        public static string Db_CONN_SUCCESS_MSG = "Connection with the database has been established successfully.";
        
        //email constants
        public static string EMAIL_SUBJECT = "Checklist Email";
        public static string EMAIL_BODY = "If you have received this email then it means Arrears system is able to send email out. You can change email configuration in web.config";
        public static string EMAIL_SUCCESS_MSG = "Email has been sent successfully. Please check your email.";
        public static string EMAIL_FAILED_MSG = "Email sending failed. Please verify email configuration settings in 'web.config' & you should also verify that firewall of the system should allow to send email.";
        
        // report file constatns
        public static string FILE_EXISTS_MSG = "File exists.";
        public static string FILE_SIZE_SMALL_MSG = "File size is too short. Build machine did not compile it. You should copy this file from un-compiled code & paste it at this path:-  ";
        public static string FILE_DOESNOT_EXIST_MSG = "You should copy this file from un-compiled code & paste it at this path:-  ";
        public static string FILE_NOT_FOUND_MSG = "Unable to determine the file location.";
        public static string FILE_EXCEPTION_MSG = "You should copy this file from un-compiled code & paste it at this path:-  ";
        public static string FILE_DOES_NOT_EXIST_SHORT_MSG = "File does not exist.";
        public static string FILES_TABLES = "filesTable";
        public static string REPORTS_FILE_PATH = "Reports File Path";
        public static string FILE_EXIST_OR_NOT = "File Exists OR Not";
        public static string REPORT_MESSAGE = "Message";
        public static string ROOT_TO_REPORTS_PATH = @"\controls\reports\";
        
        //login name constants
        public static string LOGIN_NAME_NOT_SAVE_MSG = "System is unable to save the login name. Check your database connection. Verify that Login Name exists in AC_LOGINS table & also active & also verify that LookupCodeId = 1 exsists in AM_LookupCode table. If user already exists & you are unable to login then please veirfy that user must be active in AM_Resource table";
        public static string LOGIN_NAME_NOT_EXISTS = "Login name does not exists.";
        public static string LOGIN_NAME_SAVE_MSG = "Login name which you entered above, that has been saved successfully in the system. Now you can login into arrears using this login name.";
        public static string LOGIN_NAME_ALREADY_EXISTS_MSG = "Login name already exists in the system. You can login into arrears using this login name.";
        
        //external agency constants
        public static string EXTERNAL_AGECNY_NOT_SAVE_MSG = "System is unable to save the external agency option in system. Check your database connection.";
        public static string EXTERNAL_AGECNY_SAVE_MSG = "External Agency Option has been saved successfully in system.";
        public static string EXTERNAL_AGECNY_ALREADY_EXISTS_MSG = "External Agency Option already exists in system.";

        public static string CONFIG_KEY = "AccessToConfigCheckList"; 
        public static string EXCEPTION_MSG_IS = "<br /> Exception Message is:";                
        

        
        
        


    }
}