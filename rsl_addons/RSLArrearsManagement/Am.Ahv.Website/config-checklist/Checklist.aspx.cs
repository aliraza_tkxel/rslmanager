﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.ObjectModel;


using System.IO;
using System.Data;

using System.Configuration;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Website.config_checklist;

//using System.Drawing;

namespace Am.Ahv.Website
{
    public partial class Checklist : PageBase
    {            
        IVerification verfication;

        #region Events
        
        protected void Page_Load(object sender, EventArgs e) 
        {   
            try
            {
                Boolean accessConfigCheckList = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings[ConfigChecklistConstants.CONFIG_KEY]);
                if (accessConfigCheckList == true)
                {
                    if (!Page.IsPostBack)
                    {
                        verfication = new FrameworkCheck();
                        verfication.checkStatus();


                        verfication = new DbConnectionCheck();
                        verfication.checkStatus();

                        verfication = new ReportsCheck();
                        verfication.checkStatus();
                    }
                }
                else
                {
                    Response.Redirect("~/Default.aspx");
                }
               

            }
            catch (Exception ex)
            {
                Response.Redirect("~/Default.aspx");
            }
        }
                 
        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            verfication = new EmailCheck();
            verfication.checkStatus();        
        }       

        protected void lnkBtnCheckFramework_Click(object sender, EventArgs e)
        {
            verfication = new FrameworkCheck();
            verfication.checkStatus();
        }

        protected void lnkBtnCheckDb_Click(object sender, EventArgs e)
        {
            verfication = new DbConnectionCheck();
            verfication.checkStatus();
        }

        protected void lnkBtnCheckReportFiles_Click(object sender, EventArgs e)
        {
            verfication = new ReportsCheck();
            verfication.checkStatus();
        }

        protected void btnLoginName_Click(object sender, EventArgs e)
        {
            Save logName = new Save();
            logName.saveLogin();
        }

        protected void btnExternalAgencyOption_Click(object sender, EventArgs e)
        {
            Save logName = new Save();
            logName.saveExternalAgency();
        }

        #endregion

        public void setTickImage(WebControl imgControl)
        {
            imgControl.Attributes.Add("src", "../style/images/tick.png");
        }

        public void setQuestionImage(WebControl imgControl)
        {
            imgControl.Attributes.Add("src", "../style/images/question.png");
        }

        public void setCrossImage(WebControl imgControl)
        {
            imgControl.Attributes.Add("src", "../style/images/cross.png");
        }

        public void setGreen(WebControl label)
        {
            label.Attributes.Add("style", "color:#009900;");            
        }

        public void setRed(WebControl label)
        {
            label.Attributes.Add("style", "color:Red;");
        }
    }
}