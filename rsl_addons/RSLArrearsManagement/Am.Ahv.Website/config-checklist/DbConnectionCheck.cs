﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Am.Ahv.Entities;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Am.Ahv.Website.config_checklist
{
    public class DbConnectionCheck : Checklist, IVerification
    {
        public TKXEL_RSLManager_UKEntities AMDBMgr = null;
        public Boolean exception = false;

        Label lblDbConnectionMsg = new Label();
        Image imgDbConnection = new Image();

        UpdatePanel UpdatePanelDbConnectionMsg = new UpdatePanel();
        UpdatePanel UpdatePanelDbConnectionImg = new UpdatePanel();

        public DbConnectionCheck()
        {
            Page page = (Page)HttpContext.Current.Handler;
            lblDbConnectionMsg = (Label)page.FindControl("lblDbConnectionMsg");
            imgDbConnection = (Image)page.FindControl("imgDbConnection");

            UpdatePanelDbConnectionMsg = (UpdatePanel)page.FindControl("UpdatePanelDbConnectionMsg");
            UpdatePanelDbConnectionImg = (UpdatePanel)page.FindControl("UpdatePanelDbConnectionImg");
        }
        public void checkStatus()
        {
            checkDbConnection();           
        }

        public void checkDbConnection()
        {

            checkConnection();
            if (exception == false)
            {
                queryDatabase();
            }
            updatePanels();            
            
        }

        private void checkConnection()
        {
            try
            {
                AMDBMgr = new TKXEL_RSLManager_UKEntities();
                exception = false;
            }
            catch (System.ArgumentException argumentexception)
            {
                exception = true;
                setError(ConfigChecklistConstants.Db_EXCEPTION_MSG + argumentexception.Message);                                
            }
            catch (Exception ex)
            {
                exception = true;
                setError(ConfigChecklistConstants.Db_EXCEPTION_MSG + ex.Message);                
            }
        }

        private void queryDatabase()
        {
            try
            {
                //List<AM_StandardLetters> letters = (from Letters in this.AMDBMgr.AM_StandardLetters where Letters.IsActive == true select Letters).ToList<AM_StandardLetters>();

                var userCount = (from ds in AMDBMgr.AC_LOGINS                                 
                                 select ds.EMPLOYEEID).Count();
                if (userCount > 0)
                {
                    setSuccess();
                }
                else
                {
                    setError(ConfigChecklistConstants.Db_EXCEPTION_MSG);
                }
                
            }
            catch (Exception ex)
            {
                setError(ConfigChecklistConstants.Db_EXCEPTION_MSG + ex.Message);               
            }
        }

        private void updatePanels()
        {
            UpdatePanelDbConnectionMsg.Update();
            UpdatePanelDbConnectionImg.Update();
        }

        private void setError(string msg)
        {
            lblDbConnectionMsg.Text = msg;
            this.setRed(lblDbConnectionMsg);
            this.setCrossImage(imgDbConnection);
        }

        private void setSuccess()
        {
            this.setTickImage(imgDbConnection);
            this.setGreen(lblDbConnectionMsg);
            lblDbConnectionMsg.Text = ConfigChecklistConstants.Db_CONN_SUCCESS_MSG;
        }

    }
}