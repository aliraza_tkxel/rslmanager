﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Transactions;
using Am.Ahv.Entities;
using System.Data.Objects;

namespace Am.Ahv.Website.config_checklist
{
    public class Save : Checklist
    {
        DbConnectionCheck dbcon = new DbConnectionCheck();
        TextBox txtLoginName = new TextBox();
        
        Label lblArrearsAccessMsg = new Label();
        Label lblExternalAgencyMsg = new Label();
        
        Image imgArrearsAccess = new Image();
        Image imgExternalAgency = new Image();


        UpdatePanel UpdatePanelArrearsAccessMsg = new UpdatePanel();
        UpdatePanel UpdatePanelArrearsAccessImg = new UpdatePanel();

        UpdatePanel UpdatePanelExternalAgencyMsg = new UpdatePanel();
        UpdatePanel UpdatePanelExternalAgnecyImg = new UpdatePanel();

        public Save()
        {
            Page page = (Page)HttpContext.Current.Handler;

            txtLoginName = (TextBox)page.FindControl("txtLoginName");
            
            lblArrearsAccessMsg = (Label)page.FindControl("lblArrearsAccessMsg");
            lblExternalAgencyMsg = (Label)page.FindControl("lblExternalAgencyMsg");
            
            imgArrearsAccess = (Image)page.FindControl("imgArrearsAccess");
            imgExternalAgency = (Image)page.FindControl("imgExternalAgency");

            UpdatePanelArrearsAccessMsg = (UpdatePanel)page.FindControl("UpdatePanelArrearsAccessMsg");
            UpdatePanelArrearsAccessImg = (UpdatePanel)page.FindControl("UpdatePanelArrearsAccessImg");

            UpdatePanelExternalAgencyMsg = (UpdatePanel)page.FindControl("UpdatePanelExternalAgencyMsg");
            UpdatePanelExternalAgnecyImg = (UpdatePanel)page.FindControl("UpdatePanelExternalAgnecyImg");
        }

        public void saveLogin()
        {
            dbcon.checkDbConnection();
            string loginName = txtLoginName.Text.Trim();

            if (dbcon.exception == false)
            {
                try
                {
                    using (TransactionScope trans = new TransactionScope())
                    {
                       
                        AC_LOGINS employee = (from ds in dbcon.AMDBMgr.AC_LOGINS
                                                where ds.LOGIN == loginName
                                                select ds).Single();
                       

                        //AC_LOGINS employee = dbcon.AMDBMgr.AC_LOGINS.Single(p => p.LOGIN == loginName);
                        
                        if (employee.LOGIN.Equals(String.Empty))
                        {
                            this.setLoginError(ConfigChecklistConstants.LOGIN_NAME_NOT_EXISTS);
                        }
                        else
                        {
                                                       
                            var userCount = (from ds in dbcon.AMDBMgr.AM_Resource
                                             where ds.EmployeeId == employee.EMPLOYEEID                                             
                                             select ds.EmployeeId).Count();

                            if (userCount == 0)
                            {
                                AM_Resource resourceObj = new AM_Resource();
                                resourceObj.LookupCodeId = 1;
                                resourceObj.EmployeeId = (int)employee.EMPLOYEEID;
                                resourceObj.IsActive = true;
                                resourceObj.CreatedDate = DateTime.Now;
                                resourceObj.CreatedBy = 1;
                                resourceObj.ModifiedDate = DateTime.Now;
                                resourceObj.ModifiedBy = 1;

                                dbcon.AMDBMgr.AddToAM_Resource(resourceObj);
                                dbcon.AMDBMgr.SaveChanges();
                                trans.Complete();
                                setLoginSuccess(ConfigChecklistConstants.LOGIN_NAME_SAVE_MSG);
                            }
                            else
                            {
                                setLoginSuccess(ConfigChecklistConstants.LOGIN_NAME_ALREADY_EXISTS_MSG);
                            }

                            
                        }                                                
                    }                                        
                }
                catch (Exception ex) 
                {
                    this.setLoginError(ConfigChecklistConstants.LOGIN_NAME_NOT_SAVE_MSG + ConfigChecklistConstants.EXCEPTION_MSG_IS + ex.Message);
                }               
            }
            else
            {
                this.setLoginError(ConfigChecklistConstants.LOGIN_NAME_NOT_SAVE_MSG);           
            }

            UpdatePanelArrearsAccessMsg.Update();
            UpdatePanelArrearsAccessImg.Update();
        }

        public void saveExternalAgency() 
        {
            dbcon.checkDbConnection();

            if (dbcon.exception == false)
            {
                try
                {
                    string strExternalAgency = "External Agency";
                    using (TransactionScope trans = new TransactionScope())
                    {
                        var userCount = (from ds in dbcon.AMDBMgr.S_AREAOFWORK
                                         where ds.DESCRIPTION == strExternalAgency 
                                         select ds.AREAOFWORKID).Count();
                        if (userCount == 0)
                        {
                            S_AREAOFWORK sareaObj = new S_AREAOFWORK();
                            sareaObj.DESCRIPTION = strExternalAgency;


                            dbcon.AMDBMgr.AddToS_AREAOFWORK(sareaObj);
                            dbcon.AMDBMgr.SaveChanges();
                            trans.Complete();
                            setExternalAgencySuccess(ConfigChecklistConstants.EXTERNAL_AGECNY_SAVE_MSG);
                        }
                        else
                        {
                            setExternalAgencySuccess(ConfigChecklistConstants.EXTERNAL_AGECNY_ALREADY_EXISTS_MSG);
                        }                        
                    }

                }
                catch (Exception ex)
                {
                    this.setLoginError(ConfigChecklistConstants.EXTERNAL_AGECNY_NOT_SAVE_MSG + ConfigChecklistConstants.EXCEPTION_MSG_IS + ex.Message);
                } 
            }
            else
            {
                this.setLoginError(ConfigChecklistConstants.EXTERNAL_AGECNY_NOT_SAVE_MSG);
            }

            UpdatePanelExternalAgencyMsg.Update();
            UpdatePanelExternalAgnecyImg.Update();
        }


        private void setLoginError(string msg)
        {
            lblArrearsAccessMsg.Text = msg;
            this.setRed(lblArrearsAccessMsg);
            setCrossImage(imgArrearsAccess);
        }

        private void setLoginSuccess(string msg)
        {
            lblArrearsAccessMsg.Text = msg;
            this.setGreen(lblArrearsAccessMsg);
            setTickImage(imgArrearsAccess);
        }

        private void setExternalAgencyError(string msg)
        {
            lblExternalAgencyMsg.Text = msg;
            this.setRed(lblExternalAgencyMsg);
            setCrossImage(imgExternalAgency);
        }

        private void setExternalAgencySuccess(string msg)
        {
            lblExternalAgencyMsg.Text = msg;
            this.setGreen(lblExternalAgencyMsg);
            setTickImage(imgExternalAgency);
        }
    }
}