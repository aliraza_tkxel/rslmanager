﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Entities;
using System.Data.Entity;
namespace Am.Ahv.Website.Services
{
    /// <summary>
    /// Summary description for SearchNames
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SearchNames : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }
        [WebMethod]
        public string[] GetCompletionList(string prefixText, int count)
        {
            Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
            string[] names = cw.GetNames(prefixText);
            return names;
        }
    }
}
