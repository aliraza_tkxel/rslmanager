﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.Casemgmt;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.BusinessManager.Dashboard;
using Am.Ahv.BusinessManager.payments;
using Am.Ahv.BusinessManager.reports;
using Am.Ahv.Entities;

namespace Am.Ahv.Website
{
    public partial class Default : PageBase
    {
        #region"Attributes"
        bool isError;
        bool isException;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion



        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
            }
            if (!IsPostBack)
            {
                try
                {
                    resetMessage();
                    if (!IsPostBack)
                    {
                        InitLookups();
                        //LoadAllControls();
                    }
                }

                catch (Exception ex)
                {
                    isException = true;
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
                finally
                {
                    if (isException)
                    {
                        setMessage(UserMessageConstants.LoadingPageError, true);
                    }
                }
            }
        }

        #region"ddlCaseOwnedBy Selected Index Changed"

        protected void ddlCaseOwnedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Am.Ahv.BusinessManager.Dashboard.ReviewList reviewListmanager = new BusinessManager.Dashboard.ReviewList();
                if (ddlCaseOwnedBy.SelectedValue == "-1")
                {
                    ddlCaseRegion.DataSource = reviewListmanager.GetAllRegionList(); ;
                    ddlCaseRegion.DataTextField = ApplicationConstants.regionname;
                    ddlCaseRegion.DataValueField = ApplicationConstants.regionId;
                    ddlCaseRegion.DataBind();
                    ddlCaseRegion.Items.Add(new ListItem("All", "-1"));
                    ddlCaseRegion.SelectedValue = "-1";
                }
                else
                {
                    SuppressedCase sc = new SuppressedCase();
                    ddlCaseRegion.DataSource = sc.GetRegionList(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                    ddlCaseRegion.DataTextField = ApplicationConstants.regionname;
                    ddlCaseRegion.DataValueField = ApplicationConstants.regionId;
                    ddlCaseRegion.DataBind();
                    ddlCaseRegion.Items.Add(new ListItem("All", "-1"));
                    ddlCaseRegion.SelectedValue = "-1";
                }
                if (ddlCaseSuburb.Items.Count > 0)
                {
                    ddlCaseSuburb.Items.Clear();
                }
                AddCaseWorker cw = new AddCaseWorker();
                ddlCaseSuburb.DataSource = cw.GetSuburbsList(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlCaseSuburb.DataTextField = ApplicationConstants.schememname;
                ddlCaseSuburb.DataValueField = ApplicationConstants.suburbId;
                ddlCaseSuburb.DataBind();
                ddlCaseSuburb.Items.Add(new ListItem("All", "-1"));
                ddlCaseSuburb.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.LoadingRegionError, true);
                }
            }
        }

        #endregion

        #region"ddlCaseRegion Selected Index Changed"

        protected void ddlCaseRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCaseSuburb.Items.Count > 0)
                {
                    ddlCaseSuburb.Items.Clear();
                }
                AddCaseWorker cw = new AddCaseWorker();
                ddlCaseSuburb.DataSource = cw.GetAllSuburbs(Convert.ToInt32(ddlCaseRegion.SelectedValue));
                ddlCaseSuburb.DataTextField = ApplicationConstants.schememname;
                ddlCaseSuburb.DataValueField = ApplicationConstants.schemeId;
                ddlCaseSuburb.DataBind();
                ddlCaseSuburb.Items.Add(new ListItem("All", "-1"));
                ddlCaseSuburb.SelectedValue = "-1";
                updpnlDashBoardSearch.Update();

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.LoadingSuburbError, true);
                }
            }

        }

        #endregion

        #region get my cases Count
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getCountMyCasesApi()
        {
            int count = 0;
            CaseDetails mycases = new CaseDetails();
            count = mycases.CountMyCases(HttpContext.Current.Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.CaseOwnedByDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.RegionDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.SuburbDashboardSearch]));
            return count.ToString();
        }
        #endregion

        #region get First Detection
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getCountFirstDetection()
        {
            int count = 0;
            FirstDetectionList firstDetectionList = new FirstDetectionList();
            count = firstDetectionList.GetFirstDetectionListAlertCount(HttpContext.Current.Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.RegionDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.SuburbDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.CaseOwnedByDashboardSearch]));
            return count.ToString();
        }
        #endregion

        #region get Count Overdue stages
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getCountOversueStages()
        {
            int count = 0;
            vacteoverdue Manager = new vacteoverdue();
            count = Manager.GetCaseOverDueCount(Convert.ToInt32(HttpContext.Current.Session[SessionConstants.CaseOwnedByDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.RegionDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.SuburbDashboardSearch]));
            return count.ToString();
        }
        #endregion

        #region get Count Overdue stages
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getCountOversueActions()
        {
            int count = 0;
            vacteoverdue Manager = new vacteoverdue();
            count = Manager.GetOverdueActionsAlertCount(Convert.ToInt32(HttpContext.Current.Session[SessionConstants.CaseOwnedByDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.RegionDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.SuburbDashboardSearch]));
            return count.ToString();
        }
        #endregion

        #region get Missed Payment count
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getCountMissedPayments()
        {

            return showMissedPaymentAlertCount(HttpContext.Current.Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.RegionDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.SuburbDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.CaseOwnedByDashboardSearch]));

        }
        #endregion

        #region get NISPDueToExpire count
        /// <summary>
        ///  get NISPDueToExpire Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getCountNISPDueToExpire()
        {
            vacteoverdue Manager = new vacteoverdue();
            return Manager.GetVacateCount(Convert.ToInt32(HttpContext.Current.Session[SessionConstants.CaseOwnedByDashboardSearch])).ToString();

        }
        #endregion


        #region get AnticipatedHBBalanceBalance
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getAnticipatedHBBalanceBalance()
        {

            return showAnticipatedHBBalanceBalance(HttpContext.Current.Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.RegionDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.SuburbDashboardSearch]), Convert.ToInt32(HttpContext.Current.Session[SessionConstants.CaseOwnedByDashboardSearch]));

        }
        #endregion

        #region get RentNetBalance
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getRentNetBalance()
        {

            double netBalance = Convert.ToDouble(HttpContext.Current.Session[SessionConstants.grossRentBalance].ToString()) - Convert.ToDouble(HttpContext.Current.Session[SessionConstants.anticipatedHBBalance].ToString());
            return "£" + Math.Round(netBalance, 0).ToString();
        }
        #endregion

        #region getRentGrossBalance
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getRentGrossBalance()
        {

            double grossBalance = Convert.ToDouble(HttpContext.Current.Session[SessionConstants.grossRentBalance].ToString());
            return "£" + Math.Round(grossBalance, 0).ToString();
        }
        #endregion


        #region"Functions"
        #region"Init Lookups"

        private void InitLookups()
        {

            try
            {

                SuppressedCase sc = new SuppressedCase();
                ddlCaseOwnedBy.DataSource = sc.CasesOwnedBy();
                ddlCaseOwnedBy.DataTextField = ApplicationConstants.EmloyeeName;
                ddlCaseOwnedBy.DataValueField = ApplicationConstants.ResourceId;
                ddlCaseOwnedBy.DataBind();
                ddlCaseOwnedBy.Items.Insert(0, new ListItem("All", "-1"));
                ddlCaseOwnedBy.SelectedValue = base.GetLoggedInUserID().ToString();

                ddlCaseRegion.DataSource = sc.GetRegionList(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlCaseRegion.DataTextField = ApplicationConstants.regionname;
                ddlCaseRegion.DataValueField = ApplicationConstants.regionId;
                ddlCaseRegion.DataBind();
                ddlCaseRegion.Items.Insert(0, new ListItem("All", "-1"));
                ddlCaseRegion.SelectedValue = "-1";

                AddCaseWorker cw = new AddCaseWorker();
                ddlCaseSuburb.DataSource = cw.GetSuburbsList(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlCaseSuburb.DataTextField = ApplicationConstants.schememname;
                ddlCaseSuburb.DataValueField = ApplicationConstants.suburbId;
                ddlCaseSuburb.DataBind();
                ddlCaseSuburb.Items.Insert(0, new ListItem("All", "-1"));
                ddlCaseSuburb.SelectedValue = "-1";

                SetSession();
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.LoadingLookupError, true);
                }
            }

        }

        #endregion

        #region "show Latest First Detection Count"
        public static string showMissedPaymentAlertCount(string postCode, int regionId, int suburbId, int resourceId)
        {
            bool allRegionFlag = (regionId > 0 ? false : true);
            bool allOwnerFlag = (resourceId > 0 ? false : true);
            bool allSuburbFlag = (suburbId > 0 ? false : true);
            Payments MissedPayments = new Payments();
            return MissedPayments.GetPaymentPlanMonitoringCount(postCode, resourceId, regionId, suburbId, allOwnerFlag, allRegionFlag, allSuburbFlag, "misspayments").ToString();


        }
        #endregion

        #region "show  Anticipated HB Balance"
        public static string showAnticipatedHBBalanceBalance(string postCode, int regionId, int suburbId, int resourceId)
        {
            try
            {
                //resourceId = -1;
                BalanceList balanceList = new BalanceList();
                AM_SP_GetAnticipatedHBBalance_Result result = new AM_SP_GetAnticipatedHBBalance_Result();
                result = balanceList.GetAnticipatedHBBalance(postCode,resourceId, regionId, suburbId);
                double hb = Convert.ToDouble(result.HB.ToString());


                if (result.NetRentBalance == null)
                {
                    result.NetRentBalance = 0;
                }
                if (result.GrossRentBalance == null)
                {
                    result.GrossRentBalance = 0;
                }
                if (result.HB == null)
                {
                    result.HB = 0;
                }
                HttpContext.Current.Session[SessionConstants.netRentBalance] = result.NetRentBalance;
                HttpContext.Current.Session[SessionConstants.grossRentBalance] = result.GrossRentBalance;
                HttpContext.Current.Session[SessionConstants.anticipatedHBBalance] = result.HB;

                return "£" + Math.Round(hb, 0).ToString();
            }
            catch (Exception ex)
            {

                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return "Problem loading data.";
            }

        }
        #endregion
        #region "show Net Rent Balance"

        #endregion

        #region"Set View State"

        public void SetViewState(int value)
        {
            ViewState[ViewStateConstants.caseOfficerIdDashBoard] = value;
            SetCaseOfficerProperty();
        }

        #endregion

        #region"Set Case Officer Property"

        public void SetCaseOfficerProperty()
        {
            lblCaseOfficer.Text = Convert.ToString(ViewState[ViewStateConstants.caseOfficerIdDashBoard]);
        }

        #endregion

        #region"Set Session"

        public void SetSession()
        {
            Session[SessionConstants.CaseOwnedByDashboardSearch] = ddlCaseOwnedBy.SelectedValue;
            Session[SessionConstants.RegionDashboardSearch] = ddlCaseRegion.SelectedValue;
            Session[SessionConstants.SuburbDashboardSearch] = ddlCaseSuburb.SelectedValue;
            Session[SessionConstants.PostCodeSearch] = txtPostCode.Text;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SetSession();
            ScriptManager.RegisterStartupScript(this, GetType(), "loadAlerts", "loadAlerts();", true);
        }
        #endregion
    }
}