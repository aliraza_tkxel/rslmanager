﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using Am.Ahv.BusinessManager.Casemgmt;
using System.IO;
using Am.Ahv.Website.pagebase;
using System.Drawing;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.Customers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI.HtmlControls;

namespace Am.Ahv.Website.secure.reports
{
    public partial class ExportBalanceListReport : PageBase
    {
        #region "Attributes"

        AddCaseWorker caseWorker = null;
        Am.Ahv.BusinessManager.Casemgmt.SuppressedCase suppressedCase = null;
        Am.Ahv.BusinessManager.reports.BalanceList balanceListManager = null;
        Validation val = new Validation();

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        bool noRecord;

        public bool NoRecord
        {
            get { return noRecord; }
            set { noRecord = value; }
        }

        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        int regionId;

        public int RegionId
        {
            get { return regionId; }
            set { regionId = value; }
        }

        int assignedToId;

        public int AssignedToId
        {
            get { return assignedToId; }
            set { assignedToId = value; }
        }

        int suburbId;

        public int SuburbId
        {
            get { return suburbId; }
            set { suburbId = value; }
        }

        int customerStatus;

        public int CustomerStatus
        {
            get { return customerStatus; }
            set { customerStatus = value; }
        }

        int assetType;

        public int AssetType
        {
            get { return assetType; }
            set { assetType = value; }
        }

        //bool allSuburbFlag;

        //public bool AllSuburbFlag
        //{
        //    get { return allSuburbFlag; }
        //    set { allSuburbFlag = value; }
        //}

        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }

        bool isError = false;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        decimal totalRentBalance = 0;
        decimal totalHB = 0;
        decimal totalADVHB = 0;
        decimal totalSLB = 0;
        decimal totalNetRentBalance = 0;
        decimal totalSum = 0; 
        #endregion

        #region"Events"
    
        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
               // Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            ResetMessage();

            //setMessage("Success Message", false);
            if (!IsPostBack)
            {
                SetDefaultPagingAttributes();
               
                ViewState["SortDirection"] = "ASC";
                ViewState["SortExpression"] = "RENTBALANCE";
                PopulateGridView(0, 0, 0, 0, 0, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                DownloadExcel();
               
            }

        }

        #endregion
        #region grid row data bound event
        protected void pgvBalanceList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                decimal price = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "RentBalance"));
                totalRentBalance += price;
                decimal pricetotalHB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "ESTHB"));
                totalHB += pricetotalHB;
                decimal pricetotalADVHB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "ADVHB"));
                totalADVHB += pricetotalADVHB;
                decimal pricetotalSLB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "SALESLEDGERBALANCE"));
                totalSLB += pricetotalSLB;
                // decimal pricetotalNetRentBalance = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NetRentBalance"));

                // decimal pricetotalSum = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Total"));
                //totalSum += pricetotalSum;

                //  Label lblNetRentBalance = (Label)e.Row.FindControl("lblNetRentBalance");
                decimal grossRentBalance = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NETARREARS"));
                // lblNetRentBalance.Text = val.SetBracs(grossRentBalance.ToString());
                //lblNetRentBalance.ForeColor = this.SetForeColor(grossRentBalance.ToString());
                totalNetRentBalance += grossRentBalance;

                Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                decimal pricetotalSum = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TOTAL"));
                // lblTotal.Text = val.SetBracs(pricetotalSum.ToString());
                // lblTotal.ForeColor = this.SetForeColor(pricetotalSum.ToString());
                totalSum += pricetotalSum;
                //total += Convert.ToDecimal(e.Row .Cells[3].Text);
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalRentBalance = (Label)e.Row.FindControl("lblTotalRentBalance");
                lblTotalRentBalance.Text = val.SetBracs(totalRentBalance.ToString());
                lblTotalRentBalance.ForeColor = this.SetForeColor(totalRentBalance.ToString());
                Label lblTotalHB = (Label)e.Row.FindControl("lblTotalHB");
                lblTotalHB.Text = val.SetBracs(totalHB.ToString());
                lblTotalHB.ForeColor = this.SetForeColor(totalHB.ToString());
                Label lblTotalADVHB = (Label)e.Row.FindControl("lblTotalADVHB");
                lblTotalADVHB.Text = val.SetBracs(totalADVHB.ToString());
                lblTotalADVHB.ForeColor = this.SetForeColor(totalADVHB.ToString());
                Label lblTotalNetRentBalance = (Label)e.Row.FindControl("lblTotalNetRentBalance");
                lblTotalNetRentBalance.Text = val.SetBracs(totalNetRentBalance.ToString());
                lblTotalNetRentBalance.ForeColor = this.SetForeColor(totalNetRentBalance.ToString());
                Label lblTotalSLB = (Label)e.Row.FindControl("lblTotalSLB");
                lblTotalSLB.Text = val.SetBracs(totalSLB.ToString());
                lblTotalSLB.ForeColor = this.SetForeColor(totalSLB.ToString());
                // val.SetBracs(totalSLB.ToString());
                Label lblSumTotal = (Label)e.Row.FindControl("lblSumTotal");
                lblSumTotal.Text = val.SetBracs(totalSum.ToString());
                lblSumTotal.ForeColor = this.SetForeColor(totalSum.ToString());
            }
        }
        #endregion
        #endregion

        #region"Populate Data"

        #region"Populate GridView"

        public void PopulateGridView(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, string sortExpression, string sortDir)
        {
            try
            {
                balanceListManager = new BusinessManager.reports.BalanceList();
                List<Am.Ahv.Entities.AM_SP_GetBalanceListExportToExcel_Result> Dataset = balanceListManager.GetBalanceListExportToExcel(_caseOwnedBy, _regionId, _suburbId, _customerStatus, _assetType, sortExpression, sortDir).ToList();
                Session["BalanceListDS"] = Dataset;
                pgvBalanceList.DataSource = Dataset;
                pgvBalanceList.DataBind();
                SetViewStateValues(Convert.ToString(_regionId), Convert.ToString(_suburbId), _customerStatus, _assetType);
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingGridView, true);
                }
            }
        }

        #endregion

        

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            //PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
            CurrentPage = 1;
            DbIndex = 0;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            RowIndex = -1;
            RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            SetViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void SetViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageSuppressedCases] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexSuppressedCases] = DbIndex;
            ViewState[ViewStateConstants.totalPagesSuppressedCases] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsSuppressedCases] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexSuppressedCases] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageSuppressedCases] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsSuppressedCases] = CurrentRecords;

        }

        public void SetViewStateValues(string _regionId, string _suburbId, int _customerStatus, int _assetType)
        {
            ViewState[ViewStateConstants.regionIdSuppressedCases] = _regionId;
            ViewState[ViewStateConstants.suburbIdSuppressedCases] = _suburbId;
            ViewState[ViewStateConstants.allRegionFlagSuppressedCases] = _customerStatus;
            ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases] = _assetType;
        }
        #endregion    

        #region"Set Rent Balance"

        public string SetRentBalance(string rent)
        {
            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) == 0.0)
                {
                    return "N/A";
                }
                else if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£" + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£" + Math.Round(Convert.ToDouble(rent), 2).ToString() + ")";
                }

            }
            else
            {
                return string.Empty;
            }

        }  
        
        #endregion

        #region"Set Next HB"

        public string SetNextHB(string rent)
        {

            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) == 0.0)
                {
                    return "N/A";
                }
                else if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£ " + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£ " + Math.Round(Convert.ToDouble(rent), 2).ToString() + ")";
                }
            }
            else
            {
                return "N/A";
            }
        }

        #endregion

        

        #region"Set As At Date"

        public string SetAsAtDate(string date)
        {
            return date;
        }

        #endregion

        #endregion

        #region"Getters"

        #region"Get ViewState Values"

        public void GetViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageSuppressedCases]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexSuppressedCases]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesSuppressedCases]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsSuppressedCases]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsSuppressedCases]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexSuppressedCases]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageSuppressedCases]);
        }

        #endregion

        #region"Get Search Indexes"

        public void GetSearchIndexes()
        {
            RegionId = Convert.ToInt32(ViewState[ViewStateConstants.regionIdSuppressedCases]);
            //  AssignedToId = Convert.ToInt32(ViewState[ViewStateConstants.assignedToSuppressedCases]);
            SuburbId = Convert.ToInt32(ViewState[ViewStateConstants.suburbIdSuppressedCases]);
            CustomerStatus = Convert.ToInt32(ViewState[ViewStateConstants.allRegionFlagSuppressedCases]);
            assetType = Convert.ToInt32(ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases]);
            //allSuburbFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allSuburbFlagSuppressedCases]);
        }

        #endregion

        #endregion

        #region"Functions"
        
        #region"Set Message"

        public void SetMessage(string str, bool IsError)
        {
            if (IsError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Script Building"

        public void ScriptBuilding()
        {
            LinkButton lbtn = new LinkButton();
            Label lbl = new Label();
            string url = string.Empty;

            for (int i = 0; i < pgvBalanceList.Rows.Count; i++)
            {
                lbtn = (LinkButton)pgvBalanceList.Rows[i].FindControl("lbtnName");
                lbl = (Label)pgvBalanceList.Rows[i].FindControl("lblCaseId");
                url = PathConstants.javaScriptCasehistory + lbl.Text + "&tid=" + lbtn.CommandArgument + "&cmd=scase";
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        #endregion
              

        #endregion

        #region"Set Single Value Bracs and Color"
        public string SetBracs(string value)
        {
            return val.SetBracs(value);
        }


        public System.Drawing.Color SetForeColor(string value)
        {
            return val.SetForeColor(value);
        }

        #endregion

        #region"Set Customer Name"

        public string SetCustomerName(string customerName1, string customerName2, string jointTenancyCount)
        {
            if (jointTenancyCount == "1")
            {
                return customerName1;
            }
            else
            {
                return customerName2 + " & " + customerName1;
            }
        }

        #endregion

        #region Export to excel

        public void DownloadExcel()
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                string fileName = "BalanceList_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year) + ".xls";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                Response.Charset = "";
                Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
                Response.Write("<head>");
                Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name>Report Data</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write(" <x:Alignment x:Horizontal=\"Center\" x:Vertical=\"Center\" " + "x:WrapText=\"true\"/>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter strWrite = new StringWriter();
                HtmlTextWriter htmWrite = new HtmlTextWriter(strWrite);
                HtmlForm htmfrm = new HtmlForm();
               
                pgvBalanceList.Parent.Controls.Add(htmfrm);
                //foreach (GridViewRow row in pgvBalanceList.Rows)
                //{
                //    row.Height =40;
                //}
                htmfrm.Attributes["runat"] = "server";
                htmfrm.Controls.Add(pgvBalanceList);
                htmfrm.RenderControl(htmWrite);
                //style to format numbers to string
                string style = "<style> .textmode{mso-number-format:\\@;}</style>";
                Response.Write(style);
                Response.Write(strWrite.ToString());
                Response.Flush();
                Response.End();
                      
            }
            catch (ThreadAbortException)
            {
                //If the download link is pressed we will get a thread abort.
            }
        }
        #endregion
    }
}

