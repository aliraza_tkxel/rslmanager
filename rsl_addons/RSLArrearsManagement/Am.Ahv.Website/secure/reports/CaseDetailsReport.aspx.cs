﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.reports;
using Am.Ahv.BusinessManager.Casemgmt;
using Microsoft.Reporting.WebForms;
using Am.Ahv.Utilities.utitility_classes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.letters;


namespace Am.Ahv.Website.secure.reports
{
    public partial class CaseDetailsReport : PageBase
    {
   
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                //Disable back btn if the case is inactive
                if (Request.QueryString["casetype"] == "inactive")
                {
                    btnBack.Visible = false;
                }

                LoadRepeaterValues();
            }
        }
        List<AM_CaseHistory> GlobalCaseHistoryList;

        public List<AM_CaseHistory> globalCaseHistoryList
        {
            get 
            {
                //if (Session[SessionConstants.GlobalCaseHistoryList] != null)
                //{
                //    return Session[SessionConstants.GlobalCaseHistoryList] as List<AM_CaseHistory>;
                //}
                //else
                //{
                    Session[SessionConstants.GlobalCaseHistoryList] = GetCaseHistoryList();
                    return GetCaseHistoryList();
                //}
                
            }
            set 
            {
                GlobalCaseHistoryList = value;
                Session[SessionConstants.GlobalCaseHistoryList] = value;
            }
        }
        private List<AM_CaseHistory> GetCaseHistoryList()
        {
            Am.Ahv.BusinessManager.reports.CaseDetailsReport CReport = new BusinessManager.reports.CaseDetailsReport();
            List<AM_CaseHistory> Chistory = CReport.GetCaseHistoryByCaseId(base.GetCaseId());
            Chistory = LoadDate(Chistory);
            return Chistory;
        }
        
       private void LoadRepeaterValues()
       {
           try
           {
            
               repcasedetails.DataSource = globalCaseHistoryList;
               repcasedetails.DataBind();
             
           }
           catch (Exception ex)
           {
               ExceptionPolicy.HandleException(ex, "Exception Policy");
           }
       }
        
       public void LoadCaseActivityRepeater(int CaseHistoryId, Repeater repcaseactivity)
       {
           AM_CaseHistory item = globalCaseHistoryList.Where(ch => ch.CaseHistoryId == CaseHistoryId).ToList().First();
           try
           {
               
               List<AM_Activity> _ActivityList = null;
               //foreach (AM_CaseHistory item in Chistory)
               //{
               _ActivityList = item.AM_Activity.Where(p => p.CaseHistoryId == item.CaseHistoryId).ToList();
               var ActivityList = (from element in _ActivityList
                               orderby element.RecordedDate descending select element).ToList();
               
                   if (ActivityList.Count > 0)
                   {
                       ActivityList = LoadActivityReferences(ActivityList);
                       ActivityList= LoadActivityDocuments(ActivityList);
                       repcaseactivity.DataSource = ActivityList;
                       repcaseactivity.DataBind();
                   }
               //}
           }
           catch (Exception ex)
           {
               ExceptionPolicy.HandleException(ex, "Exception Policy");
           }
       }

       private List<AM_Activity> LoadActivityDocuments(List<AM_Activity> ActivityList)
       {
          
           foreach (AM_Activity am in ActivityList)
           {
               am.ActionId=am.AM_Documents.Count();
           }
           return ActivityList;
       }

       private List<Entities.AM_Activity> LoadActivityReferences(List<Entities.AM_Activity> ActivityList)
       {
           foreach (AM_Activity item in ActivityList)
           {
               
               item.AM_Documents.Count();
               item.AM_ResourceReference.Load();
               item.AM_Resource.AM_LookupCodeReference.Load();
               item.AM_LookupCodeReference.Load();
           }
           return ActivityList;
       }
        //For Loading appropriate Date
       private List<Entities.AM_CaseHistory> LoadDate(List<Entities.AM_CaseHistory> Chistory)
       {
           foreach (AM_CaseHistory item in Chistory)
           {

               item.ModifiedDate = DateOperations.AddDate(item.AM_Action.RecommendedFollowupPeriod, item.AM_Action.AM_LookupCode1.CodeName, DateTime.Now);
           }
           return Chistory;
       }
       public string DocumentCount(string Isdocument, string CaseHistoryId)
       {
           if (Isdocument.Equals("Yes"))
           {
               Am.Ahv.BusinessManager.reports.CaseDetailsReport Creport = new BusinessManager.reports.CaseDetailsReport();
               int id = int.Parse(CaseHistoryId);
               return Creport.GetDocumentCountByCaseHistory(id).ToString();
           }
           else
           {
              return "0";
           }
       }
       public string FormatDate(string value)
       {
           DateTime date = Convert.ToDateTime(value);
           return date.ToString("dd/MM/yyyy HH:mm");
       }

       protected void repcasedetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
       {
           try
           {
               if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
               {
                   Repeater rep = (Repeater)e.Item.FindControl("repcaseactivity");
                   Label Lb = (Label)e.Item.FindControl("lblcasehistoryid");
                   int CaseHistoryId = int.Parse(Lb.Text.ToString());
                   LoadCaseActivityRepeater(CaseHistoryId, rep);
               }
           }
           catch (Exception ex)
           {
               ExceptionPolicy.HandleException(ex, "Exception Policy");
           }
       }

       

       #region"Load Employee Name"

       public string LoadEmployeeName(string resourceId)
       {
           if (resourceId.Equals(string.Empty))
           {
               return string.Empty;
           }
           else
           {
               LetterResourceArea letterMgr = new LetterResourceArea();
               return letterMgr.GetEmployeeName(int.Parse(resourceId));
           }
       }

       #endregion

       #region"Check Standard Letter Attached"

       public string CheckStandardLetterAttached(string value)
       {
           if (value == "True")
           {
               return "Yes";
           }
           else
           {
               return "No";
           }
       }

       #endregion
      
       #region"Btn Back Click"

       protected void btnBack_Click(object sender, EventArgs e)
       {
           Response.Redirect(PathConstants.casehistory, false);
       }
       #endregion 
       
    

       public Boolean checkActivity(string ActionId, string StatusId, string CaseId, string CaseHistoryId)
       {
           LetterResourceArea letterMgr = new LetterResourceArea();
           return Convert.ToBoolean(letterMgr.ActivityChecker(ActionId,StatusId,CaseId,CaseHistoryId));
           
       }

        public string checkIfEmpty(string value)
        {
            if (value == "" || value == null)
            {
              
                return "false";
            }
            else
            {
                
                return "true";
            }

        }

    }
}