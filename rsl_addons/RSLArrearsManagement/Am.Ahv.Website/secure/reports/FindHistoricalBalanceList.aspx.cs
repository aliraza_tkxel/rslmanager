﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;
using System.Drawing;
using Am.Ahv.Utilities.utitility_classes;


namespace Am.Ahv.Website.secure.reports
{
    public partial class FindHistoricalBalanceList : PageBase
    {

        Am.Ahv.BusinessManager.reports.BalanceList HistoricalBalanceListManager = new Am.Ahv.BusinessManager.reports.BalanceList();
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckHistoricalBalanceList();
        }

        public void CheckHistoricalBalanceList()
        {
            try
            {
                int count = 0;
                Am.Ahv.BusinessManager.reports.BalanceList HistoricalBalanceListManager = new Am.Ahv.BusinessManager.reports.BalanceList();
                int total = HistoricalBalanceListManager.FindHistoricalBalanceListCount();                

                for (int i = 0; i < total; i += 100)
                {
                    var historicalList = HistoricalBalanceListManager.FindHistoricalBalanceList(i, 100);
                    if (historicalList == null)
                    {
                        break;
                    }
                    if (historicalList.Count == 0)
                    {
                        break;
                    }
                    foreach (var temp in historicalList)                    
                    {
                        string currentMonth = DateOperations.GetMonthName((temp.AccountTimeStamp.Value.Month - 1));
                        int currentYear = temp.AccountTimeStamp.Value.Year;

                        if (!HistoricalBalanceListManager.CheckHistoricalTable(Convert.ToInt32(temp.CUSTOMERID), Convert.ToInt32(temp.TenancyId), currentMonth, currentYear))
                        {                            
                            // Am.Ahv.BusinessManager.reports.BalanceList newList = new BusinessManager.reports.BalanceList();
                            AM_HistoricalBalanceList newList = new AM_HistoricalBalanceList();
                            newList.CustomerId = Convert.ToInt32(temp.CUSTOMERID);
                            newList.HB = Convert.ToDouble(temp.HB);
                            newList.TenancyId = Convert.ToInt32(temp.TenancyId);
                            newList.SLB = Convert.ToDouble(temp.TotalCost);
                            newList.RentBalance = Convert.ToDouble(temp.RentBalance);
                            newList.CreatedDate = DateTime.Now;
                            newList.month = currentMonth;
                            newList.year = currentYear;
                            newList.AccountTimeStamp = temp.AccountTimeStamp.Value;
                            HistoricalBalanceListManager.AddHistoricalBalanceList(newList);
                            count++;
                        }
                    }
                }

                string emailBody = "It is to notify you that Historical Balance List schedule job has been completed successfully and has <br />identified " + count + " customers." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                if (PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailSubjectFindHistoricalBalanceList, emailBody))
                {
                    SetMessage(UserMessageConstants.successFindHistoricalBalanceList, false);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(0, true, UserMessageConstants.EmailFailureSubjectFindHistoricalBalanceList, UserMessageConstants.FailureEmailBodyFindHistoricalBalanceList);
                    SetMessage(UserMessageConstants.ErrorFindHistoricalBalanceList, true);
                }
                catch (Exception exInner)
                {
                    ExceptionPolicy.HandleException(exInner, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                }        
            }
        }

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
    }
}
