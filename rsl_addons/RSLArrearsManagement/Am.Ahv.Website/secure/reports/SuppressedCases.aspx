﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="SuppressedCases.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.SuppressedCases" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlCaseList" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>

 <table width="100%">        
        <tr >
            <td class="tdClass" colspan="6" style=" padding-left:10px" >
                <asp:Label ID="lblMySuppressedCases" runat="server" Font-Bold="true" ForeColor="Black" Font-Size="Medium" Text="Suppressed Cases"></asp:Label>
            </td>
        </tr>
        <tr >
            <td class="tdClass" colspan="6" style=" padding-left:25px">
                <asp:UpdatePanel ID="updpnlDropDown" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>                    
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblAssignedTo" runat="server" Text="Assigned to:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAssignedTo" AutoPostBack="true" Width="310px" runat="server" 
                                onselectedindexchanged="ddlAssignedTo_SelectedIndexChanged">                                
                            </asp:DropDownList>                            
                        </td>
                    </tr>                    
                    <tr >
                        <td>
                            <asp:Label ID="lblRegion" runat="server" Text="Patch:"></asp:Label>                            
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRegion" AutoPostBack="true" Width="310px" runat="server" 
                                onselectedindexchanged="ddlRegion_SelectedIndexChanged">                                
                            </asp:DropDownList>                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblSuburb" runat="server" Text="Scheme:"></asp:Label>                            
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSuburb" AutoPostBack="false" Width="310px" runat="server">                                
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                            <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                onclick="btnSearch_Click" />        
                        </td>
                    </tr>
                </table>   
               
                </ContentTemplate>
                </asp:UpdatePanel>
                <br />              
            </td>            
        </tr>                      
        <tr>
            <td colspan="6" valign="top" class="tdClass" style=" padding-left:20px; padding-right:15px" >
               <asp:UpdatePanel ID="updpnlSuppressedCases" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
               <asp:Panel ID="pnlGridSuppressedCases" runat="server" Width="100%">
                    
                        
                <cc1:PagingGridView ID="pgvSuppressedCases" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" 
                                    EmptyDataRowStyle-HorizontalAlign="Center" RowStyle-Wrap="true" AllowPaging="false" GridLines="None" runat="server" AutoGenerateColumns="false" width="100%" OnSorting="pgvSuppressedCases_sorting">
                    <Columns>                    
                        <asp:TemplateField HeaderText="Name:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true" SortExpression="CustomerName" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="15%" Height="25px" HorizontalAlign="Left" />
                            <ItemTemplate>                            
                              <asp:Label ID="lblCaseId" runat="server" Text='<%#Eval("CaseId") %>' Visible="false"></asp:Label>
                              <asp:Label ID="lblCustomerId" Visible="false" runat="server" Text='<%#Eval("customerId") %>'></asp:Label>
                                <asp:LinkButton ID="lbtnName" OnClick="lbtnName_Click" runat="server" CommandArgument='<%#Eval("TenancyId") %>' Text='<%# this.SetCustomerName(Eval("CustomerName").ToString(), Eval("CustomerName2").ToString(), Eval("JointTenancyCount").ToString()) %>' ForeColor="Black"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true" SortExpression="CustomerAddress" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="25%" Height="25px" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("CustomerAddress") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Telephone:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="Telephone" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="15%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>
                                <asp:Label ID="lblTelephone" runat="server" Text='<%#Eval("Telephone") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Suppressed:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="SuppressedDate" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="15%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>                                
                                <asp:Label ID="lblDateSuppressed" runat="server" Text='<%#Eval("SuppressedDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="By:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="SuppressedBy" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>
                                <asp:Label ID="lblBy" runat="server" Text='<%#Eval("SuppressedBy") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reason:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="SuppressedReason" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="20%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>
                                <asp:Label ID="lblReason" runat="server" Text='<%# Eval("SuppressedReason")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <%-- <asp:TemplateField>
                            <ItemStyle Width="10%" />
                            <ItemTemplate>
                                <asp:Button ID="btnViewCase" runat="server" Text="View Case" />
                            </ItemTemplate>
                        </asp:TemplateField>    --%>                    
                    </Columns>
                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="tableHeader" />
                    <PagerSettings Mode="NumericFirstLast" />
                    <RowStyle Wrap="True" />
                </cc1:PagingGridView>

                
                </asp:Panel>
                 <table width="100%" class="tableFooter">
                    <tr>
                        <td width="20%" style="padding-left:20px">
                            <asp:Label ID="lblRecords" runat="server" Text="Records"></asp:Label>&nbsp;
                            <asp:Label ID="lblCurrentRecords" runat="server"></asp:Label>&nbsp;
                            <asp:Label ID="lblOf" runat="server" Text="of"></asp:Label>&nbsp;
                            <asp:Label ID="lblTotalRecords" runat="server" ></asp:Label>
                        </td>
                        <td width="20%" style="padding-left:20px">
                             <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>&nbsp;
                             <asp:Label ID="lblCurrentPage" runat="server" ></asp:Label>&nbsp;
                             <asp:Label ID="lblOfPage" runat="server" Text="of"></asp:Label>&nbsp;
                             <asp:Label ID="lblTotalPages" runat="server" ></asp:Label>
                        </td>
                        <td width="10%" style="padding-left:10px">                 
                            <asp:LinkButton ID="lbtnPrevious" runat="server" Text="< Previous" 
                                onclick="llbtnPrevious_Click"></asp:LinkButton>
                        </td>
                        <td width="10%" style="padding-left:10px">
                            <asp:LinkButton ID="lbtnNext" runat="server" Text="Next >" 
                                onclick="llbtnNext_Click"></asp:LinkButton>
                        </td>
                        <td width="15%">
                        </td>
                        <td width="15%">
                            <asp:Label ID="lblRecordsPerPage" runat="server" Text="Records per page:"></asp:Label>
                        </td>
                        <td width="5%">
                            <asp:DropDownList ID="ddlRecordsPerPage" runat="server" AutoPostBack="true" 
                                onselectedindexchanged="ddlRecordsPerPage_SelectedIndexChanged">                                
                            </asp:DropDownList>
                        </td>                 
                    </tr>
                </table>  
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <span class="Error">Please wait...</span>
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
                </ContentTemplate>
                    </asp:UpdatePanel>               
            </td>
        </tr>        
        <tr>
            <td colspan="6" class="tdClass">
               <br />
               <br />
            </td>            
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
