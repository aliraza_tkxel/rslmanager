﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using Am.Ahv.BusinessManager.Casemgmt;
using System.IO;
using Am.Ahv.Website.pagebase;
using System.Drawing;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.Customers;
using System.Text.RegularExpressions;

namespace Am.Ahv.Website.secure.reports
{
    public partial class BalanceListReport : PageBase
    {
        #region "Attributes"

        AddCaseWorker caseWorker = null;
        Am.Ahv.BusinessManager.Casemgmt.SuppressedCase suppressedCase = null;
        Am.Ahv.BusinessManager.reports.BalanceList balanceListManager = null;
        Validation val = new Validation();

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        bool noRecord;

        public bool NoRecord
        {
            get { return noRecord; }
            set { noRecord = value; }
        }

        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        int regionId;

        public int RegionId
        {
            get { return regionId; }
            set { regionId = value; }
        }

        int assignedToId;

        public int AssignedToId
        {
            get { return assignedToId; }
            set { assignedToId = value; }
        }
        string rentBalanceFrom;

        public string RentBalanceFrom
        {
            get { return rentBalanceFrom; }
            set { rentBalanceFrom = value; }
        }
        string rentBalanceTo;

        public string RentBalanceTo
        {
            get { return rentBalanceTo; }
            set { rentBalanceTo = value; }
        }
        int suburbId;

        public int SuburbId
        {
            get { return suburbId; }
            set { suburbId = value; }
        }

        int customerStatus;

        public int CustomerStatus
        {
            get { return customerStatus; }
            set { customerStatus = value; }
        }

        int assetType;

        public int AssetType
        {
            get { return assetType; }
            set { assetType = value; }
        }

        //bool allSuburbFlag;

        //public bool AllSuburbFlag
        //{
        //    get { return allSuburbFlag; }
        //    set { allSuburbFlag = value; }
        //}

        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }

        bool isError = false;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        decimal totalRentBalance = 0;
        decimal totalHB = 0;
        decimal totalADVHB = 0;
        decimal totalSLB = 0;
        decimal totalNetRentBalance = 0;
        decimal totalSum = 0;
        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
               // RedirectToLoginPage();
            }
            ResetMessage();

            //setMessage("Success Message", false);
            if (!IsPostBack)
            {
                SetDefaultPagingAttributes();
                InitLookup();
                ViewState["SortDirection"] = "DESC";
                ViewState["SortExpression"] = "RENTBALANCE ";
                PopulateGridView(0, 0, 0, 0, 0, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), string.Empty, string.Empty);
                SetCurrentRecordsCount(0, 0, 0, 0, 0, string.Empty, string.Empty);
                SetPagingLabels();
            }

        }

        #endregion

        #region"LBtn Next Click"

        protected void llbtnNext_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            IncreasePaging();
        }

        #endregion

        #region"Lbtn Previous Click"

        protected void llbtnPrevious_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            ReducePaging();
        }

        #endregion

        #region"lbtn First Click"

        protected void lbtnFirst_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            GoToFirstPage();
        }

        #endregion

        #region"lbtn Last Click"

        protected void lbtnLast_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            GoToLastpage();
        }

        #endregion
        #region"DDL Case Own By Selected Index Changed"

        protected void ddlAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                populateRegions(Convert.ToInt32(ddlAssignedTo.SelectedValue));
                PopulateSuburbs();
                updpnlDropDown.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionAgainstSuburbFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Populate Lookups"

        //public void initRegionLookup()
        public void populateRegions(int caseOwnedById)
        {
            try
            {
                Am.Ahv.BusinessManager.Casemgmt.CaseList caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();

                ddlRegion.DataSource = caseList.getRegionList(caseOwnedById);
                ddlRegion.DataTextField = "Patch";
                ddlRegion.DataValueField = "PatchId";
                ddlRegion.DataBind();
                ddlRegion.Items.Add(new ListItem("All", "-1"));
                ddlRegion.SelectedValue = "-1";
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingRegionFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Populate Subusrbs"

        public void PopulateSuburbs()
        {
            if (ddlRegion.SelectedValue != "-1")
            {
                caseWorker = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
                // Am.Ahv.BusinessManager.Casemgmt.CaseList caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();
                ddlSuburb.DataSource = caseWorker.GetAllSuburbs(Convert.ToInt32(ddlRegion.SelectedValue));
                ddlSuburb.DataTextField = ApplicationConstants.schememname;
                ddlSuburb.DataValueField = ApplicationConstants.schemeId;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";
            }
            else
            {
                ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                ddlSuburb.DataTextField = string.Empty;
                ddlSuburb.DataValueField = string.Empty;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";
            }
        }

        #endregion

        #region"DDL Region Selected Index Changed"

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlRegion.SelectedValue != "-1")
                {
                    caseWorker = new AddCaseWorker();

                    ddlSuburb.DataSource = caseWorker.GetSuburbsListByRegion(ddlRegion.SelectedItem.Text, Convert.ToInt32(ddlAssignedTo.SelectedValue));
                    ddlSuburb.DataTextField = ApplicationConstants.schememname;
                    ddlSuburb.DataValueField = ApplicationConstants.schemeId;
                    ddlSuburb.DataBind();

                    ddlSuburb.Items.Add(new ListItem("All", "-1"));
                    ddlSuburb.SelectedValue = "-1";

                }
                else
                {
                    ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                    ddlSuburb.DataTextField = string.Empty;
                    ddlSuburb.DataValueField = string.Empty;
                    ddlSuburb.DataBind();
                    ddlSuburb.Items.Add(new ListItem("All", "-1"));
                    ddlSuburb.SelectedValue = "-1";
                }
                updpnlDropDown.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSuburbAndCaseOwnedBy, true);
                }
            }
        }

        #endregion

        #region"Btn Search Click"

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SetDefaultPagingAttributes();
                GetViewStateValues();
                Search();
                SetPagingLabels();
                updpnlSuppressedCases.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionButtonSearch, true);
                }
            }
        }

        #region"Search"

        private void Search()
        {
            RegionId = (ddlRegion.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlRegion.SelectedValue);
            SuburbId = (ddlSuburb.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlSuburb.SelectedValue);
            CustomerStatus = (ddlCustomerStatus.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlCustomerStatus.SelectedValue);
            AssetType = (ddlAssetType.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlAssetType.SelectedValue);
            AssignedToId = (ddlAssignedTo.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlAssignedTo.SelectedValue);
            RentBalanceFrom = txtFrom.Text.ToString();
            RentBalanceTo = txtTo.Text.ToString();
            PopulateGridView(AssignedToId, RegionId, SuburbId, CustomerStatus, AssetType, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), RentBalanceFrom, RentBalanceTo);
            SetCurrentRecordsCount(AssignedToId, RegionId, SuburbId, CustomerStatus, AssetType, RentBalanceFrom, RentBalanceTo);
        }

        #endregion

        #endregion

        #region" DDL records per Page Selected Index Changed"

        protected void ddlRecordsPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SetDefaultPagingAttributes();
                GetViewStateValues();
                if (pgvBalanceList.Rows.Count > 0)
                {
                    DbIndex = 1;
                    GetSearchIndexes();
                    PopulateGridView(AssignedToId, RegionId, SuburbId, CustomerStatus, assetType, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), RentBalanceFrom, RentBalanceTo);
                    SetCurrentRecordsCount(AssignedToId, RegionId, SuburbId, CustomerStatus, assetType, RentBalanceFrom, RentBalanceTo);
                    SetPagingLabels();
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionRecordsPerPage, true);
                }
            }

        }

        #endregion

        #region"LBTN Name Click"

        protected void lbtnName_Click(object sender, EventArgs e)
        {
            //GridViewRow gvr = ((LinkButton)sender).Parent.Parent as GridViewRow;
            //int index = gvr.RowIndex;

            //LinkButton lbtn = new LinkButton();
            //Label lbl = new Label();
            //string script = string.Empty;
            //lbtn = (LinkButton)pgvBalanceList.Rows[index].FindControl("lbtnName");
            //lbl = (Label)pgvBalanceList.Rows[index].FindControl("lblCaseId");

            //Session.Remove(SessionConstants.TenantId);
            //Session.Remove(SessionConstants.CaseId);

            //base.SetCaseId(Convert.ToInt32(lbl.Text));
            //base.SetTenantId(Convert.ToInt32(lbtn.CommandArgument));
            //string url = PathConstants.casehistory;

            ////lbtn.Attributes.Add("onClick", "window.open('../../secure/general/DownloadFile.aspx','_blank','directories=no,height=250,width=250,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no')");

            ////ScriptManager.RegisterStartupScript(page,
            ////    typeof(Page),
            ////    "Redirect",
            ////    script,
            ////    true);

            ////script = "<script language='text/Javascript'> window.location ='"+url+"';</script>";
            ////ClientScript.RegisterClientScriptBlock(this.GetType(), "window", script);

            //script = "<script language='JavaScript'>";
            ////script += "window.location = '" + url + "'";
            //script += "alert('hello jonnay')";
            //script += "</";
            //script += "script>";


            //ClientScript.RegisterClientScriptBlock(this.GetType(), "test", script);



            ////Response.Redirect(PathConstants.casehistory);
            //// Response.Redirect("<script>window.open('"+url+"');</script>");
        }

        #endregion

        #region"lbtn Open Case Click"

        protected void lbtnOpenCase_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = ((LinkButton)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;

            LinkButton lbtn = new LinkButton();
            Label lbl = new Label();
            Label lblCustomer = new Label();
            lblCustomer = (Label)pgvBalanceList.Rows[index].FindControl("lblCustomerId");
            Session[SessionConstants.CustomerId] = lblCustomer.Text;
            lbtn = (LinkButton)pgvBalanceList.Rows[index].FindControl("lbtnOpenCase");
            if (lbtn.Text == "Open Case")
            {
                Response.Redirect(PathConstants.openNewCaseBalanceListReport + "?cmd=open&id=" + lbtn.CommandArgument, false);
            }
            else
            {
                lbl = (Label)pgvBalanceList.Rows[index].FindControl("lblCaseId");
                if (lbl != null)
                {
                    Response.Redirect(PathConstants.CaseDetailsBalanceListReport + "?cmd=scase&cid=" + lbl.Text + "&tid=" + lbtn.CommandArgument, false);
                }
                else
                {
                    Response.Redirect(PathConstants.openNewCaseBalanceListReport + "?cmd=open&id=" + lbtn.CommandArgument, false);
                }
            }
        }

        #endregion

        #region"lbtn Case Detail Click"

        protected void lbtnCaseDetail_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = ((LinkButton)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;

            LinkButton lbtn = new LinkButton();
            Label lbl = new Label();
            lbtn = (LinkButton)pgvBalanceList.Rows[index].FindControl("lbtnOpenCase");
            lbl = (Label)pgvBalanceList.Rows[index].FindControl("lblCaseId");
            if (lbl != null)
            {
                Response.Redirect(PathConstants.CaseDetailsBalanceListReport + "?cmd=scase&cid=" + lbtn.CommandArgument + "&tid=" + lbl.Text, false);
            }
            else
            {
                Response.Redirect(PathConstants.openNewCaseBalanceListReport + "?cmd=open&id=" + lbtn.CommandArgument, false);
            }
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Populate GridView"

        public void PopulateGridView(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, int index, int pageSize, string sortExpression, string sortDir, string rentBalanceFrom, string rentBalanceTo)
        {
            try
            {
                balanceListManager = new BusinessManager.reports.BalanceList();
                List<Am.Ahv.Entities.AM_SP_GETRENTBALANCELIST_Result> Dataset = balanceListManager.GetBalanceListReport(_caseOwnedBy, _regionId, _suburbId, _customerStatus, _assetType, index, pageSize, sortExpression, sortDir, rentBalanceFrom, rentBalanceTo).ToList();
                Session["BalanceListDS"] = Dataset;
                pgvBalanceList.DataSource = Dataset;
                pgvBalanceList.DataBind();
                SetViewStateValues(Convert.ToString(_regionId), Convert.ToString(_suburbId), _customerStatus, _assetType, rentBalanceFrom, rentBalanceTo);
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingGridView, true);
                }
            }
        }

        #endregion

        #region"Populate Lookups"

        public void InitLookup()
        {
            try
            {
                caseWorker = new AddCaseWorker();
                suppressedCase = new SuppressedCase();
                Customers customer = new Customers();
                Am.Ahv.BusinessManager.reports.BalanceList historicalBalanceList = new Am.Ahv.BusinessManager.reports.BalanceList();
                ddlAssignedTo.DataSource = suppressedCase.GetAssignedToList();
                ddlAssignedTo.DataTextField = "EmployeeName";
                ddlAssignedTo.DataValueField = "ResourceId";
                ddlAssignedTo.DataBind();
                ddlAssignedTo.Items.Insert(0, new ListItem("All", "-1"));

                ddlAssignedTo.SelectedValue = "-1";
                ddlRegion.DataSource = caseWorker.GetAllRegion();
                ddlRegion.DataTextField = ApplicationConstants.regionname;
                ddlRegion.DataValueField = ApplicationConstants.regionId;
                ddlRegion.DataBind();
                ddlRegion.Items.Insert(0, new ListItem("All", "-1"));
                ddlRegion.SelectedValue = "-1";

                ddlSuburb.Items.Insert(0, new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";

                ddlCustomerStatus.DataSource = customer.GetCustomerStatus();
                ddlCustomerStatus.DataTextField = "CustomerStatusDescription";
                ddlCustomerStatus.DataValueField = "CustomerStatusId";
                ddlCustomerStatus.DataBind();
                ddlCustomerStatus.Items.Insert(0, new ListItem("All", "-1"));
                ddlCustomerStatus.SelectedValue = "-1";

                ddlAssetType.DataSource = historicalBalanceList.GetAssetType();
                ddlAssetType.DataTextField = "AssetTypeDescription";
                ddlAssetType.DataValueField = "AssetTypeId";
                ddlAssetType.DataBind();
                ddlAssetType.Items.Insert(0, new ListItem("All", "-1"));
                ddlAssetType.SelectedValue = "-1";

                ddlRecordsPerPage.DataSource = ApplicationConstants.recordsPerPageSuppressedCases;
                ddlRecordsPerPage.DataBind();
                ddlRecordsPerPage.SelectedValue = ApplicationConstants.pagingPageSizeCaseList.ToString();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingRegion, true);
                }
            }
        }

        #endregion

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            //PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
            CurrentPage = 1;
            DbIndex = 1;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            RowIndex = -1;
            RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            SetViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void SetViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageSuppressedCases] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexSuppressedCases] = DbIndex;
            ViewState[ViewStateConstants.totalPagesSuppressedCases] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsSuppressedCases] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexSuppressedCases] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageSuppressedCases] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsSuppressedCases] = CurrentRecords;

        }

        public void SetViewStateValues(string _regionId, string _suburbId, int _customerStatus, int _assetType, string _rentBalanceFrom, string _rentBalanceTo)
        {
            ViewState[ViewStateConstants.regionIdSuppressedCases] = _regionId;
            ViewState[ViewStateConstants.suburbIdSuppressedCases] = _suburbId;
            ViewState[ViewStateConstants.allRegionFlagSuppressedCases] = _customerStatus;
            ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases] = _assetType;
            ViewState[ViewStateConstants.rentBalanceFrom] = _rentBalanceFrom;
            ViewState[ViewStateConstants.rentBalanceTo] = _rentBalanceTo;
        }

        #endregion

        #region"Set Paging Labels"

        public void SetPagingLabels()
        {
            try
            {

                int count = TotalRecords;

                if (count > Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    SetCurrentRecordsDisplayed();
                    SetTotalPages(count);
                    lblCurrentRecords.Text = (CurrentRecords).ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    lblCurrentRecords.Text = pgvBalanceList.Rows.Count.ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = CurrentPage.ToString();
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = false;
                    lbtnFirst.Enabled = false;
                    lbtnLast.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    lbtnPrevious.Enabled = false;
                    lbtnFirst.Enabled = false;
                }
                else if (CurrentPage < TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnLast.Enabled = true;
                    lbtnPrevious.Enabled = true;
                    lbtnFirst.Enabled = true;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnLast.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    lbtnNext.Enabled = true;
                    lbtnLast.Enabled = true;
                }

                SetViewStateValues();
            }
            catch (FileLoadException flException)
            {
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"Set Total Pages"

        public void SetTotalPages(int count)
        {
            try
            {
                if (count % Convert.ToInt32(ddlRecordsPerPage.SelectedValue) > 0)
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue)) + 1;
                }
                else
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                }
            }
            catch (DivideByZeroException divide)
            {
                IsException = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionTotalPages, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Displayed"

        public void SetCurrentRecordsDisplayed()
        {
            try
            {
                if (pgvBalanceList.Rows.Count == Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    CurrentRecords = CurrentPage * pgvBalanceList.Rows.Count;
                }
                else if (CurrentPage == TotalPages)
                {
                    CurrentRecords = TotalRecords;
                }
                else if (RemoveFlag == true)
                {
                    CurrentRecords -= 1;
                }
                else
                {
                    CurrentRecords += pgvBalanceList.Rows.Count;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionCurrentRecords, true);
                }
            }

        }

        #endregion

        #region"Set Total Records Count"

        public void SetCurrentRecordsCount(int _caseOwnedBy, int _regionId, int _suburbId, int _customerStatus, int _assetType, string rentBalanceFrom, string rentBalanceTo)
        {
            try
            {
                balanceListManager = new BusinessManager.reports.BalanceList();
                TotalRecords = balanceListManager.GetTotalBalanceListRecordsCount(_caseOwnedBy, _regionId, _suburbId, _customerStatus, _assetType, rentBalanceFrom, rentBalanceTo);
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionTotalRecords, true);
                }
            }
        }

        #endregion

        #region"Set Rent Balance"

        public string SetRentBalance(string rent)
        {
            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) == 0.0)
                {
                    return "N/A";
                }
                else if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£" + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£" + Math.Round(Convert.ToDouble(rent), 2).ToString() + ")";
                }

            }
            else
            {
                return string.Empty;
            }

        }

        #endregion

        #region"Set Next HB"

        public string SetNextHB(string rent)
        {

            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) == 0.0)
                {
                    return "N/A";
                }
                else if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£ " + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£ " + Math.Round(Convert.ToDouble(rent), 2).ToString() + ")";
                }
            }
            else
            {
                return "N/A";
            }
        }

        #endregion

        #region"Set Case Open Button"

        public string SetCaseOpenButton(string isactive)
        {
            if (isactive == "True")
            {
                return "Case Detail";
            }
            else
            {
                return "Open Case";
            }
        }

        #endregion

        #region"Set As At Date"

        public string SetAsAtDate(string date)
        {
            return date;
        }

        #endregion

        #endregion

        #region"Getters"

        #region"Get ViewState Values"

        public void GetViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageSuppressedCases]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexSuppressedCases]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesSuppressedCases]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsSuppressedCases]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsSuppressedCases]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexSuppressedCases]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageSuppressedCases]);
        }

        #endregion

        #region"Get Search Indexes"

        public void GetSearchIndexes()
        {
            RegionId = Convert.ToInt32(ViewState[ViewStateConstants.regionIdSuppressedCases]);
            SuburbId = Convert.ToInt32(ViewState[ViewStateConstants.suburbIdSuppressedCases]);
            CustomerStatus = Convert.ToInt32(ViewState[ViewStateConstants.allRegionFlagSuppressedCases]);
            AssetType = Convert.ToInt32(ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases]);
            RentBalanceFrom = ViewState[ViewStateConstants.rentBalanceFrom].ToString();
            RentBalanceTo = ViewState[ViewStateConstants.rentBalanceTo].ToString();
        }

        #endregion

        #endregion

        #region"Functions"
        #region"Go to first page"

        public void GoToFirstPage()
        {
            if (CurrentPage > 1)
            {
                CurrentPage = 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                    lbtnLast.Enabled = true;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex = 1;
                GetSearchIndexes();
                PopulateGridView(Convert.ToInt32(ddlAssignedTo.SelectedValue), RegionId, SuburbId, CustomerStatus, assetType, currentPage, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), RentBalanceFrom, RentBalanceTo);
                SetPagingLabels();
            }
        }

        #endregion
        #region"Reduce Paging"

        public void ReducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                GetSearchIndexes();
                PopulateGridView(Convert.ToInt32(ddlAssignedTo.SelectedValue), RegionId, SuburbId, CustomerStatus, assetType, CurrentPage, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), RentBalanceFrom, RentBalanceTo);
                SetPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void IncreasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                GetSearchIndexes();
                PopulateGridView(Convert.ToInt32(ddlAssignedTo.SelectedValue)  , RegionId, SuburbId, CustomerStatus, assetType, CurrentPage, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), RentBalanceFrom, RentBalanceTo);
                SetPagingLabels();
            }
        }

        #endregion
        #region"Go to last page"

        public void GoToLastpage()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage = TotalPages;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                    lbtnFirst.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex = TotalPages - 1;
                GetSearchIndexes();
                PopulateGridView(Convert.ToInt32(ddlAssignedTo.SelectedValue), RegionId, SuburbId, CustomerStatus, assetType, CurrentPage, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), RentBalanceFrom, RentBalanceTo);
                SetPagingLabels();
            }
        }

        #endregion
        #region"Set Message"

        public void SetMessage(string str, bool IsError)
        {
            if (IsError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Script Building"

        public void ScriptBuilding()
        {
            LinkButton lbtn = new LinkButton();
            Label lbl = new Label();
            string url = string.Empty;

            for (int i = 0; i < pgvBalanceList.Rows.Count; i++)
            {
                lbtn = (LinkButton)pgvBalanceList.Rows[i].FindControl("lbtnName");
                lbl = (Label)pgvBalanceList.Rows[i].FindControl("lblCaseId");
                url = PathConstants.javaScriptCasehistory + lbl.Text + "&tid=" + lbtn.CommandArgument + "&cmd=scase";
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        #endregion


        #endregion

        #region"sorting"

        protected void pgvBalanceList_sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = e.SortDirection;
            }


            SetDefaultPagingAttributes();
            GetViewStateValues();
            GetSearchIndexes();
            PopulateGridView(AssignedToId, RegionId, SuburbId, CustomerStatus, assetType, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), e.SortExpression, GetSortDirection(e.SortExpression).ToString(), RentBalanceFrom, RentBalanceTo);
            SetCurrentRecordsCount(AssignedToId, RegionId, SuburbId, CustomerStatus, assetType, RentBalanceFrom, RentBalanceTo);
            SetPagingLabels();
            //List<Am.Ahv.Entities.AM_SP_GetBalanceList_Result> Dataset = Session["BalanceListDS"] as List<Am.Ahv.Entities.AM_SP_GetBalanceList_Result>;
            //SortGrid<AM_SP_GetBalanceList_Result>(Dataset, e, GetSortDirection(e.SortExpression));

        }


        public void SortGrid<T>(IEnumerable<T> dataSource, GridViewSortEventArgs e, SortDirection sortDirection)
        {
            e.SortDirection = sortDirection;
            IEnumerable<T> set = pagebase.PageBase.SortGrid<T>(dataSource, e);
            pgvBalanceList.DataSource = set;
            pgvBalanceList.DataBind();
        }

        //protected string GridViewSortExpression
        //{
        //    get { return ViewState["SortExpression"] as string ?? string.Empty; }
        //    set { ViewState["SortExpression"] = value; }
        //}



        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            // SortDirection sortDirection = SortDirection.Ascending;

            // Retrieve the last column that was sorted.
            string sortExpression = Convert.ToString(ViewState["SortExpression"]);

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression.ToUpper() == column.ToUpper())
                {
                    string lastDirection = Convert.ToString(ViewState["SortDirection"]);
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        ViewState["SortDirection"] = "DESC";
                        //sortDirection = SortDirection.Descending;
                    }
                    else
                    {
                        ViewState["SortDirection"] = "ASC";
                    }
                }
                else
                {
                    ViewState["SortDirection"] = "DESC";
                }
            }

            // Save new values in ViewState.
            //ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return ViewState["SortDirection"].ToString();
        }

        #endregion

        #region"Set Single Value Bracs and Color"
        public string SetBracs(string value)
        {
            return val.SetBracs(value);
        }

        public string SetBracsAndCurrency(string value)
        {
            return val.SetBracsAndCurrency(value);
        }
        public System.Drawing.Color SetForeColor(string value)
        {
            return val.SetForeColor(value);
        }

        #endregion

        #region"Set Customer Name"

        public string SetCustomerName(string customerName1, string customerName2, string jointTenancyCount)
        {
            if (jointTenancyCount == "1")
            {
                return customerName1;
            }
            else
            {
                return customerName2 + " & " + customerName1;
            }
        }

        #endregion

        protected void pgvBalanceList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                decimal price = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "RENTBALANCETOTAL"));
                totalRentBalance = price;
                decimal pricetotalHB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "ESTTOTAL"));
                totalHB = pricetotalHB;
                decimal pricetotalADVHB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "ADVTOTAL"));
                totalADVHB = pricetotalADVHB;
                decimal pricetotalSLB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "SALESLEDGERBALANCETOTAL"));
                totalSLB = pricetotalSLB;
                // decimal pricetotalNetRentBalance = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NetRentBalance"));

                // decimal pricetotalSum = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Total"));
                //totalSum += pricetotalSum;

                //  Label lblNetRentBalance = (Label)e.Row.FindControl("lblNetRentBalance");
                decimal grossRentBalance = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NETARREARSTOTAL"));
                // lblNetRentBalance.Text = val.SetBracs(grossRentBalance.ToString());
                //lblNetRentBalance.ForeColor = this.SetForeColor(grossRentBalance.ToString());
                totalNetRentBalance = grossRentBalance;

                Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                decimal pricetotalSum = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "GRANDTOTAL"));
                // lblTotal.Text = val.SetBracs(pricetotalSum.ToString());
                // lblTotal.ForeColor = this.SetForeColor(pricetotalSum.ToString());
                totalSum = pricetotalSum;
                //total += Convert.ToDecimal(e.Row .Cells[3].Text);
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalRentBalance = (Label)e.Row.FindControl("lblTotalRentBalance");
                lblTotalRentBalance.Text = val.SetBracsAndCurrency(totalRentBalance.ToString());
                lblTotalRentBalance.ForeColor = this.SetForeColor(totalRentBalance.ToString());
                Label lblTotalHB = (Label)e.Row.FindControl("lblTotalHB");
                lblTotalHB.Text = val.SetBracsAndCurrency(totalHB.ToString());
                lblTotalHB.ForeColor = this.SetForeColor(totalHB.ToString());
                Label lblTotalADVHB = (Label)e.Row.FindControl("lblTotalADVHB");
                lblTotalADVHB.Text = val.SetBracsAndCurrency(totalADVHB.ToString());
                lblTotalADVHB.ForeColor = this.SetForeColor(totalADVHB.ToString());
                Label lblTotalNetRentBalance = (Label)e.Row.FindControl("lblTotalNetRentBalance");
                lblTotalNetRentBalance.Text = val.SetBracsAndCurrency(totalNetRentBalance.ToString());
                lblTotalNetRentBalance.ForeColor = this.SetForeColor(totalNetRentBalance.ToString());
                Label lblTotalSLB = (Label)e.Row.FindControl("lblTotalSLB");
                lblTotalSLB.Text = val.SetBracsAndCurrency(totalSLB.ToString());
                lblTotalSLB.ForeColor = this.SetForeColor(totalSLB.ToString());
                // val.SetBracs(totalSLB.ToString());
                Label lblSumTotal = (Label)e.Row.FindControl("lblSumTotal");
                lblSumTotal.Text = val.SetBracsAndCurrency(totalSum.ToString());
                lblSumTotal.ForeColor = this.SetForeColor(totalSum.ToString());
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {


        }

        protected void pgvBalanceList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


    }
}

