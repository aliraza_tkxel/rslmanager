﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="ArrearsBalanceReport.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.ArrearsBalanceReport" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript" language="javascript">
     function Clickheretoprint() {
         window.print();
         //         var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
         //         disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
         //         var content_vlue = document.getElementById("ReportViewer1").innerHTML;

         //         var docprint = window.open("", "", disp_setting);
         //         docprint.document.open();
         //         docprint.document.write('<html><head><title>Inel Power System</title>');
         //         docprint.document.write('</head><body onLoad="self.print()"><center>');
         //         docprint.document.write(content_vlue);
         //         docprint.document.write('</center></body></html>');
         //         docprint.document.close();
         //         docprint.focus();
     }
    </script>
    <style type="text/css" media="print">
        .hidden_Print
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server" Style="color: White;"></asp:Label>
    </asp:Panel>
     <asp:Button ID="btnPrint" CssClass="hidden_Print" runat="server" Text="Print" OnClientClick="Clickheretoprint()" />
     <asp:Button ID="btnBack" class="hidden_Print" OnClick="btnBack_Click" runat="server" Text="Back" />
    <asp:Panel ID="pnlarrearskpi" runat="server" Width="355" Height="148">
        <asp:UpdatePanel ID="uparrearskpi" UpdateMode="Conditional" runat="server">
            <ContentTemplate>           
                <asp:Label ID="lblNoRecord" runat="server" Text="No Record Found" ForeColor="Black" Font-Bold="true" Visible="false"></asp:Label>
                    <br />
               <%-- <asp:Chart ID="Chart1" runat="server" Height="145" Palette="BrightPastel">
                    <series>
                        <asp:Series Name="Series1" ChartType="Column" IsValueShownAsLabel="false">
                        </asp:Series>
                    </series>
                    <chartareas>
                        <asp:ChartArea Name="ChartArea1" BackGradientStyle="Center">
                        </asp:ChartArea>
                    </chartareas>
                </asp:Chart>--%>
                <asp:Chart ID="chartArrearsBalance" runat="server" Visible="true" Height="145" 
                    Palette="BrightPastel" onclick="chartArrearsBalance_Click">
                    <Series>
                        <asp:Series Name="chartSeries" ChartType="Column" IsXValueIndexed="false" >
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="chartAreaArrearsBalance" BackGradientStyle="Center">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>                
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

</asp:Content>
