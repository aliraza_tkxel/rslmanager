﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.IO;
using Am.Ahv.BusinessManager.Casemgmt;
using System.Drawing;
using Am.Ahv.Website.pagebase;

namespace Am.Ahv.Website.secure.reports
{
    public partial class StatusAndActionReport : PageBase
    {
        #region "Attributes"

        AddCaseWorker caseWorker = null;
        SuppressedCase suppressedCase = null;
        Am.Ahv.BusinessManager.statusmgmt.StatusAndActionReport statusAndActionReportMgr = null;

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        bool noRecord;

        public bool NoRecord
        {
            get { return noRecord; }
            set { noRecord = value; }
        }

        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        int regionId;

        public int RegionId
        {
            get { return regionId; }
            set { regionId = value; }
        }

        int assignedToId;

        public int AssignedToId
        {
            get { return assignedToId; }
            set { assignedToId = value; }
        }

        int suburbId;

        public int SuburbId
        {
            get { return suburbId; }
            set { suburbId = value; }
        }

        bool allRegionFlag;

        public bool AllRegionFlag
        {
            get { return allRegionFlag; }
            set { allRegionFlag = value; }
        }

        bool allAssignedToFlag;

        public bool AllAssignedToFlag
        {
            get { return allAssignedToFlag; }
            set { allAssignedToFlag = value; }
        }

        bool allSuburbFlag;

        public bool AllSuburbFlag
        {
            get { return allSuburbFlag; }
            set { allSuburbFlag = value; }
        }

        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }
        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
               Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            ResetMessage();
            //setMessage("Success Message", false);
            if (!IsPostBack)
            {
                SetDefaultPagingAttributes();
                InitLookup();
                PopulateGridView(0, 0, 0, true, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                SetCurrentRecordsCount(0, 0, 0, true, true, true);
                SetTotalFieldCount();
                SetPagingLabels();
            }

        }

        #endregion

        #region"LBtn Next Click"

        protected void llbtnNext_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            IncreasePaging();
            SetTotalFieldCount();
        }

        #endregion

        #region"Lbtn Previous Click"

        protected void llbtnPrevious_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            ReducePaging();
            SetTotalFieldCount();
        }

        #endregion

        #region"DDL Region Selected Index Changed"

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlRegion.SelectedValue != "-1")
                {
                    caseWorker = new AddCaseWorker();

                    ddlSuburb.DataSource = caseWorker.GetSuburbsListByRegion(ddlRegion.SelectedItem.Text, Convert.ToInt32(ddlAssignedTo.SelectedValue));
                    ddlSuburb.DataTextField = ApplicationConstants.schememname;
                    ddlSuburb.DataValueField = ApplicationConstants.schemeId;
                    ddlSuburb.DataBind();
                    //if (ddlSuburb.Items.Count > 1)
                    //{
                    ddlSuburb.Items.Add(new ListItem("All", "-1"));
                    ddlSuburb.SelectedValue = "-1";
                    //}

                }
                else
                {
                    ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                    ddlSuburb.DataTextField = string.Empty;
                    ddlSuburb.DataValueField = string.Empty;
                    ddlSuburb.DataBind();
                    ddlSuburb.Items.Add(new ListItem("All", "-1"));
                    ddlSuburb.SelectedValue = "-1";
                }
                updpnlDropDownStatusActionReport.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSuburbAndCaseOwnedBy, true);
                }
            }
        }

        #endregion

        #region"Btn Search Click"

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SetDefaultPagingAttributes();
                GetViewStateValues();
                PopulateGridView(Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                SetCurrentRecordsCount(Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false);
                       
               
                SetTotalFieldCount();
                SetPagingLabels();
                updpnlStatusActionReport.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionButtonSearch, true);
                }
            }
        }

        #endregion

        #region" DDL records per Page Selected Index Changed"

        protected void ddlRecordsPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SetDefaultPagingAttributes();
                GetViewStateValues();
                if (pgvStatusActionReport.Rows.Count > 0)
                {
                    DbIndex = 1;
                    GetSearchIndexes();
                    PopulateGridView(AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                    SetCurrentRecordsCount(AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag);
                    SetTotalFieldCount();
                    SetPagingLabels();
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionRecordsPerPage, true);
                }
            }

        }

        #endregion

        #region"DDL Assigned To Selected Index Changed"

        protected void ddlAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                caseWorker = new AddCaseWorker();
                suppressedCase = new SuppressedCase();

                if (ddlAssignedTo.SelectedValue != "-1")
                {
                    ddlRegion.DataSource = suppressedCase.GetRegionList(Convert.ToInt32(ddlAssignedTo.SelectedValue));
                    ddlRegion.DataTextField = "LOCATION";
                    ddlRegion.DataValueField = "PATCHID";
                    ddlRegion.DataBind();

                    ddlRegion.Items.Add(new ListItem("All", "-1"));
                    ddlRegion.SelectedValue = "-1";

                }
                else
                {
                    ddlRegion.DataSource = caseWorker.GetAllRegion();
                    ddlRegion.DataTextField = ApplicationConstants.regionname;
                    ddlRegion.DataValueField = ApplicationConstants.regionId;
                    ddlRegion.DataBind();
                    ddlRegion.Items.Add(new ListItem("All", "-1"));
                    ddlRegion.SelectedValue = "-1";
                }

                ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                ddlSuburb.DataTextField = string.Empty;
                ddlSuburb.DataValueField = string.Empty;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingRegion, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Populate GridView"

        public void PopulateGridView(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize)
        {
            try
            {
                statusAndActionReportMgr = new BusinessManager.statusmgmt.StatusAndActionReport();
                pgvStatusActionReport.DataSource = statusAndActionReportMgr.GetStatusAndActionReport(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize);
                pgvStatusActionReport.DataBind();
                SetViewStateValues(Convert.ToString(_regionId), Convert.ToString(_assignedTo), Convert.ToString(_suburbId), _allRegionFlag, _allAssignedToFlag, _allSuburbFlag);

            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingGridView, true);
                }
            }
        }

        #endregion

        #region"Populate Lookups"

        public void InitLookup()
        {
            try
            {
                caseWorker = new AddCaseWorker();
                suppressedCase = new SuppressedCase();

                ddlAssignedTo.DataSource = suppressedCase.GetAssignedToList();
                ddlAssignedTo.DataTextField = "EmployeeName";
                ddlAssignedTo.DataValueField = "ResourceId";
                ddlAssignedTo.DataBind();
                ddlAssignedTo.Items.Add(new ListItem("All", "-1"));
                ddlAssignedTo.SelectedValue = "-1";

                ddlRegion.DataSource = caseWorker.GetAllRegion();
                ddlRegion.DataTextField = ApplicationConstants.regionname;
                ddlRegion.DataValueField = ApplicationConstants.regionId;
                ddlRegion.DataBind();
                ddlRegion.Items.Add(new ListItem("All", "-1"));
                ddlRegion.SelectedValue = "-1";

                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";

                ddlRecordsPerPage.DataSource = ApplicationConstants.recordsPerPageSuppressedCases;
                ddlRecordsPerPage.DataBind();
                ddlRecordsPerPage.SelectedValue = ApplicationConstants.pagingPageSizeCaseList.ToString();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingRegion, true);
                }
            }
        }

        #endregion

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            //PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
            CurrentPage = 1;
            DbIndex = 1;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            RowIndex = -1;
            RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            SetViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void SetViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageSuppressedCases] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexSuppressedCases] = DbIndex;
            ViewState[ViewStateConstants.totalPagesSuppressedCases] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsSuppressedCases] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexSuppressedCases] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageSuppressedCases] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsSuppressedCases] = CurrentRecords;

        }

        public void SetViewStateValues(string _regionId, string _assignedToId, string _suburbId, bool _allRegionFlag, bool _allAssignedToFlag, bool _allSuburbFlag)
        {
            ViewState[ViewStateConstants.regionIdSuppressedCases] = _regionId;
            ViewState[ViewStateConstants.assignedToSuppressedCases] = _assignedToId;
            ViewState[ViewStateConstants.suburbIdSuppressedCases] = _suburbId;
            ViewState[ViewStateConstants.allRegionFlagSuppressedCases] = _allRegionFlag;
            ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases] = _allAssignedToFlag;
            ViewState[ViewStateConstants.allSuburbFlagSuppressedCases] = _allSuburbFlag;
        }

        #endregion

        #region"Set Paging Labels"

        public void SetPagingLabels()
        {
            try
            {

                int count = TotalRecords;

                if (count > Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    SetCurrentRecordsDisplayed();
                    SetTotalPages(count);
                    lblCurrentRecords.Text = (CurrentRecords).ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    lblCurrentRecords.Text = pgvStatusActionReport.Rows.Count.ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = CurrentPage.ToString();
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    lbtnPrevious.Enabled = false;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    lbtnNext.Enabled = true;
                }
               
                SetViewStateValues();
            }
            catch (FileLoadException flException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSettingPageLabelsCaseWorker, true);
                }
            }
        }

        #endregion

        #region"Set Total Pages"

        public void SetTotalPages(int count)
        {
            try
            {
                if (count % Convert.ToInt32(ddlRecordsPerPage.SelectedValue) > 0)
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue)) + 1;
                }
                else
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                }
            }
            catch (DivideByZeroException divide)
            {
                IsException = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionTotalPages, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Displayed"

        public void SetCurrentRecordsDisplayed()
        {
            try
            {
                if (pgvStatusActionReport.Rows.Count == Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    CurrentRecords = CurrentPage * pgvStatusActionReport.Rows.Count;
                }
                else if (RemoveFlag == true)
                {
                    CurrentRecords -= 1;
                }
                else
                {
                    CurrentRecords += pgvStatusActionReport.Rows.Count;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionCurrentRecords, true);
                }
            }

        }

        #endregion

        #region"Set Current Records Count"

        public void SetCurrentRecordsCount(int _assignedToId, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag)
        {
            try
            {
                statusAndActionReportMgr = new BusinessManager.statusmgmt.StatusAndActionReport();
                TotalRecords = statusAndActionReportMgr.GetStatusAndActionReportCount(_assignedToId, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag);
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionTotalRecords, true);
                }
            }
        }

        #endregion

        #region"Set Total Field Count"

        public void SetTotalFieldCount()
        {
            Label lblIgnore = new Label();
            Label lblRecord = new Label();
            int ignoreCount = 0;
            int actionCount = 0;

            for (int i = 0; i < pgvStatusActionReport.Rows.Count; i++)
            {
                lblIgnore = (Label)pgvStatusActionReport.Rows[i].FindControl("lblActionIgnoreCount");
                lblRecord = (Label)pgvStatusActionReport.Rows[i].FindControl("lblActionRecordCount");
                if (lblIgnore.Text.Equals(string.Empty))
                {
                    ignoreCount += 0;
                }
                else
                {
                    ignoreCount += Convert.ToInt32(lblIgnore.Text);
                }
                if (lblRecord.Text.Equals(string.Empty))
                {
                    actionCount += 0;
                }
                else
                {
                    actionCount += Convert.ToInt32(lblRecord.Text);
                }
            }

            lblTotalIgnored.Text = ignoreCount.ToString();
            lblTotalActioned.Text = actionCount.ToString();
        }

        #endregion

        #endregion

        #region"Getters"

        #region"Get ViewState Values"

        public void GetViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageSuppressedCases]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexSuppressedCases]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesSuppressedCases]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsSuppressedCases]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsSuppressedCases]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexSuppressedCases]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageSuppressedCases]);
        }

        #endregion

        #region"Get Search Indexes"

        public void GetSearchIndexes()
        {
            RegionId = Convert.ToInt32(ViewState[ViewStateConstants.regionIdSuppressedCases]);
            AssignedToId = Convert.ToInt32(ViewState[ViewStateConstants.assignedToSuppressedCases]);
            SuburbId = Convert.ToInt32(ViewState[ViewStateConstants.suburbIdSuppressedCases]);
            AllRegionFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allRegionFlagSuppressedCases]);
            AllAssignedToFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases]);
            allSuburbFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allSuburbFlagSuppressedCases]);
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Reduce Paging"

        public void ReducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                GetSearchIndexes();
                PopulateGridView(AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, DbIndex ,Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                SetPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void IncreasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                GetSearchIndexes();
                PopulateGridView(AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, DbIndex,  Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                SetPagingLabels();
            }
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #endregion
    }
}