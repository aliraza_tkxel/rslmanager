﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMBlankMasterPage.Master"
    AutoEventWireup="true" CodeBehind="ExportHistoricalBalanceListReport.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.ExportHistoricalBalanceListReport" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc11" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 325px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlCaseList" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 10px">
                    <asp:Label ID="lblMySuppressedCases" runat="server" Font-Bold="True" ForeColor="Black"
                        Font-Size="Medium" Text="Historical Balance List"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td colspan="6" valign="top" class="tdClass" style="padding-left: 10px; padding-right: 10px">
                    <asp:UpdatePanel ID="updpnlSuppressedCases" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlGridSuppressedCases" runat="server" Width="100%">
                                <cc1:PagingGridView ID="pgvSuppressedCases" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true"
                                    EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center" ShowFooter ="true" 
                                    RowStyle-Wrap="true" GridLines="None" runat="server" AutoGenerateColumns="False"
                                    Width="100%"  OrderBy="" AllowPaging="false"                                    
                                    onrowdatabound="pgvSuppressedCases_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tenancy" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                           HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="false" />
                                            <ItemStyle Width="7%" Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTenancyID" runat="server" Text='<%# Eval("TenancyId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="15%" Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" Text='<%# this.SetCustomerName(Eval("CustomerName").ToString(), Eval("CustomerName2").ToString(), Eval("JointTenancyCount").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="20%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("CustomerAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalText" runat="server" Text="Totals:"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance(Gross)" ShowHeader="true" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-ForeColor="black" >
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("RentBalance").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("RentBalance").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalRentBalance" runat="server" Text=""></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ant HB" ShowHeader="true"  HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAntHB" runat="server" ForeColor='<%# this.SetForeColor(Eval("HB").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("HB").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalHB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adv HB" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="8%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" ForeColor='<%# this.SetForeColor(Eval("ADVHB").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("ADVHB").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalADVHB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance(Net)" ShowHeader="true" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("NetRentBalance").ToString()) %>'
                                                    Text='<%# SetBracs(Eval("NetRentBalance").ToString())%>'></asp:Label>
                                            </ItemTemplate>
                                             <FooterTemplate>
                                                <asp:Label ID="lblTotalNetRentBalance" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sales Ledger Balance" ShowHeader="true" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px"  BackColor="#EEEEEE"  />
                                            <ItemTemplate>
                                                <asp:Label ID="lblSLB" runat="server" ForeColor='<%# this.SetForeColor(Eval("TotalCost").ToString()) %>'
                                                    Text='<%# SetBracs(Eval("TotalCost").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalSLB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total" ShowHeader="true"  HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbltotal" runat="server" ForeColor='<%# this.SetForeColor(Eval("Total").ToString()) %>'
                                                    Text='<%# SetBracs(Eval("Total","{0:n2}").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblSumTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <FooterStyle CssClass="tableFooter" />
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <RowStyle Wrap="True" />
                                </cc1:PagingGridView>
                            </asp:Panel>
                                                       
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="tdClass">
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
