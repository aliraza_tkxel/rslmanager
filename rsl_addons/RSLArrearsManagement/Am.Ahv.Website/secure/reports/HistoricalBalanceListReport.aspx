﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master"
    AutoEventWireup="true" CodeBehind="HistoricalBalanceListReport.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.HistoricalBalanceListReport" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc11" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 325px;
        }
    </style>
    <script type="text/javascript" >
        function redirectToExportToExcel() {
            window.open("ExportHistoricalBalanceListReport.aspx", "", "width=900,height=600,left=100,top=100,scrollbars=1,resizable=1");

        }
</script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlCaseList" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 10px">
                    <asp:Label ID="lblMySuppressedCases" runat="server" Font-Bold="True" ForeColor="Black"
                        Font-Size="Medium" Text="Historical Balance List"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 25px">
                    <asp:UpdatePanel ID="updpnlDropDown" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAssignedTo" runat="server" Text="Case Officer:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlAssignedTo" AutoPostBack="true" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlAssignedTo_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <%--<td>
                            <asp:Label ID="lblDateAsAt" runat="server" Text="Date(As at):"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDateAsAt" runat="server" ssWidth="100px"></asp:TextBox>
                            <cc11:CalendarExtender ID="txtDateOfReferral0_CalendarExtender" runat="server" 
                                Animated="false" EnabledOnClient="true" Format="dd/MM/yyyy" 
                                PopupButtonID="btnImageCalendar" TargetControlID="txtDateAsAt">
                            </cc11:CalendarExtender>
                            <asp:ImageButton ID="btnImageCalendar" runat="server" 
                                ImageUrl="~/style/images/Calendar-icon.png" />
                            <asp:Label ID="lblReferralHelp" runat="server" Text="Date format: DD/MM/YYYY"></asp:Label>
                        </td>--%>
                                    <td>
                                        <asp:Label ID="lblMonth" runat="server" Text="As At:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlMonth" AutoPostBack="false" ssWidth="120px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <%-- <td><asp:Label ID="lblYear" runat="server" Text="Year:"></asp:Label></td>--%>
                                    <td>
                                        <asp:DropDownList ID="ddlYear" AutoPostBack="false" ssWidth="120px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRegion" runat="server" Text="Patch:"></asp:Label>
                                    </td>
                                    <td class="style1">
                                        <asp:DropDownList ID="ddlRegion" AutoPostBack="true" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSuburb" runat="server" Text="Scheme:"></asp:Label>
                                    </td>
                                    <td class="style1">
                                        <asp:DropDownList ID="ddlSuburb" AutoPostBack="false" Width="310px" runat="server">
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCustomerStatus" runat="server" Text="Customer Status:"></asp:Label>
                                    </td>
                                    <td class="style1">
                                        <asp:DropDownList ID="ddlCustomerStatus" AutoPostBack="false" Width="310px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAssetType" runat="server" Text="Asset Type:"></asp:Label>
                                    </td>
                                    <td class="style1">
                                        <asp:DropDownList ID="ddlAssetType" AutoPostBack="false" Width="310px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp; <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" />
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="btnExport" runat="server" Text="Export to CSV" 
                                             onclick="btnExport_Click"  OnClientClick="redirectToExportToExcel();"  />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="6" valign="top" class="tdClass" style="padding-left: 10px; padding-right: 10px">
                    <asp:UpdatePanel ID="updpnlSuppressedCases" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlGridSuppressedCases" runat="server" Width="100%">
                                <cc1:PagingGridView ID="pgvSuppressedCases" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true"
                                    EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center" ShowFooter ="true" 
                                    RowStyle-Wrap="true" GridLines="None" runat="server" AutoGenerateColumns="False"
                                    Width="100%" AllowSorting="True" OrderBy="" AllowPaging="false" 
                                    OnSorting="pgvHistoricalBalanceList_sorting" 
                                    onrowdatabound="pgvSuppressedCases_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tenancy" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="TenancyId" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="false" />
                                            <ItemStyle Width="7%" Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTenancyID" runat="server" Text='<%# Eval("TenancyId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="CustomerName" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="15%" Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblName" runat="server" Text='<%# this.SetCustomerName(Eval("CustomerName").ToString(), Eval("CustomerName2").ToString(), Eval("JointTenancyCount").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="CustomerAddress" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="20%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("CustomerAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                             <FooterTemplate>
                                                <asp:Label ID="lblTotalText" runat="server" Text="Totals:"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance(Gross)" ShowHeader="true" HeaderStyle-HorizontalAlign="Center"
                                            SortExpression="RentBalance" HeaderStyle-ForeColor="black" >
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("RentBalance").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("RentBalance").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalRentBalance" runat="server" Text=""></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ant HB" ShowHeader="true" SortExpression="HB" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAntHB" runat="server" ForeColor='<%# this.SetForeColor(Eval("HB").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("HB").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalHB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adv HB" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" 
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="8%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" ForeColor='<%# this.SetForeColor(Eval("ADVHB").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("ADVHB").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalADVHB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance(Net)" ShowHeader="true" SortExpression="NetRentBalance"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("NetRentBalance").ToString()) %>'
                                                    Text='<%# SetBracs(Eval("NetRentBalance").ToString())%>'></asp:Label>
                                            </ItemTemplate>
                                             <FooterTemplate>
                                                <asp:Label ID="lblTotalNetRentBalance" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sales Ledger Balance" ShowHeader="true" SortExpression="TotalCost"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px"  BackColor="#EEEEEE"  />
                                            <ItemTemplate>
                                                <asp:Label ID="lblSLB" runat="server" ForeColor='<%# this.SetForeColor(Eval("TotalCost").ToString()) %>'
                                                    Text='<%# SetBracs(Eval("TotalCost").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalSLB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total" ShowHeader="true" SortExpression="Total" HeaderStyle-HorizontalAlign="Center"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbltotal" runat="server" ForeColor='<%# this.SetForeColor(Eval("Total").ToString()) %>'
                                                    Text='<%# SetBracs(Eval("Total","{0:n2}").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblSumTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <FooterStyle CssClass="tableFooter" />
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <RowStyle Wrap="True" />
                                </cc1:PagingGridView>
                            </asp:Panel>
                            <table width="100%" class="tableFooter">
                                <tr>
                                    <td width="20%" style="padding-left: 20px">
                                        <asp:Label ID="lblRecords" runat="server" Text="Records"></asp:Label>&nbsp;
                                        <asp:Label ID="lblCurrentRecords" runat="server"></asp:Label>&nbsp;
                                        <asp:Label ID="lblOf" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:Label ID="lblTotalRecords" runat="server"></asp:Label>
                                    </td>
                                    <td width="20%" style="padding-left: 20px">
                                        <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>&nbsp;
                                        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>&nbsp;
                                        <asp:Label ID="lblOfPage" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:Label ID="lblTotalPages" runat="server"></asp:Label>
                                    </td>
                                      <td width="6%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnFirst" runat="server" Text="<< First" OnClick="lbtnFirst_Click"></asp:LinkButton>
                                    </td>
                                    <td width="8%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnPrevious" runat="server" Text="< Previous" OnClick="llbtnPrevious_Click"></asp:LinkButton>
                                    </td>
                                    <td width="6%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnNext" runat="server" Text="Next >" OnClick="llbtnNext_Click"></asp:LinkButton>
                                    </td>
                                     <td width="6%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnLast" runat="server" Text="Last >>" OnClick="lbtnLast_Click"></asp:LinkButton>
                                    </td>
                                    <td width="15%">
                                    </td>
                                    <td width="15%">
                                        <asp:Label ID="lblRecordsPerPage" runat="server" Text="Records per page:"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:DropDownList ID="ddlRecordsPerPage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRecordsPerPage_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <span class="Error">Please wait...</span>
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="tdClass">
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
