﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.IO;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.payments;
using System.Collections;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.utitility_classes;
using System.Reflection;
using System.Linq.Expressions;

namespace Am.Ahv.Website.secure.reports
{
    public partial class PaymentPlanMonitoring : PageBase
    {

        #region "Attributes"

        AddCaseWorker caseWorker = null;
        Am.Ahv.BusinessManager.Casemgmt.SuppressedCase suppressedCase = null;
        Payments paymentManager = null;
        Validation val = new Validation();

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        bool noRecord;

        public bool NoRecord
        {
            get { return noRecord; }
            set { noRecord = value; }
        }

        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        int regionId;

        public int RegionId
        {
            get { return regionId; }
            set { regionId = value; }
        }

        int assignedToId;

        public int AssignedToId
        {
            get { return assignedToId; }
            set { assignedToId = value; }
        }

        int suburbId;

        public int SuburbId
        {
            get { return suburbId; }
            set { suburbId = value; }
        }

        bool allRegionFlag;

        public bool AllRegionFlag
        {
            get { return allRegionFlag; }
            set { allRegionFlag = value; }
        }

        bool allAssignedToFlag;

        public bool AllAssignedToFlag
        {
            get { return allAssignedToFlag; }
            set { allAssignedToFlag = value; }
        }

        bool allSuburbFlag;

        public bool AllSuburbFlag
        {
            get { return allSuburbFlag; }
            set { allSuburbFlag = value; }
        }

        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }

        string queryRe = "";

        public string QueryRe
        {
            get { return queryRe; }
            set { queryRe = value; }
        }

        string pCode = "";

        public string PCode
        {
            get { return pCode; }
            set { pCode = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            ResetMessage();

            //setMessage("Success Message", false);
            if (!IsPostBack)
            {
                queryRe = (Request.QueryString["cmd"] == "MissedPayments") ? "MissedPayments" : "";
                pCode = (Request.QueryString["cmd"] == "MissedPayments") ? Session[SessionConstants.PostCodeSearch].ToString() : "";
                SetDefaultPagingAttributes();
                InitLookup();

                //PopulateGridView(0, 0, 0, true, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                ViewState["SortDirection"] = "ASC";

                //update by:umair
                //update date:17/10/2011
                //ViewState["SortExpression"] = "CustomerName";
                ViewState["SortExpression"] = "LastPaymentDate";
                //end update
                if (Request.QueryString["cmd"] == "MissedPayments")
                {

                    queryRe = "misspayments";
                    PopulateGridView(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked = true);

                    // PopulateGridView(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue),  Convert.ToInt32(ddlSuburb.SelectedValue), true, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe , Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked );
                    //SetCurrentRecordsCount(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, true, true, queryRe);
                }
                else
                {
                    PopulateGridView(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), -1, -1, true, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);


                    // SetCurrentRecordsCount(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), -1, -1, true, true, true, queryRe);
                }
                SetPagingLabels();
            }
            queryRe = Convert.ToString(ViewState["result"]);

            if (ddlShowMissed.Checked == true)
            {
                queryRe = "misspayments";
            }

        }

        #endregion

        #region"LBtn Next Click"

        protected void llbtnNext_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            IncreasePaging();
        }

        #endregion

        #region"Lbtn Previous Click"

        protected void llbtnPrevious_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            ReducePaging();
        }

        #endregion

        #region"DDL Region Selected Index Changed"

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlRegion.SelectedValue != "-1")
                {
                    caseWorker = new AddCaseWorker();

                    ddlSuburb.DataSource = caseWorker.GetUserSuburbs(Convert.ToInt32(ddlAssignedTo.SelectedValue));
                    ddlSuburb.DataTextField = ApplicationConstants.schememname;
                    ddlSuburb.DataValueField = ApplicationConstants.schemeId;
                    ddlSuburb.DataBind();


                    ddlSuburb.Items.Add(new ListItem("All", "-1"));
                    ddlSuburb.SelectedValue = "-1";





                }
                else
                {
                    ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                    ddlSuburb.DataTextField = string.Empty;
                    ddlSuburb.DataValueField = string.Empty;
                    ddlSuburb.DataBind();
                    ddlSuburb.Items.Add(new ListItem("All", "-1"));
                    ddlSuburb.SelectedValue = "-1";
                }
                updpnlDropDown.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSuburbAndCaseOwnedBy, true);
                }
            }
        }

        #endregion

        #region"Btn Search Click"

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlShowMissed.Checked == true)
                {
                    queryRe = "misspayments";
                }

                SetDefaultPagingAttributes();
                GetViewStateValues();
                if (ddlAssignedTo.SelectedValue == "-1")
                {
                    if (ddlRegion.SelectedValue != "-1")
                    {
                        if (ddlSuburb.SelectedValue != "-1")
                        {


                            PopulateGridView(pCode, -1, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), true, false, false, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                            //SetCurrentRecordsCount(pCode, 0, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), true, false, false, queryRe);
                        }
                        else
                        {
                            PopulateGridView(pCode, -1, Convert.ToInt32(ddlRegion.SelectedValue), -1, true, false, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                            //SetCurrentRecordsCount(pCode, -1, Convert.ToInt32(ddlRegion.SelectedValue), -1, true, false, true, queryRe);
                        }
                    }
                    else
                    {
                        PopulateGridView(pCode, -1, -1, -1, true, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                        //SetCurrentRecordsCount(pCode, -1, -1, -1, true, true, true, queryRe);
                    }
                }
                else
                {
                    if (ddlRegion.SelectedValue != "-1")
                    {
                        if (ddlSuburb.SelectedValue != "-1")
                        {
                            PopulateGridView(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                            //SetCurrentRecordsCount(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, queryRe);
                        }
                        else
                        {
                            PopulateGridView(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, false, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                           // SetCurrentRecordsCount(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, false, true, queryRe);
                        }
                    }
                    else
                    {
                        if (ddlSuburb.SelectedValue == "-1")
                        {
                            PopulateGridView(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), -1, -1, false, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                            //SetCurrentRecordsCount(pCode, Convert.ToInt32(ddlAssignedTo.SelectedValue), -1, -1, false, true, true, queryRe);
                        }
                    }
                }
                SetPagingLabels();
                updpnlPaymentPlanMonitoring.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionButtonSearch, true);
                }
            }
        }

        #endregion

        #region" DDL records per Page Selected Index Changed"

        protected void ddlRecordsPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SetDefaultPagingAttributes();
                GetViewStateValues();
                if (pgvPaymentPlanMonitoring.Rows.Count > 0)
                {
                    DbIndex = 0;
                    GetSearchIndexes();
                    PopulateGridView(pCode, AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                    //SetCurrentRecordsCount(pCode, AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, queryRe);
                    SetPagingLabels();
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionRecordsPerPage, true);
                }
            }

        }

        #endregion

        #region"DDL Assigned To Selected Index Changed"

        protected void ddlAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                caseWorker = new AddCaseWorker();
                suppressedCase = new SuppressedCase();

                if (ddlAssignedTo.SelectedValue != "-1")
                {
                    // ddlRegion.DataSource = suppressedCase.GetRegionList(Convert.ToInt32(ddlAssignedTo.SelectedValue));
                    ddlRegion.DataSource = caseWorker.GetUserRegion(Convert.ToInt32(ddlAssignedTo.SelectedValue));
                    ddlRegion.DataTextField = "LOCATION";
                    ddlRegion.DataValueField = "PATCHID";
                    ddlRegion.DataBind();
                    ddlRegion.Items.Insert (0,new ListItem("All", "-1"));
                    ddlRegion.SelectedValue = "-1";

                }
                else
                {
                    ddlRegion.DataSource = caseWorker.GetAllRegion();
                    ddlRegion.DataTextField = ApplicationConstants.regionname;
                    ddlRegion.DataValueField = ApplicationConstants.regionId;
                    ddlRegion.DataBind();
                    ddlRegion.Items.Add(new ListItem("All", "-1"));
                    ddlRegion.SelectedValue = "-1";
                }
                ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                ddlSuburb.DataTextField = string.Empty;
                ddlSuburb.DataValueField = string.Empty;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingRegion, true);
                }
            }
        }

        #endregion

        #region"LBTN Name Click"

        protected void lbtnName_Click(object sender, EventArgs e)
        {

            Label lbl = new Label();
            Label lblCustomer = new Label();
            LinkButton lbtn = new LinkButton();
            GridViewRow gvr = ((LinkButton)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;

            lbl = (Label)pgvPaymentPlanMonitoring.Rows[index].FindControl("lblTenantId");
            lbtn = (LinkButton)pgvPaymentPlanMonitoring.Rows[index].FindControl("lbtnName");
            lblCustomer = (Label)pgvPaymentPlanMonitoring.Rows[index].FindControl("lblCustomerId");

            base.SetCaseId(Convert.ToInt32(lbtn.CommandArgument));
            base.SetTenantId(Convert.ToInt32(lbl.Text));
            Session[SessionConstants.CustomerId] = lblCustomer.Text;
            base.RemovePaymentPlanSession();
            Response.Redirect(PathConstants.casehistory, false);






        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Populate GridView"

        public void PopulateGridView(string _postCode, int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag, int index, int pageSize, string queryRe, string sortExpression, string sortDir, bool showmissed)
        {
            try
            {
                paymentManager = new Payments();
                //suppressedCase = new SuppressedCase();   


                List<PaymentPlanMonitoringWrapper> Dataset = paymentManager.GetPaymentPlanMonitorings(_postCode, _assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize, queryRe, sortExpression, sortDir, showmissed).OfType<PaymentPlanMonitoringWrapper>().ToList();

                Session["PaymentPlanMonitoringtDS"] = Dataset;
                TotalRecords = Dataset.Count();
                //pgvPaymentPlanMonitoring.DataSource = paymentManager.GetPaymentPlanMonitoring(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag, index, pageSize, queryRe);
                pgvPaymentPlanMonitoring.DataSource = GetPage(Dataset, index, pageSize);
                pgvPaymentPlanMonitoring.DataBind();
                //ScriptBuilding();
                SetViewStateValues(Convert.ToString(_regionId), Convert.ToString(_assignedTo), Convert.ToString(_suburbId), _allRegionFlag, _allAssignedToFlag, _allSuburbFlag);
                SetTotalRentBalance(_assignedTo, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag);

            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingGridView, true);
                }
            }
        }
        List<PaymentPlanMonitoringWrapper> GetPage(IList<PaymentPlanMonitoringWrapper> list, int page, int pageSize)
        {
            return list.Skip(page * pageSize).Take(pageSize).ToList();
        }
        #endregion

        #region"Populate Lookups"

        public void InitLookup()
        {
            try
            {
                caseWorker = new AddCaseWorker();
                suppressedCase = new SuppressedCase();

                ddlAssignedTo.DataSource = suppressedCase.GetAssignedToList();
                ddlAssignedTo.DataTextField = "EmployeeName";
                ddlAssignedTo.DataValueField = "ResourceId";
                ddlAssignedTo.DataBind();
                ddlAssignedTo.Items.Insert (0,new ListItem("All", "-1"));
                if (Request.QueryString["cmd"] == "MissedPayments")
                {
                    ddlAssignedTo.SelectedValue = Session[SessionConstants.CaseOwnedByDashboardSearch].ToString();
                }
                else
                {
                    ddlAssignedTo.SelectedValue = base.GetLoggedInUserID().ToString();
                }
                ddlRegion.DataSource = caseWorker.GetUserRegion(Convert.ToInt32(ddlAssignedTo.SelectedValue));
                ddlRegion.DataTextField = ApplicationConstants.regionname;
                ddlRegion.DataValueField = ApplicationConstants.regionId;
                ddlRegion.DataBind();
                ddlRegion.Items.Insert (0,new ListItem("All", "-1"));
                ddlRegion.SelectedValue = "-1";

                if (Request.QueryString["cmd"] == "MissedPayments")
                {
                    ddlRegion.SelectedValue = Session[SessionConstants.RegionDashboardSearch].ToString();
                }
                else
                {
                    ddlRegion.SelectedValue = "-1";
                }



                ddlSuburb.DataSource = caseWorker.GetUserSuburbs(Convert.ToInt32(ddlAssignedTo.SelectedValue));
                ddlSuburb.DataTextField = ApplicationConstants.schememname;
                ddlSuburb.DataValueField = ApplicationConstants.suburbId;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";

                if (Request.QueryString["cmd"] == "MissedPayments")
                {
                    string sub_value = Session[SessionConstants.SuburbDashboardSearch].ToString();
                    ddlSuburb.ClearSelection();
                    ddlSuburb.SelectedValue = sub_value;


                }
                else
                {

                    ddlSuburb.SelectedValue = "-1";
                }


                ddlRecordsPerPage.DataSource = ApplicationConstants.recordsPerPageSuppressedCases;
                ddlRecordsPerPage.DataBind();
                ddlRecordsPerPage.SelectedValue = ApplicationConstants.pagingPageSizeCaseList.ToString();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingRegion, true);
                }
            }
        }

        #endregion

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            //PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
            CurrentPage = 1;
            DbIndex = 0;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            RowIndex = -1;
            RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            SetViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void SetViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageSuppressedCases] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexSuppressedCases] = DbIndex;
            ViewState[ViewStateConstants.totalPagesSuppressedCases] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsSuppressedCases] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexSuppressedCases] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageSuppressedCases] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsSuppressedCases] = CurrentRecords;
            ViewState["MissedPayments"] = queryRe;


        }

        public void SetViewStateValues(string _regionId, string _assignedToId, string _suburbId, bool _allRegionFlag, bool _allAssignedToFlag, bool _allSuburbFlag)
        {
            ViewState[ViewStateConstants.regionIdSuppressedCases] = _regionId;
            ViewState[ViewStateConstants.assignedToSuppressedCases] = _assignedToId;
            ViewState[ViewStateConstants.suburbIdSuppressedCases] = _suburbId;
            ViewState[ViewStateConstants.allRegionFlagSuppressedCases] = _allRegionFlag;
            ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases] = _allAssignedToFlag;
            ViewState[ViewStateConstants.allSuburbFlagSuppressedCases] = _allSuburbFlag;
            ViewState["MissedPayments"] = queryRe;
        }

        #endregion

        #region"Set Paging Labels"

        public void SetPagingLabels()
        {
            try
            {

                int count = TotalRecords;
                pgvPaymentPlanMonitoring.PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
                if (count > Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    SetCurrentRecordsDisplayed();
                    SetTotalPages(count);
                    lblCurrentRecords.Text = (CurrentRecords).ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    lblCurrentRecords.Text = pgvPaymentPlanMonitoring.Rows.Count.ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = CurrentPage.ToString();
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    lbtnPrevious.Enabled = false;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    lbtnNext.Enabled = true;
                }
                //FileStream fs = null;
                //if (File.Exists("PagingConstants.txt"))
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Append);
                //}
                //else
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Create, FileAccess.ReadWrite);
                //}
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine(System.DateTime.Now.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.WriteLine("ToatlPages = " + TotalPages.ToString());
                //sw.WriteLine("Current Page = " + CurrentPage.ToString());
                //sw.WriteLine("Current Number of Records = " + CurrentRecords.ToString());
                //sw.WriteLine("Records in Grid View = " + pgvUsers.Rows.Count.ToString());
                //sw.WriteLine("Total Records = " + count.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.Close();
                //fs.Close();
                SetViewStateValues();
            }
            catch (FileLoadException flException)
            {
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"Set Total Pages"

        public void SetTotalPages(int count)
        {
            try
            {
                if (count % Convert.ToInt32(ddlRecordsPerPage.SelectedValue) > 0)
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue)) + 1;
                }
                else
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                }
            }
            catch (DivideByZeroException divide)
            {
                IsException = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionTotalPages, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Displayed"

        public void SetCurrentRecordsDisplayed()
        {
            try
            {
                if (pgvPaymentPlanMonitoring.Rows.Count == Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    CurrentRecords = CurrentPage * pgvPaymentPlanMonitoring.Rows.Count;
                }
                else if (RemoveFlag == true)
                {
                    CurrentRecords -= 1;
                }
                else
                {
                    CurrentRecords += pgvPaymentPlanMonitoring.Rows.Count;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionCurrentRecords, true);
                }
            }

        }

        #endregion

        

        #region"Set Total Rent Balance"

        public void SetTotalRentBalance(int _assignedTo, int _regionId, int _suburbId, bool _allAssignedToFlag, bool _allRegionFlag, bool _allSuburbFlag)
        {
            Label lbl = new Label();
            Label lbl2 = new Label();

            ArrayList list = new ArrayList();
            ArrayList rentBalance = new ArrayList();

            double rent = 0.0;

            for (int i = 0; i < pgvPaymentPlanMonitoring.Rows.Count; i++)
            {
                lbl = (Label)pgvPaymentPlanMonitoring.Rows[i].FindControl("lblRentBalance");
                lbl2 = (Label)pgvPaymentPlanMonitoring.Rows[i].FindControl("lbltenancyId");
                if (!list.Contains(lbl2.Text))
                {
                    list.Add(lbl2.Text);
                    if (lbl.ForeColor == Color.Red)
                    {
                        string[] str = lbl.Text.Split('(', '£', ')');
                        rentBalance.Add(str[2]);
                    }
                    else
                    {
                        string[] str = lbl.Text.Split('£');
                        rentBalance.Add(str[1]);
                    }
                }
            }
            for (int j = 0; j < rentBalance.Count; j++)
            {
                rent = rent + Convert.ToDouble(rentBalance[j]);
            }
            lblTotalRentBalance.Text = val.SetBracs(rent.ToString());
            lblTotalRentBalance.ForeColor = val.SetForeColor(rent.ToString());

        }

        #endregion

        #endregion

        #region"Getters"

        #region"Get ViewState Values"

        public void GetViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageSuppressedCases]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexSuppressedCases]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesSuppressedCases]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsSuppressedCases]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsSuppressedCases]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexSuppressedCases]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageSuppressedCases]);
            queryRe = Convert.ToString(ViewState["MissedPayments"]);

        }

        #endregion

        #region"Get Search Indexes"

        public void GetSearchIndexes()
        {
            RegionId = Convert.ToInt32(ViewState[ViewStateConstants.regionIdSuppressedCases]);
            AssignedToId = Convert.ToInt32(ViewState[ViewStateConstants.assignedToSuppressedCases]);
            SuburbId = Convert.ToInt32(ViewState[ViewStateConstants.suburbIdSuppressedCases]);
            AllRegionFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allRegionFlagSuppressedCases]);
            AllAssignedToFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases]);
            allSuburbFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allSuburbFlagSuppressedCases]);
            queryRe = Convert.ToString(ViewState["MissedPayments"]);
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Reduce Paging"

        public void ReducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                GetSearchIndexes();
                List<PaymentPlanMonitoringWrapper> Dataset = Session["PaymentPlanMonitoringtDS"] as List<PaymentPlanMonitoringWrapper>;
                pgvPaymentPlanMonitoring.DataSource = GetPage(Dataset, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                pgvPaymentPlanMonitoring.DataBind();
                //PopulateGridView(pCode, AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                SetPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void IncreasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                GetSearchIndexes();
                // PopulateGridView(pCode, AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), ddlShowMissed.Checked);
                //pgvPaymentPlanMonitoring.PageIndex = DbIndex;
                List<PaymentPlanMonitoringWrapper> Dataset = Session["PaymentPlanMonitoringtDS"] as List<PaymentPlanMonitoringWrapper>;
                pgvPaymentPlanMonitoring.DataSource = GetPage(Dataset, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                pgvPaymentPlanMonitoring.DataBind();
                SetPagingLabels();
            }
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool IsException)
        {
            if (IsException == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Script Building"

        public void ScriptBuilding()
        {
            LinkButton lbtn = new LinkButton();
            Label lbl = new Label();
            string url = string.Empty;

            for (int i = 0; i < pgvPaymentPlanMonitoring.Rows.Count; i++)
            {
                lbtn = (LinkButton)pgvPaymentPlanMonitoring.Rows[i].FindControl("lbtnName");
                lbl = (Label)pgvPaymentPlanMonitoring.Rows[i].FindControl("lblCaseId");
                url = PathConstants.javaScriptCasehistory + lbl.Text + "&tid=" + lbtn.CommandArgument + "&cmd=scase";
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        #endregion

        #region"Set Customer Name"

        public string SetCustomerName(string customerName1, string customerName2, string jointTenancyCount)
        {
            if (jointTenancyCount == "1")
            {
                return customerName1;
            }
            else
            {
                return customerName2 + " & " + customerName1;
            }
        }

        #endregion


        public Color SetForeColor(string value)
        {
            return val.SetForeColor(value);
        }


        public string SetBracs(string value)
        {
            return val.SetBracs(value);
        }



        #endregion

        #region"sorting"

        protected void pgvPaymentPlanMonitoring_sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = e.SortDirection;
            }

            //IQueryable Dataset = Session["SuppressedCaseListDS"];
            //SuppressedCases.SortGrid<>(Dataset, e, GetSortDirection(e.SortExpression));

            DbIndex = 0;

            //List<PaymentPlanMonitoringWrapper> wrapperList = Session["PaymentPlanMonitoringtDS"] as List<PaymentPlanMonitoringWrapper>;

            //wrapperList.OrderBy(e.SortExpression + " " + e.SortDirection);
            //Session["PaymentPlanMonitoringtDS"] = wrapperList;
            //pgvPaymentPlanMonitoring.DataSource = GetPage(wrapperList, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
            //pgvPaymentPlanMonitoring.DataBind();
            //DataTable dt = convertToDataTable(wrapperList);
            //DataView dv = dt.DefaultView;
            //dv.Sort = e.SortExpression + e.SortDirection;
            //DataTable sortedDT = dv.ToTable();


            SetDefaultPagingAttributes();
            GetViewStateValues();
            GetSearchIndexes();
            PopulateGridView(pCode, AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), queryRe, e.SortExpression, GetSortDirection(e.SortExpression).ToString(), ddlShowMissed.Checked);
            //SetCurrentRecordsCount(pCode, AssignedToId, RegionId, SuburbId, AllAssignedToFlag, AllRegionFlag, AllSuburbFlag, queryRe);
            SetPagingLabels();

        }






        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            // SortDirection sortDirection = SortDirection.Ascending;

            // Retrieve the last column that was sorted.
            string sortExpression = Convert.ToString(ViewState["SortExpression"]);

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression.ToUpper() == column.ToUpper())
                {
                    string lastDirection = Convert.ToString(ViewState["SortDirection"]);
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        ViewState["SortDirection"] = "DESC";
                        //sortDirection = SortDirection.Descending;
                    }
                    else
                    {
                        ViewState["SortDirection"] = "ASC";
                    }
                }
                else
                {
                    ViewState["SortDirection"] = "DESC";
                }
            }

            // Save new values in ViewState.
            //ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return ViewState["SortDirection"].ToString();
        }

        #endregion

        protected void ddlSuburb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlShowMissed_CheckedChanged(object sender, EventArgs e)
        {




        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }

    public static class OrderByHelper
    {
        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> enumerable, string orderBy)
        {
            return enumerable.AsQueryable().OrderBy(orderBy).AsEnumerable();
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> collection, string orderBy)
        {
            foreach (OrderByInfo orderByInfo in ParseOrderBy(orderBy))
                collection = ApplyOrderBy<T>(collection, orderByInfo);

            return collection;
        }

        private static IQueryable<T> ApplyOrderBy<T>(IQueryable<T> collection, OrderByInfo orderByInfo)
        {
            string[] props = orderByInfo.PropertyName.Split('.');
            Type type = typeof(T);

            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (string prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                PropertyInfo pi = type.GetProperty(prop, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);
            string methodName = String.Empty;

            if (!orderByInfo.Initial && collection is IOrderedQueryable<T>)
            {
                if (orderByInfo.Direction == SortDirection.Ascending)
                    methodName = "ThenBy";
                else
                    methodName = "ThenByDescending";
            }
            else
            {
                if (orderByInfo.Direction == SortDirection.Ascending)
                    methodName = "OrderBy";
                else
                    methodName = "OrderByDescending";
            }

            //TODO: apply caching to the generic methodsinfos?
            return (IOrderedQueryable<T>)typeof(Queryable).GetMethods().Single(
                method => method.Name == methodName
                        && method.IsGenericMethodDefinition
                        && method.GetGenericArguments().Length == 2
                        && method.GetParameters().Length == 2)
                .MakeGenericMethod(typeof(T), type)
                .Invoke(null, new object[] { collection, lambda });

        }

        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> pSeq)
        {
            return pSeq ?? Enumerable.Empty<T>();
        }


        private static IEnumerable<OrderByInfo> ParseOrderBy(string orderBy)
        {
            if (String.IsNullOrEmpty(orderBy))
                yield break;

            string[] items = orderBy.Split(',');
            bool initial = true;
            foreach (string item in items)
            {
                string[] pair = item.Trim().Split(' ');

                if (pair.Length > 2)
                    throw new ArgumentException(String.Format("Invalid OrderBy string '{0}'. Order By Format: Property, Property2 ASC, Property2 DESC", item));

                string prop = pair[0].Trim();

                if (String.IsNullOrEmpty(prop))
                    throw new ArgumentException("Invalid Property. Order By Format: Property, Property2 ASC, Property2 DESC");

                SortDirection dir = SortDirection.Ascending;

                if (pair.Length == 2)
                    dir = ("desc".Equals(pair[1].Trim(), StringComparison.OrdinalIgnoreCase) ? SortDirection.Descending : SortDirection.Ascending);

                yield return new OrderByInfo() { PropertyName = prop, Direction = dir, Initial = initial };

                initial = false;
            }

        }

        private class OrderByInfo
        {
            public string PropertyName { get; set; }
            public SortDirection Direction { get; set; }
            public bool Initial { get; set; }
        }

        private enum SortDirection
        {
            Ascending = 0,
            Descending = 1
        }
    }

}

