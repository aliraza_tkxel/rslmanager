﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMBlankMasterPage.Master"
    AutoEventWireup="true" CodeBehind="ExportBalanceListReport.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.ExportBalanceListReport" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlCaseList" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 10px">
                    <asp:Label ID="lblBalanceList" runat="server" Font-Bold="true" ForeColor="Black"
                        Font-Size="Medium" Text="Balance List"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td colspan="6" valign="top" class="tdClass" style="padding-left: 20px; padding-right: 15px">
                    <asp:UpdatePanel ID="updpnlSuppressedCases" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlGridSuppressedCases" runat="server" Width="100%">
                                <cc1:PagingGridView ID="pgvBalanceList" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true"
                                    EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center"
                                    AllowPaging="false" GridLines="None" runat="server" AutoGenerateColumns="false"
                                   OnRowDataBound="pgvBalanceList_RowDataBound"
                                    ShowFooter="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tenancy" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                             HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="false" />
                                            <ItemStyle Width="7%" Height="25px" HorizontalAlign="Left" />
                                            <FooterStyle Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("TenancyId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Left" 
                                            ShowHeader="true" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="12%" Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label runat="server"  Text='<%#  Eval("FULLNAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Payment" ShowHeader="true" 
                                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="15%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("LASTPAYMENTDATE") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" Text="Totals:"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance(Gross)" ShowHeader="true" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label ForeColor='<%# this.SetForeColor(Eval("RENTBALANCE").ToString()) %>' runat="server"
                                                    Text='<%# this.SetBracs(Eval("RENTBALANCE").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalRentBalance" runat="server" Text=""></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ant HB" ShowHeader="true"  HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" ForeColor='<%# this.SetForeColor(Eval("ESTHB").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("ESTHB").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalHB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adv HB" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="8%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" ForeColor='<%# this.SetForeColor(Eval("ADVHB").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("ADVHB").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalADVHB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance(Net)" ShowHeader="true" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("NETARREARS").ToString()) %>'
                                                 Text='<%# this.SetBracs(Eval("NETARREARS").ToString()) %>'
                                                ></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalNetRentBalance" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sales Ledger Balance" ShowHeader="true" 
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# SetBracs(Eval("SALESLEDGERBALANCE").ToString()) %>' ForeColor='<%# this.SetForeColor(Eval("SALESLEDGERBALANCE").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalSLB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total" ShowHeader="true"  HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotal" runat="server" Text='<%# SetBracs(Eval("TOTAL").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblSumTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                       
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <FooterStyle CssClass="tableFooter" />
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <RowStyle Wrap="True" />
                                </cc1:PagingGridView>
                            </asp:Panel>
                            
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="tdClass">
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
