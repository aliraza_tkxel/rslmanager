﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMBlankMasterPage.Master" AutoEventWireup="true" CodeBehind="CaseActivities.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.CaseActivities" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

 <script type="text/javascript" language="javascript">
     function Clickheretoprint() {
         window.print();
         //         var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
         //         disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
         //         var content_vlue = document.getElementById("ReportViewer1").innerHTML;

         //         var docprint = window.open("", "", disp_setting);
         //         docprint.document.open();
         //         docprint.document.write('<html><head><title>Inel Power System</title>');
         //         docprint.document.write('</head><body onLoad="self.print()"><center>');
         //         docprint.document.write(content_vlue);
         //         docprint.document.write('</center></body></html>');
         //         docprint.document.close();
         //         docprint.focus();
     }
</script>
    <style type="text/css" media="print">
       .hidden_Print
       {
           display:none;
           }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:Panel ID="pnlMessage" runat="server" Visible="false">
              <asp:Label ID="lblMessage" runat="server"></asp:Label>
     </asp:Panel>
         <asp:Button ID="btnPrint" CssClass="hidden_Print" runat="server" Text="Print" OnClientClick="Clickheretoprint()" />
        <asp:Button ID="btnBack" class="hidden_Print" OnClick="btnBack_Click" runat="server" Text="Back" />
    <rsweb:ReportViewer ID="CaseActivitiesReportViewer" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" InteractiveDeviceInfos="(Collection)"  ShowToolBar="false"
        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"  SizeToReportContent="true"
        Width="885px">
        <LocalReport ReportPath="controls\reports\ViewActivitiesPrintOut.rdlc">
        </LocalReport>
    </rsweb:ReportViewer>
</asp:Content>
