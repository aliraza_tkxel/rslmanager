﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Am.Ahv.BusinessManager.reports;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;

namespace Am.Ahv.Website.secure.reports
{
    public partial class NoticeToVacateCasesList : PageBase
    {
        #region"Attributes"
        bool isError;
        bool isException;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion

        #region"Events"       

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            resetMessage();
            try
            {
                if (!IsPostBack)
                {
                    LoadNoticetoVacateCasesList();
                }
            }
            catch(Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex,"Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.LoadingPageError, IsException);
                }
            }
        }


        #endregion

        #region"Btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.dashboardPath);
        }

        #endregion

        #endregion

        #region "Functions"

        #region"Load Notice to Vacate Cases List"
        public void LoadNoticetoVacateCasesList()
        {
            try
            {
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                Am.Ahv.BusinessManager.reports.BalanceList BL = new BusinessManager.reports.BalanceList();
                ReportDataSource repds = new ReportDataSource();
                repds.Name = ApplicationConstants.NoticeToVacateDS;
                repds.Value = BL.GetNoticeToVacateCasesList();
                ReportViewer1.LocalReport.DataSources.Add(repds);
                ReportViewer1.ServerReport.Refresh();
            }

            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.LoadingNoticeToVacateCasesList, IsException);
                }
            }

        }
        #endregion

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

    }
}