﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMBlankMasterPage.Master"
    AutoEventWireup="true" CodeBehind="FlexiblePaymentPlan.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.FlexiblePaymentPlan" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function Clickheretoprint() {
            window.print();
            //         var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
            //         disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
            //         var content_vlue = document.getElementById("ReportViewer1").innerHTML;

            //         var docprint = window.open("", "", disp_setting);
            //         docprint.document.open();
            //         docprint.document.write('<html><head><title>Inel Power System</title>');
            //         docprint.document.write('</head><body onLoad="self.print()"><center>');
            //         docprint.document.write(content_vlue);
            //         docprint.document.write('</center></body></html>');
            //         docprint.document.close();
            //         docprint.focus();
        }
    </script>
    <style type="text/css" media="print">
        .hidden_Print
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div>
        <div>
            <asp:Button ID="btnPrint" CssClass="hidden_Print" runat="server" Text="Print" OnClientClick="Clickheretoprint()" />
            &nbsp;&nbsp;
            <asp:Button ID="btnBack" class="hidden_Print" OnClick="btnBack_Click" runat="server" Text="Back" />
        </div>
        
        <rsweb:ReportViewer ID="FlexibleReportViewer" runat="server" ShowToolBar="false"
            Font-Names="Verdana" Font-Size="8pt" SizeToReportContent="true" InteractiveDeviceInfos="(Collection)"
            ShowFindControls="False" ShowPageNavigationControls="False" ShowRefreshButton="False"
            ShowZoomControl="False" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
            Width="970px">
            
            <LocalReport ReportPath="controls\reports\ParentReport.rdlc">
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
</asp:Content>
