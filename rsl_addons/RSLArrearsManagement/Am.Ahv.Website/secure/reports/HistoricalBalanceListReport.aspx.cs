﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.Casemgmt;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.IO;
using Am.Ahv.BusinessManager.reports;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BusinessManager.Customers;
using System.Globalization;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Entities;

namespace Am.Ahv.Website.secure.reports
{
    public partial class HistoricalBalanceListReport : PageBase
    {


        Validation val = new Validation();
        #region "Attributes"
        bool isException=false;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        bool noRecord;

        public bool NoRecord
        {
            get { return noRecord; }
            set { noRecord = value; }
        }

        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        int regionId;

        public int RegionId
        {
            get { return regionId; }
            set { regionId = value; }
        }

        int assignedToId;

        public int AssignedToId
        {
            get { return assignedToId; }
            set { assignedToId = value; }
        }

        int suburbId;

        public int SuburbId
        {
            get { return suburbId; }
            set { suburbId = value; }
        }

       string dateAsAt;

        public string DateAsAt
        {
            get { return dateAsAt; }
            set { dateAsAt = value; }
        }

        int customerStatus;

        public int CustomerStatus
        {
            get { return customerStatus; }
            set { customerStatus = value; }
        }

        int assetType;

        public int AssetType
        {
            get { return assetType; }
            set { assetType = value; }
        }
      


        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }

        int months;

        public int Months
        {
            get { return months; }
            set { months = value; }
        }

        int years;

        public int Years
        {
            get { return years; }
            set { years = value; }
        }

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        decimal totalRentBalance = 0;
        decimal totalHB = 0;
        decimal totalADVHB = 0;
        decimal totalSLB = 0;
        decimal totalNetRentBalance = 0;
        decimal totalSum = 0; 
        #endregion
     
        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            resetMessage();

            if (!IsPostBack)
            {
                if (Request.QueryString["cmd"] != null && Request.QueryString["month"] != null && Request.QueryString["year"] != null)
                {
                    if (Request.QueryString["cmd"] == "ArrearsKPI")
                    {
                        string month = Convert.ToString(Request.QueryString["month"]);
                        string year = Convert.ToString(Request.QueryString["year"]);

                        initlookups();
                        SetDefaultPagingAttributes();
                        ViewState["SortDirection"] = "ASC";
                        ViewState["SortExpression"] = "C_CUSTOMERTENANCY.TenancyId";
                        ddlMonth.SelectedValue = month;
                        ddlYear.SelectedValue = year;
                        PopulateGridView(0,0, 0, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), 0, 0, ddlMonth.SelectedIndex, int.Parse(ddlYear.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                        SetCurrentRecordsCount(0,0, 0, 0, 0, ddlMonth.SelectedIndex, int.Parse(ddlYear.SelectedValue));
                        SetPagingLabels();
                    }
                }
                else
                {
                    initlookups();
                    SetDefaultPagingAttributes();
                    ViewState["SortDirection"] = "ASC";
                    ViewState["SortExpression"] = "TenancyId";
                    PopulateGridView(0,0, 0, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), 0, 0, 0, 0, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                    SetCurrentRecordsCount(0,0, 0, 0, 0, 0, 0);
                    SetPagingLabels();
                }
            }
        }

        #endregion        

        #region"ddlRegion_SelectedIndexChanged"
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlRegion.SelectedValue != "-1")
                {
                    AddCaseWorker caseWorker = new AddCaseWorker();

                    ddlSuburb.DataSource = caseWorker.GetSuburbsListByRegion(ddlRegion.SelectedItem.Text, Convert.ToInt32(ddlAssignedTo.SelectedValue));
                    ddlSuburb.DataTextField = ApplicationConstants.schememname;
                    ddlSuburb.DataValueField = ApplicationConstants.schemeId;
                    ddlSuburb.DataBind();
                    //if (ddlSuburb.Items.Count > 1)
                    //{
                        ddlSuburb.Items.Add(new ListItem("All", "-1"));
                        ddlSuburb.SelectedValue = "-1";
                    //}
                }
                else
                {
                    ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                    ddlSuburb.DataTextField = string.Empty;
                    ddlSuburb.DataValueField = string.Empty;
                    ddlSuburb.DataBind();
                    ddlSuburb.Items.Add(new ListItem("All", "-1"));
                    ddlSuburb.SelectedValue = "-1";
                }
                updpnlDropDown.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionSuburbAndCaseOwnedBy, true);
                }
            }
        }
        #endregion

        #region"btnSearch_Click"
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SetDefaultPagingAttributes();
                GetViewStateValues();
                Search();
                SetPagingLabels();
                updpnlSuppressedCases.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionButtonSearch, true);
                }
            }
        }
        #endregion

        #region"llbtnPrevious_Click"
        protected void llbtnPrevious_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            ReducePaging();
        }
        #endregion

        #region"llbtnNext_Click"
        protected void llbtnNext_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            IncreasePaging();
        }
        #endregion

        #region"ddlRecordsPerPage_SelectedIndexChanged"
        protected void ddlRecordsPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
             
            try
            {
                SetDefaultPagingAttributes();
                GetViewStateValues();
                if (pgvSuppressedCases.Rows.Count > 0)
                {
                    DbIndex = 0;
                    GetSearchIndexes();
                    PopulateGridView(AssignedToId,RegionId, SuburbId, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), customerStatus, assetType, months, years, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                    SetCurrentRecordsCount(AssignedToId,RegionId, SuburbId,customerStatus,assetType,Months,Years);
                    SetPagingLabels();
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionRecordsPerPage, true);
                }
            }
        }
        #endregion

        #region"lbtn First Click"

        protected void lbtnFirst_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            GoToFirstPage();
        }

        #endregion

        #region"lbtn Last Click"

        protected void lbtnLast_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            GoToLastpage();
        }

        #endregion

        #region  "Grid row data bound event"
        protected void pgvSuppressedCases_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                decimal price = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "RentBalance"));
                totalRentBalance += price;
                decimal pricetotalHB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "HB"));
                totalHB += pricetotalHB;
                decimal pricetotalADVHB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "ADVHB"));
                totalADVHB += pricetotalADVHB;
                decimal pricetotalSLB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalCost"));
                totalSLB += pricetotalSLB;
                decimal pricetotalNetRentBalance = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NetRentBalance"));
                totalNetRentBalance += pricetotalNetRentBalance;
                decimal pricetotalSum = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Total"));
                totalSum += pricetotalSum;


            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalRentBalance = (Label)e.Row.FindControl("lblTotalRentBalance");
                lblTotalRentBalance.Text = val.SetBracs(totalRentBalance.ToString());
                lblTotalRentBalance.ForeColor = this.SetForeColor(totalRentBalance.ToString());
                Label lblTotalHB = (Label)e.Row.FindControl("lblTotalHB");
                lblTotalHB.Text = val.SetBracs(totalHB.ToString());
                lblTotalHB.ForeColor = this.SetForeColor(totalHB.ToString());
                Label lblTotalADVHB = (Label)e.Row.FindControl("lblTotalADVHB");
                lblTotalADVHB.Text = val.SetBracs(totalADVHB.ToString());
                lblTotalADVHB.ForeColor = this.SetForeColor(totalADVHB.ToString());
                Label lblTotalNetRentBalance = (Label)e.Row.FindControl("lblTotalNetRentBalance");
                lblTotalNetRentBalance.Text = val.SetBracs(totalNetRentBalance.ToString());
                lblTotalNetRentBalance.ForeColor = this.SetForeColor(totalNetRentBalance.ToString());
                Label lblTotalSLB = (Label)e.Row.FindControl("lblTotalSLB");
                lblTotalSLB.Text = val.SetBracs(totalSLB.ToString());
                lblTotalSLB.ForeColor = this.SetForeColor(totalSLB.ToString());
                // val.SetBracs(totalSLB.ToString());
                Label lblSumTotal = (Label)e.Row.FindControl("lblSumTotal");
                lblSumTotal.Text = val.SetBracs(totalSum.ToString());
                lblSumTotal.ForeColor = this.SetForeColor(totalSum.ToString());
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"init lookups"
        private void initlookups()
        {
            try
            {
                AddCaseWorker caseWorker = new AddCaseWorker();
                SuppressedCase suppressedCase = new SuppressedCase();
                Customers customer = new Customers();
                Am.Ahv.BusinessManager.reports.BalanceList historicalBalanceList = new Am.Ahv.BusinessManager.reports.BalanceList();

                ddlAssignedTo.DataSource = suppressedCase.GetAssignedToList();
                ddlAssignedTo.DataTextField = "EmployeeName";
                ddlAssignedTo.DataValueField = "ResourceId";
                ddlAssignedTo.DataBind();
                ddlAssignedTo.Items.Add(new ListItem("All", "-1"));
                ddlAssignedTo.SelectedValue = "-1";

                ddlRegion.DataSource = caseWorker.GetAllRegion();
                ddlRegion.DataTextField = ApplicationConstants.regionname;
                ddlRegion.DataValueField = ApplicationConstants.regionId;
                ddlRegion.DataBind();
                ddlRegion.Items.Add(new ListItem("All", "-1"));
                ddlRegion.SelectedValue = "-1";

                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";


                ddlCustomerStatus.DataSource = customer.GetCustomerStatus();
                ddlCustomerStatus.DataTextField = "CustomerStatusDescription";
                ddlCustomerStatus.DataValueField = "CustomerStatusId";
                ddlCustomerStatus.DataBind();
                ddlCustomerStatus.Items.Add(new ListItem("All", "-1"));
                ddlCustomerStatus.SelectedValue = "-1";

                ddlAssetType.DataSource = historicalBalanceList.GetAssetType();
                ddlAssetType.DataTextField = "AssetTypeDescription";
                ddlAssetType.DataValueField = "AssetTypeId";
                ddlAssetType.DataBind();
                ddlAssetType.Items.Add(new ListItem("All", "-1"));
                ddlAssetType.SelectedValue = "-1";

                ddlRecordsPerPage.DataSource = ApplicationConstants.recordsPerPageSuppressedCases;
                ddlRecordsPerPage.DataBind();
                ddlRecordsPerPage.SelectedValue = ApplicationConstants.pagingPageSizeCaseList.ToString();

                BindDDL("Year");
                BindDDL("Month");              

            }

            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionLoadingRegion, true);
                }
            }
        }
        #endregion

        #region"DDL Case Own By Selected Index Changed"

        protected void ddlAssignedTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                populateRegions(Convert.ToInt32(ddlAssignedTo.SelectedValue));
                PopulateSuburbs();
                updpnlDropDown.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionAgainstSuburbFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Populate Lookups"

        //public void initRegionLookup()
        public void populateRegions(int caseOwnedById)
        {
            try
            {
                Am.Ahv.BusinessManager.Casemgmt.CaseList caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();

                ddlRegion.DataSource = caseList.getRegionList(caseOwnedById);
                ddlRegion.DataTextField = "Patch";
                ddlRegion.DataValueField = "PatchId";
                ddlRegion.DataBind();
                ddlRegion.Items.Add(new ListItem("All", "-1"));
                ddlRegion.SelectedValue = "-1";
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionLoadingRegionFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Populate Subusrbs"

        public void PopulateSuburbs()
        {
            if (ddlRegion.SelectedValue != "-1")
            {
                AddCaseWorker caseWorker = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
                // Am.Ahv.BusinessManager.Casemgmt.CaseList caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();
                ddlSuburb.DataSource = caseWorker.GetAllSuburbs(Convert.ToInt32(ddlRegion.SelectedValue));
                ddlSuburb.DataTextField = ApplicationConstants.schememname;
                ddlSuburb.DataValueField = ApplicationConstants.schemeId;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";
            }
            else
            {
                ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                ddlSuburb.DataTextField = string.Empty;
                ddlSuburb.DataValueField = string.Empty;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";
            }
        }

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            //isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
        #region"Go to first page"

        public void GoToFirstPage()
        {
            if (CurrentPage > 1)
            {
                CurrentPage = 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                    lbtnLast.Enabled = true;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex = 0;
                GetSearchIndexes();
                PopulateGridView(AssignedToId, RegionId, SuburbId, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), customerStatus, assetType, months, years, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                SetPagingLabels();
            }
        }

        #endregion
        #region"Reduce Paging"

        public void ReducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                GetSearchIndexes();
                PopulateGridView(AssignedToId,RegionId, SuburbId, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), customerStatus, assetType, months, years, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                SetPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void IncreasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                GetSearchIndexes();
                PopulateGridView(AssignedToId,RegionId, SuburbId, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), customerStatus, assetType, months, years, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                SetPagingLabels();
            }
        }

        #endregion
        #region"Go to last page"

        public void GoToLastpage()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage = TotalPages;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                    lbtnFirst.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex = TotalPages - 1;
                GetSearchIndexes();
                PopulateGridView(AssignedToId, RegionId, SuburbId, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), customerStatus, assetType, months, years, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                SetPagingLabels();
            }
        }

        #endregion
        #region"Populate GridView"

        public void PopulateGridView(int _caseOwnedBy,int _regionId, int _suburbId, int index, int pageSize, int _customerStatus, int _assetType, int _months, int _years, string sortExpression, string sortDir)
        {
            try
            {              
                BusinessManager.reports.BalanceList Bl = new BusinessManager.reports.BalanceList();

                 List<Am.Ahv.Entities.AM_SP_GetHistoricalBalanceList_Result> Dataset= Bl.GetHistoricalBalanceList(_caseOwnedBy,_regionId, _suburbId, index, pageSize, _customerStatus, _assetType, _months, _years, sortExpression, sortDir).ToList();
                 Session["HistoricalBalanceListDS"] = Dataset;

                 pgvSuppressedCases.DataSource = Dataset; 
                pgvSuppressedCases.DataBind();
               // ScriptBuilding();
                SetViewStateValues(Convert.ToString(_regionId), Convert.ToString(_suburbId),Convert.ToString(_customerStatus),Convert.ToString(_assetType),Convert.ToInt32(_months),Convert.ToInt32(_years));

            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionLoadingGridView, true);
                }
            }
        }

        #endregion

        #region"Get ViewState Values"

        public void GetViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageHistoricalBalanceList]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexHistoricalBalanceList]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesHistoricalBalanceList]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsHistoricalBalanceList]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsHistoricalBalanceList]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexHistoricalBalanceList]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageHistoricalBalanceList]);
        }

        #endregion

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            //PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
            CurrentPage = 1;
            DbIndex = 0;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            RowIndex = -1;
            RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            SetViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void SetViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageHistoricalBalanceList] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexHistoricalBalanceList] = DbIndex;
            ViewState[ViewStateConstants.totalPagesHistoricalBalanceList] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsHistoricalBalanceList] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexHistoricalBalanceList] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageHistoricalBalanceList] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsHistoricalBalanceList] = CurrentRecords;

        }

        public void SetViewStateValues(string _regionId, string _suburbId,string _customerStatus, string _assetType,int _months,int _years)
        {
            ViewState[ViewStateConstants.regionIdSuppressedCases] = _regionId;
           // ViewState[ViewStateConstants.assignedToSuppressedCases] = _assignedToId;
            ViewState[ViewStateConstants.suburbIdSuppressedCases] = _suburbId;
            ViewState[ViewStateConstants.hblCustomerStatus] = _customerStatus;
            ViewState[ViewStateConstants.hblAssetType] = _assetType;
            ViewState["Months"] = _months;
            ViewState["Years"] = _years;

            //ViewState["DateAsAt"] = _dateAsAt;
            //ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases] = _allAssignedToFlag;
            //ViewState[ViewStateConstants.allSuburbFlagSuppressedCases] = _allSuburbFlag;
        }

        #endregion

        #region"Set Paging Labels"

        public void SetPagingLabels()
        {
            try
            {

                int count = TotalRecords;

                if (count > Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    SetCurrentRecordsDisplayed();
                    SetTotalPages(count);
                    lblCurrentRecords.Text = (CurrentRecords).ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    lblCurrentRecords.Text = pgvSuppressedCases.Rows.Count.ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = CurrentPage.ToString();
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = false;
                    lbtnFirst.Enabled = false;
                    lbtnLast.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    lbtnPrevious.Enabled = false;
                    lbtnFirst.Enabled = false;
                }
                else if (CurrentPage < TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnLast.Enabled = true;
                    lbtnPrevious.Enabled = true;
                    lbtnFirst.Enabled = true;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnLast.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    lbtnNext.Enabled = true;
                    lbtnLast.Enabled = true;
                }
                //FileStream fs = null;
                //if (File.Exists("PagingConstants.txt"))
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Append);
                //}
                //else
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Create, FileAccess.ReadWrite);
                //}
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine(System.DateTime.Now.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.WriteLine("ToatlPages = " + TotalPages.ToString());
                //sw.WriteLine("Current Page = " + CurrentPage.ToString());
                //sw.WriteLine("Current Number of Records = " + CurrentRecords.ToString());
                //sw.WriteLine("Records in Grid View = " + pgvUsers.Rows.Count.ToString());
                //sw.WriteLine("Total Records = " + count.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.Close();
                //fs.Close();
                SetViewStateValues();
            }
            catch (FileLoadException flException)
            {
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"Set Total Pages"

        public void SetTotalPages(int count)
        {
            try
            {
                if (count % Convert.ToInt32(ddlRecordsPerPage.SelectedValue) > 0)
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue)) + 1;
                }
                else
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                }
            }
            catch (DivideByZeroException divide)
            {
                IsException = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionTotalPages, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Displayed"

        public void SetCurrentRecordsDisplayed()
        {
            try
            {
                if (pgvSuppressedCases.Rows.Count == Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    CurrentRecords = CurrentPage * pgvSuppressedCases.Rows.Count;
                }
                else if (CurrentPage == TotalPages)
                {
                    CurrentRecords = TotalRecords;
                }
                else if (RemoveFlag == true)
                {
                    CurrentRecords -= 1;
                }
                else
                {
                    CurrentRecords += pgvSuppressedCases.Rows.Count;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionCurrentRecords, true);
                }
            }

        }

        #endregion

        #region"Set Total Records Count"

        public void SetCurrentRecordsCount(int _caseOwnedBy,int _regionId, int _suburbId,int customerStatus,int assetType,int months,int years)
        {
            try
            {
                
                BusinessManager.reports.BalanceList Bl = new BusinessManager.reports.BalanceList();

                TotalRecords = Bl.GetHistoricalBalanceListCount( _caseOwnedBy,_regionId, _suburbId,customerStatus,assetType,months,years);//suppressedCase.GetSuppressedCasesCount(_assignedToId, _regionId, _suburbId, _allAssignedToFlag, _allRegionFlag, _allSuburbFlag);
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionTotalRecords, true);
                }
            }
        }

        #endregion

        #region"Set Rent Balance"

        public string SetRentBalance(string rent)
        {
            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£" + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£" + Math.Round(Convert.ToDouble(rent), 2).ToString()+")";
                }
            }
            else
            {
                return string.Empty;
            }

        }


        public string SetZeros(string value)
        {
            if (!value.Equals(string.Empty))
            {
                return Math.Round(Math.Abs(Convert.ToDouble(value)), 2).ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        #region"Set Next HB"

        public string SetNextHB(string rent)
        {

            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) == 0.0)
                {
                    return "N/A";
                }
                else if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£ " + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£ " + Math.Round(Convert.ToDouble(rent), 2).ToString() + ")";
                }
            }
            else
            {
                return "N/A";
            }
        }

        #endregion


        //public Color SetForeColor(string rent)
        //{
        //    if (!rent.Equals(string.Empty))
        //    {
        //        if (Convert.ToDouble(rent) < 0.0)
        //        {
        //            return Color.Black;
        //        }
        //        else
        //        {
        //            return Color.Red;
        //        }
        //    }
        //    else
        //    {
        //        return Color.Black;
        //    }

        //}


        #endregion

        #region"Get Search Indexes"

        public void GetSearchIndexes()
        {
            customerStatus = Convert.ToInt32(ViewState[ViewStateConstants.hblCustomerStatus]);
            assetType= Convert.ToInt32(ViewState[ViewStateConstants.hblAssetType]);
            RegionId = Convert.ToInt32(ViewState[ViewStateConstants.regionIdSuppressedCases]);
            //AssignedToId = Convert.ToInt32(ViewState[ViewStateConstants.assignedToSuppressedCases]);
            SuburbId = Convert.ToInt32(ViewState[ViewStateConstants.suburbIdSuppressedCases]);
            //DateAsAt = (ViewState["DateAsAt"]).ToString();
            Years = Convert.ToInt32(ViewState["Years"]);
            Months = Convert.ToInt32(ViewState["Months"]);
        }

        #endregion    

        #region"Search"

        private void Search()
        {
            RegionId=(ddlRegion.SelectedValue=="-1")?0:Convert.ToInt32(ddlRegion.SelectedValue);
            SuburbId = (ddlSuburb.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlSuburb.SelectedValue);
            CustomerStatus = (ddlCustomerStatus.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlCustomerStatus.SelectedValue);
            AssetType = (ddlAssetType.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlAssetType.SelectedValue);
            Months = (ddlMonth.SelectedIndex == 0) ? 0 : ddlMonth.SelectedIndex;
            Years = (ddlYear.SelectedIndex == 0) ? 0 : Convert.ToInt32(ddlYear.SelectedValue);
            AssignedToId = (ddlAssignedTo.SelectedValue == "-1") ? 0 : Convert.ToInt32(ddlAssignedTo.SelectedValue);

            ViewState["SortDirection"] = "ASC";
            ViewState["SortExpression"] = "C_CUSTOMERTENANCY.TenancyId";

            PopulateGridView(AssignedToId,RegionId, SuburbId, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), CustomerStatus, AssetType, Months, Years, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
            SetCurrentRecordsCount(AssignedToId,RegionId, SuburbId,CustomerStatus, AssetType,Months,Years);
            
            
            
        }

        #endregion

        #endregion

        #region"Bind DDL"

        private void BindDDL(string ddl)
        {
            switch (ddl)
            {
                case "Year":
                    ddlYear.DataSource = GetYear();
                    ddlYear.DataBind();
                    ddlYear.Items.Insert(0, new ListItem("Year"));
                    break;        
                case "Month":
                    string[] names = DateTimeFormatInfo.CurrentInfo.MonthNames;
                    //ddlMonth.DataSource = GetMonth();                    
                    ddlMonth.DataSource = names;
                    ddlMonth.DataBind();
                    ddlMonth.Items.Insert(0, new ListItem("Month"));
                    ddlMonth.Items.Remove("");
                    break;
            }
        }

        #endregion

        #region"Get Year"

        private List<int> GetYear()
        {
            List<int> year = new List<int>();
            for (int i = (DateTime.Now.Year) - 10; i != (DateTime.Now.Year) + 20; i++)
            {
                year.Add(i);
            }
            return year;

        }

        #endregion

        #region"Get Month"

        private List<int> GetMonth()
        {
            List<int> month = new List<int>();
            for (int i = 1; i <= 12; i++)
            {
                month.Add(i);
            }
            return month;
        }

        #endregion

        #region"sorting"

        protected void pgvHistoricalBalanceList_sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = e.SortDirection;
            }

            SetDefaultPagingAttributes();
            GetViewStateValues();
            GetSearchIndexes();
            PopulateGridView(AssignedToId,RegionId, SuburbId, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), customerStatus, assetType, months, years, e.SortExpression, GetSortDirection(e.SortExpression).ToString());
            SetCurrentRecordsCount(AssignedToId,RegionId, SuburbId, customerStatus, assetType, Months, Years);
            SetPagingLabels();

            //List<Am.Ahv.Entities.AM_SP_GetHistoricalBalanceList_Result> Dataset = Session["HistoricalBalanceListDS"] as List<Am.Ahv.Entities.AM_SP_GetHistoricalBalanceList_Result>;
            //SortGrid<AM_SP_GetHistoricalBalanceList_Result>(Dataset, e, GetSortDirection(e.SortExpression));

        }

        public void SortGrid<T>(IEnumerable<T> dataSource, GridViewSortEventArgs e, SortDirection sortDirection)
        {
            e.SortDirection = sortDirection;
            IEnumerable<T> set = pagebase.PageBase.SortGrid<T>(dataSource, e);
            pgvSuppressedCases.DataSource = set;
            pgvSuppressedCases.DataBind();
        }

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            // SortDirection sortDirection = SortDirection.Ascending;

            // Retrieve the last column that was sorted.
            string sortExpression = Convert.ToString(ViewState["SortExpression"]);

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression.ToUpper() == column.ToUpper())
                {
                    string lastDirection = Convert.ToString(ViewState["SortDirection"]);
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        ViewState["SortDirection"] = "DESC";
                        //sortDirection = SortDirection.Descending;
                    }
                    else
                    {
                        ViewState["SortDirection"] = "ASC";
                    }
                }
                else
                {
                    ViewState["SortDirection"] = "DESC";
                }
            }

            // Save new values in ViewState.
            //ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return ViewState["SortDirection"].ToString();
        }


        #endregion

        #region"Set Single Value Bracs and Color"
        public string SetBracs(string value)
        {
            return val.SetBracs(value);
        }


        public System.Drawing.Color SetForeColor(string value)
        {
            return val.SetForeColor(value);
        }

        #endregion

        #region"Set Customer Name"

        public string SetCustomerName(string customerName1, string customerName2, string jointTenancyCount)
        {
            if (jointTenancyCount == "1")
            {
                return customerName1;
            }
            else
            {
                return customerName2 + " & " + customerName1;
            }
        }

        #endregion

        protected void btnExport_Click(object sender, EventArgs e)
        {
           
        }

       
    }
}