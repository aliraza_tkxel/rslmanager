﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.Casemgmt;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.IO;
using Am.Ahv.BusinessManager.reports;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BusinessManager.Customers;
using System.Globalization;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Entities;
using System.Web.UI.HtmlControls;
using System.Threading;

namespace Am.Ahv.Website.secure.reports
{
    public partial class ExportHistoricalBalanceListReport : PageBase
    {


        Validation val = new Validation();
        #region "Attributes"
        bool isException=false;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        bool noRecord;

        public bool NoRecord
        {
            get { return noRecord; }
            set { noRecord = value; }
        }

        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        int regionId;

        public int RegionId
        {
            get { return regionId; }
            set { regionId = value; }
        }

        int assignedToId;

        public int AssignedToId
        {
            get { return assignedToId; }
            set { assignedToId = value; }
        }

        int suburbId;

        public int SuburbId
        {
            get { return suburbId; }
            set { suburbId = value; }
        }

       string dateAsAt;

        public string DateAsAt
        {
            get { return dateAsAt; }
            set { dateAsAt = value; }
        }

        int customerStatus;

        public int CustomerStatus
        {
            get { return customerStatus; }
            set { customerStatus = value; }
        }

        int assetType;

        public int AssetType
        {
            get { return assetType; }
            set { assetType = value; }
        }
      


        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }

        int months;

        public int Months
        {
            get { return months; }
            set { months = value; }
        }

        int years;

        public int Years
        {
            get { return years; }
            set { years = value; }
        }

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        decimal totalRentBalance = 0;
        decimal totalHB = 0;
        decimal totalADVHB = 0;
        decimal totalSLB = 0;
        decimal totalNetRentBalance = 0;
        decimal totalSum = 0; 
        #endregion
     
        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                //Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            resetMessage();

            if (!IsPostBack)
            {
           
                    ViewState["SortDirection"] = "ASC";
                    ViewState["SortExpression"] = "TenancyId";
                    PopulateGridView(0,0, 0, DbIndex, Convert.ToInt32(100), 0, 0, 0, 0, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                    DownloadExcel();
             
            }
        }

        #endregion        

        #region "Grid view row data bound event"        
        protected void pgvSuppressedCases_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    decimal price = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "RentBalance"));
                    totalRentBalance += price;
                    decimal pricetotalHB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "HB"));
                    totalHB += pricetotalHB;
                    decimal pricetotalADVHB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "ADVHB"));
                    totalADVHB += pricetotalADVHB;
                    decimal pricetotalSLB = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalCost"));
                    totalSLB += pricetotalSLB;
                    decimal pricetotalNetRentBalance = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NetRentBalance"));
                    totalNetRentBalance += pricetotalNetRentBalance;
                    decimal pricetotalSum = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Total"));
                    totalSum += pricetotalSum;


                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label lblTotalRentBalance = (Label)e.Row.FindControl("lblTotalRentBalance");
                    lblTotalRentBalance.Text = val.SetBracs(totalRentBalance.ToString());
                    lblTotalRentBalance.ForeColor = this.SetForeColor(totalRentBalance.ToString());
                    Label lblTotalHB = (Label)e.Row.FindControl("lblTotalHB");
                    lblTotalHB.Text = val.SetBracs(totalHB.ToString());
                    lblTotalHB.ForeColor = this.SetForeColor(totalHB.ToString());
                    Label lblTotalADVHB = (Label)e.Row.FindControl("lblTotalADVHB");
                    lblTotalADVHB.Text = val.SetBracs(totalADVHB.ToString());
                    lblTotalADVHB.ForeColor = this.SetForeColor(totalADVHB.ToString());
                    Label lblTotalNetRentBalance = (Label)e.Row.FindControl("lblTotalNetRentBalance");
                    lblTotalNetRentBalance.Text = val.SetBracs(totalNetRentBalance.ToString());
                    lblTotalNetRentBalance.ForeColor = this.SetForeColor(totalNetRentBalance.ToString());
                    Label lblTotalSLB = (Label)e.Row.FindControl("lblTotalSLB");
                    lblTotalSLB.Text = val.SetBracs(totalSLB.ToString());
                    lblTotalSLB.ForeColor = this.SetForeColor(totalSLB.ToString());
                    // val.SetBracs(totalSLB.ToString());
                    Label lblSumTotal = (Label)e.Row.FindControl("lblSumTotal");
                    lblSumTotal.Text = val.SetBracs(totalSum.ToString());
                    lblSumTotal.ForeColor = this.SetForeColor(totalSum.ToString());
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionLoadingGridView, true);
                }
            }
        }
        #endregion
        #endregion
        #region"Functions"

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            //isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
        
        #region"Populate GridView"

        public void PopulateGridView(int _caseOwnedBy,int _regionId, int _suburbId, int index, int pageSize, int _customerStatus, int _assetType, int _months, int _years, string sortExpression, string sortDir)
        {
            try
            {              
                BusinessManager.reports.BalanceList Bl = new BusinessManager.reports.BalanceList();

                 List<Am.Ahv.Entities.AM_SP_GetHistoricalBalanceListExportToExcel_Result> Dataset= Bl.GetHistoricalBalanceListExportToExcel(_caseOwnedBy,_regionId, _suburbId, index, pageSize, _customerStatus, _assetType, _months, _years, sortExpression, sortDir).ToList();
                 Session["HistoricalBalanceListDS"] = Dataset;

                 pgvSuppressedCases.DataSource = Dataset; 
                pgvSuppressedCases.DataBind();
               // ScriptBuilding();
                SetViewStateValues(Convert.ToString(_regionId), Convert.ToString(_suburbId),Convert.ToString(_customerStatus),Convert.ToString(_assetType),Convert.ToInt32(_months),Convert.ToInt32(_years));

            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.exceptionLoadingGridView, true);
                }
            }
        }

        #endregion

        #region"Get ViewState Values"

        public void GetViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageHistoricalBalanceList]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexHistoricalBalanceList]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesHistoricalBalanceList]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsHistoricalBalanceList]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsHistoricalBalanceList]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexHistoricalBalanceList]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageHistoricalBalanceList]);
        }

        #endregion

        #region"Set ViewState Values"

        public void SetViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageHistoricalBalanceList] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexHistoricalBalanceList] = DbIndex;
            ViewState[ViewStateConstants.totalPagesHistoricalBalanceList] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsHistoricalBalanceList] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexHistoricalBalanceList] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageHistoricalBalanceList] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsHistoricalBalanceList] = CurrentRecords;

        }

        public void SetViewStateValues(string _regionId, string _suburbId,string _customerStatus, string _assetType,int _months,int _years)
        {
            ViewState[ViewStateConstants.regionIdSuppressedCases] = _regionId;
           // ViewState[ViewStateConstants.assignedToSuppressedCases] = _assignedToId;
            ViewState[ViewStateConstants.suburbIdSuppressedCases] = _suburbId;
            ViewState[ViewStateConstants.hblCustomerStatus] = _customerStatus;
            ViewState[ViewStateConstants.hblAssetType] = _assetType;
            ViewState["Months"] = _months;
            ViewState["Years"] = _years;

            //ViewState["DateAsAt"] = _dateAsAt;
            //ViewState[ViewStateConstants.allAssignedToFlagSuppressedCases] = _allAssignedToFlag;
            //ViewState[ViewStateConstants.allSuburbFlagSuppressedCases] = _allSuburbFlag;
        }

        #endregion

        #region"Set Rent Balance"

        public string SetRentBalance(string rent)
        {
            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£" + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£" + Math.Round(Convert.ToDouble(rent), 2).ToString()+")";
                }
            }
            else
            {
                return string.Empty;
            }

        }


        public string SetZeros(string value)
        {
            if (!value.Equals(string.Empty))
            {
                return Math.Round(Math.Abs(Convert.ToDouble(value)), 2).ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        #region"Set Next HB"

        public string SetNextHB(string rent)
        {

            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) == 0.0)
                {
                    return "N/A";
                }
                else if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£ " + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£ " + Math.Round(Convert.ToDouble(rent), 2).ToString() + ")";
                }
            }
            else
            {
                return "N/A";
            }
        }

        #endregion


        //public Color SetForeColor(string rent)
        //{
        //    if (!rent.Equals(string.Empty))
        //    {
        //        if (Convert.ToDouble(rent) < 0.0)
        //        {
        //            return Color.Black;
        //        }
        //        else
        //        {
        //            return Color.Red;
        //        }
        //    }
        //    else
        //    {
        //        return Color.Black;
        //    }

        //}


        #endregion

        #region Export to excel

        public void DownloadExcel()
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                string fileName = "HistoricalBalanceList_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year) + ".xls";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                Response.Charset = "";
                Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
                Response.Write("<head>");
                Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
                Response.Write("<!--[if gte mso 9]><xml>");
                Response.Write("<x:ExcelWorkbook>");
                Response.Write("<x:ExcelWorksheets>");
                Response.Write("<x:ExcelWorksheet>");
                Response.Write("<x:Name>Report Data</x:Name>");
                Response.Write("<x:WorksheetOptions>");
                Response.Write("<x:Print>");
                Response.Write("<x:ValidPrinterInfo/>");
                Response.Write("</x:Print>");
                Response.Write(" <x:Alignment x:Horizontal=\"Center\" x:Vertical=\"Center\" " + "x:WrapText=\"true\"/>");
                Response.Write("</x:WorksheetOptions>");
                Response.Write("</x:ExcelWorksheet>");
                Response.Write("</x:ExcelWorksheets>");
                Response.Write("</x:ExcelWorkbook>");
                Response.Write("</xml>");
                Response.Write("<![endif]--> ");
                Response.ContentType = "application/vnd.ms-excel";
                StringWriter strWrite = new StringWriter();
                HtmlTextWriter htmWrite = new HtmlTextWriter(strWrite);
                HtmlForm htmfrm = new HtmlForm();

                pgvSuppressedCases .Parent.Controls.Add(htmfrm);
                foreach (GridViewRow row in pgvSuppressedCases.Rows)
                {
                    row.Height = 40;
                }
                htmfrm.Attributes["runat"] = "server";
                htmfrm.Controls.Add(pgvSuppressedCases);
                htmfrm.RenderControl(htmWrite);
                //style to format numbers to string
                string style = "<style> .textmode{mso-number-format:\\@;}</style>";
                Response.Write(style);
                Response.Write(strWrite.ToString());
                Response.Flush();
                Response.End();

            }
            catch (ThreadAbortException)
            {
                //If the download link is pressed we will get a thread abort.
            }
        }
        #endregion
        #endregion

        #region"Set Single Value Bracs and Color"
        public string SetBracs(string value)
        {
            return val.SetBracs(value);
        }


        public System.Drawing.Color SetForeColor(string value)
        {
            return val.SetForeColor(value);
        }

        #endregion

        #region"Set Customer Name"

        public string SetCustomerName(string customerName1, string customerName2, string jointTenancyCount)
        {
            if (jointTenancyCount == "1")
            {
                return customerName1;
            }
            else
            {
                return customerName2 + " & " + customerName1;
            }
        }

        #endregion

        
    }
}