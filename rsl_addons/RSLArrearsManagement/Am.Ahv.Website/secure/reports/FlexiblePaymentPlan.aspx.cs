﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Customers;
using Am.Ahv.BusinessManager.Dashboard;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.payments;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Website.pagebase;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Utilities.constants;


namespace Am.Ahv.Website.secure.reports
{
    public partial class FlexiblePaymentPlan : PageBase
    {

        #region"Attributes"
        CustomersReport CR = null;
        bool isError;
        bool isException;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion

        #region"Events"

        #region"Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            resetMessage();
            try
            {
                if (!IsPostBack)
                {
                    if (CheckPaymentPlanType())
                    {
                        FlexibleReportViewer.ProcessingMode = ProcessingMode.Local;
                        FlexibleReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadUserSummary);
                        FlexibleReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadPaymentPlan);
                        FlexibleReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadPaymentSchedule);
                        FlexibleReportViewer.ServerReport.Refresh();
                    }
                    else
                    {
                        Response.Redirect(PathConstants.regularPaymentReport + "?PaymentPlanHistoryId=" + Request.QueryString["PaymentPlanHistoryId"].ToString(), false);
                    }
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.LoadingPageError, true);
                }
            }
        }
        #endregion

        #region"Btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["PaymentPlanHistoryId"] != null)
            {
                Response.Redirect(PathConstants.casehistory, false);
            }
            else
            {
                string path = Session[SessionConstants.PaymentPlanBackPage].ToString();
                Response.Redirect(path, false);
            }
        }
        #endregion

        #endregion

        #region"Event Handlers"

        #region"Load User Summary"
        //void LoadUserSummary(object sender, SubreportProcessingEventArgs e)
        //{
        //    try
        //    {
        //        List<PaymentPlanWrapper> ListPlan;
        //        FlexibleReportManager p = new FlexibleReportManager();
        //        CR = new BusinessManager.statusmgmt.CustomersReport();
        //        CustomerInformationWrapper customer = CR.GetCustomerInformation(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
        //        if (customer != null)
        //        {
        //            List<CustomerInformationWrapper> UserList = new List<CustomerInformationWrapper>(1);
        //            UserList.Add(customer);
        //            ReportDataSource repds = new ReportDataSource();
        //            repds.Name = ApplicationConstants.UserSummaryDS;
        //            repds.Value = UserList;
        //            e.DataSources.Add(repds);
        //        }
        //        else
        //        {
        //            setMessage(UserMessageConstants.RetrievingUserSummary, true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        isException = true;
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");
        //    }
        //    finally
        //    {
        //        if (IsException)
        //        {
        //            setMessage(UserMessageConstants.LoadingUserSummary, true);

        //        }
        //    }
        //}


        private const string UserSummaryDataset = "DataSet1";


        void LoadUserSummary(object sender, SubreportProcessingEventArgs e)
        {
            try
            {
                List<PaymentPlanWrapper> ListPlan;
                FlexibleReportManager p = new FlexibleReportManager();
                BusinessManager.statusmgmt.CustomersReport CR = new BusinessManager.statusmgmt.CustomersReport();
                CustomerInformationWrapper customer = CR.GetCustomerInformation(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                
                if (Request.QueryString["PaymentPlanHistoryId"] != null)
                {
                    ListPlan = p.GetPaymentPlanSummaryHistory(Int32.Parse(Request.QueryString["PaymentPlanHistoryId"].ToString()));
                }
                else
                {
                    ListPlan = p.GetPaymentPlanSummaryWrappered(base.GetTenantId());
                }
                if (ListPlan == null)
                {
                    ListPlan = new List<PaymentPlanWrapper>(1);
                    setMessage(UserMessageConstants.RPPpaymentplan, true);
                    return;
                }
                else
                {
                    Session[SessionConstants.paymentPLanSummary] = ListPlan;
                }
                if (customer != null)
                {
                    customer.RentBalance = customer.RentBalance;
                    customer.Amount = "£ " + Math.Abs(Convert.ToDouble(ListPlan[0].LastPayment)).ToString() + " " + ListPlan[0].LastPaymentDate;
                    customer.LastPayment = ListPlan[0].LastPaymentDate;
                    customer.Date = ListPlan[0].CreatedDate;
                    
                    
                    List<CustomerInformationWrapper> UserList = new List<CustomerInformationWrapper>(1);
                    UserList.Add(customer);
                    ReportDataSource repds = new ReportDataSource();
                    repds.Name = UserSummaryDataset;
                    repds.Value = UserList;
                    e.DataSources.Add(repds);
                }
                else
                {
                   setMessage(UserMessageConstants.RPPusersummary,true);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion

        #region"Load Payment Plan"

        void LoadPaymentPlan(object sender, SubreportProcessingEventArgs e)
        {
            try
            {             
                List<PaymentPlanWrapper> ListPlan;
                ReportDataSource repds = new ReportDataSource();
                FlexibleReportManager p = new FlexibleReportManager();                

                repds.Name = ApplicationConstants.PaymentPlanDS;                
                if (Session[SessionConstants.paymentPLanSummary] != null)
                {
                    repds.Value = Session[SessionConstants.paymentPLanSummary];
                    e.DataSources.Add(repds);
                }             
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }
        #endregion

        #region"Load Payment Schedule"
        void LoadPaymentSchedule(object sender, SubreportProcessingEventArgs e)
        {
            try
            {                
                List<PaymentWrapper> PaymentList;
                RegularPaymentManager rpm = new RegularPaymentManager();
                if (Request.QueryString["PaymentPlanHistoryId"] != null)
                {
                    PaymentList = rpm.GetPaymentByPaymentPlanHistoryId(Int32.Parse(Request.QueryString["PaymentPlanHistoryId"].ToString()));
                }
                else
                {
                    PaymentList = rpm.GetPaymentByTenantId(base.GetTenantId());
                }
                ReportDataSource repds = new ReportDataSource();
                repds.Name = ApplicationConstants.PaymentScheduleDS;
                if (PaymentList == null)
                {
                    PaymentList = new List<PaymentWrapper>();
                    setMessage(UserMessageConstants.RetrievingPaymentschedule, true);
                }
                repds.Value = PaymentList;
                e.DataSources.Add(repds);
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.LoadingPaymentSchedule, true);
                }
            }
        }
        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Check Payment Plan Type"

        public bool CheckPaymentPlanType()
        {
            Payments paymentmanager = new Payments();
            string str = paymentmanager.GetPaymentPlanType(base.GetTenantId());
            if (str.Equals("Flexible"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        

    }

   



}


