﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMBlankMasterPage.Master" AutoEventWireup="true" CodeBehind="NoticeToVacateCasesList.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.NoticeToVacateCasesList" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript" language="javascript">
    function Clickheretoprint() {
        window.print();
        //         var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
        //         disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
        //         var content_vlue = document.getElementById("ReportViewer1").innerHTML;

        //         var docprint = window.open("", "", disp_setting);
        //         docprint.document.open();
        //         docprint.document.write('<html><head><title>Inel Power System</title>');
        //         docprint.document.write('</head><body onLoad="self.print()"><center>');
        //         docprint.document.write(content_vlue);
        //         docprint.document.write('</center></body></html>');
        //         docprint.document.close();
        //         docprint.focus();
    }
</script>
    <style type="text/css" media="print">
       .hidden_Print
       {
           display:none;
           }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:Panel ID="pnlMessage" runat="server" Visible="false">
              <asp:Label ID="lblMessage" runat="server"></asp:Label>
     </asp:Panel>
       <div align="left" class="hidden_Print">
        <asp:Button ID="btnPrint" runat="server" OnClientClick="window.print();" Text="Print" />    &nbsp;&nbsp;
        <asp:Button ID="btnBack" class="hide_print" OnClick="btnBack_Click" runat="server" Text="Back" />
    </div>
    <rsweb:ReportViewer ID="ReportViewer1" ShowToolBar="false" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" SizeToReportContent="true" InteractiveDeviceInfos="(Collection)" 
        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1175px">
        <LocalReport ReportPath="controls\reports\NoticeToVacateCasesList.rdlc">
        </LocalReport>
    </rsweb:ReportViewer>
</asp:Content>
