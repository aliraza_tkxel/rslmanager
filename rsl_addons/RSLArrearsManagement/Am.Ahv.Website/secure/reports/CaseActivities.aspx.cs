﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Am.Ahv.BusinessManager.reports;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.BusinessManager.Customers;
using Am.Ahv.Entities;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;


namespace Am.Ahv.Website.secure.reports
{
    public partial class CaseActivities : PageBase
    {
        #region"Attributes"
        bool isError;
        bool isException;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion

        #region"Events"        

        #region"Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            resetMessage();
            try
            {
                if (!IsPostBack)
                {
                    CaseActivitiesReportViewer.ProcessingMode = ProcessingMode.Local;
                    CaseActivitiesReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadUserSummary);
                    CaseActivitiesReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadCaseActivities);
                    CaseActivitiesReportViewer.ServerReport.Refresh();

                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex,"Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.LoadingPageError,true);
                }
            }

        }
        #endregion

        #region"Btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.viewActivitiesPath, false);
        }
        #endregion

        #endregion

        #region"Load Case Activities"
        void LoadCaseActivities(object sender, SubreportProcessingEventArgs e)
        {
            try
            {
                CaseActivitiesReportManager Mgr = new CaseActivitiesReportManager();

                List<CaseActivitiesWrapper> CaseList = Mgr.GetActivitiesReport(base.GetCaseId());

                    ReportDataSource repds = new ReportDataSource();
                    repds.Name = ApplicationConstants.CaseActivitiesDS;
                    repds.Value = CaseList;
                    e.DataSources.Add(repds);
                 if (CaseList.Count == 0)
                {
                      setMessage(UserMessageConstants.RetrievingCaseActivities,true);
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.LoadingCaseActivities,true);
                }
            }
        }

        #endregion

        #region"Load User Summary"
        void LoadUserSummary(object sender, SubreportProcessingEventArgs e)
        {
            try
            {

                CustomersReport CR = new CustomersReport();
                CustomerInformationWrapper customer = CR.GetCustomerInformation(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                if (customer != null)
                {
                    List<CustomerInformationWrapper> UserList = new List<CustomerInformationWrapper>(1);
                    UserList.Add(customer);
                    ReportDataSource repds = new ReportDataSource();
                    repds.Name = ApplicationConstants.UserSummaryDS;
                    repds.Value = UserList;
                    e.DataSources.Add(repds);
                }
                else
                {
                    List<AM_SP_GetCustomerInformation_Result> UserList = new List<AM_SP_GetCustomerInformation_Result>(1);
                    ReportDataSource repds = new ReportDataSource();
                    repds.Name = ApplicationConstants.UserSummaryDS;
                    repds.Value = UserList;
                    e.DataSources.Add(repds);
                    setMessage(UserMessageConstants.RetrievingUserSummary, true);
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    setMessage(UserMessageConstants.LoadingUserSummary, true);
                }
            }
        }

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
    }
}