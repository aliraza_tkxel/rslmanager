﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMBlankMasterPage.Master"
    AutoEventWireup="true" CodeBehind="BalanceList.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.BalanceList" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function Clickheretoprint() {
            window.print();
            //         var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
            //         disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
            //         var content_vlue = document.getElementById("ReportViewer1").innerHTML;

            //         var docprint = window.open("", "", disp_setting);
            //         docprint.document.open();
            //         docprint.document.write('<html><head><title>Inel Power System</title>');
            //         docprint.document.write('</head><body onLoad="self.print()"><center>');
            //         docprint.document.write(content_vlue);
            //         docprint.document.write('</center></body></html>');
            //         docprint.document.close();
            //         docprint.focus();
        }
    </script>
    <style type="text/css" media="print">
        .hidden_Print
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div class="hidden_Print">
        <asp:Button ID="btnPrint" CssClass="hidden_Print" runat="server" Text="Print" OnClientClick="Clickheretoprint()" />&nbsp;
        <asp:Button ID="btnBack" class="hide_print" OnClick="btnBack_Click" runat="server" Text="Back" />
    </div>
    <rsweb:ReportViewer ID="ReportViewerBalanceList" runat="server" Font-Names="Verdana"
        SizeToReportContent="true" Font-Size="8pt" InteractiveDeviceInfos="(Collection)"
        Width="895px" ShowToolBar="false" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
        <LocalReport ReportPath="controls\reports\BalanceList.rdlc" EnableHyperlinks="true">
        </LocalReport>
    </rsweb:ReportViewer>
    <div class="hidden_Print">
        <table align="center" class="tableFooter">
            <tr>
                <td class="style4">
                    <asp:Label ID="lblRecords" runat="server" Text="Records"></asp:Label>&nbsp;
                    <asp:Label ID="lblCurrentRecords" runat="server"></asp:Label>&nbsp;
                    <asp:Label ID="lblOf" runat="server" Text="of"></asp:Label>&nbsp;
                    <asp:Label ID="lblTotalRecords" runat="server"></asp:Label>
                </td>
                <td style="padding-left: 20px" class="style4">
                    <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>&nbsp;
                    <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>&nbsp;
                    <asp:Label ID="lblOfPage" runat="server" Text="of"></asp:Label>&nbsp;
                    <asp:Label ID="lblTotalPages" runat="server"></asp:Label>
                </td>
                <td style="padding-left: 10px" class="style9">
                    <asp:LinkButton ID="lbtnPrevious" runat="server" Text="< Previous" OnClick="llbtnPrevious_Click"></asp:LinkButton>
                </td>
                <td style="padding-left: 10px" class="style9">
                    <asp:LinkButton ID="lbtnNext" runat="server" Text="Next >" OnClick="llbtnNext_Click"></asp:LinkButton>
                </td>
                <td class="style4">
                    <asp:Label ID="lblRecordsPerPage" runat="server" Text="Records per page:"></asp:Label>
                </td>
                <td width="5%">
                    <asp:Label ID="lbl" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
