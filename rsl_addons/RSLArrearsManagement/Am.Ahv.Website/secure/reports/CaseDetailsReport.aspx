﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaseDetailsReport.aspx.cs"
    Inherits="Am.Ahv.Website.secure.reports.CaseDetailsReport" MasterPageFile="~/masterpages/AMBlankMasterPage.Master" %>

<%@ Register Src="~/controls/reports/CustomerSummary.ascx" TagName="CustomerSummary"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript" language="javascript">
        function Clickheretoprint() {
            var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
            disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
            var content_vlue = document.getElementById("print_content").innerHTML;

            var docprint = window.open("", "", disp_setting);
            docprint.document.open();
            docprint.document.write('<html><head><title></title>');
            docprint.document.write('</head><body onLoad="self.print()"><center>');
            docprint.document.write(content_vlue);
            docprint.document.write('</center></body></html>');
            docprint.document.close();
            docprint.focus();
        }
</script>
 <style media="print" type="text/css">
        .hide_print
        {
            display: none;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div align="left" class="hide_print">
        <asp:Button ID="btnPrint" runat="server" OnClientClick="window.print();" Text="Print" />    &nbsp;&nbsp;
        <asp:Button ID="btnBack" class="hide_print" OnClick="btnBack_Click" runat="server" Text="Back" />
    </div>
    <div id="print_content">
    <asp:Panel ID="pnlmain" runat="server" GroupingText=" " Width="900px" 
            HorizontalAlign="Left" Font-Names="Arial" Font-Size="11pt">
        <h3>
            <asp:Label ID="lblarrearscasedetails" runat="server" Text="Arrears Case Details"></asp:Label></h3>
        <hr />
        <asp:Panel ID="pnlcustomersummary" runat="server" Font-Names="Arial" 
            Font-Size="11pt">
            <uc1:CustomerSummary ID="CustomerSummary1" runat="server" />
        </asp:Panel>
        <asp:Panel ID="pnlcsdetails" runat="server" Font-Names="Arial" Font-Size="11pt">
            <table width="100%" cellpadding="5" cellspacing="0">
                <asp:Repeater ID="repcasedetails" runat="server" OnItemDataBound="repcasedetails_ItemDataBound" >
                    <HeaderTemplate>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td width="15%">
                                <asp:Label ID="lbldate" runat="server" Text="Date:"></asp:Label>
                            </td>
                            <td width="15%">
                                <asp:Label ID="lblstage" runat="server" Text="Stage"></asp:Label>
                                /<asp:Label ID="lblaction" runat="server" Text="Action:"></asp:Label>
                            </td>
                           
                            <td width="55%">
                                <asp:Label ID="lblNotes" runat="server" Text="Notes:"></asp:Label>
                            </td>

                            <td width="15%">
                                <asp:Label ID="lblAssignedTo" runat="server" Text="Assigned To:"></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>

                     <asp:Repeater ID="repcaseactivity"  runat="server" EnableViewState="true" ViewStateMode="Enabled" Visible="true">
                              <ItemTemplate>
                               <tr>
                         <td width="15%" valign="top">
                                 <asp:Label ID="Label3" runat="server" Text=' <%# this.FormatDate(Eval("RecordedDate").ToString())%> '></asp:Label>
                            </td>

                            <td width="15%" valign="top">
                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("AM_Status.Title") %>'></asp:Label>
                                /<br /><asp:Label ID="Label5" runat="server" Text='<%#Eval("AM_Action.Title") %>'></asp:Label>
                            </td>
                               <td width="55%">
                                        <%--<b>
                                            <asp:Label ID="lblcadatecreated" runat="server" Text=' <%# this.FormatDate(Eval("RecordedDate").ToString())%> '>
                                            </asp:Label></b> 
                                            &nbsp; <b>--%>
                                        <table>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <b><asp:Label ID="lblcatitle" runat="server" Text="Activity:"></asp:Label></b>
                                                        <asp:Label ID="lblcatitleusr" runat="server" Text='<%#Eval("AM_LookupCode.CodeName")%>'></asp:Label></b>
                                                        &nbsp; 
                                                        <b><asp:Label ID="lblcarecordedby" runat="server" Text="Recorded By:"></asp:Label></b>
                                                        <asp:Label ID="lblcarecordedbyusr" runat="server" Text='<%# LoadEmployeeName(Eval("RecordedBy").ToString())%>'></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>   
                                                    <div>
                                                        <asp:Label ID="lblcanotes" runat="server" Text='<%#Eval("Notes")%>'></asp:Label>
                                                    </div>
                                                 </td>
                                            </tr>    
                                            <tr>
                                                <td>
                                                    <div>
                                                        <asp:Label ID="lblcadocumentupload" runat="server" Text="Document Uploaded:" Visible="false"></asp:Label></b>
                                                        <asp:Label ID="lblcadocumentuploadusr" runat="server" Text='<%#Eval("ActionId")%>' Visible="false"></asp:Label></td>
                                                    </div>
                                                 </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div>
                                                         <b><asp:Label ID="lblcastandardletter" runat="server" Text="Standard Letter:" Visible='<%# this.CheckStandardLetterAttached(Eval("IsLetterAttached").ToString())=="No"?false:true %>' ></asp:Label></b>
                                                        <asp:Label ID="lblcastandardletterusr" runat="server" Text='<%# this.CheckStandardLetterAttached(Eval("IsLetterAttached").ToString()) %>' Visible='<%# this.CheckStandardLetterAttached(Eval("IsLetterAttached").ToString())=="No"?false:true %>'></asp:Label></td>
                                                    </div>
                                                </td>
                                           </tr>
                                    </table>
                        </td>      
                         <td width="15%" valign="top"><asp:Label ID="lblAssignedToin" runat="server" Text='<%# LoadEmployeeName(Eval("AM_Case.CaseOfficer").ToString()) %>'></asp:Label></td>
                                    </tr> 
                                     <tr>
                            <td colspan="4">
                                <br />
                            </td>
                        </tr>       
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td width="15%" valign="top">
                                 <asp:Label ID="lblcasehistoryid" runat="server" Text='<%#Eval("CaseHistoryId") %>' Visible="false"></asp:Label>
                                 <asp:Label ID="lbldateusr" runat="server" Text=' <%# this.FormatDate(Eval("CreatedDate").ToString())%> '></asp:Label>
                            </td>

                            <td width="15%" valign="top">
                                <asp:Label ID="lblstageusr" runat="server" Text='<%#Eval("AM_Status.Title") %>'></asp:Label>
                                /<br /><asp:Label ID="lblactionusr" runat="server" Text='<%#Eval("AM_Action.Title") %>'></asp:Label>
                            </td>

                            
                            <td width="55%" valign="top">
                                <div>
                                    <asp:Label ID="lblcasenotesusr" runat="server" Text='<%#Eval("Notes") %>' Visible='<%#Eval("IsActionIgnored").ToString()=="False"?true:false %>'></asp:Label>
                                </div>
                            </td>

                            <td width="15%" valign="top">
                                <asp:Label ID="Label1" runat="server" Text='<%# LoadEmployeeName(Eval("AM_Case.CaseOfficer").ToString()) %>'></asp:Label>
                            </td>
                         </tr>
                         <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <div>
                                    <b><asp:Label ID="lbldocumentupload" runat="server" Text="Document Uploaded:" Visible='<%# this.DocumentCount(Eval("IsDocumentUpload").ToString(), Eval("CaseHistoryId").ToString())=="0"?false:true %>'></asp:Label></b>
                                    <asp:Label ID="lbldocumentuploadusr" runat="server" Text='<%# this.DocumentCount(Eval("IsDocumentUpload").ToString(), Eval("CaseHistoryId").ToString()) %>' Visible='<%# this.DocumentCount(Eval("IsDocumentUpload").ToString(), Eval("CaseHistoryId").ToString())=="0"?false:true %>' ></asp:Label>
                                </div>
                            </td>
                            <td></td>
                         </tr>
                         <tr>
                            <td></td>
                            <td></td>
                            <td>               
                                <div>
                                     <asp:Label ID="lblstandardletter" runat="server" Text="Standard Letter:" Font-Bold="true" Visible='<%# this.CheckStandardLetterAttached(Eval("IsDocumentAttached").ToString())=="No"?false:true %>'></asp:Label></b>
                                      <asp:Label ID="lblstandardletterusr" runat="server" Text='<%# this.CheckStandardLetterAttached(Eval("IsDocumentAttached").ToString()) %>' Visible='<%# this.CheckStandardLetterAttached(Eval("IsDocumentAttached").ToString())=="No"?false:true %>'></asp:Label>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                         <tr>
                            <td></td>
                            <td></td>
                            <td>               
                               <div>
                                  <b> <asp:Label ID="lbloutcome" runat="server" Text="Outcome:" Visible='<%# Eval("OutcomeLookupCodeId")==null?false:true %>'></asp:Label></b>
                                      <asp:Label ID="lbloutcomeusr" runat="server" Text='<%# Eval("AM_LookupCode.CodeName") %>' Visible='<%# Eval("OutcomeLookupCodeId")==null?false:true %>'></asp:Label>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                         <tr>
                            <td></td>
                            <td></td>
                            <td>               
                               <div>  
                                   <b><asp:Label ID="lblreason" runat="server" Text="Reason:" Visible='<%# (Eval("Reason")=="" || Eval("Reason")==null || Eval("IsActionIgnored").ToString()=="True")  ?false:true %>'></asp:Label></b>
                                      <asp:Label ID="lblreasonusr" runat="server" Text='<%#Eval("Reason")%>' Visible='<%# (Eval("Reason")=="" || Eval("Reason")==null || Eval("IsActionIgnored").ToString()=="True")?false:true %>'></asp:Label>
                                    
                                      <b><asp:Label ID="Label2" runat="server" Text="Ignored Reason:" Visible='<%# Eval("IsActionIgnored").ToString()=="True"?true:false %>'></asp:Label></b>
                                      <asp:Label ID="Label6" runat="server" Text='<%#Eval("ActionIgnoreReason")%>' Visible='<%# Eval("IsActionIgnored").ToString()=="True"?true:false %>'></asp:Label>
                               </div>
                            </td>

                            

                            <td></td>
                        </tr>                 
                        <tr>
                            <td colspan="4">
                                <br />
                            </td>
                        </tr>   
                         
                                           

                    </ItemTemplate>
                    <FooterTemplate>
                        <tr>
                            <td width="100%" colspan="4">
                                <hr />
                                <span>Broadland Housing Association, NCFC, Jarrold Stand, Carrow Road, Norwich, NR1 1HU</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="4">
                                <span>Telephone:01603 750200   Fax: 01603 750222 Email:enq@broadlandhousing.org </span>
                            </td>
                        </tr>
                    </FooterTemplate>
                </asp:Repeater>
            </table>
        </asp:Panel>
    </asp:Panel>
    </div>
</asp:Content>
