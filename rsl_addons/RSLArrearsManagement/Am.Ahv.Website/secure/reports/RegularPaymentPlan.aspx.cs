﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Customers;
using Am.Ahv.BusinessManager.Dashboard;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.payments;
using Microsoft.Reporting.WebForms;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;
using System.Drawing;
namespace Am.Ahv.Website.secure.reports
{
    public partial class RegularPaymentPlan : PageBase
    {
        Am.Ahv.BusinessManager.Customers.Customers CR = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                //  base.RedirectToLoginPage();

                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();

            }
            HiderError();
            if (!IsPostBack)
            {
                if (CheckPaymentPlanType())
                {
                    LoadReports();
                }
                else
                {
                    Response.Redirect(PathConstants.flexiblePaymentReport + "?PaymentPlanHistoryId=" + Request.QueryString["PaymentPlanHistoryId"].ToString(), false);
                }
            }

        }

        #region"Btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["PaymentPlanHistoryId"] != null)
            {
                Response.Redirect(PathConstants.casehistory, false);
            }
            else
            {
                string path = Session[SessionConstants.PaymentPlanBackPage].ToString();
                Response.Redirect(path, false);
            }
        }
        #endregion

        /// <summary>
        /// Data Set Name Constants
        /// </summary>
        private const string UserSummaryDataset = "DataSet1";
        private const string PaymentPlanDataset = "PaymentPlanSummaryDataSet";
        private const string PaymentScheduleDataset = "test";
        private void LoadReports()
        {
            this.ReportViewer1.ProcessingMode = ProcessingMode.Local;
            this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadUserSummary);
            this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadPaymentPlan);
            this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadPaymentSchedule);
            this.ReportViewer1.ServerReport.Refresh();
        }
        void LoadUserSummary(object sender, SubreportProcessingEventArgs e)
        {
            try
            {
                List<PaymentPlanWrapper> ListPlan;
                FlexibleReportManager p = new FlexibleReportManager();
                BusinessManager.statusmgmt.CustomersReport CR = new BusinessManager.statusmgmt.CustomersReport();
                CustomerInformationWrapper customer = CR.GetCustomerInformation(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));

                if (Request.QueryString["PaymentPlanHistoryId"] != null)
                {
                    ListPlan = p.GetPaymentPlanSummaryHistory(Int32.Parse(Request.QueryString["PaymentPlanHistoryId"].ToString()));
                }
                else
                {
                    ListPlan = p.GetPaymentPlanSummaryWrappered(base.GetTenantId());
                }
                if (ListPlan == null)
                {
                    ListPlan = new List<PaymentPlanWrapper>(1);
                    ShowError(UserMessageConstants.RPPpaymentplan);
                    return;
                }
                else
                {
                    Session[SessionConstants.paymentPLanSummary] = ListPlan;
                }
                if (customer != null)
                {
                    customer.RentBalance = customer.RentBalance;
                    customer.Amount = "(£ " + Math.Round(Math.Abs(Convert.ToDouble(ListPlan[0].LastPayment)), 2).ToString("0,0.00") + ")  " + ListPlan[0].LastPaymentDate;
                    customer.LastPayment = ListPlan[0].LastPaymentDate;
                    customer.Date = ListPlan[0].CreatedDate;


                    List<CustomerInformationWrapper> UserList = new List<CustomerInformationWrapper>(1);
                    UserList.Add(customer);
                    ReportDataSource repds = new ReportDataSource();
                    repds.Name = UserSummaryDataset;
                    repds.Value = UserList;
                    e.DataSources.Add(repds);
                }
                else
                {
                    ShowError(UserMessageConstants.RPPusersummary);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowError(string message)
        {
            pnlError.Visible = true;
            lblerror.Visible = true;
            lblerror.Text = message;

            lblerror.ForeColor = Color.Red;
            lblerror.Font.Bold = true;

            upreportpanel.Update();

        }
        private void HiderError()
        {
            pnlError.Visible = false;
            lblerror.Visible = false;
            lblerror.Text = string.Empty;
        }
        void LoadPaymentPlan(object sender, SubreportProcessingEventArgs e)
        {
            try
            {

                List<PaymentPlanWrapper> ListPlan;
                ReportDataSource repds = new ReportDataSource();
                FlexibleReportManager p = new FlexibleReportManager();
                repds.Name = PaymentPlanDataset;

                if (Session[SessionConstants.paymentPLanSummary] != null)
                {
                    repds.Value = Session[SessionConstants.paymentPLanSummary];
                    e.DataSources.Add(repds);
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void LoadPaymentSchedule(object sender, SubreportProcessingEventArgs e)
        {
            try
            {

                RegularPaymentManager rpm = new RegularPaymentManager();
                List<PaymentWrapper> PaymentList;
                if (Request.QueryString["PaymentPlanHistoryId"] != null)
                {
                    PaymentList = rpm.GetPaymentByPaymentPlanHistoryId(Int32.Parse(Request.QueryString["PaymentPlanHistoryId"].ToString()));
                }
                else
                {
                    PaymentList = rpm.GetPaymentByTenantId(base.GetTenantId());
                }
                ReportDataSource repds = new ReportDataSource();
                repds.Name = ApplicationConstants.PaymentScheduleDS;
                if (PaymentList == null)
                {
                    PaymentList = new List<PaymentWrapper>(1);
                    ShowError(UserMessageConstants.RPPpaymentschedule);
                }

                repds.Value = PaymentList;
                e.DataSources.Add(repds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region"Check Payment Plan Type"

        public bool CheckPaymentPlanType()
        {
            Payments paymentmanager = new Payments();
            string str = paymentmanager.GetPaymentPlanType(base.GetTenantId());
            if (str.Equals("Regular"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion



      
    }
}