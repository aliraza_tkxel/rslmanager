﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegularPaymentPlan.aspx.cs"
    Inherits="Am.Ahv.Website.secure.reports.RegularPaymentPlan" MasterPageFile="~/masterpages/AMBlankMasterPage.Master" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css"> 
    body:nth-of-type(1) img[src*="Blank.gif"]{display:none;}
    </style>
    <script type="text/javascript" language="javascript">
        function Clickheretoprint() {
            window.print();
            
            //         var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
            //         disp_setting += "scrollbars=yes,width=650, height=600, left=100, top=25";
            //         var content_vlue = document.getElementById("ReportViewer1").innerHTML;

            //         var docprint = window.open("", "", disp_setting);
            //         docprint.document.open();
            //         docprint.document.write('<html><head><title>Inel Power System</title>');
            //         docprint.document.write('</head><body onLoad="self.print()"><center>');
            //         docprint.document.write(content_vlue);
            //         docprint.document.write('</center></body></html>');
            //         docprint.document.close();
            //         docprint.focus();
        }
    </script>
    <style type="text/css" media="print">
        .hidden_Print
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upreportpanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlError" runat="server" Visible="false">
                <asp:Label ID="lblerror" runat="server" Visible="false"></asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>
        <asp:Button ID="btnPrint" CssClass="hidden_Print" runat="server" Text="Print" OnClientClick="Clickheretoprint()" />
        &nbsp;&nbsp;
        <asp:Button ID="btnBack" class="hidden_Print" OnClick="btnBack_Click" runat="server"
            Text="Back" />
        <rsweb:ReportViewer ID="ReportViewer1" ShowToolBar="false" runat="server" Font-Names="Verdana"
            Font-Size="11pt" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
            WaitMessageFont-Size="14pt" Width="970px" ShowZoomControl="False" 
            SizeToReportContent="true" Height="50px" AsyncRendering="False" >
            <LocalReport ReportPath="controls\reports\RegularPaymentSummaryReport.rdlc">            
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
</asp:Content>
