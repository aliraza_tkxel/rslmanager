﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Entities;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using System.Data;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Website.pagebase;

namespace Am.Ahv.Website.secure.reports
{
    public partial class ArrearsBalanceReport : PageBase
    {
        #region Attributes

        Am.Ahv.BusinessManager.Dashboard.arrearskpi Cw = null;

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        } 

        #endregion

        #region"Events"       

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                PopulateData();
                //string str = FileOperations.getCurUrlAbsolutePath();
            }
        }

        #endregion

        #region"Btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.dashboardPath, false);
        }

        #endregion

        #endregion

        #region"Populate Data"

        public void PopulateData()
        {
            try
            {
                Cw = new BusinessManager.Dashboard.arrearskpi();
                List<AM_SP_Region_Amount_Result> list = Cw.GetRegionAmount(-1);
                if (list.Count > 0 && list != null)
                {
                    chartArrearsBalance.DataSource = list;
                    chartArrearsBalance.Series["chartSeries"].XValueMember = "Region";                
                    chartArrearsBalance.Series["chartSeries"].YValueMembers = ApplicationConstants.AKPITotalRent;
                    chartArrearsBalance.DataBind();
                    //Chart1.Visible = true;
                    lblNoRecord.Visible = false;
                }
                else
                {
                    lblNoRecord.Visible = true; 
                }
            }
            catch (ArgumentException arg)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arg, "Exception Policy");
            }
            catch (EntityException entitexcepption)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitexcepption, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingArrearsBalanceReport, true);
                }
            }
        }

        #endregion

        protected void chartArrearsBalance_Click(object sender, ImageMapEventArgs e)
        {
            
        }

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
    }
}