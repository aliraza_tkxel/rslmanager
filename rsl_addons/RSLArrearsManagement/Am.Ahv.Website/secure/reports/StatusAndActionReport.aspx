﻿<%@ Page Title="Stages And Action Report :: Arrears Management " Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="StatusAndActionReport.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.StatusAndActionReport" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 18%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <asp:Panel ID="pnlCaseList" Width="100%" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>

 <table width="100%">        
        <tr >
            <td class="tdClass" colspan="6" style=" padding-left:10px" >
                <asp:Label ID="lblStatusActionReport" runat="server" Font-Bold="true" ForeColor="Black" Font-Size="Medium" Text="Stage and Action Report"></asp:Label>
            </td>
        </tr>
        <tr >
            <td class="tdClass" colspan="6" style=" padding-left:25px">
                <asp:UpdatePanel ID="updpnlDropDownStatusActionReport" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>                    
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblAssignedTo" runat="server" Text="Assigned to:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAssignedTo" AutoPostBack="true" Width="310px" runat="server" 
                                onselectedindexchanged="ddlAssignedTo_SelectedIndexChanged">                                
                            </asp:DropDownList>                            
                        </td>
                    </tr>                    
                    <tr >
                        <td>
                            <asp:Label ID="lblRegion" runat="server" Text="Patch:"></asp:Label>                            
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRegion" AutoPostBack="true" Width="310px" runat="server" 
                                onselectedindexchanged="ddlRegion_SelectedIndexChanged">                                
                            </asp:DropDownList>                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblSuburb" runat="server" Text="Scheme:"></asp:Label>                            
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSuburb" AutoPostBack="false" Width="310px" runat="server">                                
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                            <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                onclick="btnSearch_Click" />        
                        </td>
                    </tr>
                </table>   
               
                </ContentTemplate>
                </asp:UpdatePanel>
                <br />              
            </td>            
        </tr>                      
        <tr>
            <td colspan="6" valign="top" class="tdClass" style=" padding-left:20px; padding-right:15px" >
               <asp:UpdatePanel ID="updpnlStatusActionReport" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
               <asp:Panel ID="pnlStatusActionReport" runat="server" Width="100%">
                <br />                  
                <cc1:PagingGridView ID="pgvStatusActionReport" EmptyDataText="No records found." 
                       EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" 
                                    EmptyDataRowStyle-HorizontalAlign="Center" 
                       RowStyle-Wrap="true" AllowPaging="false" GridLines="None"  runat="server" 
                       AutoGenerateColumns="false" Width="100%">
                    <Columns>                    
                        <asp:TemplateField HeaderText="Stage/Action" HeaderStyle-HorizontalAlign="Left" ShowHeader="true">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="50%" Height="25px" HorizontalAlign="Left" />
                            <ItemTemplate>                            
                                <asp:Label ID="lblStatusActionTitle" runat="server" Text='<%#Eval("Title") %>'></asp:Label>
                                <%--<asp:LinkButton ID="lbtnName" runat="server" Text='<%#Eval("CustomerName") %>' ForeColor="Black"></asp:LinkButton>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ignored" HeaderStyle-HorizontalAlign="Left" ShowHeader="true">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="25%" Height="25px" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblActionIgnoreCount" Font-Underline="true" runat="server" Text='<%#Eval("ActionIgnoreCount") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actioned" ShowHeader="true" HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="25%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>                                
                                <asp:Label ID="lblActionRecordCount" Font-Underline="true" runat="server" Text='<%#Eval("ActionRecordCount") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                    </Columns>
                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                    <PagerSettings Mode="NumericFirstLast" />
                    <RowStyle Wrap="True" />
                </cc1:PagingGridView>                
                </asp:Panel>
                    <table width="100%">
                        <tr>
                            <td width="50%" class="tableFooter">
                                <asp:Label ID="lblTotal" runat="server" Text="Total:" Font-Bold="true"></asp:Label>
                            </td>
                            <td width="25%" class="tableFooter">
                                <asp:Label ID="lblTotalIgnored" Font-Underline="true" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td width="25%" class="tableFooter">
                                <asp:Label ID="lblTotalActioned" Font-Underline="true" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                    </table>
                 <table width="100%" class="tableFooter">
                    <tr>
                        <td style="padding-left:20px" class="style1">
                            <asp:Label ID="lblRecords" runat="server" Text="Records"></asp:Label>&nbsp;
                            <asp:Label ID="lblCurrentRecords" runat="server"></asp:Label>&nbsp;
                            <asp:Label ID="lblOf" runat="server" Text="of"></asp:Label>&nbsp;
                            <asp:Label ID="lblTotalRecords" runat="server" ></asp:Label>
                        </td>
                        <td width="15%" style="padding-left:20px">
                             <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>&nbsp;
                             <asp:Label ID="lblCurrentPage" runat="server" ></asp:Label>&nbsp;
                             <asp:Label ID="lblOfPage" runat="server" Text="of"></asp:Label>&nbsp;
                             <asp:Label ID="lblTotalPages" runat="server" ></asp:Label>
                        </td>
                        <td width="10%" style="padding-left:10px">                 
                            <asp:LinkButton ID="lbtnPrevious" runat="server" Text="< Previous" 
                                onclick="llbtnPrevious_Click"></asp:LinkButton>
                        </td>
                        <td width="10%" style="padding-left:10px">
                            <asp:LinkButton ID="lbtnNext" runat="server" Text="Next >" 
                                onclick="llbtnNext_Click"></asp:LinkButton>
                        </td>
                        <td width="30%">
                        </td>
                        <td width="15%">
                            <asp:Label ID="lblRecordsPerPage" runat="server" Text="Records per page:"></asp:Label>
                        </td>
                        <td width="5%">
                            <asp:DropDownList ID="ddlRecordsPerPage" runat="server" AutoPostBack="true" 
                                onselectedindexchanged="ddlRecordsPerPage_SelectedIndexChanged">                                
                            </asp:DropDownList>
                        </td>                 
                    </tr>
                </table>  
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <span class="Error">Please wait...</span>
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
                </ContentTemplate>
                    </asp:UpdatePanel>               
            </td>
        </tr>        
        <tr>
            <td colspan="6" class="tdClass">
               <br />
               <br />
            </td>            
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
