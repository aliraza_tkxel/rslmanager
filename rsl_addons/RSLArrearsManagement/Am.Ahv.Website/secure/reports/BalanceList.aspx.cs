﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.reports;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using System.IO;

namespace Am.Ahv.Website.secure.reports
{
    public partial class BalanceList : PageBase
    {

        #region"Attributes"

        Am.Ahv.BusinessManager.reports.BalanceList balance = null;
        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int listCount;

        public int ListCount
        {
            get { return listCount; }
            set { listCount = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }

        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        #endregion

        #region"Events"

       

        #region"Btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.dashboardPath);
        }
        #endregion

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            ResetMessage();
            if(!IsPostBack)
            {
                //FileStream fs = null;
                //if (File.Exists("PagingConstants.txt"))
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Append);
                //}
                //else
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Create, FileAccess.ReadWrite);
                //}
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine(System.DateTime.Now.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.WriteLine(Request.PhysicalApplicationPath);
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.Close();
                //fs.Close();
                //string path = Request.PhysicalApplicationPath;
                SetDefaultPagingAttributes();
                LoadBalanceList(DbIndex, ApplicationConstants.pagingPageSizeBalanceList);
                SetPagingLabels();
                //ReportViewerBalanceList.ProcessingMode = ProcessingMode.Local;
                //ReportViewerBalanceList.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LoadBalanceList);
                //ReportViewerBalanceList.ServerReport.Refresh();                    
            }
        }

        #endregion

        #region"LBtn Next Click"

        protected void llbtnNext_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            IncreasePaging();
        }

        #endregion

        #region"Lbtn Previous Click"

        protected void llbtnPrevious_Click(object sender, EventArgs e)
        {
            GetViewStateValues();
            ReducePaging();
        }

        #endregion        

        #endregion

        #region"Load"

        #region"Load Balance List"

        void LoadBalanceList(int startIndex, int endIndex)
        {
            try
            {
                balance = new BusinessManager.reports.BalanceList();
                List<BalanceListWrapper> list = new List<BalanceListWrapper>();
                ReportDataSource reportDataSource = new ReportDataSource();                
                list = balance.GetBalanceList(startIndex, endIndex);
                ListCount = list.Count;
                reportDataSource.Name = ApplicationConstants.balanceListArrearsReport;
                reportDataSource.Value = list;
                ReportViewerBalanceList.LocalReport.DataSources.Clear();
                ReportViewerBalanceList.LocalReport.DataSources.Add(reportDataSource);
                ReportViewerBalanceList.DataBind();
                SetTotalRecordsCount();
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingArrearsBalanceReport, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion   

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            //PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
            CurrentPage = 1;
            DbIndex = 0;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            RowIndex = -1;
            RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            ListCount = 0;
            SetViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void SetViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageBalanceList] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexBalanceList] = DbIndex;
            ViewState[ViewStateConstants.totalPagesBalanceList] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsBalanceList] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexBalanceList] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageBalanceList] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsBalanceList] = CurrentRecords;
            ViewState[ViewStateConstants.ListCountBalanceList] = ListCount;

        }       

        #endregion

        #region"Get ViewState Values"

        public void GetViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageBalanceList]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexBalanceList]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesBalanceList]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsBalanceList]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsBalanceList]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexBalanceList]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageBalanceList]);
            ListCount = Convert.ToInt32(ViewState[ViewStateConstants.ListCountBalanceList]);
        }

        #endregion

        #region"Reduce Paging"

        public void ReducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                LoadBalanceList(DbIndex*ApplicationConstants.pagingPageSizeBalanceList, ApplicationConstants.pagingPageSizeBalanceList);                
                SetPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void IncreasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                LoadBalanceList(DbIndex * ApplicationConstants.pagingPageSizeBalanceList,ApplicationConstants.pagingPageSizeBalanceList);
                SetPagingLabels();
            }
        }

        #endregion

        #region"Set Paging Labels"

        public void SetPagingLabels()
        {
            try
            {
                int count = TotalRecords;

                if (count > ApplicationConstants.pagingPageSizeBalanceList)
                {
                    SetCurrentRecordsDisplayed();
                    SetTotalPages(count);
                    lblCurrentRecords.Text = (CurrentRecords).ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    lblCurrentRecords.Text = ListCount.ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = CurrentPage.ToString();
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    lbtnPrevious.Enabled = false;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    lbtnNext.Enabled = true;
                }
                //FileStream fs = null;
                //if (File.Exists("PagingConstants.txt"))
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Append);
                //}
                //else
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Create, FileAccess.ReadWrite);
                //}
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine(System.DateTime.Now.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.WriteLine("ToatlPages = " + TotalPages.ToString());
                //sw.WriteLine("Current Page = " + CurrentPage.ToString());
                //sw.WriteLine("Current Number of Records = " + CurrentRecords.ToString());
                //sw.WriteLine("Records in Grid View = " + pgvUsers.Rows.Count.ToString());
                //sw.WriteLine("Total Records = " + count.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.Close();
                //fs.Close();
                SetViewStateValues();
                lbl.Text = ApplicationConstants.pagingPageSizeBalanceList.ToString();
            }
            catch (FileLoadException flException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionCurrentRecordsFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Set Total Pages"

        public void SetTotalPages(int count)
        {
            try
            {
                if (count % ApplicationConstants.pagingPageSizeBalanceList > 0)
                {
                    TotalPages = (count / ApplicationConstants.pagingPageSizeBalanceList) + 1;
                }
                else
                {
                    TotalPages = (count / ApplicationConstants.pagingPageSizeBalanceList);
                }
            }
            catch (DivideByZeroException divide)
            {
                IsError = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionTotalPagesFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Displayed"

        public void SetCurrentRecordsDisplayed()
        {
            try
            {
                if (ListCount == ApplicationConstants.pagingPageSizeBalanceList)
                {
                    CurrentRecords = CurrentPage * ListCount;
                }
                else if (RemoveFlag == true)
                {
                    CurrentRecords -= 1;
                }
                else
                {
                    CurrentRecords += ListCount;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionCurrentRecordsFirstDetection, true);
                }
            }

        }

        #endregion

        #region"Set Total Records Count"

        public void SetTotalRecordsCount()
        {
            try
            {
                balance = new BusinessManager.reports.BalanceList();
                TotalRecords = 0;//balance.GetTotalBalanceListRecordsCount();
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        #endregion

    }
}