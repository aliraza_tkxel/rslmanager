﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master"
    AutoEventWireup="true" CodeBehind="BalanceListReport.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.BalanceListReport" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" >
    function redirectToExportToExcel() {
        window.open("ExportBalanceListReport.aspx", "", "width=900,height=600,left=100,top=100,scrollbars=1,resizable=1");

    }
    function openCustomerRecord(customerId) {
        wd = window.open("/Customer/CRM.asp?CustomerID=" + customerId, "_blank", "toolbar=yes, scrollbars=yes, resizable=yes, width=800, height=600");
    }
</script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlCaseList" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 10px">
                    <asp:Label ID="lblBalanceList" runat="server" Font-Bold="true" ForeColor="Black"
                        Font-Size="Medium" Text="Balance List"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 25px">
                    <asp:UpdatePanel ID="updpnlDropDown" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td style=' width:25%;'  >
                                        <asp:Label ID="lblAssignedTo" runat="server" Text="Case Officer:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlAssignedTo" AutoPostBack="true" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlAssignedTo_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                         <td colspan="4" align="center"    >Search Rent Balance between:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRegion" runat="server" Text="Patch:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRegion" AutoPostBack="true" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right"> £</td>
                                   
                                   <td >  <asp:TextBox ID="txtFrom" runat="server" Width="100" ></asp:TextBox>  </td>
   <td align="right" >And £</td> <td  > <asp:TextBox ID="txtTo" runat="server" Width="100"></asp:TextBox></td>
                                  
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSuburb" runat="server" Text="Scheme:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSuburb" AutoPostBack="false" Width="310px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                               
                                </tr>
                                <tr>
                                    <td style=' width:25%;'>
                                        <asp:Label ID="lblCustomerStatus" runat="server" Text="Customer Status:"></asp:Label>
                                    </td>
                                    <td class="style1">
                                        <asp:DropDownList ID="ddlCustomerStatus" AutoPostBack="false" Width="310px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style=' width:45%;'>
                                        <asp:Label ID="lblAssetType" runat="server" Text="Asset Type:"></asp:Label>
                                    </td>
                                    <td class="style1">
                                        <asp:DropDownList ID="ddlAssetType" AutoPostBack="false" Width="310px" runat="server">
                                        </asp:DropDownList>
                                       
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>  <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" /></td>
                                     <td align="right"   style=' width:10%;'   >
                                       <asp:Button ID="btnExport" runat="server" Text="Export to CSV" 
                                            OnClientClick="redirectToExportToExcel();" OnClick="btnExport_Click"     />
                                     </td> 
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="6" valign="top" class="tdClass" style="padding-left: 20px; padding-right: 15px">
                    <asp:UpdatePanel ID="updpnlSuppressedCases" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlGridSuppressedCases" runat="server" Width="100%">
                                <cc1:PagingGridView ID="pgvBalanceList" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true"
                                    EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center"
                                    AllowPaging="false" GridLines="None" runat="server" AutoGenerateColumns="false"
                                    OnSorting="pgvBalanceList_sorting" OnRowDataBound="pgvBalanceList_RowDataBound"
                                    ShowFooter="True" 
                                    onselectedindexchanged="pgvBalanceList_SelectedIndexChanged">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tenancy" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="TENANCYID" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="false" />
                                            <ItemStyle Width="7%" Height="25px" HorizontalAlign="Left" />
                                            <FooterStyle Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("TENANCYID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Left" SortExpression="CUSTOMERID"
                                            ShowHeader="true" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="12%" Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate><a href='#' runat="server" onclick='<%# String.Format("openCustomerRecord({0})", Eval("CUSTOMERID")) %>' >
                                                <asp:Label ID="Label1" runat="server" Text='<%#  Eval("FULLNAME")%>' ></asp:Label></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Last Payment" ShowHeader="true" SortExpression="LASTPAYMENTDATE"
                                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# Eval("LASTPAYMENTDATE") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label runat="server" Text="Totals:"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance(Gross)" ShowHeader="true" SortExpression="RentBalance"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label ForeColor='<%#this.SetForeColor(Eval("RENTBALANCE").ToString()) %>' runat="server"
                                                    Text='<%# this.SetBracsAndCurrency(Eval("RENTBALANCE").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalRentBalance" runat="server" Text=""></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ant HB" ShowHeader="true" SortExpression="ESTHB" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" ForeColor='<%# this.SetForeColor(Eval("ESTHB").ToString()) %>'
                                                    Text='<%# this.SetBracsAndCurrency(Eval("ESTHB").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalHB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Adv HB" ShowHeader="true" SortExpression="ADVHB" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="8%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" ForeColor='<%# this.SetForeColor(Eval("ADVHB").ToString()) %>'
                                                    Text='<%# this.SetBracsAndCurrency(Eval("ADVHB").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalADVHB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance(Net)" ShowHeader="true" SortExpression="NETARREARS"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblNetRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("NETARREARS").ToString()) %>'
                                                 Text='<%# this.SetBracsAndCurrency(Eval("NETARREARS").ToString()) %>'
                                                ></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalNetRentBalance" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sales Ledger Balance" ShowHeader="true" SortExpression="SALESLEDGERBALANCE"
                                            HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" BackColor="#EEEEEE" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# SetBracsAndCurrency(Eval("SALESLEDGERBALANCE").ToString()) %>' ForeColor='<%# this.SetForeColor(Eval("SALESLEDGERBALANCE").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalSLB" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle BackColor="#EEEEEE" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total" ShowHeader="true" SortExpression="TOTAL" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="10%" HorizontalAlign="Right" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotal" runat="server" Text='<%# SetBracsAndCurrency(Eval("TOTAL").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblSumTotal" runat="server"></asp:Label>
                                            </FooterTemplate>
                                            <FooterStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <FooterStyle CssClass="tableFooter" />
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <RowStyle Wrap="True" />
                                </cc1:PagingGridView>
                            </asp:Panel>
                            <table width="100%" class="tableFooter">
                                <tr>
                                    <td width="20%" style="padding-left: 20px">
                                        <asp:Label ID="lblRecords" runat="server" Text="Records"></asp:Label>&nbsp;
                                        <asp:Label ID="lblCurrentRecords" runat="server"></asp:Label>&nbsp;
                                        <asp:Label ID="lblOf" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:Label ID="lblTotalRecords" runat="server"></asp:Label>
                                    </td>
                                    <td width="20%" style="padding-left: 20px">
                                        <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>&nbsp;
                                        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>&nbsp;
                                        <asp:Label ID="lblOfPage" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:Label ID="lblTotalPages" runat="server"></asp:Label>
                                    </td>
                                    <td width="6%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnFirst" runat="server" Text="<< First" OnClick="lbtnFirst_Click"></asp:LinkButton>
                                    </td>
                                    <td width="8%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnPrevious" runat="server" Text="< Previous" OnClick="llbtnPrevious_Click"></asp:LinkButton>
                                    </td>
                                    <td width="6%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnNext" runat="server" Text="Next >" OnClick="llbtnNext_Click"></asp:LinkButton>
                                    </td>
                                    <td width="6%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnLast" runat="server" Text="Last >>" OnClick="lbtnLast_Click"></asp:LinkButton>
                                    </td>
                                    <td width="10%">
                                    </td>
                                    <td width="15%">
                                        <asp:Label ID="lblRecordsPerPage" runat="server" Text="Records per page:"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:DropDownList ID="ddlRecordsPerPage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRecordsPerPage_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <span class="Error">Please wait...</span>
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="tdClass">
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
