﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master"
    AutoEventWireup="true" CodeBehind="PaymentPlanMonitoring.aspx.cs" Inherits="Am.Ahv.Website.secure.reports.PaymentPlanMonitoring" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlCaseList" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 10px">
                    <asp:Label ID="lblPaymentPlanMonitoring" runat="server" Font-Bold="true" ForeColor="Black"
                        Font-Size="Medium" Text="Payment Plan Monitoring"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 25px">
                    <asp:UpdatePanel ID="updpnlDropDown" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAssignedTo" runat="server" Text="Assigned to:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlAssignedTo" AutoPostBack="true" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlAssignedTo_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRegion" runat="server" Text="Patch:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRegion" AutoPostBack="true" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSuburb" runat="server" Text="Scheme:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSuburb" AutoPostBack="True" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlSuburb_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:CheckBox ID="ddlShowMissed" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged"
                                            Text="                   " />
                                    </td>
                                    <td>
                                        <asp:Label ID="ddlLabelShow0" runat="server" Text="Show Missed Payments"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="6" valign="top" class="tdClass" style="padding-left: 20px; padding-right: 15px">
                    <asp:UpdatePanel ID="updpnlPaymentPlanMonitoring" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlGridPaymentPlanMonitoring" runat="server" Width="100%">
                                <cc1:PagingGridView ID="pgvPaymentPlanMonitoring" EmptyDataText="No records found."
                                    EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center"
                                    RowStyle-Wrap="true" AllowPaging="false" GridLines="None" runat="server" AutoGenerateColumns="false"
                                    Width="100%" OnSorting="pgvPaymentPlanMonitoring_sorting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tenancy Id:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="AM_PaymentPlan.TennantId" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbltenancyId" runat="server" Text='<%#Eval("TenancyId") %>' Visible="true"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="CustomerName" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="15%" Height="25px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomerId" Visible="false" runat="server" Text='<%#Eval("CustomerId") %>'></asp:Label>
                                                <asp:Label ID="lblTenantId" runat="server" Visible="false" Text='<%# Eval("tenancyId")%>'></asp:Label>
                                                <asp:LinkButton ID="lbtnName" OnClick="lbtnName_Click" runat="server" CommandArgument='<%#Eval("CaseId") %>'
                                                    Text='<%# this.SetCustomerName(Eval("CustomerName").ToString(), Eval("CustomerName2").ToString(), Eval("JointTenancyCount").ToString()) %>'
                                                    ForeColor="Black"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance:" ShowHeader="true" SortExpression="RentBalance"
                                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="15%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRentBalance" Font-Bold="true" runat="server" Text='<%# this.SetBracs(Eval("RentBalance").ToString()) %>'
                                                    ForeColor='<%# this.SetForeColor(Eval("RentBalance").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Monthly Rent:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="WeeklyRent" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="25px" />
                            <ItemTemplate>                                
                                <asp:Label ID="lblWeeklyRent" runat="server" Text='<%# this.SetBracs(Eval("WeeklyRent").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Payment Plan:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="AM_PaymentPlan.AmountToBeCollected" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="8%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentPlan" runat="server" Text='<%# this.SetBracs(Eval("PaymentPlan").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="1st Missed:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="FirstMissedDate" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="12%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblStart" runat="server" Text='<%# Eval("FirstMissedDate")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="AM_PaymentPlan.EndDate" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="12%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblEnd" runat="server" Text='<%# Eval("EndDate")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="To Date:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="8%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("ToDate")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payments Remaining:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="8%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentsRemaining" runat="server" Text='<%# Eval("RemainingPayments")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Missed:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="8%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblMissed" runat="server" Text='<%# Eval("Missed")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Review Date:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="ReviewDate" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="15%" HorizontalAlign="Left" Height="25px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblReviewDate" runat="server" Text='<%# Eval("ReviewDate")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField>
                            <ItemStyle Width="10%" />
                            <ItemTemplate>
                                <asp:Button ID="btnViewCase" runat="server" Text="View Case" />
                            </ItemTemplate>
                        </asp:TemplateField>    --%>
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <RowStyle Wrap="True" />
                                </cc1:PagingGridView>
                            </asp:Panel>
                            <table width="100%" class="tableFooter">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblTotal" Font-Bold="true" runat="server" Text="Total"></asp:Label>
                                    </td>
                                    <td style="padding-left: 5px">
                                        <asp:Label ID="lblTotalRentBalance" Font-Bold="true" ForeColor="Red" runat="server"
                                            Text="Total"></asp:Label>
                                    </td>
                                    <td colspan="5">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%" style="padding-left: 20px">
                                        <asp:Label ID="lblRecords" runat="server" Text="Records"></asp:Label>&nbsp;
                                        <asp:Label ID="lblCurrentRecords" runat="server"></asp:Label>&nbsp;
                                        <asp:Label ID="lblOf" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:Label ID="lblTotalRecords" runat="server"></asp:Label>
                                    </td>
                                    <td width="20%" style="padding-left: 20px">
                                        <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>&nbsp;
                                        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>&nbsp;
                                        <asp:Label ID="lblOfPage" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:Label ID="lblTotalPages" runat="server"></asp:Label>
                                    </td>
                                    <td width="10%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnPrevious" runat="server" Text="< Previous" OnClick="llbtnPrevious_Click"></asp:LinkButton>
                                    </td>
                                    <td width="10%" style="padding-left: 10px">
                                        <asp:LinkButton ID="lbtnNext" runat="server" Text="Next >" OnClick="llbtnNext_Click"></asp:LinkButton>
                                    </td>
                                    <td width="15%">
                                    </td>
                                    <td width="15%">
                                        <asp:Label ID="lblRecordsPerPage" runat="server" Text="Records per page:"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:DropDownList ID="ddlRecordsPerPage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRecordsPerPage_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                <ProgressTemplate>
                                    <span class="Error">Please wait...</span>
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            
        </table>
    </asp:Panel>
</asp:Content>
