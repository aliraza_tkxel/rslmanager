﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Am.Ahv.Website.masterpages
{
    public delegate void reloadOutcome(bool status);
    public partial class Outcome : PageBase 
    {
        public event reloadOutcome reloadOutcome;

       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
                Outcome1.invokeEvent += delegate(bool eventInvoke, bool edit)
                {
                    if (eventInvoke == true)
                    {
                        if (edit == true)
                        {
                            if (!Convert.ToString(Session[SessionConstants.EditOutcomeId]).Equals(string.Empty))
                            {
                                AddOutcome1.Visible = true;
                                AddOutcome1.SetOutcomeTextField();
                            }
                            else
                            {
                                AddOutcome1.Visible = true;
                            }
                        }
                        else
                        {
                            AddOutcome1.Visible = true;
                            Session.Remove(SessionConstants.EditOutcomeId);
                            AddOutcome1.ResetField();
                        }
                    }
                };
                AddOutcome1.reload += delegate(bool status)
                {
                    if (status == true)
                    {
                        Outcome1.ReloadControl();

                        //AddOutcome1.Visible = false;
                        // upOutcomes.Update();
                        // reloadOutcome(true);
                        //populateGridView();
                    }
                };            
        }
    }
}
