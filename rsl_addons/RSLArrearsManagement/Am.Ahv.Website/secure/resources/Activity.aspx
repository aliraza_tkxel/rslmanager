﻿<%@ Page Title="Add New Activity :: Arrears Management" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master"
    AutoEventWireup="true" CodeBehind="Activity.aspx.cs" Inherits="Am.Ahv.Website.masterpages.Activity" %>

<%@ Register Src="~/controls/resources/Activity.ascx" TagName="Activity" TagPrefix="uc1" %>
<%@ Register Src="~/controls/resources/AddActivity.ascx" TagName="AddActivity" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upactivity" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td valign="top">
                        <uc1:Activity ID="Activity1" runat="server" />
                    </td>
                    <td valign="top">
                        <uc2:AddActivity ID="AddActivity1" EnableViewState="true" Visible="true" runat="server" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <span class="Error">Please wait...</span>
            <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
