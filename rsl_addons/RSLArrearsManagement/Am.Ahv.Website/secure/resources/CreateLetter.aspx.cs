﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Website.pagebase;
using System.Data;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.BusinessManager.resource;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using System.Drawing;
using Am.Ahv.BusinessManager.letters;
using System.Text;
using System.Text.RegularExpressions;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.utitility_classes;
using System.Collections;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Am.Ahv.BusinessManager.Customers;

namespace Am.Ahv.Website.secure.resources
{
    public partial class CreateLetter : PageBase
    {
        #region"Attributes"

        static bool isCustomerEditView = false ;
        Status statusManager = null;
        LetterResourceArea letterManager = null;
        Users resourceManager = null;
        CaseDetails caseDetails = null;
        int OldLetterHistoryID = 0;
        Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker addCaseWorkerManager = null;
        Am.Ahv.BusinessManager.statusmgmt.Action actionManager = null;

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }

            //fckTextEditor.BasePath = Path.Combine(this.ResolveUrl(fckTextEditor.BasePath), "/");
            ResetMessage();
            btnSave.Attributes.Add("onClick", "return confirm('Are you sure you want to save the letter?');");
            btnSaveAsPDF.Attributes.Add("onClick", "return confirm('Are you sure you want to save the letter?');");
            if (!IsPostBack)
            {
                //fckTextEditor.BasePath = "../../controls" + this.ResolveUrl(fckTextEditor.BasePath);
                //fckTextEditor.BasePath = Path.Combine(this.ResolveUrl(fckTextEditor.BasePath), "/fckeditor/");
                fckTextEditor.BasePath = "~/" + this.ResolveUrl(fckTextEditor.BasePath);
                //  RemoveCreateLetterCache();
                if (Request.QueryString["cmd"] == null && Request.QueryString["id"] == null)
                {
                    PopulateLookups();
                }
                isCustomerEditView = false;
                if (Request.QueryString["cmd"] == "edit")
                {
                    if (Request.QueryString["TID"] != null && Validation.CheckIntegerValue(Request.QueryString["TID"]))
                    {
                        isCustomerEditView = true;
                        base.SetTenantId(Convert.ToInt32(Request.QueryString["TID"]));

                    }
                    if (Validation.CheckIntegerValue(Request.QueryString["id"]))
                    {
                        base.SetStandardLetterIdSession(Convert.ToInt32(Request.QueryString["id"]));
                        EditLetter(false);
                    }
                    
                    Session[ViewStateConstants.PageUrl] = Request.UrlReferrer;
                }
                else if (Request.QueryString["cmd"] == "cedit" || Request.QueryString["cmd"] == "activityedit")
                {
                    if (Request.QueryString["id"] != null)
                    {
                        if (Request.QueryString["TID"] != null && Validation.CheckIntegerValue(Request.QueryString["TID"]))
                        {
                            isCustomerEditView = true;
                            base.SetTenantId(Convert.ToInt32(Request.QueryString["TID"]));

                        }
                        if (Request.QueryString["hId"] != null)
                        {
                            if (Validation.CheckIntegerValue(Request.QueryString["hId"]))
                            {
                                Session[SessionConstants.StandardLetterHistoryIdCreateLetter] = Convert.ToInt32(Request.QueryString["hId"]);
                                OldLetterHistoryID = Convert.ToInt32(Session[SessionConstants.StandardLetterHistoryIdCreateLetter]);
                                Session[SessionConstants.IsStandardLetterHistoryCreateLetter] = "yes";
                                base.SetStandardLetterIdSession(Convert.ToInt32(Request.QueryString["id"]));
                                EditLetter(true);
                                btnSaveAsPDF.Text = "Save Letter";
                                ShowSignatureRequiredLabels();
                            }
                        }
                        else if (Validation.CheckIntegerValue(Request.QueryString["id"]))
                        {
                            base.SetStandardLetterIdSession(Convert.ToInt32(Request.QueryString["id"]));
                            Session[SessionConstants.IsStandardLetterHistoryCreateLetter] = "no";
                            EditLetter(false);
                            btnSaveAsPDF.Text = "Save Letter";
                            ShowSignatureRequiredLabels();
                        }
                    }
                    // If the user is coming back after previewing.
                    if (Request.QueryString["ispreviewback"] != null)
                    {
                        if (Request.QueryString["ispreviewback"] == "yes")
                        {
                            if (Convert.ToString(Session[SessionConstants.IsStandardLetterHistoryCreateLetter]) == "yes")
                            {
                                EditLetter(true);
                            }
                            else
                            {
                                EditLetter(false);
                            }
                            ShowSignatureRequiredLabels();
                        }
                    }
                    // To show page COntrols.
                    if (Request.QueryString["cmd"] == "cedit")
                    {
                        btnSave.Visible = false;
                        btnSaveAsPDF.Visible = true;
                        lbtnBack.Visible = false;
                        btnBacktoCase.Visible = true;
                        btnSaveAsPDF.Text = "Save Letter";
                    }
                    else
                    {
                        btnSave.Visible = false;
                        btnSaveAsPDF.Visible = true;
                        lbtnBack.Visible = false;
                        btnBacktoCase.Visible = false;
                    }
                    if (Request.UrlReferrer == null)
                    {
                        Response.Redirect(PathConstants.mycaselistef, false);
                    }
                    else if (!Request.UrlReferrer.AbsoluteUri.Contains("LetterPreview.aspx"))
                    {
                        Session[ViewStateConstants.PageUrl] = Request.UrlReferrer;
                    }
                }
                else if (Request.QueryString["cmd"] == "new")
                {
                    Session.Remove(SessionConstants.StandardLetterIdCreateLetter);
                    Session.Remove(SessionConstants.StandardLetterHistoryIdCreateLetter);
                    PopulateLookups();
                }
                else if (Request.QueryString["cmd"] == "backedit")
                {
                    if (Request.QueryString["Id"] != null && Request.QueryString["TID"] != null)
                    {
                        if (Validation.CheckIntegerValue(Request.QueryString["TID"]))
                        {
                            isCustomerEditView = true;
                            base.SetTenantId(Convert.ToInt32(Request.QueryString["TID"]));
                        }
                        if (Validation.CheckIntegerValue(Request.QueryString["Id"]))
                        {
                            base.SetStandardLetterIdSession(Convert.ToInt32(Request.QueryString["Id"]));
                            EditLetter(false);
                        }
                        Session[ViewStateConstants.PageUrl] = Request.UrlReferrer;
                    }
                    else
                        PopulateLookups();
                }
            }
        }

        #endregion

        #region"DDL Status Selected Index Changed"

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlStatus.SelectedValue != "-1")
                {
                    PopulateActionDropDown();
                }
                else
                {
                    ResetActionDropDown();
                }
                updpnlStatus.Update();
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.errorActionDropDownCreateLetter, true);
                }
            }
        }

        #endregion


        #region"DDL Contact Selected Index Changed"

        protected void ddlContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                manualAddressChk.Checked = false;
                saveToCAddressSession();
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.errorActionDropDownCreateLetter, true);
                }
            }
        }

        #endregion


        #region"Manual Address Checkbox"
        protected void manualAddress_CheckChanged(object sender, EventArgs e)
        {
            saveToCAddressSession();
            if (manualAddressChk.Checked)
            {
                addressTbl.Attributes.Add("style", "display:table;");
                ddlContact.SelectedValue = "-1";
            }
            else
            {
                addressTbl.Attributes.Add("style", "display:none;");
            }
        }
        #endregion


        #region"Individual Letter  Checkbox"
        protected void individualLetterCHK_CheckChanged(object sender, EventArgs e)
        {
            base.SetIsInvidualLetterReq(Convert.ToInt32(individualLetterCHK.Checked));
        }
        #endregion

        #region"Btn Reset Click"

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ResetPage();
            base.RemoveCreateLetterCache();
            updpnlMain.Update();
        }

        #endregion

        #region"DDL Team Selected Index Changed"

        protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTeam.SelectedValue != "-1")
                {
                    PopulateFromDropDown();
                }
                else
                {
                    ResetFromDropDown();
                }
                updpnlSignature.Update();
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.errorFromDropDownCreateLetter, true);
                }
            }
        }

        #endregion

        #region"Btn Preview Click"

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            int minutes = Convert.ToInt32(ConfigurationManager.AppSettings["CachedTimer"]);
            TimeSpan span = new TimeSpan(0, minutes, 0);
            try
            {
                if (manualAddressChk.Checked)
                {
                    List<string> data = new List<string>();
                    base.SetIsManualAddress(1);
                    data.Add(txtName.Text);
                    data.Add(txtAdd1.Text);
                    data.Add(txtAdd2.Text);
                    data.Add(txtAdd3.Text);
                    data.Add(txtCityTown.Text);
                    data.Add(txtPostCode.Text);
                    base.SetManualAddreesData(data);
                    Session.Remove(ApplicationConstants.CustomerContactAdd);
                }
                if (Request.QueryString["cmd"] == null)
                {

                    Session[ApplicationConstants.Letter] = fckTextEditor.Value;
                    // Cache.Insert(ApplicationConstants.Letter,fckTextEditor.Value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, span);
                    CacheLookups();
                    Response.Redirect(PathConstants.previewTemplate, false);
                }
                else if (Request.QueryString[ApplicationConstants.cmd] == "edit")
                {
                    Session[ApplicationConstants.Letter] = fckTextEditor.Value;
                    //  Cache.Insert(ApplicationConstants.Letter, fckTextEditor.Value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, span);
                    CacheLookups();
                    if (Request.QueryString["TID"] != null)
                    {
                        saveToCAddressSession();
                        Response.Redirect(PathConstants.previewLetter + "&type=edit&TID=" + Convert.ToInt32(Request.QueryString["TID"]), false);
                    }
                    else
                        Response.Redirect(PathConstants.previewTemplate + "&type=edit", false);
                }
                else if (Request.QueryString[ApplicationConstants.cmd] == "backedit")
                {
                    Session[ApplicationConstants.Letter] = fckTextEditor.Value;
                    //  Cache.Insert(ApplicationConstants.Letter, fckTextEditor.Value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, span);
                    CacheLookups();
                    if (Request.QueryString["TID"] != null)
                    {
                        saveToCAddressSession();
                        Response.Redirect(PathConstants.previewLetter + "&type=edit&TID=" + Convert.ToInt32(Request.QueryString["TID"]), false);
                    }
                    else
                        Response.Redirect(PathConstants.previewTemplate + "&type=edit", false);
                }
                else if (Request.QueryString[ApplicationConstants.cmd] == "cedit")
                {
                    if (ValidateSignatureFields())
                    {
                        Session[ApplicationConstants.Letter] = fckTextEditor.Value;
                        //Cache.Insert(ApplicationConstants.Letter, fckTextEditor.Value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, span);
                        CacheLookups();
                        Response.Redirect(PathConstants.previewLetter + "&type=cedit", false);
                    }
                }
                else if (Request.QueryString[ApplicationConstants.cmd] == "activityedit")
                {
                    if (ValidateSignatureFields())
                    {
                        Session[ApplicationConstants.Letter] = fckTextEditor.Value;
                        //Cache.Insert(ApplicationConstants.Letter, fckTextEditor.Value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, span);
                        CacheLookups();
                        Response.Redirect(PathConstants.previewLetter + "&type=aedit", false);
                    }
                }
                else if (Request.QueryString["cmd"] == "new")
                {
                    Session[ApplicationConstants.Letter] = fckTextEditor.Value;
                    // Cache.Insert(ApplicationConstants.Letter, fckTextEditor.Value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, span);
                    CacheLookups();
                    Response.Redirect(PathConstants.previewTemplate + "&type=new", false);
                }
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"Btn Cancel Click"

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ResetPage();
            base.RemoveCreateLetterCache();
            ResetMessage();
            Session.Remove(SessionConstants.IsStandardLetterHistoryCreateLetter);
            if (Request.QueryString["cmd"] != null)
            {
                if (Request.QueryString["Id"] != null && Request.QueryString["TID"] != null)
                {
                    if (Session[CacheConstants.PageCommand] != null)
                        Response.Redirect(PathConstants.LetterTemplatesArea + "?cmd=" + Session[CacheConstants.PageCommand] + "&SID=" + base.GetStatusId() + "&AID=" + base.GetActionId(), false);
                    else
                        Response.Redirect(PathConstants.LetterTemplatesArea + "?cmd=UpdateCaseSelect&SID=" + base.GetStatusId() + "&AID=" + base.GetActionId(), false);
                }
                else if (Request.QueryString["cmd"] == "new" || Request.QueryString["cmd"] == "edit")
                {
                    Response.Redirect(PathConstants.LetterTemplatesArea + "?cmd=new");
                }
                else
                {
                    BackToCaseRedirection();
                }
            }
            //updpnlMain.Update();
        }

        #endregion

        #region"Btn Save As PDF Click"

        protected void btnSaveAsPDF_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateFields() && ValidateSignatureFields())
                {
                    string filname = GetDestinationPage();
                    if (base.GetStandardLetterId() == null)
                    {
                        if (filname == "UpdateCase.aspx")
                        {
                            RemovePageSessions();
                            Response.Redirect(PathConstants.updatecase + "?cmd=sexpire", false);
                        }
                        else
                        {
                            RemovePageSessions();
                            Response.Redirect(PathConstants.openNewCase + "?cmd=sexpire", false);
                        }
                    }
                    //updpnlButton.Update();
                    int historyId = SaveLetter(true, true);
                    BacktoCaseWithHistory(historyId);
                }
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.errorSavingLetterCreateLetter, true);
                }
            }
        }

        #endregion

        #region"Btn Back To Case Click"
        // applicable only if the Source page is Open Case or Update Case.
        protected void btnBacktoCase_Click(object sender, EventArgs e)
        {
            Session.Remove(SessionConstants.IsStandardLetterHistoryCreateLetter);
            BackToCaseRedirection();
        }

        #endregion

        #region"DDl Personalization Selected Index Changed"

        protected void ddlPersonalization_SelectedIndexChanged(object sender, EventArgs e)
        {
            //updpnlPersonalization.Update();
            /*
            string option = ddlPersonalization.SelectedItem.Text;
            switch(option)
            {
                case "Weekly Rent":
                    TextEditorPersonalization(option);
                    break;
                case "Market Rent":
                    TextEditorPersonalization(option);
                    break;
                case "Current Rent Balance":
                    TextEditorPersonalization(option);
                    break;
                case "Todays Date":
                    TextEditorPersonalization(option);
                    break;
                default:
                    rteLetter.Text = hflText.Value;
                    break;
            }
             */
        }

        #endregion

        #region"Btn Save Click"

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateFields())
                {
                    if (manualAddressChk.Checked)
                    {
                        List<string> data = new List<string>();
                        base.SetIsManualAddress(1);
                        data.Add(txtName.Text);
                        data.Add(txtAdd1.Text);
                        data.Add(txtAdd2.Text);
                        data.Add(txtAdd3.Text);
                        data.Add(txtCityTown.Text);
                        data.Add(txtPostCode.Text);
                        base.SetManualAddreesData(data);
                        Session.Remove(ApplicationConstants.CustomerContactAdd);
                    }
                    if (isCustomerEditView)
                        saveToCAddressSession();
                    SaveLetter(false, false);
                    if (Request.QueryString["Id"] != null && Request.QueryString["TID"] != null)
                    {
                        if (Session[CacheConstants.PageCommand] != null)
                            Response.Redirect(PathConstants.LetterTemplatesArea + "?cmd=" + Session[CacheConstants.PageCommand] + "&SID=" + base.GetStatusId() + "&AID=" + base.GetActionId(), false);
                        else
                            Response.Redirect(PathConstants.LetterTemplatesArea + "?cmd=UpdateCaseSelect&SID=" + base.GetStatusId() + "&AID=" + base.GetActionId(), false);
                    }
                    else
                        Response.Redirect(PathConstants.LetterTemplatesArea, false);
                    //ResetPage();
                    //updpnlMain.Update();                    
                }
                else
                {
                    return;
                }
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.errorSavingLetterCreateLetter, true);
                }
            }
        }

        #endregion

        #region"Lbtn Back Click"

        protected void lbtnBack_Click(object sender, EventArgs e)
        {
            RemoveCreateLetterCache();
            Response.Redirect(PathConstants.LetterTemplatesArea, false);
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Populate Lookups"

        public void PopulateLookups()
        {
            try
            {
                statusManager = new Status();
                List<Am.Ahv.Entities.AM_Status> statusList = statusManager.GetAllStatus();
                ddlStatus.DataSource = statusManager.GetAllStatus();
                ddlStatus.DataTextField = "Title";
                ddlStatus.DataValueField = "StatusId";
                ddlStatus.DataBind();
                ddlStatus.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                ddlStatus.SelectedValue = "-1";

                ddlAction.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                ddlAction.SelectedValue = "-1";

                letterManager = new LetterResourceArea();
                //ddlPersonalization.DataSource = letterManager.GetPersonalization();
                //ddlPersonalization.DataTextField = "CodeName";
                //ddlPersonalization.DataValueField = "LookupCodeId";
                //ddlPersonalization.DataBind();
                //ddlPersonalization.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                //ddlPersonalization.SelectedValue = "-1";

                ddlSignOff.DataSource = letterManager.GetSignOff();
                ddlSignOff.DataTextField = "CodeName";
                ddlSignOff.DataValueField = "LookupCodeId";
                ddlSignOff.DataBind();
                ddlSignOff.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                ddlSignOff.SelectedValue = "-1";

                addCaseWorkerManager = new BusinessManager.Casemgmt.AddCaseWorker();
                ddlTeam.DataSource = addCaseWorkerManager.GetAllTeams();
                ddlTeam.DataValueField = ApplicationConstants.teamId;
                ddlTeam.DataTextField = ApplicationConstants.teamname;
                ddlTeam.DataBind();
                ddlTeam.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", ApplicationConstants.defaulvalue));
                ddlTeam.SelectedValue = ApplicationConstants.defaulvalue;

                ddlFrom.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                ddlFrom.SelectedValue = "-1";

                ddlContact.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                ddlFrom.SelectedValue = "-1";

                if (CheckCache())
                {
                    PopulateStandardLetterFromCache();
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
        }

        #endregion

        #region"Populate Contact Drop Down"

        public void PopulateContactDropDrown()
        {
            try
            {
                Customers customerManager = new Customers();
                IQueryable list = null;
                list = customerManager.getAddressAndTypeByCustomerId((base.GetCustomerId()));
                if (list != null)
                {
                    ddlContact.Items.Clear();
                    ddlContact.DataSource = list;
                    ddlContact.DataValueField = "ADDRESSID";
                    ddlContact.DataTextField = "DESCRIPTION";
                    ddlContact.DataBind();
                    ddlContact.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                    ddlContact.SelectedValue = "-1";
                }
                else
                {
                    ResetContactDropDown();
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion





        #region"Populate Action Drop Down"

        public void PopulateActionDropDown()
        {
            try
            {
                actionManager = new BusinessManager.statusmgmt.Action();
                List<AM_Action> list = null;
                list = actionManager.GetActionByStatus(int.Parse(ddlStatus.SelectedValue));
                if (list != null && list.Count > 0)
                {
                    ddlAction.Items.Clear();
                    ddlAction.DataSource = list;
                    ddlAction.DataValueField = "ActionId";
                    ddlAction.DataTextField = "Title";
                    ddlAction.SelectedValue = list[0].ActionId.ToString();
                    ddlAction.DataBind();
                    ddlAction.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                    ddlAction.SelectedValue = "-1";
                }
                else
                {
                    ResetActionDropDown();
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Populate From Drop Down"

        public void PopulateFromDropDown()
        {
            try
            {
                letterManager = new LetterResourceArea();
                DataTable data = letterManager.GetTeamMembers(int.Parse(ddlTeam.SelectedValue));

                if (data != null && data.Rows.Count > 0)
                {
                    ddlFrom.Items.Clear();
                    string id = data.Rows[0]["ResourceId"].ToString();
                    ddlFrom.DataSource = data;
                    ddlFrom.DataValueField = "ResourceId";
                    ddlFrom.DataTextField = "EmployeeName";
                    ddlFrom.SelectedValue = id;
                    ddlFrom.DataBind();
                    ddlFrom.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
                    ddlFrom.SelectedValue = "-1";

                }
                else
                {
                    ResetFromDropDown();
                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Populate Standatrd Letter"

        public void PopulateStandardLetter(int id)
        {
            try
            {
                letterManager = new LetterResourceArea();
                AM_StandardLetters letter = new AM_StandardLetters();
                letter = letterManager.GetStandardLetter(id);
                ddlStatus.SelectedValue = letter.StatusId.ToString();

                PopulateActionDropDown();
                ddlAction.SelectedValue = letter.ActionId.ToString();
                txtTitle.Text = letter.Title;
                txtCode.Text = letter.Code;
                fckTextEditor.Value = HttpUtility.HtmlDecode(letter.Body);
                if (letter.SignOffLookupCode != null)
                {
                    ddlSignOff.SelectedValue = letter.SignOffLookupCode.ToString();
                }
                if (letter.TeamId != null)
                {
                    ddlTeam.SelectedValue = letter.TeamId.ToString();
                    PopulateFromDropDown();
                }
                if (letter.FromResourceId != null)
                {
                    ddlFrom.SelectedValue = letter.FromResourceId.ToString();
                }
                if (isCustomerEditView)
                    enableCustomerContactEditView(letter, null);
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region"Populate Standard Letter From History"
        public void enableCustomerContactEditView(AM_StandardLetters letter, AM_StandardLetterHistory letterhistory)
        {
            bool jointTenantLetterReq = false;
            letterManager = new LetterResourceArea();
            PopulateContactDropDrown();
            //if (letter != null || letterhistory != null)
            //{
            //    int? addCID = 0, thirdPartyCID = 0;
            //    if (letter != null)
            //    {
            //        addCID = letter.AdditionalContactID;
            //        thirdPartyCID = letter.ThirdPartyContactID;
            //        jointTenantLetterReq = letter.JointTenantLetterRequired == 1 ? true : false;
            //    }
            //    else
            //    {
            //        addCID = letterhistory.AdditionalContactID;
            //        thirdPartyCID = letterhistory.ThirdPartyContactID;
            //        jointTenantLetterReq = letterhistory.JointTenantLetterRequired == 1 ? true : false;
            //    }
            //    if (addCID == -1 && thirdPartyCID == -1)
            //    {
            //        ddlContact.SelectedValue = "-1";
            //    }
            //    else if (addCID != -1)
            //    {
            //        ddlContact.SelectedValue = addCID.ToString();
            //    }
            //    else
            //    {
            //        AM_StandardLetters_ThirdPartyContacts thirdPartyContact = letterManager.getThirdPartyContactDetails((int)thirdPartyCID);
            //        List<string> contactDetails = new List<string>();
            //        contactDetails.Add(thirdPartyContact.ContactName);
            //        contactDetails.Add(thirdPartyContact.Address1);
            //        contactDetails.Add(thirdPartyContact.Address2);
            //        contactDetails.Add(thirdPartyContact.Address3);
            //        contactDetails.Add(thirdPartyContact.TownCity);
            //        contactDetails.Add(thirdPartyContact.PostCode);
            //        base.SetManualAddreesData(contactDetails);
            //        lblAdd1.Text = thirdPartyContact.Address1;
            //        lblAdd2.Text = thirdPartyContact.Address2;
            //        lblAdd3.Text = thirdPartyContact.Address3;
            //        lblCityTown.Text = thirdPartyContact.TownCity;
            //        lblContact.Text = thirdPartyContact.ContactName;
            //        lblPostCode.Text = thirdPartyContact.PostCode;
            //        manualAddressChk.Checked = true;
            //        manualAddress_CheckChanged(null, null);
            //    }
            //}
            //else
            {
                jointTenantLetterReq = base.GetIsInvidualLetterReq() == 1 ? true : false;
                if (base.GetIsManualAddress() == 1)
                {
                    List<string> contactDetails = base.GetManualAddreesData();
                    txtName.Text = contactDetails.ElementAt(0);
                    txtAdd1.Text = contactDetails.ElementAt(1);
                    txtAdd2.Text = contactDetails.ElementAt(2);
                    txtAdd3.Text = contactDetails.ElementAt(3);
                    txtCityTown.Text = contactDetails.ElementAt(4);
                    txtPostCode.Text = contactDetails.ElementAt(5);
                    manualAddressChk.Checked = true;
                    manualAddress_CheckChanged(null, null);
                }
                else
                {
                    ddlContact.SelectedValue = base.GetCustomerContactAddressID().ToString();
                }
            }
            tenantAddressRow.Attributes.Add("style", "display:table-row;");
            if(checkCustomer().Contains('&'))
                jointTenantPanel.Attributes.Add("style", "display:table-cell;");
            individualLetterCHK.Checked = jointTenantLetterReq;
            individualLetterCHK_CheckChanged(null,null);
        }
        #endregion



        #region"Populate Standard Letter From History"

        public void PopulateStandardLetterFromHistory(int id)
        {
            try
            {
                letterManager = new LetterResourceArea();
                AM_StandardLetterHistory letter = new AM_StandardLetterHistory();
                letter = letterManager.GetStandardLetterHistoryById(id);
                if (letter != null)
                {
                    ddlStatus.SelectedValue = letter.StatusId.ToString();

                    PopulateActionDropDown();
                    ddlAction.SelectedValue = letter.ActionId.ToString();
                    txtTitle.Text = letter.Title;
                    txtCode.Text = letter.Code;
                    fckTextEditor.Value = HttpUtility.HtmlDecode(letter.Body);
                    if (letter.SignOffLookUpCode != null)
                    {
                        ddlSignOff.SelectedValue = letter.SignOffLookUpCode.ToString();
                    }
                    if (letter.TeamId != null)
                    {
                        ddlTeam.SelectedValue = letter.TeamId.ToString();
                        PopulateFromDropDown();
                    }
                    if (letter.FromResourceId != null)
                    {
                        ddlFrom.SelectedValue = letter.FromResourceId.ToString();
                    }
                    if (isCustomerEditView)
                        enableCustomerContactEditView(null, letter);
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Population Standard Letter From Cache"

        public void PopulateStandardLetterFromCache()
        {
            if (Session[CacheConstants.StatusCreateLetter] != null)
            {
                ddlStatus.SelectedValue = Session[CacheConstants.StatusCreateLetter].ToString();
            }
            if (Session[CacheConstants.ActionCreateLetter] != null)
            {
                PopulateActionDropDown();
                ddlAction.SelectedValue = Session[CacheConstants.ActionCreateLetter].ToString();
            }
            if (Session[CacheConstants.TitleCreateLetter] != null)
            {
                txtTitle.Text = Session[CacheConstants.TitleCreateLetter].ToString();
            }
            if (Session[CacheConstants.CodeCreateLetter] != null)
            {
                txtCode.Text = Session[CacheConstants.CodeCreateLetter].ToString();
            }
            if (Session[ApplicationConstants.Letter] != null)
            {
                fckTextEditor.Value = Session[ApplicationConstants.Letter].ToString();
            }
            if (Session[ApplicationConstants.SignOff] != null)
            {
                System.Web.UI.WebControls.ListItem item = ddlSignOff.Items.FindByText(Session[ApplicationConstants.SignOff].ToString());
                ddlSignOff.SelectedValue = item.Value;
            }
            if (Session[ApplicationConstants.Team] != null)
            {
                System.Web.UI.WebControls.ListItem item = ddlTeam.Items.FindByText(Session[ApplicationConstants.Team].ToString());
                ddlTeam.SelectedValue = item.Value;
                PopulateFromDropDown();
            }
            if (Session[ApplicationConstants.From] != null)
            {
                System.Web.UI.WebControls.ListItem item = ddlFrom.Items.FindByText(Session[ApplicationConstants.From].ToString());
                ddlFrom.SelectedValue = item.Value;
            }
            if (isCustomerEditView)
                enableCustomerContactEditView(null, null);
            //if (Cache[CacheConstants.PersonalizationCreateLetter] != null)
            //{
            //    //ddlPersonalization.SelectedValue = Cache[CacheConstants.PersonalizationCreateLetter].ToString();
            //}           

        }

        #endregion

        #endregion

        #region"Functions"

        #region"Remove Page Sessions"

        public void RemovePageSessions()
        {
            Session.Remove(ViewStateConstants.PageUrl);
            Session.Remove(SessionConstants.isSavedPDFCreateLetter);
            Session.Remove(SessionConstants.IsStandardLetterHistoryCreateLetter);
        }

        #endregion

        #region"Save Letter"

        public int SaveLetter(bool isPdf, bool isCaseEdit)
        {
            try
            {

                resourceManager = new Users();
                caseDetails = new CaseDetails();
                AM_Case currCase = new AM_Case();
                letterManager = new LetterResourceArea();
                AM_StandardLetters standardLetters = new AM_StandardLetters();
                AM_StandardLetterHistory standardLetterHistory = new AM_StandardLetterHistory();
                AM_StandardLetters_ThirdPartyContacts SLThirdPartyContacts = new AM_StandardLetters_ThirdPartyContacts();
                

                standardLetters = SetStandardLetter(isPdf);
                standardLetterHistory = SetStandardLetterHistory(isPdf);
                
                currCase.JointTenantLetterRequired = individualLetterCHK.Checked == true ? 1 : 0;
                //standardLetters.JointTenantLetterRequired = individualLetterCHK.Checked == true ? 1 : 0;
                //standardLetterHistory.JointTenantLetterRequired = individualLetterCHK.Checked == true ? 1 : 0;
               
                //Saving Data for THIRD PARTY CONTACT & ADDITIONAL CONTACT 
                if (ddlContact.SelectedValue != "-1" && !manualAddressChk.Checked)
                {
                    currCase.AdditionalContactID = int.Parse(ddlContact.SelectedValue);
                    currCase.ThirdPartyContactID = 0;
                    //standardLetters.AdditionalContactID = int.Parse(ddlContact.SelectedValue);
                    //standardLetters.ThirdPartyContactID = 0;
                    //standardLetterHistory.AdditionalContactID = int.Parse(ddlContact.SelectedValue);
                    //standardLetterHistory.ThirdPartyContactID = 0;
                }
                else if (ddlContact.SelectedValue == "-1" && manualAddressChk.Checked)
                {
                    currCase.AdditionalContactID = -1;
                    //standardLetters.AdditionalContactID = -1;
                    //standardLetterHistory.AdditionalContactID = -1;
                    SLThirdPartyContacts = thirdPartyContactToDT(base.GetManualAddreesData());
                    if (base.GetStandardLetterId() <= 0)
                    {
                        currCase.ThirdPartyContactID = letterManager.SaveThirdPartyContact(SLThirdPartyContacts);
                        //standardLetters.ThirdPartyContactID = letterManager.SaveThirdPartyContact(SLThirdPartyContacts);
                        //standardLetterHistory.ThirdPartyContactID = standardLetters.ThirdPartyContactID;
                    }
                    else
                    {
                        currCase.ThirdPartyContactID = letterManager.UpdateThirdPartyContact(SLThirdPartyContacts);
                        //standardLetters.ThirdPartyContactID = letterManager.UpdateThirdPartyContact(SLThirdPartyContacts);
                        //standardLetterHistory.ThirdPartyContactID = standardLetters.ThirdPartyContactID;
                    }
                }
                caseDetails.UpdateCaseContactInfo(base.GetCaseId(), currCase, isCustomerEditView);
                if (!isCaseEdit)
                {

                    if (base.GetStandardLetterId() <= 0)
                    {
                        standardLetters.CreatedBy = base.GetLoggedInUserID();
                        standardLetterHistory.CreatedBy = base.GetLoggedInUserID();

                        standardLetters.CreatedDate = DateTime.Now;
                        standardLetterHistory.CreatedDate = DateTime.Now;

                        standardLetters.ModifiedBy = base.GetLoggedInUserID();
                        standardLetterHistory.ModifiedBy = base.GetLoggedInUserID();

                        standardLetters.ModifiedDate = DateTime.Now;
                        standardLetterHistory.ModifiedDate = DateTime.Now;

                        letterManager.SaveLetter(standardLetters, standardLetterHistory);
                        //SetMessage(UserMessageConstants.lettersaveSuccessCreateLetter, false);
                        return -1;
                    }
                    else
                    {
                        standardLetters.ModifiedBy = base.GetLoggedInUserID();
                        standardLetterHistory.ModifiedBy = base.GetLoggedInUserID();

                        standardLetters.ModifiedDate = DateTime.Now;
                        standardLetterHistory.ModifiedDate = DateTime.Now;

                        standardLetters.StandardLetterId = base.GetStandardLetterId();
                        standardLetterHistory.StandardLetterId = base.GetStandardLetterId();

                        standardLetterHistory.CreatedBy = base.GetLoggedInUserID();
                        standardLetterHistory.CreatedDate = DateTime.Now;
                        letterManager.UpdateStandardLetter(standardLetters, standardLetterHistory);
                        //SetMessage(UserMessageConstants.updateLetterCreateLetter, false);
                        return -1;
                    }
                }
                else
                {
                    
                    standardLetterHistory.ModifiedBy = base.GetLoggedInUserID();                    
                    standardLetterHistory.ModifiedDate = DateTime.Now;                    
                    standardLetterHistory.StandardLetterId = base.GetStandardLetterId();
                    standardLetterHistory.CreatedBy = base.GetLoggedInUserID();
                    standardLetterHistory.CreatedDate = DateTime.Now;

                    return letterManager.AddEditedStandardLetter(standardLetterHistory);
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Save As PDF"

        public void SaveAsPDF()
        {
            try
            {
                string[] f = DateTime.Today.GetDateTimeFormats();
                letterManager = new LetterResourceArea();
                //Document doc = new Document();
                Document doc = new Document(iTextSharp.text.PageSize.A4, 57, 57, 103, 73);
                string filePath = Request.PhysicalApplicationPath + "/Letter1.pdf";
                MemoryStream ms = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                String str = HttpUtility.HtmlDecode(fckTextEditor.Value);
                string newString = "";
                StringReader sr = null;

                string OrganisationName = ConfigurationManager.AppSettings["OrganisationName"].ToString();
                string OrganisationAddress = ConfigurationManager.AppSettings["OrganisationAddress"].ToString();
                string OrganisationPhone = ConfigurationManager.AppSettings["OrganisationPhone"].ToString();
                string OrganisationEmail = ConfigurationManager.AppSettings["OrganisationEmail"].ToString();

                // Phrase footerText = new Phrase(OrganisationName + "," + OrganisationAddress + " Tel: " + OrganisationPhone + " Email: " + OrganisationEmail);
                // HeaderFooter headerFooter = new HeaderFooter(footerText, false);
                //headerFooter.Border = iTextSharp.text.Rectangle.TOP_BORDER;
                //headerFooter.Alignment = iTextSharp.text.Rectangle.ALIGN_CENTER;
                //doc.Footer = headerFooter;

                if (str.Contains("/style/images/userfiles/"))
                {
                    newString = str.Replace("/style/images/userfiles/file/", Server.MapPath(PathConstants.LetterTemplateImages) + "\\file\\");
                    sr = new StringReader(newString);
                }
                else
                {
                    sr = new StringReader(str);
                }
                HTMLWorker htmlWork = new HTMLWorker(doc);
                doc.Open();

                //string stripped = Regex.Replace(str, @"<(.|\n)*?>", string.Empty);
                AM_SP_GetCustomerInfoForLetter_Result customer = letterManager.GetCustomerInfo(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                // doc.Add(new Paragraph(f[25].Substring(0, f[25].Length - 4)));
                // doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph(checkCustomer()));
                doc.Add(new Paragraph(customer.HOUSENUMBER + ", " + customer.ADDRESS1));
                doc.Add(new Paragraph(customer.ADDRESS2));
                doc.Add(new Paragraph(customer.ADDRESS3));
                doc.Add(new Paragraph(customer.TOWNCITY));
                doc.Add(new Paragraph(customer.COUNTY));
                doc.Add(new Paragraph(customer.POSTCODE));
                doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph("Tenancy Ref: " + base.GetTenantId().ToString()));
                doc.Add(new Paragraph("Date: " + DateTime.Today.ToString("dd/MM/yyyy")));
                doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph("Dear " + checkCustomer()));
                doc.Add(new Paragraph(Environment.NewLine));
                htmlWork.Parse(sr);
                doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph(ddlSignOff.SelectedItem.Text + ","));
                doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph(Environment.NewLine));
                doc.Add(new Paragraph(ddlFrom.SelectedItem.Text));
                doc.Add(new Paragraph(getJobTitle()));
                doc.Add(new Paragraph("Direct Dial: " + getWorkDirectDial()));

                if (getWorkEmail() != null)
                {
                    if (getWorkEmail() != "")
                    {
                        doc.Add(new Paragraph("Email: " + getWorkEmail()));
                    }
                }

                //doc.Add(new Paragraph(ddlTeam.SelectedItem.Text));
                //doc.Add(new Paragraph(resourceManager.getJobTitle(Convert.ToInt32(ddlFrom.SelectedValue))));


                //doc.Add(new Paragraph("<Organisation Name>, <Organisation Address> Tel: <Organisation Telephone Number> Email: <Organisation Email>"));

                doc.Close();
                Response.Clear();
                Response.AppendHeader("content-disposition", "attachment; filename= Letter1.pdf");
                Response.ContentType = "application/pdf";
                Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                Response.OutputStream.Flush();
                File.Delete(filePath);
                //Response.End();
                //HttpContext.Current.Response.End();     
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Reset Action Drop Down"

        public void ResetActionDropDown()
        {
            ddlAction.Items.Clear();
            ddlAction.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
            ddlAction.SelectedValue = "-1";
        }

        #endregion

        #region"Reset Contact Drop Down"

        public void ResetContactDropDown()
        {
            ddlContact.SelectedValue = "-1";
            Session.Remove(ApplicationConstants.CustomerContactAdd);
        }

        #endregion

        #region"Reset From Drop Down"

        public void ResetFromDropDown()
        {
            ddlFrom.Items.Clear();
            ddlFrom.Items.Add(new System.Web.UI.WebControls.ListItem("Please Select", "-1"));
            ddlFrom.SelectedValue = "-1";
        }

        #endregion


        #region"Reset Manual Address Table"

        public void ResetManualAddressTable()
        {
            manualAddressChk.Checked = false;
            txtName.Text = string.Empty;
            txtAdd1.Text = string.Empty;
            txtAdd2.Text = string.Empty;
            txtAdd3.Text = string.Empty;
            txtCityTown.Text = string.Empty;
            txtPostCode.Text = string.Empty;
            clearManualAddressSession();
            manualAddress_CheckChanged(null, null);
        }

        #endregion

        #region"Reset Page"

        public void ResetPage()
        {
            ddlStatus.SelectedValue = "-1";
            txtTitle.Text = string.Empty;
            txtCode.Text = string.Empty;
            ResetActionDropDown();
            fckTextEditor.Value = string.Empty;
            //ddlPersonalization.SelectedValue = "-1";
            ddlSignOff.SelectedValue = "-1";
            ddlTeam.SelectedValue = "-1";
            ResetFromDropDown();
            ResetContactDropDown();
            ResetManualAddressTable();
            RemoveCreateLetterCache();
            Session.Remove(ApplicationConstants.IsInvidualLetterReq);
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = System.Drawing.Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Text Editor Personalization"

        public void TextEditorPersonalization(string type)
        {
            int index = int.Parse(hflIndex.Value);

            String data = hflText.Value.ToString();
            StringBuilder sb = new StringBuilder(data);
            Array ar = data.ToArray();
            Regex.Replace(data, @"<(.|\n)*?>", string.Empty);

            //sb.Insert(index, "{" + type + "}");
            string str = data;
            //   rteLetter.Text = sb.ToString();
        }

        #endregion

        #region"Edit Letter"

        public void EditLetter(bool isStandardLetterHistory)
        {
            try
            {
                lblAddaNewTemplate.Visible = false;
                lblEditTemplate.Visible = true;
                PopulateLookups();
                if (!CheckCache())
                {
                    if (isStandardLetterHistory)
                    {
                        PopulateStandardLetterFromHistory(int.Parse(Session[SessionConstants.StandardLetterHistoryIdCreateLetter].ToString()));
                    }
                    else
                    {
                        PopulateStandardLetter(base.GetStandardLetterId());
                    }
                }
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.errorEditLetterCreateLetter, true);
                }
            }
        }

        #endregion

        #region"Show Signature Required Labels"

        public void ShowSignatureRequiredLabels()
        {
            lblRequiredSignOff.Visible = true;
            lblRequiredTeam.Visible = true;
            lblRequiredFrom.Visible = true;
        }

        #endregion

        #region"Session Lookups"
        private void CacheLookups()
        {

            if (ddlSignOff.SelectedValue != "-1")
            {
                Session["SignOff"] = ddlSignOff.SelectedItem.Text;
            }
            if (ddlTeam.SelectedValue != "-1")
            {
                Session["Team"] = ddlTeam.SelectedItem.Text;

                if (ddlFrom.SelectedValue != "-1")
                {
                    Session["From"] = ddlFrom.SelectedItem.Text;
                }
            }
            Session[CacheConstants.StatusCreateLetter] = ddlStatus.SelectedValue;
            Session[CacheConstants.ActionCreateLetter] = ddlAction.SelectedValue;
            Session[CacheConstants.TitleCreateLetter] = txtTitle.Text;
            Session[CacheConstants.CodeCreateLetter] = txtCode.Text;
        }

        #endregion

        #region"Set Standard Letter"

        public AM_StandardLetters SetStandardLetter(bool isPdf)
        {
            AM_StandardLetters standardLetters = new AM_StandardLetters();

            standardLetters.Body = HttpUtility.HtmlEncode(fckTextEditor.Value);
            standardLetters.Code = txtCode.Text;
            standardLetters.Title = txtTitle.Text;
            standardLetters.StatusId = int.Parse(ddlStatus.SelectedValue);
            standardLetters.ActionId = int.Parse(ddlAction.SelectedValue);
            if (isPdf)
            {
                standardLetters.IsPrinted = true;
            }
            else
            {
                standardLetters.IsPrinted = false;
            }
            if (ddlSignOff.SelectedValue != "-1")
            {
                standardLetters.SignOffLookupCode = int.Parse(ddlSignOff.SelectedValue);
            }
            if (ddlTeam.SelectedValue != "-1")
            {
                standardLetters.TeamId = int.Parse(ddlTeam.SelectedValue);
            }
            if (ddlFrom.SelectedValue != "-1")
            {
                standardLetters.FromResourceId = int.Parse(ddlFrom.SelectedValue);
            }

            standardLetters.IsActive = true;

            return standardLetters;
        }

        #endregion


        #region

        public AM_StandardLetters_ThirdPartyContacts thirdPartyContactToDT(List<string> contactData)
        {
            AM_StandardLetters_ThirdPartyContacts thirdPartyContact = new AM_StandardLetters_ThirdPartyContacts();

            thirdPartyContact.ContactName = contactData.ElementAt(0);
            thirdPartyContact.Address1 = contactData.ElementAt(1);
            thirdPartyContact.Address2 = contactData.ElementAt(2);
            thirdPartyContact.Address3 = contactData.ElementAt(3);
            thirdPartyContact.TownCity = contactData.ElementAt(4);
            thirdPartyContact.PostCode = contactData.ElementAt(5);

            return thirdPartyContact;
        }
        #endregion



        #region"Set Standard Letter History"

        public AM_StandardLetterHistory SetStandardLetterHistory(bool isPdf)
        {
            AM_StandardLetterHistory standardLetterHistory = new AM_StandardLetterHistory();

            standardLetterHistory.Body = HttpUtility.HtmlEncode(fckTextEditor.Value);
            standardLetterHistory.Code = txtCode.Text;
            standardLetterHistory.Title = txtTitle.Text;
            standardLetterHistory.StatusId = int.Parse(ddlStatus.SelectedValue);
            standardLetterHistory.ActionId = int.Parse(ddlAction.SelectedValue);
            if (isPdf)
            {
                standardLetterHistory.IsPrinted = true;
            }
            else
            {
                standardLetterHistory.IsPrinted = false;
            }
            if (ddlSignOff.SelectedValue != "-1")
            {
                standardLetterHistory.SignOffLookUpCode = int.Parse(ddlSignOff.SelectedValue);
            }
            if (ddlTeam.SelectedValue != "-1")
            {
                standardLetterHistory.TeamId = int.Parse(ddlTeam.SelectedValue);
            }
            if (ddlFrom.SelectedValue != "-1")
            {
                standardLetterHistory.FromResourceId = int.Parse(ddlFrom.SelectedValue);
            }
            standardLetterHistory.IsActive = true;

            return standardLetterHistory;
        }

        #endregion

        #region"Back to Case Redirection"

        public void BackToCaseRedirection()
        {
            string filname = GetDestinationPage();
            base.RemoveCreateLetterCache();
            if (Session[SessionConstants.isSavedPDFCreateLetter] == null)
            {
                if (filname == "UpdateCase.aspx")
                {
                    RemovePageSessions();
                    Response.Redirect(PathConstants.updatecase + "?cmd=cedit", false);
                }
                else if (filname == "AddCaseActivity.aspx")
                {
                    RemovePageSessions();
                    Response.Redirect(PathConstants.addCaseActivityPath + "?cmd=activityedit&slid=" + base.GetStandardLetterId().ToString(), false);
                }
                else
                {
                    RemovePageSessions();
                    Response.Redirect(PathConstants.openNewCase + "?cmd=cedit", false);
                }
            }
        }

        #endregion

        #region"Get Destination Page"

        public string GetDestinationPage()
        {
            Uri url = (Uri)Session[ViewStateConstants.PageUrl];
            FileOperations fo = new FileOperations();
            string filname = fo.GetUrlFileName(url.AbsolutePath);
            return filname;
        }

        #endregion

        #region"Back to Case With History"

        public void BacktoCaseWithHistory(int historyId)
        {
            string filname = GetDestinationPage();
            OldLetterHistoryID = Convert.ToInt32(Session[SessionConstants.StandardLetterHistoryIdCreateLetter]);
            if (filname == "UpdateCase.aspx")
            {
                RemovePageSessions();
                Response.Redirect(PathConstants.updatecase + "?cmd=cedit&slid=" + base.GetStandardLetterId().ToString() + ";" + historyId.ToString() + ";" + OldLetterHistoryID.ToString() + "&p=true", false);
            }
            else if (filname == "AddCaseActivity.aspx")
            {
                RemovePageSessions();
                Response.Redirect(PathConstants.addCaseActivityPath + "?cmd=activityedit&slid=" + base.GetStandardLetterId().ToString() + ";" + historyId.ToString() + "&p=true", false);
            }
            else
            {
                RemovePageSessions();
                Response.Redirect(PathConstants.openNewCase + "?cmd=cedit&slid=" + base.GetStandardLetterId().ToString() + ";" + historyId.ToString() + ";" + OldLetterHistoryID.ToString() + "&p=true", false);
            }
        }

        #endregion

        #endregion

        #region"Validation"

        #region"Check Cache"

        public bool CheckCache()
        {
            if (Session[CacheConstants.StatusCreateLetter] != null || Session[CacheConstants.ActionCreateLetter] != null ||
                Session[CacheConstants.TitleCreateLetter] != null || Session[CacheConstants.CodeCreateLetter] != null ||
                Session[CacheConstants.PersonalizationCreateLetter] != null || Session[ApplicationConstants.Letter] != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Validate Fields"

        public bool ValidateFields()
        {
            if (ddlStatus.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectStatusCreateLetter, true);
                return false;
            }
            else if (ddlAction.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectActionCreateLetter, true);
                return false;
            }
            else if (txtTitle.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.enterTitleCreateLetter, true);
                return false;
            }
            else if (txtCode.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.enterCodeCreateLetter, true);
                return false;
            }
            else if (manualAddressChk.Checked && (txtName.Text.Equals(string.Empty) || txtAdd1.Text.Equals(string.Empty)))
            {
                SetMessage(UserMessageConstants.enterThirdPartyContact, true);
                return false;
            }
            //else if (ddlSignOff.SelectedValue == "-1")
            //{
            //    SetMessage(UserMessageConstants.selectSignOffCreateLetter, true);
            //    return false;
            //}
            //else if (ddlTeam.SelectedValue == "-1")
            //{
            //    SetMessage(UserMessageConstants.selectTeamCreateLetter, true);
            //    return false;
            //}
            //else if (ddlFrom.SelectedValue == "-1")
            //{
            //    SetMessage(UserMessageConstants.selectFromCreateLetter, true);
            //    return false;
            //}
            else
            {
                return true;
            }
        }

        #endregion

        #region"Validate Signature Fields"

        public bool ValidateSignatureFields()
        {
            if (ddlSignOff.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectSignOffCreateLetter, true);
                return false;
            }
            else if (ddlTeam.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectTeamCreateLetter, true);
                return false;
            }
            else if (ddlFrom.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectFromCreateLetter, true);
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #endregion

        public void clearManualAddressSession()
        {
            Session.Remove(ApplicationConstants.ManualContactName);
            Session.Remove(ApplicationConstants.ManualAddress1);
            Session.Remove(ApplicationConstants.ManualAddress2);
            Session.Remove(ApplicationConstants.ManualAddress3);
            Session.Remove(ApplicationConstants.ManualCityTown);
            Session.Remove(ApplicationConstants.ManualPostCode);
            base.SetIsManualAddress(0);
        }

        public void saveToCAddressSession()
        {
            base.SetIsInvidualLetterReq(Convert.ToInt32(individualLetterCHK.Checked));
            if (manualAddressChk.Checked)
            {
                List<string> data = new List<string>();
                base.SetIsManualAddress(1);
                data.Add(txtName.Text);
                data.Add(txtAdd1.Text);
                data.Add(txtAdd2.Text);
                data.Add(txtAdd3.Text);
                data.Add(txtCityTown.Text);
                data.Add(txtPostCode.Text);
                base.SetManualAddreesData(data);
                Session.Remove(ApplicationConstants.CustomerContactAdd);
            }
            else if (!manualAddressChk.Checked && ddlContact.SelectedValue != "-1")
            {
                base.SetCustomerContactAddressID(Convert.ToInt32(ddlContact.SelectedValue));
                clearManualAddressSession();
            }
            else
            {
                clearManualAddressSession();
                Session.Remove(ApplicationConstants.CustomerContactAdd);
            }

        }

        public string checkCustomer()
        {

            AM_SP_GetCustomerInfoForLetter_Result customer = letterManager.GetCustomerInfo(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
            if (customer.JointTenancyCount == 2)
            {
                return customer.CustomerName + " & " + customer.CustomerName2;
            }
            else
            {
                return customer.CustomerName;
            }
        }

        public string getJobTitle()
        {
            return Convert.ToString(resourceManager.getJobTitle(Convert.ToInt32(ddlFrom.SelectedItem.Value)));
        }

        public string getWorkEmail()
        {
            return Convert.ToString(resourceManager.getWorkEmail(Convert.ToInt32(ddlFrom.SelectedItem.Value)));
        }

        public string getWorkDirectDial()
        {
            return Convert.ToString(resourceManager.getWorkDirectDial(Convert.ToInt32(ddlFrom.SelectedItem.Value)));
        }

    }
}