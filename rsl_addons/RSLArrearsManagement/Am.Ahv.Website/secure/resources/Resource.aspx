﻿<%@ Page Title="Resources :: Rsl Manager" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master"
    AutoEventWireup="true" CodeBehind="Resource.aspx.cs" Inherits="Am.Ahv.Website.masterpages.Resource" %>

<%@ Register Src="../../controls/resources/Users.ascx" TagName="Users" TagPrefix="uc1" %>
<%@ Register Src="~/controls/resources/Letters.ascx" TagName="Letters" TagPrefix="uc2" %>
<%@ Register Src="~/controls/resources/ViewStatusAction.ascx" TagName="StatusandActions"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upresource" runat="server">
        <ContentTemplate>
            <table border="0" cellspacing="0">
                <tr>
                    <td rowspan="2" valign="top" align="center">
                        <uc1:Users ID="Users1" runat="server" />
                    </td>
                    <td valign="top" width="12px">
                       
                    </td>
                    <td valign="top" align="right" >
                        <uc3:StatusandActions ID="StatusandActions1" runat="server" />
                        <br />
                        <uc2:Letters ID="Letters1" runat="server" />
                    </td>
                </tr>
            </table>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <span class="Error">Please wait...</span>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
