﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Am.Ahv.Website.masterpages
{
    public partial class Activity : PageBase 
    {
        #region"Events"
        
        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!base.CheckSession())
                {
                    Response.Redirect(PathConstants.BridgePath);
                    //RedirectToLoginPage();
                }
                string value = string.Empty;
                if (!IsPostBack)
                {
                    if (Request.QueryString["cmd"] == "Edit")
                    {
                        if (!Convert.ToString(Session[SessionConstants.EditActivityId]).Equals(string.Empty))
                        {
                            Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
                            value = lookup.getLookup(Convert.ToInt32(Session[SessionConstants.EditActivityId]));
                            AddActivity1.Visible = true;
                            AddActivity1.setActivityTextField(value);
                        }
                    }
                    if (Request.QueryString["cmd"] == "New")
                    {
                        AddActivity1.Visible = true;
                        Session.Remove(SessionConstants.EditActivityId);
                        AddActivity1.setActivityTextField(string.Empty);
                    }
                }
                Activity1.invokeEvent += delegate(bool eventInvoke, bool edit)
                {
                    if (eventInvoke == true)
                    {
                        if (edit)
                        {
                            if (!Convert.ToString(Session[SessionConstants.EditActivityId]).Equals(string.Empty))
                            {
                                Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
                                value = lookup.getLookup(Convert.ToInt32(Session[SessionConstants.EditActivityId])); ;
                                AddActivity1.Visible = true;
                                AddActivity1.setActivityTextField(value);
                            }
                            else
                            {
                                AddActivity1.Visible = true;
                            }
                        }
                        else
                        {
                            AddActivity1.Visible = true;
                            Session.Remove(SessionConstants.EditActivityId);
                            AddActivity1.ResetField();
                        }
                    }
                };
                AddActivity1.reload += delegate(bool status)
                {
                    if (status == true)
                    {
                        Activity1.ReloadControl();
                        AddActivity1.setActivityTextField(string.Empty);
                        // AddActivity1.Visible = false;
                    }
                };
            }
            catch (NullReferenceException nullException)
            {
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
        }
        
        #endregion

        #endregion
    }
}
