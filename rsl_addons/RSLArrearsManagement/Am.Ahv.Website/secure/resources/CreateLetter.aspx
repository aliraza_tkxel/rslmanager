﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="CreateLetter.aspx.cs"
    Inherits="Am.Ahv.Website.secure.resources.CreateLetter" %>

<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function GetIndex() {

            var elem = document.getElementById('ctl00_ContentPlaceHolder1_hflIndex');
            elem.value = richeditor.GetWindow().getSelection().getRangeAt(0).startOffset;
            var textValue = document.getElementById('ctl00_ContentPlaceHolder1_hflText');
            textValue.value = richeditor.toHtmlString();
            alert(elem.value);
        }

//        function ShowAddressSection() {
//            var div = document.getElementById('addressTbl');
//            if (div.style.display !== 'none') {
//                div.style.display = 'none';
//            }
//            else {
//                div.style.display = 'table';
//            }
//        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hflIndex" runat="server" />
    <asp:HiddenField ID="hflText" runat="server" />
    <asp:UpdatePanel ID="updpnlMain" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td class="tdClass">
                        <table width="100%">
                            <tr>
                                <td style="padding-left: 10px">
                                    <asp:Label ID="lblLetters" runat="server" Text="Letters"></asp:Label>
                                </td>
                                <td align="right" style="padding-right: 10px">
                                    <asp:LinkButton ID="lbtnBack" runat="server" Font-Bold="true" Text="< Back to Templates"
                                        ForeColor="Black" OnClick="lbtnBack_Click"></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="tdClass" style="padding-left: 10px">
                        <asp:UpdatePanel ID="updpnlStatus" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td colspan="4">
                                            <asp:Label ID="lblAddaNewTemplate" runat="server" Text="Add a New Template" Font-Bold="true"></asp:Label>
                                            <asp:Label ID="lblEditTemplate" Visible="false" runat="server" Text="Edit Template"
                                                Font-Bold="true"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="15%">
                                            <asp:Label ID="lblStatus" runat="server" Text="Stage:"></asp:Label>
                                        </td>
                                        <td width="30%">
                                            <asp:Label ID="lblRequiredStatus" runat="server" Text="*" ForeColor="Red"></asp:Label>&nbsp;
                                            <asp:DropDownList ID="ddlStatus" Width="150px" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td width="15%">
                                            <asp:Label ID="lblTitle" runat="server" Text="Title:"></asp:Label>
                                        </td>
                                        <td width="40%">
                                            <asp:Label ID="lblRequiredTitle" runat="server" Text="*" ForeColor="Red"></asp:Label>&nbsp;
                                            <asp:TextBox ID="txtTitle" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblAction" runat="server" Text="Action:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRequiredAction" runat="server" Text="*" ForeColor="Red"></asp:Label>&nbsp;
                                            <asp:DropDownList ID="ddlAction" Width="150px" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCode" runat="server" Text="Code:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRequiredCode" runat="server" Text="*" ForeColor="Red"></asp:Label>&nbsp;
                                            <asp:TextBox ID="txtCode" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr id="tenantAddressRow" runat="server" style="display:none;">
                                        <td>
                                            <asp:Label ID="lblContact" runat="server" Text="Contact:"></asp:Label>
                                        </td>
                                        <td style="padding-left: 8px;">
                                            <asp:Label ID="lblRequiredContact" runat="server" Text="" ForeColor="Red"></asp:Label>&nbsp;
                                            <asp:DropDownList ID="ddlContact" Width="150px" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlContact_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblmanualAddressChk" runat="server" Text="Manual Address:"></asp:Label>
                                        </td>
                                        <td style="padding-left: 15px;">
                                            <asp:CheckBox ID="manualAddressChk"  Text="" OnCheckedChanged="manualAddress_CheckChanged" AutoPostBack="true" runat="server" ></asp:CheckBox>
                                        </td>
                                    <%--    <td>
                                            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>&nbsp;
                                            <asp:TextBox ID="TextBox1" runat="server" Width="200px"></asp:TextBox>
                                        </td>--%>
                                    </tr>
                                </table>
                                <br />
                                <table width="100%" id="addressTbl" style="display:none;" runat="server">
                                    <tr>
                                        <td width="15%">
                                            <asp:Label ID="lblName" runat="server" Text="Contact Name:"></asp:Label>
                                        </td>
                                        <td width="30%">
                                            <asp:Label ID="lblRequiredName" runat="server" Text="*" ForeColor="Red"></asp:Label>&nbsp;
                                            <asp:TextBox ID="txtName" runat="server" Width="144px"></asp:TextBox>
                                        </td>
                                        <td width="15%">
                                            <asp:Label ID="lblAdd1" runat="server" Text="Address 1:"></asp:Label>
                                        </td>
                                        <td width="40%">
                                            <asp:Label ID="lblRequiredAdd1" runat="server" Text="*" ForeColor="Red"></asp:Label>&nbsp;
                                            <asp:TextBox ID="txtAdd1" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblAdd2" runat="server"  Text="Address 2:"></asp:Label>
                                        </td>
                                        <td width="15%" style="padding-left: 16px;">
                                            <asp:Label ID="Label9" runat="server" Text=""  ForeColor="Red"></asp:Label>
                                            <asp:TextBox ID="txtAdd2" runat="server" Width="144px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAdd3" runat="server" Text="Address 3:"></asp:Label>
                                        </td>
                                        <td style="padding-left: 16px;">
                                            <asp:Label ID="Label11" runat="server" Text=""  ForeColor="Red"></asp:Label>
                                            <asp:TextBox ID="txtAdd3" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <asp:Label ID="lblCityTown" runat="server" Text="City/Town:"></asp:Label>
                                        </td>
                                        <td style="padding-left: 16px;"> 
                                            <asp:Label ID="Label13" runat="server" Text=""  ForeColor="Red"></asp:Label>
                                            <asp:TextBox ID="txtCityTown" runat="server" Width="144px"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPostCode" runat="server" Text="PostCode:"></asp:Label>
                                        </td>
                                        <td style="padding-left: 16px;">
                                            <asp:Label ID="Label14" runat="server" Text=""  ForeColor="Red"></asp:Label>
                                            <asp:TextBox ID="txtPostCode" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td class="tdClass" style="padding-left: 10px">
                        <%--<cc1:RichTextEditor ID="rteLetter" BorderWidth="1" EnableViewState="true" BorderColor="Black" runat="server"
                    Width="95%"></cc1:RichTextEditor>--%>
                        <FCKeditorV2:FCKeditor ID="fckTextEditor" EnableViewState="true" Visible="true" ToolbarSet="PersonalizedToolbar"
                            runat="server" Height="400">
                        </FCKeditorV2:FCKeditor>
                        <asp:Label ID="lblTemp" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdClass" style="padding-left: 10px">
                        <table width="100%">
                            <tr>
                                <td width="35%">
                                    <asp:Panel ID="pnlPersonalization" runat="server" GroupingText=" " Width="100%">
                                        <asp:Label ID="lblPersonalizationText" runat="server" Font-Bold="true" Text="Enter the following codes for the following:"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblWeeklyRent" runat="server" Text="Rent Charge = [RC]" Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblMarketRent" runat="server" Text="Market Rent = [MR]" Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblCurrentRentBalance" runat="server" Text="Current Rent Balance = [RB]"
                                            Font-Bold="true"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblTodaysDate" runat="server" Text="Todays Date = [TD]" Font-Bold="true"></asp:Label>
                                        <br />
                                        <%--<asp:Label ID="lblPersonalization" runat="server" Text="Personalization:"></asp:Label>&nbsp;&nbsp;
                                        <asp:UpdatePanel ID="updpnlPersonalization" UpdateMode="Conditional" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlPersonalization" AutoPostBack="true" runat="server" Width="150px"
                                                    OnSelectedIndexChanged="ddlPersonalization_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>--%>
                                    </asp:Panel>
                                </td>
                                <td width="40%">
                                    <asp:UpdatePanel ID="updpnlSignature" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlSignature" runat="server" GroupingText=" " Width="100%">
                                                <table width="100%">
                                                    <tr>
                                                        <td width="20%">
                                                            <asp:Label ID="lblSignOff" runat="server" Text="Sign Off:"></asp:Label>
                                                        </td>
                                                        <td width="80%" id="mylist">
                                                            <asp:Label ID="lblRequiredSignOff" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>&nbsp;
                                                            <asp:DropDownList ID="ddlSignOff" runat="server" Width="130px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">
                                                            <asp:Label ID="lblTeam" runat="server" Text="Team:"></asp:Label>
                                                        </td>
                                                        <td width="60%">
                                                            <asp:Label ID="lblRequiredTeam" ForeColor="Red" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                                                            <asp:DropDownList ID="ddlTeam" runat="server" Width="130px" AutoPostBack="true" OnSelectedIndexChanged="ddlTeam_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">
                                                            <asp:Label ID="lblFrom" runat="server" Text="From:"></asp:Label>
                                                        </td>
                                                        <td width="60%">
                                                            <asp:Label ID="lblRequiredFrom" ForeColor="Red" runat="server" Text="*" Visible="false"></asp:Label>&nbsp;
                                                            <asp:DropDownList ID="ddlFrom" AppendDataBoundItems="true" runat="server" Width="130px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>

                                <td width="25%" valign="top" id="jointTenantPanel" runat="server" style="display:none;">
                                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel1" runat="server" GroupingText=" " Width="100%">
                                                <table width="100%">
                                                    <tr>
                                                        <td >
                                                            <asp:CheckBox ID="individualLetterCHK"  Text="Joint Tenant Individual Letter" OnCheckedChanged="individualLetterCHK_CheckChanged" AutoPostBack="true" runat="server" ></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                   
                                                </table>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right" style="padding-right: 5px;">
                                    <asp:UpdatePanel ID="updpnlButton" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <asp:Button ID="btnBacktoCase" Visible="false" runat="server" Text="Back to Case"
                                                OnClick="btnBacktoCase_Click" />&nbsp;
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />&nbsp;
                                            <asp:Button ID="btnPreview" runat="server" Text="Preview & Print" OnClick="btnPreview_Click" />&nbsp;
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />&nbsp;
                                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                                            <asp:Button ID="btnSaveAsPDF" Visible="false" runat="server" Text="Save Letter" OnClick="btnSaveAsPDF_Click" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSave" />
                                            <asp:PostBackTrigger ControlID="btnSaveAsPDF" />
                                            <asp:PostBackTrigger ControlID="btnPreview" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                <ProgressTemplate>
                    <span class="Error">Please wait...</span>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
