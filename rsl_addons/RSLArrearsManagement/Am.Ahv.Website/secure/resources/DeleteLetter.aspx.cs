﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExtAspNet;
using Am.Ahv.BusinessManager.letters;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;

namespace Am.Ahv.Website.secure.resources
{
    public partial class DeleteLetter : PageBase
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            //btnClose.OnClientClick = ActiveWindow.GetConfirmHidePostBackReference();
         }

        private int GetQueryStringValue()
        {
            try
            {
                int StandardLetterId = int.Parse(Request.QueryString["Value"].ToString());
                return StandardLetterId;

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return -1;
            }
        }

        protected void btnyes_Click1(object sender, EventArgs e)
        {
            int StandardLetterId = GetQueryStringValue();
            if (StandardLetterId != -1)
            {
                LetterResourceArea Letters = new LetterResourceArea();
                bool flag = Letters.DeleteLetter(StandardLetterId);
                if (flag)
                {
                    PageContext.RegisterStartupScript(ActiveWindow.GetHideRefreshReference());
                }
            }
        }



    }
}