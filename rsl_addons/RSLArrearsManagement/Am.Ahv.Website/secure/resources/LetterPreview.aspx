﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMBlankMasterPage.Master"
    AutoEventWireup="true" CodeBehind="LetterPreview.aspx.cs" Inherits="Am.Ahv.Website.secure.resources.LetterPreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function Clickheretoprint() {
            var disp_setting = "toolbar=yes,location=no,directories=yes,menubar=yes,";
            disp_setting += "scrollbars=yes,width=650, height=650, left=0, top=25";
            var content_vlue;

            var indvidualLetter = document.getElementById('<%=HidindvidualLetterReq.ClientID %>');
            var lblTenantName = document.getElementById('<%=lblTenantName.ClientID %>');
            var lblDearTenant = document.getElementById('<%=lblDearTenant.ClientID %>');

            if (indvidualLetter.value == "1") {
                setMargin();
                var lblCutomer1 = document.getElementById('<%=HidCutomer1.ClientID %>');
                var lblCutomer2 = document.getElementById('<%=HidCutomer2.ClientID %>');
                var backuplblString = lblTenantName.textContent;
                lblTenantName.textContent = lblCutomer1.value;
                lblDearTenant.textContent = lblCutomer1.value;
                content_vlue = document.getElementById("print_content").innerHTML;
                lblTenantName.textContent = lblCutomer2.value;
                lblDearTenant.textContent = lblCutomer2.value;
                content_vlue = content_vlue + " <p style=\"page-break-after:always;\"></p> " + document.getElementById("print_content").innerHTML;
                lblTenantName.textContent = backuplblString.textContent;
                lblDearTenant.textContent = backuplblString.textContent;
            }
            else {
                setMargin();

                content_vlue = document.getElementById("print_content").innerHTML;
            }
            var docprint = window.open("", "", disp_setting);
            docprint.document.open();
            //  docprint.document.write('<html><head><link href="../../style/css/default.css" rel="stylesheet" type="text/css" /><title></title>');
            //docprint.document.write('<html><head><title>Standard Letter Print Preview</title>');
            docprint.document.write('</head><body onLoad="self.print()" style="font-family:Arial;font-size:12pt;">');

            docprint.document.write(content_vlue);
            docprint.document.write('</body></html>');
            docprint.document.close();
            docprint.focus();
        }
        function closeWindow() {
            alert("hi");

            window.Close();

        }

        function setMargin() {
            var table = document.getElementById('belowTable');
            table.style.marginLeft = "0cm";
        }

    </script>
    <style media="print" type="text/css">
        .hide_print
        {
            display: none;
        }
        .font_print
        {
            font-family: Arial;
            font-size: 12pt;
        }
        #lblLetterBody
        {
            margin-left: 2.5px;
            margin-right: 2.5px;
            font: arial;
        }
    </style>
    <style type="text/css">
        .style2
        {
            width: 2.40cm;
        }
        .style3
        {
        }
        .dearStyle
        {
            margin-left: 2.40cm;
        }
        .styleMargin
        {
            margin-left: 2.54cm;
            margin-right: 2.54cm;
            font-family: Arial;
            font-size: 12pt;
        }
        .styleMarginWithoutRightMargin
        {
            margin-left: 2.54cm;
            font-family: Arial;
            font-size: 12pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table style="width: 98%;">
            <tr>
                <td align="justify" class="style3" colspan="2">
                    <asp:Button ID="Button1" runat="server" OnClick="btnBack_Click" Text="Back" />
                    <asp:Button ID="btnClose" Visible="false" runat="server" OnClientClick="window.close();return false;"
                        Text="Close" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button2" runat="server" OnClientClick="Clickheretoprint()" Text="Print" />
                </td>
            </tr>
        </table>
    </div>
    <div style="border-width: thin; border-style: solid; font-family: Arial; font-size: 12pt;">
        &nbsp;&nbsp;&nbsp;&nbsp; Letters<br />
        <div style="border-width: thin; border-top-style: solid">
            <div>
                <asp:UpdatePanel ID="panelAddactivity" runat="server">
                    <contenttemplate>
                        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            &nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </asp:Panel>
                    </contenttemplate>
                </asp:UpdatePanel>
            </div>
            <div id="print_content" class="font_print">
                <asp:UpdatePanel ID="updatePnlTenentInfo" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
                        <table style="width: 98%; margin: 0px  57px 0px 0; text-align: left;">
                         
                            <tr>
                                <td align="justify">
                                    <asp:Label ID="lblTenancyId" runat="server" CssClass="styleMargin"></asp:Label>
                                </td>
                    
                            </tr>
                            <tr>
                                <td style='font: 12pt arial; white-space: nowrap;'>
                                    <asp:Label ID="lblDate" CssClass="styleMargin"  runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="justify">
                                <div class="styleMargin">
                                    <asp:Label ID="lblTenantName" runat="server"></asp:Label></div>
                                </td>
                            </tr>
                         
                            <tr>
                              <td align="justify">
                                    <asp:Label ID="lblHouseNumber" CssClass="styleMarginWithoutRightMargin" runat="server"></asp:Label>
                                    <asp:Label ID="lblAddress1"  runat="server"></asp:Label>
                                    </td> 
                            </tr>
                            <tr  runat="server" id="rowAddress2">
                              
                                <td>
                                    <asp:Label ID="lblAddress2" CssClass="styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr runat="server" id="rowAddress3">
                                
                                <td >
                                    <asp:Label ID="lblAddress3"  CssClass="styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr runat="server" id="rowTownCity">
                                
                                <td>
                                    <asp:Label ID="lblTownCity" CssClass="styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr runat="server" id="rowCountry">
                                
                                <td>
                                    <asp:Label ID="lblCountry"  CssClass="styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr runat="server" id="rowPostCode">
                                
                                <td>
                                    <asp:Label ID="lblPostCode" CssClass="styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                      
                        </table>
                    </contenttemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="updatePnlHeading" runat="server" UpdateMode="Conditional">
                    <contenttemplate>
                        <table id="belowTable" style="width: 98.5%; margin: 0px 57px 2.52cm 0; text-align:left;" >
                        <tr>
                                <td class="style2">
                                    &nbsp;
                                    <asp:HiddenField ID="HidCutomer1"  runat="server" Value="" />
                                    <asp:HiddenField ID="HidCutomer2"  runat="server" Value="" />
                                    <asp:HiddenField ID="HidindvidualLetterReq" runat="server" Value="0" />
                                </td>
                        </tr>
                            <tr>
                                
                                <td align="justify" >
                                    <asp:Label ID="lblDear" runat="server"  CssClass="styleMarginWithoutRightMargin"></asp:Label>
                                    <asp:Label ID="lblDearTenant" runat="server"   ></asp:Label>                                    
                                </td>
                            </tr>
                            <tr>
                                
                                <td>
                                <div class="styleMargin">
                                    <asp:Label ID="lblLetterBody" CssClass="font_print" runat="server"></asp:Label></div>
                                </td>
                                 
                            </tr>
                            <tr>
                                
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                
                                <td>
                                    <asp:Label ID="lblSignOff" CssClass="font_print styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                
                                <td>
                                    &nbsp;
                                </td>
                            </tr>                            
                            <tr>
                                
                                <td>
                                    <asp:Label ID="lblFrom" CssClass="font_print styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                
                                <td>
                                    <asp:Label ID="lblTeamName" runat="server" CssClass="font_print styleMargin"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                 
                                <td>
                                    <asp:Label ID="lblDirectDial" CssClass="font_print styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                
                                <td>
                                    <asp:Label ID="lblEmail" CssClass="font_print styleMargin" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </contenttemplate>
                </asp:UpdatePanel>
                <br />
            </div>
        </div>
        <div class="hide_print">
            <table style="width: 100%;">
                <tr>
                    <td align="right">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
