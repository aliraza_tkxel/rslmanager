﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeleteLetter.aspx.cs" Inherits="Am.Ahv.Website.secure.resources.DeleteLetter" %>

<%@ Register Assembly="ExtAspNet" Namespace="ExtAspNet" TagPrefix="ext" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head1" runat="server">
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
    <ext:PageManager ID="PageManager1" AutoSizePanelID="Panel1" HideScrollbar="true"
        runat="server" />
    <ext:Panel ID="Panel1" runat="server" Layout="Container" ShowBorder="False" ShowHeader="false"
        BodyPadding="5px" EnableBackgroundColor="true" Height="130">
        <Items>
            <ext:Label ID="lblwarningtxtct" runat="server" Text="Are you sure you want to delete the Letter?">
            </ext:Label>
            <ext:Button ID="btnyes" Text="Yes" runat="server" OnClick="btnyes_Click1" >
            </ext:Button>
         
        </Items>
    </ext:Panel>
    </form>
</body>
</html>
