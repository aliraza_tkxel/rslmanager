<%@ Page Title= '' Language="C#" AutoEventWireup="true" CodeBehind="Status.aspx.cs" Inherits="Am.Ahv.Website.secure.status_and_actions.Status"
    MasterPageFile="~/masterpages/AMMasterPage.Master" %>

<%@ MasterType VirtualPath="~/masterpages/AMMasterPage.Master" %>
<%@ Register Src="~/controls/resources/FirstDetectionList.ascx" TagName="FirstDetectionList"
    TagPrefix="uc1" %>
<%@ Register Src="~/controls/resources/AddStatus.ascx" TagName="AddStatus"
    TagPrefix="uc3" %>
<%@ Register Src="~/controls/resources/Action.ascx" TagName="Action" TagPrefix="uc4" %>
<%@ Register Src="../../controls/resources/ActivitiesList.ascx" TagName="ActivitiesList"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<script runat="server">
   
</script>
    <style type="text/css">
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="StatusIntegratedPanel" runat="server">
        <asp:UpdatePanel ID="IntegratedupdatePanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table class="tableClass" width="100%">
                    <tr>
                        <td class="tableHeader" colspan="2">
                            <asp:Label ID="lblTitle" runat="server" Text="Stage and Actions"></asp:Label>
                        </td>
                        <tr>
                            <td align="left" valign="top">
                                <asp:Panel ID="Panel1" runat="server" Height="200px" ScrollBars="Vertical" 
                                    Width="250px">
                                    <asp:TreeView ID="tree" runat="server" ForeColor="Black" 
                                        OnSelectedNodeChanged="tree_SelectedNodeChanged" 
                                        ontreenodecollapsed="tree_TreeNodeCollapsed" 
                                        OnTreeNodePopulate="tree_TreeNodePopulate" ShowLines="true" 
                                        ondisposed="tree_Disposed" onload="tree_Load">
                                        <Nodes>
                                            <asp:TreeNode PopulateOnDemand="true" Text="Stage" Value="100"></asp:TreeNode>
                                        </Nodes>
                                    </asp:TreeView>
                                </asp:Panel>
                            </td>
                            <td rowspan="3" style="border-left:1px solid #000000;" valign="top">
                                <asp:Panel ID="FirstDetectionPanel" runat="server">
                                    <uc1:FirstDetectionList ID="FirstDetectionList1" runat="server" 
                                        Visible="false" />
                                    <uc3:AddStatus ID="AddStatus1" runat="server" Visible="true" />
                                    <uc4:Action ID="Action1" runat="server" Visible="false" />
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #000000; border-top:1px solid #000000;margin:0px;padding:0px;" valign="top">
                                <asp:Label ID="lblActivities" runat="server" Text="Activities"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <uc2:ActivitiesList ID="ActivitiesList1" runat="server" />
                            </td>
                        </tr>
                        </tr>
                   
                </table>
                 <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <span class="Error">Please wait...</span>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
