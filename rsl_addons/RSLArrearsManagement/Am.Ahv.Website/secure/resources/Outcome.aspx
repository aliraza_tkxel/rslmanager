﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="Outcome.aspx.cs" Inherits="Am.Ahv.Website.masterpages.Outcome" %>

<%@ Register src="../../controls/resources/AddOutcome.ascx" tagname="AddOutcome" tagprefix="uc2" %>
<%@ Register src="../../controls/resources/Outcome.ascx" tagname="Outcome" tagprefix="uc1" %>
<%@ Register src="../../controls/resources/ActivitiesList.ascx" tagname="ActivitiesList" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upoutcome" runat="server"><ContentTemplate>
    <table>
        <tr>
            <td valign="top">
            <%--  <asp:UpdatePanel ID="upOutcomes" UpdateMode="Conditional" runat="server">
                <ContentTemplate>--%>
                    <uc1:Outcome ID="Outcome1" runat="server" />
                <%--</ContentTemplate>
               </asp:UpdatePanel>  --%>
            </td>
            <td valign="top">
                <%--<asp:UpdatePanel ID="upAddOutcomes" UpdateMode="Conditional" Visible="false" runat="server">
                    <ContentTemplate>--%>
                        <uc2:AddOutcome ID="AddOutcome1" Visible="true" runat="server" />
                  <%--  </ContentTemplate>
               </asp:UpdatePanel>--%>
            </td>
        </tr>
    </table>
     <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <span class="Error">Please wait...</span>
            <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    </ContentTemplate></asp:UpdatePanel>
   <%-- <br />
    <uc3:ActivitiesList ID="ActivitiesList1" runat="server" />
    <br />--%>
    
    
</asp:Content>
