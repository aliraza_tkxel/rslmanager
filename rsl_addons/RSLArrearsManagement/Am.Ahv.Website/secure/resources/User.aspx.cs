﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Utilities.utitility_classes;

namespace Am.Ahv.Website.secure.resources
{
    public partial class User : PageBase 
    {

       

        protected void Page_Load(object sender, EventArgs e)
        {
            string query = "id";

            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                lblflagvalue.Text = "true";

            }
            if (Convert.ToBoolean(lblflagvalue.Text))
            {
                if (Request.QueryString[query] != null && !Convert.ToString(Request.QueryString[query]).Equals(string.Empty) && Validation.CheckIntegerValue(Request.QueryString[query].ToString()))
                {
                    int ResourceId = int.Parse(Request.QueryString[query].ToString());
                    AddCaseWorker1.initlookups();
                    AddCaseWorker1.PopulateResrouce(ResourceId);
                    lblflagvalue.Text = "false";
                    //ViewState["CWflag"] = false;

                }
            }
            AddCaseWorker1.updateEvent += delegate(bool flag)
            {
                if (flag == true)
                {
                    Users1.Reload();
                    updpnlUsers.Update();
                }
            };
        }
    }
}
