﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.letters;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.utitility_classes;
using System.Data;
using System.Configuration;

namespace Am.Ahv.Website.secure.resources
{
    public partial class LettersResourceArea :PageBase
    {
        #region Attributes
        LetterResourceArea LettersManager;
        bool mainflag = false;
        #endregion
        #region Events
        #region Pageload
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
            }
            bool flag = false;
            if (!IsPostBack)
            {
                InitLookups();
                if (!CheckPageRequest())
                {
                    ClearCachedObject();
                    pnlseachletters.Visible = true;
                    ViewState["PageValue"] = true;
                    pnlselection.Visible = false;
                    LoadGrid(true);
                    flag = true;
                }
                if (Request.QueryString["cmd"] != null)
                {
                    if (Request.QueryString["cmd"] == "new")
                    {
                        if (!CheckPageRequest())
                        {
                            Session.Remove(SessionConstants.TenantId);
                            ClearCachedObject();
                            pnlseachletters.Visible = true;
                            ViewState["PageValue"] = true;
                            pnlselection.Visible = false;
                            LoadGrid(true);
                            flag = true;
                        }
                        
                    }
                }

                //Check if there is cached Action available inside the cacheobject
                if (!CheckQueryString())
                {
                    if (IsCachedAction())
                    {
                        SearchChacedActionLetters();
                    }
                    //else
                    //{
                        if (Request.QueryString["SID"] != null && Request.QueryString["AID"] != null)
                        {
                            //if (!mainflag)
                            //{
                            if (Validation.CheckIntegerValue(Request.QueryString["SID"].ToString()) && Validation.CheckIntegerValue(Request.QueryString["AID"].ToString()))
                            {
                                base.SetStatusId(int.Parse(Request.QueryString["SID"].ToString()));
                                base.setActionId(int.Parse(Request.QueryString["AID"].ToString()));
                                int ActionId = int.Parse(Request.QueryString["AID"].ToString());
                                int StatusId = int.Parse(Request.QueryString["SID"].ToString());
                                LoadCaseValues(ActionId, StatusId);
                                btnBack.Visible = true;
                            }
                            //}
                        }
                    //}
                }
                else if (CheckPageRequest())
                {
                    if (!mainflag)
                    {
                        pnlseachletters.Visible = true;
                        ViewState["PageValue"] = true;
                        pnlselection.Visible = false;
                        LoadGrid(true);
                    }
                }
                else
                {
                    if (!flag)
                    {
                        pnlseachletters.Visible = true;
                        ViewState["PageValue"] = true;
                        pnlselection.Visible = false;
                        LoadGrid(true);
                    }
                }
            }
        }
        #endregion
        #region Preview Link Button Click
        protected void lbtnpreview_Click(object sender, EventArgs e)
        {
            Response.Redirect("");
        }
        #endregion
        #region Edit Link Button Click
        protected void lbtnedit_Click(object sender, EventArgs e)
        {
            Response.Redirect("");
        }
        #endregion
        #region Select Link Button Click
        protected void lbtnselect_Click(object sender, EventArgs e)
        {
            Response.Redirect("~secure/resources/Status.aspx", true);
        }
        #endregion
        #region Status Selected index Changed
        protected void ddlStatus1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlStatus1.SelectedValue != null)
                {
                    int StatusId = int.Parse(ddlStatus1.SelectedValue);
                    ddlaction1.Items.Clear();
                    LoadActionDdl(StatusId);
                }
            }
            catch (NullReferenceException nullref)
            {
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }

        }
        #endregion
        #region SearchButton
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                bool value = Convert.ToBoolean(ViewState["PageValue"].ToString());
                LoadGrid(value);
                uplettersearch.Update();

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion
        #region RowCommand Event
        protected void grdsearchedletters_RowCommand(object sender, ExtAspNet.GridCommandEventArgs e)
        {
            try
            {
                ExtAspNet.Grid gridview = sender as ExtAspNet.Grid;

                int StandardLetterid = (int)gridview.DataKeys[e.RowIndex].GetValue(0);
                //gridview.DataKeys[int.Parse(e.CommandArgument.ToString())].GetValue(e.RowIndex);
                //(int)gridview.DataKeys[0].GetValue(e.RowIndex);
                switch (e.CommandName)
                {
                    case "Edit":
                        SaveGridParameters();
                        base.RemoveCreateLetterCache();
                        Response.Redirect(PathConstants.LRAMovetoEdit + StandardLetterid, true);
                        return;
                    case "Preview":
                        SaveGridParameters();
                        if (Request.QueryString["cmd"] == "newAction")
                        {
                            Response.Redirect(PathConstants.LRAMovetopreview + "Action&Id=" + StandardLetterid, true);
                        }
                        else
                        {
                            Response.Redirect(PathConstants.LRAMovetopreview + "&Id=" + StandardLetterid, true);
                        }
                        //Response.Redirect(PathConstants.LRAMovetopreview + StandardLetterid, true);
                        return;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }

        }
        #endregion
        #region Push Action Object in Cache
        private void PushAction(AM_Action action)
        {
            Session["CachedAction"] = action;
            DataTable ListItemCollection = Session["LettersCollection"] as DataTable;
            Session["LettersCollection"] = ListItemCollection;
        }
        #endregion
        #region Paging SearchLetterGrid
        protected void grdsearchedletters_PageIndexChange(object sender, ExtAspNet.GridPageEventArgs e)
        {
            grdsearchedletters.PageIndex = e.NewPageIndex;
        }
        #endregion
        #region"Btn Create New Template"
        protected void btnCreateNewTemplate_Click(object sender, EventArgs e)
        {
            SaveGridParameters();
            base.RemoveCreateLetterCache();
            Response.Redirect(PathConstants.LetterCreateTemplate + "?cmd=new", false);
        }
        #endregion
        #region PreRowDataBound
        protected void grdsearchedletters_PreRowDataBound(object sender, ExtAspNet.GridPreRowEventArgs e)
        {

            // Display Preview and Select Buttons
            ExtAspNet.LinkButtonField lbpreview = grdsearchedletters.FindColumn("lbbuttonpreview") as ExtAspNet.LinkButtonField;
            // ExtAspNet.LinkButtonField lbselect = grdsearchedletters.FindColumn("lbbuttonselect") as ExtAspNet.LinkButtonField;
            ExtAspNet.LinkButtonField lbedit = grdsearchedletters.FindColumn("lbbuttonedit") as ExtAspNet.LinkButtonField;
            lbpreview.Enabled = true;
            // lbselect.Enabled = true;
            lbedit.Enabled = true;
        }
        #endregion
        #endregion
        #region Utility Methods
        #region InitLookups
        private void InitLookups()
        {
            try
            {
                LettersManager = new LetterResourceArea();
                //Setting the Status Dropdown List
                ddlStatus1.Items.Clear();
                List<AM_Status> Statuslist = LettersManager.GetAllStatus();
                ddlStatus1.DataSource = Statuslist;
                ddlStatus1.DataValueField = ApplicationConstants.StatusId;
                ddlStatus1.DataTextField = ApplicationConstants.Title;
                ddlStatus1.SelectedValue = Statuslist[0].StatusId.ToString();
                ddlStatus1.DataBind();

                ddlStatus1.Items.Add(new ExtAspNet.ListItem("Please Select Stage", ApplicationConstants.defaulvalue));
                ddlaction1.Items.Add(new ExtAspNet.ListItem("Please Select Action", ApplicationConstants.defaulvalue));
                ddlaction1.SelectedValue = ApplicationConstants.defaulvalue;
                ddlStatus1.SelectedValue = ApplicationConstants.defaulvalue;
                if (IsGridParameters())
                {
                    LoadGridParameters();
                    bool value = Convert.ToBoolean(ViewState["PageValue"].ToString());
                    LoadGrid(value);
                    mainflag = true;
                }

            }
            catch (NullReferenceException nullref)
            {
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion

        #region LoadGrid
        private void LoadGrid(bool value)
        {
            try
            {
                LettersManager = new LetterResourceArea();
                List<AM_StandardLetters> sl = new List<AM_StandardLetters>();

                sl = LettersManager.SearchStandardLetters(int.Parse(ddlStatus1.SelectedValue), int.Parse(ddlaction1.SelectedValue), txttile.Text, txtcode.Text);

                if (value)
                {

                    grdsearchedletters.Items.Clear();
                    //grdsearchedletters.Rows.Clear();
                    //grdsearchedletters.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
                    //grdsearchedletters.EnableViewState = false;
                    //grdselection.Visible = false;
                    pnlseachletters.Visible = true;
                    pnlselection.Visible = false;
                    grdsearchedletters.DataSource = sl;// LettersManager.SearchStandardLetters(int.Parse(ddlStatus1.SelectedValue),int.Parse( ddlaction1.SelectedValue), txttile.Text, txtcode.Text);
                    grdsearchedletters.DataBind();
                    grdsearchedletters.PageIndex = 0;
                }
                else
                {
                    grdselection.Items.Clear();
                    //grdselection.Rows.Clear();
                    //grdselection.EnableViewState = false;
                    //grdselection.ViewStateMode = System.Web.UI.ViewStateMode.Disabled;
                    pnlseachletters.Visible = false;
                    pnlselection.Visible = true;
                    grdselection.DataSource = sl;
                    grdselection.DataBind();
                    grdselection.PageIndex = 0;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion
        #region LoadActionDDl
        private void LoadActionDdl(int StatusId)
        {
            try
            {
                if (ddlStatus1.SelectedValue != ApplicationConstants.defaulvalue)
                {
                    LettersManager = new LetterResourceArea();
                    List<AM_Action> ActionList = LettersManager.GetActionByStatusId(StatusId);
                    ddlaction1.Items.Clear();
                    ddlaction1.DataSource = ActionList;
                    ddlaction1.DataValueField = ApplicationConstants.ActionId;
                    ddlaction1.DataTextField = ApplicationConstants.Title;
                    // ddlaction1.SelectedValue = ActionList[0].ActionId.ToString();
                    ddlaction1.DataBind();
                    ddlaction1.Items.Add(new ExtAspNet.ListItem("Please Select Action", ApplicationConstants.defaulvalue));
                    ddlaction1.SelectedValue = ApplicationConstants.defaulvalue;

                }
                else
                {
                    ddlaction1.DataSource = ApplicationConstants.emptyDataSource;
                    ddlaction1.DataTextField = string.Empty;
                    ddlaction1.DataValueField = string.Empty;
                    ddlaction1.DataBind();
                    ddlaction1.Items.Add(new ExtAspNet.ListItem("Please Select Action", ApplicationConstants.defaulvalue));
                    ddlaction1.SelectedValue = ApplicationConstants.defaulvalue;
                }
                updpnlDropDowns.Update();
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion
        #region Load Case Values
        private void LoadCaseValues(int ActionId, int StatusId)
        {
            ddlStatus1.SelectedValue = StatusId.ToString();
            LoadActionDdl(StatusId);
            ddlaction1.SelectedValue = ActionId.ToString();
            if (!mainflag)
            {
                LoadGrid(false);
            }
        }
        #endregion
        #region Save Grid Parameteres
        private void SaveGridParameters()
        {
            Session[CacheConstants.ActionId] = ddlaction1;
            Session[CacheConstants.StatusId] = ddlStatus1.SelectedValue;
            Session[CacheConstants.Code] = txtcode.Text;
            Session[CacheConstants.Title] = txttile.Text;
            Session[CacheConstants.SearchValue] = ViewState["PageValue"].ToString();
            Session[CacheConstants.PageCommand] = Request.QueryString["cmd"];
        }
        #endregion
        #region ClearCachedObjects
        public void ClearCachedObject()
        {
            try
            {
                if (IsGridParameters())
                {
                    Session.Remove(CacheConstants.ActionId);
                    Session.Remove(CacheConstants.Code);
                    Session.Remove(CacheConstants.SearchValue);
                    Session.Remove(CacheConstants.StatusId);
                    Session.Remove(CacheConstants.Title);
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion
        #region CheckPageRequest
        private bool CheckPageRequest()
        {
            try
            {
                FileOperations fo = new FileOperations();
                string filname = string.Empty;
                if (Request.UrlReferrer != null)
                {
                    filname = fo.GetUrlFileName(Request.UrlReferrer.AbsolutePath);
                }
                if (filname.Equals(PathConstants.createLetterFileName) || filname.Equals(PathConstants.previewLetterFileName))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return false;
            }

        }
        #endregion
        #region Load GridParameters
        public void LoadGridParameters()
        {
            txtcode.Text = Session[CacheConstants.Code].ToString();
            txttile.Text = Session[CacheConstants.Title].ToString();
            ddlStatus1.SelectedValue = Session[CacheConstants.StatusId].ToString();
            ViewState["PageValue"] = Session[CacheConstants.SearchValue].ToString();
            LoadActionDdl(int.Parse(ddlStatus1.SelectedValue));
        }
        #endregion
        #region IsGridParameters
        private bool IsGridParameters()
        {
            if ((Session[CacheConstants.ActionId] != null || Session[CacheConstants.StatusId] != null || Session[CacheConstants.Code] != null || Session[CacheConstants.Title] != null || Session[CacheConstants.SearchValue] != null) && Request.QueryString["cmd"] != "ActivitySelect" && Request.QueryString["cmd"] != "UpdateCaseSelect")
            {
                return true;
            }
            else
                if (Convert.ToString(Session[CacheConstants.ActionId]).Equals(string.Empty) && Convert.ToString(Session[CacheConstants.StatusId]).Equals(string.Empty) && Convert.ToString(Session[CacheConstants.Code]).Equals(string.Empty) && Convert.ToString(Session[CacheConstants.Title]).Equals(string.Empty) && Convert.ToString(Session[CacheConstants.SearchValue]).Equals(string.Empty))
                {
                    return false;
                }
                else
                {
                    return false;
                }
        }
        #endregion
        #region Check Query String
        private bool CheckQueryString()
        {
            FileOperations fo = new FileOperations();
            if (Request.UrlReferrer != null)
            {
                string Filename = fo.GetUrlFileName(Request.UrlReferrer.AbsolutePath);
                if (Filename.Equals(PathConstants.statusFileName) || Request.QueryString["cmd"] == "CaseSelect" || Request.QueryString["cmd"] == "UpdateCaseSelect" ||
                    Request.QueryString["cmd"] == "newAction" || Request.QueryString["cmd"] == "ActivitySelect" || Request.QueryString["cmd"] == "ActivitySelectPreview")
                {
                    ViewState["PageValue"] = false;
                    btnBack.Visible = true;
                    return false;
                }
                else
                {
                    ViewState["PageValue"] = true;
                    return true;
                }
            }
            else
            {
                ViewState["PageValue"] = true;
                return true;

            }
        }
        #endregion
        #region FormatDate
        protected string FormatDate(object value)
        {
            DateTime date = Convert.ToDateTime(value);
            return date.ToString("dd/MM/yyyy");
        }
        #endregion
        #region CheckCacheForAction
        private bool IsCachedAction()
        {
            AM_Action CachedAction = Session["CachedAction"] as AM_Action;
            // Added By Zunair Minhas...
            DataTable ListCollection = Session["LettersCollection"] as DataTable;

            /* Commented By Zunair Minhas on 23 June 2010
                List<AM_StandardLetters> ListCollection = Cache.Get("LettersCollection") as List<AM_StandardLetters>;
             * */
            if (CachedAction != null && ListCollection != null)
            {
                Session[SessionConstants.CachedActionObject] = CachedAction;
                Session["LettersCollection"] = ListCollection;
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region SearchCachedActionLetters
        private void SearchChacedActionLetters()
        {
            try
            {
                //  grdsearchedletters.FindControl("");
                AM_Action Action = Session[SessionConstants.CachedActionObject] as AM_Action;
                if (Action != null)
                {
                    ddlStatus1.SelectedValue = Action.StatusId.ToString();
                    if (Action.ActionId != 0)
                    {
                        LoadActionDdl(Action.StatusId);
                        ddlaction1.SelectedValue = Action.ActionId.ToString();
                    }
                    LoadGrid(false);
                }

            }
            catch (NullReferenceException nullref)
            {
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion
        #endregion
        #region Page Methods
        /// <summary>
        /// For Calling the IFrame 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string GetEditUrl(object value)
        {
            SaveGridParameters();
            return "javascript:" + Window1.GetShowReference("DeleteLetter.aspx?value=" + value, "Delete Letter");
        }

        #endregion
        #region Select Grid Functionality
        #region PreRowDataBound
        protected void grdselection_PreRowDataBound(object sender, ExtAspNet.GridPreRowEventArgs e)
        {
            ExtAspNet.LinkButtonField lbpreview = grdselection.FindColumn("lbbuttonpreview") as ExtAspNet.LinkButtonField;
            ExtAspNet.LinkButtonField lbbuttonselect = grdselection.FindColumn("lbbuttonselect") as ExtAspNet.LinkButtonField;
            ExtAspNet.LinkButtonField lbedit = grdselection.FindColumn("lbbuttonedit") as ExtAspNet.LinkButtonField;
            lbpreview.Enabled = true;
            lbpreview.Enabled = true;
            lbbuttonselect.Enabled = true;
        }
        #endregion
        #region RowCommand Event
        protected void grdselection_RowCommand(object sender, ExtAspNet.GridCommandEventArgs e)
        {
            try
            {
                ExtAspNet.Grid gridview = sender as ExtAspNet.Grid;
                //LetterResourceArea lRArea = new LetterResourceArea();
                int StandardLetterid = (int)gridview.DataKeys[e.RowIndex].GetValue(0);
               // AM_StandardLetterHistory slHistory = lRArea.GetLatestStandardLetterHistory(StandardLetterid);
                //if (slHistory == null)
                //{
                //    return;
                //}
                switch (e.CommandName)
                {
                    case "Edit":
                        if (Session[SessionConstants.TenantId] != null)
                        {
                            int tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                            SaveGridParameters();
                            base.RemoveCreateLetterCache();
                            Response.Redirect(PathConstants.LRAMovetoEdit + StandardLetterid + "&TID=" + tenantid, true);
                        }
                        return;
                    case "Preview":
                        SaveGridParameters();
                        if (Request.QueryString["cmd"] == "newAction")
                        {
                            Response.Redirect(PathConstants.LRAMovetopreview + "Action&Id=" + StandardLetterid, false);
                        }
                        else if (Request.QueryString["cmd"] == "CaseSelect")
                        {
                            Response.Redirect(PathConstants.LRAMovetopreview + "case&Id=" + StandardLetterid, false);
                        }
                        else if (Request.QueryString["cmd"] == "UpdateCaseSelect")
                        {
                            Response.Redirect(PathConstants.LRAMovetopreview + "UpdateCase&Id=" + StandardLetterid, false);
                        }
                        else if (Request.QueryString["cmd"] == "ActivitySelect" || Request.QueryString["cmd"] == "ActivitySelectPreview")
                        {
                            Response.Redirect(PathConstants.LRAMovetopreview + "Activity&Id=" + StandardLetterid, false);
                        }
                        else
                        {
                            Response.Redirect(PathConstants.LRAMovetopreview + "&Id=" + StandardLetterid, false);
                        }
                        break;
                    case "Select":
                        if (Request.QueryString["cmd"] != null)
                        {
                            if (Request.QueryString["cmd"] == "CaseSelect")
                            {
                                SaveGridParameters();
                                Response.Redirect(PathConstants.openNewCase + "?cmd=CaseSelect&LID=" + StandardLetterid, false);
                            }
                            else if (Request.QueryString["cmd"] == "UpdateCaseSelect")
                            {
                                SaveGridParameters();
                                Response.Redirect(PathConstants.updatecase + "?cmd=UpdateCaseSelect&LID=" + StandardLetterid, false);
                            }
                            else if (Request.QueryString["cmd"] == "ActivitySelect" || Request.QueryString["cmd"] == "ActivitySelectPreview")
                            {
                                Response.Redirect(PathConstants.AddCaseActivity + "?cmd=LetterSelect&LID=" + StandardLetterid, false);
                            }
                            else
                            {
                                AM_Action action = Session[SessionConstants.CachedActionObject] as AM_Action;
                                PushAction(action);
                                Response.Redirect(PathConstants.LRAmovetoselect + action.StatusId + "&Id=" + StandardLetterid, false);
                            }
                        }

                        break;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }

        }
        #endregion
        #region Paging Selection Grid
        protected void grdselection_PageIndexChange(object sender, ExtAspNet.GridPageEventArgs e)
        {
            grdselection.PageIndex = e.NewPageIndex;
        }
        #endregion

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(Session["PrevPage"].ToString() + "?cmd=BackToAction");
        }

        #endregion
    }
}