﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Entities;
using Am.Ahv.Website.controls.status_and_actions;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Am.Ahv.Website.secure.status_and_actions
{
    public partial class Status : PageBase
    {
        #region"Attributes"

        bool actionFlag;
        bool notfound = true;
        public bool ActionFlag
        {
            get { return actionFlag; }
            set { actionFlag = value; }
        }

        bool actionUpdate;

        public bool ActionUpdate
        {
            get { return actionUpdate; }
            set { actionUpdate = value; }
        }

        private bool editFlag;

        public bool EditFlag
        {
            get { return editFlag; }
            set { editFlag = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!base.CheckSession())
                {
                    Response.Redirect(PathConstants.BridgePath);
                    //RedirectToLoginPage();
                }
                if (!IsPostBack)
                {
                    if (Request.QueryString["cmd"] == "Edit")
                    {
                        if (!Convert.ToString(Session[SessionConstants.StatusID]).Equals(string.Empty))
                        {
                            if (Convert.ToString(Session[SessionConstants.StatusType]).Equals("Initial Case Monitoring"))
                            {
                                Action1.Visible = false;
                                FirstDetectionList1.Visible = true;
                                AddStatus1.Visible = false;
                                FirstDetectionList1.InitLookups();
                                FirstDetectionList1.LoadFirstDetection();
                            }
                            else
                            {

                                Action1.Visible = false;
                                FirstDetectionList1.Visible = false;
                                AddStatus1.Visible = true;
                                AddStatus1.InitLookups();
                                AddStatus1.LoadStatus();
                            }
                            EditFlag = true;
                        }
                    }
                    else if (Request.QueryString["cmd"] == "BackToAction")
                    {
                        Action1.Visible = true;
                        FirstDetectionList1.Visible = false;
                        AddStatus1.Visible = false;

                        // Session[SessionConstants.StatusID] = tree.SelectedNode.Parent.Value;
                        Session.Remove(SessionConstants.ActionId);
                        Action1.Initlookups();
                        //   Action1.SetStatus(Convert.ToInt32(tree.SelectedNode.Parent.Value));

                        Action1.LoadCachedAction(0);

                        // Action1.ResetPage();
                    }
                    else
                    {
                        Action1.Visible = false;
                        FirstDetectionList1.Visible = false;
                        AddStatus1.Visible = true;
                        AddStatus1.InitLookups();
                    }

                }
                Action1.updateStatusAndActionTreeEvent += delegate(bool flag)
                {
                    if (flag == true)
                    {
                        if (tree.SelectedNode == null)
                        {
                            for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
                            {
                                if (tree.Nodes[0].ChildNodes[i].Value == Convert.ToString(Session[SessionConstants.StatusID]))
                                {
                                    for (int j = 0; j < tree.Nodes[0].ChildNodes[i].ChildNodes.Count; j++)
                                    {
                                        if (tree.Nodes[0].ChildNodes[i].ChildNodes[j].Text.Contains("Add New Action"))
                                        {
                                            tree.Nodes[0].ChildNodes[i].ChildNodes[j].Selected = true;
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        ActionUpdate = true;
                        ActionFlag = true;
                        reloadTreeView(tree.SelectedNode);
                        IntegratedupdatePanel.Update();
                    }
                };
                AddStatus1.update += delegate(bool flag)
                {
                    if (flag == true)
                    {
                        if (tree.SelectedNode == null)
                        {
                            for (int i = 0; i < tree.Nodes[0].ChildNodes.Count; i++)
                            {
                                if (tree.Nodes[0].ChildNodes[i].Text == "1. First Detection")
                                {
                                    tree.Nodes[0].ChildNodes[i].Selected = true;
                                    break;
                                }
                            }
                        }
                        //    return;
                        ActionUpdate = false;
                        ActionFlag = true;
                        reloadTreeView(tree.SelectedNode);
                        IntegratedupdatePanel.Update();
                    }
                };
                FirstDetectionList1.updateTree += delegate(bool flag)
                {
                    if (flag == true)
                    {
                        if (tree.SelectedNode == null)
                            return;
                        ActionUpdate = false;
                        ActionFlag = true;
                        reloadTreeView(tree.SelectedNode);
                        IntegratedupdatePanel.Update();
                    }
                };

                AddStatus1.reset += delegate(bool flag)
                {
                    if (flag == true)
                    {
                        resetNodeSelection();
                    }
                };

                FirstDetectionList1.reset += delegate(bool flag)
                {
                    if (flag == true)
                    {
                        resetNodeSelection();
                    }
                };

                Action1.reset += delegate(bool flag)
                {
                    if (flag == true)
                    {
                        resetNodeSelection();
                    }
                };

                resetMessage();

            }
            catch (NullReferenceException nullException)
            {
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"Tree Node Populate"

        public void tree_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {

            try
            {
                if (e.Node.ChildNodes.Count == 0)
                {
                    switch (e.Node.Depth)
                    {
                        case 0:
                            PopulateStatus(e.Node);
                            break;
                        case 1:
                            PopulateActions(e.Node);
                            break;
                    }
                    if (notfound && Request.QueryString["cmd"] == "search")
                    {
                        int StatusId = int.Parse(Request.QueryString["statusid"].ToString());
                        int LetterId = int.Parse(Request.QueryString["Id"].ToString());
                        SearchTreeControl(StatusId, tree.Nodes[0].ChildNodes, LetterId);

                    }
                }


            }

            catch (NullReferenceException nullException)
            {
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
        }

        private void SearchTreeControl(int StatusId, TreeNodeCollection e, int LetterId)
        {

            for (int i = 0; i < e.Count; i++)
            {
                if (e[i].Value == StatusId.ToString())
                {
                    e[i].Expand();
                    // Added By Zunair Minhas
                    for (int j = 0; j < e[i].ChildNodes.Count; j++)
                    {
                        if (e[i].ChildNodes[j].Text.Equals("Add New Action"))
                        {

                            e[i].ChildNodes[j].Selected = true;
                        }
                    }
                    /* Commented By Zunair Minhas on 23 June 2010
                    e[i].Selected = true;
                     */
                    Action1.Visible = true;

                    if (Action1.IsCachedAction())
                    {
                        Action1.Initlookups();
                        Action1.LoadCachedAction(LetterId);
                        //Response.Write("hello");
                        Session.Remove("CachedAction");
                        Session.Remove("LettersCollection");
                        notfound = false;
                    }

                    FirstDetectionList1.Visible = false;
                    AddStatus1.Visible = false;
                    return;
                }

            }
        }

        #endregion

        #region"Tree Selected Node Changed"

        protected void tree_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                int Id = int.Parse(tree.SelectedNode.Value);
                if (Id == -11)
                {
                    Action1.Visible = true;
                    FirstDetectionList1.Visible = false;
                    AddStatus1.Visible = false;

                    Session[SessionConstants.StatusID] = tree.SelectedNode.Parent.Value;
                    Session.Remove(SessionConstants.ActionId);
                    Action1.Initlookups();
                    Action1.SetStatus(Convert.ToInt32(tree.SelectedNode.Parent.Value));
                    Action1.ResetPage();
                    Action1.DisableDeleteButton();
                }
                if (Id == -12)
                {

                    AddStatus1.Visible = true;
                    Action1.Visible = false;
                    FirstDetectionList1.Visible = false;
                    AddStatus1.InitLookups();
                    AddStatus1.ResetPage();
                    Session.Remove(SessionConstants.StatusID);
                    Session.Remove(SessionConstants.StatusType);
                }
                else
                {
                    if (tree.SelectedNode.Target == "stage")
                    {
                        Action1.Visible = false;
                        if (tree.SelectedNode.Text.Contains("Initial Case Monitoring"))
                        // if (tree.SelectedNode.Text.Equals("1. First Detection"))
                        {

                            Action1.Visible = false;
                            FirstDetectionList1.Visible = true;
                            AddStatus1.Visible = false;
                            Session[Am.Ahv.Utilities.constants.SessionConstants.StatusID] = tree.SelectedNode.Value;
                            FirstDetectionList1.InitLookups();
                            FirstDetectionList1.LoadFirstDetection();
                        }
                        else
                        {
                            Action1.Visible = false;
                            FirstDetectionList1.Visible = false;
                            AddStatus1.Visible = true;
                            Session[Am.Ahv.Utilities.constants.SessionConstants.StatusID] = tree.SelectedNode.Value;
                            AddStatus1.InitLookups();
                            AddStatus1.LoadStatus();
                        }
                        IntegratedupdatePanel.Update();
                    }
                    if (tree.SelectedNode.Target == "action")
                    {
                        Action1.Visible = true;
                        FirstDetectionList1.Visible = false;
                        AddStatus1.Visible = false;

                        Session[SessionConstants.ActionId] = tree.SelectedNode.Value;
                        Session[SessionConstants.StatusID] = tree.SelectedNode.Parent.Value;

                        Action1.Initlookups();
                        Action1.SetStatus(Convert.ToInt32(tree.SelectedNode.Parent.Value));
                        Action1.LoadAction();


                        IntegratedupdatePanel.Update();
                    }
                }
            }
            catch (NullReferenceException nullException)
            {
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
        }

        #endregion

        #region"Tree Node Collapsed"

        protected void tree_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
        {
            try
            {
                if (ActionFlag)
                {
                    if (e.Node.ChildNodes.Count > 0)
                    {
                        e.Node.ChildNodes.Clear();
                    }
                    e.Node.PopulateOnDemand = true;
                    if (!ActionUpdate)
                    {
                        PopulateStatus(e.Node);
                        foreach (TreeNode node in e.Node.ChildNodes)
                        {
                            if (node.Text != "Add New Stage")
                            {
                                PopulateActions(node);
                            }
                        }
                    }
                    else
                    {
                        PopulateActions(e.Node);
                    }

                    // tree_TreeNodePopulate(sender, e);         
                    // e.Node.CollapseAll();
                }
                else
                {
                    e.Node.CollapseAll();
                }
            }
            catch (NullReferenceException nullException)
            {
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Populate Actions"

        public void PopulateActions(TreeNode Node)
        {
            try
            {
                Am.Ahv.BusinessManager.statusmgmt.Action action = new Am.Ahv.BusinessManager.statusmgmt.Action();
                int statusid = int.Parse(Node.Value);
                List<AM_Action> actionlist = action.GetActionByStatus(statusid);
                if (actionlist != null)
                {
                    foreach (AM_Action item in actionlist)
                    {
                        TreeNode newnode = new TreeNode();
                        newnode.Value = item.ActionId.ToString();
                        newnode.Text = item.Title;
                        newnode.Target = "action";
                        Node.ChildNodes.Add(newnode);
                    }
                }
                TreeNode Addnode = new TreeNode();
                Addnode.Value = "-11";
                Addnode.Text = "Add New Action";
                Node.ChildNodes.Add(Addnode);
                //if (IsPostBack)
                //{
                //    Node.Expand();
                //}
                //else
                {
                    Node.CollapseAll();
                }
            }
            catch (EntityException entityexception)
            {
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"Populate Status"

        public void PopulateStatus(TreeNode Node)
        {
            try
            {
                int count = 1;
                Am.Ahv.BusinessManager.statusmgmt.Status statusmanager = new Am.Ahv.BusinessManager.statusmgmt.Status();
                List<AM_Status> Statuslist = statusmanager.GetAllStatus();
                if (Statuslist.Count > 0)   
                {
                    foreach (AM_Status item in Statuslist)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = count.ToString() + ". " + item.Title;
                        newNode.Value = item.StatusId.ToString();
                        newNode.Target = "stage";
                        Node.ChildNodes.Add(newNode);
                        if (Node.Text.Contains("Initial Case Monitoring"))
                            Node.Selected = true;
                        //  tree.Nodes.Add(newNode);
                        count += 1;
                    }
                }
                Node.ChildNodes.Add(new TreeNode("Add New Stage", "-12"));
            }
            catch (EntityException entityexception)
            {
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }

        }

        #endregion

        #region"Reload Tree View"

        public void reloadTreeView(TreeNode node)
        {
            try
            {
                collapseAndExpandNode(node.Parent);
            }
            catch (NullReferenceException nullException)
            {
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
        }

        #endregion

        #region"Collapse And Expand Node"

        public void collapseAndExpandNode(TreeNode node)
        {
            try
            {
                node.Collapse();
                node.Expand();
            }
            catch (NullReferenceException nullException)
            {
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
        }

        #endregion

        #endregion

        #region "Reset Message"

        public void resetMessage()
        {
            masterpages.AMMasterPage.message = string.Empty;
        }

        #endregion

        #region"Reset Node Selection"

        public void resetNodeSelection()
        {
            try
            {
                tree.Nodes[0].Selected = true;
            }
            catch (NullReferenceException nullException)
            {
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
        }

        #endregion

        #region TreeControlSearch
        private void SearchTreeControl(int StatusId)
        {

        }
        #endregion

        protected void tree_Disposed(object sender, EventArgs e)
        {

        }

        protected void tree_Load(object sender, EventArgs e)
        {


        }
    }
}
