﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LettersResourceArea.aspx.cs"
    Inherits="Am.Ahv.Website.secure.resources.LettersResourceArea" MasterPageFile="~/masterpages/AMMasterPage.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ExtAspNet" Namespace="ExtAspNet" TagPrefix="ext" %>
<asp:Content ID="header" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style4
        {
            width: 200px;
        }
        .style9
        {
            width: 17%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<div>
    <asp:Panel ID="pnlCaseList" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 10px">
                    <div style="padding:10px;">
                    <span style="float:left;">
                    <asp:Label ID="lblFirstDetection" runat="server" Font-Bold="true" ForeColor="Black"
                        Font-Size="Medium" Text="Letters"></asp:Label>
                         </span>

                         <span style="float:right;margin-bottom:5px;">
                         <asp:Button ID="btnBack" runat="server" Text="Back" Visible="false" onclick="btnBack_Click" />
                    </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 25px">
                    <asp:UpdatePanel ID="updpnlDropDowns" UpdateMode="Conditional" runat="server">
                        <contenttemplate>
                            <table>
                                <tr>
                                    <td colspan="4">
                                        <b>
                                            <asp:Label ID="lbltemplates" runat="server" Text="Templates"></asp:Label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblstatus" runat="server" Text="Stage:"></asp:Label>
                                    </td>
                                    <td>
                                  <%--  <ext:Panel ID="dropdownpanel" runat="server">
                                    <Items>--%>
                                    <ext:DropDownList ID="ddlStatus1" AutoPostBack="true" Width="200px" runat="server" 
                                            OnSelectedIndexChanged="ddlStatus1_SelectedIndexChanged"  ></ext:DropDownList>
                                    
                                 <%--   </Items>
                                    
                                    </ext:Panel>--%>
                                    
                                       <%-- <asp:DropDownList ID="ddlStatus" AutoPostBack="true" Width="200px" runat="server"
                                            OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                        </asp:DropDownList>--%>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbltitle" runat="server" Text="Title:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txttile" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblaction" runat="server" Text="Action:"></asp:Label>
                                    </td>
                                    <td>
                                     <ext:DropDownList ID="ddlaction1" AutoPostBack="true" Width="200px" runat="server" 
                                            ></ext:DropDownList>
                                       <%-- <asp:DropDownList ID="ddlaction" AutoPostBack="false" Width="200px" runat="server">
                                        </asp:DropDownList>--%>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblcode" runat="server" Text="Code:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcode" runat="server" Width="200px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="Button1" runat="server" Text="Search" OnClick="Button1_Click" />
                                    </td>
                                </tr>
                            </table>
                     </contenttemplate></asp:UpdatePanel>
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="6" valign="top" class="tdClass" style="padding-left: 20px; padding-right: 15px">
                    <asp:Panel ID="pnlgrid" runat="server">
                        <asp:UpdatePanel ID="uplettersearch" runat="server" UpdateMode="Conditional">
                            <contenttemplate>
                                <ext:PageManager ID="PageManager1" runat="server" />
                                 <asp:Panel id="pnlseachletters" runat="server">
                                 <ext:Grid ID="grdsearchedletters" runat="server" AutoHeight="true" AutoWidth="true"
                                    AllowPaging="true" AllowSorting="true" EnableAlternateRowColor="true" EnableAjax="true"
                                    EnableLightBackgroundColor="true" ShowBorder="true" PageSize="20" Title="Letters"
                                    DataKeyNames="StandardLetterId" EnableMultiSelect="false" 
                                    onprerowdatabound="grdsearchedletters_PreRowDataBound" 
                                    onrowcommand="grdsearchedletters_RowCommand" 
                                         onpageindexchange="grdsearchedletters_PageIndexChange" >
                                    <Columns>
                                        <ext:TemplateField HeaderText="Code:">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcodeusr" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Code")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:TemplateField HeaderText="Stage:">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatususr" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "AM_Status.Title")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:TemplateField HeaderText="Action:">
                                            <ItemTemplate>
                                                <asp:Label ID="lblactionusr" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "AM_Action.Title")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:TemplateField HeaderText="Title:">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:TemplateField HeaderText="Modified">
                                        <ItemTemplate>
                                        <asp:Label ID="lblmodifieddate" runat="server" Text='<%#FormatDate(DataBinder.Eval(Container.DataItem,"ModifiedDate")) %>'></asp:Label>                                        
                                        </ItemTemplate>                                        
                                        </ext:TemplateField>                                        
                                        <ext:TemplateField HeaderText="By:">
                                            <ItemTemplate>
                                                <asp:Label ID="lblmodifiedBy" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Body")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        
                                        
                                        <ext:LinkButtonField ColumnID="lbbuttonpreview" CommandName="Preview" Text="Preview" EnablePostBack="true" Width="60"/>
                                        <ext:LinkButtonField ColumnID="lbbuttonedit"  CommandName="Edit"   Text="Edit" EnablePostBack="true" Width="35"/>
                                        <ext:TemplateField Width="50">
                                            <ItemTemplate>
                                                  <a href="<%# GetEditUrl(DataBinder.Eval(Container.DataItem, "StandardLetterId")) %>">Delete</a>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        
                                    </Columns>
                                </ext:Grid>
                                <br />
                                <div style="float:right">
                                <asp:Button ID="btnCreateNewTemplate" runat="server" Text="Add New Template" 
                                    onclick="btnCreateNewTemplate_Click" />
                                    </div>
                                </asp:Panel>
                                <asp:Panel id="pnlselection" runat="server">
                                 <ext:Grid ID="grdselection" runat="server" AutoHeight="true" AutoWidth="true"
                                    AllowPaging="true" AllowSorting="true" EnableAlternateRowColor="true" EnableAjax="true"
                                    EnableLightBackgroundColor="true" ShowBorder="true" PageSize="20" Title="Letters"
                                    DataKeyNames="StandardLetterId" EnableMultiSelect="false" 
                                    onprerowdatabound="grdselection_PreRowDataBound" 
                                    onrowcommand="grdselection_RowCommand" 
                                        onpageindexchange="grdselection_PageIndexChange" >
                                    <Columns>
                                        <ext:TemplateField HeaderText="Code:">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Code")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:TemplateField HeaderText="Stage:">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "AM_Status.Title")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:TemplateField HeaderText="Action:">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "AM_Action.Title")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:TemplateField HeaderText="Title:">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Title")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:TemplateField HeaderText="Modified">
                                        <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%#FormatDate(DataBinder.Eval(Container.DataItem,"ModifiedDate")) %>'></asp:Label>
                                        
                                        </ItemTemplate>
                                        
                                        </ext:TemplateField>
                                        
                                        <ext:TemplateField HeaderText="By:">
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Body")%>'></asp:Label>
                                            </ItemTemplate>
                                        </ext:TemplateField>
                                        <ext:LinkButtonField ColumnID="lbbuttonpreview" CommandName="Preview" CommandArgument="StandardLetterId" Text="Preview" EnablePostBack="true" Width="70" />
                                         <ext:LinkButtonField ColumnID="lbbuttonedit"  CommandName="Edit"   CommandArgument="StandardLetterId" Text="Edit" EnablePostBack="true" Width="35"/>
                                        <ext:LinkButtonField ColumnID="lbbuttonselect"  CommandArgument="StandardLetterId" CommandName="Select"  Text="Select" EnablePostBack="true" width="70"/>
                                    </Columns>
                                </ext:Grid>
                                </asp:Panel>
                                <ext:Window ID="Window1" Title="Edit" Popup="false" EnableIFrame="true" runat="server"
                                    EnableConfirmOnClose="true" IFrameUrl="about:blank" Target="Parent" IsModal="True"
                                    Width="300px" Height="200px">
                                </ext:Window>
                        </contenttemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>
</asp:Content>
