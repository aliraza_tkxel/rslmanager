using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.letters;
using Am.Ahv.Entities;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using System.Configuration.Assemblies;
using Am.Ahv.BusinessManager.resource;
using Am.Ahv.Utilities.utitility_classes;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;
using System.Configuration;
using iTextSharp.text.html.simpleparser;
using Am.Ahv.BusinessManager.Casemgmt;
using System.Collections;
using System.Xml;
using System.Globalization;
using System.Threading;

namespace Am.Ahv.Website.secure.resources
{
    public partial class LetterPreview : PageBase
    {
        Users resourceManager = new Users();
        #region"Attributes"
        bool isError;
        bool isException;
        string marketRent = string.Empty;
        string rentBalance = string.Empty;
        string todayDate = string.Empty;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        static string backURL;
        public string BackURL
        {
            get { return backURL; }
            set { backURL = value; }
        }
        #endregion

        #region"Events"

        #region"Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!base.CheckSession())
                {
                    Response.Redirect(PathConstants.BridgePath);
                    //RedirectToLoginPage();
                }
                resetMessage();
                if (!IsPostBack)
                {
                    if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.PreviewLetter)
                    {
                        PreviewLetter();
                        BackURL = PathConstants.LetterCreateTemplate;
                    }
                    else if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.PreviewTemplate)
                    {
                        PreviewTemplate();
                        BackURL = PathConstants.LetterCreateTemplate;
                    }
                    else if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.search + "Action")
                    {
                        ShowStandardLetter(Int32.Parse(Request.QueryString[ApplicationConstants.Id]));
                        BackURL = PathConstants.lettersResourceAreaPathCached + "?cmd=newAction";

                    }
                    else if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.search + "case")
                    {
                        ShowStandardLetter(Int32.Parse(Request.QueryString[ApplicationConstants.Id]));
                        BackURL = PathConstants.lettersResourceAreaPathCached + "?cmd=CaseSelect";

                    }
                    else if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.search + "UpdateCase")
                    {
                        ShowStandardLetter(Int32.Parse(Request.QueryString[ApplicationConstants.Id]));
                        if (Request.QueryString["Id"] != null)
                        {
                            BackURL = PathConstants.lettersResourceAreaPathCached + "?cmd=UpdateCaseSelect&SID=" + base.GetStatusId() + "&AID=" + base.GetActionId();
                        }
                        else
                            BackURL = PathConstants.lettersResourceAreaPathCached + "?cmd=UpdateCaseSelect";

                    }
                    else if (Request.QueryString["cmd"] == ApplicationConstants.search + "Activity")
                    {
                        ShowStandardLetter(Int32.Parse(Request.QueryString[ApplicationConstants.Id]));
                        BackURL = PathConstants.lettersResourceAreaPathCached + "?cmd=ActivitySelectPreview";
                    }
                    else if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.search)
                    {
                        ShowStandardLetter(Int32.Parse(Request.QueryString[ApplicationConstants.Id]));
                        BackURL = PathConstants.lettersResourceAreaPathCached + "?cmd=new";

                    }
                    else if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.search + "PopUp")
                    {
                        string id = Request.QueryString[ApplicationConstants.Id].ToString();
                        string[] str = id.Split(';');
                        if (str[1] != "%" && str[1] != "0")
                        {
                            ShowStandardLetterHistory(int.Parse(str[1]));
                        }
                        else
                        {
                            ShowStandardLetter(Int32.Parse(str[0]));
                        }
                        Button1.Visible = false;
                        btnClose.Visible = true;
                    }
                    else if (Request.QueryString[ApplicationConstants.cmd] == null)
                    {
                        PreviewTemplate();
                        BackURL = PathConstants.LetterCreateTemplate;
                    }

                    if (Request.QueryString["type"] != null)
                    {
                        if (Request.QueryString["type"] == "new")
                        {
                            BackURL = PathConstants.LetterCreateTemplate + "?cmd=new";
                        }
                        else if (Request.QueryString["type"] == "edit")
                        {
                            if (Request.QueryString["TID"] != null)
                                BackURL = PathConstants.LetterCreateTemplate + "?cmd=backedit" + "&Id=" + base.GetStandardLetterId() + "&TID=" + Request.QueryString["TID"];
                            else
                                BackURL = PathConstants.LetterCreateTemplate + "?cmd=backedit";
                        }
                        else if (Request.QueryString["type"] == "cedit")
                        {
                            BackURL = PathConstants.LetterCreateTemplate + "?cmd=cedit&ispreviewback=yes";
                        }
                        else if (Request.QueryString["type"] == "aedit")
                        {
                            BackURL = PathConstants.LetterCreateTemplate + "?cmd=activityedit&ispreviewback=yes";
                        }
                    }




                    //if (Request.UrlReferrer != null)
                    //{
                    //    BackURL = Request.UrlReferrer.PathAndQuery;
                    //    //if (Request.UrlReferrer.Query != null)
                    //    //{
                    //    //    if (Request.UrlReferrer.Query == "?cmd=new")
                    //    //    {
                    //    //        BackURL = BackURL + "&p=true";
                    //    //    }
                    //    //}
                    //}
                    //else if (Request.UrlReferrer == null)
                    //{
                    //  //  btnBack.Enabled = false;
                    //}
                    //LoadOrganisationDetailandLogo();
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion

        #region"btn Back Click"
        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (BackURL != null)
            {
                Response.Redirect(BackURL, false);
            }
        }
        #endregion

        #endregion

        #region"Function"

        #region"Preview Template"
        private void PreviewTemplate()
        {
            try
            {
                PopulateData();
                SetLetterContents();
            }
            catch (Exception ex)
            {
                setMessage(UserMessageConstants.previewError, true);
                throw ex;
            }
        }
        #endregion

        #region"Preview Letter"
        private void PreviewLetter()
        {
            try
            {

                ShowTennantInfo();
                SetLetterContents();
                //ShowStandardLetter(base.GetTenantId());
            }
            catch (Exception ex)
            {
                setMessage(UserMessageConstants.previewError, true);
                throw ex;
            }

        }
        #endregion

        #region"Show Standard Letter"
        private void ShowStandardLetter(int StandardLetterId)
        {
            try
            {
                DateTime LetterCreateDate;
                LetterResourceArea LSA = new LetterResourceArea();
                AM_StandardLetters Letter = LSA.GetStandardLetter(StandardLetterId);

                ViewState[ViewStateConstants.CompleteLetterObject] = Letter;
                ViewState[ViewStateConstants.CompleteLetterObjectType] = ApplicationConstants.StandardLetter;

                string _employeeJobTitle = (Letter.FromResourceId == null) ? "" : resourceManager.getJobTitle(Letter.FromResourceId.Value);
                string _employeeWorkEmail = (Letter.FromResourceId == null) ? "" : (resourceManager.getWorkEmail(Letter.FromResourceId.Value));
                string _employeeWorkDirectDial = (Letter.FromResourceId == null) ? "" : (resourceManager.getWorkDirectDial(Letter.FromResourceId.Value));

                ///  Call function here
                Letter.Body = ReplaceContents(Letter.Body);
                lblLetterBody.Text = HttpUtility.HtmlDecode(Letter.Body);
                if (Letter.FromResourceId != null)
                {
                    lblFrom.Text = LSA.GetEmployeeNameForLetter(Letter.FromResourceId.Value);
                }
                if (Letter.TeamId != null)
                {
                    //lblTeamName.Text = LSA.GetTeamName(Letter.TeamId);
                    lblTeamName.Text = _employeeJobTitle;
                }

                if (_employeeWorkDirectDial != null)
                {
                    lblDirectDial.Text = "Direct Dial: " + _employeeWorkDirectDial;
                }

                if (_employeeWorkEmail != null)
                {
                    lblEmail.Text = "Email: " + _employeeWorkEmail;
                }

                Letter.AM_LookupCode1Reference.Load();
                if (Letter.SignOffLookupCode != null)
                {
                    lblSignOff.Text = Letter.AM_LookupCode1.CodeName;
                }
                if (this.todayDate != null && this.todayDate.Length != 0)
                {
                    LetterCreateDate = DateTime.Parse(this.todayDate);
                }
                else
                {
                    //if(Letter.CreatedDate!=null )
                    //    LetterCreateDate = Letter.CreatedDate;
                    //else
                    LetterCreateDate = DateTime.Today;
                }

                if (Session[SessionConstants.TenantId] == null)
                {
                    PopulateData();
                }
                else
                {
                    ShowTennantInfo();
                    //if there 'll be no history of letter then there 'll not be employee id 
                    Session["_EmployeeId"] = (Letter.FromResourceId == null) ? null : Letter.FromResourceId.Value.ToString();
                    if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.search + "PopUp")
                    {
                        SaveAsPDF(Letter.Body, lblFrom.Text, lblTeamName.Text, lblSignOff.Text, LetterCreateDate);
                    }
                }

            }
            catch (Exception ex)
            {
                setMessage(UserMessageConstants.previewError, true);
                throw ex;
            }
        }

        #region"Show Standard Letter History"

        public void ShowStandardLetterHistory(int id)
        {
            try
            {
                DateTime LetterCreateDate;
                LetterResourceArea LSA = new LetterResourceArea();
                AM_StandardLetterHistory Letter = LSA.GetStandardLetterHistoryById(id);
                ViewState[ViewStateConstants.CompleteLetterObject] = Letter;
                ViewState[ViewStateConstants.CompleteLetterObjectType] = ApplicationConstants.StandardLetterHisotry;

                ///  Call function here
                Letter.Body = ReplaceContents(Letter.Body);
                lblLetterBody.Text = HttpUtility.HtmlDecode(Letter.Body);
                if (Letter.FromResourceId != null)
                {
                    lblFrom.Text = LSA.GetEmployeeNameForLetter(Letter.FromResourceId.Value);
                }
                if (Letter.TeamId != null)
                {
                    lblTeamName.Text = LSA.GetTeamName(Letter.TeamId);
                    //lblTeamName.Text = resourceManager.getJobTitle(Letter.FromResourceId.Value);
                }
                Letter.AM_LookupCode1Reference.Load();
                if (Letter.SignOffLookUpCode != null)
                {
                    lblSignOff.Text = Letter.AM_LookupCode1.CodeName;
                }
                if (this.todayDate.Length != 0)
                {
                    LetterCreateDate = DateTime.Parse(this.todayDate);
                }
                else
                {
                    //if (Letter.CreatedDate != null)
                    //     LetterCreateDate = Letter.CreatedDate;
                    // else
                    LetterCreateDate = DateTime.Today;
                }

                if (Session[SessionConstants.TenantId] == null)
                {
                    PopulateData();
                }
                else
                {
                    ShowTennantInfo();
                    Session["_EmployeeId"] = Letter.FromResourceId.Value;
                    if (Request.QueryString[ApplicationConstants.cmd] == ApplicationConstants.search + "PopUp")
                    {
                        SaveAsPDF(Letter.Body, lblFrom.Text, lblTeamName.Text, lblSignOff.Text, LetterCreateDate);
                    }
                }

            }
            catch (Exception ex)
            {
                setMessage(UserMessageConstants.previewError, true);
                throw ex;
            }
        }

        #endregion

        private string ReplaceContents(string MainString)
        {
            try
            {

                this.GetMrRbTodayDate();

                MainString = HttpUtility.HtmlDecode(MainString);
                string RentBalanceCode = "[RB]";
                string RentAmountCode = "[RC]";
                string DateCode = "[TD]";
                string MarketRent = "[MR]";
                LetterResourceArea letterarea = new LetterResourceArea();

                if (Session[SessionConstants.TenantId] != null && this.marketRent != null && this.rentBalance != null && this.todayDate != null && Session[SessionConstants.WeeklyRentAmount] != null)
                {
                    //writing the below line to remove time from datetime string
                    DateTime letterDate = DateTime.Parse(this.todayDate);

                    int tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                    MainString = MainString.Replace(RentAmountCode, "<b>" + base.GetWeeklyRentAmount().ToString() + "</b>");

                    MainString = MainString.Replace(RentBalanceCode, "<b>" + this.rentBalance + "</b>");
                    MainString = MainString.Replace(DateCode, letterDate.ToShortDateString());
                    MainString = MainString.Replace(MarketRent, this.marketRent);
                }
                else if (Session[SessionConstants.TenantId] != null && Session[SessionConstants.Rentbalance] != null && Session[SessionConstants.WeeklyRentAmount] != null)
                {
                    int tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                    MainString = MainString.Replace(RentAmountCode, "<b>" + base.GetWeeklyRentAmount().ToString() + "</b>");

                    MainString = MainString.Replace(RentBalanceCode, "<b>" + base.GetRentBalance().ToString() + "</b>");
                    MainString = MainString.Replace(DateCode, DateTime.Now.ToString("dd/MM/yyyy"));
                    MainString = MainString.Replace(MarketRent, letterarea.GetMarketRent(tenantid).ToString());
                }
                else
                {
                    MainString = MainString.Replace(RentBalanceCode, "<b>DummyRentBalance</b>");
                    MainString = MainString.Replace(RentAmountCode, "<b>DummyWeeklyRentAmount</b>");
                    MainString = MainString.Replace(MarketRent, "<b>Dummy Market Rent</b>");
                    MainString = MainString.Replace(DateCode, DateTime.Now.ToString("dd/MM/yyyy"));
                }

                return MainString;
            }
            catch (ArgumentNullException argumentnull)
            {
                setMessage(UserMessageConstants.previewError, true);
                ExceptionPolicy.HandleException(argumentnull, "Exception Policy");
                return null;
            }
            catch (ArgumentException argumentexception)
            {
                setMessage(UserMessageConstants.previewError, true);
                ExceptionPolicy.HandleException(argumentexception, "Exception Policy");
                return null;
            }
            catch (Exception ex)
            {
                setMessage(UserMessageConstants.previewError, true);
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return null;
            }
        }
        #endregion

        #region "GetMrRbTodayDate"
        public void GetMrRbTodayDate()
        {

            this.marketRent = null;
            this.rentBalance = null;
            this.todayDate = null;
            LetterResourceArea LRA = new BusinessManager.letters.LetterResourceArea();
            List<AM_ActivityAndStandardLetters> ActivityLetterList = new List<AM_ActivityAndStandardLetters>();
            AM_ActivityAndStandardLetters ActivityLetter = new AM_ActivityAndStandardLetters();

            int ActivityId = 0;
            if (!String.IsNullOrEmpty(Request.QueryString["LetterActivityId"]))
            {
                ActivityId = Int32.Parse(Request.QueryString["LetterActivityId"]);
                ActivityLetterList = LRA.GetStandardLetterByActivityId(ActivityId);

                if (ActivityLetterList.Count() > 0)
                {
                    ActivityLetter = ActivityLetterList.First();
                    this.marketRent = ActivityLetter.MarketRent == null ? null : ActivityLetter.MarketRent.ToString();
                    this.rentBalance = ActivityLetter.RentBalance == null ? null : ActivityLetter.RentBalance.ToString();
                    this.todayDate = ActivityLetter.CreatedDate == null ? null : ActivityLetter.CreatedDate.ToString();

                }

            }
            else if (!String.IsNullOrEmpty(Request.QueryString["CaseLetterId"]))
            {
                int CaseId = Int32.Parse(Request.QueryString["CaseLetterId"]);
                CaseHistory CaseHist = new CaseHistory();

                AM_StandardLetters Letter = new AM_StandardLetters();
                AM_StandardLetterHistory LetterHistory = new AM_StandardLetterHistory();

                AM_CaseAndStandardLetter CaseLetter = new AM_CaseAndStandardLetter();
                int LetterId = 0;

                if (ViewState[ViewStateConstants.CompleteLetterObjectType].ToString() == ApplicationConstants.StandardLetter)
                {
                    Letter = (AM_StandardLetters)ViewState[ViewStateConstants.CompleteLetterObject];
                    LetterId = Letter.StandardLetterId;
                }
                else if (ViewState[ViewStateConstants.CompleteLetterObjectType].ToString() == ApplicationConstants.StandardLetterHisotry)
                {
                    LetterHistory = (AM_StandardLetterHistory)ViewState[ViewStateConstants.CompleteLetterObject];
                    LetterId = LetterHistory.StandardLetterId;
                }

                int CaseHistoryId = (int)Session[SessionConstants.CaseHistoryId];
                CaseLetter = CaseHist.GetFirstCaseStandardLetter(CaseHistoryId, LetterId);

                this.marketRent = CaseLetter.MarketRent == null ? null : CaseLetter.MarketRent.ToString();
                this.rentBalance = CaseLetter.RentBalance == null ? null : CaseLetter.RentBalance.ToString();
                this.todayDate = CaseLetter.CreateDate == null ? null : CaseLetter.CreateDate.ToString();


            }
        }
        #endregion
        #region"Get Todays Date"
        private string GetTodayDate()
        {
            //string[] f = DateTime.Today.GetDateTimeFormats();
            //return f[25].Substring(0, f[25].Length - 4);
            return DateTime.Today.ToString("dd MMMM yyyy");
        }
        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = System.Drawing.Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Populate Data"

        private void PopulateData()
        {
            lblDate.Text = GetTodayDate();
            lblTenantName.Text = "< Tanant Name >";
            lblHouseNumber.Text = "< HouseNumber >";
            lblAddress1.Text = "< Address Line 1 >";
            lblAddress2.Text = "< Address Line 2 >";
            lblAddress3.Text = "< Address Line 3 >";
            lblTownCity.Text = "< Town/City >";
            lblCountry.Text = "< County >";
            lblPostCode.Text = "< PostCode >";

            lblTenancyId.Text = "< Tenancy Ref >";
            lblDate.Text = "< DD/MM/YYYY >";
            lblDear.Text = "Dear";
            lblDearTenant.Text = "< Tenant Name >";
        }
        #endregion

        #region"Show Tennant Info"
        private void ShowTennantInfo()
        {
            try
            {
                if (checkCustomer().Contains('&') && base.GetIsInvidualLetterReq() == 1)
                    HidindvidualLetterReq.Value = "1";

                if ((Request.QueryString["TID"] != null && Validation.CheckIntegerValue(Request.QueryString["TID"]) && base.GetIsManualAddress() == 1) || base.GetIsManualAddress() == 1)
                {
                    List<string> data = base.GetManualAddreesData();
                    lblTenantName.Text = data.ElementAt(0);
                   // lblTenantName.Attributes.Add("Style", "margin-left:2.5cm");
                    lblDearTenant.Text = data.ElementAt(0);
                    lblAddress1.Text = data.ElementAt(1);
                    lblAddress2.Text = data.ElementAt(2);
                    lblAddress3.Text = data.ElementAt(3);
                    lblTownCity.Text = data.ElementAt(4);
                    lblPostCode.Text = data.ElementAt(5);
                    lblCountry.Text = "";
                    lblHouseNumber.Text = "";
                    lblDear.Text = "Dear ";
                    lblDate.Text = GetTodayDate();
                    lblTenancyId.Text = "Tenancy Ref: <b>" + base.GetTenantId().ToString() + "</b>";

                }
                else
                {
                    LetterResourceArea LSA = new LetterResourceArea();
                    AM_SP_GetCustomerInfoForLetter_Result CustomerInfo = null;
                    if (Request.QueryString["TID"] != null && Validation.CheckIntegerValue(Request.QueryString["TID"]) && base.GetCustomerContactAddressID() != -1)
                        CustomerInfo = LSA.GetCustomerInfo(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]), base.GetCustomerContactAddressID());
                    else
                        CustomerInfo = LSA.GetCustomerInfo(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));//base.GetTenantId()

                    if (CustomerInfo != null)
                    {
                        if (CustomerInfo.JointTenancyCount == 2)
                        {
                            lblTenantName.Text = CustomerInfo.CustomerName + " " + CustomerInfo.CustomerName2;
                            lblDearTenant.Text = CustomerInfo.CustomerName + " " + CustomerInfo.CustomerName2;
                        }
                        else
                        {
                            lblTenantName.Text = CustomerInfo.CustomerName;
                            lblDearTenant.Text = CustomerInfo.CustomerName;
                        }

                        lblHouseNumber.Text = CustomerInfo.HOUSENUMBER.Length > 0 ? CustomerInfo.HOUSENUMBER + " " : "";
                        lblAddress1.Text = CustomerInfo.ADDRESS1;
                        lblAddress2.Text = CustomerInfo.ADDRESS2;
                        lblAddress3.Text = CustomerInfo.ADDRESS3;
                        lblTownCity.Text = CustomerInfo.TOWNCITY;
                        lblCountry.Text = CustomerInfo.COUNTY;
                        lblPostCode.Text = CustomerInfo.POSTCODE;
                        lblDear.Text = "Dear ";
                        //lblDearTenant.Text = CustomerInfo.CustomerName;
                        lblDate.Text = GetTodayDate();
                        lblTenancyId.Text = "Tenancy Ref: <b>" + base.GetTenantId().ToString() + "</b>";
                    }
                    else
                    {
                        lblTenantName.Text = UserMessageConstants.NoCustomerInfoPreviewLetter;
                        lblTenantName.ForeColor = System.Drawing.Color.Red;
                    }
                }

                if (lblAddress2.Text.Length < 1)
                    rowAddress2.Attributes.Add("style", "display:none;");
                if (lblAddress3.Text.Length < 1)
                    rowAddress3.Attributes.Add("style", "display:none;");
                if (lblTownCity.Text.Length < 1)
                    rowTownCity.Attributes.Add("style", "display:none;");
                if (lblPostCode.Text.Length < 1)
                    rowPostCode.Attributes.Add("style", "display:none;");
                if (lblCountry.Text.Length < 1)
                    rowCountry.Attributes.Add("style", "display:none;");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region"Set Letter's Contents"
        private void SetLetterContents()
        {

            string _employeeJobTitle = resourceManager.getJobTitle(Convert.ToInt32(Session["_EmployeeId"]));
            string _employeeWorkEmail = (resourceManager.getWorkEmail(Convert.ToInt32(Session["_EmployeeId"])));
            string _employeeWorkDirectDial = (resourceManager.getWorkDirectDial(Convert.ToInt32(Session["_EmployeeId"])));

            try
            {

                if (Session[ApplicationConstants.Letter] != null)
                {
                    lblLetterBody.Text = ReplaceContents((string)Session[ApplicationConstants.Letter]);
                    //lblLetterBody.Text = (string)Cache[ApplicationConstants.Letter];

                }
                if (Session[ApplicationConstants.SignOff] != null)
                {
                    lblSignOff.Text = (string)Session[ApplicationConstants.SignOff];
                }
                if (Session[ApplicationConstants.From] != null)
                {
                    lblFrom.Text = (string)Session[ApplicationConstants.From];
                }

                if (_employeeJobTitle != "")
                {
                    lblTeamName.Text = _employeeJobTitle;
                }

                if (_employeeWorkDirectDial != "")
                {
                    lblDirectDial.Text = "Direct Dial: " + _employeeWorkDirectDial;
                }

                if (_employeeWorkEmail != "")
                {
                    lblEmail.Text = _employeeWorkEmail;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        //#region"Load Organisation Detail and Logo"
        //private void LoadOrganisationDetailandLogo()
        //{
        //    try
        //    {
        //        string destinationPath = System.Configuration.ConfigurationManager.AppSettings[ApplicationConstants.LetterLogo];
        //       // ImgLogo.ImageUrl = destinationPath + "Logo.gif";
        //        string orgInfo = System.Configuration.ConfigurationManager.AppSettings[ApplicationConstants.OrgName];
        //        if (orgInfo.Equals(ApplicationConstants.OrgNameVal))
        //        {
        //            lblOrgName.Text = "< " + orgInfo + " >";
        //        }
        //        else
        //        {
        //            lblOrgName.Text = orgInfo;
        //        }
        //        orgInfo = System.Configuration.ConfigurationManager.AppSettings[ApplicationConstants.OrgAddress];
        //        if (orgInfo.Equals(ApplicationConstants.OrgAddressVal))
        //        {
        //            lblOrgAddress.Text = "< " + orgInfo + " >";
        //        }
        //        else
        //        {
        //            lblOrgAddress.Text = orgInfo;
        //        }

        //        orgInfo = System.Configuration.ConfigurationManager.AppSettings[ApplicationConstants.OrgTel];
        //        if (orgInfo.Equals(ApplicationConstants.OrgTelVal))
        //        {
        //            lblOrgTel.Text = "< " + orgInfo + " >";
        //        }
        //        else
        //        {
        //            lblOrgTel.Text = orgInfo;
        //        }
        //        orgInfo = System.Configuration.ConfigurationManager.AppSettings[ApplicationConstants.OrgEmail];
        //        if (orgInfo.Equals(ApplicationConstants.OrgEmailVal))
        //        {
        //            lblOrgEmail.Text = "< " + orgInfo + " >";
        //        }
        //        else
        //        {
        //            lblOrgEmail.Text = orgInfo;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //#endregion

        private void setBackURL()
        {

        }


        #endregion

        #region"Save As PDF"

        public void SaveAsPDF(string body, string from, string team, string signOff, DateTime letterdate)
        {
            try
            {
                string customerName = "";
                int count = 1;
                if (HidindvidualLetterReq.Value == "1")
                    count = 2;
                customerName = HidCutomer1.Value;

                CreateLetter createLetter = new CreateLetter();
                string[] f = DateTime.Today.GetDateTimeFormats();
                LetterResourceArea letterManager = new LetterResourceArea();
                //Document doc = new Document();

                Document doc = new Document(iTextSharp.text.PageSize.A4, 57, 57, 45, 73);
                string filePath = Request.PhysicalApplicationPath + "/Letter1.pdf";
                MemoryStream ms = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                //String str = HttpUtility.HtmlDecode(body);
                String str = body;
                string newString = "";
                StringReader sr = null;

                string OrganisationName = ConfigurationManager.AppSettings["OrganisationName"].ToString();
                string OrganisationAddress = ConfigurationManager.AppSettings["OrganisationAddress"].ToString();
                string OrganisationPhone = ConfigurationManager.AppSettings["OrganisationPhone"].ToString();
                string OrganisationEmail = ConfigurationManager.AppSettings["OrganisationEmail"].ToString();

                string _employeeJobTitle = String.Empty;
                string _employeeWorkEmail = String.Empty;
                string _employeeWorkDirectDial = String.Empty;
                //if there 'll be no history of letter then there 'll not be employee id 
                if (Session["_EmployeeId"] != null && !Session["_EmployeeId"].Equals(String.Empty))
                {
                    _employeeJobTitle = (resourceManager.getJobTitle(Convert.ToInt32(Session["_EmployeeId"])));
                    _employeeWorkEmail = (resourceManager.getWorkEmail(Convert.ToInt32(Session["_EmployeeId"])));
                    _employeeWorkDirectDial = (resourceManager.getWorkDirectDial(Convert.ToInt32(Session["_EmployeeId"])));
                }

                // Phrase footerText = new Phrase(OrganisationName + "," + OrganisationAddress + " Tel: " + OrganisationPhone + " Email: " + OrganisationEmail);
                //  HeaderFooter headerFooter = new HeaderFooter(footerText, false);
                //  headerFooter.Border = iTextSharp.text.Rectangle.TOP_BORDER;
                //  headerFooter.Alignment = iTextSharp.text.Rectangle.ALIGN_CENTER;
                //  doc.Footer = headerFooter;
                //str = str.Replace("<p>&nbsp;</p>", "\n");
                if (str.Contains("/style/images/userfiles/"))
                {
                    newString = str.Replace("/style/images/userfiles/file/", Server.MapPath(PathConstants.LetterTemplateImages) + "\\file\\");
                    sr = new StringReader(newString);
                }
                else
                {
                    sr = new StringReader(str);
                }
                HTMLWorker htmlWork = new HTMLWorker(doc);
                ArrayList parsedList = HTMLWorker.ParseToList(sr, null);
                doc.Open();

                for (int i = 0; i < count; i++)
                {
                    if (i > 0)
                    {
                        customerName = HidCutomer2.Value;
                        doc.NewPage();
                    }
                    //string stripped = Regex.Replace(str, @"<(.|\n)*?>", string.Empty);
                    AM_SP_GetCustomerInfoForLetter_Result customer = letterManager.GetCustomerInfo(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    // doc.Add(new Paragraph(f[25].Substring(0, f[25].Length - 4)));
                    //doc.Add(new Paragraph(Environment.NewLine));

                    doc.Add(new Paragraph(" Tenancy Ref:" + base.GetTenantId().ToString()));
                    doc.Add(new Paragraph(" " + letterdate.ToString("dd MMMM yyyy")));
                    //doc.Add(new Paragraph(DateTime.Now.ToString("dd/MM/yyyy")));
                    doc.Add(new Paragraph(Environment.NewLine));
                    doc.Add(new Paragraph(" " + customerName));
                    lblHouseNumber.Text = lblHouseNumber.Text.Length > 0 ? lblHouseNumber.Text + " " : "";
                    doc.Add(new Paragraph(" " + lblHouseNumber.Text + lblAddress1.Text));
                    if (lblAddress2.Text.Length > 0)
                        doc.Add(new Paragraph(" " +  lblAddress2.Text));
                    if (lblAddress3.Text.Length > 0)
                        doc.Add(new Paragraph(" " + lblAddress3.Text));
                    if (lblTownCity.Text.Length > 0)
                        doc.Add(new Paragraph(" " + lblTownCity.Text));
                    if (lblCountry.Text.Length > 0)
                        doc.Add(new Paragraph(" " + lblCountry.Text));
                    if (lblPostCode.Text.Length > 0)
                        doc.Add(new Paragraph(" " + lblPostCode.Text));
                    doc.Add(new Paragraph(Environment.NewLine));
                    doc.Add(new Paragraph("Dear " + customerName));
                    doc.Add(new Paragraph(Environment.NewLine));

                    //sr = new StringReader(sr.ToString().Replace("<br/>", Environment.NewLine));
                    for (int k = 0; k < parsedList.Count; k++)
                    {
                        IElement element = (IElement)parsedList[k];
                        if (element.Chunks.Count == 0)
                        {
                            doc.Add(new Paragraph(Environment.NewLine));
                        }
                        doc.Add(element);
                    }

                    doc.Add(new Paragraph(Environment.NewLine));
                    doc.Add(new Paragraph(signOff + ","));
                    doc.Add(new Paragraph(Environment.NewLine));
                    doc.Add(new Paragraph(Environment.NewLine));
                    doc.Add(new Paragraph(Environment.NewLine));
                    doc.Add(new Paragraph(Environment.NewLine));
                    doc.Add(new Paragraph(from));
                    doc.Add(new Paragraph(_employeeJobTitle));
                    doc.Add(new Paragraph("Direct Dial: " + _employeeWorkDirectDial));

                    if (_employeeWorkEmail != null)
                    {
                        if (_employeeWorkEmail != "")
                        {
                            doc.Add(new Paragraph("Email: " + _employeeWorkEmail));
                        }
                    }
                    //doc.Add(new Paragraph(team));
                    // doc.Add(new Paragraph(resourceManager.getJobTitle(Convert.ToInt32(ddlFrom.SelectedValue))));
                    //doc.Add(new Paragraph("<Organisation Name>, <Organisation Address> Tel: <Organisation Telephone Number> Email: <Organisation Email>"));
                }
                doc.Close();
                Response.Clear();
                Response.AppendHeader("content-disposition", "attachment; filename= Letter1.pdf");
                Response.ContentType = "application/pdf";
                Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
                Response.OutputStream.Flush();


                File.Delete(filePath);
                //Response.End();
                //HttpContext.Current.Response.End();     
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void ConvertHtmlToPDF()
        //{
        //    string filePath = Request.PhysicalApplicationPath + "/Letter1.pdf";

        //    HtmlForm form = new HtmlForm();

        //    Document doc = new Document();
        //    MemoryStream ms = new MemoryStream();
        //    PdfWriter writer = PdfWriter.GetInstance(doc, ms);          
        //    String str = HttpUtility.HtmlDecode(fckTextEditor.Value);
        //    string newString = "";
        //    StringReader sr = null;
        //    if (str.Contains("/style/images/userfiles/"))
        //    {
        //        newString = str.Replace("/style/images/userfiles/file/", Server.MapPath(PathConstants.LetterTemplateImages) + "\\file\\");
        //        sr = new StringReader(newString);
        //    }              
        //    else
        //    {
        //        sr = new StringReader(str);
        //    }

        //    HTMLWorker htmlWork = new HTMLWorker(doc);
        //    doc.Open();

        //    htmlWork.Parse(sr);

        //    doc.Close();

        //    Response.Clear();
        //    Response.AppendHeader("content-disposition", "attachment; filename= Letter1.pdf");
        //    Response.ContentType = "application/pdf";
        //    Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
        //    Response.OutputStream.Flush();

        //    File.Delete(filePath);
        //    Response.End();
        //}

        #endregion

        public string checkCustomer()
        {
            LetterResourceArea letterManager = new LetterResourceArea();
            AM_SP_GetCustomerInfoForLetter_Result customer = letterManager.GetCustomerInfo(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
            if (customer.JointTenancyCount == 2)
            {
                HidCutomer1.Value = customer.CustomerName;
                HidCutomer2.Value = customer.CustomerName2;
                return customer.CustomerName + " & " + customer.CustomerName2;

            }
            else
            {
                HidCutomer1.Value = customer.CustomerName;
                return customer.CustomerName;
            }
        }


    }


}