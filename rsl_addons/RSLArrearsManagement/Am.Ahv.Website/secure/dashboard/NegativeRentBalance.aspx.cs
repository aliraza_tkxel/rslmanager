﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.Dashboard;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Configuration;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;
using System.Drawing;
namespace Am.Ahv.Website.secure.dashboard
{
    public partial class NegativeRentBalance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetRentBalance();
            }
        }

        private void GetRentBalance()
        {
            try
            {
                arrearssummary aar = new arrearssummary();
                int timer = int.Parse(ConfigurationManager.AppSettings["ConnectionTimer"].ToString());
                int count = aar.GetNegativeRentBalance(timer);
                
                string emailBody = "It is to notify you that Negative Rent Balance schedule job has been completed successfully and has <br />identified " + count + " customers." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                if (PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailSubjectNegativeRentBalance, emailBody))
                {
                    SetMessage(UserMessageConstants.successNegativeRentBalance, false);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(0, true, UserMessageConstants.EmailFailureSubjectNegativeRentBalance, UserMessageConstants.FailureEmailBodyNegativeRentBalance);
                    SetMessage(UserMessageConstants.ErrorNegativeRentBalance, true);
                }
                catch (Exception exInner)
                {
                    ExceptionPolicy.HandleException(exInner, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                }                
            }
        }

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Send Email Notification"

        public bool SendEmailNotification(int count, bool success)
        {
            try
            {
                bool isSent = false;
                if (success)
                {
                    string emailBody = "It is to notify you that Negative Rent Balance schedule job has been completed successfully and has <br />identified " + count + " customers." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                    string emailTo = ConfigurationManager.AppSettings["AdministratorEmailAddress"].ToString();
                    while (true)
                    {
                        if (PageBase.SendEmail(emailTo, UserMessageConstants.EmailSubjectNegativeRentBalance, emailBody, true))
                        {
                            isSent = true;
                            break;
                        }
                    }
                    return isSent;
                }
                else
                {
                    string emailTo = ConfigurationManager.AppSettings["AdministratorEmailAddress"].ToString();
                    while (true)
                    {
                        if (PageBase.SendEmail(emailTo, UserMessageConstants.EmailFailureSubjectNegativeRentBalance, UserMessageConstants.FailureEmailBodyNegativeRentBalance, true))
                        {
                            isSent = true;
                            break;
                        }
                    }
                    return isSent;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                return false;
            }

        }

        #endregion

    }
}