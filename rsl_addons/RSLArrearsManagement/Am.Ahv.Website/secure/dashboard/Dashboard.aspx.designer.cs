﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Am.Ahv.Website.secure.dashboard {
    
    
    public partial class Dashboard {
        
        /// <summary>
        /// FirstDetectionCount1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Am.Ahv.Website.controls.dashboard.FirstDetectionCount FirstDetectionCount1;
        
        /// <summary>
        /// overduevacate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Am.Ahv.Website.controls.dashboard.overduevacatedash overduevacate;
        
        /// <summary>
        /// MyCases1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Am.Ahv.Website.controls.dashboard.MyCases MyCases1;
    }
}
