﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Am.Ahv.Website.secure.dashboard.Dashboard" %>
<%@ Register src="~/controls/dashboard/FirstDetectionCount.ascx" tagname="FirstDetectionCount" tagprefix="uc1" %>
<%@ Register src="~/controls/dashboard/MyCases.ascx" tagname="MyCases" tagprefix="uc2" %>
<%@ Register Src="~/controls/dashboard/overduevacatedash.ascx" TagName="overduevcate" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <uc1:FirstDetectionCount ID="FirstDetectionCount1" runat="server" />

    
    <uc1:overduevcate ID="overduevacate" runat="server"></uc1:overduevcate>

    </div>

    <div>
        <uc2:MyCases ID="MyCases1" runat="server" />
    </div>
    

</asp:Content>
