﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;

namespace Am.Ahv.Website.secure.dashboard
{
    public partial class Dashboard : PageBase
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
           
        }
    }
}