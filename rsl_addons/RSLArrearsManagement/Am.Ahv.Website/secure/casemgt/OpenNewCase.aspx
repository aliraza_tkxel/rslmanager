﻿<%@ Page Title="Open New Case :: Arrears Management" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master"
    AutoEventWireup="true" CodeBehind="OpenNewCase.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.OpenNewCase" %>

<%@ Register TagPrefix="uclUserInfo" TagName="userInfo" Src="~/controls/casemgt/UserInfoSummary.ascx" %>
<%@ Register TagPrefix="uclLastPayment" TagName="lastPayment" Src="~/controls/casemgt/ClientLastPayment.ascx" %>
<%@ Register TagPrefix="uclPaymentPlan" TagName="PaymentPlan" Src="~/controls/casemgt/AddRegularPaymentPlan.ascx" %>
<%@ Register TagPrefix="uclFlexiblePaymentPlan" TagName="uclFlexiblePaymentPlan"
    Src="~/controls/casemgt/AddFlexiblePaymentPlan.ascx" %>
<%@ Register TagPrefix="uclCaseSetting" TagName="CaseSettings" Src="~/controls/casemgt/OpenCaseSettings.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="border: solid 2px Black; width: 100%" class="style2">
        <tr>
            <td style="border-bottom: solid 2px Black; padding: 10px" class="style2">
               Initial Case Monitoring: Open New Case
            </td>
            <td align="right" style="border-bottom: solid 2px Black; padding: 10px">
                <asp:Button ID="btnBack" runat="server" Text="< Back" OnClick="btnBack_Click" />
            </td>
        </tr>
        <tr>
            <td class="style2" style="padding: 10px" colspan="2">
                <asp:UpdatePanel ID="updpnlDiv" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="float: left; width: 35%">
                            <div style="margin-left: 15px">
                                <uclUserInfo:userInfo ID="userInforSummary" runat="server" />
                            </div>
                            <div style="margin-left: 15px; margin-top: 10px">
                                <uclLastPayment:lastPayment ID="lastPaymnt" runat="server" />
                            </div>
                            <div style="margin-left: 15px; margin-top: 10px;">
                                <div runat="server" id="regularPaymentDiv" visible="true" style="position: relative;">
                                    <uclPaymentPlan:PaymentPlan ID="regularPaymentPlan" runat="server" />
                                </div>
                                <div runat="server" style="position: relative;" visible="false" id="flexiblePaymentDiv">
                                    <uclFlexiblePaymentPlan:uclFlexiblePaymentPlan ID="flexiblePlan" runat="server" />
                                </div>
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                    <ProgressTemplate>
                                        <span class="Error">Please wait...</span>
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                        <div style="float: left; width: 55%; height: 100%; margin-left: 20px">
                            <uclCaseSetting:CaseSettings ID="caseSettings" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
