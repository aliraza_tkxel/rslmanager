using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.Casemgmt;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Website.pagebase;
using System.Globalization;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BusinessManager.payments;
namespace Am.Ahv.Website.secure.casemgt
{
    public partial class CaseDetails : PageBase
    {
        #region Attributes
        private bool isError { set; get; }
        private int caseid { set; get; }
        bool isException { set; get; }
        private int tenantid { set; get; }
        private int casehistoryid { set; get; }
        public string CaseHistoryPageIndex { get; set; }
        
        #region Business Manager
        Am.Ahv.BusinessManager.Casemgmt.CaseDetails CDetails = null;
        #endregion

        #endregion

        #region Events

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!base.CheckSession())
                {
                    Response.Redirect(PathConstants.BridgePath);
                    //RedirectToLoginPage();
                }
                resetMessage();
                if (!IsPostBack)
                {
                    if (!SetSessionValue())
                    {
                        Response.Redirect(PathConstants.mycaseList);
                    }
                    else
                    {
                        if (Request.QueryString["CaseHistoryId"] != null)
                        {
                            int casehistoryid = Int32.Parse(Request.QueryString["CaseHistoryId"]);
                            PopulateCaseDetails(casehistoryid, true);
                        }
                    }
                }
                if (CheckPaymentPlan())
                {
                    if (CheckPaymentPlanType())
                    {
                        flexiblePaymentDiv.Visible = true;
                        regularPaymentDiv.Visible = false;
                        flexiblePlan.FirstLookPaymentPlan(false);
                        Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Flexible";
                    }
                    else
                    {
                        flexiblePaymentDiv.Visible = false;
                        regularPaymentDiv.Visible = true;
                        regularPaymentPlan.FirstLookPaymentPlan(false);
                        Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Regular";
                    }
                }
                //getHistoryCount();
                flexiblePlan.invokeEvent += delegate(bool flag, string value)
                {
                    if (flag == true)
                    {
                        flexiblePaymentDiv.Visible = false;
                        regularPaymentDiv.Visible = true;
                        updpnlDiv.Update();
                        regularPaymentPlan.ControlLoad(value);
                    }
                    else
                    {
                        flexiblePaymentDiv.Visible = false;
                        regularPaymentDiv.Visible = true;
                        updpnlDiv.Update();
                        regularPaymentPlan.ControlLoad(value);
                        regularPaymentPlan.DisablePaymentPlan();
                        regularPaymentPlan.DisableCancelButton();
                        regularPaymentPlan.ShowUpdateButton();
                    }

                };

                regularPaymentPlan.visibleFlexiblePaymentPlan += delegate(bool flag, string value)
                {
                    if (flag == true)
                    {
                        flexiblePaymentDiv.Visible = true;
                        regularPaymentDiv.Visible = false;
                        updpnlDiv.Update();
                        flexiblePlan.ControlLoad(value);
                        // flexiblePlan.updateControl();
                        //updpnlDiv.Update();
                    }
                    else
                    {
                        flexiblePaymentDiv.Visible = true;
                        regularPaymentDiv.Visible = false;
                        updpnlDiv.Update();
                        flexiblePlan.ControlLoad(value);
                        flexiblePlan.DisablePaymentPlan();
                        flexiblePlan.DisableCancelButton();
                        flexiblePlan.ShowUpdateButton();
                    }
                };
                Session[SessionConstants.PaymentPlanBackPage] = PathConstants.casedetails;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.CDpageload, true);
                }
            }
        }

        #endregion

        #region"Lbtn Click"

        void LBtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> idlist = Session[Am.Ahv.Utilities.constants.SessionConstants.HistoryIdList] as List<int>;
                LinkButton lbtn = sender as LinkButton;
                int HistoryID = int.Parse(lbtn.CommandArgument);
                int CurrentIndex = int.Parse(lbtn.Text);
                if (CurrentIndex - 1 == 0)
                {
                   // lbprevious.Enabled = false;
                   // lbnext.Enabled = true;
                }
                else
                    if (CurrentIndex == idlist.Count)
                    {
                    //    lbnext.Enabled = false;
                     //   lbprevious.Enabled = true;
                    }
                    else
                    {
                      //  lbnext.Enabled = true;
                     //   lbprevious.Enabled = true;
                    }

                ViewState[Am.Ahv.Utilities.constants.ViewStateConstants.CaseHistoryPageIndex] = CurrentIndex;
                PopulateCaseDetails(HistoryID, false);
            }
            catch (IndexOutOfRangeException index)
            {
                isException = true;
                ExceptionPolicy.HandleException(index, "Exception Policy");
            }
            catch (NullReferenceException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.CDLbtn, true);
                }
            }
        }

        #endregion

        #region"Btn Back Click"

        protected void btnback_Click(object sender, EventArgs e)
        {
            //go to casehistory
            Response.Redirect(PathConstants.casehistory, false);
        }

        #endregion

        #region"Btn Update Case Click"

        protected void btnupdatecase_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.updatecase, false);
        }

        #endregion

        #endregion

        #region Utility Functions
                
        #region "Set History List"
        /// <summary>
        /// Persisting the HistoryList IDs inside Session
        /// </summary>
        private void SetHistoryIdList()
        {
            try
            {

                CDetails = new BusinessManager.Casemgmt.CaseDetails();
                List<int> LButtonList = CDetails.GetCaseHistoryList(caseid);
                if (LButtonList != null)
                {
                    Session[Am.Ahv.Utilities.constants.SessionConstants.HistoryIdList] = LButtonList;
                    casehistoryid = LButtonList[0];
                    if (LButtonList.Count == 1)
                    {
                       // lbnext.Enabled = false;
                       // lbprevious.Enabled = false;
                    }
                    else
                    {
                        SetPageIndex();
                    }
                }
                else
                {
                    //setMessage("No More Case Details", false);
                //    lbnext.Enabled = false;
                   // lbprevious.Enabled = false;
                }
            }
            catch (NullReferenceException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.CDsethistorylist, true);
                }
            }
        }
        #endregion
        
        #region PopulateCaseDetails

        /// <summary>
        /// Populate the Case details
        /// </summary>
        /// <param name="CaseID"></param>
        /// <param name="IsCase"></param>

        private void PopulateCaseDetails(int CaseID, bool IsCase)
        {
            try
            {
                CDetails = new BusinessManager.Casemgmt.CaseDetails();
                DateTime noticeIssuedate = new DateTime();
                DateTime noticevaccatedate = new DateTime();
                CultureInfo Ci = new CultureInfo("en-GB");
                AM_CaseHistory cdetails = CDetails.GetCaseHistoryById(CaseID);
                if (cdetails != null)
                {

                    lblinitiateduser.Text = CDetails.GetResourceNameById(cdetails.AM_Resource2.EmployeeId);
                    lblcasemanageruser.Text = CDetails.GetResourceNameById(cdetails.AM_Resource.EmployeeId);
                    lblcaseofficeruser.Text = CDetails.GetResourceNameById(cdetails.AM_Resource1.EmployeeId);
                    lblarrearsstatususer.Text = cdetails.AM_Status.Title;
                    lblstatusreviewdateuser.Text = cdetails.StatusReview.ToString("dd/MM/yyyy");
                    lblarrearsactionuser.Text = cdetails.AM_Action.Title;
                    if (cdetails.IsPaymentPlanIgnored != null)
                    {
                        if (cdetails.IsPaymentPlanIgnored.Value)
                        {
                            lblPaymentPlanUser.Text = "Not Set Up - " + cdetails.PaymentPlanIgnoreReason;
                        }
                        else
                        {
                            if (cdetails.AM_PaymentPlanHistory != null)
                            {
                                cdetails.AM_PaymentPlanHistoryReference.Load();
                                lblPaymentPlanUser.Text = "Set Up - A payment plan was set up on " + cdetails.AM_PaymentPlanHistory.CreatedDate.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                lblPaymentPlanUser.Text = "Not Set Up";
                            }
                        }
                    }
                    lblactionreviewdateuser.Text = cdetails.ActionReviewDate.ToString("dd/MM/yyyy");
                    if (cdetails.RecoveryAmount != null)
                    {
                        lblrecoveryamountuser.Text = cdetails.RecoveryAmount.ToString();
                    }
                    else
                    {
                        lblrecoveryamountuser.Text = string.Empty;
                    }
                    if (!cdetails.AM_LookupCodeReference.IsLoaded)
                    {
                        cdetails.AM_LookupCodeReference.Load();
                    }
                    if (cdetails.OutcomeLookupCodeId != null)
                    {
                        lbloutcomeuser.Text = cdetails.AM_LookupCode.CodeName;
                    }
                    else
                    {
                        lbloutcomeuser.Text = string.Empty;
                    }
                    //Converting DateFormat

                    if (cdetails.NoticeIssueDate != null)
                    {
                        noticeIssuedate = Convert.ToDateTime(cdetails.NoticeIssueDate, Ci);
                        lblnoticeissueuser.Text = noticeIssuedate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        lblnoticeissueuser.Text = string.Empty;
                    }
                    if (cdetails.NoticeExpiryDate != null)
                    {
                        noticevaccatedate = Convert.ToDateTime(cdetails.NoticeExpiryDate, Ci);

                        lblnoticeexpiryuser.Text = noticevaccatedate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        lblnoticeexpiryuser.Text = string.Empty;
                    }
                    if (cdetails.WarrantExpiryDate!= null)
                    {
                        noticevaccatedate = Convert.ToDateTime(cdetails.WarrantExpiryDate, Ci);

                        lblWarrantExpiryUser.Text = noticevaccatedate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        lblWarrantExpiryUser.Text = string.Empty;
                    }
                    if (cdetails.HearingDate != null)
                    {
                        noticevaccatedate = Convert.ToDateTime(cdetails.HearingDate, Ci);

                        lblHearingDateUser.Text = noticevaccatedate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        lblHearingDateUser.Text = string.Empty;
                    }
                    List<AM_Documents> doc = cdetails.AM_Documents.ToList();
                    if (doc != null && doc.Count > 0)
                    {
                        lbldocumentsuser.Text = doc.First().DocumentName;
                    }
                    else
                    {
                        lbldocumentsuser.Text = "No Documents";
                    }
                    lblnotesuser.Text = cdetails.Notes;
                    if (cdetails.IsActionIgnored == true)
                    {
                        lblreasonuser.Text = "Ignore - " + cdetails.ActionIgnoreReason;
                    }
                }
                else
                {
                  
                    setMessage(Am.Ahv.Utilities.constants.UserMessageConstants.NoCaseDetail, true);
                    upcasedetails.Update();
                }



            }
            catch (FormatException formatex)
            {
                isException = true;
                ExceptionPolicy.HandleException(formatex, "Exception Policy");
            }
            catch (NullReferenceException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.CDpopulatecase, true);
                }
            }




            //try
            //{
            //    CDetails = new BusinessManager.Casemgmt.CaseDetails();
            //    DateTime noticeIssuedate = new DateTime();
            //    DateTime noticevaccatedate = new DateTime();
            //    CultureInfo Ci = new CultureInfo("en-GB");
            //    AM_CaseHistory cdetails = CDetails.GetCaseHistoryById(CaseID);
            //    if (cdetails != null)
            //    {

            //        lblinitiateduser.Text = CDetails.GetResourceNameById(cdetails.AM_Resource2.EmployeeId);
            //        lblcasemanageruser.Text = CDetails.GetResourceNameById(cdetails.AM_Resource.EmployeeId);
            //        lblcaseofficeruser.Text = CDetails.GetResourceNameById(cdetails.AM_Resource1.EmployeeId);
            //        lblarrearsstatususer.Text = cdetails.AM_Status.Title;
            //        lblstatusreviewdateuser.Text = cdetails.StatusReview.ToString("dd/MM/yyyy");
            //        lblarrearsactionuser.Text = cdetails.AM_Action.Title;
            //        lblactionreviewdateuser.Text = cdetails.ActionReviewDate.ToString("dd/MM/yyyy");
            //        if (cdetails.RecoveryAmount != null)
            //        {
            //            lblrecoveryamountuser.Text = cdetails.RecoveryAmount.ToString();
            //        }
            //        else
            //        {
            //            lblrecoveryamountuser.Text = string.Empty;
            //        }
            //        if (!cdetails.AM_LookupCodeReference.IsLoaded)
            //        {
            //            cdetails.AM_LookupCodeReference.Load();
            //        }
            //        if (cdetails.OutcomeLookupCodeId != null)
            //        {
            //            lbloutcomeuser.Text = cdetails.AM_LookupCode.CodeName;
            //        }
            //        else
            //        {
            //            lbloutcomeuser.Text = string.Empty;
            //        }
            //        //Converting DateFormat

            //        if (cdetails.NoticeIssueDate != null)
            //        {
            //            noticeIssuedate = Convert.ToDateTime(cdetails.NoticeIssueDate, Ci);
            //            lblnoticeissueuser.Text = noticeIssuedate.ToString("dd/MM/yyyy");
            //        }
            //        else 
            //        {
            //            lblnoticeissueuser.Text = string.Empty;
            //        }
            //        if (cdetails.NoticeExpiryDate != null)
            //        {
            //            noticevaccatedate = Convert.ToDateTime(cdetails.NoticeExpiryDate, Ci);

            //            lblnoticeexpiryuser.Text = noticevaccatedate.ToString("dd/MM/yyyy");
            //        }
            //        else
            //        {
            //            lblnoticeexpiryuser.Text = string.Empty;
            //        }



            //        List<AM_Documents> doc = cdetails.AM_Documents.ToList();
            //        if (doc != null && doc.Count > 0)
            //        {
            //            lbldocumentsuser.Text = doc.First().DocumentName;
            //        }
            //        else
            //        {
            //            lbldocumentsuser.Text = "No Documents";
            //        }
            //        lblnotesuser.Text = cdetails.Notes;
            //        lblreasonuser.Text = cdetails.Reason;
            //    }
            //    else
            //    {
            //      //  lbnext.Enabled = false;
            //       // lbprevious.Enabled = false;
            //        setMessage(Am.Ahv.Utilities.constants.UserMessageConstants.NoCaseDetail, true);
            //        upcasedetails.Update();
            //    }

              

            //}
            //catch (FormatException formatex)
            //{
            //    isException = true;
            //    ExceptionPolicy.HandleException(formatex, "Exception Policy");
            //}
            //catch (NullReferenceException nullref)
            //{
            //    isException = true;
            //    ExceptionPolicy.HandleException(nullref, "Exception Policy");
            //}
            //catch (Exception ex)
            //{
            //    isException = true;
            //    ExceptionPolicy.HandleException(ex, "Exception Policy");
            //}
            //finally
            //{
            //    if (isException)
            //    {
            //        setMessage(UserMessageConstants.CDpopulatecase, true);
            //    }
            //}
        }
        #endregion

        #region SetPageIndex
        private void SetPageIndex()
        {
            try
            {
                ViewState[Am.Ahv.Utilities.constants.ViewStateConstants.CaseHistoryPageIndex] = 1;
                //lbprevious.Enabled = false;
                //lbnext.Enabled = true;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.CDsetpageindex, true);
                }
            }
        }
        #endregion

        #region"Set Session Value"

        private bool SetSessionValue()
        {
            if (Session[SessionConstants.CaseId] != null && Session[SessionConstants.TenantId] != null)
            {
                caseid = int.Parse(Session[SessionConstants.CaseId].ToString());
                tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                return true;
            }
            else
            {
                if (Request.QueryString["caseid"] != null && Request.QueryString["tenid"] != null)
                {
                    if (Validation.CheckIntegerValue(Request.QueryString["caseid"].ToString()) && Validation.CheckIntegerValue(Request.QueryString["tenid"].ToString()))
                    {
                        base.SetCaseId(int.Parse(Request.QueryString["caseid"].ToString()));
                        base.SetTenantId(int.Parse(Request.QueryString["tenid"].ToString()));
                        caseid = int.Parse(Request.QueryString["caseid"].ToString());
                        tenantid = int.Parse(Request.QueryString["tenid"].ToString());
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        #region Error Message

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #endregion

        #region"Check Payment Plan"

        public bool CheckPaymentPlan()
        {
            OpenCase openCase = new OpenCase();
            return openCase.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Check Payment Plan Type"

        public bool CheckPaymentPlanType()
        {
            Payments paymentmanager = new Payments();
            string str = paymentmanager.GetPaymentPlanType(base.GetTenantId());
            if (str.Equals("Flexible"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #endregion

        
    }
}