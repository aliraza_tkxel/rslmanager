﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using System.Data;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BusinessManager.payments;
using System.Drawing;
using System.Drawing.Imaging;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Website.pagebase;
using System.Threading;


namespace Am.Ahv.Website.secure.casemgt
{
    public partial class FindMissedPayments : System.Web.UI.Page
    {
        /// <summary>
        /// Schedule Job -Runs on Demand (Manual)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    ProgressBarModalPopupExtender.Show();
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            SetMessage(UserMessageConstants.FindMissedPaymentsScheduleJobInProgress, false);
            //updpnlJob.Update();
            //ProgressBarModalPopupExtender.Show();
            //Thread.Sleep(7000);
            CheckMissedPayments();
           // Panel1.Visible = false;
        }

        #region"Check Missed Payments"

        public void CheckMissedPayments()
        {
            int count = 0;
            try
            {
                Am.Ahv.BusinessManager.payments.Payments missedPayments = new Am.Ahv.BusinessManager.payments.Payments();
                count = missedPayments.CheckMissedPayment();
                
                string emailBody = "It is to notify you that 'Find Missed Payments' schedule job has been completed successfully and has <br />identified " + count + " missed payments against customers having active payment plan." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                if (PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailSubjectFindMissedPayments, emailBody))
                {
                    SetMessage(UserMessageConstants.FindMissedPaymentsScheduleJobSuccess, false);
                }
            }
            catch (EntityException ee)
            {
                ExceptionPolicy.HandleException(ee, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailFailureSubjectFindMissedPayments, UserMessageConstants.FailureEmailBodyFindMissedPayments);
                    SetMessage(UserMessageConstants.FindMissedPaymentsScheduleJobFailure, true);
                }
                catch (Exception ex)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindMissedPayments, true);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailFailureSubjectFindMissedPayments, UserMessageConstants.FailureEmailBodyFindMissedPayments);
                    SetMessage(UserMessageConstants.FindMissedPaymentsScheduleJobFailure, true);
                }
                catch (Exception exInner)
                {
                    ExceptionPolicy.HandleException(exInner, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindMissedPayments, true);
                }
            }
        }

        #endregion

        /// <summary>
        /// Sets the success or error message. 
        /// </summary>
        /// <param name="str">text/message success or error </param>
        /// <param name="isError"> true or false</param>
        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
    }
}