﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="ViewActivities.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.ViewActivities" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>

<%@ Register src="~/controls/casemgt/UserInfoSummary.ascx" tagname="UserInfoSummary" tagprefix="uc2" %>
<%@ Register src="~/controls/casemgt/ClientLastPayment.ascx" tagname="ClientLastPayment" tagprefix="uc3" %>
<%@ Register src="~/controls/casemgt/AddFlexiblePaymentPlan.ascx" tagname="AddFlexiblePaymentPlan" tagprefix="uc4" %>
<%@ Register src="~/controls/casemgt/AddRegularPaymentPlan.ascx" tagname="AddRegularPaymentPlan" tagprefix="uc5" %>

<%@ Register src="../../controls/casemgt/ViewActivites.ascx" tagname="ViewActivites" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<table style="border: solid 2px Black;" width="100%" cellspacing="1">
    <tr>
        <td colspan="2" width="50%" align="left" style=" margin-right:20px;border-bottom: solid 2px Black;">
            <asp:Label ID="lblCaseDetail" runat="server" Text="Case Detail"></asp:Label>
            <br />
        </td>        
    </tr>
    <tr> 
        <td colspan="2" style="padding:10px" >
            <asp:UpdatePanel ID="updpnlDiv" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="float:left; width:40%; height:100%"> 
                        <div style=" margin-left:15px">
                          <uc2:UserInfoSummary ID="UserInfoSummary1" runat="server" />
                        </div>
                        
                        <div style="  margin-left:15px; margin-top:10px" >
                           <uc3:ClientLastPayment ID="ClientLastPayment1" runat="server" />
                        </div>
                        <div style="margin-left:15px; margin-top:10px;" >
                        
                         <div runat="server" id="regularPaymentDiv" visible="true" style="position:relative;">
                            <uc5:AddRegularPaymentPlan ID="regularPaymentPlan" runat="server" />
                          </div>
                          
                          <div runat="server" style="position:relative;" visible="false" id="flexiblePaymentDiv">                    
                             <uc4:AddFlexiblePaymentPlan ID="AddFlexiblePaymentPlan1" runat="server" />
                         </div>         
                        </div>
                     </div>
                    <div style="float:left; width:55%; height:100%; margin-left:20px">
                        <uc1:ViewActivites ID="ViewActivites1" runat="server" />
                    </div>
                    <asp:UpdateProgress ID="UpdateProgressActivities" runat="server">
                        <ProgressTemplate>
                            <span class="Error">Please wait...</span>
                               <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>  
<%--        <td style="float:left; width:35%; margin-left:15px;">
            <uc2:UserInfoSummary ID="UserInfoSummary1" runat="server" />
            <br />
            <uc3:ClientLastPayment ID="ClientLastPayment1" runat="server" />
            <br />
            <uc4:AddFlexiblePaymentPlan ID="flexiblePlan" Visible="false" runat="server" />
            <br />
            <br />
            <uc5:AddRegularPaymentPlan ID="regularPaymentPlan" Visible="true" runat="server" />
        </td>
        <td style="float:left; width:55%; height:100%; margin-left:20px">
            <uc1:UpdateCase ID="UpdateCase1" runat="server" />
        </td> --%>  
    </tr>
</table>

</asp:Content>
