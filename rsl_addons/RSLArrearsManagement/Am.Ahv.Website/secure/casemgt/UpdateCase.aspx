﻿<%@ Page Title="Update Case :: Arrears Management" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateCase.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.UpdateCase" %>

<%@ Register src="~/controls/casemgt/UpdateCase.ascx" tagname="UpdateCase" tagprefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>

<%@ Register src="~/controls/casemgt/UserInfoSummary.ascx" tagname="UserInfoSummary" tagprefix="uc2" %>
<%@ Register src="~/controls/casemgt/ClientLastPayment.ascx" tagname="ClientLastPayment" tagprefix="uc3" %>
<%@ Register src="~/controls/casemgt/AddFlexiblePaymentPlan.ascx" tagname="AddFlexiblePaymentPlan" tagprefix="uc4" %>
<%@ Register src="~/controls/casemgt/AddRegularPaymentPlan.ascx" tagname="AddRegularPaymentPlan" tagprefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<table cellpadding="5px" width="100%" class="tableClass" >
    <tr>
        <td colspan="2" class="tableHeader" border=2>
            <table width="100%">
                <tr>
                    <td>
                        <asp:Label ID="lblUpdateCase" runat="server" Text="Update Case"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:Button ID="btnBack" runat="server" Text="< Back" onclick="btnBack_Click" />
                       
                    </td>
                </tr>
                </table>
         </td>
    </tr>
    <tr> 
        <td colspan="2" style="padding:10px" >
            <asp:UpdatePanel ID="updpnlDiv" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="float:left; width:40%; height:100%"> 
                        <div style=" margin-left:15px">
                          <uc2:UserInfoSummary ID="UserInfoSummary1" runat="server" />
                        </div>
                        
                        <div style="  margin-left:15px; margin-top:10px" >
                           <uc3:ClientLastPayment ID="ClientLastPayment1" runat="server" />
                        </div>
                        <div style="margin-left:15px; margin-top:10px;" >
                        
                         <div runat="server" id="regularPaymentDiv" visible="true" style="position:relative;">
                            <uc5:AddRegularPaymentPlan ID="regularPaymentPlan" runat="server" />
                          </div>
                          
                          <div runat="server" style="position:relative;" visible="false" id="flexiblePaymentDiv">                    
                             <uc4:AddFlexiblePaymentPlan ID="AddFlexiblePaymentPlan1" runat="server" />
                         </div>         
                        </div>
                     </div>
                    <div style="float:left; width:55%; height:100%; margin-left:20px">
                        <uc1:UpdateCase ID="UpdateCase2" runat="server" />
                    </div>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                        <ProgressTemplate>
                            <span class="Error">Please wait...</span>
                               <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>  
<%--        <td style="float:left; width:35%; margin-left:15px;">
            <uc2:UserInfoSummary ID="UserInfoSummary1" runat="server" />
            <br />
            <uc3:ClientLastPayment ID="ClientLastPayment1" runat="server" />
            <br />
            <uc4:AddFlexiblePaymentPlan ID="flexiblePlan" Visible="false" runat="server" />
            <br />
            <br />
            <uc5:AddRegularPaymentPlan ID="regularPaymentPlan" Visible="true" runat="server" />
        </td>
        <td style="float:left; width:55%; height:100%; margin-left:20px">
            <uc1:UpdateCase ID="UpdateCase1" runat="server" />
        </td> --%>  
    </tr>
</table>

        

</asp:Content>
