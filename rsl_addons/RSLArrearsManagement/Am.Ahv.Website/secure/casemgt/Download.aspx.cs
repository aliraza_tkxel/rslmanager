﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using Am.Ahv.Website.pagebase;

namespace Am.Ahv.Website.secure.casemgt
{
    public partial class Download : PageBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DownloadFile();
        }

        public void DownloadFile()
        {
            if (Request.QueryString["file"] != null)
            {
                string file = Request.QueryString["file"].ToString();
                string[] ext = file.Split('.');
                string type = string.Empty;
                string filePath = string.Empty;
                if (Request.QueryString["p"] != null)
                {
                    filePath = ConfigurationManager.AppSettings["ActivityUploadPath"] + file;
                }
                else
                {
                    filePath = ConfigurationManager.AppSettings["CaseDocumentsUploadPath"] + file;
                }
                Response.Clear();

                switch (ext[1])
                {
                    case "pdf":
                        type = "application/pdf";
                        break;
                    case "doc":
                        type = "application/vnd.ms-word";
                        break;
                    case "docx":
                        type = "application/vnd.ms-word";
                        break;
                    case "xls":
                        type = "application/vnd.ms-excel";
                        break;
                    case "xlsx":
                        type = "application/vnd.ms-excel";
                        break;
                    case "png":
                        type = "application/image/png";
                        break;
                    case "jpg":
                        type = "application/image/jpg";
                        break;

                }

                Response.AppendHeader("content-disposition", "attachment; filename=" + file);
                Response.ContentType = type;
                Response.WriteFile(filePath);
                Response.Flush();
                Response.End();
            }
        }
    }
}