﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Casemgmt;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using System.Text;
using Am.Ahv.Entities;
using System.Globalization;
using System.Configuration;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BusinessManager.payments;
using System.Data;
using System.Collections;
using Am.Ahv.BusinessManager.resource;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Am.Ahv.Website.secure.casemgt
{

    public partial class CaseHistory : PageBase
    {
        #region Attributes
        
        Payments paymentManager = null;

        private bool isError { set; get; }
        bool isException { set; get; }
        private int caseId { set; get; }
        private int tenantId { set; get; }
        protected ImageButton ImageButton1 { set; get; }
        //protected DataList ActivityStandardLetterDataSet{ set; get; }

        #region CaseHistory Manager

        Am.Ahv.BusinessManager.Casemgmt.CaseHistory HistoryManager = null;

        #endregion

        #endregion        

        #region Events

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                if (!base.CheckSession())
                {
                    Response.Redirect(PathConstants.BridgePath);
                    //RedirectToLoginPage();
                }
                resetMessage();

                
                
                if (!IsPostBack)
                {
                           
                    if (CheckPaymentPlanExistance())
                    {
                        if (CheckPaymentPlanType())
                        {
                            flexiblePaymentDiv.Visible = true;
                            regularPaymentDiv.Visible = false;
                            flexiblePlan.FirstLookPaymentPlan(false);
                            updpnlPaymentPlan.Update();
                            Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Flexible";
                        }
                        else
                        {
                            
                            flexiblePaymentDiv.Visible = false;
                            regularPaymentDiv.Visible = true;
                            regularPaymentPlan.FirstLookPaymentPlan(false);
                            updpnlPaymentPlan.Update();
                            Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Regular";
                            
                        }
                    }
                    if (Request.QueryString["cmd"] != null && Request.QueryString["tid"] != null &&
                        Request.QueryString["cid"] != null)
                    {
                        if (Request.QueryString["cmd"] == "scase")
                        {
                            if (Validation.CheckIntegerValue(Request.QueryString["cid"]))
                            {
                                base.SetCaseId(Convert.ToInt32(Request.QueryString["cid"]));
                            }
                            if (Validation.CheckIntegerValue(Request.QueryString["tid"]))
                            {
                                base.SetTenantId(Convert.ToInt32(Request.QueryString["tid"]));
                            }
                        }
                    }

                    if (SetSessionValues())
                    {
                        if (Request.QueryString["cmd"] != null)
                        {
                            if (Request.QueryString["cmd"] == "OpenCase")
                            {
                                LoadGrid();
                                LoadNotesGrid();
                                //LoadModalPopup();
                                ClearSupressCaseFields();
                                ClearIgnoreCaseFields();
                            }
                        }
                        else
                        {
                            LoadGrid();
                            LoadNotesGrid();
                            ClearSupressCaseFields();
                            ClearIgnoreCaseFields();
                            if (!CheckVisitedCase())
                            {
                                LoadModalPopup();
                            }
                           
                        }
                    }
                    else
                    {
                        Response.Redirect(PathConstants.mycaseList);
                    }
                    
                    if (Request.QueryString["cmd"] == "Success")
                    {
                        LoadGrid();
                        LoadNotesGrid();
                        //LoadModalPopup();
                        ClearSupressCaseFields();
                        ClearIgnoreCaseFields();
                        SetMessage(UserMessageConstants.successUpdateCase, false);
                    }
                    CheckPaymentPlan();
                }
                flexiblePlan.invokeEvent += delegate(bool flag, string value)
                {
                    if (flag == true)
                    {
                        flexiblePaymentDiv.Visible = false;
                        regularPaymentDiv.Visible = true;
                        regularPaymentPlan.ControlLoad(value);
                        updpnlPaymentPlan.Update();
                    }
                    else
                    {
                        flexiblePaymentDiv.Visible = false;
                        regularPaymentDiv.Visible = true;
                        regularPaymentPlan.ControlLoad(value);
                        regularPaymentPlan.DisablePaymentPlan();
                        regularPaymentPlan.DisableCancelButton();
                        regularPaymentPlan.ShowUpdateButton();
                        updpnlPaymentPlan.Update();
                    }

                };

                regularPaymentPlan.visibleFlexiblePaymentPlan += delegate(bool flag, string value)
                {
                    if (flag == true)
                    {
                        flexiblePaymentDiv.Visible = true;
                        regularPaymentDiv.Visible = false;
                        flexiblePlan.ControlLoad(value);
                        updpnlPaymentPlan.Update();
                    }
                    else
                    {
                        flexiblePaymentDiv.Visible = true;
                        regularPaymentDiv.Visible = false;
                        flexiblePlan.ControlLoad(value);
                        flexiblePlan.DisablePaymentPlan();
                        flexiblePlan.DisableCancelButton();
                        flexiblePlan.ShowUpdateButton();
                        updpnlPaymentPlan.Update();
                    }
                };
                Session[SessionConstants.PaymentPlanBackPage] = PathConstants.casehistory;
            }
            catch (Exception ex)
            {
                isException = true;
                //Response.Write(ex);
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHpageload, true);
                }
            }
        }

        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#00FF00';");
            }
        }

        #endregion

        #region SL Image Button Click
        protected void imgbuttonsl_Click(object sender, ImageClickEventArgs e)
        {
            //Code Goes here
            try
            {
                ImageButton btn = sender as ImageButton;
                if (btn.ImageUrl != string.Empty)
                {
                    int ID = Convert.ToInt32(btn.CommandArgument);
                    ViewState[ViewStateConstants.CaseLetterId] = Session[SessionConstants.CaseId];

                    Session[SessionConstants.CaseHistoryId] = ID;
                    LoadLetters(ID);
                    LoadDocs(ID);
                    this.PopupExtenderdocuments.Show();
                }
                else
                {
                    btn.Visible = false;
                }
                uphistorydetails.Update();

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHimagebutton, true);
                }
            }


        }
        #endregion        

        #region SL Activity Image Button Click
        protected void imgbuttonDocument_Click(object sender, ImageClickEventArgs e)
        {
            //Code Goes here
            try
            {
                ImageButton btn = sender as ImageButton;
                if (btn.ImageUrl != string.Empty)
                {
                    int ID = Convert.ToInt32(btn.CommandArgument);                    
                    
                    ViewState[ViewStateConstants.LetterActivityId] =  ID;
                    LoadActivityLetters(ID);
                    LoadActivityDocs(ID);
                    this.PopupExtenderActivitydocuments.Show();
                }
                else
                {
                    btn.Visible = false;
                }
                //uphistorydetails.Update();

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHimagebutton, true);
                }
            }


        }
        #endregion

        #region Image Button Click
        protected void imgbutton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton btn = sender as ImageButton;
                if (btn.ImageUrl != string.Empty)
                {
                    int ID = Convert.ToInt32(btn.CommandArgument);                    
                    LoadDocs(ID);
                    LoadLetters(ID);
                    this.PopupExtenderdocuments.Show();
                }
                else
                {
                    btn.Visible = false;
                }
                uphistorydetails.Update();
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHimagebutton, true);
                }
            }
        }
        #endregion

        #region"Btn Print Case Click"

        protected void btnprintcase_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;

            Session.Remove(SessionConstants.GlobalCaseHistoryList);
            //Response.Redirect(PathConstants.caseDetailsReportPath);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"window.open('" + PathConstants.caseDetailsReportPathPopup + "','New');");
            sb.Append(@"</script>");
            ScriptManager.RegisterStartupScript(btn, this.GetType(), "NewWindow", sb.ToString(), false);
        }

        #endregion

        #region"Btn Update Case Click"

        protected void btnupdatecase_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.updatecase);
        }


        #endregion

        #region"Btn View Activities Click"

        protected void btnviewactivities_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.viewActivitiesPath, false);
        }

        #endregion

        #region"Btn Payment Plan Click"

        protected void btnPaymentPlan_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.regularPaymentReport, false);
        }

        #endregion

        #region"PaymentPlanFlag_Click"

        protected void PaymentPlanFlag_Click(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;

            //string ImageType = btn.ImageUrl;
            //if(ImageType.Contains("im_greenbutton"))
            //{
            if (btn.CommandArgument != "")
            {
                
                //Response.Redirect(PathConstants.flexiblePaymentReport + "?PaymentPlanHistoryId=" + btn.CommandArgument);

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script language='javascript'>");
                sb.Append(@"window.open('" + PathConstants.flexiblePaymentReportPopup + "?PaymentPlanHistoryId=" + btn.CommandArgument + "','New');");
                sb.Append(@"</script>");
                ScriptManager.RegisterStartupScript(btn, this.GetType(), "NewWindow", sb.ToString(), false);
            }
            //}



        }
        #endregion

        #region"Btn Back Click"
        protected void btnback_Click(object sender, EventArgs e)
        {
            //go to my caselist
            Response.Redirect(PathConstants.mycaselistef, false);
        }

        #endregion

        #region"Btn Add Activity Click"
        protected void btnaddactivity_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.addCaseActivityPath, false);
        }
        #endregion

        #region CaseNotes View Button Click Event
        protected void btnview_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            Response.Redirect(PathConstants.viewActivityPath + btn.CommandArgument, false);
        }
        #endregion

        #region"Lbtn Document Name Click"

        protected void lbtnDocumentName_Click(object sender, EventArgs e)
        {
            //DataListItem item = ((LinkButton)sender).Parent as DataListItem;
            //int index = item.ItemIndex;
            //LinkButton lbtn = (LinkButton)dataSetUploadedDocuments.Items[index].FindControl("lbtnDocumentName");

            ////string fileName = "test.xls";
            ////string filePath = Server.MapPath(ConfigurationManager.AppSettings["CaseDocumentsUploadPath"]);
            //string filePath = ConfigurationManager.AppSettings["CaseDocumentsUploadPath"] + lbtn.Text;
            //Response.Clear();

            //Response.AppendHeader("content-disposition", "attachment; filename=" + lbtn.Text);
            //Response.ContentType = "application/pdf";
            //Response.WriteFile(filePath);
            //Response.Flush();
            //Response.End();
        }

        #endregion

        #region Modal Popup Events

        #region Ignore Case

        protected void btnignoresave_Click(object sender, EventArgs e)
        {
            try
            {
                SetSessionValues();
                HistoryManager = new BusinessManager.Casemgmt.CaseHistory();
                //Boolean flag = HistoryManager.ActionIgnoreCount(caseId);
                //if (flag)
                {
                    if (txtignorereason.Text != string.Empty)
                    {
                        Boolean flag1 = HistoryManager.IgnoreCase(caseId, txtignorereason.Text, Convert.ToInt32(Session[SessionConstants.NextStatusIdCaseSummary]), Convert.ToInt32(Session[SessionConstants.NextActionIdCaseSummary]), true);
                        if (flag1)
                        {
                            upalertpanel_ModalPopupExtender1.Hide();
                        }
                    }
                    else
                    {
                        lblignorerrormsg.Text = "Please enter reason";
                        upalertpanel.Update();
                        upalertpanel_ModalPopupExtender.Show();
                        upalertpanel_ModalPopupExtender1.Show();
                    }
                }
                //else
                //{
                //    isError = true;

                //}
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
            finally
            {
                if (isException)
                {
                    upalertpanel_ModalPopupExtender.Hide();
                    upalertpanel_ModalPopupExtender1.Hide();
                    SetMessage(UserMessageConstants.CHignoresavebtn, true);
                }
            }

        }

        #endregion

        #region Supress Case

        protected void btnsupresssave_Click(object sender, EventArgs e)
        {
            HistoryManager = new BusinessManager.Casemgmt.CaseHistory();
            try
            {
                SetSessionValues();
                if (txtsupressreason.Text != string.Empty && txtreviewdate.Text != string.Empty)
                {
                    if (Am.Ahv.Utilities.utitility_classes.DateOperations.isValidUkDate(txtreviewdate.Text))
                    {
                        CultureInfo cultEnGb = new CultureInfo("en-GB");
                        CultureInfo cultEnUs = new CultureInfo("en-US");
                        DateTime date = Convert.ToDateTime(txtreviewdate.Text, cultEnGb);
                        date = Convert.ToDateTime(date, cultEnUs);
                        DateTime nextdate = Am.Ahv.Utilities.utitility_classes.DateOperations.AddDate(3, "Months", DateTime.Now);

                        int i = DateTime.Compare(date, nextdate);
                        // pagebase.PageBase baseobject= new PageBase();
                        if (i <= 0)
                        {
                            Boolean flag = HistoryManager.SupressCase(caseId, txtsupressreason.Text, date, base.GetLoggedInUserID());
                            if (flag)
                            {
                                upmodalextendersupresscase.Hide();
                            }
                        }
                        else
                        {
                            this.lblsupressmessage.Text = "Please Select date within three months";
                            upalertpanel.Update();
                            upalertpanel_ModalPopupExtender.Show();
                            upmodalextendersupresscase.Show();
                        }
                    }
                    else
                    {
                        this.lblsupressmessage.Text = " Enter Valid Date";
                        upalertpanel.Update();
                        upalertpanel_ModalPopupExtender.Show();
                        upmodalextendersupresscase.Show();
                    }
                }
                else
                {
                    this.lblsupressmessage.Text = "Please Enter Reason and Select a date within three months";
                    upalertpanel.Update();
                    upalertpanel_ModalPopupExtender.Show();
                    upmodalextendersupresscase.Show();
                }

            }

            catch (FormatException format)
            {
                isException = true;
                ExceptionPolicy.HandleException(format, "Exception Policy");
                throw format;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    upalertpanel_ModalPopupExtender.Hide();
                    upmodalextendersupresscase.Hide();
                    SetMessage(UserMessageConstants.CHsupressbtn, true);
                }
            }
        }

        #endregion

        #region Supress Case Cancel Button

        protected void btnsupresscancel_Click(object sender, EventArgs e)
        {
            upalertpanel.Update();
            upalertpanel_ModalPopupExtender.Show();
            upmodalextendersupresscase.Show();
        }

        #endregion

        #region Ignore Case Cancel Button

        protected void btnignorecancel_Click(object sender, EventArgs e)
        {
            upalertpanel.Update();
            upalertpanel_ModalPopupExtender.Show();
            upalertpanel_ModalPopupExtender1.Show();
        }

        #endregion

        #region View Case Button

        #region"Btn View Case Click"

        protected void btnviewcase_Click(object sender, EventArgs e)
        {
            //if (Session[SessionConstants.CaseId] != null && Session[SessionConstants.TenantId] != null)
            //{
            //    ////Session.Remove(SessionConstants.CaseId);
            //    ////Session.Remove(SessionConstants.TenantId);
            //    //Session[SessionConstants.CaseId] = caseId;
            //    //Session[SessionConstants.TenantId] = tenantId;
            //    Response.Redirect(PathConstants.casedetails);
            //}
            //else
            //{
            //    Session[SessionConstants.CaseId] = caseId;
            //    Session[SessionConstants.TenantId] = tenantId;
            //    Response.Redirect(PathConstants.casedetails);
            //}

            GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;
            Button btnviewcase = new Button();
            btnviewcase = (Button)gvhistorydetails.Rows[index].FindControl("btnviewcase");

            Response.Redirect(PathConstants.casedetails + "?CaseHistoryId=" + btnviewcase.CommandArgument);
        }

        #endregion

        #region"Show Notes"

        protected void ShowNotes(object sender, EventArgs e)
        {
            //GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
            //int index = gvr.RowIndex;
            //Button btnDelete = new Button();
            //btnDelete = (Button)pgvOutcomes.Rows[index].FindControl("btnDelete");
            ////Session[SessionConstants.EditOutcomeId] = btnDelete.CommandArgument;
            ////invokeEvent(true, true);
            //if (IsOutcomeFree(btnDelete.CommandArgument))
            //{
            //    DeleteOutcome(btnDelete.CommandArgument);
            //    PopulateGridView();
            //    SetPagingLabels();
            //}
            //else
            //{
            //    SetMessage("Couldn't be deleted, it is used by Case.", true);
            //}

        }

        #endregion

        #region record Action
        protected void btnrecordaction_Click(object sender, EventArgs e)
        {
            try
            {
                SetSessionValues();
                // HistoryManager = new BusinessManager.Casemgmt.CaseHistory();
                //Boolean flag = HistoryManager.RecordActionCount(caseId);
                //if (flag)
                {
                    Response.Redirect(PathConstants.updatecase + "?cmd=RecordAction", false);
                }
                //else
                //{
                //  isError = true;
                //}
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHexceptionactioncount, true);
                    upalertpanel_ModalPopupExtender.Hide();
                    upalertpanel_ModalPopupExtender.Hide();
                }
            }
        }
        #endregion

        #endregion

        #endregion

        #endregion

        #region Utility Functions

        #region Load Letters
        private void LoadLetters(int CaseHistoryId)
        {
            HistoryManager = new BusinessManager.Casemgmt.CaseHistory();
            List<AM_CaseAndStandardLetter> caslist = HistoryManager.GetStandardLetterByCaseHistoryId(CaseHistoryId);
            if (caslist != null)
            {
                caslist = RemoveNull(caslist);
                this.sldatalist.DataSource = caslist;
                //this.lboxstandardletters.DataTextField = "AM_StandardLetters.Title";
                //this.lboxstandardletters.DataValueField = "StandardLetterId";
                this.sldatalist.DataBind();
                LinkStandardLetters();
                lblnostandardletters.Visible = false;
                slpanel.Visible = true;
            }
            else
            {
                lblnostandardletters.Text = "No Standard Letters Attached";
                lblnostandardletters.Visible = true;
                slpanel.Visible = false;
            }
            upmodalpanel.Update();

        }
        #endregion'

        #region Load Activity Letters
        private void LoadActivityLetters(int ActivityId)
        {
            BusinessManager.letters.LetterResourceArea LRA = new BusinessManager.letters.LetterResourceArea();

            List<AM_ActivityAndStandardLetters> Letterlist = LRA.GetStandardLetterByActivityId(ActivityId);


            if (Letterlist != null && Letterlist.Count != 0)
            {
                RemoveNull(Letterlist);
                this.ActivityStandardLetterDataSet.DataSource = Letterlist;
                this.ActivityStandardLetterDataSet.DataBind();
                LinkActivityStandardLetters();
                lblnoActivitystandardletters.Visible = false;
                pnlActivityStandardLetters.Visible = true;
            }
            else
            {
                lblnoActivitystandardletters.Text = "No Standard Letters Attached";
                lblnoActivitystandardletters.Visible = true;
                pnlActivityStandardLetters.Visible = false;
            }
            updpnlActivityModalPopUp.Update();

        }
        #endregion

        #region Load Activity Panel

        private void LoadActivityDocs(int ActivityID)
        {
            Am.Ahv.BusinessManager.documents.Document DocManager = new Am.Ahv.BusinessManager.documents.Document();
            List<Am.Ahv.Entities.AM_Documents> doclist = DocManager.GetDocumentsByActivityId(ActivityID);
            if (doclist != null && doclist.Count != 0)
            {
                ActivityDataSet.DataSource = doclist;
                ActivityDataSet.DataBind();
                ActivityDocumentsScriptBuilder();
                lblNoActivityDocs.Visible = false;
                pnlActivityDataSet.Visible = true;
            }
            else
            {
                lblNoActivityDocs.Text = "No Document Attached";
                lblNoActivityDocs.Visible = true;
                pnlActivityDataSet.Visible = false;
            }
            updpnlActivityModalPopUp.Update();
        }
        #endregion

        #region"Remove Null"

        public List<AM_CaseAndStandardLetter> RemoveNull(List<AM_CaseAndStandardLetter> caseLetter)
        {
            if (caseLetter != null)
            {
                for (int i = 0; i < caseLetter.Count; i++ )
                {
                    if (caseLetter[i].StandardLetterHistoryId == null)
                    {
                        caseLetter[i].StandardLetterHistoryId = 0;
                    }
                }
            }
            return caseLetter;
        }

        public List<AM_ActivityAndStandardLetters> RemoveNull(List<AM_ActivityAndStandardLetters> activityLetter)
        {
            if (activityLetter != null)
            {
                for (int i = 0; i < activityLetter.Count; i++)
                {
                    if (activityLetter[i].StandardLetterHistoryId == null)
                    {
                        activityLetter[i].StandardLetterHistoryId = 0;
                    }
                }
            }
            return activityLetter;
        }
        #endregion

        #region"Make Standard Letter Id"

        public string MakeStandardLetterId(string StandardLetterHistoryId, string StandardLetterId)
        {
            if (StandardLetterHistoryId != string.Empty || StandardLetterHistoryId != null || StandardLetterHistoryId != "0")
            {
                return StandardLetterId + ";" + StandardLetterHistoryId;
            }
            else
            {
                return StandardLetterId + ";%";
            }
        }

        #endregion

        #region LoadGrid
        private void LoadGrid()
        {
            HistoryManager = new Am.Ahv.BusinessManager.Casemgmt.CaseHistory();
            Am.Ahv.BusinessManager.Casemgmt.CaseDetails caseDetailsManager = new Am.Ahv.BusinessManager.Casemgmt.CaseDetails();
            
            try
            {
                SetSessionValues();
            AM_Case currentCase = caseDetailsManager.GetCaseById(caseId);

                if (currentCase != null)
                {
                    base.SetIsInvidualLetterReq((int)currentCase.JointTenantLetterRequired);
                    base.SetCustomerContactAddressID((int)currentCase.AdditionalContactID);
                    if (currentCase.ThirdPartyContactID > 0)
                    {
                        BusinessManager.letters.LetterResourceArea LRA = new BusinessManager.letters.LetterResourceArea();
                        AM_StandardLetters_ThirdPartyContacts thirdPartyContact = LRA.getThirdPartyContactDetails((int)currentCase.ThirdPartyContactID);
                        List<string> contactDetails = new List<string>();
                        contactDetails.Add(thirdPartyContact.ContactName);
                        contactDetails.Add(thirdPartyContact.Address1);
                        contactDetails.Add(thirdPartyContact.Address2);
                        contactDetails.Add(thirdPartyContact.Address3);
                        contactDetails.Add(thirdPartyContact.TownCity);
                        contactDetails.Add(thirdPartyContact.PostCode);
                        base.SetManualAddreesData(contactDetails);
                        base.SetIsManualAddress(1);
                    }
                    else
                        base.SetIsManualAddress(0);
                }

                Am.Ahv.BusinessManager.reports.CaseDetailsReport CReport = new BusinessManager.reports.CaseDetailsReport();
                List<AM_CaseHistory> Chistory = CReport.GetCaseHistoryByCaseId(base.GetCaseId());
                if (Chistory != null)
                {
                    Chistory = LoadCaseActionStatusHistory(Chistory);
                    Chistory = LoadUserNames(Chistory);
                    if (Chistory[0].IsActive == false)
                    {
                        divCaseClose.Visible = true;
                    }
                    else
                    {
                        divCaseClose.Visible = false;
                    }
                }
                List<AM_CaseHistory> NewList1 = new List<AM_CaseHistory>();
                for (int i = 0; i < Chistory.Count; i++)
                {
                    AM_CaseHistory newObj4 = Utilities.utitility_classes.ExtensionMethods.DeepClone<AM_CaseHistory>(Chistory[i]);
                    if (i + 1 < Chistory.Count)
                    {
                        if (Chistory[i].ActionId == Chistory[i + 1].ActionId)
                        {
                            if (Chistory[i].IsPaymentPlan == true && Chistory[i].IsCaseUpdated == false)
                            {
                                if ((Chistory[i].IsPaymentPlanUpdated == null || Chistory[i].IsPaymentPlanUpdated == false) && (Chistory[i + 1].IsPaymentPlanUpdated == null || Chistory[i + 1].IsPaymentPlanUpdated == false))
                                {
                                    newObj4.AM_ActionHistory.Title = "Payment Plan";
                                    newObj4.PaymentPlanIgnoreReason = "Created";
                                    newObj4.IsDocumentAttached = false;
                                    newObj4.IsDocumentUpload = false;
                                    newObj4.IsSuppressed = false;
                                    newObj4.IsActive = false;
                                    NewList1.Add(newObj4);
                                }
                                else
                                {
                                    NewList1.Add(Chistory[i]);
                                }
                            }
                            else
                            {
                                NewList1.Add(Chistory[i]);
                               
                            }
                        }
                        else
                        {
                            NewList1.Add(Chistory[i]);
                        }
                    }
                    else
                    {
                        NewList1.Add(Chistory[i]);
                    }
                }

                List<AM_CaseHistory> NewList = new List<AM_CaseHistory>();
                foreach (AM_CaseHistory obj in NewList1)
                {
                    if (obj.IsPaymentPlanIgnored == true)
                    {
                        // check payment plan ignore block.
                        AM_CaseHistory newObj = Utilities.utitility_classes.ExtensionMethods.DeepClone<AM_CaseHistory>(obj);
                        newObj.PaymentPlanIgnoreReason = "Ignored";
                        if (newObj.PaymentPlanHistoryId == null)
                        {
                            newObj.PaymentPlanHistoryId = 0;
                        }
                        if (newObj.PaymentPlanId == null)
                        {
                            newObj.PaymentPlanId = 0;
                        }
                        if (newObj.IsPaymentPlan == null)
                        {
                            newObj.IsPaymentPlan = false;
                        }
                        newObj.AM_ActionHistory.Title = "Payment Plan";
                        newObj.IsDocumentAttached = false;
                        newObj.IsDocumentUpload = false;
                        newObj.IsSuppressed = false;
                        newObj.IsActive = false;
                        NewList.Add(newObj);
                    }

                    if (obj.IsPaymentPlanClose == true)
                    {

                        // check payment plan ignore block.
                        AM_CaseHistory newObj = Utilities.utitility_classes.ExtensionMethods.DeepClone<AM_CaseHistory>(obj);
                        newObj.PaymentPlanIgnoreReason = "Closed";
                      
                        if (newObj.PaymentPlanHistoryId == null)
                        {
                            newObj.PaymentPlanHistoryId = 0;
                        }
                        if (newObj.PaymentPlanId == null)
                        {
                            newObj.PaymentPlanId = 0;
                        }
                        if (newObj.IsPaymentPlan == null)
                        {
                            newObj.IsPaymentPlan = false;
                        }
                        newObj.AM_ActionHistory.Title = "Payment Plan";
                        newObj.IsDocumentAttached = false;
                        newObj.IsDocumentUpload = false;
                        newObj.IsSuppressed = false;
                        newObj.IsActive = false;
                        NewList.Add(newObj);
                    }

                    else if (obj.IsActionIgnored == true)
                    {
                        // check action ignore block.
                        AM_CaseHistory newObj1 = Utilities.utitility_classes.ExtensionMethods.DeepClone<AM_CaseHistory>(obj);
                        newObj1.PaymentPlanIgnoreReason = "Ignored";
                        if (newObj1.PaymentPlanHistoryId == null)
                        {
                            newObj1.PaymentPlanHistoryId = 0;
                        }
                        if (newObj1.PaymentPlanId == null)
                        {
                            newObj1.PaymentPlanId = 0;
                        }
                        if (newObj1.IsPaymentPlan == null)
                        {
                            newObj1.IsPaymentPlan = false;
                        }
                        newObj1.IsPaymentPlan = false;
                        newObj1.IsDocumentAttached = false;
                        newObj1.IsDocumentUpload = false;
                        NewList.Add(newObj1);
                    }
                    else if (obj.IsPaymentPlanUpdated == true)
                    {
                        // payment plan update block.
                        //Updated by: Umair
                        //Update Date:05/01/2011
                        //if (obj.IsPaymentPlanClose==false) 
                        //{
                            AM_CaseHistory newObj3 = Utilities.utitility_classes.ExtensionMethods.DeepClone<AM_CaseHistory>(obj);
                            newObj3.PaymentPlanIgnoreReason = "Updated";
                            newObj3.AM_ActionHistory.Title = "Payment Plan";
                            newObj3.IsDocumentAttached = false;
                            newObj3.IsDocumentUpload = false;
                            newObj3.IsActive = false;
                            //newObj3.IsPaymentPlan = true;
                            NewList.Add(newObj3);
                        //}
                        //else
                        //{
                        //    AM_CaseHistory newObj = Utilities.utitility_classes.ExtensionMethods.DeepClone<AM_CaseHistory>(obj);
                        //    newObj.PaymentPlanIgnoreReason = "Closed";
                        //    newObj.AM_ActionHistory.Title = "Payment Plan";
                        //    newObj.IsDocumentAttached = false;
                        //    newObj.IsDocumentUpload = false;
                        //    newObj.IsSuppressed = false;
                        //    newObj.IsActive = false;
                        //    NewList.Add(newObj);
                        //}


                        //update end 


                        
                    }
                    else
                    {
                        if (obj.PaymentPlanHistoryId == null)
                        {
                            obj.PaymentPlanHistoryId = 0;
                        }
                        if (obj.PaymentPlanId == null)
                        {
                            obj.PaymentPlanId = 0;
                        }
                        //if (obj.IsPaymentPlan == null)
                        //{
                        //    obj.IsPaymentPlan = false;
                        //}
                        if (obj.PaymentPlanIgnoreReason != "Created")
                        {
                            obj.PaymentPlanIgnoreReason = "";
                            obj.IsPaymentPlan = false;
                            obj.PaymentPlanIgnoreReason = obj.ActionReviewDate.ToString("dd/MM/yyyy");
                        }

                        //obj.IsDocumentAttached = false;
                        //obj.IsDocumentUpload = false;
                        
                        NewList.Add(obj);
                    }
                }

                this.gvhistorydetails.DataSource = NewList;//Chistory;//HistoryManager.GetCaseHistory(caseId);
                this.gvhistorydetails.DataBind();
            }
            catch (NullReferenceException nullref)
            {
                isException = true;
                // Response.Write(nullref);
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (ArgumentException argument)
            {
                isException = true;
                // Response.Write(argument);
                ExceptionPolicy.HandleException(argument, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                //Response.Write(ex);
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHloadgrid, true);
                }
            }
        }

        #endregion

        #region Lazy Loading
        private List<AM_CaseHistory> LoadCaseActionStatusHistory(List<AM_CaseHistory> Chistory)
        {
            foreach (AM_CaseHistory item in Chistory)
            {
                item.AM_ActionHistoryReference.Load();
                item.AM_StatusHistoryReference.Load();
                item.AM_Resource2Reference.Load();
            }
            return Chistory;
        }

        private List<AM_CaseHistory> LoadUserNames(List<AM_CaseHistory> Chistory)
        {
            Users user = new Users();
            foreach (AM_CaseHistory item in Chistory)
            {
                item.AM_Resource2Reference.Load();
                // string emp = user.GetEmployeeNameByUserId(item.AM_Resource2.EmployeeId);
                string emp = user.GetEmployeeName(item.AM_Resource2.EmployeeId);
                if (emp != null)
                {
                    if (emp != "")
                    {
                        //string[] str = emp.Split(',');
                        //if (str.Count() > 0)
                        //{
                        //    if (str.Count() == 2)
                        //    {
                        //        item.ActionIgnoreReason = str[0][0].ToString() + str[1][0].ToString();
                        //    }
                        //    else if (str.Count() == 1)
                        //    {
                        //        item.ActionIgnoreReason = str[0][0].ToString();
                        //    }
                        //}
                        ///////////////////////////////////
                        //if (emp.Contains(' '))
                        //{
                        //    item.ActionIgnoreReason = emp[0].ToString() + emp[emp.IndexOf(' ') + 1];
                        //}
                        //else
                        //{
                        //    item.ActionIgnoreReason = emp[0].ToString();
                        //}

                        if (emp.IndexOf(' ') + 1 < emp.Length)
                        {
                            item.ActionIgnoreReason = emp[0].ToString() + emp[emp.IndexOf(' ') + 1];
                        }
                        else
                        {
                            item.ActionIgnoreReason = emp[0].ToString();
                        }
                    }
                }
            }
            return Chistory;
        }

        //private List<AM_CaseHistory> LoadUserNames(List<AM_CaseHistory> Chistory)
        //{
        //    Users user = new Users();
        //    foreach (AM_CaseHistory item in Chistory)
        //    {
        //        item.AM_Resource2Reference.Load();
        //        string emp = user.GetEmployeeName(item.AM_Resource2.EmployeeId);
        //        if (emp != null && emp != "")
        //        {
        //            item.ActionIgnoreReason = emp[0].ToString() + emp[emp.IndexOf(' ') + 1];
        //        }
        //    }
        //    return Chistory;
        //}

        #endregion

        #region LoadNotesGrid
        private void LoadNotesGrid()
        {
            SetSessionValues();
            HistoryManager = new BusinessManager.Casemgmt.CaseHistory();
            gvcasenotes.DataSource = HistoryManager.GetActivityByCase(caseId);
            gvcasenotes.DataBind();
        }
        #endregion

        #region FormatDateString
        public string FormatDate(string value)
        {
            DateTime date = Convert.ToDateTime(value);
            return date.ToString("dd/MM/yyyy");
        }
        #endregion

        #region DateComparison

        public string Compare(string incomingdate)
        {
            try
            {
                DateTime indate = Convert.ToDateTime(incomingdate);
                int i = indate.CompareTo(DateTime.Now);
                if (i < 0)
                {
                    return "overdue";
                }
                else
                {
                    return indate.ToString("dd/MM/yyyy");
                }
            }
            catch (FormatException formatex)
            {
                isException = true;
                ExceptionPolicy.HandleException(formatex, "Exception Policy");
                return null;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return null;
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHdatacomparison, true);
                }
            }
        }

        #endregion

        #region LoadDocumentsInsidePanel
        private void LoadDocs(int CaseHistoryID)
        {
            HistoryManager = new Am.Ahv.BusinessManager.Casemgmt.CaseHistory();
            List<Am.Ahv.Entities.AM_Documents> doclist = HistoryManager.GetDocumentsByHistory(CaseHistoryID);
            if (doclist != null)
            {
                dataSetUploadedDocuments.DataSource = doclist;
                dataSetUploadedDocuments.DataBind();
                ScriptBuilder();
                lblnodocs.Visible = false;
                pnlDataSet.Visible = true;
            }
            else
            {
                lblnodocs.Text = "No Document Attached";
                lblnodocs.Visible = true;
                pnlDataSet.Visible = false;
            }
            upmodalpanel.Update();
        }
        #endregion

        #region "Load Case Notes Grid"
        private void LoadCaseNotesGrid(int CaseHistoryId)
        {

            HistoryManager = new Am.Ahv.BusinessManager.Casemgmt.CaseHistory();
            List<AM_CaseHistory> casehistory = HistoryManager.GetCaseHistoryNotes(CaseHistoryId);
            if (casehistory != null)
            {
                gvcasenotes.DataSource = casehistory;
                gvcasenotes.DataBind();
                uphistorydetails.Update();
            }

        }
        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region Modal Pop Load

        private void LoadModalPopup()
        {
            HistoryManager = new BusinessManager.Casemgmt.CaseHistory();
            Am.Ahv.BusinessManager.Casemgmt.CaseDetails caseDetailsManager = new Am.Ahv.BusinessManager.Casemgmt.CaseDetails();
            SetSessionValues();
            AM_Case currentCase = caseDetailsManager.GetCaseById(caseId);
           
            if (currentCase != null)
            {
                
                currentCase.AM_ActionReference.Load();
                currentCase.AM_Action.AM_LookupCode1Reference.Load();
                currentCase.AM_Action.AM_LookupCodeReference.Load();
                currentCase.AM_StatusReference.Load();
                currentCase.AM_Status.AM_LookupCode1Reference.Load();
                currentCase.AM_Status.AM_LookupCodeReference.Load();
                if (CheckActionFollowUp((currentCase.AM_Action.RecommendedFollowupPeriod), currentCase.AM_Action.AM_LookupCode1.CodeName, currentCase.ActionRecordedDate.Value))
                {
                    Dictionary<string, string> Datadictonary = HistoryManager.GenrateAlert(currentCase, false, Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    if (Datadictonary != null)
                    {
                        SetModalPopupControls(Datadictonary, true);
                        upalertpanel_ModalPopupExtender.Show();
                    }
                }
                else if (CheckTriggerForNextAction((currentCase.AM_Action.NextActionAlert), currentCase.AM_Action.AM_LookupCode.CodeName, currentCase.ActionRecordedDate.Value))
                {
                    Dictionary<string, string> Datadictonary = HistoryManager.GenrateAlert(currentCase, false, Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    if (Datadictonary != null)
                    {
                        SetModalPopupControls(Datadictonary, false);
                        upalertpanel_ModalPopupExtender.Show();
                    }
                }
                else if (CheckStatusFollowUpDate((currentCase.AM_Status.ReviewPeriod), currentCase.AM_Status.AM_LookupCode.CodeName, currentCase.StatusRecordedDate.Value))
                {
                    Dictionary<string, string> Datadictonary = HistoryManager.GenrateAlert(currentCase, true, Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    if (Datadictonary != null)
                    {
                        SetModalPopupControls(Datadictonary, true);
                        upalertpanel_ModalPopupExtender.Show();
                    }
                }
                else if (CheckTriggerForNextStatus((currentCase.AM_Status.NextStatusAlert), currentCase.AM_Status.AM_LookupCode1.CodeName, currentCase.StatusRecordedDate.Value))
                {
                    Dictionary<string, string> Datadictonary = HistoryManager.GenrateAlert(currentCase, true, Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    if (Datadictonary != null)
                    {
                        SetModalPopupControls(Datadictonary, false);
                        upalertpanel_ModalPopupExtender.Show();
                    }
                }
                else
                {
                    upalertpanel_ModalPopupExtender.Hide();
                    return;
                }
            }
        }

        #endregion

        #region"Check Action Follow Up Date"

        public bool CheckActionFollowUp(int period, string duration, DateTime recordedDate)
        {
            if (recordedDate != null)
            {
                DateTime date = DateOperations.AddDate(period, duration, recordedDate);
                if (DateOperations.CompareDates(date, DateTime.Now))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Check Trigger For Next Action"

        public bool CheckTriggerForNextAction(int period, string duration, DateTime recordedDate)
        {
            if (recordedDate != null)
            {
                DateTime date = DateOperations.AddDate(period, duration, recordedDate);
                if (DateOperations.CompareDates(date, DateTime.Now))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Check Trigger For Next Status"

        public bool CheckTriggerForNextStatus(int period, string duration, DateTime recordedDate)
        {
            if (recordedDate != null)
            {
                DateTime date = DateOperations.AddDate(period, duration, recordedDate);
                if (DateOperations.CompareDates(date, DateTime.Now))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Check Status Follow Up Date"

        public bool CheckStatusFollowUpDate(int period, string duration, DateTime recordedDate)
        {
            if (recordedDate != null)
            {
                DateTime date = DateOperations.AddDate(period, duration, recordedDate);
                if (DateOperations.CompareDates(date, DateTime.Now))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Set Modal Popup Controls"

        public void SetModalPopupControls(Dictionary<string, string> Datadictonary, bool isFollowUp)
        {
            if (isFollowUp)
            {
                this.lblalertmessage.Text = UserMessageConstants.ModelPopupTitleFollowUpCaseHistory;
            }
            else
            {
                this.lblalertmessage.Text = UserMessageConstants.ModelPopupTitleTriggerCaseHistory;
            }
            this.lblstatususer.Text = Datadictonary["Status"];
            this.lblactionuser.Text = Datadictonary["Action"];
            this.lblcontactuser.Text = Datadictonary["Contactnumber"];
            this.lblactiondateuser.Text = Datadictonary["ReviewDate"];
            this.lblletterusr.Text = Datadictonary["Letter"];
            lblCustomer.Text = Datadictonary["CustomerName"];
            lblNextActionDetails.Text = Datadictonary["NextActiondetails"];
            //DateTime date = Convert.ToDateTime(Datadictonary["ReviewDate"]);
            DateTime date = DateOperations.ConvertStringToDate(Datadictonary["ReviewDate"]);

            int i = date.CompareTo(DateTime.Now);
            if (i < 0)
            {
                btnsupress.Enabled = true;
                //btnpostponeaction.Enabled = false;
            }
            else
            {
                if (i > 0)
                {
                    btnpostponeaction.Enabled = true;
                    btnsupress.Enabled = false;
                }
                else
                {
                    btnsupress.Enabled = true;
                    btnpostponeaction.Enabled = true;
                }
            }
            if (!Datadictonary["ActionId"].Equals(UserMessageConstants.ModelPopupNoNextActionCaseHistory))
            {
                Session[SessionConstants.NextStatusIdCaseSummary] = Datadictonary["StatusId"];
                Session[SessionConstants.NextActionIdCaseSummary] = Datadictonary["ActionId"];
                btnrecordaction.Enabled = true;
            }
            else
            {
                btnrecordaction.Enabled = true;
            }
        }

        #endregion

        #region Clearsupresscase fields
        private void ClearSupressCaseFields()
        {


            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>");

            // Clear Immunization  Contact Fields
            sb.AppendLine("function ClearSupressCaseFields()");
            sb.AppendLine("{");

            sb.Append("control = document.getElementById('");
            sb.Append(this.txtsupressreason.ClientID);
            sb.AppendLine("');");
            sb.AppendLine("control.value = '';");

            sb.Append("control = document.getElementById('");
            sb.Append(this.txtreviewdate.ClientID);
            sb.AppendLine("');");
            sb.AppendLine("control.value = '';");

            sb.Append("control = document.getElementById('");
            sb.Append(this.lblsupresserrormsg.ClientID);
            sb.AppendLine("');");
            sb.AppendLine("control.value = '';");

            sb.AppendLine("}");
            sb.AppendLine("</script>");
            base.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClearSupressCaseFields", sb.ToString());
        }
        #endregion

        #region Clear IgnoreCase Fields
        private void ClearIgnoreCaseFields()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>");

            // Clear Immunization  Contact Fields
            sb.AppendLine("function ClearIgnoreCaseFields()");
            sb.AppendLine("{");

            sb.Append("control = document.getElementById('");
            sb.Append(this.txtignorereason.ClientID);
            sb.AppendLine("');");
            sb.AppendLine("control.value = '';");

            sb.Append("control = document.getElementById('");
            sb.Append(this.lblignorerrormsg.ClientID);
            sb.AppendLine("');");
            sb.AppendLine("control.value = '';");

            sb.AppendLine("}");
            sb.AppendLine("</script>");
            base.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClearIgnoreCaseFields", sb.ToString());
        }
        #endregion

        #region setsession value
        private bool SetSessionValues()
        {
            if (Session[SessionConstants.TenantId] != null && Session[SessionConstants.CaseId] != null)
            {
                caseId = int.Parse(Session[SessionConstants.CaseId].ToString());
                tenantId = int.Parse(Session[SessionConstants.TenantId].ToString());
                return true;
            }
            else
            {
                if (Request.QueryString["caseid"] != null && Request.QueryString["tenid"] != null)
                {
                    if (Validation.CheckIntegerValue(Request.QueryString["caseid"].ToString()) && Validation.CheckIntegerValue(Request.QueryString["tenid"].ToString()))
                    {
                        base.SetCaseId(int.Parse(Request.QueryString["caseid"].ToString()));
                        base.SetTenantId(int.Parse(Request.QueryString["tenid"].ToString()));
                        caseId = int.Parse(Request.QueryString["caseid"].ToString());
                        tenantId = int.Parse(Request.QueryString["tenid"].ToString());
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region"Set Image URL"

        public string SetImageURL(string isPaymentPlan)
        {
            if (isPaymentPlan == "True")
            {
                return "~/style/images/im_greenbutton.jpg";
            }
            else return "~/style/images/im_redbutton.jpg";

        }

        #endregion        

        #region"Script Builder"

        public void ScriptBuilder()
        {
            LinkButton lbtn = new LinkButton();
            string url = string.Empty;

            for (int i = 0; i < dataSetUploadedDocuments.Items.Count; i++)
            {
                lbtn = (LinkButton)dataSetUploadedDocuments.Items[i].FindControl("lbtnDocumentName");
                url = PathConstants.downloadFile + "?file=" + lbtn.Text;
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        public void ActivityDocumentsScriptBuilder()
        {
            LinkButton lbtn = new LinkButton();
            string url = string.Empty;

            for (int i = 0; i < ActivityDataSet.Items.Count; i++)
            {
                lbtn = (LinkButton)ActivityDataSet.Items[i].FindControl("lbtnDocumentName");
                url = PathConstants.downloadFile + "?p=asa&file=" + lbtn.Text;
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        #endregion

        #region "Link Standard Letters"

        private void LinkStandardLetters()
        {
            LinkButton lbtn = new LinkButton();
            string url = string.Empty;
            for (int i = 0; i < sldatalist.Items.Count; i++)
            {
                lbtn = (LinkButton)sldatalist.Items[i].FindControl("lbtnstandardletter");
                if (ViewState[ViewStateConstants.CaseLetterId] != null)
                {
                    url = PathConstants.CHRedirecttopreview + "PopUp&Id=" + lbtn.CommandArgument.ToString() + "&CaseLetterId=" + ViewState[ViewStateConstants.CaseLetterId].ToString();
                }
                else
                {
                    url = PathConstants.CHRedirecttopreview + "PopUp&Id=" + lbtn.CommandArgument.ToString();
                }
                
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        private void LinkActivityStandardLetters()
        {

            LinkButton lbtn = new LinkButton();
            string url = string.Empty;
            for (int i = 0; i < ActivityStandardLetterDataSet.Items.Count; i++)
            {
                lbtn = (LinkButton)ActivityStandardLetterDataSet.Items[i].FindControl("lbtnstandardletter");
                
                if (ViewState[ViewStateConstants.LetterActivityId] != null)
                {
                    url = PathConstants.CHRedirecttopreview + "PopUp&Id=" + lbtn.CommandArgument.ToString() + "&LetterActivityId=" + ViewState[ViewStateConstants.LetterActivityId].ToString();
                }
                else
                {
                    url = PathConstants.CHRedirecttopreview + "PopUp&Id=" + lbtn.CommandArgument.ToString();
                }
                
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        #endregion

        #region"Check Payment Plan"

        public void CheckPaymentPlan()
        {
            try
            {
                paymentManager = new Payments();
                //btnPaymentPlan.Enabled = paymentManager.CheckPaymentPlan(base.GetTenantId());
            }
            catch (NullReferenceException nullException)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                isException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                isException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                isException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CheckPaymentPlanPrintPayments, true);
                }
            }

        }

        #endregion

        #region"Check Visited Case"

        public bool CheckVisitedCase()
        {
            ArrayList list;
            list = Session[SessionConstants.visitedCases] as ArrayList;
            if (list == null)
            {
                list = new ArrayList();
                list.Add(base.GetCaseId());
                Session[SessionConstants.visitedCases] = list;
                return false;
            }
            if (list.Contains(base.GetCaseId()))
            {
                return true;
            }
            else
            {
                list.Add(base.GetCaseId());
                Session[SessionConstants.visitedCases] = list;
                return false;
            }
        }

        #endregion

        #region"Check Payment Plan"

        public bool CheckPaymentPlanExistance()
        {
            OpenCase openCase = new OpenCase();
            return openCase.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Check Payment Plan Type"

        public bool CheckPaymentPlanType()
        {
            Payments paymentmanager = new Payments();
            string str = paymentmanager.GetPaymentPlanType(base.GetTenantId());
            if (str.Equals("Flexible"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region "Deep Clone"

        //public static T DeepClone<T>(this T a)
        //{
        //    using (MemoryStream stream = new MemoryStream())
        //    {
        //        BinaryFormatter formatter = new BinaryFormatter();
        //        formatter.Serialize(stream, a);
        //        stream.Position = 0;
        //        return (T)formatter.Deserialize(stream);
        //    }
        //}

        #endregion

        #region"Check Standard Letter and Document Attached"

        public bool CheckStandardLetterAndDocumentAttached(string upload, string attached)
        {
            if (upload.Equals("False") && attached.Equals("False"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #endregion

    }
}