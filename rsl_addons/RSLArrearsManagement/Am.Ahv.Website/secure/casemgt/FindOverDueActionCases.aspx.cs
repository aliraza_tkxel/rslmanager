﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using System.Data;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BusinessManager.payments;
using System.Drawing;
using System.Drawing.Imaging;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Website.pagebase;
using System.Threading;


namespace Am.Ahv.Website.secure.casemgt
{
    public partial class FindOverDueActionCases : System.Web.UI.Page
    {
        /// <summary>
        /// Schedule Job -Runs on Demand (Manual)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    ProgressBarModalPopupExtender.Show();
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            SetMessage(UserMessageConstants.FindOverDueActionCasesScheduleJobInProgress, false);
            //updpnlJob.Update();
            //ProgressBarModalPopupExtender.Show();
            //Thread.Sleep(7000);
            CheckOverDueActionCases();
           // Panel1.Visible = false;
        }

        #region"Check OverDue Action Cases"

        public void CheckOverDueActionCases()
        {
            int count = 0;
            try
            {
                Am.Ahv.BusinessManager.Casemgmt.CaseList overDueCases = new Am.Ahv.BusinessManager.Casemgmt.CaseList();
                count = overDueCases.FindOverDueActionCases();
                
                string emailBody = "It is to notify you that 'Find  OverDue Action Cases' schedule job has been completed successfully and has <br />identified " + count + " cases with overdue actions against customers having active payment plan." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                if (PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailSubjectFindOverDueActionCases, emailBody))
                {
                    SetMessage(UserMessageConstants.FindOverDueActionCasesScheduleJobSuccess, false);
                }
            }
            catch (EntityException ee)
            {
                ExceptionPolicy.HandleException(ee, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailFailureSubjectFindOverDueActionCases, UserMessageConstants.FailureEmailBodyFindOverDueActionCases);
                    SetMessage(UserMessageConstants.FindOverDueActionCasesScheduleJobFailure, true);
                }
                catch (Exception ex)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindOverDueActionCases, true);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailFailureSubjectFindOverDueActionCases, UserMessageConstants.FailureEmailBodyFindOverDueActionCases);
                    SetMessage(UserMessageConstants.FindOverDueActionCasesScheduleJobFailure, true);
                }
                catch (Exception exInner)
                {
                    ExceptionPolicy.HandleException(exInner, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindOverDueActionCases, true);
                }
            }
        }

        #endregion

        /// <summary>
        /// Sets the success or error message. 
        /// </summary>
        /// <param name="str">text/message success or error </param>
        /// <param name="isError"> true or false</param>
        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
    }
}