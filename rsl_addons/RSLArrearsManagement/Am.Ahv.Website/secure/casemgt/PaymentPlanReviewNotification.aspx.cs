﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.payments;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using Am.Ahv.Website.pagebase;
using System.Drawing;
using System.Configuration;

namespace Am.Ahv.Website.secure.casemgt
{
    public partial class PaymentPlanReviewNotification : PageBase
    {
        #region"Attributes"

        Payments payment = null;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckReviewDate();
        }

        #region"Functions"

        #region"Check Review Date"

        public void CheckReviewDate()
        {
            try
            {
                payment = new Payments();
                DataTable dataTable = payment.GetPaymentPlanReview();
                int count = 0;
                if (dataTable != null)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    //foreach(KeyValuePair<string, string> item in data)
                    {
                        DateTime date = DateOperations.ConvertStringToDate(dataTable.Rows[i]["ReviewDate"].ToString());
                        string currentDate = DateTime.Now.ToShortDateString();
                        DateTime date2 = Convert.ToDateTime(currentDate);
                        if (date.CompareTo(date2) == 0)
                        {
                            string CheckReviewDateEmailBody = "The Payment Plan review date for tenant " + dataTable.Rows[i]["FIRSTNAME"].ToString() + "" + dataTable.Rows[i]["LASTNAME"].ToString() +
                                       "at " + dataTable.Rows[i]["PROPERTYID"].ToString() + " " + dataTable.Rows[i]["ADDRESS1"].ToString() + ", " + dataTable.Rows[i]["TOWNCITY"].ToString() + ", " + dataTable.Rows[i]["POSTCODE"].ToString() +
                                        "is" + date + ".";
                            PageBase.SendEmail(dataTable.Rows[i]["Email"].ToString(), UserMessageConstants.emailSubjectPaymentPlanNotification, CheckReviewDateEmailBody, true);
                            count++;
                        }
                    }
                }
                string emailBody = "It is to notify you that payment plan review notification schedule job has been completed successfully and has <br />sent " + count + " emails." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                if (PageBase.SendEmailNotification(count, true, UserMessageConstants.EmailSubjectPaymentPlanReviewNotification, emailBody))
                {
                    SetMessage(UserMessageConstants.successPaymentPlanReviewNotification, false);
                }
            }
            catch (EntityException eex)
            {
                ExceptionPolicy.HandleException(eex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(0, true, UserMessageConstants.EmailFailureSubjectPaymentPlanReviewNotification, UserMessageConstants.FailureEmailBodyPaymentPlanReviewNotification);
                    SetMessage(UserMessageConstants.ErrorPaymentPlanReviewNotification, true);
                }
                catch (Exception ex)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(0, true, UserMessageConstants.EmailFailureSubjectPaymentPlanReviewNotification, UserMessageConstants.FailureEmailBodyPaymentPlanReviewNotification);
                    SetMessage(UserMessageConstants.ErrorPaymentPlanReviewNotification, true);
                }
                catch (Exception exInner)
                {
                    ExceptionPolicy.HandleException(exInner, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                }
            }

        }


        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Send Email Notification"

        public bool SendEmailNotification(int count, bool success)
        {
            try
            {
                bool isSent = false;
                if (success)
                {
                    string emailBody = "It is to notify you that payment plan review notification schedule job has been completed successfully and has <br />sent " + count + " emails." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                    string emailTo = ConfigurationManager.AppSettings["AdministratorEmailAddress"].ToString();
                    while (true)
                    {
                        if (PageBase.SendEmail(emailTo, UserMessageConstants.EmailSubjectPaymentPlanReviewNotification, emailBody, true))
                        {
                            isSent = true;
                            break;
                        }
                    }
                    return isSent;
                }
                else
                {
                    string emailTo = ConfigurationManager.AppSettings["AdministratorEmailAddress"].ToString();
                    while (true)
                    {
                        if (PageBase.SendEmail(emailTo, UserMessageConstants.EmailFailureSubjectPaymentPlanReviewNotification, UserMessageConstants.FailureEmailBodyPaymentPlanReviewNotification, true))
                        {
                            isSent = true;
                            break;
                        }
                    }
                    return isSent;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                return false;
            }

        }

        #endregion

        #endregion
    }
}