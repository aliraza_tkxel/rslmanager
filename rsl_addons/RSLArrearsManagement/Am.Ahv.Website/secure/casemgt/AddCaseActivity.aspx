﻿<%@ Page Title="Add Case Activity :: Arrears Management" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="AddCaseActivity.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.AddActivity" %><%@ Register src="../../controls/casemgt/UserInfoSummary.ascx" tagname="UserInfoSummary" tagprefix="uc1" %>
<%@ Register src="~/controls/casemgt/ClientLastPayment.ascx" tagname="ClientLastPayment" tagprefix="uc2" %>
<%@ Register src="~/controls/casemgt/AddCaseActivity.ascx" tagname="AddCaseActivity" tagprefix="uc4" %>
<%@ Register src="~/controls/casemgt/AddRegularPaymentPlan.ascx" tagname="AddRegularPaymentPlan" tagprefix="uc3" %>
<%@ Register TagPrefix="uclFlexiblePaymentPlan" TagName="uclFlexiblePaymentPlan" Src="~/controls/casemgt/AddFlexiblePaymentPlan.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../style/scripts/OpenNewCase.js" type="text/javascript"></script>
    
    <style type="text/css">
        .style2
        {
            width: 100%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table width="100%" class="tableClass" >
     <tr><td class="tableHeader">
 Case Detail
     </td></tr>   
    <tr>
     <td class="style2" style="padding:10px">
      <asp:UpdatePanel ID="updpnlDiv" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
 <div style="float:left; width:35%"> 
         <div style=" margin-left:15px">          
             <uc1:UserInfoSummary ID="UserInfoSummary1" runat="server" />          
        </div>
        
        <div style="margin-left:15px; margin-top:10px" >         
            <uc2:ClientLastPayment ID="ClientLastPayment1" runat="server" />         
        </div>
        <div style="margin-left:15px; margin-top:10px;" >        
         <div runat="server" id="regularPaymentDiv" visible="true" style="position:relative;">     
             <uc3:AddRegularPaymentPlan ID="AddRegularPaymentPlan1" runat="server" />      
         </div>          
          <div runat="server" style="position:relative;" visible="false" id="flexiblePaymentDiv">   
             <uclFlexiblePaymentPlan:uclFlexiblePaymentPlan  ID="flexiblePlan" runat="server"/>
         </div>         
        </div>
 </div>

<div style=" border-color:gray;width:60%;margin-left:20px; float:left" class="tableClass">
    <uc4:AddCaseActivity ID="AddCaseActivity1" runat="server" />
</div>
<asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <span class="Error">Please wait...</span>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
 </ContentTemplate>
     </asp:UpdatePanel>
 </td>
     </tr>
 </table>   
</asp:Content>
