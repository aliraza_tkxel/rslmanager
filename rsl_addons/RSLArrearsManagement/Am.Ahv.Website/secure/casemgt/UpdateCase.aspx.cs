﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.BusinessManager.payments;

namespace Am.Ahv.Website.secure.casemgt
{
    public partial class UpdateCase : PageBase
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }

            if (!IsPostBack)
            {
                if (CheckPaymentPlan())
                {
                    if (CheckPaymentPlanType())
                    {
                        flexiblePaymentDiv.Visible = true;
                        regularPaymentDiv.Visible = false;
                        AddFlexiblePaymentPlan1.FirstLookPaymentPlan(false);
                        Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Flexible";
                    }
                    else
                    {
                        flexiblePaymentDiv.Visible = false;
                        regularPaymentDiv.Visible = true;
                        regularPaymentPlan.FirstLookPaymentPlan(false);
                        Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Regular";
                    }
                }
            }

            AddFlexiblePaymentPlan1.invokeEvent += delegate(bool flag, string value)
            {
                if (flag == true)
                {
                    flexiblePaymentDiv.Visible = false;
                    regularPaymentDiv.Visible = true;
                    updpnlDiv.Update();
                    regularPaymentPlan.ControlLoad(value);
                }
                else
                {
                    flexiblePaymentDiv.Visible = false;
                    regularPaymentDiv.Visible = true;
                    updpnlDiv.Update();
                    regularPaymentPlan.ControlLoad(value);
                    regularPaymentPlan.DisablePaymentPlan();
                    regularPaymentPlan.DisableCancelButton();
                    regularPaymentPlan.ShowUpdateButton();
                }

            };

            regularPaymentPlan.visibleFlexiblePaymentPlan += delegate(bool flag, string value)
            {
                if (flag == true)
                {
                    flexiblePaymentDiv.Visible = true;
                    regularPaymentDiv.Visible = false;
                    updpnlDiv.Update();
                    AddFlexiblePaymentPlan1.ControlLoad(value);
                    // flexiblePlan.updateControl();
                    //updpnlDiv.Update();
                }
                else
                {
                    flexiblePaymentDiv.Visible = true;
                    regularPaymentDiv.Visible = false;
                    updpnlDiv.Update();
                    AddFlexiblePaymentPlan1.ControlLoad(value);
                    AddFlexiblePaymentPlan1.DisablePaymentPlan();
                    AddFlexiblePaymentPlan1.DisableCancelButton();
                    AddFlexiblePaymentPlan1.ShowUpdateButton();
                }
            };
            Session[SessionConstants.PaymentPlanBackPage] = PathConstants.updatecase;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.casehistory);
        }

        #region"Check Payment Plan"

        public bool CheckPaymentPlan()
        {
            OpenCase openCase = new OpenCase();
            return openCase.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Check Payment Plan Type"

        public bool CheckPaymentPlanType()
        {
            Payments paymentmanager = new Payments();
            string str = paymentmanager.GetPaymentPlanType(base.GetTenantId());
            if (str.Equals("Flexible"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}