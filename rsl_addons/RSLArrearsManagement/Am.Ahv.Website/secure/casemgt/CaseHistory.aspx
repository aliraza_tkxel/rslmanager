<%@ Page Language="C#" Title="Case Detail :: Arrears Management" AutoEventWireup="true"
    CodeBehind="CaseHistory.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.CaseHistory"
    MasterPageFile="~/masterpages/AMMasterPage.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uclUserInfo" TagName="userInfo" Src="~/controls/casemgt/UserInfoSummary.ascx" %>
<%@ Register TagPrefix="uclLastPayment" TagName="lastPayment" Src="~/controls/casemgt/ClientLastPayment.ascx" %>
<%@ Register TagPrefix="uclPaymentPlan" TagName="PaymentPlan" Src="~/controls/casemgt/AddRegularPaymentPlan.ascx" %>
<%@ Register TagPrefix="uclFlexiblePaymentPlan" TagName="uclFlexiblePaymentPlan"
    Src="~/controls/casemgt/AddFlexiblePaymentPlan.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="border: 1px solid black; padding: 10px; overflow: hidden; margin: 0px;">
        <div style="border-bottom: 1px solid black; overflow: hidden; margin-bottom: 10px;">
            <h1 style="float: left; font-size: 12px; font-family: Tahoma;">
                <asp:Label ID="lblcasedetails" runat="server" Text="Case Details"></asp:Label>
            </h1>
            <p style="float: right;">
                <asp:Button ID="btnback" runat="server" Text="Back" OnClick="btnback_Click" />
            </p>
        </div>
        <!-- Clearing Floats-->
        <div style="clear: both;">
        </div>
        <asp:Panel ID="pnlControls" runat="server">
            <asp:UpdatePanel ID="updpnlDiv" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Left Side -->
                    <div style="margin-top: 0; float: left; width: 32%;">
                        <div style="margin-bottom: 10px; padding: 5px;">
                            <uclUserInfo:userInfo ID="userInforSummary" runat="server" />
                        </div>
                        <div style="margin-bottom: 10px;">
                            <uclLastPayment:lastPayment ID="lastPaymnt" runat="server" />
                        </div>
                        <asp:UpdatePanel ID="updpnlPaymentPlan" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 10px;">
                                    <div runat="server" id="regularPaymentDiv" style="position: relative; margin-bottom: 10px;"
                                        visible="true">
                                        <uclPaymentPlan:PaymentPlan ID="regularPaymentPlan" runat="server" />
                                    </div>
                                    <div runat="server" visible="false" id="flexiblePaymentDiv" style="position: relative;
                                        margin-bottom: 10px;">
                                        <uclFlexiblePaymentPlan:uclFlexiblePaymentPlan ID="flexiblePlan" runat="server" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <!--End Left Side -->
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Panel ID="pnlOuterCaseHistory" runat="server">
            <asp:UpdatePanel ID="uphistorydetails" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 67%; float: right;">
                        <div>
                            <asp:Panel ID="pnlcasehistory" GroupingText=" " runat="server" ScrollBars="Vertical"
                                Height="368px">
                                 <div style="float: left; width:100%; ">
                                <h4 style="font-size: 12px; font-family: Tahoma; float:left; ">
                                    <asp:Label ID="lblcasehistorytitle" runat="server" Text="Case Summary"></asp:Label>
                                </h4>
                                 <div style="float: right; margin-left: 5px;">
                                    <div id="divCaseClose" runat="server" style="float: right; height: 20px; width: 90px;
                                        background-color: Red; color: White; margin:auto; font-weight:bold; padding:10px; font-size:15px;     ">
                                        Case Closed
                                    </div>
                                </div>
                                </div> 
                                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                </asp:Panel>
                               
                                <div style="float: right; margin-top: 5px; margin-bottom: 10px; margin-left: 5px;">
                                    <asp:Button ID="btnprintcase" runat="server" Text="Print Case" Width="85px" OnClick="btnprintcase_Click"
                                        Visible="true" />
                                    &nbsp;<asp:Button ID="btnupdatecase" runat="server" Text="Update Case" Width="85px"
                                        OnClick="btnupdatecase_Click" />
                                </div>
                                <cc1:PagingGridView ID="gvhistorydetails" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    GridLines="None" HorizontalAlign="Left" EmptyDataText="No Record" Width="100%"
                                    DataKeyNames="CaseId" ShowHeaderWhenEmpty="True" AllowPaging="false">
                                    <HeaderStyle CssClass="tableHeader" />
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <AlternatingRowStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    <Columns>
                                        <asp:TemplateField></asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbldatecreated" CssClass="distanceLabelClass" runat="server" Text=' <%#this.FormatDate(Eval("ActionRecordedDate").ToString()) %> '></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Stage">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" CssClass="distanceLabelClass" runat="server" Text=' <%# Eval("AM_StatusHistory.Title") %> '></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" CssClass="distanceLabelClass" Text=' <%# Eval("AM_ActionHistory.Title") %> '></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Review">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" CssClass="distanceLabelClass" Text='<%#Eval("PaymentPlanIgnoreReason")%>'> </asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User">
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser" runat="server" CssClass="distanceLabelClass" Text=' <%# Eval("ActionIgnoreReason") %> '> </asp:Label></ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnImgPaymentPlanRedFlag" runat="server" ImageUrl="~/style/images/paperclip_img.jpg"
                                                    CommandArgument=' <%# Eval("PaymentPlanHistoryId") %>' OnClick="PaymentPlanFlag_Click"
                                                    Visible='<%# Eval("IsPaymentPlan") %>' />
                                                <%--  <asp:ImageButton ID="btnImgPaymentPlanGreenFlag" runat="server" ImageUrl="~/style/images/im_greenbutton.jpg"
                                                                    CommandArgument=' <%# Eval("PaymentPlanHistoryId") %> ' Visible='<%# Eval("IsPaymentPlan") %>'
                                                                    OnClick="PaymentPlanFlag_Click" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Image ID="imgbutton" runat="server" ImageUrl="~/style/images/SuppressedCaseIcon1.jpg"
                                                    Visible='<%# Eval("IsSuppressed") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbuttonsl" runat="server" ImageUrl="~/style/images/Document Icon.jpg"
                                                    CommandArgument=' <%# Eval("CaseHistoryId") %> ' Visible='<%#  CheckStandardLetterAndDocumentAttached(Eval("IsDocumentAttached").ToString(), Eval("IsDocumentUpload").ToString()) %>'
                                                    OnClick="imgbuttonsl_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Button ID="btnviewcase" runat="server" Text="View Case" Width="70px" OnClick="btnviewcase_Click"
                                                    Visible=' <%# Eval("IsActive") %> ' CommandArgument=' <%# Eval("CaseHistoryId") %> ' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:HoverMenuExtender ID="HoverMenuExtender1" runat="server" TargetControlID="lbldatecreated"
                                                    PopupControlID="panelPopUp" PopDelay="50" OffsetX="0" OffsetY="0" HoverCssClass="popupHover">
                                                </asp:HoverMenuExtender>
                                                <asp:Panel ID="panelPopUp" runat="server" CssClass="popupMenu">
                                                    <asp:Label runat="server" ID="lbl" Text='<%# Eval("Notes") %>'></asp:Label>
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    </Columns>
                                    <RowStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </cc1:PagingGridView>
                                <div style="clear: both;">
                                </div>
                            </asp:Panel>
                            <!-- Clearing Floatsb cvv-->
                            <div style="clear: both;">
                            </div>
                        </div>
                        <div style="margin-top: 10px;">
                            <asp:Panel ID="pnlCaseNotes" runat="server" GroupingText=" " Height="310px">
                                <asp:Panel ID="innerPanel" runat="server" Height="310px" ScrollBars="Vertical">
                                    <h4 style="font-size: 12px; font-family: Tahoma;">
                                        <asp:Label ID="lblcasenotesdetails" runat="server" Text="Activity Summary"></asp:Label>
                                    </h4>
                                    <cc1:PagingGridView ID="gvcasenotes" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                        GridLines="None" HorizontalAlign="Center" EmptyDataText="No Record" Width="450px"
                                        AllowPaging="false">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcreateddate" runat="server" Text='<%# this.FormatDate(Eval("RecordedDate").ToString()) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Activity">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActivity" runat="server" Text='<%# Eval("AM_LookupCode.CodeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Stage">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStage" runat="server" Text='<%# Eval("AM_Status.Title") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAction" runat="server" Text='<%# Eval("AM_Action.Title") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/style/images/Document Icon.jpg"
                                                        CommandArgument=' <%# Eval("ActivityId") %> ' Visible='<%# (Eval("IsLetterAttached").ToString()=="True" || Eval("IsDocumentAttached").ToString()=="True")?true:false %>'
                                                        OnClick="imgbuttonDocument_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:Button ID="btnview" runat="server" CommandArgument='<%# Eval("ActivityId") %>'
                                                        Text="View" OnClick="btnview_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </cc1:PagingGridView>
                                </asp:Panel>
                                <hr />
                                <div style="float: right; margin-top: 10px;">
                                    <asp:Button ID="btnaddactivity" runat="server" OnClick="btnaddactivity_Click" Text="Add New Activity"
                                        Width="110px" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnviewactivities" runat="server" Text="Activity Detail" Width="110px"
                                        OnClick="btnviewactivities_Click" />
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div style="margin-top: 10px;">
                        <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                            <ProgressTemplate>
                                <span class="Error">Please wait...</span>
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upmodalpanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:Panel ID="pnlDocuments" runat="server" GroupingText=" " Width="420px" BackColor="White">
                        <table width="400px">
                            <tr>
                                <td colspan="4">
                                    <h3>
                                        <asp:Label ID="lblcasenotes" runat="server" Text="Documents" Style="font-weight: 700"></asp:Label></h3>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="pinImage" runat="server" ImageUrl="~/style/images/paperclip_img.gif"
                                        Height="20px" />
                                </td>
                                <td>
                                    <asp:Label ID="lblstandardletters" runat="server" Text="Standard Letters"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td>
                                    <asp:Panel ID="slpanel" BorderWidth="1" runat="server">
                                        <asp:DataList ID="sldatalist" runat="server" Width="169px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnstandardletter" OnClick="lbtnDocumentName_Click" runat="server"
                                                    Text='<%#Eval("AM_StandardLetters.Title") %>' CommandArgument='<%# MakeStandardLetterId(Eval("StandardLetterHistoryId").ToString(), Eval("StandardLetterId").ToString()) %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </asp:Panel>
                                    <asp:Label ID="lblnostandardletters" runat="server" Font-Bold="true" ForeColor="Red"
                                        Visible="true" Width="169px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Image ID="imguploaddocuments" runat="server" ImageUrl="~/style/images/Document Icon.gif"
                                        Height="20px" />
                                </td>
                                <td>
                                    <asp:Label ID="lbluploadeddocs" runat="server" Text="Uploaded Documents"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td>
                                    <asp:Panel ID="pnlDataSet" BorderWidth="1" runat="server">
                                        <asp:DataList ID="dataSetUploadedDocuments" runat="server" Width="169px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnDocumentName" OnClick="lbtnDocumentName_Click" runat="server"
                                                    Text='<%#Eval("DocumentName") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </asp:Panel>
                                    <asp:Label ID="lblnodocs" runat="server" Font-Bold="true" ForeColor="Red" Visible="true"
                                        Width="169px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnclose" runat="server" Text="Close Window" Width="120px" OnClientClick="$find('mdlpop').hide(); return false;" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Label ID="lbldisp" runat="server"></asp:Label>
                    <asp:ModalPopupExtender ID="PopupExtenderdocuments" runat="server" CancelControlID="btnclose"
                        BehaviorID="mdlpop" PopupControlID="pnlDocuments" TargetControlID="lbldisp" Enabled="true"
                        BackgroundCssClass="modalBackground" DropShadow="false">
                    </asp:ModalPopupExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Label ID="lblalert" runat="server"></asp:Label>
        </asp:Panel>
        <asp:UpdatePanel ID="upalertpanel" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlAlert" runat="server" GroupingText=" " Width="530px" BackColor="white">
                    <br />
                    <table>
                        <tr>
                            <td align="right" colspan="4">
                                <%--<asp:ImageButton ID="imgclose" runat="server" ImageAlign="Top" ImageUrl="~/style/images/Close Icon.jpg"
                                            Width="15px" Height="15px" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <asp:Label ID="lblalertmessage" runat="server" Text="The next Stage and Action details are outlined below: "></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblstatus" runat="server" Text="Status:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblstatususer" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblaction" runat="server" Text="Action:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblactionuser" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblActionDetails" runat="server" Text="Next Action Details:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblNextActionDetails" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCustomerName" runat="server" Text="Name:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomer" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblcontact" runat="server" Text="Contact:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblcontactuser" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblactionduedate" runat="server" Text="Action Due:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblactiondateuser" runat="server"></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblletter" runat="server" Text="Letter:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblletterusr" runat="server"></asp:Label><br />
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblactionmessage" runat="server" Text="What Whould you like to do? "></asp:Label>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnpostponeaction" runat="server" Text="Postpone Action" Width="120px" />
                            </td>
                            <td>
                                <asp:Button ID="btnignorestatus" runat="server" Text="Ignore Status/Action" Width="120px" />
                            </td>
                            <td>
                                <asp:Button ID="btnrecordaction" runat="server" Text="Record Action Now" Width="120px"
                                    OnClick="btnrecordaction_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnsupress" runat="server" Text="Supress Case" Width="120px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:ModalPopupExtender ID="upalertpanel_ModalPopupExtender" runat="server" TargetControlID="lblactionmessage"
                    PopupControlID="pnlAlert" Enabled="true" BackgroundCssClass="modalBackground"
                    DropShadow="true">
                </asp:ModalPopupExtender>
                <asp:Panel ID="pnlignoreaction" runat="server" GroupingText=" " Width="300px" BackColor="white">
                    <table>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:ImageButton ID="imgignorebtn" runat="server" ImageAlign="Top" ImageUrl="~/style/images/Close Icon.jpg"
                                    Width="15px" Height="15px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>
                                    <asp:Label ID="lblignoremessage" runat="server" Text="Ignore Action"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblignorerrormsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblignorereason" runat="server" Text="Reason"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtignorereason" runat="server" TextMode="MultiLine">
                        
                        
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnignorecancel" runat="server" Text="Cancel" OnClick="btnignorecancel_Click"
                                    OnClientClick="ClearIgnoreCaseFields()" />
                                <asp:Button ID="btnignoresave" runat="server" Text="Save" OnClick="btnignoresave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:ModalPopupExtender ID="upalertpanel_ModalPopupExtender1" runat="server" TargetControlID="btnignorestatus"
                    PopupControlID="pnlignoreaction" CancelControlID="imgignorebtn" Enabled="true"
                    BackgroundCssClass="modalBackground" DropShadow="true">
                </asp:ModalPopupExtender>
                <asp:Panel ID="pnlsupresscase" runat="server" GroupingText=" " Width="300px" BackColor="white">
                    <table>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:ImageButton ID="imgsupress" runat="server" ImageAlign="Top" ImageUrl="~/style/images/Close Icon.jpg"
                                    Width="15px" Height="15px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>
                                    <asp:Label ID="lblsupressmessage" runat="server" Text="Supress Case"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblsupresserrormsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblreasonsupress" runat="server" Text="Reason:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtsupressreason" runat="server" TextMode="MultiLine">
                        
                        
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblreview" runat="server" Text="Review:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtreviewdate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtreviewdate"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnsupresscancel" runat="server" Text="Cancel" OnClick="btnsupresscancel_Click"
                                    OnClientClick="ClearSupressCaseFields()" />
                                <asp:Button ID="btnsupresssave" runat="server" Text="Save" OnClick="btnsupresssave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:ModalPopupExtender ID="upmodalextendersupresscase" runat="server" TargetControlID="btnsupress"
                    PopupControlID="pnlsupresscase" CancelControlID="imgsupress" Enabled="true" BackgroundCssClass="modalBackground"
                    DropShadow="true">
                </asp:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="updpnlActivityModalPopUp" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlActivityDocuments" runat="server" GroupingText=" " Width="420px"
                    BackColor="White">
                    <table width="400px">
                        <tr>
                            <td colspan="4">
                                <h3>
                                    <asp:Label ID="lblActivityDocs" runat="server" Text="Documents" Style="font-weight: 700"></asp:Label></h3>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgActivityStandardLetters" runat="server" ImageUrl="~/style/images/paperclip_img.gif"
                                    Height="20px" />
                            </td>
                            <td>
                                <asp:Label ID="lblActivityStandardletters" runat="server" Text="Standard Letters"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                            <td>
                                <asp:Panel ID="pnlActivityStandardLetters" BorderWidth="1" runat="server">
                                    <asp:DataList ID="ActivityStandardLetterDataSet" runat="server" Width="169px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnstandardletter" OnClick="lbtnDocumentName_Click" runat="server"
                                                Text='<%#Eval("AM_StandardLetters.Title") %>' CommandArgument='<%# MakeStandardLetterId(Eval("StandardLetterHistoryId").ToString(), Eval("StandardLettersId").ToString()) %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </asp:Panel>
                                <asp:Label ID="lblnoActivitystandardletters" runat="server" Font-Bold="true" ForeColor="Red"
                                    Visible="true" Width="169px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imguploadActivitydocuments" runat="server" ImageUrl="~/style/images/Document Icon.gif"
                                    Height="20px" />
                            </td>
                            <td>
                                <asp:Label ID="lbluploadedActivitydocs" runat="server" Text="Uploaded Documents"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                            <td>
                                <asp:Panel ID="pnlActivityDataSet" BorderWidth="1" runat="server">
                                    <asp:DataList ID="ActivityDataSet" runat="server" Width="169px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnDocumentName" OnClick="lbtnDocumentName_Click" runat="server"
                                                Text='<%#Eval("DocumentName") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </asp:Panel>
                                <asp:Label ID="lblNoActivityDocs" runat="server" Font-Bold="true" ForeColor="Red"
                                    Visible="true" Width="169px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            </td>
                            <td align="right">
                                <asp:Button ID="btnActivityDocumentsClose" runat="server" Text="Close Window" Width="120px" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Label ID="lbldispActivity" runat="server"></asp:Label>
                <asp:ModalPopupExtender ID="PopupExtenderActivitydocuments" runat="server" CancelControlID="btnActivityDocumentsClose"
                    PopupControlID="pnlActivityDocuments" TargetControlID="lbldispActivity" Enabled="true"
                    BackgroundCssClass="modalBackground" DropShadow="false">
                </asp:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="margin-bottom: 40px;">
    </div>
</asp:Content>
