﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.payments;
using Am.Ahv.BusinessManager.Casemgmt;


namespace Am.Ahv.Website.secure.casemgt
{
    public partial class ViewCaseActivity : PageBase
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                if (CheckPaymentPlan())
                {
                    if (CheckPaymentPlanType())
                    {
                        flexiblePaymentDiv.Visible = true;
                        regularPaymentDiv.Visible = false;
                        flexiblePlan.FirstLookPaymentPlan(false);
                        Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Flexible";
                    }
                    else
                    {
                        flexiblePaymentDiv.Visible = false;
                        regularPaymentDiv.Visible = true;
                        AddRegularPaymentPlan1.FirstLookPaymentPlan(false);
                        Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Regular";
                    }
                }
            }

            flexiblePlan.invokeEvent += delegate(bool flag, string value)
            {
                if (flag == true)
                {
                    flexiblePaymentDiv.Visible = false;
                    regularPaymentDiv.Visible = true;
                    updpnlDiv.Update();
                    AddRegularPaymentPlan1.ControlLoad(value);
                }
                else
                {
                    flexiblePaymentDiv.Visible = false;
                    regularPaymentDiv.Visible = true;
                    updpnlDiv.Update();
                    AddRegularPaymentPlan1.ControlLoad(value);
                    AddRegularPaymentPlan1.DisablePaymentPlan();
                    AddRegularPaymentPlan1.DisableCancelButton();
                    AddRegularPaymentPlan1.ShowUpdateButton();
                }
            };

            AddRegularPaymentPlan1.visibleFlexiblePaymentPlan += delegate(bool flag, string value)
            {
                if (flag == true)
                {
                    flexiblePaymentDiv.Visible = true;
                    regularPaymentDiv.Visible = false;
                    updpnlDiv.Update();
                    flexiblePlan.ControlLoad(value);
                    // flexiblePlan.updateControl();
                    //updpnlDiv.Update();
                }
                else
                {
                    flexiblePaymentDiv.Visible = true;
                    regularPaymentDiv.Visible = false;
                    updpnlDiv.Update();
                    flexiblePlan.ControlLoad(value);
                    flexiblePlan.DisablePaymentPlan();
                    flexiblePlan.DisableCancelButton();
                    flexiblePlan.ShowUpdateButton();
                }
            };
            Session[SessionConstants.PaymentPlanBackPage] = PathConstants.viewActivitiesPath;
        }
        #region"Check Payment Plan"

        public bool CheckPaymentPlan()
        {
            OpenCase openCase = new OpenCase();
            return openCase.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Check Payment Plan Type"

        public bool CheckPaymentPlanType()
        {
            Payments paymentmanager = new Payments();
            string str = paymentmanager.GetPaymentPlanType(base.GetTenantId());
            if (str.Equals("Flexible"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}