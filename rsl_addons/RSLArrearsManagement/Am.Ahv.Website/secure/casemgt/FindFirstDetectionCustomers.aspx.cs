﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Website.pagebase;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.IO;
using Am.Ahv.Utilities.utitility_classes;
using System.Drawing;
using System.Configuration;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.Customers;

namespace Am.Ahv.Website.secure.casemgt
{
    public partial class FindFirstDetectionCustomers : PageBase
    {
        Am.Ahv.BusinessManager.Casemgmt.FirstDetectionList firstDetectionManager = null;
        
        #region"Page Load"

       /// <summary>
       /// Schedule Job 
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsGetCustomersRentParametersJobInProgress())
            {
                SetMessage(UserMessageConstants.GetCustomersRentParametersJobInProgress, true);
                return;
            }
            else
            {
                SetMessage(UserMessageConstants.FindFirstDetectionCustomersScheduleJobInProgress, false);
                CheckFirstDetection();
            }
            
        }

        #endregion

        #region"Is get Customers Rent Parameters Job In Progress"

        public Boolean IsGetCustomersRentParametersJobInProgress()
        {
            Customers customerManager = new Customers();
            return customerManager.IsGetCustomersRentParametersJobInProgress();            
        }

        #endregion

        /// <summary>
        /// Detects the defaulters 
        /// and add them in first detection list 
        /// </summary>
        #region"Check First Detection"

        public void CheckFirstDetection()
        {
            int defaulterCount = 0;
            try
            {
                firstDetectionManager = new Am.Ahv.BusinessManager.Casemgmt.FirstDetectionList();
                int total = 0;
                total = firstDetectionManager.GetRentListCount();
                double rent = 0.0;
                var statusParameters = firstDetectionManager.GetStatusParameters();
                int count = 0;
                //Update:Umair
                //Update Date:24 08 2011 and 28 11 2011
                if (total > 0)
                {
                    firstDetectionManager.AddFirstDetectionHistory(true);
                    firstDetectionManager.DeleteFirstDetection(true);
                }
                //update end
                for (int i = 0; i < total; i += 100)
                {
                    var customerRentList = firstDetectionManager.GetRentList(i, 100);                    
                    //DateTime startDate;
                    if (customerRentList == null)
                    {
                        break;
                    }
                    if (customerRentList.Count == 0)
                    {
                        break;
                    }
                    
                    foreach (var temp in customerRentList)
                    {
                        //rent = Convert.ToDouble(RentBalance.CalculateRentBalance(Convert.ToDouble(temp.RentBalance), Convert.ToDouble(temp.rentAmount)));
                        rent = Convert.ToDouble(temp.RentBalance);
                        bool check = true;
                        if (rent > 0)
                        {
                            count += 1;
                            if (statusParameters.IsRentParameter == true)
                            {
                                if (statusParameters.CodeName == "Weeks")
                                {
                                    if (Math.Abs(rent) > Convert.ToDouble((Convert.ToInt32(statusParameters.RentParameter) * Convert.ToDouble(Convert.ToDouble(temp.rentAmount) / 4.0))))
                                    {
                                        if (!firstDetectionManager.CheckDefaultertenant(temp.TenancyId))
                                        {
                                            firstDetectionManager.AddFirstDetection(temp.CustomerId, temp.TenancyId, DateTime.Now, true);
                                            defaulterCount += 1;
                                        }
                                    }
                                }
                                else if (statusParameters.CodeName == "Days")
                                {
                                    int days = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
                                    if (Math.Abs(rent) > Convert.ToDouble((Convert.ToInt32(statusParameters.RentParameter)) * Convert.ToDouble(Convert.ToDouble(temp.rentAmount) / Convert.ToDouble(days))))
                                    {
                                        if (!firstDetectionManager.CheckDefaultertenant(temp.TenancyId))
                                        {
                                            firstDetectionManager.AddFirstDetection(temp.CustomerId, temp.TenancyId, DateTime.Now, true);
                                            defaulterCount += 1;
                                        }
                                    }
                                }
                                else if (statusParameters.CodeName == "Months")
                                {
                                    if (Math.Abs(rent) > Convert.ToDouble((Convert.ToInt32(statusParameters.RentParameter)) * temp.rentAmount))
                                    {
                                        if (!firstDetectionManager.CheckDefaultertenant(temp.TenancyId))
                                        {
                                            firstDetectionManager.AddFirstDetection(temp.CustomerId, temp.TenancyId, DateTime.Now, true);
                                            defaulterCount += 1;
                                        }
                                    }
                                }
                                else if (statusParameters.CodeName == "Years")
                                {
                                    if (Math.Abs(rent) > Convert.ToDouble((Convert.ToInt32(statusParameters.RentParameter)) * Convert.ToDouble(temp.rentAmount * 12)))
                                    {
                                        if (!firstDetectionManager.CheckDefaultertenant(temp.TenancyId))
                                        {
                                            firstDetectionManager.AddFirstDetection(temp.CustomerId, temp.TenancyId, DateTime.Now, true);
                                            defaulterCount += 1;
                                        }
                                    }
                                }
                            }
                            else if (statusParameters.IsRentAmount == true)
                            {
                                if (Math.Abs(rent) > statusParameters.RentAmount.Value)
                                {
                                    // For Testing...
                                    //testingFirstDetection(temp.TenancyId, temp.CustomerId, count, rent, Convert.ToDouble(temp.RentBalance), Convert.ToDouble(temp.rentAmount), true);
                                    //check = false;
                                    /////////////////////////
                                    //Update by:Umair
                                    //Update Date:25 08 2011
                                    // if (!firstDetectionManager.CheckDefaultertenant(temp.CustomerId))
                                    if (firstDetectionManager.CheckDefaultertenant(temp.CustomerId))
                                    //end update
                                    {
                                        firstDetectionManager.AddFirstDetection(temp.CustomerId, temp.TenancyId, DateTime.Now, true);
                                        defaulterCount += 1;
                                    }
                                }
                            }
                        }
                        // for testing................
                        //if (check)
                        //{
                        //    testingFirstDetection(temp.TenancyId, temp.CustomerId, count, rent, Convert.ToDouble(temp.RentBalance), Convert.ToDouble(temp.rentAmount), false);
                        //}
                        ///////////////////////
                    }
                }

                //Update by:Umair
                //Update Date:28 11 2011
                firstDetectionManager.DeleteFirstDetectionHistory();
                //Update End

                string emailBody = "It is to notify you that First Detection schedule job has been completed successfully and has <br />identified " + defaulterCount + " customers in the first detection list." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                if (PageBase.SendEmailNotification(defaulterCount, true, UserMessageConstants.EmailSubjectFindFirstDetectionCustomers, emailBody))
                {
                    SetMessage(UserMessageConstants.successFindFirstDetectionCustomers, false);
                }
            }
            catch (EntityException entityexception)
            {
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(defaulterCount, false, UserMessageConstants.EmailFailureSubjectFindFirstDetectionCustomers, UserMessageConstants.FailureEmailBodyFindFirstDetectionCustomers);
                    SetMessage(UserMessageConstants.ErrorFindFirstDetectionCustomers, true);
                }
                catch (Exception ex)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(defaulterCount, false, UserMessageConstants.EmailFailureSubjectFindFirstDetectionCustomers, UserMessageConstants.FailureEmailBodyFindFirstDetectionCustomers);
                    SetMessage(UserMessageConstants.ErrorFindFirstDetectionCustomers, true);
                }
                catch (Exception exInner)
                {
                    ExceptionPolicy.HandleException(exInner, "Exception Policy");
                    SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                }
            }

        }

        #endregion

        
        /// <summary>
        /// send Email Notification to Administrator When
        /// First Detection Shedule job Execute.
        /// </summary>
        /// <param name="count">How many Records Detected</param>
        /// <param name="success">either successfully execute or not</param>
        /// <returns>bool (true or false) - email sent or not</returns>
        
        #region"Send Email Notification"

        public bool SendEmailNotification(int count, bool success)
        {
            try
            {
                bool isSent = false;
                if (success)
                {
                    string emailBody = string.Empty;
                    string emailTo = ConfigurationManager.AppSettings["AdministratorEmailAddress"].ToString();
                    while (true)
                    {
                        if (PageBase.SendEmail(emailTo, UserMessageConstants.EmailSubjectFindFirstDetectionCustomers, emailBody, true))
                        {
                            isSent = true;
                            break;
                        }
                    }
                    return isSent;
                }
                else
                {
                    string emailTo = ConfigurationManager.AppSettings["AdministratorEmailAddress"].ToString();
                    while (true)
                    {
                        if (PageBase.SendEmail(emailTo, UserMessageConstants.EmailFailureSubjectFindFirstDetectionCustomers, UserMessageConstants.FailureEmailBodyFindFirstDetectionCustomers, true))
                        {
                            isSent = true;
                            break;
                        }
                    }
                    return isSent;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                SetMessage(UserMessageConstants.ErrorInEmailFindFirstDetectionCustomers, true);
                return false;
            }

        }

        #endregion

       
        /// <summary>
        /// Sets the success or error message. 
        /// </summary>
        /// <param name="str">text/message success or error </param>
        /// <param name="isError"> true or false</param>
        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

       
        /// <summary>
        /// Just For Testing 
        /// Write all the Records on File.
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="customerId"></param>
        /// <param name="count"></param>
        /// <param name="rent"></param>
        /// <param name="actualRent"></param>
        /// <param name="weeklyRent"></param>
        /// <param name="isdefaulter"></param>
        #region"Testing First Detection"

        public void testingFirstDetection(int tenantId, int customerId, int count, double rent, double actualRent, double weeklyRent, bool isdefaulter)
        {
            try
            {
                FileStream fs = null;
                string path = @"c:\CheckingFirstDetection_UK.txt";
                if (File.Exists(path))
                {
                    fs = new FileStream(path, FileMode.Append);
                }
                else
                {
                    fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite);
                }
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine(System.DateTime.Now.ToString());
                sw.WriteLine("-------------------------------------------------------------------");
                sw.WriteLine("tenantId: " + tenantId);
                sw.WriteLine("customerId: " + customerId);
                sw.WriteLine("isdefaulter: " + isdefaulter);
                sw.WriteLine("count: " + count);
                sw.WriteLine("Actual Rent: " + actualRent);
                sw.WriteLine("Weekly Rent: " + weeklyRent);
                sw.WriteLine("Calculated Rent: " + rent);
                sw.WriteLine("-------------------------------------------------------------------");
                sw.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion
    }
}