﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using System.Configuration;
using Am.Ahv.Website.pagebase;
using System.Data;
using Am.Ahv.BusinessManager.Customers;

namespace Am.Ahv.Website.secure.casemgt
{
    public partial class GetCustomerRentParameters : System.Web.UI.Page
    {
        #region"Page Load"

        /// <summary>
        /// Schedule Job 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (RegisterCustomerRentParametersScheduleJobStatus())
            {
                GetCustomersRent();
            }
            else
            {
                SetMessage(UserMessageConstants.JobInProgressCustomerRentParameters, true);
            }

        }

        #endregion

        #region"Register Customer Rent Parameters Schedule Job Status"

        public bool RegisterCustomerRentParametersScheduleJobStatus()
        {
            try
            {
                Customers customerManager = new Customers();
                return customerManager.RegisterCustomerRentParametersScheduleJobStatus();
            }
            catch (EntityException ee)
            {
                ExceptionPolicy.HandleException(ee, "Exception Policy");
                return false;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return false;
            }           
        }

        #endregion

        #region"Get Customers Rent"

        public void GetCustomersRent()
        {
            Customers customerManager = new Customers();
            try
            {                
                int timer = int.Parse(ConfigurationManager.AppSettings["ConnectionTimer"]);
                customerManager.GetCustomersRentParameters(timer);
                string emailBody = "It is to notify you that get customers rent parameters schedule job has been completed successfully." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                if (PageBase.SendEmailNotification(0, true, UserMessageConstants.EmailSuccessSubjectCustomerRentParameters, emailBody))
                {
                    customerManager.EndCustomerRentParametersScheduleJob();
                    SetMessage(UserMessageConstants.successFindCustomerRentParameters, false);
                }
            }
            catch (EntityException ee)
            {
                ExceptionPolicy.HandleException(ee, "Exception Policy");
                
                try
                {
                    PageBase.SendEmailNotification(0, false, UserMessageConstants.EmailFailureSubjectCustomerRentParameters, UserMessageConstants.FailureEmailBodyFindFirstDetectionCustomers);
                    customerManager.ErrorCustomerRentParametersScheduleJob(ee.InnerException.ToString());
                    SetMessage(UserMessageConstants.ErrorCustomerRentParameters, true);
                }
                catch (Exception ex)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                    customerManager.ErrorCustomerRentParametersScheduleJob(ex.InnerException.ToString());
                    SetMessage(UserMessageConstants.ErrorInEmailCustomerRentParameters, true);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(0, false, UserMessageConstants.EmailFailureSubjectCustomerRentParameters, UserMessageConstants.FailureEmailBodyFindFirstDetectionCustomers);
                    customerManager.ErrorCustomerRentParametersScheduleJob(ex.Message.ToString());
                    SetMessage(UserMessageConstants.ErrorCustomerRentParameters, true);
                }
                catch (Exception exInner)
                {
                    ExceptionPolicy.HandleException(exInner, "Exception Policy");
                    customerManager.ErrorCustomerRentParametersScheduleJob(ex.Message.ToString());
                    SetMessage(UserMessageConstants.ErrorInEmailCustomerRentParameters, true);
                }
            }
        }

        #endregion      


        /// <summary>
        /// Sets the success or error message. 
        /// </summary>
        /// <param name="str">text/message success or error </param>
        /// <param name="isError"> true or false</param>
        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
    }
}