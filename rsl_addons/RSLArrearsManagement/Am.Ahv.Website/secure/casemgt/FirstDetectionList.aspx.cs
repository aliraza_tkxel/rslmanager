﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.IO;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Entities;

namespace Am.Ahv.Website.secure.casemgt
{
    public partial class FirstDetectionList : PageBase
    {
        private string fontColor = "color";
        private string redColor = "#FF0000";
        #region "Attributes"

        Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker caseWorker;
        Am.Ahv.BusinessManager.Casemgmt.FirstDetectionList firstDetectionList;

        bool noRecord;

        public bool NoRecord
        {
            get { return noRecord; }
            set { noRecord = value; }
        }

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }


        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        int regionId;

        public int RegionId
        {
            get { return regionId; }
            set { regionId = value; }
        }

        int suburbId;

        public int SuburbId
        {
            get { return suburbId; }
            set { suburbId = value; }
        }

        bool allRegionFlag;

        public bool AllRegionFlag
        {
            get { return allRegionFlag; }
            set { allRegionFlag = value; }
        }

        bool allSuburbFlag;

        public bool AllSuburbFlag
        {
            get { return allSuburbFlag; }
            set { allSuburbFlag = value; }
        }

        string surname;

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        string address1;
        public string Address1
        {
            get { return address1; }
            set { address1 = value; }
        }

        string tenancyid;
        public string Tenancyid
        {
            get { return tenancyid; }
            set { tenancyid = value; }
        }

        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }
        #endregion

        DateTime latestDate = new DateTime(1800, 01, 24);
        Validation val = new Validation();

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            resetMessage();
            //setMessage("Success Message", false);
            if (!IsPostBack)
            {                
                if (Request.QueryString["cmd"] != null)
                {
                    if (Request.QueryString["cmd"] == "cexpire")
                    {
                        setMessage(UserMessageConstants.CacheExpire, true);
                    }
                    else if (Request.QueryString["cmd"] == "FirstDetectionAlert")
                    {
                        if (CheckDashboardSession())
                        {
                            ViewState["SortDirection"] = "ASC";
                            ViewState["SortExpression"] = "TENANCYID";
                            ShowDashboardSearchCriteria();
                        }
                    }
                    else if (Request.QueryString["cmd"] == "previoustenants")
                    {
                        pnlGridPreviousTenants.Visible = true;
                        pnlGridFirstDetection.Visible = false;
                        if (CheckDashboardSession())
                        {
                            ViewState["SortDirection"] = "ASC";
                            ViewState["SortExpression"] = "TENANCYID";
                            ViewState[ViewStateConstants.previoustenantsFirstDetectionList] = "true";
                            lblFirstDetection.Text = UserMessageConstants.PreviousTenantsFirstDetection;
                            ShowDashboardSearchCriteria();
                        }
                    }
                }
                else
                {
                    ViewState["SortDirection"] = "ASC";
                    ViewState["SortExpression"] = "TENANCYID";
                    setDefaultPagingAttributes();                  
                    PopulateCaseOwnedBy();
                    populateRegions(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                    populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                    setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue),  txtAddress.Text, txtTenancyid.Text);
                    setPagingLabels();
                }
            }

         }

        #endregion

        #region"LBtn Next Click"

        protected void llbtnNext_Click(object sender, EventArgs e)
        {
            getViewStateValues();
            increasePaging();
        }

        #endregion

        #region"Lbtn Previous Click"

        protected void llbtnPrevious_Click(object sender, EventArgs e)
        {
            getViewStateValues();
            reducePaging();
        }

        #endregion

        #region"DDL Region Selected Index Changed"

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateSuburbs();
                updpnlDropDowns.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionAgainstSuburbFirstDetection, true);
                }
            }
        }

        #endregion

        #region"DDL Case Own By Selected Index Changed"

        protected void ddlCaseOwnedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                populateRegions(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                PopulateSuburbs();
                updpnlDropDowns.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionAgainstSuburbFirstDetection, true);
                }
            }
        }

        #endregion

        #region"DDL Suburb Selected Index Changed"

        protected void ddlSuburb_SelectedIndexChanged(object sender, EventArgs e)
        {           
        }

        #endregion

        #region"Btn Search Click"

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                {
                    ViewState["SortDirection"] = "ASC";
                    ViewState["SortExpression"] = "TENANCYID";
                }
                else
                {
                    ViewState["SortDirection"] = "ASC";
                    ViewState["SortExpression"] = "TENANCYID";
                }
                setDefaultPagingAttributes();
                getViewStateValues();
                if (txtSurname.Text == string.Empty)
                {
                    txtSurname.Text = "";
                }
                if (ddlRegion.SelectedValue == "-1")
                {
                    if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                    {
                        populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                        setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                    }
                    else
                    {
                        populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                        setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                    }
                }
                else
                {
                    if (ddlSuburb.SelectedValue != "-1")
                    {
                        if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                        {
                            populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                            setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                        }
                        else
                        {
                            populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                            setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                        }
                    }
                    else
                    {
                        if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                        {
                            populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                            setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                        }
                        else
                        {
                            populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                            setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                        }
                    }
                }
                setPagingLabels();
                updpnlFirsDetection.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionButtonSearchFirstDetection, true);
                }
            }
        }

        #endregion

        #region" DDL records per Page Selected Index Changed"

        protected void ddlRecordsPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setDefaultPagingAttributes();
                getViewStateValues();
                //if (pgvFirstDetection.Rows.Count > 0)
                //{
                    DbIndex = 1;
                    getSearchIndexes();
                    if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                    {
                        populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]),Address1,Tenancyid);
                        setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Address1, Tenancyid);
                    }
                    else
                    {
                        populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), Address1, Tenancyid);
                        setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Address1, Tenancyid);
                    }
                    setPagingLabels();
                //}
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionRecordsPerPageFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Btn Open New Case Click"

        protected void btnOpenNewCase_Click(object sender, EventArgs e)
        {
            Button btn = new Button();
            Label lbl = new Label();            
            GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;
            
            btn = (Button)pgvFirstDetection.Rows[index].FindControl("btnOpenNewCase");
            lbl = (Label)pgvFirstDetection.Rows[index].FindControl("lblCustomerId");
            
            Session[SessionConstants.CustomerId] = lbl.Text;
            Session[SessionConstants.TenantId] = btn.CommandArgument;
            
            Session.Remove(SessionConstants.UpdateFlagPaymentPlan);
            Session.Remove(SessionConstants.OpenCaseFlag);
            base.RemovePaymentPlanSession();
            Response.Redirect(PathConstants.openNewCase);           
        }


        #endregion

        #region"Lbtn Name Click"

        protected void lbtnName_Click(object sender, EventArgs e)
        {
           //// string url;
           // LinkButton btn = new LinkButton();
           // GridViewRow gvr = ((LinkButton)sender).Parent.Parent as GridViewRow;
           // int index = gvr.RowIndex;
           // btn = (LinkButton)pgvFirstDetection.Rows[index].FindControl("lbtnName");
           // Session[SessionConstants.CustomerId] = btn.CommandArgument;

           //// url = string.Format("../../../Customer/CRM.asp?CustomerID={0}", btn.CommandArgument);
           //// btn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
           // //Response.Redirect(string.Format("../../../Customer/CRM.asp?CustomerID={0}",btn.CommandArgument));
        }


        protected void popUpRentBalance(bool isPreviousTenants)            
        {
            LinkButton btn = new LinkButton();
            LinkButton btn2 = new LinkButton();
            string url;
            string customerId;
            string tenancyId;

            if (isPreviousTenants)
            {
                for (int i = 0; i < pgvPreviousTenants.Rows.Count; i++)
                {
                    Label lbl = new Label();
                    btn = (LinkButton)pgvPreviousTenants.Rows[i].FindControl("lbtnRentBalance");
                    btn2 = (LinkButton)pgvPreviousTenants.Rows[i].FindControl("lbtnName");
                    customerId = btn2.CommandArgument;

                    lbl = (Label)pgvPreviousTenants.Rows[i].FindControl("lblTenancyId");
                    tenancyId = lbl.Text;
                    url = "../../../Customer/Popups/Tenant_Statement.asp?customerid=" + customerId + "&tenancyid=" + tenancyId;
                    btn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);

                    if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                    {
                        Button btnView = (Button)pgvPreviousTenants.Rows[i].FindControl("btnOpenNewCase");
                        btnView.Visible = false;
                        //updated by:umair
                        //update date:7/10/2011
                        Label lblCid = (Label)pgvPreviousTenants.Rows[i].FindControl("lblCaseId");
                        if (lblCid.Text.Equals(string.Empty) == false)
                        {
                            Button btnViewCase = (Button)pgvPreviousTenants.Rows[i].FindControl("btnViewCase");
                            btnViewCase.Visible = true;
                        }
                        //end update
                    }
                }
            }
            else
            {
                for (int i = 0; i < pgvFirstDetection.Rows.Count; i++)
                {
                    Label lbl = new Label();
                    btn = (LinkButton)pgvFirstDetection.Rows[i].FindControl("lbtnRentBalance");
                    btn2 = (LinkButton)pgvFirstDetection.Rows[i].FindControl("lbtnName");
                    customerId = btn2.CommandArgument;

                    lbl = (Label)pgvFirstDetection.Rows[i].FindControl("lblTenancyId");
                    tenancyId = lbl.Text;
                    url = "../../../Customer/Popups/Tenant_Statement.asp?customerid=" + customerId + "&tenancyid=" + tenancyId;
                    btn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);

                    if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                    {
                        Button btnView = (Button)pgvFirstDetection.Rows[i].FindControl("btnOpenNewCase");
                        btnView.Visible = false;
                    }
                }
            }
        }


        #region"popUp Customer Name"

        public void popUpCustomerName(bool isPreviousTenant)
        {
            LinkButton lbtn = new LinkButton();
            Label lbl = new Label();
            string url = string.Empty;

            if (isPreviousTenant)
            {
                for (int i = 0; i < pgvPreviousTenants.Rows.Count; i++)
                {
                    lbtn = (LinkButton)pgvPreviousTenants.Rows[i].FindControl("lbtnName");
                    url = string.Format("../../../Customer/CRM.asp?CustomerID={0}", lbtn.CommandArgument);
                    lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
                }
            }
            else
            {
                for (int i = 0; i < pgvFirstDetection.Rows.Count; i++)
                {
                    lbtn = (LinkButton)pgvFirstDetection.Rows[i].FindControl("lbtnName");
                    url = string.Format("../../../Customer/CRM.asp?CustomerID={0}", lbtn.CommandArgument);
                    lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
                }
            }
        }     

        #endregion

        #endregion

        #endregion

        #region"Populate Data"

        #region"Populate GridView"

        public void populateGridView(string _postCode,int _caseOwnedBy, int _regionId, int _suburbId, bool _allRegionFlag, bool _allSuburbFlag, string _surname, int index, int pageSize, string sortExpression, string sortDirection,string _address1,string _tenancyid)
        {
            try
            {
                firstDetectionList = new Am.Ahv.BusinessManager.Casemgmt.FirstDetectionList();
                List<Am.Ahv.Entities.AM_SP_getFirstDetectionList_Result> Dataset = firstDetectionList.GetFirstDetectionList(_postCode, _caseOwnedBy, _regionId, _suburbId, _allRegionFlag, _allSuburbFlag, _surname, index, pageSize, sortExpression, sortDirection, _address1, _tenancyid).ToList();
               
                    Session["FirstDetectionDS"] = Dataset;
               
                pgvFirstDetection.DataSource = Dataset;//firstDetectionList.GetFirstDetectionList(_regionId, _suburbId, _allRegionFlag, _allSuburbFlag, _surname, index, pageSize);
                pgvFirstDetection.DataBind();
                popUpRentBalance(false);
                popUpCustomerName(false);
                setViewStateValues(Convert.ToString(_regionId), Convert.ToString(_suburbId), _allRegionFlag, _allSuburbFlag, _surname,_address1,_tenancyid);
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            { 
                if(IsError)
                {
                    setMessage(UserMessageConstants.exceptionLoadingGridViewFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Populate Previous Tenants"

        public void populatePreviousTenants(int _caseOwnedBy, int _regionId, int _suburbId, bool _allRegionFlag, bool _allSuburbFlag, string _surname, int index, int pageSize, string sortExpression, string sortDirection,string _address1,string _tenancyid)
        {
            try
            {
                firstDetectionList = new Am.Ahv.BusinessManager.Casemgmt.FirstDetectionList();
                List<Am.Ahv.Entities.AM_SP_GetPreviousTenantsList_Result> Dataset = firstDetectionList.GetPreviousTenants(_caseOwnedBy, _regionId, _suburbId, _allRegionFlag, _allSuburbFlag, _surname, index, pageSize, sortExpression, sortDirection,_address1,_tenancyid).ToList();

                Session["FirstDetectionDS"] = Dataset;

                pgvPreviousTenants.DataSource = Dataset;//firstDetectionList.GetFirstDetectionList(_regionId, _suburbId, _allRegionFlag, _allSuburbFlag, _surname, index, pageSize);
                pgvPreviousTenants.DataBind();
                popUpRentBalance(true);
                popUpCustomerName(true);
                setViewStateValues(Convert.ToString(_regionId), Convert.ToString(_suburbId), _allRegionFlag, _allSuburbFlag, _surname,_address1,_tenancyid);                
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionLoadingGridViewFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Populate Lookups"

        //public void initRegionLookup()
        public void populateRegions(int UserId)
        {
            try
            {
                Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker caseWorker = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
              //  Am.Ahv.BusinessManager.Casemgmt.CaseList caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();
                
               // ddlRegion.DataSource = caseList.getRegionList(caseOwnedById);
               ddlRegion.DataSource = caseWorker.GetUserRegion( UserId);
               // ddlRegion.DataTextField = "Patch";
               // ddlRegion.DataValueField = "PatchId";
               ddlRegion.DataTextField = "LOCATION";
               ddlRegion.DataValueField = "PATCHID";
                ddlRegion.DataBind();
                ddlRegion.Items.Add(new ListItem("All", "-1"));

                if (Request.QueryString["cmd"] == "FirstDetectionAlert" || Request.QueryString["cmd"] == "previoustenants")
                {
                    ddlRegion.SelectedValue = Convert.ToString(Session[SessionConstants.RegionDashboardSearch]);

                }
                else
                {
                    ddlRegion.SelectedValue = "-1";
                }
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionLoadingRegionFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Populate Case Owned By"

        public void PopulateCaseOwnedBy()
        {
            try
            {
                //Am.Ahv.BusinessManager.Casemgmt.CaseList caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();
                
                //ddlCaseOwnedBy.DataSource = caseList.getCaseOwnerList();
                //ddlCaseOwnedBy.DataTextField = "EmployeeName";
                //ddlCaseOwnedBy.DataValueField = "ResourceId";
                //ddlCaseOwnedBy.DataBind();
                //ddlCaseOwnedBy.Items.Add(new ListItem("All", "-1"));
                SuppressedCase sc = new SuppressedCase();
                ddlCaseOwnedBy.DataSource = sc.CasesOwnedBy();
                ddlCaseOwnedBy.DataTextField = ApplicationConstants.EmloyeeName;
                ddlCaseOwnedBy.DataValueField = ApplicationConstants.ResourceId;
                ddlCaseOwnedBy.DataBind();
                ddlCaseOwnedBy.Items.Insert(0,new ListItem("All", "-1"));
                ddlCaseOwnedBy.SelectedValue = base.GetLoggedInUserID().ToString();
                if (Request.QueryString["cmd"] == "FirstDetectionAlert" || Request.QueryString["cmd"] == "previoustenants")
                {
                    ddlCaseOwnedBy.SelectedValue = Convert.ToString(Session[SessionConstants.CaseOwnedByDashboardSearch]);
                }
                else
                {
                    ddlCaseOwnedBy.SelectedValue = base.GetLoggedInUserID().ToString();
                }
                ddlRecordsPerPage.DataSource = ApplicationConstants.recordsPerPageFirstDetection;
                ddlRecordsPerPage.DataBind();
                ddlRecordsPerPage.SelectedValue = ApplicationConstants.recordsPerPageFirstDetection.ToString();                
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Populate Subusrbs"

        public void PopulateSuburbs()
        {
            if (ddlRegion.SelectedValue != "-1")
            {
                caseWorker = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
               // Am.Ahv.BusinessManager.Casemgmt.CaseList caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();
                //ddlSuburb.DataSource = caseWorker.GetUserSuburbs(Convert.ToInt32( ddlCaseOwnedBy.SelectedValue));
                ddlSuburb.DataSource = caseWorker.GetUserSuburbs(Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlSuburb.DataTextField = ApplicationConstants.schememname;
                ddlSuburb.DataValueField = ApplicationConstants.schemeId;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));

                if (Request.QueryString["cmd"] == "FirstDetectionAlert" || Request.QueryString["cmd"] == "previoustenants")
                {
                    ddlSuburb.SelectedValue = Convert.ToString(Session[SessionConstants.SuburbDashboardSearch]);
                }
                else
                {
                    ddlSuburb.SelectedValue = "-1";
                }
                
                
            }
            else
            {
                ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                ddlSuburb.DataTextField = string.Empty;
                ddlSuburb.DataValueField = string.Empty;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";
            }
        }

        #endregion

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void setDefaultPagingAttributes()
        {
            //PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
            CurrentPage = 1;
            DbIndex = 1;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            RowIndex = -1;
            RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            setViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void setViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageFirstDetectionList] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexFirstDetectionList] = DbIndex;
            ViewState[ViewStateConstants.totalPagesFirstDetectionList] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsFirstDetectionList] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexFirstDetectionList] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageFirstDetectionList] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsFirstDetectionList] = CurrentRecords;

        }

        public void setViewStateValues(string _regionId, string _suburbId, bool _allRegionFlag, bool _allSuburbFlag, string _surname,string _address1,string _tenancyid)
        {
            ViewState[ViewStateConstants.regionIdFirstDetectionList] = _regionId;
            ViewState[ViewStateConstants.suburbIdFirstDetectionList] = _suburbId;
            ViewState[ViewStateConstants.allRegionFlagFirstDetectionList] = _allRegionFlag;
            ViewState[ViewStateConstants.allSuburbFlagFirstDetectionList] = _allSuburbFlag;
            ViewState[ViewStateConstants.surnameFirstDetectionList] = _surname;
            ViewState[ViewStateConstants.address1FirstDetectionList] = _address1;
            ViewState[ViewStateConstants.tenancyidFirstDetectionList] = _tenancyid;
        }

        #endregion

        #region"Set Paging Labels"

        public void setPagingLabels()
        {
            try
            {

                int count = TotalRecords;

                if (count > Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    setCurrentRecordsDisplayed();
                    setTotalPages(count);
                    lblCurrentRecords.Text = (CurrentRecords).ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                    {
                        lblCurrentRecords.Text = pgvPreviousTenants.Rows.Count.ToString();
                    }
                    else
                    {
                        lblCurrentRecords.Text = pgvFirstDetection.Rows.Count.ToString();
                    }
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = CurrentPage.ToString();
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    lbtnPrevious.Enabled = false;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    lbtnNext.Enabled = true;
                }
                //FileStream fs = null;
                //if (File.Exists("PagingConstants.txt"))
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Append);
                //}
                //else
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Create, FileAccess.ReadWrite);
                //}
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine(System.DateTime.Now.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.WriteLine("ToatlPages = " + TotalPages.ToString());
                //sw.WriteLine("Current Page = " + CurrentPage.ToString());
                //sw.WriteLine("Current Number of Records = " + CurrentRecords.ToString());
                //sw.WriteLine("Records in Grid View = " + pgvUsers.Rows.Count.ToString());
                //sw.WriteLine("Total Records = " + count.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.Close();
                //fs.Close();
                setViewStateValues();
            }
            catch (FileLoadException flException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionCurrentRecordsFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Set Total Pages"

        public void setTotalPages(int count)
        {
            try
            {
                if (count % Convert.ToInt32(ddlRecordsPerPage.SelectedValue) > 0)
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue)) + 1;
                }
                else
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                }
            }
            catch (DivideByZeroException divide)
            {
                IsError = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionTotalPagesFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Displayed"

        public void setCurrentRecordsDisplayed()
        {
            try
            {
                if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                {
                    if (pgvPreviousTenants.Rows.Count == Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                    {
                        CurrentRecords = CurrentPage * pgvPreviousTenants.Rows.Count;
                    }
                    else if (RemoveFlag == true)
                    {
                        CurrentRecords -= 1;
                    }
                    else
                    {
                        CurrentRecords += pgvPreviousTenants.Rows.Count;
                    }
                }
                else
                {
                    if (pgvFirstDetection.Rows.Count == Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                    {
                        CurrentRecords = CurrentPage * pgvFirstDetection.Rows.Count;
                    }
                    else if (RemoveFlag == true)
                    {
                        CurrentRecords -= 1;
                    }
                    else
                    {
                        CurrentRecords += pgvFirstDetection.Rows.Count;
                    }
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionCurrentRecordsFirstDetection, true);
                }
            }

        }

        #endregion

        #region"Set Total Records Count"

        public void setCurrentRecordsCount(string _postCode, int _caseOwnedBy, int _regionId, int _suburbId, bool _allRegionFlag, bool _allSuburbFlag, string _surname, int index, int pageSize, string _address1, string _tenancyid)
        {
            try
            {
                firstDetectionList = new Am.Ahv.BusinessManager.Casemgmt.FirstDetectionList();
                TotalRecords = firstDetectionList.GetFirstDetectionRecordsCount(_postCode,_caseOwnedBy,_regionId, _suburbId, _allRegionFlag, _allSuburbFlag, _surname, index, pageSize,_address1,_tenancyid);
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionCurrentRecordsFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Set Total Previous Tenants Records Count"

        public void setTotalPreviousTenantsRecordsCount(int _caseOwnedBy, int _regionId, int _suburbId, bool _allRegionFlag, bool _allSuburbFlag, string _surname, int index, int pageSize, string _address1, string _tenancyid)
        {
            try
            {
                firstDetectionList = new Am.Ahv.BusinessManager.Casemgmt.FirstDetectionList();
                TotalRecords = firstDetectionList.GetPreviousTenantsRecordsCount(_caseOwnedBy, _regionId, _suburbId, _allRegionFlag, _allSuburbFlag, _surname, index, pageSize,_address1,_tenancyid);
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionCurrentRecordsFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Set Rent Balance"

        public string SetRentBalance(string rent, string weeklyRent)
        {
            //string balance = RentBalance.CalculateRentBalance(Convert.ToDouble(rent), Convert.ToDouble(weeklyRent));
            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£" + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£" + Math.Round(Convert.ToDouble(rent), 2).ToString()+")";
                }
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion

        #region"Set Owed To BHA"

        public string SetOwedToBHA(string HB, string rentBalance)
        {
            if (HB.Equals(string.Empty))
            {
                if (Convert.ToDouble(rentBalance) > 0.0)
                {
                    return "£" + Math.Round(Math.Abs(Convert.ToDouble(rentBalance)), 2).ToString();
                }
                else
                {
                    return "(£" + Math.Round(Math.Abs(Convert.ToDouble(rentBalance)), 2).ToString() + ")";
                }
            }
            else
            {
                double result = 0.0;
                result = Math.Abs(Convert.ToDouble(rentBalance)) - Math.Abs(Convert.ToDouble(HB));
                if (result < 0.0)
                {
                    return "£" + Math.Round(Math.Abs(result), 2).ToString();
                }
                else
                {
                    return "(£" + Math.Round(Math.Abs(result), 2).ToString() + ")";
                }
            }

        }

        #endregion

        #region"Set Fore Color of Owed to BHA"

        public System.Drawing.Color SetForeColorofOwedToBHA(string HB, string rentBalance)
        {
            string result = SetOwedToBHA(HB, rentBalance);
            string[] resultSplit;
            double OwedToBHA = 0.0;
            if (result.Contains('£'))
            {
                resultSplit = result.Split('£');
                if (resultSplit.Count() == 2)
                {
                    if (resultSplit[1].Contains(')'))
                    {
                        string[] resultSplit2 = resultSplit[1].Split(')');
                        OwedToBHA = Convert.ToDouble(resultSplit2[0]);
                    }
                    else
                    {
                        OwedToBHA = Convert.ToDouble(resultSplit[1]);
                    }
                }
            }
            if (OwedToBHA > 0.0)
            {
                return System.Drawing.Color.Red;
            }
            else
            {
                return System.Drawing.Color.Black;
            }
        }

        #endregion

        #region"Set Customer Name"

        public string SetCustomerName(string customerName1, string customerName2, string jointTenancyCount)
        {
            if (jointTenancyCount == "1")
            {
                return customerName1;
            }
            else
            {
                return customerName2 + " & " + customerName1;
            }
        }

        #endregion

        #endregion

        #region"Getters"

        #region"Get ViewState Values"

        public void getViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageFirstDetectionList]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexFirstDetectionList]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesFirstDetectionList]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsFirstDetectionList]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsFirstDetectionList]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexFirstDetectionList]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageFirstDetectionList]);
        }

        #endregion

        #region"Get Search Indexes"

        public void getSearchIndexes()
        {
            RegionId = Convert.ToInt32(ViewState[ViewStateConstants.regionIdFirstDetectionList]);
            SuburbId = Convert.ToInt32(ViewState[ViewStateConstants.suburbIdFirstDetectionList]);
            AllRegionFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allRegionFlagFirstDetectionList]);
            AllSuburbFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allSuburbFlagFirstDetectionList]);
            Surname = Convert.ToString(ViewState[ViewStateConstants.surnameFirstDetectionList]);
            Address1 = Convert.ToString(ViewState[ViewStateConstants.address1FirstDetectionList]);
            Tenancyid = Convert.ToString(ViewState[ViewStateConstants.tenancyidFirstDetectionList]); 
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Reduce Paging"

        public void reducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                getSearchIndexes();
                if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                {
                    populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex , Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]),Address1,Tenancyid);
                    //setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                }
                else
                {
                    //if (Convert.ToString(ViewState["SortDirection"]).Equals("ASC"))
                    //{
                        populateGridView(Session[SessionConstants.PostCodeSearch].ToString(),Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]),Address1,Tenancyid);
                    //}
                    //else
                    //{
                    //    populateGridView(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, TotalRecords - (DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue)), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                    //}
                }
                setPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void increasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                getSearchIndexes();
                if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                {
                    populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex , Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]),Address1,Tenancyid);
                    //setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                }
                else
                {
                    //if (Convert.ToString(ViewState["SortDirection"]).Equals("ASC"))
                    //{
                        populateGridView(Session[SessionConstants.PostCodeSearch].ToString(),Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]),Address1,Tenancyid);
                    //}
                    //else
                    //{
                    //    populateGridView(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, TotalRecords - (DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue)), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));
                    //}
                }
                setPagingLabels();
            }
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
            isError = false;
        }

        #endregion

        #region"Get Days"

        public string GetDays(string value)
        {
            try
            {
                if (value == null)
                {
                    return string.Empty;
                }
                else
                {
                    DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);                    
                    DateTime date = DateOperations.ConvertStringToDate(value);
                    
                    TimeSpan res = dt.Subtract(date);                    
                    return res.Days.ToString();
                }
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (FormatException fe)
            {
                IsError = true;
                ExceptionPolicy.HandleException(fe, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionGetDaysFirstDetection, true);
                }
            }
            return "0";
        }

        #endregion

        #region"Set Next HB"

        public string SetNextHB(string rent)
        {
            
            if (!rent.Equals(string.Empty))
            {
                if (Convert.ToDouble(rent) == 0.0)
                {
                    return "N/A";
                }
                else if (Convert.ToDouble(rent) < 0.0)
                {
                    return "£ " + Math.Round(Math.Abs(Convert.ToDouble(rent)), 2).ToString();
                }
                else
                {
                    return "(£ " + Math.Round(Convert.ToDouble(rent), 2).ToString()+")";
                }
            }
            else
            {
                return "N/A";
            }
        }

        #endregion

        #region"Check Dashboard Session"

        public bool CheckDashboardSession()
        {
            if (Session[SessionConstants.RegionDashboardSearch] != null || !Convert.ToString(Session[SessionConstants.RegionDashboardSearch]).Equals(string.Empty) ||
                Session[SessionConstants.SuburbDashboardSearch] != null || !Convert.ToString(Session[SessionConstants.SuburbDashboardSearch]).Equals(string.Empty))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Show dashboard Search Criteria"

        public void ShowDashboardSearchCriteria()
        {
            try
            {                
                setDefaultPagingAttributes();
                PopulateCaseOwnedBy();
                populateRegions(Convert.ToInt32(Session[SessionConstants.CaseOwnedByDashboardSearch]));
                {
                    if (!Convert.ToString(Session[SessionConstants.RegionDashboardSearch]).Equals("-1"))
                    {
                        populateRegions(Convert.ToInt32(Session[SessionConstants.CaseOwnedByDashboardSearch]));
                        ddlRegion.SelectedValue = Convert.ToString(Session[SessionConstants.RegionDashboardSearch]);
                        PopulateSuburbs();
                        if (!Convert.ToString(Session[SessionConstants.SuburbDashboardSearch]).Equals("-1"))
                        {
                            ddlSuburb.SelectedValue = Convert.ToString(Session[SessionConstants.SuburbDashboardSearch]);
                            if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                            {
                                populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]),txtAddress.Text,txtTenancyid.Text);
                                setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                            }
                            else
                            {
                                populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                                setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                            }
                        }
                        else
                        {
                            if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                            {
                                populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                                setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                            }
                            else
                            {
                                populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                                setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, false, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                            }
                        }
                    }
                    else
                    {
                        if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                        {
                            populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                            setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                        }
                        else
                        {
                            populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), txtAddress.Text, txtTenancyid.Text);
                            setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlRegion.SelectedValue), -1, true, true, txtSurname.Text, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), txtAddress.Text, txtTenancyid.Text);
                        }
                    }
                }
                setPagingLabels();
                // RemoveDashboardSession();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    setMessage(UserMessageConstants.exceptionDashboardSearchCriteria, true);
                }
            }
        }

        #endregion

        #region"Remove Dashboard Session"

        public void RemoveDashboardSession()
        {
            Session.Remove(SessionConstants.CaseOwnedByDashboardSearch);
            Session.Remove(SessionConstants.RegionDashboardSearch);
            Session.Remove(SessionConstants.SuburbDashboardSearch);
        }

        #endregion

        #region"sorting"

        protected void pgvFirstDetection_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = e.SortDirection;
            }

            setDefaultPagingAttributes();
            getViewStateValues();
            getSearchIndexes();
            
            //if (Convert.ToString(ViewState["SortDirection"]).Equals("DESC"))
            //{
                if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
                {
                    if (e.SortExpression == "AM_FirstDetecionList.TENANCYID")
                    {
                        e.SortExpression = "TENANCYID";
                    }
                    populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), e.SortExpression, GetSortDirection(e.SortExpression).ToString(), Address1, Tenancyid);
                    setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Address1, Tenancyid);
                }
                else
                {
                    populateGridView(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), e.SortExpression, GetSortDirection(e.SortExpression).ToString(), Address1, Tenancyid);
                    setCurrentRecordsCount(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Address1, Tenancyid);
                }
            //}
            //else
            //{
            //    if (Convert.ToString(ViewState[ViewStateConstants.previoustenantsFirstDetectionList]).Equals("true"))
            //    {
            //        setTotalPreviousTenantsRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
            //        DbIndex = 1;
            //        populatePreviousTenants(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, TotalRecords - (DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue)), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]));                    
                    
            //    }
            //    else
            //    {
            //        setCurrentRecordsCount(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
            //        DbIndex = 1;
            //        populateGridView(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), RegionId, SuburbId, AllRegionFlag, AllSuburbFlag, Surname, TotalRecords - (DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue)), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), e.SortExpression, GetSortDirection(e.SortExpression).ToString());
            //    }
            //}

            setPagingLabels();
            //List<Am.Ahv.Entities.AM_SP_getFirstDetectionList_Result> Dataset = Session["FirstDetectionDS"] as List<Am.Ahv.Entities.AM_SP_getFirstDetectionList_Result>;
            //SortGrid<AM_SP_getFirstDetectionList_Result>(Dataset, e, GetSortDirection(e.SortExpression));
        
        }


        public void SortGrid<T>(IEnumerable<T> dataSource, GridViewSortEventArgs e, SortDirection sortDirection)
        {
            e.SortDirection = sortDirection;
            IEnumerable<T>set= pagebase.PageBase.SortGrid<T>(dataSource, e);
            pgvFirstDetection.DataSource = set;
            pgvFirstDetection.DataBind();
        }

        //protected string GridViewSortExpression
        //{
        //    get { return ViewState["SortExpression"] as string ?? string.Empty; }
        //    set { ViewState["SortExpression"] = value; }
        //}

        

        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
           // SortDirection sortDirection = SortDirection.Ascending;

            // Retrieve the last column that was sorted.
            string sortExpression = Convert.ToString(ViewState["SortExpression"]);

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression.ToUpper() == column.ToUpper())
                {
                    string lastDirection = Convert.ToString(ViewState["SortDirection"]);
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        ViewState["SortDirection"] = "DESC";
                        //sortDirection = SortDirection.Descending;
                    }
                    else
                    {
                        ViewState["SortDirection"] = "ASC";
                    }
                }
                else
                {
                    ViewState["SortDirection"] = "DESC";
                }
            }

            // Save new values in ViewState.
            //ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return ViewState["SortDirection"].ToString();
        }

        #endregion

        #region"Set Single Value Bracs and Color"
        public string SetBracs(string value)
        {
            return val.SetBracs(value);
        }


        public System.Drawing.Color SetForeColor(string value)
        {
            return val.SetForeColor(value);
        }

        #endregion

        #region"Set Amount and date Bracs and Color"

        public string SetBracs(string date, string amount)
        {
            return val.SetBracs(date, amount);
            
        }

        public System.Drawing.Color SetForeColor(string date, string amount)
        {
            return val.SetForeColor(date, amount);
        }

        #endregion


        #endregion

        #region"Btn View Click"

        protected void btnViewCase_Click(object sender, EventArgs e)
        {
            Label lbl = new Label();
            Label lblCustomer = new Label();
            Button btn = new Button();
            GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;

            lbl = (Label)pgvPreviousTenants.Rows[index].FindControl("lblTenantId");
            btn = (Button)pgvPreviousTenants.Rows[index].FindControl("btnViewCase");
            lblCustomer = (Label)pgvPreviousTenants.Rows[index].FindControl("lblCustomerId");

            base.SetCaseId(Convert.ToInt32(btn.CommandArgument));
            base.SetTenantId(Convert.ToInt32(lbl.Text));
            Session[SessionConstants.CustomerId] = lblCustomer.Text;
            base.RemovePaymentPlanSession();
            //GetCustomerInformation();
            Response.Redirect(PathConstants.casehistory, false);
        }

        #endregion

    }
}
