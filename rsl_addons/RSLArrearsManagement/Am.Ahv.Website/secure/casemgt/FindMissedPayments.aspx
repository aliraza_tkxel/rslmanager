﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindMissedPayments.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.FindMissedPayments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function StartProgressBar() {
            var myExtender = $find('ProgressBarModalPopupExtender');
            myExtender.show();
            return true;
        }
        function HideProgressBar() {
            var myExtender = $find('ProgressBarModalPopupExtender');
            myExtender.hide();
            return true;

        }
</script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   
     <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
       <div>
            <%--<asp:Button ID="btnSubmit" OnClientClick="StartProgressBar()"
                     runat="server" Text="Submit Time" Width="170px" />--%>
            <asp:ModalPopupExtender ID="ProgressBarModalPopupExtender" runat="server"
                     BackgroundCssClass="ModalBackground" behaviorID="ProgressBarModalPopupExtender"
                     TargetControlID="hiddenField" PopupControlID="Panel1" />
            <asp:Panel ID="Panel1" runat="server" Style="display: none; background-color: #C0C0C0;">
                 <span class="Error">Please wait...</span>
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
            </asp:Panel>
            <asp:HiddenField ID="hiddenField" runat="server" />
       </div>
   </ContentTemplate>
</asp:UpdatePanel>

    </div>
    </form>
</body>
</html>
