﻿<%@ Page Title="My Cases :: Arrears Management" Language="C#" MasterPageFile="~/masterpages/AMMasterPage.Master" AutoEventWireup="true" CodeBehind="CaseList.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.CaseList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 200px;
        }
        .style2
        {
            width: 15%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pnlCaseList"  Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>

 <table >        
        <tr >
            <td class="tdClass" colspan="6" style=" padding-left:10px" >
                <asp:Label ID="lblMyCaseList" runat="server" Font-Bold="true" ForeColor="Black" Font-Size="Medium" Text="Active Case List"></asp:Label>
            </td>
        </tr>
        <tr >
            <td class="tdClass" colspan="6" style=" padding-left:25px">
                <asp:UpdatePanel ID="updpnlDropDown" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>                    
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblCaseOwnedBy" runat="server" Text="Case owned by:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCaseOwnedBy" runat="server" AutoPostBack="True" 
                                onselectedindexchanged="ddlCaseOwnedBy_SelectedIndexChanged" Width="310px">
                                <asp:ListItem Text="All" Value="-1">
                                </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>                    
                    <tr>
                        <td>
                            <asp:Label ID="lblRegion" runat="server" Text="Patch:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="true" 
                                onselectedindexchanged="ddlRegion_SelectedIndexChanged" Width="310px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr >
                        <td>
                            <asp:Label ID="lblSuburb" runat="server" Text="Scheme:"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSuburb" AutoPostBack="true" Width="310px" runat="server" onselectedindexchanged="ddlSuburb_SelectedIndexChanged">
                                <asp:ListItem Text="All" Value="-1">
                                </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    
                    <tr >
                        <td>
                            <asp:Label ID="lblSurname" runat="server" Text="Surname:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Surname" runat="server" Width="300" 
                                ontextchanged="Surname_TextChanged"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click" 
                                Text="Search" />
                        </td>
                    </tr>

                </table>   
                
                </ContentTemplate>
                </asp:UpdatePanel>
                <br />              
            </td>            
        </tr>                      
        <tr>
            <td colspan="6" valign="top" class="tdClass" style=" padding-left:20px; padding-right:15px" >
               <asp:UpdatePanel ID="updpnlCaseList" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
               <asp:Panel ID="pnlGridCaseList" runat="server">
                    
                        
                <cc1:PagingGridView ID="pgvCaseList" Width="100%" EmptyDataText="No records found." 
                       EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" 
                                    EmptyDataRowStyle-HorizontalAlign="Center" 
                       AllowPaging="false" GridLines="None" runat="server" AutoGenerateColumns="false" 
                       onselectedindexchanged="pgvCaseList_SelectedIndexChanged" AllowSorting="true" onsorting="pgvCaseList_Sorting">
                    <Columns>
                    <asp:TemplateField  HeaderText="Tenancy ID:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true" SortExpression="AM_Case.TenancyId" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="10%" Height="10px" HorizontalAlign="Left" />
                            <ItemTemplate>                            
                                <asp:Label ID="lblTId" runat="server"  Text='<%# Eval("TenancyId")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField  HeaderText="Name:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true" SortExpression="CustomerName" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="14%" Height="10px" HorizontalAlign="Left" />
                            <ItemTemplate>                            
                                <asp:LinkButton ID="lbtnName" CommandArgument='<%#Eval("CUSTOMERID") %>' runat="server" Text='<%# this.SetCustomerName(Eval("CustomerName").ToString(), Eval("CustomerName2").ToString(), Eval("JointTenancyCount").ToString()) %>' ForeColor="Black"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true" HeaderStyle-ForeColor="black" SortExpression="CustomerAddress">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="20%" Height="10px" HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("CustomerAddress") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rent Balance:" ShowHeader="true" SortExpression="RentBalance" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                            <ItemTemplate>
                                <asp:Label ID="lblRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("RentBalance").ToString()) %>' Text='<%# this.SetBracs(Eval("RentBalance").ToString()) %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Owed To BHA:" SortExpression="OwedToBHA" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="8%" HorizontalAlign="Left" Height="10px" />
                            <ItemTemplate>
                                <asp:Label ID="lblowedToBHA" runat="server" ForeColor='<%# this.SetForeColor( Eval("OwedToBHA").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("OwedToBHA").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Stage:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="StatusTitle" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                            <ItemTemplate>                                
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusTitle") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Action:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="ActionTitle" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                            <ItemTemplate>
                                <asp:Label ID="lblLastAction" runat="server" Text='<%#Eval("ActionTitle") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="false" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="4%" HorizontalAlign="Left" Height="10px" />
                            <ItemTemplate>
                                <asp:Image ID="imgSuppressedCase" ImageUrl="~/style/images/SuppressedCaseIcon1.jpg" Visible='<%# this.ConvertStringBitToBoolean(Eval("IsSuppressed").ToString())%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Plan:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="PaymentPlan" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="7%" HorizontalAlign="Left" Height="10px" />
                            <ItemTemplate>
                                <asp:Label ID="lblPaymentPlan" runat="server" Text='<%# this.checkBoolean(Eval("PaymentPlan").ToString())%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Est HB:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left" SortExpression="EstimatedHBDue" HeaderStyle-ForeColor="black">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Width="15%" HorizontalAlign="Left" Height="10px" />
                            <ItemTemplate>
                                <asp:Label ID="lblNextHB" runat="server" ForeColor='<%# this.SetForeColor(Eval("EstimatedHBDue").ToString()) %>' Text='<%#this.SetBracs(Eval("EstimatedHBDue").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemStyle Width="9%" />
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerId" Visible="false" runat="server" Text='<%#Eval("CUSTOMERID") %>'></asp:Label>
                                <asp:Label ID="lblTenantId" runat="server" Visible ="false" Text='<%# Eval("TenancyId")%>'></asp:Label>
                                <asp:Button ID="btnViewCase" OnClick="btnViewCase_Click" CommandArgument='<%#Eval("CaseId") %>' runat="server" Text="View Case" />
                            </ItemTemplate>
                        </asp:TemplateField>                   
                        <asp:TemplateField>
                        <ItemTemplate>
                        <asp:HoverMenuExtender ID="HoverMenuExtender1" runat="server" TargetControlID="imgSuppressedCase" PopupControlID="panelPopUp"
                            PopDelay="50" OffsetX="22" OffsetY="-5" HoverCssClass="popupHover">
                            </asp:HoverMenuExtender>
                            <asp:Panel ID="panelPopUp" runat="server"  CssClass="popupMenu">                                                                        
                                <asp:Label runat = "server" ID ="Label1" Font-Bold="true" Text = "Case Suppressed" Visible='<%# this.ConvertStringBitToBoolean(Eval("IsSuppressed").ToString())%>' ></asp:Label> <br />
                                <asp:Label runat = "server" ID ="Label2" Font-Bold="true" Text = "Review Date: " Visible='<%# this.ConvertStringBitToBoolean(Eval("IsSuppressed").ToString())%>' ></asp:Label>
                                <asp:Label runat = "server" ID ="lbl" Font-Bold="true" Text = '<%# Eval("SuppressedDate") %>' Visible='<%# this.ConvertStringBitToBoolean(Eval("IsSuppressed").ToString())%>' ></asp:Label>
                            </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                   
                    </Columns>
                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="tableHeader" />
                    <PagerSettings Mode="NumericFirstLast" />
                </cc1:PagingGridView>

                
                </asp:Panel>
                 <table align="center" class="tableFooter">
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblRecords" runat="server" Text="Records"></asp:Label>&nbsp;
                            <asp:Label ID="lblCurrentRecords" runat="server"></asp:Label>&nbsp;
                            <asp:Label ID="lblOf" runat="server" Text="of"></asp:Label>&nbsp;
                            <asp:Label ID="lblTotalRecords" runat="server" ></asp:Label>
                        </td>
                        <td style="padding-left:20px" class="style1">
                             <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>&nbsp;
                             <asp:Label ID="lblCurrentPage" runat="server" ></asp:Label>&nbsp;
                             <asp:Label ID="lblOfPage" runat="server" Text="of"></asp:Label>&nbsp;
                             <asp:Label ID="lblTotalPages" runat="server" ></asp:Label>
                        </td>
                        <td style="padding-left:10px" class="style2">                 
                            <asp:LinkButton ID="lbtnPrevious" runat="server" Text="< Previous" 
                                onclick="llbtnPrevious_Click"></asp:LinkButton>
                        </td>
                        <td style="padding-left:10px" class="style2">
                            <asp:LinkButton ID="lbtnNext" runat="server" Text="Next >" 
                                onclick="llbtnNext_Click"></asp:LinkButton>
                        </td>
                        <td class="style1">
                            <asp:Label ID="lblRecordsPerPage" runat="server" Text="Records per page:"></asp:Label>
                        </td>
                        <td width="5%">
                            <asp:DropDownList ID="ddlRecordsPerPage" runat="server" AutoPostBack="true" 
                                onselectedindexchanged="ddlRecordsPerPage_SelectedIndexChanged">                                
                            </asp:DropDownList>
                        </td>                 
                    </tr>
                </table>  
                <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                <ProgressTemplate>
                    <span class="Error">Please wait...</span>
                    <asp:Image ID="Image3" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
                </ContentTemplate>
                    </asp:UpdatePanel>               
            </td>
        </tr>        
        <tr>
            <td colspan="6" class="tdClass">
               <br />
               <br />
            </td>            
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
