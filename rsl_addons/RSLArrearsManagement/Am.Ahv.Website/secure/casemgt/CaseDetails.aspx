﻿<%@ Page Title="Case Detail :: Arrears Management" Language="C#" AutoEventWireup="true" CodeBehind="CaseDetails.aspx.cs" Inherits="Am.Ahv.Website.secure.casemgt.CaseDetails"
    MasterPageFile="~/masterpages/AMMasterPage.Master" %>
<%@ Register TagPrefix="uclUserInfo" TagName="userInfo" Src="~/controls/casemgt/UserInfoSummary.ascx" %>
<%@ Register TagPrefix="uclLastPayment" TagName="lastPayment" Src="~/controls/casemgt/ClientLastPayment.ascx" %>
<%@ Register TagPrefix="uclPaymentPlan" TagName="PaymentPlan" Src="~/controls/casemgt/AddRegularPaymentPlan.ascx" %>
<%@ Register TagPrefix="uclFlexiblePaymentPlan" TagName="uclFlexiblePaymentPlan" Src="~/controls/casemgt/AddFlexiblePaymentPlan.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 184px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table class="tableClass" cellpadding="0">
        <tr>
            <td colspan="2" class="tableHeader">
                <table width="100%">
                    <tr>
                        <td align="left" >
                            
                                <asp:Label ID="Label1" runat="server" Text="Case Details"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnback" runat="server" Text="Back" onclick="btnback_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
          <tr>
            <td>
                <asp:Panel ID="pnlControls" runat="server">
                  <asp:UpdatePanel ID="updpnlDiv" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>                        
                         <div style="float:left; width:100%"> 
                                 <div style="margin-bottom:10px;padding:5px;">
                                  <uclUserInfo:userInfo ID="userInforSummary" runat="server" />
                                </div>
                                
                                <div style="margin-bottom:10px;padding:5px;">
                                  <uclLastPayment:lastpayment ID="lastPaymnt" runat="server" />
                                </div>
                                
                                <div style="margin-bottom:10px;padding:5px;">
                                
                                 <div runat="server" id="regularPaymentDiv" visible="true">
                                    <uclPaymentPlan:PaymentPlan ID="regularPaymentPlan" runat="server" />
                                  </div>
                                  
                                  <div runat="server" visible="false" id="flexiblePaymentDiv">
                                    <uclFlexiblePaymentPlan:uclFlexiblePaymentPlan  ID="flexiblePlan" runat="server"/>
                                 </div>         
                                </div>
                        </div>
                       
                    </ContentTemplate>
                 </asp:UpdatePanel>
               </asp:Panel>      
            </td>
   
    <td valign="top" style="width:66%">
   
   <asp:Panel ID="pnlOuterCaseDetails" Width="100%" runat="server">  
    <asp:Panel ID="pncasedetails" runat="server" GroupingText=" ">
        <asp:UpdatePanel ID="upcasedetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table cellpadding="5px" width="100%">
                 <tr>
                        <td colspan="2" class="tableHeader" style="border-color:Gray;">
                           <asp:Label ID="lblcasedetails" runat="server" Text="Case Details"></asp:Label>
                        </td>                       
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                        </td>                       
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblinitiatedby" runat="server" Text="Initiated By:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblinitiateduser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblcasemanager" runat="server" Text="Case Manager:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblcasemanageruser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblcaseofficer" runat="server" Text="Case Officer:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblcaseofficeruser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblarrearsstatus" runat="server" Text="Arrears Stage:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblarrearsstatususer" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblstatusreview" runat="server" Text="Stage Review:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblstatusreviewdateuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblarrearsaction" runat="server" Text="Arrears Action:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblarrearsactionuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblactionreview" runat="server" Text="Action Review:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblactionreviewdateuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblrecoveryamount" runat="server" Text="Recovery Amount:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblrecoveryamountuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblnoticeissue" runat="server" Text="Notice Issue:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblnoticeissueuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblnoticeexpiry" runat="server" Text="Notice Expiry:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblnoticeexpiryuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblHearingDate" runat="server" Text="Hearing Date:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblHearingDateUser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblWarrantExpiry" runat="server" Text="Warrant Expiry Date:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblWarrantExpiryUser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lbldocuments" runat="server" Text="Documents:"></asp:Label>
                            <br />
                            <br />
                        </td>
                        <td>
                            <asp:Label ID="lbldocumentsuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblnotes" runat="server" Text="Notes:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblnotesuser" runat="server"></asp:Label>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lbloutcome" runat="server" Text="Outcome:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbloutcomeuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblreason" runat="server" Text="Reason:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblreasonuser" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblPaymentPlan" runat="server" Text="Payment Plan:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblPaymentPlanUser" runat="server"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td class="style1">
                        </td>
                        <td>
                            <asp:TextBox ID="textbox" runat="server" TextMode="MultiLine" Width="250px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnlpaging" runat="server">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblactionlabel" runat="server" Text="Action:" Visible="False"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td align="right"><asp:Button ID="btnupdatecase" runat="server" 
                                        Text="Update Case" Width="120px" onclick="btnupdatecase_Click" /></td>
                            </tr>
                        </table>
                </asp:Panel>
                        </td>
                        
                    </tr>
                </table>               
                
                  <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                            <ProgressTemplate>
                                <span class="Error">Please wait...</span>
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    </asp:Panel>
    
    </td></tr>
    </table>
</asp:Content>
