﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Utilities.constants;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BusinessManager.resource;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.statusmgmt;
namespace Am.Ahv.Website.secure.casemgt
{
    public partial class CaseList : PageBase
    {
        #region "Attributes"

        AddCaseWorker caseWorker;

        Am.Ahv.BusinessManager.Casemgmt.CaseList caseList;
        Validation val = new Validation();
        CustomersReport customer = null;

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        bool noRecord;

        public bool NoRecord
        {
            get { return noRecord; }
            set { noRecord = value; }
        }

        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

        int recordsPerPage;

        public int RecordsPerPage
        {
            get { return recordsPerPage; }
            set { recordsPerPage = value; }
        }

        int regionId;

        public int RegionId
        {
            get { return regionId; }
            set { regionId = value; }
        }

        int caseOwnerId;

        public int CaseOwnerId
        {
            get { return caseOwnerId; }
            set { caseOwnerId = value; }
        }

        int suburbId;

        public int SuburbId
        {
            get { return suburbId; }
            set { suburbId = value; }
        }

        bool allRegionFlag;

        public bool AllRegionFlag
        {
            get { return allRegionFlag; }
            set { allRegionFlag = value; }
        }

        bool allCaseOwnerFlag;

        public bool AllCaseOwnerFlag
        {
            get { return allCaseOwnerFlag; }
            set { allCaseOwnerFlag = value; }
        }

        bool allSuburbFlag;

        public bool AllSuburbFlag
        {
            get { return allSuburbFlag; }
            set { allSuburbFlag = value; }
        }

        private string statusTitle;

        public string StatusTitle
        {
            get { return statusTitle; }
            set { statusTitle = value; }
        }

        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int currentRecords;

        public int CurrentRecords
        {
            get { return currentRecords; }
            set { currentRecords = value; }
        }

        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool removeFlag = false;

        public bool RemoveFlag
        {
            get { return removeFlag; }
            set { removeFlag = value; }
        }

        bool isNisp = false;

        public bool IsNisp
        {
            get { return isNisp; }
            set { isNisp = value; }
        }

        string pCode = "";

        public string PCode
        {
            get { return pCode; }
            set { pCode = value; }
        }

        string overdueCheck = "";

        public string OverdueCheck
        {
            get { return overdueCheck; }
            set { overdueCheck = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!base.CheckSession())
            {
                //Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            ResetMessage();
            //setMessage("Success Message", false);

            if (!IsPostBack)
            {

                SetMessage("", true);
                IsError = false;
                if (Request.QueryString["cmd"] != null)
                {
                    if (Request.QueryString["cmd"] == "Error")
                    {
                        SetMessage(UserMessageConstants.CLquery, true);
                        return;
                    }
                    else if (Request.QueryString["cmd"] == "cexpire")
                    {
                        SetMessage(UserMessageConstants.CacheExpire, true);
                    }
                    else if (Request.QueryString["cmd"] == "mycases")
                    {
                        if (CheckDashboardSession())
                        {
                            ViewState["SortDirection"] = "ASC";
                            //update by:umair
                            //update records:14/10/2011
                            //ViewState["SortExpression"] = "AM_Case.ModifiedDate";
                            ViewState["SortExpression"] = "AM_Case.ActionReviewDate";
                            //update end

                            ShowDashboardSearchCriteria(false, false);
                            ViewState[ViewStateConstants.isFirstDetectionCaseList] = "false";
                            pCode = Session[SessionConstants.PostCodeSearch].ToString();
                        }
                    }
                    else if (Request.QueryString["cmd"] == "FirstDetectionCases")
                    {
                        if (CheckDashboardSession())
                        {
                            ViewState["SortDirection"] = "ASC";
                            //update by:umair
                            //update records:14/10/2011
                            //ViewState["SortExpression"] = "AM_Case.ModifiedDate";
                            ViewState["SortExpression"] = "AM_Case.ActionReviewDate";
                            //update end
                            ShowDashboardSearchCriteria(true, false);
                            ViewState[ViewStateConstants.isFirstDetectionCaseList] = "true";
                        }
                    }
                    else if (Request.QueryString["cmd"] == "nisp")
                    {
                        if (CheckDashboardSession())
                        {
                            ViewState["SortDirection"] = "ASC";
                            //update by:umair
                            //update records:14/10/2011
                            //ViewState["SortExpression"] = "AM_Case.ModifiedDate";
                            ViewState["SortExpression"] = "AM_Case.ActionReviewDate";
                            //update end
                            ShowDashboardSearchCriteria(false, true);
                            ViewState[ViewStateConstants.isFirstDetectionCaseList] = "false";
                            pCode = Session[SessionConstants.PostCodeSearch].ToString();
                        }
                    }
                    else if (Request.QueryString["cmd"] == "overdueStages")
                    {
                        if (CheckDashboardSession())
                        {

                            OverdueCheck = "Stages";
                            ViewState["SortDirection"] = "ASC";
                            //update by:umair
                            //update records:14/10/2011
                            //ViewState["SortExpression"] = "AM_Case.ModifiedDate";
                            ViewState["SortExpression"] = "AM_Case.ActionReviewDate";
                            //update end
                            ShowDashboardSearchCriteria(false, false);
                            ViewState[ViewStateConstants.isFirstDetectionCaseList] = "false";
                            pCode = Session[SessionConstants.PostCodeSearch].ToString();
                        }
                    }
                    else if (Request.QueryString["cmd"] == "overdueActions")
                    {
                        if (CheckDashboardSession())
                        {
                            OverdueCheck = "Actions";
                            ViewState["SortDirection"] = "ASC";
                            //update by:umair
                            //update records:14/10/2011
                            //ViewState["SortExpression"] = "AM_Case.ModifiedDate";
                            ViewState["SortExpression"] = "AM_FindOverDueActionCase.TenancyId";
                            //update end
                            ShowDashboardSearchCriteria(false, false);
                            ViewState[ViewStateConstants.isFirstDetectionCaseList] = "false";
                            pCode = Session[SessionConstants.PostCodeSearch].ToString();
                        }
                    }
                }
                else
                {
                    setDefaultPagingAttributes();
                    initRegionLookup();
                    PopulateCaseOwnedBy();
                    PopulateRegions();

                    ViewState["SortDirection"] = "ASC";
                    //update by:umair
                    //update records:14/10/2011
                    //ViewState["SortExpression"] = "AM_Case.ModifiedDate";
                    ViewState["SortExpression"] = "AM_Case.ActionReviewDate";
                    //update end
                    populateGridView(pCode, -1, Convert.ToInt16(ddlCaseOwnedBy.SelectedValue), -1, true, true, true, DbIndex, ApplicationConstants.pagingPageSizeCaseList, string.Empty, string.Empty, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), false, OverdueCheck);
                    setCurrentRecordsCount(pCode, -1, Convert.ToInt16(ddlCaseOwnedBy.SelectedValue), -1, true, true, true, string.Empty, string.Empty, false, OverdueCheck);

                    setPagingLabels();
                    ViewState[ViewStateConstants.isFirstDetectionCaseList] = "false";

                }




            }

        }

        #endregion

        #region"LBtn Next Click"

        protected void llbtnNext_Click(object sender, EventArgs e)
        {
            getViewStateValues();
            increasePaging();
        }

        #endregion

        #region"Lbtn Previous Click"

        protected void llbtnPrevious_Click(object sender, EventArgs e)
        {
            getViewStateValues();
            reducePaging();
        }

        #endregion

        #region"DDL Region Selected Index Changed"

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlRegion.SelectedValue != "-1")
                {
                    PopulateSuburbs();
                    // PopulateCaseOwnedBy();

                }
                else
                {
                    ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
                    ddlSuburb.DataTextField = string.Empty;
                    ddlSuburb.DataValueField = string.Empty;
                    ddlSuburb.DataBind();
                    ddlSuburb.Items.Insert(0, new ListItem("All", "-1"));
                    ddlSuburb.SelectedValue = "-1";

                    //ddlCaseOwnedBy.DataSource = ApplicationConstants.emptyDataSource;
                    //ddlCaseOwnedBy.DataTextField = string.Empty;
                    //ddlCaseOwnedBy.DataValueField = string.Empty;
                    //ddlCaseOwnedBy.DataBind();
                    //ddlCaseOwnedBy.Items.Add(new ListItem("All", "-1"));
                    //ddlCaseOwnedBy.SelectedValue = "-1";
                }
                ViewState[ViewStateConstants.isFirstDetectionCaseList] = "false";
                updpnlDropDown.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionSuburbAndCaseOwnedBy, true);
                }
            }
        }

        #endregion

        #region"DDL Suburb Selected Index Changed"

        protected void ddlSuburb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlSuburb.SelectedValue != "-1")
                {
                    // PopulateCaseOwnedBySuburb();
                }
                else
                {
                    if (ddlRegion.SelectedValue != "-1")
                    {   //commented by nadir
                        //PopulateCaseOwnedBy();
                    }
                    //else
                    //{
                    //    ddlCaseOwnedBy.Items.Clear();
                    //    ddlCaseOwnedBy.Items.Insert(0,new ListItem("All", "-1"));
                    //    ddlCaseOwnedBy.SelectedValue = "-1";
                    //}
                }
                ViewState[ViewStateConstants.isFirstDetectionCaseList] = "false";
                updpnlDropDown.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionAgainstSuburb, true);
                }
            }
        }

        #endregion

        #region"Btn Search Click"

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                bool nisp = false;
                if (Request.QueryString["cmd"] == "nisp")
                {
                    nisp = true;
                }
                //update by:umair
                //update date:18/11/2011
                else if (Request.QueryString["cmd"] == "overdueStages")
                {
                    OverdueCheck = "Stages";
                }
                else if (Request.QueryString["cmd"] == "overdueActions")
                {
                    OverdueCheck = "Actions";
                }
                //update end
                ViewState["SortDirection"] = "ASC";
                //update by:umair
                //update records:14/10/2011
                //ViewState["SortExpression"] = "AM_Case.ModifiedDate";
                ViewState["SortExpression"] = "AM_Case.ActionReviewDate";
                //update end
                setDefaultPagingAttributes();
                getViewStateValues();
                if (ddlRegion.SelectedValue == "-1")
                {
                    if (ddlCaseOwnedBy.SelectedValue == "-1")
                    {
                        if (Convert.ToString(ViewState[ViewStateConstants.isFirstDetectionCaseList]) == "false")
                        {

                            populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, -1, true, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                            setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, -1, true, true, true, string.Empty, Surname.Text, nisp, OverdueCheck);

                        }
                        else
                        {
                            populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, -1, true, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                            setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, -1, true, true, true, "Initial Case Monitoring", Surname.Text, nisp, OverdueCheck);

                        }
                    }
                    else
                    {
                        if (Convert.ToString(ViewState[ViewStateConstants.isFirstDetectionCaseList]) == "false")
                        {
                            populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, true, false, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                            setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, true, false, true, string.Empty, Surname.Text, nisp, OverdueCheck);
                        }
                        else
                        {
                            populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, true, false, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                            setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, true, false, true, "Initial Case Monitoring", Surname.Text, nisp, OverdueCheck);

                        }
                    }
                }
                else
                {
                    if (ddlSuburb.SelectedValue != "-1")
                    {
                        if (ddlCaseOwnedBy.SelectedValue != "-1")
                        {
                            if (Convert.ToString(ViewState[ViewStateConstants.isFirstDetectionCaseList]) == "false")
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, string.Empty, Surname.Text, nisp, OverdueCheck);

                            }
                            else
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, "Initial Case Monitoring", Surname.Text, nisp, OverdueCheck);

                            }
                        }
                        else
                        {
                            if (Convert.ToString(ViewState[ViewStateConstants.isFirstDetectionCaseList]) == "false")
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, Convert.ToInt32(ddlSuburb.SelectedValue), false, true, false, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, Convert.ToInt32(ddlSuburb.SelectedValue), false, true, false, string.Empty, Surname.Text, nisp, OverdueCheck);

                            }
                            else
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, Convert.ToInt32(ddlSuburb.SelectedValue), false, true, false, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, Convert.ToInt32(ddlSuburb.SelectedValue), false, true, false, "Initial Case Monitoring", Surname.Text, nisp, OverdueCheck);

                            }
                        }
                    }
                    else
                    {
                        if (ddlCaseOwnedBy.SelectedValue != "-1")
                        {
                            if (Convert.ToString(ViewState[ViewStateConstants.isFirstDetectionCaseList]) == "false")
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, false, false, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, false, false, true, string.Empty, Surname.Text, nisp, OverdueCheck);

                            }
                            else
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, false, false, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, false, false, true, "Initial Case Monitoring", Surname.Text, nisp, OverdueCheck);

                            }
                        }
                        else
                        {
                            if (Convert.ToString(ViewState[ViewStateConstants.isFirstDetectionCaseList]) == "false")
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, -1, false, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, -1, false, true, true, string.Empty, Surname.Text, nisp, OverdueCheck);

                            }
                            else
                            {
                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, -1, false, true, true, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), nisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), -1, -1, false, true, true, "Initial Case Monitoring", Surname.Text, nisp, OverdueCheck);

                            }
                        }
                    }
                }
                setPagingLabels();
                updpnlCaseList.Update();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionButtonSearch, true);
                }
            }
        }

        #endregion

        #region" DDL records per Page Selected Index Changed"

        protected void ddlRecordsPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                setDefaultPagingAttributes();
                getViewStateValues();
                getSearchIndexes();
                if (pgvCaseList.Rows.Count > 0)
                {
                    DbIndex = 0;
                    getSearchIndexes();

                    populateGridView(pCode, RegionId, CaseOwnerId, SuburbId, AllRegionFlag, AllCaseOwnerFlag, AllSuburbFlag, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), StatusTitle, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), IsNisp, OverdueCheck);
                    setCurrentRecordsCount(pCode, RegionId, CaseOwnerId, SuburbId, AllRegionFlag, AllCaseOwnerFlag, AllSuburbFlag, string.Empty, Surname.Text, IsNisp, OverdueCheck);

                    setPagingLabels();
                }
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionRecordsPerPage, true);
                }
            }

        }

        #endregion

        #region"Btn View Click"

        protected void btnViewCase_Click(object sender, EventArgs e)
        {
            Label lbl = new Label();
            Label lblCustomer = new Label();
            Button btn = new Button();
            GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;

            lbl = (Label)pgvCaseList.Rows[index].FindControl("lblTenantId");
            btn = (Button)pgvCaseList.Rows[index].FindControl("btnViewCase");
            lblCustomer = (Label)pgvCaseList.Rows[index].FindControl("lblCustomerId");

            base.SetCaseId(Convert.ToInt32(btn.CommandArgument));
            base.SetTenantId(Convert.ToInt32(lbl.Text));
            //Session[SessionConstants.CustomerId] = lblCustomer.Text;
            base.SetCustomerId(Convert.ToInt32(lblCustomer.Text));
            base.RemovePaymentPlanSession();
            GetCustomerInformation();
            Response.Redirect(PathConstants.casehistory, false);
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Populate GridView"

        public void populateGridView(string _postCode, int _regionId, int _caseOwnerId, int _suburbId, bool _allRegionFlag, bool _allOwnerFlag, bool _allSuburbFlag, int index, int pageSize, string _statusTitle, string surname, string sortExpression, string sortDir, bool _isNisp, string OverdueCheck)
        {
            try
            {
                caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();

                if (OverdueCheck == "Actions")
                {
                    ViewState["SortExpression"] = "AM_FindOverDueActionCase.TenancyId";
                    sortExpression = ViewState["SortExpression"].ToString();
                    List<AM_SP_GetOverDueList_Result> Dataset = caseList.getOverDueActionCaseList(_postCode, _regionId, _caseOwnerId, _suburbId, _allRegionFlag, _allOwnerFlag, _allSuburbFlag, index, pageSize, _statusTitle, surname, sortExpression, sortDir, _isNisp, OverdueCheck);

                    Session["CaseListDS"] = Dataset;
                    pgvCaseList.DataSource = Dataset;
                }
                else
                {
                    List<Am.Ahv.Entities.AM_SP_GetCaseList_Result> Dataset = caseList.getUserList(_postCode, _regionId, _caseOwnerId, _suburbId, _allRegionFlag, _allOwnerFlag, _allSuburbFlag, index, pageSize, _statusTitle, surname, sortExpression, sortDir, _isNisp, OverdueCheck).OfType<Am.Ahv.Entities.AM_SP_GetCaseList_Result>().ToList();
                    //caseList.getUserList(_regionId, _caseOwnerId, _suburbId, _allRegionFlag, _allOwnerFlag, _allSuburbFlag, index, pageSize, _statusTitle, surname);
                    Session["CaseListDS"] = Dataset;
                    pgvCaseList.DataSource = Dataset;

                }



                //  pgvCaseList.DataSource = caseList.getUserList(_regionId, _caseOwnerId, _suburbId, _allRegionFlag, _allOwnerFlag, _allSuburbFlag, index, pageSize, _statusTitle, surname);

                pgvCaseList.DataBind();
                popUpCustomerName();

                setViewStateValues(Convert.ToString(_regionId), Convert.ToString(_caseOwnerId), Convert.ToString(_suburbId), _allRegionFlag, _allOwnerFlag, _allSuburbFlag, _statusTitle, _isNisp, OverdueCheck);

            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingGridView, true);
                }
            }
        }

        #endregion

        #region"Populate Lookups"

        public void initRegionLookup()
        {
            try
            {
                // PopulateRegions();

                ddlRecordsPerPage.DataSource = ApplicationConstants.recordsPerPageCaseList;
                ddlRecordsPerPage.DataBind();
                ddlRecordsPerPage.SelectedValue = ApplicationConstants.pagingPageSizeCaseList.ToString();
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingRegion, true);
                }
            }
        }

        #endregion

        #region"Populate Case Owned By"

        public void PopulateCaseOwnedBy()
        {
            try
            {
                // caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();

                // //if (ddlRegion.SelectedValue == "-1" && ddlSuburb.SelectedValue == "-1")
                // //{
                //     ddlCaseOwnedBy.DataSource = caseList.getCaseOwnerList();
                //// }
                //// else if (ddlRegion.SelectedValue != "-1" && ddlSuburb.SelectedValue == "-1")
                // //{
                //   // ddlCaseOwnedBy.DataSource = caseList.getCaseOwnerList(Convert.ToInt32(ddlRegion.SelectedValue));
                // //}
                //// else if (ddlRegion.SelectedValue != "-1" && ddlSuburb.SelectedValue != "-1")
                // //{
                ////     ddlCaseOwnedBy.DataSource = caseList.getCaseOwnerList(Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue));
                // //}

                // ddlCaseOwnedBy.DataTextField = "EmployeeName";
                // ddlCaseOwnedBy.DataValueField = "ResourceId";
                // ddlCaseOwnedBy.DataBind();
                // ddlCaseOwnedBy.Items.Add(new ListItem("All", "-1"));
                // ddlCaseOwnedBy.SelectedValue = "-1";

                SuppressedCase sc = new SuppressedCase();
                ddlCaseOwnedBy.DataSource = sc.CasesOwnedBy();
                ddlCaseOwnedBy.DataTextField = ApplicationConstants.EmloyeeName;
                ddlCaseOwnedBy.DataValueField = ApplicationConstants.ResourceId;
                ddlCaseOwnedBy.DataBind();
                ddlCaseOwnedBy.Items.Insert(0, new ListItem("All", "-1"));
                if (Request.QueryString["cmd"] == "mycases" || Request.QueryString["cmd"] == "overdueStages" || Request.QueryString["cmd"] == "overdueActions" || Request.QueryString["cmd"] == "nisp")
                {
                    if (Session[SessionConstants.CaseOwnedByDashboardSearch].ToString() == "-1")
                    {
                        ddlCaseOwnedBy.SelectedValue = "-1";
                    }
                    else
                    {
                        ddlCaseOwnedBy.SelectedValue = Session[SessionConstants.CaseOwnedByDashboardSearch].ToString();
                    }
                }
                else
                {
                    ddlCaseOwnedBy.SelectedValue = GetLoggedInUserID().ToString();
                }



            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Populate Case Owned By Suburb"

        public void PopulateCaseOwnedBySuburb()
        {
            try
            {
                caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();
                ddlCaseOwnedBy.DataSource = caseList.getCaseOwnerList();
                ddlCaseOwnedBy.DataTextField = "EmployeeName";
                ddlCaseOwnedBy.DataValueField = "ResourceId";
                ddlCaseOwnedBy.DataBind();
                ddlCaseOwnedBy.Items.Insert(0, new ListItem("All", "-1"));
                ddlCaseOwnedBy.SelectedValue = "-1";
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Populate Suburbs"

        public void PopulateSuburbs()
        {
            try
            {
                caseWorker = new AddCaseWorker();
                //commented by nadir 
                // ddlSuburb.DataSource = caseWorker.GetAllSuburbs(Convert.ToInt32(ddlRegion.SelectedValue));               
                ddlSuburb.DataSource = caseWorker.GetUserSuburbs(Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlSuburb.DataTextField = ApplicationConstants.schememname;
                ddlSuburb.DataValueField = ApplicationConstants.schemeId;
                ddlSuburb.DataBind();
                ddlSuburb.Items.Add(new ListItem("All", "-1"));
                ddlSuburb.SelectedValue = "-1";
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Populate Regions"

        public void PopulateRegions()
        {
            try
            {
                caseWorker = new AddCaseWorker();
                //ddlRegion.DataSource = caseWorker.GetAllRegion();
                //ddlRegion.DataSource = caseWorker.GetUserRegions();
                ddlRegion.DataSource = caseWorker.GetUserRegion(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlRegion.DataTextField = ApplicationConstants.regionname;
                ddlRegion.DataValueField = ApplicationConstants.regionId;
                ddlRegion.DataBind();
                ddlRegion.Items.Add(new ListItem("All", "-1"));
                ddlRegion.SelectedValue = "-1";
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"popUp Customer Name"

        public void popUpCustomerName()
        {
            LinkButton lbtn = new LinkButton();
            Label lbl = new Label();
            string url = string.Empty;

            for (int i = 0; i < pgvCaseList.Rows.Count; i++)
            {
                lbtn = (LinkButton)pgvCaseList.Rows[i].FindControl("lbtnName");
                url = string.Format("../../../Customer/CRM.asp?CustomerID={0}", lbtn.CommandArgument);
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        #endregion

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void setDefaultPagingAttributes()
        {
            //PageSize = Convert.ToInt32(ddlRecordsPerPage.SelectedValue);
            CurrentPage = 1;
            DbIndex = 0;
            TotalPages = 0;
            CurrentRecords = 0;
            TotalRecords = 0;
            RowIndex = -1;
            RecordsPerPage = ApplicationConstants.pagingPageSizeCaseList;
            setViewStateValues();

        }

        #endregion

        #region"Set ViewState Values"

        public void setViewStateValues()
        {
            ViewState[ViewStateConstants.currentPageCaseList] = CurrentPage;
            ViewState[ViewStateConstants.dbIndexCaseList] = DbIndex;
            ViewState[ViewStateConstants.totalPagesCaseList] = TotalPages;
            ViewState[ViewStateConstants.totalRecordsCaseList] = TotalRecords;
            ViewState[ViewStateConstants.rowIndexCaseList] = RowIndex;
            ViewState[ViewStateConstants.recordsPerPageCaseList] = RecordsPerPage;
            ViewState[ViewStateConstants.currentRecordsCaseList] = CurrentRecords;

        }

        public void setViewStateValues(string _regionId, string _caseOwnerId, string _suburbId, bool _allRegionFlag, bool _allCaseOwnerFlag, bool _allSuburbFlag, string _statusTitle, bool _isNISP, string _OverdueCheck)
        {
            ViewState[ViewStateConstants.regionIdCaseList] = _regionId;
            ViewState[ViewStateConstants.caseOwnerIdCaseList] = _caseOwnerId;
            ViewState[ViewStateConstants.suburbIdCaseList] = _suburbId;
            ViewState[ViewStateConstants.allRegionFlagCaseList] = _allRegionFlag;
            ViewState[ViewStateConstants.allCaseOwnerFlagCaseList] = _allCaseOwnerFlag;
            ViewState[ViewStateConstants.allSuburbFlagCaseList] = _allSuburbFlag;
            ViewState[ViewStateConstants.StatusTitleCaseList] = _statusTitle;
            ViewState[ViewStateConstants.IsNISPCaseList] = _isNISP;
            ViewState[ViewStateConstants.IsOverdueCheckList] = _OverdueCheck;

        }

        #endregion

        #region"Set Paging Labels"

        public void setPagingLabels()
        {
            try
            {

                int count = TotalRecords;

                if (count > Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    setCurrentRecordsDisplayed();
                    setTotalPages(count);
                    lblCurrentRecords.Text = (CurrentRecords).ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    lblCurrentRecords.Text = pgvCaseList.Rows.Count.ToString();
                    lblTotalRecords.Text = count.ToString();
                    lblCurrentPage.Text = CurrentPage.ToString();
                    lblTotalPages.Text = CurrentPage.ToString();
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    lbtnPrevious.Enabled = false;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    lbtnNext.Enabled = true;
                }
                //FileStream fs = null;
                //if (File.Exists("PagingConstants.txt"))
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Append);
                //}
                //else
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Create, FileAccess.ReadWrite);
                //}
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine(System.DateTime.Now.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.WriteLine("ToatlPages = " + TotalPages.ToString());
                //sw.WriteLine("Current Page = " + CurrentPage.ToString());
                //sw.WriteLine("Current Number of Records = " + CurrentRecords.ToString());
                //sw.WriteLine("Records in Grid View = " + pgvUsers.Rows.Count.ToString());
                //sw.WriteLine("Total Records = " + count.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.Close();
                //fs.Close();
                setViewStateValues();
            }
            catch (FileLoadException flException)
            {
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"Set Total Pages"

        public void setTotalPages(int count)
        {
            try
            {
                if (count % Convert.ToInt32(ddlRecordsPerPage.SelectedValue) > 0)
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue)) + 1;
                }
                else
                {
                    TotalPages = (count / Convert.ToInt32(ddlRecordsPerPage.SelectedValue));
                }
            }
            catch (DivideByZeroException divide)
            {
                IsError = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionTotalPages, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Displayed"

        public void setCurrentRecordsDisplayed()
        {
            try
            {
                if (pgvCaseList.Rows.Count == Convert.ToInt32(ddlRecordsPerPage.SelectedValue))
                {
                    CurrentRecords = CurrentPage * pgvCaseList.Rows.Count;
                }
                else if (RemoveFlag == true)
                {
                    CurrentRecords -= 1;
                }
                else
                {
                    CurrentRecords += pgvCaseList.Rows.Count;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionCurrentRecords, true);
                }
            }

        }

        #endregion

        #region"Set Total Records Count"


        public void setCurrentRecordsCount(string _postCode, int _regionId, int _caseOwnerId, int _suburbId, bool _allRegionFlag, bool _allCaseOwnerFlag, bool _allSuburbFlag, string _statusTitle, string surname, bool isNisp, string OverdueCheck)
        {
            try
            {
                caseList = new Am.Ahv.BusinessManager.Casemgmt.CaseList();
                TotalRecords = caseList.getRecordsCount(_postCode, _regionId, _caseOwnerId, _suburbId, _allRegionFlag, _allCaseOwnerFlag, _allSuburbFlag, _statusTitle, surname, isNisp, OverdueCheck);

            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionTotalRecords, true);
                }
            }
        }

        #endregion

        

        #region"Set Customer Name"

        public string SetCustomerName(string customerName1, string customerName2, string jointTenancyCount)
        {
            if (jointTenancyCount == "1")
            {
                return customerName1;
            }
            else
            {
                return customerName2 + " & " + customerName1;
            }
        }

        #endregion

        #endregion

        #region"Getters"

        #region"Get ViewState Values"

        public void getViewStateValues()
        {
            CurrentPage = Convert.ToInt32(ViewState[ViewStateConstants.currentPageCaseList]);
            DbIndex = Convert.ToInt32(ViewState[ViewStateConstants.dbIndexCaseList]);
            TotalPages = Convert.ToInt32(ViewState[ViewStateConstants.totalPagesCaseList]);
            CurrentRecords = Convert.ToInt32(ViewState[ViewStateConstants.currentRecordsCaseList]);
            TotalRecords = Convert.ToInt32(ViewState[ViewStateConstants.totalRecordsCaseList]);
            RowIndex = Convert.ToInt32(ViewState[ViewStateConstants.rowIndexCaseList]);
            RecordsPerPage = Convert.ToInt32(ViewState[ViewStateConstants.recordsPerPageCaseList]);
        }

        #endregion

        #region"Get Search Indexes"

        public void getSearchIndexes()
        {
            RegionId = Convert.ToInt32(ViewState[ViewStateConstants.regionIdCaseList]);
            CaseOwnerId = Convert.ToInt32(ViewState[ViewStateConstants.caseOwnerIdCaseList]);
            SuburbId = Convert.ToInt32(ViewState[ViewStateConstants.suburbIdCaseList]);
            AllRegionFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allRegionFlagCaseList]);
            AllCaseOwnerFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allCaseOwnerFlagCaseList]);
            allSuburbFlag = Convert.ToBoolean(ViewState[ViewStateConstants.allSuburbFlagCaseList]);
            StatusTitle = Convert.ToString(ViewState[ViewStateConstants.StatusTitleCaseList]);
            IsNisp = Convert.ToBoolean(ViewState[ViewStateConstants.IsNISPCaseList]);
            overdueCheck = Convert.ToString(ViewState[ViewStateConstants.IsOverdueCheckList]);
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Reduce Paging"

        public void reducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = false;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                getSearchIndexes();
                populateGridView(pCode, RegionId, CaseOwnerId, SuburbId, AllRegionFlag, AllCaseOwnerFlag, AllSuburbFlag, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), StatusTitle, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), IsNisp, OverdueCheck);
                setPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void increasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lbtnNext.Enabled = false;
                    lbtnPrevious.Enabled = true;
                }
                else
                {
                    lbtnNext.Enabled = true;
                    lbtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                getSearchIndexes();
                populateGridView(pCode, RegionId, CaseOwnerId, SuburbId, AllRegionFlag, AllCaseOwnerFlag, AllSuburbFlag, DbIndex * Convert.ToInt32(ddlRecordsPerPage.SelectedValue), Convert.ToInt32(ddlRecordsPerPage.SelectedValue), StatusTitle, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), IsNisp, OverdueCheck);
                setPagingLabels();
            }
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Check Boolean"

        public string checkBoolean(string value)
        {
            try
            {
                if (value != string.Empty)
                {
                    if (value == "1")
                    {
                        return "Yes";
                    }
                    else
                    {
                        return "No";
                    }
                }
                else
                {
                    return "No";
                }
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionCheckBoolean, true);
                }
            }
            return "No";
        }

        #endregion

        #region"Convert String Bit To Boolean"

        public Boolean ConvertStringBitToBoolean(string value)
        {
            if (value != string.Empty)
            {
                if (value == "0")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Check Dashboard Session"

        public bool CheckDashboardSession()
        {
            if (Session[SessionConstants.CaseOwnedByDashboardSearch] != null || !Convert.ToString(Session[SessionConstants.CaseOwnedByDashboardSearch]).Equals(string.Empty) ||
                Session[SessionConstants.RegionDashboardSearch] != null || !Convert.ToString(Session[SessionConstants.RegionDashboardSearch]).Equals(string.Empty) ||
                Session[SessionConstants.SuburbDashboardSearch] != null || !Convert.ToString(Session[SessionConstants.SuburbDashboardSearch]).Equals(string.Empty))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Show dashboard Search Criteria"

        public void ShowDashboardSearchCriteria(bool isFirstDetection, bool isNisp)
        {
            try
            {
                setDefaultPagingAttributes();
                initRegionLookup();
                PopulateCaseOwnedBy();
                PopulateRegions();
                if (Session[SessionConstants.CaseOwnedByDashboardSearch].ToString() == "-1")
                {
                    if (!isFirstDetection)
                    {

                        populateGridView(pCode, -1, -1, -1, true, true, true, DbIndex, ApplicationConstants.pagingPageSizeCaseList, string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), isNisp, OverdueCheck);
                        setCurrentRecordsCount(pCode, -1, -1, -1, true, true, true, string.Empty, Surname.Text, isNisp, OverdueCheck);

                    }
                    else
                    {

                        populateGridView(pCode, -1, -1, -1, true, true, true, DbIndex, ApplicationConstants.pagingPageSizeCaseList, "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), isNisp, OverdueCheck);
                        setCurrentRecordsCount(pCode, -1, -1, -1, true, true, true, "Initial Case Monitoring", Surname.Text, isNisp, OverdueCheck);

                    }
                }
                else
                {

                    if (!Convert.ToString(Session[SessionConstants.RegionDashboardSearch]).Equals("-1"))
                    {
                        ddlRegion.SelectedValue = Convert.ToString(Session[SessionConstants.RegionDashboardSearch]);
                        PopulateSuburbs();
                        if (!Convert.ToString(Session[SessionConstants.SuburbDashboardSearch]).Equals("-1"))
                        {
                            ddlSuburb.SelectedValue = Convert.ToString(Session[SessionConstants.SuburbDashboardSearch]);
                            if (!isFirstDetection)
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, DbIndex, ApplicationConstants.pagingPageSizeCaseList, string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), isNisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, string.Empty, Surname.Text, isNisp, OverdueCheck);

                            }
                            else
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, DbIndex, ApplicationConstants.pagingPageSizeCaseList, "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), isNisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlSuburb.SelectedValue), false, false, false, "Initial Case Monitoring", Surname.Text, isNisp, OverdueCheck);

                            }
                        }
                        else
                        {
                            if (!isFirstDetection)
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, false, false, true, DbIndex, ApplicationConstants.pagingPageSizeCaseList, string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), isNisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, false, false, true, string.Empty, Surname.Text, isNisp, OverdueCheck);

                            }
                            else
                            {

                                populateGridView(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, false, false, true, DbIndex, ApplicationConstants.pagingPageSizeCaseList, "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), isNisp, OverdueCheck);
                                setCurrentRecordsCount(pCode, Convert.ToInt32(ddlRegion.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, false, false, true, "Initial Case Monitoring", Surname.Text, isNisp, OverdueCheck);

                            }
                        }
                    }
                    else
                    {
                        if (!isFirstDetection)
                        {

                            populateGridView(pCode, -1, Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, true, false, true, DbIndex, ApplicationConstants.pagingPageSizeCaseList, string.Empty, Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), isNisp, OverdueCheck);
                            setCurrentRecordsCount(pCode, -1, Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, true, false, true, string.Empty, Surname.Text, isNisp, OverdueCheck);

                        }
                        else
                        {

                            populateGridView(pCode, -1, Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, true, false, true, DbIndex, ApplicationConstants.pagingPageSizeCaseList, "Initial Case Monitoring", Surname.Text, Convert.ToString(ViewState["SortExpression"]), Convert.ToString(ViewState["SortDirection"]), isNisp, OverdueCheck);
                            setCurrentRecordsCount(pCode, -1, Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), -1, true, false, true, "Initial Case Monitoring", Surname.Text, isNisp, OverdueCheck);

                        }
                    }
                }

                setPagingLabels();
                // RemoveDashboardSession();
                //int i = -1;
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionDashboardSearchCriteria, true);
                }
            }

        }

        #endregion

        #region"Remove Dashboard Session"

        public void RemoveDashboardSession()
        {
            Session.Remove(SessionConstants.CaseOwnedByDashboardSearch);
            Session.Remove(SessionConstants.RegionDashboardSearch);
            Session.Remove(SessionConstants.SuburbDashboardSearch);
        }

        #endregion

        #region FormatDateString
        public string FormatDate(DateTime value)
        {
            DateTime date = Convert.ToDateTime(value);
            return date.ToString("dd/MM/yyyy");
        }
        #endregion

        protected void pgvCaseList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        #region"sorting"

        protected void pgvCaseList_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["SortDirection"] == null)
            {
                ViewState["SortDirection"] = e.SortDirection;
            }

            //setDefaultPagingAttributes();
            getViewStateValues();
            getSearchIndexes();

            populateGridView(pCode, RegionId, CaseOwnerId, SuburbId, AllRegionFlag, AllCaseOwnerFlag, AllSuburbFlag, DbIndex, Convert.ToInt32(ddlRecordsPerPage.SelectedValue), StatusTitle, Surname.Text, e.SortExpression, GetSortDirection(e.SortExpression).ToString(), IsNisp, OverdueCheck);
            setCurrentRecordsCount(pCode, RegionId, CaseOwnerId, SuburbId, AllRegionFlag, AllCaseOwnerFlag, AllSuburbFlag, string.Empty, string.Empty, IsNisp, OverdueCheck);
            setPagingLabels();
            // List<Am.Ahv.Entities.AM_SP_GetCaseList_Result> Dataset = Session["CaseListDS"] as List<Am.Ahv.Entities.AM_SP_GetCaseList_Result>;
            //SortGrid<Am.Ahv.Entities.AM_SP_GetCaseList_Result>(Dataset, e, GetSortDirection(e.SortExpression));

        }


        public void SortGrid<T>(IEnumerable<T> dataSource, GridViewSortEventArgs e, SortDirection sortDirection)
        {
            e.SortDirection = sortDirection;
            IEnumerable<T> set = pagebase.PageBase.SortGrid<T>(dataSource, e);
            pgvCaseList.DataSource = set;
            pgvCaseList.DataBind();
        }

        //protected string GridViewSortExpression
        //{
        //    get { return ViewState["SortExpression"] as string ?? string.Empty; }
        //    set { ViewState["SortExpression"] = value; }
        //}



        private string GetSortDirection(string column)
        {

            // By default, set the sort direction to ascending.
            // SortDirection sortDirection = SortDirection.Ascending;

            // Retrieve the last column that was sorted.
            string sortExpression = Convert.ToString(ViewState["SortExpression"]);

            if (sortExpression != null)
            {
                // Check if the same column is being sorted.
                // Otherwise, the default value can be returned.
                if (sortExpression.ToUpper() == column.ToUpper())
                {
                    string lastDirection = Convert.ToString(ViewState["SortDirection"]);
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        ViewState["SortDirection"] = "DESC";
                        //sortDirection = SortDirection.Descending;
                    }
                    else
                    {
                        ViewState["SortDirection"] = "ASC";
                    }
                }
                else
                {
                    ViewState["SortDirection"] = "DESC";
                }
            }

            // Save new values in ViewState.
            //ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return ViewState["SortDirection"].ToString();
        }

        #endregion


        #region FormatDateString
        public string FormatDate(string value)
        {
            DateTime date = Convert.ToDateTime(value);
            return date.ToString("dd/MM/yyyy");
        }
        #endregion

        #region"Set Single Value Bracs and Color"
        public string SetBracs(string value)
        {
            return val.SetBracs(value);
        }


        public System.Drawing.Color SetForeColor(string value)
        {
            return val.SetForeColor(value);
        }

        #endregion

        #region"Set Amount and date Bracs and Color"

        public string SetBracs(string date, string amount)
        {
            return val.SetBracs(date, amount);
        }

        public System.Drawing.Color SetForeColor(string date, string amount)
        {
            return val.SetForeColor(date, amount);
        }

        #endregion

        public void GetCustomerInformation()
        {
            try
            {
                BusinessManager.Customers.Customers customerManager = new BusinessManager.Customers.Customers();

                if (!Convert.ToString(Session[SessionConstants.TenantId]).Equals(string.Empty) && !Convert.ToString(Session[SessionConstants.CustomerId]).Equals(string.Empty))
                {
                    AM_SP_GetCustomerInformation_Result customer = customerManager.GetCustomerInformation(Convert.ToInt32(Session[SessionConstants.TenantId]), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    PopulateCustomerInformation(customer);
                }
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptingGettingUserInfo, true);
                }
            }
        }

        #region"Populate Customer Information"

        public void PopulateCustomerInformation(AM_SP_GetCustomerInformation_Result customer)
        {
            //Nouman added the code
            if (customer != null)
            {
                double rent = Convert.ToDouble(customer.RentBalance);//Convert.ToDouble(RentBalance.CalculateRentBalance(Convert.ToDouble(customer.RentBalance), Convert.ToDouble(customer.rentAmount)));

                Session[SessionConstants.OwedTBHA] = Math.Round(Convert.ToDouble(customer.OwedToBHA), 2);
                Session[SessionConstants.lastPayment] = Math.Round(Convert.ToDouble(customer.LastPayment), 2).ToString();
                Session[SessionConstants.Rentbalance] = Math.Round(rent, 2).ToString();
                Session[SessionConstants.WeeklyRentAmount] = customer.rentAmount.Value;
                Session[SessionConstants.LastMonthNetArrears] = customer.LastMonthNetArrears.ToString();
                SetLastPaymentInformation();
            }

        }

        #endregion


        #region"Set Last Payment Information"

        public void SetLastPaymentInformation()
        {
            try
            {
                AM_SP_GetCustomerLastPayment_Result lastPayment = new AM_SP_GetCustomerLastPayment_Result();
                customer = new CustomersReport();

                if (!Convert.ToString(Session[SessionConstants.CustomerId]).Equals(string.Empty))
                {
                    lastPayment = customer.GetCustomerLastPayment(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    if (lastPayment != null)
                    {
                        if (lastPayment.lastPayment.ToString().Equals(string.Empty))
                        {

                        }
                        else
                        {
                            Session[SessionConstants.HBPayment] = lastPayment.lastPayment;
                        }

                        double val = RentBalance.CalculateHBCyclePayment(Math.Abs(Convert.ToDouble(lastPayment.lastPayment)));
                        Session[SessionConstants.HBCyclePayment] = val;

                    }
                    else
                    {

                    }

                }
            }
            catch (NullReferenceException nullException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsError = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsError = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptiongettinglastPayment, true);
                }
            }
        }

        #endregion

        protected void ddlCaseOwnedBy_SelectedIndexChanged(object sender, EventArgs e)
        {

            PopulateRegions();


            ddlSuburb.DataSource = ApplicationConstants.emptyDataSource;
            ddlSuburb.DataTextField = string.Empty;
            ddlSuburb.DataValueField = string.Empty;
            ddlSuburb.DataBind();
            ddlSuburb.Items.Add(new ListItem("All", "-1"));
            ddlSuburb.SelectedValue = "-1";





        }

        #endregion

        protected void Surname_TextChanged(object sender, EventArgs e)
        {

        }
    }
}


