﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using System.Configuration;
using Am.Ahv.Website.pagebase;
using System.Data;
using Am.Ahv.BusinessManager.Customers;

namespace Am.Ahv.Website.secure.casemgt
{
    public partial class GetCustomerRentParametersHistory : System.Web.UI.Page
    {
        #region"Page Load"

        /// <summary>
        /// Schedule Job 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (RegisterCustomerRentParametersHistoryScheduleJobStatus())
            {
                GetCustomersRentHistory();
            }
            else
            {
                SetMessage(UserMessageConstants.JobInProgressCustomerRentParametersHistory, true);
            }

        }

        #endregion

        #region"Register Customer Rent Parameters History Schedule Job Status"

        public bool RegisterCustomerRentParametersHistoryScheduleJobStatus()
        {
            try
            {
                Customers customerManager = new Customers();
                return customerManager.RegisterCustomerRentParametersHistoryScheduleJobStatus();
            }
            catch (EntityException ee)
            {
                ExceptionPolicy.HandleException(ee, "Exception Policy");
                return false;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return false;
            }
        }

        #endregion

        #region"Get Customers Rent History"

        public void GetCustomersRentHistory()
        {
            Customers customerManager = new Customers();
            try
            {
                int timer = int.Parse(ConfigurationManager.AppSettings["ConnectionTimer"]);
                customerManager.GetCustomersRentParametersHistory(timer);
                string emailBody = "It is to notify you that get customers rent parameters history schedule job has been completed successfully." +
                                        "<br /><br /><br /><br />This is a sytem generated email do not reply to this email.";
                if (PageBase.SendEmailNotification(0, true, UserMessageConstants.EmailSuccessSubjectCustomerRentParametersHistory, emailBody))
                {
                    customerManager.EndCustomerRentParametersHistoryScheduleJob();
                    SetMessage(UserMessageConstants.successFindCustomerRentParametersHistory, false);
                }
            }
            catch (EntityException ee)
            {
                ExceptionPolicy.HandleException(ee, "Exception Policy");

                try
                {
                    PageBase.SendEmailNotification(0, false, UserMessageConstants.EmailFailureSubjectCustomerRentParametersHistory, UserMessageConstants.FailureEmailBodyFindFirstDetectionCustomers);
                    customerManager.ErrorCustomerRentParametersHistoryScheduleJob(ee.InnerException.ToString());
                    SetMessage(UserMessageConstants.ErrorCustomerRentParametersHistory, true);
                }
                catch (Exception ex)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                    customerManager.ErrorCustomerRentParametersHistoryScheduleJob(ex.InnerException.ToString());
                    SetMessage(UserMessageConstants.ErrorInEmailCustomerRentParametersHistory, true);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                try
                {
                    PageBase.SendEmailNotification(0, false, UserMessageConstants.EmailFailureSubjectCustomerRentParametersHistory, UserMessageConstants.FailureEmailBodyFindFirstDetectionCustomers);
                    customerManager.ErrorCustomerRentParametersHistoryScheduleJob(ex.Message.ToString());
                    SetMessage(UserMessageConstants.ErrorCustomerRentParametersHistory, true);
                }
                catch (Exception exInner)
                {
                    ExceptionPolicy.HandleException(exInner, "Exception Policy");
                    customerManager.ErrorCustomerRentParametersHistoryScheduleJob(ex.Message.ToString());
                    SetMessage(UserMessageConstants.ErrorInEmailCustomerRentParametersHistory, true);
                }
            }
        }

        #endregion 

        /// <summary>
        /// Sets the success or error message. 
        /// </summary>
        /// <param name="str">text/message success or error </param>
        /// <param name="isError"> true or false</param>
        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
    }
}