﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FirstDetectionList.aspx.cs"
    Inherits="Am.Ahv.Website.secure.casemgt.FirstDetectionList" MasterPageFile="~/masterpages/AMMasterPage.Master"
    Title="First Detection List :: Arrears Management" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style4
        {
            width: 200px;
        }
        .style9
        {
            width: 17%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnlCaseList" Height="400px" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 10px">
                    <asp:Label ID="lblFirstDetection" runat="server" Font-Bold="true" ForeColor="Black"
                        Font-Size="Medium" Text="Initial Case Monitoring"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdClass" colspan="6" style="padding-left: 25px">
                    <asp:UpdatePanel ID="updpnlDropDowns" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCaseOwnedBy" runat="server" Text="Case owned by:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCaseOwnedBy" AutoPostBack="true" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlCaseOwnedBy_SelectedIndexChanged">
                                            <asp:ListItem Text="All" Value="-1">
                                            </asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRegion" runat="server" Text="Patch:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRegion" AutoPostBack="true" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged">
                                            <asp:ListItem Text="All" Value="-1">
                                            </asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSuburb" runat="server" Text="Scheme:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlSuburb" AutoPostBack="false" Width="310px" runat="server"
                                            OnSelectedIndexChanged="ddlSuburb_SelectedIndexChanged">
                                            <asp:ListItem Text="All" Value="0">
                                            </asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSurname" runat="server" Text="Surname:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSurname" runat="server" Width="300px"></asp:TextBox>&nbsp;&nbsp;
                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAddress" runat="server" Text="Address:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAddress" runat="server" Width="300px"></asp:TextBox>&nbsp;&nbsp;
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblTenancyid" runat="server" Text="Tenancy No:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTenancyid" runat="server" Width="300px"></asp:TextBox>&nbsp;&nbsp;
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="6" valign="top" class="tdClass" style="padding-left: 20px; padding-right: 15px">
                    <asp:UpdatePanel ID="updpnlFirsDetection" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <!-- First detection grid View Panel-->
                            <asp:Panel ID="pnlGridFirstDetection" runat="server">
                                <cc1:PagingGridView ID="pgvFirstDetection" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true"
                                    EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center"
                                    AllowPaging="false" GridLines="None" runat="server" AutoGenerateColumns="false"
                                    OnSorting="pgvFirstDetection_Sorting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="1st Detected:" HeaderStyle-HorizontalAlign="Left"
                                            ShowHeader="true" SortExpression="FirstDetectionDate" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="12%" Height="10px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl1stDetection" runat="server" Text='<%#DateTime.Parse(Eval("FirstDetectionDate").ToString()).ToString("dd/MM/yyyy") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Days:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="AM_FirstDetecionList.FirstDetectionDate " HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="7%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblDays" runat="server" Text='<%# this.GetDays(Eval("FirstDetectionDate").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenancy:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="AM_FirstDetecionList.TENANCYID" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" Height="10px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTenancyId" runat="server" Text='<%#Eval("TENANCYID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="CustomerName" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="13%" Height="10px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblName" runat="server" Text='<%#Eval("CustomerName") %>'></asp:Label>--%>
                                                <asp:LinkButton ID="lbtnName" OnClick="lbtnName_Click" CommandArgument='<%#Eval("CUSTOMERID") %>'
                                                    runat="server" Text='<%# this.SetCustomerName(Eval("CustomerName").ToString(), Eval("CustomerName2").ToString(), Eval("JointTenancyCount").ToString()) %>' ForeColor="Black"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="CustomerAddress" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="20%" Height="10px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("CustomerAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance:" ShowHeader="true" SortExpression="RentBalance"
                                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("RentBalance").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("RentBalance").ToString()) %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Owed To BHA:" SortExpression="OwedToBHA" ShowHeader="true"
                                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblowedToBHA" runat="server" ForeColor='<%# this.SetForeColor(Eval("OwedToBHA").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("OwedToBHA").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                     
                                        <asp:TemplateField HeaderText="Last Payment:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="LastCPAY" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastPayment" runat="server" ForeColor='<%# this.SetForeColor(Eval("TRANSACTIONDATE").ToString(), Eval("LastCPAY").ToString()) %>' Text='<%# this.SetBracs(Eval("TRANSACTIONDATE").ToString(),Eval("LastCPAY").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Est HB:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black" SortExpression="EstimatedHBDue">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastCPAY" runat="server" ForeColor='<%# this.SetForeColor(Eval("EstimatedHBDue").ToString()) %>'
                                                    Text='<%#this.SetBracs(Eval("EstimatedHBDue").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                          <asp:TemplateField HeaderText="Next DD:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="NextAmount" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblNextPayment" runat="server" ForeColor='<%# this.SetForeColor(Convert.ToString(Eval("NextDD")), Eval("NextAmount").ToString()) %>' Text='<%# this.SetBracs(Convert.ToString(Eval("NextDD")),Eval("NextAmount").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                      
                                      
                                        <asp:TemplateField>
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomerId" Visible="false" runat="server" Text='<%#Eval("CUSTOMERID") %>'></asp:Label>
                                                <asp:Button ID="btnOpenNewCase" OnClick="btnOpenNewCase_Click" runat="server" Width="75px"
                                                    CommandArgument='<%#Eval("TENANCYID") %>' Text="New Case" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <PagerSettings Mode="NumericFirstLast" />
                                </cc1:PagingGridView>
                            </asp:Panel>

                            <!-- Previous tenants grid View Panel-->

                            <asp:Panel ID="pnlGridPreviousTenants" Visible="false" runat="server">
                                <cc1:PagingGridView ID="pgvPreviousTenants" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true"
                                    EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center"
                                    AllowPaging="false" GridLines="None" runat="server" AutoGenerateColumns="false"
                                    OnSorting="pgvFirstDetection_Sorting">
                                    <Columns>                                        
                                        <asp:TemplateField HeaderText="Tenancy:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="AM_Customer_Rent_Parameters.TENANCYID" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" Height="10px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblTenancyId" runat="server" Text='<%#Eval("TENANCYID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="CustomerName" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="13%" Height="10px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblName" runat="server" Text='<%#Eval("CustomerName") %>'></asp:Label>--%>
                                                <asp:LinkButton ID="lbtnName" OnClick="lbtnName_Click" CommandArgument='<%#Eval("CUSTOMERID") %>'
                                                    runat="server" Text='<%#Eval("CustomerName") %>' ForeColor="Black"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Address:" HeaderStyle-HorizontalAlign="Left" ShowHeader="true"
                                            SortExpression="CustomerAddress" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="20%" Height="10px" HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("CustomerAddress") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent Balance:" ShowHeader="true" SortExpression="RentBalance"
                                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnRentBalance" runat="server" ForeColor='<%# this.SetForeColor(Eval("RentBalance").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("RentBalance").ToString()) %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Owed To BHA:" SortExpression="RentBalance" ShowHeader="true"
                                            HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblowedToBHA" runat="server" ForeColor='<%# this.SetForeColor(Eval("OwedToBHA").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("OwedToBHA").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="Last Payment:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            SortExpression="LastCPAY" HeaderStyle-ForeColor="black">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastPayment" runat="server" ForeColor='<%# this.SetForeColor(Eval("LastCPAY").ToString()) %>'
                                                    Text='<%# this.SetBracs(Eval("LastCPAY").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Est HB:" ShowHeader="true" HeaderStyle-HorizontalAlign="Left"
                                            HeaderStyle-ForeColor="black" SortExpression="EstimatedHBDue">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="10%" HorizontalAlign="Left" Height="10px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastCPAY" runat="server" ForeColor='<%# this.SetForeColor(Eval("EstimatedHBDue").ToString()) %>'
                                                    Text='<%#this.SetBracs(Eval("EstimatedHBDue").ToString()) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCustomerId" Visible="false" runat="server" Text='<%#Eval("CUSTOMERID") %>'></asp:Label>
                                                <asp:Label ID="lblTenantId" runat="server" Visible ="false" Text='<%# Eval("TenancyId")%>'></asp:Label>
                                                <asp:Label ID="lblCaseId" runat="server" Visible ="false" Text='<%# Eval("CaseId")%>'></asp:Label>
                                                <asp:Button ID="btnOpenNewCase" OnClick="btnOpenNewCase_Click" runat="server" Width="75px"
                                                    CommandArgument='<%#Eval("TENANCYID") %>' Text="New Case" />
                                                <asp:Button ID="btnViewCase" OnClick="btnViewCase_Click" CommandArgument='<%#Eval("CaseId") %>' runat="server" Text="View Case" Visible=false />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" ForeColor="Red" HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <PagerSettings Mode="NumericFirstLast" />
                                </cc1:PagingGridView>
                            </asp:Panel>
                            <table align="center" class="tableFooter">
                                <tr>
                                    <td class="style4">
                                        <asp:Label ID="lblRecords" runat="server" Text="Records"></asp:Label>&nbsp;
                                        <asp:Label ID="lblCurrentRecords" runat="server"></asp:Label>&nbsp;
                                        <asp:Label ID="lblOf" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:Label ID="lblTotalRecords" runat="server"></asp:Label>
                                    </td>
                                    <td style="padding-left: 20px" class="style4">
                                        <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label>&nbsp;
                                        <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>&nbsp;
                                        <asp:Label ID="lblOfPage" runat="server" Text="of"></asp:Label>&nbsp;
                                        <asp:Label ID="lblTotalPages" runat="server"></asp:Label>
                                    </td>
                                    <td style="padding-left: 10px" class="style9">
                                        <asp:LinkButton ID="lbtnPrevious" runat="server" Text="< Previous" OnClick="llbtnPrevious_Click"></asp:LinkButton>
                                    </td>
                                    <td style="padding-left: 10px" class="style9">
                                        <asp:LinkButton ID="lbtnNext" runat="server" Text="Next >" OnClick="llbtnNext_Click"></asp:LinkButton>
                                    </td>
                                    <td class="style4">
                                        <asp:Label ID="lblRecordsPerPage" runat="server" Text="Records per page:"></asp:Label>
                                    </td>
                                    <td width="5%">
                                        <asp:DropDownList ID="ddlRecordsPerPage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRecordsPerPage_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <asp:UpdateProgress ID="UpdateProgress2" runat="server">
                                <ProgressTemplate>
                                    <span class="Error">Please wait...</span>
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="6" class="tdClass">
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
