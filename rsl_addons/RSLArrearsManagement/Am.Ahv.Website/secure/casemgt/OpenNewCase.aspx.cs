using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Customers;
using System.Collections;
using Am.Ahv.Entities;

using Am.Ahv.Utilities.constants;


using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.BusinessManager.payments;
using Am.Ahv.BusinessManager.Casemgmt;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Am.Ahv.Website.secure.casemgt
{
    public partial class OpenNewCase : PageBase
    {
        #region "Events"       

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!base.CheckSession())
            {
                Response.Redirect(PathConstants.BridgePath);
                //RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                if (Request.QueryString["cmd"] == "open")
                {
                    if (Validation.CheckIntegerValue(Request.QueryString["id"]))
                    {
                        base.SetTenantId(Convert.ToInt32(Request.QueryString["id"]));
                    }
                }

                regularPaymentPlan.DisablePaymentPlanFields();
             
                //this.loadUserSummary();
                //this.loadCaseSettings();
                //ViewState["OutStanding_Arrears_Amount"] = 1200;  // this is to be decided that from where we will get these values

            }

            #region"Commented Because of the New Update (Didn't Deleted the Code for future use"

            /*
             * 
             * this part of code was mentioned in the post back block
             
                if (CheckPaymentPlan())
                {
                    if (CheckPaymentPlanType())
                    {
                        flexiblePaymentDiv.Visible = true;
                        regularPaymentDiv.Visible = false;
                        flexiblePlan.FirstLookPaymentPlan(false);
                        Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Flexible";
                    }
                    else
                    {
                        flexiblePaymentDiv.Visible = false;
                        regularPaymentDiv.Visible = true;
                        regularPaymentPlan.FirstLookPaymentPlan(false);
                        Session[SessionConstants.PaymentPlanTypePaymentPlan] = "Regular";
                    }
                }
             
             */

            //flexiblePlan.invokeEvent += delegate(bool flag, string value)
            //{
            //    if (flag == true)
            //    {
            //        flexiblePaymentDiv.Visible = false;
            //        regularPaymentDiv.Visible = true;
            //        updpnlDiv.Update();
            //        regularPaymentPlan.ControlLoad(value);
            //    }
            //    else
            //    {
            //        flexiblePaymentDiv.Visible = false;
            //        regularPaymentDiv.Visible = true;
            //        updpnlDiv.Update();
            //        regularPaymentPlan.ControlLoad(value);
            //        regularPaymentPlan.DisablePaymentPlan();
            //        regularPaymentPlan.DisableCancelButton();
            //        regularPaymentPlan.ShowUpdateButton();
            //    }

            //};

            //regularPaymentPlan.visibleFlexiblePaymentPlan += delegate(bool flag, string value)
            //{
            //    if (flag == true)
            //    {
            //        flexiblePaymentDiv.Visible = true;
            //        regularPaymentDiv.Visible = false;
            //        updpnlDiv.Update();
            //        flexiblePlan.ControlLoad(value);
            //        // flexiblePlan.updateControl();
            //        //updpnlDiv.Update();
            //    }
            //    else
            //    {
            //        flexiblePaymentDiv.Visible = true;
            //        regularPaymentDiv.Visible = false;
            //        updpnlDiv.Update();
            //        flexiblePlan.ControlLoad(value);
            //        flexiblePlan.DisablePaymentPlan();
            //        flexiblePlan.DisableCancelButton();
            //        flexiblePlan.ShowUpdateButton();
            //    }
            //};

            //regularPaymentPlan.OpenCaseAndRegularPaymentPlan += delegate(bool value)
            //{
            //    if (value == true)
            //    {
            //        bool exception = false;
            //        try
            //        {
            //            AM_PaymentPlan paymentPlan = regularPaymentPlan.SetPaymentPlan();
            //            AM_PaymentPlanHistory paymentPlanHistory = regularPaymentPlan.SetPaymentPlanHistory();
            //            List<AM_Payment> listPayments = regularPaymentPlan.SavePayments(paymentPlan);

            //            AM_Case amCase = caseSettings.SetCaseFields();
            //            AM_CaseHistory caseHistory = caseSettings.SetCaseHistoryFields();

            //            ArrayList arrayListStandardLetters = caseSettings.GetArrayListStandardLetter();
            //            ArrayList arrayListDocumentName = caseSettings.GetArrayListDocumentName();
            //            DataTable dtActionData = caseSettings.GetActionDatatable();

            //            Am.Ahv.BusinessManager.statusmgmt.Status statusManager = new BusinessManager.statusmgmt.Status();
            //            Am.Ahv.BusinessManager.statusmgmt.Action actionManager = new BusinessManager.statusmgmt.Action();

            //            amCase.ActionHistoryId = actionManager.GetActionHistoryByActionId(amCase.ActionId);
            //            caseHistory.ActionHistoryId = amCase.ActionHistoryId;

            //            amCase.StatusHistoryId = statusManager.GetStatusHistoryIdByStatus(amCase.StatusId);
            //            caseHistory.StatusHistoryId = amCase.StatusHistoryId;

            //            OpenCase openCaseManager = new OpenCase();
            //            int id = openCaseManager.SavePaymentPlanAndOpenNewCase(paymentPlan, paymentPlanHistory, listPayments, amCase, caseHistory, arrayListDocumentName, arrayListStandardLetters, base.GetTenantId(), base.GetLoggedInUserID(), dtActionData);
            //            if (id > 0)
            //            {
            //                Session.Remove(SessionConstants.OpenCaseFlag);
            //                base.SetCaseId(id);
            //                Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
            //            }
            //            else
            //            {
            //                regularPaymentPlan.SetMessage(UserMessageConstants.exceptingSavingPaymentPlan, true);
            //                caseSettings.SetMessage(UserMessageConstants.exceptingSavingCase, true);
            //            }
            //        }
            //        catch (EntityException ee)
            //        {
            //            exception = true;
            //            ExceptionPolicy.HandleException(ee, "Exception Policy");
            //        }
            //        catch (Exception ex)
            //        {
            //            exception = true;
            //            ExceptionPolicy.HandleException(ex, "Exception Policy");
            //        }
            //        finally
            //        {
            //            if (exception)
            //            {
            //                regularPaymentPlan.SetMessage(UserMessageConstants.exceptingSavingPaymentPlan, true);
            //                caseSettings.SetMessage(UserMessageConstants.exceptingSavingCase, true);
            //            }
            //        }
            //    }
            //};

            //flexiblePlan.OpenCaseAndFlexiblePaymentPlan += delegate(bool value)
            //{
            //    if (value)
            //    {
            //        bool exception = false;
            //        try
            //        {
            //            AM_PaymentPlan paymentPlan = flexiblePlan.SetPaymentPlan();
            //            AM_PaymentPlanHistory paymentPlanHistory = flexiblePlan.SetPaymentPlanHistory();
            //            List<AM_Payment> listPayments = flexiblePlan.SavePayments(paymentPlan);

            //            AM_Case amCase = caseSettings.SetCaseFields();
            //            AM_CaseHistory caseHistory = caseSettings.SetCaseHistoryFields();

            //            ArrayList arrayListStandardLetters = caseSettings.GetArrayListStandardLetter();
            //            ArrayList arrayListDocumentName = caseSettings.GetArrayListDocumentName();
            //            DataTable dtActionData = caseSettings.GetActionDatatable();

            //            Am.Ahv.BusinessManager.statusmgmt.Status statusManager = new BusinessManager.statusmgmt.Status();
            //            Am.Ahv.BusinessManager.statusmgmt.Action actionManager = new BusinessManager.statusmgmt.Action();

            //            amCase.ActionHistoryId = actionManager.GetActionHistoryByActionId(amCase.ActionId);
            //            caseHistory.ActionHistoryId = amCase.ActionHistoryId;

            //            amCase.StatusHistoryId = statusManager.GetStatusHistoryIdByStatus(amCase.StatusId);
            //            caseHistory.StatusHistoryId = amCase.StatusHistoryId;

            //            OpenCase openCaseManager = new OpenCase();
            //            int id = openCaseManager.SavePaymentPlanAndOpenNewCase(paymentPlan, paymentPlanHistory, listPayments, amCase, caseHistory, arrayListDocumentName, arrayListStandardLetters, base.GetTenantId(), base.GetLoggedInUserID(), dtActionData);
            //            if (id > 0)
            //            {
            //                Session.Remove(SessionConstants.OpenCaseFlag);
            //                base.SetCaseId(id);
            //                Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
            //            }
            //            else
            //            {
            //                regularPaymentPlan.SetMessage(UserMessageConstants.exceptingSavingPaymentPlan, true);
            //                caseSettings.SetMessage(UserMessageConstants.exceptingSavingCase, true);
            //            }
            //        }
            //        catch (EntityException ee)
            //        {
            //            exception = true;
            //            ExceptionPolicy.HandleException(ee, "Exception Policy");
            //        }
            //        catch (Exception ex)
            //        {
            //            exception = true;
            //            ExceptionPolicy.HandleException(ex, "Exception Policy");
            //        }
            //        finally
            //        {
            //            if (exception)
            //            {
            //                regularPaymentPlan.SetMessage(UserMessageConstants.exceptingSavingPaymentPlan, true);
            //                caseSettings.SetMessage(UserMessageConstants.exceptingSavingCase, true);
            //            }
            //        }
            //    }
            //};

            #endregion

            Session[SessionConstants.PaymentPlanBackPage] = PathConstants.openNewCase;
        }

        #endregion

        #region"btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.firstDetectionList);
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Check Payment Plan"

        public bool CheckPaymentPlan()
        {
            OpenCase openCase = new OpenCase();
            return openCase.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Check Payment Plan Type"

        public bool CheckPaymentPlanType()
        {
            Payments paymentmanager = new Payments();
            string str = paymentmanager.GetPaymentPlanType(base.GetTenantId());
            if (str.Equals("Flexible"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        //protected void loadUserSummary()
        //{
        //   // Customers customer = new Customers();
        //    ArrayList arrayList = customer.getCustomerDatails(5);

        //    C__CUSTOMER customerEntity = (C__CUSTOMER)arrayList[0];
        //    C_ADDRESS addressEntity = (C_ADDRESS)arrayList[1];
        //    C_TENANCY tenancyEntity = (C_TENANCY)arrayList[2];
        //    F_RENTJOURNAL rentJournal = (F_RENTJOURNAL)arrayList[3];

        //    // F_RENTJOURNAL=customer. 
        //    //C_ADDRESS customerAddressEntity=customer.get
        //    //C__CUSTOMER customerRecord ;//=(C__CUSTOMER) list[0];

        //    userInforSummary.SetInformationOfCustomer(customerEntity, addressEntity);

        //    //this.lastPaymnt.setLastPaymentInformation(tenancyEntity, rentJournal);
        //    //  userInforSummary.setInformation(@"Ali Zafar", @"H.No T157/Gt New Town Cantt Lahore Pakistan", @"Bingo2", @"$1235", @"$2300", @"24/03/2010");


        //}



        protected void loadCaseSettings()
        {
            //Dictionary<int, String> caseManagerDict = new Dictionary<int, String>();
            //caseManagerDict.Add(-1, "Select One");
            //caseManagerDict.Add(1, "Ali");
            //caseManagerDict.Add(2, "Noor Muhammad");

           // this.caseSettings.setUpCaseSetting(caseManagerDict);
        }

        #endregion
    }
}