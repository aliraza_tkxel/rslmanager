﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddCaseWorker.ascx.cs"
    Inherits="Am.Ahv.Website.controls.casemgt.AddCaseWorker" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style5
    {
        width: 401px;
    }
    .style7
    {
        width: 90px;
    }
</style>
<table class="tableClass">
    <tr>
        <td class="tableHeader">
            Add Case Worker
        </td>
    </tr>
    <tr>
        <td class="style5">
        
<asp:Panel ID="CaseWorkerPanel" runat="server" Width="100%">
    <asp:UpdatePanel ID="caseworkerup" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table cellpadding="5px">
                <tr>
                    <td valign="middle" align="left" colspan="3">
                        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="style7" valign="top">
                    <asp:Label ID="lblquickfind" runat="server" Text="Quick Find:"></asp:Label>
                    </td>
                    <td valign="middle">
                        &nbsp;</td>
                    <td  align="left"  valign="top">
                        <asp:TextBox ID="quickfindtxt" runat="server" Width="175px"></asp:TextBox>&nbsp;
                        <asp:Button ID="findbtn" Text="Find" runat="server" Width="66px" OnClick="findbtn_Click" />
                        <br />
                        <cc1:AutoCompleteExtender ID="quickfindtxt_AutoCompleteExtender" runat="server" Enabled="True"
                            ServicePath="~/services/SearchNames.asmx" CompletionInterval="50" TargetControlID="quickfindtxt"
                            UseContextKey="True" ServiceMethod="GetCompletionList" MinimumPrefixLength="2"
                            EnableCaching="true" CompletionSetCount="12">
                        </cc1:AutoCompleteExtender>                       
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorStatustext" ControlToValidate="quickfindtxt"
                            runat="server" ErrorMessage="150 Max Length" ValidationExpression="^[\s\S]{0,150}$"></asp:RegularExpressionValidator>
                    </td>                    
                </tr>
                <tr>
                    <td valign="top" class="style7">
                        <asp:Label ID="lbltype" runat="server" Text="Type:"></asp:Label>
                    </td>
                    <td valign="middle">
                        <asp:Label ID="lblTypeDDL" runat="server" Font-Bold="true" ForeColor="Red" 
                            Text="*"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="typeddl" runat="server" Width="175px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style7" valign="top">
                        <asp:Label ID="lblteam" runat="server" Text="Team:"></asp:Label>
                    </td>
                    <td valign="middle">
                        <asp:Label ID="lblTeamrequired" runat="server" Font-Bold="true" ForeColor="Red" 
                            Text="*"></asp:Label>
                    </td>
                    <td align="left"  valign="top">
                        <asp:DropDownList ID="teamddl" runat="server" AutoPostBack="true" 
                            OnSelectedIndexChanged="teamddl_SelectedIndexChanged" Width="175px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style7" valign="top">
                      <asp:Label ID="lblteammember" runat="server" Text="Team Member:"></asp:Label>
                    </td>
                    <td valign="middle">
                        <asp:Label ID="lblTeamMemberRequired" runat="server" Font-Bold="true" 
                            ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left"  valign="top">
                        <asp:DropDownList ID="teammemberddl" runat="server" Width="175px">
                        </asp:DropDownList>
                    </td>
                </tr>                
                <tr>
                    <td class="style7" valign="top">
                   <asp:Label ID="lblselectregion" runat="server" Text="Patch:"></asp:Label>
                    </td>
                    <td valign="top" style=" padding-top:10px;">
                        <asp:Label Visible="false" ID="lblregionrequired" runat="server" Font-Bold="true" 
                            ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left"  valign="top">
                        <asp:DropDownList ID="regionddl" runat="server" Width="175px" 
                            AutoPostBack="True" OnSelectedIndexChanged="regionddl_SelectedIndexChanged">
                        </asp:DropDownList>
                        <br />
                        <br />
                        <asp:Button ID="raddbtn" Text="Add" runat="server" Width="66px" 
                            OnClick="raddbtn_Click" />&nbsp;
                        <asp:Button ID="rremovebtn" Text="Remove" runat="server" Width="66px" 
                            OnClick="rremovebtn_Click" />
                    </td>                                        
                </tr>
                <tr>
                    <td class="style7" valign="top">
                    </td>
                    <td valign="middle">
                        &nbsp;</td>
                    <td align="left"  valign="top">
                        <asp:ListBox ID="regionlbox" runat="server" Width="175px" AutoPostBack="true" 
                            onselectedindexchanged="regionlbox_SelectedIndexChanged"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td class="style7" valign="top">
                   <asp:Label ID="lblsuburb" runat="server" Text="Scheme:"></asp:Label>
                    </td>
                    <td valign="middle">
                        &nbsp;</td>
                    <td align="left"  valign="top">
                        <asp:DropDownList ID="suburbddl" runat="server" Width="175px" 
                            AutoPostBack="True" onselectedindexchanged="suburbddl_SelectedIndexChanged" 
                            style="margin-bottom: 0px">
                        </asp:DropDownList>
                        &nbsp;<br /> <br />
                        <asp:Button ID="saddbtn" runat="server" OnClick="saddbtn_Click" Text="Add" 
                            Width="66px" />
                        &nbsp;                                          
                        <asp:Button ID="sremovebtn" Text="Remove" runat="server" Width="66px" 
                            OnClick="sremovebtn_Click" />
                    </td>                    
                </tr>
                <tr>
                    <td class="style7" valign="top">
                    </td>
                    <td valign="middle">
                        &nbsp;</td>
                    <td align="left"  valign="top">
                        <asp:ListBox ID="suburbboxl" runat="server" Width="175px"></asp:ListBox>
                    </td>
                </tr>                
                <tr>
                    <td class="style7" valign="top">
                        <asp:Label ID="lblStreet" runat="server" Text="Street:"></asp:Label>
                    </td>
                    <td valign="middle">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        <asp:DropDownList ID="streetddl" runat="server" Width="175px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style7" valign="top">
                        &nbsp;</td>
                    <td valign="middle">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        <asp:Button ID="streetAddBtn" runat="server" OnClick="streetAddBtn_Click" Text="Add" 
                            Width="66px" />
                        &nbsp;
                        <asp:Button ID="streetRemoveBtn" runat="server" OnClick="streetRemoveBtn_Click" 
                            Text="Remove" Width="66px" />
                    </td>
                </tr>
                <tr>
                    <td class="style7" valign="top">
                        &nbsp;</td>
                    <td valign="middle">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        <asp:ListBox ID="streetbox" runat="server" Width="175px"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td class="style7" valign="top">
                    </td>
                    <td valign="middle">
                        &nbsp;</td>
                    <td align="left"  valign="top">
                        <asp:Button ID="savebtn" runat="server" OnClick="savebtn_Click" Text="Save" 
                            Width="66px" />
                        <asp:Button ID="cancelbtn" runat="server" OnClick="cancelbtn_Click" 
                            Text="Cancel" Width="66px" />
                    </td>
                </tr>
                <tr>
                    <td class="tableFooter" colspan="3">
                        &nbsp;</td>
                </tr>
            </table>
             <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                <ProgressTemplate>
                    <span class="Error">Please wait...</span>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/style/images/wait.gif" Width="266px" />
                </ProgressTemplate>
            </asp:UpdateProgress>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
</td>
    </tr>    
</table>