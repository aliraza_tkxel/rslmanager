using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.payments;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Utilities.utitility_classes;
using System.Text.RegularExpressions;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.BusinessManager.statusmgmt;
namespace Am.Ahv.Website.controls.casemgt
{
    public delegate void FlexiblePaymentControl(bool visible, string value);
    public delegate void OpenCaseAgainstRegularPaymentPlan(bool value);

    public partial class AddRegularPaymentPlan : UserControlBase
    {
        #region"Attributes"

        bool isValid;
        Validation validateInput = new Validation();

        Payments paymentManager = null;
        OpenCase openCase = null;

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region"Events"

        #region"Delegate Event"

        public event FlexiblePaymentControl visibleFlexiblePaymentPlan;
        public event OpenCaseAgainstRegularPaymentPlan OpenCaseAndRegularPaymentPlan;

        #endregion

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            //new work
            if (Session[SessionConstants.CaseId] != null)
            {
                int caseId = Convert.ToInt32(Session[SessionConstants.CaseId].ToString());
                CaseDetails caseDetail = new CaseDetails();

                AM_Case caseData = caseDetail.GetCaseById(caseId);
                if (Convert.ToBoolean(caseData.IsActive))
                {
                    EnablePaymentPlan();
                }
                else
                {
                    DisablePaymentPlan();

                }
            }
            else
            {
                DisablePaymentPlan();

            }
            //end new work

            ResetMessage();
            if (!IsPostBack)
            {
                if (Session[SessionConstants.UpdateFlagPaymentPlan] == null)
                {
                    InitLookup();
                    SetWeeklyRent();
                    //InitLookup();
                    SetDefaultParameters();
                }
            }

            CheckPaymentPlanPrint();
            SetCreationDate();
        }

        #endregion

        #region"Btn Create Regular Payment Plan Click"

        protected void btnCreateRegularPaymentPlan_Click(object sender, EventArgs e)
        {
            if (CheckPaymentPlan())
            {
                SetMessage(UserMessageConstants.paymentPlanAlreadyExist, true);
                return;
            }

            if (!CheckCase())
            {
                if (Session[SessionConstants.OpenCaseFlag] != null)
                {
                    if (Convert.ToString(Session[SessionConstants.OpenCaseFlag]) == "true")
                    {
                        OpenCaseAndRegularPaymentPlan(true);
                    }
                    else
                    {
                        SetMessage(UserMessageConstants.CaseNotOpenPaymentPlan, true);
                        return;
                    }
                }
                else
                {
                    SetMessage(UserMessageConstants.CaseNotOpenPaymentPlan, true);
                    return;
                }
            }

            if (!this.validate())
            {
                return;
            }
            else
            {
                if (!CreatePaymentPlan())
                {
                    return;
                }
                else
                {
                    SetMessage(UserMessageConstants.paymentPlanSuccess, false);
                    //ResetFields();
                    DisablePaymentPlan();
                    DisableCancelButton();
                    ShowUpdateButton();
                    CheckPaymentPlanPrint();
                    Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                }
            }

        }

        #endregion

        #region"Txt Start Date Text Changed"

        protected void txtStartDate_TextChanged(object sender, EventArgs e)
        {
            /*
             * Exluded............
             * 
            //  this.CalculateEndDate();
            DateTime date = new DateTime();
            date = DateOperations.ConvertStringToDate(txtStartDate.Text);
            if (!DateOperations.FutureDateValidation(date))
            {
                SetMessage(UserMessageConstants.paymentPlanStartDateFuture, true);
                return;
            }
            else if (!DateOperations.isValidUkDate(txtStartDate.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanValidReviewDate, true);
                return;
            }
            */
        }

        #endregion

        #region"Txt Review Date Text Changed"

        protected void txtReviewDate_TextChanged(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            DateTime startDate = new DateTime();
            startDate = DateOperations.ConvertStringToDate(txtFirstCollectionDate.Text);
            date = DateOperations.ConvertStringToDate(txtReviewDate.Text);
            if (!DateOperations.FutureDateValidation(date))
            {
                SetMessage(UserMessageConstants.paymentPlanReviewDateFuture, true);
                return;
            }
            else if (!DateOperations.isValidUkDate(txtReviewDate.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanValidReviewDate, true);
                return;
            }
            else if (CompareTwoDates(date, startDate) < 0)
            {
                SetMessage(UserMessageConstants.paymentPlanReviewDateBeforeStartDate, true);
                return;
            }

        }

        #endregion

        #region"Txt First Collection Date Text Chnaged"

        protected void txtFirstCollectionDate_TextChanged(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            DateTime startDate = new DateTime();
            //startDate = DateOperations.ConvertStringToDate(txtStartDate.Text);
            date = DateOperations.ConvertStringToDate(txtFirstCollectionDate.Text);
            if (!DateOperations.FutureDateValidation(date))
            {
                SetMessage(UserMessageConstants.paymentPlanFirstCollectionFuture, true);
                return;
            }
            else if (!DateOperations.isValidUkDate(txtFirstCollectionDate.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanValidReviewDate, true);
                return;
            }
            //else if (CompareTwoDates(date, startDate) < 0)
            //{
            //    SetMessage(UserMessageConstants.paymentPlanFirstCollectionDateBeforeStartDate, true);
            //    return;
            //}
            this.CalculateEndDate();
        }

        #endregion

        #region"Drop Down List 2 Selected Index Changed"

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetAmountToBeCollected();
            SetRentPayableFrequency();
            this.CalculateEndDate();
        }

        #endregion

        #region"Txt Amount Collected Text Changed"

        protected void txtAmountCollected_TextChanged(object sender, EventArgs e)
        {
            Double amntOwnedToBHA = (Double)Session[SessionConstants.OwedTBHA];
            Double amntArrearsCollection = Math.Round(Convert.ToDouble(txtArrearsCollection.Text), 2);

            if (!validateInput.Validate_Currency_Text_Box(txtAmountCollected.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Agreement Amount.", true);
                return;
            }

            double rentBalance = Convert.ToDouble(txtArrearsBalance.Text);

            if (Convert.ToDouble(txtAmountCollected.Text) > rentBalance)
            {
                SetMessage(UserMessageConstants.paymentPlanAmountGreaterThanrentBalance, true);
                return;
            }
            else if (Convert.ToDouble(txtAmountCollected.Text) < rentBalance && (ddlFrequencyRegularPayment.SelectedItem.Text.Equals("Full Amount")))
            {
                SetMessage(UserMessageConstants.paymentPlanAmountEqualRentBalance, true);
                return;
            }

            else if ((amntOwnedToBHA != amntArrearsCollection && Convert.ToDouble(txtAmountCollected.Text) == 0.0) || (Convert.ToDouble(txtAmountCollected.Text) < 0.0))
            {
                SetMessage("'Agreed Amount' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
            this.CalculateEndDate();
            SetArrearsCollection();
        }

        #endregion

        #region"Txt Arrears Balance Text Changed"

        protected void TxtArrearsBalance_TextChanged(object sender, EventArgs e)
        {


            if (!validateInput.Validate_Currency_Text_Box(txtArrearsBalance.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Owed to BHA.", true);
                return;
            }

            else if (Convert.ToDouble(txtAmountCollected.Text) > Convert.ToDouble(txtArrearsBalance.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanAmountToBeCollectedgreater, true);
                return;
            }

            else if (Convert.ToDouble(txtArrearsBalance.Text) == 0.0 || Convert.ToDouble(txtArrearsBalance.Text) < 0.0)
            {
                SetMessage("'Owed to BHA' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }


            this.CalculateEndDate();
        }

        #endregion

        #region"DDL Payment Type Selected Index Changed"

        protected void ddlPaymentType_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (ddlPaymentType.SelectedItem.Text.Equals("Flexible"))
            {
                visibleFlexiblePaymentPlan(true, ddlPaymentType.SelectedValue);
            }
            else
                return;
        }

        #endregion

        #region"Txt Weekly Rent Amount Text Changed"

        protected void txtWeeklyRentAmount_TextChanged(object sender, EventArgs e)
        {
            if (!validateInput.Validate_Currency_Text_Box(txtWeeklyRentAmount.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Monthly Rent.", true);
                return;
            }
            else if (Convert.ToDouble(txtWeeklyRentAmount.Text) == 0.0 || Convert.ToDouble(txtWeeklyRentAmount.Text) < 0.0)
            {
                SetMessage("'Monthly Rent' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }

        }

        #endregion

        #region"Txt Rent Payable Text Changed"

        protected void TxtRentPayable_TextChanged(object sender, EventArgs e)
        {
            if (!validateInput.Validate_Currency_Text_Box(txtRentPayable.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Rent Payable.", true);
                return;
            }
            //else if (Convert.ToDouble(txtRentPayable.Text) == 0.0 || Convert.ToDouble(txtRentPayable.Text) < 0.0)
            else if (Convert.ToDouble(txtRentPayable.Text) < 0.0)
            {
                SetMessage("'Rent Payable' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
            SetArrearsCollection();
        }

        #endregion

        #region"Txt Arrears Collection Text Changed"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtArrearsCollection_TextChanged(object sender, EventArgs e)
        {
            if (!validateInput.Validate_Currency_Text_Box(txtArrearsCollection.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Arrears Collection.", true);
                return;
            }

            else if (Convert.ToDouble(txtArrearsCollection.Text) == 0.0 || Convert.ToDouble(txtArrearsCollection.Text) < 0.0)
            {
                SetMessage("'Arrears Collection' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
            SetArrearsCollection();
        }

        #endregion

        #region"Btn Print Click"

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.regularPaymentReport, false);
        }

        #endregion


        #region"Btn Update Click"

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ShowCreateUpdateButton();
            EnablePaymentPlan();
            EnableCancelButton();

        }

        #endregion

        #region"Btn Close Click"

        protected void btnClose_Click(object sender, EventArgs e)
        {
            if (UpdatePaymentPlan(false))
            {

                InitLookup();
                ResetFields();
                SetWeeklyRent();
                CheckPaymentPlanPrint();
                SetCreationDate();
                SetDefaultParameters();

                Session[SessionConstants.paymentPlanId] = null;
                Session[SessionConstants.UpdateFlagPaymentPlan] = null;
                Response.Redirect(PathConstants.casehistory, false);

            }
            else
            {
                SetMessage(UserMessageConstants.problemInUpdatePaymentPlan, true);
            }

        }

        #endregion

        #region"Btn Create Update Click"

        protected void btnCreateUpdate_Click(object sender, EventArgs e)
        {
            if (!this.validate())
            {
                return;
            }
            else
            {
                if (UpdatePaymentPlan(true))
                {
                    SetMessage(UserMessageConstants.UpdateSuccessfulPaymentPlan, false);
                    DisablePaymentPlan();
                    ShowUpdateButton();
                    DisableCancelButton();
                    Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                }
                else
                {
                    SetMessage(UserMessageConstants.problemInUpdatePaymentPlan, true);
                }
            }
        }

        #endregion

        #region"Btn Cancel Click"

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(Session[SessionConstants.UpdateFlagPaymentPlan]).Equals("true") &&
                Convert.ToString(Session[SessionConstants.PaymentPlanTypePaymentPlan]).Equals("Flexible"))
            {
                ListItem item = ddlPaymentType.Items.FindByText("Flexible");
                visibleFlexiblePaymentPlan(false, item.Value);
            }
            else
            {
                if (Session[SessionConstants.paymentPlanId] != null)
                {
                    ResetFields();
                    LoadPaymentPlan(true);
                    ShowUpdateButton();
                    DisablePaymentPlan();
                    DisableCancelButton();
                }
                else
                {
                    ResetFields();
                }
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Create Payment Plan"

        public bool CreatePaymentPlan()
        {
            bool success = false;
            try
            {
                AM_PaymentPlan paymentPlan = new AM_PaymentPlan();
                AM_PaymentPlanHistory paymentPlanlHistory = new AM_PaymentPlanHistory();

                paymentPlan = SetPaymentPlan();
                paymentPlanlHistory = SetPaymentPlanHistory();

                Payments payments = new Payments();

                List<AM_Payment> listPayment = SavePayments(paymentPlan);

                if (payments.AddNewPayment(paymentPlan, paymentPlanlHistory, listPayment) == 1)
                {
                    success = true;
                }
                else
                {
                    success = false;
                }
                //paymentPlanlHistory.PaymentPlanId = paymentPlanId;
                //paymentPlanlHistory.AM_PaymentPlanReference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_PaymentPlan", "PaymentPlanId", paymentPlanId);
                //int paymentPlanHistoryId = payments.AddPaymentPlanHistory(paymentPlanlHistory);
                //SetCasePaymentPlan();
                ////if (!this.SavePayments(paymentPlan, paymentPlanId, paymentPlanHistoryId))
                //return false;             
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingSavingPaymentPlan, true);
                }
            }
            return success;
        }

        #endregion

        #region"Update Payment Plan"

        public bool UpdatePaymentPlan(bool ActivePaymentPlan)
        {
            bool success = false;
            try
            {
                AM_PaymentPlan paymentPlan = new AM_PaymentPlan();
                AM_PaymentPlanHistory paymentPlanlHistory = new AM_PaymentPlanHistory();

                paymentPlan.TennantId = base.GetTenantId();
                paymentPlanlHistory.TennantId = base.GetTenantId();

                paymentPlan.PaymentPlanType = Convert.ToInt32(ddlPaymentType.SelectedValue);
                paymentPlanlHistory.PaymentPlanType = Convert.ToInt32(ddlPaymentType.SelectedValue);

                //paymentPlan.StartDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);
                //paymentPlanlHistory.StartDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);

                paymentPlan.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);
                paymentPlanlHistory.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);

                paymentPlan.AmountToBeCollected = Convert.ToDouble(txtAmountCollected.Text);
                paymentPlanlHistory.AmountToBeCollected = Convert.ToDouble(txtAmountCollected.Text);

                paymentPlan.WeeklyRentAmount = Convert.ToDouble(txtWeeklyRentAmount.Text);
                paymentPlanlHistory.WeeklyRentAmount = Convert.ToDouble(txtWeeklyRentAmount.Text);

                paymentPlan.ReviewDate = DateTime.ParseExact(txtReviewDate.Text, "dd/MM/yyyy", null);
                paymentPlanlHistory.ReviewDate = DateTime.ParseExact(txtReviewDate.Text, "dd/MM/yyyy", null);

                paymentPlan.CreatedDate = DateTime.Now;
                paymentPlanlHistory.CreatedDate = DateTime.Now;

                paymentPlan.ModifiedDate = DateTime.Now;
                paymentPlanlHistory.ModifiedDate = DateTime.Now;

                paymentPlan.IsCreated = true;
                paymentPlanlHistory.IsCreated = true;

                paymentPlan.FrequencyLookupCodeId = Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue);
                paymentPlanlHistory.FrequencyLookupCodeId = Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue);

                paymentPlan.CreatedBy = base.GetCurrentLoggedinUser();
                paymentPlanlHistory.CreatedBy = base.GetCurrentLoggedinUser();

                paymentPlan.ModifiedBy = base.GetCurrentLoggedinUser();
                paymentPlanlHistory.ModifiedBy = base.GetCurrentLoggedinUser();

                paymentPlan.FirstCollectionDate = DateTime.ParseExact(txtFirstCollectionDate.Text, "dd/MM/yyyy", null);
                paymentPlanlHistory.FirstCollectionDate = DateTime.ParseExact(txtFirstCollectionDate.Text, "dd/MM/yyyy", null);

                paymentPlan.LastPaymentDate = DateOperations.ConvertStringToDate(Convert.ToString(Session[SessionConstants.lastPaymentDate]));
                paymentPlanlHistory.LastPaymentDate = DateOperations.ConvertStringToDate(Convert.ToString(Session[SessionConstants.lastPaymentDate]));

                paymentPlan.RentBalance = Convert.ToDouble(txtArrearsBalance.Text);
                paymentPlanlHistory.RentBalance = Convert.ToDouble(txtArrearsBalance.Text);

                paymentPlan.LastPayment = Convert.ToDouble(Session[SessionConstants.lastPayment]);
                paymentPlanlHistory.LastPayment = Convert.ToDouble(Session[SessionConstants.lastPayment]);

                paymentPlan.RentPayable = Convert.ToDouble(txtRentPayable.Text);
                paymentPlanlHistory.RentPayable = Convert.ToDouble(txtRentPayable.Text);

                paymentPlan.ArrearsCollection = Convert.ToDouble(txtArrearsCollection.Text);
                paymentPlanlHistory.ArrearsCollection = Convert.ToDouble(txtArrearsCollection.Text);

                paymentPlan.IsActive = ActivePaymentPlan;
                paymentPlanlHistory.IsActive = ActivePaymentPlan;

                Payments payments = new Payments();

                List<AM_Payment> listPayment = SavePayments(paymentPlan);

                if (Session[SessionConstants.paymentPlanId] != null)
                {
                    if (payments.UpdatePaymentPlan(Convert.ToInt32(Session[SessionConstants.paymentPlanId]), paymentPlan, paymentPlanlHistory, listPayment))
                    {
                        success = true;
                    }
                    else
                    {
                        success = false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingSavingPaymentPlan, true);
                }
            }
            return success;
        }

        #endregion

        #region"Installments"

        public DateTime Installments(double totalRentAmount, double amountToBeCollected, string duration, DateTime firstCollection)
        {
            DateTime time = new DateTime();
            try
            {
                double result = CreateInstallments(totalRentAmount, amountToBeCollected);
                if (result == 0.0)
                {
                    SetMessage(UserMessageConstants.paymentPlanUnableToCreate, true);
                    return firstCollection;
                }
                time = DateOperations.AddRegularDate(result, duration, firstCollection);

                return time;
            }
            catch (ArithmeticException ax)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ax, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingInInstallments, true);
                }
            }
            return time;
        }

        #endregion

        #region"Create Installments"

        public double CreateInstallments(double totalRentAmount, double amountToBeCollected)
        {
            try
            {
                Double amntOwnedToBHA = (Double)Session[SessionConstants.OwedTBHA];
                Double amntArrearsCollection = Math.Round(Convert.ToDouble(txtArrearsCollection.Text), 2);

                if ((ddlFrequencyRegularPayment.SelectedItem.Text == "Full Amount") || (amntOwnedToBHA == amntArrearsCollection && Convert.ToDouble(txtAmountCollected.Text) == 0.0))
                {
                    return 1.0;
                }
                else
                {

                    if (totalRentAmount % amountToBeCollected == 0)
                    {
                        //return calculateReminder();
                        // Update by Hussain: Fixing an infinte loop
                        return totalRentAmount / amountToBeCollected;
                    }
                    else if (totalRentAmount % amountToBeCollected > 0)
                    {
                        return (Math.Ceiling(totalRentAmount / amountToBeCollected));
                    }
                }
                return 0.0;
            }
            catch (ArithmeticException ax)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ax, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingInInstallments, true);
                }
            }
            return 0.0;
        }

        #endregion

        #region"Save Payments"

        public List<AM_Payment> SavePayments(AM_PaymentPlan paymentPlan)
        {

            List<AM_Payment> listPayments = new List<AM_Payment>();
            try
            {
                paymentManager = new Payments();


                DateTime firstDateTime;
                firstDateTime = DateTime.ParseExact(txtFirstCollectionDate.Text, "dd/MM/yyyy", null);
                DateTime paymentDate = new DateTime(firstDateTime.Year, firstDateTime.Month, firstDateTime.Day);
                double arrearAmount = Convert.ToDouble(txtArrearsBalance.Text);
                int totalInstallments = Convert.ToInt32(CreateInstallments(arrearAmount, Convert.ToDouble(txtAmountCollected.Text)));
                DateTime tempDate = DateTime.Now;

                for (int i = 0; i < totalInstallments; i++)
                {
                    AM_Payment payment = new AM_Payment();
                    payment.Date = DateOperations.AddDate(Convert.ToDouble(i), ddlFrequencyRegularPayment.SelectedItem.Text, paymentDate);
                    if (i + 1 == totalInstallments)
                    {
                        if (ddlFrequencyRegularPayment.SelectedItem.Text != "Full Amount")
                        {
                            double reminder = calculateReminder();
                            if (reminder > 0.0)
                            {
                                payment.Payment = Math.Round(reminder, 2);
                            }
                            else if (reminder == 0.0)
                            {
                                if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fornightly")
                                {
                                    payment.Payment = Convert.ToDouble(CalcArrearCollection_Fornightly());
                                }
                                else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
                                {
                                    payment.Payment = Convert.ToDouble(CalcArrearCollection_Fornightly());
                                }
                                else
                                {
                                    payment.Payment = (Convert.ToDouble(txtAmountCollected.Text) + Convert.ToDouble(txtRentPayable.Text));
                                }
                            }
                        }
                        else
                        {
                            payment.Payment = Convert.ToDouble(txtRentPayable.Text) + Convert.ToDouble(txtArrearsBalance.Text);
                        }
                    }
                    else
                    {
                        if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
                        {
                            payment.Payment = Convert.ToDouble(CalcArrearCollection_Fornightly());
                        }
                        else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
                        {
                            payment.Payment = Convert.ToDouble(CalcArrearCollection_Weekly());
                        }
                        else
                        {
                            payment.Payment = (Convert.ToDouble(txtAmountCollected.Text) + Convert.ToDouble(txtRentPayable.Text));
                        }
                    }
                    listPayments.Add(payment);
                    tempDate = payment.Date;
                }

                //for (int j = 0; j < 2; j++)
                //{
                //    AM_Payment payment = new AM_Payment();
                //    payment.Date = DateOperations.AddDate(Convert.ToDouble(j + 1), "Weekly", tempDate);
                //    payment.Payment = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]);
                //    listPayments.Add(payment);
                //}
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingSavingPaymentInstallments, true);
                }
            }
            return listPayments;
        }

        #endregion

        #region"Get End Date"

        //protected DateTime GetEndDate()
        //{
        //    //DateTime firstDateTime = DateTime.ParseExact(txtFirstCollectionDate.Text, "dd/MM/yyyy", null);

        //    //int arrearAmount = Convert.ToInt32(Session[SessionConstants.Rentbalance]);
        //    //int installments = arrearAmount / Convert.ToInt32(txtAmountCollected.Text);
        //    //installments -= 1;

        //    //DateTime endDateTime;

        //    //bool frequenceSelectedInMonth = false;

        //    //if (Convert.ToInt32(ddlFrequencyRegularPayment.SelectedIndex) >= 0 || Convert.ToInt32(ddlFrequencyRegularPayment.SelectedIndex) < 4)
        //    //{
        //    //    endDateTime = firstDateTime.AddDays(Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue) * installments);
        //    //    frequenceSelectedInMonth = false;
        //    //}
        //    //else
        //    //{
        //    //    endDateTime = firstDateTime.AddMonths(installments);
        //    //    frequenceSelectedInMonth = true;
        //    //}

        //    //return endDateTime;
        //    return DateTime.Now;
        //}

        #endregion

        #region"Calculate End Date"

        private void CalculateEndDate()
        {
            if (txtFirstCollectionDate.Text.Equals("") || txtAmountCollected.Text.Equals(""))
            {
                return;
            }
            DateTime dateTime;
            //double arrearAmount = base.GetRentBalance();
            if (txtArrearsBalance.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.InvalidArrearsBalancePaymentPlan, true);
                return;
            }
            double arrearAmount = Convert.ToDouble(txtArrearsBalance.Text);

            dateTime = DateOperations.ConvertStringToDate(txtFirstCollectionDate.Text);
            if (ddlFrequencyRegularPayment.SelectedValue != "-1")
            {
                txtEndDate.Text = Installments(arrearAmount, Convert.ToDouble(txtAmountCollected.Text), ddlFrequencyRegularPayment.SelectedItem.Text, dateTime).ToString("dd/MM/yyyy");

            }
        }

        #endregion

        #region"Validate"

        public bool validate()
        {
            this.isValid = true;

            Double amntOwnedToBHA = (Double)Session[SessionConstants.OwedTBHA];
            Double amntArrearsCollection = Math.Round(Convert.ToDouble(txtArrearsCollection.Text), 2);

            if (ddlPaymentType.SelectedValue == "-1")
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentPlanTypeSelection, true);
                return isValid;
            }

            else if (ddlFrequencyRegularPayment.SelectedValue == "-1")
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentProvideFrequency, true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtWeeklyRentAmount.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Monthly Rent.", true);
                return isValid;
            }
            else if (Convert.ToDouble(txtWeeklyRentAmount.Text) == 0.0 || Convert.ToDouble(txtWeeklyRentAmount.Text) < 0.0)
            {
                isValid = false;
                SetMessage("'Monthly Rent' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtRentPayable.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Rent Payable.", true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtArrearsBalance.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Owed To BHA.", true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtAmountCollected.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Agreement Amount.", true);
                return isValid;
            }
            else if ((amntOwnedToBHA != amntArrearsCollection && Convert.ToDouble(txtAmountCollected.Text) == 0.0) || (Convert.ToDouble(txtAmountCollected.Text) < 0.0))
            {
                isValid = false;
                SetMessage("'Agreed Amount' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtArrearsCollection.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Arrears Collection.", true);
                return isValid;
            }
            else if (Convert.ToDouble(txtArrearsCollection.Text) == 0.0 || Convert.ToDouble(txtArrearsCollection.Text) < 0.0)
            {
                isValid = false;
                SetMessage("'Arrears Collection' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return isValid;
            }

            else if (Convert.ToDouble(txtAmountCollected.Text) > Convert.ToDouble(txtArrearsBalance.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentPlanAmountToBeCollectedgreater, true);
                return isValid;
            }

            else if (txtReviewDate.Text.Equals(""))
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentPlanReviewDate, true);
                return isValid;
            }
            else if (txtFirstCollectionDate.Text.Equals(""))
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentPlanFirstCollection, true);
                return isValid;
            }

            if (isValid)
            {
                DateTime firstDateTime = DateTime.ParseExact(txtFirstCollectionDate.Text, "dd/MM/yyyy", null);

                if (!DateOperations.todayAndFutureDateValidation(firstDateTime))
                {
                    isValid = false;
                    SetMessage(UserMessageConstants.paymentPlanFirstCollectionFuture, true);
                    return isValid;
                }
            }
            return isValid;
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Control Load"

        public void ControlLoad(string value)
        {
            if (Convert.ToString(Session[SessionConstants.UpdateFlagPaymentPlan]).Equals("true"))
            //&&                Convert.ToString(Session[SessionConstants.PaymentPlanTypePaymentPlan]).Equals("Regular"))
            {
                FirstLookPaymentPlan(true);
                EnablePaymentPlan();
                ShowCreateUpdateButton();
                EnableCancelButton();
            }
            else
            {
                InitLookup();
                ResetFields();
                SetWeeklyRent();
                CheckPaymentPlanPrint();
                SetCreationDate();
                SetDefaultParameters();
            }
            ddlPaymentType.SelectedValue = value;
        }

        #endregion

        #region"Reset Fields"

        public void ResetFields()
        {
            ddlPaymentType.SelectedValue = "-1";
            txtEndDate.Text = string.Empty;
            ddlFrequencyRegularPayment.SelectedValue = "-1";
            txtAmountCollected.Text = string.Empty;
            //  txtWeeklyRentAmount.Text = string.Empty;
            txtReviewDate.Text = string.Empty;
            txtFirstCollectionDate.Text = string.Empty;
            //txtStartDate.Text = string.Empty;
        }

        #endregion

        #region"Check Payment Plan"

        public bool CheckPaymentPlan()
        {
            paymentManager = new Payments();
            return paymentManager.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Compare Two Dates"

        public int CompareTwoDates(DateTime date1, DateTime date2)
        {
            if (DateTime.Compare(date1, date2) < 0)
            {
                return -1;
            }
            else if (DateTime.Compare(date1, date2) == 0)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        #endregion

        #region"Set Case Payment Plan"

        public bool SetCasePaymentPlan()
        {
            paymentManager = new Payments();
            return paymentManager.SetCasePaymentPlan(base.GetTenantId(), base.GetCurrentLoggedinUser());
        }

        #endregion

        #region"Disable Payment Plan"

        public void DisablePaymentPlan()
        {
            ddlPaymentType.Enabled = false;
            txtFirstCollectionDate.Enabled = false;
            //txtStartDate.Enabled = false;
            txtEndDate.Enabled = false;
            txtReviewDate.Enabled = false;
            txtAmountCollected.Enabled = false;
            ddlFrequencyRegularPayment.Enabled = false;
            txtWeeklyRentAmount.Enabled = false;
            txtRentPayable.Enabled = false;
            txtArrearsBalance.Enabled = false;
            txtArrearsCollection.Enabled = false;
        }

        #endregion

        #region"Enable Payment Plan"

        public void EnablePaymentPlan()
        {
            ddlPaymentType.Enabled = true;
            txtFirstCollectionDate.Enabled = true;
            //txtStartDate.Enabled = true;
            txtReviewDate.Enabled = true;
            txtAmountCollected.Enabled = true;
            ddlFrequencyRegularPayment.Enabled = true;
            txtWeeklyRentAmount.Enabled = true;
            txtRentPayable.Enabled = true;
            txtArrearsBalance.Enabled = true;
            txtArrearsCollection.Enabled = true;
        }

        #endregion

        #region"Show Update Button"

        public void ShowUpdateButton()
        {
            btnUpdate.Visible = true;
            btnClose.Visible = true;
            btnCreateRegularPaymentPlan.Visible = false;
            btnCreateUpdate.Visible = false;
        }

        #endregion

        #region"Show Create Update Button"

        public void ShowCreateUpdateButton()
        {
            btnUpdate.Visible = false;
            btnClose.Visible = false;
            btnCreateRegularPaymentPlan.Visible = false;
            btnCreateUpdate.Visible = true;
        }

        #endregion

        #region"Show Create Button"

        public void ShowCreateButton()
        {
            btnUpdate.Visible = false;
            btnClose.Visible = false;
            btnCreateRegularPaymentPlan.Visible = true;
            btnCreateUpdate.Visible = false;
        }

        #endregion

        #region"Enable Cancel Button"

        public void EnableCancelButton()
        {
            btnCancel.Enabled = true;
        }

        #endregion

        #region"Disable Cancel Button"

        public void DisableCancelButton()
        {
            btnCancel.Enabled = false;
        }

        #endregion

        #region"Check Case"

        public bool CheckCase()
        {
            try
            {
                OpenCase openCaseManager = new OpenCase();
                return openCaseManager.CheckCase(base.GetTenantId());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Disable Payment Plan Fields"

        public void DisablePaymentPlanFields()
        {
            ddlPaymentType.Enabled = false;
            txtWeeklyRentAmount.Enabled = false;
            txtAmountCollected.Enabled = false;
            ddlFrequencyRegularPayment.Enabled = false;
            //txtStartDate.Enabled = false;
            txtFirstCollectionDate.Enabled = false;
            txtReviewDate.Enabled = false;
            btnPrint.Enabled = false;
            btnCancel.Enabled = false;
            btnCreateRegularPaymentPlan.Enabled = false;
            txtRentPayable.Enabled = false;
            txtArrearsBalance.Enabled = false;
            txtArrearsCollection.Enabled = false;
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Init Lookup"

        public void InitLookup()
        {
            paymentManager = new Payments();
            ddlFrequencyRegularPayment.DataSource = paymentManager.GetFrequencyData();
            ddlFrequencyRegularPayment.DataTextField = "CodeName";
            ddlFrequencyRegularPayment.DataValueField = "LookupCodeId";
            ddlFrequencyRegularPayment.DataBind();
            ddlFrequencyRegularPayment.Items.Add(new ListItem("Please Select", "-1"));
            ddlFrequencyRegularPayment.SelectedValue = "-1";

            ddlPaymentType.DataSource = paymentManager.GetPaymentPlanType();
            ddlPaymentType.DataTextField = "CodeName";
            ddlPaymentType.DataValueField = "LookupCodeId";
            ddlPaymentType.DataBind();
            ddlPaymentType.Items.Add(new ListItem("Please Select", "-1"));
            ddlPaymentType.SelectedValue = "-1";
        }

        #endregion

        #region"Set Weekly Rent"

        public void SetWeeklyRent()
        {
            if (Session[SessionConstants.WeeklyRentAmount] != null)
            {
                txtWeeklyRentAmount.Text = Math.Round(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]), 2).ToString();
            }
            SetRentPayable();
            SetRentBalance();
        }

        #endregion

        #region"Set Rent Payable"

        public void SetRentPayable()
        {
            if (Session[SessionConstants.WeeklyRentAmount] != null)
            {
                if (Session[SessionConstants.HBCyclePayment] == null)
                {
                    txtRentPayable.Text = Math.Round(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]), 2).ToString();
                }
                else
                {
                    if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly" || ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
                    {
                        SetRentPayableFrequency();
                    }
                    else
                    {
                        txtRentPayable.Text = Math.Abs(Math.Round((Math.Abs(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount])) - Math.Abs(Convert.ToDouble(Session[SessionConstants.HBCyclePayment]))), 2)).ToString();
                    }
                }
            }
            SetArrearsCollection();
        }

        #endregion

        #region"Set Rent Payable Frequency"

        public void SetRentPayableFrequency()
        {
            if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
            {
                //updated by:umair
                //update date:26 08 2011

                //double MonthlyHB = 0.0;
                //if (Session[SessionConstants.HBCyclePayment] != null)
                /*if (Session[SessionConstants.HBPayment] != null)
                {



                    //MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
                    //double rentPayable = ((Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount])-MonthlyHB) / 30.42) * 14;
                    double FortnightlyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 26;
                    double FortnightlyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 2;
                    double rentPayable = FortnightlyRent - FortnightlyHB;

                    txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
                    SetArrearsCollection();
                }
                else
                {
                    double FortnightlyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 26;
                    double rentPayable = FortnightlyRent; 

                    txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
                    SetArrearsCollection();
                }*/
                //update end

                // Updated by Hussain (14/11/2012)
                // Commented out the code written by Umair 26 08 2011 and added the following code

                double fortnightlyRent = 0.0;

                // Option A
                // The fortnightlyRend (Monthly Rent in the formula) is fetched from the server. If no session entry is found, they get from
                // the text field. This maybe the cause that intermentiantly the formula fails when converting between month and fortnightly
                if (Session[SessionConstants.WeeklyRentAmount] != null)
                {
                    fortnightlyRent = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]);
                }
                else
                {
                    fortnightlyRent = Convert.ToDouble(txtWeeklyRentAmount.Text);
                }

                // Option B
                // Taking the value of the monthly rent from the text field instead of from the session. This is so that if the user changes
                // the value of the text box, it reflects in the caluclations
                //fortnightlyRent = Convert.ToDouble(txtWeeklyRentAmount.Text);

                double fortnightlyHB = 0.0;

                if (Session[SessionConstants.HBPayment] != null)
                {
                    // Based on Peter's email dated 15/11/2012, the following has been changed from HBPayment to HBCyclePayment and discussion 
                    // with Adnan, we're using the monthly HB (HBCyclePayment), HBPayment is the HB for 4 weeks. 
                    //fortnightlyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 2.0;
                    fortnightlyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBCyclePayment]));
                }


                double fortnightlyRentPayable = (fortnightlyRent - fortnightlyHB) * 12.0 / 26.0;

                txtRentPayable.Text = Convert.ToString(Math.Round(fortnightlyRentPayable, 2));
                SetArrearsCollection();
                // Update by Hussain end

            }
            else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
            {
                //updated by:umair
                //update date:26 08 2011

                //double MonthlyHB = 0.0;
                //if (Session[SessionConstants.HBCyclePayment] != null)
                /*if (Session[SessionConstants.HBPayment] != null)
                {
                    //MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
                    //double rentPayable = ((Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB) / 30.42) * 7;
                    double WeeklyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 52;
                    double WeeklyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 4;
                    double rentPayable = WeeklyRent - WeeklyHB;

                    txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
                    SetArrearsCollection();
                }
                else
                {
                    double WeeklyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 52;
                    double rentPayable = WeeklyRent;

                    txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
                    SetArrearsCollection();
                }*/
                //update end

                // Updated by Hussain (14/11/2012)
                // Commented out the code written by Umair 26 08 2011 and added the following code

                double weeklyRent = 0.0;

                // Option A
                // The fortnightlyRend (Monthly Rent in the formula) is fetched from the server. If no session entry is found, they get from
                // the text field. This maybe the cause that intermentiantly the formula fails when converting between month and weekly
                if (Session[SessionConstants.WeeklyRentAmount] != null)
                {
                    weeklyRent = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]);
                }
                else
                {
                    weeklyRent = Convert.ToDouble(txtWeeklyRentAmount.Text);
                }

                // Option B
                // Taking the value of the monthly rent from the text field instead of from the session. This is so that if the user changes
                // the value of the text box, it reflects in the caluclations
                //weeklyRent = Convert.ToDouble(txtWeeklyRentAmount.Text);

                double weeklyHB = 0.0;

                if (Session[SessionConstants.HBPayment] != null)
                {
                    // Based on Peter's email dated 15/11/2012, the following has been changed from HBPayment to HBCyclePayment and discussion 
                    // with Adnan, we're using the monthly HB (HBCyclePayment), HBPayment is the HB for 4 weeks. 

                    //weeklyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 2.0;
                    weeklyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBCyclePayment]));

                }


                double weeklyRentPayable = (weeklyRent - weeklyHB) * 12.0 / 52.0;

                txtRentPayable.Text = Convert.ToString(Math.Round(weeklyRentPayable, 2));
                SetArrearsCollection();
                // Update by Hussain end
            }
            else
            {
                SetRentPayable();
            }

        }

        #endregion

        #region"Set Rent Balance"

        public void SetRentBalance()
        {
            if (Session[SessionConstants.LastMonthNetArrears] != null)
            {
                txtArrearsBalance.Text = Convert.ToString(Math.Abs(Convert.ToDouble(Session[SessionConstants.LastMonthNetArrears])));
            }
        }

        #endregion

        #region"Set Arrears Collection"

        public void SetArrearsCollection()
        {
            double agreedAmount = 0.0;
            if (txtAmountCollected.Text.Equals(string.Empty))
            {
                agreedAmount = 0.0;
            }
            else
            {
                agreedAmount = Convert.ToDouble(txtAmountCollected.Text);
            }

            if (ddlFrequencyRegularPayment.SelectedValue != "-1")
            {
                if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
                {
                    //Calc Arrear Collection for Fornightly
                    txtArrearsCollection.Text = CalcArrearCollection_Fornightly();

                }
                else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
                {
                    txtArrearsCollection.Text = CalcArrearCollection_Weekly();

                }
                else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Full Amount")
                {
                    txtArrearsCollection.Text = Convert.ToString((Convert.ToDouble(txtRentPayable.Text)) + Convert.ToDouble(txtArrearsBalance.Text));

                }
                else
                {
                    txtArrearsCollection.Text = Convert.ToString((Convert.ToDouble(txtRentPayable.Text)) + agreedAmount);
                }
            }
            else
            {
                if (txtRentPayable.Text != null && txtRentPayable.Text != "")
                {
                    txtArrearsCollection.Text = Convert.ToString((Convert.ToDouble(txtRentPayable.Text)) + agreedAmount);
                }
                else
                {
                    txtArrearsCollection.Text = Convert.ToString(agreedAmount);
                }

            }
        }

        #endregion

        #region"Set Default Parameters"

        public void SetDefaultParameters()
        {
            ListItem payment = ddlPaymentType.Items.FindByText("Regular");
            ddlPaymentType.SelectedValue = payment.Value;

            ListItem frequency = ddlFrequencyRegularPayment.Items.FindByText("Monthly");
            ddlFrequencyRegularPayment.SelectedValue = frequency.Value;
        }

        #endregion

        #region"Set Creation Date"

        public void SetCreationDate()
        {
            if (Session[SessionConstants.CreateDatePaymentPlan] == null)
            {
                lblCreationDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                lblCreationDate.Text = Convert.ToString(Session[SessionConstants.CreateDatePaymentPlan]);
            }
        }

        #endregion

        #region"Check Payment Plan Print"

        public void CheckPaymentPlanPrint()
        {
            try
            {
                paymentManager = new Payments();
                btnPrint.Enabled = paymentManager.CheckPaymentPlan(base.GetTenantId());
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.CheckPaymentPlanPrintPayments, true);
                }
            }

        }

        #endregion

        #region"Load Payment Plan"

        public void LoadPaymentPlan(bool showDateFields)
        {
            try
            {
                int caseId = Convert.ToInt32(Session[SessionConstants.CaseId].ToString());
                CaseDetails caseDetail = new CaseDetails();
                AM_Case caseData = caseDetail.GetCaseById(caseId);
                paymentManager = new Payments();
                AM_PaymentPlan paymentPlan = paymentManager.GetPaymentPlan(base.GetTenantId());
                if (paymentPlan != null && Convert.ToBoolean(caseData.IsActive))
                {

                    ddlPaymentType.SelectedValue = paymentPlan.PaymentPlanType.Value.ToString();
                    if (showDateFields)
                    {
                        //txtStartDate.Text = paymentPlan.StartDate.Value.ToString("dd/MM/yyyy");
                        txtEndDate.Text = paymentPlan.EndDate.ToString("dd/MM/yyyy");
                        txtReviewDate.Text = paymentPlan.ReviewDate.ToString("dd/MM/yyyy");
                        txtFirstCollectionDate.Text = paymentPlan.FirstCollectionDate.Value.ToString("dd/MM/yyyy");
                    }

                    ddlFrequencyRegularPayment.SelectedValue = paymentPlan.FrequencyLookupCodeId.ToString();
                    txtAmountCollected.Text = paymentPlan.AmountToBeCollected.ToString();
                    txtRentPayable.Text = paymentPlan.RentPayable.ToString();
                    txtArrearsBalance.Text = paymentPlan.RentBalance.ToString();
                    txtWeeklyRentAmount.Text = paymentPlan.WeeklyRentAmount.ToString();
                    txtArrearsCollection.Text = paymentPlan.ArrearsCollection.ToString();
                    lblCreationDate.Text = paymentPlan.CreatedDate.ToString("dd/MM/yyyy");
                    Session[SessionConstants.CreateDatePaymentPlan] = lblCreationDate.Text;
                    Session[SessionConstants.paymentPlanId] = paymentPlan.PaymentPlanId;
                }

            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"First Look Payment Plan"

        public void FirstLookPaymentPlan(bool isBridge)
        {
            try
            {
                InitLookup();
                if (isBridge)
                {
                    LoadPaymentPlan(false);
                }
                else
                {
                    LoadPaymentPlan(true);
                }
                SetWeeklyRent();
                CheckPaymentPlanPrint();
                DisablePaymentPlan();
                ShowUpdateButton();
                DisableCancelButton();

                Session[SessionConstants.UpdateFlagPaymentPlan] = "true";
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.problemLoadingPaymentPlan, true);
                }
            }
        }

        #endregion

        #region"Set Payment Plan"

        public AM_PaymentPlan SetPaymentPlan()
        {
            AM_PaymentPlan paymentPlan = new AM_PaymentPlan();

            paymentPlan.TennantId = base.GetTenantId();
            paymentPlan.PaymentPlanType = Convert.ToInt32(ddlPaymentType.SelectedValue);
            //paymentPlan.StartDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);
            paymentPlan.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);
            paymentPlan.AmountToBeCollected = Convert.ToDouble(txtAmountCollected.Text);
            paymentPlan.WeeklyRentAmount = Convert.ToDouble(txtWeeklyRentAmount.Text);
            paymentPlan.ReviewDate = DateTime.ParseExact(txtReviewDate.Text, "dd/MM/yyyy", null);
            paymentPlan.CreatedDate = DateTime.Now;
            paymentPlan.ModifiedDate = DateTime.Now;
            paymentPlan.IsCreated = true;
            paymentPlan.FrequencyLookupCodeId = Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue);
            paymentPlan.CreatedBy = base.GetCurrentLoggedinUser();
            paymentPlan.ModifiedBy = base.GetCurrentLoggedinUser();
            paymentPlan.FirstCollectionDate = DateTime.ParseExact(txtFirstCollectionDate.Text, "dd/MM/yyyy", null);

            paymentPlan.LastPaymentDate = DateOperations.ConvertStringToDate(Convert.ToString(Session[SessionConstants.lastPaymentDate]));
            paymentPlan.RentBalance = Convert.ToDouble(txtArrearsBalance.Text);
            paymentPlan.LastPayment = Convert.ToDouble(Session[SessionConstants.lastPayment]);
            paymentPlan.RentPayable = Convert.ToDouble(txtRentPayable.Text);
            paymentPlan.ArrearsCollection = Convert.ToDouble(txtArrearsCollection.Text);

            return paymentPlan;
        }

        #endregion

        #region"Set Payment Plan History"

        public AM_PaymentPlanHistory SetPaymentPlanHistory()
        {
            AM_PaymentPlanHistory paymentPlanlHistory = new AM_PaymentPlanHistory();


            paymentPlanlHistory.TennantId = base.GetTenantId();
            paymentPlanlHistory.PaymentPlanType = Convert.ToInt32(ddlPaymentType.SelectedValue);
            //paymentPlanlHistory.StartDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);
            paymentPlanlHistory.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);
            paymentPlanlHistory.AmountToBeCollected = Convert.ToDouble(txtAmountCollected.Text);
            paymentPlanlHistory.WeeklyRentAmount = Convert.ToDouble(txtWeeklyRentAmount.Text);
            paymentPlanlHistory.ReviewDate = DateTime.ParseExact(txtReviewDate.Text, "dd/MM/yyyy", null);
            paymentPlanlHistory.CreatedDate = DateTime.Now;
            paymentPlanlHistory.ModifiedDate = DateTime.Now;
            paymentPlanlHistory.IsCreated = true;
            paymentPlanlHistory.FrequencyLookupCodeId = Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue);
            paymentPlanlHistory.CreatedBy = base.GetCurrentLoggedinUser();
            paymentPlanlHistory.ModifiedBy = base.GetCurrentLoggedinUser();
            paymentPlanlHistory.FirstCollectionDate = DateTime.ParseExact(txtFirstCollectionDate.Text, "dd/MM/yyyy", null);

            paymentPlanlHistory.LastPaymentDate = DateOperations.ConvertStringToDate(Convert.ToString(Session[SessionConstants.lastPaymentDate]));
            paymentPlanlHistory.RentBalance = Convert.ToDouble(txtArrearsBalance.Text);
            paymentPlanlHistory.LastPayment = Convert.ToDouble(Session[SessionConstants.lastPayment]);
            paymentPlanlHistory.RentPayable = Convert.ToDouble(txtRentPayable.Text);
            paymentPlanlHistory.ArrearsCollection = Convert.ToDouble(txtArrearsCollection.Text);
            return paymentPlanlHistory;
        }

        #endregion

        #region "Calculate Reminder"
        public double calculateReminder()
        {
            //update by umair
            //update date:30 08 2011

            //double MonthlyHB=0.0;
            //double _RentPayable = 0.0;


            //if (Session[SessionConstants.HBCyclePayment] != null)
            //{   if (Convert.ToDouble(Session[SessionConstants.HBCyclePayment]) != 0.0)
            //    {
            //        MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
            //        _RentPayable = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB);
            //    }
            //    else
            //    {
            //        _RentPayable = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]);
            //    }
            //}

            double _RentPayable = Convert.ToDouble(txtRentPayable.Text);
            double _AgreedAmount = txtAmountCollected.Text == "" ? 0.0 : Convert.ToDouble(txtAmountCollected.Text);
            double _TotalAmountToBeCollected = txtArrearsBalance.Text == "" ? 0.0 : Convert.ToDouble(txtArrearsBalance.Text);
            double _totalInstallments;

            if (ddlFrequencyRegularPayment.SelectedItem.Text == "Full Amount")
            {
                _totalInstallments = 1.0;
            }
            else
            {
                _totalInstallments = CreateInstallments(_TotalAmountToBeCollected, _AgreedAmount);
            }

            return _RentPayable + (_TotalAmountToBeCollected - ((_totalInstallments - 1.0) * _AgreedAmount));

            //if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
            //{
            //   return ((_RentPayable*12)/52*2) + (_TotalAmountToBeCollected - ((_totalInstallments - 1.0) * _AgreedAmount));
            //}
            //else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
            //{
            //    return ((_RentPayable * 12) / 52) + (_TotalAmountToBeCollected - ((_totalInstallments - 1.0) * _AgreedAmount));
            //}
            //else
            //{
            //    return _RentPayable + (_TotalAmountToBeCollected - ((_totalInstallments - 1.0) * _AgreedAmount));
            //}

            //end update 
        }

        #endregion

        #region "calculate Arrears Collection Fornightly"

        public string CalcArrearCollection_Fornightly()
        {
            //double _RentPayable = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
            double _AgreedAmount = txtAmountCollected.Text == "" ? 0.0 : Convert.ToDouble(txtAmountCollected.Text);
            //double _result = ((_RentPayable * 12) / 52 * 2) + _AgreedAmount;

            //update by: Umair 
            //Update Date:26 08 2011

            //double MonthlyHB = 0.0;
            double _result = 0.0;

            if (txtRentPayable.Text.Equals(string.Empty))
            {
                //if (Session[SessionConstants.HBCyclePayment] != null)
                if (Session[SessionConstants.HBPayment] != null)
                {
                    //MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
                    //_result = (((Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB) / 30.42) * 14) + _AgreedAmount;
                    double FortnightlyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 26;
                    double FortnightlyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 2;
                    _result = (FortnightlyRent - FortnightlyHB) + _AgreedAmount;
                }
                else
                {
                    double FortnightlyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 26;
                    _result = FortnightlyRent + _AgreedAmount;
                }
            }
            else
            {
                _result = Convert.ToDouble(txtRentPayable.Text) + _AgreedAmount;

            }
            //update end
            return Math.Round(_result, 2).ToString();
        }

        #endregion

        #region "calculate Arrears Collection Weekly"

        public string CalcArrearCollection_Weekly()
        {

            // double _RentPayable = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
            double _AgreedAmount = txtAmountCollected.Text == "" ? 0.0 : Convert.ToDouble(txtAmountCollected.Text);
            //double _result = ((_RentPayable * 12) / 52) + _AgreedAmount;

            //update by: Umair 
            //Update Date:26 08 2011

            //double MonthlyHB = 0.0;
            double _result = 0.0;

            if (txtRentPayable.Text.Equals(string.Empty))
            {

                //if (Session[SessionConstants.HBCyclePayment] != null)
                if (Session[SessionConstants.HBPayment] != null)
                {
                    //MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
                    //_result = (((Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB) / 30.42) * 7) + _AgreedAmount;
                    double WeeklyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 52;
                    double WeeklyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 4;
                    _result = (WeeklyRent - WeeklyHB) + _AgreedAmount;
                }
                else
                {
                    double WeeklyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 52;
                    _result = WeeklyRent + _AgreedAmount;
                }
            }
            else
            {
                _result = Convert.ToDouble(txtRentPayable.Text) + _AgreedAmount;

            }
            //update end
            return Math.Round(_result, 2).ToString();
        }

        #endregion


        #region "Set Amount to be collected"
        public void SetAmountToBeCollected()
        {
            if (ddlFrequencyRegularPayment.SelectedItem.Text == "Full Amount")
            {
                txtAmountCollected.Text = txtArrearsBalance.Text;
            }
            else
            {
                txtAmountCollected.Text = "";
            }
        }
        #endregion


        #endregion
    }
}