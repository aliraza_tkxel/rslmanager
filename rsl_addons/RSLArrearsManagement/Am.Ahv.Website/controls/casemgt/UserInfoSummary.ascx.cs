using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.Customers;
using System.Drawing;
using System.Data;
using Am.Ahv.Website.pagebase;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Utilities.utitility_classes;



namespace Am.Ahv.Website.controls.casemgt
{
    public partial class UserInfoSummary : UserControlBase
    {
        #region"Attributes"

        Am.Ahv.BusinessManager.Customers.Customers customerManager = null;
        Validation val = new Validation();

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                GetCustomerInformation();
            }
            ResetMessage();
        }

        #endregion      

        #endregion

        #region"Populate Data"

        #region"Get Customer Information"

        public void GetCustomerInformation()
        {
            try
            {
                customerManager = new BusinessManager.Customers.Customers();               

                if (!Convert.ToString(Session[SessionConstants.TenantId]).Equals(string.Empty) && !Convert.ToString(Session[SessionConstants.CustomerId]).Equals(string.Empty))
                {
                    AM_SP_GetCustomerInformation_Result customer = customerManager.GetCustomerInformation(Convert.ToInt32(Session[SessionConstants.TenantId]), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    PopulateCustomerInformation(customer);
                }
                else
                {
                    lbtnUserName.Text = "No Information is available.";
                    lbtnUserName.ForeColor = Color.Red;
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingGettingUserInfo, true);
                }
            }
        }

        #endregion

        #region"Populate Customer Information"

        public void PopulateCustomerInformation(AM_SP_GetCustomerInformation_Result customer)
        {
            RemoveSessionCustomerInformation();
            //Nouman added the code
            if (customer != null)
            {
                double rent = Convert.ToDouble(customer.RentBalance);//Convert.ToDouble(RentBalance.CalculateRentBalance(Convert.ToDouble(customer.RentBalance), Convert.ToDouble(customer.rentAmount)));
                if (customer.JointTenancyCount == 1)
                {
                    lbtnUserName.Text = customer.CustomerName;
                }
                else
                {
                    lbtnUserName.Text = customer.CustomerName + " & " + customer.CustomerName2; 
                }
                
                if (customer.CustomerAddress != null)
                {
                    lblUserAddress.Text = customer.CustomerAddress;
                }
                else
                {
                    lblUserAddress.Text = string.Empty;
                }
                lblTenancyId.Text = customer.TENANCYID.ToString();


                lbtnRentBalance.Text = val.SetBracs(rent.ToString());
                lbtnRentBalance.ForeColor = val.SetForeColor(rent.ToString());
                

                string renturl = "../../../Customer/Popups/Tenant_Statement.asp?customerid=" + customer.CustomerId.ToString() +"&tenancyid=" + customer.TENANCYID.ToString();
                lbtnRentBalance.Attributes.Add("onClick", "window.open('" + renturl + "'," + ApplicationConstants.javaScriptWindowAttributes);

                lblOwedToBHAAmount.Text = val.SetBracs(customer.OwedToBHA.ToString());
                lblOwedToBHAAmount.ForeColor = val.SetForeColor(customer.OwedToBHA.ToString());

                
                Session[SessionConstants.OwedTBHA] = Math.Round(Convert.ToDouble(customer.OwedToBHA), 2);
                Session[SessionConstants.LastMonthNetArrears] = Math.Round(Convert.ToDouble(customer.LastMonthNetArrears), 2);
                
                lblLastPayment.Text = val.SetBracs(customer.LastPayment.ToString());
                lblLastPayment.ForeColor = val.SetForeColor(customer.LastPayment.ToString());
                
                lblLastPaymentDate.Text = customer.LastPaymentDate;
                
                
                Session[SessionConstants.lastPayment] = Math.Round(Convert.ToDouble(customer.LastPayment), 2).ToString();
                
                Session[SessionConstants.lastPaymentDate] = lblLastPaymentDate.Text;
                Session[SessionConstants.Rentbalance] = Math.Round(rent, 2).ToString();
                Session[SessionConstants.WeeklyRentAmount] = customer.rentAmount.Value;
                
                lblMonthlyRentAmount.Text = "� " + Math.Round(Math.Abs(Convert.ToDouble(customer.rentAmount)), 2).ToString();

                string url = string.Format("../../../Customer/CRM.asp?CustomerID={0}", Convert.ToString(Session[SessionConstants.CustomerId]));
                lbtnUserName.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
            else
            {
                lbtnUserName.Text = "No Information Available.";
                lbtnUserName.ForeColor = Color.Red;                
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Set Information Of Customer"

        public void SetInformationOfCustomer(C__CUSTOMER customer,C_ADDRESS address)
        {
            lbtnUserName.Text = customer.TITLE + ' ' + customer.FIRSTNAME + ' ' + customer.LASTNAME;
            if (address == null)
            {
                lblUserAddress.Text = "Not Given";
            }
            else
            {
                lblUserAddress.Text = address.ADDRESS1;
            }
        }

        #endregion

        #region"Set Information"

        public void setInformation(string userName, string address, string suburb,string rentBalance,string lastPaymentAmount, string lastPaymentDate )
        {
            lbtnUserName.Text = userName;
            lblUserAddress.Text = address;
            lblTenancyId.Text = suburb;
            lbtnRentBalance.Text = rentBalance;
            lblLastPayment.Text = lastPaymentAmount;
            lblLastPaymentDate.Text =lastPaymentDate;

        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion       

        public void RemoveSessionCustomerInformation()
        {
            Session.Remove(SessionConstants.lastPayment);
            Session.Remove(SessionConstants.OwedTBHA);
            Session.Remove(SessionConstants.WeeklyRentAmount);
            Session.Remove(SessionConstants.Rentbalance);
            Session.Remove(SessionConstants.lastPaymentDate);

        }

        #endregion
    }
}