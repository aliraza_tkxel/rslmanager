﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Entities;
using System.Data;
using System.Drawing;
//using System.Configuration.Assemblies;
using System.IO;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Utilities.utitility_classes;

using Am.Ahv.BusinessManager.resource;
using Am.Ahv.BusinessManager.documents;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.reports;
using System.Text.RegularExpressions;
using Am.Ahv.BusinessManager.letters;
using System.Configuration;
using System.Collections;
namespace Am.Ahv.Website.controls.casemgt
{
    public partial class AddActivity : UserControlBase
    {
        #region"Attributes"
        int resourceID = -1;
        Users userManager = null;
        ArrayList SavedLetters;
        ArrayList StandardLetterHistoryList;
        double marketRent = 0;
        double rentBalance = 0;
        DateTime todayDate = new DateTime();
        double rentAmount = 0;

        bool isError;
        bool isException;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set {  isException = value;}
        }
        static int statusid;
        public int StatusId
        {
            get { return statusid; }
            set { statusid = value; }
        }
        static int actionid;
        public int ActionId
        {
            get { return actionid; }
            set { actionid = value; }
        }
        #endregion
      
        #region "Events"

        #region "Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            resetMessage();
            try
            {

                userManager = new Users();
                if (!IsPostBack)
                {
                    initlookups();
                    showAcionAndStatus();
                    if (Request.QueryString[ApplicationConstants.cmd] != null)
                    {
                        if (Request.QueryString[ApplicationConstants.cmd] == "LetterSelect")
                        {
                            if (Validation.CheckIntegerValue(Request.QueryString["LID"]))
                            {
                                PopulateActivity();
                                PopulateLetters(int.Parse(Request.QueryString["LID"]));
                            }
                        }
                        else if (Request.QueryString["cmd"] == "activityedit")
                        {
                            PopulateActivity();
                            PopulateLetters(0);
                            //CheckSavedLetters();
                            if (Request.QueryString["p"] != null)
                            {
                                if (Request.QueryString["p"] == "true")
                                {
                                    if (Request.QueryString["slid"] != null)
                                    {
                                        ChangeTheColorOfLetter(0, "Black", Convert.ToString(Request.QueryString["slid"]), true);
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    //else
                    //{
                    //    initlookups();
                    //    showAcionAndStatus();
                    //}
                    
                }
                CheckSavedLetters();
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex,"Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.errorPageLoading, true);
                }
            }
        }

        #endregion

        #region "btn Save Activity Click"
        protected void btnSaveActivity_Click(object sender, EventArgs e)
        {
            SaveActivity();
            RemoveActivitySessions();
        }
        #endregion

        #region "btn Cancel Click"
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            clearForm();
        }
         #endregion

        #region "ddlActivity Selected Index Changed"
        protected void ddlActivity_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowReferralView();
        }
        #endregion
        
        #region "RadioButtonList ReferralType_Selected Index Changed"
        protected void RadioButtonListReferralType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonListReferralType.SelectedItem.Text == ApplicationConstants.ReferralType_Internal)
            {
                ShowInternalView();

            }
            else if (RadioButtonListReferralType.SelectedItem.Text == ApplicationConstants.ReferralType_External)
            {
                ShowExternalView();
            }
            else if (RadioButtonListReferralType.SelectedItem.Text == ApplicationConstants.ReferralType_SupportWorker)
            {
                ShowInternalView();
                LoadSupportWorker();
            }
        }
                #endregion

        #region "ddlTeam Selected Index Changed"

         protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlTeam.SelectedValue != Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue)
                {
                    if (RadioButtonListReferralType.SelectedItem.Text == ApplicationConstants.ReferralType_SupportWorker)
                    {
                        Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();

                        if (ddlName.Items.Count > 0)
                            ddlName.Items.Clear();
                        List<E__EMPLOYEE> emplist = cw.GetSupportWorkers(int.Parse(ddlTeam.SelectedValue));

                        ddlName.DataSource = PopluateEmps(emplist);
                        ddlName.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.EmloyeeName;
                        ddlName.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.EmployeId;
                        ddlName.DataBind();
                        ddlName.Items.Add(new ListItem(ApplicationConstants.SelectName, Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue));
                        ddlName.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                    }
                    else
                    {
                        Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();

                            if (ddlName.Items.Count > 0)
                            ddlName.Items.Clear();
                            ddlName.DataSource = PopluateEmps(cw.GetTeamMembers(int.Parse(ddlTeam.SelectedValue)));//cw.GetTeamMembers(int.Parse(ddlTeam.SelectedValue));
                            ddlName.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.EmloyeeName;
                             ddlName.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.EmployeId;
                             ddlName.DataBind();
                             ddlName.Items.Add(new ListItem(ApplicationConstants.SelectName, Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue));
                             ddlName.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                    }
                }
                else
                {
                    if (ddlName.Items.Count > 0)
                        ddlName.Items.Clear();
                    ddlName.Items.Add(new ListItem(ApplicationConstants.SelectName, Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue));
                }
            }

            catch (NullReferenceException nullref)
            {
                isError = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (KeyNotFoundException keynotfound)
            {
                isError = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (Exception ex)
            {
                isError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally {
                if (isError) setMessage(UserMessageConstants.ErrorLoadingNames,true);
            }
        }

       
       #endregion

        #region "btnSaveReferral_Click"
        protected void btnSaveReferral_Click(object sender, EventArgs e)
        {
            SaveReferral();
            RemoveActivitySessions();
        }
        #endregion

        #region"btn Back to activities_Click"
        protected void btnBacktoactivities_Click(object sender, EventArgs e)
        {
            RemoveActivitySessions();
            Response.Redirect(PathConstants.viewActivitiesPath, false);
        }
        #endregion

        #region"ddlOrganisation Selected Index Changed"
        
        protected void ddlOrganisation_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadContacts(ddlOrganisation.SelectedValue);
        }
        #endregion

        #region "ddlContacts Selected Index Changed"
        protected void ddlContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadEmail(ddlContacts.SelectedValue);
        }
        #endregion

        #region"Show Internal View"

        private void ShowInternalView()
        {
            lblActivityHeading.Text = ApplicationConstants.ActivityHeading;
            AddActivityPanel1.Visible = false;
            AddActivityPanel1.Update();
            AddActivityPanel2.Visible = false;
            AddActivityPanel2.Update();
            ActivityUpdatePanel3.Visible = false;
            ActivityUpdatePanel3.Update();
            updatePnlHeading.Update();

            UpdatePanelRefer1.Visible = true;
            UpdatePanelRefer1.Update();
            UpdatePanelRefer2.Visible = true;
            UpdatePanelRefer2.Update();

            ActivityUpdatePanel1.Visible = true;
            ActivityUpdatePanel1.Update();
            ActivityUpdatePanel3.Visible = true;
            ActivityUpdatePanel3.Update();

            AddActivityPanel1.Visible = true;
            AddActivityPanel1.Update();
            //ActivityUpdatePanel2.Visible = false;
            //ActivityUpdatePanel2.Update();
            ActivityUpdatePanel4.Visible = false;
            ActivityUpdatePanel4.Update();
        }
        #endregion

        #region"Show External View"
        private void ShowExternalView()
        {
            AddActivityPanel2.Visible = true;
            AddActivityPanel2.Update();
            ActivityUpdatePanel3.Visible = true;
            ActivityUpdatePanel3.Update();
            UpdatePanelRefer1.Visible = true;
            UpdatePanelRefer2.Update();
            lblActivityHeading.Text = ApplicationConstants.ReferHeading;
            updatePnlHeading.Update();

            ActivityUpdatePanel1.Visible = false;
            ActivityUpdatePanel1.Update();
            AddActivityPanel1.Visible = false;
            AddActivityPanel1.Update();
        }
        #endregion

        #region"btn Add Click"
        protected void btnAdd_Click(object sender, EventArgs e)
        {
           // FileInfo file = new FileInfo(FlUpload.FileName);
            //string fileExt = Path.GetExtension(FlUpload.FileName);
            //int size = Convert.ToInt32(ConfigurationManager.AppSettings["FileSize"]);
            //if (!FileOperations.CheckFileExtension(FlUpload.FileName))
            //{
            //    SetMessage(UserMessageConstants.invalidImageType, true);
            //    return false;                
        //    [1:57:31 PM] zunairminhas:   #region"Validate File"

        //public bool ValidateFile()
        //{
        //   // FileInfo file = new FileInfo(FlUpload.FileName);
        //    string fileExt = Path.GetExtension(FlUpload.FileName);
        //    int size = Convert.ToInt32(ConfigurationManager.AppSettings["FileSize"]);
        //    if (!FileOperations.CheckFileExtension(FlUpload.FileName))
        //    {
        //        SetMessage(UserMessageConstants.invalidImageType, true);
        //        return false;                
        //    }
        //    else if (FlUpload.PostedFile.ContentLength > size)
        //    {
        //        SetMessage(UserMessageConstants.fileSizeCaseOpen, true);
        //        return false;
        //    }
        //    else
        //        return true;
        //}
            try
          {

              if (txtActivityFile.HasFile)
              {
                  if (FileOperations.CheckFileExtension(txtActivityFile.FileName))
                  {
                      int size = Convert.ToInt32(ConfigurationManager.AppSettings["FileSize"]);
                      if (txtActivityFile.PostedFile.ContentLength <= size)
                      {
                          string destinationPath = System.Configuration.ConfigurationManager.AppSettings[ApplicationConstants.ActivityUploadPath];
                          if (!Directory.Exists(destinationPath))
                          {
                              Directory.CreateDirectory(destinationPath);
                          }

                          destinationPath = destinationPath + txtActivityFile.FileName;
                          txtActivityFile.SaveAs(destinationPath);
                          lstLetters.Items.Add(new ListItem(txtActivityFile.FileName));
                          ActivityUpdatePanel4.Update();
                      }
                      else
                      {
                          setMessage(UserMessageConstants.ActivityFileSizeMessage,true);
                      }
                  }
                  else
                  {
                      setMessage(UserMessageConstants.invalidImageType,true);
                  }
                  
                 
              }
          }
          catch (DirectoryNotFoundException ex)
          {
              IsException = true;
              ExceptionPolicy.HandleException(ex, "Exception Policy");
          }
          catch (Exception ex)
          {
              IsException = true;
              ExceptionPolicy.HandleException(ex, "Exception Policy");
          }
          finally
          {
              if (IsException)
              {
                 // SetMessage(UserMessageConstants.exceptionUploadFileCaseOpen, true);
              }
          }
        }
        
        #endregion

        #region"Btn Standard Letter Click"

        protected void btnStandardLetter_Click(object sender, EventArgs e)
        {
            try
            {
                PutActivityinSession();
                Response.Redirect(PathConstants.lettersResourceAreaPathCached + "?cmd=ActivitySelect&SID=" + StatusId.ToString() + "&AID=" + ActionId.ToString(), false);
            }
            catch (NullReferenceException nullref)
            {
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"btn Remove Click"
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (lstLetters.SelectedValue == "-1" || lstLetters.SelectedValue == "")
            {
               // SetMessage(UserMessageConstants.selectOptionRemoveDocuments, true);
                return;
            }
            else
            {
               // if (Validation.CheckIntegerValue(lstLetters.SelectedValue))
              //  {
                    int index = lstLetters.SelectedIndex;
                    lstLetters.Items.RemoveAt(index);
                    if (lstLetters.Items.Count <=0)
                    {
                        Session["ActivityLetters"] = "No Data";
                    }

                    Session["ActivityLetters"] = lstLetters;
              //  }
             //   else
             //   {
                    //GetListFromViewState();
                    //arrayListDocumentName.Remove(lstDocuments.SelectedItem.Text);
                    //int index = lstDocuments.SelectedIndex;
                    //lstDocuments.Items.RemoveAt(index);
                    //SetViewStateList();
             //   }
            }
        }
        #endregion       

        #region"Btn Edit Click"

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (lstLetters.SelectedValue == "-1" || lstLetters.SelectedValue == "")
            {
                setMessage(UserMessageConstants.selectOptionEditDocuments, true);
                return;
            }
            else
            {
                if (Validation.CheckIntegerValue(lstLetters.SelectedValue))
                {

                    int tenantid = -1;
                    if (Session[SessionConstants.TenantId] != null)
                        tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                    PutActivityinSession();
                    SavedLetters = GetSavedLettersViewStateList();
                    if (SavedLetters != null)
                    {
                        if (SavedLetters.Contains(int.Parse(lstLetters.SelectedValue)))
                        {
                            int historyId = GetStandardLetterHistoryId(int.Parse(lstLetters.SelectedValue));
                            if (historyId > 0)
                            {
                                Response.Redirect(PathConstants.activityLetterTemplateEdit + "id=" + lstLetters.SelectedValue + "&hId=" + historyId.ToString() + "&TID=" + tenantid, false);
                            }
                            else
                            {
                                Response.Redirect(PathConstants.activityLetterTemplateEdit + "id=" + lstLetters.SelectedValue + "&TID=" + tenantid, false);
                            }
                        }
                        else
                        {
                            Response.Redirect(PathConstants.activityLetterTemplateEdit + "id=" + lstLetters.SelectedValue + "&TID=" + tenantid, false);
                        }
                    }
                    else
                    {
                        Response.Redirect(PathConstants.activityLetterTemplateEdit + "id=" + lstLetters.SelectedValue + "&TID=" + tenantid, false);
                    }
                }
                else
                {
                    setMessage(UserMessageConstants.selectStandardLettersEditDocuments, true);
                    return;
                }
            }
        }

        #endregion

        #region"Btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.casehistory, false);
        }

        #endregion

        #endregion

        #region Populating Employees
        private DataTable PopluateEmps(List<E__EMPLOYEE> emplist)
        {
            DataTable dtEmployee = new DataTable();
            dtEmployee.Columns.Add("EmployeeName");
            dtEmployee.Columns.Add("EmployeeId");
            if (emplist != null)
            {
                if (emplist.Count > 0)
                {
                    for (int i = 0; i < emplist.Count; i++)
                    {
                        DataRow dr = dtEmployee.NewRow();
                        dr["EmployeeName"] = emplist[i].FIRSTNAME + " " + emplist[i].LASTNAME;
                        dr["EmployeeId"] = emplist[i].EMPLOYEEID;
                        dtEmployee.Rows.Add(dr);
                    }
                }
            }
            return dtEmployee;
        }
        #endregion

        #region"Init Lookups"

        private void initlookups()
        {
            try
            {
                Am.Ahv.BusinessManager.Casemgmt.AddCaseActivity addCaseActivity = new Am.Ahv.BusinessManager.Casemgmt.AddCaseActivity();
                ddlActivity.DataSource = addCaseActivity.GetActivityTypes();
                ddlActivity.DataValueField = ApplicationConstants.codeId;
                ddlActivity.DataTextField = ApplicationConstants.codename;
                ddlActivity.DataBind();
                ddlActivity.Items.Add(new ListItem(ApplicationConstants.SelectActivity, ApplicationConstants.defaulvalue));
                ddlActivity.SelectedValue = ApplicationConstants.defaulvalue;

                ddlOrganisation.DataSource = addCaseActivity.GetExternalAgencyOrganisations();
                ddlOrganisation.DataValueField = ApplicationConstants.orgId;
                ddlOrganisation.DataTextField = ApplicationConstants.orgName;
                ddlOrganisation.DataBind();
                ddlOrganisation.Items.Add(new ListItem(ApplicationConstants.SelectOrganisation,ApplicationConstants.defaulvalue));
                ddlOrganisation.SelectedValue = ApplicationConstants.defaulvalue;

                ddlContacts.Items.Add(new ListItem(ApplicationConstants.SelectContact, ApplicationConstants.defaulvalue));
                ddlContacts.SelectedValue = ApplicationConstants.defaulvalue;

                Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker addCaseWorker = new BusinessManager.Casemgmt.AddCaseWorker();

                ddlTeam.DataSource = addCaseWorker.GetAllTeams();
                ddlTeam.DataValueField = ApplicationConstants.teamId;
                ddlTeam.DataTextField = ApplicationConstants.teamname;
                ddlTeam.DataBind();
                ddlTeam.Items.Add(new ListItem(ApplicationConstants.SelectTeam, ApplicationConstants.defaulvalue));
                ddlTeam.SelectedValue = ApplicationConstants.defaulvalue;

                List<AM_LookupCode> LookupcodeList = addCaseActivity.GetReferralType();
                RadioButtonListReferralType.DataValueField = ApplicationConstants.codeId;
                RadioButtonListReferralType.DataTextField = ApplicationConstants.codename;
                RadioButtonListReferralType.DataSource = LookupcodeList;
                RadioButtonListReferralType.DataBind();
                RadioButtonListReferralType.Items[0].Selected = true;
                txtDateOfReferral.Text = DateTime.Today.Date.Day.ToString() + "/" + DateTime.Today.Date.Month.ToString() + "/" + DateTime.Today.Date.Year.ToString();

            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException) setMessage(UserMessageConstants.ErrorLoadingLookups, true);
            }
        }

        #endregion

        #region "Functions"

       #region "show Action and Status"

        private void showAcionAndStatus()
        {
            try
            {
                CaseDetails CDetails = new CaseDetails();
                AM_Case cdetails = CDetails.GetCaseById(base.GetCaseId());
                if (cdetails != null)
                {
                    this.lblAction.Text = cdetails.AM_Action.Title;
                    this.lblStatus.Text = cdetails.AM_Status.Title;
                    ActionId = cdetails.AM_Action.ActionId;
                    StatusId = cdetails.AM_Status.StatusId;
                }
            }
             catch(Exception ex)
            {
                isException = true;
                 ExceptionPolicy.HandleException(ex,"Exception Policy");
            }
            finally
            {
                if (isException) 
                    setMessage(UserMessageConstants.ErrorActionStatus,true);
            }


           
        }
        #endregion

       #region"Save Activity"
        private void SaveActivity()
        {
            try
            {
                ActivityValidation();
                if (!isError)
                {
                    CaseDetails CDetails = new CaseDetails();
                    AM_Case cdetails = CDetails.GetCaseById(base.GetCaseId());
                    AM_Activity activity = new AM_Activity();
                    CaseHistory caseHistory = new CaseHistory();
                    activity.ActionHistoryId = cdetails.ActionHistoryId;
                    activity.StatusHistoryId = cdetails.StatusHistoryId;
                    activity.ActionId = cdetails.ActionId;
                    activity.StatusId = cdetails.StatusId;
                    activity.ActivityLookupId = Convert.ToInt32(ddlActivity.SelectedValue);
                    //activity.Title = txtTitle.Text;
                    activity.Notes = txtNotes.Text;
                   // activity.Reason = txtReason.Text;
                    activity.RecordedBy = base.GetCurrentLoggedinUser();
                    activity.RecordedDate = DateTime.Now;
                    activity.CaseId = base.GetCaseId();
                    activity.CaseHistoryId = caseHistory.GetLatestCaseHistoryId(base.GetCaseId());
                    List<string> ListLetters = new List<string>();
                    activity.IsDocumentAttached = false;
                    activity.IsLetterAttached = false;

                    StandardLetterHistoryList = GetStandardLetterHistoryList();
                    SavedLetters = GetSavedLettersViewStateList();

                    foreach (ListItem item in lstLetters.Items)
                    {
                        bool flag = false;
                        if (item.Value != "-1")
                        {
                            if (!Validation.CheckIntegerValue(item.Value))
                            {
                                activity.IsDocumentAttached = true;
                            }
                            else
                            {
                                activity.IsLetterAttached = true;                                
                                if (StandardLetterHistoryList != null)
                                {
                                    for (int j = 0; j < StandardLetterHistoryList.Count; j++)
                                    {
                                        string[] str = StandardLetterHistoryList[j].ToString().Split(';');
                                        if (str[0].Equals(item.Value))
                                        {
                                            ListLetters.Add(StandardLetterHistoryList[j].ToString());
                                            flag = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (!flag)
                        {
                            ListLetters.Add(item.Value);
                        }
                    }
                    AddCaseActivity activityMgr = new AddCaseActivity();
                    this.setRbMrDate();
                    int isActivitySaved = activityMgr.SaveActivity(activity, ListLetters, this.marketRent, this.rentBalance, this.todayDate, this.rentAmount);     
                    setMessage(UserMessageConstants.ActivitySavedMassage, false);
                    clearForm();
                }
            }
            catch (DirectoryNotFoundException ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.errorSavingCaseActivity, true);
                }
            }  
        }
        #endregion

        #region set Rb Mr Date

        /// <summary>
        /// This function sets the rent balance, market rent and today's date, assuming that new case object is filled with entries
        /// </summary>
        /// <param name="newCase">object of new case</param>
        private void setRbMrDate()
        {
            PageBase pageBase = new PageBase();
            this.rentBalance = Convert.ToDouble(base.GetRentBalance());
            this.todayDate = DateTime.Now;
            this.rentAmount = Convert.ToDouble(pageBase.GetWeeklyRentAmount());

            if (Session[SessionConstants.TenantId] != null && Session[SessionConstants.Rentbalance] != null)
            {
                LetterResourceArea letterarea = new LetterResourceArea();

                int tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                this.marketRent = letterarea.GetMarketRent(tenantid);

            }
        }
        #endregion 

       #region "Save Referral"

        private void SaveReferral()
        {
             try
             {
                 if (RadioButtonListReferralType.SelectedItem.Text == ApplicationConstants.ReferralType_Internal || RadioButtonListReferralType.SelectedItem.Text == ApplicationConstants.ReferralType_SupportWorker)
                 {
                     SaveInternalReferral();
                 }
                 else if (RadioButtonListReferralType.SelectedItem.Text == ApplicationConstants.ReferralType_External)
                 {
                     SaveExternalReferral();
                 }
             }
             catch (Exception ex)
             {
                 isException = true;
                 ExceptionPolicy.HandleException(ex, "Exception Policy");
             }
             finally 
             {
                 if (isException)
                 {
                     setMessage(UserMessageConstants.CaseActivityReferralSavingError, true);
                 }
             }

        
        }
        #endregion

       #region "Activity Validation"

        private void ActivityValidation()
        {                
            if (ddlActivity.SelectedValue == ApplicationConstants.defaulvalue)
            {
                setMessage(UserMessageConstants.noActivityTypeSelected,true);
            }
            //else if (txtTitle.Text == "")
            //{
            //    setMessage(UserMessageConstants.noTitleEntered, true);
            //}
            else if (txtNotes.Text == "")
            {
                setMessage(UserMessageConstants.noNotesEntered, true);
            }
            //else if (txtNotes.Text.Length > 1000)
            //{
            //    setMessage("Notes can not have more than 1000 characters", true);
            //}
            //else if (lstLetters.Items.Count == 0)
            //{
            //    setMessage(UserMessageConstants.noDocumentSelected, true);
            //}
            //else if (txtReason.Text == "")
            //{
            //    setMessage(UserMessageConstants.noReasonEntered, true);
            //}
            //else if (lstLetters.Items.Count > 0)
            //{
            //    foreach (ListItem l in lstLetters.Items)
            //    {
            //        if (!Validation.CheckIntegerValue(l.Value))
            //        {
            //            if (!FileOperations.CheckFileExtension(l.Value))
            //            {
            //                setMessage(UserMessageConstants.invalidImageType, true);
            //            }
            //        }
            //    }
                
            //}
        }

        #endregion

       #region "Referral Validation"
        private void ReferralValidation()
        {
            
            if (ddlActivity.SelectedValue == ApplicationConstants.defaulvalue)
            {
                setMessage(UserMessageConstants.noActivityTypeSelected, true);
            }
            else if (txtDateOfReferral.Text == "")
            {
                setMessage(UserMessageConstants.noReferralDateSelected,true);
            }
            else if(txtDateFollowup.Text == "")
            {
                setMessage(UserMessageConstants.noFollowupdateSelected,true);
            }
            else if (ddlTeam.SelectedValue == ApplicationConstants.defaulvalue)
            {
                setMessage(UserMessageConstants.noTeamSelected,true);
            }
            else if (ddlName.Items.Count > 1 && ddlName.SelectedValue == ApplicationConstants.defaulvalue)
            { 
                setMessage(UserMessageConstants.noNameSelected,true);
            }
            else if (txtNotes.Text == "")
            {
                setMessage(UserMessageConstants.noNotesEntered,true);
            }
            //else if (txtNotes.Text.Length > 1000)
            //{
            //    setMessage("Notes can not have more than 1000 characters", true);
            //}
            else if(!DateOperations.isValidUkDate(txtDateOfReferral.Text))
            {
                setMessage(UserMessageConstants.InvalidReferralDate,true);
            }
            else if (!DateOperations.isValidUkDate(txtDateFollowup.Text))
            {
                setMessage(UserMessageConstants.InvalidFollowupDate, true);
            }
        }
              #endregion

       #region"External Referral Validation"
        private void ExternalReferralValidation()
        {
           if (txtDateOfReferral.Text == "")
            {
                setMessage(UserMessageConstants.noReferralDateSelected, true);
            }
            else if (txtDateFollowup.Text == "")
            {
                setMessage(UserMessageConstants.noFollowupdateSelected, true);
            }
           else if(ddlOrganisation.SelectedValue == ApplicationConstants.defaulvalue)
           {
               setMessage(UserMessageConstants.noOrgSelected,true);
           }
           else if (ddlContacts.SelectedValue == ApplicationConstants.defaulvalue)
           {
               setMessage(UserMessageConstants.noContactSelected,true);
           }
           else if (txtEmail.Text == "")
           {
               setMessage(UserMessageConstants.noEmail,true);
           }
            else if (txtNotes.Text == "")
            {
                setMessage(UserMessageConstants.noNotesEntered, true);
            }
           //else if (txtNotes.Text.Length > 1000)
           //{
           //    setMessage("Notes can not have more than 1000 characters", true);
           //}
           else if (!DateOperations.isValidUkDate(txtDateOfReferral.Text))
           {
               setMessage(UserMessageConstants.InvalidReferralDate, true);
           }
           else if (!DateOperations.isValidUkDate(txtDateFollowup.Text))
           {
               setMessage(UserMessageConstants.InvalidFollowupDate, true);
           }
           else if(!isValidEmail(txtEmail.Text))
           {
               setMessage(UserMessageConstants.EmailError,true);
           }


        }
           #endregion

       #region"Show Referral View"
        private void  ShowReferralView()
        {
            try
            {
                if (ddlActivity.SelectedItem.Text == ApplicationConstants.Refer)
                {
                    UpdatePanelRefer1.Visible = true;
                    UpdatePanelRefer1.Update();
                    UpdatePanelRefer2.Visible = true;
                    UpdatePanelRefer2.Update();
                    ActivityUpdatePanel1.Visible = true;
                    ActivityUpdatePanel1.Update();
                    ActivityUpdatePanel3.Visible = true;
                    ActivityUpdatePanel3.Update();
                    AddActivityPanel1.Visible = true;
                    AddActivityPanel1.Update();
                    //ActivityUpdatePanel2.Visible = false;
                    //ActivityUpdatePanel2.Update();
                    ActivityUpdatePanel4.Visible = false;
                    ActivityUpdatePanel4.Update();
                }
                else
                {
                    UpdatePanelRefer1.Visible = false;
                    UpdatePanelRefer1.Update();
                    UpdatePanelRefer2.Visible = false;
                    UpdatePanelRefer2.Update();
                    //ActivityUpdatePanel2.Visible = true;
                    //ActivityUpdatePanel2.Update();
                    ActivityUpdatePanel4.Visible = true;
                    ActivityUpdatePanel4.Update();
                    AddActivityPanel2.Visible = false;
                    AddActivityPanel2.Update();
                    AddActivityPanel1.Visible = false;
                    AddActivityPanel1.Update();
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex,"Exception Polocy");
            }
            finally 
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ReferralViewError, true);
                }
            }
        }
           #endregion        

       #region"Load Contacts"

        private void LoadContacts(string OrgId)
        {
            try
            {

                if (ddlContacts.Items.Count > 0)
                    ddlContacts.Items.Clear();        
                AddCaseActivity activityMgr = new AddCaseActivity();
                ddlContacts.DataSource = activityMgr.GetContacts( Int32.Parse(OrgId));
                ddlContacts.DataValueField = ApplicationConstants.EmployeId;
                ddlContacts.DataTextField =ApplicationConstants.ContactName;
                ddlContacts.DataBind();
                ddlContacts.Items.Add(new ListItem(ApplicationConstants.SelectContact,ApplicationConstants.defaulvalue));
                ddlContacts.SelectedValue = ApplicationConstants.defaulvalue;
                   
            }

            catch (NullReferenceException nullref)
            {
                isError = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (KeyNotFoundException keynotfound)
            {
                isError = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (Exception ex)
            {
                isError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isError)
                {
                    setMessage(UserMessageConstants.ErrorLoadingContacts, true);
                }
            }
        }
       
       #endregion

       #region"Load Email Address "
        private void LoadEmail(string EmployeeId)
        {
            try
            {
                AddCaseActivity activityMgr = new AddCaseActivity();
                txtEmail.Text =  activityMgr.GetContactEmail(Int32.Parse(EmployeeId));
            }
            catch(Exception ex)
            {
                ExceptionPolicy.HandleException(ex,"Exception Policy");
            }
        }
       #endregion

       #region"Save Internal Referral"

        private void SaveInternalReferral()
        {
            try
            {
               
                    ReferralValidation();
                    if (!isError)
                    {
                        AM_Referral referral = new AM_Referral();
                        referral.DateOfReferral = DateOperations.ConvertStringToDate(txtDateOfReferral.Text);
                        referral.FollowupDate = DateOperations.ConvertStringToDate(txtDateFollowup.Text);
                        referral.ReferralTypeLookupCodeId = Convert.ToInt32(RadioButtonListReferralType.Items[0].Value);
                        referral.TeamId = Convert.ToInt32(ddlTeam.SelectedValue);
                        if(ddlName.SelectedValue != ApplicationConstants.defaulvalue)
                        {
                            referral.TeamMemberName = ddlName.SelectedItem.Text;
                        }
                        referral.IsCustomerAware = true;
                        AddCaseActivity activityMgr = new AddCaseActivity();
                        AM_Referral savedReferObj = activityMgr.SaveReferral(referral);
                        if (savedReferObj.ReferralId > 0)
                        {
                            AM_Activity activity = new AM_Activity();
                            CaseDetails CDetails = new CaseDetails();
                            CaseHistory caseHistory = new CaseHistory();
                            AM_Case cdetails = CDetails.GetCaseById(base.GetCaseId());
                            activity.ActionHistoryId = cdetails.ActionHistoryId;
                            activity.StatusHistoryId = cdetails.StatusHistoryId;
                            activity.ActionId = cdetails.ActionId;
                            activity.StatusId = cdetails.StatusId;
                            activity.ActivityLookupId = Convert.ToInt32(ddlActivity.SelectedValue);
                            activity.Notes = txtNotes.Text;
                            activity.ReferralId = savedReferObj.ReferralId;
                            activity.RecordedDate = DateTime.Now;
                            activity.CaseId = base.GetCaseId();
                            activity.CaseHistoryId = caseHistory.GetLatestCaseHistoryId(base.GetCaseId());
                            activity.RecordedBy = base.GetCurrentLoggedinUser();
                            activity.RecordedDate = DateTime.Now;
                            activity.IsLetterAttached = false;
                            activity.IsDocumentAttached = false;
                            activityMgr.SaveActivity(activity);
                            setMessage(UserMessageConstants.CaseActivityReferralMsg, false);
                            ClearReferralForm();
                        }
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        #endregion

       #region"Save External Referral"

        private void SaveExternalReferral()
        {
            try 
            {
                ExternalReferralValidation();
                if (!isError)
                {
                    AM_Referral referral = new AM_Referral();
                    referral.DateOfReferral = DateOperations.ConvertStringToDate(txtDateOfReferral.Text);
                    referral.FollowupDate = DateOperations.ConvertStringToDate(txtDateFollowup.Text);
                    referral.ReferralTypeLookupCodeId = Convert.ToInt32(RadioButtonListReferralType.SelectedValue);
                    referral.IsCustomerAware = true;
                    referral.OrganisationId = Convert.ToInt32(ddlOrganisation.SelectedValue);
                    referral.ContactId = Convert.ToInt32(ddlContacts.SelectedValue);
                    referral.TeamMemberEmail = txtEmail.Text;

                    AddCaseActivity activityMgr = new AddCaseActivity();
                    string EmailBody = HttpUtility.HtmlDecode(UserMessageConstants.EmailFormat);
                    EmailBody =  EmailBody.Replace("[ReferralDate]", txtDateOfReferral.Text);
                    EmailBody = EmailBody.Replace("[FollowUpDate]", txtDateFollowup.Text);
                    EmailBody =  EmailBody.Replace("[Notes]", txtNotes.Text);
                    EmailBody =  EmailBody.Replace("[CustomerNumber]",GetCustomerId());
                    EmailBody = HttpUtility.HtmlDecode(EmailBody);
                    try
                    {
                        //int x = 0;
                        //int i = (1 / x);
                        PageBase.SendEmail(txtEmail.Text, UserMessageConstants.EmailSubjectExternalReferral, EmailBody, true);
                    }
                    catch (Exception ex)
                    {
                        setMessage(UserMessageConstants.ErrorSendingEmailAddCaseActivity, true);
                        return;
                    }
                    AM_Referral savedReferObj = activityMgr.SaveReferral(referral);
                    if (savedReferObj.ReferralId > 0)
                    {
                        AM_Activity activity = new AM_Activity();
                        CaseDetails CDetails = new CaseDetails();
                        CaseHistory caseHistory = new CaseHistory();
                        AM_Case cdetails = CDetails.GetCaseById(base.GetCaseId());
                        activity.ActionHistoryId = cdetails.ActionHistoryId;
                        activity.StatusHistoryId = cdetails.StatusHistoryId;
                        activity.ActionId = cdetails.ActionId;
                        activity.StatusId = cdetails.StatusId;
                        activity.ActivityLookupId = Convert.ToInt32(ddlActivity.SelectedValue);
                        activity.Notes = txtNotes.Text;
                        activity.ReferralId = savedReferObj.ReferralId;
                        activity.RecordedDate = DateTime.Now;
                        activity.CaseId = base.GetCaseId();
                        activity.CaseHistoryId = caseHistory.GetLatestCaseHistoryId(base.GetCaseId());
                        activity.RecordedBy = base.GetCurrentLoggedinUser();
                        activity.RecordedDate = DateTime.Now;
                        activity.IsLetterAttached = false;
                        activity.IsDocumentAttached = false;

                        activityMgr.SaveActivity(activity);
                        setMessage(UserMessageConstants.CaseActivityReferralMsg, false);
                        ClearReferralForm();
                    }
                    //setMessage(UserMessageConstants.CaseActivityReferralMsg, false);
                    //ClearReferralForm();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

       #region"Populate Letters"

        public void PopulateLetters(int letterId)
        {
            try
            {
                LetterResourceArea lresource = new LetterResourceArea();
                List<AM_StandardLetters> listcollection = new List<AM_StandardLetters>();
                if (Session["ActivityLetters"] != null)
                {
                    if (!Convert.ToString(Session["ActivityLetters"]).Equals("No Data"))
                    {
                        listcollection = Session["ActivityLetters"] as List<AM_StandardLetters>;
                    }                   
                }
                
                if (letterId != 0)
                {
                    ViewState["ActivityLetterId"] = letterId;
                    AM_StandardLetters Standardletters = lresource.GetStandardLetter(letterId);
                    listcollection.Add(Standardletters);
                }
                lstLetters.Items.Clear();
                lstLetters.DataSource = listcollection;
                lstLetters.DataTextField = "Title";
                lstLetters.DataValueField = "StandardLetterId";
                lstLetters.DataBind();

                if (Session["ActivityDocumentsUploaded"] != null)
                {
                    ArrayList documents = new ArrayList();
                    documents = Session["ActivityDocumentsUploaded"] as ArrayList;
                    
                    for(int i =0; i < documents.Count;i++)
                    {
                        lstLetters.Items.Add(documents[i].ToString());
                    }
                }

                if (Session["ActivitySavedLettersList"] != null)
                {
                    SavedLetters = Session["ActivitySavedLettersList"] as ArrayList;
                    SetSavedLettersViewStateList();
                }               
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

       #region"Load Letter List"

        private List<AM_StandardLetters> LoadLetterList(ListItemCollection listItemCollection)
        {
            List<AM_StandardLetters> Slist = new List<AM_StandardLetters>();
            AM_StandardLetters sletter = null;
            foreach (ListItem item in listItemCollection)
            {
                sletter = new AM_StandardLetters();
                if (item.Value != "-1")
                {
                    if (Validation.CheckIntegerValue(item.Value))
                    {
                        sletter.StandardLetterId = int.Parse(item.Value);
                        sletter.Title = item.Text;
                        Slist.Add(sletter);
                    }
                    else
                    {
                        sletter.StandardLetterId = -2; 
                        sletter.Title = item.Value;
                        Slist.Add(sletter);
                    }

                }
            }
            return Slist;
        }

        #endregion

       #region"Put Activity in Session"

        public void PutActivityinSession()
        {
            Session["ActivityAction"] = lblAction.Text;
            Session["ActivityStatus"] = lblStatus.Text;
            Session["Activity"] = ddlActivity.SelectedValue;
            //Session["ActivityTitle"] = txtTitle.Text;
            Session["ActivityNotes"] = txtNotes.Text;
           // Session["ActivityReason"] = txtReason.Text;
            List<AM_StandardLetters> Letters = new List<AM_StandardLetters>(); //LoadLetterList(lstLetters.Items);
            AM_StandardLetters sletter = null;
            ArrayList documents = new ArrayList();
            foreach (ListItem item in lstLetters.Items)
            {
                sletter = new AM_StandardLetters();
                if (item.Value != "-1")
                {
                    if (Validation.CheckIntegerValue(item.Value))
                    {
                        sletter.StandardLetterId = int.Parse(item.Value);
                        sletter.Title = item.Text;
                        Letters.Add(sletter);
                    }
                    else
                    {
                        documents.Add(item.Value);                        
                    }
                }
            }
            if (Letters.Count == 0)
            {
                Session["ActivityLetters"] = "No Data";
            }
            else
            {
                Session["ActivityLetters"] = Letters;
            }
            if (documents.Count > 0)
            {
                Session["ActivityDocumentsUploaded"] = documents;
            }

            SavedLetters = GetSavedLettersViewStateList();
            Session["ActivitySavedLettersList"] = SavedLetters;
        }

        #endregion

       #region"Populate Activity"
        private void PopulateActivity()
        {
            if (Session["ActivityStatus"] != null)
            {
                lblStatus.Text = Session["ActivityStatus"].ToString();
            }
            if (Session["ActivityAction"] != null)
            {
                 lblAction.Text = Session["ActivityAction"].ToString();
            }
            if(Session["Activity"] != null)
            {
                if(Session["Activity"].ToString()!="" && Session["Activity"].ToString()!="-1" )
                {
                    ddlActivity.SelectedValue = Session["Activity"].ToString();
                }
            }
            if(Session["ActivityTitle"]!=null)
            {
                 //txtTitle.Text = Session["ActivityTitle"].ToString();
            }
            if(Session["ActivityNotes"]!=null)
            {
                  txtNotes.Text = Session["ActivityNotes"].ToString();
            }
            //if (Session["ActivityReason"]!=null)
            //{
            //     txtReason.Text = Session["ActivityReason"].ToString();
            //}


     
        }
        #endregion

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion   

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        # region "Clear Form"
        private void clearForm()
        {
            txtNotes.Text = "";
            //txtReason.Text = "";
            //txtTitle.Text = "";
            ddlActivity.SelectedValue = ApplicationConstants.defaulvalue;
        
            lstLetters.Items.Clear();

        }
        #endregion

        #region "Clear Referral Form"
        private void ClearReferralForm()
        {
            txtNotes.Text = "";
            txtDateFollowup.Text = "";
            txtDateOfReferral.Text = "";
            ddlTeam.SelectedValue = ApplicationConstants.defaulvalue;
            ddlOrganisation.SelectedValue = ApplicationConstants.defaulvalue;
            txtEmail.Text = "";
            if (ddlName.Items.Count > 0)
            {
                ddlName.Items.Clear();
            }
            if(ddlContacts.Items.Count > 0)
            {
                ddlContacts.Items.Clear();
            }
            
            
        }
        #endregion

        #region"Populate Activity"
        void PopulateActivity(int activityId)
        {
            try
            {
                AddCaseActivity activityMgr = new AddCaseActivity();
                AM_Activity obj = activityMgr.GetActivityById(activityId);
                ddlActivity.SelectedValue = obj.ActivityLookupId.ToString();
                //txtTitle.Text = obj.Title;
                txtNotes.Text = obj.Notes;
               // txtReason.Text = obj.Reason;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        #endregion

        #region "is Valid Email"
        private bool isValidEmail(string email)
        {
            Regex expression = new Regex(RegularExpressionConstants.emailExp);
            return expression.IsMatch(email);
        }
        #endregion

        #region"Load Support Worker"
        private void LoadSupportWorker()
        {
          
            try
            {
                if (ddlTeam.SelectedValue != Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue)
                {
                    AddCaseActivity ca = new AddCaseActivity();

                    if (ddlName.Items.Count > 0)
                        ddlName.Items.Clear();
                    ddlName.DataSource = ca.GetSupportWorker(int.Parse(ddlTeam.SelectedValue));
                    ddlName.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.EMPLOYEENAME;
                    ddlName.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.EmployeId;
                    ddlName.DataBind();
                    ddlName.Items.Add(new ListItem(ApplicationConstants.SelectName, Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue));
                    ddlName.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                }
                else
                {
                    if (ddlName.Items.Count > 0)
                        ddlName.Items.Clear();
                    ddlName.Items.Add(new ListItem(ApplicationConstants.SelectName, Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue));
                }
            }

            catch (NullReferenceException nullref)
            {
                isError = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (KeyNotFoundException keynotfound)
            {
                isError = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (Exception ex)
            {
                isError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isError) setMessage(UserMessageConstants.ErrorLoadingNames, true);
            }

            
        }
        #endregion

        #region"GetCustomerId"
        private string GetCustomerId()
        {
            AddCaseActivity ca = new AddCaseActivity();
            return ca.GetCustomerId(base.GetTenantId()).ToString();

        }
        #endregion        

        #region"Get Saved Letters View State List"

        public ArrayList GetSavedLettersViewStateList()
        {
            if (!Convert.ToString(ViewState[ViewStateConstants.SavedLettersUpdateCase]).Equals(string.Empty))
            {
                return ViewState[ViewStateConstants.SavedLettersUpdateCase] as ArrayList;
            }
            else
                return null;
        }

        #endregion

        #region"Get Standard Letter History Id"

        public int GetStandardLetterHistoryId(int slId)
        {
            StandardLetterHistoryList = GetStandardLetterHistoryList();
            if (StandardLetterHistoryList != null)
            {
                for (int i = 0; i < StandardLetterHistoryList.Count; i++)
                {
                    string[] str = StandardLetterHistoryList[i].ToString().Split(';');
                    if (str.Count() == 2)
                    {
                        if (int.Parse(str[0]) == slId)
                            return int.Parse(str[1]);
                    }
                    else
                    {
                        return -1;
                    }
                }
                return -1;
            }
            else
            {
                return -1;
            }
        }

        #endregion

        #region"Set Standard Letter History List"

        public void SetStandardLetterHistoryList(string letter)
        {
            StandardLetterHistoryList = GetStandardLetterHistoryList();
            if (StandardLetterHistoryList == null)
            {
                StandardLetterHistoryList = new ArrayList();
            }
            if (!StandardLetterHistoryList.Contains(letter))
            {
                for (int i = 0; i < StandardLetterHistoryList.Count; i++)
                {
                    string[] str = StandardLetterHistoryList[i].ToString().Split(';');
                    string[] str2 = letter.Split(';');
                    if (str2[0].Equals(str[0]))
                    {
                        StandardLetterHistoryList.RemoveAt(i);
                    }
                }
                StandardLetterHistoryList.Add(letter);
                Session[SessionConstants.SavedLetterHistoryListOpenCase] = StandardLetterHistoryList;
            }
        }

        #endregion

        #region"Get Standard Letter History List"

        public ArrayList GetStandardLetterHistoryList()
        {
            StandardLetterHistoryList = Session[SessionConstants.SavedLetterHistoryListOpenCase] as ArrayList;
            return StandardLetterHistoryList;
        }

        #endregion

        #region"Change the Color of Letter"

        public void ChangeTheColorOfLetter(int id, string color, string letter, bool fromPageLoad)
        {
            if (fromPageLoad)
            {
                string[] strId = letter.Split(';');
                id = int.Parse(strId[0]);
            }
            for (int i = 0; i < lstLetters.Items.Count; i++)
            {
                if (Validation.CheckIntegerValue(lstLetters.Items[i].Value))
                {
                    string[] str = lstLetters.Items[i].Value.Split(';');
                    if (id == int.Parse(str[0]))
                    {
                        lstLetters.Items[i].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, color);
                        if (color == "Black")
                        {
                            if (fromPageLoad)
                            {
                                SetSavedLetters(id);
                                SetStandardLetterHistoryList(letter);
                            }
                            else
                            {
                                SetSavedLetters(id);
                            }

                        }
                    }
                }
            }
        }

        #endregion

        #region"Set Saved Letters"

        public void SetSavedLetters(int id)
        {
            //SavedLetters = new ArrayList();
            SavedLetters = GetSavedLettersViewStateList();
            if (SavedLetters == null)
            {
                SavedLetters = new ArrayList();
            }
            if (!SavedLetters.Contains(id))
            {
                SavedLetters.Add(id);
                SetSavedLettersViewStateList();
            }
            else
                return;
        }

        #endregion

        #region"Check Saved Letters"

        public void CheckSavedLetters()
        {
            SavedLetters = new ArrayList();
            SavedLetters = GetSavedLettersViewStateList();
            if (lstLetters.Items.Count > 0)
            {
                //for (int i = 0; i < SavedLetters.Count; i++)
                {
                    int i = 0;
                    foreach (ListItem item in lstLetters.Items)
                    {
                        if (Validation.CheckIntegerValue(item.Value))
                        {
                            string[] str = item.Value.Split(';');
                            if (SavedLetters != null)
                            {
                                if (i < SavedLetters.Count)
                                {
                                    if (SavedLetters.Contains(int.Parse(str[0])))//[i].ToString()) == int.Parse(str[0]))
                                    {
                                        ChangeTheColorOfLetter(int.Parse(str[0]), "Black", "", false);
                                        i++;
                                    }
                                    else
                                    {
                                        ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                                    }
                                }
                                else if (!SavedLetters.Contains(int.Parse(str[0])))
                                {
                                    ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                                }
                            }
                            else
                            {
                                ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region"Set Saved Letters View State List"

        public void SetSavedLettersViewStateList()
        {
            ViewState.Remove(ViewStateConstants.SavedLettersUpdateCase);
            ViewState[ViewStateConstants.SavedLettersUpdateCase] = SavedLetters;
        }

        #endregion

        #region"Remove Activity Sessions"

        public void RemoveActivitySessions()
        {
            Session.Remove("ActivityAction");
            Session.Remove("ActivityStatus");
            Session.Remove("Activity");
            Session.Remove("ActivityTitle");
            Session.Remove("ActivityNotes");
            Session.Remove("ActivityLetters");
            Session.Remove("ActivityDocumentsUploaded");
            Session.Remove("ActivitySavedLettersList");
            Session.Remove(SessionConstants.SavedLetterHistoryListOpenCase);
        }

        #endregion

    }
}