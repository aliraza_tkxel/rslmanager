﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Entities;
using Am.Ahv.Website.pagebase;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Utilities.utitility_classes;


namespace Am.Ahv.Website.controls.casemgt
{
    public partial class ViewCaseActivity : UserControlBase

    {
        #region"Attributes"
        bool isError;
        bool isException;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion

        #region"Events"

        #region"Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            resetMessage();
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["activityid"] != null)
                    {
                        if (Validation.CheckIntegerValue(Request.QueryString[ApplicationConstants.ActivityId]))
                        {
                            string activityId = Request.QueryString[ApplicationConstants.ActivityId];
                            if (activityId != string.Empty && activityId != null)
                            {
                                Session[SessionConstants.ViewCaseActivityId] = activityId;
                                
                                PopulateActivity(Convert.ToInt32(activityId));
                            }
                            else
                            {
                                setMessage(UserMessageConstants.ActivityLoadError, true);
                            }
                        }
                    }
                    else if (Session[SessionConstants.ViewCaseActivityId] != null)
                    {
                        PopulateActivity(Convert.ToInt32(Session[SessionConstants.ViewCaseActivityId]));
                    }
                }
                Session[SessionConstants.PaymentPlanBackPage] = PathConstants.viewActivitiesPath;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {

                if (isException)
                {
                    setMessage(UserMessageConstants.PageLoadError, true);
                }
            }
        }
        #endregion

        #region"btn back click"
        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.casehistory);
        }
        #endregion

        #endregion
        
        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Populate Activity"
        void PopulateActivity(int activityId)
        {
            try
            {
                AddCaseActivity activityMgr = new AddCaseActivity();
                AM_Activity obj = activityMgr.GetActivityById(activityId);
                lblActivityValue.Text = obj.AM_LookupCode.CodeName;
                //lblTitleValue.Text = obj.Title;
                lblNotesValue.Text = obj.Notes;
                lblReasonValue.Text = obj.Reason;
                showAcionAndStatus();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region "show Action and Status"

        private void showAcionAndStatus()
        {
            try
            {
                CaseDetails CDetails = new CaseDetails();
                AM_Case cdetails = CDetails.GetCaseById(base.GetCaseId());
                if (cdetails != null)
                {
                    this.lblActionValue.Text = cdetails.AM_Action.Title;
                    this.lblStatusValue.Text = cdetails.AM_Status.Title;
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                    setMessage(UserMessageConstants.ErrorActionStatus, true);
            }



        }
        #endregion
       
    }
}