﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddCaseActivity.ascx.cs" Inherits="Am.Ahv.Website.controls.casemgt.AddActivity" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<style type="text/css">
    #TextArea1
    {
        height: 104px;
    }
    #txtareaReason
    {
        height: 98px;
    }
    .style17
    {
        width: 5px;
    }
    .style19
    {
        width: 335px;
    }
    .style34
    {
        width: 10px;
    }
    .style35
    {
        width: 11px;
    }
    .style37
    {
        width: 9px;
    }
            
    .style49
    {
        width: 1px;
    }
        
    .style51
    {
        width: 118px;
    }
    .style57
    {
        width: 117px;
    }
        
    .style59
    {
        width: 120px;
    }
            
    .style60
    {
        width: 2px;
    }
    .localListBox
    {}
            
    </style>
 
<div>
    
    <asp:UpdatePanel ID = "updatePnlHeading" runat = "server" 
        UpdateMode="Conditional">
    
    <ContentTemplate>
     
    <table style="width:100%; border-color:Gray;" class="tableHeader" cellpadding="5px">
        <tr>
            <td>
           
                <asp:Label ID="lblActivityHeading" runat="server" Text="Add Activity"></asp:Label>
           
            </td>
            <td align="right" colspan="2">
                <asp:Button ID="btnBack" runat="server" Text="< Back To Case Summary" OnClick="btnBack_Click" />&nbsp;&nbsp;
                <asp:Button ID="btnBacktoactivities" runat="server" 
                    Text="&lt; Back to activities" onclick="btnBacktoactivities_Click"  />
            </td>
        </tr>
    </table>
    </ContentTemplate>
     </asp:UpdatePanel>
     
    
</div>

<div>
    <asp:UpdatePanel ID="panelAddactivity" runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
</div>
    
            <div>
            <asp:UpdatePanel ID = "AddActivityPanel" runat = "server">
            <ContentTemplate>
            <div>
            

                 <asp:UpdatePanel ID="ActivityUpdatePanel1" runat="server" UpdateMode = "Conditional">
                            <ContentTemplate>
                                <table style="width:100%;">
                                    <tr>
                                        <td class="style51" valign="top" width="120px">
                                            <asp:Label ID="lblStatusLabel" runat="server" Text="Stage:"></asp:Label>
                                        </td>
                                        <td class="style35" align="center" valign="middle">
                                            &nbsp;</td>
                                        <td valign="top">
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style51" valign="top">
                                            <asp:Label ID="lblActionLabel" runat="server" Text="Action:"></asp:Label>
                                        </td>
                                        <td class="style35" align="center" valign="middle">
                                            &nbsp;</td>
                                        <td valign="top">
                                            <asp:Label ID="lblAction" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style51" valign="top">
                                            <asp:Label ID="lblActivity" runat="server" Text="Activity: "></asp:Label>
                                        </td>
                                        <td class="style35" align="center" valign="middle">
                                            <asp:Label ID="lblTypeDDL11" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td valign="top">
                                            <asp:DropDownList ID="ddlActivity" runat="server" 
                                                onselectedindexchanged="ddlActivity_SelectedIndexChanged" Width="175px" 
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>            
            </div>

            <div>
                <%--<asp:UpdatePanel ID="ActivityUpdatePanel2" runat="server" 
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <table style="width:100%;">
                                    <tr>
                                        <td class="style51" valign="top">
                                            <asp:Label ID="lblTitle" runat="server" Text="Title:"></asp:Label>
                                        </td>
                                        <td class="style34" align="center" valign="middle">
                                            <asp:Label ID="lblTypeDDL12" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="txtTitle" runat="server" Width="173px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
            
            </div>

            <div>
                <asp:UpdatePanel ID="UpdatePanelRefer1" runat="server" UpdateMode="Conditional" 
                            Visible="False">
                            <ContentTemplate>
                                <table style="width:100%;">
                                    <tr>
                                        <td valign="top" class="style51">
                                            <asp:Label ID="lblReferralDate" runat="server" Text="Date of Referral:"></asp:Label>
                                        </td>
                                        <td valign="middle" class="style49" align="left">
                                            <asp:Label ID="lblTypeDDL13" runat="server" Font-Bold="True" ForeColor="Red" 
                                                Text="*" Width="1px"></asp:Label>
                                        </td>
                                        <td  valign="top">
                                            <asp:TextBox ID="txtDateOfReferral" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtDateOfReferral0_CalendarExtender" runat="server" 
                                                Animated="false" EnabledOnClient="true" Format="dd/MM/yyyy" 
                                                PopupButtonID="btnImageCalendar" TargetControlID="txtDateOfReferral">
                                            </cc1:CalendarExtender>
                                            &nbsp;<asp:ImageButton ID="btnImageCalendar" runat="server" 
                                                ImageUrl="~/style/images/Calendar-icon.png" />
                                            <asp:Label ID="lblReferralHelp" runat="server" Text="Date format: DD/MM/YYYY"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="style51" >
                                            <asp:Label ID="lblFollowup" runat="server" Text="Follow up:"></asp:Label>
                                        </td>
                                        <td class="style49" valign="middle" dir="ltr" align="left">
                                            <asp:Label ID="lblTypeDDL14" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="txtDateFollowup" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtDateFollowup0_CalendarExtender" runat="server" 
                                                Animated="false" EnabledOnClient="true" Format="dd/MM/yyyy" 
                                                PopupButtonID="btnImageCalendarFollowup" TargetControlID="txtDateFollowup">
                                            </cc1:CalendarExtender>
                                            &nbsp;<asp:ImageButton ID="btnImageCalendarFollowup" runat="server" 
                                                ImageUrl="~/style/images/Calendar-icon.png" />
                                            <asp:Label ID="lblFollowupDateHelp" runat="server" 
                                                Text="Date format: DD/MM/YYYY"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="style51" >
                                            <asp:Label ID="lblReferralType" runat="server">Referral Type:</asp:Label>
                                        </td>
                                        <td align="left" valign="middle">
                                            <asp:Label ID="lblTypeDDL15" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="RadioButtonListReferralType" runat="server" 
                                                AutoPostBack="True" 
                                                onselectedindexchanged="RadioButtonListReferralType_SelectedIndexChanged" 
                                                RepeatDirection="Horizontal">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        
                
                </div>

                <div>

                    <asp:UpdatePanel ID = "AddActivityPanel1" runat = "server" UpdateMode = "Conditional" Visible = "false">
                    
                        <ContentTemplate>
                            <table style = "width:100%">
                                    <tr>
                                        <td valign="top" class="style51">
                                            <asp:Label ID="lblTeam" runat="server">Team:</asp:Label>
                                        </td>
                            
                                        <td class="style34" align="center" valign="middle">
                                            <asp:Label ID="lblTypeDDL16" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlTeam" runat="server" Width="175px" AutoPostBack="True" 
                                                onselectedindexchanged="ddlTeam_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="style51">
                                            <asp:Label ID="lblName" runat="server">Name:</asp:Label>
                                        </td>
                                        <td class="style34" align="center" valign="middle">
                                            <asp:Label ID="lblTypeDDL17" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlName" runat="server" Width="175px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                            
                            </table>
                   
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

                <div>
                
                <asp:UpdatePanel ID = "AddActivityPanel2" runat = "server" UpdateMode = "Conditional" Visible = "false">
                    <ContentTemplate>
                        <table>
                        
                            <tr>
                                        <td valign="top" class="style59">
                                            <asp:Label ID="lblOrganization" runat="server" Text = "Organisation:"></asp:Label>
                                        </td>
                                        <td class="style49" align="center" valign="middle">
                                            <asp:Label ID="lblOrganisationRequired" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td class="style19">
                                            <asp:DropDownList ID="ddlOrganisation" runat="server" Width="175px" 
                                                AutoPostBack="True" 
                                                onselectedindexchanged="ddlOrganisation_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="style59">
                                            <asp:Label ID="lblContacts" runat="server" Text = "Contacts:"></asp:Label>
                                        </td>
                                        <td class="style49" align="center" valign="middle">
                                            <asp:Label ID="lblContactsRequired" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td class="style19">
                                            <asp:DropDownList ID="ddlContacts" runat="server" Width="175px" 
                                                AutoPostBack="True" onselectedindexchanged="ddlContacts_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" class="style59">
                                            <asp:Label ID="lblEmail" runat="server" Text = "Email:"></asp:Label>
                                        </td>
                                        <td valign = "middle" align="center">
                                            <asp:Label ID="lblEmailRequired" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td valign = "top">
                                            <asp:TextBox ID = "txtEmail" runat = "server"></asp:TextBox>
                                        </td>    
                                    
                                    </tr>
                            
                        </table>
                    
                    </ContentTemplate>
                
                </asp:UpdatePanel>

                </div>


                
                <div>        
                <asp:UpdatePanel ID="ActivityUpdatePanel3" runat="server" UpdateMode = "Conditional">
                            <ContentTemplate>
                                <table style="width:100%;">
                                    <tr>
                                        <td class="style51" valign="top">
                                            <asp:Label ID="lblNotes" runat="server" Text="Notes:"></asp:Label>
                                        </td>
                                        <td class="style17" valign="top" align="center">
                                            <asp:Label ID="lblTypeDDL10" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" 
                                                 Height="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    
                   <div>
                    <asp:UpdatePanel ID="UpdatePanelRefer2" runat="server" UpdateMode="Conditional" 
                            Visible="False">
                            <ContentTemplate>
                                <table style="width:100%;">
                                    <tr>
                                        <td class="style57">
                                            <asp:Label ID="lblCustomerAware" runat="server" Text="Customer Aware:"></asp:Label>
                                        </td>
                                        <td class="style37" valign="middle" align="center">
                                            <asp:Label ID="lblTypeDDL19" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="checkboxCustomerAware" runat="server" Checked="True" 
                                                Enabled="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style57">
                                            &nbsp;</td>
                                        <td class="style37" align="center" valign="middle">
                                            &nbsp;</td>
                                        <td align="right">
                                            <asp:Button ID="btnSaveReferral" runat="server" Text="Save Referral" 
                                                onclick="btnSaveReferral_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
            </div>

            <div>
                <asp:UpdatePanel ID="ActivityUpdatePanel4" runat="server" 
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <table style="width:100%;">
                                    <tr>
                                        <td class="style57" valign="top">
                                            <asp:Label ID="lblLetter" runat="server">Letter</asp:Label>
                                        </td>
                                        <td class="style60" align="center" valign="middle">
                                            &nbsp;</td>
                                        <td>
                                            <asp:Button ID="btnStandardLetter" runat="server" 
                                                onclick="btnStandardLetter_Click" Text="Standard Letter" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class = "style57" valign="top">
                                            &nbsp;</td>
                                        <td class="style60" align="center" valign="middle">
                                            &nbsp;</td>
                                        <td>
                                            <asp:Label ID="lblOR" runat="server">OR</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style57" valign="top">
                                            &nbsp;</td>
                                        <td align="center" class="style60" valign="middle">
                                        </td>
                                        <td>
                                            <asp:FileUpload ID="txtActivityFile" runat="server" />
                                            <br />
                                            <asp:Label ID="lbl" runat="server" Text="(Max file size 2MB)" Font-Italic="true"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style57" valign="top">
                                            <asp:Label ID="lblDocument0" runat="server" Text="Documents:"></asp:Label>
                                        </td>
                                        <td class="style60" align="center" valign="middle">
                                            <asp:Label ID="lblTypeDDL20" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*" Visible="false"></asp:Label>
                                        </td>
                                        <td>
                                             <asp:ListBox ID="lstLetters" runat="server" CssClass="localListBox" 
                                                 Width="165px"></asp:ListBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style57" valign="top">
                                        </td>
                                        <td class="style60" align="center" valign="middle">
                                        </td>
                                        <td>
                                             <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick = "btnAdd_Click"/>
                                             &nbsp;<asp:Button ID="btnRemove" runat="server" Text="Remove" 
                                                 onclick="btnRemove_Click" />&nbsp;
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                                             </td>
                                    </tr>
                                    <%--<tr>
                                        <td class="style57" valign="top">
                                            <asp:Label ID="lblReason0" runat="server" Text="Reason: "></asp:Label>
                                        </td>
                                        <td class="style60" valign="middle" align="center">
                                            <asp:Label ID="lblTypeDDL22" runat="server" Font-Bold="true" ForeColor="Red" 
                                                Text="*"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtReason" runat="server" Rows="5" TextMode="MultiLine" 
                                                ontextchanged="txtReason_TextChanged"></asp:TextBox>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td class="style57">
                                            &nbsp;</td>
                                        <td class="style60">
                                            &nbsp;</td>
                                        <td align="right">
                                            <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                                                Text="Cancel" Width="66px" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnSaveActivity" runat="server" OnClick="btnSaveActivity_Click" 
                                                Text="Save Activity" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>
        <asp:PostBackTrigger ControlID = "btnSaveActivity" />
        <asp:PostBackTrigger ControlID = "btnAdd" />
        </Triggers>
        
                        </asp:UpdatePanel>
            
            </div>
            

            </ContentTemplate>
            
            </asp:UpdatePanel>

            </div>