﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Entities;
using System.Web.Services;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Website.masterpages;
using Am.Ahv.Utilities.constants;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Website.pagebase;
namespace Am.Ahv.Website.controls.casemgt
{

    #region"Delegate"

    public delegate void updateUserPanel(bool flag);

    #endregion

    public partial class AddCaseWorker : UserControlBase
    {
        #region"Attributes"

        Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker cw = null;
        List<ListItem> regionlist = null;
        List<ListItem> suburblist = null;
        List<ListItem> streetlist = null;
        bool flag = false;
        bool isException = false;
        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        #endregion

        #region "Events"

        #region"Delegate Event"

        public event updateUserPanel updateEvent;

        #endregion

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            resetMessage();
            string query = "id";

            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString[query] != null && !Convert.ToString(Request.QueryString[query]).Equals(string.Empty) && Validation.CheckIntegerValue(Request.QueryString[query].ToString()))
                    {
                        
                    }                    
                    else
                    {
                        initlookups();
                        ClearSessionObjects();
                        resetPage();

                        regionlist = new List<ListItem>();
                        suburblist = new List<ListItem>();
                        streetlist = new List<ListItem>();
                        
                        Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] = regionlist;
                        Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] = suburblist;
                        Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] = streetlist;
                    }

                    
                }                

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACPageload, true);
                }
            }
        }

        #endregion

        #region"Cancel Btn Click"

        protected void cancelbtn_Click(object sender, EventArgs e)
        {
            resetPage();
            EnableControls();
            Label lblflag = this.Parent.FindControl("lblflagvalue") as Label;
            lblflag.Text = "false";
        }

        #endregion

        #region Region Drop down selected Index Change Event

        protected void regionddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateSuburbs(); 
            }
            catch (NullReferenceException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACSuburbddl, true);
                }
            }
        }

        #endregion

        #region Suburb Drop down selected Index Change Event        
        protected void suburbddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PopulateStreets();
            }
            catch (NullReferenceException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACStreetddl, true);
                }
            }
        }
        #endregion

        #region Region Add Button Click
        protected void raddbtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool i = isException;
                List<ListItem> rlist = Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] as List<ListItem>;
                
                if (regionddl.SelectedValue == Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue)
                {
                    setMessage(UserMessageConstants.selectRegion, true);
                    return;
                }
                if (rlist != null && !IsPresent(rlist, regionddl.SelectedItem))
                {
                    rlist.Add(regionddl.SelectedItem);
                    List<ListItem> newrlist = new List<ListItem>();
                    foreach (ListItem item in rlist)
                    {
                        if (item != null)
                        {
                            newrlist.Add(item);
                        }
                    }
                    regionlbox.DataSource = newrlist;
                    this.regionlbox.DataBind();
                    Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] = newrlist;
                    bool ii = isException;
                }
                else
                    if (rlist == null)
                    {
                        rlist = new List<ListItem>();

                        rlist.Add(regionddl.SelectedItem);
                        regionlbox.DataSource = rlist;
                        this.regionlbox.DataBind();
                      Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] = rlist;
                       
                    }
                    else
                    {
                        setMessage(UserMessageConstants.regionSuburbAlreadyAddedCaseWorker, true);
                    }
            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACRegionAddbtn, true);
                }
            }
        }
        #endregion

        #region Suburb Add Button Click

        protected void saddbtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<ListItem> slist = Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] as List<ListItem>;
                if (suburbddl.SelectedValue == "-1")
                {
                    setMessage(UserMessageConstants.selectSuburb, true);
                    return;
                }
                if (slist != null && !IsPresent(slist, suburbddl.SelectedItem))
                {
                    slist.Add(suburbddl.SelectedItem);
                    suburbboxl.Items.Clear(); 
                    suburbboxl.DataSource = slist;
                    suburbboxl.DataTextField = "";
                    suburbboxl.DataValueField = "";
                    suburbboxl.DataBind();
                    Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] = slist;
                }
                else
                    if (slist == null)
                    {
                        slist = new List<ListItem>();
                        slist.Add(suburbddl.SelectedItem);
                        suburbboxl.Items.Clear();
                        suburbboxl.DataSource = slist;
                        suburbboxl.DataTextField = "";
                        suburbboxl.DataValueField = "";
                        suburbboxl.DataBind();
                        Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] = slist;
                    }
                    else
                    {
                        setMessage(UserMessageConstants.regionSuburbAlreadyAddedCaseWorker, true);
                    }
            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACSuburbAddbtn, true);
                }
            }
        }
        #endregion

        #region Street Add Button Click

        protected void streetAddBtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<ListItem> slist = Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] as List<ListItem>;
                if (streetddl.SelectedValue == "-1")
                {
                    setMessage(UserMessageConstants.selectStreet, true);
                    return;
                }
                if (slist != null && !IsPresent(slist, streetddl.SelectedItem))
                {
                    slist.Add(streetddl.SelectedItem);
                    streetbox.DataSource = slist;
                    streetbox.DataBind();
                    Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] = slist;
                }
                else
                    if (slist == null)
                    {
                        slist = new List<ListItem>();
                        slist.Add(streetddl.SelectedItem);
                        streetbox.DataSource = slist;
                        streetbox.DataBind();
                        Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] = slist;
                    }
                    else
                    {
                        setMessage(UserMessageConstants.regionSuburbAlreadyAddedCaseWorker, true);
                    }
            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACStreetAddbtn, true);
                }
            }
        }
        #endregion

        #region suburb Remove Button Click
        protected void sremovebtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<ListItem> slist = Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] as List<ListItem>;
                if (slist != null && !Convert.ToString(Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList]).Equals(string.Empty))
                {
                    if (!Convert.ToString(suburbboxl.SelectedItem).Equals(string.Empty))
                    {
                        slist.RemoveAt(suburbboxl.SelectedIndex);
                    }
                    else
                    {
                        int count = slist.Count;
                        if (count > 0)
                        {
                            slist.RemoveAt(count - 1);
                        }
                        else
                        {
                            setMessage("Please add an Item", true);
                        }
                    }
                    Session.Remove(SessionConstants.SuburbList);
                    Session[SessionConstants.SuburbList] = slist;
                    suburbboxl.DataSource = slist;
                    suburbboxl.DataBind();
                }
            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACSuburbRemovebtn, true);
                }
            }
        }
        #endregion

        #region street Remove Button Click
        protected void streetRemoveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<ListItem> slist = Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] as List<ListItem>;
                if (slist != null && !Convert.ToString(Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList]).Equals(string.Empty))
                {
                    if (!Convert.ToString(streetbox.SelectedItem).Equals(string.Empty))
                    {
                        slist.RemoveAt(streetbox.SelectedIndex);
                    }
                    else
                    {
                        int count = slist.Count;
                        if (count > 0)
                        {
                            slist.RemoveAt(count - 1);
                        }
                        else
                        {
                            setMessage("Please add an Item", true);
                        }
                    }
                    Session.Remove(SessionConstants.StreetList);
                    Session[SessionConstants.StreetList] = slist;
                    streetbox.DataSource = slist;
                    streetbox.DataBind();
                }
            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACStreetRemovebtn, true);
                }
            }
        }
        #endregion

        #region Region Remove Button Click
        protected void rremovebtn_Click(object sender, EventArgs e)
        {
            try
            {
                List<ListItem> _rlist = Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] as List<ListItem>;
                List<ListItem> rlist = new List<ListItem>();
                foreach (ListItem listitem in _rlist)
                {
                    if (listitem != null)
                    {
                        rlist.Add(listitem);
                    }
                }
                if (rlist != null && !Convert.ToString(Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList]).Equals(string.Empty))
                {
                    if (!Convert.ToString(regionlbox.SelectedItem).Equals(string.Empty))
                    {
                        rlist.RemoveAt(regionlbox.SelectedIndex);
                    }
                    else
                    {
                        int count = rlist.Count;
                        if (count > 0)
                        {
                            rlist.RemoveAt(count - 1);
                        }
                        else
                        {
                            setMessage("Please add an Item", true);
                        }

                    }
                    Session.Remove(SessionConstants.RegionList);

                    Session[SessionConstants.RegionList] = rlist;
                    regionlbox.DataSource = rlist;
                    regionlbox.DataBind();
                }
            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACRegionRemovebtn, true);
                }
            }

        }
        #endregion

        #region Find Button Click Event
        protected void findbtn_Click(object sender, EventArgs e)
        {
            try
            {
                E__EMPLOYEE Employee = null;
                if (teamddl.SelectedValue == Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue)
                {
                    cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
                    if (quickfindtxt.Text != string.Empty)
                    {
                        Employee = cw.GetExactName(quickfindtxt.Text);
                        if (Employee != null)
                        {
                            quickfindtxt.Text = Employee.FIRSTNAME + " " + Employee.LASTNAME;
                            Session[Am.Ahv.Utilities.constants.SessionConstants.Employee] = Employee;
                            E_TEAM team = cw.GetTeamByEmployeeId(Employee.EMPLOYEEID);
                            if (team != null)
                            {
                                teamddl.SelectedValue = team.TEAMID.ToString();
                                teamddl.Enabled = false;
                                PopulateTeamMembers(team.TEAMID);
                                teammemberddl.SelectedValue = Employee.EMPLOYEEID.ToString();
                                teammemberddl.Enabled = false;
                            }
                            teamddl.Enabled = false;
                            teammemberddl.Enabled = false;
                        }
                        else
                        {
                            setMessage("No Employee with " + quickfindtxt.Text + " Name Found", true);
                        }
                    }
                    else
                    {
                        setMessage("Please enter Employee Name", true);
                    }
                }
                else
                {
                    cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
                    if (quickfindtxt.Text != string.Empty)
                    {
                        Employee = cw.GetNarrowSearched(quickfindtxt.Text, int.Parse(teamddl.SelectedValue));
                        if (Employee != null)
                        {
                            quickfindtxt.Text = Employee.FIRSTNAME + " " + Employee.LASTNAME;
                            Session[Am.Ahv.Utilities.constants.SessionConstants.Employee] = Employee;
                        }
                        else
                        {
                            setMessage("No Employee with " + quickfindtxt.Text + " Name found", true);
                        }
                    }
                    else
                    {
                        setMessage("Please enter Employee name", true);
                    }
                }
            }
            catch (ObjectNotFoundException objnotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(objnotfound, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACFindbtn, true);
                }
            }

        }
        #endregion

        #region Team Drop Down Selected Index change Event
        /// <summary>
        /// Search Narrow Down Criterias
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void teamddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (teamddl.SelectedValue != Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue)
                {
                    quickfindtxt.Enabled = false;
                    findbtn.Enabled = false;
                    PopulateTeamMembers(int.Parse(teamddl.SelectedValue));
                }
                else
                {
                    teammemberddl.Items.Add(new ListItem("Select Member", Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue));
                }
            }

            catch (NullReferenceException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACTeamddl, true);
                }
            }

        }
        #endregion

        #region Save Button Click Event
        protected void savebtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool flag = AddResource();
                if (flag)
                {
                    ClearSessionObjects();
                    if (Session["ResourceCW"] != null && !Convert.ToString(Session["ResourceCW"]).Equals(string.Empty))
                    {
                        resetPage();
                        EnableControls();

                        setMessage(UserMessageConstants.resourceUpdated, false);
                        updateEvent(true);
                    }
                    else
                    {
                        resetPage();
                        setMessage(UserMessageConstants.resourceAdded, false);
                        updateEvent(true);
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACSavebtn, true);
                }
            }
        }
        //enable the disabled controls in the update.
        #region Enable Controls
        private void EnableControls()
        {
            quickfindtxt.Enabled = true;
            typeddl.Enabled = true;
            teamddl.Enabled = true;

        }
        #endregion
        #endregion

        #endregion

        #region Utility Methods

        #region"Init Lookups"

        public void initlookups()
        {
            try
            {
                cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
                teammemberddl.Items.Add(new ListItem("Select Member", ApplicationConstants.defaulvalue));
                //Binding Region Dropdowns
                regionddl.DataSource = cw.GetAllRegion();
                regionddl.DataValueField = ApplicationConstants.regionId;
                regionddl.DataTextField = ApplicationConstants.regionname;
                regionddl.DataBind();
                regionddl.Items.Add(new ListItem("Select Patch", ApplicationConstants.defaulvalue));
                regionddl.SelectedValue = ApplicationConstants.defaulvalue;

                PopulateSuburbs();
                PopulateStreets();
                //streetddl.DataSource = cw.GetAllStreets();               
                //streetddl.DataBind();
                //streetddl.Items.Add(new ListItem("Select Street", ApplicationConstants.defaulvalue));
                //streetddl.SelectedValue = ApplicationConstants.defaulvalue;
                
                typeddl.DataSource = cw.GetUserTypes();
                typeddl.DataTextField = ApplicationConstants.codename;
                typeddl.DataValueField = ApplicationConstants.codeId;
                typeddl.DataBind();
                typeddl.Items.Add(new ListItem("Select case worker type", ApplicationConstants.defaulvalue));
                typeddl.SelectedValue = ApplicationConstants.defaulvalue;
                
                teamddl.DataSource = cw.GetAllTeams();
                teamddl.DataTextField = ApplicationConstants.teamname;
                teamddl.DataValueField = ApplicationConstants.teamId;
                teamddl.DataBind();
                teamddl.Items.Add(new ListItem("Select Team", ApplicationConstants.defaulvalue));
                teamddl.SelectedValue = ApplicationConstants.defaulvalue;
            }
            catch (KeyNotFoundException keynotfound)
            {
                isError = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isError = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isError)
                {
                    setMessage(UserMessageConstants.ACinitlookup, true);
                }
            }
        }

        #endregion

        #region IsPresent
        private bool IsPresent(List<ListItem> itemlist, ListItem item)
        {
            try
            {
                for (int i = 0; i < itemlist.Count; i++)
                {
                    if (itemlist[i] != null)
                    {
                        if (itemlist[i].Value == item.Value)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (IndexOutOfRangeException indexout)
            {
                isException = true;
                ExceptionPolicy.HandleException(indexout, "Exception Policy");
                return false;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return false;
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACIspresent, true);
                }
            }
        }
        #endregion

        #region clearing session objects
        private void ClearSessionObjects()
        {
            try
            {
                Session.Remove(SessionConstants.RegionList);
                Session.Remove(SessionConstants.SuburbList);
                Session.Remove(SessionConstants.StreetList);
                Session.Remove(Am.Ahv.Utilities.constants.SessionConstants.Employee);                
            }
            catch (NullReferenceException nullrefer)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullrefer, "Exception Policy");

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACclearsession, true);
                }
            }
        }
        #endregion

        #region converting listtypes
        private List<int> GetRegionList(List<ListItem> ItemList)
        {
            try
            {
                List<int> RegionList = new List<int>();
                for (int i = 0; i < ItemList.Count; i++)
                {
                    RegionList.Add(int.Parse(ItemList[i].Value));
                }
                return RegionList;
            }
            catch (NullReferenceException nullrefer)
            {
                ExceptionPolicy.HandleException(nullrefer, "Exception Policy");
                return null;
            }
            catch (IndexOutOfRangeException argumentex)
            {
                ExceptionPolicy.HandleException(argumentex, "Exception Policy");
                return null;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return null;
            }
        }
        #endregion

        #region Adding Resource
        private bool AddResource()
        {
            try
            {
                DataTable dt = new DataTable();
                bool regionEmptyFlag = false;
                bool suburbEmptyFlag = false;
                if (Session["ResourceCW"] == null && Convert.ToString(Session["ResourceCW"]).Equals(string.Empty))
                {
                    AM_Resource Resource = new AM_Resource();
                    cw = new BusinessManager.Casemgmt.AddCaseWorker();

                    if (typeddl.SelectedValue == Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue)
                    {
                        // Start - Changes By Aamir Waheed May 21,2013
                        // setMessage(UserMessageConstants.requiredDropDownMessage, true);
                        setMessage("Please select the \"type\"", true);
                        // End - Changes By Aamir Waheed May 21,2013
                        return false;
                    }
                    //if (regionddl.SelectedValue == Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue && regionlbox.Items.Count == 0)
                    //{
                    //    setMessage(UserMessageConstants.requiredDropDownMessage, true);
                    //    return false;
                    //}
                    if (quickfindtxt.Text == string.Empty && Session[Am.Ahv.Utilities.constants.SessionConstants.Employee] == null)
                    {
                        if (teamddl.SelectedValue == Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue)
                        {
                            setMessage(UserMessageConstants.requiredDropDownMessage, true);
                            return false;
                        }
                        if (teammemberddl.SelectedValue == Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue)
                        {
                            setMessage(UserMessageConstants.requiredDropDownMessage, true);
                            return false;
                        }
                        Resource.EmployeeId = (int.Parse(teammemberddl.SelectedValue));
                    }
                    else
                    {
                        E__EMPLOYEE Emp = Session[Am.Ahv.Utilities.constants.SessionConstants.Employee] as E__EMPLOYEE;
                        if (Emp != null)
                        {
                            Resource.EmployeeId = Emp.EMPLOYEEID;
                        }
                        else
                        {
                            // Start - Changes By Aamir Waheed May 21,2013
                            
                            E__EMPLOYEE Employee = null;                       

                            if (quickfindtxt.Text != string.Empty)
                            {
                                Employee = cw.GetExactName(quickfindtxt.Text);
                                if (Employee != null)
                                {
                                    quickfindtxt.Text = Employee.FIRSTNAME + " " + Employee.LASTNAME;
                                    //Session[Am.Ahv.Utilities.constants.SessionConstants.Employee] = Employee;
                                    E_TEAM team = cw.GetTeamByEmployeeId(Employee.EMPLOYEEID);
                                    if (team != null)
                                    {
                                        teamddl.SelectedValue = team.TEAMID.ToString();
                                        teamddl.Enabled = false;
                                        PopulateTeamMembers(team.TEAMID);
                                        teammemberddl.SelectedValue = Employee.EMPLOYEEID.ToString();
                                        teammemberddl.Enabled = false;
                                    }
                                    teamddl.Enabled = false;
                                    teammemberddl.Enabled = false;

                                    Resource.EmployeeId = Employee.EMPLOYEEID;
                                }
                                else
                                {
                                    setMessage("No Employee with " + quickfindtxt.Text + " Name Found", true);
                                    return false;
                                }
                            }

                            // setMessage("No Employee Selected", false);
                            // End - Changes By Aamir Waheed May 21,2013
                            

                        }
                    }
                    Resource.CreatedBy = base.GetCurrentLoggedinUser();
                    Resource.CreatedDate = DateTime.Now;
                    Resource.ModifiedDate = DateTime.Now;
                    Resource.IsActive = true;
                    Resource.LookupCodeId = int.Parse(typeddl.SelectedValue);
                    AM_Resource Dummyresource = null;
                    List<ListItem> RegionList = Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] as List<ListItem>;
                    List<ListItem> SuburbList = Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] as List<ListItem>;
                    List<ListItem> StreetList = Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] as List<ListItem>;
                    
                    {
                        if (regionEmptyFlag == false || suburbEmptyFlag == false)
                        {
                            dt = GetRegionSuburbDataTable(RegionList, SuburbList, StreetList);
                        }
                    }
                    if (!cw.IsResourceExist(Resource.EmployeeId))
                    {
                        Dummyresource = cw.AddJunction(Resource, Convert.ToInt32(teammemberddl.SelectedValue), dt, true);
                        if (Dummyresource != null)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        setMessage(UserMessageConstants.ACWresourceexist, true);
                        return false;
                    }
                   // }
                    //else
                    //{
                    //    setMessage(UserMessageConstants.ACWaddregion, true);
                    //    return false;
                    //}
                }
                else
                {
                    AM_Resource Resource = Session["ResourceCW"] as AM_Resource;
                    Resource.ModifiedDate = DateTime.Now;
                    Resource.ModifiedBy = base.GetCurrentLoggedinUser();
                    AM_Resource Dummyresource = new AM_Resource();
                    List<ListItem> RegionList = Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] as List<ListItem>;
                    List<ListItem> SuburbList = Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] as List<ListItem>;
                    List<ListItem> StreetList = Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] as List<ListItem>;

                    dt = GetRegionSuburbDataTable(RegionList, SuburbList, StreetList);
                    //if (SuburbList != null)
                    //{
                    //    if (SuburbList.Count != 0)
                    //    {
                    //        dt = GetRegionSuburbDataTable(RegionList, SuburbList);
                    //    }
                    //}
                    //else if (RegionList != null)
                    //{
                    //    if (RegionList.Count != 0)
                    //    {
                    //        dt = GetRegionSuburbDataTable(RegionList, SuburbList);
                    //    }
                    //}
                    cw = new BusinessManager.Casemgmt.AddCaseWorker();
                    
                    Dummyresource = cw.AddJunction(Resource, -1, dt, false);
                    if (Dummyresource != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                   // }
                    //else
                    //{
                    //    setMessage(UserMessageConstants.ACWaddregion, true);
                    //    return false;
                    //}
                }
            }
            catch (IndexOutOfRangeException indexout)
            {
                isException = true;
                ExceptionPolicy.HandleException(indexout, "Exception Policy");
                return false;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return false;
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACaddresource, true);
                }
            }

        }
        #endregion

        #region Populating the Resource
        public void PopulateResrouce(int ResourceID)
        {
            try
            {
                Session[Am.Ahv.Utilities.constants.SessionConstants.EditResourceId] = ResourceID;
                cw = new BusinessManager.Casemgmt.AddCaseWorker();
                AM_Resource Resource = cw.GetResourceById(ResourceID);
                if (Resource != null)
                {
                    Session["ResourceCW"] = Resource;
                    string EmployeeName = cw.GetEmployeeNameAndTeam(Resource.EmployeeId);
                    if (EmployeeName != string.Empty && EmployeeName != null)
                    {
                        string[] str = EmployeeName.Split(';');
                        if (str.Count() > 0)
                        {
                            quickfindtxt.Text = str[0];
                            teamddl.SelectedValue = str[1];
                            PopulateTeamMembers(int.Parse(str[1]));
                            teammemberddl.SelectedValue = Resource.EmployeeId.ToString();
                        }
                    }
                    typeddl.SelectedValue = Resource.LookupCodeId.ToString();
                    quickfindtxt.Enabled = false;
                    teamddl.Enabled = false;
                    typeddl.Enabled = false;
                    teamddl.Enabled = false;
                    teammemberddl.Enabled = false;
                    //Get the region List
                    List<AM_ResourcePatchDevelopment> AreasList = cw.GetAreas(ResourceID);
                    if (AreasList != null)
                    {
                        FillSessionLists(AreasList);
                    }
                    regionlist = Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] as List<ListItem>;
                    if (regionlist != null && regionlist.Count > 0)
                    {
                        List<ListItem> newRegionList=new List<ListItem>();
                        foreach (ListItem region in regionlist)
                        {
                            if (region != null)
                            {
                                newRegionList.Add(region);
                            }
                        }
                        regionlbox.DataSource = newRegionList;
                        regionlbox.DataBind();
                    }
                    suburblist = Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] as List<ListItem>;
                    if (suburblist != null && suburblist.Count > 0)
                    {
                        suburbboxl.DataSource = suburblist;
                        suburbboxl.DataBind();
                    }

                    streetlist = Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] as List<ListItem>;
                    if (streetlist != null && streetlist.Count > 0)
                    {
                        streetbox.DataSource = streetlist;
                        streetbox.DataBind();
                    }

                }
            }
            catch (NullReferenceException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage("Error Populating Resource", true);
                }
            }
        }
        #endregion

        #region Filling the SessionLists for Suburbs & Regions
        private void FillSessionLists(List<AM_ResourcePatchDevelopment> AreasList)
        {
            try
            {
                

                cw = new BusinessManager.Casemgmt.AddCaseWorker();
                List<P_SCHEME> SuburbList = cw.GetSuburbs(AreasList);
                List<ListItem> RList = new List<ListItem>();
                List<ListItem> SList = new List<ListItem>();
                List<ListItem> StList = new List<ListItem>();
                List<String> tempStList = new List<String>();
                //Put try catch block
                if (AreasList != null)
                {
                    ListItem litem = null;
                    foreach (AM_ResourcePatchDevelopment item in AreasList)
                    {
                        litem = new ListItem();
                        litem = regionddl.Items.FindByValue(item.PatchId.ToString());
                        if (!RList.Contains(litem))
                        {
                            RList.Add(litem);
                        }

                        tempStList = cw.GetAllStreets();
                        litem = null;
                        if (item.Address != null && tempStList.Contains(item.Address.ToString()))
                        {
                            litem = new ListItem(item.Address.ToString());
                            StList.Add(litem);
                        }
                    }

                    Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] = RList;
                    Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] = StList;

                    if (SuburbList != null)
                    {
                        foreach (P_SCHEME item in SuburbList)
                        {
                            litem = new ListItem();
                            //litem.Value = item.DEVELOPMENTID.ToString() + ";" + item.PATCHID.ToString();
                            litem.Value = item.SCHEMEID.ToString();
                            litem.Text = item.SCHEMENAME;
                            SList.Add(litem);
                        }
                        Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] = SList;
                    }
                    else
                    {
                        Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] = null;
                    }
                    

                }
                else
                {
                    Session[Am.Ahv.Utilities.constants.SessionConstants.RegionList] = null;
                    Session[Am.Ahv.Utilities.constants.SessionConstants.SuburbList] = null;
                    Session[Am.Ahv.Utilities.constants.SessionConstants.StreetList] = null;
                }
            }
            catch (NullReferenceException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACPopulate, true);
                }
            }
        }
        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Validate"

        public void validate(string str)
        {
            Validation validation = new Validation();

            if (validation.emptyString(str))
            {
                IsError = true;
                return;
            }
            else if (validation.lengthString(str))
            {
                IsError = true;
                setMessage(UserMessageConstants.stringLengthMessage, true);
                return;
            }
            else
            {
                IsError = false;
            }
        }

        #endregion

        #region"Reset Page"

        public void resetPage()
        {
            try
            {
                quickfindtxt.Text = string.Empty;
                quickfindtxt.Enabled = true;
                findbtn.Enabled = true;
                teamddl.Enabled = true;
                teammemberddl.Enabled = true;
                typeddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                teamddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                teammemberddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                streetddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                suburbddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                regionddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                Session.Remove("ResourceCW");

                regionlbox.Items.Clear();
                suburbboxl.Items.Clear();
                streetbox.Items.Clear();
                HttpResponse.RemoveOutputCacheItem("/secure/resources/User.aspx");
                ClearSessionObjects();
            }
            catch (NullReferenceException nullref)
            {
                isError = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isError)
                {
                    setMessage(UserMessageConstants.ACresetpage, true);
                }
            }
        }

        #endregion

        #region"Bind region Id With Suburb Id"

        //public void BindRegionIdWithSuburbId()
        //{
        //    foreach (ListItem item in suburbddl.Items)
        //    {
        //        item.Value += ";" + regionddl.SelectedValue;
        //    }
        //}

        #endregion

        #region"Get Region Suburb Data Table"

        public DataTable GetRegionSuburbDataTable(List<ListItem> _regionList, List<ListItem> _suburbList, List<ListItem> _streetList)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RegionId");
            dt.Columns.Add("SuburbId");
            dt.Columns.Add("Street");

            if (_regionList != null || _suburbList != null)
            {
                if (_suburbList != null)
                {
                    for (int i = 0; i < _suburbList.Count; i++)
                    {
                        DataRow dr = dt.NewRow();
                        string[] str = _suburbList[i].Value.Split(';');
                        //dr["RegionId"] = str[1];
                        dr["RegionId"] = string.Empty;
                        dr["SuburbId"] = str[0];
                        dt.Rows.Add(dr);
                    }
                    if (_regionList != null)
                    {
                        for (int j = 0; j < _regionList.Count; j++)
                        {
                            DataRow dr = dt.NewRow();
                            if (dt.Rows.Count > 0)
                            {
                                if (_regionList[j] != null)
                                {
                                    if (!CheckRegion(dt, _regionList[j]))
                                    {
                                        dr["RegionId"] = _regionList[j].Value;
                                        dr["SuburbId"] = string.Empty;
                                        dt.Rows.Add(dr);
                                    }
                                }
                            }
                            else
                            {
                                if (_regionList[j] != null)
                                {
                                    dr["RegionId"] = _regionList[j].Value;
                                }
                               // dr["RegionId"] = string.Empty;
                                dr["SuburbId"] = string.Empty;
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                }
                else
                {
                    if (_regionList != null)
                    {
                        for (int j = 0; j < _regionList.Count; j++)
                        {
                            DataRow dr = dt.NewRow();
                            if (_regionList[j] != null)
                            {
                                dr["RegionId"] = _regionList[j].Value;
                            }
                            dr["SuburbId"] = string.Empty;
                            dt.Rows.Add(dr);
                        }
                    }
                }
            }

            if (_streetList != null)
            {
                for (int j = 0; j < _streetList.Count; j++)
                {
                    DataRow dr = dt.NewRow();
                    if (_streetList[j] != null)
                    {
                        dr["Street"] = _streetList[j].Value;
                    }
                    dr["RegionId"] = string.Empty;
                    dr["SuburbId"] = string.Empty;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }

        #endregion

        #region"check region"

        public bool CheckRegion(DataTable dt, ListItem region)
        {
            bool isPresent = false;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                    if (dt.Rows[i]["RegionId"].ToString() == region.Value)
                    {
                        isPresent = true;
                        break;
                    }
                
            }
            return isPresent;
        }

        #endregion

        #region"Populate Team Members"

        public void PopulateTeamMembers(int teamId)
        {
            cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
            teammemberddl.Items.Clear();
            List<E__EMPLOYEE> empList = cw.GetTeamMembers(teamId);
            DataTable dtEmployee = new DataTable();
            dtEmployee.Columns.Add("EmployeeName");
            dtEmployee.Columns.Add("EmployeeId");
            if (empList != null)
            {
                if (empList.Count > 0)
                {
                    for (int i = 0; i < empList.Count; i++)
                    {
                        DataRow dr = dtEmployee.NewRow();
                        dr["EmployeeName"] = empList[i].FIRSTNAME + " " + empList[i].LASTNAME;
                        dr["EmployeeId"] = empList[i].EMPLOYEEID;
                        dtEmployee.Rows.Add(dr);
                    }
                }
            }
            teammemberddl.DataSource = dtEmployee;
            teammemberddl.DataTextField = "EmployeeName";
            teammemberddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.EmployeId;
            teammemberddl.DataBind();
            teammemberddl.Items.Add(new ListItem("Select Member", Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue));
            teammemberddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
        }

        #endregion

        #region"Populate Suburbs"

        public void PopulateSuburbs()
        {
            try
            {
                cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
                if (regionddl.SelectedValue != ApplicationConstants.defaulvalue)
                {
                    List<P_SCHEME> suburbs = cw.GetAllSuburbs(int.Parse(regionddl.SelectedValue));
                    if (suburbs != null)
                    {
                        suburbddl.DataSource = suburbs;
                       // suburbddl.DataTextField = ApplicationConstants.suburbname;
                        suburbddl.DataTextField = ApplicationConstants.schememname;
                        suburbddl.DataValueField = ApplicationConstants.schemeId;
                        suburbddl.DataBind();
                       // BindRegionIdWithSuburbId();
                        suburbddl.Items.Add(new ListItem("Select Scheme", ApplicationConstants.defaulvalue));
                        suburbddl.SelectedValue = ApplicationConstants.defaulvalue;
                    }
                    else
                    {

                    }
                }
                else
                {
                    //suburbddl.DataSource = cw.GetAllSuburbs();
                    ////suburbddl.DataTextField = ApplicationConstants.suburbname;
                    //suburbddl.DataTextField = ApplicationConstants.schememname;
                    //suburbddl.DataValueField = ApplicationConstants.schemeId;
                    //suburbddl.DataBind();

                    suburbddl.Items.Clear();
                    suburbddl.Items.Add(new ListItem("Select Scheme", ApplicationConstants.defaulvalue));
                    suburbddl.SelectedValue = ApplicationConstants.defaulvalue;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        #region PopulateStreets
        private void PopulateStreets()
        {
            cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
            
            if (suburbddl.SelectedValue != ApplicationConstants.defaulvalue)
            {
                int suburbId = (int.Parse(suburbddl.SelectedValue));
                streetddl.DataSource = cw.GetStreetBySuburbId(suburbId);
                streetddl.DataBind();
                streetddl.Items.Add(new ListItem("Select Street", ApplicationConstants.defaulvalue));
                streetddl.SelectedValue = ApplicationConstants.defaulvalue;
            }
            else
            {
                //streetddl.DataSource = cw.GetAllStreets();
                //streetddl.DataBind();
                streetddl.Items.Clear();
                streetddl.Items.Add(new ListItem("Select Street", ApplicationConstants.defaulvalue));
                streetddl.SelectedValue = ApplicationConstants.defaulvalue;
            }
        }
        #endregion

        protected void regionlbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
               // int patchId =Convert.ToInt32 ( regionlbox.SelectedValue);
                cw = new Am.Ahv.BusinessManager.Casemgmt.AddCaseWorker();
                suburbboxl.DataSource = cw.GetSuburbsListByRegion(regionlbox.SelectedValue, Convert.ToInt32(Session[Am.Ahv.Utilities.constants.SessionConstants.EditResourceId]));
                suburbboxl.DataTextField = ApplicationConstants.schememname;
                suburbboxl.DataValueField = ApplicationConstants.schemeId;
                suburbboxl.DataBind();
            }
            catch (NullReferenceException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.ACPopulate, true);
                }
            }
        }

       

        #endregion
    }
}