﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.Customers;
using Am.Ahv.Utilities.constants;
using System.Drawing;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Utilities.utitility_classes;
namespace Am.Ahv.Website.controls.casemgt
{
    public partial class ClientLastPayment : UserControlBase
    {
        #region"Attributes"
        
        CustomersReport customer = null;
        
        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
            if (!IsPostBack)
            {                
                SetLastPaymentInformation();
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Set Last Payment Information"

        public void SetLastPaymentInformation()
        {
            try
            {                
                AM_SP_GetCustomerLastPayment_Result lastPayment = new AM_SP_GetCustomerLastPayment_Result();
                customer = new CustomersReport();

                if (!Convert.ToString(Session[SessionConstants.CustomerId]).Equals(string.Empty))
                {
                    lastPayment = customer.GetCustomerLastPayment(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                    if (lastPayment != null)
                    {
                        if (lastPayment.lastPayment.ToString().Equals(string.Empty))
                        {
                            SetMessage(UserMessageConstants.NoHBRecordLastPayment, true);
                            lastPaymentLabel.Visible = false;
                            lastPaymentTextBox.Visible = false;
                            btnHBSchedule.Enabled = false;
                        }
                        else
                        {
                            Session[SessionConstants.HBPayment] = lastPayment.lastPayment;
                            if (lastPayment.lastPayment.Value < 0)
                            {
                                txtLastCPayAmout.Text = "£ " + Math.Round(Math.Abs(Convert.ToDouble(lastPayment.lastPayment)), 2);
                                txtLastCPayAmout.ForeColor = Color.Black;
                            }
                            else
                            {
                                txtLastCPayAmout.Text = "(£ " + Math.Round(Convert.ToDouble(lastPayment.lastPayment), 2).ToString() + ")";
                                txtLastCPayAmout.ForeColor = Color.Red;
                            }
                            //lblCPayStartDate.Text = lastPayment.Startdate.ToString();
                            //lblCPAyLastDate.Text = lastPayment.TransactionDate.ToString();

                            btnHBSchedule.Enabled = true;
                        }                        
                        txtEstimatedHBDueAmount.Text = "£ " + Math.Round(Math.Abs(Convert.ToDouble(lastPayment.EstimatedHBDue)), 2).ToString();
                        double val = RentBalance.CalculateHBCyclePayment(Math.Abs(Convert.ToDouble(lastPayment.lastPayment)));
                        txtHBCyclePayment.Text = "£ " + val.ToString();
                        Session[SessionConstants.HBCyclePayment] = val;

                        string url = "../../../Customer/HousingBenefit.asp?TenancyId=" + Convert.ToString(base.GetTenantId()) + "&CustomerId=" + Convert.ToString(Session[SessionConstants.CustomerId]) + "&HBID=" + lastPayment.HBID.ToString();
                        btnHBSchedule.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
                        
                    }
                    else
                    {
                        lblCPayStartDate.Text = UserMessageConstants.NoHBRecordLastPayment;
                        lblCPayStartDate.Font.Bold = true;
                        lblCPayStartDate.ForeColor = Color.Red;
                        lblCPAyLastDate.Visible = false;
                        txtLastCPayAmout.Visible = false;
                        btnHBSchedule.Enabled = false;
                    }

                }                
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptiongettinglastPayment, true);
                }
            }
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #endregion

    }
}