﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.resource;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.utitility_classes;
using System.IO;
using Am.Ahv.Utilities.constants;
using System.Drawing;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.BusinessManager.Casemgmt;
using System.Globalization;
using System.Configuration;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.letters;
using System.Collections;
using Am.Ahv.BusinessManager.payments;


namespace Am.Ahv.Website.controls.casemgt
{
    public partial class OpenCaseSettings : UserControlBase
    {
        #region"Attributes"

        Users userManager = null;
        Status statusManager = null;
        Am.Ahv.BusinessManager.statusmgmt.Action actionManager = null;
        bool isException;
        OpenCase openCase = null;
        ArrayList arrayListDocumentName;
        ArrayList arrayListStandardLetter;
        ArrayList arrayListDocuments;
        ArrayList SavedLetters;
        ArrayList StandardLetterHistoryList;
        DataTable ActionData;
        List<AM_StandardLetterHistory> listStandardLetters;
        double marketRent = 0;
        double rentBalance = 0;
        DateTime todayDate = new DateTime();
        double rentAmount = 0;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
            if (!IsPostBack)
            {
                PopulateLookups();
                SetCaseManager();
                SetCaseOfficer();
                //ResetFields();
                if (Request.QueryString["cmd"] != null)
                {
                    if (Request.QueryString["cmd"] == "cedit")
                    {
                        PopulateCase();
                        PopulateLetters(0);
                        //CheckSavedLetters();
                        if (Request.QueryString["p"] != null)
                        {
                            if (Request.QueryString["p"] == "true")
                            {
                                if (Request.QueryString["slid"] != null)
                                {
                                    ChangeTheColorOfLetter(0, "Black", Convert.ToString(Request.QueryString["slid"]), true);
                                }
                                else
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else if (Request.QueryString["cmd"] == "CaseSelect")
                    {
                        if (Validation.CheckIntegerValue(Request.QueryString["LID"]))
                        {
                            PopulateCase();
                            PopulateLetters(int.Parse(Request.QueryString["LID"]));
                            // CheckSavedLetters();
                        }
                    }
                    else if (Request.QueryString["cmd"] == "sexpire")
                    {
                        SetMessage(UserMessageConstants.SessionExpiredOpenCase, true);
                        PopulateLookups();
                    }
                }
                updpnlOpenCase.Update();
            }


            //if (Request.QueryString["cmd"] != null && Request.QueryString["LID"] != null)
            //{
            //    if (Request.QueryString["cmd"] == "CaseSelect")
            //    {                
            //        if (Validation.CheckIntegerValue(Request.QueryString["LID"].ToString()))
            //        {
            //            //caseSettings.PopulateLookups();
            //            //caseSettings.PopulateCase();
            //            //caseSettings.PopulateLetters(int.Parse(Request.QueryString["LID"]));
            //        }
            //    }
            //}
            CheckSavedLetters();
        }

        #endregion

        #region"Arrears Selection Change"

        public void arrearsSelectionChange(object sender, EventArgs e)
        {
            int selectedArrearsStatus = Convert.ToInt32(ddlArrearsStatus.SelectedValue);
            //ddlArrearsStatus.SelectedValue = selectedArrearsStatus;
            this.LoadArrearsActionBySelectedStatus(selectedArrearsStatus);
            PopulateStatusReview(int.Parse(ddlArrearsStatus.SelectedValue));
            CheckFileUploadControl(selectedArrearsStatus);
            updpnlOpenCase.Update();
        }

        #endregion

        #region"DDl Arrears Action Selected Index Changed"

        protected void ddlArrearsAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
            PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
            CheckSavedLetters();
            CheckDocumentListBox();
            updpnlOpenCase.Update();
        }

        #endregion

        #region"Btn Upload Click"

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string destinationPath = ConfigurationManager.AppSettings["CaseDocumentsUploadPath"];
            if (!ValidateFile())
            {
                return;
            }
            try
            {
                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }
                destinationPath = destinationPath + FlUpload.FileName;
                FlUpload.SaveAs(destinationPath);
                lstDocuments.Items.Add(new ListItem(FlUpload.FileName));
                arrayListDocumentName.Add(FlUpload.FileName);
                SetViewStateList();
                updpnlOpenCase.Update();
            }
            catch (DirectoryNotFoundException ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionUploadFileCaseOpen, true);
                }
            }
        }

        #endregion

        #region"Btn Save Click"

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckCase())
            {
                SetMessage(UserMessageConstants.AlreadyExistCaseOpen, true);
                return;
            }
            if (!Validate())
            {
                updpnlOpenCase.Update();
                return;
            }
            Session[SessionConstants.OpenCaseFlag] = "true";
            
            #region"Commented Code because of the New Update (Didn't Delete the Code for future Use)"

            //if (CheckPaymentPlanMandatory())
            //{
            //    if (CheckPaymentPlan())
            //    {
            //        if (GetStatusRanking(int.Parse(ddlArrearsStatus.SelectedValue)) == 1)
            //        {
            //            if (GetActionRanking(int.Parse(ddlArrearsAction.SelectedValue)) == 1)
            //            {
            //                if (!OpenCase(false, string.Empty))
            //                {
            //                    SetMessage(UserMessageConstants.exceptingSavingCase, true);
            //                    updpnlOpenCase.Update();
            //                    return;
            //                }
            //                else
            //                {
            //                    Session.Remove(SessionConstants.OpenCaseFlag);
            //                    Session.Remove(ViewStateConstants.AddedStandardLetterListOpenNewCase);
            //                    Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
            //                    //ResetFields();
            //                    //SetMessage(UserMessageConstants.caseOpenSuccess, false);
            //                    //updpnlOpenCase.Update();
            //                    //return;
            //                }
            //            }
            //            else
            //            {
            //                ShowModalPopup();
            //            }
            //        }
            //        else
            //        {
            //            ShowModalPopup();
            //        }
            //    }
            //    else
            //    {
            //        lblIgnoreError.Visible = false;
            //        modalPopupPaymentPlanIgnoreReason.Show();
            //    }
            //}

            #endregion

          //  else
            {
                if (GetStatusRanking(int.Parse(ddlArrearsStatus.SelectedValue)) == 1)
                {
                    //if (GetActionRanking(int.Parse(ddlArrearsAction.SelectedValue)) == 1)
                    {
                        if (!OpenCase(false, string.Empty))
                        {
                            SetMessage(UserMessageConstants.exceptingSavingCase, true);
                            updpnlOpenCase.Update();
                            return;
                        }
                        else
                        {
                            Session.Remove(SessionConstants.OpenCaseFlag);
                            Session.Remove(ViewStateConstants.AddedStandardLetterListOpenNewCase);
                            Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                            //ResetFields();
                            //SetMessage(UserMessageConstants.caseOpenSuccess, false);
                            //updpnlOpenCase.Update();
                            //return;
                        }
                    }
                    //else
                    //{
                    //    ShowModalPopup();
                    //}
                }
                else
                {
                    ShowModalPopup();
                }
            }

        }

        #endregion

        #region"Txt Action Review Date text Changed"

        protected void txtActionReview_TextChanged(object sender, EventArgs e)
        {
            DateTime date = DateOperations.ConvertStringToDate(txtActionReview.Text);
            if (!DateOperations.FutureDateValidation(date))
            {
                SetMessage(UserMessageConstants.futureActionDateCaseOpen, true);
                return;
            }
            else if (!DateOperations.isValidUkDate(txtActionReview.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanValidDate, true);
                return;
            }
        }

        #endregion

        #region"Txt Status Review Text Changed"

        protected void txtStatusReview_TextChanged(object sender, EventArgs e)
        {
            DateTime date = DateOperations.ConvertStringToDate(txtActionReview.Text);
            if (!DateOperations.FutureDateValidation(date))
            {
                SetMessage(UserMessageConstants.futureActionDateCaseOpen, true);
                return;
            }
            else if (!DateOperations.isValidUkDate(txtStatusReview.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanValidDate, true);
                return;
            }
        }

        #endregion

        #region"Btn Cancel Click"

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ResetMessage();
            ResetFields();
            updpnlOpenCase.Update();
        }

        #endregion

        #region"Btn Standard Letter Click"

        protected void btnStandardLetter_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlArrearsAction.SelectedValue == "-1")
                {
                    SetMessage(UserMessageConstants.selectActionOpenCase, true);
                    return;
                }
                AM_Case caseNew = GetCachedCase();
                base.ClearCachedObject();
                base.RemoveCreateLetterCache();
                //int minutes = Convert.ToInt32(ConfigurationManager.AppSettings["CachedTimer"]);
                //TimeSpan span = new TimeSpan(0, minutes, 0);
                Session.Add("CachedCase", caseNew);
                FileOperations f = new FileOperations();
                string Path = f.getCurUrlAbsolutePath();
                Session["PrevPage"] = Path;
                Response.Redirect(PathConstants.lettersResourceAreaPathCached + "?cmd=CaseSelect&SID=" + ddlArrearsStatus.SelectedValue + "&AID=" + ddlArrearsAction.SelectedValue, false);
            }
            catch (NullReferenceException nullref)
            {
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"Lst Documents Selected Index Changed"

        protected void lstDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        #endregion

        #region"Btn Edit Click"

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (lstDocuments.SelectedValue == "-1" || lstDocuments.SelectedValue == "")
            {
                SetMessage(UserMessageConstants.selectOptionEditDocuments, true);
                return;
            }
            else
            {
                if (Validation.CheckIntegerValue(lstDocuments.SelectedValue))
                {
                    base.RemoveCreateLetterCache();
                    AM_Case caseNew = GetCachedCase();
                    int id = new LetterResourceArea().GetSLetterByHistoryID(int.Parse(lstDocuments.SelectedValue.ToString())).StandardLetterId;
                    //int minutes = Convert.ToInt32(ConfigurationManager.AppSettings["CachedTimer"]);
                    //TimeSpan span = new TimeSpan(0, minutes, 0);
                    Session.Add("CachedCase", caseNew);
                    SavedLetters = GetSavedLettersViewStateList();
                    if (SavedLetters != null)
                    {
                        if (SavedLetters.Contains(int.Parse(lstDocuments.SelectedValue)))
                        {
                            int historyId = GetStandardLetterHistoryId(int.Parse(lstDocuments.SelectedValue));
                            //int historyId = int.Parse(lstDocuments.SelectedValue.ToString());
                            if (historyId > 0)
                            {
                                //Response.Redirect(PathConstants.editLetterTemplate + "id=" + lstDocuments.SelectedValue + "&hId=" + historyId.ToString(), false);
                                Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + historyId.ToString(), false);
                            }
                            else
                            {
                                Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + int.Parse(lstDocuments.SelectedValue.ToString()), false);
                            }
                        }
                        else
                        {
                            Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + int.Parse(lstDocuments.SelectedValue.ToString()), false);
                        }
                    }
                    else
                    {
                        Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + int.Parse(lstDocuments.SelectedValue.ToString()), false);
                    }
                }
                else
                {
                    SetMessage(UserMessageConstants.selectStandardLettersEditDocuments, true);
                    return;
                }
            }
        }

        #endregion

        #region"btn Remove Click"

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (lstDocuments.SelectedValue == "-1" || lstDocuments.SelectedValue == "")
            {
                SetMessage(UserMessageConstants.selectOptionRemoveDocuments, true);
                return;
            }
            else
            {
                if (Validation.CheckIntegerValue(lstDocuments.SelectedValue))
                {
                    int index = lstDocuments.SelectedIndex;
                    lstDocuments.Items.RemoveAt(index);
                }
                else
                {
                    GetListFromViewState();
                    arrayListDocumentName.Remove(lstDocuments.SelectedItem.Text);
                    int index = lstDocuments.SelectedIndex;
                    lstDocuments.Items.RemoveAt(index);
                    SetViewStateList();
                }
            }
        }

        #endregion

        #region"Btn Action Save Click"

        protected void btnActionSave_Click(object sender, EventArgs e)
        {
            if (txtActionDetail.Text.Equals(string.Empty))
            {
                if (Convert.ToInt32(ViewState[ViewStateConstants.ModalPopupPageUpdateCase]) == 0)
                {
                    lblActionError.Text = UserMessageConstants.ActionModalPopupErrorUpdateCase;
                    lblActionError.Visible = true;
                }
                else
                {
                    lblActionError.Text = UserMessageConstants.ActionModalPopupIgnoreErrorUpdateCase;
                    lblActionError.Visible = true;
                }
                modalPopupActionDetails.Show();
                return;
            }
            else
            {
                lblActionError.Visible = false;
                SetActionDataInArrayList(Convert.ToInt32(ViewState[ViewStateConstants.ignoredActionIdUpdateCase]), Convert.ToInt32(ViewState[ViewStateConstants.IgnoredStatusUpdateCase]));
                if (!OpenCase(Convert.ToBoolean(ViewState[ViewStateConstants.isPaymentPlanIgnoredPaymentPlan]), Convert.ToString(ViewState[ViewStateConstants.IgnoreReasonPaymentPlan])))
                {
                    SetMessage(UserMessageConstants.exceptingSavingCase, true);
                    updpnlOpenCase.Update();
                    return;
                }
                else
                {
                    Session.Remove(SessionConstants.OpenCaseFlag);
                    Session.Remove(ViewStateConstants.AddedStandardLetterListOpenNewCase);
                    Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                    //ResetFields();
                    //SetMessage(UserMessageConstants.caseOpenSuccess, false);
                    //updpnlOpenCase.Update();
                    //return;
                }
            }
        }

        #endregion

        #region"Btn Next Click"

        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (txtActionDetail.Text.Equals(string.Empty))
            {
                if (Convert.ToInt32(ViewState[ViewStateConstants.ModalPopupPageUpdateCase]) == 0)
                {
                    lblActionError.Text = UserMessageConstants.ActionModalPopupErrorUpdateCase;
                    lblActionError.Visible = true;
                }
                else
                {
                    lblActionError.Text = UserMessageConstants.ActionModalPopupIgnoreErrorUpdateCase;
                    lblActionError.Visible = true;
                }
            }
            else
            {
                lblActionError.Visible = false;
                SetActionDataInArrayList(Convert.ToInt32(ViewState[ViewStateConstants.ignoredActionIdUpdateCase]), Convert.ToInt32(ViewState[ViewStateConstants.IgnoredStatusUpdateCase]));
                IterateIgnoredActions();                
            }
            modalPopupActionDetails.Show();
        }

        #endregion

        #region"Btn Action Cancel Click"

        protected void btnActionCancel_Click(object sender, EventArgs e)
        {
            ResetActionDetails();
            modalPopupActionDetails.Hide();
        }

        #endregion

        #region"Btn Ignore Reason Cancel Click"

        protected void btnIgnoreReasonCancel_Click(object sender, EventArgs e)
        {
            txtIgnoreReason.Text = string.Empty;
            modalPopupPaymentPlanIgnoreReason.Hide();
            Session[SessionConstants.OpenCaseFlag] = "true";
        }

        #endregion

        #region"Btn Reason Save Click"

        protected void btnReasonSave_Click(object sender, EventArgs e)
        {
            if (txtIgnoreReason.Text.Equals(string.Empty))
            {
                lblIgnoreError.Text = UserMessageConstants.EnterIgnoreReasonPaymentPlan;
                lblIgnoreError.Visible = true;
                modalPopupPaymentPlanIgnoreReason.Show();
            }
            else
            {
                ViewState[ViewStateConstants.isPaymentPlanIgnoredPaymentPlan] = "true";
                ViewState[ViewStateConstants.IgnoreReasonPaymentPlan] = txtIgnoreReason.Text;
                modalPopupPaymentPlanIgnoreReason.Hide();

                if (GetStatusRanking(int.Parse(ddlArrearsStatus.SelectedValue)) == 1)
                {
                    if (GetActionRanking(int.Parse(ddlArrearsAction.SelectedValue)) == 1)
                    {
                        if (!OpenCase(true, txtIgnoreReason.Text))
                        {
                            SetMessage(UserMessageConstants.exceptingSavingCase, true);
                            updpnlOpenCase.Update();
                            return;
                        }
                        else
                        {
                            Session.Remove(SessionConstants.OpenCaseFlag);
                            Session.Remove(ViewStateConstants.AddedStandardLetterListOpenNewCase);
                            Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", true);
                            //ResetFields();
                            //SetMessage(UserMessageConstants.caseOpenSuccess, false);
                            //updpnlOpenCase.Update();
                            //return;
                        }
                    }
                    else
                    {
                        ShowModalPopup();
                    }
                }
                else
                {
                    ShowModalPopup();
                }
            }
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Populate Lookups"

        public void PopulateLookups()
        {
            try
            {
                userManager = new Users();
                statusManager = new Status();

                lblCaseInitiatorName.Text = base.GetCurrentLoggedInUserName();

                ddlCaseOfficer.DataSource = userManager.GetActivePersons("Case Officer");
                ddlCaseOfficer.DataTextField = "name";
                ddlCaseOfficer.DataValueField = "ResourceId";
                ddlCaseOfficer.DataBind();
                ddlCaseOfficer.Items.Add(new ListItem("Please Select", "-1"));
                ddlCaseOfficer.SelectedValue = "-1";

                ddlCaseManager.DataSource = userManager.GetActivePersons("Case Manager");
                ddlCaseManager.DataTextField = "name";
                ddlCaseManager.DataValueField = "ResourceId";
                ddlCaseManager.DataBind();
                ddlCaseManager.Items.Add(new ListItem("Please Select", "-1"));
                ddlCaseManager.SelectedValue = "-1";

                PopulateStatus();
                PopulateStatusReview(int.Parse(ddlArrearsStatus.SelectedValue));
                this.LoadArrearsActionBySelectedStatus(int.Parse(ddlArrearsStatus.SelectedValue));
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
        }

        #endregion

        #region"Populate Status"

        public void PopulateStatus()
        {
            try
            {
                statusManager = new Status();
                List<Am.Ahv.Entities.AM_Status> statusList = statusManager.GetAllStatus();
                Am.Ahv.Entities.AM_Status firstStatus = statusList[0];
                ddlArrearsStatus.DataSource = statusManager.GetAllStatus();
                ddlArrearsStatus.DataTextField = "Title";
                ddlArrearsStatus.DataValueField = "StatusId";
                ddlArrearsStatus.DataBind();
                CheckFileUploadControl(int.Parse(ddlArrearsStatus.SelectedValue));
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
        }

        #endregion

        #region"Populate Actions"

        public void PopulateActions(int statusId)
        {
            try
            {
                Am.Ahv.BusinessManager.statusmgmt.Action action = new Am.Ahv.BusinessManager.statusmgmt.Action();
                List<AM_Action> listAction = action.GetActionByStatus(statusId);
                if (listAction != null)
                {
                    ddlArrearsAction.DataSource = listAction;
                    ddlArrearsAction.DataTextField = "Title";
                    ddlArrearsAction.DataValueField = "ActionId";
                    ddlArrearsAction.DataBind();
                }
                else
                {
                    ddlArrearsAction.Items.Clear();
                    ddlArrearsAction.Items.Add(new ListItem("Please Select", "-1"));
                    ddlArrearsAction.SelectedValue = "-1";
                    txtActionReview.Text = DateTime.Now.ToString("dd/MM/yyyy");
                }

            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Load Arrears Action By Selected Status"

        private void LoadArrearsActionBySelectedStatus(int arrearsStatusId)
        {
            try
            {
                PopulateActions(arrearsStatusId);
                //if (ddlArrearsAction.SelectedValue != "-1")
              //  {
                    PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
                    PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                    CheckSavedLetters();
                    CheckDocumentListBox();
                //}
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
        }

        #endregion

        #region"Populate Status Review"

        public void PopulateStatusReview(int statusId)
        {
            try
            {
                statusManager = new Status();
                DateTime reviewDate = new DateTime();
                AM_Status status = statusManager.GetStatusReviewPeriod(statusId);
                AM_LookupCode lookupList = status.AM_LookupCode;
                if (lookupList != null)
                {
                    reviewDate = DateOperations.AddDate(status.ReviewPeriod, lookupList.CodeName, DateTime.Now);
                    // CultureInfo ci = new CultureInfo("en-GB");
                    //DateTime date = Convert.ToDateTime(reviewDate, ci);
                    txtStatusReview.Text = reviewDate.ToString("dd/MM/yyyy");
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }

        }

        #endregion

        #region"Populate Actions Review"

        public void PopulateActionsReview(int actionId)
        {
            try
            {
                if(actionId != -1)
                {
                    actionManager = new BusinessManager.statusmgmt.Action();
                    DateTime reviewdate = new DateTime();
                    AM_Action action = actionManager.GetActionReview(actionId);
                    AM_LookupCode lookup = action.AM_LookupCode1;
                    if (lookup != null)
                    {
                        reviewdate = DateOperations.AddDate(action.RecommendedFollowupPeriod, lookup.CodeName, DateTime.Now);
                        //CultureInfo ci = new CultureInfo("en-GB");
                        //DateTime date = Convert.ToDateTime(reviewdate, ci.DateTimeFormat);
                        txtActionReview.Text = reviewdate.ToString("dd/MM/yyyy");
                    }
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }

        }

        #endregion

        #region"Populate Letters By Action Id"

        public void PopulateLettersByActionId(int actionId)
        {
            try
            {
                if (actionId != -1)
                {
                    LetterResourceArea letters = new LetterResourceArea();
                    List<AM_StandardLetterHistory> list = new List<AM_StandardLetterHistory>();
                    list = letters.GetLettersByActionId(actionId);
                    lstDocuments.Items.Clear();
                    if (list != null && list.Count > 0)
                    {
                        lstDocuments.DataSource = list;
                        lstDocuments.DataTextField = "Title";
                        //lstDocuments.DataValueField = "StandardLetterId";
                        lstDocuments.DataValueField = "StandardLetterHistoryId";
                        lstDocuments.DataBind();
                    }
                    else
                    {
                        //btnStandardLetter.Enabled = false;
                    }
                }
                else
                {
                    lstDocuments.Items.Clear();
                }
                GetStandardLetterViewState();
                if (listStandardLetters != null)
                {
                    if (listStandardLetters.Count > 0)
                    {
                        for (int i = 0; i < listStandardLetters.Count; i++)
                        {
                            ListItem item = new ListItem(listStandardLetters[i].Title, listStandardLetters[i].StandardLetterId.ToString());
                            lstDocuments.Items.Add(item);
                        }
                    }
                }
                AddDocumentsFromArrayList();
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingDocuments, true);
                }
            }
        }

        #endregion

        #region"Populate Case"

        public void PopulateCase()
        {
            AM_Case caseCached = new AM_Case();
            if (Session["CachedCase"] == null)
            {
                Response.Redirect(PathConstants.firstDetectionList + "?cmd=cexpire", false);
            }
            else
            {
                caseCached = Session["CachedCase"] as AM_Case;
                ddlArrearsStatus.SelectedValue = caseCached.StatusId.ToString();
                ddlCaseManager.SelectedValue = caseCached.CaseManager.ToString();
                ddlCaseOfficer.SelectedValue = caseCached.CaseOfficer.ToString();
                txtStatusReview.Text = caseCached.StatusReview.ToString("dd/MM/yyyy");
                PopulateActions(caseCached.StatusId);
                ddlArrearsAction.SelectedValue = caseCached.ActionId.ToString();
                txtActionReview.Text = caseCached.ActionReviewDate.ToString("dd/MM/yyyy");
                txtNotes.Text = caseCached.Notes;
            }
        }

        #endregion

        #region"Populate Letters"

        public void PopulateLetters(int letterId)
        {
            try
            {
                LetterResourceArea lresource = new LetterResourceArea();
                List<AM_StandardLetterHistory> listcollection = new List<AM_StandardLetterHistory>();
                if (Session["letterList"] == null)
                {
                    Response.Redirect(PathConstants.firstDetectionList + "?cmd=cexpire", false);
                }
                else if (!Convert.ToString(Session["letterList"]).Equals("No data"))
                {
                    listcollection = Session["letterList"] as List<AM_StandardLetterHistory>;
                }
                if (letterId != 0)
                {
                    ViewState["LetterId"] = letterId;
                    AM_StandardLetterHistory Standardletters = lresource.GetSLetterHistoryBySLetterID(letterId);
                    listcollection.Add(Standardletters);
                    if (listStandardLetters != null)
                    {
                        GetStandardLetterViewState();
                        listStandardLetters.Add(Standardletters);
                        SetStandardLetterViewState();
                    }
                    else
                    {
                        listStandardLetters = new List<AM_StandardLetterHistory>();
                        listStandardLetters.Add(Standardletters);
                        SetStandardLetterViewState();
                    }
                }
                lstDocuments.Items.Clear();
                lstDocuments.DataSource = listcollection;
                lstDocuments.DataTextField = "Title";
                lstDocuments.DataValueField = "StandardLetterHistoryId";
                lstDocuments.DataBind();
                //int k =0;
                //for (int j = 0; j < lstDocuments.Items.Count; j++)
                //{
                //    if (lstDocuments.Items[j].Value == listcollection[k].StandardLetterId.ToString())
                //    {
                //        if (listcollection[k].IsPrinted != null)
                //        {
                //            if (listcollection[k].IsPrinted.Value)
                //            {
                //                lstDocuments.Items[j].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "Red");
                //            }
                //            k++;
                //        }
                //    }
                //}
                if (Session["documentsArrayList"] == null)
                {
                    Response.Redirect(PathConstants.firstDetectionList + "?cmd=cexpire", false);
                }
                else if (!Convert.ToString(Session["documentsArrayList"]).Equals("No data"))
                {
                    arrayListDocumentName = Session["documentsArrayList"] as ArrayList;
                    SetViewStateList();
                }
                if (Session["SavedLettersUpdateCase"] == null)
                {
                    Response.Redirect(PathConstants.firstDetectionList + "?cmd=cexpire", false);
                }
                else if (!Convert.ToString(Session["SavedLettersUpdateCase"]).Equals("No data"))
                {
                    SavedLetters = Session["SavedLettersUpdateCase"] as ArrayList;
                    SetSavedLettersViewStateList();
                }
                for (int i = 0; i < arrayListDocumentName.Count; i++)
                {
                    lstDocuments.Items.Add(arrayListDocumentName[i].ToString());
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Open Case Settings"

        public OpenCaseSettings()
        {
            arrayListDocumentName = new ArrayList();
        }

        #endregion

        #region"Add Documents From Array List"

        public void AddDocumentsFromArrayList()
        {
            GetListFromViewState();
            if (arrayListDocumentName.Count <= 0)
            {
                return;
            }
            for (int i = 0; i < arrayListDocumentName.Count; i++)
            {
                lstDocuments.Items.Add(new ListItem(arrayListDocumentName[i].ToString()));
            }
        }

        #endregion

        #region"Get Cached Case"

        public AM_Case GetCachedCase()
        {
            AM_Case cachedCase = new AM_Case();
            cachedCase.InitiatedBy = base.GetCurrentLoggedinUser();
            cachedCase.CaseManager = int.Parse(ddlCaseManager.SelectedValue);
            cachedCase.CaseOfficer = int.Parse(ddlCaseOfficer.SelectedValue);
            cachedCase.StatusId = int.Parse(ddlArrearsStatus.SelectedValue);
            cachedCase.StatusReview = DateOperations.GetCulturedDateTime(txtStatusReview.Text);
            cachedCase.ActionId = int.Parse(ddlArrearsAction.SelectedValue);
            cachedCase.ActionReviewDate = DateOperations.GetCulturedDateTime(txtActionReview.Text);
            cachedCase.Notes = txtNotes.Text;
            List<AM_StandardLetterHistory> cachedLetters = LoadLetterList(lstDocuments.Items);

            //int minutes = Convert.ToInt32(ConfigurationManager.AppSettings["CachedTimer"]);
            //TimeSpan span = new TimeSpan(0,minutes,0);
            if (cachedLetters != null && cachedLetters.Count > 0)
            {
                Session.Add("letterList", cachedLetters);
            }
            else
            {
                Session.Add("letterList", "No data");
            }
            GetListFromViewState();
            if (arrayListDocumentName.Count > 0)
            {
                Session.Add("documentsArrayList", arrayListDocumentName);
            }
            else
            {
                Session.Add("documentsArrayList", "No data");
            }
            SavedLetters = new ArrayList();
            SavedLetters = GetSavedLettersViewStateList();
            if (SavedLetters != null)
            {
                Session.Add("SavedLettersUpdateCase", SavedLetters);
            }
            else
            {
                Session.Add("SavedLettersUpdateCase", "No data");
            }
            return cachedCase;
        }

        #endregion

        #region"Load Letter List"

        private List<AM_StandardLetterHistory> LoadLetterList(ListItemCollection listItemCollection)
        {
            List<AM_StandardLetterHistory> Slist = new List<AM_StandardLetterHistory>();
            AM_StandardLetterHistory sletter = null;
            foreach (ListItem item in listItemCollection)
            {
                sletter = new AM_StandardLetterHistory();
                if (item.Value != "-1")
                {
                    if (Validation.CheckIntegerValue(item.Value))
                    {
                        sletter.StandardLetterId = new LetterResourceArea().GetSLetterByHistoryID( int.Parse(item.Value)).StandardLetterId;
                        sletter.StandardLetterHistoryId = int.Parse(item.Value);
                        sletter.Title = item.Text;
                        Slist.Add(sletter);
                    }

                }
            }
            return Slist;
        }

        #endregion

        #region"Set Defaulter Bit"

        public bool SetDefaulterBit()
        {
            try
            {
                openCase = new OpenCase();
                return openCase.SetDefaulterBit(base.GetTenantId(), base.GetCurrentLoggedinUser());
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
            if (IsException)
                return true;
            else
                return false;

        }

        #endregion

        #region"Validate File"

        public bool ValidateFile()
        {
            // FileInfo file = new FileInfo(FlUpload.FileName);
            string fileExt = Path.GetExtension(FlUpload.FileName);
            int size = Convert.ToInt32(ConfigurationManager.AppSettings["FileSize"]);
            if (!fileExt.Equals(".pdf") && !fileExt.Equals(".doc") && !fileExt.Equals(".docx") &&
                !fileExt.Equals(".xls") && !fileExt.Equals(".xlsx") && !fileExt.Equals(".png") &&
                !fileExt.Equals(".jpg"))
            {
                SetMessage(UserMessageConstants.invalidImageType, true);
                return false;
            }
            //else if (file.Length > size)
            //{
            //    SetMessage(UserMessageConstants.fileSizeCaseOpen, true);
            //    return false;
            //}
            else
                return true;
        }


        #endregion

        #region"Open Case"

        public bool OpenCase(bool isPaymentPlanIgnored, string ignoreReason)
        {
            openCase = new OpenCase();
            int caseId = 0;
            try
            {
                AM_Case newCase = new AM_Case();
                AM_CaseHistory caseHistory = new AM_CaseHistory();
                
                statusManager = new Status();
                actionManager = new BusinessManager.statusmgmt.Action();

                newCase = SetCaseFields();
                caseHistory = SetCaseHistoryFields();                

                newCase.ActionHistoryId = actionManager.GetActionHistoryByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                caseHistory.ActionHistoryId = newCase.ActionHistoryId;

                newCase.StatusHistoryId = statusManager.GetStatusHistoryIdByStatus(int.Parse(ddlArrearsStatus.SelectedValue));
                caseHistory.StatusHistoryId = newCase.StatusHistoryId;

                bool paymentPlan = CheckPaymentPlan();

                newCase.IsPaymentPlan = paymentPlan;
                caseHistory.IsPaymentPlan = paymentPlan;

                newCase.IsPaymentPlanUpdated = false;
                caseHistory.IsPaymentPlanUpdated = false;

                if (paymentPlan)
                {
                    Payments paymentPlanManager = new Payments();
                    AM_PaymentPlan paymentPlanId = paymentPlanManager.GetPaymentPlan(base.GetTenantId());
                    if (paymentPlanId != null)
                    {
                        AM_PaymentPlanHistory paymentPlanHistoryId = paymentPlanManager.GetPaymentPlanHistory(paymentPlanId.PaymentPlanId);
                        if (paymentPlanHistoryId != null)
                        {
                            newCase.PaymentPlanId = paymentPlanId.PaymentPlanId;
                            caseHistory.PaymentPlanId = paymentPlanId.PaymentPlanId;

                            newCase.PaymentPlanHistoryId = paymentPlanHistoryId.PaymentPlanHistoryId;
                            caseHistory.PaymentPlanHistoryId = paymentPlanHistoryId.PaymentPlanHistoryId;
                        }
                    }
                }

                newCase.IsPaymentPlanIgnored = isPaymentPlanIgnored;
                caseHistory.IsPaymentPlanIgnored = isPaymentPlanIgnored;

                newCase.PaymentPlanIgnoreReason = ignoreReason;
                caseHistory.PaymentPlanIgnoreReason = ignoreReason;

                //caseId = openCase.OpenNewCase(newCase);

                //caseHistory.AM_CaseReference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_Case", "CaseId", caseId);
                //caseHistory.CaseId = caseId;
                SetStandardLetterandDocumentsName();
               // base.SetCaseId(caseId);
                
                //set rent balance, market rent and today's date
                this.setRbMrDate();
                
                ActionData = GetActionDatatable();
               
                
                int id = openCase.AddCaseHistoryandDocuments(newCase, caseHistory, arrayListDocuments, arrayListStandardLetter, base.GetTenantId(), base.GetCurrentLoggedinUser(), ActionData, this.marketRent, this.rentBalance, this.todayDate, this.rentAmount);
                if ( id < 0)
                {                    
                    return false;
                }
                else
                {
                    base.SetCaseId(id);
                    Session.Remove(SessionConstants.SavedLetterHistoryListOpenCase);
                    return true;
                }

            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                   // openCase.DisableOpenCase(caseId);
                    SetMessage(UserMessageConstants.exceptingSavingCase, true);
                }
            }
            if (IsException)
                return false;
            else
                return true;
        }


        #endregion

        #region set Rb Mr Date

        /// <summary>
        /// This function sets the rent balance, market rent and today's date, assuming that new case object is filled with entries
        /// </summary>
        /// <param name="newCase">object of new case</param>
        private void setRbMrDate(){
            PageBase pageBase = new PageBase();
            this.rentBalance = Convert.ToDouble(base.GetRentBalance());
            this.todayDate = DateTime.Now;
            this.rentBalance = Convert.ToDouble(base.GetRentBalance());
            this.rentAmount = Convert.ToDouble(pageBase.GetWeeklyRentAmount());

            if (Session[SessionConstants.TenantId] != null && Session[SessionConstants.Rentbalance] != null)
            {
                LetterResourceArea letterarea = new LetterResourceArea();

                int tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                this.marketRent = letterarea.GetMarketRent(tenantid);

            }
        }
        #endregion 

        #region"Check Payment Plan"

        public bool CheckPaymentPlan()
        {
            openCase = new OpenCase();
            return openCase.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Add Documents"

        public bool AddDocuments(int caseId, int caseHistoryId)
        {
            try
            {
                AM_Documents doc = new AM_Documents();
                openCase = new OpenCase();
                string name;

                for (int i = 0; i < lstDocuments.Items.Count; i++)
                {
                    //doc.AM_CaseReference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_Case", "CaseId", caseId);
                    //doc.AM_CaseHistoryReference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_CaseHistory", "CasehistoryId", caseHistoryId);
                    doc.CaseHistoryId = caseHistoryId;
                    doc.CaseId = caseId;
                    FileOperations.UploadFile(lstDocuments.Items[i].Text, caseId);
                    name = FileOperations.ChangeFileName(lstDocuments.Items[i].Text, caseId);
                    doc.DocumentName = name;
                    doc.IsActive = true;
                    openCase.AddDocuments(doc);
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingSavingDocuments, true);
                }
            }
            if (IsException)
                return false;
            else
                return true;
        }

        #endregion

        #region"Set Standard Letter and Documents Name"

        public void SetStandardLetterandDocumentsName()
        {
            try
            {
                arrayListDocuments = new ArrayList();
                arrayListStandardLetter = new ArrayList();
                //SavedLetters = GetSavedLettersViewStateList();
                StandardLetterHistoryList = GetStandardLetterHistoryList();

                string name = string.Empty;

                for (int i = 0; i < lstDocuments.Items.Count; i++)
                {
                    if (Validation.CheckIntegerValue(lstDocuments.Items[i].Value))
                    {
                        bool flag = false;
                        if (StandardLetterHistoryList != null)
                        {
                            for (int j = 0; j < StandardLetterHistoryList.Count; j++)
                            {
                                string[] str = StandardLetterHistoryList[j].ToString().Split(';');
                                if (str.Length > 2)
                                {
                                    if (str[2].Equals(lstDocuments.Items[i].Value))
                                    {
                                        //arrayListStandardLetter.Add(StandardLetterHistoryList[j].ToString());
                                        arrayListStandardLetter.Add(StandardLetterHistoryList[j].ToString().Split(';')[1]);
                                        flag = true;
                                        break;
                                    }
                                }
                                if (str[0].Equals(lstDocuments.Items[i].Value))
                                {
                                    arrayListStandardLetter.Add(StandardLetterHistoryList[j].ToString());
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        if (!flag)
                        {
                            arrayListStandardLetter.Add(lstDocuments.Items[i].Value + ";%");
                        }
                    }
                    else
                    {
                        //FileOperations.UploadFile(lstDocuments.Items[i].Text, caseId);
                        //name = FileOperations.ChangeFileName(lstDocuments.Items[i].Text, caseId);
                        arrayListDocuments.Add(lstDocuments.Items[i].Text);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion                

        #region"Validate"

        public bool Validate()
        {
            if (ddlCaseManager.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectCaseManagerCaseOpen, true);
                return false;
            }
            else if (ddlCaseOfficer.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectCaseOfficerCaseOpen, true);
                return false;
            }
            else if (ddlArrearsAction.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectActionOpenCase, true);
                return false;
            }
            else if (txtNotes.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.enterNotesUpdateCase, true);
                return false;
            }
            //else if (lstDocuments.Items.Count == 0)
            //{
            //    SetMessage(UserMessageConstants.selectDocuments, true);
            //    return false;
            //}
            else
                return true;
        }

        #endregion

        #region"Reset Fields"

        public void ResetFields()
        {
            ddlCaseManager.SelectedValue = "-1";
            ddlCaseOfficer.SelectedValue = "-1";
            PopulateStatus();
            PopulateStatusReview(int.Parse(ddlArrearsStatus.SelectedValue));
            this.LoadArrearsActionBySelectedStatus(int.Parse(ddlArrearsStatus.SelectedValue));
            lstDocuments.Items.Clear();
            arrayListDocumentName.Clear();
            ViewState.Remove(ViewStateConstants.DocumentsAttachedOpenCase);
            txtNotes.Text = string.Empty;
        }

        #endregion

        #region"Check Case"

        public bool CheckCase()
        {
            try
            {
                openCase = new BusinessManager.Casemgmt.OpenCase();
                return openCase.CheckCase(base.GetTenantId());
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
            if (IsException)
                return true;
            else
                return false;

        }

        #endregion

        #region"Check Document Upload"

        public bool CheckDocumentUpload(int statusid)
        {
            try
            {
                statusManager = new Status();
                return statusManager.CheckDocumentUpload(statusid);
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.uploadDocuments, true);
                }
            }
            if (IsException)
                return true;
            else
                return false;
        }

        #endregion

        #region"Check Document List Box"

        public void CheckDocumentListBox()
        {
            if (!FlUpload.Enabled && !btnStandardLetter.Enabled)
            {
                lstDocuments.Enabled = false;
                btnUpload.Enabled = false;
                btnEdit.Enabled = false;
                btnRemove.Enabled = false;
            }
            else
            {
                lstDocuments.Enabled = true;
                btnUpload.Enabled = true;
                btnEdit.Enabled = true;
                btnRemove.Enabled = true;
            }
        }

        #endregion

        #region"Check File Upload Control"

        public void CheckFileUploadControl(int statusId)
        {
            if (CheckDocumentUpload(statusId))
            {
                FlUpload.Enabled = true;
            }
            else
            {
                FlUpload.Enabled = false;
            }
        }


        #endregion

        #region"Check Document Attached"

        public bool CheckDocumentAttached()
        {
            bool stadardLetterFlag = false;
            for (int i = 0; i < lstDocuments.Items.Count; i++)
            {
                if (Validation.CheckIntegerValue(lstDocuments.Items[i].Value))
                {
                    stadardLetterFlag = true;
                    break;
                }
                else
                {
                    stadardLetterFlag = false;
                }
            }
            return stadardLetterFlag;
        }

        #endregion

        #region"Check Document Upload"

        public bool CheckDocumentUpload()
        {
            bool documentUploadFlag = false;
            foreach (ListItem items in lstDocuments.Items)
            {
                if (!Validation.CheckIntegerValue(items.Value))
                {
                    documentUploadFlag = true;
                    break;
                }
                else
                {
                    documentUploadFlag = false;
                }
            }
            return documentUploadFlag;
        }

        #endregion

        #region"Change the Color of Letter"

        public void ChangeTheColorOfLetter(int id, string color, string letter, bool fromPageLoad)
        {
            if (fromPageLoad)
            {
                string[] strId = letter.Split(';');
                id = new LetterResourceArea().GetSLetterHistoryBySLetterID( int.Parse(strId[0])).StandardLetterHistoryId;

            }
            for (int i = 0; i < lstDocuments.Items.Count; i++)
            {
                if (Validation.CheckIntegerValue(lstDocuments.Items[i].Value))
                {
                    string[] str = lstDocuments.Items[i].Value.Split(';');
                    if (id == int.Parse(str[0]))
                    {
                        lstDocuments.Items[i].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, color);
                        if (color == "Black")
                        {
                            if (fromPageLoad)
                            {
                                SetSavedLetters(id);
                                SetStandardLetterHistoryList(letter);
                            }
                            else
                            {
                                SetSavedLetters(id);
                            }
                            
                        }
                    }
                }
            }
        }

        #endregion

        #region"Set Saved Letters"

        public void SetSavedLetters(int id)
        {
            //SavedLetters = new ArrayList();
            SavedLetters = GetSavedLettersViewStateList();
            if (SavedLetters == null)
            {
                SavedLetters = new ArrayList();
            }
            if (!SavedLetters.Contains(id))
            {
                SavedLetters.Add(id);
                SetSavedLettersViewStateList();
            }
            else
                return;
        }

        #endregion

        #region"Check Saved Letters"

        public void CheckSavedLetters()
        {
            SavedLetters = new ArrayList();
            SavedLetters = GetSavedLettersViewStateList();
            if (lstDocuments.Items.Count > 0)
            {
                //for (int i = 0; i < SavedLetters.Count; i++)
                {
                    int i = 0;
                    foreach (ListItem item in lstDocuments.Items)
                    {
                        if (Validation.CheckIntegerValue(item.Value))
                        {
                            string[] str = item.Value.Split(';');
                            if (SavedLetters != null)
                            {
                                if (i < SavedLetters.Count)
                                {
                                    if (SavedLetters.Contains(int.Parse(str[0])))//[i].ToString()) == int.Parse(str[0]))
                                    {
                                        ChangeTheColorOfLetter(int.Parse(str[0]), "Black", "", false);
                                        i++;
                                    }
                                    else
                                    {
                                        ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                                    }
                                }
                                else if (!SavedLetters.Contains(int.Parse(str[0])))
                                {
                                    ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                                }
                            }
                            else
                            {
                                ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region"Check Payment Plan Mandatory"

        public bool CheckPaymentPlanMandatory()
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            return actionManager.CheckPaymentPlanMandatory(int.Parse(ddlArrearsAction.SelectedValue));
        }

        #endregion

        #region"Set Action Ranking"

        public void SetStatusAndActionRanking(int rank, int statusId)
        {
            statusManager = new Status();
            actionManager = new BusinessManager.statusmgmt.Action();
            AM_Status status = statusManager.GetStatusByRanking(1);
            AM_Action action = actionManager.GetActionByRank(status.StatusId, 1);
            ViewState[ViewStateConstants.RecordedActionRankingUpdateCase] = 1;// GetActionRanking(actionId);
            ListItem item = ddlArrearsAction.Items.FindByValue(Convert.ToString(action.ActionId));
            if (item != null)
            {
                ViewState[ViewStateConstants.RecordedActionUpdateCase] = item.Text;
            }
            else
            {
                ViewState[ViewStateConstants.RecordedActionUpdateCase] = action.Title;
            }
            ViewState[ViewStateConstants.RecordedStatusUpdateCase] = status.StatusId;
            ViewState[ViewStateConstants.RecordedStatusRankingUpdateCase] = 1;// GetStatusRanking(statusId);
            ViewState[ViewStateConstants.ignoredActionIdUpdateCase] = action.ActionId;
            ViewState[ViewStateConstants.StatusRankingIterationUpdateCase] = ViewState[ViewStateConstants.RecordedStatusRankingUpdateCase];
            ViewState[ViewStateConstants.IgnoredStatusUpdateCase] = ViewState[ViewStateConstants.RecordedStatusUpdateCase];
        }

        #endregion

        #region"Get Action Ranking"

        public int GetActionRanking(int actionId)
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            AM_Action action = actionManager.GetActionRanking(actionId);
            return action.Ranking;

        }

        #endregion

        #region"Get Status Ranking"

        public int GetStatusRanking(int statusId)
        {
            try
            {
                statusManager = new Status();
                AM_Status status = statusManager.GetStatusRanking(statusId);
                if (status != null)
                {
                    return status.Ranking;
                }
                else
                {
                    return 0;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Show Modal Popup"

        public void ShowModalPopup()
        {
            ResetActionDetails();
            SetStatusAndActionRanking(int.Parse(ddlArrearsAction.SelectedValue), int.Parse(ddlArrearsStatus.SelectedValue));
            statusManager = new Status();
            AM_Status CurrentStatus = statusManager.GetStatusRanking(int.Parse(ddlArrearsStatus.SelectedValue));
            if (CurrentStatus != null)
            {
                ViewState[ViewStateConstants.CurrentStatusRankingUpdateCase] = CurrentStatus.Ranking;
            }

            actionManager = new BusinessManager.statusmgmt.Action();
            AM_Action currentActionRank = actionManager.GetActionRanking(int.Parse(ddlArrearsAction.SelectedValue));

            if (currentActionRank != null)
            {
                //if ((currentActionRank.Ranking < Convert.ToInt32(ViewState[ViewStateConstants.RecordedActionRankingUpdateCase])) &&
                //     (currentActionRank.StatusId <= Convert.ToInt32(ViewState[ViewStateConstants.RecordedStatusUpdateCase])))
                //{
                //    SetMessage(UserMessageConstants.PreviousActionUpdateCase, true);
                //    return;
                //}
                if (CurrentStatus.Ranking > 1)
                {
                    ViewState[ViewStateConstants.NewStatusFlagUpdateCase] = true;
                    if (IsProceduralActionLeftWithInStageRange(CurrentStatus.Ranking))
                    {
                        ViewState[ViewStateConstants.CurrentActionRankUpdateCase] = currentActionRank.Ranking;
                        SetModalPopupControls(1, -1);
                        //SetMessage(UserMessageConstants.proceduralActionLeftUpdateCase, true);
                        //return;
                    }
                    else
                    {
                        if (!OpenCase(false, string.Empty))
                        {
                            SetMessage(UserMessageConstants.exceptingSavingCase, true);
                            updpnlOpenCase.Update();
                            return;
                        }
                        else
                        {
                            Session.Remove(SessionConstants.OpenCaseFlag);
                            Session.Remove(ViewStateConstants.AddedStandardLetterListOpenNewCase);
                            Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                        }
                    }
                }                
            }
            modalPopupPaymentPlanIgnoreReason.Hide();
            modalPopupActionDetails.Show();
        }

        #endregion

        #region"Is Status Ranking Equal"

        public bool IsStatusRankingEqual()
        {
            if (Convert.ToInt32(ViewState[ViewStateConstants.CurrentStatusRankingUpdateCase]) == Convert.ToInt32(ViewState[ViewStateConstants.StatusRankingIterationUpdateCase]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Set Modal Popup Controls"

        public bool SetModalPopupControls(int rankId, int currentIndex)
        {
            lblActionError.Visible = false;
            actionManager = new BusinessManager.statusmgmt.Action();
            //if (ViewState[ViewStateConstants.RecordedActionRankingUpdateCase] != null && ViewState[ViewStateConstants.RecordedActionUpdateCase] != null)
            //{
         
                AM_Action action = actionManager.GetActionByRank(Convert.ToInt32(ViewState[ViewStateConstants.IgnoredStatusUpdateCase]), rankId);
                if (action.IsProceduralAction)
                {
                    lblAction.Text = action.Title;
                    lblTitle.Text = UserMessageConstants.ActionIgnoredTitleUpdateCase;
                    ViewState[ViewStateConstants.ignoredActionIdUpdateCase] = action.ActionId;
                    //if (IsLastActionIgnored(rankId) && IsStatusRankingEqual())
                    if (IsLastActionIgnored(rankId, action.ActionId, currentIndex))
                    {
                        txtActionDetail.Text = string.Empty;
                        btnNext.Enabled = false;
                        btnActionSave.Enabled = true;
                        ViewState[ViewStateConstants.ModalPopupPageUpdateCase] = currentIndex;
                    }
                    else
                    {
                        txtActionDetail.Text = string.Empty;
                        btnNext.Enabled = true;
                        btnActionSave.Enabled = false;
                        ViewState[ViewStateConstants.ModalPopupPageUpdateCase] = currentIndex;
                    }
                    return true;
                }
                else
                {
                    if (IsLastActionIgnored(rankId, action.ActionId, currentIndex) && IsStatusRankingEqual())
                    {
                        if (!OpenCase(false, string.Empty))
                        {
                            SetMessage(UserMessageConstants.exceptingSavingCase, true);
                            updpnlOpenCase.Update();
                            return false;
                        }
                        else
                        {
                            Session.Remove(SessionConstants.OpenCaseFlag);
                            Session.Remove(ViewStateConstants.AddedStandardLetterListOpenNewCase);
                            Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                        }
                    }
                    else
                    {
                        IterateIgnoredActions();
                    }
                    return false;
                }
         
            return false;
            //}
        }

        #endregion

        #region"Set Action Data In Array List"

        public void SetActionDataInArrayList(int actionId, int statusId)
        {
            ActionData = new DataTable();
            ActionData.Columns.Add("ActionId");
            ActionData.Columns.Add("StatusId");
            ActionData.Columns.Add("IgnoreReason");
            if (ViewState[ViewStateConstants.ActionDataArrayListUpdateCase] != null)
            {
                ActionData = ViewState[ViewStateConstants.ActionDataArrayListUpdateCase] as DataTable;
            }
            DataRow dr = ActionData.NewRow();
            dr["ActionId"] = actionId;
            dr["StatusId"] = statusId;
            dr["IgnoreReason"] = txtActionDetail.Text;
            ActionData.Rows.Add(dr);
            ViewState[ViewStateConstants.ActionDataArrayListUpdateCase] = ActionData;
        }

        #endregion

        #region"Action Ranking Iteration"

        public void ActionRankingIteration()
        {
            if (ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] == null)
            {
                ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] = Convert.ToInt32(ViewState[ViewStateConstants.RecordedActionRankingUpdateCase]) + 1;
            }
            else
            {
                ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] = Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]) + 1;
            }
        }

        #endregion

        #region"Reset Action Details"

        public void ResetActionDetails()
        {
            if (ActionData != null)
            {
                ActionData.Clear();
            }
            ViewState.Remove(ViewStateConstants.ActionDataArrayListUpdateCase);
            ViewState.Remove(ViewStateConstants.ActionListCounterUpdateCase);
            ViewState.Remove(ViewStateConstants.ModalPopupPageUpdateCase);
            ViewState.Remove(ViewStateConstants.ActionRankingIterationUpdateCase);
            ViewState[ViewStateConstants.StatusRankingIterationUpdateCase] = ViewState[ViewStateConstants.RecordedStatusRankingUpdateCase];
            ViewState[ViewStateConstants.IgnoredStatusUpdateCase] = ViewState[ViewStateConstants.RecordedStatusUpdateCase];
        }

        #endregion

        #region"Status Ranking Iteration"

        public void StatusRankingIteration()
        {
            ViewState[ViewStateConstants.StatusRankingIterationUpdateCase] = Convert.ToInt32(ViewState[ViewStateConstants.StatusRankingIterationUpdateCase]) + 1;
        }

        #endregion

        #region"Set Case Fields"

        public AM_Case SetCaseFields()
        {
            AM_Case newCase = new AM_Case();

            newCase.TenancyId = base.GetTenantId();
            newCase.InitiatedBy = base.GetCurrentLoggedinUser();
            newCase.ActionId = int.Parse(ddlArrearsAction.SelectedValue);
            newCase.ActionReviewDate = DateOperations.GetCulturedDateTime(txtActionReview.Text);
            newCase.CaseManager = int.Parse(ddlCaseManager.SelectedValue);
            newCase.StatusId = int.Parse(ddlArrearsStatus.SelectedValue);            
            newCase.CaseOfficer = int.Parse(ddlCaseOfficer.SelectedValue);
            newCase.StatusReview = DateOperations.GetCulturedDateTime(txtStatusReview.Text);
            newCase.Notes = txtNotes.Text;
            newCase.ActionRecordedDate = DateTime.Now;
            newCase.CreatedDate = DateTime.Now;
            newCase.ModifiedDate = DateTime.Now;
            newCase.ModifiedBy = base.GetCurrentLoggedinUser();
            newCase.IsActive = true;
            newCase.IsSuppressed = false;
            newCase.StatusRecordedDate = DateTime.Now;
            newCase.IsCaseUpdated = false;

            if (lstDocuments.Items.Count > 0)
            {
                if (CheckDocumentAttached())
                {
                    newCase.IsDocumentAttached = true;            
                }
                else
                {
                    newCase.IsDocumentAttached = false;
                }

                if (CheckDocumentUpload())
                {
                    newCase.IsDocumentUpload = true;
                }
                else
                {
                    newCase.IsDocumentUpload = false;
                }
            }
            else
            {
                newCase.IsDocumentAttached = false;
                newCase.IsDocumentUpload = false;
            }
            newCase.RentBalance = Convert.ToDouble(base.GetRentBalance());

            //if (Session[SessionConstants.TenantId] != null && Session[SessionConstants.Rentbalance] != null)
            //{
            //    LetterResourceArea letterarea = new LetterResourceArea();

            //    int tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());                
            //    newCase.MarketRent = letterarea.GetMarketRent(tenantid);                
            //}
            return newCase;
        }

        #endregion

        #region"Set Case History Fields"

        public AM_CaseHistory SetCaseHistoryFields()
        {
            AM_CaseHistory caseHistory = new AM_CaseHistory();
                        
            caseHistory.TennantId = base.GetTenantId();            
            caseHistory.InitiatedById = base.GetCurrentLoggedinUser();
            caseHistory.ActionId = int.Parse(ddlArrearsAction.SelectedValue);
            caseHistory.ActionReviewDate = DateOperations.GetCulturedDateTime(txtActionReview.Text);
            caseHistory.CaseManager = int.Parse(ddlCaseManager.SelectedValue);
            caseHistory.StatusId = int.Parse(ddlArrearsStatus.SelectedValue);            
            caseHistory.CaseOfficer = int.Parse(ddlCaseOfficer.SelectedValue);
            caseHistory.StatusReview = DateOperations.GetCulturedDateTime(txtStatusReview.Text);
            caseHistory.Notes = txtNotes.Text;
            caseHistory.ActionRecordedDate = DateTime.Now;
            caseHistory.CreatedDate = DateTime.Now;
            caseHistory.ModifiedDate = DateTime.Now;
            caseHistory.ModifiedBy = base.GetCurrentLoggedinUser();
            caseHistory.IsActive = true;
            caseHistory.IsSuppressed = false;
            caseHistory.StatusRecordedDate = DateTime.Now;
            caseHistory.IsCaseUpdated = false;

            if (lstDocuments.Items.Count > 0)
            {
                if (CheckDocumentAttached())
                {                    
                    caseHistory.IsDocumentAttached = true;
                }
                else
                {                 
                    caseHistory.IsDocumentAttached = false;
                }

                if (CheckDocumentUpload())
                {                 
                    caseHistory.IsDocumentUpload = true;
                }
                else
                {                 
                    caseHistory.IsDocumentUpload = false;
                }
            }
            else
            {                
                caseHistory.IsDocumentAttached = false;                
                caseHistory.IsDocumentUpload = false;
            }
            caseHistory.RentBalance = Convert.ToDouble(base.GetRentBalance());
            return caseHistory;
        }

        #endregion

        #region"Get Action Data Table"

        public DataTable GetActionDatatable()
        {
            return ViewState[ViewStateConstants.ActionDataArrayListUpdateCase] as DataTable;
        }

        #endregion

        #region"Get Array List Standard Letter"

        public ArrayList GetArrayListStandardLetter()
        {
            SetStandardLetterandDocumentsName();
            return arrayListStandardLetter;
        }

        #endregion

        #region"Get Array List Document Name"

        public ArrayList GetArrayListDocumentName()
        {
            SetStandardLetterandDocumentsName();
            return arrayListDocumentName;
        }

        #endregion

        #region"Get Standard Letter History Id"

        public int GetStandardLetterHistoryId(int slId)
        {
            int sl = new LetterResourceArea().GetSLetterByHistoryID(slId).StandardLetterId;
            StandardLetterHistoryList = GetStandardLetterHistoryList();
            if (StandardLetterHistoryList != null)
            {
                for (int i = 0; i < StandardLetterHistoryList.Count; i++)
                {
                    string[] str = StandardLetterHistoryList[i].ToString().Split(';');
                    if (str.Count() >= 2)
                    {
                        if (int.Parse(str[0]) == sl)
                            return int.Parse(str[1]);
                    }
                    else
                    {
                        return -1;
                    }
                    
                }
                return -1;
            }
            else
            {
                return -1;
            }
        }

        #endregion

        #endregion

        #region"View State"

        #region"Set View State List"

        public void SetViewStateList()
        {
            ViewState[ViewStateConstants.DocumentsAttachedOpenCase] = arrayListDocumentName;
        }

        #endregion

        #region"Get List From View State"

        public void GetListFromViewState()
        {
            if (!Convert.ToString(ViewState[ViewStateConstants.DocumentsAttachedOpenCase]).Equals(string.Empty))
            {
                arrayListDocumentName = ViewState[ViewStateConstants.DocumentsAttachedOpenCase] as ArrayList;
            }
        }

        #endregion

        #region"Set Saved Letters View State List"

        public void SetSavedLettersViewStateList()
        {
            ViewState.Remove(ViewStateConstants.SavedLettersUpdateCase);
            ViewState[ViewStateConstants.SavedLettersUpdateCase] = SavedLetters;
        }

        #endregion

        #region"Get Saved Letters View State List"

        public ArrayList GetSavedLettersViewStateList()
        {
            if (!Convert.ToString(ViewState[ViewStateConstants.SavedLettersUpdateCase]).Equals(string.Empty))
            {
                return ViewState[ViewStateConstants.SavedLettersUpdateCase] as ArrayList;
            }
            else
                return null;
        }

        #endregion

        #region"Set Standard Letter View State"

        public void SetStandardLetterViewState()
        {
            if (listStandardLetters != null)
            {
                Session[ViewStateConstants.AddedStandardLetterListOpenNewCase] = listStandardLetters;
            }
        }

        #endregion

        #region"Get Standard Letter View State"

        public void GetStandardLetterViewState()
        {
            if (Session[ViewStateConstants.AddedStandardLetterListOpenNewCase] != null)
            {
                listStandardLetters = Session[ViewStateConstants.AddedStandardLetterListOpenNewCase] as List<AM_StandardLetterHistory>;
            }
        }

        #endregion


        #endregion

        #region"Set Standard Letter History List"

        public void SetStandardLetterHistoryList(string letter)
        {
            StandardLetterHistoryList = GetStandardLetterHistoryList();
            if (StandardLetterHistoryList == null)
            {
                StandardLetterHistoryList = new ArrayList();
            }
            if (!StandardLetterHistoryList.Contains(letter))
            {
                for (int i = 0; i < StandardLetterHistoryList.Count; i++)
                {
                    string[] str = StandardLetterHistoryList[i].ToString().Split(';');
                    string[] str2 = letter.Split(';');
                    if (str2[0].Equals(str[0]))
                    {
                        StandardLetterHistoryList.RemoveAt(i);
                    }
                }
                StandardLetterHistoryList.Add(letter);
                Session[SessionConstants.SavedLetterHistoryListOpenCase] = StandardLetterHistoryList;
            }
        }

        #endregion

        #region"Get Standard Letter History List"

        public ArrayList GetStandardLetterHistoryList()
        {
            StandardLetterHistoryList = Session[SessionConstants.SavedLetterHistoryListOpenCase] as ArrayList;
            return StandardLetterHistoryList;
        }

        #endregion

        #region"Iterate Ignored Actions"

        /// <summary>
        /// Iterate the Ignored Actions to record the ignore reasons.
        /// </summary>

        public void IterateIgnoredActions()
        {
            if (Convert.ToInt32(ViewState[ViewStateConstants.RecordedStatusUpdateCase]) == Convert.ToInt32(ddlArrearsStatus.SelectedValue))
            {
                ViewState[ViewStateConstants.NewStatusFlagUpdateCase] = false;
                ActionRankingIteration();
                SetModalPopupControls(Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]), Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]));
            }
            else
            {
                ViewState[ViewStateConstants.NewStatusFlagUpdateCase] = true;
                ActionRankingIteration();
                statusManager = new Status();
                actionManager = new BusinessManager.statusmgmt.Action();
                AM_Action action = actionManager.GetActionByRank(Convert.ToInt32(ViewState[ViewStateConstants.IgnoredStatusUpdateCase]), Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]));
                if (action != null)
                {
                    ViewState[ViewStateConstants.ignoredActionIdUpdateCase] = action.ActionId;
                    SetModalPopupControls(action.Ranking, Convert.ToInt32(ViewState[ViewStateConstants.ModalPopupPageUpdateCase]) + 1);
                }
                else
                {
                    StatusRankingIteration();
                    if (Convert.ToInt32(ViewState[ViewStateConstants.StatusRankingIterationUpdateCase]) == Convert.ToInt32(ViewState[ViewStateConstants.CurrentStatusRankingUpdateCase]))
                    {
                        if (!OpenCase(false, string.Empty))
                        {
                            SetMessage(UserMessageConstants.exceptingSavingCase, true);
                            updpnlOpenCase.Update();
                            return;
                        }
                        else
                        {
                            Session.Remove(SessionConstants.OpenCaseFlag);
                            Session.Remove(ViewStateConstants.AddedStandardLetterListOpenNewCase);
                            Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                        }
                    }
                    else
                    {
                        AM_Status status = statusManager.GetStatusByRanking(Convert.ToInt32(ViewState[ViewStateConstants.StatusRankingIterationUpdateCase]));
                        if (status != null)
                        {
                            ViewState[ViewStateConstants.IgnoredStatusUpdateCase] = status.StatusId;
                            ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] = 1;
                            AM_Action action2 = actionManager.GetActionByRank(Convert.ToInt32(ViewState[ViewStateConstants.IgnoredStatusUpdateCase]), Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]));
                            if (action2 != null)
                            {
                                ViewState[ViewStateConstants.ignoredActionIdUpdateCase] = action2.ActionId;
                                SetModalPopupControls(action2.Ranking, Convert.ToInt32(ViewState[ViewStateConstants.ModalPopupPageUpdateCase]) + 1);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region"Is Last Action Ignored"

        /// <summary>
        /// Check if the Action Ignored is last or not?
        /// </summary>
        /// <param name="rankId">rank id of the ignored action</param>
        /// <returns></returns>

        public bool IsLastActionIgnored(int rankId, int previousIgnoredActionId, int isFirstIteration)
        {
            //if ((((rankId + 1) >= Convert.ToInt32(ViewState[ViewStateConstants.CurrentActionRankUpdateCase])) &&
            //    (Convert.ToInt32(ViewState[ViewStateConstants.StatusRankingIterationUpdateCase]) == Convert.ToInt32(ViewState[ViewStateConstants.CurrentStatusRankingUpdateCase]))) ||
            //    ((actionManager.GetActionByRank(Convert.ToInt32(ViewState[ViewStateConstants.IgnoredStatusUpdateCase]), rankId + 1) == null) &&
            //    (Convert.ToInt32(ViewState[ViewStateConstants.StatusRankingIterationUpdateCase]) + 1 == Convert.ToInt32(ViewState[ViewStateConstants.CurrentStatusRankingUpdateCase])) &&
            //     Convert.ToInt32(ViewState[ViewStateConstants.CurrentActionRankUpdateCase]) == 1) ||
            //     (IsIgnoredProceduralActionRemaining(Convert.ToInt32(ViewState[ViewStateConstants.IgnoredStatusUpdateCase]), rankId + 1, int.Parse(ddlArrearsAction.SelectedValue), int.Parse(ddlArrearsStatus.SelectedValue)) == false))
            if ((IsIgnoredProceduralActionRemaining(Convert.ToInt32(ViewState[ViewStateConstants.IgnoredStatusUpdateCase]), rankId + 1, int.Parse(ddlArrearsAction.SelectedValue), int.Parse(ddlArrearsStatus.SelectedValue), previousIgnoredActionId, isFirstIteration) == false))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Is Ignored Procedural Action Remaining"

        /// <summary>
        /// Check if there exist any Ignored procedural action
        /// </summary>
        /// <param name="statusId"></param>
        /// <param name="rankId"></param>
        /// <param name="currentActionId"></param>
        /// <param name="currentStatusId"></param>
        /// <returns></returns>

        public bool IsIgnoredProceduralActionRemaining(int statusId, int rankId, int currentActionId, int currentStatusId, int previousIgnoredActionId, int isFirstIteration)
        {
            try
            {
                actionManager = new BusinessManager.statusmgmt.Action();
                return actionManager.IsIgnoredProceduralActionRemaining(statusId, rankId, currentActionId, currentStatusId, previousIgnoredActionId, isFirstIteration);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Procedural Action Left Within Stage Range"

        public bool IsProceduralActionLeftWithInStageRange(int currentStageRank)
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            return actionManager.IsProceduralActionLeftWithInStageRange(currentStageRank);
        }

        #endregion

        #region"Set Case Officer"

        public void SetCaseOfficer()
        {
            ListItem item = ddlCaseOfficer.Items.FindByValue(Convert.ToString(base.GetCurrentLoggedinUser()));
            if (item != null)
            {
                ddlCaseOfficer.SelectedValue = item.Value;
            } 
        }

        #endregion

        #region"Set Case Manager"

        public void SetCaseManager()
        {
            string user = "Charlie Conley";
            ListItem item = ddlCaseManager.Items.FindByText(user);
            if (item != null)
            {
                ddlCaseManager.SelectedValue = item.Value;
            }
 
        }

        #endregion
    }
}