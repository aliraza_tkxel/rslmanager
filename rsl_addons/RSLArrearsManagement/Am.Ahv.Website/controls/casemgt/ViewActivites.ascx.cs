﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Casemgmt;
using System.Data;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager;

namespace Am.Ahv.Website.controls.casemgt
{
    public partial class ViewActivites : UserControlBase
    {

        #region"Attributes"

        AddCaseActivity caseActivityManager = null;

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
            if (!IsPostBack)
            {
                PopulateGridView();
            }
        }

        #endregion

        #region"Btn Back Click"

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.casehistory, false);
        }

        #endregion

        #region"Btn Add Activity Click"

        protected void btnAddActivity_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.addCaseActivityPath, false);
        }

        #endregion

        #region"LBTN Hide Notes Click"

        protected void lbtnHideNotes_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            LinkButton lbtnShow = new LinkButton();
            Label lbl = new Label();
            int index = ((RepeaterItem)lbtn.NamingContainer).ItemIndex;
            lbtnShow = (LinkButton)rptrCaseActivitiesList.Items[index].FindControl("lbtnShowNotes");
            lbtn = (LinkButton)rptrCaseActivitiesList.Items[index].FindControl("lbtnHideNotes");
            lbl = (Label)rptrCaseActivitiesList.Items[index].FindControl("lblNotes");
            lbtnShow.Visible = true;
            lbl.Visible = false;
            lbtn.Visible = false;
            updpnlCaseActivities.Update();
            
        }

        #endregion

        #region"LBTN Show Notes Click"

        protected void lbtnShowNotes_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            LinkButton lbtnHide = new LinkButton();
            Label lbl = new Label();
            int index = ((RepeaterItem)lbtn.NamingContainer).ItemIndex;
            lbtnHide = (LinkButton)rptrCaseActivitiesList.Items[index].FindControl("lbtnHideNotes");
            lbtn = (LinkButton)rptrCaseActivitiesList.Items[index].FindControl("lbtnShowNotes");
            lbl = (Label)rptrCaseActivitiesList.Items[index].FindControl("lblNotes");
            lbtnHide.Visible = true;
            lbl.Visible = true;
            lbtn.Visible = false;
            updpnlCaseActivities.Update();
        }

        #endregion

        #region"Btn Print Click"

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.caseActivitiesReportPath, false);
        }


        #endregion       

        protected void lbtnDocumentName_Click(object sender, EventArgs e)
        {
            //DataListItem item = ((LinkButton)sender).Parent as DataListItem;
            //int index = item.ItemIndex;
            //LinkButton lbtn = (LinkButton)dataSetUploadedDocuments.Items[index].FindControl("lbtnDocumentName");

            ////string fileName = "test.xls";
            ////string filePath = Server.MapPath(ConfigurationManager.AppSettings["CaseDocumentsUploadPath"]);
            //string filePath = ConfigurationManager.AppSettings["CaseDocumentsUploadPath"] + lbtn.Text;
            //Response.Clear();

            //Response.AppendHeader("content-disposition", "attachment; filename=" + lbtn.Text);
            //Response.ContentType = "application/pdf";
            //Response.WriteFile(filePath);
            //Response.Flush();
            //Response.End();
        }

        #region SL Image Button Click
        protected void imgbuttonDocument_Click(object sender, ImageClickEventArgs e)
        {
            //Code Goes here
            try
            {
                ImageButton btn = sender as ImageButton;
                if (btn.ImageUrl != string.Empty)
                {
                    int ID = Convert.ToInt32(btn.CommandArgument);
                    LoadLetters(ID);
                    LoadDocs(ID);
                    this.PopupExtenderdocuments.Show();
                }
                else
                {
                    btn.Visible = false;
                }
                //uphistorydetails.Update();

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHimagebutton, true);
                }
            }


        }
        #endregion

        #region Load Letters
        private void LoadLetters(int ActivityId)
        {
            BusinessManager.letters.LetterResourceArea LRA = new BusinessManager.letters.LetterResourceArea();

            List<AM_ActivityAndStandardLetters> Letterlist = LRA.GetStandardLetterByActivityId(ActivityId);


            if (Letterlist != null && Letterlist.Count != 0)
            {
                this.sldatalist.DataSource = Letterlist;
                this.sldatalist.DataBind();
                LinkStandardLetters();
                lblnostandardletters.Visible = false;
                slpanel.Visible = true;
            }
            else
            {
                lblnostandardletters.Text = "No Standard Letters Attached";
                lblnostandardletters.Visible = true;
                slpanel.Visible = false;
            }
            upmodalpanel.Update();

        }
        #endregion

        #region Image Button Click
        protected void imgbuttonLetter_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton btn = sender as ImageButton;
                if (btn.ImageUrl != string.Empty)
                {
                    int ID = Convert.ToInt32(btn.CommandArgument);
                    LoadDocs(ID);
                    LoadLetters(ID);
                    this.PopupExtenderdocuments.Show();
                }
                else
                {
                    btn.Visible = false;
                }
                //uphistorydetails.Update();
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.CHimagebutton, true);
                }
            }
        }
        #endregion



        #endregion

        #region"Populate Data"

        #region"Populate Grid View"

        public void PopulateGridView()
        {
            try
            {
                caseActivityManager = new AddCaseActivity();
                rptrCaseActivitiesList.DataSource = caseActivityManager.GetCaseActivities(base.GetCaseId());
                rptrCaseActivitiesList.DataBind();
                updpnlCaseActivities.Update();
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionloadingViewActivities, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Functions"      

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion       

        public void ScriptBuilder()
        {
            LinkButton lbtn = new LinkButton();
            string url = string.Empty;

            for (int i = 0; i < dataSetUploadedDocuments.Items.Count; i++)
            {
                lbtn = (LinkButton)dataSetUploadedDocuments.Items[i].FindControl("lbtnDocumentName");
                url = PathConstants.downloadFile + "?p=asa&file=" + lbtn.Text;
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }
        private void LinkStandardLetters()
        {

            LinkButton lbtn = new LinkButton();
            string url = string.Empty;
            for (int i = 0; i < sldatalist.Items.Count; i++)
            {
                lbtn = (LinkButton)sldatalist.Items[i].FindControl("lbtnstandardletter");
                url = PathConstants.CHRedirecttopreview + "PopUp&Id=" + lbtn.CommandArgument.ToString();
                lbtn.Attributes.Add("onClick", "window.open('" + url + "'," + ApplicationConstants.javaScriptWindowAttributes);
            }
        }

        #region"Make Standard Letter Id"

        public string MakeStandardLetterId(string StandardLetterHistoryId, string StandardLetterId)
        {
            if (StandardLetterHistoryId != string.Empty || StandardLetterHistoryId != null)
            {
                return StandardLetterId + ";" + StandardLetterHistoryId;
            }
            else
            {
                return StandardLetterId + ";%";
            }
        }

        #endregion

        #region LoadDocumentsInsidePanel
        private void LoadDocs(int ActivityID)
        {
            Am.Ahv.BusinessManager.documents.Document DocManager = new Am.Ahv.BusinessManager.documents.Document();
            List<Am.Ahv.Entities.AM_Documents> doclist = DocManager.GetDocumentsByActivityId(ActivityID);
            if (doclist != null && doclist.Count != 0)
            {
                dataSetUploadedDocuments.DataSource = doclist;
                dataSetUploadedDocuments.DataBind();
                ScriptBuilder();
                lblnodocs.Visible = false;
                pnlDataSet.Visible = true;
            }
            else
            {
                lblnodocs.Text = "No Document Attached";
                lblnodocs.Visible = true;
                pnlDataSet.Visible = false;
            }
            upmodalpanel.Update();
        }
        #endregion


        #endregion


    }
}