﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateCase.ascx.cs"
    Inherits="Am.Ahv.Website.controls.casemgt.UpdateCase" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="PanelUpdateCase" runat="server" Width="100%" BorderWidth="1px" BorderColor="Gray"
    Height="100%">
    <%--<div style="border-bottom: solid 1px Gray; font-weight:bold; padding:5px; width:95%; margin-left:1%; margin-right:3%">
<asp:Label ID="lblUpdateCase" runat="server" Text="Case Details"></asp:Label>
</div>--%>
    <div>
        <asp:UpdatePanel ID="updpnlUpdateCase" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <table cellpadding="5px">
                    <tr>
                        <td align="left" class="tableHeader" style="border-color: Gray;" colspan="3">
                            <div style="float: left;">
                                <asp:Label ID="lblCaseDetails" runat="server" Text="Case Details"></asp:Label></div>
                            <div style="float: right;">
                                <asp:Button ID="btnSuppressedCase" runat="server" Text="Suppress Case"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblintiatedby" runat="server" Text="Initiated By:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Label ID="lblCaseInitiatorName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblcasemanager" runat="server" Text="Case Manager:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarCasemanager" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCaseManager" runat="server" CssClass="localSelect" Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblcaseofficer" runat="server" Text="Case Officer:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarCaseOfficer" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCaseOfficer" runat="server" CssClass="localSelect" Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblarrearsstatus" runat="server" Text="Arrears Stage:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarStatus" runat="server" Font-Bold="true" ForeColor="Red" Text="*"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlArrearsStatus" runat="server" CssClass="localSelect" AutoPostBack="True"
                                OnSelectedIndexChanged="ddlArrearsStatus_SelectedIndexChanged" Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblstatusreview" runat="server" Text="Stage Review:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarReview" runat="server" Font-Bold="true" ForeColor="Red" Text="*"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtStatusReview" runat="server" CssClass="localInput" AutoPostBack="true"
                                OnTextChanged="txtStatusReview_TextChanged"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtStatusReview"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblarrearsaction" runat="server" Text="Arrears Action:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarAction" runat="server" Font-Bold="true" ForeColor="Red" Text="*"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlArrearsAction" runat="server" CssClass="localSelect" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlArrearsAction_SelectedIndexChanged" Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblActionReview" runat="server" Text="Action Review:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarActionreview" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtActionReview" AutoPostBack="true" runat="server" OnTextChanged="txtActionReview_TextChanged"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtenderActionReview" runat="server" TargetControlID="txtActionReview"
                                Format="dd/MM/yyyy" PopupPosition="TopRight">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr id="rowRecoverAmount" runat="server">
                        <td align="left" valign="middle">
                            <asp:Label ID="lblRecoveryAmount" runat="server" Text="Gross Rent Balance:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarRecoveryAmount" runat="server" Font-Bold="true" ForeColor="Red" Visible="false"
                                Text="*"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtRecoveryAmount" AutoPostBack="true" runat="server" OnTextChanged="txtRecoveryAmount_TextChanged"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="rowNoticeIssue" runat="server">
                        <td align="left" valign="middle">
                            <asp:Label ID="lblNoticeIssue" runat="server" Text="Notice Issue:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarNoticeIssue" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*" Visible="true"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNoticeIssue" AutoPostBack="true" runat="server" OnTextChanged="txtNoticeIssue_TextChanged"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtenderNoticeIssue" runat="server" TargetControlID="txtNoticeIssue"
                                Format="dd/MM/yyyy" PopupPosition="TopRight">
                            </asp:CalendarExtender>
                        </td>
                    </tr>                    
                    <tr id="rowNoticeExpiry" runat="server">
                        <td align="left" valign="middle">
                            <asp:Label ID="lblNoticeExpiry" runat="server" Text="Notice Expiry:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarNoticeExpiry" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*" Visible="true"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNoticeExpiry" AutoPostBack="true" runat="server" OnTextChanged="txtNoticeExpiry_TextChanged"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtenderNoticeExpiry" runat="server" TargetControlID="txtNoticeExpiry"
                                Format="dd/MM/yyyy" PopupPosition="TopRight">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr id="rowHearingDate" runat="server">
                        <td align="left" valign="middle">
                            <asp:Label ID="lblHearingDate" runat="server" Text="Hearing Date:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="Label4" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*" Visible="false"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtHearingDate" AutoPostBack="true" runat="server" OnTextChanged="txtHearingDate_TextChanged"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtenderHearingDate" runat="server" TargetControlID="txtHearingDate"
                                Format="dd/MM/yyyy" PopupPosition="TopRight">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr id="rowWarrantExpiry" runat="server">
                        <td align="left" valign="middle">
                            <asp:Label ID="lblWarrantExpiryDate" runat="server" Text="Warrant Expiry Date:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="Label6" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*" Visible="false"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtWarrantExpiryDate" AutoPostBack="true" runat="server" OnTextChanged="txtWarrantExpiry_TextChanged"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtenderWarrantExpiry" runat="server" TargetControlID="txtWarrantExpiryDate"
                                Format="dd/MM/yyyy" PopupPosition="TopRight">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblLetter" runat="server" Text="Letter:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:Button ID="btnStandardLetter" runat="server" Text="Standard Letter:" OnClick="btnStandardLetter_Click" />
                            &nbsp; OR
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="center" valign="middle">
                            &nbsp;
                        </td>
                        <td align="left">
                            <div style="float: left;">
                                <asp:FileUpload ID="FlUpload" runat="server" />
                            </div>
                            <div style="float: right;">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lbldocuments" runat="server" Text="Documents:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                        </td>
                        <td align="left">
                            <asp:ListBox ID="lstDocuments" runat="server" CssClass="localListBox" Width="250px">
                            </asp:ListBox>
                            <br />
                            <br />
                            <asp:Button ID="btnRemove" runat="server" Text="Remove" OnClick="btnRemove_Click" />
                            &nbsp;<asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnUpload_Click" />
                            &nbsp;<asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblnotes" runat="server" Text="Notes:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarNotes" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*" Visible="true"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtNotes" runat="server" Height="200px" TextMode="MultiLine" CssClass="localInput"
                                Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                            <asp:Label ID="lblOutcome" runat="server" Text="Outcomes:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Label ID="lblStarOutcome" Visible="false" runat="server" Font-Bold="true" ForeColor="Red"
                                Text="*"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlOutcome" runat="server" CssClass="localSelect" AutoPostBack="true"
                                Width="250px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblReason" runat="server" Text="Reason:"></asp:Label>
                        </td>
                        <td align="center" valign="middle">
                            &nbsp;
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtReason" runat="server" Rows="4" TextMode="MultiLine" CssClass="localInput"
                                Width="250px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td style="text-align: right">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            <asp:Button ID="btnSaveAmends" runat="server" Text="Save Amends" OnClick="btnSaveAmends_Click" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlsupresscase" runat="server" GroupingText=" " BackColor="white">
                    <table>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:ImageButton ID="imgsupress" runat="server" ImageAlign="Top" ImageUrl="~/style/images/Close Icon.jpg"
                                    Width="15px" Height="15px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>
                                    <asp:Label ID="lblsupressmessage" runat="server" Text="Supress Case"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblsupresserrormsg" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblreasonsupress" runat="server" Text="Reason:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtsupressreason" runat="server" TextMode="MultiLine">
            
            
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblreview" runat="server" Text="Review:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtreviewdate" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtreviewdate"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                                <asp:Button ID="btnsupresscancel" runat="server" Text="Cancel" OnClick="btnsupresscancel_Click"
                                    OnClientClick="ClearSupressCaseFields()" />
                                <asp:Button ID="btnsupresssave" runat="server" Text="Save" OnClick="btnsupresssave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:ModalPopupExtender ID="upmodalextendersupresscase" runat="server" TargetControlID="btnSuppressedCase"
                    PopupControlID="pnlsupresscase" CancelControlID="imgsupress" Enabled="true" BackgroundCssClass="modalBackground"
                    DropShadow="true">
                </asp:ModalPopupExtender>
                <asp:Panel ID="pnlAction" runat="server" GroupingText=" " Width="400px" BackColor="white">
                    <table>
                        <tr>
                            <td align="right" colspan="2">
                                <%--<asp:ImageButton ID="imgignorebtn" runat="server" ImageAlign="Top" ImageUrl="~/style/images/Close Icon.jpg"
                                            Width="15px" Height="15px" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>
                                    <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblActionError" ForeColor="Red" Font-Bold="true" Visible="false" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAction" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtActionDetail" runat="server" TextMode="MultiLine">                  
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>                            
                            <td align="right" colspan="2">
                                <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" />
                                <asp:Button ID="btnActionCancel" runat="server" Text="Cancel" OnClick="btnActionCancel_Click" />
                                <asp:Button ID="btnActionSave" runat="server" Text="Save" OnClick="btnActionSave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:ModalPopupExtender ID="modalPopupActionDetails" runat="server" PopupControlID="pnlAction"
                    CancelControlID="btnActionCancel" Enabled="true" BackgroundCssClass="modalBackground" TargetControlID="lblTitle"
                    DropShadow="true">
                </asp:ModalPopupExtender>

                               <asp:Panel ID="pnlIgnorePaymentPlan" runat="server" GroupingText=" " Width="300px" BackColor="white">
                    <table>
                        <tr>
                            <td align="right" colspan="2">
                                <%--<asp:ImageButton ID="imgignorebtn" runat="server" ImageAlign="Top" ImageUrl="~/style/images/Close Icon.jpg"
                                            Width="15px" Height="15px" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>
                                    <asp:Label ID="Label1" runat="server" Text="A payment plan is mandatory against the selected action.<br /> Please create a payment plan or enter the reason to ignore."></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblIgnoreError" ForeColor="Red" Font-Bold="true" Visible="false" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Reason: "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtIgnoreReason" runat="server" TextMode="MultiLine">                  
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>                            
                            <td align="right" colspan="2">                                
                                <asp:Button ID="btnIgnoreReasonCancel" runat="server" Text="Cancel" OnClick="btnIgnoreReasonCancel_Click" />
                                <asp:Button ID="btnReasonSave" runat="server" Text="Save" OnClick="btnReasonSave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:ModalPopupExtender ID="modalPopupPaymentPlanIgnoreReason" runat="server" PopupControlID="pnlIgnorePaymentPlan"
                    CancelControlID="btnIgnoreReasonCancel" Enabled="true" BackgroundCssClass="modalBackground" TargetControlID="Label1"
                    DropShadow="true">
                </asp:ModalPopupExtender>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAdd" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Panel>
