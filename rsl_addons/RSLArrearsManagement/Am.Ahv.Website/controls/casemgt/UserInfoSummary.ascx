<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserInfoSummary.ascx.cs"
    Inherits="Am.Ahv.Website.controls.casemgt.UserInfoSummary" %>
<style type="text/css">
    .divStyle
    {
        vertical-align: middle;
        padding: 2px;
    }
</style>
<asp:Panel ID="Panel1" runat="server" Height="190px" Width="100%" BorderWidth="1px" BorderColor="Gray">
    <div style="margin: 5px">
        <div style="border-bottom: solid 1px Black" class="divStyle">
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <%--<asp:Label ID="lblUserName" runat="server" CssClass="NormalPointFontBold"></asp:Label>--%>
            <asp:LinkButton ID="lbtnUserName" runat="server" CssClass="NormalPointFontBold"></asp:LinkButton>
        </div>
        <div style="width: 234px; margin-top: 5px" class="divStyle">
            <span>
                <asp:Label ID="lbladdress" runat="server" Text="Address:"></asp:Label>
            </span> 
            <span>
                <asp:Label ID="lblUserAddress" runat="server"></asp:Label>
            </span>
        </div>
        <div style="height: 20px;" class="divStyle">
            Tenancy ID:
            <asp:Label ID="lblTenancyId" runat="server"></asp:Label>
        </div>
        <div style="height: 20px" class="divStyle">
            <asp:Label ID="lblrentbalanceusr" runat="server" Text="Rent Balance:"></asp:Label>
            <span class="NormalPointFontBold">
                <%--<asp:Label ID="lblRentBalance" runat="server"></asp:Label>--%>
                 <asp:LinkButton ID="lbtnRentBalance" runat="server" CssClass="NormalPointFontBold"></asp:LinkButton>
            </span>
        </div>
        <div style="height: 20px" class="divStyle">
            <asp:Label ID="lblOwedToBHA" runat="server" Text="Owed to BHA:"></asp:Label>
            <span class="NormalPointFontBold">
                <asp:Label ID="lblOwedToBHAAmount" runat="server"></asp:Label>
            </span>
        </div>
        <div style="height: 20px" class="divStyle">
            <asp:Label ID="lbllastpaymentusr" runat="server" Text="Last Payment:"></asp:Label>
            <span class="NormalPointFontBold">
                <asp:Label ID="lblLastPayment" runat="server"></asp:Label>
            </span><span style="margin-left: 5px">
                <asp:Label ID="lblLastPaymentDate" runat="server"></asp:Label>
            </span>
        </div>  
        <div style="height: 20px" class="divStyle">
            <asp:Label ID="lblMonthlyRent" runat="server" Text="Monthly Rent:"></asp:Label>
            <span class="NormalPointFontBold">
                <asp:Label ID="lblMonthlyRentAmount" runat="server"></asp:Label>
            </span>
        </div>       
    </div>
</asp:Panel>
