﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OpenCaseSettings.ascx.cs"
    Inherits="Am.Ahv.Website.controls.casemgt.OpenCaseSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style1
    {
        width: 142px;
        padding-bottom: 12px;
        vertical-align: middle;
    }
    .localInput
    {
        border: 1px solid Gray;
        font-size: 12px;
        padding: 2px;
        width: 200px;
    }
    
    
    .localSelect
    {
        border: 1px solid Gray;
        font-size: 12px;
        padding: 2px;
        width: 200px;
    }
    
    .localListBox
    {
        border: 1px solid Gray;
        font-size: 12px;
        padding: 2px;
        width: 200px;
    }
</style>
<asp:Panel ID="PanelOpenCase" runat="server" Width="100%" BorderWidth="1px" BorderColor="Gray"
    Height="100%">
    <div style="border-bottom: solid 1px Gray; font-weight: bold; padding: 5px; width: 95%;
        margin-left: 1%; margin-right: 3%">
        <asp:Label ID="lblopennewcase" runat="server" Text="Open a New Case"></asp:Label>
    </div>
    <div>
        <asp:UpdatePanel ID="updpnlOpenCase" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <table style="width: 100%; padding: 10px; vertical-align: middle">
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblintiatedby" runat="server" Text="Initiated By"></asp:Label>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblCaseInitiatorName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblcasemanager" runat="server" Text="Case Manager"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblStarCasemanager" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                            <asp:DropDownList ID="ddlCaseManager" runat="server" CssClass="localSelect">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblcaseofficer" runat="server" Text="Case Officer"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblStarCaseOfficer" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                            <asp:DropDownList ID="ddlCaseOfficer" runat="server" CssClass="localSelect">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblarrearsstatus" runat="server" Text="Arrears Stage"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblStarStatus" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                            <asp:DropDownList ID="ddlArrearsStatus" runat="server" CssClass="localSelect" OnSelectedIndexChanged="arrearsSelectionChange"
                                AutoPostBack="True" Enabled="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblstatusreview" runat="server" Text="Stage Review"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblStarReview" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                            <asp:TextBox ID="txtStatusReview" runat="server" CssClass="localInput" AutoPostBack="True"
                                OnTextChanged="txtStatusReview_TextChanged"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtStatusReview"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblarrearsaction" runat="server" Text="Arrears Action"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblStarAction" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                            <asp:DropDownList ID="ddlArrearsAction" runat="server" CssClass="localSelect" AutoPostBack="true" Enabled="true"
                                OnSelectedIndexChanged="ddlArrearsAction_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                        <asp:Label ID="lblActionReview" runat="server" Text="Action Review"></asp:Label>
                            
                        </td>
                        <td>
                            <asp:Label ID="lblStarActionreview" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                            <asp:TextBox ID="txtActionReview" AutoPostBack="true" runat="server" CssClass="localInput"
                                OnTextChanged="txtActionReview_TextChanged"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtenderActionReview" runat="server" TargetControlID="txtActionReview"
                                Format="dd/MM/yyyy" PopupPosition="TopRight">
                            </asp:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                        <asp:Label ID="lblLetter" runat="server" Text="Letter"></asp:Label>                            
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;<asp:Button ID="btnStandardLetter" runat="server" Text="Standard Letter" OnClick="btnStandardLetter_Click" />
                            <br />
                            &nbsp;&nbsp;&nbsp;<asp:Label ID="lblOR" runat="server" Text="OR"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <div style="float: left;">
                                &nbsp;&nbsp;&nbsp;<asp:FileUpload ID="FlUpload" Visible="true" runat="server" Width="130px" /></div>
                            <div style="float: right;">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lbldocuments" runat="server" Text="Documents"></asp:Label>
                        </td>
                        <td>                            
                            &nbsp;&nbsp;&nbsp;<asp:ListBox ID="lstDocuments" runat="server" CssClass="localListBox" OnSelectedIndexChanged="lstDocuments_SelectedIndexChanged">
                            </asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;<asp:Button ID="btnUpload" runat="server" Text="Add" OnClick="btnUpload_Click" />&nbsp;
                            <asp:Button ID="btnRemove" runat="server" Text="Remove" OnClick="btnRemove_Click" />&nbsp;
                            <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="lblnotes" runat="server" Text="Notes"></asp:Label>
                        </td>
                        <td>
                           <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                           <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="localInput" Height="200px" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                        </td>
                        <td style="text-align: right">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save Case" />
                        </td>
                    </tr>
                </table>
                 <asp:Panel ID="pnlAction" runat="server" GroupingText=" " Width="450px" BackColor="white">
                    <table>
                        <tr>
                            <td align="right" colspan="2">
                                <%--<asp:ImageButton ID="imgignorebtn" runat="server" ImageAlign="Top" ImageUrl="~/style/images/Close Icon.jpg"
                                            Width="15px" Height="15px" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>
                                    <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblActionError" ForeColor="Red" Font-Bold="true" Visible="false" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAction" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtActionDetail" runat="server" TextMode="MultiLine">                  
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>                            
                            <td align="right" colspan="2">
                                <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" />
                                <asp:Button ID="btnActionCancel" runat="server" Text="Cancel" OnClick="btnActionCancel_Click" />
                                <asp:Button ID="btnActionSave" runat="server" Text="Save" OnClick="btnActionSave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:ModalPopupExtender ID="modalPopupActionDetails" runat="server" PopupControlID="pnlAction"
                    CancelControlID="btnActionCancel" Enabled="true" BackgroundCssClass="modalBackground" TargetControlID="lblTitle"
                    DropShadow="true">
                </asp:ModalPopupExtender>
                  <asp:Panel ID="pnlIgnorePaymentPlan" runat="server" GroupingText=" " Width="400px" BackColor="white">
                    <table>
                        <tr>
                            <td align="right" colspan="2">
                                <%--<asp:ImageButton ID="imgignorebtn" runat="server" ImageAlign="Top" ImageUrl="~/style/images/Close Icon.jpg"
                                            Width="15px" Height="15px" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>
                                    <asp:Label ID="lbl" runat="server" Text="A payment plan is mandatory against the selected action.<br /> Please create a payment plan or enter the reason to ignore."></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblIgnoreError" ForeColor="Red" Font-Bold="true" Visible="false" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblReason" runat="server" Text="Reason: "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtIgnoreReason" runat="server" TextMode="MultiLine">                  
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>                            
                            <td align="right" colspan="2">                                
                                <asp:Button ID="btnIgnoreReasonCancel" runat="server" Text="Create Payment Plan Now" OnClick="btnIgnoreReasonCancel_Click" />
                                <asp:Button ID="btnReasonSave" runat="server" Text="Save" OnClick="btnReasonSave_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:ModalPopupExtender ID="modalPopupPaymentPlanIgnoreReason" runat="server" PopupControlID="pnlIgnorePaymentPlan"
                    CancelControlID="btnIgnoreReasonCancel" Enabled="true" BackgroundCssClass="modalBackground" TargetControlID="lbl"
                    DropShadow="true">
                </asp:ModalPopupExtender>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUpload" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Panel>
