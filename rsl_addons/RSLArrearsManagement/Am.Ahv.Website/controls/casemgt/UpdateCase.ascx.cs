﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.resource;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.utitility_classes;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.Drawing;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Website.pagebase;
using System.Text.RegularExpressions;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.BusinessManager.lookup;
using System.Transactions;
using System.Configuration;
using System.IO;
using System.Text;
using System.Globalization;
using System.Collections;
using Am.Ahv.BusinessManager.letters;
using Am.Ahv.BusinessManager.payments;

namespace Am.Ahv.Website.controls.casemgt
{
    public partial class UpdateCase : UserControlBase
    {
        #region"Attributes"

        Users userManager = null;
        Status statusManager = null;
        Am.Ahv.BusinessManager.statusmgmt.Action actionManager = null;
        CaseDetails caseDetails = null;
        Lookup lookupManager = null;
        OpenCase openCase = null;
        Am.Ahv.BusinessManager.Casemgmt.CaseHistory HistoryManager = null;
        ArrayList arrayListDocumentName;
        ArrayList SavedLetters;
        ArrayList StandardLetterHistoryList;
        List<AM_StandardLetterHistory> oldSL;
        DataTable ActionData;

        double marketRent = 0;
        double rentBalance = 0;
        DateTime todayDate = new DateTime();
        double rentAmount = 0;

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        bool isDocumentAttached;

        public bool IsDocumentAttached
        {
            get { return isDocumentAttached; }
            set { isDocumentAttached = value; }
        }

        int totalAttached;

        public int TotalAttached
        {
            get { return totalAttached; }
            set { totalAttached = value; }
        }

        #endregion

        #region"Events"

        #region"Page Load"
        /// <summary>
        /// Responsible for taking the request and calling the 
        /// desired methods at the start up of the user control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
            if (!IsPostBack)
            {
                Isupressed();
                PopulateLookups();
                if (!CheckSessionValues())
                {
                    Response.Redirect(PathConstants.casehistory);
                }
                else if (Request.QueryString["cmd"] != null)
                {
                    if (Request.QueryString["cmd"] == "UpdateCaseSelect")
                    {
                        if (Validation.CheckIntegerValue(Request.QueryString["LID"]))
                        {
                            PopulateCaseFromCache();
                            PopulateLetters(int.Parse(Request.QueryString["LID"]));
                            CheckSavedLetters();
                        }
                    }
                    else if (Request.QueryString["cmd"] == "cedit")
                    {
                        PopulateCaseFromCache();
                        PopulateLetters(0);
                        CheckSavedLetters();
                        if (Request.QueryString["p"] != null)
                        {
                            if (Request.QueryString["p"] == "true")
                            {
                                if (Request.QueryString["slid"] != null)
                                {
                                    ChangeTheColorOfLetter(0, "Black", Convert.ToString(Request.QueryString["slid"]), true);
                                }
                                else
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else if (Request.QueryString["cmd"] == "sexpire")
                    {
                        SetMessage(UserMessageConstants.SessionExpiredUpdateCase, true);
                        PopulateLookups();
                    }
                    else if (Request.QueryString["cmd"] == "RecordAction")
                    {
                        LoadCaseForActionRecord();
                    }
                    else if (Request.QueryString["cmd"] == "BackToAction")
                    {
                        PopulateCaseFromCache();
                        PopulateLetters(0);
                    }
                }
                else
                {
                    LoadCase();
                }
            }
            CheckSavedLetters();
        }

        #endregion

        #region"Btn Standard Letter Click"

        /// <summary>
        /// Button Event. It is invoked when the user wishes to add some Standard Letters/Templates
        /// to the case. It stores the current state/values of controls set by the user in the session
        /// and redirects to the Letter Resource Area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnStandardLetter_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlArrearsAction.SelectedValue == "-1")
                {
                    SetMessage(UserMessageConstants.selectActionOpenCase, true);
                    return;
                }
                AM_Case caseNew = GetCachedCase();
                base.ClearCachedObject();
                base.RemoveCreateLetterCache();
                Session.Add("CachedUpdateCase", caseNew);
                FileOperations f = new FileOperations();
                string Path = f.getCurUrlAbsolutePath();
                Session["PrevPage"] = Path;
                Response.Redirect(PathConstants.lettersResourceAreaPathCached + "?cmd=UpdateCaseSelect&SID=" + ddlArrearsStatus.SelectedValue + "&AID=" + ddlArrearsAction.SelectedValue, false);
            }
            catch (NullReferenceException nullref)
            {
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #region"DDL Arrears Status Selected Index Changed"

        /// <summary>
        /// This event invokes when the user changes the selected value of Stage(Status) drop down list.
        /// It populates the data on the controls according to the selected Stage (Status).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void ddlArrearsStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedArrearsStatus = Convert.ToInt32(ddlArrearsStatus.SelectedValue);
            this.LoadArrearsActionBySelectedStatus(selectedArrearsStatus);
            statusManager = new Status();
            AM_Status status = statusManager.GetStatusById(selectedArrearsStatus);
            if (status != null)
            {
                PopulateStatusReview(int.Parse(ddlArrearsStatus.SelectedValue), status);
                CheckNoticeParameters(status);
            }
            //PopulateStatusReview(int.Parse(ddlArrearsStatus.SelectedValue));
            //CheckFileUploadControl(selectedArrearsStatus);
            //CheckRecoveryAmount(selectedArrearsStatus);
            updpnlUpdateCase.Update();
        }

        #endregion

        #region"Txt Status Review Text Changed"

        /// <summary>
        /// This event invokes when the user changes the Stage(Status) Review Date.
        /// It is responsible for validating the date selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtStatusReview_TextChanged(object sender, EventArgs e)
        {
            ValidateDate(txtStatusReview.Text, "Stage Review");
        }

        #endregion

        #region"DDL Arrears Action Selected Index Changed"

        /// <summary>
        /// This event invokes when the user change the selected value in the Action drop down list.
        /// It populates the data in the controls accroding to the selected Action.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void ddlArrearsAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
            PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
            CheckSavedLetters();
            CheckDocumentListBox();
            updpnlUpdateCase.Update();
        }

        #endregion

        #region"Txt Action Review Text Changed"

        /// <summary>
        /// This event invokes when the user changes the date in the Action Review field.
        /// It is responsible for validating the selected date.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtActionReview_TextChanged(object sender, EventArgs e)
        {
            ValidateDate(txtActionReview.Text, "Action Review");
        }

        #endregion

        #region"Txt Recovery Amount Text Changed"

        /// <summary>
        /// This event invokes when the user changes the Recovery Amount(now Rent Balance or Gross Rent Balance in UK version).
        /// It is responsible for validating the amount.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtRecoveryAmount_TextChanged(object sender, EventArgs e)
        {
            if (ddlOutcome.SelectedItem.Text != "Close Case")
            {
                ValidateRecoveryAmount(txtRecoveryAmount.Text);
            }

        }

        #endregion

        #region"Txt Notice Issue Text Changed"

        /// <summary>
        /// This event invokes when the user changes or input the Notice Issue Date.
        /// It is responsible for validating the selected date.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtNoticeIssue_TextChanged(object sender, EventArgs e)
        {
            if (ValidateDate(txtNoticeIssue.Text, "Notice Issue"))
            {
                SetNoticeExpiryDate();
            }
        }

        #endregion

        #region"Set Notice Expiry Date"

        public void SetNoticeExpiryDate()
        {
            if (rowNoticeExpiry.Visible)
            {
                DateTime noticeIssuedate = DateOperations.ConvertStringToDate(txtNoticeIssue.Text);
                DateTime date = DateOperations.AddDate(11, "Months", noticeIssuedate);
                txtNoticeExpiry.Text = date.ToString("dd/MM/yyyy");
            }
        }

        #endregion

        #region"Txt Notice Expiry Text Changed"

        /// <summary>
        /// This event is invoked when the user changes or inputs the Notice Expiry Date.
        /// It is responsible for validating the selected date.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtNoticeExpiry_TextChanged(object sender, EventArgs e)
        {
            ValidateDate(txtNoticeExpiry.Text, "Notice Expiry");
        }

        #endregion

        #region"Txt Warrent Expiry Date Text Changed"

        /// <summary>
        /// This event is invoked when the user changes or inputs the Notice Expiry Date.
        /// It is responsible for validating the selected date.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtWarrantExpiry_TextChanged(object sender, EventArgs e)
        {
            ValidateDate(txtWarrantExpiryDate.Text, "Warrent Expiry Date");
        }

        #endregion

        #region"Txt Hearing Date Text Changed"

        /// <summary>
        /// This event is invoked when the user changes or inputs the Notice Expiry Date.
        /// It is responsible for validating the selected date.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtHearingDate_TextChanged(object sender, EventArgs e)
        {
            //ValidateDate(txtHearingDate.Text, "Hearing Date");
        }

        #endregion

        #region"Btn Remove Click"

        /// <summary>
        /// This event is invoked when the user wishes to remove any uploaded document 
        /// or attached standard letter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (lstDocuments.SelectedValue == "-1" || lstDocuments.SelectedValue == "")
                {
                    SetMessage(UserMessageConstants.selectOptionEditDocuments, true);
                    updpnlUpdateCase.Update();
                    return;
                }

                if (CheckNewStandardLetterId(lstDocuments.SelectedValue))
                {
                    lstDocuments.Items.RemoveAt(lstDocuments.SelectedIndex);
                    updpnlUpdateCase.Update();
                }
                else if (CheckStandardLetterId(lstDocuments.SelectedValue))
                {
                    string[] value = lstDocuments.SelectedValue.Split(';');
                    int docId = int.Parse(value[0]);
                    caseDetails = new CaseDetails();
                    caseDetails.DisableCaseStandardLetter(docId, base.GetCaseId());
                    lstDocuments.Items.Clear();
                    PopulateDocuments();
                    updpnlUpdateCase.Update();
                }
                else if (!Validation.CheckIntegerValue(lstDocuments.SelectedValue))
                {
                    GetListFromViewState();
                    arrayListDocumentName.Remove(lstDocuments.SelectedItem.Text);
                    lstDocuments.Items.RemoveAt(lstDocuments.SelectedIndex);
                    SetViewStateList();
                    updpnlUpdateCase.Update();
                }
                else
                {
                    int docId = int.Parse(lstDocuments.SelectedValue);
                    caseDetails = new CaseDetails();
                    caseDetails.DisableDocument(docId);
                    lstDocuments.Items.Clear();
                    PopulateDocuments();
                    updpnlUpdateCase.Update();
                }
                CheckSavedLetters();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadUpdateCase, true);
                }
            }
        }

        #endregion

        #region"Btn Upload Click"

        /// <summary>
        /// This event is invoked when the user wishes to upload any document 
        /// with the help of File Upload Control. The screen name of this control is "ADD".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            GetListFromViewState();
            UploadFile();
        }

        #endregion

        #region"btn Save Amends Click"

        /// <summary>
        /// This event is invoked when the user wishes to save the amends he has made to the case.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSaveAmends_Click(object sender, EventArgs e)
        {

            try
            {

                if (!Validate())
                {
                    return;
                }
                if (CheckPaymentPlanMandatory())
                {
                    if (!CheckPaymentPlan())
                    {
                        lblIgnoreError.Visible = false;
                        modalPopupPaymentPlanIgnoreReason.Show();
                    }
                    else
                    {
                        ViewState[ViewStateConstants.isPaymentPlanIgnoredPaymentPlan] = "false";
                        ViewState[ViewStateConstants.IgnoreReasonPaymentPlan] = string.Empty;
                        ShowModalPopup();

                    }
                }
                else
                {
                    ViewState[ViewStateConstants.isPaymentPlanIgnoredPaymentPlan] = "false";
                    ViewState[ViewStateConstants.IgnoreReasonPaymentPlan] = string.Empty;
                    ShowModalPopup();


                }




            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionInUpdateCase, true);
                }
            }
        }

        #endregion

        #region"Btn Action Save Click"

        /// <summary>
        /// This event is invoked when the user is inputting the Ignore reasons for the actions he has 
        /// ignored (if any) through the PopUp. When the user is inputting the last ignored actions reason the Save Button is 
        /// enabled and the user selects it to Submit his changes(Amends).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnActionSave_Click(object sender, EventArgs e)
        {
            if (txtActionDetail.Text.Equals(string.Empty))
            {
                if (Convert.ToInt32(ViewState[ViewStateConstants.ModalPopupPageUpdateCase]) == 0)
                {
                    lblActionError.Text = UserMessageConstants.ActionModalPopupErrorUpdateCase;
                    lblActionError.Visible = true;
                }
                else
                {
                    lblActionError.Text = UserMessageConstants.ActionModalPopupIgnoreErrorUpdateCase;
                    lblActionError.Visible = true;
                }
                modalPopupActionDetails.Show();
                return;
            }
            else
            {
                lblActionError.Visible = false;
                SetActionDataInArrayList(Convert.ToInt32(Session[ViewStateConstants.ignoredActionIdUpdateCase]), Convert.ToInt32(Session[ViewStateConstants.IgnoredStatusUpdateCase]));
                if (CaseUpdate())
                {
                    Response.Redirect(PathConstants.updateCaseRedirectPath, false);
                }
                else
                {
                    SetMessage(UserMessageConstants.exceptionInUpdateCase, true);
                    return;
                }
            }
        }

        #endregion

        #region"Btn Cancel Click"

        /// <summary>
        /// This events invokes when the user Wishes to Remove all the changes he has made.
        /// It will reset the page. All the information entered will be lost.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            GetListFromViewState();
            ResetPage();
            ResetMessage();
            arrayListDocumentName.Clear();
            ViewState.Remove(ViewStateConstants.DocumentsAttachedUpdateCase);
            updpnlUpdateCase.Update();
        }

        #endregion

        #region Supress Case Cancel Button

        /// <summary>
        /// Hidees the Suppress Case Popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnsupresscancel_Click(object sender, EventArgs e)
        {
            upmodalextendersupresscase.Hide();
        }

        #endregion

        #region Clearsupresscase fields

        /// <summary>
        /// Clears the Suppressed Case fileds in the Suppressed Case PopUp.
        /// </summary>

        private void ClearSupressCaseFields()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>");

            // Clear Immunization  Contact Fields
            sb.AppendLine("function ClearSupressCaseFields()");
            sb.AppendLine("{");

            sb.Append("control = document.getElementById('");
            sb.Append(this.txtsupressreason.ClientID);
            sb.AppendLine("');");
            sb.AppendLine("control.value = '';");

            sb.Append("control = document.getElementById('");
            sb.Append(this.txtreviewdate.ClientID);
            sb.AppendLine("');");
            sb.AppendLine("control.value = '';");

            sb.Append("control = document.getElementById('");
            sb.Append(this.lblsupresserrormsg.ClientID);
            sb.AppendLine("');");
            sb.AppendLine("control.value = '';");

            sb.AppendLine("}");
            sb.AppendLine("</script>");
            base.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClearSupressCaseFields", sb.ToString());
        }

        #endregion

        #region"btn Suppress Save Click"

        /// <summary>
        /// Saves the Suppressed Case reason and review date fin the database and Hides teh popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnsupresssave_Click(object sender, EventArgs e)
        {
            HistoryManager = new BusinessManager.Casemgmt.CaseHistory();
            try
            {
                if (txtsupressreason.Text != string.Empty && txtreviewdate.Text != string.Empty)
                {
                    if (Am.Ahv.Utilities.utitility_classes.DateOperations.isValidUkDate(txtreviewdate.Text))
                    {
                        CultureInfo cultEnGb = new CultureInfo("en-GB");
                        CultureInfo cultEnUs = new CultureInfo("en-US");
                        DateTime date = Convert.ToDateTime(txtreviewdate.Text, cultEnGb);
                        date = Convert.ToDateTime(date, cultEnUs);

                        DateTime nextdate = Am.Ahv.Utilities.utitility_classes.DateOperations.AddDate(3, "Months", DateTime.Now);

                        int i = DateTime.Compare(date, nextdate);

                        if (i <= 0)
                        {
                            Boolean flag = HistoryManager.SupressCase(base.GetCaseId(), txtsupressreason.Text, date, base.GetCurrentLoggedinUser());
                            if (flag)
                            {
                                upmodalextendersupresscase.Hide();
                                //ResetPage();
                                SetMessage(UserMessageConstants.suppressSuccessUpdateCase, false);
                            }
                        }
                        else
                        {
                            this.lblsupressmessage.Text = UserMessageConstants.SuppressReviewDateUpdateCase;
                            upmodalextendersupresscase.Show();
                        }
                    }
                    else
                    {
                        this.lblsupressmessage.Text = UserMessageConstants.SuppressReviewInvalidDateUpdateCase;
                        upmodalextendersupresscase.Show();
                    }
                }
                else
                {
                    this.lblsupressmessage.Text = UserMessageConstants.SuppressCaseRequiredFieldsUpdateCase;
                    upmodalextendersupresscase.Show();
                }
            }
            catch (FormatException format)
            {
                isException = true;
                ExceptionPolicy.HandleException(format, "Exception Policy");
                throw format;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    //upalertpanel_ModalPopupExtender.Hide();
                    upmodalextendersupresscase.Hide();
                    SetMessage(UserMessageConstants.CHsupressbtn, true);
                }
            }
        }

        #endregion

        #region"Btn Edit Click"

        /// <summary>
        /// This event is invoked when the user wishes to eidt any Standard Letter/Template.
        /// It saves the current state of teh page in the session and redirects to Create/Edit Letter Screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (lstDocuments.SelectedValue == "-1" || lstDocuments.SelectedValue == "")
            {
                SetMessage(UserMessageConstants.selectOptionEditDocuments, true);
                return;
            }
            else
            {
                int tenantid = -1;
                if (Session[SessionConstants.TenantId] != null)
                    tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                if (CheckStandardLetterId(lstDocuments.SelectedValue))
                {
                    string[] value = lstDocuments.SelectedValue.Split(';');
                    int id = new LetterResourceArea().GetSLetterByHistoryID(int.Parse(value[0])).StandardLetterId;
                    int letterHistoryId = int.Parse(value[0]);
                    base.RemoveCreateLetterCache();
                    AM_Case caseNew = GetCachedCase();
                           

                    Session.Add("CachedUpdateCase", caseNew);
                    SavedLetters = GetSavedLettersViewStateList();
                    if (SavedLetters != null)
                    {
                        //if (SavedLetters.Contains(id))
                        if (SavedLetters.Contains(letterHistoryId))
                        {
                            int historyId = GetStandardLetterHistoryId(letterHistoryId);
                            
                            if (historyId > 0)
                            {
                                Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + historyId.ToString() + "&TID=" + tenantid, false);
                            }
                            else
                            {
                                Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&TID=" + tenantid, false);
                            }
                        }
                        else
                        {
                            //int historyId = GetCaseStandardLetterHistoryId(id, base.GetCaseId());
                            int historyId = letterHistoryId;
                            if (historyId > 0)
                            {
                                Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + historyId.ToString() + "&TID=" + tenantid, false);
                            }
                            else
                            {
                                Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&TID=" + tenantid, false);
                            }
                        }
                    }
                    else
                    {
                        //int historyId = GetCaseStandardLetterHistoryId(id, base.GetCaseId());
                        int historyId = letterHistoryId;
                        if (historyId > 0)
                        {
                            Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + historyId.ToString() + "&TID=" + tenantid, false);
                        }
                        else
                        {
                            Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&TID=" + tenantid, false);
                        }
                    }
                }
                else if (CheckNewStandardLetterId(lstDocuments.SelectedValue))
                {
                    string[] value = lstDocuments.SelectedValue.Split(';');
                    //int id = int.Parse(value[0]);
                    int id = new LetterResourceArea().GetSLetterByHistoryID(int.Parse(value[0])).StandardLetterId;
                    int letterHistoryId = int.Parse(value[0]);
                    base.RemoveCreateLetterCache();
                    AM_Case caseNew = GetCachedCase();

                    Session.Add("CachedUpdateCase", caseNew);
                    SavedLetters = GetSavedLettersViewStateList();
                    if (SavedLetters != null)
                    {
                        if (SavedLetters.Contains(letterHistoryId))
                        {
                            int historyId = GetStandardLetterHistoryId(id, letterHistoryId);
                            //int historyId = letterHistoryId;
                            if (historyId > 0)
                            {
                                Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + historyId.ToString() + "&TID=" + tenantid, false);
                            }
                            else
                            {
                                Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&TID=" + tenantid, false);
                            }
                        }
                        else
                        {
                            Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + letterHistoryId.ToString() + "&TID=" + tenantid, false);
                        }
                    }
                    else
                    {
                        Response.Redirect(PathConstants.editLetterTemplate + "id=" + id + "&hId=" + letterHistoryId.ToString() + "&TID=" + tenantid, false);
                    }
                }
                else
                {
                    SetMessage(UserMessageConstants.selectStandardLettersEditDocuments, true);
                    return;
                }
            }
        }

        #endregion

        #region"Btn Next Click"

        /// <summary>
        /// This event is invoked when the user has ignored multiple actions
        /// and the popup is there to ask for the reason of all the actions ignored one by one.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnNext_Click(object sender, EventArgs e)
        {
            if (txtActionDetail.Text.Equals(string.Empty))
            {
                if (Convert.ToInt32(ViewState[ViewStateConstants.ModalPopupPageUpdateCase]) == 0)
                {
                    lblActionError.Text = UserMessageConstants.ActionModalPopupErrorUpdateCase;
                    lblActionError.Visible = true;
                }
                else
                {
                    lblActionError.Text = UserMessageConstants.ActionModalPopupIgnoreErrorUpdateCase;
                    lblActionError.Visible = true;
                }
            }
            else
            {
                lblActionError.Visible = false;
                SetActionDataInArrayList(Convert.ToInt32(Session[ViewStateConstants.ignoredActionIdUpdateCase]), Convert.ToInt32(Session[ViewStateConstants.IgnoredStatusUpdateCase]));
                IterateIgnoredActions();
            }
            modalPopupActionDetails.Show();
        }

        #endregion

        #region"Btn Action Cancel Click"

        /// <summary>
        /// This event is invoked when the user wishes to cancel and hide the popup 
        /// asking the user to input the reasons for the Ignored Actions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnActionCancel_Click(object sender, EventArgs e)
        {
            ResetActionDetails();
            modalPopupActionDetails.Hide();
        }

        #endregion

        #region"Btn Ignore Reason Cancel Click"

        /// <summary>
        /// This event is ignore when the user wishes to cancel the Popup asking the reason
        /// to ignore the payment plan against any action (on which it is mandatory).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnIgnoreReasonCancel_Click(object sender, EventArgs e)
        {
            txtIgnoreReason.Text = string.Empty;
            modalPopupPaymentPlanIgnoreReason.Hide();
        }

        #endregion

        #region"Btn Reason Save Click"

        /// <summary>
        /// This event is invoked when the user Saves the reason of the Ignored Payment Plan
        /// against any action(on which it is mandatory). It will save the reason in the view state and will 
        /// check if teh user has ignored some actions too. if so the action Ignore Popup will appear else it will save the
        /// amends in teh database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnReasonSave_Click(object sender, EventArgs e)
        {
            if (txtIgnoreReason.Text.Equals(string.Empty))
            {
                lblIgnoreError.Text = UserMessageConstants.EnterIgnoreReasonPaymentPlan;
                lblIgnoreError.Visible = true;
                modalPopupPaymentPlanIgnoreReason.Show();
            }
            else
            {
                ViewState[ViewStateConstants.isPaymentPlanIgnoredPaymentPlan] = "true";
                ViewState[ViewStateConstants.IgnoreReasonPaymentPlan] = txtIgnoreReason.Text;
                modalPopupPaymentPlanIgnoreReason.Hide();
                ShowModalPopup();
                updpnlUpdateCase.Update();
            }
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Load Case"

        /// <summary>
        /// This Function is responsible for loading the case from the database when the Page/User Control loads.
        /// It also save the some information in the session to check the Ignored Actions and Statge(Status).
        /// </summary>

        public void LoadCase()
        {
            try
            {
                Am.Ahv.BusinessManager.statusmgmt.Action actionMgr = new BusinessManager.statusmgmt.Action();
                Am.Ahv.BusinessManager.statusmgmt.Status statusMgr = new Status();
                caseDetails = new CaseDetails();
                AM_Case caseObj = caseDetails.GetCaseById(base.GetCaseId()); // caseDetails.GetCaseById(Convert.ToInt32(Session[SessionConstants.CaseId]));
                lblCaseInitiatorName.Text = caseDetails.GetInitiatedByName(caseObj.AM_Resource.EmployeeId);
                ddlCaseOfficer.SelectedValue = caseObj.CaseOfficer.ToString();
                ddlCaseManager.SelectedValue = caseObj.CaseManager.ToString();
                Session[ViewStateConstants.StatusIdUpdateCase] = caseObj.StatusId;
                Session[ViewStateConstants.StatusRecordedDateUpdateCase] = caseObj.StatusRecordedDate.Value.ToString("dd/MM/yyyy");
                Session[SessionConstants.ActionRecordedDateUpdateCase] = caseObj.ActionRecordedDate.Value.ToString("dd/MM/yyyy");

                SetStatusAndActionRanking(caseObj.ActionId, caseObj.StatusId);
                AM_Action action = actionMgr.GetNextAction(caseObj.ActionId, caseObj.StatusId);
                Session[SessionConstants.NextActionUpdateCase] = action;
                if (action != null)
                {
                    LoadArrearsActionBySelectedStatus(caseObj.StatusId);
                    ddlArrearsAction.SelectedValue = action.ActionId.ToString();
                    ddlArrearsStatus.SelectedValue = caseObj.StatusId.ToString();
                    PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
                    PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                    caseObj.AM_StatusReference.Load();
                    CheckNoticeParameters(caseObj.AM_Status);
                    //CheckFileUploadControl(caseObj.StatusId);
                    //CheckRecoveryAmount(caseObj.StatusId);
                    txtStatusReview.Text = caseObj.StatusReview.ToString("dd/MM/yyyy");
                }
                else
                {
                    AM_Status status = statusMgr.GetNextStatus(caseObj.StatusId);
                    if (status != null)
                    {
                        LoadArrearsActionBySelectedStatus(status.StatusId);
                        ddlArrearsStatus.SelectedValue = status.StatusId.ToString();
                        PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
                        PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                        CheckNoticeParameters(status);
                        //CheckFileUploadControl(status.StatusId);
                        PopulateStatusReview(status.StatusId, status);
                        //CheckRecoveryAmount(status.StatusId);
                    }
                    else
                    {
                        LoadArrearsActionBySelectedStatus(caseObj.StatusId);
                        ddlArrearsAction.SelectedValue = caseObj.ActionId.ToString();
                        ddlArrearsStatus.SelectedValue = caseObj.StatusId.ToString();
                        PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
                        PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                        caseObj.AM_StatusReference.Load();
                        CheckNoticeParameters(caseObj.AM_Status);
                        txtStatusReview.Text = caseObj.StatusReview.ToString("dd/MM/yyyy");
                        //CheckRecoveryAmount(caseObj.StatusId);
                    }

                }

                PopulateDocuments();
                //PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                PopulateRecoveryAmount();
                if (caseObj.NoticeExpiryDate == null)
                {
                    txtNoticeExpiry.Text = string.Empty;
                }
                else
                {
                    txtNoticeExpiry.Text = caseObj.NoticeExpiryDate.Value.ToString("dd/MM/yyyy");
                }
                if (caseObj.NoticeIssueDate == null)
                {
                    txtNoticeIssue.Text = string.Empty;
                }
                else
                {
                    txtNoticeIssue.Text = caseObj.NoticeIssueDate.Value.ToString("dd/MM/yyyy");
                }
                ddlOutcome.SelectedValue = caseObj.OutcomeLookupCodeId.ToString();
                txtReason.Text = caseObj.Reason;

                if (rowWarrantExpiry.Visible)
                {
                    if (caseObj.WarrantExpiryDate == null)
                    {
                        txtWarrantExpiryDate.Text = string.Empty;
                    }
                    else
                    {
                        txtWarrantExpiryDate.Text = caseObj.WarrantExpiryDate.Value.ToString("dd/MM/yyyy");
                    }
                }
                if (rowHearingDate.Visible)
                {
                    if (caseObj.HearingDate == null)
                    {
                        txtHearingDate.Text = string.Empty;
                    }
                    else
                    {
                        txtHearingDate.Text = caseObj.HearingDate.Value.ToString("dd/MM/yyyy");
                    }
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadUpdateCase, true);
                }
            }
        }

        #endregion

        #region"Load Case For Record Action"

        /// <summary>
        /// This function loads the case when the User is restricted to Save the selected Stage(Status) and Action
        /// against any case. This happens so when the user has been redirected from the PopUp appeared on Case History/Summary page.
        /// </summary>

        public void LoadCaseForActionRecord()
        {
            try
            {
                Am.Ahv.BusinessManager.statusmgmt.Action actionMgr = new BusinessManager.statusmgmt.Action();
                Am.Ahv.BusinessManager.statusmgmt.Status statusMgr = new Status();
                caseDetails = new CaseDetails();
                AM_Case caseObj = caseDetails.GetCaseById(base.GetCaseId()); // caseDetails.GetCaseById(Convert.ToInt32(Session[SessionConstants.CaseId]));
                lblCaseInitiatorName.Text = caseDetails.GetInitiatedByName(caseObj.AM_Resource.EmployeeId);
                ddlCaseOfficer.SelectedValue = caseObj.CaseOfficer.ToString();
                ddlCaseManager.SelectedValue = caseObj.CaseManager.ToString();
                Session[ViewStateConstants.StatusIdUpdateCase] = caseObj.StatusId;
                Session[ViewStateConstants.StatusRecordedDateUpdateCase] = caseObj.StatusRecordedDate.Value.ToString("dd/MM/yyyy");
                Session[SessionConstants.ActionRecordedDateUpdateCase] = caseObj.ActionRecordedDate.Value.ToString("dd/MM/yyyy");
                txtStatusReview.Text = caseObj.StatusReview.ToString("dd/MM/yyyy");
                AM_Action action = actionMgr.GetNextAction(caseObj.ActionId, caseObj.StatusId);
                Session[SessionConstants.NextActionUpdateCase] = action;
                if (Session[SessionConstants.NextStatusIdCaseSummary] != null)
                {
                    LoadArrearsActionBySelectedStatus(Convert.ToInt32(Session[SessionConstants.NextStatusIdCaseSummary]));
                    ddlArrearsStatus.SelectedValue = Convert.ToString(Session[SessionConstants.NextStatusIdCaseSummary]);
                    ddlArrearsAction.SelectedValue = Convert.ToString(Session[SessionConstants.NextActionIdCaseSummary]);
                    PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
                    PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                    //ddlArrearsStatus.Enabled = false;
                    //ddlArrearsAction.Enabled = false;
                }

                SetStatusAndActionRanking(caseObj.ActionId, caseObj.StatusId);
                //CheckRecoveryAmount(caseObj.StatusId);
                caseObj.AM_StatusReference.Load();
                CheckNoticeParameters(caseObj.AM_Status);
                PopulateDocuments();
                PopulateRecoveryAmount();
                //PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                if (caseObj.NoticeExpiryDate == null)
                {
                    txtNoticeExpiry.Text = string.Empty;
                }
                else
                {
                    txtNoticeExpiry.Text = caseObj.NoticeExpiryDate.Value.ToString("dd/MM/yyyy");
                }
                if (caseObj.NoticeIssueDate == null)
                {
                    txtNoticeIssue.Text = string.Empty;
                }
                else
                {
                    txtNoticeIssue.Text = caseObj.NoticeIssueDate.Value.ToString("dd/MM/yyyy");
                }
                ddlOutcome.SelectedValue = caseObj.OutcomeLookupCodeId.ToString();
                txtReason.Text = caseObj.Reason;
                if (rowWarrantExpiry.Visible)
                {
                    if (caseObj.WarrantExpiryDate == null)
                    {
                        txtWarrantExpiryDate.Text = string.Empty;
                    }
                    else
                    {
                        txtWarrantExpiryDate.Text = caseObj.WarrantExpiryDate.Value.ToString("dd/MM/yyyy");
                    }
                }
                if (rowHearingDate.Visible)
                {
                    if (caseObj.HearingDate == null)
                    {
                        txtHearingDate.Text = string.Empty;
                    }
                    else
                    {
                        txtHearingDate.Text = caseObj.HearingDate.Value.ToString("dd/MM/yyyy");
                    }
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadUpdateCase, true);
                }
            }
        }

        #endregion

        #region"Populate Documents"

        /// <summary>
        /// This method is responsible for Populating documents and standard letter/templates 
        /// against the case.
        /// </summary>

        public void PopulateDocuments()
        {
            try
            {
                caseDetails = new CaseDetails();
                int caseId = base.GetCaseId();
                lstDocuments.DataSource = caseDetails.GetCaseDocuments(caseId); //caseDetails.GetCaseDocuments(Convert.ToInt32(Session[SessionConstants.CaseId]));
                lstDocuments.DataTextField = "DocumentName";
                lstDocuments.DataValueField = "DocumentId";
                lstDocuments.DataBind();

                DataTable dt = caseDetails.GetCaseStandardLetterHistory(caseId);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lstDocuments.Items.Add(new ListItem(dt.Rows[i]["StandardLetter"].ToString(), dt.Rows[i]["StandardLetterHistoryId"].ToString() + ";SL_"));
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadUpdateCase, true);
                }
            }
        }

        #endregion

        #region"Populate Lookups"

        /// <summary>
        /// Populate the Default data on the Update Case Screen. Above the data of the specific case.
        /// </summary>

        public void PopulateLookups()
        {
            try
            {
                userManager = new Users();
                statusManager = new Status();

                ddlCaseOfficer.DataSource = userManager.GetActivePersons("Case Officer");
                ddlCaseOfficer.DataTextField = "name";
                ddlCaseOfficer.DataValueField = "ResourceId";
                ddlCaseOfficer.DataBind();
                ddlCaseOfficer.Items.Add(new ListItem("Please Select", "-1"));
                ddlCaseOfficer.SelectedValue = "-1";

                ddlCaseManager.DataSource = userManager.GetActivePersons("Case Manager");
                ddlCaseManager.DataTextField = "name";
                ddlCaseManager.DataValueField = "ResourceId";
                ddlCaseManager.DataBind();
                ddlCaseManager.Items.Add(new ListItem("Please Select", "-1"));
                ddlCaseManager.SelectedValue = "-1";

                PopulateStatus();
                PopulateStatusReview(int.Parse(ddlArrearsStatus.SelectedValue), null);
                this.LoadArrearsActionBySelectedStatus(int.Parse(ddlArrearsStatus.SelectedValue));
                PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
                PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                PopulateOutcomes();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
        }

        #endregion

        #region"Populate Outcomes"

        /// <summary>
        /// It is responsible for populating the Outcomes drop down list. 
        /// </summary>

        public void PopulateOutcomes()
        {
            try
            {
                lookupManager = new Lookup();
                ddlOutcome.DataSource = lookupManager.getLookupListings("Outcome");
                ddlOutcome.DataTextField = "CodeName";
                ddlOutcome.DataValueField = "LookupCodeId";
                ddlOutcome.DataBind();
                ddlOutcome.Items.Add(new ListItem("Please Select", "-1"));
                ddlOutcome.SelectedValue = "-1";
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
        }

        #endregion

        #region"Populate Status"

        /// <summary>
        /// It is responsible for populating the Stage(Status).
        /// </summary>

        public void PopulateStatus()
        {
            try
            {
                statusManager = new Status();
                List<Am.Ahv.Entities.AM_Status> statusList = statusManager.GetAllStatus();
                Am.Ahv.Entities.AM_Status firstStatus = statusList[0];
                ddlArrearsStatus.DataSource = statusManager.GetAllStatus();
                ddlArrearsStatus.DataTextField = "Title";
                ddlArrearsStatus.DataValueField = "StatusId";
                ddlArrearsStatus.DataBind();
                CheckFileUploadControl(int.Parse(ddlArrearsStatus.SelectedValue));
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
        }

        #endregion

        #region"Populate Actions"

        /// <summary>
        /// It is responsible for Populating the Actions against a Specific Stage(Status).
        /// </summary>
        /// <param name="statusId"></param>

        public void PopulateActions(int statusId)
        {
            try
            {
                Am.Ahv.BusinessManager.statusmgmt.Action action = new Am.Ahv.BusinessManager.statusmgmt.Action();
                List<AM_Action> listAction = action.GetActionByStatus(statusId);
                if (listAction != null)
                {
                    ddlArrearsAction.Items.Clear();
                    ddlArrearsAction.DataSource = listAction;
                    ddlArrearsAction.DataTextField = "Title";
                    ddlArrearsAction.DataValueField = "ActionId";
                    ddlArrearsAction.SelectedValue = listAction[0].ActionId.ToString();
                    ddlArrearsAction.DataBind();
                }
                else
                {
                    ddlArrearsAction.Items.Clear();
                    ddlArrearsAction.Items.Add(new ListItem("Please Select", "-1"));
                    ddlArrearsAction.SelectedValue = "-1";
                    txtActionReview.Text = DateTime.Now.ToString("dd/MM/yyyy");
                }
            }
            catch (NullReferenceException nullException)
            {
                throw nullException;
            }
            catch (EntitySqlException entitySqlException)
            {
                throw entitySqlException;
            }
            catch (EntityException entityException)
            {
                throw entityException;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        #endregion

        #region"Load Arrears Action By Selected Status"

        /// <summary>
        /// This function is responsible for populating actions against a Stage(Status)
        /// and all other data against an action.
        /// </summary>
        /// <param name="arrearsStatusId">Stage Id(Status Id)</param>

        private void LoadArrearsActionBySelectedStatus(int arrearsStatusId)
        {
            try
            {
                PopulateActions(arrearsStatusId);
                //PopulateActionsReview(int.Parse(ddlArrearsAction.SelectedValue));
                //PopulateLettersByActionId(int.Parse(ddlArrearsAction.SelectedValue));
                CheckSavedLetters();
                CheckDocumentListBox();

            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
        }

        #endregion

        #region"Populate Status Review"

        /// <summary>
        /// This function is responsible for Populating Stage Review date(Status Review Date) against
        /// a selected Stage(Status).
        /// </summary>
        /// <param name="statusId"></param>

        public void PopulateStatusReview(int statusId, AM_Status status)
        {
            try
            {
                statusManager = new Status();
                DateTime reviewDate = new DateTime();
                string recordedDateString = Convert.ToString(Session[ViewStateConstants.StatusRecordedDateUpdateCase]);
                if (recordedDateString == string.Empty)
                {
                    return;
                }
                DateTime recordedDate = DateOperations.ConvertStringToDate(recordedDateString);
                if (status == null)
                {
                    status = statusManager.GetStatusReviewPeriod(statusId);
                }
                AM_LookupCode lookupList = status.AM_LookupCode;
                if (lookupList != null)
                {
                    reviewDate = DateOperations.AddDate(status.ReviewPeriod, lookupList.CodeName, recordedDate);
                    txtStatusReview.Text = reviewDate.ToString("dd/MM/yyyy");
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }

        }

        #endregion

        #region"Populate Actions Review"

        /// <summary>
        /// This function is responsible for populating Action Review Date against a Selected Action.
        /// </summary>
        /// <param name="actionId"></param>

        public void PopulateActionsReview(int actionId)
        {
            try
            {
                if (actionId != -1)
                {
                    actionManager = new BusinessManager.statusmgmt.Action();
                    DateTime reviewdate = new DateTime();
                    string recordedDateString = Convert.ToString(Session[SessionConstants.ActionRecordedDateUpdateCase]);
                    if (recordedDateString == string.Empty)
                    {
                        return;
                    }
                    DateTime recordedDate = DateOperations.ConvertStringToDate(recordedDateString);
                    AM_Action action = actionManager.GetActionReview(actionId);
                    if (action != null)
                    {
                        AM_LookupCode lookup = action.AM_LookupCode1;
                        if (lookup != null)
                        {
                            reviewdate = DateOperations.AddDate(action.RecommendedFollowupPeriod, lookup.CodeName, recordedDate);
                            txtActionReview.Text = reviewdate.ToString("dd/MM/yyyy");
                        }
                    }
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }

        }

        #endregion

        #region"Populate Recovery Amount"

        /// <summary>
        /// This method is responsible for Calculating or Populating Recovery Amount.
        /// </summary>

        public void PopulateRecoveryAmount()
        {
            txtRecoveryAmount.Text = Convert.ToString(CalculateRecoveryAmount());
        }

        #endregion

        #region"Populate Letters By Action Id"

        /// <summary>
        /// This method id responsible for populating the Standard Letters/Templates
        /// against a selected Action.
        /// </summary>
        /// <param name="actionId"></param>

        public void PopulateLettersByActionId(int actionId)
        {
            try
            {
                PopulateDocuments();
                if (actionId != -1)
                {
                    //LetterResourceArea letters = new LetterResourceArea();
                    //DataTable dt = letters.GetLettersByActionIdDataTable(actionId);

                    //if (dt.Rows.Count > 0 && dt != null)
                    //{
                    //    for (int i = 0; i < dt.Rows.Count; i++)
                    //    {
                    //        lstDocuments.Items.Add(new ListItem(dt.Rows[i]["Title"].ToString(), dt.Rows[i]["StandardLetterHistoryId"].ToString() + ";NewSL_"));
                    //    }
                    //    AddDocumentsFromArrayList();
                    //}
                    //else
                    //{
                    //    // btnStandardLetter.Enabled = false;
                    //}
                    LetterResourceArea letters = new LetterResourceArea();
                    List<AM_StandardLetterHistory> list = new List<AM_StandardLetterHistory>();
                    list = letters.GetLettersByActionId(actionId);
                    lstDocuments.Items.Clear();
                    if (list != null && list.Count > 0)
                    {
                        lstDocuments.DataSource = list;
                        lstDocuments.DataTextField = "Title";
                        //lstDocuments.DataValueField = "StandardLetterId";
                        lstDocuments.DataValueField = "StandardLetterHistoryId";
                        lstDocuments.DataBind();
                    }
                    else
                    {
                        //btnStandardLetter.Enabled = false;
                    }
                }
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingDocuments, true);
                }
            }
        }

        #endregion

        #region"Populate Case From Cache"

        /// <summary>
        /// This method is responsible for populating Case from Session. First time it was poulating 
        /// the Case from Cache but after few updates Session replaced Cache.
        /// </summary>

        public void PopulateCaseFromCache()
        {
            AM_Case caseCached = new AM_Case();
            if (Session["CachedUpdateCase"] == null)
            {
                Response.Redirect(PathConstants.caseList + "?cmd=cexpire", false);
            }
            else
            {
                caseCached = Session["CachedUpdateCase"] as AM_Case;
                ddlArrearsStatus.SelectedValue = caseCached.StatusId.ToString();
                ddlCaseManager.SelectedValue = caseCached.CaseManager.ToString();
                ddlCaseOfficer.SelectedValue = caseCached.CaseOfficer.ToString();
                txtStatusReview.Text = caseCached.StatusReview.ToString("dd/MM/yyyy");
                PopulateActions(caseCached.StatusId);
                ddlArrearsAction.SelectedValue = caseCached.ActionId.ToString();
                txtActionReview.Text = caseCached.ActionReviewDate.ToString("dd/MM/yyyy");
                txtNotes.Text = caseCached.Notes;
                txtRecoveryAmount.Text = caseCached.RecoveryAmount.ToString();
                if (caseCached.NoticeIssueDate != null)
                {
                    txtNoticeIssue.Text = caseCached.NoticeIssueDate.Value.ToString("dd/MM/yyyy");
                }
                if (caseCached.NoticeExpiryDate != null)
                {
                    txtNoticeExpiry.Text = caseCached.NoticeExpiryDate.Value.ToString("dd/MM/yyyy");
                }
                txtNotes.Text = caseCached.Notes;
                ddlOutcome.SelectedValue = caseCached.OutcomeLookupCodeId.ToString();
                txtReason.Text = caseCached.Reason;
                //Updated by :Umair
                //Update Date:24 Nov 2011
                statusManager = new Status();
                AM_Status status = statusManager.GetStatusById(caseCached.StatusId);
                if (status != null)
                {
                    CheckNoticeParameters(status);
                }
                //update end
            }
        }

        #endregion

        #region"Populate Letters"

        /// <summary>
        /// This function is responsible for Poulating letter which have been attached
        /// from Letter Resource Area and also from the session(if exists).
        /// </summary>
        /// <param name="letterId">Standard Letter Id</param>

        public void PopulateLetters(int letterId)
        {
            try
            {
                LetterResourceArea lresource = new LetterResourceArea();
                DataTable listcollection = new DataTable();
                listcollection.Columns.Add("StandardLetterHistoryId");
                listcollection.Columns.Add("Title");
                if (Session[SessionConstants.StandardLetterListUpdateCase] == null)
                {
                    Response.Redirect(PathConstants.caseList + "?cmd=cexpire", false);
                }
                else if (!Convert.ToString(Session[SessionConstants.StandardLetterListUpdateCase]).Equals("No data"))
                {
                    listcollection = Session[SessionConstants.StandardLetterListUpdateCase] as DataTable;
                }
                if (letterId != 0)
                {
                    //ViewState[ViewStateConstants.LetterIdUpdateCase] = letterId;
                    ViewState[ViewStateConstants.LetterIdUpdateCase] = lresource.GetSLetterHistoryBySLetterID(letterId).StandardLetterHistoryId;
                    AM_StandardLetterHistory Standardletters = lresource.GetSLetterHistoryBySLetterID(letterId);
                    lstDocuments.Items.Clear();
                    lstDocuments.DataSource = listcollection;
                    lstDocuments.DataTextField = "Title";
                    lstDocuments.DataValueField = "StandardLetterHistoryId";
                    lstDocuments.DataBind();
                    lstDocuments.Items.Add(new ListItem(Standardletters.Title, Standardletters.StandardLetterHistoryId + ";NewSL_"));
                }
                else
                {
                    lstDocuments.Items.Clear();
                    lstDocuments.DataSource = listcollection;
                    lstDocuments.DataTextField = "Title";
                    lstDocuments.DataValueField = "StandardLetterHistoryId";
                    lstDocuments.DataBind();
                }
                if (Session[SessionConstants.DocumentsArrayListUpdateCase] == null)
                {
                    Response.Redirect(PathConstants.caseList + "?cmd=cexpire", false);
                }
                else if (!Convert.ToString(Session[SessionConstants.DocumentsArrayListUpdateCase]).Equals("No data"))
                {
                    arrayListDocumentName = Session[SessionConstants.DocumentsArrayListUpdateCase] as ArrayList;
                    SetViewStateList();
                }
                if (Session[SessionConstants.SavedStandardLetterListUpdateCase] == null)
                {
                    Response.Redirect(PathConstants.caseList + "?cmd=cexpire", false);
                }
                else if (!Convert.ToString(Session[SessionConstants.SavedStandardLetterListUpdateCase]).Equals("No data"))
                {
                    SavedLetters = Session[SessionConstants.SavedStandardLetterListUpdateCase] as ArrayList;
                    SetSavedLettersViewStateList();
                }
                for (int i = 0; i < arrayListDocumentName.Count; i++)
                {
                    lstDocuments.Items.Add(arrayListDocumentName[i].ToString());
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Constructor"
        /// <summary>
        /// Constructor of Update Case
        /// </summary>
        public UpdateCase()
        {
            arrayListDocumentName = new ArrayList();
        }

        #endregion

        #region"Is Suppressed"

        /// <summary>
        /// This function is responsible for checking whether the Suppressed Case button should be enabled or disabled.
        /// </summary>

        private void Isupressed()
        {
            try
            {
                CaseHistory Manager = new BusinessManager.Casemgmt.CaseHistory();
                if (Manager.IsSuppressed(base.GetCaseId()))
                {
                    btnSuppressedCase.Enabled = true;
                }
                else if (Manager.CheckSuppress(base.GetCaseId()))
                {
                    btnSuppressedCase.Enabled = false;
                }
                else
                {
                    btnSuppressedCase.Enabled = true;
                }
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionIsSuppressUpdateCase, true);
                }
            }
        }

        #endregion

        #region"Validate"

        /// <summary>
        /// this function is responsible for validating all the mandatory options and data in other options.
        /// </summary>
        /// <returns></returns>

        public bool Validate()
        {
            if (ddlCaseManager.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectCaseManagerUpdateCase, true);
                return false;
            }
            else if (ddlCaseOfficer.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectCaseOfficerUpdateCase, true);
                return false;
            }
            else if (!ValidateDate(txtStatusReview.Text, "Stage Review"))
            {
                return false;
            }
            else if (!ValidateDate(txtActionReview.Text, "Action Review"))
            {
                return false;
            }
            else if (ddlArrearsAction.SelectedValue == "-1")
            {
                SetMessage(UserMessageConstants.selectActionOpenCase, true);
                return false;
            }
            else if (ddlOutcome.SelectedItem.Text != "Close Case" && !ValidateRecoveryAmount(txtRecoveryAmount.Text) && txtRecoveryAmount.Enabled)
            {
                return false;
            }
            else if (txtStatusReview.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.enterStatusReviewUpdateCase, true);
                return false;
            }
            else if (txtActionReview.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.enterActionReviewUpdateCase, true);
                return false;
            }
            else if (txtNotes.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.enterNotesUpdateCase, true);
                return false;
            }
            else if (rowNoticeIssue.Visible)
            {
                if (txtNoticeIssue.Text.Equals(string.Empty))
                {
                    SetMessage(UserMessageConstants.enterNoticeIssueUpdateCase, true);
                    return false;
                }
                //else if (!ValidateDate(txtNoticeIssue.Text, "Notice Issue"))
                // {
                //    return false;
                //}
            }
            else if (rowNoticeExpiry.Visible)
            {
                if (txtNoticeExpiry.Text.Equals(string.Empty))
                {
                    SetMessage(UserMessageConstants.enterNoticeExpiryUpdateCase, true);
                    return false;
                }
                else if (!ValidateDate(txtNoticeExpiry.Text, "Notice Expiry"))
                {
                    return false;
                }
            }
            //else if (txtNotes.Text.Length > 1000)
            //{
            //    SetMessage(UserMessageConstants.txtNotesUpdateCase, true);
            //    return false;
            //}
            return true;
        }

        #endregion

        #region"Check Standard Letter Id"

        /// <summary>
        /// This function is responsible for checking whether the value is of Standard Letter or not?
        /// </summary>
        /// <param name="value">Value</param>
        /// <returns> bool </returns>

        public bool CheckStandardLetterId(string value)
        {
            if (value.Contains(";SL_"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Check New Standard Letter Id"

        /// <summary>
        /// This function is responsible for checking whether the value is of New Standard Letter or not?
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>

        public bool CheckNewStandardLetterId(string value)
        {
            if (value.Contains(";NewSL_"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Validate File"

        /// <summary>
        /// This function is responsible for validating file.
        /// If the file has allowed extension or not?
        /// </summary>
        /// <returns></returns>

        public bool ValidateFile()
        {
            string fileExt = Path.GetExtension(FlUpload.FileName);
            int size = Convert.ToInt32(ConfigurationManager.AppSettings["FileSize"]);
            if (!FileOperations.CheckFileExtension(FlUpload.FileName))
            {
                SetMessage(UserMessageConstants.invalidImageType, true);
                return false;
            }
            else
                return true;
        }

        #endregion

        #region"Case Update"

        /// <summary>
        /// This method is responsible for Setting all the updated fields and commiting the changes in the database.
        /// </summary>
        /// <returns></returns>

        public bool CaseUpdate()
        {
            try
            {
                AM_Case caseObj = new AM_Case();
                AM_CaseHistory caseHistory = new AM_CaseHistory();
                statusManager = new Status();
                actionManager = new BusinessManager.statusmgmt.Action();
                caseDetails = new CaseDetails();
                int actionHistoryId = 0;
                int statusHistoryId = 0;
                CultureInfo info = new CultureInfo("en-GB");

                caseObj.CaseOfficer = int.Parse(ddlCaseOfficer.SelectedValue);
                caseHistory.CaseOfficer = int.Parse(ddlCaseOfficer.SelectedValue);

                caseObj.TenancyId = base.GetTenantId();
                caseHistory.TennantId = base.GetTenantId();

                caseObj.RentBalance = Convert.ToDouble(base.GetRentBalance());
                caseHistory.RentBalance = Convert.ToDouble(base.GetRentBalance());

                caseObj.CaseManager = int.Parse(ddlCaseManager.SelectedValue);
                caseHistory.CaseManager = int.Parse(ddlCaseManager.SelectedValue);

                caseObj.StatusId = int.Parse(ddlArrearsStatus.SelectedValue);
                caseHistory.StatusId = int.Parse(ddlArrearsStatus.SelectedValue);

                statusHistoryId = statusManager.GetStatusHistoryIdByStatus(int.Parse(ddlArrearsStatus.SelectedValue));
                caseObj.StatusHistoryId = statusHistoryId;
                caseHistory.StatusHistoryId = statusHistoryId;

                if (IsNewStatusRecorded())
                {
                    caseObj.StatusRecordedDate = DateTime.Now;
                    caseHistory.StatusRecordedDate = DateTime.Now;
                }
                else
                {
                    if (Session[ViewStateConstants.StatusRecordedDateUpdateCase] != null)
                    {
                        caseObj.StatusRecordedDate = DateOperations.ConvertStringToDate(Session[ViewStateConstants.StatusRecordedDateUpdateCase].ToString());
                        caseHistory.StatusRecordedDate = DateOperations.ConvertStringToDate(Session[ViewStateConstants.StatusRecordedDateUpdateCase].ToString());
                    }
                    else
                    {
                        caseObj.StatusRecordedDate = DateTime.Now;
                        caseHistory.StatusRecordedDate = DateTime.Now;
                    }
                }

                caseObj.StatusReview = DateOperations.GetCulturedDateTime(txtStatusReview.Text);
                caseHistory.StatusReview = DateOperations.GetCulturedDateTime(txtStatusReview.Text);

                caseObj.ActionId = int.Parse(ddlArrearsAction.SelectedValue);
                caseHistory.ActionId = int.Parse(ddlArrearsAction.SelectedValue);

                actionHistoryId = actionManager.GetActionHistoryByActionId(int.Parse(ddlArrearsAction.SelectedValue));

                caseObj.ActionHistoryId = actionHistoryId;
                caseHistory.ActionHistoryId = actionHistoryId;

                caseObj.ActionReviewDate = DateOperations.GetCulturedDateTime(txtActionReview.Text);
                caseHistory.ActionReviewDate = DateOperations.GetCulturedDateTime(txtActionReview.Text);

                caseObj.ActionRecordedDate = DateTime.Now;
                caseHistory.ActionRecordedDate = DateTime.Now;

                if (txtRecoveryAmount.Enabled)
                {
                    caseObj.RecoveryAmount = Convert.ToDouble(txtRecoveryAmount.Text);
                    caseHistory.RecoveryAmount = Convert.ToDouble(txtRecoveryAmount.Text);
                }

                if (!txtNoticeIssue.Text.Equals(string.Empty))
                {
                    caseObj.NoticeIssueDate = DateOperations.GetCulturedDateTime(txtNoticeIssue.Text);
                    caseHistory.NoticeIssueDate = DateOperations.GetCulturedDateTime(txtNoticeIssue.Text);
                }

                if (!txtNoticeExpiry.Text.Equals(string.Empty))
                {
                    caseObj.NoticeExpiryDate = DateOperations.GetCulturedDateTime(txtNoticeExpiry.Text);
                    caseHistory.NoticeExpiryDate = DateOperations.GetCulturedDateTime(txtNoticeExpiry.Text);
                }

                if (!txtWarrantExpiryDate.Text.Equals(string.Empty))
                {
                    caseObj.WarrantExpiryDate = DateOperations.GetCulturedDateTime(txtWarrantExpiryDate.Text);
                    caseHistory.WarrantExpiryDate = DateOperations.GetCulturedDateTime(txtWarrantExpiryDate.Text);
                }

                if (!txtHearingDate.Text.Equals(string.Empty))
                {
                    caseObj.HearingDate = DateOperations.GetCulturedDateTime(txtHearingDate.Text);
                    caseHistory.HearingDate = DateOperations.GetCulturedDateTime(txtHearingDate.Text);
                }

                caseObj.Notes = txtNotes.Text;
                caseHistory.Notes = txtNotes.Text;

                caseObj.Reason = txtReason.Text;
                caseHistory.Reason = txtReason.Text;

                caseObj.IsSuppressed = false;
                caseHistory.IsSuppressed = false;


                if (ddlOutcome.SelectedValue != "-1")
                {
                    if (ddlOutcome.SelectedItem.Text == "Close Case")
                    {
                        caseObj.IsActive = false;
                        caseHistory.IsActive = false;

                        caseObj.OutcomeLookupCodeId = int.Parse(ddlOutcome.SelectedValue);
                        caseHistory.OutcomeLookupCodeId = int.Parse(ddlOutcome.SelectedValue);
                    }
                    else
                    {
                        caseObj.IsActive = true;
                        caseHistory.IsActive = true;

                        caseObj.OutcomeLookupCodeId = int.Parse(ddlOutcome.SelectedValue);
                        caseHistory.OutcomeLookupCodeId = int.Parse(ddlOutcome.SelectedValue);
                    }
                }
                else
                {
                    caseObj.IsActive = true;
                    caseHistory.IsActive = true;
                }

                if (lstDocuments.Items.Count > 0)
                {
                    if (CheckDocumentAttached())
                    {
                        if (CheckNewStandardLetterAttached())
                        {
                            caseObj.IsDocumentAttached = true;
                            caseHistory.IsDocumentAttached = true;
                        }
                        else
                        {
                            caseObj.IsDocumentAttached = true;
                            caseHistory.IsDocumentAttached = false;
                        }
                    }
                    else
                    {
                        caseObj.IsDocumentAttached = false;
                        caseHistory.IsDocumentAttached = false;
                    }

                    if (CheckDocumentUpload())
                    {
                        if (CheckNewDocumentUpload())
                        {
                            caseObj.IsDocumentUpload = true;
                            caseHistory.IsDocumentUpload = true;
                        }
                        else
                        {
                            caseObj.IsDocumentUpload = true;
                            caseHistory.IsDocumentUpload = false;
                        }
                    }
                    else
                    {
                        caseObj.IsDocumentUpload = false;
                        caseHistory.IsDocumentUpload = false;
                    }
                }
                else
                {
                    caseObj.IsDocumentAttached = false;
                    caseHistory.IsDocumentAttached = false;

                    caseObj.IsDocumentUpload = false;
                    caseHistory.IsDocumentUpload = false;
                }

                caseObj.ModifiedBy = base.GetCurrentLoggedinUser();
                caseHistory.ModifiedBy = base.GetCurrentLoggedinUser();

                caseObj.ModifiedDate = DateTime.Now;
                caseHistory.ModifiedDate = DateTime.Now;

                caseHistory.InitiatedById = base.GetCurrentLoggedinUser();

                if (Convert.ToString(ViewState[ViewStateConstants.isPaymentPlanIgnoredPaymentPlan]).Equals("true"))
                {
                    caseObj.IsPaymentPlanIgnored = true;
                    caseHistory.IsPaymentPlanIgnored = true;

                    caseObj.PaymentPlanIgnoreReason = Convert.ToString(ViewState[ViewStateConstants.IgnoreReasonPaymentPlan]);
                    caseHistory.PaymentPlanIgnoreReason = Convert.ToString(ViewState[ViewStateConstants.IgnoreReasonPaymentPlan]);
                }
                else
                {
                    caseObj.IsPaymentPlanIgnored = false;
                    caseHistory.IsPaymentPlanIgnored = false;

                    caseObj.PaymentPlanIgnoreReason = string.Empty;
                    caseHistory.PaymentPlanIgnoreReason = string.Empty;
                }

                caseObj.IsPaymentPlan = CheckPaymentPlan();
                caseHistory.IsPaymentPlan = false;// caseObj.IsPaymentPlan;                

                if (caseObj.IsPaymentPlan.Value)
                {
                    Payments paymentManager = new Payments();
                    AM_PaymentPlan paymentPlan = paymentManager.GetPaymentPlan(base.GetTenantId());
                    if (paymentPlan != null)
                    {
                        AM_PaymentPlanHistory paymentPlanHistory = paymentManager.GetPaymentPlanHistory(paymentPlan.PaymentPlanId);
                        if (paymentPlanHistory != null)
                        {
                            caseObj.PaymentPlanId = paymentPlan.PaymentPlanId;
                            caseHistory.PaymentPlanId = paymentPlan.PaymentPlanId;

                            caseObj.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                            caseHistory.PaymentPlanHistoryId = paymentPlanHistory.PaymentPlanHistoryId;
                        }
                    }
                }

                GetListFromViewState();
                List<AM_StandardLetterHistory> st = GetNewStandardLetterList();
                bool flag = false;
                ActionData = ViewState[ViewStateConstants.ActionDataArrayListUpdateCase] as DataTable;

                //set rent balance, market rent and today's date
                this.setRbMrDate();

                if (arrayListDocumentName != null && arrayListDocumentName.Count > 0)
                {
                    flag = caseDetails.UpdateCase(base.GetCaseId(), caseObj, caseHistory, arrayListDocumentName, st, ActionData, oldSL, this.marketRent, this.rentBalance, this.todayDate, this.rentAmount);
                }
                else
                {
                    flag = caseDetails.UpdateCase(base.GetCaseId(), caseObj, caseHistory, st, ActionData, oldSL, this.marketRent, this.rentBalance, this.todayDate, this.rentAmount);
                }
                if (!flag)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (TransactionAbortedException transactionaborted)
            {
                IsException = true;
                ExceptionPolicy.HandleException(transactionaborted, "Exception Policy");
            }
            catch (TransactionException transactionexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(transactionexception, "Exception Policy");
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionInUpdateCase, true);
                }
            }
            return false;
        }

        #endregion

        #region set Rb Mr Date

        /// <summary>
        /// This function sets the rent balance, market rent and today's date, assuming that new case object is filled with entries
        /// </summary>
        /// <param name="newCase">object of new case</param>
        private void setRbMrDate()
        {
            PageBase pageBase = new PageBase();
            this.rentBalance = Convert.ToDouble(base.GetRentBalance());
            this.todayDate = DateTime.Now;
            this.rentBalance = Convert.ToDouble(base.GetRentBalance());
            this.rentAmount = Convert.ToDouble(pageBase.GetWeeklyRentAmount());

            if (Session[SessionConstants.TenantId] != null && Session[SessionConstants.Rentbalance] != null)
            {
                LetterResourceArea letterarea = new LetterResourceArea();

                int tenantid = int.Parse(Session[SessionConstants.TenantId].ToString());
                this.marketRent = letterarea.GetMarketRent(tenantid);

            }
        }
        #endregion

        #region"Check Notice Parameters"

        public void CheckNoticeParameters(AM_Status status)
        {
            if (status.IsRecoveryAmount != null)
            {
                rowRecoverAmount.Visible = status.IsRecoveryAmount.Value;
            }
            else
            {
                rowRecoverAmount.Visible = false;
            }
            if (status.IsWarrantExpiryDate != null)
            {
                rowWarrantExpiry.Visible = status.IsWarrantExpiryDate.Value;
            }
            else
            {
                rowWarrantExpiry.Visible = false;
            }
            if (status.IsNoticeIssueDate != null)
            {
                rowNoticeIssue.Visible = status.IsNoticeIssueDate.Value;
            }
            else
            {
                rowNoticeIssue.Visible = false;
            }
            if (status.IsNoticeExpiryDate != null)
            {
                rowNoticeExpiry.Visible = status.IsNoticeExpiryDate.Value;
            }
            else
            {
                rowNoticeExpiry.Visible = false;
            }
            if (status.IsHearingDate != null)
            {
                rowHearingDate.Visible = status.IsHearingDate.Value;
            }
            else
            {
                rowHearingDate.Visible = false;
            }
        }

        #endregion

        #region"Check Session Values"

        /// <summary>
        /// This function is responsible for checking whether the mandatory Session values exist or not?
        /// </summary>
        /// <returns></returns>

        public bool CheckSessionValues()
        {
            if (Convert.ToString(base.GetCaseId()).Equals(string.Empty))
            {
                return false;
            }
            else if (Convert.ToString(base.GetTenantId()).Equals(string.Empty))
            {
                return false;
            }
            else if (Convert.ToString(base.GetWeeklyRentAmount()).Equals(string.Empty))
            {
                return false;
            }
            else if (Convert.ToString(base.GetRentBalance()).Equals(string.Empty))
            {
                return false;
            }
            return true;
        }

        #endregion

        #region"Calculate Recover Amount"

        /// <summary>
        /// this method is responsible for calculating Recovery Amount.
        /// Before Updates teh Reovery Amount Calculation was used but after updates the Rent Balance is used
        /// instead of Recovery Amount.
        /// </summary>
        /// <returns></returns>

        public double CalculateRecoveryAmount()
        {
            try
            {
                //double weeklyRent = base.GetWeeklyRentAmount();
                double rentBalance = base.GetRentBalance();
                //double totalBalance = RentBalance.CalculateRecoveryAmount(rentBalance, weeklyRent);
                //return totalBalance;

                return rentBalance;
            }
            catch (DivideByZeroException divide)
            {
                IsException = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionPopulatingLookups, true);
                }
            }
            return 0.0;
        }

        #endregion

        #region"Validate Recovery Amount"

        /// <summary>
        /// This method is responsible for calculating whether the Recovery Amount 
        /// is valid or not?
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>

        public bool ValidateRecoveryAmount(string value)
        {
            Regex rgx = new Regex(RegularExpressionConstants.AmountExp);

            if (!rgx.IsMatch(value))
            {
                SetMessage(UserMessageConstants.paymentValidAmount, true);
                return false;
            }
            else if (Convert.ToDouble(value) == 0.0)
            {
                SetMessage(UserMessageConstants.paymentValidAmount, true);
                return false;
            }
            return true;
        }

        #endregion

        #region"Get New Standard Letter List"

        /// <summary>
        /// Thsi method is responsible for Retriving the Newly attached Standard Letters/templates.
        /// </summary>
        /// <returns></returns>

        public List<AM_StandardLetterHistory> GetNewStandardLetterList()
        {
            List<AM_StandardLetterHistory> list = new List<AM_StandardLetterHistory>();
            AM_StandardLetterHistory sl = null;
            int i = 0;
            StandardLetterHistoryList = GetStandardLetterHistoryList();
            oldSL = new List<AM_StandardLetterHistory>();
            foreach (ListItem l in lstDocuments.Items)
            {
                if (CheckNewStandardLetterId(l.Value))
                {
                    sl = new AM_StandardLetterHistory();
                    string[] value = l.Value.Split(';');
                    sl.StandardLetterId = new LetterResourceArea().GetSLetterByHistoryID(int.Parse(value[0])).StandardLetterId;
                    sl.Title = l.Text;
                    if (StandardLetterHistoryList != null)
                    {
                        for (int j = 0; j < StandardLetterHistoryList.Count; j++)
                        {
                            string[] str = StandardLetterHistoryList[j].ToString().Split(';');
                            if (sl.StandardLetterId == int.Parse(str[0]))
                            {
                                if (str[1] != "%")
                                {
                                    sl.StatusId = int.Parse(str[1]);
                                }
                                break;
                            }
                        }
                    }
                    else
                    {
                        sl.StatusId = -1;
                    }

                    list.Insert(i, sl);
                    i++;
                }
                else if (CheckStandardLetterId(l.Value))
                {
                    if (StandardLetterHistoryList != null)
                    {
                        for (int j = 0; j < StandardLetterHistoryList.Count; j++)
                        {
                            sl = new AM_StandardLetterHistory();
                            string[] str = StandardLetterHistoryList[j].ToString().Split(';');
                            string[] value = l.Value.Split(';');
                            sl.StandardLetterId = int.Parse(value[0]);
                            sl.Title = l.Text;

                            if (int.Parse(value[0]) == int.Parse(str[2]))
                            {
                                if (str[1] != "%")
                                {
                                    sl.StatusId = int.Parse(str[1]);
                                    oldSL.Add(sl);
                                }
                                break;
                            }
                        }
                    }

                }
            }
            return list;
        }

        #endregion

        #region"Validate Date"

        /// <summary>
        /// This function is responsible for validating any date.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="control"></param>
        /// <returns></returns>

        public bool ValidateDate(string value, string control)
        {
            DateTime date;
            if (!value.Equals(string.Empty))
            {
                date = DateOperations.ConvertStringToDate(value);
            }
            else
            {
                SetMessage(UserMessageConstants.ValidDateUpdateCase + "'" + control + "'.", true);
                return false;
            }
            if (ddlOutcome.SelectedItem.Text.ToLower() != "Close Case".ToLower())
            {
                if (!DateOperations.FutureDateValidation(date))
                {
                    SetMessage(UserMessageConstants.futureDateUpdateCase + "'" + control + "'.", true);
                    return false;
                }
            }
            if (!DateOperations.isValidUkDate(value))
            {
                SetMessage(UserMessageConstants.ValidDateUpdateCase + "'" + control + "'.", true);
                return false;
            }
            return true;
        }

        #endregion

        #region"Set Message"

        /// <summary>
        /// This function is responsible for Setting the Success and failure messages.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="isError"></param>

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        /// <summary>
        /// This function is responsible for Reseting the Success and failure messages.
        /// </summary>

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Upload File"

        /// <summary>
        /// This function is responsible for Uploading the file.
        /// </summary>

        public void UploadFile()
        {
            string destinationPath = ConfigurationManager.AppSettings["CaseDocumentsUploadPath"];
            if (!ValidateFile())
            {
                return;
            }
            try
            {
                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }
                string fileName = FlUpload.FileName;
                destinationPath = destinationPath + fileName;
                FlUpload.SaveAs(destinationPath);
                arrayListDocumentName.Add(fileName);
                lstDocuments.Items.Add(new ListItem(fileName));
                SetViewStateList();
                updpnlUpdateCase.Update();
            }
            catch (DirectoryNotFoundException ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionUploadFileCaseOpen, true);
                }
            }
        }

        #endregion

        #region"Reset Page"

        /// <summary>
        /// This function is responsible for Resetting the page.
        /// </summary>

        public void ResetPage()
        {
            ddlCaseManager.SelectedValue = "-1";
            ddlCaseOfficer.SelectedValue = "-1";
            PopulateStatus();
            PopulateStatusReview(int.Parse(ddlArrearsStatus.SelectedValue), null);
            this.LoadArrearsActionBySelectedStatus(int.Parse(ddlArrearsStatus.SelectedValue));
            lstDocuments.Items.Clear();
            txtNotes.Text = string.Empty;
            ddlOutcome.SelectedValue = "-1";
            txtReason.Text = string.Empty;
            txtNoticeExpiry.Text = string.Empty;
            txtNoticeIssue.Text = string.Empty;
            txtRecoveryAmount.Text = string.Empty;
        }

        #endregion

        #region"Add Documents From Array List"

        /// <summary>
        /// This function is responsible for Adding the documents from Array List to the control.
        /// </summary>

        public void AddDocumentsFromArrayList()
        {
            GetListFromViewState();
            if (arrayListDocumentName.Count <= 0)
            {
                return;
            }
            for (int i = 0; i < arrayListDocumentName.Count; i++)
            {
                lstDocuments.Items.Add(new ListItem(arrayListDocumentName[i].ToString()));
            }
        }

        #endregion

        #region"Get Cached Case"

        /// <summary>
        /// This function is responsible for retrieving the case from the Cache/Session. Before Update the Cache was used
        /// after update Session has replaced the Cache.
        /// </summary>
        /// <returns></returns>

        public AM_Case GetCachedCase()
        {

            userManager = new Users();
            AM_Case cachedCase = new AM_Case();
            cachedCase.InitiatedBy = base.GetCurrentLoggedinUser();
            cachedCase.CaseManager = int.Parse(ddlCaseManager.SelectedValue);
            cachedCase.CaseOfficer = int.Parse(ddlCaseOfficer.SelectedValue);
            cachedCase.StatusId = int.Parse(ddlArrearsStatus.SelectedValue);
            cachedCase.StatusReview = DateOperations.GetCulturedDateTime(txtStatusReview.Text);
            cachedCase.ActionId = int.Parse(ddlArrearsAction.SelectedValue);
            cachedCase.ActionReviewDate = DateOperations.GetCulturedDateTime(txtActionReview.Text);
            if (txtRecoveryAmount.Text != string.Empty)
            {
                cachedCase.RecoveryAmount = Convert.ToDouble(txtRecoveryAmount.Text);
            }
            string strExpire = txtNoticeExpiry.Text;
            if (strExpire != null && strExpire != "")
            {
                cachedCase.NoticeExpiryDate = DateOperations.GetCulturedDateTime(txtNoticeExpiry.Text);
            }
            string strIssue = txtNoticeIssue.Text;
            if (strIssue != "" && strIssue != null)
            {
                cachedCase.NoticeIssueDate = DateOperations.GetCulturedDateTime(txtNoticeIssue.Text);
            }
            cachedCase.Notes = txtNotes.Text;
            cachedCase.OutcomeLookupCodeId = int.Parse(ddlOutcome.SelectedValue);
            cachedCase.Reason = txtReason.Text;
            DataTable cachedLetters = LoadLetterList(lstDocuments.Items);

            int minutes = Convert.ToInt32(ConfigurationManager.AppSettings["CachedTimer"]);
            TimeSpan span = new TimeSpan(0, minutes, 0);
            if (cachedLetters.Rows.Count > 0)
            {
                Session.Add("letterListUpdateCase", cachedLetters);
            }
            else
            {
                Session.Add("letterListUpdateCase", "No data");
            }
            GetListFromViewState();
            //arrayListDocumentName = new ArrayList();
            if (arrayListDocumentName.Count > 0)
            {
                Session.Add("documentsArrayListUpdateCase", arrayListDocumentName);
            }
            else
            {
                Session.Add("documentsArrayListUpdateCase", "No data");
            }
            SavedLetters = new ArrayList();
            SavedLetters = GetSavedLettersViewStateList();
            //SavedLetters = new ArrayList();
            if (SavedLetters != null)
            {
                Session.Add("SavedLettersUpdateCase", SavedLetters);
            }
            else
            {
                Session.Add("SavedLettersUpdateCase", "No data");
            }
            return cachedCase;
        }

        #endregion

        #region"Load Letter List"

        /// <summary>
        /// This function is responsible for loading standard letter list and filtering out uploading documents 
        /// from the list. 
        /// </summary>
        /// <param name="listItemCollection"></param>
        /// <returns>DataTable of Standard Letters</returns>

        private DataTable LoadLetterList(ListItemCollection listItemCollection)
        {
            //List<AM_StandardLetters> Slist = new List<AM_StandardLetters>();
            DataTable dt = new DataTable();
            dt.Columns.Add("StandardLetterHistoryId");
            dt.Columns.Add("Title");
            //AM_StandardLetters sletter = null;
            foreach (ListItem item in listItemCollection)
            {
                //sletter = new AM_StandardLetters();
                if (item.Value != "-1")
                {
                    if (Validation.CheckIntegerValue(item.Value) || CheckNewStandardLetterId(item.Value) ||
                        CheckStandardLetterId(item.Value))
                    {
                        DataRow dr = dt.NewRow();
                        dr["StandardLetterHistoryId"] = item.Value;
                        dr["Title"] = item.Text;
                        dt.Rows.Add(dr);
                        //Slist.Add(sletter);
                    }
                }
            }
            return dt;
        }

        #endregion

        #region"View State"

        #region"Set View State List"

        /// <summary>
        /// This function is responsible for storing array list documents in View State.
        /// </summary>

        public void SetViewStateList()
        {
            ViewState[ViewStateConstants.DocumentsAttachedUpdateCase] = arrayListDocumentName;
        }

        #endregion

        #region"Get List From View State"

        /// <summary>
        /// This function is responsible for retrieving the documnet array list from the view state.
        /// </summary>

        public void GetListFromViewState()
        {
            if (!Convert.ToString(ViewState[ViewStateConstants.DocumentsAttachedUpdateCase]).Equals(string.Empty))
            {
                arrayListDocumentName = ViewState[ViewStateConstants.DocumentsAttachedUpdateCase] as ArrayList;
            }
        }

        #endregion

        #region"Set Saved Letters View State List"

        /// <summary>
        /// This function is responsible for saving letters view state in view state. 
        /// </summary>

        public void SetSavedLettersViewStateList()
        {
            ViewState.Remove(ViewStateConstants.SavedLettersUpdateCase);
            ViewState[ViewStateConstants.SavedLettersUpdateCase] = SavedLetters;
        }

        #endregion

        #region"Get Saved Letters View State List"

        /// <summary>
        /// This function is responsible for restrieving the the saved letters array list from view state.
        /// </summary>
        /// <returns></returns>

        public ArrayList GetSavedLettersViewStateList()
        {
            if (!Convert.ToString(ViewState[ViewStateConstants.SavedLettersUpdateCase]).Equals(string.Empty))
            {
                return ViewState[ViewStateConstants.SavedLettersUpdateCase] as ArrayList;
            }
            else
                return null;
        }

        #endregion

        #endregion

        #region"Check Document Upload"

        /// <summary>
        /// This function is responsible for Checking the document upload control 
        /// against a Stage(Status).
        /// </summary>
        /// <param name="statusid"></param>
        /// <returns></returns>

        public bool CheckDocumentUpload(int statusid)
        {
            try
            {
                statusManager = new Status();
                return statusManager.CheckDocumentUpload(statusid);
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.uploadDocuments, true);
                }
            }
            if (IsException)
                return true;
            else
                return false;
        }

        #endregion

        #region"Check Document List Box"

        /// <summary>
        ///This function is responsible for enabling and disabling the 
        ///list document, add button, edit button, remove button.
        /// </summary>

        public void CheckDocumentListBox()
        {
            if (!FlUpload.Enabled && !btnStandardLetter.Enabled)
            {
                lstDocuments.Enabled = false;
                btnAdd.Enabled = false;
                btnEdit.Enabled = false;
                btnRemove.Enabled = false;
            }
            else
            {
                lstDocuments.Enabled = true;
                btnAdd.Enabled = true;
                btnEdit.Enabled = true;
                btnRemove.Enabled = true;
            }
        }

        #endregion

        #region"Check File Upload Control"

        /// <summary>
        /// This function is responsible for enabling and disabling the file upload control 
        /// against a specific Stage(Status).
        /// </summary>
        /// <param name="statusId"></param>

        public void CheckFileUploadControl(int statusId)
        {
            if (CheckDocumentUpload(statusId))
            {
                FlUpload.Enabled = true;
            }
            else
            {
                FlUpload.Enabled = false;
            }
        }


        #endregion

        #region"Check Document Attached"

        /// <summary>
        /// This function is responsible for if any document(Standard Letter) is attached or not?
        /// </summary>
        /// <returns></returns>

        public bool CheckDocumentAttached()
        {
            bool standardLetterFlag = false;
            foreach (ListItem items in lstDocuments.Items)
            {
                if (CheckStandardLetterId(items.Value) || CheckNewStandardLetterId(items.Value))
                {
                    standardLetterFlag = true;
                    break;
                }
                else
                {
                    standardLetterFlag = false;
                }
            }
            return standardLetterFlag;
        }

        #endregion

        #region"Check Document Upload"

        /// <summary>
        /// This function is responsible for checking if any document is uploaded or not?
        /// </summary>
        /// <returns></returns>

        public bool CheckDocumentUpload()
        {
            bool documentUploadFlag = false;
            foreach (ListItem items in lstDocuments.Items)
            {
                if (!CheckStandardLetterId(items.Value) && !CheckNewStandardLetterId(items.Value))
                {
                    documentUploadFlag = true;
                    break;
                }
                else
                {
                    documentUploadFlag = false;
                }
            }
            return documentUploadFlag;
        }

        #endregion

        #region"Change the Color of Letter"

        /// <summary>
        /// This function is responsible for changing the color of the attached standard letter.
        /// </summary>
        /// <param name="id"> plane id of the standard letter</param>
        /// <param name="color">the color which needs to be assigned to the letter.</param>
        /// <param name="letter">letter id with any bit concatinated.</param>
        /// <param name="fromPageLoad">to know from which method it is being called.</param>

        public void ChangeTheColorOfLetter(int id, string color, string letter, bool fromPageLoad)
        {
            //if (fromPageLoad)
            //{
            //    string[] strId = letter.Split(';');
            //    id = int.Parse(strId[0]);
            //}
            //for (int i = 0; i < lstDocuments.Items.Count; i++)
            //{
            //    if (CheckNewStandardLetterId(lstDocuments.Items[i].Value) || CheckStandardLetterId(lstDocuments.Items[i].Value))
            //    {
            //        string[] str = lstDocuments.Items[i].Value.Split(';');
            //        if (id == int.Parse(str[0]))
            //        {
            //            lstDocuments.Items[i].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, color);
            //            if (color == "Black")
            //            {
            //                if (fromPageLoad)
            //                {
            //                    SetSavedLetters(id);
            //                    SetStandardLetterHistoryList(letter);
            //                }
            //                else
            //                {
            //                    SetSavedLetters(id);
            //                }

            //            }
            //        }
            //    }
            //}
            if (fromPageLoad)
            {
                string[] strId = letter.Split(';');
                //id = new LetterResourceArea().GetSLetterHistoryBySLetterID(int.Parse(strId[0])).StandardLetterHistoryId;
                //id = int.Parse(strId[0]);
                if (strId.Length > 2)
                {
                    id = strId[2].Equals("0") ? new LetterResourceArea().GetSLetterHistoryBySLetterID(int.Parse(strId[0])).StandardLetterHistoryId : int.Parse(strId[2]);
                }
                else
                {
                    id = new LetterResourceArea().GetSLetterHistoryBySLetterID(int.Parse(strId[0])).StandardLetterHistoryId;
                }
            }
            for (int i = 0; i < lstDocuments.Items.Count; i++)
            {
                if (CheckNewStandardLetterId(lstDocuments.Items[i].Value) || CheckStandardLetterId(lstDocuments.Items[i].Value))
                {
                    string[] str = lstDocuments.Items[i].Value.Split(';');
                    if (id == int.Parse(str[0]))
                    {
                        lstDocuments.Items[i].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, color);
                        if (color == "Black")
                        {
                            if (fromPageLoad)
                            {
                                SetSavedLetters(id);
                                SetStandardLetterHistoryList(letter);
                            }
                            else
                            {
                                SetSavedLetters(id);
                            }

                        }
                    }
                }
            }
        }

        #endregion

        #region"Set Saved Letters"

        /// <summary>
        /// This function is responsible for storing the standard letters in array list.
        /// </summary>
        /// <param name="id"></param>

        public void SetSavedLetters(int id)
        {
            //SavedLetters = new ArrayList();
            SavedLetters = GetSavedLettersViewStateList();
            if (SavedLetters == null)
            {
                SavedLetters = new ArrayList();
            }
            if (!SavedLetters.Contains(id))
            {
                SavedLetters.Add(id);
                SetSavedLettersViewStateList();
            }
            else
                return;
        }

        #endregion

        #region"Check Saved Letters"

        /// <summary>
        /// This function is responsible for checking whether the letter is saved. Saved is the state of
        /// letter which is maintained to change the color of letter from red to black.
        /// </summary>

        public void CheckSavedLetters()
        {
            SavedLetters = new ArrayList();
            SavedLetters = GetSavedLettersViewStateList();
            if (lstDocuments.Items.Count > 0)
            {
                //for (int i = 0; i < SavedLetters.Count; i++)
                {
                    int i = 0;
                    foreach (ListItem item in lstDocuments.Items)
                    {
                        if (!FileOperations.CheckFileExtension(item.Text))
                        {
                            string[] str = item.Value.Split(';');
                            if (SavedLetters != null)
                            {
                                if (i < SavedLetters.Count)
                                {
                                    if (SavedLetters.Contains(int.Parse(str[0])))//[i].ToString()) == int.Parse(str[0]))
                                    {
                                        ChangeTheColorOfLetter(int.Parse(str[0]), "Black", "", false);
                                        i++;
                                    }
                                    else
                                    {
                                        ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                                    }
                                }
                                else if (!SavedLetters.Contains(int.Parse(str[0])))
                                {
                                    ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                                }
                            }
                            else
                            {
                                ChangeTheColorOfLetter(int.Parse(str[0]), "Red", "", false);
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region"Check New Document Upload"

        /// <summary>
        /// This function is responsible for checking whether any new document is uploaded or not?
        /// </summary>
        /// <returns></returns>

        public bool CheckNewDocumentUpload()
        {
            bool newDocumentFlag = false;
            foreach (ListItem item in lstDocuments.Items)
            {
                if (!Validation.CheckIntegerValue(item.Value))
                {
                    if (CheckStandardLetterId(item.Value) || CheckNewStandardLetterId(item.Value))
                    {
                        newDocumentFlag = false;
                    }
                    else
                    {
                        newDocumentFlag = true;
                        break;
                    }
                }
                else
                {
                    newDocumentFlag = false;
                }
            }
            return newDocumentFlag;
        }

        #endregion

        #region"Check New Standard Letter Attached"

        /// <summary>
        /// This function is responsible for checking if any new Standard Letter is attached or not?
        /// </summary>
        /// <returns></returns>

        public bool CheckNewStandardLetterAttached()
        {
            bool newStandardLetterFlag = false;
            foreach (ListItem item in lstDocuments.Items)
            {
                if (CheckNewStandardLetterId(item.Value))
                {
                    newStandardLetterFlag = true;
                    break;
                }
                else
                {
                    newStandardLetterFlag = false;
                }
            }
            return newStandardLetterFlag;
        }

        #endregion

        #region"Set Action Ranking"

        /// <summary>
        /// To Save the Current Status Id, Current Status Ranking, Current Action Id, Current Action Ranking. 
        /// </summary>
        /// <param name="actionId"></param>
        /// <param name="statusId"></param>
        public void SetStatusAndActionRanking(int actionId, int statusId)
        {
            Session[ViewStateConstants.RecordedActionRankingUpdateCase] = GetActionRanking(actionId);
            if (Session[ViewStateConstants.RecordedActionUpdateCase] == null)
            {
                ListItem item = ddlArrearsAction.Items.FindByValue(Convert.ToString(actionId));
                if (item != null)
                {
                    Session[ViewStateConstants.RecordedActionUpdateCase] = item.Text;
                }
                else
                {
                    Session[ViewStateConstants.RecordedActionUpdateCase] = 0;
                }
            }
            Session[ViewStateConstants.RecordedStatusUpdateCase] = statusId;
            Session[ViewStateConstants.RecordedStatusRankingUpdateCase] = GetStatusRanking(statusId);
            Session[ViewStateConstants.ignoredActionIdUpdateCase] = actionId;
            Session[ViewStateConstants.StatusRankingIterationUpdateCase] = Session[ViewStateConstants.RecordedStatusRankingUpdateCase];
            Session[ViewStateConstants.IgnoredStatusUpdateCase] = Session[ViewStateConstants.RecordedStatusUpdateCase];
        }

        #endregion

        #region"Get Action Ranking"

        /// <summary>
        /// to get the ranking for any action.
        /// </summary>
        /// <param name="actionId"></param>
        /// <returns></returns>
        public int GetActionRanking(int actionId)
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            AM_Action action = actionManager.GetActionRanking(actionId);
            Session[ViewStateConstants.RecordedActionUpdateCase] = action.Title;
            return action.Ranking;

        }

        #endregion

        #region"Get Status Ranking"
        /// <summary>
        /// This function is responsible for getting ranking of any stage(status).
        /// </summary>
        /// <param name="statusId"></param>
        /// <returns></returns>
        public int GetStatusRanking(int statusId)
        {
            try
            {
                statusManager = new Status();
                AM_Status status = statusManager.GetStatusRanking(statusId);
                if (status != null)
                {
                    return status.Ranking;
                }
                else
                {
                    return 0;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Show Modal Popup"

        /// <summary>
        /// To show the modal popup which is responsible for taking reasons of ignored actions.
        /// </summary>

        public void ShowModalPopup()
        {
            ResetActionDetails();
            statusManager = new Status();
            AM_Status CurrentStatus = statusManager.GetStatusRanking(int.Parse(ddlArrearsStatus.SelectedValue));
            if (CurrentStatus != null)
            {
                ViewState[ViewStateConstants.CurrentStatusRankingUpdateCase] = CurrentStatus.Ranking;
            }

            actionManager = new BusinessManager.statusmgmt.Action();
            AM_Action currentActionRank = actionManager.GetActionRanking(int.Parse(ddlArrearsAction.SelectedValue));

            if (currentActionRank != null)
            {
                // Viewstate[RecordedStatusUpdateCase] contains the status id that had been recorded in last update.
                if (Convert.ToInt32(Session[ViewStateConstants.RecordedStatusUpdateCase]) == Convert.ToInt32(ddlArrearsStatus.SelectedValue))
                {
                    ViewState[ViewStateConstants.NewStatusFlagUpdateCase] = false;
                }
                else
                {
                    ViewState[ViewStateConstants.NewStatusFlagUpdateCase] = true;
                }
                ViewState[ViewStateConstants.CurrentActionRankUpdateCase] = currentActionRank.Ranking;

                if (currentActionRank.StatusId != Convert.ToInt32(Session[ViewStateConstants.RecordedStatusUpdateCase]))
                {
                    if (IsProceduralActionLeftWithinAStage(Convert.ToInt32(Session[ViewStateConstants.RecordedStatusUpdateCase])) == false)
                    {
                        if (CaseUpdate())
                        {
                            Response.Redirect(PathConstants.updateCaseRedirectPath, false);
                        }
                    }
                    else
                    {
                        ActionRankingIteration();
                        SetModalPopupControls(int.Parse(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase].ToString()), -1);
                    }
                }
                else
                {
                    if (CaseUpdate())
                    {
                        Response.Redirect(PathConstants.updateCaseRedirectPath, false);
                    }
                    else
                    {
                        return;
                    }
                }
            }

            modalPopupPaymentPlanIgnoreReason.Hide();
            modalPopupActionDetails.Show();
        }

        #endregion

        #region"Is Procedural Action Left Within a Stage"

        public bool IsProceduralActionLeftWithinAStage(int stageId)
        {
            try
            {
                actionManager = new BusinessManager.statusmgmt.Action();
                return actionManager.IsProceduralActionLeftWithinStage(stageId, base.GetCaseId());
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Set Modal Popup Controls"

        /// <summary>
        /// This function is responsible for setting controls and Action Title on the modal popup.
        /// </summary>
        /// <param name="rankId"></param>
        /// <param name="currentIndex"></param>

        public bool SetModalPopupControls(int rankId, int currentIndex)
        {
            lblActionError.Visible = false;
            actionManager = new BusinessManager.statusmgmt.Action();
            if (Session[ViewStateConstants.RecordedActionRankingUpdateCase] != null && Session[ViewStateConstants.RecordedActionUpdateCase] != null)
            {
                AM_Action action = new AM_Action();
                if (currentIndex == -1)
                {
                    action = GetFirstProceduralIgnoredAction(Convert.ToInt32(Session[ViewStateConstants.IgnoredStatusUpdateCase]));
                    if (action == null)
                    {
                        if (CaseUpdate())
                        {
                            Response.Redirect(PathConstants.updateCaseRedirectPath, false);
                        }
                    }
                    else
                    {
                        ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] = action.Ranking;
                    }
                }
                else
                {
                    action = actionManager.GetActionByRank(Convert.ToInt32(Session[ViewStateConstants.IgnoredStatusUpdateCase]), rankId);
                    if (ViewState["previousIgnoredActionId"] == null && currentIndex == -1)
                    {
                        ViewState["previousIgnoredActionId"] = 0;
                    }
                    else
                    {

                    }
                }
                {

                    if (action.IsProceduralAction && !IsActionAlreadyRecorded(action.ActionId))
                    {
                        lblAction.Text = action.Title;
                        Session[ViewStateConstants.ignoredActionIdUpdateCase] = action.ActionId;
                        lblTitle.Text = UserMessageConstants.ActionIgnoredTitleUpdateCase;
                        //if (IsLastIgnoredAction(rankId)  && IsStatusRankingEqual())

                        if ((IsLastIgnoredAction(action.Ranking, action.ActionId, currentIndex) == true))
                        {
                            txtActionDetail.Text = string.Empty;
                            btnNext.Enabled = false;
                            btnActionSave.Enabled = true;
                            ViewState[ViewStateConstants.ModalPopupPageUpdateCase] = currentIndex;
                        }
                        else
                        {
                            txtActionDetail.Text = string.Empty;
                            btnNext.Enabled = true;
                            btnActionSave.Enabled = false;
                            ViewState[ViewStateConstants.ModalPopupPageUpdateCase] = currentIndex;
                        }
                        ViewState["previousIgnoredActionId"] = action.ActionId;
                        return true;
                    }
                    else
                    {
                        if (IsLastIgnoredAction(action.Ranking, Convert.ToInt32(ViewState["previousIgnoredActionId"]), currentIndex))
                        {
                            if (CaseUpdate())
                            {
                                Response.Redirect(PathConstants.updateCaseRedirectPath, false);
                            }
                        }
                        else
                        {
                            IterateIgnoredActions();
                        }
                        return false;
                    }
                }
                return false;
            }
            return false;
        }

        #endregion

        #region"Get First Procedural Ignored Action"

        public AM_Action GetFirstProceduralIgnoredAction(int statusId)
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            return actionManager.GetFirstProceduralIgnoredAction(statusId, base.GetCaseId());
        }

        #endregion

        #region"Is Action Already Recorded"

        public bool IsActionAlreadyRecorded(int actionId)
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            return actionManager.IsActionAlreadyRecorded(actionId, base.GetCaseId());
        }

        #endregion

        #region"Set Action Data In Array List"

        /// <summary>
        /// This function is responsible for saving the ignored reasons of actions in the array list.
        /// </summary>
        /// <param name="actionId"></param>
        /// <param name="statusId"></param>

        public void SetActionDataInArrayList(int actionId, int statusId)
        {
            ActionData = new DataTable();
            ActionData.Columns.Add("ActionId");
            ActionData.Columns.Add("StatusId");
            ActionData.Columns.Add("IgnoreReason");
            if (ViewState[ViewStateConstants.ActionDataArrayListUpdateCase] != null)
            {
                ActionData = ViewState[ViewStateConstants.ActionDataArrayListUpdateCase] as DataTable;
            }
            DataRow dr = ActionData.NewRow();
            dr["ActionId"] = actionId;
            dr["StatusId"] = statusId;
            dr["IgnoreReason"] = txtActionDetail.Text;
            ActionData.Rows.Add(dr);
            ViewState[ViewStateConstants.ActionDataArrayListUpdateCase] = ActionData;
        }

        #endregion

        #region"Action Ranking Iteration"

        /// <summary>
        /// This function is responsible for iterating the ranking of ignored actions to get the next ignored action.
        /// </summary>

        public void ActionRankingIteration()
        {
            if (ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] == null)
            {
                ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] = Convert.ToInt32(Session[ViewStateConstants.RecordedActionRankingUpdateCase]) + 1;
            }
            else
            {
                ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] = Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]) + 1;
            }
        }

        #endregion

        #region"Reset Action Details"

        /// <summary>
        /// Reset the details of ignored action data from view state and session.
        /// </summary>

        public void ResetActionDetails()
        {
            if (ActionData != null)
            {
                ActionData.Clear();
            }
            ViewState.Remove(ViewStateConstants.ActionDataArrayListUpdateCase);
            ViewState.Remove(ViewStateConstants.ActionListCounterUpdateCase);
            ViewState.Remove(ViewStateConstants.ModalPopupPageUpdateCase);
            ViewState.Remove(ViewStateConstants.ActionRankingIterationUpdateCase);
            Session[ViewStateConstants.StatusRankingIterationUpdateCase] = Session[ViewStateConstants.RecordedStatusRankingUpdateCase];
            Session[ViewStateConstants.IgnoredStatusUpdateCase] = Session[ViewStateConstants.RecordedStatusUpdateCase];
        }

        #endregion

        #region"Status Ranking Iteration"

        /// <summary>
        /// This function is responsible for iterating the ignored status.
        /// </summary>

        public void StatusRankingIteration()
        {
            Session[ViewStateConstants.StatusRankingIterationUpdateCase] = Convert.ToInt32(Session[ViewStateConstants.StatusRankingIterationUpdateCase]) + 1;
            Session[ViewStateConstants.RecordedActionRankingUpdateCase] = 1;
        }

        #endregion

        #region"Check Payment Plan Mandatory"

        /// <summary>
        /// responsible for checking whether the payment plan is mandatory against the action or not?
        /// </summary>
        /// <returns></returns>

        public bool CheckPaymentPlanMandatory()
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            return actionManager.CheckPaymentPlanMandatory(int.Parse(ddlArrearsAction.SelectedValue));
        }

        #endregion

        #region"Check Payment Plan"

        /// <summary>
        /// This function is responsible for checking whether the payment plan has been created against the tenant or not?
        /// </summary>
        /// <returns></returns>

        public bool CheckPaymentPlan()
        {
            openCase = new OpenCase();
            return openCase.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Check Recovery Amount"

        /// <summary>
        /// This function is responsible for checking whether the recovery amount is applicable against the stage(status) or not?
        /// </summary>
        /// <param name="statusId"></param>

        public void CheckRecoveryAmount(int statusId)
        {
            try
            {
                caseDetails = new CaseDetails();
                txtRecoveryAmount.Enabled = caseDetails.CheckRecoveryAmount(statusId);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is New Status Recorded"

        /// <summary>
        /// This function is responsible for if the user has recorded the new stage(status).
        /// </summary>
        /// <returns></returns>

        public bool IsNewStatusRecorded()
        {
            if (Session[ViewStateConstants.StatusIdUpdateCase] != null)
            {
                if (ddlArrearsStatus.SelectedValue != Convert.ToString(Session[ViewStateConstants.StatusIdUpdateCase]))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Set Standard Letter History List"

        /// <summary>
        /// This function is responsible for saving the standard letter history ids in the array list.
        /// </summary>
        /// <param name="letter"></param>

        public void SetStandardLetterHistoryList(string letter)
        {
            StandardLetterHistoryList = GetStandardLetterHistoryList();
            if (StandardLetterHistoryList == null)
            {
                StandardLetterHistoryList = new ArrayList();
            }
            if (!StandardLetterHistoryList.Contains(letter))
            {
                for (int i = 0; i < StandardLetterHistoryList.Count; i++)
                {
                    string[] str = StandardLetterHistoryList[i].ToString().Split(';');
                    string[] str2 = letter.Split(';');
                    if (str2[0].Equals(str[0]))
                    {
                        StandardLetterHistoryList.RemoveAt(i);
                    }
                }
                StandardLetterHistoryList.Add(letter);
                Session[SessionConstants.SavedLetterHistoryListUpdateCase] = StandardLetterHistoryList;
            }
        }

        #endregion

        #region"Get Standard Letter History List"

        /// <summary>
        /// This function is responsible for retrieving the standard letter array list from session.
        /// </summary>
        /// <returns></returns>

        public ArrayList GetStandardLetterHistoryList()
        {
            StandardLetterHistoryList = Session[SessionConstants.SavedLetterHistoryListUpdateCase] as ArrayList;
            return StandardLetterHistoryList;
        }

        #endregion

        #region"Get Standard Letter History Id"

        /// <summary>
        /// This function is responsible for retrieving the standard letter history id from the array list.
        /// </summary>
        /// <param name="slId"></param>
        /// <returns></returns>

        public int GetStandardLetterHistoryId(int slId)
        {
            //StandardLetterHistoryList = GetStandardLetterHistoryList();
            //if (StandardLetterHistoryList != null)
            //{
            //    for (int i = 0; i < StandardLetterHistoryList.Count; i++)
            //    {
            //        string[] str = StandardLetterHistoryList[i].ToString().Split(';');
            //        if (str.Count() == 2)
            //        {
            //            if (int.Parse(str[0]) == slId)
            //                return int.Parse(str[1]);
            //        }
            //        else
            //        {
            //            return -1;
            //        }
            //    }
            //    return -1;
            //}
            //else
            //{
            //    return -1;
            //}
            int sl = new LetterResourceArea().GetSLetterByHistoryID(slId).StandardLetterId;
            StandardLetterHistoryList = GetStandardLetterHistoryList();
            if (StandardLetterHistoryList != null)
            {
                for (int i = 0; i < StandardLetterHistoryList.Count; i++)
                {
                    string[] str = StandardLetterHistoryList[i].ToString().Split(';');
                    if (str.Count() >= 2)
                    {
                        if (int.Parse(str[0]) == sl)
                            return int.Parse(str[1]);
                    }
                    else
                    {
                        return -1;
                    }

                }
                return -1;
            }
            else
            {
                return -1;
            }
        }

        public int GetStandardLetterHistoryId(int slId, int lhId)
        {
            StandardLetterHistoryList = GetStandardLetterHistoryList();
            if (StandardLetterHistoryList != null)
            {
                for (int i = 0; i < StandardLetterHistoryList.Count; i++)
                {
                    string[] str = StandardLetterHistoryList[i].ToString().Split(';');
                    if (str.Count() >= 2)
                    {
                        if (int.Parse(str[0]) == slId)
                            return int.Parse(str[1]);
                    }
                    else
                    {
                        return -1;
                    }

                }
                return -1;
            }
            else
            {
                return -1;
            }
        }

        #endregion

        #region"Get Case Standard Letter History Id"

        /// <summary>
        /// This function is responsible for retrieving the standard letter history id of specific standard letter from
        /// database stored against the case.
        /// </summary>
        /// <param name="stLId"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>

        public int GetCaseStandardLetterHistoryId(int stLId, int caseId)
        {
            LetterResourceArea letter = new LetterResourceArea();
            return letter.GetCaseStandardLetterHistoryId(stLId, caseId);

        }

        #endregion

        #region"Iterate Ignored Actions"

        /// <summary>
        /// Iterates the Ignored Actions to record the ignore reasons.
        /// </summary>

        public void IterateIgnoredActions()
        {
            if (Convert.ToInt32(Session[ViewStateConstants.RecordedStatusUpdateCase]) == Convert.ToInt32(ddlArrearsStatus.SelectedValue))
            {
                ViewState[ViewStateConstants.NewStatusFlagUpdateCase] = false;
                ActionRankingIteration();
                SetModalPopupControls(Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]), Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]));
            }
            else
            {
                ViewState[ViewStateConstants.NewStatusFlagUpdateCase] = true;
                ActionRankingIteration();
                statusManager = new Status();
                actionManager = new BusinessManager.statusmgmt.Action();
                AM_Action action = actionManager.GetActionByRank(Convert.ToInt32(Session[ViewStateConstants.IgnoredStatusUpdateCase]), Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]));
                if (action != null)
                {
                    Session[ViewStateConstants.ignoredActionIdUpdateCase] = action.ActionId;
                    SetModalPopupControls(action.Ranking, Convert.ToInt32(ViewState[ViewStateConstants.ModalPopupPageUpdateCase]) + 1);
                }
                else
                {
                    StatusRankingIteration();
                    AM_Status status = statusManager.GetStatusByRanking(Convert.ToInt32(Session[ViewStateConstants.StatusRankingIterationUpdateCase]));
                    if (status != null)
                    {
                        Session[ViewStateConstants.IgnoredStatusUpdateCase] = status.StatusId;
                        ViewState[ViewStateConstants.ActionRankingIterationUpdateCase] = 1;
                        AM_Action action2 = actionManager.GetActionByRank(Convert.ToInt32(Session[ViewStateConstants.IgnoredStatusUpdateCase]), Convert.ToInt32(ViewState[ViewStateConstants.ActionRankingIterationUpdateCase]));
                        if (action2 != null)
                        {
                            Session[ViewStateConstants.ignoredActionIdUpdateCase] = action2.ActionId;
                            SetModalPopupControls(action2.Ranking, Convert.ToInt32(ViewState[ViewStateConstants.ModalPopupPageUpdateCase]) + 1);
                        }
                    }
                }
            }
        }

        #endregion

        #region"Is Last Ignored Action"

        /// <summary>
        /// Checks whether the current action is the last action ignored among more than one actions or not?
        /// </summary>
        /// <param name="rankId"> rank of ignored action</param>
        /// <returns></returns>

        public bool IsLastIgnoredAction(int rankId, int previousIgnoredActionId, int isFirstIteration)
        {
            try
            {
                actionManager = new BusinessManager.statusmgmt.Action();
                if (IsProceduralActionRecorded(Convert.ToInt32(Session[ViewStateConstants.IgnoredStatusUpdateCase]), rankId + 1) == false)
                {
                    if ((IsIgnoredProceduralActionRemaining(Convert.ToInt32(Session[ViewStateConstants.IgnoredStatusUpdateCase]), rankId + 1, int.Parse(ddlArrearsAction.SelectedValue), Convert.ToInt32(ddlArrearsStatus.SelectedValue), previousIgnoredActionId, isFirstIteration) == false))
                    {

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Procedural Action Recorded"

        public bool IsProceduralActionRecorded(int statusId, int rank)
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            return actionManager.IsProceduralActionRecorded(statusId, rank, base.GetCaseId());
        }

        #endregion

        #region"Is Ignored Procedural Action Remaining"

        /// <summary>
        /// Check if there exist any Ignored procedural action
        /// </summary>
        /// <param name="statusId"></param>
        /// <param name="rankId"></param>
        /// <param name="currentActionId"></param>
        /// <param name="currentStatusId"></param>
        /// <returns></returns>

        public bool IsIgnoredProceduralActionRemaining(int statusId, int rankId, int currentActionId, int currentStatusId, int previousIgnoredActionId, int isFirstIteration)
        {
            try
            {
                actionManager = new BusinessManager.statusmgmt.Action();
                return actionManager.IsIgnoredProceduralActionRemaining(statusId, rankId, currentActionId, currentStatusId, previousIgnoredActionId, isFirstIteration);
            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Is Status Ranking Equal"

        public bool IsStatusRankingEqual()
        {
            if (Convert.ToInt32(ViewState[ViewStateConstants.CurrentStatusRankingUpdateCase]) == Convert.ToInt32(Session[ViewStateConstants.StatusRankingIterationUpdateCase]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #endregion
    }
}