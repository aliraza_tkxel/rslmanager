﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientLastPayment.ascx.cs"
    Inherits="Am.Ahv.Website.controls.casemgt.ClientLastPayment" %>
   
<div style="padding: 5px; border: 1px solid gray;width: auto; overflow: hidden;height:150px;">
    <asp:Panel ID="Panel1" runat="server" Height="162px">
        <div class="NormalPointFontBold" style="padding: 5px">
            <asp:Label ID="Label1" runat="server" Text="Housing Benefit:"></asp:Label>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
        </div>
        <div style="float: left; padding-top: 5px;">
            <div id="divEstimatedHBDue" runat="server" visible="true" class="NormalPointFont"
                style="float: left; padding-top: 5px;padding-left:7px;">
                <asp:Label ID="lblEstiatedHBDue" runat="server" Text="Estimated HB Due:"></asp:Label>
            </div>
            <div id="divEstimatedHBDueAmount" runat="server" visible="true" style="float: right;
                padding-top: 5px;margin-left:40px;*margin-left:25px;">
                <asp:TextBox ID="txtEstimatedHBDueAmount" ForeColor="Black" ReadOnly="true" Font-Bold="true"
                    runat="server" Width="72px"></asp:TextBox>
            </div>
        </div>
        <div style="float: left;">
            <div class="NormalPointFont" style="float: left; padding-top: 5px;padding-left:7px;">
                <asp:Label ID="lblHBCyclePayment" runat="server" Text="Monthly HB:"></asp:Label>
            </div>
            <div style="float: right; padding-top: 5px;margin-left:76px;*margin-left:25px;">
                <asp:TextBox ID="txtHBCyclePayment" ForeColor="Black" ReadOnly="true" Font-Bold="true"
                    runat="server" Width="72px"></asp:TextBox>
            </div>
        </div>
        <div style="float: left; padding-top: 5px;">
            <div id="lastPaymentLabel" runat="server" visible="true" class="NormalPointFont"
                style="float: left; padding-top: 5px;margin-left:5px;">
                <asp:Label ID="lblCPayStartDate" runat="server" Text=""></asp:Label><asp:Label ID="lblCPAyLastDate"
                    runat="server" Text="HB Cycle Payment:"></asp:Label>
            </div>
            <div id="lastPaymentTextBox" runat="server" visible="true" style="float: right; padding-top: 5px;margin-left:41px;*margin-left:8px;margin-top:-5px;">
                <asp:TextBox ID="txtLastCPayAmout" ForeColor="Black" ReadOnly="true" Font-Bold="true"
                    runat="server" Width="72px"></asp:TextBox>
            </div>
        </div>
        <!-- Clearing Floats-->
        <div style="clear: both;">
        </div>
        <!-- Clearing Floats-->
        <div style="clear: both;">
        </div>
        <div class="NormalPointFontBold">
            <div style="float: left; padding-top: 5px; width: 180px; padding-bottom: 5px;">
                <asp:Button ID="btnHBSchedule" runat="server" Text="Click to see HB schedule" />
                <!-- Clearing Floats-->
                <div style="clear: both;">
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
