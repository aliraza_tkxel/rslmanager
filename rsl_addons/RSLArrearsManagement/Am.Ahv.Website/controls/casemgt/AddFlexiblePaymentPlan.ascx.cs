﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.payments;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Utilities.utitility_classes;
using System.Text.RegularExpressions;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.Casemgmt;
namespace Am.Ahv.Website.controls.casemgt
{
    public delegate void RegularPaymentControl(bool visible, string value);
    public delegate void OpenCaseAgainstFlexiblePaymentPlan(bool value);

    public partial class AddFlexiblePaymentPlan : UserControlBase
    {
        #region"Attributes"

        String validationMessage;

        Payments paymentManager = null;
        Validation validateInput = new Validation();

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region"Events"

        #region"Delegate Event"

        public event RegularPaymentControl invokeEvent;
        public event OpenCaseAgainstFlexiblePaymentPlan OpenCaseAndFlexiblePaymentPlan;

        #endregion

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if (Session[SessionConstants.UpdateFlagPaymentPlan] == null)
                {
                    InitLookups();
                    LoadDefaultFlexiblePayments();
                    SetWeeklyRent();
                    SetDefaultParameters();
                }
            }
            CheckPaymentPlanPrint();
            SetCreationDate();
            ResetMessage();
            
        }

        #endregion

        #region"Btn Create Payment Plan Click"

        protected void btnCreatePaymentPlan_Click(object sender, EventArgs e)
        {
            if (CheckPaymentPlan())
            {
                SetMessage(UserMessageConstants.paymentPlanAlreadyExist, true);
                return;
            }

            if (!CheckCase())
            {
                if (Session[SessionConstants.OpenCaseFlag] != null)
                {
                    if (Convert.ToString(Session[SessionConstants.OpenCaseFlag]) == "true")
                    {
                        OpenCaseAndFlexiblePaymentPlan(true);
                    }
                    else
                    {
                        SetMessage(UserMessageConstants.CaseNotOpenPaymentPlan, true);
                        return;
                    }
                }
                else
                {
                    SetMessage(UserMessageConstants.CaseNotOpenPaymentPlan, true);
                    return;
                }
            }

            if (!Validate())
            {
                //    lblFlexiblePaymentErrorMessage.Text = validationMessage;
                return;
            }
            else
            {
                if (!CreatePaymentPlan())
                {
                    return;
                }
                else
                {
                    SetMessage(UserMessageConstants.paymentPlanSuccess, false);
                    DisablePaymentPlan();
                    ShowUpdateButton();
                    DisableCancelButton();
                    CheckPaymentPlanPrint();
                    Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                }
            }

        }

        #endregion

        #region"DDL Frequency Regular Payment Selected Index Changed"

        protected void ddlFrequencyRegularPayment_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.CalculateEndDate();
            //SetRentPayableFrequency();
            SetAmountToBeCollected();
            SetRentPayableFrequency();
            this.CalculateEndDate();

        }

        #endregion

        #region"DDL Payment Type Selected Index Changed"

        protected void ddlPaymentType_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (ddlPaymentType.SelectedItem.Text.Equals("Regular"))
            {
                invokeEvent(true, ddlPaymentType.SelectedValue);
            }
            else
                return;
        }

        #endregion

        #region"Imaging Button Click"

        protected void ImagimgButton_Click(object sender, ImageClickEventArgs e)
        {
            TextBox txtBoxDate;
            TextBox txtBoxValue;
            Label lblPayment;
            GridViewRow gridRow;
            List<String> dates = new List<string>();
            List<String> values = new List<string>();
            ArrayList list = (ArrayList)ViewState["list"];

            list.Add(0);
            gridDiv.Style["height"] = ((list.Count) * 30).ToString() + "px";

            foreach (GridViewRow gRow in gvFlexiblePayment.Rows)
            {
                txtBoxDate = (TextBox)gRow.FindControl("txtPaymentDate");
                txtBoxValue = (TextBox)gRow.FindControl("txtPaymentValue");
                dates.Add(txtBoxDate.Text);
                values.Add(txtBoxValue.Text);
            }

            gvFlexiblePayment.DataSource = list;
            gvFlexiblePayment.DataBind();

            for (int i = 0; i < (gvFlexiblePayment.Rows.Count - 1); i++)
            {
                gridRow = gvFlexiblePayment.Rows[i];
                txtBoxDate = (TextBox)gridRow.FindControl("txtPaymentDate");
                txtBoxValue = (TextBox)gridRow.FindControl("txtPaymentValue");
                lblPayment = (Label)gridRow.FindControl("lblPaymentNumber");
                if (i == 0)
                {
                    lblPayment.Text = (i + 2).ToString() + "nd" + " Payment" + ":";
                }
                else if (i == 1)
                {
                    lblPayment.Text = (i + 2).ToString() + "rd" + " Payment" + ":";
                }
                else
                {
                    lblPayment.Text = (i + 2).ToString() + "th" + " Payment" + ":";
                }
                txtBoxDate.Text = dates[i];
                txtBoxValue.Text = values[i];
            }
            gridRow = gvFlexiblePayment.Rows[gvFlexiblePayment.Rows.Count - 1];
            lblPayment = (Label)gridRow.FindControl("lblPaymentNumber");
            int count = gvFlexiblePayment.Rows.Count;
            if (count == 2)
            {
                lblPayment.Text = (count + 1).ToString() + "rd" + " Payment" + ":";
            }
            else
            {
                lblPayment.Text = (count + 1).ToString() + "th" + " Payment" + ":";
            }

        }

        #endregion

        #region"Txt 1st Payment Text Changed"

        protected void txtPayment1_TextChanged(object sender, EventArgs e)
        {
            if (!validateInput.Validate_Currency_Text_Box(txtPaymentVlue1.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Monthly Rent.", true);
                return;
            }
            else if (Convert.ToDouble(txtPaymentVlue1.Text) < 0)
            {
                SetMessage("'1st Payment' "+UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
            //else if (Convert.ToDouble(txtPaymentVlue1.Text) > base.GetRentBalance())
            //{
            //    SetMessage(UserMessageConstants.paymentPlanOverFlexibleAmounts, true);
            //    return;
            //}
            CalculateEndDate();
            //else if (Convert.ToDouble(txtPaymentVlue1.Text) == base.GetRentBalance())
            //{
            //    CalculateEndDate();
            //}

        }

        #endregion

        #region"Txt Payment Text Changed"

        protected void txtPayment_TextChanged(object sender, EventArgs e)
        {
            TextBox txt;
            GridViewRow gvr = ((TextBox)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;
            txt = (TextBox)gvFlexiblePayment.Rows[index].FindControl("txtPaymentValue");

            if (!validateInput.Validate_Currency_Text_Box(txt.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Monthly Rent.", true);
                return;
            }
            else if (Convert.ToDouble(txt.Text) < 0)
            {
                SetMessage("'Flexible Payments' "+UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
            //else if (GetRemainingBalance() < 0)
            //{
            //    SetMessage(UserMessageConstants.paymentPlanOverFlexibleAmounts, true);
            //    return;
            //}
            //else if (Convert.ToDouble(txt.Text) > base.GetRentBalance())
            //{
            //    SetMessage(UserMessageConstants.paymentPlanOverFlexibleAmounts, true);
            //    return;
            //}
            //else if (GetRemainingBalance() == 0.0)
            {
                CalculateEndDate();
            }

        }

        #endregion

        #region"Txt Amount Collected Text Changed"

        protected void txtAmountCollected_TextChanged(object sender, EventArgs e)
        {
            if (!validateInput.Validate_Currency_Text_Box(txtAmountCollected.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Agreement Amount.", true);
                return;
            }
            else if (Convert.ToDouble(txtAmountCollected.Text) == 0.0 || Convert.ToDouble(txtAmountCollected.Text) < 0.0)
            {
                SetMessage("'Agreed Amount' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }

            double remainingBalance = GetRemainingBalance();
            
            //if (Convert.ToDouble(txtAmountCollected.Text) > remainingBalance)
            //{
            //    SetMessage(UserMessageConstants.paymentPlanAmountGreaterThanRemainingRentBalance, true);
            //    return;
            //}
            //else
            //if (Convert.ToDouble(txtAmountCollected.Text) < remainingBalance && (ddlFrequencyRegularPayment.SelectedItem.Text.Equals("Full Amount")))
            //{
            //    SetMessage(UserMessageConstants.paymentPlanAmountEqualRemainingRentBalance, true);
            //    return;
            //}
            CalculateEndDate();
            SetArrearsCollection();

        }

        #endregion

        #region"Txt Arrears Balance Text Changed"

        protected void TxtArrearsBalance_TextChanged(object sender, EventArgs e)
        {
            

            if (!validateInput.Validate_Currency_Text_Box(txtArrearsBalance.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Owed to BHA.", true);
                return;
            }

            //else if (Convert.ToDouble(txtAmountCollected.Text) > Convert.ToDouble(txtArrearsBalance.Text))
            //{
            //    SetMessage(UserMessageConstants.paymentPlanAmountToBeCollectedgreater, true);
            //    return;
            //}

            else if (Convert.ToDouble(txtArrearsBalance.Text) == 0.0 || Convert.ToDouble(txtArrearsBalance.Text) < 0.0)
            {
                SetMessage("'Owed to BHA' "+UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
            this.CalculateEndDate();
        }

        #endregion

        #region"Txt Weekly Rent Amount Text Changed"

        protected void txtWeeklyRentAmount_TextChanged(object sender, EventArgs e)
        {
            if (!validateInput.Validate_Currency_Text_Box(txtWeeklyRentAmount.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Monthly Rent.", true);
                return;
            }
            else if (Convert.ToDouble(txtWeeklyRentAmount.Text) == 0.0 || Convert.ToDouble(txtWeeklyRentAmount.Text) < 0.0)
            {
                SetMessage("'Monthly Rent' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
        }

        #endregion

        #region"Txt Rent Payable Text Changed"

        protected void TxtRentPayable_TextChanged(object sender, EventArgs e)
        {
            if (!validateInput.Validate_Currency_Text_Box(txtRentPayable.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Rent Payable.", true);
                return;
            }
            //else if (Convert.ToDouble(txtRentPayable.Text) == 0.0 || Convert.ToDouble(txtRentPayable.Text) < 0.0)
            else if (Convert.ToDouble(txtRentPayable.Text) < 0.0)
            {
                SetMessage("'Rent Payable' "+UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
            SetArrearsCollection();
        }

        #endregion

        #region"Txt Arrears Collection Text Changed"

        /// <summary>
        /// Arrears Collection = Rent Payable + Agreed Amount
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtArrearsCollection_TextChanged(object sender, EventArgs e)
        {
            if (!validateInput.Validate_Currency_Text_Box(txtArrearsCollection.Text))
            {
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Arrears Collection.", true);
                return;
            }
            else if (Convert.ToDouble(txtArrearsCollection.Text) == 0.0 || Convert.ToDouble(txtArrearsCollection.Text) < 0.0)
            {
                SetMessage("'Arrears Collection' "+UserMessageConstants.paymentAmountgretaterThanZero, true);
                return;
            }
            SetArrearsCollection();
        }

        #endregion

        #region"Txt Payment Date1 Selected Text Changed"

        protected void txtPaymentDate1_TextChanged(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            date = DateOperations.ConvertStringToDate(txtPaymentDate1.Text);
            if (!DateOperations.FutureDateValidation(date))
            {
                SetMessage(UserMessageConstants.paymentPlandatePayment, true);
                return;
            }
            else if (!DateOperations.isValidUkDate(txtPaymentDate1.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanValidDate, true);
                return;
            }
            CalculateEndDate();

        }

        #endregion

        #region"Txt Payment Date Selected Text Changed"

        protected void txtPaymentDate_TextChanged(object sender, EventArgs e)
        {
            if (txtPaymentVlue1.Text.Equals(string.Empty) || txtPaymentDate1.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.paymentPlanFirstPayment, true);
                return;
            }
            TextBox txt = new TextBox();
            //TextBox txtBoxDate;      
            //GridViewRow gridRow;
            //DateTime datePrevious;

            GridViewRow gvr = ((TextBox)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;
            txt = (TextBox)gvFlexiblePayment.Rows[index].FindControl("txtPaymentDate");
            if (txt.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.paymentPlanValidDate, true);
                return;
            }
            DateTime date = DateOperations.ConvertStringToDate(txt.Text);
            if (!DateOperations.FutureDateValidation(date))
            {
                SetMessage(UserMessageConstants.paymentPlandatePayment, true);
                return;
            }
            else if (!DateOperations.isValidUkDate(txt.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanValidDate, true);
                return;
            }
            else
            {
                FutureDateValidations();
            }
            CalculateEndDate();
        }

        #endregion

        #region"Txt Review Date Text Changed"

        protected void txtReviewDate_TextChanged(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            DateTime startDate = new DateTime();
            if (!txtPaymentDate1.Text.Equals(string.Empty))
            {
                startDate = DateOperations.ConvertStringToDate(txtPaymentDate1.Text);
            }
            else
            {
                SetMessage(UserMessageConstants.paymentInvalidPayment1Date, true);
                return;
            }
            date = DateOperations.ConvertStringToDate(txtReviewDate.Text);
            if (!DateOperations.FutureDateValidation(date))
            {
                SetMessage(UserMessageConstants.paymentPlanReviewDateFuture, true);
                return;
            }
            else if (!DateOperations.isValidUkDate(txtReviewDate.Text))
            {
                SetMessage(UserMessageConstants.paymentPlanValidReviewDate, true);
                return;
            }
            else if (CompareTwoDates(date, startDate) < 0)
            {
                SetMessage(UserMessageConstants.paymentPlanReviewDateBefore1stPayment, true);
                return;
            }

        }

        #endregion

        #region"Btn Print Click"

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.flexiblePaymentReport, false);
        }

        #endregion

        #region"Btn Update Click"

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ShowCreateUpdateButton();
            EnablePaymentPlan();
            EnableCancelButton();
        }

        #endregion

        #region"Btn Close Click"

        protected void btnClose_Click(object sender, EventArgs e)
        {
            if (UpdatePaymentPlan(false))
            {
                InitLookups();
                ResetFields();
                SetWeeklyRent();
                CheckPaymentPlanPrint();
                SetCreationDate();
                SetDefaultParameters();

                Session[SessionConstants.paymentPlanId] = null;
                Session[SessionConstants.UpdateFlagPaymentPlan] = null;
                Response.Redirect(PathConstants.casehistory, false);
            }
            else
            {
                SetMessage(UserMessageConstants.problemInUpdatePaymentPlan, true);
            }

        }
        #endregion

        #region"Btn Create Update Click"

        protected void btnCreateUpdate_Click(object sender, EventArgs e)
        {
            if (!this.Validate())
            {
                return;
            }
            else
            {
                if (UpdatePaymentPlan(true))
                {
                    SetMessage(UserMessageConstants.UpdateSuccessfulPaymentPlan, false);
                    DisablePaymentPlan();
                    ShowUpdateButton();
                    DisableCancelButton();
                    Response.Redirect(PathConstants.casehistory + "?cmd=OpenCase", false);
                }
                else
                {
                    SetMessage(UserMessageConstants.problemInUpdatePaymentPlan, true);
                }
            }
        }

        #endregion

        #region"Btn Cancel Click"

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(Session[SessionConstants.UpdateFlagPaymentPlan]).Equals("true") &&
                Convert.ToString(Session[SessionConstants.PaymentPlanTypePaymentPlan]).Equals("Regular"))
            {
                ListItem item = ddlPaymentType.Items.FindByText("Regular");
                invokeEvent(false, item.Value);
            }
            else
            {
                if (Session[SessionConstants.paymentPlanId] != null)
                {
                    ResetFields();
                    LoadPaymentPlan(true);
                    ShowUpdateButton();
                    DisablePaymentPlan();
                    DisableCancelButton();
                }
                else
                {
                    ResetFields();
                }
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Load Default Flexible Payments"

        public void LoadDefaultFlexiblePayments()
        {
            ArrayList list = new ArrayList();
            list.Add(0);
            gvFlexiblePayment.DataSource = list;
            gvFlexiblePayment.DataBind();
            ViewState["list"] = list;
        }

        #endregion

        #region"Check Payment Plan"

        public bool CheckPaymentPlan()
        {
            paymentManager = new Payments();
            return paymentManager.CheckPaymentPlan(base.GetTenantId());
        }

        #endregion

        #region"Create Payment Plan"

        public bool CreatePaymentPlan()
        {
            bool success = false;
            try
            {
                AM_PaymentPlan paymentPlan = new AM_PaymentPlan();
                AM_PaymentPlanHistory paymentPlanlHistory = new AM_PaymentPlanHistory();

                paymentPlan = SetPaymentPlan();
                paymentPlanlHistory = SetPaymentPlanHistory();

                Payments payments = new Payments();
                List<AM_Payment> listPayment = new List<AM_Payment>();
                listPayment = SavePayments(paymentPlan);

                if (payments.AddNewPayment(paymentPlan, paymentPlanlHistory, listPayment) == 1)
                {
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingSavingPaymentPlan, true);

                }

            }
            return success;
        }

        #endregion

        #region"Update Payment Plan"

        public bool UpdatePaymentPlan(bool ActivePaymentPlan)
        {
            bool success = false;
            try
            {
                AM_PaymentPlan paymentPlan = new AM_PaymentPlan();
                AM_PaymentPlanHistory paymentPlanlHistory = new AM_PaymentPlanHistory();

                paymentPlan.TennantId = base.GetTenantId();
                paymentPlanlHistory.TennantId = base.GetTenantId();

                //   DateTime dateTime = new DateTime();

                paymentPlan.PaymentPlanType = Convert.ToInt32(ddlPaymentType.SelectedValue);
                paymentPlanlHistory.PaymentPlanType = Convert.ToInt32(ddlPaymentType.SelectedValue);

                paymentPlan.StartDate = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);
                paymentPlanlHistory.StartDate = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);

                paymentPlan.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);
                paymentPlanlHistory.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);

                paymentPlan.AmountToBeCollected = Convert.ToDouble(txtAmountCollected.Text);
                paymentPlanlHistory.AmountToBeCollected = Convert.ToDouble(txtAmountCollected.Text);

                paymentPlan.WeeklyRentAmount = Convert.ToDouble(txtWeeklyRentAmount.Text);
                paymentPlanlHistory.WeeklyRentAmount = Convert.ToDouble(txtWeeklyRentAmount.Text);

                paymentPlan.ReviewDate = DateTime.ParseExact(txtReviewDate.Text, "dd/MM/yyyy", null);
                paymentPlanlHistory.ReviewDate = DateTime.ParseExact(txtReviewDate.Text, "dd/MM/yyyy", null);

                paymentPlan.CreatedDate = DateTime.Now;
                paymentPlanlHistory.CreatedDate = DateTime.Now;

                paymentPlan.ModifiedDate = DateTime.Now;
                paymentPlanlHistory.ModifiedDate = DateTime.Now;

                paymentPlan.IsCreated = true;
                paymentPlanlHistory.IsCreated = true;

                paymentPlan.FrequencyLookupCodeId = Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue);
                paymentPlanlHistory.FrequencyLookupCodeId = Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue);

                paymentPlan.CreatedBy = base.GetCurrentLoggedinUser();
                paymentPlanlHistory.CreatedBy = base.GetCurrentLoggedinUser();

                paymentPlan.ModifiedBy = base.GetCurrentLoggedinUser();
                paymentPlanlHistory.ModifiedBy = base.GetCurrentLoggedinUser();

                paymentPlan.FirstCollectionDate = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);
                paymentPlanlHistory.FirstCollectionDate = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);

                paymentPlan.LastPaymentDate = DateOperations.ConvertStringToDate(Convert.ToString(Session[SessionConstants.lastPaymentDate]));
                paymentPlanlHistory.LastPaymentDate = DateOperations.ConvertStringToDate(Convert.ToString(Session[SessionConstants.lastPaymentDate]));

                paymentPlan.RentBalance = Convert.ToDouble(txtArrearsBalance.Text);
                paymentPlanlHistory.RentBalance = Convert.ToDouble(txtArrearsBalance.Text);

                paymentPlan.LastPayment = Convert.ToDouble(Session[SessionConstants.lastPayment]);
                paymentPlanlHistory.LastPayment = Convert.ToDouble(Session[SessionConstants.lastPayment]);

                paymentPlan.RentPayable = Convert.ToDouble(txtRentPayable.Text);
                paymentPlanlHistory.RentPayable = Convert.ToDouble(txtRentPayable.Text);

                paymentPlan.ArrearsCollection = Convert.ToDouble(txtArrearsCollection.Text);
                paymentPlanlHistory.ArrearsCollection = Convert.ToDouble(txtArrearsCollection.Text);

                paymentPlan.IsActive = ActivePaymentPlan;
                paymentPlanlHistory.IsActive = ActivePaymentPlan;

                Payments payments = new Payments();

                List<AM_Payment> listPayment = SavePayments(paymentPlan);

                if (Session[SessionConstants.paymentPlanId] != null)
                {
                    if (payments.UpdatePaymentPlan(Convert.ToInt32(Session[SessionConstants.paymentPlanId]), paymentPlan, paymentPlanlHistory, listPayment))
                    {
                        success = true;
                    }
                    else
                    {
                        success = false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingSavingPaymentPlan, true);
                }
            }
            return success;
        }

        #endregion

        #region"Installments"

        public DateTime Installments(double totalRentAmount, double amountToBeCollected, string duration, DateTime firstCollection)
        {
            DateTime time = new DateTime();
            try
            {
                double result = CreateInstallments(totalRentAmount, amountToBeCollected);
                time = DateOperations.AddDate(result, duration, firstCollection);

                return time;
            }
            catch (ArithmeticException ax)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ax, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingInInstallments, true);
                }
            }
            return time;
        }

        #endregion

        #region"Create Installments"

        public double CreateInstallments(double totalRentAmount, double amountToBeCollected)
        {
            try
            {
                if (totalRentAmount % amountToBeCollected == 0)
                {
                    return totalRentAmount / amountToBeCollected;                    
                }
                else if (totalRentAmount % amountToBeCollected > 0)
                {
                    return Math.Ceiling(totalRentAmount / amountToBeCollected);
                }
                return 0.0;
            }
            catch (ArithmeticException ax)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ax, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingInInstallments, true);
                }
            }
            return 0.0;
        }

        #endregion

        #region"Get First Payment"

        protected DateTime GetFirstPaymentDate()
        {
            GridViewRow gvRow = gvFlexiblePayment.Rows[0];
            TextBox txtBoxDate = (TextBox)gvRow.FindControl("txtPaymentDate");
            return DateTime.ParseExact(txtBoxDate.Text, "dd/MM/yyyy", null);
        }

        #endregion

        #region"Calculate End Date"

        private void CalculateEndDate()
        {
            if (!ValidateForEndDate())
            {
                return;
            }
            double arrearAmount = Convert.ToDouble(txtArrearsBalance.Text);
            if (Convert.ToDouble(txtAmountCollected.Text) == 0.0)
            {
                if (GetRemainingBalance() != 0.0)
                {
                    SetMessage(UserMessageConstants.paymentPlanUnableToCreate, true);
                    return;
                }
            }
            txtEndDate.Text = this.GetEndDate().ToString("dd/MM/yyyy");
        }

        #endregion

        #region"Get End Date"

        protected DateTime GetEndDate()
        {
            DateTime lastPaymentDate = this.GetLatestCustomePaymentDate();

            //updated by: umair
            //update date:27 september 2011
            //to get months difference bw first and last flexible payment date
            if (DateOperations.CompareDates(DateTime.Now, lastPaymentDate))
            {
                ViewState[ViewStateConstants.FlexiblePaymentMonths] = 0;//1;

            }
            else
            {
                ViewState[ViewStateConstants.FlexiblePaymentMonths] = Convert.ToInt32(lastPaymentDate.Month - DateTime.Now.Month);
            }
            //end update


            //int arrearAmount = Convert.ToInt32(Session[SessionConstants.rentbalance]);
            double remainingBalance = this.GetRemainingBalance();
   
            if (remainingBalance <= 0)
            {
                return lastPaymentDate;
            }
            //updated by:umair
            //date:23 Aug 2011
            //double installments = CreateInstallments(Convert.ToDouble(remainingBalance), Convert.ToDouble(txtArrearsCollection.Text));
            double installments = CreateInstallments(Convert.ToDouble(remainingBalance), Convert.ToDouble(txtAmountCollected.Text));
            //end update
            lastPaymentDate = DateOperations.AddDate(installments, ddlFrequencyRegularPayment.SelectedItem.Text, lastPaymentDate);
            return lastPaymentDate;
        }

        #endregion

        #region"Save Custom Payments"

        protected bool SaveCustomPayments(int paymentPlanId, int paymentPlanHistoryId)
        {
            try
            {
                TextBox txtBoxDate;
                TextBox txtBoxValue;
                AM_Payment payment = new AM_Payment();
                paymentManager = new Payments();

                payment.Date = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);
                payment.Payment = Convert.ToDouble(txtPaymentVlue1.Text);
                payment.PaymentPlanId = paymentPlanId;
                payment.PaymentPlanHistoryId = paymentPlanHistoryId;
                paymentManager.AddPayment(payment);

                foreach (GridViewRow gRow in gvFlexiblePayment.Rows)
                {
                    paymentManager = new Payments();
                    txtBoxDate = (TextBox)gRow.FindControl("txtPaymentDate");
                    txtBoxValue = (TextBox)gRow.FindControl("txtPaymentValue");

                    if (!txtBoxValue.Text.Equals("") && !txtBoxDate.Text.Equals(""))
                    {

                        payment.Date = DateTime.ParseExact(txtBoxDate.Text, "dd/MM/yyyy", null);
                        payment.Payment = Convert.ToDouble(txtBoxValue.Text);
                        payment.PaymentPlanId = paymentPlanId;
                        payment.PaymentPlanHistoryId = paymentPlanHistoryId;
                        paymentManager.AddPayment(payment);
                    }
                }


                //updated by: umair
                //update date:27 september 2011
                //to get months difference bw first and last flexible payment date

                DateTime firstDateTime = this.GetLatestCustomePaymentDate();
                DateTime paymentDate = new DateTime(firstDateTime.Year, firstDateTime.Month, firstDateTime.Day);

                //if (DateOperations.CompareDates(payment.Date,  paymentDate))
                if (DateOperations.CompareDates(DateTime.Now, paymentDate))
                {
                    ViewState[ViewStateConstants.FlexiblePaymentMonths] = 0;//1;

                }
                else
                {
                    ViewState[ViewStateConstants.FlexiblePaymentMonths] = Convert.ToInt32(paymentDate.Month - DateTime.Now.Month);
                }
                //end update

            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingSavingPaymentInstallments, true);
                }
            }
            if (IsException)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region"Save Payments"

        public List<AM_Payment> SavePayments(AM_PaymentPlan paymentPlan)
        {
            List<AM_Payment> listPayment = new List<AM_Payment>();
            try
            {
                //if (!SaveCustomPayments(paymentPlanId, paymentPlanHistoryId))
                //    return false;                

                TextBox txtBoxDate;
                TextBox txtBoxValue;
                AM_Payment payment = new AM_Payment();
                paymentManager = new Payments();

                payment.Date = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);
                payment.Payment = Convert.ToDouble(txtPaymentVlue1.Text);
                payment.IsFlexible = true;
                listPayment.Add(payment);
                DateTime tempDate = DateTime.Now;
                foreach (GridViewRow gRow in gvFlexiblePayment.Rows)
                {
                    AM_Payment loopPayment = new AM_Payment();
                    txtBoxDate = (TextBox)gRow.FindControl("txtPaymentDate");
                    txtBoxValue = (TextBox)gRow.FindControl("txtPaymentValue");

                    if (!txtBoxValue.Text.Equals("") && !txtBoxDate.Text.Equals(""))
                    {
                        loopPayment.Date = DateTime.ParseExact(txtBoxDate.Text, "dd/MM/yyyy", null);
                        loopPayment.Payment = Convert.ToDouble(txtBoxValue.Text);
                        loopPayment.IsFlexible = true;
                        listPayment.Add(loopPayment);
                        tempDate = loopPayment.Date;
                    }
                }

                DateTime firstDateTime = this.GetLatestCustomePaymentDate();
                DateTime paymentDate = new DateTime(firstDateTime.Year, firstDateTime.Month, firstDateTime.Day);

                //updated by: umair
                //update date:27 september 2011
                //to get months difference bw first and last flexible payment date
                if (DateOperations.CompareDates(DateTime.Now, paymentDate))
                {
                    ViewState[ViewStateConstants.FlexiblePaymentMonths] = 0;//1;
                    
                }
                else
                {
                    ViewState[ViewStateConstants.FlexiblePaymentMonths] = Convert.ToInt32(paymentDate.Month - DateTime.Now.Month);
                }
                //end update

                double arrearAmount = GetRemainingBalance();
                int totalInstallments = Convert.ToInt32(CreateInstallments(arrearAmount, Convert.ToDouble(txtAmountCollected.Text)));
                

                for (int i = 0; i < totalInstallments; i++)
                {
                    AM_Payment paymentRegular = new AM_Payment();
                    paymentRegular.Date = DateOperations.AddDate(Convert.ToDouble(i + 1), ddlFrequencyRegularPayment.SelectedItem.Text, paymentDate);
                    //************************************************************************************************************
                    //if (i + 1 == totalInstallments)
                    //{
                    //    double reminder = arrearAmount % (Convert.ToDouble(txtAmountCollected.Text) + Convert.ToDouble(txtRentPayable.Text));
                    //    if (reminder > 0)
                    //    {
                    //        paymentRegular.Payment = Math.Round(reminder, 2);
                    //    }
                    //}
                    //else
                    //{
                    //    paymentRegular.Payment = (Convert.ToDouble(txtAmountCollected.Text) + Convert.ToDouble(txtRentPayable.Text));
                    //}
                    //********************************************************************************************************************
                    
                    
                    if (i + 1 == totalInstallments)
                    {
                        if (ddlFrequencyRegularPayment.SelectedItem.Text != "Full Amount")
                        {
                            double reminder = calculateReminder();
                            if (reminder > 0.0)
                            {
                                paymentRegular.Payment = Math.Round(reminder, 2);
                            }
                            else if (reminder == 0.0)
                            {
                                if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fornightly")
                                {
                                    paymentRegular.Payment = Convert.ToDouble(CalcArrearCollection_Fornightly());
                                }
                                else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
                                {
                                    paymentRegular.Payment = Convert.ToDouble(CalcArrearCollection_Fornightly());
                                }
                                else
                                {
                                    paymentRegular.Payment = (Convert.ToDouble(txtAmountCollected.Text) + Convert.ToDouble(txtRentPayable.Text));
                                }
                            }
                        }
                        else
                        {
                            paymentRegular.Payment = Convert.ToDouble(txtRentPayable.Text) + Convert.ToDouble(txtArrearsBalance.Text);
                        }
                    }
                    else
                    {
                        if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
                        {
                            paymentRegular.Payment = Convert.ToDouble(CalcArrearCollection_Fornightly());
                        }
                        else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
                        {
                            paymentRegular.Payment = Convert.ToDouble(CalcArrearCollection_Weekly());
                        }
                        else
                        {
                            paymentRegular.Payment = (Convert.ToDouble(txtAmountCollected.Text) + Convert.ToDouble(txtRentPayable.Text));
                        }
                    }

                    paymentRegular.IsFlexible = false;
                    listPayment.Add(paymentRegular);
                    tempDate = paymentRegular.Date;
                }
 
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptingSavingPaymentInstallments, true);
                }
            }
            return listPayment;
        }

        #endregion

        #region"Get Remaining Balance"

        protected double GetRemainingBalance()
        {
            double amount = GetFlexiblePaymentSum();
            double remainingBalance = Convert.ToDouble(txtArrearsBalance.Text) - amount;
            //update by:umair
            //update date:27/09/2011
            //reason:to add the rent if it flexible payment crossed 2 months
            double months =Convert.ToDouble(ViewState[ViewStateConstants.FlexiblePaymentMonths]);
            if(months!=0) 
            {
                double MonthlyRentAmount=Math.Round(Math.Abs(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount])),2);
                double MonthlyHBAmount=Math.Round(Math.Abs(Convert.ToDouble(Session[SessionConstants.HBCyclePayment])),2);
                double rentpayble=MonthlyRentAmount-MonthlyHBAmount;
                rentpayble=(rentpayble<0)?0:rentpayble;
                remainingBalance=remainingBalance +(months*rentpayble);
            }    
            //end update
            
            return remainingBalance;
        }

        #endregion

        #region"Get Latest Customer Payment Date"

        protected DateTime GetLatestCustomePaymentDate()
        {
            TextBox txtBoxDate;
            TextBox txtBoxValue;
            DateTime lastDate = DateTime.Now;

            if (!txtPaymentVlue1.Text.Equals(string.Empty) && !txtPaymentDate1.Text.Equals(string.Empty))
            {
                lastDate = DateOperations.ConvertStringToDate(txtPaymentDate1.Text);
            }

            foreach (GridViewRow gRow in gvFlexiblePayment.Rows)
            {
                txtBoxDate = (TextBox)gRow.FindControl("txtPaymentDate");
                txtBoxValue = (TextBox)gRow.FindControl("txtPaymentValue");

                if (txtBoxValue.Text.Equals("") || txtBoxDate.Text.Equals(""))
                {
                    continue;
                }
                lastDate = DateOperations.ConvertStringToDate(txtBoxDate.Text);
            }
            return lastDate;
        }

        #endregion

        #region"Validate For End Date"
        // This validation is requred because the requred fields for endate calculation are different from overall required fields
        protected bool ValidateForEndDate()
        {


            bool isValid = true;

            if (!ValidateEmptyGrid())
            {
                SetMessage(UserMessageConstants.paymentInvalidFlexiblePayments, true);
                isValid = false;
            }

            if (txtPaymentVlue1.Text.Equals(string.Empty) || txtPaymentDate1.Text.Equals(string.Empty))
            {
                SetMessage(UserMessageConstants.paymentPlanFirstPayment, true);
                isValid = false;
            }
            //else if (GetRemainingBalance() < 0)
            //{
            //    SetMessage(UserMessageConstants.paymentPlanOverFlexibleAmounts, true);
            //    isValid = false;
            //}
            else if (txtAmountCollected.Text.Equals(""))
            {
                isValid = false;
            }
            return isValid;
        }

        #endregion

        #region"Control Load"

        public void ControlLoad(string value)
        {
            if (Convert.ToString(Session[SessionConstants.UpdateFlagPaymentPlan]).Equals("true"))
            //&&                Convert.ToString(Session[SessionConstants.PaymentPlanTypePaymentPlan]).Equals("Flexible"))
            {
                FirstLookPaymentPlan(true);
                EnablePaymentPlan();
                ShowCreateUpdateButton();
                EnableCancelButton();
            }
            else
            {
                InitLookups();
                ResetFields();
                SetWeeklyRent();
                CheckPaymentPlanPrint();
                SetCreationDate();
                SetDefaultParameters();
            }
            ddlPaymentType.SelectedValue = value;
        }

        #endregion

        #region"Validate"

        protected bool Validate()
        {
            bool isValid = true;
            //if (gvFlexiblePayment.Rows.Count > 0)
            //{
            //    GridViewRow gRow = gvFlexiblePayment.Rows[0];
            //    TextBox txtBoxDate = (TextBox)gRow.FindControl("txtPaymentDate");
            //    TextBox txtBoxValue = (TextBox)gRow.FindControl("txtPaymentValue");
            //}

            if (txtPaymentVlue1.Text.Equals(string.Empty) || txtPaymentDate1.Text.Equals(string.Empty))
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentPlanFirstPayment, true);
                return isValid;
            }
            //else if (GetRemainingBalance() < 0)
            //{
            //    isValid = false;
            //    SetMessage(UserMessageConstants.paymentPlanOverFlexibleAmounts, true);
            //    return isValid;
            //}

            else if (ddlPaymentType.SelectedValue == "-1")
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentPlanTypeSelection, true);
                return isValid;
            }

            else if (ddlFrequencyRegularPayment.SelectedValue == "-1")
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentProvideFrequency, true);
                return isValid;
            }
           
            else if (!validateInput.Validate_Currency_Text_Box(txtWeeklyRentAmount.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Monthly Rent.", true);
                return isValid;
            }
            else if (Convert.ToDouble(txtWeeklyRentAmount.Text) == 0.0 || Convert.ToDouble(txtWeeklyRentAmount.Text) < 0.0)
            {
                isValid = false;
                SetMessage("'Monthly Rent' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtRentPayable.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Rent Payable.", true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtArrearsBalance.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Owed To BHA.", true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtAmountCollected.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Agreement Amount.", true);
                return isValid;
            }

            else if (Convert.ToDouble(txtAmountCollected.Text) == 0.0 || Convert.ToDouble(txtAmountCollected.Text) < 0.0)
            {
                isValid = false;
                SetMessage("'Agreed Amount' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return isValid;
            }

            else if (!validateInput.Validate_Currency_Text_Box(txtArrearsCollection.Text))
            {
                isValid = false;
                SetMessage(UserMessageConstants.currencyIsNotInCorrectFormat + " Arrears Collection.", true);
                return isValid;
            }

            else if (Convert.ToDouble(txtArrearsCollection.Text) == 0.0 || Convert.ToDouble(txtArrearsCollection.Text) < 0.0)
            {
                isValid = false;
                SetMessage("'Arrears Collection' " + UserMessageConstants.paymentAmountgretaterThanZero, true);
                return isValid;
            }

            //else if (Convert.ToDouble(txtAmountCollected.Text) > Convert.ToDouble(txtArrearsBalance.Text))
            //{
            //    isValid = false;
            //    SetMessage(UserMessageConstants.paymentPlanAmountToBeCollectedgreater, true);
            //    return isValid;
            //}
            else if (txtReviewDate.Text.Equals(""))
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentPlanReviewDate, true);
                return isValid;
            }
            else if (txtWeeklyRentAmount.Text.Equals("") || Convert.ToDouble(txtWeeklyRentAmount.Text) < 0)
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentPlanWeeklyRentAmount, true);
                return isValid;
            }
         
            else if (!FutureDateValidations())
            {
                isValid = false;
                return isValid;
            }

            else if (!ValidateEmptyGrid())
            {
                isValid = false;
                SetMessage(UserMessageConstants.paymentInvalidFlexiblePayments, true);
                return isValid;
            }
            else if (!FutureDateValidations())
            {
                SetMessage(UserMessageConstants.paymentPlandatePayment, true);
                return false;
            }
            //else if (!CheckCompleteAmount())
            //{
            //    SetMessage(UserMessageConstants.paymentPlanOverFlexibleAmounts, true);
            //    return false;
            //}
            return isValid;
        }


        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Compare Two Dates"

        public int CompareTwoDates(DateTime date1, DateTime date2)
        {
            if (DateTime.Compare(date1, date2) < 0)
            {
                return -1;
            }
            else if (DateTime.Compare(date1, date2) == 0)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        #endregion

        #region"Reset Fields"

        public void ResetFields()
        {
            Label lbl = new Label();
            Label emptyLabel = new Label();
            ImageButton imgButton = new ImageButton();
            int count = 1;
            ArrayList list = new ArrayList();
            list.Add(0);

            ddlPaymentType.SelectedValue = "-1";
            txtPaymentVlue1.Text = string.Empty;
            txtPaymentDate1.Text = string.Empty;
            gridDiv.Style["height"] = "30px";
            gvFlexiblePayment.DataSource = list;
            gvFlexiblePayment.DataBind();

            txtEndDate.Text = string.Empty;
            ddlFrequencyRegularPayment.SelectedValue = "-1";
            txtAmountCollected.Text = string.Empty;
            txtWeeklyRentAmount.Text = string.Empty;
            txtReviewDate.Text = string.Empty;
        }

        #endregion

        #region"Set Case Payment Plan"

        public bool SetCasePaymentPlan()
        {
            paymentManager = new Payments();
            return paymentManager.SetCasePaymentPlan(base.GetTenantId(), base.GetCurrentLoggedinUser());
        }

        #endregion

        #region"Future Date Validations"

        public bool FutureDateValidations()
        {
            TextBox txt = new TextBox();
            TextBox txtBoxDate;
            GridViewRow gridRow;
            DateTime datePrevious;
            DateTime date;
            int result = 0;
            bool invalid = false;

            for (int i = 0; i < gvFlexiblePayment.Rows.Count; i++)
            {
                if (i == 0)
                {
                    gridRow = gvFlexiblePayment.Rows[i];
                    txtBoxDate = (TextBox)gridRow.FindControl("txtPaymentDate");
                    if (!txtPaymentDate1.Text.Equals(string.Empty) && !txtBoxDate.Text.Equals(string.Empty))
                    {
                        datePrevious = DateOperations.ConvertStringToDate(txtPaymentDate1.Text);
                        date = DateOperations.ConvertStringToDate(txtBoxDate.Text);
                        result = CompareTwoDates(date, datePrevious);
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    txtBoxDate = (TextBox)gvFlexiblePayment.Rows[i].FindControl("txtPaymentDate");
                    txt = (TextBox)gvFlexiblePayment.Rows[i - 1].FindControl("txtPaymentDate");
                    if (!txtBoxDate.Text.Equals(string.Empty) && !txt.Text.Equals(string.Empty))
                    {
                        datePrevious = DateOperations.ConvertStringToDate(txt.Text);
                        date = DateOperations.ConvertStringToDate(txtBoxDate.Text);
                        result = CompareTwoDates(date, datePrevious);
                    }
                    else
                    {
                        break;
                    }
                }
                if (result < 0)
                {
                    SetMessage(UserMessageConstants.paymentPlandatePayment, true);
                    invalid = true;
                    break;
                }
                else if (result == 0)
                {
                    SetMessage(UserMessageConstants.paymentPlanDifferentPaymentDate, true);
                    invalid = true;
                    break;
                }
            }
            if (invalid)
            {
                return false;
            }
            else
                return true;
        }

        #endregion

        #region"Validate Empty Grid"

        public bool ValidateEmptyGrid()
        {
            TextBox txtBoxDate;
            TextBox txtBoxValue;
            bool paymentFlag = false;
           // GridViewRow gRow = gvFlexiblePayment.Rows[0];

            for (int i = 0; i < gvFlexiblePayment.Rows.Count; i++)
            {
                txtBoxDate = (TextBox)gvFlexiblePayment.Rows[i].FindControl("txtPaymentDate");
                txtBoxValue = (TextBox)gvFlexiblePayment.Rows[i].FindControl("txtPaymentValue");

                if (txtBoxDate.Text.Equals(string.Empty) && txtBoxValue.Text.Equals(string.Empty))
                {
                    continue;
                }
                else if (!txtBoxDate.Text.Equals(string.Empty))
                {
                    if (txtBoxValue.Text.Equals(string.Empty))
                    {
                        paymentFlag = true;
                        break;
                    }
                }
                else if (!txtBoxValue.Text.Equals(string.Empty))
                {
                    if (txtBoxDate.Text.Equals(string.Empty))
                    {
                        paymentFlag = true;
                        break;
                    }
                }
            }
            if (paymentFlag)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region"get Flexible Payment Sum"

        public double GetFlexiblePaymentSum()
        {
            TextBox txtBoxDate;
            TextBox txtBoxValue;

            double amount = 0.0;
            if (!txtPaymentVlue1.Text.Equals(string.Empty) && !txtPaymentDate1.Text.Equals(string.Empty))
            {
                amount = Convert.ToDouble(txtPaymentVlue1.Text);
            }
            foreach (GridViewRow gRow in gvFlexiblePayment.Rows)
            {
                txtBoxDate = (TextBox)gRow.FindControl("txtPaymentDate");
                txtBoxValue = (TextBox)gRow.FindControl("txtPaymentValue");

                if (txtBoxValue.Text.Equals("") || txtBoxDate.Text.Equals(""))
                {
                    continue;
                }
                amount += Convert.ToDouble(txtBoxValue.Text);
            }

            return amount;
        }

        #endregion

        #region"Check Complete Amounts"

        public bool CheckCompleteAmount()
        {
            double amount = 0.0;
            if (!txtAmountCollected.Text.Equals(string.Empty))
            {
                amount = GetFlexiblePaymentSum() + Convert.ToDouble(txtAmountCollected.Text);
            }
            else
            {
                amount = GetFlexiblePaymentSum();
            }
            if (amount > base.GetRentBalance())
                return false;
            else
                return true;

        }

        #endregion

        #region"Disable Payment Plan"

        public void DisablePaymentPlan()
        {
            ddlPaymentType.Enabled = false;
            txtEndDate.Enabled = false;
            txtReviewDate.Enabled = false;
            txtAmountCollected.Enabled = false;
            ddlFrequencyRegularPayment.Enabled = false;
            txtWeeklyRentAmount.Enabled = false;
            gvFlexiblePayment.Enabled = false;
            txtPaymentVlue1.Enabled = false;
            txtPaymentDate1.Enabled = false;
            txtRentPayable.Enabled = false;
            txtArrearsBalance.Enabled = false;
            txtArrearsCollection.Enabled = false;
        }

        #endregion

        #region"Enable Payment Plan"

        public void EnablePaymentPlan()
        {
            ddlPaymentType.Enabled = true;
            //    txtFirstCollectionDate.Enabled = true;
            //  txtStartDate.Enabled = true;
            txtReviewDate.Enabled = true;
            txtAmountCollected.Enabled = true;
            ddlFrequencyRegularPayment.Enabled = true;
            txtWeeklyRentAmount.Enabled = true;
            gvFlexiblePayment.Enabled = true;
            txtPaymentVlue1.Enabled = true;
            txtPaymentDate1.Enabled = true;
            txtRentPayable.Enabled = true;
            txtArrearsBalance.Enabled = true;
            txtArrearsCollection.Enabled = true;
        }

        #endregion

        #region"Show Update Button"

        public void ShowUpdateButton()
        {
            btnUpdate.Visible = true;
            btnClose.Visible = true; 
            btnCreatePaymentPlan.Visible = false;
            btnCreateUpdate.Visible = false;
        }

        #endregion

        #region"Show Create Update Button"

        public void ShowCreateUpdateButton()
        {
            btnUpdate.Visible = false;
            btnClose.Visible = false; 
            btnCreatePaymentPlan.Visible = false;
            btnCreateUpdate.Visible = true;
        }

        #endregion

        #region"Show Create Button"

        public void ShowCreateButton()
        {
            btnUpdate.Visible = false;
            btnClose.Visible = false; 
            btnCreatePaymentPlan.Visible = true;
            btnCreateUpdate.Visible = false;
        }

        #endregion

        #region"Enable Cancel Button"

        public void EnableCancelButton()
        {
            btnCancel.Enabled = true;
        }

        #endregion

        #region"Disable Cancel Button"

        public void DisableCancelButton()
        {
            btnCancel.Enabled = false;
        }

        #endregion

        #region"Check Case"

        public bool CheckCase()
        {
            try
            {
                OpenCase openCaseManager = new OpenCase();
                return openCaseManager.CheckCase(base.GetTenantId());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Init Lookups"

        public void InitLookups()
        {
            paymentManager = new Payments();
            ddlFrequencyRegularPayment.DataSource = paymentManager.GetFrequencyData();
            ddlFrequencyRegularPayment.DataTextField = "CodeName";
            ddlFrequencyRegularPayment.DataValueField = "LookupCodeId";
            ddlFrequencyRegularPayment.DataBind();
            ddlFrequencyRegularPayment.Items.Add(new ListItem("Please Select", "-1"));
            ddlFrequencyRegularPayment.SelectedValue = "-1";

            ddlPaymentType.DataSource = paymentManager.GetPaymentPlanType();
            ddlPaymentType.DataTextField = "CodeName";
            ddlPaymentType.DataValueField = "LookupCodeId";
            ddlPaymentType.DataBind();
            ddlPaymentType.Items.Add(new ListItem("Please Select", "-1"));
            ddlPaymentType.SelectedValue = "-1";
        }

        #endregion

        #region"Set Weekly Rent"

        public void SetWeeklyRent()
        {
            //paymentManager = new Payments();
            //txtWeeklyRentAmount.Text = Convert.ToString(paymentManager.GetWeeklyRent(tenantId));
            if (Session[SessionConstants.WeeklyRentAmount] != null)
            {
                txtWeeklyRentAmount.Text = Math.Round(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]), 2).ToString();
            }
            SetRentPayable();
            SetRentBalance();
        }

        #endregion

        #region"Set Rent Payable"

        //public void SetRentPayable()
        //{
        //    if (Session[SessionConstants.WeeklyRentAmount] != null)
        //    {
        //        if (Session[SessionConstants.HBCyclePayment] == null)
        //        {
        //            txtRentPayable.Text = Math.Round(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]), 2).ToString();
        //        }
        //        else
        //        {
        //            txtRentPayable.Text = Math.Abs(Math.Round((Math.Abs(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount])) - Math.Abs(Convert.ToDouble(Session[SessionConstants.HBCyclePayment]))), 2)).ToString();
        //        }
        //    }
        //    SetArrearsCollection();
        //}

        public void SetRentPayable()
        {
            if (Session[SessionConstants.WeeklyRentAmount] != null)
            {
                if (Session[SessionConstants.HBCyclePayment] == null)
                {
                    txtRentPayable.Text = Math.Round(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]), 2).ToString();
                }
                else
                {
                    if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly" || ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
                    {
                        SetRentPayableFrequency();
                    }
                    else
                    {
                        txtRentPayable.Text = Math.Abs(Math.Round((Math.Abs(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount])) - Math.Abs(Convert.ToDouble(Session[SessionConstants.HBCyclePayment]))), 2)).ToString();
                    }
                }
            }
            SetArrearsCollection();
        }

        #endregion

        #region"Set Rent Payable Frequency"

        //public void SetRentPayableFrequency()
        //{
        //    if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
        //    {
        //        double HBCycle = 0.0;
        //        if (Session[SessionConstants.HBCyclePayment] != null || Convert.ToDouble(Session[SessionConstants.HBCyclePayment]) == 0.0)
        //        {
        //            HBCycle = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
        //            double rentPayable = (HBCycle / 30.42) * 14;
        //            txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
        //        }
        //        else
        //        {
        //            txtRentPayable.Text = Math.Round(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]), 2).ToString();
        //        }
        //    }
        //    else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
        //    {
        //        double HBCycle = 0.0;
        //        if (Session[SessionConstants.HBCyclePayment] != null || Convert.ToDouble(Session[SessionConstants.HBCyclePayment]) == 0.0)
        //        {
        //            HBCycle = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
        //            double rentPayable = (HBCycle / 30.42) * 7;
        //            txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
        //        }
        //        else
        //        {
        //            txtRentPayable.Text = Math.Round(Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]), 2).ToString();
        //        }
        //    }
        //    else
        //    {
        //        SetRentPayable();
        //    }
        //    SetArrearsCollection();
        //}


        public void SetRentPayableFrequency()
        {
            if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
            {
                //updated by:umair
                //update date:30 08 2011

                //double MonthlyHB = 0.0;
                //if (Session[SessionConstants.HBCyclePayment] != null)
                /*if (Session[SessionConstants.HBPayment] != null)
                {
                    //MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
                    //double rentPayable = ((Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB) / 30.42) * 14;
                    double FortnightlyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 26;
                    double FortnightlyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 2;
                    double rentPayable = FortnightlyRent - FortnightlyHB;

                    txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
                    SetArrearsCollection();
                }
                else
                {
                    double FortnightlyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 26;
                    double rentPayable = FortnightlyRent;

                    txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
                    SetArrearsCollection();
                }*/
                //update end

                // Updated by Hussain (14/11/2012)
                // Commented out the code written by Umair 30 08 2011 and added the following code

                double fortnightlyRent = 0.0;

                // Option A
                // The fortnightlyRend (Monthly Rent in the formula) is fetched from the server. If no session entry is found, they get from
                // the text field. This maybe the cause that intermentiantly the formula fails when converting between month and fortnightly
                if (Session[SessionConstants.WeeklyRentAmount] != null)
                {
                    fortnightlyRent = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]);
                }
                else
                {
                    fortnightlyRent = Convert.ToDouble(txtWeeklyRentAmount.Text);
                }

                // Option B
                // Taking the value of the monthly rent from the text field instead of from the session. This is so that if the user changes
                // the value of the text box, it reflects in the caluclations
                //fortnightlyRent = Convert.ToDouble(txtWeeklyRentAmount.Text);

                double fortnightlyHB = 0.0;

                if (Session[SessionConstants.HBPayment] != null)
                {
                    // Based on Peter's email dated 15/11/2012, the following has been changed from HBPayment to HBCyclePayment and discussion 
                    // with Adnan, we're using the monthly HB (HBCyclePayment), HBPayment is the HB for 4 weeks. 
                    //fortnightlyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 2.0;
                    fortnightlyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBCyclePayment]));
                }


                double fortnightlyRentPayable = (fortnightlyRent - fortnightlyHB) * 12.0 / 26.0;

                txtRentPayable.Text = Convert.ToString(Math.Round(fortnightlyRentPayable, 2));
                SetArrearsCollection();
                // Update by Hussain end
            }
            else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
            {
                //updated by:umair
                //update date:30 08 2011

                //double MonthlyHB = 0.0;
                //if (Session[SessionConstants.HBCyclePayment] != null)
                /*if (Session[SessionConstants.HBPayment] != null)
                {

                    //MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
                    //double rentPayable = ((Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB) / 30.42) * 7;
                    double WeeklyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 52;
                    double WeeklyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 4;
                    double rentPayable = WeeklyRent - WeeklyHB;

                    txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
                    SetArrearsCollection();
                }
                else
                {
                    double WeeklyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 52;
                    double rentPayable = WeeklyRent;

                    txtRentPayable.Text = Convert.ToString(Math.Round(rentPayable, 2));
                    SetArrearsCollection();
                }*/
                //update end

                // Updated by Hussain (14/11/2012)
                // Commented out the code written by Umair 26 08 2011 and added the following code

                double weeklyRent = 0.0;

                // Option A
                // The fortnightlyRend (Monthly Rent in the formula) is fetched from the server. If no session entry is found, they get from
                // the text field. This maybe the cause that intermentiantly the formula fails when converting between month and weekly
                if (Session[SessionConstants.WeeklyRentAmount] != null)
                {
                    weeklyRent = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]);
                }
                else
                {
                    weeklyRent = Convert.ToDouble(txtWeeklyRentAmount.Text);
                }

                // Option B
                // Taking the value of the monthly rent from the text field instead of from the session. This is so that if the user changes
                // the value of the text box, it reflects in the caluclations
                //weeklyRent = Convert.ToDouble(txtWeeklyRentAmount.Text);

                double weeklyHB = 0.0;

                if (Session[SessionConstants.HBPayment] != null)
                {
                    // Based on Peter's email dated 15/11/2012, the following has been changed from HBPayment to HBCyclePayment and discussion 
                    // with Adnan, we're using the monthly HB (HBCyclePayment), HBPayment is the HB for 4 weeks. 
                    //weeklyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 2.0;
                    weeklyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBCyclePayment]));
                }


                double weeklyRentPayable = (weeklyRent - weeklyHB) * 12.0 / 52.0;

                txtRentPayable.Text = Convert.ToString(Math.Round(weeklyRentPayable, 2));
                SetArrearsCollection();
                // Update by Hussain end
            }
            else
            {
                SetRentPayable();
            }

        }

        #endregion

        #region"Set Rent Balance"

        public void SetRentBalance()
        {
            //if (Session[SessionConstants.OwedTBHA] != null)
            //{
            //    txtArrearsBalance.Text = Convert.ToString(Math.Abs(Convert.ToDouble(Session[SessionConstants.OwedTBHA])));
            //}

            if (Session[SessionConstants.LastMonthNetArrears] != null)
            {
                txtArrearsBalance.Text = Convert.ToString(Math.Abs(Convert.ToDouble(Session[SessionConstants.LastMonthNetArrears])));
            }
        }

        #endregion

        #region"Set Arrears Collection"

        //public void SetArrearsCollection()
        //{
        //    double agreedAmount = 0.0;
        //    if (txtAmountCollected.Text.Equals(string.Empty))
        //    {
        //        agreedAmount = 0.0;
        //    }
        //    else
        //    {
        //        agreedAmount = Convert.ToDouble(txtAmountCollected.Text);
        //    }
        //    txtArrearsCollection.Text = Convert.ToString(Convert.ToDouble(txtRentPayable.Text) + agreedAmount);
        //}

        public void SetArrearsCollection()
        {
            double agreedAmount = 0.0;
            if (txtAmountCollected.Text.Equals(string.Empty))
            {
                agreedAmount = 0.0;
            }
            else
            {
                agreedAmount = Convert.ToDouble(txtAmountCollected.Text);
            }

            if (ddlFrequencyRegularPayment.SelectedValue != "-1")
            {
                if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
                {
                    //Calc Arrear Collection for Fornightly
                    txtArrearsCollection.Text = CalcArrearCollection_Fornightly();

                }
                else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
                {
                    txtArrearsCollection.Text = CalcArrearCollection_Weekly();

                }
                else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Full Amount")
                {
                    txtArrearsCollection.Text = Convert.ToString(Convert.ToDouble(txtRentPayable.Text) + Convert.ToDouble(txtArrearsBalance.Text));

                }
                else
                {
                    txtArrearsCollection.Text = Convert.ToString(Convert.ToDouble(txtRentPayable.Text) + agreedAmount);
                }
            }
            else
            {
                if (txtRentPayable.Text != null && txtRentPayable.Text != "")
                {
                    txtArrearsCollection.Text = Convert.ToString(Convert.ToDouble(txtRentPayable.Text) + agreedAmount);
                }
                else
                {
                    txtArrearsCollection.Text = Convert.ToString(agreedAmount);
                }
            }
        }



        #endregion

        #region"Set Default Parameters"

        public void SetDefaultParameters()
        {
            ListItem payment = ddlPaymentType.Items.FindByText("Regular");
            ddlPaymentType.SelectedValue = payment.Value;

            ListItem frequency = ddlFrequencyRegularPayment.Items.FindByText("Monthly");
            ddlFrequencyRegularPayment.SelectedValue = frequency.Value;
        }

        #endregion

        #region"Set Creation Date"

        public void SetCreationDate()
        {
            if (Session[SessionConstants.CreateDatePaymentPlan] == null)
            {
                lblCreationDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                lblCreationDate.Text = Convert.ToString(Session[SessionConstants.CreateDatePaymentPlan]);
            }
        }

        #endregion

        #region"Check Payment Plan Print"

        public void CheckPaymentPlanPrint()
        {
            try
            {
                paymentManager = new Payments();
                btnPrint.Enabled = paymentManager.CheckPaymentPlan(base.GetTenantId());
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.CheckPaymentPlanPrintPayments, true);
                }
            }

        }

        #endregion

        #region"Load Payment Plan"

        public void LoadPaymentPlan(bool showDateFields)
        {
            try
            {
                paymentManager = new Payments();
                AM_PaymentPlan paymentPlan = paymentManager.GetPaymentPlan(base.GetTenantId());
                if (paymentPlan != null)
                {
                    ddlPaymentType.SelectedValue = paymentPlan.PaymentPlanType.Value.ToString();
                    //   txtStartDate.Text = paymentPlan.StartDate.Value.ToString("dd/MM/yyyy");
                    if (showDateFields)
                    {
                        txtEndDate.Text = paymentPlan.EndDate.ToString("dd/MM/yyyy");
                        txtReviewDate.Text = paymentPlan.ReviewDate.ToString("dd/MM/yyyy");
                    }

                    ddlFrequencyRegularPayment.SelectedValue = paymentPlan.FrequencyLookupCodeId.ToString();
                    txtAmountCollected.Text = paymentPlan.AmountToBeCollected.ToString();
                    txtRentPayable.Text = paymentPlan.RentPayable.ToString();
                    txtArrearsBalance.Text = paymentPlan.RentBalance.ToString();
                    txtWeeklyRentAmount.Text = paymentPlan.WeeklyRentAmount.ToString();
                    txtArrearsCollection.Text = paymentPlan.ArrearsCollection.ToString();
                    lblCreationDate.Text = paymentPlan.CreatedDate.ToString("dd/MM/yyyy");
                    Session[SessionConstants.CreateDatePaymentPlan] = lblCreationDate.Text;
                    Session[SessionConstants.paymentPlanId] = paymentPlan.PaymentPlanId;

                    List<AM_Payment> listPayments = paymentManager.LoadFlexiblePayments(paymentPlan.PaymentPlanId);
                    if (listPayments != null)
                    {
                        if (listPayments.Count > 0)
                        {
                            LoadFlexiblePayments(listPayments);
                        }
                        else
                        {
                            LoadDefaultFlexiblePayments();
                        }
                    }
                }

            }
            catch (EntityException ee)
            {
                throw ee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region"Load Flexible Payments"

        public void LoadFlexiblePayments(List<AM_Payment> listPayments)
        {
            TextBox txtBoxDate;
            TextBox txtBoxValue;
            Label lblPayment;

            txtPaymentVlue1.Text = listPayments[0].Payment.ToString();
            txtPaymentDate1.Text = listPayments[0].Date.ToString("dd/MM/yyyy");

            ArrayList list = new ArrayList();

            for (int i = 1; i < listPayments.Count; i++)
            {
                list.Add(i);
            }
            ViewState["list"] = list;
            gvFlexiblePayment.DataSource = list;
            gvFlexiblePayment.DataBind();

            for (int i = 0; i + 1 < listPayments.Count; i++)
            {
                //gridRow = gvFlexiblePayment.Rows[gvFlexiblePayment.Rows.Count - 1];
                txtBoxDate = (TextBox)gvFlexiblePayment.Rows[i].FindControl("txtPaymentDate");
                txtBoxValue = (TextBox)gvFlexiblePayment.Rows[i].FindControl("txtPaymentValue");
                lblPayment = (Label)gvFlexiblePayment.Rows[i].FindControl("lblPaymentNumber");
                if (i == 0)
                {
                    lblPayment.Text = (i + 2).ToString() + "nd" + " Payment" + ":";
                }
                else if (i == 1)
                {
                    lblPayment.Text = (i + 2).ToString() + "rd" + " Payment" + ":";
                }
                else
                {
                    lblPayment.Text = (i + 2).ToString() + "th" + " Payment" + ":";
                }
                txtBoxDate.Text = listPayments[i + 1].Date.ToString("dd/MM/yyyy");
                txtBoxValue.Text = listPayments[i + 1].Payment.ToString();
            }
        }

        #endregion

        #region"First Look Payment Plan"

        public void FirstLookPaymentPlan(bool isBridge)
        {
            try
            {
                InitLookups();
                if (isBridge)
                {
                    LoadPaymentPlan(false);
                }
                else
                {
                    LoadPaymentPlan(true);
                }
                SetWeeklyRent();
                CheckPaymentPlanPrint();
                DisablePaymentPlan();
                ShowUpdateButton();
                DisableCancelButton();
                
                Session[SessionConstants.UpdateFlagPaymentPlan] = "true";
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.problemLoadingPaymentPlan, true);
                }
            }
        }

        #endregion

        #region"Set Payment Plans"

        public AM_PaymentPlan SetPaymentPlan()
        {
            AM_PaymentPlan paymentPlan = new AM_PaymentPlan();

            paymentPlan.TennantId = base.GetTenantId();
            paymentPlan.PaymentPlanType = Convert.ToInt32(ddlPaymentType.SelectedValue);
            paymentPlan.StartDate = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);
            paymentPlan.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);
            paymentPlan.AmountToBeCollected = Convert.ToDouble(txtAmountCollected.Text);
            paymentPlan.WeeklyRentAmount = Convert.ToDouble(txtWeeklyRentAmount.Text);
            paymentPlan.ReviewDate = DateTime.ParseExact(txtReviewDate.Text, "dd/MM/yyyy", null);
            paymentPlan.CreatedDate = DateTime.Now;
            paymentPlan.ModifiedDate = DateTime.Now;
            paymentPlan.IsCreated = true;
            paymentPlan.FrequencyLookupCodeId = Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue);
            paymentPlan.CreatedBy = base.GetCurrentLoggedinUser();
            paymentPlan.ModifiedBy = base.GetCurrentLoggedinUser();
            paymentPlan.FirstCollectionDate = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);
            paymentPlan.LastPaymentDate = DateOperations.ConvertStringToDate(Convert.ToString(Session[SessionConstants.lastPaymentDate]));
            paymentPlan.RentBalance = Convert.ToDouble(txtArrearsBalance.Text);
            paymentPlan.LastPayment = Convert.ToDouble(Session[SessionConstants.lastPayment]);
            paymentPlan.RentPayable = Convert.ToDouble(txtRentPayable.Text);
            paymentPlan.ArrearsCollection = Convert.ToDouble(txtArrearsCollection.Text);
            return paymentPlan;
        }

        #endregion

        #region"Set Payment Plan History"

        public AM_PaymentPlanHistory SetPaymentPlanHistory()
        {
            AM_PaymentPlanHistory paymentPlanHistory = new AM_PaymentPlanHistory();

            paymentPlanHistory.TennantId = base.GetTenantId();
            paymentPlanHistory.PaymentPlanType = Convert.ToInt32(ddlPaymentType.SelectedValue);
            paymentPlanHistory.StartDate = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);
            paymentPlanHistory.EndDate = DateTime.ParseExact(txtEndDate.Text, "dd/MM/yyyy", null);
            paymentPlanHistory.AmountToBeCollected = Convert.ToDouble(txtAmountCollected.Text);
            paymentPlanHistory.WeeklyRentAmount = Convert.ToDouble(txtWeeklyRentAmount.Text);
            paymentPlanHistory.ReviewDate = DateTime.ParseExact(txtReviewDate.Text, "dd/MM/yyyy", null);
            paymentPlanHistory.CreatedDate = DateTime.Now;
            paymentPlanHistory.ModifiedDate = DateTime.Now;
            paymentPlanHistory.IsCreated = true;
            paymentPlanHistory.FrequencyLookupCodeId = Convert.ToInt32(ddlFrequencyRegularPayment.SelectedValue);
            paymentPlanHistory.CreatedBy = base.GetCurrentLoggedinUser();
            paymentPlanHistory.ModifiedBy = base.GetCurrentLoggedinUser();
            paymentPlanHistory.FirstCollectionDate = DateTime.ParseExact(txtPaymentDate1.Text, "dd/MM/yyyy", null);
            paymentPlanHistory.LastPaymentDate = DateOperations.ConvertStringToDate(Convert.ToString(Session[SessionConstants.lastPaymentDate]));
            paymentPlanHistory.RentBalance = Convert.ToDouble(txtArrearsBalance.Text);
            paymentPlanHistory.LastPayment = Convert.ToDouble(Session[SessionConstants.lastPayment]);
            paymentPlanHistory.RentPayable = Convert.ToDouble(txtRentPayable.Text);
            paymentPlanHistory.ArrearsCollection = Convert.ToDouble(txtArrearsCollection.Text);

            return paymentPlanHistory;

        }

        #endregion

        #endregion


        
        #region "Calculate Reminder"
        public double calculateReminder()
        {
            //update by umair
            //update date:30 08 2011

            //double MonthlyHB = 0.0;
            //double _RentPayable = 0.0;

            //if (Session[SessionConstants.HBCyclePayment] != null)
            //{
            //    if (Convert.ToDouble(Session[SessionConstants.HBCyclePayment]) != 0.0)
            //    {
            //        MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
            //        _RentPayable = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB);
            //    }
            //    else
            //    {
            //        _RentPayable = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]);
            //    }
            //}

            double _RentPayable = Convert.ToDouble(txtRentPayable.Text);
            double _AgreedAmount = txtAmountCollected.Text == "" ? 0.0 : Convert.ToDouble(txtAmountCollected.Text);
            double _TotalAmountToBeCollected = GetRemainingBalance();
            double _totalInstallments;

            if (ddlFrequencyRegularPayment.SelectedItem.Text == "Full Amount")
            {
                _totalInstallments = 1.0;
            }
            else
            {
                _totalInstallments = CreateInstallments(_TotalAmountToBeCollected, _AgreedAmount);
            }

            return _RentPayable + (_TotalAmountToBeCollected - ((_totalInstallments - 1.0) * _AgreedAmount));

            //if (ddlFrequencyRegularPayment.SelectedItem.Text == "Fortnightly")
            //{
            //    return ((_RentPayable * 12) / 52 * 2) + (_TotalAmountToBeCollected - ((_totalInstallments - 1.0) * _AgreedAmount));
            //}
            //else if (ddlFrequencyRegularPayment.SelectedItem.Text == "Weekly")
            //{
            //    return ((_RentPayable * 12) / 52) + (_TotalAmountToBeCollected - ((_totalInstallments - 1.0) * _AgreedAmount));
            //}
            //else
            //{
            //    return _RentPayable + (_TotalAmountToBeCollected - ((_totalInstallments - 1.0) * _AgreedAmount));
            //}
            
            //end update
        }

        #endregion


        //#region "calculate Arrears Collection Fornightly"

        //public string CalcArrearCollection_Fornightly()
        //{
        //    double _RentPayable = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
        //    double _AgreedAmount = txtAmountCollected.Text == "" ? 0.0 : Convert.ToDouble(txtAmountCollected.Text);
        //    double _result = ((_RentPayable * 12) / 52 * 2) + _AgreedAmount;

        //    return Math.Round(_result, 2).ToString();
        //}

        //#endregion

        //#region "calculate Arrears Collection Weekly"

        //public string CalcArrearCollection_Weekly()
        //{

        //    double _RentPayable = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
        //    double _AgreedAmount = txtAmountCollected.Text == "" ? 0.0 : Convert.ToDouble(txtAmountCollected.Text);
        //    double _result = ((_RentPayable * 12) / 52) + _AgreedAmount;

        //    return Math.Round(_result, 2).ToString();
        //}

        //#endregion

        #region "calculate Arrears Collection Fornightly"

        public string CalcArrearCollection_Fornightly()
        {
            //double _RentPayable = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
            double _AgreedAmount = txtAmountCollected.Text == "" ? 0.0 : Convert.ToDouble(txtAmountCollected.Text);
            //double _result = ((_RentPayable * 12) / 52 * 2) + _AgreedAmount;

            //update by: Umair 
            //Update Date:30 08 2011

            //double MonthlyHB = 0.0;
            double _result = 0.0;

            if (txtRentPayable.Text.Equals(string.Empty))
            {
                //if (Session[SessionConstants.HBCyclePayment] != null)
                if (Session[SessionConstants.HBPayment] != null)
                {
                    //MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
                    //_result = (((Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB) / 30.42) * 14) + _AgreedAmount;
                    double FortnightlyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 26;
                    double FortnightlyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 2;
                    _result = (FortnightlyRent - FortnightlyHB) + _AgreedAmount;

                }
                else
                {
                    double FortnightlyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 26;
                    _result = FortnightlyRent + _AgreedAmount;
                }
            }
            else
            {
                _result = Convert.ToDouble(txtRentPayable.Text) + _AgreedAmount;
            }
            //update end
            return Math.Round(_result, 2).ToString();
        }

        #endregion

        #region "calculate Arrears Collection Weekly"

        public string CalcArrearCollection_Weekly()
        {

            // double _RentPayable = Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
            double _AgreedAmount = txtAmountCollected.Text == "" ? 0.0 : Convert.ToDouble(txtAmountCollected.Text);
            //double _result = ((_RentPayable * 12) / 52) + _AgreedAmount;

            //update by: Umair 
            //Update Date:30 08 2011

            //double MonthlyHB = 0.0;
            double _result = 0.0;

            if (txtRentPayable.Text.Equals(string.Empty))
            {

                //if (Session[SessionConstants.HBCyclePayment] != null)
                if (Session[SessionConstants.HBPayment] != null)
                {
                    //MonthlyHB = Convert.ToDouble(Session[SessionConstants.HBCyclePayment]);
                    //_result = (((Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) - MonthlyHB) / 30.42) * 7)+ _AgreedAmount;
                    double WeeklyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 52;
                    double WeeklyHB = Math.Abs(Convert.ToDouble(Session[SessionConstants.HBPayment])) / 4;
                    _result = (WeeklyRent - WeeklyHB) + _AgreedAmount;
                }
                else
                {
                    double WeeklyRent = (Convert.ToDouble(Session[SessionConstants.WeeklyRentAmount]) * 12) / 52;
                    _result = WeeklyRent + _AgreedAmount;
                }
                
            }
            else
            {
                _result = Convert.ToDouble(txtRentPayable.Text) + _AgreedAmount;
                
            }
            //update end

            return Math.Round(_result, 2).ToString();
        }

        #endregion


        #region "Set Amount to be collected"
        public void SetAmountToBeCollected()
        {
            if (ddlFrequencyRegularPayment.SelectedItem.Text == "Full Amount")
            {
                txtAmountCollected.Text = txtArrearsBalance.Text;
            }
            else
            {
                txtAmountCollected.Text = "";
            }
        }
        #endregion

    }
}