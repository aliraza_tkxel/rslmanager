﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddFlexiblePaymentPlan.ascx.cs"
    Inherits="Am.Ahv.Website.controls.casemgt.AddFlexiblePaymentPlan" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="Panel_flexiblePlan" runat="server" Width="100%" Height="100%" BorderWidth="1px"
    BorderColor="Gray" CssClass="topmessagesuccess">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div style="margin: 5px; height: 100%;">
                <div style="height: 21%; position: relative" class="NormalPointFontBold">
                    <asp:Label ID="lblPaymentPlan" runat="server" Text="Payment Plan">
                    </asp:Label>
                </div>
                <div style="text-align: right; margin-right: 15px; margin-bottom: 5px">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <div style="height: 21px; margin-right: 10px" class="NormalPointFont">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="lblCreateionDate" runat="server" Text="Creation Date:"></asp:Label></div>
                    <div style="float: right; width: 130px; text-align: right">
                        <asp:Label ID="lblCreationDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="height: 21%; margin-right: 10px;" class="NormalPointFont">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="lblType" runat="server" Text="Type:">
                        </asp:Label>
                    </div>
                    <div style="float: right; width: 130px; text-align: right;">
                        <asp:Label ID="lblStarPaymentType" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:DropDownList ID="ddlPaymentType" AutoPostBack="true" runat="server" Width="100px"
                            OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged1">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div id="dvPayments" style="width: 100%">
                    <div style="height: 35%; vertical-align: middle; margin-right: 0px; padding-top: 5px"
                        runat="server" id="gridDiv">
                        <div style="float: left; margin-right: 0px; text-align: left">
                            <div style="float: left">
                                <asp:Label ID="lbl1stPayment" runat="server" Text="1st Payment:">
                                </asp:Label>&nbsp;&nbsp;<%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%><asp:Label
                                    ID="lblStarPayment" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                                <asp:TextBox ID='txtPaymentVlue1' runat="server" CssClass="smallerinputBox" OnTextChanged="txtPayment1_TextChanged"
                                    AutoPostBack="true"></asp:TextBox>
                                <asp:TextBox ID="txtPaymentDate1" runat="server" CssClass="smallerinputBox" OnTextChanged="txtPaymentDate1_TextChanged"
                                    AutoPostBack="true"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtPaymentDate1"
                                    Format="dd/MM/yyyy" PopupPosition="TopRight">
                                </asp:CalendarExtender>
                                <asp:GridView ID="gvFlexiblePayment" runat="server" AutoGenerateColumns="false" ShowHeader="False"
                                    GridLines="None">
                                    <Columns>
                                        <asp:TemplateField ControlStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentNumber" runat="server" Text="2nd Payment:"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblEmpty" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID='txtPaymentValue' runat="server" CssClass="smallerinputBox" OnTextChanged="txtPayment_TextChanged"
                                                    AutoPostBack="true"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="smallerinputBox" OnTextChanged="txtPaymentDate_TextChanged"
                                                    AutoPostBack="true"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtPaymentDate"
                                                    Format="dd/MM/yyyy" PopupPosition="TopRight">
                                                </asp:CalendarExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImagimgButton" runat="server" ImageUrl="../../style/images/i.jpg"
                                                    Height="15px" Width="15px" OnClick="ImagimgButton_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear">
                </div>
                <br />
                <div style="height: 29%;" class="casemanagementControlsMainDivs">
                    <div style="float: left; height: 19px" class="NormalPointFont">
                        Frequency:</div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="lblStarFrequency" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:DropDownList ID="ddlFrequencyRegularPayment" runat="server" Width="100px" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlFrequencyRegularPayment_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div style="height: 21%;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        Monthly Rent:
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="lblWeeklyRentAmount" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtWeeklyRentAmount" Enabled="true" AutoPostBack="true" OnTextChanged="txtWeeklyRentAmount_TextChanged"
                            runat="server" CssClass="smallinputBox"></asp:TextBox>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div style="height: 21%;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        Rent Payable:
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="Label1" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtRentPayable" Enabled="true" AutoPostBack="true" OnTextChanged="TxtRentPayable_TextChanged"
                            runat="server" CssClass="smallinputBox"></asp:TextBox>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div style="height: 21%;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        Last Month Net Arrears:
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="Label2" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtArrearsBalance" runat="server" CssClass="smallinputBox" AutoPostBack="True"
                            OnTextChanged="TxtArrearsBalance_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div style="height: 21%;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        Agreement Amount:
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="lblStarAmoutCollected" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtAmountCollected" runat="server" CssClass="smallinputBox" AutoPostBack="True"
                            OnTextChanged="txtAmountCollected_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div style="height: 21%;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        Arrears Collection:
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="Label3" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtArrearsCollection" runat="server" CssClass="smallinputBox" AutoPostBack="True"
                            OnTextChanged="txtArrearsCollection_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div style="height: 29%;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        End Date:
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="smallinputBox" Enabled="false"
                            ReadOnly="True"></asp:TextBox>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div style="height: 21%;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        Review Date:
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="lblStarReviewDate" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtReviewDate" OnTextChanged="txtReviewDate_TextChanged" AutoPostBack="true"
                            runat="server" onkeypress="return false" CssClass="smallinputBox">
                        </asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtenderReviewDate" runat="server" PopupPosition="TopRight"
                            TargetControlID="txtReviewDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                </div>
                <div class="clear">
                </div>
                <div style="height: 21%; margin-top: 10px; margin-bottom: 5px" class="casemanagementControlsMainDivs">
                    <div style="float: left">
                        <asp:Button ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_Click" />
                    </div>
                    <div style="float: left">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                    </div>
                    <div style="float: right">
                        <asp:Button ID="btnCreatePaymentPlan" Width="125px" runat="server" Text="Create Payment Plan"
                            OnClick="btnCreatePaymentPlan_Click" />
                        <asp:Button ID="btnClose" runat="server" OnClick="btnClose_Click" Text="Close" Width="50px" Visible="false" />
                        <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" Width="50px" Visible="false" />
                        <asp:Button ID="btnCreateUpdate" runat="server" OnClick="btnCreateUpdate_Click" Visible="false"
                            Text="Create Payment Plan" Width="125px" />
                    <!--This is part is build by umair for close button--start-->
                    <asp:ConfirmButtonExtender ID="btnClose_ConfirmButtonExtender" runat="server" 
                        TargetControlID="btnClose"
                        DisplayModalPopupID="btnClose_ModelPopupExtender" />
                    <br />
                    <asp:ModalPopupExtender ID="btnClose_ModelPopupExtender" runat="server" 
                            TargetControlID="btnClose" 
                            PopupControlID="pnl_btnClose" 
                            OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
                            BackgroundCssClass="modalBackground" DropShadow="true"   />
                     <asp:Panel ID="pnl_btnClose" runat="server" style="display:none; width:200px; background-color:White; border-width:2px; border-color:Black; border-style:solid; padding:20px;">
                        "Are you sure you want to close the Payment Plan?"
                        <br /><br />
                        <div style="text-align:right;">
                            <asp:Button ID="ButtonOk" runat="server" Text="Yes" />
                            <asp:Button ID="ButtonCancel" runat="server" Text="No" />
                        </div>
                     </asp:Panel>
                     <!--end-->
                        <input type="hidden" value="" id="paymentData" />
                        <input type="hidden" value="1" id="totalPayments" />
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
