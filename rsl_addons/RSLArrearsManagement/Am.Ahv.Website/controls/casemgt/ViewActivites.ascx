﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewActivites.ascx.cs"
    Inherits="Am.Ahv.Website.controls.casemgt.ViewActivites" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style1
    {
        width: 421px;
    }
</style>
<asp:Panel ID="PanelOpenCase" runat="server" Width="100%" BorderWidth="1px" BorderColor="Gray">
    <table width="100%" cellspacing="1">
        <tr>
            <td align="left" colspan="2">
                <asp:Button ID="btnPrint" runat="server" Text="Print Activities" OnClick="btnPrint_Click" />
            </td>
            <td colspan="2">
            </td>
            <td align="right" colspan="2">
                <asp:Button ID="btnBack" runat="server" Text="< Back To Case Summary" OnClick="btnBack_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <br />
                <asp:UpdatePanel ID="updpnlCaseActivities" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <asp:Repeater ID="rptrCaseActivitiesList" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <td style=" width:20%" align="left">
                                        <asp:Label ID="lblHeaderDate" runat="server" Font-Bold="true" Text="Date"></asp:Label>
                                    </td>
                                    <td style=" width:20%" align="left">
                                        <asp:Label ID="lblHeaderStatus" runat="server" Font-Bold="true" Text="Stage"></asp:Label>
                                    </td>
                                    <td align="left" style=" width:20%" colspan="2">
                                        <asp:Label ID="lblHeaderActivity" runat="server" Font-Bold="true" Text="Activity"></asp:Label>
                                    </td>
                                    <td align="left" style=" width:25%">
                                        <asp:Label ID="lblHeaderRecordedBy" runat="server" Font-Bold="true" Text="Recorded By"></asp:Label>
                                    </td>
                                    <td align="center" style=" width:15%">                                        
                                    </td>                                   
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <hr />
                                    </td>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td colspan="6">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblDate" runat="server" Font-Bold="true" Text='<%#Eval("RecordedDate") %>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblStatus" runat="server" Font-Bold="true" Text='<%#Eval("StatusTitle") %>'></asp:Label>
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:Label ID="lblActivity" runat="server" Font-Bold="true" Text='<%#Eval("CodeName") %>'></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblRecordedBy" runat="server" Font-Bold="true" Text='<%#Eval("RecordedBy") %>'></asp:Label>
                                    </td>                                    
                                    <td align="left">
                                       
                                         <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/style/images/Document Icon.jpg" 
                                            CommandArgument=' <%# Eval("ActivityId") %> ' Visible='<%# Eval("IsDocumentAttached") %>'
                                                                    OnClick="imgbuttonDocument_Click" />
                                    </td>
                                    <%--<td align="left">
                                            <asp:ImageButton ID="imgbuttonsl" runat="server" ImageUrl="~/style/images/paperclip_img.gif" 
                                            CommandArgument=' <%# Eval("ActivityId") %> ' Visible='<%# Eval("IsLetterAttached") %>'
                                                                    OnClick="imgbuttonLetter_Click" />

                                    </td>--%>
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lbtnHideNotes" OnClick="lbtnHideNotes_Click" Font-Italic="true"
                                            ForeColor="Black" runat="server" Font-Bold="true" Text="Hide Notes"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnShowNotes" OnClick="lbtnShowNotes_Click" Visible="false"
                                            Font-Italic="true" ForeColor="Black" runat="server" Font-Bold="true" Text="Show Notes"></asp:LinkButton>
                                    </td>
                                    <td colspan="5">
                                        <asp:Label ID="lblNotes" runat="server" Text='<%#Eval("Notes") %>'></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>                      
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right">
                <asp:Button ID="btnAddActivity" runat="server" Text="Add Activity" OnClick="btnAddActivity_Click" />
            </td>
        </tr>
        <tr>
            <td class="style1">
                  <asp:UpdatePanel ID="upmodalpanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <asp:Panel ID="pnlDocuments" runat="server" GroupingText=" " Width="420px" BackColor="White">
                                <table width="400px">
                                    <tr>
                                        <td colspan="4">
                                            <h3>
                                                <asp:Label ID="lblcasenotes" runat="server" Text="Documents" Style="font-weight: 700"></asp:Label></h3>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="pinImage" runat="server" ImageUrl="~/style/images/paperclip_img.gif"
                                                Height="20px" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblstandardletters" runat="server" Text="Standard Letters"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                        <td>
                                            <asp:Panel ID="slpanel" BorderWidth="1" runat="server">
                                                <asp:DataList ID="sldatalist" runat="server" Width="169px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnstandardletter" OnClick="lbtnDocumentName_Click" runat="server"
                                                            Text='<%#Eval("AM_StandardLetters.Title") %>' CommandArgument='<%# MakeStandardLetterId(Eval("StandardLetterHistoryId").ToString(), Eval("StandardLettersId").ToString()) %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </asp:Panel>
                                            <asp:Label ID="lblnostandardletters" runat="server" Font-Bold="true" ForeColor="Red"
                                                Visible="true" Width="169px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="imguploaddocuments" runat="server" ImageUrl="~/style/images/Document Icon.gif"
                                                Height="20px" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lbluploadeddocs" runat="server" Text="Uploaded Documents"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                        <td>
                                            <asp:Panel ID="pnlDataSet" BorderWidth="1" runat="server">
                                                <asp:DataList ID="dataSetUploadedDocuments" runat="server" Width="169px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnDocumentName" OnClick="lbtnDocumentName_Click" runat="server"
                                                            Text='<%#Eval("DocumentName") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </asp:Panel>
                                            <asp:Label ID="lblnodocs" runat="server" Font-Bold="true" ForeColor="Red" Visible="true"
                                                Width="169px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                        </td>
                                        <td align="right">
                                            <asp:Button ID="btnclose" runat="server" Text="Close Window" Width="120px" OnClientClick="$find('mdlpop').hide(); return false;" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Label ID="lbldisp" runat="server"></asp:Label>
                            <asp:ModalPopupExtender ID="PopupExtenderdocuments" runat="server" CancelControlID="btnclose"
                                BehaviorID="mdlpop" PopupControlID="pnlDocuments" TargetControlID="lbldisp" Enabled="true"
                                BackgroundCssClass="modalBackground" DropShadow="false">
                            </asp:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Panel>
