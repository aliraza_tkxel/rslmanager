﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewCaseActivity.ascx.cs" Inherits="Am.Ahv.Website.controls.casemgt.ViewCaseActivity" %>
<style type="text/css">
    .style1
    {
        width: 188px;
    }
</style>
<div>
    <asp:UpdatePanel ID="panelAddactivity" runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
</div>
<div style="width: 315px">
    
    <table style="width: 99%;">
        <tr>
            <td align="right" colspan="3">
                <asp:Button ID="btnBack" runat="server" onclick="btnBack_Click" Text="Back" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lblStatus" runat="server">Stage:</asp:Label>
            </td>
            <td align="right">
                &nbsp;</td>
            <td class="style1">
                <asp:Label ID="lblStatusValue" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lblAction" runat="server" Text="Action:"></asp:Label>
            </td>
            <td align="right">
                &nbsp;</td>
            <td class="style1" colspan="1">
                <asp:Label ID="lblActionValue" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lblActivity" runat="server" Text="Activity:"></asp:Label>
            </td>
            <td align="right">
                &nbsp;</td>
            <td class="style1">
                <asp:Label ID="lblActivityValue" runat="server"></asp:Label>
            </td>
        </tr>
        <%--<tr>
            <td align="right">
                <asp:Label ID="lblTitle" runat="server" Text="Title:"></asp:Label>
            </td>
            <td align="right">
                &nbsp;</td>
            <td class="style1">
                <asp:Label ID="lblTitleValue" runat="server"></asp:Label>
            </td>
        </tr>--%>
        <tr>
            <td align="right">
                <asp:Label ID="lblNotes" runat="server" Text="Notes:"></asp:Label>
            </td>
            <td align="right">
                &nbsp;</td>
            <td class="style1">
                <asp:Label ID="lblNotesValue" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lblReason" runat="server" Text="Reason:"></asp:Label>
            </td>
            <td align="right">
                &nbsp;</td>
            <td class="style1">
                <asp:Label ID="lblReasonValue" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    
</div>