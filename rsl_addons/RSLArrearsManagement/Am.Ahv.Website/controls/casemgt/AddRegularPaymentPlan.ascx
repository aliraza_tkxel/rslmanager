﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddRegularPaymentPlan.ascx.cs" Inherits="Am.Ahv.Website.controls.casemgt.AddRegularPaymentPlan" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

    <asp:Panel ID="regularPaymentPanel" runat="server" Width="100%" BorderWidth = "1px" BorderColor ="Gray" >
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="height: 21px;" class="NormalPointFontBold">
                    <asp:Label ID="lblpaymentplan" runat="server" Text="Payment Plan:"></asp:Label>
                </div>
                <div style="text-align: right; margin-right: 15px; margin-bottom: 5px">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                </div>
                <div style="height: 21px; margin-right: 10px" class="NormalPointFont">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="lblCreateionDate" runat="server" Text="Creation Date:"></asp:Label></div>
                    <div style="float: right; width: 130px; text-align: right">
                        <asp:Label ID="lblCreationDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="height: 21px; margin-right: 10px" class="NormalPointFont">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="lbltype" runat="server" Text="Type:"></asp:Label></div>
                    <div style="float: right; width: 130px; text-align: right">
                        <asp:Label ID="lblStarPaymentType" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:DropDownList ID="ddlPaymentType" AutoPostBack="true" runat="server" Width="105px"
                            OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged1">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="height: 29px" class="casemanagementControlsMainDivs">
                    <div style="float: left; height: 19px" class="NormalPointFont">
                        <asp:Label ID="lblfrequency" runat="server" Text="Frequency:"></asp:Label></div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="lblStarFrequency" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:DropDownList ID="ddlFrequencyRegularPayment" runat="server" Width="105px" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="height: 21px" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="lblweeklyrentamount" runat="server" Text="Monthly Rent:"></asp:Label>
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="lblStartWeeklyRentAmount" runat="server" Text="*" Font-Bold="true"
                            ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtWeeklyRentAmount" Enabled="true" runat="server" CssClass="smallinputBox"
                            AutoPostBack="True" OnTextChanged="txtWeeklyRentAmount_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <div style="height: 21px" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="Label1" runat="server" Text="Rent Payable:"></asp:Label>
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="Label2" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtRentPayable" Enabled="true" runat="server" CssClass="smallinputBox"
                            AutoPostBack="True" OnTextChanged="TxtRentPayable_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <div style="height: 21px;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="lblArrearsBalance" runat="server" Text="Last Month Net Arrears:"></asp:Label>
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="Label4" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtArrearsBalance" runat="server" CssClass="smallinputBox" AutoPostBack="True"
                            OnTextChanged="TxtArrearsBalance_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <div style="height: 21px;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="lblamounttbcollect" runat="server" Text="Agreement Amount:"></asp:Label>
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="lblStarAmountCollected" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtAmountCollected" runat="server" CssClass="smallinputBox" AutoPostBack="True"
                            OnTextChanged="txtAmountCollected_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <div style="height: 21px;" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="Label3" runat="server" Text="Arrears Collection:"></asp:Label>
                    </div>
                    <div style="float: right; text-align: right">
                        <asp:Label ID="Label5" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtArrearsCollection" runat="server" CssClass="smallinputBox" AutoPostBack="True"
                            OnTextChanged="txtArrearsCollection_TextChanged"></asp:TextBox>
                    </div>
                </div>
                <%--        <div style="height: 21px" class="casemanagementControlsMainDivs">
                    <div style="float: left" class="NormalPointFont">
                        <asp:Label ID="lblstart" runat="server" Text="Start Date:"></asp:Label>
                    </div>
                    <div style="float: right; width: 130px; text-align: right">
                        <asp:Label ID="lblStarStartDate" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                        <asp:TextBox ID="txtStartDate" runat="server" AutoPostBack="true" OnTextChanged="txtStartDate_TextChanged"
                            CssClass="smallinputBox"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtStartDate">
                        </asp:CalendarExtender>
                        <%--<asp:TextBox ID="txtStartDate" AutoPostBack="true" OnTextChanged="txtStartDate_TextChanged" runat="server" onkeypress="return false"   CssClass="smallinputBox" ></asp:TextBox>                
           <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" TargetControlID="txtStartDate" >
           </asp:CalendarExtender>
            </div>
            </div>--%>
            <div style="height: 21px;" class="casemanagementControlsMainDivs">
                <div style="float: left" class="NormalPointFont">
                    <asp:Label ID="lblfirstcollectiondate" runat="server" Text="First Collection Date:"></asp:Label></div>
                <div style="float: right; text-align: right">
                    <asp:Label ID="lblStarFirstCollectionDate" runat="server" Text="*" Font-Bold="true"
                        ForeColor="Red"></asp:Label>&nbsp;
                    <asp:TextBox ID="txtFirstCollectionDate" onkeypress="return false" runat="server"
                        CssClass="smallinputBox" OnTextChanged="txtFirstCollectionDate_TextChanged" AutoPostBack="true">
                    </asp:TextBox>
                </div>
                <asp:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" TargetControlID="txtFirstCollectionDate"
                    PopupPosition="TopRight">
                </asp:CalendarExtender>
            </div>
            <div style="height: 21px;" class="casemanagementControlsMainDivs">
                <div style="float: left" class="NormalPointFont">
                    <asp:Label ID="lblend" runat="server" Text="End Date:"></asp:Label>
                </div>
                <div style="float: right; width: 130px; text-align: right">
                    <asp:TextBox ID="txtEndDate" onkeypress="return false" Enabled="false" runat="server"
                        ReadOnly="true" CssClass="smallinputBox">
                    </asp:TextBox>
                </div>
            </div>
            <div style="height: 21px" class="casemanagementControlsMainDivs">
                <div style="float: left" class="NormalPointFont">
                    <asp:Label ID="lblreviewdate" runat="server" Text="Review Date:"></asp:Label>
                </div>
                <div style="float: right; text-align: right">
                    <asp:Label ID="lblStarReviewDate" runat="server" Text="*" Font-Bold="true" ForeColor="Red"></asp:Label>&nbsp;
                    <asp:TextBox ID="txtReviewDate" runat="server" onkeypress="return false" AutoPostBack="true"
                        CssClass="smallinputBox" OnTextChanged="txtReviewDate_TextChanged">
                    </asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" TargetControlID="txtReviewDate"
                        PopupPosition="TopRight">
                    </asp:CalendarExtender>
                </div>
            </div>
            <div style="height: 30px; margin-top: 10px; margin-bottom: 5px" class="casemanagementControlsMainDivs">
                <div style="float: left">
                    <asp:Button ID="btnPrint" runat="server" Text="Print" OnClick="btnPrint_Click" />
                </div>
                <div style="float: left">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                </div>
                <div style="float: right;">
                    <asp:Button ID="btnCreateRegularPaymentPlan" runat="server" OnClick="btnCreateRegularPaymentPlan_Click"
                        Text="Create Payment Plan" Width="125px" />
                    <asp:Button ID="btnClose" runat="server" OnClick="btnClose_Click" Text="Close" Width="50px" Visible="false" />
                    <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="Update" Width="50px" Visible="false" />
                    <asp:Button ID="btnCreateUpdate" runat="server" OnClick="btnCreateUpdate_Click" Visible="false"
                        Text="Create Payment Plan" Width="125px" />
                    <!--This is part is build by umair for close button--start-->
                    <asp:ConfirmButtonExtender ID="btnClose_ConfirmButtonExtender" runat="server" 
                        TargetControlID="btnClose"
                        DisplayModalPopupID="btnClose_ModelPopupExtender" />
                    <br />
                    <asp:ModalPopupExtender ID="btnClose_ModelPopupExtender" runat="server" 
                            TargetControlID="btnClose" 
                            PopupControlID="pnl_btnClose" 
                            OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
                            BackgroundCssClass="modalBackground" />
                     <asp:Panel ID="pnl_btnClose" runat="server" style="display:none; width:200px; background-color:White; border-width:2px; border-color:Black; border-style:solid; padding:20px;">
                        "Are you sure you want to close the Payment Plan?"
                        <br /><br />
                        <div style="text-align:right;">
                            <asp:Button ID="ButtonOk" runat="server" Text="Yes" />
                            <asp:Button ID="ButtonCancel" runat="server" Text="No" />
                        </div>
                     </asp:Panel>
                     <!--end-->
                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Panel>
