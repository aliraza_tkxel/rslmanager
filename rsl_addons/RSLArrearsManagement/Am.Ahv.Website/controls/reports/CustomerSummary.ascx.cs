﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;
namespace Am.Ahv.Website.controls.reports
{
    public partial class CustomerSummary : UserControlBase
    {

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadUserSummary();
            }
        }

        #endregion

        #region"Load User Summary"

        private void LoadUserSummary()
        {
            CustomersReport CR = new CustomersReport();
            if (!Convert.ToString(Session[SessionConstants.CustomerId]).Equals(string.Empty))
            {
                CustomerInformationWrapper customer = CR.GetCustomerInformation(base.GetTenantId(), Convert.ToInt32(Session[SessionConstants.CustomerId]));
                if (customer != null)
                {
                    lblcustomername.Text = customer.CustomerName;
                    lbldateusr.Text = customer.Date;
                    lblcustomeraddress.Text = customer.CustomerAddress;
                    //lblcustomeraddress.Text += "<br>" + customer.Suburb;
                    lblrentbalanceusr.Text = customer.RentBalance.ToString();
                    lbllastpaymentusr.Text = customer.Amount.ToString() + " " + customer.LastPaymentDate;
                    if (Session[SessionConstants.LastMonthNetArrears] != null)
                    {
                        lblOwedtoBHAusr.Text = Convert.ToString(Math.Abs(Convert.ToDouble(Session[SessionConstants.LastMonthNetArrears])));
                    }
                    else
                    {
                        lblOwedtoBHAusr.Text = customer.OwedToBHA.ToString();
                    }
                    lblEstimatedHbusr.Text = customer.EstimatedHB.ToString();
                    lbltenancyIdusr.Text = customer.TenancyId.ToString();
                    
                }
                else
                {
                    lblError.Text = "No Customer Information available";
                    lblError.Visible = true;
                }
            }
        }

        #endregion
    }
}