﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerSummary.ascx.cs"
    Inherits="Am.Ahv.Website.controls.reports.CustomerSummary" %>
<asp:Panel ID="pnlcustomersummary" runat="server" Width="100%" 
    Font-Names="Arial" Font-Size="11pt">
    <table width="100%">
        <tr>
            <td colspan="2">
                <asp:Label ID="lblError" Visible="false" ForeColor="Red" Font-Bold="true" runat="server"></asp:Label>
                <asp:Label ID="lblcustomername" runat="server"></asp:Label>
            </td>
            <td align="justify" style="font-weight:700;">
                <asp:Label ID="lbldate" runat="server" Text="Date:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbldateusr" runat="server"></asp:Label>
            </td>
        </tr>

         <tr>
            <td colspan="2">
                <asp:Label ID="Label6" runat="server"></asp:Label>
            </td>
            <td align="justify" style="font-weight:700;">
                <asp:Label ID="lbltenancyId" runat="server" Text="Tenancy Ref:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbltenancyIdusr" runat="server"></asp:Label>
            </td>
        </tr>

        <tr>
            <td colspan="2" style="width:290px;padding-right:250px;">
                <asp:Label ID="lblcustomeraddress" runat="server"></asp:Label>
            </td>
            <td align="justify" style="font-weight:700;">
                <asp:Label ID="lblrentbalance" runat="server" Text="Rent Balance:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblrentbalanceusr" runat="server" ForeColor="red" Font-Bold="true"></asp:Label>
            </td>
        </tr>

        <tr>
            <td colspan="2">
            </td>
            <td align="justify" style="font-weight:700;">
                Estimated HB:</td>
            <td>
                <asp:Label ID="lblEstimatedHbusr" runat="server"></asp:Label>
            </td>
        </tr>

          <tr>
            <td colspan="2">
            </td>
            <td align="justify" style="font-weight:700;">
                Last Month Net Arrears:</td>
            <td>
                <asp:Label ID="lblOwedtoBHAusr" runat="server" ForeColor="red" Font-Bold="true"></asp:Label>
            </td>
        </tr>

        <tr>
            <td colspan="2">
            </td>
            <td align="justify" style="font-weight:700;">
                <asp:Label ID="lbllastpayment" runat="server" Text="Last Payment"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbllastpaymentusr" runat="server"></asp:Label>
            </td>
        </tr>

    </table>
</asp:Panel>
