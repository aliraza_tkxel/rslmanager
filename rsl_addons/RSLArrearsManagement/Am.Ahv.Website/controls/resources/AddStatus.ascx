﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddStatus.ascx.cs" Inherits="Am.Ahv.Website.controls.status_and_actions.AddStatus" %>
<style type="text/css">
    .style1
    {
        width: 150px;
    }
    .style2
    {
        width: 10px;
    }
</style>
<asp:Panel ID="firstlistPanel" runat="server" Width="500px">
    <asp:UpdatePanel ID="firstlistupanel" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td colspan="3">
                        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblstatustitle" runat="server" Text="Initial Case Monitoring:"></asp:Label>
                       <!-- Stage Title -->

                    </td>
                    <td class="style2">
                        <asp:Label ID="lblrequiredField" runat="server" Font-Bold="true" ForeColor="Red"
                            Text="*"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="statustxt" runat="server" EnableViewState="true" MaxLength="150"
                            Width="110px"></asp:TextBox>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblranking" runat="server" Text="Ranking:"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:Label ID="lblrequiredDDL0" runat="server" Font-Bold="true" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="rankingddl" runat="server" EnableViewState="true" Width="105px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblreviewperiod" runat="server" Text="Review Period:"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:Label ID="lblRequired0" runat="server" Font-Bold="true" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="reviewperiodtxt" runat="server" EnableViewState="true" Width="50px"></asp:TextBox>
                        <asp:DropDownList ID="reviewddl" runat="server" EnableViewState="true" Width="105px" >
                        </asp:DropDownList>
                        <i>after creation date</i><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorreviewperiodtxt" runat="server"
                            ControlToValidate="reviewperiodtxt" ErrorMessage="Only Digits" Height="19px"
                            ValidationExpression="^[0-9]+$" Width="165px"></asp:RegularExpressionValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Please enter less than 100"
                            ControlToValidate="reviewperiodtxt" MinimumValue="1" MaximumValue="99" Type="Integer"
                            Display="Dynamic"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lbltriggernext" runat="server" Text="Trigger for next status alert:"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:Label ID="lblRequiredAlert0" runat="server" Font-Bold="true" ForeColor="Red"
                            Text="*"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="triggertxt" runat="server" EnableViewState="true" Width="50px"></asp:TextBox>
                        <asp:DropDownList ID="triggerddl" runat="server" EnableViewState="true" Width="105px">
                        </asp:DropDownList>
                        <i>after creation date</i><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatortriggertxt" runat="server"
                            ControlToValidate="triggertxt" ErrorMessage="Only Digits" Height="19px" ValidationExpression="^[0-9]+$"
                            Width="165px"></asp:RegularExpressionValidator>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Please enter less than 100"
                            ControlToValidate="triggertxt" MinimumValue="1" MaximumValue="99" Type="Integer"
                            Display="Dynamic"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblnextstatusdetail" runat="server" Text="Next status Details:"></asp:Label>
                    </td>
                    <td class="style2">
                        <asp:Label ID="lblRequiredDetail0" runat="server" Font-Bold="true" ForeColor="Red"
                            Text="*"></asp:Label>
                    </td>
                    <td>
                        &nbsp;<asp:TextBox ID="statusdetailstxt" runat="server" EnableViewState="true" Height="60px"
                            TextMode="MultiLine" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblnoticeparameter" runat="server" Text="Notice Parameter required"></asp:Label>
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox ID="noticeparachk" runat="server" Checked="false" EnableViewState="true" AutoPostBack="True" OnCheckedChanged="noticeparachk_CheckedChanged" />
                    </td>
                </tr>
                <tr id="RecoveryAmount" runat="server" visible="false">
                    <td class="style1">
                        <asp:Label ID="lblrecoveryamount" runat="server" Text="Rent Balance:"></asp:Label>
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox ID="recoverychk" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr id="noticeissue" runat="server" visible="false">
                    <td class="style1">
                        <asp:Label ID="lblnoticeissuedate" runat="server" Text="Notice Issue Date:"></asp:Label>
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox ID="issuedatechk" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr id="NoticeExpiryDate" runat="server" visible="false">
                    <td class="style1">
                        <asp:Label ID="lblnoticeexpriry" runat="server" Text="Notice Expiry Date:"></asp:Label>
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox ID="expirydatechk" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr id="HearingDate" runat="server" visible="false">
                    <td class="style1">
                        <asp:Label ID="lblHearingDate" runat="server" Text="Hearing Date:"></asp:Label>
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox ID="HearingDateCheck" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr id="WarrantExpiryDate" runat="server" visible="false">
                    <td class="style1">
                        <asp:Label ID="lblWarrantExpiryDate" runat="server" Text="Warrant Expiry Date:"></asp:Label>
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox ID="WarrantExpiryDateCheck" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lbldocumentupload" runat="server" Text="Document Upload:"></asp:Label>
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td>
                        <asp:CheckBox ID="docuploadchk" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td class="style2">
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="cancelbtn" runat="server" OnClick="cancelbtn_Click" Text="Cancel"
                            Width="60px" />
                        &nbsp;
                        <asp:Button ID="savebtn" runat="server" OnClick="savebtn_Click" Text="Save Changes"
                            Width="100px" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
