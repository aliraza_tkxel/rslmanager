using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.IO;
using System.Drawing;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Entities;
namespace Am.Ahv.Website.controls.resources
{
    public partial class Users : UserControlBase
    {
        #region "Attributes"    
            
        int rowIndex;

        public int RowIndex
        {
            get { return rowIndex; }
            set { rowIndex = value; }
        }

            int currentPage;

            public int CurrentPage
            {
              get { return currentPage; }
              set { currentPage = value; }
            }
            int dbIndex;

            public int DbIndex
            {
              get { return dbIndex; }
              set { dbIndex = value; }
            }
            int totalPages;

            public int TotalPages
            {
                get { return totalPages; }
                set { totalPages = value; }
            }
            int totalRecords;

            public int TotalRecords
            {
                get { return totalRecords; }
                set { totalRecords = value; }
            }
            bool removeFlag = false;

            public bool RemoveFlag
            {
                get { return removeFlag; }
                set { removeFlag = value; }
            }

            bool isException;

            public bool IsException
            {
                get { return isException; }
                set { isException = value; }
            }
        #endregion
                    
        #region "Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
            if (!IsPostBack)
            {
                SetDefaultPagingAttributes();
                PopulateGridView();
                SetPagingLabels();                
            }            
        }

        #endregion

        #region"LBTN Previous Click"

        public void lBtnPrevious_Click(object sender, EventArgs e)
        {
            GetHiddenValues();
            ReducePaging();
            //populateGridView();
            //setPagingLabels();                            
        }

        #endregion

        #region"LBTN Next Click"

        public void lBtnNext_Click(object sender, EventArgs e)
        {
            GetHiddenValues();
            IncreasePaging();
            //populateGridView();
            //setPagingLabels();                
            
        }

        #endregion
        
        #region"PGV Users Row Editing"

        protected void pgvUsers_RowEditing(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
                int index = gvr.RowIndex;

                RowIndex = Convert.ToInt32(hflRowIndex.Value);
                if (RowIndex != -1 && RowIndex != index)
                {
                    ResetRow(RowIndex);
                }

                Label lb = new Label();
                DropDownList ddl = new DropDownList();
                Button btnEdit = new Button();
                Button btnSave = new Button();
                HiddenField hfl = new HiddenField();

                btnEdit = (Button)pgvUsers.Rows[index].FindControl("btnEdit");
                btnSave = (Button)pgvUsers.Rows[index].FindControl("btnSave");
                lb = (Label)pgvUsers.Rows[index].FindControl("lblUserType");
                ddl = (DropDownList)pgvUsers.Rows[index].FindControl("ddlUserTypeOptions");
                hfl = (HiddenField)pgvUsers.Rows[index].FindControl("hflLookupId");

                Session[SessionConstants.EditResourceId] = btnEdit.CommandArgument;
                btnEdit.Visible = false;
                btnSave.Visible = true;
                lb.Visible = false;
                ddl = PopulateUserType(index, ddl);
                ddl.EnableViewState = true;
                ddl.Visible = true;
                ddl.SelectedValue = hfl.Value;
                hflRowIndex.Value = index.ToString();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if(IsException)
                {
                    SetMessage(UserMessageConstants.exceptionEditingCaseWorker, true);
                }
            }
            
        }

        #endregion

        #region"PGV Users Row Updating"

        protected void pgvUsers_RowUpdating(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
                int index = gvr.RowIndex;
                Label lb = new Label();
                DropDownList ddl = new DropDownList();
                Button btnEdit = new Button();
                Button btnSave = new Button();
                UpdatePanel updpnlUser = new UpdatePanel();

                btnEdit = (Button)pgvUsers.Rows[index].FindControl("btnEdit");
                btnSave = (Button)pgvUsers.Rows[index].FindControl("btnSave");
                lb = (Label)pgvUsers.Rows[index].FindControl("lblUserType");
                ddl = (DropDownList)pgvUsers.Rows[index].FindControl("ddlUserTypeOptions");

                if (ddl.SelectedValue == "-1")
                {
                    RemoveFlag = true;
                    UpdateResource(ddl.SelectedValue, false);
                }
                else
                {
                    UpdateResource(ddl.SelectedValue, true);
                }
                btnEdit.Visible = true;
                btnSave.Visible = false;
                lb.Visible = true;
                ddl.Visible = false;
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionUpdatingCaseWorker, true);
                }
            }
        }

        #endregion

        #region"LBTN Add Case Worker Click"

        protected void lBtnAddCaseWorker_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.addCaseWorkerPath);
        }

        #endregion

        #region"lBtn Employee Name Click"

        protected void lBtnEmployeeName_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = ((LinkButton)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;

            int currentPage = Convert.ToInt32(hflCurrentPage.Value);
            int dbIndex = Convert.ToInt32(hflDBIndex.Value);
            int totalRecord = Convert.ToInt32(hflTotalRecords.Value);  

            LinkButton lbtn = new LinkButton();
            lbtn = (LinkButton)pgvUsers.Rows[index].FindControl("lBtnEmployeeName");
            string id = lbtn.CommandArgument;
            Session.Remove(Am.Ahv.Utilities.constants.SessionConstants.StreetList);
            Session.Remove(Am.Ahv.Utilities.constants.SessionConstants.SuburbList);
            Session.Remove(Am.Ahv.Utilities.constants.SessionConstants.RegionList);
            Response.Redirect(PathConstants.addCaseWorkerPath + "?cmd=edit&id=" + id + "&cp=" + currentPage.ToString() + "&di=" + dbIndex.ToString() + "&tr=" + totalRecord.ToString(), false);
        }

        #endregion

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            //PageSize = ApplicationConstants.pagingPageSize;
            String currentPageKey = "cp";
            String dbIndexKey = "di";
            String totalRecordKey = "tr";
            if (Request.QueryString[currentPageKey] != null
                && !Convert.ToString(Request.QueryString[currentPageKey]).Equals(string.Empty))
            {
                CurrentPage = Convert.ToInt32(Request.QueryString[currentPageKey]);
            }
            else
            {
                CurrentPage = 1;
            }

            if (Request.QueryString[dbIndexKey] != null
               && !Convert.ToString(Request.QueryString[dbIndexKey]).Equals(string.Empty))
            {
                DbIndex = Convert.ToInt32(Request.QueryString[dbIndexKey]);
            }
            else
            {
                DbIndex = 0;
            }

            if (Request.QueryString[totalRecordKey] != null
             && !Convert.ToString(Request.QueryString[totalRecordKey]).Equals(string.Empty))
            {
                TotalRecords = Convert.ToInt32(Request.QueryString[totalRecordKey]);
            }
            else
            {
                TotalRecords = 0;
            }

            TotalPages = 0;
            RowIndex = -1;
            SetHiddenFields();
          
        }

        #endregion

        #region"Set Hidden Fields"

        public void SetHiddenFields()
        {
            hflCurrentPage.Value = CurrentPage.ToString();
            hflDBIndex.Value = DbIndex.ToString();           
            hflTotalPages.Value = TotalPages.ToString();
            hflTotalRecords.Value = TotalRecords.ToString();
            hflRowIndex.Value = RowIndex.ToString();
        }

        #endregion

        #region"Set Paging Lables"

        public void SetPagingLabels()
        {
            try
            {

                int count = GetRecordCount();

                if (count > ApplicationConstants.pagingPageSize)
                {
                    SetCurrentRecordsDisplayed(count);
                    SetTotalPages(count);
                    lblUserOnPage.Text = (TotalRecords).ToString();
                    lblUserTotal.Text = count.ToString();
                    lblPageCurrent.Text = CurrentPage.ToString();
                    lblPageTotal.Text = TotalPages.ToString();
                }
                else
                {
                    TotalPages = 1;
                    lblUserOnPage.Text = pgvUsers.Rows.Count.ToString();
                    lblUserTotal.Text = count.ToString();
                    lblPageCurrent.Text = CurrentPage.ToString();
                    lblPageTotal.Text = CurrentPage.ToString();
                    BtnNext.Enabled = false;
                    BtnPrevious.Enabled = false;
                }
                if (CurrentPage == 1)
                {
                    BtnPrevious.Enabled = false;
                }
                else if (CurrentPage == TotalPages && CurrentPage > 1)
                {
                    BtnNext.Enabled = false;
                    BtnPrevious.Enabled = true;
                }
                if (TotalPages > CurrentPage)
                {
                    BtnNext.Enabled = true;
                }
                //FileStream fs = null;
                //if (File.Exists("PagingConstants.txt"))
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Append);
                //}
                //else
                //{
                //    fs = new FileStream("PagingConstants.txt", FileMode.Create, FileAccess.ReadWrite);
                //}
                //StreamWriter sw = new StreamWriter(fs);
                //sw.WriteLine(System.DateTime.Now.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.WriteLine("ToatlPages = " + TotalPages.ToString());
                //sw.WriteLine("Current Page = " + CurrentPage.ToString());
                //sw.WriteLine("Current Number of Records = " + TotalRecords.ToString());
                //sw.WriteLine("Records in Grid View = " + pgvUsers.Rows.Count.ToString());
                //sw.WriteLine("Total Records = " + count.ToString());
                //sw.WriteLine("-------------------------------------------------------------------");
                //sw.Close();
                //fs.Close();
                SetHiddenFields();
            }
            catch (FileLoadException flException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(flException, "Exception Policy");
            }
            catch (FileNotFoundException fnException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(fnException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSettingPageLabelsCaseWorker, true);
                }
            }
        }

        #endregion

        #region"Set Total Pages"

        public void SetTotalPages(int count)
        {
            try
            {
                if (count % ApplicationConstants.pagingPageSize > 0)
                {
                    TotalPages = (count / ApplicationConstants.pagingPageSize) + 1;
                }
                else
                {
                    TotalPages = (count / ApplicationConstants.pagingPageSize);
                }
            }
            catch (DivideByZeroException divide)
            {
                IsException = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSettingTotalPagesOutcomes, true);
                }
            }
        }

        #endregion

        #region"Set Current Records Dsiplayed"

        public void SetCurrentRecordsDisplayed(int count)
        {
            try
            {
                if (pgvUsers.Rows.Count == ApplicationConstants.pagingPageSize)
                {
                    TotalRecords = CurrentPage * pgvUsers.Rows.Count;
                }
                else if (RemoveFlag == true)
                {
                    TotalRecords -= 1;
                }
                else if (count==TotalRecords)
                {
                    TotalRecords = count;
                }
                else
                {
                    TotalRecords += pgvUsers.Rows.Count;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSettingCurrentRecordsOutcomes, true);
                }
            }

        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            lblMessage.Visible = true;
            this.updPnlMessage.Update();
        }

        #endregion

        #endregion

        #region"Getters"

        #region"Get Hidden Values"

        public void GetHiddenValues()
        {
            CurrentPage = Convert.ToInt32(hflCurrentPage.Value);
            DbIndex = Convert.ToInt32(hflDBIndex.Value);
            TotalPages = Convert.ToInt32(hflTotalPages.Value);
            TotalRecords = Convert.ToInt32(hflTotalRecords.Value);            
        }

        #endregion

        #endregion

        #region "Populate Data"

        #region"Populate Grid View"

        public void PopulateGridView()
        {
            try
            {
                Am.Ahv.BusinessManager.resource.Users user = new Am.Ahv.BusinessManager.resource.Users();
                pgvUsers.DataSource = user.getUserListings(DbIndex, ApplicationConstants.pagingPageSize);
                pgvUsers.DataBind();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionDataCaseWorker, true);
                }
            }
        }

        #endregion

        #region "Populate User Type"

        public DropDownList PopulateUserType(int rowIndex, DropDownList ddl)
        {
            try
            {
                Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
                const string codeID = "LookupCodeId";
                const string codeName = "CodeName";
                ddl.DataSource = lookup.getLookupListings("UserType");
                ddl.DataValueField = codeID;
                ddl.DataTextField = codeName;
                ddl.DataBind();
                ddl.Items.Add(new ListItem("Remove User", "-1"));
                return ddl;
            }
            catch (NullReferenceException e)
            {
                IsException = true;
                ExceptionPolicy.HandleException(e, "Exception Policy");
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionUserTypeCaseWorker, true);
                }
            }

            return ddl;
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Reload"

        public void Reload()
        {
            GetHiddenValues();
            PopulateGridView();
            SetPagingLabels();
            upUsers.Update(); // update update panel here...
        }

        #endregion

        #region"Update Resource"

        public void UpdateResource(string value, bool active)
        {
            try
            {
                Am.Ahv.BusinessManager.resource.Users resource = new Am.Ahv.BusinessManager.resource.Users();
                AM_Resource resourceObj = new AM_Resource();
                resourceObj.ModifiedBy = base.GetCurrentLoggedinUser();
                resourceObj.ModifiedDate = DateTime.Now;
                resourceObj.IsActive = active;
                if (Convert.ToInt32(value) > 0)
                {
                    resourceObj.LookupCodeId = Convert.ToInt32(value);
                }
                resource.updateResource(Convert.ToInt32(Session[SessionConstants.EditResourceId]), resourceObj);

                Session.Remove(SessionConstants.EditResourceId);
                if (GetRecordCount() % ApplicationConstants.pagingPageSize > 0)
                {
                    GetHiddenValues();
                    PopulateGridView();
                    SetPagingLabels();
                    upUsers.Update(); // update update panel here...
                }
                else
                {
                    GetHiddenValues();
                    ReducePaging();
                }

                Session.Remove(SessionConstants.EditResourceId);
                GetHiddenValues();
                PopulateGridView();
                SetPagingLabels();
                upUsers.Update(); // update update panel here...
            }
            catch (ArgumentOutOfRangeException argumentException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(argumentException, "Exception Policy");
            }
            catch (EntitySqlException entitySqlException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entitySqlException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionUpdateResourceCaseWorker, true);
                }
            }

        }

        #endregion

        #region"Reduce Paging"

        public void ReducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    BtnNext.Enabled = true;
                    BtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    BtnNext.Enabled = true;
                    BtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                PopulateGridView();
                SetPagingLabels();     
            }
        }

        #endregion

        #region"Increase Paging"

        public void IncreasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    BtnNext.Enabled = false;
                    BtnPrevious.Enabled = true;
                }
                else
                {
                    BtnNext.Enabled = true;
                    BtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                PopulateGridView();
                SetPagingLabels();     
            }
        }

        #endregion

        #region"Get Record Count"

        public int GetRecordCount()
        {
            try
            {
                Am.Ahv.BusinessManager.resource.Users user = new Am.Ahv.BusinessManager.resource.Users();
                return user.getRecordsCount();
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntityException entityException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityException, "Exception Policy");
            }
            catch (Exception exception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(exception, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionrecordCountCaseWorker, true);
                }
            }
            return 0;
        }

        #endregion               

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;

         //   pnlMessage.Visible = false;

            lblMessage.Visible = false;
            this.updPnlMessage.Update();

        }

        #endregion   

        #region"Reset Row"

        public void ResetRow(int index)
        {
            Label lb = new Label();
            DropDownList ddl = new DropDownList();
            Button btnEdit = new Button();
            Button btnSave = new Button();

            btnEdit = (Button)pgvUsers.Rows[index].FindControl("btnEdit");
            btnSave = (Button)pgvUsers.Rows[index].FindControl("btnSave");
            lb = (Label)pgvUsers.Rows[index].FindControl("lblUserType");
            ddl = (DropDownList)pgvUsers.Rows[index].FindControl("ddlUserTypeOptions");

            Session.Remove(SessionConstants.EditResourceId);
            btnEdit.Visible = true;
            btnSave.Visible = false;
            lb.Visible = true;
            ddl.Visible = false;

        }

        #endregion

        #endregion


    }
}