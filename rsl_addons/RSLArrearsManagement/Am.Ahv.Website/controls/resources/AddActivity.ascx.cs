﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.Drawing;
using Am.Ahv.Website.pagebase;
namespace Am.Ahv.Website.controls.resources
{
    #region"Reload User Control Activity"

    public delegate void reloadUserControlActivity(bool status);

    #endregion

    public partial class AddActivity : UserControlBase
    {
        #region"Attributes"

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #region"Is Error"

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        #endregion

        #endregion

        #region"Events"

        #region"Reload User Control"

        public event reloadUserControlActivity reload;

        #endregion

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {
                IsError = false;                             
            }
            ResetMessage();
        }

        #endregion

        #region"Btn Cancel Click"

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtActivity.Text = String.Empty;
            if (!Convert.ToString(Session[SessionConstants.EditActivityId]).Equals(string.Empty))
            {
                Session.Remove(SessionConstants.EditActivityId);
                //this.Visible = false;
            }
            ResetMessage();
                        
        }

        #endregion

        #region "Btn Save Click"

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
                if (!Convert.ToString(Session[SessionConstants.EditActivityId]).Equals(string.Empty))
                {
                    Validate(txtActivity.Text);
                    if (!IsError)
                    {
                        lookup.updateLookup(Convert.ToInt32(Session[SessionConstants.EditActivityId]), txtActivity.Text, base.GetCurrentLoggedinUser());
                        Session.Remove(SessionConstants.EditActivityId);
                        Response.Redirect(PathConstants.addMoreStatusPath, false);
                        //SetMessage(UserMessageConstants.InsertSuccess, false);
                    }
                    else
                    {
                        Session.Remove(SessionConstants.EditActivityId);
                        return;
                    }
                    
                }
                else if (!Convert.ToString(Session[SessionConstants.ActivityListingsId]).Equals(string.Empty))
                {
                    Validate(txtActivity.Text);
                    if (!IsError)
                    {
                        lookup.updateLookup(Convert.ToInt32(Session[SessionConstants.ActivityListingsId]), txtActivity.Text, base.GetCurrentLoggedinUser());
                        Session.Remove(SessionConstants.ActivityListingsId);
                        Response.Redirect(PathConstants.addMoreStatusPath, false);
                        //SetMessage(UserMessageConstants.InsertSuccess, false);
                    }
                    else
                    {
                        Session.Remove(SessionConstants.ActivityListingsId);
                        return;
                    }
                  
                }
                else
                {
                    Validate(txtActivity.Text);
                    if (!IsError)
                    {
                        lookup.addLookup(txtActivity.Text, "Activity", base.GetCurrentLoggedinUser());
                        Response.Redirect(PathConstants.addMoreStatusPath, false);
                        //SetMessage(UserMessageConstants.InsertSuccess, false);
                    }
                    else
                    {
                        return;
                    }
                }
                reload(true);
                txtActivity.Text = String.Empty;
            }
            catch (ArgumentOutOfRangeException ie)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ie, "Exception Policy");
            }
            catch (UpdateException u)
            {
                IsException = true;
                ExceptionPolicy.HandleException(u, "Exception Policy");
            }
            catch (EntitySqlException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (EntityException eex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(eex, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSavingActivity, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Set Outcome Text Field"

        public void setActivityTextField(string value)
        {
            txtActivity.Text = value;
            btnDelete.Visible = true;             
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion   

        #region"Reset Field"

        public void ResetField()
        {
            txtActivity.Text = string.Empty;
        }

        #endregion

        #endregion

        #region"Validate"

        public void Validate(string activity)
        {
            Validation validation = new Validation();

            if (validation.emptyString(activity))
            {
                IsError = true;
                SetMessage(UserMessageConstants.InputActivity, true);
                return;
            }
            else if (validation.lengthString(activity))
            {
                IsError = true;
                SetMessage(UserMessageConstants.stringLengthMessage, true);
                return;
            }
            else
            {
                IsError = false;
            }


        }
        #endregion

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
            if (!Convert.ToString(Session[SessionConstants.EditActivityId]).Equals(string.Empty))
            {
                Validate(txtActivity.Text);
                if (!IsError)
                {
                    lookup.DeleteLookup(Convert.ToInt32(Session[SessionConstants.EditActivityId]),  base.GetCurrentLoggedinUser());
                    Session.Remove(SessionConstants.EditActivityId);
                    Response.Redirect(PathConstants.activityPath);
                    //SetMessage(UserMessageConstants.InsertSuccess, false);
                }
                
               
            }
        }

        

       

    }
}