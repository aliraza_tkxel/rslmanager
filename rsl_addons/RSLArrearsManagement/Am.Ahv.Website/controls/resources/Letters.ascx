﻿<%@ Control Language="C#" AutoEventWireup="true"  CodeBehind="Letters.ascx.cs" Inherits="Am.Ahv.Website.controls.resources.Letters" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>


<div>
    <asp:UpdatePanel ID="panelAddactivity" runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
</div>
<table class="tableClass">
<tr>
    <td class="tableHeader">
        Templates
    </td>
</tr>
<tr>
    <td style="padding-left:20px;">       
      
<asp:Panel ID="LetterPanel" runat="server" Width="350px" Height="180px" ScrollBars="auto">
    <cc1:PagingGridView ID="LetterGrid" runat="server" AutoGenerateColumns="False" 
         GridLines="None" Width="350px">
        <AlternatingRowStyle HorizontalAlign="Left" />
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnEdit" runat="server" Enabled="true" Text="Edit" OnClick="btnEdit_Click" CommandArgument = '<%# Eval("StandardLetterId") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="deslbl" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           <%-- <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:Label ID="StatusLbl" runat="server" Text='<%# Eval("status") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
        <HeaderStyle HorizontalAlign="Left" />
        <PagerSettings Mode="NumericFirstLast" />
        <RowStyle HorizontalAlign="Left" VerticalAlign="Middle" />
    </cc1:PagingGridView>
</asp:Panel>
</td>
</tr>
<tr>
    <td align="right" class="tableFooter">
        <asp:Button ID="btnAddMoreLetters" runat="server" Width="120px" Text="+ More Templates" 
            onclick="btnAddMoreLetters_Click" />
    </td>
</tr>
</table>