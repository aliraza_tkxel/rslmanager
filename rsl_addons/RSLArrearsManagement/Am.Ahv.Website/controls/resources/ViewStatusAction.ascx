﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewStatusAction.ascx.cs"
    Inherits="Am.Ahv.Website.controls.status_and_actions.ViewStatusAction" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>

<table class="tableClass" style="height:200px;">
    <tr>
        <td class="tableHeader">
            Stage and Actions
        </td>
    </tr>
    <tr>
        <td valign="top" style="padding-left:20px" >       

<asp:Panel ID="DListPanel" runat="server" Width="350px" Height="100%">
   <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                     <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
    <asp:UpdatePanel ID="actionviewup" runat="server">
        <ContentTemplate>
           
            <asp:Panel ID="pnlStatusView"  ScrollBars="auto"
                runat="server" Height="180px">
             <cc1:PagingGridView ID="gvStatusview" runat="server" AutoGenerateColumns="False" GridLines="None"  Width="350px" ShowHeader="False" PageSize="4"
                 AllowPaging="false"  EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" 
                                    EmptyDataRowStyle-HorizontalAlign="Center">
                 <PagerSettings Mode="NumericFirstLast" />
                 <RowStyle BorderStyle="None" HorizontalAlign="Left" />
                    <Columns>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblTitle" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <asp:Button ID="editbtn" runat="server" Text="Edit" OnClick="editbtn_Click" CommandArgument='<%# Eval("StatusId") %>' />
                            </ItemTemplate></asp:TemplateField></Columns><AlternatingRowStyle HorizontalAlign="Left" Wrap="True" VerticalAlign="Top" />
              </cc1:PagingGridView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
</td>
    </tr>
    <tr>
        <td align="right" class="tableFooter">
            <asp:Button ID="btnAddMoreStatus" Width="140px" runat="server" Text="+ More Stage &amp; Actions" 
                onclick="btnAddMoreStatus_Click" />
        </td>
    </tr>
</table>