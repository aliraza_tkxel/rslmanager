﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Utilities.constants;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Website.pagebase;
namespace Am.Ahv.Website.controls.status_and_actions
{
    public partial class ViewStatusAction : UserControlBase
    {
        // Busiiness Manager Object
        Status StatusManager = null;
        //int pagesize = 4;
        bool isError;
        bool isException { set; get; }
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        
        #region Events

        #region PageLoad

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
               // ViewState["PageSize"] = pagesize;
              //  ViewState["PageCount"] = GetTotalCount();
            }
        }
        #endregion

        #region EditButtonClick

        protected void editbtn_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
                int index = gvr.RowIndex;
                Label lb = new Label();
                Button btn = sender as Button;
                btn = (Button)gvStatusview.Rows[index].FindControl("editbtn");
                lb = (Label)gvStatusview.Rows[index].FindControl("lblTitle");

                if (lb.Text == "Initial Case Monitoring")
                {
                    Session[SessionConstants.StatusID] = btn.CommandArgument;
                    Session[SessionConstants.StatusType] = lb.Text;
                }
                else
                {
                    Session[SessionConstants.StatusID] = btn.CommandArgument;
                    Session[SessionConstants.StatusType] = "Status";
                }
                Response.Redirect(PathConstants.editStatusPath, false);

            }
            catch (NullReferenceException nullException)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.exceptionEditStatus, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Load Data"

        private void LoadData()
        {
            try
            {
                StatusManager = new Status();
                this.gvStatusview.DataSource = StatusManager.GetAllStatus();
                this.gvStatusview.DataBind();
            }
            catch (EntityException entityexception)
            {
                isException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.exceptionGettingListStatus, true);
                }
            }
        }

        protected void btnAddMoreStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.addMoreStatusPath);
        }        

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion   
    }
}