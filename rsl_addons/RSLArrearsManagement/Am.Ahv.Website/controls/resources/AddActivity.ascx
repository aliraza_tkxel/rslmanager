﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddActivity.ascx.cs"
    Inherits="Am.Ahv.Website.controls.resources.AddActivity" %>
<table width="100%" class="tableClass">
    <tr>
        <td class="tableHeader" colspan="3">
            <asp:Label ID="lblNewActivity" runat="server" Text="New Activity"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
            <br />
        </td>
    </tr>
    <tr>
        <td valign="middle" align="right">
            <asp:Label ID="lblTitle" runat="server" Text="Title :" Width="50px"></asp:Label>
        </td>
        <td valign="middle" align="center">
            <asp:Label ID="lblErrorStar" Font-Bold="true" Visible="true" ForeColor="Red" runat="server"
                Text="*"></asp:Label>
        </td>
        <td align="left">
            <asp:TextBox ID="txtActivity" EnableViewState="false" runat="server" Width="350px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td align="right" colspan="3">
            <asp:Button Text="Delete Activity" ID="btnDelete" runat="server" 
                onclick="btnDelete_Click" Height="26px" Visible="false"   />
            <asp:Button ID="btnCancel" TabIndex="1" runat="server" Text="Cancel" Height="26px"
                Width="50px" OnClick="btnCancel_Click" />
            &nbsp;<asp:Button ID="btnSave" Width="50px" runat="server" TabIndex="0" Text="Save"
                OnClick="btnSave_Click" Height="26px" />
        </td>
    </tr>
</table>
