﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Am.Ahv.Utilities.utitility_classes;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Website.pagebase;
namespace Am.Ahv.Website.controls.resources
{
    #region"Reload User Control Outcome"

    public delegate void reloadUserControlOutcome(bool status);

    #endregion
    public partial class AddOutcome : UserControlBase
    {
        #region"Attributes"

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #region"Is Error"

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        #endregion

        #endregion

        #region"Events"

        #region"Reload User Control"

        public event reloadUserControlOutcome reload;

        #endregion

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IsError = false;
            }
            ResetMessage();
        }

        #endregion

        #region"Btn Cancel Click"

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtOutcome.Text = String.Empty;
            if (!Convert.ToString(Session[SessionConstants.EditOutcomeId]).Equals(string.Empty))
            {
                Session.Remove(SessionConstants.EditOutcomeId);

            }
            ResetMessage();
        }

        #endregion

        #region "Btn Save Click"

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
                if (!Convert.ToString(Session[SessionConstants.EditOutcomeId]).Equals(string.Empty))
                {
                    Validate(txtOutcome.Text);
                    if (!IsError)
                    {
                        lookup.updateLookup(Convert.ToInt32(Session[SessionConstants.EditOutcomeId]), txtOutcome.Text, base.GetCurrentLoggedinUser());
                        Response.Redirect(PathConstants.resourcePath, false);
                        //SetMessage(UserMessageConstants.updateSuccess, false);
                    }
                    else
                    {
                        return;
                    }
                    Session.Remove(SessionConstants.EditOutcomeId);
                }
                else
                {
                    Validate(txtOutcome.Text);
                    if (!IsError)
                    {
                        lookup.addLookup(txtOutcome.Text, "Outcome", base.GetCurrentLoggedinUser());
                        Response.Redirect(PathConstants.resourcePath, false);
                        //SetMessage(UserMessageConstants.updateSuccess, false);
                    }
                    else
                    {
                        return;
                    }
                }
                reload(true);
                txtOutcome.Text = String.Empty;
            }
            catch (ArgumentOutOfRangeException ie)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ie, "Exception Policy");
            }
            catch (UpdateException u)
            {
                IsException = true;
                ExceptionPolicy.HandleException(u, "Exception Policy");
            }
            catch (EntitySqlException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (EntityException eex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(eex, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSavingOutcome, true);
                }
            }

        }

        #endregion

        #endregion

        #region"Functions"

        #region"Set Outcome Text Field"

        public void SetOutcomeTextField()
        {
            try
            {
                if (!Convert.ToString(Session[SessionConstants.EditOutcomeId]).Equals(string.Empty))
                {
                    Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
                    txtOutcome.Text = lookup.getLookup(Convert.ToInt32(Session[SessionConstants.EditOutcomeId]));
                }
            }
            catch (NullReferenceException e)
            {
                IsException = true;
                ExceptionPolicy.HandleException(e, "Exception Policy");
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSettingOutcome, true);
                }
            }
        }

        #endregion

        #region"Reset Field"

        public void ResetField()
        {
            txtOutcome.Text = string.Empty;
        }

        #endregion

        #endregion

        #region"Validate"

        public void Validate(string outcome)
        {
            Validation validation = new Validation();

            if (validation.emptyString(outcome))
            {
                IsError = true;
                SetMessage(UserMessageConstants.InputOutcome, true);
                return;
            }
            else if (validation.lengthString(outcome))
            {
                IsError = true;
                SetMessage(UserMessageConstants.stringLengthMessage, true);
                return;
            }
            else
            {
                IsError = false;
            }


        }
        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion   
    }
}