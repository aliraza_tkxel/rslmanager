﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivitiesList.ascx.cs" Inherits="Am.Ahv.Website.controls.resources.ActivitiesList" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<table width="100%">  
    <tr>
        <td  align="left" valign="top">
            <asp:Panel ID="panelActivityList" Height = "200px" ScrollBars="Vertical" Width="100%" runat="server">
              <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                     <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
          <cc1:PagingGridView  ID="pgvActivitiesList" EnableViewState="true" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="true"  
                                               GridLines="None" PagerStyle-HorizontalAlign="Center" runat="server" width="99%">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="lblActivityId" runat="server" Visible="false" Text='<%#Eval("LookupCodeId") %>' ></asp:Label>                       
                        <asp:LinkButton ID="lbtnActivityTitle" ForeColor="Black" runat="server" Text='<%#Eval("CodeName") %>' OnClick="lbtnActivityTitle_Click"></asp:LinkButton>       
                    </ItemTemplate>                                        
                </asp:TemplateField>               
            </Columns>                         
          </cc1:PagingGridView>  
           </asp:Panel>                                                         
        
        </td>
        </tr>
        <tr>
        <td  align="left">
            <asp:LinkButton ID="lbtnAddANewActivity" runat="server"  ForeColor="Black"
                Text="Add a New Activity" onclick="lbtnAddMoreActivities_Click"></asp:LinkButton>       
        </td>
        
    </tr>        
</table>