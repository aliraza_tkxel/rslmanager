using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using System.Configuration.Assemblies;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.Drawing;
using Am.Ahv.Website.pagebase;

namespace Am.Ahv.Website.controls.resources
{
    public partial class ActivitiesList : UserControlBase
    {
        #region"Attributes"

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }        

        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
            PopulateGridView();
            //ConfigurationManager.ConnectionStrings;
        }

        #endregion

        #region"Lbtn Add More Activity Click"

        protected void lbtnAddMoreActivities_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.addNewActivityPath);
        }

        #endregion

        #region"Lbtn Activity Title Click"

        protected void lbtnActivityTitle_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = ((LinkButton)sender).Parent.Parent as GridViewRow;
                int index = gvr.RowIndex;
                Label lbl = new Label();
                lbl = (Label)pgvActivitiesList.Rows[index].FindControl("lblActivityId");
                Session[SessionConstants.EditActivityId] = lbl.Text;
                Response.Redirect(Am.Ahv.Utilities.constants.PathConstants.editActivityPath);
            }
            catch (NullReferenceException nl)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nl, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionActivityTitle, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Populate Grid View"

        public void PopulateGridView()
        {
            try
            {
                Am.Ahv.BusinessManager.lookup.Lookup lookp = new Am.Ahv.BusinessManager.lookup.Lookup();
                pgvActivitiesList.DataSource = lookp.getLookupListings("Activity");
                pgvActivitiesList.DataBind();
            }
            catch (NullReferenceException e)
            {
                IsException = true;
                ExceptionPolicy.HandleException(e, "Exception Policy");
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionloadingActivityList, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion   


    }
}