﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activity.ascx.cs" Inherits="Am.Ahv.Website.controls.resources.Activity" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
            <style type="text/css">
                .style1
                {
                	width: 250px;
                }
                .style2
                {
                    width: 250px;
                }
                .style4
                {
                    width: 120px;
                }
                .style5
                {
                    height: 350px;
                }
            </style>
            <table class="tableClass" width="100%"> <%--Table for displaying Activities--%>
                <tr>
                    <td colspan="4" align="left" class="tableHeader">
                        <asp:Label ID="lblActivity" runat="server" Text="Activities"></asp:Label>
                    </td>                    
                </tr>
                <tr valign="top">
                    <td colspan="4" class="style5">  
                             <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                <br />
                            </asp:Panel>                                          
                          <cc1:PagingGridView  ID="pgvActivities" EnableViewState="true" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="true"  
                                               GridLines="None" PagerStyle-HorizontalAlign="Center" runat="server" width="99%" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" 
                                    EmptyDataRowStyle-HorizontalAlign="Center">
                                <Columns>                                                  
                                   <asp:TemplateField>
                                        <ItemTemplate>
                                            <%--<asp:HiddenField ID="hflLookupTypeId" runat="server" Value='<%#Eval("LookupCodeId") %>' />--%>
                                            <asp:Button ID="btnEdit" CommandArgument='<%#Eval("LookupCodeId") %>' OnClick="btnEdit_Click"  runat="server" Text="Edit" />                                            
                                        </ItemTemplate>                                        
                                   </asp:TemplateField>               
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                              <asp:Label ID="lblActivityTitle" runat="server" Text='<%#Eval("CodeName") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                                 
                                </Columns>                         
                         </cc1:PagingGridView>
                    </td>
                </tr>
                <tr>
                    <td  colspan="4">
                        </td>
                </tr>
                <tr>
                    <td class="style1">
                        Activity 
                        <asp:Label ID="lblActivityOnPage" runat="server" ></asp:Label>
                        &nbsp;of&nbsp;
                        <asp:Label ID="lblActivityTotal" runat="server" ></asp:Label>
                    </td>
                    <td class="style2" >
                        Page
                        <asp:Label ID="lblPageCurrent" runat="server"></asp:Label>
                        of
                        <asp:Label ID="lblPageTotal" runat="server"></asp:Label>
                    </td>
                    <td class="style4" >            
                        <asp:LinkButton ID="lBtnPrevious" CausesValidation="false" runat="server" OnClick="lBtnPrevious_Click" EnableViewState="true" Text="< Previous" ></asp:LinkButton>
                    </td>
                    <td class="style4" >      
                        <asp:LinkButton ID="lBtnNext" CausesValidation="false" runat="server" OnClick="lBtnNext_Click" EnableViewState="true" Text="Next >"></asp:LinkButton>        
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="right" style=" padding-right:20px;" class="tableFooter">
                        <asp:Button ID="btnNewActivity" runat="server" Width="100px" CausesValidation="false" Text="+ New Activity" 
                            onclick="btnNewActivity_Click" />
                    </td>
                </tr>
            </table>

<asp:HiddenField ID="hflCurrentPage" runat="server" Value = "1"/>
<asp:HiddenField ID="hflTotalPages" runat="server" Value="0" />
<asp:HiddenField ID="hflTotalRecords" runat="server" Value="0" />
<asp:HiddenField ID="hflDBIndex" runat="server" Value="0" />
<asp:HiddenField ID="hflPageSize" runat="server" value="10"/>
<asp:HiddenField ID="hflCurrentRecords" runat="server"/>
