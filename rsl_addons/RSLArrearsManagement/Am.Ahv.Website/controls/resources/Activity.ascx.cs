﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Website.pagebase;
namespace Am.Ahv.Website.controls.resources
{
    #region"Delegate"

    public delegate void sendEventToParentPageActivity(bool EventInvoked, bool edit);

    #endregion

    public partial class Activity : UserControlBase
    {
        #region "Attributes"
        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }
        
        bool newRecordFlag;

        public bool NewRecordFlag
        {
            get { return newRecordFlag; }
            set { newRecordFlag = value; }
        }

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region "Setters"

        #region"Set Default Paging Attributes"

        private void SetDefaultPagingAttributes()
        {
            CurrentPage = 1;
            DbIndex = 0;
            TotalPages = 0;
            TotalRecords = 0;
            SetHiddenFields();
        }

        #endregion

        #region"Set Hidden fields"

        public void SetHiddenFields()
        {
            hflCurrentPage.Value = CurrentPage.ToString();
            hflDBIndex.Value = DbIndex.ToString();
            hflTotalPages.Value = TotalPages.ToString();
            hflTotalRecords.Value = TotalRecords.ToString();
            
        }

        #endregion

        #region"Set Paging Labels"

        public void SetPagingLabels()
        {
            Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
            int count = lookup.getLookupCount("Activity");

            if (count > ApplicationConstants.pagingPageSizeOutcomes)
            {
                SetCurrentRecordsDisplayed();
                SetTotalPages(count);
                lblActivityOnPage.Text = (TotalRecords).ToString();
                lblActivityTotal.Text = count.ToString();
                lblPageCurrent.Text = CurrentPage.ToString();
                lblPageTotal.Text = TotalPages.ToString();
            }
            else
            {
                TotalPages = 1;
                lblActivityOnPage.Text = pgvActivities.Rows.Count.ToString();
                lblActivityTotal.Text = count.ToString();
                lblPageCurrent.Text = CurrentPage.ToString();
                lblPageTotal.Text = CurrentPage.ToString();
                lBtnNext.Enabled = false;
                lBtnPrevious.Enabled = false;
            }
            if (CurrentPage == 1)
            {
                lBtnPrevious.Enabled = false;
            }
            if ((count % ApplicationConstants.pagingPageSizeOutcomes > 0) && CurrentPage != TotalPages)
            {
                lBtnNext.Enabled = true;
            //    lBtnPrevious.Enabled = true;
            }
           
            
            SetHiddenFields();
        }

        #endregion

        #region"Set Total Pages"

        public void SetTotalPages(int count)
        {
            if (count % ApplicationConstants.pagingPageSizeOutcomes > 0)
            {
                TotalPages = (count / ApplicationConstants.pagingPageSizeOutcomes) + 1;
            }
            else
            {
                TotalPages = (count / ApplicationConstants.pagingPageSizeOutcomes);
            }
        }

        #endregion

        #region"Set Current Records To Display"

        public void SetCurrentRecordsDisplayed()
        {

            if (pgvActivities.Rows.Count == ApplicationConstants.pagingPageSizeOutcomes)
            {
                TotalRecords = CurrentPage * pgvActivities.Rows.Count;
            }
            else if (NewRecordFlag == false)
            {
                TotalRecords += pgvActivities.Rows.Count;
            }
            else
            {
                TotalRecords += 1;
            }
        }

        #endregion

        #endregion

        #region"Events"

        #region"Invoke Event"

        public event sendEventToParentPageActivity invokeEvent;

        #endregion

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
            if (!IsPostBack)
            {
                SetDefaultPagingAttributes();
                PopulateGridView();
                SetPagingLabels();
            }
            NewRecordFlag = false;
        }

        #endregion

        #region"Lbtn Previous Click"

        public void lBtnPrevious_Click(object sender, EventArgs e)
        {
            GetHiddenValues();
            ReducePaging();
        }

        #endregion

        #region"Lbtn Next Click"

        public void lBtnNext_Click(object sender, EventArgs e)
        {
            GetHiddenValues();
            IncreasePaging();
        }

        #endregion

        #region"Btn Edit Click"

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;
            Button btnEdit = new Button();
            btnEdit = (Button)pgvActivities.Rows[index].FindControl("btnEdit");
            Session[SessionConstants.EditActivityId] = btnEdit.CommandArgument;
            invokeEvent(true, true);
        }

        #endregion

        #region"Btn Add New Activity Click"

        protected void btnNewActivity_Click(object sender, EventArgs e)
        {
            invokeEvent(true, false);
        }

        #endregion

        #endregion

        #region"getters"

        #region"Get Hidden Values"

        public void GetHiddenValues()
        {
            CurrentPage = Convert.ToInt32(hflCurrentPage.Value);
            DbIndex = Convert.ToInt32(hflDBIndex.Value);
            TotalPages = Convert.ToInt32(hflTotalPages.Value);
            TotalRecords = Convert.ToInt32(hflTotalRecords.Value);
        }

        #endregion

        #endregion

        #region "Populate Data"

        #region"Populate Grid View"

        public void PopulateGridView()
        {
            try
            {
                Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
                pgvActivities.DataSource = lookup.getLookupListings("Activity", DbIndex, ApplicationConstants.pagingPageSizeOutcomes);
                pgvActivities.DataBind();
            }
            catch (NullReferenceException e)
            {
                IsException = true;
                ExceptionPolicy.HandleException(e, "Exception Policy");
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionloadingActivity, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Functions"

        #region"reload Control"

        public void ReloadControl()
        {
            NewRecordFlag = true;
            GetHiddenValues();
            PopulateGridView();
            SetPagingLabels();
        }

        #endregion

        #region"Reduce Paging"

        public void ReducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lBtnNext.Enabled = true;
                    lBtnPrevious.Enabled = false;
                }
                //else if (CurrentPage == (TotalPages -1))
                //{
                //    BtnNext.Enabled = false;
                //    BtnPrevious.Enabled = true;
                //}
                else
                {
                    lBtnNext.Enabled = true;
                    lBtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                PopulateGridView();
                SetPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void IncreasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lBtnNext.Enabled = false;
                    lBtnPrevious.Enabled = true;
                }
                else
                {
                    lBtnNext.Enabled = true;
                    lBtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                PopulateGridView();
                SetPagingLabels();
            }
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion   

        #endregion
    }
}