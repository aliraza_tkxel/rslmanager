﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Outcome.ascx.cs" EnableViewState="true" Inherits="Am.Ahv.Website.controls.outcome.Outcome" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
             <style type="text/css">
                 .style1
                 {
                     width: 203px;
                 }
             </style>
             <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <table class="tableClass"  width="100%" > <%--Table for displaying Activities--%>
                <tr>
                    <td colspan="4" align="left" class="tableHeader">
                        <asp:Label ID="lblOutcome" runat="server" Text="Outcomes"></asp:Label>
                    </td>                    
                </tr>
                <tr valign="top">
                    <td colspan="4" >       
                                                  
                          <cc1:PagingGridView  ID="pgvOutcomes" EnableViewState="true" AutoGenerateColumns="false" AllowPaging="false" AllowSorting="true"  
                                               GridLines="None" PagerStyle-HorizontalAlign="Center" runat="server" width="100%" EmptyDataText="No records found." EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" 
                                    EmptyDataRowStyle-HorizontalAlign="Center">
                                <Columns>                                                  
                                 
                                   <asp:TemplateField>
                                       
                                        <ItemTemplate>
                                           
                                             <asp:Button ID = "btnDelete" CommandArgument = '<%#Eval("LookupCodeId") %>' runat = "server" Text = "Delete" OnClientClick="return confirm('Are You Sure You Want to Delete?');" OnClick = "btnDelete_Click"/>
                                            <asp:Button ID="btnEdit" CommandArgument='<%#Eval("LookupCodeId") %>' OnClick="btnEdit_Click"  runat="server" Text="Edit" />                                            
                                        </ItemTemplate>                                        
                                   </asp:TemplateField>               
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                              <asp:Label ID="lblOutcomeTitle" runat="server" Text='<%#Eval("CodeName") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                                 
                                </Columns>                         
                         </cc1:PagingGridView>                                                           
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4" class="tableHeader">
                        </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" width="300px">
                        <asp:Label ID="lblOutComePaging" runat="server" Text="Outcomes"></asp:Label>
                        <asp:Label ID="lblOutcomeOnPage" runat="server" Text="3"></asp:Label>
                        of
                        <asp:Label ID="lblOutcomeTotal" runat="server" Text="3" Width=""></asp:Label>
                    </td>
                    <td align="center" valign="middle" width="230px" >
                        Page
                        <asp:Label ID="lblPageCurrent" runat="server"></asp:Label>
                        of
                        <asp:Label ID="lblPageTotal" runat="server"></asp:Label>
                    </td>
                    <td align="left" valign="middle" width="100px" >            
                        <asp:LinkButton ID="lBtnPrevious" CausesValidation="false" runat="server" OnClick="lBtnPrevious_Click" EnableViewState="true" Text="< Previous" ></asp:LinkButton>
                    </td>
                    <td valign="middle" width="100px" >      
                        <asp:LinkButton ID="lBtnNext" runat="server" CausesValidation="false" OnClick="lBtnNext_Click" EnableViewState="true" Text="Next >"></asp:LinkButton>        
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="right" style=" padding-right:20px;" class="tableFooter">
                        <asp:Button ID="btnNewOutcome" Width="121px" runat="server" 
                            CausesValidation="false" Visible="true" Text="Add New Outcome" 
                            onclick="btnNewOutcome_Click" />
                    </td>
                </tr>
            </table>

<asp:HiddenField ID="hflCurrentPage" runat="server" Value = "1"/>
<asp:HiddenField ID="hflTotalPages" runat="server" Value="0" />
<asp:HiddenField ID="hflTotalRecords" runat="server" Value="0" />
<asp:HiddenField ID="hflDBIndex" runat="server" Value="0" />
<asp:HiddenField ID="hflPageSize" runat="server" value="10"/>
<asp:HiddenField ID="hflCurrentRecords" runat="server"/>


