﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Utilities.utitility_classes;
using System.Data;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Website.pagebase;
namespace Am.Ahv.Website.controls.status_and_actions
{
    #region"Delegate"

    #region"Update Tree"

    public delegate void updateStatusAndActionTreeFromAddStatus(bool flag);

    #endregion

    #region"Reset Selected Node"

    public delegate void resetNodeSelectionStatus(bool flag);

    #endregion

    #endregion

    public partial class AddStatus : UserControlBase
    {
        #region"Attributes"

        Am.Ahv.BusinessManager.statusmgmt.Status StatusManager = null;
        bool isException { set; get; }
        #region"IsError"

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        #endregion

        #endregion

        #region Events

        #region"Delegate Event"

        #region"Update Tree Event"

        public event updateStatusAndActionTreeFromAddStatus update;

        #endregion

        #region"Reset Selected Node"

        public event resetNodeSelectionStatus reset;

        #endregion

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // initLookups();
            }
            ResetMessage();
        }

        #endregion

        #region "Save Button Click"

        protected void savebtn_Click(object sender, EventArgs e)
        {
            try
            {
                //   CheckSelection();
                if (!IsError)
                {
                    bool flag = AddStatusstatushistory();
                    if (flag)
                    {
                        ResetPage();
                        update(true);

                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.exceptionSavingStatus, true);
                }
            }
        }

        #endregion

        #region"Cancel Btn Click"

        protected void cancelbtn_Click(object sender, EventArgs e)
        {
            ResetMessage();
            ResetPage();
            reset(true);
            if (Session[SessionConstants.StatusID] != null)
            {
                Session.Remove(SessionConstants.StatusID);
            }
        }

        #endregion

        #endregion

        #region Lookups

        public void InitLookups()
        {
            StatusManager = new Status();
            try
            {
                //******** setting the rent frequency drop down
                //frequencyddl.DataSource = StatusManager.GetRentList();
                //frequencyddl.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                //frequencyddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                //frequencyddl.DataBind();
                //frequencyddl.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                //frequencyddl.SelectedValue = ApplicationConstants.defaulvalue;


                ////****************Setting Radion Button Checked True

                //param1chk.Checked = true;

                ////******* Setting Rent Period Drop Down List

                //periodddl.DataSource = StatusManager.GetReviewPeriod();
                //periodddl.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                //periodddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                //periodddl.DataBind();
                //periodddl.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                //periodddl.SelectedValue = ApplicationConstants.defaulvalue;

                //***** Setting Review Period Drop Down Llist
                reviewddl.DataSource = StatusManager.GetReviewPeriod();
                reviewddl.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                reviewddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                reviewddl.DataBind();
                reviewddl.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                reviewddl.SelectedValue = ApplicationConstants.defaulvalue;

                ///****** Setting Trigger Period Drop Down List
                ///
                triggerddl.DataSource = StatusManager.GetReviewPeriod();
                triggerddl.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                triggerddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                triggerddl.DataBind();
                triggerddl.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                triggerddl.SelectedValue = ApplicationConstants.defaulvalue;

                ///************** Setting Ranking Drop Downlist
                ///
                List<int> RankList = StatusManager.GetStatusRanking();
                if (RankList != null)
                {
                    rankingddl.DataSource = RankList;
                    rankingddl.DataBind();
                    rankingddl.SelectedValue = RankList.Last().ToString();
                }
                else
                {
                    rankingddl.Items.Add(new ListItem("1", "1"));
                }
            }
            catch (KeyNotFoundException keynotfound)
            {
                IsError = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingLookupsStatus, true);
                }
            }
        }

        #endregion

        #region Utility Methods

        #region"Check Selection"

        public void CheckSelection()
        {
            //if (param1chk.Checked && param2chk.Checked)
            //{
            //    SetMessage(UserMessageConstants.invalidSelection, true);
            //    IsError = true;
            //}
            //else
            //{
            //    IsError = false;
            //}
        }

        #endregion

        #region"Add Status Status History"

        public bool AddStatusstatushistory()
        {
            try
            {
                AM_Status status = new AM_Status();
                AM_StatusHistory statushistory = new AM_StatusHistory();
                StatusManager = new Status();
                ///setting tilte
                ///
                Validate(statustxt.Text);
                if (!IsError)
                {
                    status.Title = this.statustxt.Text;
                    statushistory.Title = this.statustxt.Text;
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return false;
                }

                Validate(reviewperiodtxt.Text);
                if (!IsError)
                {
                    status.ReviewPeriod = int.Parse(reviewperiodtxt.Text);
                    statushistory.ReviewPeriod = int.Parse(reviewperiodtxt.Text);
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return false;
                }
                //setting review period dropdown
                if (reviewddl.SelectedValue == ApplicationConstants.defaulvalue)
                {
                    SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                    return false;
                }
                else
                {
                    status.ReviewPeriodFrequencyLookupCodeId = int.Parse(reviewddl.SelectedValue);
                    statushistory.ReviewPeriodFrequencyLookupCodeId = int.Parse(reviewddl.SelectedValue);
                }

                Validate(triggertxt.Text);
                if (!IsError)
                {
                    status.NextStatusAlert = int.Parse(triggertxt.Text);
                    statushistory.NextStatusAlert = int.Parse(triggertxt.Text);
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return false;
                }

                //setting trigger drop down list
                if (triggerddl.SelectedValue == ApplicationConstants.defaulvalue)
                {
                    SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                    return false;
                }
                else
                {
                    status.NextStatusAlertFrequencyLookupCodeId = int.Parse(triggerddl.SelectedValue);
                    statushistory.NextStatusAlertFrequencyLookupCodeId = int.Parse(triggerddl.SelectedValue);
                }

                //settig next status details
                Validate(statusdetailstxt.Text);
                if (!IsError)
                {
                    status.NextStatusDetail = statusdetailstxt.Text;
                    statushistory.NextStatusDetail = statusdetailstxt.Text;
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return false;
                }

                // setting notice parameter required checkbox

                status.IsNoticeParameterRequired = noticeparachk.Checked;
                statushistory.IsNoticeParameterRequired = noticeparachk.Checked;

                //setting recovery amount check box
                status.IsRecoveryAmount = recoverychk.Checked;
                statushistory.IsRecoveryAmount = recoverychk.Checked;

                //setting notice Issue date
                status.IsNoticeIssueDate = issuedatechk.Checked;
                statushistory.IsNoticeIssueDate = issuedatechk.Checked;

                //setting notice expiry date

                status.IsNoticeExpiryDate = expirydatechk.Checked;
                statushistory.IsNoticeExpiryDate = expirydatechk.Checked;

                status.IsWarrantExpiryDate = WarrantExpiryDateCheck.Checked;
                statushistory.IsWarrantExpiryDate = WarrantExpiryDateCheck.Checked;

                status.IsHearingDate = HearingDateCheck.Checked;
                statushistory.IsHearingDate = HearingDateCheck.Checked;

                //setting document upload parameter
                status.IsDocumentUpload = docuploadchk.Checked;
                statushistory.IsDocumentUpload = docuploadchk.Checked;

                status.CreatedDate = DateTime.Now;
                statushistory.CreatedDate = DateTime.Now;
                status.ModifiedDate = DateTime.Now;
                statushistory.ModifiedDate = DateTime.Now;

                status.CreatedBy = base.GetCurrentLoggedinUser();
                statushistory.CreatedBy = base.GetCurrentLoggedinUser();
                status.ModifiedBy = base.GetCurrentLoggedinUser();
                statushistory.ModifiedBy = base.GetCurrentLoggedinUser();

                AM_Status dummy = new AM_Status();

                if (!Convert.ToString(Session[SessionConstants.StatusID]).Equals(string.Empty))
                {
                    bool rankUpdate = CompareRanks();
                    int statusid = Convert.ToInt32(Session[SessionConstants.StatusID]);
                    //bool statusflag = StatusManager.GetRankByStatusID(statusid, int.Parse(rankingddl.SelectedValue));
                    if (rankUpdate)
                    {
                        if (CheckStatusByRank())
                        {
                            status.Ranking = Convert.ToInt32(rankingddl.SelectedValue);
                            statushistory.Ranking = Convert.ToInt32(rankingddl.SelectedValue);
                            int lastRank = GetRank();
                            if (lastRank != -1)
                            {
                                if (!StatusManager.updateStatus(statusid, status, statushistory, lastRank))
                                {
                                    SetMessage(UserMessageConstants.StatusAlreadyAssignedMessage, true);
                                    return false;
                                }
                                else
                                {
                                    SetMessage(UserMessageConstants.updateSuccess, false);
                                    Session.Remove(SessionConstants.StatusID);
                                    ResetPage();
                                }
                            }
                        }
                        else
                        {
                            SetMessage(UserMessageConstants.StatusUnAssignedRankMessage, true);
                            return false;
                        }
                    }
                    else
                    {
                        status.Ranking = int.Parse(rankingddl.SelectedValue);
                        statushistory.Ranking = int.Parse(rankingddl.SelectedValue);
                        StatusManager.updateStatus(statusid, status, statushistory);
                        SetMessage(UserMessageConstants.updateSuccess, false);
                        Session.Remove(SessionConstants.StatusID);
                        ResetPage();
                        //       return true;
                    }
                    //else
                    //{                        
                    //    SetMessage(UserMessageConstants.MultipleRankUpdateMessage, true);
                    //    return false;
                    //}
                }
                else
                {
                    ////setting ranking
                    if (!StatusManager.IsPresent(int.Parse(rankingddl.SelectedValue)))
                    {
                        status.Ranking = Convert.ToInt32(rankingddl.SelectedValue);
                        statushistory.Ranking = Convert.ToInt32(rankingddl.SelectedValue);
                    }
                    else
                    {
                        SetMessage(UserMessageConstants.MultipleRankMessage, true);
                        return false;
                    }

                    dummy = StatusManager.SaveStatus(status, statushistory);
                    SetMessage(UserMessageConstants.InsertSuccess, false);
                    // ResetPage();
                }
                if (dummy != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (KeyNotFoundException keynotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (NullReferenceException nr)
            {
                isException = true;
                ExceptionPolicy.HandleException(nr, "Exception Policy");
            }
            catch (EntityException entityexception)
            {
                isException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (ObjectNotFoundException objnotfound)
            {
                isException = true;
                ExceptionPolicy.HandleException(objnotfound, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.exceptionSavingStatus, true);
                }
            }
            return false;
        }
        public void RegenrateRankingDdl()
        {
            try
            {
                StatusManager = new Status();
                List<int> RankList = StatusManager.GetStatusRanking();
                if (RankList != null)
                {
                    rankingddl.DataSource = RankList;
                    rankingddl.DataBind();
                    rankingddl.SelectedValue = RankList.Last().ToString();
                }
                else
                {
                    rankingddl.Items.Add(new ListItem("1", "1"));
                }
            }
            catch (NullReferenceException nullref)
            {
                isException = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion

        #endregion

        #region"Populate Data"

        #region"Load Status

        public void LoadStatus()
        {
            try
            {
                if (!Convert.ToString(Session[SessionConstants.StatusID]).Equals(string.Empty))
                {
                    StatusManager = new Status();
                    AM_Status status = StatusManager.GetStatusById(Convert.ToInt32(Session[SessionConstants.StatusID]));
                    statustxt.Text = status.Title;
                    //if (status.IsRentAmount == true)
                    //{
                    //    rentamounttxt.Text = status.RentAmount.ToString();
                    //    param2chk.Checked = Convert.ToBoolean(status.IsRentAmount);
                    //}
                    //if (status.IsRentParameter == true)
                    //{
                    //    frequencyddl.SelectedValue = status.RentParameter.ToString();
                    //    periodddl.SelectedValue = status.RentParameterFrequencyLookupCodeId.ToString();
                    //    param1chk.Checked = Convert.ToBoolean(status.IsRentParameter);
                    //}
                    rankingddl.SelectedValue = status.Ranking.ToString();
                    ViewState[ViewStateConstants.StatusRank] = status.Ranking;
                    reviewperiodtxt.Text = status.ReviewPeriod.ToString();
                    reviewddl.SelectedValue = status.ReviewPeriodFrequencyLookupCodeId.ToString(); //status.AM_LookupCodeReference.EntityKey.EntityKeyValues[0].Value.ToString();
                    triggertxt.Text = status.NextStatusAlert.ToString();
                    triggerddl.SelectedValue = status.NextStatusAlertFrequencyLookupCodeId.ToString();// status.AM_LookupCode1Reference.EntityKey.EntityKeyValues[0].Value.ToString();
                    statusdetailstxt.Text = status.NextStatusDetail;
                    noticeparachk.Checked = Convert.ToBoolean(status.IsNoticeParameterRequired);
                    if (status.IsNoticeParameterRequired.Value)
                    {
                        RecoveryAmount.Visible = true;
                        noticeissue.Visible = true;
                        NoticeExpiryDate.Visible = true;
                        HearingDate.Visible = true;
                        WarrantExpiryDate.Visible = true;
                    }
                    docuploadchk.Checked = Convert.ToBoolean(status.IsDocumentUpload);
                    recoverychk.Checked = Convert.ToBoolean(status.IsRecoveryAmount);
                    issuedatechk.Checked = Convert.ToBoolean(status.IsNoticeIssueDate);
                    expirydatechk.Checked = Convert.ToBoolean(status.IsNoticeExpiryDate);
                    HearingDateCheck.Checked = Convert.ToBoolean(status.IsHearingDate);
                    WarrantExpiryDateCheck.Checked = Convert.ToBoolean(status.IsWarrantExpiryDate);
                }
            }
            catch (ObjectNotFoundException keynotfound)
            {
                IsError = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingStatus, true);
                }
            }
        }

        #endregion

        #endregion

        #region"Compare Ranks"

        public bool CompareRanks()
        {
            int lastRank = GetRank();
            if (lastRank == -1)
            {
                return false;
            }
            else
            {
                int updatedRank = Convert.ToInt32(rankingddl.SelectedValue);
                if (updatedRank == -1)
                {
                    return false;
                }
                else if (lastRank != updatedRank)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        #region"Get Rank"

        public int GetRank()
        {
            if (ViewState[ViewStateConstants.StatusRank] != null)
            {
                return Convert.ToInt32(ViewState[ViewStateConstants.StatusRank]);
            }
            else
            {
                return -1;
            }
        }

        #endregion

        #region"Check Status By Rank"

        public bool CheckStatusByRank()
        {
            StatusManager = new Status();
            AM_Status status = StatusManager.GetStatusByRanking(Convert.ToInt32(rankingddl.SelectedValue));
            if (status == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region"Reset Page"

        public void ResetPage()
        {
            try
            {
                statustxt.Text = string.Empty;

                //frequencyddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                //periodddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                //param1chk.Checked = true;
                //rentamounttxt.Text = string.Empty;
                //param2chk.Checked = false;
                reviewperiodtxt.Text = string.Empty;
                reviewddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                triggertxt.Text = string.Empty;
                triggerddl.SelectedValue = Am.Ahv.Utilities.constants.ApplicationConstants.defaulvalue;
                statusdetailstxt.Text = string.Empty;
                noticeparachk.Checked = false;
                recoverychk.Checked = false;
                issuedatechk.Checked = false;
                expirydatechk.Checked = false;
                HearingDateCheck.Checked = false;
                docuploadchk.Checked = false;
                WarrantExpiryDateCheck.Checked = false;
                RecoveryAmount.Visible = false;
                noticeissue.Visible = false;
                NoticeExpiryDate.Visible = false;
                HearingDate.Visible = false;
                WarrantExpiryDate.Visible = false;
                RegenrateRankingDdl();
            }
            catch (KeyNotFoundException keynotfound)
            {
                IsError = true;
                ExceptionPolicy.HandleException(keynotfound, "Exception Policy");
            }
            catch (ArgumentOutOfRangeException nullref)
            {
                IsError = true;
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsError)
                {
                    SetMessage(UserMessageConstants.exceptionResetPageStatus, true);
                }
            }
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Validate"

        public void Validate(string str)
        {
            Validation validation = new Validation();

            if (validation.emptyString(str))
            {
                IsError = true;
                return;
            }
            else if (validation.lengthString(str))
            {
                IsError = true;
                SetMessage(UserMessageConstants.stringLengthMessage, true);
                return;
            }
            else
            {
                IsError = false;
            }
        }

        #endregion

        #region"Notice Parameters Check Box Checked Changed"

        protected void noticeparachk_CheckedChanged(object sender, EventArgs e)
        {
            if (noticeparachk.Checked)
            {
                if (ViewState["RecoveryAmount"] != null)
                {

                    if (Convert.ToBoolean(ViewState["RecoveryAmount"]))
                    {
                        RecoveryAmount.Visible = true;
                    }
                    else
                    {
                        RecoveryAmount.Visible = false;
                    }
                }
                else
                {
                    RecoveryAmount.Visible = true;
                }
                if (ViewState["NoticeIssueDate"] != null)
                {

                    if (Convert.ToBoolean(ViewState["NoticeIssueDate"]))
                    {
                        noticeissue.Visible = true;

                    }
                    else
                    {
                        noticeissue.Visible = false;
                    }
                }
                else
                {
                    noticeissue.Visible = true;
                }

                if (ViewState["NoticeExpiryDate"] != null)
                {
                    if (Convert.ToBoolean(ViewState["NoticeExpiryDate"]))
                    {
                        NoticeExpiryDate.Visible = true;
                    }
                    else
                    {
                        NoticeExpiryDate.Visible = false;
                    }
                }
                else
                {
                    NoticeExpiryDate.Visible = true;
                }
                if (ViewState["HearingDate"] != null)
                {
                    if (Convert.ToBoolean(ViewState["HearingDate"]))
                    {
                        HearingDate.Visible = true;

                    }
                    else
                    {
                        HearingDate.Visible = false;
                    }
                }
                else
                {
                    HearingDate.Visible = true;
                }

                if (ViewState["WarrantExpireDate"] != null)
                {
                    if (Convert.ToBoolean(ViewState["WarrantExpireDate"]))
                    {
                        WarrantExpiryDate.Visible = true;

                    }
                    else
                    {
                        WarrantExpiryDate.Visible = false;
                    }
                }
                else
                {
                    WarrantExpiryDate.Visible = true;
                }
            }
            else
            {
                recoverychk.Checked = false;
                noticeparachk.Checked = false;
                issuedatechk.Checked = false;
                expirydatechk.Checked = false;
                HearingDateCheck.Checked = false;
                WarrantExpiryDateCheck.Checked = false;

                RecoveryAmount.Visible = false;
                noticeissue.Visible = false;
                NoticeExpiryDate.Visible = false;
                HearingDate.Visible = false;
                WarrantExpiryDate.Visible = false;


            }
        }

        #endregion
    }
}