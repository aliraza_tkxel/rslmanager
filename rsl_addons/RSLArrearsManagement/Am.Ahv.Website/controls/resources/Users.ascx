﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Users.ascx.cs" Inherits="Am.Ahv.Website.controls.resources.Users" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:UpdatePanel ID="upUsers" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updPnlMessage">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table class="tableClass">
            <tr>
                <td class="tableHeader">
                    <asp:Label ID="lblUserHeading" runat="server" Text="Users"></asp:Label>
                </td>
            </tr>
            <tr valign="top">
                <td valign="top" style="padding-bottom: 30px; padding-left: 5px;">
                    <asp:Panel ID="pnlUser" runat="server" Width="350px">
                        <cc1:PagingGridView ID="pgvUsers" EnableViewState="true" AutoGenerateColumns="false"
                            AllowSorting="true" GridLines="None" runat="server" Width="350px" EmptyDataText="No records found."
                            EmptyDataRowStyle-Font-Bold="true" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-HorizontalAlign="Center">
                            <AlternatingRowStyle VerticalAlign="Top" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnEdit" CausesValidation="false" CommandArgument='<%#Eval("ResourceId") %>'
                                            OnClick="pgvUsers_RowEditing" runat="server" Text="Edit" />
                                        <asp:Button ID="btnSave" CausesValidation="false" CommandArgument='<%#Eval("ResourceId") %>'
                                            OnClick="pgvUsers_RowUpdating" Visible="false" runat="server" Text="Save" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton Font-Underline="True" ForeColor="Black" CausesValidation="false"
                                            ID="lBtnEmployeeName" runat="server" Text='<%# Eval("EmployeeName") %>' CommandArgument='<%# Eval("ResourceId") %>'
                                            OnClick="lBtnEmployeeName_Click"></asp:LinkButton><br />
                                        <asp:Label ID="lBtnDepartment" runat="server" Text='<%# Eval("JOBTITLE") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hflLookupId" runat="server" Value='<%#Eval("LookupCodeId") %>' />
                                        <asp:Label ID="lblUserType" runat="server" Text='<%# Eval("CodeName") %>'></asp:Label>
                                        <asp:DropDownList ID="ddlUserTypeOptions" Width="105px" Visible="false" EnableViewState="true"
                                            runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings PreviousPageText="" NextPageText="" Mode="NextPrevious" Position="Bottom">
                            </PagerSettings>
                            <RowStyle VerticalAlign="Top" />
                        </cc1:PagingGridView>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="tableHeader">
                </td>
            </tr>
            <tr valign="top">
                <td>
                    <table style="width: 350px;">
                        <tr>
                            <td valign="top" width="100px">
                                <asp:Label ID="lblUsers" runat="server" Text="Users"></asp:Label><br />
                                <asp:Label ID="lblUserOnPage" runat="server"></asp:Label>
                                &nbsp;<asp:Label ID="lblUsersOf" runat="server" Text="of"></asp:Label>
                                &nbsp;<asp:Label ID="lblUserTotal" runat="server"></asp:Label>
                            </td>
                            <td valign="top" width="100px">
                                <asp:Label ID="lblPage" runat="server" Text="Page"></asp:Label><br />
                                <asp:Label ID="lblPageCurrent" runat="server"></asp:Label>
                                &nbsp;<asp:Label ID="lblPageOf" runat="server" Text="of"></asp:Label>
                                &nbsp;<asp:Label ID="lblPageTotal" runat="server"></asp:Label>
                            </td>
                            <td valign="top" width="70px">
                                <asp:LinkButton ID="BtnPrevious" runat="server" CausesValidation="false" OnClick="lBtnPrevious_Click"
                                    Text="&lt; Previous"></asp:LinkButton>
                            </td>
                            <td valign="top" width="50px">
                                <asp:LinkButton ID="BtnNext" runat="server" CausesValidation="false" OnClick="lBtnNext_Click"
                                    Text="Next &gt;"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="right" class="tableFooter">
                    <asp:Button ID="lBtnAddCaseWorker" runat="server" CausesValidation="false" OnClick="lBtnAddCaseWorker_Click"
                        Text="+ Case Worker" Width="100px" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hflCurrentPage" runat="server" />
        <asp:HiddenField ID="hflTotalPages" runat="server" />
        <asp:HiddenField ID="hflTotalRecords" runat="server" />
        <asp:HiddenField ID="hflDBIndex" runat="server" />
        <asp:HiddenField ID="hflCurrentRecords" runat="server" />
        <asp:HiddenField ID="hflRowIndex" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>
