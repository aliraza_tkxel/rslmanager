﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Action.ascx.cs" Inherits="Am.Ahv.Website.controls.resources.Action" %>
<style type="text/css">
    .style1
    {
        width: 35%;
    }
</style>
<table style="width: 500px">
    <tr>
        <td colspan="3">
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td width="15%">
            <asp:Label ID="lblActionTitle" runat="server" Text="Action Title:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="lblStatus" Font-Bold="true" runat="server" Text=""></asp:Label>
        </td>
        <td width="50%">
            <asp:Label ID="lblErrorStar" runat="server" Text="*" Visible="true" Font-Bold="true"
                ForeColor="Red"></asp:Label> 
            <asp:TextBox ID="txtActionTitle" EnableViewState="true" runat="server" AutoPostBack="true"
                Width="147px" OnTextChanged="txtActionTitle_TextChanged"></asp:TextBox>
            <br />
        </td>
    </tr>
    <tr>
        <td width="20%">
            <asp:Label ID="lblRanking" runat="server" Text="Ranking:"></asp:Label>
        </td>
        <td colspan="2">
            <asp:Label ID="lblErrorRanking" runat="server" Text="*" EnableViewState="true" Visible="true"
                Font-Bold="true" ForeColor="Red"></asp:Label>
            <asp:DropDownList ID="ddlRanking" runat="server" Width="105px" EnableViewState="true">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblRecommendedFollowupPeriod" runat="server" Text="Remind Me Period:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="lblErrorFollowup" runat="server" Text="*" Visible="true" Font-Bold="true"
                ForeColor="Red"></asp:Label>
            <asp:TextBox ID="txtRecommendedFollowUpPeriod" runat="server" Width="50px" EnableViewState="true"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lblErrorFollowupDDL" runat="server" Text="*" Visible="true" Font-Bold="true"
                ForeColor="Red"></asp:Label>
            <asp:DropDownList ID="ddlRecommendedFollowUpPeriodFrequency" Width="105px"
                runat="server" EnableViewState="true">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td width="30px">
            &nbsp;
        </td>
        <td colspan="2">
            <asp:RegularExpressionValidator ID="revFollowup" ControlToValidate="txtRecommendedFollowUpPeriod"
                ValidationExpression="^\d+$" runat="server" ErrorMessage="Please enter in numeric form"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Please enter less than 100"
                ControlToValidate="txtRecommendedFollowUpPeriod" MinimumValue="1" MaximumValue="99"
                Type="Integer" Display="Dynamic"></asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td width="30%">
            <asp:Label ID="lblTriggerForNextAction" runat="server" Text="Trigger For Next Action:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="lblErrorNextAction" runat="server" Text="*" Visible="true" Font-Bold="true"
                ForeColor="Red"></asp:Label>
             <asp:TextBox ID="txtTriggerForNextAction" runat="server" Width="50px" EnableViewState="true"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lblErrorNextActionDDL" runat="server" Text="*" Visible="true" Font-Bold="true"
                ForeColor="Red"></asp:Label>
            <asp:DropDownList ID="ddlTriggerForNextAction" runat="server" Width="105px"
                EnableViewState="true">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td width="30px">
            &nbsp;
        </td>
        <td colspan="2">
            <asp:RegularExpressionValidator ID="revNextActionAlert" ControlToValidate="txtTriggerForNextAction"
                ValidationExpression="^\d+$" runat="server" ErrorMessage="Please enter in numeric form"></asp:RegularExpressionValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Please enter less than 100"
                ControlToValidate="txtTriggerForNextAction" MinimumValue="1" MaximumValue="99"
                Type="Integer" Display="Dynamic"></asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td width="30%">
            <asp:Label ID="lblNextActionDetails" runat="server" Text="Next Action Details:"></asp:Label>
        </td>
        <td colspan="2">
            <asp:Label ID="lblErrorNextActionDetails" runat="server" Text="*" Visible="true"
                Font-Bold="true" ForeColor="Red"></asp:Label>
             <asp:TextBox ID="txtNextActionDetails" TextMode="MultiLine" runat="server"
                Height="70px" EnableViewState="true" Width="280px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td width="20%">
        </td>
        <td width="20%" colspan="2">
            <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
            &nbsp;
            <asp:Button ID="btnRemove" Width="70px" runat="server" Text="Remove" OnClick="btnRemove_Click" />
        </td>
    </tr>
    <tr>
        <td width="20%">
            <asp:Label ID="lblStandardLetter" runat="server" Text="Standard Letter:"></asp:Label>
        </td>
        <td colspan="2">
            <asp:ListBox ID="lboxstandardletters" runat="server" Height="70px" Width="291px">
            </asp:ListBox>
        </td>
    </tr>
    <tr>
        <td width="40%">
            <asp:Label ID="lblProceduralAction" runat="server" Text="This is a Procedural Action:"></asp:Label>
        </td>
        <td width="10%" colspan="2">
            <asp:CheckBox ID="chkboxProceduralAction" runat="server" EnableViewState="true" />
            &nbsp;
            <asp:Label ID="lbl" runat="server" Text="(Select so that an alert is generated)"></asp:Label>
        </td>
    </tr>
    <tr>
        <td width="40%">
            <asp:Label ID="lblProceduralAction0" runat="server" Text="Payment Plan Mandatory:"></asp:Label>
        </td>
        <td width="10%" colspan="2">
            <asp:CheckBox ID="checkboxPaymentPlan" runat="server" EnableViewState="true" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="right">
            <asp:Button ID="btnDelete" runat="server" Width="70px" Text="Delete" OnClick="btnDelete_Click" />
            &nbsp;
            <asp:Button ID="btnCancel" runat="server" Width="70px" Text="Cancel" OnClick="btnCancel_Click" />
        </td>
        <td>
            <asp:Button ID="btnSaveChanges" Width="100px" runat="server" Text="Save Changes"
                OnClick="btnSaveChanges_Click" />
        </td>
    </tr>
</table>
