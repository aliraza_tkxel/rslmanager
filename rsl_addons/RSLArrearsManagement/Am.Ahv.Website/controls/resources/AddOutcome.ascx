﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddOutcome.ascx.cs" Inherits="Am.Ahv.Website.controls.resources.AddOutcome" %>
<style type="text/css">
    .style1
    {
        width: 100px;
    }
</style>
<table class="tableClass" width="100%">
    <tr>
        <td class="tableHeader">
            <asp:Label ID="lblNewOutcome" runat="server" Text="New Outcome"></asp:Label>
        </td>        
    </tr>
    <tr>
        <td>        
            <table>
                <tr>
                    <td colspan="4">
                         <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </asp:Panel>
                        <br />                                              
                    </td>        
                </tr>
                <tr>
                    <td align="left" class="style1" valign="bottom">
                        <asp:Label ID="lblTitle" runat="server" Text="Title :" Width="50px"></asp:Label>&nbsp;
                        </td>
                    <td align="center" valign="middle">
                        <asp:Label ID="lblErrorStar" Font-Bold="true" Visible="true" ForeColor="Red" runat="server" Text="*"></asp:Label>
                    </td>
                    <td align="left" valign="top" >
                        <asp:TextBox ID="txtOutcome" MaxLength="150" runat="server" Width="260px"></asp:TextBox>
                    </td>
                    <td align="left">
                        
                    </td>
            
                </tr>
                <tr>
                    <td colspan ="4">
                        <br />
                        <br />
                    </td>        
                </tr>
                <tr>
                    <td align="right" class="style1">
                        &nbsp;</td>
                    <td align="right">
                        &nbsp;</td>
                    <td align="right" width= "352px">
                        <asp:Button ID="btnCancel" TabIndex="1" runat="server" Text="Cancel" Height="26px" 
                            Width="50px" onclick="btnCancel_Click" />
                    </td>
                    <td align="left" width="100px" >
                        <asp:Button Width="50px" ID="btnSave" TabIndex="0" runat="server" Text="Save" onclick="btnSave_Click" 
                            Height="26px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <br />                     
                    </td>        
                </tr>
            </table>
        </td>    
    </tr>
    
</table>

