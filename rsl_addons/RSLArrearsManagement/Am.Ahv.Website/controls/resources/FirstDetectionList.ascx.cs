﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.statusmgmt;
using Am.Ahv.Utilities.utitility_classes;
using System.Data;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Transactions;
using System.Drawing;
using Am.Ahv.Website.pagebase;
//using System.Transactions;

namespace Am.Ahv.Website.controls.status_and_actions
{
    #region"Delegate"

    #region"Update Tree"

    public delegate void updateStatusAndActionTreeFromFirstDetection(bool flag);

    #endregion

    #region"Reset Node Selection"

    public delegate void resetNodeSelectionFirstDetection(bool flag);

    #endregion

    #endregion

    public partial class FirstDetectionList : UserControlBase
    {

        #region"Attributes"

        Am.Ahv.BusinessManager.statusmgmt.Status StatusManager = null;
        bool isException { set; get; }

        #region"Is Error"

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        #endregion

        #endregion

        #region Events

        #region"Delegate Event"

        #region"Update Tree Event"

        public event updateStatusAndActionTreeFromFirstDetection updateTree;

        #endregion

        #region"Reset Selected Node Event"

        public event resetNodeSelectionFirstDetection reset;

        #endregion

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ResetMessage();
            if (!IsPostBack)
            {
                InitLookups();
            }
        }
        #endregion

        #region savebutton

        protected void savebtn_Click(object sender, EventArgs e)
        {
            CheckSelection();
            if (!IsError)
            {
                bool flag = AddStatus();

                if (flag)
                {
                    ResetPage();
                    updateTree(true);
                }

            }
            else
            {
                return;
            }
        }

        #endregion

        #region"Cancel Button Click"

        protected void cancelbtn_Click(object sender, EventArgs e)
        {
            ResetPage();
            ResetMessage();
            reset(true);
        }

        #endregion

        #endregion

        #region Lookups

        public void InitLookups()
        {
            try
            {
                StatusManager = new Status();
                statustxt.Text = "Initial Case Monitoring";
                statustxt.ReadOnly = true;
                //******** setting the rent frequency drop down
                frequencyddl.DataSource = StatusManager.GetRentList();
                frequencyddl.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                frequencyddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                frequencyddl.DataBind();
                frequencyddl.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                frequencyddl.SelectedValue = ApplicationConstants.defaulvalue;

                //******* Setting Rent Period Drop Down List

                periodddl.DataSource = StatusManager.GetReviewPeriod();
                periodddl.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                periodddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                periodddl.DataBind();
                periodddl.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                periodddl.SelectedValue = ApplicationConstants.defaulvalue;

                //***** Setting Review Period Drop Down Llist
                reviewddl.DataSource = StatusManager.GetReviewPeriod();
                reviewddl.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                reviewddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                reviewddl.DataBind();
                reviewddl.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                reviewddl.SelectedValue = ApplicationConstants.defaulvalue;

                ///****** Setting Trigger Period Drop Down List
                ///
                triggerddl.DataSource = StatusManager.GetReviewPeriod();
                triggerddl.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                triggerddl.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                triggerddl.DataBind();
                triggerddl.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                triggerddl.SelectedValue = ApplicationConstants.defaulvalue;
            }
            catch (NullReferenceException e)
            {
                isException = true;
                ExceptionPolicy.HandleException(e, "Exception Policy");
            }
            catch (EntityException ee)
            {
                isException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingLookupsFirstDetection, true);
                }
            }
        }

        #endregion

        #region"Populate Data"

        public void LoadFirstDetection()
        {
            try
            {
                if (!Convert.ToString(Session[SessionConstants.StatusID]).Equals(string.Empty))
                {
                    StatusManager = new Status();
                    AM_Status status = StatusManager.GetStatusById(Convert.ToInt32(Session[SessionConstants.StatusID]));
                    statustxt.Text = status.Title;
                    if (status.IsRentAmount == true)
                    {
                        rentamounttxt.Text = status.RentAmount.ToString();
                        param2chk.Checked = Convert.ToBoolean(status.IsRentAmount);
                    }
                    if (status.IsRentParameter == true)
                    {
                        frequencyddl.SelectedValue = status.RentParameter.ToString();
                        periodddl.SelectedValue = status.RentParameterFrequencyLookupCodeId.ToString();
                        param1chk.Checked = Convert.ToBoolean(status.IsRentParameter);
                    }
                    rankingddl.SelectedValue = status.Ranking.ToString();
                    reviewperiodtxt.Text = status.ReviewPeriod.ToString();
                    reviewddl.SelectedValue = status.ReviewPeriodFrequencyLookupCodeId.ToString();// status.AM_LookupCodeReference.EntityKey.EntityKeyValues[0].Value.ToString();
                    triggertxt.Text = status.NextStatusAlert.ToString();
                    triggerddl.SelectedValue = status.NextStatusAlertFrequencyLookupCodeId.ToString();// status.AM_LookupCodeReference.EntityKey.EntityKeyValues[0].Value.ToString();
                    statusdetailstxt.Text = status.NextStatusDetail;
                    noticeparachk.Checked = Convert.ToBoolean(status.IsNoticeParameterRequired);
                    if (status.IsNoticeParameterRequired.Value)
                    {
                        RecoveryAmount.Visible = true;
                        noticeissue.Visible = true;
                        NoticeExpiryDate.Visible = true;
                        HearingDate.Visible = true;
                        WarrantExpiryDate.Visible = true;
                    }
                    docuploadchk.Checked = Convert.ToBoolean(status.IsDocumentUpload);
                    recoverychk.Checked = Convert.ToBoolean(status.IsRecoveryAmount);
                    issuedatechk.Checked = Convert.ToBoolean(status.IsNoticeIssueDate);
                    expirydatechk.Checked = Convert.ToBoolean(status.IsNoticeExpiryDate);
                    HearingDateCheck.Checked = Convert.ToBoolean(status.IsHearingDate);
                    WarrantExpiryDateCheck.Checked = Convert.ToBoolean(status.IsWarrantExpiryDate);

                    if (Convert.ToBoolean(status.IsNoticeParameterRequired))
                    {
                        if (Convert.ToBoolean(status.IsRecoveryAmount))
                        {
                            RecoveryAmount.Visible = true;
                            ViewState["RecoveryAmount"] = true;
                        }
                        else
                        {
                            RecoveryAmount.Visible = false;
                        }
                        if (Convert.ToBoolean(status.IsNoticeIssueDate))
                        {
                            noticeissue.Visible = true;
                            ViewState["NoticeIssueDate"] = true;
                        }
                        else
                        {
                            noticeissue.Visible = false;
                        }
                        if (Convert.ToBoolean(status.IsNoticeExpiryDate))
                        {
                            NoticeExpiryDate.Visible = true;
                            ViewState["NoticeExpiryDate"] = true;
                        }
                        else
                        {
                            NoticeExpiryDate.Visible = false;
                        }
                        if (Convert.ToBoolean(status.IsHearingDate))
                        {
                            HearingDate.Visible = true;
                            ViewState["HearingDate"] = true;
                        }
                        else
                        {
                            HearingDate.Visible = false;
                        }
                        if (Convert.ToBoolean(status.IsWarrantExpiryDate))
                        {

                            WarrantExpiryDate.Visible = true;
                            ViewState["WarrantExpireDate"] = true;
                        }
                        else
                        {
                            WarrantExpiryDate.Visible = false;
                        }
                    }
                    else
                    {
                        recoverychk.Checked = false;
                        noticeparachk.Checked = false;
                        issuedatechk.Checked = false;
                        expirydatechk.Checked = false;
                        HearingDateCheck.Checked = false;
                        WarrantExpiryDateCheck.Checked = false;

                        RecoveryAmount.Visible = false;
                        noticeissue.Visible = false;
                        NoticeExpiryDate.Visible = false;
                        HearingDate.Visible = false;
                        WarrantExpiryDate.Visible = false;
                    }

                }
            }
            catch (EntityException entityexception)
            {
                isException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingFirstDetection, true);
                }
            }
        }

        #endregion

        #region Utility Methods

        #region"Check Selection"

        public void CheckSelection()
        {
            if (param1chk.Checked && param2chk.Checked)
            {
                SetMessage(UserMessageConstants.invalidSelection, true);
                IsError = true;
            }
            else
            {
                IsError = false;
            }
        }

        #endregion

        #region"Add Status"

        public bool AddStatus()
        {
            try
            {
                AM_Status status = new AM_Status();
                AM_StatusHistory statushistory = new AM_StatusHistory();
                StatusManager = new Status();

                ///setting tilte
                Validate(statustxt.Text);
                if (!IsError)
                {
                    status.Title = this.statustxt.Text;
                    statushistory.Title = this.statustxt.Text;
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return false;
                }

                //setting ranking

                if (rankingddl.SelectedValue == ApplicationConstants.defaulvalue)
                {
                    SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                    return false;
                }
                else
                {
                    status.Ranking = int.Parse(rankingddl.SelectedValue);
                    statushistory.Ranking = int.Parse(rankingddl.SelectedValue);
                }

                if (param1chk.Checked)
                {
                    //setting rent parameter
                    if (frequencyddl.SelectedValue == ApplicationConstants.defaulvalue)
                    {
                        SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                        return false;
                    }
                    else
                    {
                        status.RentParameter = int.Parse(frequencyddl.SelectedValue);
                        statushistory.RentParameter = int.Parse(frequencyddl.SelectedValue);
                    }

                    //setting rent parameter review period drop down list
                    if (periodddl.SelectedValue == ApplicationConstants.defaulvalue)
                    {
                        SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                        return false;
                    }
                    else
                    {
                        status.RentParameterFrequencyLookupCodeId = int.Parse(periodddl.SelectedValue);
                        statushistory.RentParameterFrequencyLookupCodeId = int.Parse(periodddl.SelectedValue);
                    }
                    //setting rent parametere period checkbox

                    status.IsRentParameter = param1chk.Checked;
                    statushistory.IsRentParameter = param1chk.Checked;

                    status.IsRentAmount = param2chk.Checked;
                    statushistory.IsRentAmount = param2chk.Checked;
                }
                if (param2chk.Checked)
                {
                    //setting rent amount
                    Validate(rentamounttxt.Text);
                    if (!IsError)
                    {
                        status.RentAmount = int.Parse(rentamounttxt.Text);
                        statushistory.RentAmount = int.Parse(rentamounttxt.Text);
                    }
                    else
                    {
                        SetMessage(UserMessageConstants.requiredFieldMessage, true);
                        return false;
                    }

                    //setting rent amount check box                
                    status.IsRentAmount = param2chk.Checked;
                    statushistory.IsRentAmount = param2chk.Checked;

                    status.IsRentParameter = param1chk.Checked;
                    statushistory.IsRentParameter = param1chk.Checked;

                }
                //setting ranking

                status.Ranking = 1;
                statushistory.Ranking = 1;

                //setting review period

                Validate(reviewperiodtxt.Text);
                if (!IsError)
                {

                    status.ReviewPeriod = int.Parse(reviewperiodtxt.Text);
                    statushistory.ReviewPeriod = int.Parse(reviewperiodtxt.Text);
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return false;
                }

                //setting review period dropdown
                if (reviewddl.SelectedValue == ApplicationConstants.defaulvalue)
                {
                    SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                    return false;
                }
                else
                {
                    status.ReviewPeriodFrequencyLookupCodeId = int.Parse(reviewddl.SelectedValue);
                    //status.AM_LookupCodeReference.EntityKey = new EntityKey("TKXEL_RSLManagerEntities.AM_LookupCode", "LookupCodeId", int.Parse(reviewddl.SelectedValue));
                    statushistory.ReviewPeriodFrequencyLookupCodeId = int.Parse(reviewddl.SelectedValue);
                }

                //setting trigger value
                Validate(triggertxt.Text);
                if (!IsError)
                {
                    status.NextStatusAlert = int.Parse(triggertxt.Text);
                    statushistory.NextStatusAlert = int.Parse(triggertxt.Text);
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return false;
                }

                //setting trigger drop down list
                if (triggerddl.SelectedValue == ApplicationConstants.defaulvalue)
                {
                    SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                    return false;
                }
                else
                {
                    status.NextStatusAlertFrequencyLookupCodeId = int.Parse(triggerddl.SelectedValue);

                    statushistory.NextStatusAlertFrequencyLookupCodeId = int.Parse(triggerddl.SelectedValue);
                }

                //settig next status details
                Validate(statusdetailstxt.Text);
                if (!IsError)
                {
                    status.NextStatusDetail = statusdetailstxt.Text;
                    statushistory.NextStatusDetail = statusdetailstxt.Text;
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return false;
                }

                // setting notice parameter required checkbox

                status.IsNoticeParameterRequired = noticeparachk.Checked;
                statushistory.IsNoticeParameterRequired = noticeparachk.Checked;

                //setting document upload parameter
                status.IsDocumentUpload = docuploadchk.Checked;
                statushistory.IsDocumentUpload = docuploadchk.Checked;

                status.IsRecoveryAmount = recoverychk.Checked;
                statushistory.IsRecoveryAmount = recoverychk.Checked;
                status.IsNoticeIssueDate = issuedatechk.Checked;
                statushistory.IsNoticeIssueDate = issuedatechk.Checked;

                status.IsNoticeExpiryDate = expirydatechk.Checked;
                statushistory.IsNoticeExpiryDate = expirydatechk.Checked;
                status.IsWarrantExpiryDate = WarrantExpiryDateCheck.Checked;
                statushistory.IsWarrantExpiryDate = WarrantExpiryDateCheck.Checked;
                status.IsHearingDate = HearingDateCheck.Checked;
                statushistory.IsHearingDate = HearingDateCheck.Checked;

                status.CreatedDate = DateTime.Now;
                statushistory.CreatedDate = DateTime.Now;
                status.ModifiedDate = DateTime.Now;
                statushistory.ModifiedDate = DateTime.Now;
                status.CreatedBy = base.GetCurrentLoggedinUser();

                statushistory.CreatedBy = base.GetCurrentLoggedinUser();
                status.ModifiedBy = base.GetCurrentLoggedinUser();
                statushistory.ModifiedBy = base.GetCurrentLoggedinUser();

                AM_Status dummy = new AM_Status();
                if (!Convert.ToString(Session[SessionConstants.StatusID]).Equals(string.Empty))
                {
                    StatusManager.updateStatus(Convert.ToInt32(Session[SessionConstants.StatusID]), status, statushistory);
                    SetMessage(UserMessageConstants.updateSuccess, false);
                    Session.Remove(SessionConstants.StatusID);
                    return true;
                }
                else
                {
                    if (!StatusManager.IsPresent(int.Parse(rankingddl.SelectedValue)))
                    {
                        dummy = StatusManager.SaveStatus(status, statushistory);
                        SetMessage(UserMessageConstants.InsertSuccess, false);
                        Session.Remove(SessionConstants.StatusID);
                        if (dummy != null)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        SetMessage(UserMessageConstants.FirstDetectionMessage, true);
                    }
                }


            }
            catch (TransactionAbortedException transactionaborted)
            {
                isException = true;
                ExceptionPolicy.HandleException(transactionaborted, "Exception Policy");
            }
            catch (TransactionException transactionexception)
            {
                isException = true;
                ExceptionPolicy.HandleException(transactionexception, "Exception Policy");
            }
            catch (EntityException entityexception)
            {
                isException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage(UserMessageConstants.exceptionAddDetection, true);
                }
            }


            return false;

        }

        private bool IsPresent()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region"Reset Page"

        public void ResetPage()
        {
            rankingddl.SelectedValue = ApplicationConstants.defaulvalue;
            frequencyddl.SelectedValue = ApplicationConstants.defaulvalue;
            periodddl.SelectedValue = ApplicationConstants.defaulvalue;
            param1chk.Checked = false;
            rentamounttxt.Text = string.Empty;
            param2chk.Checked = false;
            reviewperiodtxt.Text = string.Empty;
            rankingddl.SelectedValue = ApplicationConstants.defaulvalue;
            reviewddl.SelectedValue = ApplicationConstants.defaulvalue;
            triggertxt.Text = string.Empty;
            triggerddl.SelectedValue = ApplicationConstants.defaulvalue;
            statusdetailstxt.Text = string.Empty;
            noticeparachk.Checked = false;
            docuploadchk.Checked = false;
            noticeparachk.Checked = false;
            recoverychk.Checked = false;
            issuedatechk.Checked = false;
            expirydatechk.Checked = false;
            WarrantExpiryDateCheck.Checked = false;
            HearingDateCheck.Checked = false;
            RecoveryAmount.Visible = false;
            noticeissue.Visible = false;
            NoticeExpiryDate.Visible = false;
            HearingDate.Visible = false;
            WarrantExpiryDate.Visible = false;


        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Validate"

        public void Validate(string str)
        {
            Validation validation = new Validation();

            if (validation.emptyString(str))
            {
                IsError = true;
                return;
            }
            else if (validation.lengthString(str))
            {
                IsError = true;
                SetMessage(UserMessageConstants.stringLengthMessage, true);
                return;
            }
            else
            {
                IsError = false;
            }
        }

        #endregion

        protected void noticeparachk_CheckedChanged(object sender, EventArgs e)
        {
            if (noticeparachk.Checked)
            {
                if (ViewState["RecoveryAmount"] != null)
                {

                    if (Convert.ToBoolean(ViewState["RecoveryAmount"]))
                    {
                        RecoveryAmount.Visible = true;
                    }
                    else
                    {
                        RecoveryAmount.Visible = false;
                    }
                }
                else
                {
                    RecoveryAmount.Visible = true;
                }
                if (ViewState["NoticeIssueDate"] != null)
                {

                    if (Convert.ToBoolean(ViewState["NoticeIssueDate"]))
                    {
                        noticeissue.Visible = true;

                    }
                    else
                    {
                        noticeissue.Visible = false;
                    }
                }
                else
                {
                    noticeissue.Visible = true;
                }

                if (ViewState["NoticeExpiryDate"] != null)
                {
                    if (Convert.ToBoolean(ViewState["NoticeExpiryDate"]))
                    {
                        NoticeExpiryDate.Visible = true;
                    }
                    else
                    {
                        NoticeExpiryDate.Visible = false;
                    }
                }
                else
                {
                    NoticeExpiryDate.Visible = true;
                }
                if (ViewState["HearingDate"] != null)
                {
                    if (Convert.ToBoolean(ViewState["HearingDate"]))
                    {
                        HearingDate.Visible = true;

                    }
                    else
                    {
                        HearingDate.Visible = false;
                    }
                }
                else
                {
                    HearingDate.Visible = true;
                }

                if (ViewState["WarrantExpireDate"] != null)
                {
                    if (Convert.ToBoolean(ViewState["WarrantExpireDate"]))
                    {
                        WarrantExpiryDate.Visible = true;

                    }
                    else
                    {
                        WarrantExpiryDate.Visible = false;
                    }
                }
                else
                {
                    WarrantExpiryDate.Visible = true;
                }
            }
            else
            {
                recoverychk.Checked = false;
                noticeparachk.Checked = false;
                issuedatechk.Checked = false;
                expirydatechk.Checked = false;
                HearingDateCheck.Checked = false;
                WarrantExpiryDateCheck.Checked = false;

                RecoveryAmount.Visible = false;
                noticeissue.Visible = false;
                NoticeExpiryDate.Visible = false;
                HearingDate.Visible = false;
                WarrantExpiryDate.Visible = false;


            }
        }

        #endregion

    }
}