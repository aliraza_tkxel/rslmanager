using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.utitility_classes;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.constants;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
using Am.Ahv.Website.pagebase;
using System.Text;
using Am.Ahv.BusinessManager.letters;
using System.Collections;
using System.Configuration;
namespace Am.Ahv.Website.controls.resources
{
    #region"Delegate"

    #region"Update Tree"

    public delegate void updateStatusAndActionTree(bool flag);

    #endregion

    #region"Reset Selected Node"

    public delegate void resetSelectedNodeAction(bool flag);

    #endregion

    #endregion

    public partial class Action : UserControlBase
    {
        #region"Attributes"

        #region"Action Manager"

        Am.Ahv.BusinessManager.statusmgmt.Action actionManager = null;

        #endregion

        #region"Is Error"

        bool isError;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }

        #endregion

        #region"Is Exception"

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        ArrayList arraListNewStandardletters;

        #endregion

        #region"Events"

        #region"Delegate Event"

        #region"Update Status And Action Tree Delegate Event"

        public event updateStatusAndActionTree updateStatusAndActionTreeEvent;

        #endregion

        #region"Reset Selected Node Event"

        public event resetSelectedNodeAction reset;

        #endregion

        #endregion

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
            ResetMessage();
        }

        #endregion

        #region"Btn Save Changes Click"

        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            SaveData();
            updateStatusAndActionTreeEvent(true);
        }

        #endregion

        #region"Btn Cancel Click"

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ResetPage();
            Session.Remove(SessionConstants.ActionId);
            reset(true);
        }

        #endregion

        #region"Btn Delete Click"
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessManager.statusmgmt.Action mgr = new BusinessManager.statusmgmt.Action();
                Button b = (Button)sender;
                if (mgr.DeleteAction(Int32.Parse(Session[SessionConstants.ActionId].ToString())))
                {
                    SetMessage("Action has been deleted successfuly", false);
                    ResetPage();
                    Session.Remove(SessionConstants.ActionId);
                    // reset(true);
                    updateStatusAndActionTreeEvent(true);
                    DisableDeleteButton();
                }

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    SetMessage("Action couldn't be deleted ", true);
                }
            }
        }
        #endregion

        #region"Btn Remove Click"

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            if (lboxstandardletters.SelectedValue == "-1" || lboxstandardletters.SelectedValue == "")
            {
                SetMessage(UserMessageConstants.selectstandardLetter, true);
                return;
            }
            else
            {
                if (CheckNewStandardLetterId(lboxstandardletters.SelectedValue))
                {
                    lboxstandardletters.Items.RemoveAt(lboxstandardletters.SelectedIndex);
                }
                else
                {
                    int id = int.Parse(lboxstandardletters.SelectedValue);
                    actionManager = new BusinessManager.statusmgmt.Action();
                    actionManager.DisableActionDocument(id, int.Parse(Session[SessionConstants.ActionId].ToString()));
                    lboxstandardletters.Items.RemoveAt(lboxstandardletters.SelectedIndex);
                }

            }

        }

        #endregion

        #region"Disable Delete Button"

        public void DisableDeleteButton()
        {
            btnDelete.Enabled = false;
        }

        #endregion

        #region"Enable Delete Button"

        public void EnableDeleteButton()
        {
            btnDelete.Enabled = true;
        }

        #endregion

        #region Add Letter Button Click

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AM_Action Action = GetCachedAction();
                if (ValidateFields())
                {
                    Session["CachedAction"] = Action;
                    Session["lblStatus"] = lblStatus.Text;
                    FileOperations f = new FileOperations();
                    string Path = f.getCurUrlAbsolutePath();
                    Session["PrevPage"] = Path;
                    Response.Redirect(PathConstants.lettersResourceAreaPathCached + "?cmd=newAction", false);
                }
                else
                {
                    SetMessage("Please fill the required fields", true);
                }
            }
            catch (NullReferenceException nullref)
            {

                ExceptionPolicy.HandleException(nullref, "Exception Policy");
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }

        }        
        #endregion

        #region"Txt Action Title Text Changed"

        protected void txtActionTitle_TextChanged(object sender, EventArgs e)
        {
            if (Validation.CheckStringLength(txtActionTitle.Text))
            {
                SetMessage(UserMessageConstants.ActionTitleLimit, true);
             
                return;
            }
            else
            {
                ResetMessage();
            }

        }

        #endregion

        #endregion

        #region"Populate Data"

        #region"Init lookups"

        public void Initlookups()
        {
            try
            {
                actionManager = new Am.Ahv.BusinessManager.statusmgmt.Action();

                // For populating recommended Follow up period Dropdown list....            
                ddlRecommendedFollowUpPeriodFrequency.DataSource = actionManager.getFollowupPeriod();
                ddlRecommendedFollowUpPeriodFrequency.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                ddlRecommendedFollowUpPeriodFrequency.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;

                ddlRecommendedFollowUpPeriodFrequency.DataBind();
                ddlRecommendedFollowUpPeriodFrequency.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                ddlRecommendedFollowUpPeriodFrequency.SelectedValue = ApplicationConstants.defaulvalue;

                // For populating  trigger for next action.            

                ddlTriggerForNextAction.DataSource = actionManager.getFollowupPeriod();
                ddlTriggerForNextAction.DataTextField = Am.Ahv.Utilities.constants.ApplicationConstants.codename;
                ddlTriggerForNextAction.DataValueField = Am.Ahv.Utilities.constants.ApplicationConstants.codeId;
                ddlTriggerForNextAction.DataBind();
                ddlTriggerForNextAction.Items.Add(new ListItem("Please Select", ApplicationConstants.defaulvalue));
                ddlTriggerForNextAction.SelectedValue = ApplicationConstants.defaulvalue;

                // RegenrateRankingDdl();
                // for binding Ranking drop downlist
                int statusID = Convert.ToInt32(Session[SessionConstants.StatusID]);
                ViewState[ViewStateConstants.ActionstatusId] = statusID;
                List<int> RankList = actionManager.GetActionCountByStatusID(statusID);
                if (RankList != null)
                {

                    ddlRanking.DataSource = RankList;
                    ddlRanking.DataBind();
                    ddlRanking.SelectedValue = RankList.Last().ToString();
                }
                else
                {

                }
            }
            catch (NullReferenceException e)
            {
                IsException = true;
                ExceptionPolicy.HandleException(e, "Exception Policy");
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionloadingLookupsAction, true);
                }
            }
        }


        #endregion

        #region"Set Status"

        public void SetStatus(int status)
        {
            try
            {
                actionManager = new Am.Ahv.BusinessManager.statusmgmt.Action();
                lblStatus.Text = actionManager.getStatus(status);
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSettingStatusAction, true);
                }
            }
        }

        #endregion

        #region"Load Action"

        public void LoadAction()
        {
            try
            {
                if (!Convert.ToString(Session[SessionConstants.ActionId]).Equals(string.Empty))
                {
                    //initlookups();
                    actionManager = new Am.Ahv.BusinessManager.statusmgmt.Action();
                    AM_Action action = actionManager.getAction(Convert.ToInt32(Session[SessionConstants.ActionId]));
                    lblStatus.Text = actionManager.getStatus(action.StatusId);
                    txtActionTitle.Text = action.Title;
                    ddlRanking.SelectedValue = Convert.ToString(action.Ranking);
                    ViewState["Rank"] = action.Ranking;
                    txtRecommendedFollowUpPeriod.Text = Convert.ToString(action.RecommendedFollowupPeriod);
                    ddlRecommendedFollowUpPeriodFrequency.SelectedValue = Convert.ToString(action.AM_LookupCode1.LookupCodeId);
                    txtTriggerForNextAction.Text = Convert.ToString(action.NextActionAlert);
                    ddlTriggerForNextAction.SelectedValue = Convert.ToString(action.AM_LookupCode.LookupCodeId);
                    txtNextActionDetails.Text = action.NextActionDetails;
                    chkboxProceduralAction.Checked = action.IsProceduralAction;
                    if (action.IsPaymentPlanMandotry != null)
                    {
                        checkboxPaymentPlan.Checked = action.IsPaymentPlanMandotry.Value;
                    }
                    else
                    {
                        checkboxPaymentPlan.Checked = false;
                    }
                    action.AM_ActionAndStandardLetters.Load();
                    List<AM_ActionAndStandardLetters> Standardletters = action.AM_ActionAndStandardLetters.ToList();
                    List<AM_StandardLetters> slist = LoadStandardLetters(Standardletters);
                    lboxstandardletters.DataSource = slist;
                    lboxstandardletters.DataTextField = "Title";
                    lboxstandardletters.DataValueField = "StandardLetterId";
                    lboxstandardletters.DataBind();
                    if (isActionAttachedwithCase(Convert.ToInt32(Session[SessionConstants.ActionId])))
                    {
                        btnDelete.Enabled = false;
                    }
                    else
                    {
                        btnDelete.Enabled = true;
                        btnDelete.CommandArgument = Session[SessionConstants.ActionId].ToString();
                    }

                }
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionLoadingAction, true);
                }
            }
        }

        private List<AM_StandardLetters> LoadStandardLetters(List<AM_ActionAndStandardLetters> Standardletters)
        {
            List<AM_StandardLetters> Slist = new List<AM_StandardLetters>();
            foreach (AM_ActionAndStandardLetters item in Standardletters)
            {
                // Changed by Zunair Minhas on 23 June 2010
                if (item.IsActive)
                {
                    item.AM_StandardLettersReference.Load();
                    Slist.Add(item.AM_StandardLetters);
                }

                /******** Previous Code
                    item.AM_StandardLettersReference.Load();
                    Slist.Add(item.AM_StandardLetters);

                 */

            }

            return Slist;
        }

        #endregion

        #endregion

        #region"Functions"

        #region"Constructor"
        // Addition by Zunair Minhas on 23 June 2010
        public Action()
        {
            arraListNewStandardletters = new ArrayList();
        }

        #endregion

        #region"Validate"

        public void Validate(string str)
        {
            Validation validation = new Validation();

            if (validation.emptyString(str))
            {
                IsError = true;
                return;
            }
            else if (validation.lengthString(str))
            {
                IsError = true;
                SetMessage(UserMessageConstants.stringLengthMessage, true);
                return;
            }
            else
            {
                IsError = false;
            }
        }

        #endregion

        #region"Save Data"

        public void SaveData()
        {
            try
            {

                AM_Action action = new AM_Action();
                AM_ActionHistory actionhistory = new AM_ActionHistory();
                Validate(txtActionTitle.Text);
                if (Validation.CheckStringLength(txtActionTitle.Text))
                {
                    SetMessage(UserMessageConstants.ActionTitleLimit, true);
                    return;
                }
                if (!IsError)
                {
                    action.Title = txtActionTitle.Text;
                    actionhistory.Title = txtActionTitle.Text;
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return;
                }
                Validate(txtRecommendedFollowUpPeriod.Text);
                if (!IsError)
                {
                    action.RecommendedFollowupPeriod = Convert.ToInt32(txtRecommendedFollowUpPeriod.Text);
                    actionhistory.RecommendedFollowupPeriod = Convert.ToInt32(txtRecommendedFollowUpPeriod.Text);
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                    return;
                }
                if (ddlRecommendedFollowUpPeriodFrequency.SelectedValue == "-1")
                {
                    lblErrorFollowupDDL.Visible = true;
                    return;
                }
                else
                {

                    action.RecommendedFollowupPeriodFrequencyLookup = int.Parse(ddlRecommendedFollowUpPeriodFrequency.SelectedValue);
                    actionhistory.RecommendedFollowupPeriodFrequencyLookup = Convert.ToInt32(ddlRecommendedFollowUpPeriodFrequency.SelectedValue);
                }
                Validate(txtTriggerForNextAction.Text);
                if (!IsError)
                {
                    action.NextActionAlert = Convert.ToInt32(txtTriggerForNextAction.Text);
                    actionhistory.NextActionAlert = Convert.ToInt32(txtTriggerForNextAction.Text);
                }
                else
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return;
                }
                if (ddlTriggerForNextAction.SelectedValue == "-1")
                {
                    SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                    return;
                }
                else
                {

                    action.NextActionAlertFrequencyLookup = int.Parse(ddlTriggerForNextAction.SelectedValue);
                    actionhistory.NextActionAlertFrequencyLookup = Convert.ToInt32(ddlTriggerForNextAction.SelectedValue);

                }
                if (txtNextActionDetails.Text.Equals(string.Empty))
                {
                    SetMessage(UserMessageConstants.requiredFieldMessage, true);
                    return;
                }
                else
                {
                    action.NextActionDetails = txtNextActionDetails.Text;
                    actionhistory.NextActionDetails = txtNextActionDetails.Text;
                }


                action.IsProceduralAction = chkboxProceduralAction.Checked;
                actionhistory.IsProceduralAction = chkboxProceduralAction.Checked;

                action.IsPaymentPlanMandotry = checkboxPaymentPlan.Checked;
                actionhistory.IsPaymentPlanMandotry = checkboxPaymentPlan.Checked;

                action.ModifiedDate = System.DateTime.Now;
                actionhistory.ModifiedDate = System.DateTime.Now;

                if (!Convert.ToString(Session[SessionConstants.StatusID]).Equals(string.Empty))
                {
                    action.StatusId = Convert.ToInt32(Session[SessionConstants.StatusID]);
                    actionhistory.StatusId = Convert.ToInt32(Session[SessionConstants.StatusID]);

                }
                else
                {
                    SetMessage(UserMessageConstants.errorSavingAction, true);
                    return;
                }

                actionManager = new Am.Ahv.BusinessManager.statusmgmt.Action();
                if (!Convert.ToString(Session[SessionConstants.ActionId]).Equals(string.Empty) && Session[SessionConstants.ActionId] != null)
                {
                    action.ModifiedBy = base.GetCurrentLoggedinUser();
                    actionhistory.ModifiedBy = base.GetCurrentLoggedinUser();

                    action.ModifiedDate = DateTime.Now;
                    actionhistory.ModifiedDate = DateTime.Now;
                    int actionid = Convert.ToInt32(Session[SessionConstants.ActionId]);
                    bool flagaction = CompareRanks();

                    if (flagaction)
                    {
                        bool flagvalue = GetActionByRank(action.StatusId);
                        if (flagvalue)
                        {
                            action.Ranking = int.Parse(ddlRanking.SelectedValue);
                            actionhistory.Ranking = int.Parse(ddlRanking.SelectedValue);
                            int Rank = GetRank();
                            if (Rank != -1)
                            {
                              //  if (isUpdatedFollowUpPeriodValid(Convert.ToInt32(Session[SessionConstants.ActionId]), action.RecommendedFollowupPeriod, ddlRecommendedFollowUpPeriodFrequency.SelectedItem.Text))
                                {
                                    bool updateflag = actionManager.updateAction(Convert.ToInt32(Session[SessionConstants.ActionId]), action, actionhistory, GetLetterList(), Rank);
                                    if (updateflag)
                                    {
                                        ResetPage();
                                        SetMessage(UserMessageConstants.updateSuccess, false);
                                        Session.Remove(SessionConstants.ActionId);
                                        //  Session.Remove(SessionConstants.StatusID);
                                    }
                                    else
                                    {
                                        SetMessage(UserMessageConstants.assignedActionUpdateError, true);
                                        return;
                                    }
                                }
                                //else
                                //{
                                //    SetMessage("Follow up Period must not be greater than its follower", true);
                                //}
                            }
                            else
                            {
                                SetMessage(UserMessageConstants.AAInvalidUpdate, true);
                                return;
                            }
                        }
                        else
                        {
                            SetMessage(UserMessageConstants.AAInvalidaction, true);
                            return;
                        }
                    }
                    else
                    {
                        action.Ranking = int.Parse(ddlRanking.SelectedValue);
                        actionhistory.Ranking = int.Parse(ddlRanking.SelectedValue);
                      //  if (isUpdatedFollowUpPeriodValid(Convert.ToInt32(Session[SessionConstants.ActionId]),action.RecommendedFollowupPeriod, ddlRecommendedFollowUpPeriodFrequency.SelectedItem.Text))
                        {
                            bool updateflag = actionManager.updateAction(Convert.ToInt32(Session[SessionConstants.ActionId]), action, actionhistory, GetLetterList());
                            if (updateflag)
                            {
                                ResetPage();
                                SetMessage(UserMessageConstants.updateSuccess, false);
                                Session.Remove(SessionConstants.ActionId);
                                //  Session.Remove(SessionConstants.StatusID);
                            }
                            else
                            {
                                SetMessage(UserMessageConstants.AAInvalidUpdate, true);
                                return;
                            }
                        }
                        //else
                        //{
                        //    SetMessage("Follow up Period must not be greater than its follower", true);
                        //}



                        //SetMessage(UserMessageConstants.AAInvalidRank, true);
                        //return;
                    }
                }
                else
                {
                    action.CreatedBy = base.GetCurrentLoggedinUser();
                    actionhistory.CreatedBy = base.GetCurrentLoggedinUser();
                    action.ModifiedBy = base.GetCurrentLoggedinUser();
                    actionhistory.ModifiedBy = base.GetCurrentLoggedinUser();
                    action.CreatedDate = System.DateTime.Now;
                    actionhistory.CreatedDate = System.DateTime.Now;
                    if (ddlRanking.SelectedValue == "-1")
                    {
                        SetMessage(UserMessageConstants.requiredDropDownMessage, true);
                        return;
                    }
                    else
                    {
                        actionManager = new BusinessManager.statusmgmt.Action();
                        int statusId = Convert.ToInt32(Session[SessionConstants.StatusID]);
                        int Rank = int.Parse(ddlRanking.SelectedValue);
                        if (!actionManager.IsActionPresent(statusId, Rank))
                        {
                            action.Ranking = Convert.ToInt32(ddlRanking.SelectedValue);
                            actionhistory.Ranking = Convert.ToInt32(ddlRanking.SelectedValue);
                        }
                        else
                        {
                            SetMessage(UserMessageConstants.MultipleActionRankMessage, true);
                            return;
                        }
                    }
                    //List<int> LetterList = GetLetterList();

                    //if (isFollowUpPeriodConsecutive(action.StatusId,action.RecommendedFollowupPeriod, ddlRecommendedFollowUpPeriodFrequency.SelectedItem.Text,action.Ranking))
                    //{
                        bool flag = actionManager.addAction(action, actionhistory, GetLetterList());
                        if (flag)
                        {
                            ResetPage();
                            SetMessage(UserMessageConstants.InsertSuccess, false);
                        }
                        else
                        {
                            isError = true;
                        }
                    //}
                    //else
                    //{
                    //    SetMessage("Follow up Period must be greater than the previous ones", true);
                    //}
                }
            }
            catch (NullReferenceException nullException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(nullException, "Exception Policy");
            }
            catch (EntityException entityexception)
            {
                IsException = true;
                ExceptionPolicy.HandleException(entityexception, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSavingAction, true);
                }
            }
        }

        private bool GetActionByRank(int p)
        {
            actionManager = new BusinessManager.statusmgmt.Action();
            int rank = int.Parse(ddlRanking.SelectedValue);
            AM_Action dummyaction = actionManager.GetActionByRank(p, rank);
            if (dummyaction != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #region CompareRanks
        private bool CompareRanks()
        {
            int Rank1 = int.Parse(ddlRanking.SelectedValue);
            int Rank2 = GetRank();
            if (Rank2 != -1 && ddlRanking.SelectedValue != "-1")
            {
                if (Rank1 != Rank2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Getting Rank from Viewstate
        private int GetRank()
        {
            if (ViewState["Rank"] != null)
            {
                return Convert.ToInt32(ViewState["Rank"]);
            }
            else
            {
                return -1;
            }
        }
        #endregion

        #endregion

        #region"Get Letter List"

        private List<int> GetLetterList()
        {
            List<int> Letterslist = new List<int>();
            foreach (ListItem item in lboxstandardletters.Items)
            {
                //---- Added by Zunair Minhas...
                if (CheckNewStandardLetterId(item.Value))
                {
                    string[] value = item.Value.Split(';');
                    Letterslist.Add(int.Parse(value[0]));
                }

                /****** Commited By Zunair Minhas on 23 June 2010
                    //Letterslist.Add(int.Parse(item.Value));
                 */

            }
            return Letterslist;
        }

        #endregion

        #region"Reset Page"

        public void ResetPage()
        {
            txtActionTitle.Text = string.Empty;

            txtRecommendedFollowUpPeriod.Text = string.Empty;
            ddlRecommendedFollowUpPeriodFrequency.SelectedValue = "-1";
            txtTriggerForNextAction.Text = string.Empty;
            ddlTriggerForNextAction.SelectedValue = "-1";
            txtNextActionDetails.Text = string.Empty;
            chkboxProceduralAction.Checked = false;
            checkboxPaymentPlan.Checked = false;
            ClearRanking();
            lboxstandardletters.Items.Clear();
            RegenrateRankingDdl();
        }

        private void RegenrateRankingDdl()
        {
            try
            {
                int StatusID = (int)ViewState[ViewStateConstants.ActionstatusId];
                actionManager = new BusinessManager.statusmgmt.Action();
                List<int> RankList = actionManager.GetActionCountByStatusID(StatusID);
                if (RankList != null)
                {

                    ddlRanking.DataSource = RankList;
                    ddlRanking.DataBind();
                    ddlRanking.SelectedValue = RankList.Last().ToString();

                }
                else
                {
                    ddlRanking.Items.Add(new ListItem("1", "1"));
                    ddlRanking.SelectedValue = "1";
                }
            }
            catch (NullReferenceException e)
            {
                IsException = true;
                ExceptionPolicy.HandleException(e, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionRegeneratingRanking, true);
                }
            }
        }

        #endregion

        #region Clear Ranking DropDown
        private void ClearRanking()
        {
            ddlRanking.Items.Clear();
        }
        #endregion

        private bool isActionAttachedwithCase(int actionId)
        {
            try
            {
                Am.Ahv.BusinessManager.statusmgmt.Action action = new BusinessManager.statusmgmt.Action();
                return action.isActionAttachedwithCase(actionId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region checks for the empty strings
        
        private bool ValidateFields()
        {
            if (txtActionTitle.Text != string.Empty && txtRecommendedFollowUpPeriod.Text != string.Empty && txtTriggerForNextAction.Text != string.Empty && txtNextActionDetails.Text != string.Empty)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Get Cached Action"

        /// <summary>
        /// Inserts Form fields inside the cached object to insert inside the cache.
        /// </summary>
        /// <returns></returns>
        private AM_Action GetCachedAction()
        {

            try
            {
                AM_Action CachedAction = new AM_Action();
                CachedAction.StatusId = base.GetStatusId();
                CachedAction.Title = txtActionTitle.Text;
                CachedAction.Ranking = int.Parse(ddlRanking.SelectedValue);
                CachedAction.RecommendedFollowupPeriod = int.Parse(txtRecommendedFollowUpPeriod.Text);
                CachedAction.RecommendedFollowupPeriodFrequencyLookup = int.Parse(ddlRecommendedFollowUpPeriodFrequency.SelectedValue);
                CachedAction.NextActionAlert = int.Parse(txtTriggerForNextAction.Text);
                CachedAction.NextActionAlertFrequencyLookup = int.Parse(ddlTriggerForNextAction.SelectedValue);
                CachedAction.NextActionDetails = txtNextActionDetails.Text;
                CachedAction.IsProceduralAction = chkboxProceduralAction.Checked;
                CachedAction.IsPaymentPlanMandotry = checkboxPaymentPlan.Checked;
                if (Session[SessionConstants.ActionId] != null)
                {
                    CachedAction.ActionId = int.Parse(Session[SessionConstants.ActionId].ToString());
                }
                // Changed by Zunair Minhas
                DataTable listcollection = LoadLetterList(lboxstandardletters.Items);

                /* Commented by Zunair Minhas on 23 June 2010
                 *  List<AM_StandardLetters> listcollection = LoadLetterList(lboxstandardletters.Items);
                 */
                Session["LettersCollection"] = listcollection;
                //Cache.Insert("LettersCollection", listcollection);
                return CachedAction;
            }
            catch (NullReferenceException nullref)
            {
                ExceptionPolicy.HandleException(nullref, "Exception Policy");
                return null;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return null;
            }
        }

        #endregion

        #region"Load Letter List"

        private DataTable LoadLetterList(ListItemCollection listItemCollection)
        {
            // Added By Zunair Minhas
            DataTable dt = new DataTable();
            dt.Columns.Add("Title");
            dt.Columns.Add("StandardLetterId");

            foreach (ListItem item in listItemCollection)
            {
                DataRow dr = dt.NewRow();

                dr["Title"] = item.Text;
                dr["StandardLetterId"] = item.Value;

                dt.Rows.Add(dr);
            }
            return dt;

            /*
             * Commented by Zunair Minhas on 23 June 2010
             *   List<AM_StandardLetters> Slist = new List<AM_StandardLetters>();
            AM_StandardLetters sletter = null;
            foreach (ListItem item in listItemCollection)
            {
                sletter = new AM_StandardLetters();
                sletter.StandardLetterId = int.Parse(item.Value);
                sletter.Title = item.Text;
                Slist.Add(sletter);
            }
            return Slist;
             
             */
        }

        #endregion

        #region"Check New Standard Letter Id"
        // Added By Zunair Minhas on 23 June 2010
        public bool CheckNewStandardLetterId(string value)
        {
            if (value.Contains(";NewSL_"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region"Is Cached Action"

        /// <summary>
        /// For checking if the action is available inside the session
        /// </summary>
        /// <returns>True/False</returns>
        public bool IsCachedAction()
        {

            if (Session["CachedAction"] != null)
            {
                return true;
            }

            else if (Convert.ToString(Session["CachedAction"]).Equals(string.Empty))
            {
                return false;
            }
            else
            {
                return false;
            }

        }

        #endregion

        #region"Load Cached Action"
        /// <summary>
        /// Filling the form fields with cached Data.
        /// </summary>
        public void LoadCachedAction(int LetterId)
        {
            try
            {
                // Added By Zunair Minhas
                LetterResourceArea lresource = new LetterResourceArea();
                ViewState["LetterId"] = LetterId;
                AM_StandardLetters Standardletters = new AM_StandardLetters();
                DataTable dt = new DataTable();
                dt.Columns.Add("Title");
                dt.Columns.Add("StandardLetterId");
                dt = Session["LettersCollection"] as DataTable;
                //dt = Cache.Get("LettersCollection") as DataTable;

                if (LetterId != 0)
                {
                    Standardletters = lresource.GetStandardLetter(LetterId);
                    //listcollection.Add(Standardletters);
                    lboxstandardletters.DataSource = dt;

                    if (Standardletters != null)
                    {
                        lboxstandardletters.DataTextField = "Title";
                        lboxstandardletters.DataValueField = "StandardLetterId";
                        lboxstandardletters.DataBind();
                    }
                    lboxstandardletters.Items.Add(new ListItem(Standardletters.Title, Standardletters.StandardLetterId + ";NewSL_"));
                }
                else
                {
                    lboxstandardletters.DataSource = dt;

                    if (Standardletters != null)
                    {
                        lboxstandardletters.DataTextField = "Title";
                        lboxstandardletters.DataValueField = "StandardLetterId";
                        lboxstandardletters.DataBind();
                    }
                }
                // for reteriving the CachedAction from the session
                AM_Action CachedAction = Session["CachedAction"] as AM_Action;
                if (CachedAction != null)
                {
                    if (!string.IsNullOrEmpty(CachedAction.Title))
                    {
                        txtActionTitle.Text = CachedAction.Title;
                    }
                    if (!string.IsNullOrEmpty(CachedAction.Ranking.ToString()))
                    {
                        ddlRanking.SelectedValue = CachedAction.Ranking.ToString();
                    }
                    if (!string.IsNullOrEmpty(CachedAction.RecommendedFollowupPeriod.ToString()))
                    {
                        txtRecommendedFollowUpPeriod.Text = CachedAction.RecommendedFollowupPeriod.ToString();
                    }
                    ddlRecommendedFollowUpPeriodFrequency.SelectedValue = CachedAction.RecommendedFollowupPeriodFrequencyLookup.ToString();
                    if (!string.IsNullOrEmpty(CachedAction.NextActionAlert.ToString()))
                    {
                        txtTriggerForNextAction.Text = CachedAction.NextActionAlert.ToString();
                    }
                    ddlTriggerForNextAction.SelectedValue = CachedAction.NextActionAlertFrequencyLookup.ToString();
                    txtNextActionDetails.Text = CachedAction.NextActionDetails;
                    chkboxProceduralAction.Checked = CachedAction.IsProceduralAction;
                    checkboxPaymentPlan.Checked = CachedAction.IsPaymentPlanMandotry.Value;
                    lblStatus.Text = Session["lblStatus"].ToString();
                }
                else
                {
                    SetMessage(UserMessageConstants.cachecleared, true);
                }

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }

        }
        #endregion

        #region"isFollowUpPeriodConsecutive"
        private bool isFollowUpPeriodConsecutive(int statusId,int periodVal, string period,int ranking)
        {
            try
            {
                Am.Ahv.BusinessManager.statusmgmt.Action action = new BusinessManager.statusmgmt.Action();
                return action.isFollowUpPeriodConsecutive(statusId,periodVal, period,ranking);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        private bool isUpdatedFollowUpPeriodValid(int actionId, int periodVal, string period)
        {
            try
            {
                Am.Ahv.BusinessManager.statusmgmt.Action action = new BusinessManager.statusmgmt.Action();
                return action.isUpdatedFollowUpPeriodValid(actionId,periodVal, period);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


}



