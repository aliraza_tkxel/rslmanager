﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FirstDetectionList.ascx.cs"
    Inherits="Am.Ahv.Website.controls.status_and_actions.FirstDetectionList" %>
<asp:Panel ID="firstlistPanel" runat="server" Width="500px">
    <asp:UpdatePanel ID="firstlistupanel" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblStatus" runat="server" Text="Initial Case Monitoring:"></asp:Label>
                       <!-- Stage Title-->
                    </td>
                    <td>
                        <asp:Label ID="lblrequiredStatus" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                        <asp:TextBox Width="110px" EnableViewState="true" ID="statustxt" ReadOnly="true"
                            Enabled="false" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblParameter" runat="server" Text="Parameters:"></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblRent" runat="server" Text="Rent >"></asp:Label>&nbsp;
                        <asp:DropDownList ID="frequencyddl" runat="server" EnableViewState="true" Width="105px">
                        </asp:DropDownList>
                        &nbsp;
                        <asp:DropDownList ID="periodddl" runat="server" Width="105px" EnableViewState="true">
                        </asp:DropDownList>
                        &nbsp;
                        <%--<asp:CheckBox ID="param1chk" runat="server" Checked="false" EnableViewState="true" />--%>
                        <asp:RadioButton ID="param1chk" runat="server" Checked="false" EnableViewState="true"
                            GroupName="selectionGroup" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblOR" runat="server" Text="OR"></asp:Label>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblRentAmount" runat="server" Text="Rent > £" EnableViewState="true"></asp:Label>&nbsp;&nbsp;
                        <asp:TextBox ID="rentamounttxt" runat="server" Width="100px" EnableViewState="true"></asp:TextBox>&nbsp;
                        <asp:RadioButton ID="param2chk" runat="server" Checked="false" EnableViewState="true"
                            GroupName="selectionGroup" /><br />
                        <%--<asp:CheckBox ID="param2chk" runat="server" Checked="false" EnableViewState="true" />--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorrentamounttxt" runat="server"
                            ErrorMessage="Only float value" ControlToValidate="rentamounttxt" ValidationExpression="^[0-9.]+$"
                            Height="19px" Width="165px"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Ranking:"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblRequiredranking" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                        <asp:DropDownList ID="rankingddl" runat="server" Width="60px" EnableViewState="true">
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Review Period:
                    </td>
                    <td>
                        <asp:Label ID="lblRequiredReview" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                        <asp:TextBox ID="reviewperiodtxt" runat="server" EnableViewState="true" Width="50px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="reviewddl" runat="server" Width="105px" EnableViewState="true">
                        </asp:DropDownList>
                        <i>after creation date</i>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorreviewperiodtxt" runat="server"
                            ErrorMessage="Only Digits" ControlToValidate="reviewperiodtxt" ValidationExpression="^[0-9]+$"
                            Height="19px" Width="165px"></asp:RegularExpressionValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Please enter less than 100"
                            ControlToValidate="reviewperiodtxt" MinimumValue="1" MaximumValue="99" Type="Integer"
                            Display="Dynamic"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Trigger for next stage alert:
                    </td>
                    <td>
                        <asp:Label ID="lblRequiredtrigger" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                        <asp:TextBox ID="triggertxt" runat="server" Width="50px" EnableViewState="true"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="triggerddl" runat="server" Width="105px" EnableViewState="true">
                        </asp:DropDownList>
                        <i>after creation date</i>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatortriggertxt" runat="server"
                            ErrorMessage="Only Digits" ControlToValidate="triggertxt" ValidationExpression="^[0-9]+$"
                            Height="19px" Width="165px"></asp:RegularExpressionValidator>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="Please enter less than 100"
                            ControlToValidate="triggertxt" MinimumValue="1" MaximumValue="99" Type="Integer"
                            Display="Dynamic"></asp:RangeValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Next status Details:
                    </td>
                    <td>
                        <asp:Label ID="lblRequiredDetail" runat="server" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label>&nbsp;
                        <asp:TextBox ID="statusdetailstxt" runat="server" TextMode="MultiLine" Width="200px"
                            EnableViewState="true" Height="60px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblnoticeparameter" runat="server" Text="Notice Parameter required"></asp:Label>
                    </td>
                    <td style="padding-left:10px;">
                        &nbsp;<asp:CheckBox ID="noticeparachk" runat="server" Checked="false" EnableViewState="true"
                            AutoPostBack="True" OnCheckedChanged="noticeparachk_CheckedChanged" />
                    </td>
                </tr>
                <tr id="RecoveryAmount" runat="server" visible="false">
                    <td>
                        <asp:Label ID="lblrecoveryamount" runat="server" Text="Rent Balance:"></asp:Label>
                    </td>
                    <td style="padding-left:10px;">
                        &nbsp;<asp:CheckBox ID="recoverychk" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr id="noticeissue" runat="server" visible="false">
                    <td>
                        <asp:Label ID="lblnoticeissuedate" runat="server" Text="Notice Issue Date:"></asp:Label>
                    </td>
                    <td style="padding-left:10px;">
                        &nbsp;<asp:CheckBox ID="issuedatechk" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr id="NoticeExpiryDate" runat="server" visible="false">
                    <td>
                        <asp:Label ID="lblnoticeexpriry" runat="server" Text="Notice Expiry Date:"></asp:Label>
                    </td>
                    <td style="padding-left:10px;">
                        &nbsp;<asp:CheckBox ID="expirydatechk" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr id="HearingDate" runat="server" visible="false">
                    <td>
                        <asp:Label ID="lblHearingDate" runat="server" Text="Hearing Date:"></asp:Label>
                    </td>
                    <td style="padding-left:10px;">
                        &nbsp;<asp:CheckBox ID="HearingDateCheck" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr id="WarrantExpiryDate" runat="server" visible="false">
                    <td>
                        <asp:Label ID="lblWarrantExpiryDate" runat="server" Text="Warrant Expiry Date:"></asp:Label>
                    </td>
                    <td style="padding-left:10px;">
                        &nbsp;<asp:CheckBox ID="WarrantExpiryDateCheck" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Document Upload:
                    </td>
                    <td style="padding-left:10px;">
                        &nbsp;<asp:CheckBox ID="docuploadchk" runat="server" Checked="false" EnableViewState="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td align="right" style="padding-right: 20px">
                        <asp:Button ID="cancelbtn" Width="60px" runat="server" Text="Cancel" OnClick="cancelbtn_Click" />
                        &nbsp;
                        <asp:Button ID="savebtn" runat="server" Text="Save Changes" Width="100px" OnClick="savebtn_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
