﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.letters;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace Am.Ahv.Website.controls.resources
{
    public partial class Letters :UserControlBase
    {
        #region"Attributes"
        bool isError;
        bool isException;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion

        #region"Events"

        #region"Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadLetterGrid();
            }
        }
        #endregion

        #region"btnEdit_Click"
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
                int index = gvr.RowIndex;
                Button btnEdit = new Button();
                btnEdit = (Button)LetterGrid.Rows[index].FindControl("btnEdit");
                Response.Redirect("CreateLetter.aspx?cmd=edit&Id=" + btnEdit.CommandArgument.ToString());
            }
            catch (Exception ex)
            {
                setMessage(UserMessageConstants.EditingError, true);
                ExceptionPolicy.HandleException(ex, "Exception Policy");

            }
        }
        #endregion

        #region"btn Add More Letters_Click"
        protected void btnAddMoreLetters_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(PathConstants.lettersResourceAreaPathCached);
            }
            catch (Exception ex)
            {
                setMessage(UserMessageConstants.MoreTemplateError, true);
               ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion

        #endregion

        #region"Functions"

        #region"Load Letter Grid"
        private void LoadLetterGrid()
        {
            try
            {
                List<AM_StandardLetters> letterlist = new List<AM_StandardLetters>();
                LetterResourceArea LSA = new LetterResourceArea();
                letterlist = LSA.GetAllLetters();
                LetterGrid.DataSource = letterlist;
                LetterGrid.DataBind();
            }
            catch (Exception ex)
            {
                setMessage(UserMessageConstants.LoadingLettersError, true);
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
        }
        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #endregion

    }
  
}