﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using System.Drawing;
using Am.Ahv.Website.pagebase;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.BusinessManager.lookup;
namespace Am.Ahv.Website.controls.outcome
{
    #region"Delegate"

    public delegate void sendEventToParentPageOutcome(bool EventInvoked, bool edit);

    #endregion

    public partial class Outcome : UserControlBase
    {

        #region "Attributes"
        int currentPage;

        public int CurrentPage
        {
            get { return currentPage; }
            set { currentPage = value; }
        }
        int dbIndex;

        public int DbIndex
        {
            get { return dbIndex; }
            set { dbIndex = value; }
        }
        int totalPages;

        public int TotalPages
        {
            get { return totalPages; }
            set { totalPages = value; }
        }
        int totalRecords;

        public int TotalRecords
        {
            get { return totalRecords; }
            set { totalRecords = value; }
        }

        bool newRecordFlag;

        public bool NewRecordFlag
        {
            get { return newRecordFlag; }
            set { newRecordFlag = value; }
        }

        bool isException;

        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }

        #endregion

        #region "Setters"
        private void SetDefaultPagingAttributes()
        {
            CurrentPage = 1;
            DbIndex = 0;
            TotalPages = 0;
            TotalRecords = 0;
            SetHiddenFields();
        }
        public void SetHiddenFields()
        {
            hflCurrentPage.Value = CurrentPage.ToString();
            hflDBIndex.Value = DbIndex.ToString();
            hflTotalPages.Value = TotalPages.ToString();
            hflTotalRecords.Value = TotalRecords.ToString();
        }
        public void SetPagingLabels()
        {
            Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
            int count = lookup.getLookupCount("Outcome");

            if (count > ApplicationConstants.pagingPageSizeOutcomes)
            {
                SetCurrentRecordsDisplayed();
                SetTotalPages(count);
                lblOutcomeOnPage.Text = (TotalRecords).ToString();
                lblOutcomeTotal.Text = count.ToString();
                lblPageCurrent.Text = CurrentPage.ToString();
                lblPageTotal.Text = TotalPages.ToString();
            }
            else
            {
                TotalPages = 1;
                lblOutcomeOnPage.Text = pgvOutcomes.Rows.Count.ToString();
                lblOutcomeTotal.Text = count.ToString();
                lblPageCurrent.Text = CurrentPage.ToString();
                lblPageTotal.Text = CurrentPage.ToString();
                lBtnNext.Enabled = false;
                lBtnPrevious.Enabled = false;
            }
            if (CurrentPage == 1)
            {
                lBtnPrevious.Enabled = false;
            }
            if ((count % ApplicationConstants.pagingPageSizeOutcomes > 0) && CurrentPage != TotalPages)
            {
                lBtnNext.Enabled = true;
                //    lBtnPrevious.Enabled = true;
            }
            SetHiddenFields();
        }
        public void SetTotalPages(int count)
        {
            try
            {
                if (count % ApplicationConstants.pagingPageSizeOutcomes > 0)
                {
                    TotalPages = (count / ApplicationConstants.pagingPageSizeOutcomes) + 1;
                }
                else
                {
                    TotalPages = (count / ApplicationConstants.pagingPageSizeOutcomes);
                }
            }
            catch (DivideByZeroException divide)
            {
                IsException = true;
                ExceptionPolicy.HandleException(divide, "Exception Policy");
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSettingTotalPagesOutcomes, true);
                }
            }
        }
        public void SetCurrentRecordsDisplayed()
        {
            try
            {
                if (pgvOutcomes.Rows.Count == ApplicationConstants.pagingPageSizeOutcomes)
                {
                    TotalRecords = CurrentPage * pgvOutcomes.Rows.Count;
                }
                else if (NewRecordFlag == false)
                {
                    TotalRecords += pgvOutcomes.Rows.Count;
                }
                else
                {
                    TotalRecords += 1;
                }
            }
            catch (ArithmeticException arithmeticException)
            {
                IsException = true;
                ExceptionPolicy.HandleException(arithmeticException, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionSettingCurrentRecordsOutcomes, true);
                }
            }
        }
        #endregion

        #region"Events"

        public event sendEventToParentPageOutcome invokeEvent;

        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
            if (!IsPostBack)
            {
                SetDefaultPagingAttributes();
                PopulateGridView();
                SetPagingLabels();
            }
            NewRecordFlag = false;
        }
        public void lBtnPrevious_Click(object sender, EventArgs e)
        {
            GetHiddenValues();
            ReducePaging();
        }
        public void lBtnNext_Click(object sender, EventArgs e)
        {
            GetHiddenValues();
            IncreasePaging();
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;
            Button btnEdit = new Button();
            btnEdit = (Button)pgvOutcomes.Rows[index].FindControl("btnEdit");
            Session[SessionConstants.EditOutcomeId] = btnEdit.CommandArgument;
            invokeEvent(true, true);
        }

        protected void btnNewOutcome_Click(object sender, EventArgs e)
        {
            invokeEvent(true, false);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            GridViewRow gvr = ((Button)sender).Parent.Parent as GridViewRow;
            int index = gvr.RowIndex;
            Button btnDelete = new Button();
            btnDelete = (Button)pgvOutcomes.Rows[index].FindControl("btnDelete");

            //Session[SessionConstants.EditOutcomeId] = btnDelete.CommandArgument;
            //invokeEvent(true, true);
            if (IsOutcomeFree(btnDelete.CommandArgument))
            {
                DeleteOutcome(btnDelete.CommandArgument);
                PopulateGridView();
                GetHiddenValues();
                SetPagingLabels();
            }
            else
            {
                SetMessage("Couldn't be deleted, it is used by Case.", true);
            }

        }




        #endregion

        #region"getters"
        public void GetHiddenValues()
        {
            CurrentPage = Convert.ToInt32(hflCurrentPage.Value);
            DbIndex = Convert.ToInt32(hflDBIndex.Value);
            TotalPages = Convert.ToInt32(hflTotalPages.Value);
            TotalRecords = Convert.ToInt32(hflTotalRecords.Value);
        }
        #endregion

        #region "Populate Data"
        public void PopulateGridView()
        {
            try
            {
                Am.Ahv.BusinessManager.lookup.Lookup lookup = new Am.Ahv.BusinessManager.lookup.Lookup();
                pgvOutcomes.DataSource = lookup.getLookupListings("Outcome", DbIndex, ApplicationConstants.pagingPageSizeOutcomes);
                pgvOutcomes.DataBind();
            }
            catch (NullReferenceException e)
            {
                IsException = true;
                ExceptionPolicy.HandleException(e, "Exception Policy");
            }
            catch (EntityException ee)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ee, "Exception Policy");
            }
            catch (Exception ex)
            {
                IsException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (IsException)
                {
                    SetMessage(UserMessageConstants.exceptionloadingOutcomes, true);
                }
            }
        }
        #endregion

        #region"Functions"
        public void ReloadControl()
        {
            NewRecordFlag = true;
            GetHiddenValues();
            PopulateGridView();
            SetPagingLabels();
        }

        #region"Reduce Paging"

        public void ReducePaging()
        {
            if (CurrentPage > 1)
            {
                CurrentPage -= 1;
                if (CurrentPage == 1)
                {
                    lBtnNext.Enabled = true;
                    lBtnPrevious.Enabled = false;
                }
                else
                {
                    lBtnNext.Enabled = true;
                    lBtnPrevious.Enabled = true;
                }
                DbIndex -= 1;
                PopulateGridView();
                SetPagingLabels();
            }
        }

        #endregion

        #region"Increase Paging"

        public void IncreasePaging()
        {
            if (CurrentPage < TotalPages)
            {
                CurrentPage += 1;
                if (CurrentPage == TotalPages)
                {
                    lBtnNext.Enabled = false;
                    lBtnPrevious.Enabled = true;
                }
                else
                {
                    lBtnNext.Enabled = true;
                    lBtnPrevious.Enabled = true;
                }
                DbIndex += 1;
                PopulateGridView();
                SetPagingLabels();
            }
        }

        #endregion

        #region"Set Message"

        public void SetMessage(string str, bool isError)
        {
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Reset Message"

        public void ResetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Is OutCome Free"
        private bool IsOutcomeFree(string outcomeId)
        {
            try
            {
                CaseDetails businessMgr = new CaseDetails();
                return businessMgr.IsOutcomeFree(Int32.Parse(outcomeId));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region"Delete OutCome"
        private bool DeleteOutcome(string outcomeId)
        {
            try
            {
                Lookup mgr = new BusinessManager.lookup.Lookup();
                return mgr.DeleteOutComeLookUp(Int32.Parse(outcomeId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion
    }
}