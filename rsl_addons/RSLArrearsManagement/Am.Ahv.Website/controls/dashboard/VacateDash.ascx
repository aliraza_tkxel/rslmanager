﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VacateDash.ascx.cs"
    Inherits="Am.Ahv.Website.controls.dashboard.vacatedash" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="upvacat" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Label ID="lblvacatecount" runat="server" CssClass="dashboardBigNumbers"></asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>
