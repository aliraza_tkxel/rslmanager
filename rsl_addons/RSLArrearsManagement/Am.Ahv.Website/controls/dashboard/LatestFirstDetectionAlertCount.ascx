﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LatestFirstDetectionAlertCount.ascx.cs" Inherits="Am.Ahv.Website.controls.dashboard.LatestFirstDetectionAlertCount" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="updateLatestFirstDetectionsAlertCount" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        
        <asp:Label ID="lblLatestFirstDetectionAlertCount" runat="server" CssClass="dashboardBigNumbersUP" ></asp:Label>
        
    </ContentTemplate>
</asp:UpdatePanel>