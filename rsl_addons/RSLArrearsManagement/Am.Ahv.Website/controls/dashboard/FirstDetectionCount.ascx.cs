﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Casemgmt;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.lookup;
using Am.Ahv.Website.pagebase;

namespace Am.Ahv.Website.controls.dashboard
{
    public partial class FirstDetectionCount : UserControlBase
    {

        #region"Attributes"
        bool isError;
        bool isException;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion

        #region"Events"

        #region"Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (GetUserType(base.GetCurrentLoggedinUser()) == ApplicationConstants.CaseOfficer)
                    {
                        showFirstDetectionCount(base.GetCurrentLoggedinUser(), 0, 0);                        
                    }
                    else
                    {
                        showFirstDetectionCount(-2, 0, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex,"Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.PageLoadError, true);
                }
            }
            
        }
        #endregion

        #endregion

        #region "Functions"
        #region "show First Detection Count"
        public void showFirstDetectionCount(int caseOfficerId, int regionId, int suburbId)
            {
                try
                {
                    FirstDetectionList firstDetectionList = new FirstDetectionList();
                    lblFirstDetectionCount.Text = firstDetectionList.CountFirstDetections(caseOfficerId,regionId,suburbId).ToString();
                    updateFirstDetectionsCount.Update();
                }
                catch (Exception ex)
                {
                    isException = true;
                    ExceptionPolicy.HandleException(ex,"Exception Policy");
                }
                finally
                {
                    if (isException)
                    {
                        setMessage(UserMessageConstants.CountingErrorFirstDetection, true);
                    }
                }
            }
        #endregion
        #endregion

        #region"Reset Message"

            public void resetMessage()
            {
                lblMessage.Text = string.Empty;
                pnlMessage.Visible = false;
            }

            #endregion

        #region"Set Message"

            public void setMessage(string str, bool isError)
            {
                this.isError = isError;
                if (isError == true)
                {
                    lblMessage.Text = str;
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Font.Bold = true;
                }
                else
                {
                    lblMessage.Text = str;
                    lblMessage.ForeColor = Color.Green;
                    lblMessage.Font.Bold = true;
                }
                pnlMessage.Visible = true;
            }

            #endregion


        #region"Get User Type"

            private string GetUserType(int userId)
            {
                Lookup lookupManager = new Lookup();
                return lookupManager.GetUserType(userId);

            }

        #endregion


    }
}