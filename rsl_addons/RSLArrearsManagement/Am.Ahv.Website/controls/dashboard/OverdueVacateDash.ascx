﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OverdueVacateDash.ascx.cs"
    Inherits="Am.Ahv.Website.controls.dashboard.overduevacatedash" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server" Style="color: White;"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="upoverduestatus" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Label ID="lbloverduecount" runat="server" CssClass="dashboardBigNumbers"></asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>
