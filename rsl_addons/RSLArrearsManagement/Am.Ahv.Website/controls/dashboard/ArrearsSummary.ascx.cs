using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using Am.Ahv.Utilities.constants;
using Am.Ahv.BusinessManager.Dashboard;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Website.pagebase;
namespace Am.Ahv.Website.controls.dashboard
{
    public partial class arrearssummary :UserControlBase
    {
        #region Attributes
        bool isError { set; get; }
        bool isException { set; get; }
        Am.Ahv.BusinessManager.Dashboard.arrearssummary ArrearsSummary = null;
        #endregion

        #region Events

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               /* commented by Adnan to speed up Dashboard
                LoadTenantCount();
                LoadLetPropertiesCount();
                LoadPaymentPlanCount();
                LoadArrearsCount();
                LoadPercentagePlan();
                LoadPercentageTenancies();
               */ 
            }
        }
        #endregion

        #endregion

        #region Utility Methods

        private void LoadTenantCount()
        {
            try
            {
                ArrearsSummary = new BusinessManager.Dashboard.arrearssummary();
                lbltenanciesinarreas.Text = ArrearsSummary.GetTenanciesCount().ToString() ;
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                    setMessage(UserMessageConstants.AStenantcount, true);
            }
        }
        private void LoadPaymentPlanCount()
        {
            try
            {
                
                ArrearsSummary = new BusinessManager.Dashboard.arrearssummary();
                lblpaymentplancount.Text = ArrearsSummary.GetPaymentPlanCount().ToString() ;

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                    setMessage(UserMessageConstants.ASpaymentplan, true);
            }
        }
        private void LoadArrearsCount()
        {
            try
            {
                ArrearsSummary = new BusinessManager.Dashboard.arrearssummary();
                decimal value = ArrearsSummary.GetArrearsCount();
                if (value != null)
                {
                    lblarrearsvalue.Text = value.ToString();
                }
                else
                {
                    lblarrearsvalue.Text = "0  :";
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                    setMessage(UserMessageConstants.ASarrearsvalue, true);
            }
        }
        private void LoadPercentagePlan()
        {
            try
            {
                ArrearsSummary = new BusinessManager.Dashboard.arrearssummary();
                lblpaymentpercentage.Text = ArrearsSummary.GetPlansPercentage().ToString();
            }
            catch (Exception ex)
            {
                isError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isError)
                    setMessage(UserMessageConstants.ASarrearsvalue, true);
            }
        }
        private void LoadPercentageTenancies()
        {
            try
            {
                int tenancyCount = int.Parse(lbltenanciesinarreas.Text);
                double letPropertiesCount = Convert.ToDouble(lblletpropcount.Text);
                lblpercentageten.Text = (Math.Round(((tenancyCount / letPropertiesCount) * 100), 2)).ToString();
                //ArrearsSummary = new BusinessManager.Dashboard.arrearssummary();
                //lblpercentageten.Text = ArrearsSummary.GetNegativeTenanciesValue().ToString();
            }
            catch (Exception ex)
            {
                isError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isError)
                    setMessage(UserMessageConstants.ASarrearsvalue, true);
            }
        }
        private void LoadLetPropertiesCount()
        {
            try
            {
                ArrearsSummary = new BusinessManager.Dashboard.arrearssummary();
                lblletpropcount.Text = ArrearsSummary.LetPropertiesCount().ToString();
            }
            catch (Exception ex)
            {
                isError = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isError)
                    setMessage(UserMessageConstants.ASarrearsvalue, true);
            }
        }
        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion
        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
        #endregion
    }
}