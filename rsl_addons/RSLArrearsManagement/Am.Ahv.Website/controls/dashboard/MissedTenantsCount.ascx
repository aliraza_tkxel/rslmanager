﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MissedTenantsCount.ascx.cs" Inherits="Am.Ahv.Website.controls.dashboard.MissedTenantsCount" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="updateMissedPaymentsAlertCount" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        
        <asp:Label ID="lblMissedPaymentsAlertCount" runat="server" CssClass="dashboardBigNumbersUP" ></asp:Label>
        
    </ContentTemplate>
</asp:UpdatePanel>