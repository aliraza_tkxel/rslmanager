﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GrossRentBalance.ascx.cs" Inherits="Am.Ahv.Website.controls.dashboard.GrossRentBalance" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="updateGrossRentBalance" runat="server" UpdateMode="Conditional">
    <ContentTemplate>  
    <div>      
        <asp:Label ID="lblGrossRentBalanceCount" runat="server" CssClass="dashboardsmallBalance" ></asp:Label>        
    </div>
    </ContentTemplate>

</asp:UpdatePanel>
