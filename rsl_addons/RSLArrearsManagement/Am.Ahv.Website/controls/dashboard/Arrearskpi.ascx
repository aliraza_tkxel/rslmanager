﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Arrearskpi.ascx.cs"
    Inherits="Am.Ahv.Website.controls.dashboard.arrearskpi" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server" Style="color: White;"></asp:Label>
</asp:Panel>
<asp:Panel ID="pnlarrearskpi" runat="server" Width="355" Height="148">
    <asp:UpdatePanel ID="uparrearskpi" UpdateMode="Conditional" runat="server">
        <ContentTemplate>           
            <asp:Label ID="lblNoRecord" runat="server" Text="No Record Found" ForeColor="Black" Font-Bold="true" Visible="false"></asp:Label>
                <br />
           <%-- <asp:Chart ID="Chart1" runat="server" Height="145" Palette="BrightPastel">
                <series>
                    <asp:Series Name="Series1" ChartType="Column" IsValueShownAsLabel="false">
                    </asp:Series>
                </series>
                <chartareas>
                    <asp:ChartArea Name="ChartArea1" BackGradientStyle="Center">
                    </asp:ChartArea>
                </chartareas>
            </asp:Chart>--%>
            <asp:Chart ID="Chart1" runat="server" Visible="true" Height="130" Width="170" Palette="BrightPastel">
                <Series>
                    <asp:Series Name="Series1" ChartType="Column"  ChartArea="ChartArea1" Color="Aqua" IsXValueIndexed="false" >                    
                    </asp:Series>


                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" BackGradientStyle="Center">
                        <Area3DStyle Enable3D="true" />
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
