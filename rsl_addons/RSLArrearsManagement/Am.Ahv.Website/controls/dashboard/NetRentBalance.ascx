﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NetRentBalance.ascx.cs" Inherits="Am.Ahv.Website.controls.dashboard.NetRentBalance" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="updateNetRentBalance" runat="server" UpdateMode="Conditional">
    <ContentTemplate>  
    <div>      
        <asp:Label ID="lblNetRentBalanceCount" runat="server" CssClass="dashboardsmallBalance" ></asp:Label>        
    </div>
    </ContentTemplate>

</asp:UpdatePanel>
