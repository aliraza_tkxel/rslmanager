﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyCases.ascx.cs" Inherits="Am.Ahv.Website.controls.dashboard.MyCases" %>

      <asp:Panel ID="pnlMessage" runat="server" Visible="false">
             <asp:Label ID="lblMessage" runat="server"></asp:Label>
      </asp:Panel>


 <asp:Panel ID="pnlMyCasesCount" runat="server">
    <asp:UpdatePanel ID="updateMyCasesCount" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Label ID="lblMyCasesCount" runat="server" CssClass="dashboardBigNumbersUP"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>


 
 