﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnticipatedHBBalance.ascx.cs" Inherits="Am.Ahv.Website.controls.dashboard.AnticipatedHBBalance" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="updateAnticipatedHBBalanceBalance" runat="server" UpdateMode="Conditional">
    <ContentTemplate>  
    <div>      
        <asp:Label ID="lblNetAnticipatedHBBalance" runat="server" CssClass="dashboardsmallBalance" ></asp:Label>        
    </div>
    </ContentTemplate>

</asp:UpdatePanel>
