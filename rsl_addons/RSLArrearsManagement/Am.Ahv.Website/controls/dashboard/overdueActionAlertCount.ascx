﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="overdueActionAlertCount.ascx.cs" Inherits="Am.Ahv.Website.controls.dashboard.overdueActionAlertCount" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="updateOverdueActionsAlertCount" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        
        <asp:Label ID="lblOverdueActionsAlertCount" runat="server" CssClass="dashboardBigNumbersUP" ></asp:Label>
        
    </ContentTemplate>
</asp:UpdatePanel>