﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.Entities;
using System.Web.UI.DataVisualization.Charting;
using Am.Ahv.BusinessManager.Dashboard;
using Am.Ahv.Utilities.constants;

using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Drawing;
namespace Am.Ahv.Website.controls.dashboard
{
    public partial class arrearskpi : System.Web.UI.UserControl
    {
        #region Attributes
        Am.Ahv.BusinessManager.Dashboard.arrearskpi Cw = null;
        bool isError { set; get; }
        bool isException { set; get; }
        int ResourceId { set; get; }
        #endregion

        #region Events
        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // ResourceId = 11;
               // LoadChart(ResourceId);
            }
        }
        #endregion
        #endregion
        #region Utility Methods

        #region LoadChart
        private void LoadChart(int ResourceID)
        {
            try
            {
                int Id = ResourceID;
                Cw = new BusinessManager.Dashboard.arrearskpi();
                List<AM_SP_Region_Amount_Result> list = Cw.GetRegionAmount(Id);
                if (list.Count > 0)
                {                    
                    Chart1.DataSource = list;
                    Chart1.Series["Series1"].XValueMember = ApplicationConstants.AKPIPatch;
                    Chart1.Series["Series1"].YValueMembers = ApplicationConstants.AKPITotalRent;
                    
                    Chart1.DataBind();
                    int count = 0;
                    foreach (DataPoint point in Chart1.Series["Series1"].Points)
                    {
                        if (list[count] != null)
                        {
                            point.Url = PathConstants.historicalBalanceListReportPath + "?cmd=ArrearsKPI&month=" + list[count].months.ToString() + "&year=" + list[count].years.ToString(); 
                            //Session[SessionConstants.monthArrearsKPI] = list[count].months.ToString();
                            //Session[SessionConstants.yearArrearsKPI] = list[count].years.ToString();
                            point.ToolTip = list[count].months + " " + list[count].years.ToString() + "\nGross  " + list[count].TotalRent.ToString();
                            count++;
                        }
                        else
                        {
                            break;
                        }
                    }
                    //Chart1.Visible = true;
                    lblNoRecord.Visible = false;
                    uparrearskpi.Update();
                }
                else
                {
                    //Chart1.Visible = false;
                    lblNoRecord.Visible = true;
                }
                uparrearskpi.Update();
            }
            catch (ArgumentException arg)
            {
                isException = true;
                ExceptionPolicy.HandleException(arg, "Exception Policy");
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally 
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.AKpichart, true);
                }
            }
        }
        #endregion
        #region ReLoadChart

        public void ReloadChart(int Resourceid)
        {
            LoadChart(Resourceid);
            uparrearskpi.Update();
        }
        #endregion
        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion
        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #endregion
    }
}