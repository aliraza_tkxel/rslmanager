﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Am.Ahv.BusinessManager.Casemgmt;
using Am.Ahv.Utilities.constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging;
using Am.Ahv.Website.pagebase;


namespace Am.Ahv.Website.controls.dashboard
{
    #region"Delegates"
    public delegate void MyCasesDelegate(string postCode,int caseOfficerId, int regionId, int suburbId);
    public delegate void ReviewListDelegate(int caseOfficerId);
    public delegate void OverdueStatus(int resourceId, int regionid, int suburbid);
    public delegate void EvacatStatus(int CaseOfficerId);
    public delegate void ArrearsKPIDelegate(int CaseOfficerId);
   // public delegate void FirstDetectionCountDelegate(int caseOfficerId, int regionId, int suburbId);
    public delegate void FirstDetectionListAlertCountDelegate(string postCode,int regionId, int suburbId,int resourceId);
    //public delegate void LatestFirstDetectionListAlertCountDelegate(string postCode,int regionId, int suburbId,int resourceId);
    public delegate void OverDueActions(int resourceId,int regionid,int suburbid);
    public delegate void MissedPaymentAlertCountDelegate(string postCode,int regionId, int suburbId,int resourceId);
    public delegate void GrossRentBalanceDelegate(string postCode, int regionId, int suburbId, int resourceId);
    public delegate void NetRentBalanceDelegate(string postCode, int regionId, int suburbId, int resourceId);
    public delegate void AnticipatedHBDelegate(string postCode, int regionId, int suburbId, int resourceId);
   
    #endregion
   
    public partial class DashboardSearch : UserControlBase
    {

        #region"Attributes"
        bool isError;
        bool isException;

        public event OverdueStatus showOverdueEvent;
        public event MyCasesDelegate showMyCasesEvent;
        public event ReviewListDelegate ReviewListEvent;
        public event EvacatStatus EvacatestatusEvent;
        public event ArrearsKPIDelegate AKPIEvent;
       // public event FirstDetectionCountDelegate FirstDetectionCountEvent;
        public event FirstDetectionListAlertCountDelegate FirstDetectionListAlertCountEvent;
        //public event FirstDetectionListAlertCountDelegate LatestFirstDetectionListAlertCountEvent;
        public event OverDueActions OverdueActionsAlterCountEvent;
        public event MissedPaymentAlertCountDelegate MissedPaymentAlertCountEvent;
        public event GrossRentBalanceDelegate GrossRentBalancedelegate;
        public event NetRentBalanceDelegate NetRentBalancedelegate;
        public event AnticipatedHBDelegate AnticipatedHBDelegate;
        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion

        #region"Events"

        #region"Page Load"

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    resetMessage();
                    if (!IsPostBack)
                    {
                        InitLookups();
                        LoadAllControls();
                    }
                }

                catch (Exception ex)
                {
                    isException = true;
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
                finally
                {
                    if (isException)
                    {
                        setMessage(UserMessageConstants.LoadingPageError, true);
                    }
                }
            }
        }

        #endregion

        #region"Btn Search Click"

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadAllControls();
            updpnlDashBoardSearch.Update();
        }

        #endregion

        #region"ddlCaseOwnedBy Selected Index Changed"

        protected void ddlCaseOwnedBy_SelectedIndexChanged(object sender, EventArgs e)
        {            
            try
            {
                Am.Ahv.BusinessManager.Dashboard.ReviewList reviewListmanager = new BusinessManager.Dashboard.ReviewList();
                if (ddlCaseOwnedBy.SelectedValue == "-1")
                {
                    ddlCaseRegion.DataSource = reviewListmanager.GetAllRegionList(); ;
                    ddlCaseRegion.DataTextField = ApplicationConstants.regionname;
                    ddlCaseRegion.DataValueField = ApplicationConstants.regionId;
                    ddlCaseRegion.DataBind();
                    ddlCaseRegion.Items.Add(new ListItem("All", "-1"));
                    ddlCaseRegion.SelectedValue = "-1";
                }
                else
                {
                    SuppressedCase sc = new SuppressedCase();
                    ddlCaseRegion.DataSource = sc.GetRegionList(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                    ddlCaseRegion.DataTextField = ApplicationConstants.regionname;
                    ddlCaseRegion.DataValueField = ApplicationConstants.regionId;
                    ddlCaseRegion.DataBind();
                    ddlCaseRegion.Items.Add(new ListItem("All", "-1"));
                    ddlCaseRegion.SelectedValue = "-1";
                }
                 if (ddlCaseSuburb.Items.Count > 0)
                {
                    ddlCaseSuburb.Items.Clear();
                }
                AddCaseWorker cw = new AddCaseWorker();
                ddlCaseSuburb.DataSource = cw.GetSuburbsList(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlCaseSuburb.DataTextField = ApplicationConstants.schememname;
                ddlCaseSuburb.DataValueField = ApplicationConstants.suburbId;
                ddlCaseSuburb.DataBind();
                ddlCaseSuburb.Items.Add(new ListItem("All", "-1"));
                ddlCaseSuburb.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.LoadingRegionError, true);
                }
            }
        }

        #endregion

        #region"ddlCaseRegion Selected Index Changed"

        protected void ddlCaseRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCaseSuburb.Items.Count > 0)
                {
                    ddlCaseSuburb.Items.Clear();
                }
                AddCaseWorker cw = new AddCaseWorker();
                ddlCaseSuburb.DataSource = cw.GetAllSuburbs(Convert.ToInt32(ddlCaseRegion.SelectedValue));
                ddlCaseSuburb.DataTextField = ApplicationConstants.schememname;
                ddlCaseSuburb.DataValueField = ApplicationConstants.schemeId ;
                ddlCaseSuburb.DataBind();
                ddlCaseSuburb.Items.Add(new ListItem("All", "-1"));
                ddlCaseSuburb.SelectedValue = "-1";
                updpnlDashBoardSearch.Update();

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.LoadingSuburbError, true);
                }
            }

        }

        #endregion

        #endregion

        #region "Functions"

        #region"Init Lookups"

        private void InitLookups()
        {

            try
            {

                SuppressedCase sc = new SuppressedCase();
                ddlCaseOwnedBy.DataSource = sc.CasesOwnedBy();
                ddlCaseOwnedBy.DataTextField = ApplicationConstants.EmloyeeName;
                ddlCaseOwnedBy.DataValueField = ApplicationConstants.ResourceId;
                ddlCaseOwnedBy.DataBind();
                ddlCaseOwnedBy.Items.Insert(0, new ListItem("All", "-1"));        
                ddlCaseOwnedBy.SelectedValue = base.GetCurrentLoggedinUser().ToString();
                
                ddlCaseRegion.DataSource = sc.GetRegionList(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlCaseRegion.DataTextField = ApplicationConstants.regionname;
                ddlCaseRegion.DataValueField = ApplicationConstants.regionId;
                ddlCaseRegion.DataBind();
                ddlCaseRegion.Items.Insert(0, new ListItem("All", "-1"));        
                ddlCaseRegion.SelectedValue = "-1";

                AddCaseWorker cw = new AddCaseWorker();
                ddlCaseSuburb.DataSource = cw.GetSuburbsList(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
                ddlCaseSuburb.DataTextField = ApplicationConstants.schememname;
                ddlCaseSuburb.DataValueField = ApplicationConstants.suburbId;
                ddlCaseSuburb.DataBind();
                ddlCaseSuburb.Items.Insert(0, new ListItem("All", "-1"));        
                ddlCaseSuburb.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.LoadingLookupError, true);
                }
            }

        }

        #endregion

        #endregion

        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Load All Controls"

        public void LoadAllControls()
        {
            Session[SessionConstants.PostCodeSearch] = Session[SessionConstants.PostCodeSearch] == null ? "" :txtPostCode.Text;
            ReviewListEvent(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
            showOverdueEvent(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue));
            EvacatestatusEvent(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
            AKPIEvent(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
            showMyCasesEvent(Session[SessionConstants.PostCodeSearch].ToString(),Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue));
           // FirstDetectionCountEvent(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue));
            FirstDetectionListAlertCountEvent(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
            //LatestFirstDetectionListAlertCountEvent(Session[SessionConstants.PostCodeSearch].ToString(),Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue),Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
            OverdueActionsAlterCountEvent(Convert.ToInt32(ddlCaseOwnedBy.SelectedValue), Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue));
            MissedPaymentAlertCountEvent(Session[SessionConstants.PostCodeSearch].ToString(),Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
            AnticipatedHBDelegate(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
            GrossRentBalancedelegate(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
            NetRentBalancedelegate(Session[SessionConstants.PostCodeSearch].ToString(), Convert.ToInt32(ddlCaseRegion.SelectedValue), Convert.ToInt32(ddlCaseSuburb.SelectedValue), Convert.ToInt32(ddlCaseOwnedBy.SelectedValue));
           
            
            SetSession();

        }

        #endregion

        #region"Set Session"

        public void SetSession()
        {
            Session[SessionConstants.CaseOwnedByDashboardSearch] = ddlCaseOwnedBy.SelectedValue;
            Session[SessionConstants.RegionDashboardSearch] = ddlCaseRegion.SelectedValue;
            Session[SessionConstants.SuburbDashboardSearch] = ddlCaseSuburb.SelectedValue;
            Session[SessionConstants.PostCodeSearch] = txtPostCode.Text;
        }

        #endregion
    }
}