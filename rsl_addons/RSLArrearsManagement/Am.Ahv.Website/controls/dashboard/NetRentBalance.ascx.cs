﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using System.Drawing;
using Am.Ahv.BusinessManager.lookup;
using Am.Ahv.BusinessManager.reports;

namespace Am.Ahv.Website.controls.dashboard
{
    public partial class NetRentBalance : System.Web.UI.UserControl
    {
        #region"Attributes"
        bool isError;
        bool isException;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IsException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion
        #region"Events"

        #region"Page Load"
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //if (GetUserType(base.GetCurrentLoggedinUser()) == ApplicationConstants.CaseOfficer)
                    //{
                    //    showFirstDetectionCount(0, 0);
                    //}
                    //else
                    //{
                    //    showFirstDetectionListAlertCount(-1, -1);
                    //}
                }
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.PageLoadError, true);
                }
            }

        }
        #endregion

        #endregion
        #region "Functions"

        #region "show Net Rent Balance"
        public void showNetRentBalance(string postCode, int regionId, int suburbId, int resourceId)
        {
            try
            {

                double netBalance = Convert.ToDouble(Session[SessionConstants.grossRentBalance].ToString()) - Convert.ToDouble(Session[SessionConstants.anticipatedHBBalance].ToString());
                lblNetRentBalanceCount.Text = "£" + Math.Round(netBalance, 0).ToString(); 
                updateNetRentBalance.Update();

            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.CountingErrorFirstDetection, true);
                }
            }
        }
        #endregion

        #endregion
        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Get User Type"

        private string GetUserType(int userId)
        {
            Lookup lookupManager = new Lookup();
            return lookupManager.GetUserType(userId);

        }

        #endregion
    }
}