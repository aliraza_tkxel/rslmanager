﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager.Dashboard;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities;
using Am.Ahv.Website.pagebase;
using Am.Ahv.Utilities.constants;
namespace Am.Ahv.Website.controls.dashboard
{
    public partial class overduevacatedash : UserControlBase
    {

        #region Attributes
        bool isError { set; get; }
        bool isException { set; get; }
        int ResourceId { set; get; }
        vacteoverdue Manager = null;
        #endregion
        #region Events
        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //ResourceId = 5;
                //LoadOverdueCaseCount(ResourceId);
              
            }
        }
        #endregion
        #endregion

        #region Utility Methods
        #region Load Overdue Case Count

        private void LoadOverdueCaseCount(int ResourceId, int regionid, int suburbid)
        {
            try
            {
                //int ID = ResrouceId;
                Manager = new vacteoverdue();
                this.lbloverduecount.Text = Manager.GetCaseOverDueCount(ResourceId, regionid, suburbid).ToString();
            }
           
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.DBoverdue, true);
                }
            }

        }
       
        #endregion
        #region ReloadControl
        public void ReloadControl(int ResourceId, int regionid, int suburbid)
        {
            LoadOverdueCaseCount(ResourceId, regionid, suburbid);
            upoverduestatus.Update();
        }
        #endregion
        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion
        #endregion
        
    }
}