﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Am.Ahv.BusinessManager;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Am.Ahv.Utilities.constants;
using System.Drawing;
using Am.Ahv.Entities;
using Am.Ahv.BusinessManager.lookup;
using Am.Ahv.BusinessManager.payments;

namespace Am.Ahv.Website.controls.dashboard
{
    public partial class MissedTenantsCount : System.Web.UI.UserControl
    {
        #region"Attributes"
        bool isError;
        bool isException;

        public bool IsError
        {
            get { return isError; }
            set { isError = value; }
        }
        public bool IssException
        {
            get { return isException; }
            set { isException = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region "Functions"


        #endregion


        #region "show Latest First Detection Count"
        public void showMissedPaymentAlertCount(string postCode,int regionId, int suburbId,int resourceId)
        {
            try
            {
                bool allRegionFlag = (regionId > 0 ? false : true);
                bool allOwnerFlag = (resourceId > 0 ? false : true);
                bool allSuburbFlag = (suburbId > 0 ? false : true);
                Payments MissedPayments = new Payments();
                lblMissedPaymentsAlertCount.Text = MissedPayments.GetPaymentPlanMonitoringCount(postCode, resourceId, regionId, suburbId, allOwnerFlag, allRegionFlag, allSuburbFlag, "misspayments").ToString();
                updateMissedPaymentsAlertCount.Update();
            }
            catch (Exception ex)
            {
                isException = true;
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            finally
            {
                if (isException)
                {
                    setMessage(UserMessageConstants.CountingErrorFirstDetection, true);
                }
            }
        }
        #endregion



        #region"Reset Message"

        public void resetMessage()
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
        }

        #endregion

        #region"Set Message"

        public void setMessage(string str, bool isError)
        {
            this.isError = isError;
            if (isError == true)
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        #endregion

        #region"Get User Type"

        private string GetUserType(int userId)
        {
            Lookup lookupManager = new Lookup();
            return lookupManager.GetUserType(userId);

        }

        #endregion
    }
}

