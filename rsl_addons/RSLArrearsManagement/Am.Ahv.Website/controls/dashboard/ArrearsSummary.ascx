﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArrearsSummary.ascx.cs"
    Inherits="Am.Ahv.Website.controls.dashboard.arrearssummary" %>
<asp:Panel ID="pnlMessage" runat="server" Visible="false">
    <asp:Label ID="lblMessage" runat="server" Style="color: White;"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="upoverduestatus" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
      <div style="overflow:hidden;">
        <table border="0" width="100%" cellpadding="0" style="border-collapse: collapse">
             <tr>
                <td  width="66" class="dashboardArrearsSummaryNumber">
                    <asp:Label ID="lblletpropcount" runat="server"></asp:Label>
                </td>
                <td width="18" class="dashboardArrearsSummaryNumber">:</td>
                <td height="20" class="dashboardArrearsSummary">
                    <asp:Label ID="lblletprop" runat="server" Text="Let Properties"></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td  width="66" class="dashboardArrearsSummaryNumber">
                    <asp:Label ID="lbltenanciesinarreas" runat="server"></asp:Label>
                </td>
                <td width="18" class="dashboardArrearsSummaryNumber">:</td>
                <td height="20" class="dashboardArrearsSummary">
                    <asp:Label ID="lbltenanciescount" runat="server" Text="Tenancies in Arrears"></asp:Label>
                </td>
            </tr>
             <tr>
                <td  width="66" class="dashboardArrearsSummaryNumber">
                    <asp:Label ID="lblpercentageten" runat="server"></asp:Label>
                </td>
                <td width="18" class="dashboardArrearsSummaryNumber">:</td>
                <td height="20" class="dashboardArrearsSummary">
                    <asp:Label ID="lblpercent" runat="server" Text="% Tenancies in Arrears"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="66" class="dashboardArrearsSummaryNumber">
                    <asp:Label ID="lblpaymentplancount" runat="server"></asp:Label>
                </td>
                <td width="18" class="dashboardArrearsSummaryNumber">:</td>
                <td height="20" class="dashboardArrearsSummary">
                    <asp:Label ID="lblpaymentplan" runat="server" Text="Payment plan"></asp:Label>
                </td>
            </tr>
              <tr>
                <td width="66" class="dashboardArrearsSummaryNumber">
                    <asp:Label ID="lblpaymentpercentage" runat="server"></asp:Label>
                </td>
                <td width="18" class="dashboardArrearsSummaryNumber">:</td>
                <td height="20" class="dashboardArrearsSummary">
                    <asp:Label ID="lblpaymentpercentageusr" runat="server" Text="% plans against cases"></asp:Label>
                </td>
            </tr>
            <tr>
                <td width="66" class="dashboardArrearsSummaryNumber">
                    <asp:Label ID="lblarrearsvalue" runat="server"></asp:Label>
                </td>
                <td width="18" class="dashboardArrearsSummaryNumber">:</td>
                <td  height="20" class="dashboardArrearsSummary" >
                    <asp:Label ID="lblarrearsvalueusr" runat="server" Text="Value of arrears cases"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
